# Tm Service

## Technical debt/custom integration :

1/ Garder un oeil sur core.report.api : il y a une famille de classes qui commencent par org.springframework et qui se chargent de l'intégration Spring MVC - Jasper Reports, qui marchent mais ne sont plus maintenues

2/ Voir si la classe org.squashtest.tm.domain.jpql.FixedSessionHolders est toujours nécessaire après la prochaine maj de QueryDSL et Hibernate. Le detail est dans les commentaires de la classe.

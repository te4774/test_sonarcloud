/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.campaign;

import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.service.statistics.CountOnEnum;

import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.stream.Collectors;

public final class IterationTestInventoryStatistics {

	private String iterationName;
	private CountOnEnum<ExecutionStatus> statistics;


	public IterationTestInventoryStatistics() {
		statistics = new CountOnEnum<>(getHandledStatuses());
	}

	public static Set<ExecutionStatus> getHandledStatuses() {
		return EnumSet.allOf(ExecutionStatus.class).stream()
			.map(ExecutionStatus::getCanonicalStatus)
			.collect(Collectors.toSet());
	}

	public String getIterationName() {
		return iterationName;
	}

	public void setIterationName(String iterationName) {
		this.iterationName = iterationName;
	}

	public LinkedHashMap<ExecutionStatus, Integer> getStatistics() {
		return statistics.getStatistics();
	}

	public void setNumber(int intValue, ExecutionStatus status) {
		this.statistics.add(status.getCanonicalStatus(), intValue);
	}

}

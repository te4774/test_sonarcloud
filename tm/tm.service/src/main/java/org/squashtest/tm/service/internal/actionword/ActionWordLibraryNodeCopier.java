/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.actionword;

import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.actionword.ActionWordLibraryNode;
import org.squashtest.tm.domain.actionword.ActionWordTreeEntity;
import org.squashtest.tm.domain.tree.TreeLibraryNode;
import org.squashtest.tm.service.internal.repository.ActionWordLibraryNodeDao;

import java.util.ArrayList;
import java.util.List;

@Component
public class ActionWordLibraryNodeCopier {

	private final ActionWordLibraryNodeDao actionWordLibraryNodeDao;

	public ActionWordLibraryNodeCopier(ActionWordLibraryNodeDao actionWordLibraryNodeDao) {
		this.actionWordLibraryNodeDao = actionWordLibraryNodeDao;
	}

	public boolean simulateCopyNodes(List<ActionWordLibraryNode> nodes, ActionWordLibraryNode target, boolean caseInsensitivity) {
		return checkIfAllNodesCanBeCopiedInTarget(nodes, target, caseInsensitivity);
	}

	private boolean checkIfAllNodesCanBeCopiedInTarget(List<ActionWordLibraryNode> nodes, ActionWordLibraryNode target, boolean caseInsensitivity) {
		return nodes.size() == actionWordLibraryNodeDao.findAllCanBeCopiedNodesInTarget(nodes, target, caseInsensitivity).size();
	}

	public List<ActionWordLibraryNode> copyNodes(List<ActionWordLibraryNode> nodes, ActionWordLibraryNode target, boolean caseInsensitivity) {
		List<Long> allCanBeCopiedNodeIds = actionWordLibraryNodeDao.findAllCanBeCopiedNodesInTarget(nodes, target, caseInsensitivity);
		List<ActionWordLibraryNode> copiedNodes = new ArrayList<>();
		for (ActionWordLibraryNode node : nodes) {
			if (allCanBeCopiedNodeIds.contains(node.getId())) {
				ActionWordLibraryNode copy = createFirstLayerCopy(node, target, caseInsensitivity);
				target.addChild(copy, caseInsensitivity);
				copiedNodes.add(copy);
			}
		}
		return copiedNodes;
	}

	private ActionWordLibraryNode createFirstLayerCopy(ActionWordLibraryNode node, ActionWordLibraryNode target, boolean caseInsensitivity) {
		ActionWordLibraryNode copy = createBasicCopy(node, target);
		for (TreeLibraryNode child : node.getChildren()) {
			createSubTreeCopy((ActionWordLibraryNode) child,copy, caseInsensitivity);
		}
		return copy;
	}

	private ActionWordLibraryNode createSubTreeCopy(ActionWordLibraryNode node, ActionWordLibraryNode target, boolean caseInsensitivity) {
		ActionWordLibraryNode copy = createBasicCopy(node, target);
		target.addChild(copy, caseInsensitivity);
		for (TreeLibraryNode child : node.getChildren()) {
			createSubTreeCopy((ActionWordLibraryNode) child,copy, caseInsensitivity);
		}
		return copy;
	}

	private ActionWordLibraryNode createBasicCopy(ActionWordLibraryNode node, ActionWordLibraryNode target) {
		ActionWordLibraryNode copy = new ActionWordLibraryNode();
		copy.setLibrary(target.getLibrary());
		copy.setName(node.getName());
		copyTreeEntity(node, copy);
		return copy;
	}

	private void copyTreeEntity(ActionWordLibraryNode node, ActionWordLibraryNode copy) {
		ActionWordTreeEntity treeEntity = node.getEntity().createCopy();
		treeEntity.setProject(copy.getLibrary().getProject());
		copy.setEntity(treeEntity);
		copy.setEntityType(node.getEntityType());
	}

}

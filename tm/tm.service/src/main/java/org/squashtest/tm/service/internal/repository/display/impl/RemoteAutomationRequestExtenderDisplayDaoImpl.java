/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.AutomationRequestDto;
import org.squashtest.tm.service.internal.display.dto.RemoteAutomationRequestExtenderDto;
import org.squashtest.tm.service.internal.repository.display.RemoteAutomationRequestExtenderDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import static org.squashtest.tm.jooq.domain.tables.RemoteAutomationRequestExtender.REMOTE_AUTOMATION_REQUEST_EXTENDER;

@Repository
public class RemoteAutomationRequestExtenderDisplayDaoImpl implements RemoteAutomationRequestExtenderDisplayDao {


	DSLContext dslContext;

	public RemoteAutomationRequestExtenderDisplayDaoImpl(DSLContext dslContext) {
		this.dslContext = dslContext;
	}

	@Override
	public void addRemoteExtender(AutomationRequestDto automationRequestDto) {

		Record record = dslContext.select(REMOTE_AUTOMATION_REQUEST_EXTENDER.REMOTE_AUTOMATION_REQUEST_EXTENDER_ID.as(RequestAliasesConstants.ID),
			REMOTE_AUTOMATION_REQUEST_EXTENDER.SERVER_ID, REMOTE_AUTOMATION_REQUEST_EXTENDER.REMOTE_STATUS,
			REMOTE_AUTOMATION_REQUEST_EXTENDER.REMOTE_ISSUE_KEY, REMOTE_AUTOMATION_REQUEST_EXTENDER.REMOTE_REQUEST_URL,
			REMOTE_AUTOMATION_REQUEST_EXTENDER.REMOTE_ASSIGNED_TO, REMOTE_AUTOMATION_REQUEST_EXTENDER.SENT_VALUE_FOR_SYNC,
			REMOTE_AUTOMATION_REQUEST_EXTENDER.SYNCHRONIZABLE_ISSUE_STATUS, REMOTE_AUTOMATION_REQUEST_EXTENDER.LAST_SYNC_DATE_SQUASH)
			.from(REMOTE_AUTOMATION_REQUEST_EXTENDER)
			.where(REMOTE_AUTOMATION_REQUEST_EXTENDER.AUTOMATION_REQUEST_ID.eq(automationRequestDto.getId()))
			.fetchOne();

		RemoteAutomationRequestExtenderDto extender = null;
		if (record != null) {
			extender = record.into(RemoteAutomationRequestExtenderDto.class);
		}

		automationRequestDto.setExtender(extender);

	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.iteration;

import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.planning.StandardWorkloadCalendar;
import org.squashtest.tm.domain.planning.WorkloadCalendar;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.service.statistics.CountOnEnum;
import org.squashtest.tm.service.statistics.campaign.IterationTestInventoryStatistics;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;

public class TestSuiteTestInventoryStatistics {

	private String testsuiteName = "";
	private Date scheduledStart;
	private Date scheduledEnd;

	private final CountOnEnum<ExecutionStatus> statusStatistics;
	private final CountOnEnum<TestCaseImportance> importanceStatistics;

	public TestSuiteTestInventoryStatistics() {
		this.statusStatistics = new CountOnEnum<>(IterationTestInventoryStatistics.getHandledStatuses());
		this.importanceStatistics = new CountOnEnum<>(TestCaseImportance.class);
	}

	public String getTestsuiteName() {
		return testsuiteName;
	}

	public void setTestsuiteName(String testsuiteName) {
		this.testsuiteName = testsuiteName;
	}

	public int getNbTotal() {
		return this.statusStatistics.calculateTotal();
	}

	public int getNbToExecute() {
		return this.statusStatistics.keep(ExecutionStatus.getNonTerminatedStatusSet()).calculateTotal();
	}

	public int getNbExecuted() {
		return this.statusStatistics.keep(ExecutionStatus.getTerminatedStatusSet()).calculateTotal();
	}

	public float getPcProgress() {
		return Math.round((float) getNbExecuted() / (float) getNbTotal() * 10000) / (float) 100;
	}

	public float getPcSuccess() {
		int success = this.statusStatistics
			.keep(ExecutionStatus.getSuccessStatusSet())
			.calculateTotal();
		return Math.round((float) success / (float) getNbExecuted() * 10000) / (float) 100;
	}

	public float getPcFailure() {
		int failure = this.statusStatistics
			.keep(Collections.singleton(ExecutionStatus.FAILURE))
			.calculateTotal();
		return Math.round((float) failure / (float) getNbExecuted() * 10000) / (float) 100;
	}

	public float getPcPrevProgress() {
		float nbToExecuteToDate = nbOfTestsToExecuteToDate(scheduledStart, scheduledEnd, new Date(), getNbTotal());
		float pcPrevProgress;
		// next line : SONAR says that's how you compare floats with 0.0f
		if (Float.floatToRawIntBits(nbToExecuteToDate) == 0) {
			pcPrevProgress = getPcProgress();
		} else {
			pcPrevProgress = Math.round(getNbExecuted() / nbOfTestsToExecuteToDate(scheduledStart, scheduledEnd,
					new Date(), getNbTotal()) * 10000) / (float) 100;
		}
		return Math.min(pcPrevProgress, (float) 100);
	}

	public int getNbPrevToExecute() {
		return (int) nbOfTestsToExecuteToDate(scheduledStart, scheduledEnd, new Date(), getNbTotal()) - getNbExecuted();
	}

	public LinkedHashMap<ExecutionStatus, Integer> getStatusStatistics() {
		return statusStatistics.getStatistics();
	}

	public LinkedHashMap<TestCaseImportance, Integer> getImportanceStatistics() {
		return importanceStatistics.getStatistics();
	}

	private float nbOfTestsToExecuteToDate(Date scheduledStart, Date scheduledEnd, Date currentDate, int nbTests) {

		float result;

		// if current date is before the start of the previsional schedule
		if (scheduledStart == null || scheduledEnd == null || currentDate.before(scheduledStart)) {
			result = 0.00f;
			// if current date is after the end of the execution schedule
		} else if (currentDate.after(scheduledEnd)) {
			result = nbTests;
		} else {

			// Get total number of business days
			WorkloadCalendar workloadCalendar = new StandardWorkloadCalendar();
			float totalNumberOfBusinessDays = workloadCalendar.getWorkload(scheduledStart, scheduledEnd);

			// Get number of open days before current date
			float numberOfSpentBusinessDays = workloadCalendar.getWorkload(scheduledStart, currentDate);

			// Compute percentage of already spent time
			float spentTime = numberOfSpentBusinessDays / totalNumberOfBusinessDays;

			result = nbTests * spentTime;
		}
		return result;
	}

	public Date getScheduledStart() {
		return scheduledStart;
	}

	public void setScheduledStart(Date scheduledStart) {
		this.scheduledStart = scheduledStart;
	}

	public Date getScheduledEnd() {
		return scheduledEnd;
	}

	public void setScheduledEnd(Date scheduledEnd) {
		this.scheduledEnd = scheduledEnd;
	}

	public void addExecutionCount(int nb, ExecutionStatus status, TestCaseImportance importance) {
		this.statusStatistics.add(status, nb);
		if(!status.isTerminatedStatus()){
			this.importanceStatistics.add(importance, nb);
		}
	}
}

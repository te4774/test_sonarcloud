/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.repository.display.KeywordTestStepDisplayDao;

import java.util.Collection;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_STEP;

@Repository
public class KeywordTestStepDisplayDaoImpl implements KeywordTestStepDisplayDao {

	private final DSLContext dslContext;

	public KeywordTestStepDisplayDaoImpl(DSLContext dslContext) {
		this.dslContext = dslContext;
	}

	@Override
	public Collection<Long> displayProjectsIdsByKeywordStepIds(List<Long> keywordStepIds) {
		return dslContext.select(ACTION_WORD.PROJECT_ID)
			.from(ACTION_WORD)
			.join(KEYWORD_TEST_STEP)
			.on(KEYWORD_TEST_STEP.ACTION_WORD_ID.equal(ACTION_WORD.ACTION_WORD_ID))
			.where(KEYWORD_TEST_STEP.TEST_STEP_ID.in(keywordStepIds))
			.fetchInto(Long.class);
	}
}

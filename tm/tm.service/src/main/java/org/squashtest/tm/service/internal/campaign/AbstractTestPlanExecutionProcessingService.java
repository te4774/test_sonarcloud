/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.campaign;

import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.squashtest.tm.core.foundation.exception.ActionException;
import org.squashtest.tm.core.foundation.lang.Wrapped;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestPlanOwner;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.script.GherkinParser;
import org.squashtest.tm.domain.testcase.KeywordTestCase;
import org.squashtest.tm.domain.testcase.ScriptedTestCase;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseVisitor;
import org.squashtest.tm.exception.execution.TestPlanItemNotExecutableException;
import org.squashtest.tm.exception.execution.TestPlanTerminatedOrNoStepsException;
import org.squashtest.tm.service.campaign.TestPlanExecutionProcessingService;
import org.squashtest.tm.service.internal.display.dto.execution.TestPlanResume;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.security.SecurityCheckableObject;
import org.squashtest.tm.service.user.UserAccountService;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

/**
 * Abstract class for services whose purposes is to process executions of a test plan
 * @param <E> class of the test plan owner
 * @author aguilhem
 */
public abstract class AbstractTestPlanExecutionProcessingService<E extends TestPlanOwner> implements TestPlanExecutionProcessingService<E> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTestPlanExecutionProcessingService.class);

	private CampaignNodeDeletionHandler campaignDeletionHandler;

	private IterationTestPlanManager testPlanManager;

	private UserAccountService userService;

	private PermissionEvaluationService permissionEvaluationService;

	protected IterationTestPlanDao iterationTestPlanDao;

	protected DSLContext dslContext;

	// Injection is made through constructor to allow simple mock injection in unit test.
	@Inject
	AbstractTestPlanExecutionProcessingService(CampaignNodeDeletionHandler campaignDeletionHandler, IterationTestPlanManager testPlanManager,
											   UserAccountService userService, PermissionEvaluationService permissionEvaluationService,
											   IterationTestPlanDao iterationTestPlanDao, DSLContext dslContext) {
		this.campaignDeletionHandler = campaignDeletionHandler;
		this.testPlanManager = testPlanManager;
		this.userService = userService;
		this.permissionEvaluationService = permissionEvaluationService;
		this.iterationTestPlanDao = iterationTestPlanDao;
		this.dslContext = dslContext;
	}

	@Override
	public void deleteAllExecutions(long testPlanOwnerId) {
		// getTest plan
		E testPlanOwner = getTestPlanOwner(testPlanOwnerId);
		List<IterationTestPlanItem> testPlan = getTestPlan(testPlanOwner);
		if (!testPlan.isEmpty()) {
			// delete all executions
			deleteAllExecutionsOfTestPlan(testPlan, testPlanOwner);
		}
	}

	private void deleteAllExecutionsOfTestPlan(List<IterationTestPlanItem> testPlan, E testPlanOwner) {
		String testerLogin = findUserLoginIfTester(testPlanOwner);
		for (IterationTestPlanItem iterationTestPlanItem : testPlan) {
			if (testerLogin == null
				|| iterationTestPlanItem.getUser() != null && iterationTestPlanItem.getUser().getLogin()
				.equals(testerLogin)) {
				List<Execution> executions = iterationTestPlanItem.getExecutions();
				if (!executions.isEmpty()) {
					campaignDeletionHandler.deleteExecutions(executions);
				}
			}
		}
	}

	@Override
	public Execution startResumeNextExecution(long testPlanOwnerId, long testPlanItemId) {
		Execution execution;
		E testPlanOwner = getTestPlanOwner(testPlanOwnerId);
		String testerLogin = findUserLoginIfTester(testPlanOwner);
		IterationTestPlanItem item = findNextExecutableTestPlanItem(testPlanOwner, testPlanItemId, testerLogin);
		execution = findUnexecutedOrCreateExecution(item);
		while (execution == null || execution.getSteps().isEmpty()) {
			item = findNextExecutableTestPlanItem(testPlanOwner, item.getId());
			execution = findUnexecutedOrCreateExecution(item);
		}
		return execution;
	}

	/**
	 * Same as {@link #startResumeNextExecution(long, long)} but resumes among filtered test plan.
	 * @param partialTestPlan partial test plan as a List of {@link IterationTestPlanItem}
	 */
	private Execution startResumeNextExecutionOfFilteredTestPlan(long testPlanOwnerId, long testPlanItemId, List<IterationTestPlanItem> partialTestPlan) {
		E transientTestPlanOwner = createTransientTestPlanOwnerWithFilteredTestPlan(testPlanOwnerId, partialTestPlan);
		String testerLogin = findUserLoginIfTester(testPlanOwnerId, transientTestPlanOwner.getClass().getName());
		IterationTestPlanItem item = findNextExecutableTestPlanItem(transientTestPlanOwner, testPlanItemId, testerLogin);
		Execution execution;
		execution = findUnexecutedOrCreateExecution(item);
		while (execution == null || execution.getSteps().isEmpty()) {
			item = findNextExecutableTestPlanItem(transientTestPlanOwner, item.getId());
			execution = findUnexecutedOrCreateExecution(item);
		}
		return execution;
	}

	private boolean partialTestPlanIdsHasNextTestCase(long testPlanOwnerId, Execution execution, List<IterationTestPlanItem> partialTestPlan) {
		boolean hasNextTestCase = false;
		if (nonNull(execution)) {
			hasNextTestCase = partialTestPlanIdsHaveMoreExecutableItems(testPlanOwnerId, execution.getTestPlan().getId(), partialTestPlan);
		}
		return hasNextTestCase;
	}

	private boolean partialTestPlanHasNextTestCase(long testPlanOwnerId, Execution execution, List<IterationTestPlanItem> partialTestPlan) {
		boolean hasNextTestCase = false;
		if (nonNull(execution)) {
			hasNextTestCase = partialTestPlanHasMoreExecutableItems(testPlanOwnerId, execution.getTestPlan().getId(), partialTestPlan);
		}
		return hasNextTestCase;
	}

	@Override
	public Execution startResume(long testPlanOwnerId) {
		Execution execution;
		E testPlanOwner = getTestPlanOwner(testPlanOwnerId);
		String testerLogin = findUserLoginIfTester(testPlanOwner);
		IterationTestPlanItem item = findFirstExecutableTestPlanItem(testerLogin, testPlanOwner);
		execution = findUnexecutedOrCreateExecution(item);
		if (execution == null || execution.getSteps().isEmpty()) {
			startResumeNextExecution(testPlanOwnerId, item.getId());
		}
		return execution;
	}

	private Execution startResumePartialTestPlan(long testPlanOwnerId, List<IterationTestPlanItem> partialTestPlan ) {
		E transientTestPlanOwner = createTransientTestPlanOwnerWithFilteredTestPlan(testPlanOwnerId, partialTestPlan);
		String testerLogin = findUserLoginIfTester(testPlanOwnerId, transientTestPlanOwner.getClass().getName());
		IterationTestPlanItem item = findFirstExecutableTestPlanItem(testerLogin, transientTestPlanOwner);
		Execution execution;
		execution = findUnexecutedOrCreateExecution(item);
		if (execution == null || execution.getSteps().isEmpty()) {
			startResumeNextExecution(testPlanOwnerId, item.getId());
		}
		return execution;
	}

	private Execution checkValidityThenResumeFilteredTestPlan(long iterationId, List<IterationTestPlanItem> partialTestPlan) {
		boolean hasDeletedTestCaseInFilteredSelection = hasDeletedTestCaseInFilteredSelection(partialTestPlan);
		if (!hasDeletedTestCaseInFilteredSelection) {
			try {
				return startResumePartialTestPlan(iterationId, partialTestPlan);
			} catch (TestPlanItemNotExecutableException ex) {
				throw new TestPlanTerminatedOrNoStepsException(ex);
			}
		} else {
			throw getTestPlanHasDeletedTestCaseException();
		}
	}

	abstract ActionException getTestPlanHasDeletedTestCaseException();

	/**
	 * Given a {@link TestPlanOwner}, retrieves its test plan items filtered with the given {@link GridFilterValue} list.
	 * @param testPlanOwnerId the {@link TestPlanOwner}
	 * @param filters the filters
	 * @return the test plan items of the given {@link TestPlanOwner} filtered with the given {@link GridFilterValue} list.
	 */
	abstract List<IterationTestPlanItem> getFilteredTestPlan(long testPlanOwnerId, List<GridFilterValue> filters);

	protected GridRequest prepareNonPaginatedGridRequest(List<GridFilterValue> filters) {
		GridRequest gridRequest = new GridRequest();
		gridRequest.setFilterValues(filters);
		return gridRequest.toNonPaginatedRequest();
	}

	protected List<Long> extractItemIdsFromGridResponse(GridResponse gridResponse) {
		return gridResponse.getDataRows().stream()
			.map(row -> Long.valueOf(row.getId()))
			.collect(Collectors.toList());
	}

	protected List<Long> extractItemIdsFromPartialTestPlan(List<IterationTestPlanItem> partialTestPlan) {
		return partialTestPlan.stream()
			.map(IterationTestPlanItem::getId)
			.collect(Collectors.toList());
	}

	@Override
	public Execution relaunch(long testPlanOwnerId) {
		deleteAllExecutions(testPlanOwnerId);
		return startResume(testPlanOwnerId);
	}

	@Override
	public TestPlanResume relaunchFilteredTestPlan(long testPlanOwnerId, List<GridFilterValue> filters) {
		List<IterationTestPlanItem> partialTestPlan = getFilteredTestPlan(testPlanOwnerId, filters);
		Execution execution = checkValidityThenRelaunchFilteredTestPlan(testPlanOwnerId, partialTestPlan);
		boolean hasNextTestCase = partialTestPlanHasNextTestCase(testPlanOwnerId, execution, partialTestPlan);
		return createNewTestPlanResume(testPlanOwnerId, execution, hasNextTestCase, extractItemIdsFromPartialTestPlan(partialTestPlan));
	}

	private Execution checkValidityThenRelaunchFilteredTestPlan(long testPlanOwnerId, List<IterationTestPlanItem> partialTestPlan) {
		boolean hasDeletedTestCaseInFilteredSelection = hasDeletedTestCaseInFilteredSelection(partialTestPlan);
		if (!hasDeletedTestCaseInFilteredSelection) {
			try {
				return startRelaunchPartialTestPlan(testPlanOwnerId, partialTestPlan);
			} catch (TestPlanItemNotExecutableException ex) {
				throw new TestPlanTerminatedOrNoStepsException(ex);
			}
		} else {
			throw getTestPlanHasDeletedTestCaseException();
		}
	}

	private Execution startRelaunchPartialTestPlan(long testPlanOwnerId, List<IterationTestPlanItem> partialTestPlan) {
		deleteAllExecutionsOfPartialTestPlan(partialTestPlan, testPlanOwnerId);
		return startResumePartialTestPlan(testPlanOwnerId, partialTestPlan);
	}

	private void deleteAllExecutionsOfPartialTestPlan(List<IterationTestPlanItem> partialTestPlan, long testPlanOwnerId) {
		E testPlanOwner = getTestPlanOwner(testPlanOwnerId);
		deleteAllExecutionsOfTestPlan(partialTestPlan, testPlanOwner);
	}

	@Override
	public boolean hasMoreExecutableItems(long testPlanOwnerId, long testPlanItemId) {
		E testPlanOwner = getTestPlanOwner(testPlanOwnerId);
		String testerLogin = findUserLoginIfTester(testPlanOwner);
		return !isLastExecutableTestPlanItem(testPlanOwner, testPlanItemId, testerLogin);
	}

	private boolean partialTestPlanIdsHaveMoreExecutableItems(long testPlanOwnerId, Long testPlanItemId, List<IterationTestPlanItem> partialTestPlan) {
		E transientTestPlanOwner = createTransientTestPlanOwnerWithFilteredTestPlan(testPlanOwnerId, partialTestPlan);
		String testerLogin = findUserLoginIfTester(testPlanOwnerId, transientTestPlanOwner.getClass().getName());
		return !isLastExecutableTestPlanItem(transientTestPlanOwner, testPlanItemId, testerLogin);
	}

	private boolean partialTestPlanHasMoreExecutableItems(long testPlanOwnerId, Long testPlanItemId, List<IterationTestPlanItem> partialTestPlan) {
		E transientTestPlanOwner = createTransientTestPlanOwnerWithFilteredTestPlan(testPlanOwnerId, partialTestPlan);
		String testerLogin = findUserLoginIfTester(testPlanOwnerId, transientTestPlanOwner.getClass().getName());
		return !isLastExecutableTestPlanItem(transientTestPlanOwner, testPlanItemId, testerLogin);
	}

	@Override
	public boolean hasPreviousExecutableItems(long testPlanOwnerId, long testPlanItemId) {
		E testPlanOwner = getTestPlanOwner(testPlanOwnerId);
		String testerLogin = findUserLoginIfTester(testPlanOwner);
		return !isFirstExecutableTestPlanItem(testPlanOwner, testPlanItemId, testerLogin);
	}

	/**
	 * Getter for a {@link TestPlanOwner} with th given id
	 * @param testPlanOwnerId the wanted {@link TestPlanOwner} id
	 * @return the {@link TestPlanOwner} with the given id
	 */
	abstract E getTestPlanOwner(long testPlanOwnerId);

	abstract E createTransientTestPlanOwnerWithFilteredTestPlan(long testPlanOwnerId, List<IterationTestPlanItem> filteredTestPLan);

	/**
	 * Getter for the {@link TestPlanOwner}'s test plan
	 * @param testPlanOwner the {@link TestPlanOwner} from whom we want the test plan
	 * @return the given {@link TestPlanOwner}'s test plan
	 */
	abstract List<IterationTestPlanItem> getTestPlan(E testPlanOwner);

	/**
	 * Find the first executable {@link IterationTestPlanItem}, for the given {@link TestPlanOwner}, assigned to the {@link org.squashtest.tm.domain.users.User} with the given testerLogin
	 * @param testerLogin a {@link org.squashtest.tm.domain.users.User} login
	 * @param testPlanOwner a {@link TestPlanOwner}
	 * @return the first executable {@link IterationTestPlanItem}, for the given {@link TestPlanOwner}, assigned to the {@link org.squashtest.tm.domain.users.User} with the given testerLogin
	 */
	abstract IterationTestPlanItem findFirstExecutableTestPlanItem(String testerLogin, E testPlanOwner);

	/**
	 * Determine if the {@link IterationTestPlanItem} with the given testPlanItemId is the last executable {@link IterationTestPlanItem}
	 * assigned to the {@link org.squashtest.tm.domain.users.User} with the given testerLogin in the {@link TestPlanOwner}'s test plan
	 * @param testPlanOwner a {@link TestPlanOwner}
	 * @param testPlanItemId {@link IterationTestPlanItem} id
	 * @param testerLogin a {@link org.squashtest.tm.domain.users.User} login
	 * @return true if the {@link IterationTestPlanItem} is the last executable in the {@link TestPlanOwner}'s test plan
	 */
	private boolean isLastExecutableTestPlanItem(E testPlanOwner, long testPlanItemId, String testerLogin) {
		List<IterationTestPlanItem> testPlans = getTestPlan(testPlanOwner);
		for (int i = testPlans.size() - 1; i >= 0; i--) {
			IterationTestPlanItem item = testPlans.get(i);
			// We have to check if the referenced test case has execution steps
			TestCase testCase = null;
			if (!item.isTestCaseDeleted()) {
				testCase = item.getReferencedTestCase();
				if (item.isExecutableThroughTestSuite() && testCaseHasSteps(testCase) && (testerLogin == null || item.isAssignedToUser(testerLogin))) {
					return testPlanItemId == item.getId();
				}
			}
		}

		return false;
	}

	private boolean testCaseHasSteps(TestCase testCase) {
		Wrapped<Boolean> hasSteps = new Wrapped<>();
		TestCaseVisitor visitor = new TestCaseVisitor() {
			@Override
			public void visit(TestCase testCase) {
				hasSteps.setValue(testCase.getSteps() != null && !testCase.getSteps().isEmpty());
			}

			@Override
			public void visit(KeywordTestCase keywordTestCase) {
				hasSteps.setValue(testCase.getSteps() != null && !testCase.getSteps().isEmpty());
			}

			@Override
			public void visit(ScriptedTestCase scriptedTestCase) {
				hasSteps.setValue(scriptedTestCase.getScript() != null && !scriptedTestCase.getScript().isEmpty() && hasScenarios(scriptedTestCase));
			}
		};
		testCase.accept(visitor);
		return hasSteps.getValue();

	}

	private boolean hasScenarios(ScriptedTestCase scriptedTestCase) {
		return GherkinParser.hasScenarios(scriptedTestCase.getScript());
	}

	/**
	 * Determine if the {@link IterationTestPlanItem} with the given testPlanItemId is the first executable {@link IterationTestPlanItem}
	 * assigned to the {@link org.squashtest.tm.domain.users.User} with the given testerLogin in the {@link TestPlanOwner}'s test plan
	 * @param testPlanOwner a {@link TestPlanOwner}
	 * @param testPlanItemId {@link IterationTestPlanItem} id
	 * @param testerLogin a {@link org.squashtest.tm.domain.users.User} login
	 * @return true if the {@link IterationTestPlanItem} is the first executable in the {@link TestPlanOwner}'s test plan
	 */
	abstract boolean isFirstExecutableTestPlanItem(E testPlanOwner, long testPlanItemId, String testerLogin);

	/**
	 * Find the next executable {@link IterationTestPlanItem} after the one with the given testPlanItemId,
	 * assigned to the {@link org.squashtest.tm.domain.users.User} with the given testerLogin, in the {@link TestPlanOwner}'s test plan
	 * @param testPlanOwner a {@link TestPlanOwner}
	 * @param testPlanItemId {@link IterationTestPlanItem} id
	 * @param testerLogin a {@link org.squashtest.tm.domain.users.User} login
	 * @return the next executable {@link IterationTestPlanItem} after the one with the given testPlanItemId,
	 * assigned to the {@link org.squashtest.tm.domain.users.User} with the given testerLogin, in the {@link TestPlanOwner}'s test plan
	 */
	abstract IterationTestPlanItem findNextExecutableTestPlanItem(E testPlanOwner, long testPlanItemId, String testerLogin);

	/**
	 * Find the next executable {@link IterationTestPlanItem} after the one with the given testPlanItemId in the {@link TestPlanOwner}'s test plan
	 * @param testPlanOwner a {@link TestPlanOwner}
	 * @param testPlanItemId {@link IterationTestPlanItem} id
	 * @return the next executable {@link IterationTestPlanItem} after the one with the given testPlanItemId,
	 * assigned to the {@link org.squashtest.tm.domain.users.User} with the given testerLogin, in the {@link TestPlanOwner}'s test plan
	 */
	abstract IterationTestPlanItem findNextExecutableTestPlanItem(E testPlanOwner, long testPlanItemId);

	private String findUserLoginIfTester(Object domainObject) {
		String testerLogin = null;
		try {
			PermissionsUtils.checkPermission(permissionEvaluationService, new SecurityCheckableObject(domainObject,
				"READ_UNASSIGNED"));
		} catch (AccessDeniedException ade) {
			LOGGER.error(ade.getMessage(), ade);
			testerLogin = userService.findCurrentUser().getLogin();
		}
		return testerLogin;
	}

	/**
	 * Same as {@link #findUserLoginIfTester(Object)} but with separate id and class name parameters.
	 * Used if you can't provide a domainObject with the id in it.
	 * @param domainObjectId the id of the domain object
	 * @param domainObjectClass the class name of the domain object
	 */
	private String findUserLoginIfTester(long domainObjectId, String domainObjectClass) {
		String testerLogin = null;
		try {
			PermissionsUtils.checkPermission(
				permissionEvaluationService,
				Collections.singletonList(domainObjectId),
				"READ_UNASSIGNED",
				domainObjectClass);
		} catch (AccessDeniedException ade) {
			LOGGER.error(ade.getMessage(), ade);
			testerLogin = userService.findCurrentUser().getLogin();
		}
		return testerLogin;
	}

	/**
	 * if has executions: will return last execution if not terminated,<br>
	 * if has no execution and is not test-case deleted : will return new execution<br>
	 * else will return null
	 *
	 * @param testPlanItem an {@link IterationTestPlanItem}
	 *
	 */
	private Execution findUnexecutedOrCreateExecution(IterationTestPlanItem testPlanItem) {
		Execution executionToReturn = null;
		if (testPlanItem.isExecutableThroughTestSuite()) {
			executionToReturn = testPlanItem.getLatestExecution();
			if (executionToReturn == null) {
				executionToReturn = testPlanManager.addExecution(testPlanItem);
			}
		}
		return executionToReturn;
	}

	/**
	 * Tells if the given partial test plan has at least one deleted test case in its items.
	 * @param partialTestPlan partial test plan as a List of {@link IterationTestPlanItem}
	 */
	protected boolean hasDeletedTestCaseInFilteredSelection(List<IterationTestPlanItem> partialTestPlan) {
		return partialTestPlan.stream().anyMatch(item -> Objects.isNull(item.getReferencedTestCase()));
	}

	@Override
	public TestPlanResume resumeWithFilteredTestPlan(long testPlanOwnerId, List<GridFilterValue> filters) {
		List<IterationTestPlanItem> partialTestPlan = getFilteredTestPlan(testPlanOwnerId, filters);
		Execution execution = checkValidityThenResumeFilteredTestPlan(testPlanOwnerId, partialTestPlan);
		boolean hasNextTestCase = partialTestPlanHasNextTestCase(testPlanOwnerId, execution, partialTestPlan);
		return createNewTestPlanResume(testPlanOwnerId, execution, hasNextTestCase, extractItemIdsFromPartialTestPlan(partialTestPlan));
	}

	protected abstract TestPlanResume createNewTestPlanResume(long testPlanOwnerId, Execution execution, boolean hasNextTestCase, List<Long> partialTestPlanItemIds);

	@Override
	public TestPlanResume resumeNextExecutionOfFilteredTestPlan(long testPlanOwnerId, long testPlanItemId, List<Long> partialTestPlanItemIds) {
		List<IterationTestPlanItem> partialTestPlan = iterationTestPlanDao.findAllByIdIn(partialTestPlanItemIds);
		Execution execution = startResumeNextExecutionOfFilteredTestPlan(testPlanOwnerId, testPlanItemId, partialTestPlan);
		boolean hasNextTestCase = partialTestPlanIdsHasNextTestCase(testPlanOwnerId, execution, partialTestPlan);
		return createNewTestPlanResume(testPlanOwnerId, execution, hasNextTestCase, partialTestPlanItemIds);
	}

}

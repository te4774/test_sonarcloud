/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.plugin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.requirement.HighLevelRequirementCreator;
import org.squashtest.tm.service.plugin.PluginFinderService;

import java.util.Collection;
import java.util.Collections;


@Service
@Transactional
public class PluginFinderServiceImpl implements PluginFinderService {

    @Autowired(required = false)
    private final Collection<HighLevelRequirementCreator> highLevelRequirementCreatorList = Collections.emptyList();


    @Override
    public boolean isPremiumPluginInstalled() {
        return !highLevelRequirementCreatorList.isEmpty();
    }
}

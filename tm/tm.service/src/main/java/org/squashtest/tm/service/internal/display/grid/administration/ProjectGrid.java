/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.jooq.domain.tables.ThirdPartyServer;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.ACL_OBJECT_IDENTITY;
import static org.squashtest.tm.jooq.domain.Tables.ACL_RESPONSIBILITY_SCOPE_ENTRY;
import static org.squashtest.tm.jooq.domain.Tables.BUGTRACKER;
import static org.squashtest.tm.jooq.domain.Tables.BUGTRACKER_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;

public class ProjectGrid extends AbstractGrid {


	private static final String HAS_PERMISSIONS = "HAS_PERMISSIONS";

	private final List<Long> projectIds;

	// Class IDs in the ACL
	private static final Long PROJECT_CLASS_ID = 1L;
	private static final Long TEMPLATE_CLASS_ID = 5L;

	private static final ThirdPartyServer bugTrackerThirdPartyServer = THIRD_PARTY_SERVER.as("BUGTRACKER_THIRD_PARTY_SERVER");
	private static final ThirdPartyServer testAutomationThirdPartyServer = THIRD_PARTY_SERVER.as("TEST_AUTOMATION_THIRD_PARTY_SERVER");

	public ProjectGrid(List<Long> projectIds) {
		this.projectIds = projectIds;
	}

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(PROJECT.NAME),
			new GridColumn(PROJECT.PROJECT_ID),
			new GridColumn(PROJECT.LABEL),
			new GridColumn(PROJECT.CREATED_ON),
			new GridColumn(PROJECT.CREATED_BY),
			new GridColumn(PROJECT.LAST_MODIFIED_ON),
			new GridColumn(PROJECT.LAST_MODIFIED_BY),
			new GridColumn(getHasPermissions().as(HAS_PERMISSIONS)),
			new GridColumn(bugTrackerThirdPartyServer.NAME.as("BUGTRACKER_NAME")),
			new GridColumn(testAutomationThirdPartyServer.NAME.as("EXECUTION_SERVER")),
			new GridColumn(DSL.field(PROJECT.PROJECT_TYPE.eq("T")).as("IS_TEMPLATE"))
		);
	}

	@Override
	protected Table<?> getTable() {


		return PROJECT
			.leftJoin(BUGTRACKER_BINDING).on(BUGTRACKER_BINDING.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.leftJoin(BUGTRACKER).on(BUGTRACKER.BUGTRACKER_ID.eq(BUGTRACKER_BINDING.BUGTRACKER_ID))
			.leftJoin(bugTrackerThirdPartyServer).on(bugTrackerThirdPartyServer.SERVER_ID.eq(BUGTRACKER.BUGTRACKER_ID))
			.leftJoin(TEST_AUTOMATION_SERVER).on(PROJECT.TA_SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
			.leftJoin(testAutomationThirdPartyServer).on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(testAutomationThirdPartyServer.SERVER_ID));
	}

	@Override
	protected Field<?> getIdentifier() {
		return PROJECT.PROJECT_ID;
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return PROJECT.PROJECT_ID;
	}

	@Override
	protected Condition craftInvariantFilter() {
		return PROJECT.PROJECT_ID.in(projectIds);
	}

	private Field<Boolean> getHasPermissions() {
		return DSL.select(
				DSL.field(DSL.count(ACL_RESPONSIBILITY_SCOPE_ENTRY.ID).gt(0)).as(HAS_PERMISSIONS))
			.from(ACL_RESPONSIBILITY_SCOPE_ENTRY)
			.innerJoin(ACL_OBJECT_IDENTITY)
			.on(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID.eq(ACL_OBJECT_IDENTITY.ID))
			.where(ACL_OBJECT_IDENTITY.CLASS_ID.eq(PROJECT_CLASS_ID).and(ACL_OBJECT_IDENTITY.IDENTITY.eq(PROJECT.PROJECT_ID)))
				.or(ACL_OBJECT_IDENTITY.CLASS_ID.eq(TEMPLATE_CLASS_ID)).and(ACL_OBJECT_IDENTITY.IDENTITY.eq(PROJECT.PROJECT_ID))
			.asField();
	}

	@Override
	protected SortField<?> getDefaultOrder() {
		return DSL.upper(PROJECT.NAME).asc();
	}
}

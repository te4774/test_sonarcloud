/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.jooq.Table;
import org.springframework.security.acls.domain.BasePermission;
import org.squashtest.tm.api.plugin.PluginType;
import org.squashtest.tm.domain.project.LibraryPluginBinding;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.service.internal.repository.CustomProjectDao;
import org.squashtest.tm.service.internal.repository.ParameterNames;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.ACL_CLASS;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP_PERMISSION;
import static org.squashtest.tm.jooq.domain.Tables.ACL_OBJECT_IDENTITY;
import static org.squashtest.tm.jooq.domain.Tables.ACL_RESPONSIBILITY_SCOPE_ENTRY;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;


public class ProjectDaoImpl extends HibernateEntityDao<Project> implements CustomProjectDao {

	private static final String PROJECT_ID = "projectId";
	private static final String PROJECT_CLASS = "org.squashtest.tm.domain.project.Project";

	@Inject
	private DSLContext DSL;

	@PersistenceContext
	private EntityManager em;

	@Override
	public long countNonFoldersInProject(long projectId) {
		Long req = (Long) executeEntityNamedQuery("project.countNonFolderInRequirement", idParameter(projectId));
		Long tc = (Long) executeEntityNamedQuery("project.countNonFolderInTestCase", idParameter(projectId));
		Long camp = (Long) executeEntityNamedQuery("project.countNonFolderInCampaign", idParameter(projectId));
		Long customReport = (Long) executeEntityNamedQuery("project.countNonFolderInCustomReport", idParameter(projectId));

		return req + tc + camp + customReport;
	}

	@Override
	public long countNonFolderInActionWord(long projectId) {
		return executeEntityNamedQuery("project.countNonFolderInActionWord", idParameter(projectId));
	}

	private SetQueryParametersCallback idParameter(final long id) {
		return new SetIdParameter(ParameterNames.PROJECT_ID, id);
	}

	private SetQueryParametersCallback idParameters(final List<Long> ids) {
		return new SetProjectIdsParameterCallback(ids);
	}

	@Override
	public List<String> findUsersWhoCreatedTestCases(List<Long> projectIds) {
		if (projectIds.isEmpty()) {
			return Collections.emptyList();
		}

		// Making it with subrequest allow faster request when having lots of data
		// With testing dataset, the time to fetch users who created/modified come from 1.5sec to 120ms on my computer
		Table<Record1<String>> distinctTclnCreatedBy = DSL.selectDistinct(TEST_CASE_LIBRARY_NODE.CREATED_BY)
			.from(TEST_CASE_LIBRARY_NODE).asTable("DISTINCT_TCLN_CREATED_BY");

		Field<String> createdBy = distinctTclnCreatedBy.field("CREATED_BY", String.class);
		return DSL.selectDistinct(createdBy)
			.from(distinctTclnCreatedBy)
			.innerJoin(TEST_CASE_LIBRARY_NODE).on(TEST_CASE_LIBRARY_NODE.CREATED_BY.eq(createdBy))
			.innerJoin(TEST_CASE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
			.where(TEST_CASE_LIBRARY_NODE.PROJECT_ID.in(projectIds))
			.orderBy(createdBy)
			.fetch(createdBy, String.class);
	}

	@Override
	public List<String> findUsersWhoModifiedTestCases(List<Long> projectIds) {
		if (projectIds.isEmpty()) {
			return Collections.emptyList();
		}
		Table<Record1<String>> distinctTclnLastModifiedBy = DSL.selectDistinct(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY)
			.from(TEST_CASE_LIBRARY_NODE).where(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY.isNotNull()).asTable("DISTINCT_TCLN_MODIFIED_BY");

		Field<String> lastModifiedBy = distinctTclnLastModifiedBy.field("LAST_MODIFIED_BY", String.class);

		return DSL.selectDistinct(lastModifiedBy)
			.from(distinctTclnLastModifiedBy)
			.innerJoin(TEST_CASE_LIBRARY_NODE).on(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY.eq(lastModifiedBy))
			.innerJoin(TEST_CASE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
			.where(TEST_CASE_LIBRARY_NODE.PROJECT_ID.in(projectIds))
			.orderBy(lastModifiedBy)
			.fetch(lastModifiedBy, String.class);
	}

	@Override
	public List<String> findUsersWhoCreatedRequirementVersions(List<Long> projectIds) {
		if (projectIds.isEmpty()) {
			return Collections.emptyList();
		}
		Table<Record1<String>> distinctRlnCreatedBy = DSL.selectDistinct(REQUIREMENT_LIBRARY_NODE.CREATED_BY)
			.from(REQUIREMENT_LIBRARY_NODE).asTable("DISTINCT_RLN_CREATED_BY");

		Field<String> createdBy = distinctRlnCreatedBy.field("CREATED_BY", String.class);

		return DSL.selectDistinct(createdBy)
			.from(distinctRlnCreatedBy)
			.innerJoin(REQUIREMENT_LIBRARY_NODE).on(REQUIREMENT_LIBRARY_NODE.CREATED_BY.eq(createdBy))
			.innerJoin(REQUIREMENT).on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT.RLN_ID))
			.where(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.in(projectIds))
			.orderBy(createdBy)
			.fetch(createdBy, String.class);
	}

	@Override
	public List<String> findUsersWhoModifiedRequirementVersions(List<Long> projectIds) {
		if (projectIds.isEmpty()) {
			return Collections.emptyList();
		}
		Table<Record1<String>> distinctResourceLastModifiedBy = DSL.selectDistinct(RESOURCE.LAST_MODIFIED_BY)
			.from(RESOURCE).where(RESOURCE.LAST_MODIFIED_BY.isNotNull()).asTable("DISTINCT_RESOURCE_MODIFIED_BY");

		Field<String> lastModifiedBy = distinctResourceLastModifiedBy.field("LAST_MODIFIED_BY", String.class);

		return DSL.selectDistinct(lastModifiedBy)
			.from(distinctResourceLastModifiedBy)
			.innerJoin(RESOURCE).on(RESOURCE.LAST_MODIFIED_BY.eq(lastModifiedBy))
			.innerJoin(REQUIREMENT_VERSION).on(RESOURCE.RES_ID.eq(REQUIREMENT_VERSION.RES_ID))
			.innerJoin(REQUIREMENT).on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
			.innerJoin(REQUIREMENT_LIBRARY_NODE).on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
			.where(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.in(projectIds))
			.orderBy(lastModifiedBy)
			.fetch(lastModifiedBy, String.class);
	}

	@Override
	public List<String> findUsersWhoExecutedItpi(List<Long> projectIds) {
		return DSL.select(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_BY)
			.from(ITERATION_TEST_PLAN_ITEM)
			.innerJoin(ITEM_TEST_PLAN_LIST).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
			.innerJoin(ITERATION).on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
			.innerJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
			.where(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.in(projectIds))
			.and(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_BY.isNotNull())
			.orderBy(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_BY)
			.fetch(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_BY, String.class);
	}

	@Override
	public List<String> findUsersAssignedToItpi(List<Long> projectIds) {
		return DSL.select(CORE_USER.LOGIN)
			.from(ITERATION_TEST_PLAN_ITEM)
			.innerJoin(CORE_USER).on(ITERATION_TEST_PLAN_ITEM.USER_ID.eq(CORE_USER.PARTY_ID))
			.innerJoin(ITEM_TEST_PLAN_LIST).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
			.innerJoin(ITERATION).on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
			.innerJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
			.where(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.in(projectIds))
			.orderBy(CORE_USER.LOGIN)
			.fetch(CORE_USER.LOGIN, String.class);
	}

	@Override
	public List<Long> findAllProjectIds() {
		return DSL.select(PROJECT.PROJECT_ID)
			.from(PROJECT)
			.where(PROJECT.PROJECT_TYPE.eq("P"))
			.fetch(PROJECT.PROJECT_ID, Long.class);
	}

	@Override
	public List<Long> findAllProjectAndTemplateIds() {
		return DSL.select(PROJECT.PROJECT_ID)
			.from(PROJECT)
			.fetch(PROJECT.PROJECT_ID, Long.class);
	}

	@Override
	public List<Long> findAllProjectIds(List<Long> partyIds) {
		return findAllProjectIdsByPermissionMaskAndClassname(partyIds, BasePermission.READ.getMask(), Project.class.getName());
	}

	@Override
	public List<Long> findAllProjectIdsByPermissionMaskAndClassname(List<Long> partyIds, int permissionMask, String classname) {
		SelectConditionStep<Record1<Long>> selectGroupsWithGivenPermissionOnProjects = DSL.select(ACL_GROUP.ID)
				.from(ACL_GROUP)
				.innerJoin(ACL_GROUP_PERMISSION).on(ACL_GROUP_PERMISSION.ACL_GROUP_ID.eq(ACL_GROUP.ID))
				.innerJoin(ACL_CLASS).on(ACL_CLASS.ID.eq(ACL_GROUP_PERMISSION.CLASS_ID))
				.where(ACL_GROUP_PERMISSION.PERMISSION_MASK.eq(permissionMask)).and(ACL_CLASS.CLASSNAME.eq(classname));

		return DSL.selectDistinct(ACL_OBJECT_IDENTITY.IDENTITY)
				.from(ACL_RESPONSIBILITY_SCOPE_ENTRY)
				.innerJoin(ACL_OBJECT_IDENTITY).on(ACL_OBJECT_IDENTITY.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID))
				.innerJoin(ACL_CLASS).on(ACL_CLASS.ID.eq(ACL_OBJECT_IDENTITY.CLASS_ID))
				.where(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.in(partyIds))
				.and(ACL_CLASS.CLASSNAME.eq(classname))
				.and(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.in(selectGroupsWithGivenPermissionOnProjects))
				.fetch(ACL_OBJECT_IDENTITY.IDENTITY, Long.class);
	}

	@Override
	public List<Long> findAllProjectIdsForAutomationWriter(List<Long> partyIds) {
		return DSL
			.selectDistinct(ACL_OBJECT_IDENTITY.IDENTITY)
			.from(ACL_RESPONSIBILITY_SCOPE_ENTRY)
			.join(ACL_OBJECT_IDENTITY).on(ACL_OBJECT_IDENTITY.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID))
			.join(ACL_CLASS).on(ACL_CLASS.ID.eq(ACL_OBJECT_IDENTITY.CLASS_ID))
			.where(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.in(partyIds)
				.and(ACL_CLASS.CLASSNAME.eq("org.squashtest.tm.domain.project.Project"))
				.and(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.in(Arrays.asList(5L, 10L))))
			.fetch(ACL_OBJECT_IDENTITY.IDENTITY, Long.class);
	}

	@Override
	public Integer countProjectsAllowAutomationWorkflow() {
		return DSL
			.selectCount()
			.from(PROJECT)
			.where(PROJECT.ALLOW_AUTOMATION_WORKFLOW.eq(true))
			.fetchOne().value1();
	}

	@Override
	public LibraryPluginBinding findPluginForProject(Long projectId, PluginType pluginType) {
		LibraryPluginBinding lpb;
		javax.persistence.Query query = entityManager.createNamedQuery("Project.findPluginForProject");
		query.setParameter(PROJECT_ID, projectId);
		query.setParameter("pluginType", pluginType);
		try {
			lpb = (LibraryPluginBinding) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
		return lpb;
	}

	@Override
	public void removeLibraryPluginBindingProperty(Long libraryPluginBindingId) {
		javax.persistence.Query query = entityManager.createNativeQuery(NativeQueries.DELETE_LIBRARY_PLUGING_PINDING_PROPERTY);
		query.setParameter("libraryPluginBindingId", libraryPluginBindingId);
		query.executeUpdate();
	}

	@Override
	public BigInteger countActivePluginInProject(long projectId) {
		Query query = em.createNativeQuery(NativeQueries.COUNT_ACTIVE_PLUGIN_IN_PROJECT);
		query.setParameter(PROJECT_ID, projectId);
		return (BigInteger) query.getSingleResult();
	}

	@Override
	public List<Long> findAllManagedProjectIds(List<Long> partyIds) {
		return DSL
			.selectDistinct(ACL_OBJECT_IDENTITY.IDENTITY)
			.from(ACL_RESPONSIBILITY_SCOPE_ENTRY)
			.join(ACL_OBJECT_IDENTITY).on(ACL_OBJECT_IDENTITY.ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID))
			.join(ACL_CLASS).on(ACL_CLASS.ID.eq(ACL_OBJECT_IDENTITY.CLASS_ID))
			.where(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.in(partyIds)
				.and(ACL_CLASS.CLASSNAME.eq("org.squashtest.tm.domain.project.Project"))
				.and(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID.eq(5L)))
			.fetch(ACL_OBJECT_IDENTITY.IDENTITY, Long.class);
	}

	@Override
	public Project fetchForAutomatedExecutionCreation(long projectId) {
		Query query = em.createNamedQuery("Project.fetchForAutomatedExecutionCreation");
		query.setParameter(PROJECT_ID, projectId);
		return (Project) query.getSingleResult();
	}

	@Override
	public String findAutomationWorkflowTypeByProjectId(Long projectId) {
		return DSL
			.select(PROJECT.AUTOMATION_WORKFLOW_TYPE)
			.from(PROJECT)
			.where(PROJECT.PROJECT_ID.eq(projectId))
			.fetchOne().into(String.class);
	}

}

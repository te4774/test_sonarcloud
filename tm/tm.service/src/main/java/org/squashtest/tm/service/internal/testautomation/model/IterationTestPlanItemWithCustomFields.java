/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.model;

import org.squashtest.tm.domain.campaign.IterationTestPlanItem;

import java.util.Map;

public class IterationTestPlanItemWithCustomFields {
	private final IterationTestPlanItem iterationTestPlanItem;
	private final Map<String, Object> customFields;

	public IterationTestPlanItemWithCustomFields(IterationTestPlanItem iterationTestPlanItem, Map<String, Object> customFields) {
		this.iterationTestPlanItem = iterationTestPlanItem;
		this.customFields = customFields;
	}

	public IterationTestPlanItem getIterationTestPlanItem() {
		return iterationTestPlanItem;
	}

	public Map<String, Object> getCustomFields() {
		return customFields;
	}

}

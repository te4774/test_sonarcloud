/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc.delegate;

import org.jooq.DSLContext;
import org.jooq.Table;
import org.jooq.TableField;

/**
 * Delete query for database using delete A from A where A.id in... form
 * In our case it's for h2 and postgres that show good performance with this form,
 * but are not compatible with mysql weird delete join syntax
 */
public class SubRequestDelegateDeleteQuery extends AbstractDelegateDeleteQuery implements DelegateDeleteQuery {

	public SubRequestDelegateDeleteQuery(TableField<?, Long> originalField, DSLContext dslContext, String operationId) {
		super(originalField, dslContext, operationId);
	}

	@Override
	public void delete(TableField<?, Long> linkedColumn) {
		Table<?> table = linkedColumn.getTable();
		dslContext.delete(table)
			.where(linkedColumn.in(selectEntityIds()))
			.execute();
	}
}

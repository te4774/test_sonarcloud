/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.system;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.AdministrationStatistics;
import org.squashtest.tm.exception.WrongUrlException;
import org.squashtest.tm.service.configuration.ConfigurationService;
import org.squashtest.tm.service.internal.repository.AdministrationDao;
import org.squashtest.tm.service.license.LicenseInfoExtender;
import org.squashtest.tm.service.system.BasicLicenseInfo;
import org.squashtest.tm.service.system.SystemAdministrationService;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

@Service("squashtest.tm.service.SystemAdministrationService")
@Transactional
public class SystemAdministrationServiceImpl implements SystemAdministrationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystemAdministrationServiceImpl.class);

    private final AdministrationDao adminDao;
    private final ConfigurationService configurationService;
    private final ApplicationContext appContext;

	@Autowired(required = false)
	private List<LicenseInfoExtender> licenseInfoExtenders = Collections.emptyList();

    public SystemAdministrationServiceImpl(AdministrationDao adminDao,
										   ConfigurationService configurationService,
										   ApplicationContext appContext,
										   List<LicenseInfoExtender> licenseInfoExtenders) {
        this.adminDao = adminDao;
        this.configurationService = configurationService;
        this.appContext = appContext;
		this.licenseInfoExtenders = licenseInfoExtenders;
	}

    @Override
    public AdministrationStatistics findAdministrationStatistics() {
        return adminDao.findAdministrationStatistics();
    }

    @Override
    public String findLoginMessage() {
        return configurationService.findConfiguration(ConfigurationService.Properties.LOGIN_MESSAGE);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeLoginMessage(String loginMessage) {
        configurationService.set(ConfigurationService.Properties.LOGIN_MESSAGE, loginMessage);
    }

    @Override
    public String findWelcomeMessage() {
        return configurationService.findConfiguration(ConfigurationService.Properties.WELCOME_MESSAGE);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeWelcomeMessage(String welcomeMessage) {
        configurationService.set(ConfigurationService.Properties.WELCOME_MESSAGE, welcomeMessage);
    }

    @Override
    public String findCallbackUrl() {
        return configurationService.findConfiguration(ConfigurationService.Properties.SQUASH_CALLBACK_URL);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeCallbackUrl(String callbackUrl) {
        try {
            new URL(callbackUrl);
        } catch (MalformedURLException ex) {
            throw new WrongUrlException("callbackUrl", ex);
        }

        configurationService.set(ConfigurationService.Properties.SQUASH_CALLBACK_URL, callbackUrl);
    }

    @Override
    public String findWhiteList() {
        return configurationService.findConfiguration(ConfigurationService.Properties.UPLOAD_EXTENSIONS_WHITELIST);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeWhiteList(String whiteList) {
        configurationService.set(ConfigurationService.Properties.UPLOAD_EXTENSIONS_WHITELIST, whiteList);
    }

    @Override
    public String findImportSizeLimit() {
        return configurationService.findConfiguration(ConfigurationService.Properties.UPLOAD_SIZE_LIMIT);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeImportSizeLimit(String sizeLimit) {
        configurationService.set(ConfigurationService.Properties.UPLOAD_SIZE_LIMIT, sizeLimit);
    }

    @Override
    public String findUploadSizeLimit() {
        return configurationService.findConfiguration(ConfigurationService.Properties.IMPORT_SIZE_LIMIT);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void changeUploadSizeLimit(String sizeLimit) {
        configurationService.set(ConfigurationService.Properties.IMPORT_SIZE_LIMIT, sizeLimit);
    }

    @Override
    public List<String> findAllPluginsFilesOnInstance() {
        String pluginPathProperty = "squash.path.plugins-path";
        String pluginsPath = appContext.getEnvironment().getProperty(pluginPathProperty);
        if (pluginsPath == null) {
            LOGGER.warn("Plugin path was not defined, I don't know where to look for plugins");
            return null;
        }

        File pluginsFolder = new File(pluginsPath);

        if (!pluginsFolder.exists() || !pluginsFolder.isDirectory()) {
            LOGGER.warn("Plugin path '{}' is not a readable folder. There will be no plugins", pluginsFolder.getAbsolutePath());
            return null;
        }

        LOGGER.info("Enumerating plugins / jars in folder '{}'", pluginsFolder.getAbsolutePath());

        File[] jarFiles = FileUtils
			.listFiles(pluginsFolder, new String[]{ "jar" }, true)
			.toArray(new File[] {});

        if (jarFiles != null) {
            return Arrays.stream(jarFiles)
                    .map(File::getName).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

	@Override
	public BasicLicenseInfo getBasicLicenseInfo() {
		LicenseInfoExtender firstLicenseInfoExtender = licenseInfoExtenders.stream().findFirst().orElse(null);
		if(firstLicenseInfoExtender != null) {
			return firstLicenseInfoExtender.getBasicLicenseInfo();
		} else {
			return null;
		}
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.testcase;

import org.squashtest.tm.service.internal.display.dto.LibraryDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.service.statistics.testcase.TestCaseStatisticsBundle;

public class TestCaseLibraryDto extends LibraryDto {
	private TestCaseStatisticsBundle statistics;
	private JsonCustomReportDashboard dashboard;
	private boolean shouldShowFavoriteDashboard;
	private boolean canShowFavoriteDashboard;
	private Long favoriteDashboardId;

	public TestCaseStatisticsBundle getStatistics() {
		return statistics;
	}

	public void setStatistics(TestCaseStatisticsBundle statistics) {
		this.statistics = statistics;
	}

	public JsonCustomReportDashboard getDashboard() {
		return dashboard;
	}

	public void setDashboard(JsonCustomReportDashboard dashboard) {
		this.dashboard = dashboard;
	}

	public boolean isShouldShowFavoriteDashboard() {
		return shouldShowFavoriteDashboard;
	}

	public void setShouldShowFavoriteDashboard(boolean shouldShowFavoriteDashboard) {
		this.shouldShowFavoriteDashboard = shouldShowFavoriteDashboard;
	}

	public boolean isCanShowFavoriteDashboard() {
		return canShowFavoriteDashboard;
	}

	public void setCanShowFavoriteDashboard(boolean canShowFavoriteDashboard) {
		this.canShowFavoriteDashboard = canShowFavoriteDashboard;
	}

	public Long getFavoriteDashboardId() {
		return favoriteDashboardId;
	}

	public void setFavoriteDashboardId(Long favoriteDashboardId) {
		this.favoriteDashboardId = favoriteDashboardId;
	}
}

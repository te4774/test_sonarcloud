/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.jooq.SelectOnConditionStep;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementFolderDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementLibraryDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.requirement.RequirementVersionsGrid;
import org.squashtest.tm.service.internal.repository.display.RequirementDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

import java.util.List;
import java.util.Optional;

import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;

@Repository
public class RequirementDisplayDaoImpl implements RequirementDisplayDao {

	private final DSLContext dsl;
	private final ActiveMilestoneHolder activeMilestoneHolder;

	public RequirementDisplayDaoImpl(DSLContext dsl, ActiveMilestoneHolder activeMilestoneHolder) {
		this.dsl = dsl;
		this.activeMilestoneHolder = activeMilestoneHolder;
	}


	@Override
	public RequirementLibraryDto findRequirementLibraryDtoById(Long libraryId) {
		return dsl
			.select(REQUIREMENT_LIBRARY.RL_ID.as(RequestAliasesConstants.ID), REQUIREMENT_LIBRARY.ATTACHMENT_LIST_ID,
				PROJECT.PROJECT_ID, PROJECT.DESCRIPTION, PROJECT.NAME)
			.from(REQUIREMENT_LIBRARY)
			.innerJoin(PROJECT).on(REQUIREMENT_LIBRARY.RL_ID.eq(PROJECT.RL_ID))
			.where(REQUIREMENT_LIBRARY.RL_ID.eq(libraryId))
			.fetchOne().into(RequirementLibraryDto.class);
	}

	@Override
	public RequirementFolderDto getRequirementFolderDtoById(long requirementFolderId) {
		return dsl.select(
			REQUIREMENT_LIBRARY_NODE.RLN_ID.as(RequestAliasesConstants.ID),
			RESOURCE.ATTACHMENT_LIST_ID, RESOURCE.DESCRIPTION, RESOURCE.NAME, REQUIREMENT_LIBRARY_NODE.PROJECT_ID)
			.from(REQUIREMENT_LIBRARY_NODE)
			.innerJoin(REQUIREMENT_FOLDER).on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT_FOLDER.RLN_ID))
			.innerJoin(RESOURCE).on(REQUIREMENT_FOLDER.RES_ID.eq(RESOURCE.RES_ID))
			.where(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(requirementFolderId))
			.fetchOne().into(RequirementFolderDto.class);
	}

	@Override
	public List<String> findRequirementVersionNamesByRequirementIds(List<Long> requirementIds) {

		SelectOnConditionStep<Record1<String>> baseQuery = requirementVersionNamesBaseQuery();
		Optional<Milestone> optionalMilestone = activeMilestoneHolder.getActiveMilestone();
		Long milestoneId = null;
		if (optionalMilestone.isPresent()) {
			milestoneId = optionalMilestone.get().getId();
			baseQuery = baseQuery
				.innerJoin(MILESTONE_REQ_VERSION).on(REQUIREMENT_VERSION.RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID));
		}

		SelectConditionStep<Record1<String>> conditionStep = baseQuery.where(REQUIREMENT.RLN_ID.in(requirementIds));

		if (milestoneId != null) {
			conditionStep = conditionStep.and(MILESTONE_REQ_VERSION.MILESTONE_ID.eq(milestoneId));
		}

		return conditionStep.fetch().getValues(RESOURCE.NAME);
	}

	@Override
	public GridResponse findVersionsById(Long requirementId, GridRequest request) {
		return new RequirementVersionsGrid(requirementId).getRows(request, dsl);
	}

	private SelectOnConditionStep<Record1<String>> requirementVersionNamesBaseQuery() {
		return dsl.select(RESOURCE.NAME).from(REQUIREMENT)
			.innerJoin(REQUIREMENT_VERSION).on(REQUIREMENT.CURRENT_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
			.innerJoin(RESOURCE).on(REQUIREMENT_VERSION.RES_ID.eq(RESOURCE.RES_ID));
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.map.MultiValueMap;
import org.jooq.DSLContext;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.core.foundation.collection.ColumnFiltering;
import org.squashtest.tm.core.foundation.collection.PagedCollectionHolder;
import org.squashtest.tm.core.foundation.collection.PagingAndMultiSorting;
import org.squashtest.tm.core.foundation.collection.PagingBackedPagedCollectionHolder;
import org.squashtest.tm.core.foundation.lang.Couple;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.scm.ScmServer;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.domain.testautomation.AutomatedTest;
import org.squashtest.tm.domain.testautomation.TestAutomationProject;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService;
import org.squashtest.tm.service.internal.campaign.CampaignNodeDeletionHandler;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.denormalizedField.PrivateDenormalizedFieldValueService;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.campaign.IterationTestPlanGrid;
import org.squashtest.tm.service.internal.display.grid.campaign.TestSuiteTestPlanGrid;
import org.squashtest.tm.service.internal.dto.AttachmentDTO;
import org.squashtest.tm.service.internal.dto.AutomatedSuiteDto;
import org.squashtest.tm.service.internal.repository.AttachmentListDao;
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao;
import org.squashtest.tm.service.internal.repository.AutomatedTestDao;
import org.squashtest.tm.service.internal.repository.ExecutionDao;
import org.squashtest.tm.service.internal.repository.GenericProjectDao;
import org.squashtest.tm.service.internal.repository.IterationDao;
import org.squashtest.tm.service.internal.repository.IterationTestPlanDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.TestSuiteDao;
import org.squashtest.tm.service.internal.testautomation.model.IterationTestPlanItemWithCustomFields;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.servers.CredentialsProvider;
import org.squashtest.tm.service.testautomation.AutomatedExecutionSetIdentifier;
import org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService;
import org.squashtest.tm.service.testautomation.AutomationDeletionCount;
import org.squashtest.tm.service.testautomation.TestAutomationCallbackService;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteCreationSpecification;
import org.squashtest.tm.service.testautomation.model.AutomatedSuitePreview;
import org.squashtest.tm.service.testautomation.model.AutomatedSuitePreview.SquashAutomProjectPreview;
import org.squashtest.tm.service.testautomation.model.AutomatedSuiteWithSquashAutomAutomatedITPIs;
import org.squashtest.tm.service.testautomation.model.SquashAutomExecutionConfiguration;
import org.squashtest.tm.service.testautomation.model.SuiteExecutionConfiguration;
import org.squashtest.tm.service.testautomation.model.TestAutomationProjectContent;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;
import org.squashtest.tm.service.testautomation.spi.TestAutomationException;
import org.squashtest.tm.service.testautomation.spi.UnknownConnectorKind;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static org.squashtest.tm.service.security.Authorizations.EXECUTE_ITERATION_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.EXECUTE_TS_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

@Transactional
@Service("squashtest.tm.service.AutomatedSuiteManagementService")
public class AutomatedSuiteManagerServiceImpl implements AutomatedSuiteManagerService {

	private static final String DELETE = "DELETE";

	private static final String EXECUTE = "EXECUTE";

	private static final Logger LOGGER = LoggerFactory.getLogger(TestAutomationConnector.class);

	private static final int DEFAULT_THREAD_TIMEOUT = 30000; // timeout as milliseconds
	public static final int BIND_VARIABLES_LIMIT = 500;

	private int timeoutMillis = DEFAULT_THREAD_TIMEOUT;

	@Inject
	private DSLContext dslContext;

	@Inject
	private AutomatedSuiteDao autoSuiteDao;

	@Inject
	private IterationDao iterationDao;

	@Inject
	private TestSuiteDao testSuiteDao;

	@Inject
	private IterationTestPlanDao testPlanDao;

	@Inject
	private ExecutionDao executionDao;

	@Inject
	private AttachmentListDao attachmentListDao;

	@Inject
	private CustomFieldValueFinderService customFieldValueFinder;

	@Inject
	private PrivateDenormalizedFieldValueService denormalizedFieldValueService;

	@Inject
	private TestAutomationCallbackService callbackService;

	@Inject
	private Provider<TaParametersBuilder> paramBuilder;

	@Inject
	private TestAutomationConnectorRegistry connectorRegistry;

	@Inject
	private PermissionEvaluationService permissionService;

	@Inject
	private CampaignNodeDeletionHandler deletionHandler;

	@Inject
	private PrivateCustomFieldValueService customFieldValuesService;

	@Inject
	private ProjectDao projectDao;

	@Inject
	private GenericProjectDao genericProjectDao;

	@Inject
	private AutomatedTestDao autoTestDao;

	@Inject
	private CredentialsProvider credentialsProvider;

	@PersistenceContext
	private EntityManager entityManager;

	public AutomatedSuiteManagerServiceImpl() {
	}

	public int getTimeoutMillis() {
		return timeoutMillis;
	}

	public void setTimeoutMillis(int timeoutMillis) {
		this.timeoutMillis = timeoutMillis;
	}

	/**
	 * @see org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService#findById(java.lang.String)
	 */
	@Override
	public AutomatedSuite findById(String id) {
		return autoSuiteDao.findById(id);
	}

	@Override
	public List<Long> findTpiIdsWithAutomaticExecutionMode(EntityReference entityReference) {
		if (EntityType.ITERATION.equals(entityReference.getType())) {
			Iteration iteration = iterationDao.findById(entityReference.getId());
			return doFindTpiIdsWithAutomaticExecutionMode(iteration.getTestPlans());
		} else if (EntityType.TEST_SUITE.equals(entityReference.getType())) {
			TestSuite suite = testSuiteDao.getOne(entityReference.getId());
			return doFindTpiIdsWithAutomaticExecutionMode(suite.getTestPlan());
		} else {
			throw new IllegalArgumentException("An iteration or a test suite was expected");
		}
	}

	@Override
	public PagedCollectionHolder<List<AutomatedSuiteDto>> getAutomatedSuitesByIterationID(Long iterationId, PagingAndMultiSorting paging, ColumnFiltering filter) {
		List<AutomatedSuiteDto> suites = autoSuiteDao.findAutomatedSuitesByIterationID(iterationId, paging, filter);
		appendHasExecutions(suites);
		appendAttachmentLists(suites);
		appendURLs(suites);
		long suiteSize = autoSuiteDao.countSuitesByIterationId(iterationId, filter);
		return new PagingBackedPagedCollectionHolder<>(paging, suiteSize, suites);
	}

	public void appendHasExecutions(List<AutomatedSuiteDto> suites) {
		List<String> suiteIds = new ArrayList<>();
		suites.forEach(suite -> suiteIds.add(suite.getSuiteId()));

		Map<String, Integer> totalExecutionsBySuiteId = dslContext
			.select(Tables.AUTOMATED_SUITE.SUITE_ID, DSL.count().as("executionsCount"))
			.from(Tables.AUTOMATED_SUITE)
			.innerJoin(Tables.AUTOMATED_EXECUTION_EXTENDER).on(Tables.AUTOMATED_SUITE.SUITE_ID.eq(Tables.AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
			.where(Tables.AUTOMATED_SUITE.SUITE_ID.in(suiteIds))
			.groupBy(Tables.AUTOMATED_SUITE.SUITE_ID)
			.fetchMap(Tables.AUTOMATED_SUITE.SUITE_ID, DSL.field("executionsCount", Integer.class));

		suites.forEach(suite -> {
			Integer nbExecutions = totalExecutionsBySuiteId.getOrDefault(suite.getSuiteId(), 0);
			suite.setHasExecutions(nbExecutions > 0);
		});
	}

	public void appendAttachmentLists(List<AutomatedSuiteDto> suites) {
		List<String> suiteIds = suites.stream().map(AutomatedSuiteDto::getSuiteId).collect(Collectors.toList());

		Table<?> selectUnion =  dslContext
			.select(Tables.AUTOMATED_SUITE.SUITE_ID, Tables.ATTACHMENT.ATTACHMENT_ID.as("ID"), Tables.ATTACHMENT.NAME)
			.from(Tables.AUTOMATED_SUITE)
			.innerJoin(Tables.ATTACHMENT).on(Tables.ATTACHMENT.ATTACHMENT_LIST_ID.eq(Tables.AUTOMATED_SUITE.ATTACHMENT_LIST_ID))
			.where(Tables.AUTOMATED_SUITE.SUITE_ID.in(suiteIds))
			.union(dslContext.select(Tables.AUTOMATED_SUITE.SUITE_ID, Tables.ATTACHMENT.ATTACHMENT_ID.as("ID"), Tables.ATTACHMENT.NAME)
				.from(Tables.AUTOMATED_SUITE)
				.innerJoin(Tables.AUTOMATED_EXECUTION_EXTENDER).on(Tables.AUTOMATED_EXECUTION_EXTENDER.SUITE_ID.eq(Tables.AUTOMATED_SUITE.SUITE_ID))
				.innerJoin(Tables.EXECUTION).on(Tables.EXECUTION.EXECUTION_ID.eq(Tables.AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID))
				.innerJoin(Tables.ATTACHMENT).on(Tables.ATTACHMENT.ATTACHMENT_LIST_ID.eq(Tables.EXECUTION.ATTACHMENT_LIST_ID))
				.where(Tables.AUTOMATED_SUITE.SUITE_ID.in(suiteIds))
			).asTable();

		Map<String, List<AttachmentDTO>> attachmentsBySuiteId = dslContext
			.select()
			.from(selectUnion)
			.orderBy(selectUnion.field("NAME")).fetch()
			.intoGroups(Tables.AUTOMATED_SUITE.SUITE_ID, AttachmentDTO.class);
		suites.forEach(suite -> suite.setAttachmentList(attachmentsBySuiteId.getOrDefault(suite.getSuiteId(), new ArrayList<>())));
	}

	public void appendURLs(List<AutomatedSuiteDto> suites) {
		List<String> suiteIds = new ArrayList<>();
		suites.forEach(suite -> suiteIds.add(suite.getSuiteId()));

		Map<String, List<String>> urlsBySuiteId = dslContext
			.select(Tables.AUTOMATED_SUITE.SUITE_ID, Tables.AUTOMATED_EXECUTION_EXTENDER.RESULT_URL)
			.from(Tables.AUTOMATED_SUITE)
			.innerJoin(Tables.AUTOMATED_EXECUTION_EXTENDER).on(Tables.AUTOMATED_EXECUTION_EXTENDER.SUITE_ID.eq(Tables.AUTOMATED_SUITE.SUITE_ID))
			.where(Tables.AUTOMATED_SUITE.SUITE_ID.in(suiteIds).and(Tables.AUTOMATED_EXECUTION_EXTENDER.RESULT_URL.isNotNull()))
			.orderBy(Tables.AUTOMATED_EXECUTION_EXTENDER.RESULT_URL)
			.fetch().intoGroups(Tables.AUTOMATED_SUITE.SUITE_ID, Tables.AUTOMATED_EXECUTION_EXTENDER.RESULT_URL);

		suites.forEach(suite -> suite.getUrlList().addAll(urlsBySuiteId.getOrDefault(suite.getSuiteId(), new ArrayList<>())));

	}

	@Override
	public PagedCollectionHolder<List<AutomatedSuiteDto>> getAutomatedSuitesByTestSuiteID(Long suiteId, PagingAndMultiSorting paging, ColumnFiltering filter) {
		List<AutomatedSuiteDto> suites = autoSuiteDao.findAutomatedSuitesByTestSuiteID(suiteId, paging, filter);
		appendHasExecutions(suites);
		appendAttachmentLists(suites);
		appendURLs(suites);
		long suiteSize = autoSuiteDao.countSuitesByTestSuiteId(suiteId, filter);
		return new PagingBackedPagedCollectionHolder<>(paging, suiteSize, suites);
	}

	@Override
	// security delegated to permission utils
	public AutomatedSuitePreview preview(AutomatedSuiteCreationSpecification specification) {

		// first, validate
		specification.validate();

		// check permission
		checkPermission(specification);

		EntityReference contextEntity = specification.getContext();
		List<Long> testPlanSubset = specification.getTestPlanSubsetIds();

		// if filters are specified and testPlanSubsetIds empty, fetch the TestPlanSubsetIds with the filters
		if (!specification.getFilterValues().isEmpty() && testPlanSubset.isEmpty()) {
			GridRequest gridRequest = prepareNonPaginatedGridRequest(specification);
			Long contextId =  contextEntity.getId();
			AbstractGrid testPlanGrid;
			switch(contextEntity.getType()) {
				case ITERATION:
					testPlanGrid = new IterationTestPlanGrid(contextId);
					break;
				case TEST_SUITE:
					testPlanGrid = new TestSuiteTestPlanGrid(contextId);
					break;
				default:
					throw new IllegalArgumentException("Type " + contextEntity.getType() + " is not supported.");
			}
			GridResponse gridResponse = testPlanGrid.getRows(gridRequest, this.dslContext);
			specification.setTestPlanSubsetIds(extractIdsFromGridResponse(gridResponse));
		}

		//fetch
		List<Couple<TestAutomationProject, Long>> projects = autoSuiteDao.findAllCalledByTestPlan(contextEntity, testPlanSubset);

		boolean needManualServerSelection = projects.stream()
			.anyMatch(couple -> couple.getA1().getServer().isManualSlaveSelection());

		// manual server selection is also required if at least one squashAutom test is present
		if (!needManualServerSelection) {
			needManualServerSelection = autoSuiteDao.itpiSelectionContainsSquashAutomTest(contextEntity, testPlanSubset);
		}

		// fetch List of SquashAutomProjectPreviews
		Map<Long, SquashAutomProjectPreview> squashAutomServersMap =
			autoSuiteDao.findAllSquashAutomProjectPreviews(contextEntity, testPlanSubset);

		// create the response
		AutomatedSuitePreview preview = new AutomatedSuitePreview();
		preview.setSpecification(specification);


		Collection<AutomatedSuitePreview.TestAutomationProjectPreview> projectPreview =
			projects.stream().map(
				couple -> {
					TestAutomationProject taProject = couple.getA1();
					Long testCount = couple.getA2();
					return new AutomatedSuitePreview.TestAutomationProjectPreview(
						taProject.getId(),
						taProject.getLabel(),
						taProject.getServer().getName(),
						taProject.getSlaves(),
						testCount
					);
				}).collect(Collectors.toList());


		preview.setManualServerSelection(needManualServerSelection);
		preview.setProjects(projectPreview);
		preview.setSquashAutomProjects(squashAutomServersMap.values());
		return preview;

	}

	private GridRequest prepareNonPaginatedGridRequest(AutomatedSuiteCreationSpecification specification) {
		GridRequest gridRequest = new GridRequest();
		gridRequest.setFilterValues(specification.getFilterValues());
		return gridRequest.toNonPaginatedRequest();
	}

	private List<Long> extractIdsFromGridResponse(GridResponse gridResponse) {
		return gridResponse.getDataRows().stream()
			.map(row -> row.getId())
			.map(stringId -> Long.valueOf(stringId))
			.collect(Collectors.toList());
	}
	@Override
	// security delegated to permission utils
	public List<String> findTestListPreview(AutomatedSuiteCreationSpecification specification, long automatedProjectId) {
		specification.validate();
		checkPermission(specification);
		return autoSuiteDao.findTestPathForAutomatedSuiteAndProject(specification.getContext(), specification.getTestPlanSubsetIds(), automatedProjectId);
	}

	/**
	 * @see org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService#sortByProject(java.lang.String)
	 */
	@Override
	public Collection<TestAutomationProjectContent> sortByProject(String autoSuiteId) {
		// security delegated to sortByProject(AutomatedSuite)
		AutomatedSuite suite = findById(autoSuiteId);
		return sortByProject(suite);
	}


	/**
	 * @see org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService#sortByProject(org.squashtest.tm.domain.testautomation.AutomatedSuite)
	 */

	@SuppressWarnings({"rawtypes", "unchecked"})
	@Override
	public Collection<TestAutomationProjectContent> sortByProject(AutomatedSuite suite) {
		// security handled by in the code

		List<AutomatedExecutionExtender> extenders = suite.getExecutionExtenders();

		PermissionsUtils.checkPermission(permissionService, extenders, EXECUTE);

		// first sort them using a map
		MultiMap testsByProjects = new MultiValueMap();

		for (AutomatedExecutionExtender extender : extenders) {
			if (extender.isProjectDisassociated()) {
				continue;
			}
			TestAutomationProject project = extender.getAutomatedProject();
			AutomatedTest test = extender.getAutomatedTest();

			testsByProjects.put(project, test);
		}


		// now make a friendly bean of it
		Collection<TestAutomationProjectContent> projectContents = new LinkedList<>();

		Set<Entry> entries = testsByProjects.entrySet();
		for (Entry e : entries) {
			TestAutomationProject project = (TestAutomationProject) e.getKey();
			Collection<AutomatedTest> tests = (Collection) e.getValue();
			TestAutomationConnector connector = connectorRegistry.getConnectorForKind(project.getServer().getKind());
			boolean orderGuaranteed = connector.testListIsOrderGuaranteed(tests);
			projectContents.add(new TestAutomationProjectContent(project, tests, orderGuaranteed));
		}

		return projectContents;

	}

	/**
	 * @see org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService#delete(java.lang.String)
	 */
	@Override
	// security delegated to delete(AutomatedSuite)
	public void delete(String automatedSuiteId) {
		AutomatedSuite suite = findById(automatedSuiteId);
		delete(suite);
	}

	/**
	 * @see org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService#delete(org.squashtest.tm.domain.testautomation.AutomatedSuite)
	 */
	@SuppressWarnings("unchecked")
	@Override
	// security handled in the code
	public void delete(AutomatedSuite suite) {

		PermissionsUtils.checkPermission(permissionService, suite.getExecutionExtenders(), DELETE);

		List<AutomatedExecutionExtender> toremove = new ArrayList<>(suite.getExecutionExtenders());
		suite.getExecutionExtenders().clear();

		List<Execution> execs =
			new ArrayList<>(CollectionUtils.collect(toremove, new ExecutionCollector()));

		deletionHandler.deleteExecutions(execs);

		autoSuiteDao.delete(suite);
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	private void deleteAutomatedSuites(List<String> automatedSuiteIds) {
		List<Long> executionIds = executionDao.findAllIdsByAutomatedSuiteIds(automatedSuiteIds);
		if (!executionIds.isEmpty()) {
			deletionHandler.bulkDeleteExecutions(executionIds);
		}
		autoSuiteDao.deleteAllByIds(automatedSuiteIds);
		autoTestDao.pruneOrphans();
	}

	@Override
	public AutomationDeletionCount countOldAutomatedSuitesAndExecutions() {
		return autoSuiteDao.countOldAutomatedSuitesAndExecutions();
	}

	@Override
	@PreAuthorize(HAS_ROLE_ADMIN)
	public void cleanOldSuites() {
		List<String> oldAutomatedSuiteIds = autoSuiteDao.getOldAutomatedSuiteIds();
		if (oldAutomatedSuiteIds.isEmpty()) {
			return;
		}
		List<List<String>> automatedSuiteIdPartitions = Lists.partition(oldAutomatedSuiteIds, BIND_VARIABLES_LIMIT);
		automatedSuiteIdPartitions.forEach(automatedSuiteIdPartition -> {
			deleteAutomatedSuites(automatedSuiteIdPartition);
			entityManager.flush();
			entityManager.clear();
		});
	}

	@Override
	@PostFilter("hasPermission(filterObject, 'READ')" + OR_HAS_ROLE_ADMIN)
	@Transactional(readOnly = true)
	public List<Execution> findExecutionsByAutomatedTestSuiteId(String automatedTestSuiteId) {

		List<Execution> executions = new ArrayList<>();
		AutomatedSuite suite = autoSuiteDao.findById(automatedTestSuiteId);
		for (AutomatedExecutionExtender e : suite.getExecutionExtenders()) {
			executions.add(e.getExecution());
		}
		return executions;
	}

	@Override
	public AutomatedSuiteWithSquashAutomAutomatedITPIs createAndExecute(AutomatedSuiteCreationSpecification specification) {
		LOGGER.info("START CREATING EXECUTIONS " + new Date());
		AutomatedSuiteWithSquashAutomAutomatedITPIs suite = createFromSpecificationSquashAutom(specification);
		LOGGER.info("END CREATING EXECUTIONS " + new Date());
		LOGGER.info("START SENDING EXECUTIONS " + new Date());
		start(
			suite,
			specification.getExecutionConfigurations(),
			specification.getSquashAutomExecutionConfigurations());
		LOGGER.info("END SENDING EXECUTIONS " + new Date());
		return suite;
	}

	// ******************* legacy (Squash TF only) public methods for automated suite and executions creation ***************************

	@Override
	// security delegated to permission utils
	public AutomatedSuite createFromSpecification(AutomatedSuiteCreationSpecification specification) {
		specification.validate();
		checkPermission(specification);

		// TODO : something better
		// for now we plug on the existing methods
		AutomatedSuite suite = null;

		Long contextId = specification.getContext().getId();
		List<Long> subset = specification.getTestPlanSubsetIds();
		boolean hasTestPlanSubset = (subset != null && !subset.isEmpty());

		if (specification.getContext().getType() == EntityType.ITERATION) {
			if (hasTestPlanSubset) {
				suite = createFromItemsAndIteration(subset, contextId);
			} else {
				suite = createFromIterationTestPlan(contextId);
			}
		} else {
			if (hasTestPlanSubset) {
				suite = createFromItemsAndTestSuite(subset, contextId);
			} else {
				suite = createFromTestSuiteTestPlan(contextId);
			}
		}

		return suite;

	}


	@Override
	@PreAuthorize(EXECUTE_ITERATION_OR_ROLE_ADMIN)
	public AutomatedSuite createFromIterationTestPlan(long iterationId) {
		Iteration iteration = iterationDao.findById(iterationId);
		List<IterationTestPlanItem> items = iteration.getTestPlans();
		Long projectId = items.get(0).getProject().getId();
		List<IterationTestPlanItem> squashTFAutomatedItems = extractSquashTFAutomatedItems(items);
		String newSuiteId = createSuiteAndClearSession(iteration);
		if (!squashTFAutomatedItems.isEmpty()) {
			createExecutionsFromItemsAndClearSession(squashTFAutomatedItems, newSuiteId, projectId);
		}
		return entityManager.find(AutomatedSuite.class, newSuiteId);
	}


	/**
	 * @see org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService#createFromTestSuiteTestPlan(long)
	 */
	@Override
	@PreAuthorize(EXECUTE_TS_OR_ROLE_ADMIN)
	public AutomatedSuite createFromTestSuiteTestPlan(long testSuiteId) {
		TestSuite suite = testSuiteDao.getOne(testSuiteId);
		List<IterationTestPlanItem> items = suite.getTestPlan();
		return createSuite(items);
	}

	/**
	 * @see org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService#createFromItemsAndIteration(java.util.List, long)
	 */
	@Override
	public AutomatedSuite createFromItemsAndIteration(List<Long> testPlanIds, long iterationId) {
		PermissionsUtils.checkPermission(permissionService, testPlanIds, EXECUTE, IterationTestPlanItem.class.getName());

		List<IterationTestPlanItem> items = testPlanDao.findAllByIdsOrderedByIterationTestPlan(testPlanIds);

		return createSuite(items);
	}

	/**
	 * @see org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService#createFromItemsAndTestSuite(java.util.List, long)
	 */
	@Override
	public AutomatedSuite createFromItemsAndTestSuite(List<Long> testPlanIds, long testSuiteId) {
		PermissionsUtils.checkPermission(permissionService, testPlanIds, EXECUTE, IterationTestPlanItem.class.getName());

		List<IterationTestPlanItem> items = testPlanDao.findAllByIdsOrderedBySuiteTestPlan(testPlanIds, testSuiteId);
		return createSuite(items);
	}

	private AutomatedSuite createSuite(List<IterationTestPlanItem> items) {
		Long projectId = items.get(0).getProject().getId();
		List<IterationTestPlanItem> squashTFAutomatedItems = extractSquashTFAutomatedItems(items);
		String newSuiteId = createSuiteAndClearSession();
		if (!squashTFAutomatedItems.isEmpty()) {
			createExecutionsFromItemsAndClearSession(squashTFAutomatedItems, newSuiteId, projectId);
		}
		return entityManager.find(AutomatedSuite.class, newSuiteId);
	}

	// ******************* public methods for executions with Squash TF or Squash AUTOM for automated suite and executions creation ***************************

	@Override
	// security delegated to permission utils
	public AutomatedSuiteWithSquashAutomAutomatedITPIs createFromSpecificationSquashAutom(AutomatedSuiteCreationSpecification specification) {
		specification.validate();
		checkPermission(specification);

		// TODO : something better
		// for now we plug on the existing methods
		AutomatedSuiteWithSquashAutomAutomatedITPIs suite = null;

		Long contextId = specification.getContext().getId();
		List<Long> subset = specification.getTestPlanSubsetIds();
		boolean hasTestPlanSubset = (subset != null && !subset.isEmpty());

		if (specification.getContext().getType() == EntityType.ITERATION) {
			if (hasTestPlanSubset) {
				suite = createFromItemsAndIterationSquashAutom(subset, contextId);
			} else {
				suite = createFromIterationTestPlanSquashAutom(contextId);
			}
		} else {
			if (hasTestPlanSubset) {
				suite = createFromItemsAndTestSuiteSquashAutom(subset, contextId);
			} else {
				suite = createFromTestSuiteTestPlanSquashAutom(contextId);
			}
		}

		return suite;

	}

	/**
	 * @see org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService#createFromIterationTestPlan(long)
	 */
	@Override
	@PreAuthorize(EXECUTE_ITERATION_OR_ROLE_ADMIN)
	public AutomatedSuiteWithSquashAutomAutomatedITPIs createFromIterationTestPlanSquashAutom(long iterationId) {
		Iteration iteration = iterationDao.findById(iterationId);
		List<IterationTestPlanItem> items = iteration.getTestPlans();
		Long projectId = items.get(0).getProject().getId();
		List<IterationTestPlanItem> squashTFAutomatedItems = extractSquashTFAutomatedItems(items);
		List<IterationTestPlanItem> squashAutomAutomatedItems = extractSquashAutomAutomatedItems(items);
		//Because of session clearing, automated suite has to be called after getting ITPI's list and projectId
		String newSuiteId = createSuiteAndClearSession(iteration);
		return createAutomatedSuiteWithSquashAutomAutomatedITPIs(newSuiteId, projectId, squashTFAutomatedItems, squashAutomAutomatedItems);
	}

	@Override
	@PreAuthorize(EXECUTE_TS_OR_ROLE_ADMIN)
	public AutomatedSuiteWithSquashAutomAutomatedITPIs createFromTestSuiteTestPlanSquashAutom(long testSuiteId) {
		TestSuite testSuite = testSuiteDao.getOne(testSuiteId);
		List<IterationTestPlanItem> items = testSuite.getTestPlan();
		Long projectId = items.get(0).getProject().getId();
		List<IterationTestPlanItem> squashTFAutomatedItems = extractSquashTFAutomatedItems(items);
		List<IterationTestPlanItem> squashAutomAutomatedItems = extractSquashAutomAutomatedItems(items);
		//Because of session clearing, automated suite has to be called after getting ITPI's list and projectId
		String newSuiteId = createSuiteAndClearSession(testSuite);
		return createAutomatedSuiteWithSquashAutomAutomatedITPIs(newSuiteId, projectId, squashTFAutomatedItems, squashAutomAutomatedItems);
	}

	@Override
	public AutomatedSuiteWithSquashAutomAutomatedITPIs createFromItemsAndIterationSquashAutom(List<Long> testPlanIds, long iterationId) {
		PermissionsUtils.checkPermission(permissionService, testPlanIds, EXECUTE, IterationTestPlanItem.class.getName());
		Iteration iteration = iterationDao.findById(iterationId);
		List<IterationTestPlanItem> items = testPlanDao.findAllByIdsOrderedByIterationTestPlan(testPlanIds);
		Long projectId = items.get(0).getProject().getId();
		List<IterationTestPlanItem> squashTFAutomatedItems = extractSquashTFAutomatedItems(items);
		List<IterationTestPlanItem> squashAutomAutomatedItems = extractSquashAutomAutomatedItems(items);
		//Because of session clearing, automated suite has to be called after getting ITPI's list and projectId
		String newSuiteId = createSuiteAndClearSession(iteration);
		return createAutomatedSuiteWithSquashAutomAutomatedITPIs(newSuiteId, projectId, squashTFAutomatedItems, squashAutomAutomatedItems);
	}

	@Override
	public AutomatedSuiteWithSquashAutomAutomatedITPIs createFromItemsAndTestSuiteSquashAutom(List<Long> testPlanIds, long testSuiteId) {
		PermissionsUtils.checkPermission(permissionService, testPlanIds, EXECUTE, IterationTestPlanItem.class.getName());
		TestSuite testSuite = testSuiteDao.getOne(testSuiteId);
		List<IterationTestPlanItem> items = testPlanDao.findAllByIdsOrderedBySuiteTestPlan(testPlanIds, testSuiteId);
		Long projectId = items.get(0).getProject().getId();
		List<IterationTestPlanItem> squashTFAutomatedItems = extractSquashTFAutomatedItems(items);
		List<IterationTestPlanItem> squashAutomAutomatedItems = extractSquashAutomAutomatedItems(items);
		//Because of session clearing, automated suite has to be called after getting ITPI's list and projectId
		String newSuiteId = createSuiteAndClearSession(testSuite);
		return createAutomatedSuiteWithSquashAutomAutomatedITPIs(newSuiteId, projectId, squashTFAutomatedItems, squashAutomAutomatedItems);
	}

	private AutomatedSuiteWithSquashAutomAutomatedITPIs createAutomatedSuiteWithSquashAutomAutomatedITPIs(String newAutoSuiteId, Long projectId, List<IterationTestPlanItem> squashTFAutomatedItems, List<IterationTestPlanItem> squashAutomAutomatedItems) {

		if (!squashTFAutomatedItems.isEmpty()) {
			createExecutionsFromItemsAndClearSession(squashTFAutomatedItems, newAutoSuiteId, projectId);
		}
		AutomatedSuite suite = entityManager.find(AutomatedSuite.class, newAutoSuiteId);

		return new AutomatedSuiteWithSquashAutomAutomatedITPIs(suite, squashAutomAutomatedItems);
	}

	// ******************* private permission check methods ***************************

	// assumes that the specification was validated first
	private void checkPermission(AutomatedSuiteCreationSpecification specification) {
		List<Long> singleId = new ArrayList<>();
		singleId.add(specification.getContext().getId());

		Class<?> clazz = (specification.getContext().getType() == EntityType.ITERATION) ? Iteration.class : TestSuite.class;
		PermissionsUtils.checkPermission(permissionService, singleId, EXECUTE, clazz.getName());
	}

	private void checkPermissionOnIteration(Long iterationId) {
		if (!permissionService.hasRole(Roles.ROLE_TA_API_CLIENT) && !permissionService.hasRoleOrPermissionOnObject(Roles.ROLE_ADMIN, EXECUTE, iterationId, Iteration.class.getName())) {
			throw new AccessDeniedException("Access is denied");
		}
	}

	private void checkPermissionOnTestSuite(Long testSuiteId) {
		if (!permissionService.hasRole(Roles.ROLE_TA_API_CLIENT) && !permissionService.hasRoleOrPermissionOnObject(Roles.ROLE_ADMIN, EXECUTE, testSuiteId, TestSuite.class.getName())) {
			throw new AccessDeniedException("Access is denied");
		}
	}

	@Override
	// security delegated to start(AutomatedSuite, Collection)
	public void start(String autoSuiteId) {
		AutomatedSuite suite = autoSuiteDao.findById(autoSuiteId);
		start(suite, new ArrayList<SuiteExecutionConfiguration>());
	}

	@Override
	// security delegated to start(AutomatedSuite, Collection)
	public void start(AutomatedSuite suite) {
		start(suite, new ArrayList<SuiteExecutionConfiguration>());
	}

	@Override
	// security delegated to start(AutomatedSuite, Collection)
	public void start(String suiteId, Collection<SuiteExecutionConfiguration> configuration) {
		AutomatedSuite suite = autoSuiteDao.findById(suiteId);
		start(suite, configuration);
	}


	@Override
	// security handled in the code
	public void start(AutomatedSuite suite, Collection<SuiteExecutionConfiguration> configuration) {
		List<AutomatedExecutionExtender> executionExtenders = getOptimizedExecutionsExtenders(suite);
		LOGGER.debug("- START CHECKING EXECUTIONS PERMISSIONS " + new Date());
		PermissionsUtils.checkPermission(permissionService, executionExtenders, EXECUTE);
		LOGGER.debug("- END CHECKING EXECUTIONS PERMISSIONS " + new Date());
		LOGGER.debug("- START SORTING EXECUTIONS " + new Date());
		ExtenderSorter sorter = new ExtenderSorter(suite, configuration);
		LOGGER.debug("- END SORTING EXECUTIONS " + new Date());
		LOGGER.debug("- START COLLECTING AND SENDING ALL AUTOMATED EXECUTIONS " + new Date());
		TestAutomationCallbackService securedCallback = new CallbackServiceSecurityWrapper(callbackService);

		while (sorter.hasNext()) {

			Entry<TestAutomationServerKind, Collection<AutomatedExecutionExtender>> extendersByKind = sorter.getNextEntry();

			TestAutomationConnector connector;

			try {
				connector = connectorRegistry.getConnectorForKind(extendersByKind.getKey());
				LOGGER.debug("-- START COLLECTING AUTOMATED EXECUTIONS FOR " + extendersByKind.getKey() + " " + new Date());
				Collection<Couple<AutomatedExecutionExtender, Map<String, Object>>> tests = collectAutomatedExecs(extendersByKind
					.getValue(), true);
				LOGGER.debug("-- END COLLECTING AUTOMATED EXECUTIONS FOR " + extendersByKind.getKey() + " " + new Date());
				LOGGER.debug("-- START SENDING AUTOMATED EXECUTIONS FOR " + extendersByKind.getKey() + " " + new Date());
				connector.executeParameterizedTests(tests, suite.getId(), securedCallback);
				LOGGER.debug("-- END SENDING AUTOMATED EXECUTIONS FOR " + extendersByKind.getKey() + " " + new Date());
			} catch (UnknownConnectorKind ex) {
				if (LOGGER.isErrorEnabled()) {
					LOGGER.error("Test Automation : unknown connector :", ex);
				}
				notifyExecutionError(extendersByKind.getValue(), ex.getMessage());
			} catch (TestAutomationException ex) {
				if (LOGGER.isErrorEnabled()) {
					LOGGER.error("Test Automation : an error occured :", ex);
				}
				notifyExecutionError(extendersByKind.getValue(), ex.getMessage());
			}

		}
		LOGGER.debug("- END COLLECTING AND SENDING ALL EXECUTIONS " + new Date());
	}

	@Override
	// security handled in the code
	public void start(
		AutomatedSuiteWithSquashAutomAutomatedITPIs suite,
		Collection<SuiteExecutionConfiguration> jenkinsConfigurations,
		Collection<SquashAutomExecutionConfiguration> squashAutomConfigurations) {

		List<AutomatedExecutionExtender> executionExtenders = getOptimizedExecutionsExtenders(suite.getSuite());
		List<IterationTestPlanItem> itpis = getOptimizedITPIs(suite.getSquashAutomAutomatedItems());
		LOGGER.debug("- START CHECKING EXECUTIONS PERMISSIONS " + new Date());
		PermissionsUtils.checkPermission(permissionService, executionExtenders, EXECUTE);
		LOGGER.debug("- END CHECKING EXECUTIONS PERMISSIONS " + new Date());
		LOGGER.debug("- START CHECKING ITPIS PERMISSIONS " + new Date());
		PermissionsUtils.checkPermission(permissionService, itpis, EXECUTE);
		LOGGER.debug("- END CHECKING ITPIS PERMISSIONS " + new Date());
		LOGGER.debug("- START SORTING EXECUTIONS " + new Date());
		ExtenderSorter extenderSorter = new ExtenderSorter(suite.getSuite(), jenkinsConfigurations);
		LOGGER.debug("- END SORTING EXECUTIONS " + new Date());
		LOGGER.debug("- START SORTING ITPIS " + new Date());
		ITPISorter itpiSorter = new ITPISorter(itpis);
		LOGGER.debug("- END SORTING ITPIS " + new Date());
		LOGGER.debug("- START COLLECTING AND SENDING ALL AUTOMATED EXECUTIONS " + new Date());
		TestAutomationCallbackService securedCallback = new CallbackServiceSecurityWrapper(callbackService);

		while (extenderSorter.hasNext()) {

			Entry<TestAutomationServerKind, Collection<AutomatedExecutionExtender>> extendersByKind = extenderSorter.getNextEntry();

			TestAutomationConnector connector;

			try {
				connector = connectorRegistry.getConnectorForKind(extendersByKind.getKey());
				LOGGER.debug("-- START COLLECTING AUTOMATED EXECUTIONS FOR " + extendersByKind.getKey() + " " + new Date());
				Collection<Couple<AutomatedExecutionExtender, Map<String, Object>>> tests = collectAutomatedExecs(extendersByKind
					.getValue(), true);
				LOGGER.debug("-- END COLLECTING AUTOMATED EXECUTIONS FOR " + extendersByKind.getKey() + " " + new Date());
				LOGGER.debug("-- START SENDING AUTOMATED EXECUTIONS FOR " + extendersByKind.getKey() + " " + new Date());
				connector.executeParameterizedTests(tests, suite.getSuite().getId(), securedCallback);
				LOGGER.debug("-- END SENDING AUTOMATED EXECUTIONS FOR " + extendersByKind.getKey() + " " + new Date());
			} catch (UnknownConnectorKind ex) {
				if (LOGGER.isErrorEnabled()) {
					LOGGER.error("Test Automation : unknown connector :", ex);
				}
				notifyExecutionError(extendersByKind.getValue(), ex.getMessage());
			} catch (TestAutomationException ex) {
				if (LOGGER.isErrorEnabled()) {
					LOGGER.error("Test Automation : an error occured :", ex);
				}
				notifyExecutionError(extendersByKind.getValue(), ex.getMessage());
			}

		}
		LOGGER.debug("- END COLLECTING AND SENDING ALL EXECUTIONS " + new Date());

		LOGGER.debug("- START COLLECTING AND SENDING ALL AUTOMATED ITPIS " + new Date());
		while (itpiSorter.hasNext()) {

			Entry<TestAutomationServerKind, Collection<IterationTestPlanItem>> itemsByKind = itpiSorter.getNextEntry();

			TestAutomationConnector connector;

			try {
				connector = connectorRegistry.getConnectorForKind(itemsByKind.getKey());
				LOGGER.debug("-- START COLLECTING AUTOMATED EXECUTIONS FOR " + itemsByKind.getKey() + " " + new Date());
				Collection<IterationTestPlanItemWithCustomFields> tests = collectItpis(itemsByKind
					.getValue(), true);
				LOGGER.debug("-- END COLLECTING AUTOMATED EXECUTIONS FOR " + itemsByKind.getKey() + " " + new Date());
				LOGGER.debug("-- START SENDING AUTOMATED EXECUTIONS FOR " + itemsByKind.getKey() + " " + new Date());
				connector.executeParameterizedTestsBasedOnITPICollection(
					tests,
					suite.getSuite().getId(),
					securedCallback,
					squashAutomConfigurations);
				LOGGER.debug("-- END SENDING AUTOMATED EXECUTIONS FOR " + itemsByKind.getKey() + " " + new Date());
			} catch (UnknownConnectorKind ex) {
				if (LOGGER.isErrorEnabled()) {
					LOGGER.error("Test Automation : unknown connector :", ex);
				}
				notifyItpiExecutionError(itemsByKind.getValue(), ex.getMessage());
			} catch (TestAutomationException ex) {
				if (LOGGER.isErrorEnabled()) {
					LOGGER.error("Test Automation : an error occured :", ex);
				}
				notifyItpiExecutionError(itemsByKind.getValue(), ex.getMessage());
			} catch (UnsupportedOperationException ex) {
				LOGGER.debug("Catch Unsupported Operation Exception");
			}

		}
		LOGGER.debug("- END COLLECTING AND SENDING ALL ITPIS " + new Date());
	}


	// ******************* create suite private methods ***************************

	private void createExecutionsFromItemsAndClearSession(List<IterationTestPlanItem> items, String newSuiteId, Long projectId) {
		List<Long> itemIds = items.stream().map(IterationTestPlanItem::getId).collect(Collectors.toList());
		List<List<Long>> partitionedIds = Lists.partition(itemIds, 10);
		for (List<Long> ids : partitionedIds) {
			createOneBatchOfExecution(ids, newSuiteId, projectId);
			entityManager.flush();
			entityManager.clear();
		}
	}

	private String createSuiteAndClearSession() {
		AutomatedSuite newSuite = autoSuiteDao.createNewSuite();
		entityManager.flush();
		String newSuiteId = newSuite.getId();
		entityManager.clear();
		return newSuiteId;
	}

	/**
	 * Create an automated suite and link it to the given Iteration.
	 *
	 * @param iteration
	 * @return
	 */
	private String createSuiteAndClearSession(Iteration iteration) {
		AutomatedSuite newSuite = autoSuiteDao.createNewSuite(iteration);
		entityManager.flush();
		String newSuiteId = newSuite.getId();
		entityManager.clear();
		return newSuiteId;
	}

	/**
	 * Create an automated suite and link it to the given TestSuite
	 *
	 * @param testSuite
	 * @return
	 */
	private String createSuiteAndClearSession(TestSuite testSuite) {
		AutomatedSuite newSuite = autoSuiteDao.createNewSuite(testSuite);
		entityManager.flush();
		String newSuiteId = newSuite.getId();
		entityManager.clear();
		return newSuiteId;
	}

	private void createOneBatchOfExecution(List<Long> ids, String newSuiteId, Long projectId) {
		// prefetch the project to avoid auto proxy queries
		projectDao.fetchForAutomatedExecutionCreation(projectId);
		AutomatedSuite automatedSuite = entityManager.find(AutomatedSuite.class, newSuiteId);
		List<IterationTestPlanItem> items = testPlanDao.fetchForAutomatedExecutionCreation(ids);
		for (IterationTestPlanItem item : items) {
			if (item.isAutomated()) {
				Execution execution = item.createAutomatedExecution();
				executionDao.save(execution);
				item.addExecution(execution);
				createCustomFieldsForExecutionAndExecutionSteps(execution);
				createDenormalizedFieldsForExecutionAndExecutionSteps(execution);
				automatedSuite.addExtender(execution.getAutomatedExecutionExtender());
			}
		}
	}

	private void createCustomFieldsForExecutionAndExecutionSteps(Execution execution) {
		customFieldValuesService.createAllCustomFieldValues(execution, execution.getProject());
		customFieldValuesService.createAllCustomFieldValues(execution.getSteps(), execution.getProject());
	}

	private void createDenormalizedFieldsForExecutionAndExecutionSteps(Execution execution) {
		LOGGER.debug("Create denormalized fields for Execution {}", execution.getId());

		TestCase sourceTC = execution.getReferencedTestCase();
		denormalizedFieldValueService.createAllDenormalizedFieldValues(sourceTC, execution);
		denormalizedFieldValueService.createAllDenormalizedFieldValuesForSteps(execution);

	}

	private List<Long> doFindTpiIdsWithAutomaticExecutionMode(List<IterationTestPlanItem> itpis) {
		return itpis.stream()
			.filter(IterationTestPlanItem::isAutomated)
			.map(IterationTestPlanItem::getId)
			.collect(Collectors.toList());
	}

	private List<AutomatedExecutionExtender> getOptimizedExecutionsExtenders(AutomatedSuite suite) {
		LOGGER.debug("- START FETCHING OPTIMIZED EXECUTIONS " + new Date());
		List<AutomatedExecutionExtender> executionExtenders = autoSuiteDao.findAndFetchForAutomatedExecutionCreation(suite.getId());
		LOGGER.debug("- FETCHED " + executionExtenders.size());
		LOGGER.debug("- END FETCHING OPTIMIZED EXECUTIONS" + new Date());
		return executionExtenders;
	}

	private List<IterationTestPlanItem> getOptimizedITPIs(List<IterationTestPlanItem> items) {
		LOGGER.debug("- START FETCHING OPTIMIZED ITPIS" + new Date());
		List<Long> itemIds = items.stream().map(IterationTestPlanItem::getId).collect(Collectors.toList());
		List<IterationTestPlanItem> itpis = testPlanDao.fetchForAutomatedExecutionCreation(itemIds);
		LOGGER.debug("- FETCHED " + itpis.size());
		LOGGER.debug("- END FETCHING OPTIMIZED ITPIS" + new Date());
		return itpis;
	}


	// ******************* execute suite private methods **************************

	private Collection<Couple<AutomatedExecutionExtender, Map<String, Object>>> collectAutomatedExecs(
		Collection<AutomatedExecutionExtender> extenders, boolean withAllCustomFields) {

		Collection<Couple<AutomatedExecutionExtender, Map<String, Object>>> tests = new ArrayList<>(
			extenders.size());

		CustomFieldValuesForExec customFieldValuesForExec = fetchCustomFieldValues(extenders, withAllCustomFields);
		for (AutomatedExecutionExtender extender : extenders) {
			tests.add(createAutomatedExecAndParams(extender, customFieldValuesForExec));
		}
		return tests;

	}

	private CustomFieldValuesForExec fetchCustomFieldValues(Collection<AutomatedExecutionExtender> extenders, boolean withAllCustomFields) {
		Map<Long, List<CustomFieldValue>> testCaseCfv = fetchTestCaseCfv(extenders);
		Map<Long, List<CustomFieldValue>> iterationCfv = withAllCustomFields ? fetchIterationCfv(extenders) : Collections.emptyMap();
		Map<Long, List<CustomFieldValue>> campaignCfv = withAllCustomFields ? fetchCampaignCfv(extenders) : Collections.emptyMap();
		Map<Long, List<CustomFieldValue>> testSuiteCfv = withAllCustomFields ? fetchTestSuiteCfv(extenders) : Collections.emptyMap();
		return new CustomFieldValuesForExec(testCaseCfv, iterationCfv, campaignCfv, testSuiteCfv);
	}

	private Map<Long, List<CustomFieldValue>> fetchTestCaseCfv(Collection<AutomatedExecutionExtender> extenders) {
		List<TestCase> testCases = extenders.stream()
			.map(extender -> extender
				.getExecution()
				.getReferencedTestCase())
			.collect(Collectors.toList());
		return customFieldValueFinder.findAllCustomFieldValues(testCases).stream().collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
	}

	private Map<Long, List<CustomFieldValue>> fetchIterationCfv(Collection<AutomatedExecutionExtender> extenders) {
		List<Iteration> iterations = extenders.stream()
			.map(extender -> extender
				.getExecution()
				.getTestPlan()
				.getIteration())
			.collect(Collectors.toList());
		return customFieldValueFinder.findAllCustomFieldValues(iterations).stream().collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
	}

	private Map<Long, List<CustomFieldValue>> fetchCampaignCfv(Collection<AutomatedExecutionExtender> extenders) {
		List<Campaign> campaigns = extenders.stream()
			.map(extender -> extender
				.getExecution()
				.getTestPlan()
				.getIteration()
				.getCampaign())
			.collect(Collectors.toList());
		return customFieldValueFinder.findAllCustomFieldValues(campaigns).stream().collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
	}

	private Map<Long, List<CustomFieldValue>> fetchTestSuiteCfv(Collection<AutomatedExecutionExtender> extenders) {
		List<TestSuite> testSuites = extenders.stream()
			.map(extender -> extender
				.getExecution()
				.getTestPlan()
				.getTestSuites())
			.flatMap(Collection::stream)
			.collect(Collectors.toList());
		return customFieldValueFinder.findAllCustomFieldValues(testSuites).stream().collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
	}

	private Couple<AutomatedExecutionExtender, Map<String, Object>> createAutomatedExecAndParams(AutomatedExecutionExtender extender, CustomFieldValuesForExec customFieldValuesForExec) {
		Execution execution = extender.getExecution();

		Collection<CustomFieldValue> tcFields = customFieldValuesForExec.getValueForTestcase(execution.getReferencedTestCase().getId());
		Collection<CustomFieldValue> iterFields = customFieldValuesForExec.getValueForIteration(execution.getIteration().getId());
		Collection<CustomFieldValue> campFields = customFieldValuesForExec.getValueForCampaign(execution.getCampaign().getId());
		Collection<CustomFieldValue> testSuiteFields = execution
			.getTestPlan()
			.getTestSuites()
			.stream()
			.map(TestSuite::getId)
			.map(customFieldValuesForExec::getValueForTestSuite)
			.flatMap(Collection::stream)
			.collect(Collectors.toList());

		Map<String, Object> params = paramBuilder.get().testCase().addEntity(
			execution.getReferencedTestCase()).addCustomFields(tcFields).
			iteration().addCustomFields(iterFields).
			campaign().addCustomFields(campFields).
			testSuite().addCustomFields(testSuiteFields).
			dataset().addEntity(execution.getTestPlan().getReferencedDataset())
			.build();

		return new Couple<>(extender, params);
	}

	private void notifyExecutionError(Collection<AutomatedExecutionExtender> failedExecExtenders, String message) {
		for (AutomatedExecutionExtender extender : failedExecExtenders) {
			extender.setExecutionStatus(ExecutionStatus.ERROR);
			extender.setResultSummary(HtmlUtils.htmlEscape(message));
		}
	}

	// ******************* execute suite with itpi (Squash Autom) private methods **************************

	private List<IterationTestPlanItem> extractSquashAutomAutomatedItems(List<IterationTestPlanItem> items) {
		return items.stream().filter(item -> {
			TestCase tc = item.getReferencedTestCase();
			boolean isAutomated = false;

			if (tc.getProject().isAllowAutomationWorkflow()) {
				isAutomated = tc.hasAutomatedStatus() && tc.isSquashAutomAutomated();
			} else {
				isAutomated = tc.isSquashAutomAutomated();
			}
			return isAutomated;
		}).collect(Collectors.toList());
	}

	private List<IterationTestPlanItem> extractSquashTFAutomatedItems(List<IterationTestPlanItem> items) {
		return items.stream().filter(item -> {
			TestCase tc = item.getReferencedTestCase();
			boolean isAutomated = false;

			if (tc.getProject().isAllowAutomationWorkflow()) {
				isAutomated = tc.hasAutomatedStatus() && tc.isSquashTFAutomated();
			} else {
				isAutomated = tc.isSquashTFAutomated();
			}
			return isAutomated;
		}).collect(Collectors.toList());
	}

	private Collection<IterationTestPlanItemWithCustomFields> collectItpis(
		Collection<IterationTestPlanItem> itpis, boolean withAllCustomFields) {

		Collection<IterationTestPlanItemWithCustomFields> tests = new ArrayList<>(
			itpis.size());

		CustomFieldValuesForExec customFieldValuesForItpi = fetchCustomFieldValuesForITPIs(itpis, withAllCustomFields);
		for (IterationTestPlanItem item : itpis) {
			tests.add(createItemAndParams(item, customFieldValuesForItpi));
		}
		return tests;

	}

	private CustomFieldValuesForExec fetchCustomFieldValuesForITPIs(Collection<IterationTestPlanItem> items, boolean withAllCustomFields) {
		Map<Long, List<CustomFieldValue>> testCaseCfv = fetchTestCaseCfvForITPIs(items);
		Map<Long, List<CustomFieldValue>> iterationCfv = withAllCustomFields ? fetchIterationCfvForITPIs(items) : Collections.emptyMap();
		Map<Long, List<CustomFieldValue>> campaignCfv = withAllCustomFields ? fetchCampaignCfvForITPIs(items) : Collections.emptyMap();
		Map<Long, List<CustomFieldValue>> testSuiteCfv = withAllCustomFields ? fetchTestSuiteCfvForITPIs(items) : Collections.emptyMap();
		return new CustomFieldValuesForExec(testCaseCfv, iterationCfv, campaignCfv, testSuiteCfv);
	}

	private Map<Long, List<CustomFieldValue>> fetchTestCaseCfvForITPIs(Collection<IterationTestPlanItem> items) {
		List<TestCase> testCases = items.stream()
			.map(IterationTestPlanItem::getReferencedTestCase)
			.collect(Collectors.toList());
		return customFieldValueFinder.findAllCustomFieldValues(testCases).stream().collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
	}

	private Map<Long, List<CustomFieldValue>> fetchIterationCfvForITPIs(Collection<IterationTestPlanItem> items) {
		List<Iteration> iterations = items.stream()
			.map(IterationTestPlanItem::getIteration)
			.collect(Collectors.toList());
		return customFieldValueFinder.findAllCustomFieldValues(iterations).stream().collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
	}

	private Map<Long, List<CustomFieldValue>> fetchCampaignCfvForITPIs(Collection<IterationTestPlanItem> items) {
		List<Campaign> campaigns = items.stream()
			.map(item -> item
				.getIteration()
				.getCampaign())
			.collect(Collectors.toList());
		return customFieldValueFinder.findAllCustomFieldValues(campaigns).stream().collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
	}

	private Map<Long, List<CustomFieldValue>> fetchTestSuiteCfvForITPIs(Collection<IterationTestPlanItem> extenders) {
		List<TestSuite> testSuites = extenders.stream()
			.map(IterationTestPlanItem::getTestSuites)
			.flatMap(Collection::stream)
			.collect(Collectors.toList());
		return customFieldValueFinder.findAllCustomFieldValues(testSuites).stream().collect(Collectors.groupingBy(CustomFieldValue::getBoundEntityId));
	}

	private IterationTestPlanItemWithCustomFields createItemAndParams(IterationTestPlanItem item, CustomFieldValuesForExec customFieldValuesForExec) {

		Collection<CustomFieldValue> tcFields = customFieldValuesForExec.getValueForTestcase(item.getReferencedTestCase().getId());
		Collection<CustomFieldValue> iterFields = customFieldValuesForExec.getValueForIteration(item.getIteration().getId());
		Collection<CustomFieldValue> campFields = customFieldValuesForExec.getValueForCampaign(item.getCampaign().getId());
		Collection<CustomFieldValue> testSuiteFields = item
			.getTestSuites()
			.stream()
			.map(TestSuite::getId)
			.map(customFieldValuesForExec::getValueForTestSuite)
			.flatMap(Collection::stream)
			.collect(Collectors.toList());

		ScmRepository itemAutomRepository = item.getReferencedTestCase().getScmRepository();
		Credentials credentials = null;

		if (itemAutomRepository != null) {
			ScmServer server = item.getReferencedTestCase().getScmRepository().getScmServer();
			Optional<Credentials> maybeCredentials = credentialsProvider.getAppLevelCredentials(server);
			credentials = maybeCredentials.orElse(null);
		}

		Map<String, Object> params = paramBuilder.get().testCase().addEntity(
			item.getReferencedTestCase()).addCustomFields(tcFields).
			iteration().addCustomFields(iterFields).
			campaign().addCustomFields(campFields).
			testSuite().addCustomFields(testSuiteFields).
			dataset().addEntity(item.getReferencedDataset()).
			scmRepository().addEntity(itemAutomRepository).
			scmRepositoryCredentials().addEntity(credentials)
			.build();

		return new IterationTestPlanItemWithCustomFields(item, params);
	}

	private void notifyItpiExecutionError(Collection<IterationTestPlanItem> failedItpis, String message) {
		for (IterationTestPlanItem item : failedItpis) {
			item.setExecutionStatus(ExecutionStatus.ERROR);
		}
	}


	/**
	 * That wrapper is a TestAutomationCallbackService, that ensures that the security context is properly set for any
	 * thread that requires its services.
	 *
	 * @author bsiri
	 */
	private static class CallbackServiceSecurityWrapper implements TestAutomationCallbackService {

		private SecurityContext secContext;

		private TestAutomationCallbackService wrapped;

		/*
		 * the SecurityContext here is the one from the original thread. The others methods will use that instance of
		 * SecurityContext for all their operations from now on (see the code, it's straightforward).
		 */
		CallbackServiceSecurityWrapper(TestAutomationCallbackService service) {
			secContext = SecurityContextHolder.getContext();
			wrapped = service;
		}

		@Override
		public void updateResultURL(AutomatedExecutionSetIdentifier execIdentifier, URL resultURL) {
			SecurityContextHolder.setContext(secContext);
			wrapped.updateResultURL(execIdentifier, resultURL);
		}

		@Override
		public void updateExecutionStatus(AutomatedExecutionSetIdentifier execIdentifier, ExecutionStatus newStatus) {
			SecurityContextHolder.setContext(secContext);
			wrapped.updateExecutionStatus(execIdentifier, newStatus);

		}

		@Override
		public void updateResultSummary(AutomatedExecutionSetIdentifier execIdentifier, String newSummary) {
			SecurityContextHolder.setContext(secContext);
			wrapped.updateResultSummary(execIdentifier, newSummary);
		}

	}


	private static class ExtenderSorter {

		private Map<Long, SuiteExecutionConfiguration> configurationByProject;

		private Map<TestAutomationServerKind, Collection<AutomatedExecutionExtender>> extendersByKind;

		private Iterator<Entry<TestAutomationServerKind, Collection<AutomatedExecutionExtender>>> iterator = null;

		public ExtenderSorter(AutomatedSuite suite, Collection<SuiteExecutionConfiguration> configuration) {

			configurationByProject = new HashMap<>(configuration.size());

			for (SuiteExecutionConfiguration conf : configuration) {
				configurationByProject.put(conf.getProjectId(), conf);
			}

			// rem : previous impl relied on a HashMap, which broke the tests on java 8. as I have no damn clue about
			// the desired order, let's retort to keys natural order using a TreeMap
			extendersByKind = new TreeMap<>();

			for (AutomatedExecutionExtender extender : suite.getExecutionExtenders()) {

				TestAutomationServerKind serverKind = extender.getAutomatedTest().getProject().getServer().getKind();

				register(extender, serverKind);

			}

			iterator = extendersByKind.entrySet().iterator();

		}

		public boolean hasNext() {
			return iterator.hasNext();
		}

		public Map.Entry<TestAutomationServerKind, Collection<AutomatedExecutionExtender>> getNextEntry() {

			return iterator.next();

		}

		private void register(AutomatedExecutionExtender extender, TestAutomationServerKind serverKind) {

			if (!extendersByKind.containsKey(serverKind)) {
				extendersByKind.put(serverKind, new LinkedList<AutomatedExecutionExtender>());
			}

			SuiteExecutionConfiguration conf = configurationByProject.get(extender.getAutomatedProject().getId());
			if (conf != null) {
				extender.setNodeName(conf.getNode());
			}

			extendersByKind.get(serverKind).add(extender);

		}

	}

	private static class ITPISorter {


		private Map<TestAutomationServerKind, Collection<IterationTestPlanItem>> itemsByKind;

		private Iterator<Entry<TestAutomationServerKind, Collection<IterationTestPlanItem>>> iterator = null;

		public ITPISorter(List<IterationTestPlanItem> items) {


			// rem : previous impl relied on a HashMap, which broke the tests on java 8. as I have no damn clue about
			// the desired order, let's retort to keys natural order using a TreeMap
			itemsByKind = new TreeMap<>();

			for (IterationTestPlanItem item : items) {

				TestAutomationServerKind serverKind = item.getReferencedTestCase().getProject().getTestAutomationServer().getKind();

				register(item, serverKind);

			}

			iterator = itemsByKind.entrySet().iterator();

		}

		public boolean hasNext() {
			return iterator.hasNext();
		}

		public Map.Entry<TestAutomationServerKind, Collection<IterationTestPlanItem>> getNextEntry() {

			return iterator.next();

		}

		private void register(IterationTestPlanItem extender, TestAutomationServerKind serverKind) {

			if (!itemsByKind.containsKey(serverKind)) {
				itemsByKind.put(serverKind, new LinkedList<IterationTestPlanItem>());
			}

			itemsByKind.get(serverKind).add(extender);

		}

	}

	private static final class ExecutionCollector implements Transformer {
		@Override
		public Object transform(Object input) {
			return ((AutomatedExecutionExtender) input).getExecution();
		}
	}

	public static class CustomFieldValuesForExec {
		Map<Long, List<CustomFieldValue>> testCaseCfv;
		Map<Long, List<CustomFieldValue>> iterationCfv;
		Map<Long, List<CustomFieldValue>> campaignCfv;
		Map<Long, List<CustomFieldValue>> suiteCfv;

		public CustomFieldValuesForExec(Map<Long, List<CustomFieldValue>> testCaseCfv, Map<Long, List<CustomFieldValue>> iterationCfv, Map<Long, List<CustomFieldValue>> campaignCfv, Map<Long, List<CustomFieldValue>> suiteCfv) {
			this.testCaseCfv = testCaseCfv;
			this.iterationCfv = iterationCfv;
			this.campaignCfv = campaignCfv;
			this.suiteCfv = suiteCfv;
		}

		public List<CustomFieldValue> getValueForTestcase(Long testCaseId) {
			return this.testCaseCfv.getOrDefault(testCaseId, emptyList());
		}

		public List<CustomFieldValue> getValueForIteration(Long iterationId) {
			return this.iterationCfv.getOrDefault(iterationId, emptyList());
		}

		public List<CustomFieldValue> getValueForCampaign(Long campaignId) {
			return this.campaignCfv.getOrDefault(campaignId, emptyList());
		}

		public List<CustomFieldValue> getValueForTestSuite(Long suiteId) {
			return this.suiteCfv.getOrDefault(suiteId, emptyList());
		}

	}
}

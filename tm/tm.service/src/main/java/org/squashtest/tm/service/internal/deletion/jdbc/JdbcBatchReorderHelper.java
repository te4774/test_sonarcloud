/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import org.jooq.BatchBindStep;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record3;
import org.jooq.Result;
import org.jooq.Table;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * WARNING : This class use raw SQL statement to MODIFY data. It could create synchronisation issues between
 * Hibernate internal session state and underlying data in database.
 * All public methods will flush and clear the session... Do not use this helper if you need your entities, in your process.
 * <p>
 * This class is dedicated to reorder our ordered collections when they are (incorrectly !) put into association tables
 */
@Component
@Transactional
public class JdbcBatchReorderHelper {

	private final EntityManager entityManager;
	private final DSLContext dslContext;
	private final Set<String> parentColumnNames = new HashSet<>(Arrays.asList("ANCESTOR_ID", "LIBRARY_ID"));
	private final Set<String> childrenColumnNames = new HashSet<>(Arrays.asList("DESCENDANT_ID", "CONTENT_ID"));
	private final Set<String> orderColumnNames = new HashSet<>(Arrays.asList("CONTENT_ORDER"));

	public JdbcBatchReorderHelper(EntityManager entityManager, DSLContext dslContext) {
		this.entityManager = entityManager;
		this.dslContext = dslContext;
	}

	public <R extends Record3<Long, Long, Integer>> void reorder(Collection<Long> parentIds, Table<R> table) {
		clearPersistenceContext();
		if (!parentIds.isEmpty()) {
			findColumnsAndReorder(parentIds, table);
		}
	}

	public <R extends Record3<Long, Long, Integer>> void reorder(Collection<Long> parentIds,
						Table<R> table,
						Field<Long> parentColumn,
						Field<Long> childrenColumn,
						Field<Integer> orderColumn) {
		clearPersistenceContext();
		if (!parentIds.isEmpty()) {
			doReorder(parentIds, table, parentColumn, childrenColumn, orderColumn);
		}
	}

	private void clearPersistenceContext() {
		entityManager.flush();
		entityManager.clear();
	}

	// To reorder it's easier and less error-prone to delete the whole affected parent content
	// end reinsert the rows.
	// Doing with update statements is a mess because of not deferrable constraints, and other nasty problems (no pk...)
	private <R extends Record3<Long, Long, Integer>> void findColumnsAndReorder(Collection<Long> parentIds, Table<R> table) {
		Field<Long> parentColumn = findCompatibleColumn(table, parentColumnNames, Long.class);
		Field<Long> childrenColumn = findCompatibleColumn(table, childrenColumnNames, Long.class);
		Field<Integer> orderColumn = findCompatibleColumn(table, orderColumnNames, Integer.class);
		doReorder(parentIds, table, parentColumn, childrenColumn, orderColumn);
	}

	private <R extends Record3<Long, Long, Integer>, T> Field<T> findCompatibleColumn(Table<R> table, Set<String> names, Class<T> clazz) {
		return Arrays.stream(table.fields())
			.filter(field -> names.contains(field.getName()))
			.filter(field -> field.getDataType().getType().equals(clazz))
			.map(field -> (Field<T>) field) // cast is safe, we filtered the datatype just above
			.findFirst()
			.orElseThrow(() -> new IllegalArgumentException("Unable to find required fields " + names + " in table : " + table));
	}

	private <R extends Record3<Long, Long, Integer>> void doReorder(Collection<Long> parentIds,
																	Table<R> table,
																	Field<Long> parentColumn,
																	Field<Long> childrenColumn,
																	Field<Integer> orderColumn) {
		Map<Long, Result<R>> groups = findGroups(parentIds, table, parentColumn, orderColumn);
		deleteAllRecords(parentIds, table, parentColumn);
		doBatchReorder(groups, table, parentColumn, childrenColumn, orderColumn);
	}

	private <R extends Record3<Long, Long, Integer>> void deleteAllRecords(Collection<Long> parentIds,
																		   Table<R> table,
																		   Field<Long> parentColumn) {
		dslContext.delete(table)
			.where(parentColumn.in(parentIds))
			.execute();
	}

	private <R extends Record3<Long, Long, Integer>> Map<Long, Result<R>> findGroups(Collection<Long> parentIds,
																			 Table<R> table,
																			 Field<Long> parentColumn,
																			 Field<Integer> orderColumn) {
		return dslContext
			.selectFrom(table)
			.where(parentColumn.in(parentIds))
			.orderBy(orderColumn)
			.fetchGroups(parentColumn);
	}

	private <R extends Record3<Long, Long, Integer>> void doBatchReorder( Map<Long, Result<R>> groups,
								Table<R> table,
								Field<Long> parentColumn,
								Field<Long> childrenColumn,
								Field<Integer> orderColumn) {
		int numberOfItems = groups.values().stream().mapToInt(List::size).sum();
		if (numberOfItems > 0) { // avoiding insert into with (null, null, null) bound to statement
			BatchBindStep batch = dslContext.batch(
				dslContext.insertInto(table,
						parentColumn, childrenColumn, orderColumn)
					.values((Long) null, null, null));
			// see jooq documentation to see why this form is mandatory. Please check that there will be at least one bind
			// else when executing the batch if no bind is done, it will result in bad statement : insert into... values(null, null, null)

			groups.forEach((parentId, children) -> {
				IntStream
					.range(0, children.size())
					.forEach(position -> {
						Record3<Long, Long, Integer> record = children.get(position);
						batch.bind(record.getValue(parentColumn), record.getValue(childrenColumn), position);
					});
			});
			batch.execute();
		}
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.servers;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.servers.StoredCredentials;
import org.squashtest.tm.domain.servers.ThirdPartyServer;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.ServerAuthConfiguration;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.function.Supplier;

/**
 * A collection of objects used as smart input bags for StoredCredentialManager's methods, allowing to isolate
 * concerns specific to each content type.
 */
public class StoredContentHandlers {
	private static final String FIND_SERVER_AUTH_CONF = "StoredCredentials.findServerAuthConfByServerId";
	private static final String FIND_APP_LEVEL_CREDENTIALS = "StoredCredentials.findAppLevelCredentialsByServerId";
	private static final String FIND_USER_CREDENTIALS = "StoredCredentials.findUserCredentialsByServerId";
	private static final String FIND_PROJECT_CREDENTIALS = "StoredCredentials.findProjectCredentialsByServerId";


	private static final String PARAM_SERVER_ID = "serverId";
	private static final String PARAM_PROJECT_ID = "projectId";
	private static final String PARAM_USERNAME = "username";

	interface StoredContentHandler<T> {
		Query getLocateQuery(EntityManager em);

		Class<T> getDeserializationClass();

		StoredCredentials buildNewStoredCredentials(EntityManager em,
													Crypto.EncryptionOutcome encryptionOutcome);
	}

	public static class AuthConfigurationHandler implements StoredContentHandler<ServerAuthConfiguration> {
		private final long serverId;

		public AuthConfigurationHandler(long serverId) {
			this.serverId = serverId;
		}

		@Override
		public Query getLocateQuery(EntityManager em) {
			return em.createNamedQuery(FIND_SERVER_AUTH_CONF)
				.setParameter(PARAM_SERVER_ID, serverId);
		}

		@Override
		public Class<ServerAuthConfiguration> getDeserializationClass() {
			return ServerAuthConfiguration.class;
		}

		@Override
		public StoredCredentials buildNewStoredCredentials(EntityManager em,
														   Crypto.EncryptionOutcome encryptionOutcome) {
			final StoredCredentials sc = new StoredCredentials();
			sc.setContentType(StoredCredentials.ContentType.CONF);
			sc.setAuthenticatedServer(em.find(ThirdPartyServer.class, serverId));
			sc.setEncryptedCredentials(encryptionOutcome.getEncryptedText());
			sc.setEncryptionVersion(encryptionOutcome.getVersion());
			return sc;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this)
				.append(PARAM_SERVER_ID, serverId)
				.toString();
		}
	}

	abstract static class CredentialsHandler implements StoredContentHandler<ManageableCredentials> {
		protected final long serverId;

		protected CredentialsHandler(long serverId) {
			this.serverId = serverId;
		}

		@Override
		public Class<ManageableCredentials> getDeserializationClass() {
			return ManageableCredentials.class;
		}

		@Override
		public StoredCredentials buildNewStoredCredentials(EntityManager em, Crypto.EncryptionOutcome encryptionOutcome) {
			final StoredCredentials sc = new StoredCredentials();
			sc.setContentType(StoredCredentials.ContentType.CRED);
			sc.setAuthenticatedServer(em.find(ThirdPartyServer.class, serverId));
			sc.setEncryptedCredentials(encryptionOutcome.getEncryptedText());
			sc.setEncryptionVersion(encryptionOutcome.getVersion());
			return sc;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this)
				.append(PARAM_SERVER_ID, serverId)
				.toString();
		}
	}

	public static class AppLevelHandler extends CredentialsHandler {
		protected AppLevelHandler(long serverId) {
			super(serverId);
		}

		@Override
		public Query getLocateQuery(EntityManager em) {
			return em.createNamedQuery(FIND_APP_LEVEL_CREDENTIALS)
				.setParameter(PARAM_SERVER_ID, serverId);
		}
	}

	public static class UserCredentialsHandler extends AppLevelHandler {
		public final String username;

		// Delegate for finding the user by username
		private final Supplier<User> userSupplier;

		UserCredentialsHandler(long serverId,
							   String username,
							   Supplier<User> userSupplier) {
			super(serverId);
			this.username = username;
			this.userSupplier = userSupplier;
		}

		@Override
		public Query getLocateQuery(EntityManager em) {
			return em.createNamedQuery(FIND_USER_CREDENTIALS)
				.setParameter(PARAM_SERVER_ID, serverId)
				.setParameter(PARAM_USERNAME, username);
		}

		@Override
		public StoredCredentials buildNewStoredCredentials(EntityManager em,
														   Crypto.EncryptionOutcome encryptionOutcome) {
			final StoredCredentials sc = super.buildNewStoredCredentials(em, encryptionOutcome);
			sc.setAuthenticatedUser(userSupplier.get());
			return sc;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this)
				.append(PARAM_SERVER_ID, serverId)
				.append(PARAM_USERNAME, username)
				.toString();
		}
	}

	public static class ProjectHandler extends AppLevelHandler {
		private final long projectId;
		private final Supplier<GenericProject> projectSupplier;

		public ProjectHandler(long serverId,
							  long projectId,
							  Supplier<GenericProject> projectSupplier) {
			super(serverId);
			this.projectId = projectId;
			this.projectSupplier = projectSupplier;
		}

		@Override
		public Query getLocateQuery(EntityManager em) {
			return em.createNamedQuery(FIND_PROJECT_CREDENTIALS)
				.setParameter(PARAM_SERVER_ID, serverId)
				.setParameter(PARAM_PROJECT_ID, projectId);
		}

		@Override
		public StoredCredentials buildNewStoredCredentials(EntityManager em,
														   Crypto.EncryptionOutcome encryptionOutcome) {
			final StoredCredentials sc = super.buildNewStoredCredentials(em, encryptionOutcome);
			sc.setProject(projectSupplier.get());
			return sc;
		}

		@Override
		public String toString() {
			return new ToStringBuilder(this)
				.append(PARAM_SERVER_ID, serverId)
				.append(PARAM_PROJECT_ID, projectId)
				.toString();
		}
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SelectSelectStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.jooq.domain.tables.Requirement;
import org.squashtest.tm.jooq.domain.tables.RequirementVersion;
import org.squashtest.tm.service.internal.display.dto.MilestoneAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneBindingDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneProjectViewDto;
import org.squashtest.tm.service.internal.display.dto.ProjectDto;
import org.squashtest.tm.service.internal.display.dto.ProjectInfoForMilestoneAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.ProjectViewDto;
import org.squashtest.tm.service.internal.repository.MilestoneDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_BINDING_PERIMETER;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.tables.CoreUser.CORE_USER;
import static org.squashtest.tm.jooq.domain.tables.Milestone.MILESTONE;
import static org.squashtest.tm.jooq.domain.tables.MilestoneBinding.MILESTONE_BINDING;
import static org.squashtest.tm.jooq.domain.tables.MilestoneTestCase.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.tables.Requirement.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.tables.TestCase.TEST_CASE;


@Repository
public class MilestoneDisplayDaoImpl implements MilestoneDisplayDao {

	private static final String RANGE = "RANGE";
	private static final String OWNER_FIRST_NAME = "OWNER_FIRST_NAME";
	private static final String OWNER_LAST_NAME = "OWNER_LAST_NAME";
	private static final String OWNER_LOGIN = "OWNER_LOGIN";

	private final DSLContext dsl;
	private final MilestoneDao milestoneDao;


	public MilestoneDisplayDaoImpl(DSLContext dsl, MilestoneDao milestoneDao) {
		this.dsl = dsl;
		this.milestoneDao = milestoneDao;
	}

	@Override
	public List<MilestoneDto> getMilestonesByTestCaseId(Long id) {
		return selectMilestoneDto()
			.from(TEST_CASE)
			.innerJoin(MILESTONE_TEST_CASE).on(MILESTONE_TEST_CASE.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
			.innerJoin(MILESTONE).on(MILESTONE_TEST_CASE.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
			.leftJoin(CORE_USER).on(CORE_USER.PARTY_ID.eq(MILESTONE.USER_ID))
			.where(TEST_CASE.TCLN_ID.eq(id))
			.fetch().into(MilestoneDto.class);
	}

	@Override
	public List<MilestoneDto> findAll() {
		return selectMilestoneDto()
			.from(Tables.MILESTONE)
			.leftJoin(CORE_USER).on(CORE_USER.PARTY_ID.eq(MILESTONE.USER_ID))
			.fetch().into(MilestoneDto.class);
	}

	@Override
	public List<MilestoneDto> findByIds(Set<Long> milestoneIds) {
		return selectMilestoneDto()
			.from(Tables.MILESTONE)
			.leftJoin(CORE_USER).on(CORE_USER.PARTY_ID.eq(MILESTONE.USER_ID))
			.where(MILESTONE.MILESTONE_ID.in(milestoneIds))
			.fetch().into(MilestoneDto.class);
	}

	@Override
	public void appendMilestoneBinding(List<ProjectDto> projects) {
		Map<Long, ProjectDto> projectsMap = projects.stream().collect(Collectors.toMap(ProjectDto::getId, Function.identity()));
		Set<Long> projectIds = projects.stream().map(ProjectDto::getId).collect(Collectors.toSet());

		dsl.select(MILESTONE_BINDING.MILESTONE_BINDING_ID, MILESTONE_BINDING.MILESTONE_ID, MILESTONE_BINDING.PROJECT_ID)
			.from(MILESTONE_BINDING)
			.where(MILESTONE_BINDING.PROJECT_ID.in(projectIds))
			.fetch()
			.forEach(record -> {
				ProjectDto project = projectsMap.get(record.get(MILESTONE_BINDING.PROJECT_ID));
				MilestoneBindingDto binding = new MilestoneBindingDto();
				binding.setId(record.get(MILESTONE_BINDING.MILESTONE_BINDING_ID));
				binding.setMilestoneId(record.get(MILESTONE_BINDING.MILESTONE_ID));
				binding.setProjectId(record.get(MILESTONE_BINDING.PROJECT_ID));
				project.getMilestoneBindings().add(binding);
			});
	}

	@Override
	public void appendBoundMilestoneInformation(List<ProjectViewDto> projects) {
		Map<Long, ProjectViewDto> projectsMap = projects.stream().collect(Collectors.toMap(ProjectViewDto::getId, Function.identity()));
		Set<Long> projectIds = projects.stream().map(ProjectDto::getId).collect(Collectors.toSet());

		dsl.select(
				MILESTONE.MILESTONE_ID,
				MILESTONE.LABEL,
				MILESTONE.STATUS,
				MILESTONE.END_DATE,
				MILESTONE.M_RANGE,
				CORE_USER.FIRST_NAME,
				CORE_USER.LAST_NAME,
				CORE_USER.LOGIN,
				MILESTONE.DESCRIPTION,
				MILESTONE_BINDING.PROJECT_ID,
				MILESTONE.LAST_MODIFIED_BY, MILESTONE.LAST_MODIFIED_ON,
				MILESTONE.CREATED_BY, MILESTONE.CREATED_ON)
			.from(MILESTONE)
			.leftJoin(MILESTONE_BINDING)
			.on(MILESTONE_BINDING.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
			.leftJoin(CORE_USER)
			.on(CORE_USER.PARTY_ID.eq(MILESTONE.USER_ID))
			.where(MILESTONE_BINDING.PROJECT_ID.in(projectIds))
			.fetch()
			.forEach(record -> {

				Long projectId = record.get(MILESTONE_BINDING.PROJECT_ID);
				Long milestoneId = record.get(MILESTONE.MILESTONE_ID);

				ProjectViewDto project = projectsMap.get(projectId);

				MilestoneDto milestone = new MilestoneDto();
				milestone.setId(milestoneId);
				milestone.setLabel(record.get(MILESTONE.LABEL));
				milestone.setStatus(record.get(MILESTONE.STATUS));
				milestone.setEndDate(record.get(MILESTONE.END_DATE));
				milestone.setRange(record.get(MILESTONE.M_RANGE));
				milestone.setOwnerFirstName(record.get(CORE_USER.FIRST_NAME));
				milestone.setOwnerLastName(record.get(CORE_USER.LAST_NAME));
				milestone.setOwnerLogin(record.get(CORE_USER.LOGIN));
				String sanitizedDescription = HTMLCleanupUtils.htmlToTrimmedText(record.get(MILESTONE.DESCRIPTION));
				milestone.setDescription(sanitizedDescription);
				milestone.setCreatedBy(record.get(MILESTONE.CREATED_BY));
				milestone.setCreatedOn(record.get(MILESTONE.CREATED_ON));
				milestone.setLastModifiedBy(record.get(MILESTONE.LAST_MODIFIED_BY));
				milestone.setLastModifiedOn(record.get(MILESTONE.LAST_MODIFIED_ON));

				MilestoneProjectViewDto milestoneProjectViewInformation = new MilestoneProjectViewDto();
				milestoneProjectViewInformation.setMilestone(milestone);
				milestoneProjectViewInformation.setMilestoneBoundToOneObjectOfProject(milestoneDao.isMilestoneBoundToOneObjectOfProject(milestoneId, projectId));

				project.getBoundMilestonesInformation().add(milestoneProjectViewInformation);

			});

	}

	@Override
	public void appendBoundProjectsToMilestoneInformation(MilestoneAdminViewDto milestone) {
		Long milestoneId = milestone.getId();

		if (milestone.getRange().equals("GLOBAL")) {
			Result<?> boundProjects = dsl.selectFrom(getProjectsBoundToMilestone(milestoneId)).fetch();
			boundProjects.forEach(record -> {
				ProjectInfoForMilestoneAdminViewDto projectInfo = createProjectInfo(milestoneId, record, true);
				milestone.getBoundProjectsInformation().add(projectInfo);
			});
		} else {
			Result<?> projectsInPerimeter = dsl.selectFrom(getProjectsInMilestonePerimeter(milestoneId)).fetch();
			projectsInPerimeter.forEach(record -> {
				Long projectId = record.get(PROJECT.PROJECT_ID);
				boolean projectIsBoundToMilestone = checkIfProjectInPerimeterIsBoundToMilestone(milestoneId, projectId);
				ProjectInfoForMilestoneAdminViewDto projectInfo = createProjectInfo(milestoneId, record, projectIsBoundToMilestone);
				milestone.getBoundProjectsInformation().add(projectInfo);

			});
		}
	}

	private ProjectInfoForMilestoneAdminViewDto createProjectInfo(Long milestoneId, Record record, boolean boundToMilestone) {
		Long projectId = record.get(PROJECT.PROJECT_ID);

		ProjectInfoForMilestoneAdminViewDto projectInfo = new ProjectInfoForMilestoneAdminViewDto();
		projectInfo.setProjectId(projectId);
		projectInfo.setProjectName(record.get(PROJECT.NAME));
		projectInfo.setTemplate(record.get("TEMPLATE", Boolean.class));
		projectInfo.setBoundToMilestone(boundToMilestone);
		projectInfo.setMilestoneBoundToOneObjectOfProject(milestoneDao.isMilestoneBoundToOneObjectOfProject(milestoneId, projectId));
		return projectInfo;
	}

	private Table<?> getProjectsBoundToMilestone(long milestoneId) {
		return dsl.select(
				PROJECT.PROJECT_ID,
				PROJECT.NAME,
				DSL.field(PROJECT.PROJECT_TYPE.eq("T")).as("TEMPLATE"))
			.from(PROJECT)
			.leftJoin(MILESTONE_BINDING)
			.on(MILESTONE_BINDING.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.where(MILESTONE_BINDING.MILESTONE_ID.eq(milestoneId))
			.asTable();
	}

	private Table<?> getProjectsInMilestonePerimeter(long milestoneId) {
		return dsl.select(
				PROJECT.PROJECT_ID,
				PROJECT.NAME,
				DSL.field(PROJECT.PROJECT_TYPE.eq("T")).as("TEMPLATE"))
			.from(PROJECT)
			.leftJoin(MILESTONE_BINDING_PERIMETER)
			.on(MILESTONE_BINDING_PERIMETER.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.where(MILESTONE_BINDING_PERIMETER.MILESTONE_ID.eq(milestoneId))
			.asTable();
	}

	private boolean checkIfProjectInPerimeterIsBoundToMilestone(long milestoneId, long projectId) {
		Result<?> boundProjects = dsl.selectFrom(getProjectsBoundToMilestone(milestoneId)).fetch();

		return boundProjects.stream()
			.anyMatch(boundProject -> boundProject.get(PROJECT.PROJECT_ID).equals(projectId));
	}

	@Override
	public Multimap<Long, Long> findMilestonesByTestCaseId(Set<Long> testCaseIds) {
		Multimap<Long, Long> milestoneIdsByTestCaseIds = ArrayListMultimap.create();
		this.dsl
			.select(MILESTONE_TEST_CASE.MILESTONE_ID, MILESTONE_TEST_CASE.TEST_CASE_ID)
			.from(MILESTONE_TEST_CASE)
			.where(MILESTONE_TEST_CASE.TEST_CASE_ID.in(testCaseIds))
			.fetchInto(record -> milestoneIdsByTestCaseIds.put(record.get(MILESTONE_TEST_CASE.TEST_CASE_ID), record.get(MILESTONE_TEST_CASE.MILESTONE_ID)));
		return milestoneIdsByTestCaseIds;
	}

	@Override
	public Multimap<Long, MilestoneDto> findMilestonesByCampaignId(Set<Long> campaignIds) {
		Multimap<Long, MilestoneDto> milestoneIdsByCampaignId = ArrayListMultimap.create();
		this.dsl
			.select(MILESTONE_CAMPAIGN.MILESTONE_ID, MILESTONE_CAMPAIGN.CAMPAIGN_ID, MILESTONE.STATUS)
			.from(MILESTONE_CAMPAIGN)
			.innerJoin(MILESTONE).on(MILESTONE_CAMPAIGN.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
			.where(MILESTONE_CAMPAIGN.CAMPAIGN_ID.in(campaignIds))
			.fetchInto(record -> milestoneIdsByCampaignId.put(record.get(MILESTONE_CAMPAIGN.CAMPAIGN_ID), createMilestoneDto(record)));
		return milestoneIdsByCampaignId;
	}

	private MilestoneDto createMilestoneDto(Record record) {
		MilestoneDto dto = new MilestoneDto();
		dto.setStatus(record.get(MILESTONE.STATUS));
		dto.setId(record.get(MILESTONE_CAMPAIGN.MILESTONE_ID));
		return dto;
	}

	@Override
	public RequirementVersionMilestones findMilestonesByRequirementVersionId(Set<Long> requirementVersionIds) {
		RequirementVersionMilestones requirementVersionMilestones = new RequirementVersionMilestones();
		appendDirectlyBoundMilestones(requirementVersionIds, requirementVersionMilestones);
		appendIndirectlyBoundMilestone(requirementVersionIds, requirementVersionMilestones);
		return requirementVersionMilestones;
	}

	private void appendIndirectlyBoundMilestone(Set<Long> requirementVersionIds, RequirementVersionMilestones requirementVersionMilestones) {
		Requirement ancestor = REQUIREMENT.as("ANCESTORS");
		RequirementVersion ancestorVersion = REQUIREMENT_VERSION.as("ANCESTORS_VERSION");
		Requirement descendants = REQUIREMENT.as("DESCENDANTS");
		RequirementVersion descendantVersion = REQUIREMENT_VERSION.as("DESCENDANTS_VERSION");

		this.dsl
			.select(ancestorVersion.RES_ID, MILESTONE_REQ_VERSION.MILESTONE_ID)
			.from(ancestorVersion)
			.innerJoin(ancestor).on(ancestor.RLN_ID.eq(ancestorVersion.REQUIREMENT_ID))
			.innerJoin(RLN_RELATIONSHIP_CLOSURE).on(ancestor.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID))
			.innerJoin(descendants).on(descendants.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
			.innerJoin(descendantVersion).on(descendants.RLN_ID.eq(descendantVersion.REQUIREMENT_ID))
			.innerJoin(MILESTONE_REQ_VERSION).on(MILESTONE_REQ_VERSION.REQ_VERSION_ID.eq(descendantVersion.RES_ID))
			.where(ancestorVersion.RES_ID.in(requirementVersionIds).and(RLN_RELATIONSHIP_CLOSURE.DEPTH.gt((short) 0)))
			.forEach(record -> requirementVersionMilestones.addIndirectlyBoundMilestone(record.get(ancestorVersion.RES_ID), record.get(MILESTONE_REQ_VERSION.MILESTONE_ID)));
	}

	private void appendDirectlyBoundMilestones(Set<Long> requirementVersionIds, RequirementVersionMilestones requirementVersionMilestones) {
		this.dsl
			.select(MILESTONE_REQ_VERSION.MILESTONE_ID, MILESTONE_REQ_VERSION.REQ_VERSION_ID)
			.from(MILESTONE_REQ_VERSION)
			.where(MILESTONE_REQ_VERSION.REQ_VERSION_ID.in(requirementVersionIds))
			.forEach(record -> requirementVersionMilestones.addDirectlyBoundMilestone(record.get(MILESTONE_REQ_VERSION.REQ_VERSION_ID), record.get(MILESTONE_REQ_VERSION.MILESTONE_ID)));
	}

	@Override
	public Multimap<Long, Long> findMilestonesByProjectId(Set<Long> projectIds) {
		Multimap<Long, Long> map = ArrayListMultimap.create();
		this.dsl
			.select(MILESTONE_BINDING.MILESTONE_ID, MILESTONE_BINDING.PROJECT_ID)
			.from(MILESTONE_BINDING)
			.where(MILESTONE_BINDING.PROJECT_ID.in(projectIds))
			.fetchInto(record -> map.put(record.get(MILESTONE_BINDING.PROJECT_ID), record.get(MILESTONE_BINDING.MILESTONE_ID)));
		return map;
	}

	@Override
	public List<MilestoneDto> getMilestonesByRequirementVersionId(Long id) {
		return selectMilestoneDto()
			.from(REQUIREMENT_VERSION)
			.innerJoin(MILESTONE_REQ_VERSION).on(MILESTONE_REQ_VERSION.REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
			.innerJoin(MILESTONE).on(MILESTONE_REQ_VERSION.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
			.leftJoin(CORE_USER).on(CORE_USER.PARTY_ID.eq(MILESTONE.USER_ID))
			.where(REQUIREMENT_VERSION.RES_ID.eq(id))
			.fetch().into(MilestoneDto.class);
	}

	@Override
	public List<MilestoneDto> getMilestonesDtoAssociableToRequirementVersion(long versionId) {
		List<MilestoneStatus> milestoneStatuses = MilestoneStatus.getAllStatusAllowingObjectBind();
		List<String> status = milestoneStatuses.stream().map(Enum::name).collect(Collectors.toList());

		return selectMilestoneDto()
			.from(REQUIREMENT_VERSION)
			.innerJoin(Tables.REQUIREMENT).on(Tables.REQUIREMENT.RLN_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
			.innerJoin(REQUIREMENT_LIBRARY_NODE).on(Tables.REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
			.innerJoin(PROJECT).on(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.innerJoin(Tables.MILESTONE_BINDING).on(Tables.MILESTONE_BINDING.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.innerJoin(Tables.MILESTONE).on(Tables.MILESTONE_BINDING.MILESTONE_ID.eq(Tables.MILESTONE.MILESTONE_ID))
			.innerJoin(Tables.CORE_USER).on(CORE_USER.PARTY_ID.eq(MILESTONE.USER_ID))
			.where(REQUIREMENT_VERSION.RES_ID.eq(versionId)
				.and(Tables.MILESTONE.STATUS.in(status)).and(Tables.MILESTONE.MILESTONE_ID.notIn(
					dsl.select(MILESTONE_REQ_VERSION.MILESTONE_ID)
						.from(REQUIREMENT_VERSION.as("RV"))
						.innerJoin(Tables.REQUIREMENT).on(Tables.REQUIREMENT.RLN_ID.eq(REQUIREMENT_VERSION.as("RV").REQUIREMENT_ID))
						.innerJoin(REQUIREMENT_VERSION.as("RV2")).on(Tables.REQUIREMENT.RLN_ID.eq(REQUIREMENT_VERSION.as("RV2").REQUIREMENT_ID))
						.innerJoin(MILESTONE_REQ_VERSION).on(REQUIREMENT_VERSION.as("RV2").RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
						.where(REQUIREMENT_VERSION.as("RV").RES_ID.eq(versionId))
				)))
			.fetchInto(MilestoneDto.class);
	}

	@Override
	public List<MilestoneDto> getMilestonesByCampaignId(Long campaignId) {
		return selectMilestoneDto()
			.from(CAMPAIGN)
			.innerJoin(MILESTONE_CAMPAIGN).on(MILESTONE_CAMPAIGN.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
			.innerJoin(MILESTONE).on(MILESTONE_CAMPAIGN.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
			.leftJoin(CORE_USER).on(CORE_USER.PARTY_ID.eq(MILESTONE.USER_ID))
			.where(CAMPAIGN.CLN_ID.eq(campaignId))
			.fetch().into(MilestoneDto.class);
	}

	@Override
	public List<MilestoneDto> getMilestonesByIterationId(Long iterationId) {
		return selectMilestoneDto()
			.from(ITERATION)
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(MILESTONE_CAMPAIGN).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(MILESTONE_CAMPAIGN.CAMPAIGN_ID))
			.innerJoin(MILESTONE).on(MILESTONE_CAMPAIGN.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
			.leftJoin(CORE_USER).on(CORE_USER.PARTY_ID.eq(MILESTONE.USER_ID))
			.where(ITERATION.ITERATION_ID.eq(iterationId))
			.fetch().into(MilestoneDto.class);
	}

	@Override
	public List<MilestoneDto> getMilestonesBySuiteId(Long suiteId) {
		return selectMilestoneDto()
			.from(TEST_SUITE)
			.innerJoin(ITERATION_TEST_SUITE).on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
			.innerJoin(ITERATION).on(ITERATION_TEST_SUITE.ITERATION_ID.eq(ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(MILESTONE_CAMPAIGN).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(MILESTONE_CAMPAIGN.CAMPAIGN_ID))
			.innerJoin(MILESTONE).on(MILESTONE_CAMPAIGN.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
			.leftJoin(CORE_USER).on(CORE_USER.PARTY_ID.eq(MILESTONE.USER_ID))
			.where(TEST_SUITE.ID.eq(suiteId))
			.fetch().into(MilestoneDto.class);
	}

	@Override
	public List<MilestoneDto> getMilestonesByExecutionId(Long executionId) {
		return selectMilestoneDto()
			.from(EXECUTION)
			.innerJoin(ITEM_TEST_PLAN_EXECUTION).on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
			.innerJoin(ITEM_TEST_PLAN_LIST).on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
			.innerJoin(ITERATION).on(ITERATION.ITERATION_ID.eq(ITEM_TEST_PLAN_LIST.ITERATION_ID))
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(MILESTONE_CAMPAIGN).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(MILESTONE_CAMPAIGN.CAMPAIGN_ID))
			.innerJoin(MILESTONE).on(MILESTONE_CAMPAIGN.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
			.leftJoin(CORE_USER).on(CORE_USER.PARTY_ID.eq(MILESTONE.USER_ID))
			.where(EXECUTION.EXECUTION_ID.eq(executionId))
			.fetch().into(MilestoneDto.class);
	}

	@Override
	public Map<Long, List<MilestoneDto>> getMilestonesByBoundEntity(List<Long> ids, NodeType handledEntityType) {
		switch (handledEntityType) {
			case CAMPAIGN:
				return ids.stream().collect(Collectors.toMap(id -> id, this::getMilestonesByCampaignId));
			case ITERATION:
				return ids.stream().collect(Collectors.toMap(id -> id, this::getMilestonesByIterationId));
			case TEST_SUITE:
				return ids.stream().collect(Collectors.toMap(id -> id, this::getMilestonesBySuiteId));
			case TEST_CASE:
				return ids.stream().collect(Collectors.toMap(id -> id, this::getMilestonesByTestCaseId));
			// TODO PCK : check if this is correct (req vs req version)
			case REQUIREMENT:
			case HIGH_LEVEL_REQUIREMENT:
				return ids.stream().collect(Collectors.toMap(id -> id, this::getMilestonesDtoForCurrentVersionByRequirementId));
			default:
				return Collections.emptyMap();
		}
	}

	private List<MilestoneDto> getMilestonesDtoForCurrentVersionByRequirementId(Long id) {
		return selectMilestoneDto()
			.from(REQUIREMENT)
			.innerJoin(REQUIREMENT_VERSION).on(REQUIREMENT.CURRENT_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
			.innerJoin(MILESTONE_REQ_VERSION).on(MILESTONE_REQ_VERSION.REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
			.innerJoin(MILESTONE).on(MILESTONE_REQ_VERSION.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
			.leftJoin(CORE_USER).on(CORE_USER.PARTY_ID.eq(MILESTONE.USER_ID))
			.where(REQUIREMENT.RLN_ID.eq(id))
			.fetch().into(MilestoneDto.class);
	}

	private SelectSelectStep<?> selectMilestoneDto() {
		return dsl.select(
			MILESTONE.MILESTONE_ID.as(RequestAliasesConstants.ID),
			MILESTONE.LABEL,
			MILESTONE.DESCRIPTION,
			MILESTONE.STATUS,
			MILESTONE.END_DATE,
			MILESTONE.M_RANGE.as(RANGE),
			MILESTONE.LAST_MODIFIED_BY,
			MILESTONE.LAST_MODIFIED_ON,
			MILESTONE.CREATED_BY,
			MILESTONE.CREATED_ON,
			CORE_USER.FIRST_NAME.as(OWNER_FIRST_NAME),
			CORE_USER.LAST_NAME.as(OWNER_LAST_NAME),
			CORE_USER.LOGIN.as(OWNER_LOGIN));
	}

	public class RequirementVersionMilestones {
		// Milestones held directly by requirement versions
		private ArrayListMultimap<Long, Long> directlyBoundMilestone = ArrayListMultimap.create();

		// Milestones held indirectly by requirement versions, because a children hold a milestone
		private ArrayListMultimap<Long, Long> indirectlyBoundMilestone = ArrayListMultimap.create();

		public void addDirectlyBoundMilestone(Long requirementVersionId, Long milestoneId) {
			this.directlyBoundMilestone.put(requirementVersionId, milestoneId);
		}

		public void addIndirectlyBoundMilestone(Long ancestorVersionId, Long milestoneId) {
			this.indirectlyBoundMilestone.put(ancestorVersionId, milestoneId);
		}

		public List<Long> getDirectlyBoundMilestone(Long requirementVersionId) {
			return this.directlyBoundMilestone.get(requirementVersionId);
		}

		public List<Long> getIndirectlyBoundMilestone(Long requirementVersionId) {
			return this.indirectlyBoundMilestone.get(requirementVersionId);
		}

		public List<Long> getAllBoundMilestone(Long requirementVersionId) {
			List<Long> allMilestones = new ArrayList<>();
			allMilestones.addAll(getDirectlyBoundMilestone(requirementVersionId));
			allMilestones.addAll(getIndirectlyBoundMilestone(requirementVersionId));
			return allMilestones;
		}
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.testcase.ActionWordFragmentValueDto;
import org.squashtest.tm.service.internal.display.dto.testcase.ActionWordParameterValueDto;
import org.squashtest.tm.service.internal.display.dto.testcase.ActionWordTextValueDto;
import org.squashtest.tm.service.internal.repository.display.ActionWordDisplayDao;

import java.util.List;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_FRAGMENT;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_PARAMETER_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_TEXT;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_STEP;
import static org.squashtest.tm.service.internal.display.dto.testcase.ActionWordFragmentValueDto.PARAMETER;
import static org.squashtest.tm.service.internal.display.dto.testcase.ActionWordFragmentValueDto.TEXT;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.VALUE;

@Repository
public class ActionWordDisplayDaoImpl implements ActionWordDisplayDao {

	private static final String FRAGMENT_TYPE = "FRAGMENT_TYPE";
	private static final String KEYWORD_STEP_ID = "KEYWORD_STEP_ID";

	private DSLContext dsl;

	public ActionWordDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public List<ActionWordFragmentValueDto> findActionWordFragmentValues(List<Long> keywordStepIds) {
		return dsl.select(KEYWORD_TEST_STEP.TEST_STEP_ID.as(KEYWORD_STEP_ID),
				DSL.val(TEXT).as(FRAGMENT_TYPE),
				ACTION_WORD_TEXT.TEXT.as(VALUE),
				ACTION_WORD_FRAGMENT.FRAGMENT_ORDER)
			.from(KEYWORD_TEST_STEP)
			.innerJoin(ACTION_WORD_FRAGMENT).on(ACTION_WORD_FRAGMENT.ACTION_WORD_ID.eq(KEYWORD_TEST_STEP.ACTION_WORD_ID))
			.innerJoin(ACTION_WORD_TEXT).on(ACTION_WORD_FRAGMENT.ACTION_WORD_FRAGMENT_ID.eq(ACTION_WORD_TEXT.ACTION_WORD_FRAGMENT_ID))
			.where(KEYWORD_TEST_STEP.TEST_STEP_ID.in(keywordStepIds))
			.union(
				dsl.select(
						ACTION_WORD_PARAMETER_VALUE.KEYWORD_TEST_STEP_ID.as(KEYWORD_STEP_ID),
						DSL.val(PARAMETER).as(FRAGMENT_TYPE),
						ACTION_WORD_PARAMETER_VALUE.VALUE.as(VALUE),
						ACTION_WORD_FRAGMENT.FRAGMENT_ORDER)
					.from(ACTION_WORD_FRAGMENT)
					.innerJoin(ACTION_WORD_PARAMETER_VALUE).on(ACTION_WORD_FRAGMENT.ACTION_WORD_FRAGMENT_ID.eq(ACTION_WORD_PARAMETER_VALUE.ACTION_WORD_FRAGMENT_ID))
					.where(ACTION_WORD_PARAMETER_VALUE.KEYWORD_TEST_STEP_ID.in(keywordStepIds)))
			.orderBy(
				DSL.field(KEYWORD_STEP_ID),
				DSL.field("FRAGMENT_ORDER"))
			.fetch()
			.stream()
			.map(record -> {
				if (TEXT.equals(record.get(FRAGMENT_TYPE))) {
					return new ActionWordTextValueDto(
						(Long) record.get(KEYWORD_STEP_ID),
						record.get(VALUE).toString());
				} else {
					return new ActionWordParameterValueDto(
						(Long) record.get(KEYWORD_STEP_ID),
						record.get(VALUE).toString());
				}
			})
			.collect(Collectors.toList());
	}
}

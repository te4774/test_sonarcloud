/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.search;

import java.util.List;

import static java.util.Collections.emptyList;

/**
 * Class representing the result of a search. It's just an id list and a count.
 * Rows to display in grid result must be fetch in a second time, using the ids.
 */
public class ResearchResult {
	private final List<Long> ids;
	private final Long count;

	public ResearchResult(List<Long> ids, Long count) {
		this.ids = ids;
		this.count = count;
	}

	public List<Long> getIds() {
		return ids;
	}

	public Long getCount() {
		return count;
	}

	public static ResearchResult emptyResult() {
		return new ResearchResult(emptyList(), 0L);
	}
}

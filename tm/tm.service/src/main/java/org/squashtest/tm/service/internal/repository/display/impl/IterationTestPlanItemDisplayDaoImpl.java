/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.OrderField;
import org.jooq.SelectConditionStep;
import org.jooq.impl.DSL;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.campaign.IterationTestPlanItemDto;
import org.squashtest.tm.service.internal.repository.display.IterationTestPlanItemDisplayDao;

import java.util.List;

import static org.squashtest.tm.jooq.domain.tables.ItemTestPlanList.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.tables.Iteration.ITERATION;
import static org.squashtest.tm.jooq.domain.tables.IterationTestPlanItem.ITERATION_TEST_PLAN_ITEM;

@Repository
public class IterationTestPlanItemDisplayDaoImpl implements IterationTestPlanItemDisplayDao {

	private final DSLContext dsl;

	public IterationTestPlanItemDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public Page<IterationTestPlanItemDto> findItemByIterationId(Long iterationId, Pageable pageable) {

		SelectConditionStep<?> initialQuery = dsl.select(ITERATION_TEST_PLAN_ITEM.LABEL, ITERATION_TEST_PLAN_ITEM.TCLN_ID, ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ORDER)
			.from(ITERATION)
			.innerJoin(ITEM_TEST_PLAN_LIST).on(ITERATION.ITERATION_ID.eq(ITEM_TEST_PLAN_LIST.ITERATION_ID))
			.innerJoin(ITERATION_TEST_PLAN_ITEM).on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
			.where(ITERATION.ITERATION_ID.eq(iterationId));

		addOrderBy(initialQuery, pageable.getSort());
		addPagination(initialQuery, pageable);

		List<IterationTestPlanItemDto> items = initialQuery.fetchInto(IterationTestPlanItemDto.class);
		long count = countItpiByIteration(iterationId);

		return new PageImpl<>(items, pageable, count);
	}

	private Integer countItpiByIteration(long iterationId) {
		return dsl.selectCount().from(ITERATION).innerJoin(ITEM_TEST_PLAN_LIST).on(ITERATION.ITERATION_ID.eq(ITEM_TEST_PLAN_LIST.ITERATION_ID))
			.innerJoin(ITERATION_TEST_PLAN_ITEM).on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
			.where(ITERATION.ITERATION_ID.eq(iterationId)).fetchOne(DSL.count());
	}

	private void addPagination(SelectConditionStep<?> condition, Pageable pageable) {
		condition.offset(Math.toIntExact(pageable.getOffset())).limit(pageable.getPageSize());
	}

	private void addOrderBy(SelectConditionStep<?> query, Sort sort) {
		sort.stream().forEach(order -> {
			String property = order.getProperty();
			boolean isAsc = order.isAscending();

			Field<?> selectField = query.getSelect().stream().filter(field -> property.equals(field.getName())).findAny().orElse(null);
			if (selectField != null) {
				OrderField orderField = isAsc ? selectField.asc() : selectField.desc();
				query.getQuery().addOrderBy(orderField);
			}
		});
	}
}

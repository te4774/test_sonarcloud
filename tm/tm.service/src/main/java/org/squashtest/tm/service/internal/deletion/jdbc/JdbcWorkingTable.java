/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Select;
import org.jooq.SelectConditionStep;
import org.jooq.Table;
import org.jooq.TableField;
import org.squashtest.tm.service.internal.deletion.jdbc.delegate.DelegateDeleteQuery;

import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static org.squashtest.tm.jooq.domain.tables.WorkDeleteEntities.WORK_DELETE_ENTITIES;

/**
 * This class represent an abstraction of a WORKING_TABLE witch in fact is a subset of the WORK_DELETE_ENTITIES table
 * scoped for one operation and one table.
 */
public class JdbcWorkingTable {

	private final DSLContext dslContext;
	// has to delegate to handle sql disparity on delete statement
	private final DelegateDeleteQuery deleteQuery;

	JdbcWorkingTable(TableField<?, Long> originalField,
					 Supplier<Select<Record3<Long, String, String>>> selector,
					 DSLContext dslContext,
					 String operationId) {
		this.dslContext = requireNonNull(dslContext);
		this.deleteQuery = DelegateDeleteQuery.get(originalField, dslContext, operationId);
		insertSelectionIntoTemporaryTable(selector, dslContext);
	}

	private void insertSelectionIntoTemporaryTable(Supplier<Select<Record3<Long, String, String>>> selector,
												   DSLContext dslContext) {
		dslContext.insertInto(WORK_DELETE_ENTITIES)
			.select(selector.get())
			.execute();
	}

	public SelectConditionStep<Record1<Long>> selectEntityIds() {
		return deleteQuery.selectEntityIds();
	}

	public void delete(TableField<?, Long> linkedColumn) {
		deleteQuery.delete(linkedColumn);
	}

	public void nullify(TableField<?, Long> linkedColumn) {
		Table<?> table = linkedColumn.getTable();
		dslContext.update(table)
			.set(linkedColumn, (Long) null)
			.where(linkedColumn.in(selectEntityIds()))
			.execute();
	}

}

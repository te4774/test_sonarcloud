/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.testingstatus;

import org.squashtest.tm.service.statistics.testingstatus.exception.IllegalStatisticRatesException;

/**
 * Synthetic status used in remote bugtracker reports (xsquash4jira, xsquash4gitlab) to indicate the testing
 * advancement of Squash TM requirements.
 */
public enum RemoteTestingStatus {
	NOT_INITIALIZED,
	CONCEPTION,
	TO_EXECUTE,
	VALIDATION_IN_PROGRESS,
	VALID,
	INVALID;

	public static RemoteTestingStatus findStatus(RemoteRequirementStatistics stats) {
		final double redactionRate = stats.redactionRate.normalizedRate;
		final double verificationRate = stats.verificationRate.normalizedRate;
		final double validationRate = stats.validationRate.normalizedRate;

		if (isZero(redactionRate) && isZero(verificationRate) && isZero(validationRate)) {
			return RemoteTestingStatus.NOT_INITIALIZED;
		} else if (isIntermediateValue(redactionRate) && isZero(verificationRate) && isZero(validationRate)) {
			return RemoteTestingStatus.CONCEPTION;
		} else if (isCompleteValue(redactionRate) && isZero(verificationRate) && isZero(validationRate)) {
			return RemoteTestingStatus.TO_EXECUTE;
		} else if (isIntermediateValue(verificationRate) && isCompleteValue(validationRate)) {
			return RemoteTestingStatus.VALIDATION_IN_PROGRESS;
		} else if (isStartedValue(verificationRate) && isNotCompleteValue(validationRate)) {
			return RemoteTestingStatus.INVALID;
		} else if (isCompleteValue(verificationRate) && isCompleteValue(validationRate)) {
			return RemoteTestingStatus.VALID;
		} else {
			String msg = String.format("Programmatic error. Unable to compute proper testing status. Rates are %f , %f , %f .", redactionRate, verificationRate, validationRate);
			throw new IllegalStatisticRatesException(msg);
		}
	}

	private static boolean isNotCompleteValue(double rate) {
		return rate < 1d;
	}

	private static boolean isCompleteValue(double rate) {
		return rate == 1d;
	}

	private static boolean isZero(double rate) {
		return rate == 0d;
	}

	private static boolean isIntermediateValue(double rate) {
		return rate > 0d && rate < 1d;
	}

	private static boolean isStartedValue(double rate) {
		return rate > 0d;
	}
}

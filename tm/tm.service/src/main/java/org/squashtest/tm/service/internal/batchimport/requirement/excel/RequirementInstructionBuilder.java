/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchimport.requirement.excel;

import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.tm.domain.requirement.RequirementNature;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.service.internal.batchimport.HighLevelRequirementTarget;
import org.squashtest.tm.service.internal.batchimport.RequirementTarget;
import org.squashtest.tm.service.internal.batchimport.RequirementVersionInstruction;
import org.squashtest.tm.service.internal.batchimport.RequirementVersionTarget;
import org.squashtest.tm.service.internal.batchimport.excel.CannotCoerceException;
import org.squashtest.tm.service.internal.batchimport.testcase.excel.InstructionBuilder;
import org.squashtest.tm.service.internal.batchimport.testcase.excel.StdColumnDef;
import org.squashtest.tm.service.internal.batchimport.testcase.excel.WorksheetDef;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.squashtest.tm.domain.requirement.RequirementNature.STANDARD;

public class RequirementInstructionBuilder extends InstructionBuilder<RequirementSheetColumn, RequirementVersionInstruction>{
	private static final Logger LOGGER = LoggerFactory.getLogger(RequirementInstructionBuilder.class);

	public RequirementInstructionBuilder(WorksheetDef<RequirementSheetColumn> worksheetDef) {
		super(worksheetDef);
	}

	@Override
	protected RequirementVersionInstruction createInstruction(Row row) {
		StdColumnDef<RequirementSheetColumn> columnDef = worksheetDef.getColumnDef(RequirementSheetColumn.REQ_NATURE);
		RequirementNature requirementNature = null;
		if (columnDef != null) {
			try {
				requirementNature = getValue(row, columnDef);
			} catch (CannotCoerceException cce) {
				String requirementNatures = Arrays.stream(RequirementNature.values()).map(Enum::toString).collect(Collectors.joining(","));
				LOGGER.debug("The value for REQ_NATURE does not exist for the corresponded enum. Authorized values are : {}. Then, default value STANDARD will be assigned.", requirementNatures, cce);
			}
		}
		if (requirementNature == null) {
			requirementNature = STANDARD;
		}

		if (requirementNature == RequirementNature.HIGH_LEVEL) {
			return new RequirementVersionInstruction(new RequirementVersionTarget(new HighLevelRequirementTarget(), 0), new RequirementVersion());
		} else {
			return new RequirementVersionInstruction(new RequirementVersionTarget(new RequirementTarget(), 0), new RequirementVersion());
		}
	}



}

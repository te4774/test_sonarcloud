/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.execution;

import java.util.List;

public class ModificationDuringExecutionView {
	private List<ExecutionStepActionTestStepPair> executionStepActionTestStepPairs;

	public List<ExecutionStepActionTestStepPair> getExecutionStepActionTestStepPairs() {
		return executionStepActionTestStepPairs;
	}

	public void setExecutionStepActionTestStepPairs(List<ExecutionStepActionTestStepPair> executionStepActionTestStepPairs) {
		this.executionStepActionTestStepPairs = executionStepActionTestStepPairs;
	}

	public static class ExecutionStepActionTestStepPair {
		private Long executionStepId;
		private Long testStepId;

		public Long getExecutionStepId() {
			return executionStepId;
		}

		public void setExecutionStepId(Long executionStepId) {
			this.executionStepId = executionStepId;
		}

		public Long getTestStepId() {
			return testStepId;
		}

		public void setTestStepId(Long testStepId) {
			this.testStepId = testStepId;
		}
	}
}

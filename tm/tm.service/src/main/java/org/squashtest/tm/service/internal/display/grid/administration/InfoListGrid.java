/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;

import org.jooq.Field;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.tables.InfoList.INFO_LIST;
import static org.squashtest.tm.jooq.domain.tables.InfoListItem.INFO_LIST_ITEM;
import static org.squashtest.tm.jooq.domain.tables.Project.PROJECT;

public class InfoListGrid extends AbstractGrid {

	private static final String SYSTEM_USER = "system";
	private static final String PROJECT_COUNT = "PROJECT_COUNT";
	private static final String INFO_LIST_ID = "INFO_LIST_ID";
	private static final String LABEL = "LABEL";
	private static final String DESCRIPTION = "DESCRIPTION";
	private static final String CODE = "CODE";
	private static final String DEFAULT_VALUE = "DEFAULT_VALUE";
	private static final String CREATED_ON = "CREATED_ON";
	private static final String CREATED_BY = "CREATED_BY";

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(DSL.field(INFO_LIST_ID)),
			new GridColumn(DSL.field(LABEL)),
			new GridColumn(DSL.field(DESCRIPTION)),
			new GridColumn(DSL.field(CODE)),
			new GridColumn(DSL.field(DEFAULT_VALUE)),
			new GridColumn(DSL.isnull(DSL.field(PROJECT_COUNT), 0).as(PROJECT_COUNT)),
			new GridColumn(DSL.field(CREATED_ON)),
			new GridColumn(DSL.field(CREATED_BY))
		);
	}

	@Override
	protected Table<?> getTable() {
		return DSL.select(
				INFO_LIST.INFO_LIST_ID.as(INFO_LIST_ID),
				INFO_LIST.LABEL.as(LABEL),
				INFO_LIST.CREATED_ON.as(CREATED_ON),
				INFO_LIST.CREATED_BY.as(CREATED_BY),
				INFO_LIST.DESCRIPTION.as(DESCRIPTION),
				INFO_LIST.CODE.as(CODE),
				INFO_LIST_ITEM.LABEL.as(DEFAULT_VALUE),
				DSL.countDistinct(PROJECT.PROJECT_ID).as(PROJECT_COUNT))
			.from(INFO_LIST)
			.leftJoin(INFO_LIST_ITEM).on(INFO_LIST_ITEM.LIST_ID.eq(INFO_LIST.INFO_LIST_ID))
			.leftJoin(PROJECT)
				.on(PROJECT.REQ_CATEGORIES_LIST.eq(INFO_LIST.INFO_LIST_ID)
				.or(PROJECT.TC_NATURES_LIST.eq(INFO_LIST.INFO_LIST_ID))
				.or(PROJECT.TC_TYPES_LIST.eq(INFO_LIST.INFO_LIST_ID)))
			.where(INFO_LIST.CREATED_BY.notEqual(SYSTEM_USER))
			.and(INFO_LIST_ITEM.IS_DEFAULT.eq(true))
			.groupBy(INFO_LIST.INFO_LIST_ID, INFO_LIST_ITEM.ITEM_ID)
			.asTable();
	}

	@Override
	protected Field<?> getIdentifier() {
		return DSL.field(INFO_LIST_ID);
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return null;
	}

	@Override
	protected SortField<?> getDefaultOrder() {
		return DSL.upper(DSL.field(LABEL, String.class)).asc();
	}
}

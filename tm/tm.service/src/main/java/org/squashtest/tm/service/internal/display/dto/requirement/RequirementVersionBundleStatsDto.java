/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.requirement;

public class RequirementVersionBundleStatsDto {
	private RequirementVersionStatsDto currentVersion;
	private RequirementVersionStatsDto children;
	private RequirementVersionStatsDto total;
	private Integer nonObsoleteDescendantsCount;
	private Integer coveredDescendantsCount;
	private Boolean haveChildren;

	public RequirementVersionStatsDto getCurrentVersion() {
		return currentVersion;
	}

	public void setCurrentVersion(RequirementVersionStatsDto currentVersion) {
		this.currentVersion = currentVersion;
	}

	public RequirementVersionStatsDto getChildren() {
		return children;
	}

	public void setChildren(RequirementVersionStatsDto children) {
		this.children = children;
	}

	public RequirementVersionStatsDto getTotal() {
		return total;
	}

	public void setTotal(RequirementVersionStatsDto total) {
		this.total = total;
	}

	public Boolean getHaveChildren() {
		return haveChildren;
	}

	public void setHaveChildren(Boolean haveChildren) {
		this.haveChildren = haveChildren;
	}

	public Integer getNonObsoleteDescendantsCount() {
		return nonObsoleteDescendantsCount;
	}

	public void setNonObsoleteDescendantsCount(Integer nonObsoleteDescendantsCount) {
		this.nonObsoleteDescendantsCount = nonObsoleteDescendantsCount;
	}

	public Integer getCoveredDescendantsCount() {
		return coveredDescendantsCount;
	}

	public void setCoveredDescendantsCount(Integer coveredDescendantsCount) {
		this.coveredDescendantsCount = coveredDescendantsCount;
	}
}



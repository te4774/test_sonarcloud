/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors;

import com.google.common.base.Strings;
import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.internal.repository.display.impl.MilestoneDisplayDaoImpl;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.countDistinct;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.length;
import static org.jooq.impl.DSL.nvl;
import static org.squashtest.tm.jooq.domain.Tables.INFO_LIST_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_SYNC_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP;

@Component
public class RequirementCollector extends AbstractTreeNodeCollector implements TreeNodeCollector {

	private final String categoryLabelAlias = "REQ_CATEGORY_LABEL";
	private final String categoryIconAlias = "REQ_CATEGORY_ICON";
	private final String categoryTypeAlias = "REQ_CATEGORY_TYPE";
	private final String hasDescriptionAlias = "HAS_DESCRIPTION";
	private final String isSynchronizedAlias = "IS_SYNCHRONIZED";
	private final RequirementVersionDisplayDao requirementVersionDisplayDao;


	public RequirementCollector(DSLContext dsl,
								CustomFieldValueDisplayDao customFieldValueDisplayDao,
								ActiveMilestoneHolder activeMilestoneHolder,
								MilestoneDisplayDao milestoneDisplayDao, RequirementVersionDisplayDao requirementVersionDisplayDao) {
		super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
		this.requirementVersionDisplayDao = requirementVersionDisplayDao;
	}

	@Override
	protected Map<Long, DataRow> doCollect(List<Long> requirementIds) {
		Map<Long, Long> requirementVersionIdsByRequirement = requirementVersionDisplayDao.findCurrentRequirementVersions(requirementIds);
		Map<Long, DataRow> requirementRows = collectRequirementRows(requirementVersionIdsByRequirement);
		appendRequirementVersionsMilestones(requirementVersionIdsByRequirement, requirementRows);
		return requirementRows;
	}

	private Map<Long, DataRow> collectRequirementRows(Map<Long, Long> requirementVersionIdsByRequirement) {
		return dsl.select(
			// @formatter:off
			REQUIREMENT_LIBRARY_NODE.RLN_ID, REQUIREMENT_LIBRARY_NODE.PROJECT_ID.as(PROJECT_ID_ALIAS)
			, RESOURCE.NAME.as(NAME_ALIAS), field(nvl(length(RESOURCE.DESCRIPTION), 0).gt(0)).as(hasDescriptionAlias)
			, REQUIREMENT_VERSION.REFERENCE, REQUIREMENT_VERSION.CRITICALITY, REQUIREMENT_VERSION.REQUIREMENT_STATUS
			, INFO_LIST_ITEM.ICON_NAME.as(categoryIconAlias), INFO_LIST_ITEM.LABEL.as(categoryLabelAlias), INFO_LIST_ITEM.ITEM_TYPE.as(categoryTypeAlias)
			, count(RLN_RELATIONSHIP.ANCESTOR_ID).as(CHILD_COUNT_ALIAS)
			, countDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID).as("COVERAGE_COUNT")
			, field(REQUIREMENT_SYNC_EXTENDER.REQ_SYNC_ID.isNotNull()).as(isSynchronizedAlias)
			, REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_PERIMETER_STATUS
			, REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID
		).from(REQUIREMENT_LIBRARY_NODE)
			.leftJoin(RLN_RELATIONSHIP).on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(RLN_RELATIONSHIP.ANCESTOR_ID))
			.innerJoin(REQUIREMENT).on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT.RLN_ID))
			.innerJoin(REQUIREMENT_VERSION).on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
			.innerJoin(RESOURCE).on(REQUIREMENT_VERSION.RES_ID.eq(RESOURCE.RES_ID))
			.innerJoin(INFO_LIST_ITEM).on(REQUIREMENT_VERSION.CATEGORY.eq(INFO_LIST_ITEM.ITEM_ID))
			.leftJoin(REQUIREMENT_VERSION_COVERAGE).on(REQUIREMENT_VERSION.RES_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
			.leftJoin(REQUIREMENT_SYNC_EXTENDER).on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID))
			.where(REQUIREMENT_VERSION.RES_ID.in(requirementVersionIdsByRequirement.values()))
			.groupBy(
				REQUIREMENT_LIBRARY_NODE.RLN_ID,
				RESOURCE.RES_ID,
				REQUIREMENT_LIBRARY_NODE.PROJECT_ID,
				RLN_RELATIONSHIP.ANCESTOR_ID,
				REQUIREMENT_VERSION.RES_ID,
				INFO_LIST_ITEM.ITEM_ID,
				REQUIREMENT_SYNC_EXTENDER.REQ_SYNC_ID)
			// @formatter:on
			.fetch().stream()
			.collect(Collectors.toMap(
				tuple -> tuple.get(REQUIREMENT_LIBRARY_NODE.RLN_ID),
				tuple -> {
					DataRow dataRow = new DataRow();
					dataRow.setId(new NodeReference(NodeType.REQUIREMENT, tuple.get(REQUIREMENT_LIBRARY_NODE.RLN_ID)).toNodeId());
					dataRow.setProjectId(tuple.get(PROJECT_ID_ALIAS, Long.class));
					dataRow.setState(tuple.get(CHILD_COUNT_ALIAS, Integer.class) > 0 ? DataRow.State.closed : DataRow.State.leaf);
					dataRow.setData(tuple.intoMap());

					if (!Strings.isNullOrEmpty(tuple.get(REQUIREMENT_VERSION.REFERENCE, String.class))) {
						dataRow.getData().replace(NAME_ALIAS, tuple.get(REQUIREMENT_VERSION.REFERENCE) + " - " + tuple.get(RESOURCE.NAME));
					}

					return dataRow;
				}
			));
	}

	private void appendRequirementVersionsMilestones(Map<Long, Long> requirementVersionIdsByRequirement, Map<Long, DataRow> requirementRows) {
		Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();
		if(activeMilestone.isPresent()){
			MilestoneDisplayDaoImpl.RequirementVersionMilestones milestonesByRequirementVersionId = this.milestoneDisplayDao.findMilestonesByRequirementVersionId(new HashSet<>(requirementVersionIdsByRequirement.values()));
			requirementRows.forEach((requirementId, dataRow) -> {
				List<Long> directlyBoundMilestone = milestonesByRequirementVersionId.getDirectlyBoundMilestone(requirementVersionIdsByRequirement.get(requirementId));
				dataRow.addData(MILESTONES_ALIAS, directlyBoundMilestone);
				boolean directlyBound = directlyBoundMilestone.contains(activeMilestone.get().getId());
				dataRow.addData(DIRECT_BIND_MILESTONES_ALIAS, directlyBound);
				List<Long> indirectlyBoundMilestone = milestonesByRequirementVersionId.getIndirectlyBoundMilestone(requirementVersionIdsByRequirement.get(requirementId));
				boolean indirectlyBound = !directlyBound && indirectlyBoundMilestone.contains(activeMilestone.get().getId());
				dataRow.addData(INDIRECT_BIND_MILESTONES_ALIAS, indirectlyBound);
				dataRow.addData(ALL_MILESTONES_ALIAS, milestonesByRequirementVersionId.getAllBoundMilestone(requirementVersionIdsByRequirement.get(requirementId)));
			});
		}
	}


	@Override
	public NodeType getHandledEntityType() {
		return NodeType.REQUIREMENT;
	}
}

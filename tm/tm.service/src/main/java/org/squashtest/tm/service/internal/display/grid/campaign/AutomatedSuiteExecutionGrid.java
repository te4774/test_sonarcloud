/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Table;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.tables.AutomatedExecutionExtender.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.tables.AutomatedSuite.AUTOMATED_SUITE;

public class AutomatedSuiteExecutionGrid extends AbstractGrid {

	public static final String NAME = "NAME";
	private String automatedSuiteId;

	public AutomatedSuiteExecutionGrid(String automatedSuiteId) {
		this.automatedSuiteId = automatedSuiteId;
	}

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(EXECUTION.EXECUTION_STATUS),
			new GridColumn(EXECUTION.NAME),
			new GridColumn(EXECUTION.REFERENCE),
			new GridColumn(EXECUTION.DATASET_LABEL),
			new GridColumn(EXECUTION.LAST_EXECUTED_ON),
			new GridColumn(EXECUTION.EXECUTION_ID)
		);
	}

	@Override
	protected Table<?> getTable() {
		return AUTOMATED_SUITE
			.innerJoin(AUTOMATED_EXECUTION_EXTENDER).on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
			.innerJoin(EXECUTION).on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID));
	}

	@Override
	protected Field<?> getIdentifier() {
		return EXECUTION.EXECUTION_ID;
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return null;
	}

	@Override
	protected Condition craftInvariantFilter() {
		return AUTOMATED_SUITE.SUITE_ID.eq(this.automatedSuiteId);
	}
}

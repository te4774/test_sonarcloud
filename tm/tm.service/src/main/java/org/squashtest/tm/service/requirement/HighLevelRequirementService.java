/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.requirement;


import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.service.internal.display.dto.requirement.HighLevelRequirementVersionDto.LinkedLowLevelRequirementDto;
import org.squashtest.tm.service.internal.dto.HighLevelRequirementExceptionSummary;

import java.util.List;
import java.util.Map;

public interface HighLevelRequirementService {
	/**
	 * Create a link between a {@link org.squashtest.tm.domain.requirement.HighLevelRequirement} and a standard non-high level requirement
	 * {@link org.squashtest.tm.domain.requirement.Requirement}
	 * @param highLevelRequirementId id of high level req
	 * @param lowLevelRequirementId id of the standard req
	 */
	void linkToHighLevelRequirement(Long highLevelRequirementId, Long lowLevelRequirementId);

	/**
	 * Unlink a standard requirement from it's high level requirement. No op if no link exist
	 * @param requirementId the id of concerned requirement
	 */
	void unlinkToHighLevelRequirement(Long requirementId);

	void bindRequirementsToHighLevelRequirement(Long highLevelRequirementId, List<Long> lowLevelRequirementIds, HighLevelRequirementExceptionSummary summary);

	void unbindRequirementFromHighLevelRequirement(Long highLevelRequirementId, List<Long> lowLevelRequirementIds);

	List<LinkedLowLevelRequirementDto> findLinkedLowLevelRequirements(Long requirementId);

	Map<Long, List<Long>> findLinkedLowLevelReqIdsMappedByHighLevelReqIdFromVersionIds(List<Long> versionIds);

	List<Requirement> findStandardRequirementsByHighLvlReqId(Long highLevelRequirementId);

	void convertIntoHighLevelRequirement(long requirementId);

	void convertIntoStandardRequirement(long requirementId);
}

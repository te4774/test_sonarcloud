/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.service.internal.display.dto.campaign.TestSuiteDto;
import org.squashtest.tm.service.internal.repository.display.TestSuiteDisplayDao;

import java.util.HashMap;
import java.util.List;

import static org.squashtest.tm.domain.project.AutomationWorkflowType.NONE;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.jenkins;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.squashAutom;
import static org.squashtest.tm.domain.testcase.TestCaseAutomatable.Y;
import static org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus.AUTOMATED;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.tables.CampaignIteration.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.tables.CampaignLibraryNode.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.tables.Iteration.ITERATION;
import static org.squashtest.tm.jooq.domain.tables.IterationTestPlanItem.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.tables.IterationTestSuite.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.tables.Project.PROJECT;
import static org.squashtest.tm.jooq.domain.tables.TestCase.TEST_CASE;
import static org.squashtest.tm.jooq.domain.tables.TestSuite.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.tables.TestSuiteTestPlanItem.TEST_SUITE_TEST_PLAN_ITEM;

@Repository
public class TestSuiteDisplayDaoImpl implements TestSuiteDisplayDao {

	private DSLContext dsl;

	public TestSuiteDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public TestSuiteDto findById(long testSuiteId) {
		SelectHavingStep<?> findAutomatedTests = findAutomatedTests();

		return dsl.select(
			TEST_SUITE.ID,
			TEST_SUITE.UUID,
			TEST_SUITE.NAME,
			TEST_SUITE.DESCRIPTION,
			TEST_SUITE.EXECUTION_STATUS,
			TEST_SUITE.ATTACHMENT_LIST_ID,
			TEST_SUITE.CREATED_BY,
			TEST_SUITE.CREATED_ON,
			TEST_SUITE.LAST_MODIFIED_BY,
			TEST_SUITE.LAST_MODIFIED_ON,
			CAMPAIGN_LIBRARY_NODE.PROJECT_ID,
			DSL.field(DSL.count(findAutomatedTests.field(TEST_CASE.TCLN_ID)).gt(0)).as("HAS_AUTOMATED_TESTS"),
			DSL.field(DSL.count(ITERATION_TEST_PLAN_ITEM.DATASET_ID).gt(0)).as("HAS_DATASETS"))
			.from(TEST_SUITE)
			.leftJoin(TEST_SUITE_TEST_PLAN_ITEM).on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
			.leftJoin(ITERATION_TEST_PLAN_ITEM).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
			.innerJoin(ITERATION_TEST_SUITE).on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
			.innerJoin(ITERATION).on(ITERATION_TEST_SUITE.ITERATION_ID.eq(ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
			.leftJoin(findAutomatedTests).on(findAutomatedTests.field(TEST_SUITE.ID).eq(TEST_SUITE.ID))
			.where(TEST_SUITE.ID.eq(testSuiteId))
			.groupBy(TEST_SUITE.ID, CAMPAIGN_LIBRARY_NODE.PROJECT_ID)
			.fetchOne()
			.into(TestSuiteDto.class);
	}

	private SelectHavingStep<?> findAutomatedTests() {
		return DSL.select(TEST_SUITE.ID, TEST_CASE.TCLN_ID)
			.from(TEST_SUITE)
			.innerJoin(TEST_SUITE_TEST_PLAN_ITEM).on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
			.innerJoin(ITERATION_TEST_PLAN_ITEM).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
			.innerJoin(TEST_CASE).on(TEST_CASE.TCLN_ID.eq(ITERATION_TEST_PLAN_ITEM.TCLN_ID))
			.innerJoin(TEST_CASE_LIBRARY_NODE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
			.leftJoin(AUTOMATION_REQUEST).on(AUTOMATION_REQUEST.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
			.innerJoin(PROJECT).on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
			.innerJoin(TEST_AUTOMATION_SERVER).on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(PROJECT.TA_SERVER_ID))
			// if workflow active, TestCase must be automatable(Y) and its automation request must be automated
			.where(((PROJECT.AUTOMATION_WORKFLOW_TYPE.ne(NONE.name())
				.and(TEST_CASE.AUTOMATABLE.eq(Y.name()))
				.and(AUTOMATION_REQUEST.REQUEST_STATUS.eq(AUTOMATED.name())))
				.or(PROJECT.AUTOMATION_WORKFLOW_TYPE.eq(NONE.name())))
			// if Jenkins server, TestCase only has to be linked to an automation test
			.and((TEST_AUTOMATION_SERVER.KIND.eq(jenkins.name()).and(TEST_CASE.TA_TEST.isNotNull()))
				// if Squash Autom server, then the 3 automation attributes must exist
				.or(TEST_AUTOMATION_SERVER.KIND.eq(squashAutom.name()).and(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.isNotNull()).and(TEST_CASE.AUTOMATED_TEST_REFERENCE.isNotNull()).and(TEST_CASE.SCM_REPOSITORY_ID.isNotNull())))
			);
	}

	@Override
	public List<NamedReference> findForIteration(Long iterationId) {
		return dsl.select(
                TEST_SUITE.ID,
                TEST_SUITE.NAME)
            .from(TEST_SUITE)
            .innerJoin(ITERATION_TEST_SUITE).on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
            .where(ITERATION_TEST_SUITE.ITERATION_ID.eq(iterationId))
            .fetch().into(NamedReference.class);
	}

	@Override
	public Long findIterationIdByTestsuiteId(Long testSuiteId) {
		return dsl.select(
			ITERATION_TEST_SUITE.ITERATION_ID)
			.from(ITERATION_TEST_SUITE)
			.where(ITERATION_TEST_SUITE.TEST_SUITE_ID.eq(testSuiteId))
			.fetchOne(0, long.class);
	}

	@Override
	public HashMap<Long, String> getExecutionStatusMap(Long testSuiteId) {
		return (HashMap<Long, String>) dsl.select(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID, ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS)
			.from(TEST_SUITE)
			.innerJoin(TEST_SUITE_TEST_PLAN_ITEM).on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
			.innerJoin(ITERATION_TEST_PLAN_ITEM).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
			.where(TEST_SUITE.ID.eq(testSuiteId))
			.fetch().intoMap(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID, ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS);
	}
	@Override
	public int getNbTestPlanItem(Long testSuiteId, String login) {
		SelectConditionStep<Record1<Integer>> allTestPlanItemsFromCampaign = dsl.selectCount()
			.from(TEST_SUITE)
			.innerJoin(TEST_SUITE_TEST_PLAN_ITEM).on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
			.innerJoin(ITERATION_TEST_PLAN_ITEM).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
			.leftJoin(CORE_USER).on(CORE_USER.PARTY_ID.eq(ITERATION_TEST_PLAN_ITEM.USER_ID))
			.where(TEST_SUITE.ID.eq(testSuiteId));
		if (login != null) {
			allTestPlanItemsFromCampaign = allTestPlanItemsFromCampaign.and(CORE_USER.LOGIN.eq(login));
		}

		return allTestPlanItemsFromCampaign.groupBy(TEST_SUITE.ID)
			.fetchOne(0, int.class);
	}
}

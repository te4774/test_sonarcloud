/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableOption;
import org.squashtest.tm.domain.environmentvariable.SingleSelectEnvironmentVariable;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.RequiredFieldException;
import org.squashtest.tm.exception.customfield.CodeAlreadyExistsException;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableManagerService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableValueService;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao;

import java.util.List;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

@Transactional
@Service("EnvironmentVariableManagerService")
public class EnvironmentVariableManagerServiceImpl implements EnvironmentVariableManagerService {

	private final EnvironmentVariableDao environmentVariableDao;
	private final EnvironmentVariableBindingService environmentVariableBindingService;
	private final EnvironmentVariableValueService environmentVariableValueService;

	public EnvironmentVariableManagerServiceImpl(EnvironmentVariableDao environmentVariableDao,
												 EnvironmentVariableBindingService environmentVariableBindingService,
												 EnvironmentVariableValueService environmentVariableValueService) {
		this.environmentVariableDao = environmentVariableDao;
		this.environmentVariableBindingService = environmentVariableBindingService;
		this.environmentVariableValueService = environmentVariableValueService;
	}

	@Override
	@PreAuthorize(HAS_ROLE_ADMIN)
	public void persist(EnvironmentVariable environmentVariable) {
		checkDuplicateCode(environmentVariable.getCode());
		checkDuplicateName(environmentVariable.getName());
		environmentVariableDao.save(environmentVariable);
	}

	@Override
	@PreAuthorize(HAS_ROLE_ADMIN)
	public void deleteEnvironmentVariable(List<Long> environmentVariableIds) {
		environmentVariableIds.forEach(id -> {
			environmentVariableBindingService.unbindByEnvironmentVariableId(id);
			EnvironmentVariable environmentVariable = environmentVariableDao.getById(id);
			environmentVariableDao.delete(environmentVariable);
		});

	}

	@Override
	@PreAuthorize(HAS_ROLE_ADMIN)
	public void changeName(Long evId, String newName) {
		String trimmedNewName = newName.trim();
		EnvironmentVariable environmentVariable = environmentVariableDao.getById(evId);
		String oldName = environmentVariable.getName();

		if (StringUtils.equals(oldName,trimmedNewName)) {
			return;
		}
		checkDuplicateName(newName);
		environmentVariable.setName(trimmedNewName);
	}

	@Override
	@PreAuthorize(HAS_ROLE_ADMIN)
	public void changeCode(Long evId, String newCode) {
		EnvironmentVariable environmentVariable = environmentVariableDao.getById(evId);
		if (StringUtils.equals(environmentVariable.getCode(),newCode)) {
			return;
		}
		checkDuplicateCode(newCode);
		environmentVariable.setCode(newCode);

	}

	@Override
	@PreAuthorize(HAS_ROLE_ADMIN)
	public void changeOptionCode(long evId, String optionLabel, String newCode) {
		if (newCode == null || newCode.trim().isEmpty()) {
			throw new RequiredFieldException("code");
		}
		SingleSelectEnvironmentVariable environmentVariable = environmentVariableDao.findSingleSelectEnvironmentVariableById(evId);
		environmentVariable.changeOptionCode(optionLabel, newCode);
	}

	@Override
	@PreAuthorize(HAS_ROLE_ADMIN)
	public void changeOptionLabel(long evid, String optionLabel, String newLabel) {
		if (newLabel == null || newLabel.trim().isEmpty()) {
			throw new RequiredFieldException("label");
		}
		SingleSelectEnvironmentVariable environmentVariable = environmentVariableDao.findSingleSelectEnvironmentVariableById(evid);
		environmentVariable.changeOptionLabel(optionLabel, newLabel);
	}

	@Override
	@PreAuthorize(HAS_ROLE_ADMIN)
	public void addOption(long evId, EnvironmentVariableOption option) {
		SingleSelectEnvironmentVariable environmentVariable = environmentVariableDao.findSingleSelectEnvironmentVariableById(evId);
		environmentVariable.addOption(option);
	}

	@Override
	@PreAuthorize(HAS_ROLE_ADMIN)
	public void removeOptions(Long evId, List<String> optionLabels) {
		optionLabels.forEach(optionLabel -> {
			SingleSelectEnvironmentVariable environmentVariable = environmentVariableDao.findSingleSelectEnvironmentVariableById(evId);
			environmentVariable.removeOptionAndReorderList(optionLabel);
		});
		environmentVariableValueService.reinitializeEnvironmentVariableValuesByValueAndEvId(optionLabels, evId);
	}

	@Override
	@PreAuthorize(HAS_ROLE_ADMIN)
	public void changeOptionsPosition(Long environmentVariableId, Integer position, List<String> labels) {
		SingleSelectEnvironmentVariable environmentVariable = environmentVariableDao.findSingleSelectEnvironmentVariableById(environmentVariableId);
		environmentVariable.moveOptions(position, labels);
	}

	private void checkDuplicateCode(String code) {
		EnvironmentVariable codeDuplicate = environmentVariableDao.findByCode(code);
		if (codeDuplicate != null) {
			throw new CodeAlreadyExistsException(null, code, EnvironmentVariable.class);
		}
	}

	private void checkDuplicateName(String name) {
		EnvironmentVariable nameDuplicate = environmentVariableDao.findByName(name);
		if (nameDuplicate != null) {
			throw new NameAlreadyInUseException("EnvironmentVariable", HtmlUtils.htmlEscape(name));
		}
	}
}

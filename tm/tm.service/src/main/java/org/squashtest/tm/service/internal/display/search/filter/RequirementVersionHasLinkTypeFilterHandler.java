/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import com.google.common.collect.Sets;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.hibernate.HibernateQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.jpql.ExtendedHibernateQuery;
import org.squashtest.tm.domain.requirement.QRequirementVersion;
import org.squashtest.tm.domain.requirement.QRequirementVersionLink;
import org.squashtest.tm.domain.requirement.QRequirementVersionLinkType;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

import java.util.List;
import java.util.Set;

import static org.squashtest.tm.domain.requirement.QRequirementVersion.requirementVersion;

@Component
public class RequirementVersionHasLinkTypeFilterHandler implements FilterHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequirementVersionHasLinkTypeFilterHandler.class);

	private final Set<String> handledPrototypes = Sets.newHashSet("REQUIREMENT_VERSION_HAS_LINK_TYPE");

	@Override
	public boolean canHandleFilter(GridFilterValue filter) {
		return this.handledPrototypes.contains(filter.getColumnPrototype());
	}

	@Override
	public void handleFilter(ExtendedHibernateQuery<?> query, GridFilterValue filter) {

		QRequirementVersion outerVersion = requirementVersion;
		QRequirementVersion reqVersionInit = new QRequirementVersion("reqVersionInit");
		QRequirementVersionLink versionLink = new QRequirementVersionLink("versionLink");
		QRequirementVersionLinkType versionType = new QRequirementVersionLinkType("versionType");

		List<String> values = filter.getValues();
		FilterOperation filterOperation = FilterOperation.valueOf(filter.getOperation());
		HibernateQuery<Integer> subquery = new ExtendedHibernateQuery<>().select(Expressions.ONE)
			.from(reqVersionInit)
			.join(reqVersionInit.requirementVersionLinks, versionLink)
			.join(versionLink.linkType, versionType)
			.where(
				reqVersionInit.id.eq(outerVersion.id)
					.and(
						(versionLink.linkDirection.isFalse().and(versionType.role2Code.in(values))
							.or(versionLink.linkDirection.isTrue().and(versionType.role1Code.in(values))
							)
						)
					)
			);

		if (FilterOperation.NONE.equals(filterOperation)) {
			query.where(subquery.notExists());
		} else {
			query.where(subquery.exists());
		}
	}

	public enum FilterOperation {
		NONE, AT_LEAST_ONE
	}
}

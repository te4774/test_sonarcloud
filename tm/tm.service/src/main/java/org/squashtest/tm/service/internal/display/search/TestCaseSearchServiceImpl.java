/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search;

import org.jooq.DSLContext;
import org.jooq.TableField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.service.display.search.TestCaseSearchService;
import org.squashtest.tm.service.internal.display.search.filter.FilterHandlers;
import org.squashtest.tm.service.internal.display.search.filter.FilterValueHandlers;
import org.squashtest.tm.service.internal.repository.ColumnPrototypeDao;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.query.QueryProcessingService;

import javax.persistence.EntityManager;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.TEST_CASE_ID;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

@Service
@Transactional(readOnly = true)
public class TestCaseSearchServiceImpl extends AbstractSearchService implements TestCaseSearchService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestCaseSearchServiceImpl.class);

	public TestCaseSearchServiceImpl(
		QueryProcessingService queryService,
		ColumnPrototypeDao columnPrototypeDao,
		EntityManager entityManager,
		ProjectFinder projectFinder,
		FilterHandlers filterHandlers,
		FilterValueHandlers gridFilterValueHandlers,
		DSLContext dslContext) {
		super(queryService, columnPrototypeDao, entityManager, projectFinder, filterHandlers, gridFilterValueHandlers, dslContext);
	}

	@Override
	protected List<String> getIdentifierColumnName() {
		return singletonList(TEST_CASE_ID);
	}

	@Override
	protected String getDefaultSortColumnName() {
		return TEST_CASE_ID;
	}

	protected EntityType getLibraryEntityType() {
		return EntityType.TEST_CASE_LIBRARY;
	}

	protected TableField<ProjectRecord, Long> getLibraryIdField() {
		return PROJECT.TCL_ID;
	}
}

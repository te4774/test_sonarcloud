/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.actionword;


import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.bdd.ActionWordParameter;
import org.squashtest.tm.domain.bdd.ActionWordParameterValue;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.service.actionword.ActionWordLibraryNodeService;
import org.squashtest.tm.service.internal.repository.ActionWordDao;
import org.squashtest.tm.service.internal.repository.KeywordTestStepDao;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ActionWordArrangerImpl implements ActionWordArranger {

	@Inject
	private ActionWordDao actionWordDao;

	@Inject
	private KeywordTestStepDao keywordTestStepDao;

	@Inject
	private ActionWordLibraryNodeService actionWordLibraryNodeService;

	@Override
	public void arrangeActionWordsBeforeProjectDeleting(Long projectId) {

		Map<Long, Long> actionsWordsWithSubstitute = actionWordDao.findAllWithDuplicateTokenByProjectId(projectId);
		replaceInEachBySubstitute(actionsWordsWithSubstitute);

		Map<Long, Long> actionWordsToMove = actionWordDao.findAllWithoutDuplicateTokenByProjectId(projectId);
		if (!actionWordsToMove.isEmpty()){
			actionWordLibraryNodeService.moveNodes(new ArrayList<>(actionWordsToMove.keySet()), Collections.min(actionWordsToMove.values()));
		}

	}
	private void replaceInEachBySubstitute(Map<Long, Long> actionsWordsWithSubstitute) {
		actionsWordsWithSubstitute.forEach((key, value) -> {
			Collection<KeywordTestStep> keywordTestSteps = keywordTestStepDao.findByActionWord(key);
			ActionWord substitute = actionWordDao.getById(value);
			keywordTestSteps.forEach( keywordTestStep -> {
				keywordTestStep.setActionWord(substitute);
				updateActionWordParams(keywordTestStep, substitute);
			});
		});
	}

	private void updateActionWordParams(KeywordTestStep targetKeywordTestStep, ActionWord substitute) {

		List<ActionWordParameterValue> parameterValues = targetKeywordTestStep.getParamValues();
		List<ActionWordParameter> actionWordParameters = substitute.getActionWordParams();

		parameterValues.forEach(parameterValue -> {
			String parameterValueName = parameterValue.getActionWordParam().getName();

			ActionWordParameter actionWordParameter = actionWordParameters.stream().filter( ActionWordParameter ->
				ActionWordParameter.getName().equals(parameterValueName)).collect(Collectors.toList()).get(0);

			parameterValue.setActionWordParam(actionWordParameter);
		});
	}

}

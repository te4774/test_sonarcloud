/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.AutomatedTestTechnologyDto;
import org.squashtest.tm.service.internal.repository.display.AutomatedTestTechnologyDisplayDao;

import java.util.List;

import static org.squashtest.tm.jooq.domain.tables.AutomatedTestTechnology.AUTOMATED_TEST_TECHNOLOGY;

@Repository
public class AutomatedTestTechnologyDisplayDaoImpl implements AutomatedTestTechnologyDisplayDao {

	private DSLContext dslContext;

	public AutomatedTestTechnologyDisplayDaoImpl(DSLContext dslContext) {
		this.dslContext = dslContext;
	}

	@Override
	public List<AutomatedTestTechnologyDto> findAll() {
		return dslContext.select(
			AUTOMATED_TEST_TECHNOLOGY.AT_TECHNOLOGY_ID.as("ID"),
			AUTOMATED_TEST_TECHNOLOGY.NAME,
			AUTOMATED_TEST_TECHNOLOGY.ACTION_PROVIDER_KEY,
			AUTOMATED_TEST_TECHNOLOGY.PREMIUM)
			.from(AUTOMATED_TEST_TECHNOLOGY)
			.fetchInto(AutomatedTestTechnologyDto.class);
	}
}

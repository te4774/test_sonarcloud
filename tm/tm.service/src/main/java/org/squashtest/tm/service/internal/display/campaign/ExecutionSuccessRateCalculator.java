/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import org.jooq.DSLContext;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.EXECUTION_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.STEP_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.STEP_STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.SUCCESS_RATE;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExecutionSuccessRateCalculator {

	private final DSLContext dslContext;

	public ExecutionSuccessRateCalculator(DSLContext dslContext) {
		this.dslContext = dslContext;
	}

	public void appendSuccessRate(GridResponse gridResponse){
		this.appendSuccessRate(gridResponse.getDataRows());
	}

	private void appendSuccessRate(List<DataRow> dataRows) {
		Set<Long> executionIds = extractIds(dataRows);

		SelectHavingStep<?> computeSuccessRate = computeSuccessRate(executionIds);

		Map<Long, Float> rateById = dslContext
			.select(
				computeSuccessRate.field(EXECUTION_ID),
				DSL.ifnull(computeSuccessRate.field(SUCCESS_RATE), 0).as(SUCCESS_RATE))
			.from(computeSuccessRate)
			.fetch().intoMap(
				computeSuccessRate.field(EXECUTION_ID, Long.class),
				computeSuccessRate.field(SUCCESS_RATE, Float.class));

		Converter<String, String> converter = CaseFormat.UPPER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL);
		String successRateKey = converter.convert(SUCCESS_RATE);
		String executionModeKey = converter.convert("EXECUTION_MODE");
		String executionStatusKey = converter.convert("EXECUTION_STATUS");

		dataRows.forEach(dataRow -> {
			long id = Long.parseLong(dataRow.getId());
			if (TestCaseExecutionMode.AUTOMATED.name().equals(dataRow.getData().get(executionModeKey))) {
				boolean isAutomatedExecutionInSuccess = ExecutionStatus.SUCCESS.name().equals(dataRow.getData().get(executionStatusKey));
				float automatedExecutionSuccessRate = isAutomatedExecutionInSuccess ? 100 : 0;
				dataRow.getData().put(successRateKey, automatedExecutionSuccessRate);
			} else {
				dataRow.getData().put(successRateKey, rateById.getOrDefault(id, 0f));
			}
		});

	}

	private Set<Long> extractIds(List<DataRow> dataRows) {
		return dataRows.stream()
			.map(DataRow::getId)
			.map(Long::parseLong)
			.collect(Collectors.toSet());
	}

	/**
	 * For each execution:
	 *  - Find all the steps for this execution with their corresponding status
	 *  - Compute the ratio of successful steps over the total number of steps
	 * @return execution step success rate
	 */
	private SelectHavingStep<?> computeSuccessRate(Set<Long> executionIds) {
		final String NUM_SUCCESS = "NUM_SUCCESS";
		final String NUM_STEPS = "NUM_STEPS";

		SelectHavingStep<?> stepsWithStatus = getStepsWithStatus(executionIds);

		// NUM_SUCCESS
		SelectHavingStep<?> successQuery = DSL.select(
				stepsWithStatus.field(EXECUTION_ID).as(EXECUTION_ID),
				DSL.count(DSL.field(STEP_ID)).as(NUM_SUCCESS))
			.from(stepsWithStatus)
			.where(stepsWithStatus.field(STEP_STATUS, String.class).eq("SUCCESS"))
			.groupBy(stepsWithStatus.field(EXECUTION_ID));

		// NUM_STEPS
		SelectHavingStep<?> totalQuery = DSL.select(
				stepsWithStatus.field(EXECUTION_ID).as(EXECUTION_ID),
				DSL.count(DSL.field(STEP_ID)).as(NUM_STEPS))
			.from(stepsWithStatus)
			.groupBy(stepsWithStatus.field(EXECUTION_ID));

		// SUCCESS_RATE
		return DSL.select(
				totalQuery.field(EXECUTION_ID).as(EXECUTION_ID),
				// Postgresql needs these explicit casts to get the division right
				successQuery.field(NUM_SUCCESS).cast(Double.class)
					.divide(totalQuery.field(NUM_STEPS).cast(Double.class)).multiply(100.0).as(SUCCESS_RATE))
			.from(successQuery).rightJoin(totalQuery)
			.on(successQuery.field(EXECUTION_ID, Long.class).eq(totalQuery.field(EXECUTION_ID, Long.class)));
	}

	private SelectHavingStep<?> getStepsWithStatus(Set<Long> executionIds) {
		// STEP_ID | STEP_STATUS
		return DSL.select(
				EXECUTION_EXECUTION_STEPS.EXECUTION_ID.as(EXECUTION_ID),
				EXECUTION_STEP.EXECUTION_STEP_ID.as(STEP_ID),
				EXECUTION_STEP.EXECUTION_STATUS.as(STEP_STATUS)
			).from(EXECUTION_EXECUTION_STEPS)
			.innerJoin(EXECUTION_STEP).on(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID.eq(EXECUTION_STEP.EXECUTION_STEP_ID))
			.where(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.in(executionIds));
	}
}

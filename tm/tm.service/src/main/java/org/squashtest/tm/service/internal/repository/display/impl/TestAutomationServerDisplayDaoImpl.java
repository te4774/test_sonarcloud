/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record12;
import org.jooq.SelectSelectStep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationServerDto;
import org.squashtest.tm.service.internal.repository.display.TestAutomationServerDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE;
import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.BASE_URL;

@Repository
public class TestAutomationServerDisplayDaoImpl implements TestAutomationServerDisplayDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestAutomationServerDisplayDaoImpl.class);

	@Autowired(required = false)
	private final Collection<TestAutomationConnector> connectors = Collections.emptyList();

	@Inject
	private DSLContext dslContext;

	@Override
	public List<TestAutomationServerDto> findAll() {
		List<TestAutomationServerDto> servers = getSelectClause()
			.from(TEST_AUTOMATION_SERVER)
			.innerJoin(THIRD_PARTY_SERVER).on(THIRD_PARTY_SERVER.SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
			.fetchInto(TestAutomationServerDto.class);

		servers.forEach(serverDto -> serverDto.setSupportsAutomatedExecutionEnvironments(supportsAutomatedExecutionEnvironments(serverDto.getKind())));

		return servers;
	}

	@Override
	public Set<TestAutomationServerKind> findAllAvailableKinds() {
		return connectors.stream()
			.map(TestAutomationConnector::getConnectorKind)
			.collect(Collectors.toSet());
	}

	@Override
	public TestAutomationServerDto getTestAutomationServerById(long testAutomationServerId) {
		final TestAutomationServerDto dto = getSelectClause()
			.from(TEST_AUTOMATION_SERVER)
			.innerJoin(THIRD_PARTY_SERVER).on(THIRD_PARTY_SERVER.SERVER_ID.eq(TEST_AUTOMATION_SERVER.SERVER_ID))
			.where(TEST_AUTOMATION_SERVER.SERVER_ID.eq(testAutomationServerId))
			.fetchOne().into(TestAutomationServerDto.class);

		dto.setSupportsAutomatedExecutionEnvironments(supportsAutomatedExecutionEnvironments(dto.getKind()));

		return dto;
	}

	private SelectSelectStep<Record12<Long, String, String, String, String, String, Boolean, String, Timestamp, String, Timestamp, String>> getSelectClause() {
		return dslContext.select(
			TEST_AUTOMATION_SERVER.SERVER_ID.as(RequestAliasesConstants.ID),
			THIRD_PARTY_SERVER.NAME,
			THIRD_PARTY_SERVER.URL.as(BASE_URL),
			THIRD_PARTY_SERVER.AUTH_PROTOCOL,
			TEST_AUTOMATION_SERVER.KIND,
			TEST_AUTOMATION_SERVER.DESCRIPTION,
			TEST_AUTOMATION_SERVER.MANUAL_SLAVE_SELECTION,
			TEST_AUTOMATION_SERVER.CREATED_BY,
			TEST_AUTOMATION_SERVER.CREATED_ON,
			TEST_AUTOMATION_SERVER.LAST_MODIFIED_BY,
			TEST_AUTOMATION_SERVER.LAST_MODIFIED_ON,
			TEST_AUTOMATION_SERVER.OBSERVER_URL
		);
	}

	@Override
	public boolean supportsAutomatedExecutionEnvironments(String kind) {
		try {
			return supportsAutomatedExecutionEnvironments(TestAutomationServerKind.valueOf(kind));
		} catch (IllegalArgumentException | NullPointerException ex) {
			if (LOGGER.isWarnEnabled()) {
				LOGGER.warn(String.format("No TestAutomationServerKind matches '%s'.", kind), ex);
			}

			return false;
		}
	}

	@Override
	public boolean supportsAutomatedExecutionEnvironments(TestAutomationServerKind connectorKind) {
		final Optional<TestAutomationConnector> matchingConnector = connectors.stream()
			.filter(con -> con.getConnectorKind().equals(connectorKind))
			.findAny();

		if (matchingConnector.isPresent()) {
			return matchingConnector.get().supportsAutomatedExecutionEnvironments();
		} else {
			if (LOGGER.isTraceEnabled()) {
				LOGGER.trace(String.format("No connector was found with kind %s.", connectorKind));
			}

			return false;
		}
	}

	public List<BoundEnvironmentVariableDto> getDefaultBoundEnvironmentVariablesByServerId(Long serverId) {
		return dslContext.select(
				ENVIRONMENT_VARIABLE_BINDING.EVB_ID.as("BINDING_ID"),
				ENVIRONMENT_VARIABLE.NAME,
				ENVIRONMENT_VARIABLE.CODE,
				ENVIRONMENT_VARIABLE.EV_ID.as(RequestAliasesConstants.ID),
				ENVIRONMENT_VARIABLE.INPUT_TYPE,
				ENVIRONMENT_VARIABLE_VALUE.VALUE,
				ENVIRONMENT_VARIABLE_VALUE.BOUND_ENTITY_TYPE.as("EV_BINDABLE_ENTITY")
			).from(ENVIRONMENT_VARIABLE)
			.leftJoin(ENVIRONMENT_VARIABLE_BINDING).on(ENVIRONMENT_VARIABLE_BINDING.EV_ID.eq(ENVIRONMENT_VARIABLE.EV_ID))
			.leftJoin(ENVIRONMENT_VARIABLE_VALUE).on((ENVIRONMENT_VARIABLE_VALUE.BOUND_ENTITY_TYPE.eq(EVBindableEntity.TEST_AUTOMATION_SERVER.name()))
				.and(ENVIRONMENT_VARIABLE_VALUE.EVB_ID.eq(ENVIRONMENT_VARIABLE_BINDING.EVB_ID)))
			.where(ENVIRONMENT_VARIABLE_BINDING.BOUND_SERVER_ID.eq(serverId))
			.fetch().into(BoundEnvironmentVariableDto.class);
	}
}

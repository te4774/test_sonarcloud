/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.requirement;

import java.util.ArrayList;
import java.util.List;

public class RequirementVersionDto extends AbstractRequirementVersionDto {
	private List<RequirementVersionLinkDto> requirementVersionLinks = new ArrayList<>();
	private LinkedHighLevelRequirementDto linkedHighLevelRequirement;

	private boolean isChildOfRequirement;

	public List<RequirementVersionLinkDto> getRequirementVersionLinks() {
		return requirementVersionLinks;
	}

	public void setRequirementVersionLinks(List<RequirementVersionLinkDto> requirementVersionLinks) {
		this.requirementVersionLinks = requirementVersionLinks;
	}

	public LinkedHighLevelRequirementDto getLinkedHighLevelRequirement() {
		return linkedHighLevelRequirement;
	}

	public void setLinkedHighLevelRequirement(LinkedHighLevelRequirementDto linkedHighLevelRequirement) {
		this.linkedHighLevelRequirement = linkedHighLevelRequirement;
	}

	public boolean isChildOfRequirement() {
		return isChildOfRequirement;
	}

	public void setChildOfRequirement(boolean childOfRequirement) {
		isChildOfRequirement = childOfRequirement;
	}

	@Override
	public boolean isHighLevelRequirement() {
		return false;
	}

	public static class LinkedHighLevelRequirementDto {
		private Long requirementId;
		private Long requirementVersionId;
		private String name;
		private String reference;
		private String path;
		private String projectName;

		public Long getRequirementId() {
			return requirementId;
		}

		public void setRequirementId(Long requirementId) {
			this.requirementId = requirementId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Long getRequirementVersionId() {
			return requirementVersionId;
		}

		public void setRequirementVersionId(Long requirementVersionId) {
			this.requirementVersionId = requirementVersionId;
		}

		public String getReference() {
			return reference;
		}

		public void setReference(String reference) {
			this.reference = reference;
		}

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public String getProjectName() {
			return projectName;
		}

		public void setProjectName(String projectName) {
			this.projectName = projectName;
		}
	}
}

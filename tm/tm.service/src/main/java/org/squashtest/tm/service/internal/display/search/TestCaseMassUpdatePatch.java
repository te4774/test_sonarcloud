/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search;

import java.util.List;

public class TestCaseMassUpdatePatch {

	private List<Long> testCaseIds;
	private Long nature;
	private Long type;
	private String automatable;
	private boolean importanceAuto;
	private String importance;
	private String status;
	private boolean changeAutomatedTestTechnology;
	private Long automatedTestTechnology;
	private boolean changeScmRepository;
	private Long scmRepository;

	public List<Long> getTestCaseIds() {
		return testCaseIds;
	}

	public void setTestCaseIds(List<Long> testCaseIds) {
		this.testCaseIds = testCaseIds;
	}

	public Long getNature() {
		return nature;
	}

	public void setNature(Long nature) {
		this.nature = nature;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getAutomatable() {
		return automatable;
	}

	public void setAutomatable(String automatable) {
		this.automatable = automatable;
	}

	public boolean isImportanceAuto() {
		return importanceAuto;
	}

	public void setImportanceAuto(boolean importanceAuto) {
		this.importanceAuto = importanceAuto;
	}

	public String getImportance() {
		return importance;
	}

	public void setImportance(String importance) {
		this.importance = importance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getAutomatedTestTechnology() {
		return automatedTestTechnology;
	}

	public void setAutomatedTestTechnology(Long automatedTestTechnology) {
		this.automatedTestTechnology = automatedTestTechnology;
	}

	public boolean isChangeAutomatedTestTechnology() {
		return changeAutomatedTestTechnology;
	}

	public void setChangeAutomatedTestTechnology(boolean changeAutomatedTestTechnology) {
		this.changeAutomatedTestTechnology = changeAutomatedTestTechnology;
	}

	public boolean isChangeScmRepository() {
		return changeScmRepository;
	}

	public void setChangeScmRepository(boolean changeScmRepository) {
		this.changeScmRepository = changeScmRepository;
	}

	public Long getScmRepository() {
		return scmRepository;
	}

	public void setScmRepository(Long scmRepository) {
		this.scmRepository = scmRepository;
	}
}

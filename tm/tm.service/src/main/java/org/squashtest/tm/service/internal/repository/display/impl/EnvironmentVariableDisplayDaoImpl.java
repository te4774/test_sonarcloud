/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.service.internal.display.dto.EnvironmentVariableDto;
import org.squashtest.tm.service.internal.display.dto.EnvironmentVariableOptionDto;
import org.squashtest.tm.service.internal.repository.display.EnvironmentVariableDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import javax.inject.Inject;

import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE;
import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE_OPTION;

@Repository
public class EnvironmentVariableDisplayDaoImpl implements EnvironmentVariableDisplayDao {

	@Inject
	private DSLContext dsl;

	@Override
	public EnvironmentVariableDto getEnvironmentVariableById(Long environmentVariableId) {
		EnvironmentVariableDto dto = dsl.select(
			ENVIRONMENT_VARIABLE.NAME,
			ENVIRONMENT_VARIABLE.CODE,
			ENVIRONMENT_VARIABLE.EV_ID.as(RequestAliasesConstants.ID),
			ENVIRONMENT_VARIABLE.INPUT_TYPE
		).from(ENVIRONMENT_VARIABLE)
			.where(ENVIRONMENT_VARIABLE.EV_ID.eq(environmentVariableId))
			.fetchOne().into(EnvironmentVariableDto.class);

		dto.setOptions(getEnvironmentVariableOptionListByEvId(environmentVariableId));

		return dto;
	}

	@Override
	public List<EnvironmentVariableOptionDto> getEnvironmentVariableOptionListByEvId(Long environmentVariableId) {
		return dsl.select(
			ENVIRONMENT_VARIABLE_OPTION.LABEL,
			ENVIRONMENT_VARIABLE_OPTION.CODE,
			ENVIRONMENT_VARIABLE_OPTION.POSITION
			).from(ENVIRONMENT_VARIABLE_OPTION)
			.where(ENVIRONMENT_VARIABLE_OPTION.EV_ID.eq(environmentVariableId))
			.orderBy(ENVIRONMENT_VARIABLE_OPTION.POSITION)
			.fetchInto(EnvironmentVariableOptionDto.class);
	}

	@Override
	public List<EnvironmentVariableDto> getAllEnvironmentVariableDto() {
		return dsl.select(
			ENVIRONMENT_VARIABLE.EV_ID.as(RequestAliasesConstants.ID),
			ENVIRONMENT_VARIABLE.NAME,
			ENVIRONMENT_VARIABLE.INPUT_TYPE,
			ENVIRONMENT_VARIABLE.CODE
		).from(ENVIRONMENT_VARIABLE)
			.orderBy(ENVIRONMENT_VARIABLE.NAME)
			.fetchInto(EnvironmentVariableDto.class);
	}
}

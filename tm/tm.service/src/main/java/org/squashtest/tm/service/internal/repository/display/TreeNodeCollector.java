/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.service.internal.display.grid.DataRow;

import java.util.List;
import java.util.Map;

/**
 * A TreeNodeCollector is responsible for collecting one type of nodes.
 * As implementation guidelines :
 * - The code should be resilient to nodes that doesn't exist. As the client can have outdated data, it could request some nodes that doesn't exist anymore.
 * - You should avoid N+1 or other poor performance practice, as this code will be called very frequently.
 * In most case you will only need 1 or 2 request even for thousand nodes. NEVER use Hibernate entities here, as we only need displaying data
 */
public interface TreeNodeCollector {
	/**
	 * Return DataRows ready to be displayed in clients.
	 *
	 * @param ids The ids of the required nodes.
	 * @return A map id-DataRow. If an id has not corresponding row, the entry shouldn't be in the map.
	 */
	Map<Long, DataRow> collect(List<Long> ids);

	/**
	 * @return The {@link NodeType} that a given collector handle.
	 */
	NodeType getHandledEntityType();
}

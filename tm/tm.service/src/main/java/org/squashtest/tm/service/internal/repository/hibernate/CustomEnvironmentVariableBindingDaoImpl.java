/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import org.jooq.DSLContext;
import org.jooq.Table;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableValue;
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto;
import org.squashtest.tm.service.internal.repository.CustomEnvironmentVariableBindingDao;

import javax.inject.Inject;
import java.util.List;

import static org.jooq.impl.DSL.rowNumber;
import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE;
import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.ENVIRONMENT_VARIABLE_VALUE;

@Repository
public class CustomEnvironmentVariableBindingDaoImpl implements CustomEnvironmentVariableBindingDao {

	@Inject
	private DSLContext dsl;

	@Override
	public List<Long> findAllBindingIdsByEvIdsAndServerId(List<Long> evIds, Long testAutomationServerId) {
		return dsl.select(
				ENVIRONMENT_VARIABLE_BINDING.EVB_ID
			).from(ENVIRONMENT_VARIABLE_BINDING)
			.where(ENVIRONMENT_VARIABLE_BINDING.BOUND_SERVER_ID.eq(testAutomationServerId))
			.and(ENVIRONMENT_VARIABLE_BINDING.EV_ID.in(evIds))
			.fetchInto(Long.class);
	}

	@Override
	public List<BoundEnvironmentVariableDto> findAllBoundEnvironmentVariables(Long serverId, Long projectId) {

		Table<?> result =
			dsl.select(
					ENVIRONMENT_VARIABLE.EV_ID.as("ID"),
					ENVIRONMENT_VARIABLE.NAME,
					ENVIRONMENT_VARIABLE.CODE,
					ENVIRONMENT_VARIABLE.INPUT_TYPE,
					ENVIRONMENT_VARIABLE_BINDING.EVB_ID.as("BINDING_ID"),
					ENVIRONMENT_VARIABLE_VALUE.VALUE,
					ENVIRONMENT_VARIABLE_VALUE.BOUND_ENTITY_TYPE.as("EV_BINDABLE_ENTITY"),
					rowNumber().over().partitionBy(ENVIRONMENT_VARIABLE_BINDING.EVB_ID).orderBy(ENVIRONMENT_VARIABLE_VALUE.BOUND_ENTITY_TYPE.asc()).cast(Long.class).as("range")
				)
				.from(ENVIRONMENT_VARIABLE)
				.innerJoin(ENVIRONMENT_VARIABLE_BINDING).on(ENVIRONMENT_VARIABLE_BINDING.EV_ID.eq(ENVIRONMENT_VARIABLE.EV_ID))
				.innerJoin(ENVIRONMENT_VARIABLE_VALUE).on(ENVIRONMENT_VARIABLE_VALUE.EVB_ID.eq(ENVIRONMENT_VARIABLE_BINDING.EVB_ID))
				.where(ENVIRONMENT_VARIABLE_BINDING.BOUND_SERVER_ID.eq(serverId))
				.and(ENVIRONMENT_VARIABLE_VALUE.BOUND_ENTITY_TYPE.eq(EVBindableEntity.TEST_AUTOMATION_SERVER.name()).and(ENVIRONMENT_VARIABLE_VALUE.BOUND_ENTITY_ID.eq(serverId)))
				.or((ENVIRONMENT_VARIABLE_VALUE.BOUND_ENTITY_TYPE.eq(EVBindableEntity.PROJECT.name())).and(ENVIRONMENT_VARIABLE_VALUE.BOUND_ENTITY_ID.eq(projectId)))
				.groupBy(ENVIRONMENT_VARIABLE.EV_ID, ENVIRONMENT_VARIABLE_BINDING.EVB_ID, ENVIRONMENT_VARIABLE_VALUE.EVB_ID, ENVIRONMENT_VARIABLE_VALUE.VALUE,
					ENVIRONMENT_VARIABLE_VALUE.BOUND_ENTITY_TYPE)
				.asTable();

		return dsl.select(
				result.field("ID"),
				result.field("NAME"),
				result.field("CODE"),
				result.field("INPUT_TYPE"),
				result.field("BINDING_ID"),
				result.field("VALUE"),
				result.field("EV_BINDABLE_ENTITY")
			).from(result)
			.where(result.field("range").coerce(Long.class).eq(1L))
			.orderBy(result.field("NAME"))
			.fetchInto(BoundEnvironmentVariableDto.class);
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.requirement;

import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.requirement.HighLevelNewRequirementVersionDto;
import org.squashtest.tm.domain.requirement.HighLevelRequirementConverter;
import org.squashtest.tm.domain.requirement.HighLevelRequirementCreator;
import org.squashtest.tm.domain.requirement.NewRequirementVersionDto;
import org.squashtest.tm.domain.requirement.Requirement;

import java.util.List;

@Component
public class RequirementFactory {

	private final List<HighLevelRequirementCreator> requirementCreators;
	private final List<HighLevelRequirementConverter> requirementConverters;

	public RequirementFactory(List<HighLevelRequirementCreator> requirementCreators,
							  List<HighLevelRequirementConverter> requirementConverters) {
		this.requirementCreators = requirementCreators;
		this.requirementConverters = requirementConverters;
	}

	Requirement createRequirement(NewRequirementVersionDto newVersionData) {
		if (newVersionData.isHighLevel()) {
			return createRequirement((HighLevelNewRequirementVersionDto) newVersionData);
		}
		return new Requirement(newVersionData.toRequirementVersion());
	}

	Requirement createRequirement(HighLevelNewRequirementVersionDto newVersionData) {
		HighLevelRequirementCreator requirementCreator = requirementCreators.stream()
			.findFirst()
			.orElseThrow(() -> new UnsupportedOperationException("A dedicated plugin is required to create high level requirements"));
		return requirementCreator.createRequirement(newVersionData);
	}

	void convertIntoHighLevelRequirement(Long requirementId) {
		HighLevelRequirementConverter requirementConverter = requirementConverters.stream()
			.findFirst()
			.orElseThrow(() -> new UnsupportedOperationException("A dedicated plugin is required to convert standard requirement into high level requirement"));
		requirementConverter.convertIntoHighLevelRequirement(requirementId);
	}

	void convertIntoStandardRequirement(Long requirementId) {
		HighLevelRequirementConverter requirementConverter = requirementConverters.stream()
				.findFirst()
				.orElseThrow(() -> new UnsupportedOperationException("A dedicated plugin is required to convert high level requirement into standard requirement"));
		requirementConverter.convertIntoStandardRequirement(requirementId);
	}

}

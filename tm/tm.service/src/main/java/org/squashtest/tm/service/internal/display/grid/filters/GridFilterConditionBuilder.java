/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.filters;

import org.jooq.Condition;
import org.jooq.Field;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

import java.sql.Timestamp;

import static java.util.Objects.requireNonNull;

public interface GridFilterConditionBuilder {
	Condition build();

	/**
	 * Default factory that link an {@link GridFilterOperation} to an implementation of {@link GridFilterConditionBuilder}.
	 * If it doesn't feat to specific needs for a given table, another factory and probably another Collector for filters must be implemented.
	 * See {@link org.squashtest.tm.service.internal.display.grid.AbstractGrid.DefaultFilterCollector} for further details.
	 * @param gridFilterValue the filter value
	 * @return {@link GridFilterConditionBuilder}
	 */
	static GridFilterConditionBuilder getConditionBuilder(Field<?> field, GridFilterValue gridFilterValue) {
		requireNonNull(gridFilterValue);
		requireNonNull(gridFilterValue.getOperation());
		GridFilterOperation operation = GridFilterOperation.valueOf(gridFilterValue.getOperation());
		switch (operation) {
			case LIKE:
				return new LikeConditionBuilder(field, gridFilterValue);
			case IN:
				return new InConditionBuilder(field, gridFilterValue);
			case BETWEEN:
				if (field.getDataType().getType().equals(Timestamp.class)) {
					@SuppressWarnings("unchecked") Field<Timestamp> timestampField = (Field<Timestamp>) field;
					return new BetweenConditionBuilder<>(timestampField, gridFilterValue);
				}
			case EQUALS:
				return new EqualConditionBuilder(field, gridFilterValue);
			default:
				throw new IllegalArgumentException("No GridFilterConditionBuilder for operation " + operation);
		}
	}
}

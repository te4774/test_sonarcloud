/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.SelectJoinStep;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.TeamAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TeamMemberDto;
import org.squashtest.tm.service.internal.repository.display.TeamDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import javax.inject.Inject;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM;
import static org.squashtest.tm.jooq.domain.Tables.CORE_TEAM_MEMBER;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;


@Repository
public class TeamDisplayDaoImpl implements TeamDisplayDao {

	@Inject
	private DSLContext dsl;

	@Override
	public TeamAdminViewDto getTeamById(Long teamId) {
		return getBaseRequest()
			.where(CORE_TEAM.PARTY_ID.eq(teamId))
			.fetchOne().into(TeamAdminViewDto.class);
	}

	@Override
	public List<TeamMemberDto> getMembersByTeam(Long teamId) {
		return dsl.select(
			CORE_USER.PARTY_ID,
			CORE_USER.FIRST_NAME,
			CORE_USER.ACTIVE,
			CORE_USER.LAST_NAME,
			CORE_USER.LOGIN
		).from(CORE_USER)
			.leftJoin(CORE_TEAM_MEMBER).on(CORE_USER.PARTY_ID.eq(CORE_TEAM_MEMBER.USER_ID))
			.where(CORE_TEAM_MEMBER.TEAM_ID.eq(teamId))
			.fetchInto(TeamMemberDto.class);
	}



	private SelectJoinStep<?> getBaseRequest() {
		return dsl.select(
			CORE_TEAM.PARTY_ID.as(RequestAliasesConstants.ID),
			CORE_TEAM.NAME,
			CORE_TEAM.DESCRIPTION,
			CORE_TEAM.CREATED_BY,
			CORE_TEAM.CREATED_ON,
			CORE_TEAM.LAST_MODIFIED_BY,
			CORE_TEAM.LAST_MODIFIED_ON
		)
			.from(CORE_TEAM);
	}
}

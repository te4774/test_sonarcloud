/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.execution;

import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.display.dto.DenormalizedCustomFieldValueDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.RequirementVersionCoverageDto;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class ExecutionView {
	private long id;
	private String name;
	private long projectId;
	private Long testCaseId;
	private int executionOrder;
	private String prerequisite;
	private String tcNatLabel;
	private String tcNatIconName;
	private String tcTypeLabel;
	private String tcTypeIconName;
	private String tcStatus;
	private String tcImportance;
	private String tcDescription;
	private String datasetLabel;
	private String comment;
	private String executionMode;
	private Date lastExecutedOn;
	private String lastExecutedBy;
	private String executionStatus;
	private List<ExecutionStepView> executionStepViews = new ArrayList<>();
	private List<DenormalizedCustomFieldValueDto> denormalizedCustomFieldValues = new ArrayList<>();
	private List<CustomFieldValueDto> customFieldValues = new ArrayList<>();
	private List<RequirementVersionCoverageDto> coverages = new ArrayList<>();
	private Long attachmentListId;
	private AttachmentListDto attachmentList;
	private String testAutomationServerKind;
	private String automatedExecutionResultUrl;
	private String automatedExecutionResultSummary;
	private URL automatedJobUrl;
	private int nbIssues;
	private long iterationId;
	private Long scriptedExecutionId;
	private Long keywordExecutionId;
	private List<MilestoneDto> milestones = new ArrayList<>();
	private Long testPlanItemId;
	private Long executionsCount;



	public ExecutionView() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getTestCaseId() {
		return testCaseId;
	}

	public void setTestCaseId(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public int getExecutionOrder() {
		return executionOrder;
	}

	public void setExecutionOrder(int executionOrder) {
		this.executionOrder = executionOrder;
	}

	public String getPrerequisite() {
		return prerequisite;
	}

	public void setPrerequisite(String prerequisite) {
		this.prerequisite = prerequisite;
	}

	public List<ExecutionStepView> getExecutionStepViews() {
		return executionStepViews;
	}

	public void setExecutionStepViews(List<ExecutionStepView> executionStepViews) {
		this.executionStepViews = executionStepViews;
	}

	public String getTcNatLabel() {
		return tcNatLabel;
	}

	public void setTcNatLabel(String tcNatLabel) {
		this.tcNatLabel = tcNatLabel;
	}

	public String getTcNatIconName() {
		return tcNatIconName;
	}

	public void setTcNatIconName(String tcNatIconName) {
		this.tcNatIconName = tcNatIconName;
	}

	public String getTcTypeLabel() {
		return tcTypeLabel;
	}

	public void setTcTypeLabel(String tcTypeLabel) {
		this.tcTypeLabel = tcTypeLabel;
	}

	public String getTcTypeIconName() {
		return tcTypeIconName;
	}

	public void setTcTypeIconName(String tcTypeIconName) {
		this.tcTypeIconName = tcTypeIconName;
	}

	public String getTcStatus() {
		return tcStatus;
	}

	public void setTcStatus(String tcStatus) {
		this.tcStatus = tcStatus;
	}

	public String getTcImportance() {
		return tcImportance;
	}

	public void setTcImportance(String tcImportance) {
		this.tcImportance = tcImportance;
	}

	public String getTcDescription() {
		return tcDescription;
	}

	public void setTcDescription(String tcDescription) {
		this.tcDescription = tcDescription;
	}

	public String getDatasetLabel() {
		return datasetLabel;
	}

	public void setDatasetLabel(String datasetLabel) {
		this.datasetLabel = datasetLabel;
	}

	public List<DenormalizedCustomFieldValueDto> getDenormalizedCustomFieldValues() {
		return denormalizedCustomFieldValues;
	}

	public void setDenormalizedCustomFieldValues(List<DenormalizedCustomFieldValueDto> denormalizedCustomFieldValues) {
		this.denormalizedCustomFieldValues = denormalizedCustomFieldValues;
	}

	public List<CustomFieldValueDto> getCustomFieldValues() {
		return customFieldValues;
	}

	public void setCustomFieldValues(List<CustomFieldValueDto> customFieldValues) {
		this.customFieldValues = customFieldValues;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public AttachmentListDto getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(AttachmentListDto attachmentList) {
		this.attachmentList = attachmentList;
	}

	public Long getAttachmentListId() {
		return attachmentListId;
	}

	public void setAttachmentListId(Long attachmentListId) {
		this.attachmentListId = attachmentListId;
	}

	public List<RequirementVersionCoverageDto> getCoverages() {
		return coverages;
	}

	public void setCoverages(List<RequirementVersionCoverageDto> coverages) {
		this.coverages = coverages;
	}

	public String getExecutionMode() {
		return executionMode;
	}

	public void setExecutionMode(String executionMode) {
		this.executionMode = executionMode;
	}

	public Date getLastExecutedOn() {
		return lastExecutedOn;
	}

	public void setLastExecutedOn(Date lastExecutedOn) {
		this.lastExecutedOn = lastExecutedOn;
	}

	public String getLastExecutedBy() {
		return lastExecutedBy;
	}

	public void setLastExecutedBy(String lastExecutedBy) {
		this.lastExecutedBy = lastExecutedBy;
	}

	public String getExecutionStatus() {
		return executionStatus;
	}

	public void setExecutionStatus(String executionStatus) {
		this.executionStatus = executionStatus;
	}

	public String getAutomatedExecutionResultUrl() {
		return automatedExecutionResultUrl;
	}

	public void setAutomatedExecutionResultUrl(String automatedExecutionResultUrl) {
		this.automatedExecutionResultUrl = automatedExecutionResultUrl;
	}

	public String getAutomatedExecutionResultSummary() {
		return automatedExecutionResultSummary;
	}

	public void setAutomatedExecutionResultSummary(String automatedExecutionResultSummary) {
		this.automatedExecutionResultSummary = automatedExecutionResultSummary;
	}

	public URL getAutomatedJobUrl() {
		return automatedJobUrl;
	}

	public void setAutomatedJobUrl(URL automatedJobUrl) {
		this.automatedJobUrl = automatedJobUrl;
	}

	public int getNbIssues() {
		return nbIssues;
	}

	public void setNbIssues(int nbIssues) {
		this.nbIssues = nbIssues;
	}

	public long getIterationId() {
		return iterationId;
	}

	public void setIterationId(long iterationId) {
		this.iterationId = iterationId;
	}

	public void setScriptedExecutionId(Long scriptedExecutionId) {
		this.scriptedExecutionId = scriptedExecutionId;
	}

	public void setKeywordExecutionId(Long keywordExecutionId) {
		this.keywordExecutionId = keywordExecutionId;
	}

	public ExecutionKind getKind() {
		return ExecutionKind.fromLeftJoinIds(this.scriptedExecutionId, this.keywordExecutionId);
	}

	public List<MilestoneDto> getMilestones() {
		return milestones;
	}

	public void setMilestones(List<MilestoneDto> milestones) {
		this.milestones = milestones;
	}

	public String getTestAutomationServerKind() {
		return testAutomationServerKind;
	}

	public void setTestAutomationServerKind(String testAutomationServerKind) {
		this.testAutomationServerKind = testAutomationServerKind;
	}
	public Long getTestPlanItemId() {
		return testPlanItemId;
	}

	public void setTestPlanItemId(Long testPlanItemId) {
		this.testPlanItemId = testPlanItemId;
	}

	public Long getExecutionsCount() {
		return executionsCount;
	}

	public void setExecutionsCount(Long executionsCount) {
		this.executionsCount = executionsCount;
	}
}

enum ExecutionKind {
	STANDARD, GHERKIN, KEYWORD;

	public static ExecutionKind fromLeftJoinIds(Long scriptedExecutionId, Long keywordExecutionId){
		boolean isGherkin = Objects.nonNull(scriptedExecutionId);
		boolean isKeyword = Objects.nonNull(keywordExecutionId);

		if (isGherkin) {
			return ExecutionKind.GHERKIN;
		}

		if (isKeyword) {
			return ExecutionKind.KEYWORD;
		}

		return ExecutionKind.STANDARD;
	}
}

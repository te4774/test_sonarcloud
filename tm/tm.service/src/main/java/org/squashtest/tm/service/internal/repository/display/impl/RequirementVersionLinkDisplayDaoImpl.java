/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record4;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionLinkDto;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionLinkDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import java.sql.Timestamp;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK_TYPE;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;

@Repository
public class RequirementVersionLinkDisplayDaoImpl implements RequirementVersionLinkDisplayDao {

	private static final String MILESTONE_LABELS = "MILESTONE_LABELS";
	private static final String MILESTONE_MAX_DATE = "MILESTONE_MAX_DATE";
	private static final String MILESTONE_MIN_DATE = "MILESTONE_MIN_DATE";
	private static final String RES_ID = "RES_ID";
	private static final String ROLE = "ROLE";

	private final DSLContext dsl;

	public RequirementVersionLinkDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public List<RequirementVersionLinkDto> findLinksByRequirementVersionId(Long reqVersionId) {
		SelectHavingStep<?> milestoneDates = getMilestoneDates();

		return dsl.select(REQUIREMENT_VERSION.RES_ID.as(RequestAliasesConstants.ID), REQUIREMENT_VERSION.REFERENCE, REQUIREMENT_VERSION.VERSION_NUMBER,
			REQUIREMENT_VERSION.REQUIREMENT_ID, RESOURCE.NAME,
			PROJECT.NAME.as(RequestAliasesConstants.PROJECT_NAME),
			DSL.field(MILESTONE_LABELS), DSL.field(MILESTONE_MIN_DATE), DSL.field(MILESTONE_MAX_DATE),
			DSL.when(REQUIREMENT_VERSION_LINK.LINK_DIRECTION.eq(true), REQUIREMENT_VERSION_LINK_TYPE.ROLE_1).otherwise(REQUIREMENT_VERSION_LINK_TYPE.ROLE_2).as(ROLE))
			.from(REQUIREMENT_VERSION_LINK)
			.innerJoin(REQUIREMENT_VERSION_LINK_TYPE).on(REQUIREMENT_VERSION_LINK.LINK_TYPE_ID.eq(REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID))
			.innerJoin(REQUIREMENT_VERSION).on(REQUIREMENT_VERSION_LINK.RELATED_REQUIREMENT_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
			.innerJoin(RESOURCE).on(REQUIREMENT_VERSION.RES_ID.eq(RESOURCE.RES_ID))
			.innerJoin(REQUIREMENT_LIBRARY_NODE).on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
			.innerJoin(PROJECT).on(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.leftJoin(milestoneDates).on(REQUIREMENT_VERSION.RES_ID.eq(milestoneDates.field(RES_ID, Long.class)))
			.where(REQUIREMENT_VERSION_LINK.REQUIREMENT_VERSION_ID.eq(reqVersionId))
			.orderBy(REQUIREMENT_VERSION.RES_ID.as(RequestAliasesConstants.ID))
			.fetchInto(RequirementVersionLinkDto.class);
	}

	private SelectHavingStep<Record4<Long, Timestamp, Timestamp, String>> getMilestoneDates() {
		return DSL.select(
			REQUIREMENT_VERSION.RES_ID.as(RES_ID),
			DSL.min(MILESTONE.END_DATE).as(MILESTONE_MIN_DATE),
			DSL.max(MILESTONE.END_DATE).as(MILESTONE_MAX_DATE),
			DSL.listAgg(MILESTONE.LABEL, ", ").withinGroupOrderBy(MILESTONE.END_DATE.asc()).as(MILESTONE_LABELS)
		)
			.from(REQUIREMENT_VERSION)
			.innerJoin(MILESTONE_REQ_VERSION).on(REQUIREMENT_VERSION.RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
			.innerJoin(MILESTONE).on(MILESTONE.MILESTONE_ID.eq(MILESTONE_REQ_VERSION.MILESTONE_ID))
			.groupBy(
				REQUIREMENT_VERSION.RES_ID,
				MILESTONE_REQ_VERSION.REQ_VERSION_ID
			);
	}
}

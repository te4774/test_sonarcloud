/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors.libraries;

import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.TableField;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

@Component
public class CampaignLibraryCollector extends AbstractLibraryCollector implements TreeNodeCollector {

	public CampaignLibraryCollector(DSLContext dsl, CustomFieldValueDisplayDao customFieldValueDisplayDao, ActiveMilestoneHolder activeMilestoneHolder, MilestoneDisplayDao milestoneDisplayDao) {
		super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
	}

	@Override
	protected TableField<ProjectRecord, Long> getLibraryColumnInProjectTable() {
		return PROJECT.CL_ID;
	}

	@Override
	protected Field<Long> getLibraryIdColumnInContentTable() {
		return CAMPAIGN_LIBRARY_CONTENT.LIBRARY_ID;
	}

	@Override
	protected Field<Long> getContentIdColumnInContentTable() {
		return CAMPAIGN_LIBRARY_CONTENT.CONTENT_ID;
	}

	@Override
	protected Table<?> getLibraryContentTable() {
		return CAMPAIGN_LIBRARY_CONTENT;
	}

	@Override
	protected Table<?> getLibraryTable() {
		return CAMPAIGN_LIBRARY;
	}

	@Override
	protected TableField<?, Long> getLibraryPrimaryKeyColumn() {
		return CAMPAIGN_LIBRARY.CL_ID;
	}

	@Override
	public NodeType getHandledEntityType() {
		return NodeType.CAMPAIGN_LIBRARY;
	}
}

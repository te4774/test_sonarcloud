/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.local;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.Select;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.jooq.domain.tables.Requirement;
import org.squashtest.tm.service.bugtracker.knownissues.local.LocalKnownIssue;
import org.squashtest.tm.service.bugtracker.knownissues.local.RequirementVersionLocalKnownIssue;

import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.BUGTRACKER_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.tables.RequirementVersion.REQUIREMENT_VERSION;

@Repository
@Transactional(readOnly = true)
public class RequirementVersionKnownIssueFinder extends BaseLocalKnownIssueFinder<Record5<Long, Long, String, String, String>> {

	public RequirementVersionKnownIssueFinder(DSLContext dsl) {
		super(dsl);
	}

	@Override
	public int countKnownIssues(Long requirementVersionId) {
		return selectKnownIssues(requirementVersionId).fetch().size();
	}

	@Override
	protected LocalKnownIssue buildIssueFromRecord(Record5<Long, Long, String, String, String> record) {
		return new RequirementVersionLocalKnownIssue(
			record.component1(),
			record.component2(),
			record.component3(),
			LocalKnownIssueFinderHelper.parseLongsAndSortDesc(record.component4()),
			LocalKnownIssueFinderHelper.parseLongsAndSortDesc(record.component5()));
	}

	@Override
	protected SelectHavingStep<Record5<Long, Long, String, String, String>> selectKnownIssues(long requirementVersionId) {
		final List<Long> requirementVersionIds = selectVerifiedDescendantsVersionIds(requirementVersionId).fetchInto(Long.class);
		requirementVersionIds.add(requirementVersionId);

		return dsl.select(
			PROJECT.PROJECT_ID,
			ISSUE.BUGTRACKER_ID,
			ISSUE.REMOTE_ISSUE_ID,
			DSL.groupConcatDistinct(EXECUTION.EXECUTION_ID),
			DSL.groupConcatDistinct(REQUIREMENT_VERSION.RES_ID))
			.from(BaseLocalKnownIssueFinder.getIssueToBugtrackerBindingJoin()
				.innerJoin(REQUIREMENT_VERSION_COVERAGE).on(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(EXECUTION.TCLN_ID))
				.innerJoin(REQUIREMENT_VERSION).on(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID)))
			.where(REQUIREMENT_VERSION.RES_ID.in(requirementVersionIds))
			// Only shows if source project's current bugtracker is the bugtracker the issue was reported with
			.and(ISSUE.BUGTRACKER_ID.eq(BUGTRACKER_BINDING.BUGTRACKER_ID))
			.groupBy(PROJECT.PROJECT_ID, ISSUE.BUGTRACKER_ID, ISSUE.REMOTE_ISSUE_ID);
	}

	private Select<Record1<Long>> selectVerifiedDescendantsVersionIds(long requirementVersionId) {
		final Requirement requirementChild = REQUIREMENT.as("r2");

		return dsl.selectDistinct(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID)
			.from(REQUIREMENT_VERSION)
			.innerJoin(REQUIREMENT).on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
			.innerJoin(RLN_RELATIONSHIP_CLOSURE).on(REQUIREMENT.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID))
			.innerJoin(requirementChild).on(
				RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(requirementChild.RLN_ID)
					.or(REQUIREMENT.RLN_ID.equal(requirementChild.HIGH_LEVEL_REQUIREMENT_ID)))
			.innerJoin(REQUIREMENT_VERSION_COVERAGE)
			.on(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(requirementChild.CURRENT_VERSION_ID))
			.where(REQUIREMENT_VERSION.RES_ID.eq(requirementVersionId)
				.and(RLN_RELATIONSHIP_CLOSURE.DEPTH.ne((short) 0)));
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import org.jooq.DSLContext;
import org.jooq.Record6;
import org.jooq.SelectUnionStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.customfield.CustomFieldValueType;
import org.squashtest.tm.domain.customfield.MultiSelectField;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedCustomFieldValueType;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolderType;
import org.squashtest.tm.service.internal.display.dto.DenormalizedCustomFieldValueDto;
import org.squashtest.tm.service.internal.repository.display.DenormalizedCustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;

import javax.inject.Inject;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_VALUE_OPTION;

@Repository
public class DenormalizedCustomFieldValueDisplayDaoImpl implements DenormalizedCustomFieldValueDisplayDao {

	@Inject
	private DSLContext dsl;

	@Override
	public ListMultimap<Long, DenormalizedCustomFieldValueDto> findDenormalizedCustomFieldValues(DenormalizedFieldHolderType fieldHolderType, List<Long> entityIds) {
		if (Objects.nonNull(fieldHolderType) && Objects.nonNull(entityIds) && !entityIds.isEmpty()) {
			return fetchDenormalizedCustomFieldValues(fieldHolderType, entityIds);
		} else {
			return ArrayListMultimap.create();
		}
	}

	@Override
	public List<DenormalizedCustomFieldValueDto> findDenormalizedCustomFieldValues(DenormalizedFieldHolderType fieldHolderType, Long entityId) {
		ListMultimap<Long, DenormalizedCustomFieldValueDto> denormalizedCustomFieldValues = this.findDenormalizedCustomFieldValues(fieldHolderType, Collections.singletonList(entityId));
		return denormalizedCustomFieldValues.get(entityId);
	}


	private ListMultimap<Long, DenormalizedCustomFieldValueDto> fetchDenormalizedCustomFieldValues(DenormalizedFieldHolderType fieldHolderType, List<Long> entityIds) {
		ListMultimap<Long, DenormalizedCustomFieldValueDto> values = ArrayListMultimap.create();
		SelectUnionStep<Record6<Long,String, String, String, Long, String>> selectRequest = craftSelectRequest(fieldHolderType, entityIds);
		selectRequest.fetch().into(DenormalizedCustomFieldValueDto.class).forEach(denormalizedCustomFieldValueDto -> {
			// Sanitize rich text fields
			if (denormalizedCustomFieldValueDto.getFieldType().equals(CustomFieldValueType.RTF.name())) {
				final String sanitized = HTMLCleanupUtils.cleanHtml(denormalizedCustomFieldValueDto.getValue());
				denormalizedCustomFieldValueDto.setValue(sanitized);
			}

			values.put(denormalizedCustomFieldValueDto.getDenormalizedFieldHolderId(), denormalizedCustomFieldValueDto);
		});
		return values;
	}

	private SelectUnionStep<Record6<Long,String, String, String, Long, String>> craftSelectRequest(DenormalizedFieldHolderType fieldHolderType, List<Long> entityIds) {
		EnumSet<DenormalizedCustomFieldValueType> customFieldValueTypes = EnumSet.allOf(DenormalizedCustomFieldValueType.class);

		Deque<SelectUnionStep<Record6<Long,String, String, String, Long, String>>> clauses = customFieldValueTypes.stream()
			.map(type -> this.getSelectClause(fieldHolderType, type, entityIds))
			.collect(Collectors.toCollection(ArrayDeque::new));

		SelectUnionStep<Record6<Long,String, String, String, Long, String>> selectRequest = clauses.pop();
		while (clauses.peek() != null) {
			selectRequest.unionAll(clauses.pop());
		}
		return selectRequest;
	}

	private SelectUnionStep<Record6<Long,String, String, String, Long, String>> getSelectClause(DenormalizedFieldHolderType fieldHolderType, DenormalizedCustomFieldValueType type, List<Long> entityIds) {
		if (type.isMultiValue()) {
			return getMultiValueClause(fieldHolderType, type, entityIds);
		} else {
			return getSingleValueClause(fieldHolderType, type, entityIds);
		}
	}

	private SelectUnionStep<Record6<Long,String, String, String, Long, String>> getMultiValueClause(DenormalizedFieldHolderType fieldHolderType, DenormalizedCustomFieldValueType type, List<Long> entityIds) {
		Table customFieldValueOption = dsl.select(DENORMALIZED_FIELD_VALUE_OPTION.DFV_ID, DENORMALIZED_FIELD_VALUE_OPTION.LABEL, DENORMALIZED_FIELD_VALUE_OPTION.POSITION)
			.from(DENORMALIZED_FIELD_VALUE_OPTION)
			.orderBy(DENORMALIZED_FIELD_VALUE_OPTION.DFV_ID, DENORMALIZED_FIELD_VALUE_OPTION.POSITION)
			.asTable("DENORMALIZED_FIELD_VALUE_OPTION");

		return dsl.select(DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_ID, DENORMALIZED_FIELD_VALUE.LABEL, DENORMALIZED_FIELD_VALUE.INPUT_TYPE,
			DSL.listAgg(type.getValueColumn(), MultiSelectField.SEPARATOR).withinGroupOrderBy(DENORMALIZED_FIELD_VALUE_OPTION.POSITION).as(RequestAliasesConstants.VALUE),
			DENORMALIZED_FIELD_VALUE.DFV_ID.as(RequestAliasesConstants.ID), DENORMALIZED_FIELD_VALUE.FIELD_TYPE)
			.from(DENORMALIZED_FIELD_VALUE)
			.leftJoin(customFieldValueOption)
			.on(DENORMALIZED_FIELD_VALUE.DFV_ID.eq(DENORMALIZED_FIELD_VALUE_OPTION.DFV_ID))
			.where(DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_TYPE.eq(fieldHolderType.name()))
			.and(DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_ID.in(entityIds))
			.and(DENORMALIZED_FIELD_VALUE.FIELD_TYPE.eq(type.getDiscriminatorValue()))
			.groupBy(DENORMALIZED_FIELD_VALUE.DFV_ID);
	}

	private SelectUnionStep<Record6<Long,String, String, String, Long, String>> getSingleValueClause(DenormalizedFieldHolderType fieldHolderType, DenormalizedCustomFieldValueType type, List<Long> entityIds) {
		return dsl.select( DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_ID, DENORMALIZED_FIELD_VALUE.LABEL, DENORMALIZED_FIELD_VALUE.INPUT_TYPE,
			type.getValueColumn().as(RequestAliasesConstants.VALUE),
			DENORMALIZED_FIELD_VALUE.DFV_ID.as(RequestAliasesConstants.ID), DENORMALIZED_FIELD_VALUE.FIELD_TYPE)
			.from(DENORMALIZED_FIELD_VALUE)
			.where(DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_TYPE.eq(fieldHolderType.name()))
			.and(DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_ID.in(entityIds))
			.and(DENORMALIZED_FIELD_VALUE.FIELD_TYPE.eq(type.getDiscriminatorValue()));
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics;

import org.squashtest.tm.domain.Level;
import org.squashtest.tm.domain.LevelComparator;

import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Utility class used to handle counting on Squash {@link Enum} keys that also implements {@link Level}.
 * A subset of the Enum can be given in constructor. In that case this class will ignore values that are not in the subset of given enum values.
 * @param <E>
 */
public class CountOnEnum<E extends Enum<E> & Level> {
	private final LinkedHashMap<E, Integer> statistics;

	/**
	 * Constructor Enum Class. All values in enum will be count
	 * @param enumClass The Enum Class
	 */
	public CountOnEnum(Class<E> enumClass) {
		this(EnumSet.allOf(requireNonNull(enumClass)));
	}

	/**
	 * Constructor with a subset of values. Values not in subset are silently ignored
	 * @param subSet the Set of accepted values
	 */
	public CountOnEnum(Set<E> subSet) {
		requireNonNull(subSet);
		this.statistics = subSet.stream()
			.sorted(LevelComparator.getInstance())
			.collect(Collectors.toMap(
				Function.identity(),
				(e) -> 0,
				(e1, e2) -> e1,
				LinkedHashMap::new));
	}

	public LinkedHashMap<E, Integer> getStatistics() {
		return statistics;
	}

	/**
	 * This overload use a subset of the initial keys. It act as a filter of retrieved keys
	 * @param subset the desired keys
	 * @return results with only the desired keys, other are discarded
	 */
	public LinkedHashMap<E, Integer> getStatistics(Set<E> subset) {
		return this.getStatistics(Function.identity(), subset);
	}

	public <T extends Enum<T> & Level> LinkedHashMap<T, Integer> getStatistics(Function<E, T> keyMapper, Class<T> outputClass) {
		return this.getStatistics(keyMapper, EnumSet.allOf(outputClass));
	}

	/**
	 * Get the statistics gathered previously, after a mapping on key is executed on results.
	 * It allow to execute a post gathering operation like converting one enum key to other enum key, aggregate several keys into one...
	 * @param keyMapper The key mapper function
	 * @param subset Output keys
	 * @param <T> Output type
	 * @return The statistics
	 */
	public <T extends Enum<T> & Level> LinkedHashMap<T, Integer> getStatistics(Function<E, T> keyMapper, Set<T> subset) {
		CountOnEnum<T> output = convert(keyMapper, subset);
		return output.statistics;
	}


	/**
	 * Keep only a subset of the initial keys. It act as a filter of retrieved keys
	 * @param subset the desired keys
	 * @return results with only the desired keys, other are discarded
	 */
	public CountOnEnum<E> keep(Set<E> subset) {
		return this.convert(Function.identity(), subset);
	}

	public  <T extends Enum<T> & Level> CountOnEnum<T> convert(Function<E, T> keyMapper, Class<T> enumClass) {
		return this.convert(keyMapper, EnumSet.allOf(enumClass));
	}

	/**
	 * Convert to a new CountOnEnum
	 * @param keyMapper The key mapper function
	 * @param subset Output keys
	 * @param <T> Output type
	 * @return The new CountOnEnum
	 */
	public  <T extends Enum<T> & Level> CountOnEnum<T> convert(Function<E, T> keyMapper, Set<T> subset) {
		CountOnEnum<T> output = new CountOnEnum<>(subset);
		statistics.forEach((key, value) -> {
			T mappedKey = keyMapper.apply(key);
			output.add(mappedKey, statistics.get(key));
		});
		return output;
	}

	public int calculateTotal() {
		return statistics.values().stream().mapToInt(Integer::intValue).sum();
	}

	public void add(E key, Long value) {
		add(key, value.intValue());
	}

	public void add(E key, Integer value) {
		// ignoring keys that are not in the subset given in constructor
		if (this.statistics.containsKey(requireNonNull(key))) {
			Integer newValue = requireNonNull(value) + this.statistics.get(requireNonNull(key));
			this.statistics.put(key, newValue);
		}
	}

	public static <E extends Enum<E> & Level> CountOnEnum<E> fromTuples(List<Object[]> tuples, Class<E> enumClass) {
		return fromTuples(tuples, enumClass, EnumSet.allOf(enumClass));
	}

	/**
	 * Generate a statistic object from a raw List of Object[], typically a result set from database.
	 *
	 * @param tuples    the tuples to converts. Each tuple must be of form : [key, value].
	 *                  Key can be either the Enum value or the String representing the value.
	 *                  Value can be either a Long or an Integer, but will be converted to Integer
	 * @param enumClass the Enum class
	 */
	public static <E extends Enum<E> & Level> CountOnEnum<E> fromTuples(List<Object[]> tuples, Class<E> enumClass, Set<E> subset) {
		CountOnEnum<E> countOnEnum = new CountOnEnum<>(subset);
		for (Object[] tuple : tuples) {
			validateTuple(tuple);
			E key = extractKey(enumClass, tuple[0]);
			Integer value = extractValue(tuple[1]);
			countOnEnum.add(key, value);
		}
		return countOnEnum;
	}

	private static void validateTuple(Object[] tuple) {
		if (Objects.isNull(tuple) || tuple.length < 2) {
			throw new IllegalArgumentException("Illegal Tuple. Null or tuples with less than two columns are rejected");
		}
	}

	private static Integer extractValue(Object obj) {
		Object valueCandidate = requireNonNull(obj);
		Integer value;
		if (Integer.class.isAssignableFrom(valueCandidate.getClass())) {
			value = (Integer) valueCandidate;
		} else if (Long.class.isAssignableFrom(valueCandidate.getClass())) {
			value = ((Long) valueCandidate).intValue();
		} else {
			throw new IllegalArgumentException(String.format("Illegal value %s of %s", valueCandidate, valueCandidate.getClass()));
		}
		return value;
	}

	private static <E extends Enum<E>> E extractKey(Class<E> enumClass, Object obj) {
		Object keyCandidate = requireNonNull(obj);
		E key;
		if (enumClass.isAssignableFrom(keyCandidate.getClass())) {
			key = (E) keyCandidate;
		} else if (String.class.isAssignableFrom(keyCandidate.getClass())) {
			key = Enum.valueOf(enumClass, (String) keyCandidate);
		} else {
			throw new IllegalArgumentException(String.format("Illegal key %s for enum %s", keyCandidate, enumClass));
		}
		return key;
	}
}

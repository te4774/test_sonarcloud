/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.bugtracker.knownissues.local;

import org.squashtest.tm.service.bugtracker.knownissues.remote.RemoteKnownIssue;

import java.util.List;
import java.util.Map;

/**
 * Holds data about an issue in TM database.
 * It's mostly used as a intermediary representation in the remote issues fetching process (see {@link RemoteKnownIssue},
 * {@link org.squashtest.tm.service.display.knownissue.TestCaseKnownIssueFinder},...)
 */
public class LocalKnownIssue {
	public final Long projectId;
	public final Long bugtrackerId;
	public final String remoteIssueId;
	public final List<Long> executionIds;

	public LocalKnownIssue(Long projectId,
						   Long bugtrackerId,
						   String remoteIssueId,
						   List<Long> executionIds) {
		this.projectId = projectId;
		this.bugtrackerId = bugtrackerId;
		this.remoteIssueId = remoteIssueId;
		this.executionIds = executionIds;
	}

	public void accept(LocalKnownIssueVisitor visitor, Map<String, Object> dataRow) {}
}

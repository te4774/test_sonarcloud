/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record3;
import org.jooq.SelectUnionStep;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.jooq.domain.tables.records.AclClassRecord;
import org.squashtest.tm.service.internal.dto.PermissionWithMask;
import org.squashtest.tm.service.internal.repository.display.AclDisplayDao;
import org.squashtest.tm.service.security.acls.model.AclClass;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.ACL_CLASS;
import static org.squashtest.tm.jooq.domain.Tables.ACL_GROUP_PERMISSION;
import static org.squashtest.tm.jooq.domain.Tables.ACL_OBJECT_IDENTITY;
import static org.squashtest.tm.jooq.domain.Tables.ACL_RESPONSIBILITY_SCOPE_ENTRY;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.service.internal.repository.display.impl.AclDisplayDaoImpl.PermissionCollector.permissionCollector;

@Repository
public class AclDisplayDaoImpl implements AclDisplayDao, InitializingBean {

	private DSLContext dsl;

	//Fetched @InitializingBean
	private List<AclClassReference> aclClass;

	public AclDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public Map<Long, Multimap<String, String>> findPermissions(List<Long> projectIds, List<Long> partyIds) {
		return craftRequest(projectIds, partyIds)
			.fetch()
			.stream()
			.collect(permissionCollector(projectIds));
	}

	private SelectUnionStep<Record3<Long, Integer, String>> craftRequest(List<Long> projectIds, List<Long> partyIds) {
		ArrayDeque<SelectUnionStep<Record3<Long, Integer, String>>> clauses = aclClass.stream()
			.map(aclClass -> craftSelectClause(projectIds, partyIds, aclClass))
			.collect(Collectors.toCollection(ArrayDeque::new));

		SelectUnionStep<Record3<Long, Integer, String>> request = clauses.pop();
		while (Objects.nonNull(clauses.peek())) {
			request.unionAll(clauses.pop());
		}
		return request;
	}

	public SelectUnionStep<Record3<Long, Integer, String>> craftSelectClause(List<Long> projectIds, List<Long> partyIds, AclClassReference aclClass) {
		return dsl.
			selectDistinct(PROJECT.PROJECT_ID, ACL_GROUP_PERMISSION.PERMISSION_MASK, DSL.val(aclClass.getName()).as("ACL_CLASS"))
			.from(PROJECT)
			.innerJoin(ACL_OBJECT_IDENTITY).on(ACL_OBJECT_IDENTITY.IDENTITY.eq(aclClass.getIdentityColumn()))
			.innerJoin(ACL_RESPONSIBILITY_SCOPE_ENTRY).on(ACL_RESPONSIBILITY_SCOPE_ENTRY.OBJECT_IDENTITY_ID.eq(ACL_OBJECT_IDENTITY.ID))
			.innerJoin(ACL_GROUP_PERMISSION).on(ACL_GROUP_PERMISSION.ACL_GROUP_ID.eq(ACL_RESPONSIBILITY_SCOPE_ENTRY.ACL_GROUP_ID))
			.where(
				PROJECT.PROJECT_ID.in(projectIds)
					.and(ACL_RESPONSIBILITY_SCOPE_ENTRY.PARTY_ID.in(partyIds))
					.and(ACL_OBJECT_IDENTITY.CLASS_ID.eq(aclClass.getId())
						.and(ACL_GROUP_PERMISSION.CLASS_ID.eq(aclClass.getId())))
			)
			.orderBy(PROJECT.PROJECT_ID, ACL_GROUP_PERMISSION.PERMISSION_MASK);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		aclClass = dsl.select().from(ACL_CLASS).fetch().into(AclClassReference.class);
		aclClass.forEach(aclClassReference -> {
			String classname = aclClassReference.getClassname();
			AclClass aclClass = AclClass.findByClassName(classname);
			aclClassReference.setAclClass(aclClass);
		});
	}

	static class PermissionCollector implements Collector<Record3<Long, Integer, String>, Map<Long, Multimap<String, String>>, Map<Long, Multimap<String, String>>> {

		private final Map<Integer, PermissionWithMask> permissionByMask = EnumSet.allOf(PermissionWithMask.class).stream().collect(Collectors.toMap(PermissionWithMask::getMask, Function.identity()));

		private List<Long> expectedProjects;

		public PermissionCollector(List<Long> projectIds) {
			this.expectedProjects = projectIds;
		}

		@Override
		public Supplier<Map<Long, Multimap<String, String>>> supplier() {
			return () -> expectedProjects.stream().collect(Collectors.toMap(Function.identity(), (id) -> ArrayListMultimap.<String, String>create()));
		}

		@Override
		public BiConsumer<Map<Long, Multimap<String, String>>, Record3<Long, Integer, String>> accumulator() {
			return (permissionMap, record) -> {
				Multimap<String, String> projectPermissions = permissionMap.get(record.get(PROJECT.PROJECT_ID));
				String aclClassName = record.get("ACL_CLASS", String.class);
				Integer mask = record.get(ACL_GROUP_PERMISSION.PERMISSION_MASK);
				PermissionWithMask permission = permissionByMask.get(mask);
				if (Objects.isNull(permission)) {
					throw new IllegalArgumentException("Unknown permission mask" + mask);
				}
				projectPermissions.put(aclClassName, permission.name());
			};
		}

		@Override
		public BinaryOperator<Map<Long, Multimap<String, String>>> combiner() {
			return (longProjectPermissionsMap, longProjectPermissionsMap2) -> {
				throw new UnsupportedOperationException();
			};
		}

		@Override
		public Function<Map<Long, Multimap<String, String>>, Map<Long, Multimap<String, String>>> finisher() {
			return Function.identity();
		}

		@Override
		public Set<Characteristics> characteristics() {
			return Collections.singleton(Characteristics.UNORDERED);
		}

		public static PermissionCollector permissionCollector(List<Long> projectsIds) {
			return new PermissionCollector(projectsIds);
		}
	}

	private static class AclClassReference extends AclClassRecord {

		private AclClass aclClass;

		public void setAclClass(AclClass aclClass) {
			this.aclClass = aclClass;
		}

		public Field<Long> getIdentityColumn() {
			return aclClass.getIdentityColumn();
		}

		public String getSimpleClassName() {
			return aclClass.getSimpleClassName();
		}

		public String getName() {
			return aclClass.name();
		}
	}
}


/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.filters;

import org.jooq.Condition;
import org.jooq.Field;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class BetweenConditionBuilder<T> implements GridFilterConditionBuilder {

	private final Field<T> field;
	private final GridFilterValue gridFilterValue;

	BetweenConditionBuilder(Field<T> field, GridFilterValue gridFilterValue) {
		requireNonNull(field);
		requireNonNull(gridFilterValue);
		List<String> values = gridFilterValue.getValues();
		if (values.isEmpty() || Objects.isNull(values.get(0))) {
			throw new IllegalArgumentException("At least one non null value is required to build a like condition");
		}
		this.field = field;
		this.gridFilterValue = gridFilterValue;
	}

	@Override
	public Condition build() {
		List<String> values = gridFilterValue.getValues();
		if(values.size() < 2){
			throw new IllegalArgumentException(" 2 values are needed for a BETWEEN operation");
		}
		LocalDate date1 = LocalDate.parse(values.get(0));
		LocalDate date2 = LocalDate.parse(values.get(1));

		/**
		 * LocalDate.parse creates a new Date set at 00:00:00 from a Timestamp without HH:mm:ss
		 * So we have to add an entire day to date2 in order to include the end date in the time slot
		 */
		date2 = date2.plusDays(1);
		return field.between((T)date1, (T)date2);
	}
}

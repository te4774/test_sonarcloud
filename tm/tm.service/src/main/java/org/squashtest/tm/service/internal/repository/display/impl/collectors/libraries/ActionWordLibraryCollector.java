/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl.collectors.libraries;

import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Table;
import org.jooq.TableField;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.actionword.ActionWordTreeDefinition;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeNodeCollector;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.count;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_LIBRARY;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.AWLN_RELATIONSHIP;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

@Component
public class ActionWordLibraryCollector extends AbstractLibraryCollector implements TreeNodeCollector {


	public ActionWordLibraryCollector(DSLContext dsl, CustomFieldValueDisplayDao customFieldValueDisplayDao, ActiveMilestoneHolder activeMilestoneHolder, MilestoneDisplayDao milestoneDisplayDao) {
		super(dsl, customFieldValueDisplayDao, activeMilestoneHolder, milestoneDisplayDao);
	}

	@Override
	protected Map<Long, DataRow> doCollect(List<Long> ids) {
		Map<Long, DataRow> libraryRows = dsl.select(
			// @formatter:off
			PROJECT.PROJECT_ID.as(PROJECT_ID_ALIAS), PROJECT.NAME,
			ACTION_WORD_LIBRARY_NODE.AWLN_ID,
			ACTION_WORD_LIBRARY.AWL_ID,
			count(AWLN_RELATIONSHIP.DESCENDANT_ID).as(CHILD_COUNT_ALIAS))
			.from(ACTION_WORD_LIBRARY_NODE)
			.innerJoin(ACTION_WORD_LIBRARY).on(ACTION_WORD_LIBRARY_NODE.AWL_ID.eq(ACTION_WORD_LIBRARY.AWL_ID))
				.and(ACTION_WORD_LIBRARY_NODE.ENTITY_TYPE.eq(ActionWordTreeDefinition.LIBRARY.name()))
			.innerJoin(PROJECT).on(PROJECT.AWL_ID.eq(ACTION_WORD_LIBRARY.AWL_ID))
			.leftJoin(AWLN_RELATIONSHIP).on(AWLN_RELATIONSHIP.ANCESTOR_ID.eq(ACTION_WORD_LIBRARY_NODE.AWLN_ID))
			.where(ACTION_WORD_LIBRARY_NODE.AWLN_ID.in(ids))
			.groupBy(ACTION_WORD_LIBRARY_NODE.AWLN_ID, ACTION_WORD_LIBRARY.AWL_ID, PROJECT.PROJECT_ID, AWLN_RELATIONSHIP.ANCESTOR_ID)
			// @formatter:on
			.fetch().stream()
			.collect(Collectors.toMap(
				tuple -> tuple.get(ACTION_WORD_LIBRARY_NODE.AWLN_ID),
				tuple -> {
					DataRow dataRow = new DataRow();
					// We want the node id here, not the entity id.
					dataRow.setId(new NodeReference(getHandledEntityType(), tuple.get(ACTION_WORD_LIBRARY_NODE.AWLN_ID)).toNodeId());
					dataRow.setProjectId(tuple.get(PROJECT_ID_ALIAS, Long.class));
					dataRow.setState(tuple.get(CHILD_COUNT_ALIAS, Integer.class) > 0 ? DataRow.State.closed : DataRow.State.leaf);
					dataRow.setData(tuple.intoMap());
					return dataRow;
				}
			));
		appendMilestonesByProject(libraryRows);
		return libraryRows;
	}

	@Override
	protected TableField<ProjectRecord, Long> getLibraryColumnInProjectTable() {
		return PROJECT.AWL_ID;
	}

	protected Field<Long> getLibraryIdColumnInContentTable() {
		return AWLN_RELATIONSHIP.ANCESTOR_ID;
	}

	protected Field<Long> getContentIdColumnInContentTable() {
		return AWLN_RELATIONSHIP.DESCENDANT_ID;
	}

	protected Table<?> getLibraryContentTable() {
		return AWLN_RELATIONSHIP;
	}

	protected Table<?> getLibraryTable() {
		return ACTION_WORD_LIBRARY;
	}

	protected TableField<?, Long> getLibraryPrimaryKeyColumn() {
		return ACTION_WORD_LIBRARY.AWL_ID;
	}

	@Override
	public NodeType getHandledEntityType() {
		return NodeType.ACTION_WORD_LIBRARY;
	}
}

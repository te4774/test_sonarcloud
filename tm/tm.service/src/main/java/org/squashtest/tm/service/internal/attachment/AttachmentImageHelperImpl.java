/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.attachment;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.utils.AttachmentImageHelper;

import java.io.IOException;
import java.io.InputStream;

@Service("squash.api.AttachmentImageHelper")
@Transactional
public class AttachmentImageHelperImpl implements AttachmentImageHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(AttachmentManagerServiceImpl.class);
	private final AttachmentRepository attachmentRepository;

	public AttachmentImageHelperImpl(AttachmentRepository attachmentRepository) {
		this.attachmentRepository = attachmentRepository;
	}

	@Override
	public String getImageBase64String(Long attachmentId) {
		InputStream is = null;
		String str = "";
		try {
			is = attachmentRepository.getContentStream(attachmentId);
			byte[] bit = is.readAllBytes();
			is.read(bit, 0, bit.length);
			is.close();
			str = Base64.encodeBase64String(bit);
		} catch (IOException ex) {
			LOGGER.error("Error: could not retrieve stream content for base64 image", ex);
		}
		return str;
	}
}

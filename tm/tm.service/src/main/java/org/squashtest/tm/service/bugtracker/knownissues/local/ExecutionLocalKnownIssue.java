/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.bugtracker.knownissues.local;

import java.util.List;
import java.util.Map;

public class ExecutionLocalKnownIssue extends LocalKnownIssue {
	public final List<Long> stepOrders;
	public final List<Long> issueIds;

	public ExecutionLocalKnownIssue(Long projectId,
                                    Long bugtrackerId,
                                    String remoteIssueId,
                                    List<Long> executionIds,
                                    List<Long> stepOrders,
									List<Long> issueIds) {
		super(projectId, bugtrackerId, remoteIssueId, executionIds);
		this.stepOrders = stepOrders;
		this.issueIds = issueIds;
	}

	@Override
	public void accept(LocalKnownIssueVisitor visitor, Map<String, Object> dataRow) {
		visitor.appendExecutionData(this, dataRow);
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.referential;

import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.service.documentation.DocumentationLinkProvider;
import org.squashtest.tm.service.internal.display.dto.BugTrackerReferentialDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldDto;
import org.squashtest.tm.service.internal.display.dto.GlobalConfigurationDto;
import org.squashtest.tm.service.internal.dto.DetailedUserDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AdminReferentialData {
	private DetailedUserDto user;
	private GlobalConfigurationDto globalConfiguration;
	private LicenseInformationDto licenseInformation;
	private List<CustomFieldDto> customFields = new ArrayList<>();
	private Set<TestAutomationServerKind> availableTestAutomationServerKinds;
	private boolean canManageLocalPassword;
	private List<TemplateConfigurablePluginDto> templateConfigurablePlugins;
	private List<BugTrackerReferentialDto> bugTrackers;
	private List<DocumentationLinkProvider.Link> documentationLinks;
	private String callbackUrl;

	public DetailedUserDto getUser() {
		return user;
	}

	public void setUser(DetailedUserDto user) {
		this.user = user;
	}

	public GlobalConfigurationDto getGlobalConfiguration() {
		return globalConfiguration;
	}

	public void setGlobalConfiguration(GlobalConfigurationDto globalConfiguration) {
		this.globalConfiguration = globalConfiguration;
	}

	public LicenseInformationDto getLicenseInformation() {
		return licenseInformation;
	}

	public void setLicenseInformation(LicenseInformationDto licenseInformation) {
		this.licenseInformation = licenseInformation;
	}

	public List<CustomFieldDto> getCustomFields() {
		return customFields;
	}

	public void setCustomFields(List<CustomFieldDto> customFields) {
		this.customFields = customFields;
	}

	public Set<TestAutomationServerKind> getAvailableTestAutomationServerKinds() {
		return availableTestAutomationServerKinds;
	}

	public void setAvailableTestAutomationServerKinds(Set<TestAutomationServerKind> availableTestAutomationServerKinds) {
		this.availableTestAutomationServerKinds = availableTestAutomationServerKinds;
	}

	public boolean isCanManageLocalPassword() {
		return canManageLocalPassword;
	}

	public void setCanManageLocalPassword(boolean canManageLocalPassword) {
		this.canManageLocalPassword = canManageLocalPassword;
	}

	public List<TemplateConfigurablePluginDto> getTemplateConfigurablePlugins() {
		return templateConfigurablePlugins;
	}

	public void setTemplateConfigurablePlugins(List<TemplateConfigurablePluginDto> templateConfigurablePlugins) {
		this.templateConfigurablePlugins = templateConfigurablePlugins;
	}

	public List<BugTrackerReferentialDto> getBugTrackers() {
		return bugTrackers;
	}

	public void setBugTrackers(List<BugTrackerReferentialDto> bugTrackers) {
		this.bugTrackers = bugTrackers;
	}

	public List<DocumentationLinkProvider.Link> getDocumentationLinks() {
		return documentationLinks;
	}

	public void setDocumentationLinks(List<DocumentationLinkProvider.Link> documentationLinks) {
		this.documentationLinks = documentationLinks;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.testcase;

public class ParameterDto {

	private Long id;
	private String name;
	private String description;
	private Long sourceTestCaseId;
	private String sourceTestCaseReference;
	private String sourceTestCaseName;
	private String sourceTestCaseProjectName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getSourceTestCaseId() {
		return sourceTestCaseId;
	}

	public void setSourceTestCaseId(Long sourceTestCaseId) {
		this.sourceTestCaseId = sourceTestCaseId;
	}

	public String getSourceTestCaseName() {
		return sourceTestCaseName;
	}

	public void setSourceTestCaseName(String sourceTestCaseName) {
		this.sourceTestCaseName = sourceTestCaseName;
	}

	public String getSourceTestCaseProjectName() {
		return sourceTestCaseProjectName;
	}

	public void setSourceTestCaseProjectName(String sourceTestCaseProjectName) {
		this.sourceTestCaseProjectName = sourceTestCaseProjectName;
	}

	public String getSourceTestCaseReference() {
		return sourceTestCaseReference;
	}

	public void setSourceTestCaseReference(String sourceTestCaseReference) {
		this.sourceTestCaseReference = sourceTestCaseReference;
	}
}

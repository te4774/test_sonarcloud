/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface CustomActionWordDao {
	/**
	 * Find all ActionWords used in the TestCases corresponding to the AutomationRequests which ids were given
	 * then update their implementation information
	 * with their owner Project's implementation technology and the current Date.
	 * @param automationRequestIds the ids of the AutomationRequests which were transmitted
	 */
	void updateActionWordImplInfoFromAutomRequestIds(Collection<Long> automationRequestIds);
	/**
	 * Retrieves all case-insensitive duplicate action words concatenated with their project name.
	 * @return All case-insensitive duplicate action words and their associated projetc
	 * as a List of String following the pattern 'actionWord - Project'
	 * where actionWord is the lower-cased duplicate action word and project is the project in which it is located
	 */
	List<String> findAllCaseInsensitiveDuplicateActions();

	Map<Long, Long> findAllWithDuplicateTokenByProjectId(Long projectId);

	/**
	 * Retrieves all action library node id and project library node id by given project id whose token does not
	 * exist in another project and used in keyword test step from another project
	 * @return All action library node id and project library node
	 * as a Map of Long, Long
	 */
	Map<Long, Long> findAllWithoutDuplicateTokenByProjectId(Long projectId);
}

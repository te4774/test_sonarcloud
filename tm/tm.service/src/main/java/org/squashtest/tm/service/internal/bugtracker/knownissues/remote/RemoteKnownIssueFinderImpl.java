/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.remote;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.csp.core.bugtracker.core.BugTrackerRemoteException;
import org.squashtest.csp.core.bugtracker.domain.BugTracker;
import org.squashtest.tm.bugtracker.definition.RemoteIssue;
import org.squashtest.tm.core.foundation.collection.DefaultPagingAndSorting;
import org.squashtest.tm.core.foundation.collection.PagingAndSorting;
import org.squashtest.tm.core.foundation.collection.SortOrder;
import org.squashtest.tm.core.foundation.collection.Sorting;
import org.squashtest.tm.service.bugtracker.BugTrackersService;
import org.squashtest.tm.service.bugtracker.knownissues.local.LocalKnownIssue;
import org.squashtest.tm.service.bugtracker.knownissues.remote.RemoteKnownIssue;
import org.squashtest.tm.service.bugtracker.knownissues.remote.RemoteKnownIssueFinder;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.CampaignFolderKnownIssueFinder;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.CampaignKnownIssueFinder;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.ExecutionKnownIssueFinder;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.ExecutionStepKnownIssueFinder;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.ExecutionUngroupedKnownIssueFinder;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.IterationKnownIssueFinder;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.RequirementVersionKnownIssueFinder;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.TestCaseKnownIssueFinder;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.TestSuiteKnownIssueFinder;
import org.squashtest.tm.service.internal.repository.BugTrackerDao;
import org.squashtest.tm.service.servers.CredentialsProvider;
import org.squashtest.tm.service.servers.UserCredentialsCache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

/**
 * Fetch an entity's known issues.
 *
 * It's a two-step process :
 *  - fetch the local data in database (see {@link TestCaseKnownIssueFinder} and friends)
 *  - fetch the corresponding remote data with bugtracker connectors
 */
@Service
@Transactional(readOnly = true)
public class RemoteKnownIssueFinderImpl implements RemoteKnownIssueFinder {

	@Value("${squashtm.bugtracker.timeout:15}")
	private long timeout;

	private final BugTrackersService remoteBugTrackersService;
	private final CredentialsProvider credentialsProvider;
	private final BugTrackerDao bugTrackerDao;
	private final TestCaseKnownIssueFinder testCaseKnownIssueFinder;
	private final RequirementVersionKnownIssueFinder requirementVersionKnownIssueFinder;
	private final IterationKnownIssueFinder iterationKnownIssueFinder;
	private final TestSuiteKnownIssueFinder testSuiteKnownIssueFinder;
	private final CampaignKnownIssueFinder campaignKnownIssueFinder;
	private final CampaignFolderKnownIssueFinder campaignFolderKnownIssueFinder;
	private final ExecutionKnownIssueFinder executionKnownIssueFinder;
	private final ExecutionStepKnownIssueFinder executionStepKnownIssueFinder;
	private final ExecutionUngroupedKnownIssueFinder executionUngroupedKnownIssueFinder;

	public RemoteKnownIssueFinderImpl(BugTrackersService remoteBugTrackersService,
									  CredentialsProvider credentialsProvider,
									  BugTrackerDao bugTrackerDao,
									  TestCaseKnownIssueFinder testCaseKnownIssueFinder,
									  RequirementVersionKnownIssueFinder requirementVersionKnownIssueFinder,
									  IterationKnownIssueFinder iterationKnownIssueFinder,
									  TestSuiteKnownIssueFinder testSuiteKnownIssueFinder,
									  CampaignKnownIssueFinder campaignKnownIssueFinder,
									  CampaignFolderKnownIssueFinder campaignFolderKnownIssueFinder,
									  ExecutionKnownIssueFinder executionKnownIssueFinder,
									  ExecutionStepKnownIssueFinder executionStepKnownIssueFinder,
									  ExecutionUngroupedKnownIssueFinder executionUngroupedKnownIssueFinder) {
		this.remoteBugTrackersService = remoteBugTrackersService;
		this.credentialsProvider = credentialsProvider;
		this.bugTrackerDao = bugTrackerDao;
		this.testCaseKnownIssueFinder = testCaseKnownIssueFinder;
		this.requirementVersionKnownIssueFinder = requirementVersionKnownIssueFinder;
		this.iterationKnownIssueFinder = iterationKnownIssueFinder;
		this.testSuiteKnownIssueFinder = testSuiteKnownIssueFinder;
		this.campaignKnownIssueFinder = campaignKnownIssueFinder;
		this.campaignFolderKnownIssueFinder = campaignFolderKnownIssueFinder;
		this.executionKnownIssueFinder = executionKnownIssueFinder;
		this.executionStepKnownIssueFinder = executionStepKnownIssueFinder;
		this.executionUngroupedKnownIssueFinder = executionUngroupedKnownIssueFinder;
	}

	@Override
	public List<RemoteKnownIssue> findForTestCase(long testCaseId, PagingAndSorting pagingAndSorting) {
		final List<LocalKnownIssue> localKnownIssues = testCaseKnownIssueFinder.getPaginatedKnownIssues(testCaseId, pagingAndSorting);
		return findRemoteKnownIssues(localKnownIssues, pagingAndSorting);
	}

	@Override
	public int getCountForTestCase(Long testCaseId) {
		return testCaseKnownIssueFinder.countKnownIssues(testCaseId);
	}

	@Override
	public List<RemoteKnownIssue> findForRequirementVersion(long requirementVersionId, PagingAndSorting pagingAndSorting) {
		final List<LocalKnownIssue> issues = requirementVersionKnownIssueFinder.getPaginatedKnownIssues(requirementVersionId, pagingAndSorting);
		return findRemoteKnownIssues(issues, pagingAndSorting);
	}

	@Override
	public int getCountForRequirementVersion(Long requirementVersionId) {
		return requirementVersionKnownIssueFinder.countKnownIssues(requirementVersionId);
	}

	@Override
	public List<RemoteKnownIssue> findForIteration(long iterationId, PagingAndSorting pagingAndSorting) {
		final List<LocalKnownIssue> localKnownIssues = iterationKnownIssueFinder.getPaginatedKnownIssues(iterationId, pagingAndSorting);
		return findRemoteKnownIssues(localKnownIssues, pagingAndSorting);
	}

	@Override
	public int getCountForIteration(Long iterationId) {
		return iterationKnownIssueFinder.countKnownIssues(iterationId);
	}

	@Override
	public List<RemoteKnownIssue> findForTestSuite(long testSuiteId, PagingAndSorting pagingAndSorting) {
		final List<LocalKnownIssue> localKnownIssues = testSuiteKnownIssueFinder.getPaginatedKnownIssues(testSuiteId, pagingAndSorting);
		return findRemoteKnownIssues(localKnownIssues, pagingAndSorting);
	}

	@Override
	public int getCountForTestSuite(Long testSuiteId) {
		return testSuiteKnownIssueFinder.countKnownIssues(testSuiteId);
	}

	@Override
	public List<RemoteKnownIssue> findForCampaign(long campaignId, PagingAndSorting pagingAndSorting) {
		final List<LocalKnownIssue> localKnownIssues = campaignKnownIssueFinder.getPaginatedKnownIssues(campaignId, pagingAndSorting);
		return findRemoteKnownIssues(localKnownIssues, pagingAndSorting);
	}

	@Override
	public int getCountForCampaign(Long campaignId) {
		return campaignKnownIssueFinder.countKnownIssues(campaignId);
	}

	@Override
	public List<RemoteKnownIssue> findForCampaignFolder(long campaignFolderId, PagingAndSorting pagingAndSorting) {
		final List<LocalKnownIssue> localKnownIssues = campaignFolderKnownIssueFinder.getPaginatedKnownIssues(campaignFolderId, pagingAndSorting);
		return findRemoteKnownIssues(localKnownIssues, pagingAndSorting);
	}

	@Override
	public int getCountForCampaignFolder(Long campaignFolderId) {
		return campaignFolderKnownIssueFinder.countKnownIssues(campaignFolderId);
	}

	@Override
	public List<RemoteKnownIssue> findForExecution(long executionId, PagingAndSorting pagingAndSorting) {
		final List<LocalKnownIssue> localKnownIssues = executionKnownIssueFinder.getPaginatedKnownIssues(executionId, pagingAndSorting);
		return findRemoteKnownIssues(localKnownIssues, pagingAndSorting);
	}

	@Override
	public int getCountForExecution(Long executionId) {
		return executionKnownIssueFinder.countKnownIssues(executionId);
	}

	@Override
	public List<RemoteKnownIssue> findForExecutionStep(long executionStepId, PagingAndSorting pagingAndSorting) {
		final List<LocalKnownIssue> localKnownIssues = executionStepKnownIssueFinder.getPaginatedKnownIssues(executionStepId, pagingAndSorting);
		return findRemoteKnownIssues(localKnownIssues, pagingAndSorting);
	}

	@Override
	public int getCountForExecutionStep(Long executionStepId) {
		return executionStepKnownIssueFinder.countKnownIssues(executionStepId);
	}

	@Override
	public List<RemoteKnownIssue> findUngroupedForExecution(Long execId) {
		final PagingAndSorting pagingAndSorting = new DefaultPagingAndSorting("", true);
		final List<LocalKnownIssue> localKnownIssues = executionUngroupedKnownIssueFinder.getPaginatedKnownIssues(execId, pagingAndSorting);
		return findRemoteKnownIssues(localKnownIssues, pagingAndSorting);
	}

	private List<RemoteKnownIssue> findRemoteKnownIssues(List<LocalKnownIssue> localKnownIssues, Sorting sorting) {
		final Map<Long, Map<Long, List<LocalKnownIssue>>> map = getWorkMap(localKnownIssues);
		final List<RemoteKnownIssue> remoteKnownIssues = collectRemoteIssues(map);
		applySorting(sorting, remoteKnownIssues);
		return remoteKnownIssues;
	}

	private void applySorting(Sorting sorting, List<RemoteKnownIssue> remoteKnownIssues) {
		remoteKnownIssues.sort(Comparator.comparing(remoteKnownIssue -> remoteKnownIssue.remoteIssue.getId()));

		if (sorting.getSortOrder().equals(SortOrder.DESCENDING)) {
			Collections.reverse(remoteKnownIssues);
		}
	}

	private List<RemoteKnownIssue> collectRemoteIssues(Map<Long, Map<Long, List<LocalKnownIssue>>> map) {
		final List<RemoteKnownIssue> remoteIssues = new ArrayList<>();

		map.forEach((projectId, issuesByBt) -> issuesByBt.forEach((btId, localIssues) -> {
			try {
				final BugTracker bugTracker = bugTrackerDao.getOne(btId);
				final Set<String> issueIds = localIssues.stream().map(local -> local.remoteIssueId).collect(Collectors.toSet());
				final Future<List<RemoteIssue>> futureIssues = remoteBugTrackersService.getIssues(projectId, issueIds, bugTracker,
					getCredentialsCache(), getLocaleContext(), getSecurityContext());

				final List<RemoteIssue> btIssues = futureIssues.get(timeout, TimeUnit.SECONDS);

				// It would be easier to map from btIssues but that would cause ambiguous behaviours when fetching ungrouped issues (e.g. execution scenario page)
				// because of duplicate ids in localIssues. We need to map from localIssues instead.
				final List<RemoteKnownIssue> remoteKnownIssues = localIssues.stream()
					.map(localIssue -> new RemoteKnownIssue(findRemoteIssue(btIssues, localIssue.remoteIssueId), bugTracker, localIssue))
					.collect(Collectors.toList());

				remoteIssues.addAll(remoteKnownIssues);
			} catch (TimeoutException | ExecutionException | InterruptedException ex) {
				throw new BugTrackerRemoteException(ex);
			}
		}));

		return remoteIssues;
	}

	private RemoteIssue findRemoteIssue(List<RemoteIssue> btIssues, String remoteIssueId) {
		return btIssues.stream()
			.filter(btIssue -> btIssue.getId().equals(remoteIssueId))
			.findAny()
			.orElseThrow(() -> new RuntimeException("Error while collecting remote issues : cannot find remote issue with id " + remoteIssueId));
	}

	// Groups issue IDs by bugtracker and by project
	private Map<Long, Map<Long, List<LocalKnownIssue>>> getWorkMap(List<LocalKnownIssue> localKnownIssues) {
		Map<Long, Map<Long, List<LocalKnownIssue>>> map = new HashMap<>();

		localKnownIssues.forEach(issue -> {
			final Long projectId = issue.projectId;

			if (!map.containsKey(projectId)) {
				map.put(projectId, new HashMap<>());
			}

			final Long bugtrackerId = issue.bugtrackerId;

			if (!map.get(projectId).containsKey(bugtrackerId)) {
				map.get(projectId).put(bugtrackerId, new ArrayList<>());
			}

			map.get(projectId).get(bugtrackerId).add(issue);
		});

		return map;
	}

	private LocaleContext getLocaleContext() {
		return LocaleContextHolder.getLocaleContext();
	}

	private SecurityContext getSecurityContext() {
		return SecurityContextHolder.getContext();
	}

	private UserCredentialsCache getCredentialsCache() {
		return credentialsProvider.getCache();
	}
}

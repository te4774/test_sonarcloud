/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.customreport.CustomReportTreeDefinition;
import org.squashtest.tm.service.internal.display.dto.LibraryDto;
import org.squashtest.tm.service.internal.display.dto.customreports.ChartDefinitionDto;
import org.squashtest.tm.service.internal.display.dto.customreports.CustomReportFolderDto;
import org.squashtest.tm.service.internal.repository.display.CustomReportDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import static org.squashtest.tm.jooq.domain.Tables.CHART_DEFINITION;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_REPORT_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_REPORT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

@Repository
public class CustomReportDisplayDaoImpl implements CustomReportDisplayDao {

	private final DSLContext dslContext;

	public CustomReportDisplayDaoImpl(DSLContext dslContext) {
		this.dslContext = dslContext;
	}

	@Override
	public ChartDefinitionDto findChartDefinitionByNodeId(Long customReportLibraryNodeId) {
		return dslContext.select(
			CHART_DEFINITION.NAME, CHART_DEFINITION.CHART_ID.as(RequestAliasesConstants.ID), CHART_DEFINITION.PROJECT_ID
		, CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID.as(RequestAliasesConstants.CRLN_ID_ALIAS)
		)
			.from(CHART_DEFINITION)
			.innerJoin(CUSTOM_REPORT_LIBRARY_NODE).on(CHART_DEFINITION.CHART_ID.eq(CUSTOM_REPORT_LIBRARY_NODE.ENTITY_ID))
			.where(CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID.eq(customReportLibraryNodeId))
				.and(CUSTOM_REPORT_LIBRARY_NODE.ENTITY_TYPE.eq(CustomReportTreeDefinition.CHART.name()))
			.fetchOneInto(ChartDefinitionDto.class);
	}

	@Override
	public LibraryDto getCustomReportLibraryDtoById(Long id) {
		return dslContext
			.select(CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID.as(RequestAliasesConstants.ID),
					PROJECT.PROJECT_ID,
					PROJECT.DESCRIPTION,
					PROJECT.NAME)
			.from(CUSTOM_REPORT_LIBRARY_NODE)
			.innerJoin(PROJECT).on(PROJECT.CRL_ID.eq(CUSTOM_REPORT_LIBRARY_NODE.CRL_ID))
			.where(CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID.eq(id))
			.fetchOne().into(LibraryDto.class);
	}

	@Override
	public CustomReportFolderDto getCustomReportFolderDtoById(long customReportFolderId) {
		return dslContext.select(
			CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID.as(RequestAliasesConstants.ID),
			CUSTOM_REPORT_LIBRARY_NODE.NAME,
			CUSTOM_REPORT_FOLDER.DESCRIPTION,
			CUSTOM_REPORT_FOLDER.PROJECT_ID)
			.from(CUSTOM_REPORT_LIBRARY_NODE)
			.innerJoin(CUSTOM_REPORT_FOLDER).on(CUSTOM_REPORT_FOLDER.CRF_ID.eq(CUSTOM_REPORT_LIBRARY_NODE.ENTITY_ID))
			.where(CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID.eq(customReportFolderId))
			.fetchOne().into(CustomReportFolderDto.class);
	}
}

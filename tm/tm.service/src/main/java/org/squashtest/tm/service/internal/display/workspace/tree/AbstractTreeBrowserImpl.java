/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.workspace.tree;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.service.display.workspace.tree.TreeBrowser;
import org.squashtest.tm.service.display.workspace.tree.TreeNodeCollectorService;
import org.squashtest.tm.service.internal.display.dto.ProjectFilterDto;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.display.ProjectFilterDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TreeBrowserDao;
import org.squashtest.tm.service.internal.repository.display.impl.RelationshipDefinition;
import org.squashtest.tm.service.project.CustomProjectFinder;
import org.squashtest.tm.service.user.UserAccountService;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import static org.squashtest.tm.domain.NodeReference.toNodeIds;

/**
 * Service implementation responsible for browsing trees in TestCase Workspace, Requirement Workspace and Campaign Workspace.
 * It use a declarative system of {@link RelationshipDefinition} to isolate the complex and heterogeneous SquashTM data
 * modeling regarding hierarchies. Yeah all the *** stuff like LIBRARY_CONTENT, TCLN_RELATIONSHIP, CAMPAIGN_ITERATION...
 */
public abstract class AbstractTreeBrowserImpl implements TreeBrowser {

	private final TreeNodeCollectorService treeNodeCollectorService;

	private final UserAccountService userAccountService;

	private final CustomProjectFinder projectFinder;

	private final ProjectFilterDisplayDao projectFilterDao;


	public AbstractTreeBrowserImpl(TreeNodeCollectorService treeNodeCollectorService,
								   UserAccountService userAccountService,
								   CustomProjectFinder projectFinder,
								   ProjectFilterDisplayDao projectFilterDao) {
		this.treeNodeCollectorService = treeNodeCollectorService;
		this.userAccountService = userAccountService;
		this.projectFinder = projectFinder;
		this.projectFilterDao = projectFilterDao;
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTreeBrowserImpl.class);

	protected abstract TreeBrowserDao getTreeBrowserDao();

	@Override
	public TreeGridResponse getInitialTree(NodeWorkspace workspace, Set<NodeReference> openedNodeCandidates, Set<NodeReference> selectedNodes) {

		// 1 - Get the projects to read
		List<Long> readableProjectIds = getReadableProjectIds();

		// 2 - Get the library references and children reference of opened nodes
		Set<NodeReference> ancestors = getTreeBrowserDao().findAncestors(new NodeReferences(selectedNodes));
		openedNodeCandidates.addAll(ancestors);
		ListMultimap<NodeReference, NodeReference> childrenReferences = getTreeBrowserDao().findChildrenReference(openedNodeCandidates);
		Set<NodeReference> nodesToCollect = getTreeBrowserDao().findLibraryReferences(workspace, readableProjectIds);
		nodesToCollect.addAll(childrenReferences.values());
		nodesToCollect.addAll(ancestors);
		nodesToCollect.addAll(selectedNodes);
		Map<NodeReference, DataRow> rows = this.treeNodeCollectorService.collectNodes(nodesToCollect);

		// 3 Assign children ids to their parents and mark as opened
		buildNodeHierarchy(openedNodeCandidates, childrenReferences, rows);

		// 4 - Filter nodes for ACLs. No profiles have rights to read projects and not read data on all tree workspaces.
		// Automation programmers cannot see project and cannot see trees.
		// So this approximation also resolve a bug when Automation programmers could see trees...
		List<DataRow> filteredNodes = rows.values().stream()
			.filter(dataRow -> readableProjectIds.contains(dataRow.getProjectId()))
			.collect(Collectors.toList());

		// 5 construct the response
		TreeGridResponse treeGridResponse = new TreeGridResponse();
		treeGridResponse.setDataRows(filteredNodes);
		return treeGridResponse;
	}

	private void buildNodeHierarchy(Set<NodeReference> openedNodes, ListMultimap<NodeReference, NodeReference> childrenReferences, Map<NodeReference, DataRow> rows) {
		openedNodes.forEach(nodeReference -> {
			if (rows.containsKey(nodeReference)) {
				DataRow openedNode = rows.get(nodeReference);
				openedNode.setState(DataRow.State.open);
				List<NodeReference> childrenRef = childrenReferences.get(nodeReference);
				openedNode.setChildren(toNodeIds(childrenRef));
				childrenRef.forEach(childRef -> rows.get(childRef).setParentRowId(nodeReference.toNodeId()));
			}
		});
	}

	@Override
	public TreeGridResponse findSubHierarchy(Set<NodeReference> rootNodes, Set<NodeReference> openedNodeCandidates) {

		// 1 - Get children reference of opened nodes
		ListMultimap<NodeReference, NodeReference> nodesInHierarchy = this.findHierarchy(rootNodes, openedNodeCandidates);

		Set<NodeReference> nodesToCollect = new HashSet<>(nodesInHierarchy.values());
		nodesToCollect.addAll(rootNodes);

		Map<NodeReference, DataRow> rows = this.treeNodeCollectorService.collectNodes(new HashSet<>(nodesToCollect));

		// 2 Assign children ids to their parents and mark parents as opened
		buildNodeHierarchy(nodesInHierarchy.keySet(), nodesInHierarchy, rows);

		// 3 - Filter nodes for ACLs. No profiles have rights to read projects and not read data on workspaces.
		List<DataRow> filteredNodes = filterDataRowByACLAndProjectFilter(rows);

		TreeGridResponse treeGridResponse = new TreeGridResponse();
		treeGridResponse.setDataRows(filteredNodes);
		return treeGridResponse;
	}

	private List<DataRow> filterDataRowByACLAndProjectFilter(Map<NodeReference, DataRow> rows) {
		List<Long> readableProjectIds = getReadableProjectIds();
		return rows.values().stream()
			.filter(dataRow -> readableProjectIds.contains(dataRow.getProjectId()))
			.collect(Collectors.toList());
	}

	private List<Long> getReadableProjectIds() {
		// 1 - Get the projects to read
		UserDto currentUser = userAccountService.findCurrentUserDto();
		List<Long> readableProjectIds = projectFinder.findAllReadableIds(currentUser);
		ProjectFilterDto filter = projectFilterDao.getProjectFilterByUserLogin(currentUser.getUsername());
		if (Objects.nonNull(filter) && filter.getActivated()) {
			readableProjectIds.retainAll(projectFilterDao.getProjectIdsByProjectFilter(filter.getId()));
		}
		return readableProjectIds;
	}

	private ListMultimap<NodeReference, NodeReference> findHierarchy(Set<NodeReference> rootNodes, Set<NodeReference> openedNodeCandidates) {
		ListMultimap<NodeReference, NodeReference> childrenReferences = getTreeBrowserDao().findChildrenReference(openedNodeCandidates);
		Set<NodeReference> nodesInHierarchy = findNodeReferenceInHierarchy(rootNodes, childrenReferences);
		ListMultimap<NodeReference, NodeReference> multimap = ArrayListMultimap.create();
		nodesInHierarchy.forEach(nodeReference -> multimap.putAll(nodeReference, childrenReferences.get(nodeReference)));
		return multimap;
	}

	private Set<NodeReference> findNodeReferenceInHierarchy(Set<NodeReference> rootNodes, ListMultimap<NodeReference, NodeReference> hierarchy) {
		Stack<NodeReference> parents = new Stack<>();
		parents.addAll(rootNodes);
		Set<NodeReference> nodesInHierarchy = new HashSet<>();
		while (!parents.empty()) {
			NodeReference parent = parents.pop();
			nodesInHierarchy.add(parent);
			if (hierarchy.containsKey(parent)) {
				parents.addAll(hierarchy.get(parent));
			}
		}
		return nodesInHierarchy;
	}

}


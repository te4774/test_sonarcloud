/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.execution;

import org.jooq.DSLContext;
import org.jooq.Record3;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permission;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolderType;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.exception.execution.ExecutionStepHasNoModifiableStepException;
import org.squashtest.tm.exception.execution.ModifDuringExecMissingReadPermissionException;
import org.squashtest.tm.exception.execution.ModifDuringExecMissingWritePermissionException;
import org.squashtest.tm.service.display.execution.ExecutionDisplayService;
import org.squashtest.tm.service.execution.ExecutionModificationService;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.ExecutionKnownIssueFinder;
import org.squashtest.tm.service.internal.display.dto.RequirementVersionCoverageDto;
import org.squashtest.tm.service.internal.display.dto.execution.ActionStepExecView;
import org.squashtest.tm.service.internal.display.dto.execution.ExecutionView;
import org.squashtest.tm.service.internal.display.dto.execution.ModificationDuringExecutionView;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.testcase.TestCaseExecutionGrid;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.DenormalizedCustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ExecutionDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ExecutionStepDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionCoverageDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestStepDisplayDao;
import org.squashtest.tm.service.internal.testcase.TestCaseCallTreeFinder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.testautomation.TestAutomationProjectFinderService;

import javax.persistence.EntityManager;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static java.util.Objects.nonNull;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.service.security.Authorizations.READ_EXECUTION_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_TC_OR_ROLE_ADMIN;

@Service
@Transactional(readOnly = true)
public class ExecutionDisplayServiceImpl implements ExecutionDisplayService {

	private final ExecutionDisplayDao executionDisplayDao;
	private final ExecutionStepDisplayDao executionStepDisplayDao;
	private final DenormalizedCustomFieldValueDisplayDao denormalizedCustomFieldValueDisplayDao;
	private final CustomFieldValueDisplayDao customFieldValueDisplayDao;
	private final AttachmentDisplayDao attachmentDisplayDao;
	private final RequirementVersionCoverageDisplayDao requirementVersionCoverageDisplayDao;
	private final TestCaseCallTreeFinder testCaseCallTreeFinder;
	private final DSLContext dslContext;
	private final ExecutionModificationService executionModificationService;
	private final TestAutomationProjectFinderService testAutomationProjectFinderService;
	private final ExecutionKnownIssueFinder executionKnownIssueFinder;
	private final MilestoneDisplayDao milestoneDisplayDao;
	private final TestStepDisplayDao testStepDisplayDao;
	private final PermissionEvaluationService permissionEvaluationService;
	private final EntityManager entityManager;

	public ExecutionDisplayServiceImpl(ExecutionDisplayDao executionDisplayDao,
									   ExecutionStepDisplayDao executionStepDisplayDao,
									   DenormalizedCustomFieldValueDisplayDao denormalizedCustomFieldValueDisplayDao,
									   CustomFieldValueDisplayDao customFieldValueDisplayDao,
									   AttachmentDisplayDao attachmentDisplayDao,
									   RequirementVersionCoverageDisplayDao requirementVersionCoverageDisplayDao,
									   TestCaseCallTreeFinder testCaseCallTreeFinder,
									   DSLContext dslContext,
									   ExecutionModificationService executionModificationService,
									   TestAutomationProjectFinderService testAutomationProjectFinderService,
									   ExecutionKnownIssueFinder executionKnownIssueFinder,
									   MilestoneDisplayDao milestoneDisplayDao,
									   TestStepDisplayDao testStepDisplayDao,
									   PermissionEvaluationService permissionEvaluationService,
									   EntityManager entityManager) {
		this.executionDisplayDao = executionDisplayDao;
		this.executionStepDisplayDao = executionStepDisplayDao;
		this.denormalizedCustomFieldValueDisplayDao = denormalizedCustomFieldValueDisplayDao;
		this.customFieldValueDisplayDao = customFieldValueDisplayDao;
		this.attachmentDisplayDao = attachmentDisplayDao;
		this.requirementVersionCoverageDisplayDao = requirementVersionCoverageDisplayDao;
		this.testCaseCallTreeFinder = testCaseCallTreeFinder;
		this.dslContext = dslContext;
		this.executionModificationService = executionModificationService;
		this.testAutomationProjectFinderService = testAutomationProjectFinderService;
		this.executionKnownIssueFinder = executionKnownIssueFinder;
		this.milestoneDisplayDao = milestoneDisplayDao;
		this.testStepDisplayDao = testStepDisplayDao;
		this.permissionEvaluationService = permissionEvaluationService;
		this.entityManager = entityManager;
	}

	@PreAuthorize(READ_TC_OR_ROLE_ADMIN)
	@Override
	public GridResponse findByTestCaseId(Long testCaseId, GridRequest request) {
		TestCaseExecutionGrid grid = new TestCaseExecutionGrid(testCaseId);
		return grid.getRows(request, dslContext);
	}

	@PreAuthorize(READ_EXECUTION_OR_ROLE_ADMIN)
	@Override
	public ExecutionView findOne(Long executionId) {
		ExecutionView executionView = executionDisplayDao.findExecutionView(executionId);
		fetchDenormalizedCustomFieldValues(executionView);
		fetchCustomFieldValues(executionView);
		fetchExecutionSteps(executionView);
		fetchAttachmentList(executionView);
		fetchCoverages(executionView);
		fetchAutomatedJobUrl(executionView);
		executionView.setNbIssues(executionKnownIssueFinder.countKnownIssues(executionId));
		executionView.setMilestones(milestoneDisplayDao.getMilestonesByExecutionId(executionId));
		return executionView;
	}

	@PreAuthorize(READ_EXECUTION_OR_ROLE_ADMIN)
	@Override
	public ModificationDuringExecutionView findOneForModificationDuringExec(Long executionId) {
		ModificationDuringExecutionView view = new ModificationDuringExecutionView();
		view.setExecutionStepActionTestStepPairs(this.executionDisplayDao.findExecutionStepActionStepPairs(executionId));
		return view;
	}

	@Override
	public ActionStepExecView findOneActionStepForModificationDuringExec(long executionId, long actionStepId) {
		ActionStepExecView actionStepExecView = this.testStepDisplayDao.findOneActionStepExecView(executionId, actionStepId);
		boolean permission = permissionEvaluationService.hasRoleOrPermissionOnObject(Roles.ROLE_ADMIN, Permission.READ.name(), actionStepExecView.getActionStepTestCaseId(), TestCase.CLASS_NAME);
		if(!permission){
			throw new AccessDeniedException("Access denied");
		}
		appendExecutionData(actionStepExecView, executionId);
		appendCoverages(actionStepExecView);
		return actionStepExecView;
	}

	@Override
	public void checkPermissionsForModificationDuringExecution(long executionId, long stepId) {
		ExecutionStep executionStep = entityManager.find(ExecutionStep.class, stepId);
		checkReferencedTestStepExist(executionStep);
		checkPermissionsOnReferencedTestCases(executionStep.getExecution().getId());
	}

	@Override
	public void checkPermissionsForModificationDuringExecutionPrologue(long executionId) {
		this.checkPermissionsOnReferencedTestCases(executionId);
	}

	private void checkReferencedTestStepExist(ExecutionStep executionStep) {
		if(Objects.isNull(executionStep.getReferencedTestStep())){
			throw new ExecutionStepHasNoModifiableStepException();
		}
	}

	private void checkPermissionsOnReferencedTestCases(Long executionId) {
		Set<Long> ids = executionDisplayDao.findAllTestCaseInExecution(executionId);
		boolean cantReadAtLeastOne = false;
		boolean canWriteAtLeastOne = false;

		// N + 1 but on called test case so it shouldn't iterate more than one or two times in real conditions.
		// PermissionEvaluationService has no batch method and creating such method would require lot of works
		for (Long id : ids) {
			Collection<String> permissions = permissionEvaluationService.permissionsOn(TestCase.CLASS_NAME, id);
			if(!permissions.contains(Permission.READ.name())){
				cantReadAtLeastOne = true;
			}
			if(permissions.contains(Permission.WRITE.name())){
				canWriteAtLeastOne = true;
			}
		}
		if(cantReadAtLeastOne){
			throw new ModifDuringExecMissingReadPermissionException();
		}

		if(!canWriteAtLeastOne){
			throw new ModifDuringExecMissingWritePermissionException();
		}
	}

	private void appendCoverages(ActionStepExecView actionStepExecView) {
		List<RequirementVersionCoverageDto> coverages = requirementVersionCoverageDisplayDao.findDirectCoverageByTestCaseId(actionStepExecView.getActionStepTestCaseId());
		actionStepExecView.setCoverages(coverages);
	}

	private void appendExecutionData(ActionStepExecView actionStepExecView, long executionId) {
		Record3<Long, String, String> record3 = this.dslContext.select(TEST_CASE_LIBRARY_NODE.TCLN_ID, TEST_CASE_LIBRARY_NODE.NAME, TEST_CASE.REFERENCE)
			.from(EXECUTION)
			.innerJoin(TEST_CASE_LIBRARY_NODE).using(EXECUTION.TCLN_ID)
			.innerJoin(TEST_CASE).using(EXECUTION.TCLN_ID)
			.where(EXECUTION.EXECUTION_ID.eq(executionId))
			.fetchOne();
		actionStepExecView.setExecutionTestCaseId(record3.get(TEST_CASE_LIBRARY_NODE.TCLN_ID));
		actionStepExecView.setExecutionTestCaseName(record3.get(TEST_CASE_LIBRARY_NODE.NAME));
		actionStepExecView.setExecutionTestCaseReference(record3.get(TEST_CASE.REFERENCE));
	}

	private void fetchAttachmentList(ExecutionView executionView) {
		Long attachmentListId = executionView.getAttachmentListId();
		executionView.setAttachmentList(this.attachmentDisplayDao.findAttachmentListById(attachmentListId));
	}

	private void fetchExecutionSteps(ExecutionView executionView) {
		long id = executionView.getId();
		executionView.setExecutionStepViews(executionStepDisplayDao.findByExecutionId(id));
	}

	private void fetchCustomFieldValues(ExecutionView executionView) {
		long id = executionView.getId();
		executionView.setCustomFieldValues(this.customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.EXECUTION, id));
	}

	private void fetchDenormalizedCustomFieldValues(ExecutionView executionView) {
		long id = executionView.getId();
		executionView.setDenormalizedCustomFieldValues(
			this.denormalizedCustomFieldValueDisplayDao.findDenormalizedCustomFieldValues(DenormalizedFieldHolderType.EXECUTION, id)
		);
	}

	private void fetchCoverages(ExecutionView executionView) {
		Long testCaseId = executionView.getTestCaseId();
		if (nonNull(testCaseId)) {
			Set<Long> calledIds = testCaseCallTreeFinder.getTestCaseCallTree(testCaseId);
			List<RequirementVersionCoverageDto> requirementVersionCoverages = requirementVersionCoverageDisplayDao.findByTestCaseIds(testCaseId, calledIds);
			executionView.setCoverages(requirementVersionCoverages);
		}
	}

	private void fetchAutomatedJobUrl(ExecutionView executionView) {
		Execution exec = executionModificationService.findById(executionView.getId());
		if (exec.isAutomated() && !exec.getAutomatedExecutionExtender().isProjectDisassociated()) {
			URL automatedJobUrl = testAutomationProjectFinderService.findProjectURL(exec.getAutomatedExecutionExtender().getAutomatedProject());
			executionView.setAutomatedJobUrl(automatedJobUrl);
		}
	}
}

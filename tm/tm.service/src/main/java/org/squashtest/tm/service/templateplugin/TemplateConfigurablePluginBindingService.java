/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.templateplugin;

import org.springframework.lang.NonNull;
import org.squashtest.tm.api.template.TemplateConfigurablePlugin;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.project.TemplateConfigurablePluginBinding;
import org.squashtest.tm.exception.templateplugin.TemplateConfigurablePluginBindingAlreadyExistsException;
import org.squashtest.tm.service.internal.display.dto.TemplateConfigurablePluginBindingDto;

import javax.validation.constraints.NotBlank;
import java.util.List;

public interface TemplateConfigurablePluginBindingService {

    /**
     * Creates a new TemplateConfigurablePluginBinding and persist it in database
     *
     * @param template the source project template
     * @param project the project to bind
     * @param plugin the TemplateConfigurablePlugin to bind
     *
     * @return the persisted TemplateConfigurablePluginBinding
     */
    TemplateConfigurablePluginBinding createTemplateConfigurablePluginBinding(@NonNull ProjectTemplate template,
                                                                              @NonNull Project project,
                                                                              @NonNull TemplateConfigurablePlugin plugin)
            throws TemplateConfigurablePluginBindingAlreadyExistsException, IllegalArgumentException;

    void removeAllForGenericProject(long genericProjectId);

    boolean hasBinding(long templateId, long projectId, @NonNull @NotBlank String pluginId);

    List<TemplateConfigurablePluginBindingDto> findAllByTemplateId(long templateId);

    List<TemplateConfigurablePluginBindingDto> findAllByTemplateIdAndPluginId(long templateId, String pluginId);

    boolean isProjectConfigurationBoundToTemplate(long projectId);

	/**
	 *
	 * This method is used in plugins
	 *
	 */
	boolean isProjectConfigurationBoundToTemplate(long projectId, String pluginId);
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.internal.api.repository.HibernateSessionClearing;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static java.util.Objects.requireNonNull;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT_LIST;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.CLN_RELATIONSHIP;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;

public class JdbcCampaignDeletionHandler extends AbstractJdbcDeletionHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(JdbcCampaignDeletionHandler.class);

	private final Set<Long> folderIds;
	private final Set<Long> campaignIds;
	private final Set<Long> allNodeIds;

	private final JdbcIterationDeletionHandler iterationDeletionHandler;

	private final EntityManager entityManager;

	public JdbcCampaignDeletionHandler(Collection<Long> folderIds,
									   Collection<Long> campaignIds,
									   DSLContext dslContext,
									   EntityManager entityManager,
									   AttachmentRepository attachmentRepository,
									   JdbcBatchReorderHelper reorderHelper,
									   JdbcIterationDeletionHandler iterationDeletionHandler,
									   String operationId
	) {
		super(dslContext, attachmentRepository, reorderHelper, operationId, entityManager);
		this.entityManager = entityManager;
		this.iterationDeletionHandler = requireNonNull(iterationDeletionHandler);
		this.folderIds = new HashSet<>(requireNonNull(folderIds));
		this.campaignIds = new HashSet<>(requireNonNull(campaignIds));
		allNodeIds = new HashSet<>(folderIds);
		allNodeIds.addAll(campaignIds);
	}

	private Condition inFolderIds(){
		return CAMPAIGN_FOLDER.CLN_ID.in(this.folderIds);
	}

	private Condition inCampaignIds(){
		return CAMPAIGN.CLN_ID.in(this.campaignIds);
	}

	private Condition inAllNodeIds(){
		return CAMPAIGN_LIBRARY_NODE.CLN_ID.in(allNodeIds);
	}

	@HibernateSessionClearing
	public OperationReport delete() {
		logStartProcess();
		clearPersistenceContext();
		deleteLinkedIterations();
		storeEntitiesToDeleteIntoWorkingTable();
		performDeleteOperations();
		cleanWorkingTable();
		logEndProcess();
		return makeOperationReport();

	}

	private void performDeleteOperations() {
		deleteCampaignTestPlanItems();
		deleteRelationships();
		deleteRootRelationships();
		deleteNodes();
		deleteCustomFieldValues();
		deleteAttachmentLists();
		deleteAttachmentContents();
	}

	private void deleteCampaignTestPlanItems() {
		workingTables.delete(CAMPAIGN.CLN_ID, CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID);
		logDelete(CAMPAIGN_TEST_PLAN_ITEM);
	}

	private void deleteNodes() {
		workingTables.delete(CAMPAIGN_FOLDER.CLN_ID, CAMPAIGN_FOLDER.CLN_ID);
		workingTables.delete(CAMPAIGN.CLN_ID, CAMPAIGN.CLN_ID);
		workingTables.delete(CAMPAIGN_LIBRARY_NODE.CLN_ID, CAMPAIGN_LIBRARY_NODE.CLN_ID);
		logDelete(CAMPAIGN_LIBRARY_NODE);
	}

	private void deleteRootRelationships() {
		Set<Long> parentIds = findParentLibraries();
		workingTables.delete(CAMPAIGN_LIBRARY_NODE.CLN_ID, CAMPAIGN_LIBRARY_CONTENT.CONTENT_ID);
		reorderHelper.reorder(parentIds, CAMPAIGN_LIBRARY_CONTENT);
		logDelete(CAMPAIGN_LIBRARY_CONTENT);
	}

	private Set<Long> findParentLibraries() {
		return dslContext.select(CAMPAIGN_LIBRARY_CONTENT.LIBRARY_ID)
			.from(CAMPAIGN_LIBRARY_CONTENT)
			.where(CAMPAIGN_LIBRARY_CONTENT.CONTENT_ID.in(allNodeIds))
			.fetchSet(CAMPAIGN_LIBRARY_CONTENT.LIBRARY_ID);
	}

	private void deleteRelationships() {
		Set<Long> parentIds = findParents();

		workingTables.delete(CAMPAIGN_LIBRARY_NODE.CLN_ID, CLN_RELATIONSHIP.ANCESTOR_ID);
		workingTables.delete(CAMPAIGN_LIBRARY_NODE.CLN_ID, CLN_RELATIONSHIP.DESCENDANT_ID);

		reorderHelper.reorder(parentIds, CLN_RELATIONSHIP);
		logDelete(CLN_RELATIONSHIP);
	}

	private Set<Long> findParents() {
		return dslContext.select(CLN_RELATIONSHIP.ANCESTOR_ID)
			.from(CLN_RELATIONSHIP)
			.where(CLN_RELATIONSHIP.DESCENDANT_ID.in(workingTables.selectIds(CAMPAIGN_LIBRARY_NODE.CLN_ID)))
			.and(CLN_RELATIONSHIP.ANCESTOR_ID.notIn(allNodeIds))
			.fetchSet(CLN_RELATIONSHIP.ANCESTOR_ID);
	}

	private void storeEntitiesToDeleteIntoWorkingTable() {
		addCampaignFolders();
		addCampaigns();
		addCampaignLibraryNodes();
		addCustomFieldValues();
		addAttachmentList();
	}

	private void addCustomFieldValues() {
		workingTables.addEntity(
			CUSTOM_FIELD_VALUE.CFV_ID,
			() -> makeSelectCustomFieldValues(CAMPAIGN_FOLDER.CLN_ID, BindableEntity.CAMPAIGN_FOLDER)
				.union(makeSelectCustomFieldValues(CAMPAIGN.CLN_ID, BindableEntity.CAMPAIGN))
		);
	}

	/**
	 * We reuse the process for deleting iterations.
	 * It's not completely optimized because some SQL request will be issued two times (ex: deleting iteration cuf, and campaign/folder cuf)
	 * However, it should be good enough, and for now I prefer to not add complexity to this already complex operation.
	 */
	private void deleteLinkedIterations() {
		iterationDeletionHandler.deleteIterations();
	}

	private void addCampaignFolders() {
		workingTables.addEntity(
			CAMPAIGN_FOLDER.CLN_ID,
			() -> makeSelectClause(CAMPAIGN_FOLDER.CLN_ID)
				.from(CAMPAIGN_FOLDER)
				.where(inFolderIds())
		);
	}

	private void addCampaigns() {
		workingTables.addEntity(
			CAMPAIGN.CLN_ID,
			() -> makeSelectClause(CAMPAIGN.CLN_ID)
				.from(CAMPAIGN)
				.where(inCampaignIds())
		);
	}

	private void addCampaignLibraryNodes() {
		workingTables.addEntity(
			CAMPAIGN_LIBRARY_NODE.CLN_ID,
			() -> makeSelectClause(CAMPAIGN_LIBRARY_NODE.CLN_ID)
				.from(CAMPAIGN_LIBRARY_NODE)
				.where(inAllNodeIds())
		);
	}

	private void addAttachmentList() {
		workingTables.addEntity(
			ATTACHMENT_LIST.ATTACHMENT_LIST_ID,
			() -> makeSelectAttachmentList(CAMPAIGN_LIBRARY_NODE.CLN_ID, CAMPAIGN_LIBRARY_NODE.ATTACHMENT_LIST_ID)
		);
	}

	private OperationReport makeOperationReport() {
		OperationReport report = new OperationReport();
		report.addRemoved(folderIds, "folder");
		report.addRemoved(campaignIds, "campaign");
		return report;
	}

	private void logStartProcess() {
		LOGGER.debug(String.format("Init deletion process of campaign folder %s, campaign %s. Operation:  %s", folderIds, campaignIds, operationId));
	}

	private void logEndProcess() {
		LOGGER.info(String.format("End deletion process of campaign folder %s, campaign %s. Time elapsed %s", folderIds, campaignIds, startDate.until(LocalDateTime.now(), ChronoUnit.MILLIS)));
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.requirement;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Record4;
import org.jooq.SelectHavingStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;

public class RequirementVersionsGrid extends AbstractGrid {

	private static final String LINKS = "LINKS";
	private static final String COVERAGES = "COVERAGES";
	private static final String RES_ID = "RES_ID";
	private static final String MILESTONE_MIN_DATE = "MILESTONE_MIN_DATE";
	private static final String MILESTONE_MAX_DATE = "MILESTONE_MAX_DATE";
	private static final String MILESTONE_LABELS = "MILESTONE_LABELS";

	private Long requirementId;

	public RequirementVersionsGrid(Long requirementId) {
		this.requirementId = requirementId;
	}

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(REQUIREMENT_LIBRARY_NODE.PROJECT_ID),
			new GridColumn(REQUIREMENT.RLN_ID),
			new GridColumn(REQUIREMENT_VERSION.RES_ID),
			new GridColumn(REQUIREMENT_VERSION.VERSION_NUMBER),
			new GridColumn(REQUIREMENT_VERSION.REFERENCE),
			new GridColumn(RESOURCE.NAME),
			new GridColumn(REQUIREMENT_VERSION.CATEGORY),
			new GridColumn(REQUIREMENT_VERSION.CRITICALITY),
			new LevelEnumColumn(RequirementStatus.class, REQUIREMENT_VERSION.REQUIREMENT_STATUS),
			new GridColumn(DSL.field(LINKS)),
			new GridColumn(DSL.field(COVERAGES)),
			new GridColumn(DSL.field(MILESTONE_MIN_DATE)),
			new GridColumn(DSL.field(MILESTONE_MAX_DATE)),
			new GridColumn(DSL.field(MILESTONE_LABELS))
		);
	}

	@Override
	protected Table<?> getTable() {
		SelectHavingStep<?> links = getRequirementLinks();
		SelectHavingStep<?> coverages = getCoverages();
		SelectHavingStep<?> milestones = getMilestoneDates();

		return REQUIREMENT
			.innerJoin(REQUIREMENT_LIBRARY_NODE).on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
			.innerJoin(REQUIREMENT_VERSION).on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
			.innerJoin(RESOURCE).on(REQUIREMENT_VERSION.RES_ID.eq(RESOURCE.RES_ID))
			.leftJoin(links).on(REQUIREMENT_VERSION.RES_ID.eq(links.field(RES_ID, Long.class)))
			.leftJoin(coverages).on(REQUIREMENT_VERSION.RES_ID.eq(coverages.field(RES_ID, Long.class)))
			.leftJoin(milestones).on(REQUIREMENT_VERSION.RES_ID.eq(milestones.field(RES_ID, Long.class)));
	}

	@Override
	protected Field<?> getIdentifier() {
		return REQUIREMENT_VERSION.RES_ID;
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return REQUIREMENT_LIBRARY_NODE.PROJECT_ID;
	}

	@Override
	protected Condition craftInvariantFilter() {
		return REQUIREMENT.RLN_ID.eq(this.requirementId);
	}

	@Override
	protected SortField<?> getDefaultOrder() {
		return REQUIREMENT_VERSION.VERSION_NUMBER.desc();
	}

	private SelectHavingStep<?> getRequirementLinks() {
		return DSL.select(REQUIREMENT_VERSION.RES_ID, DSL.count(REQUIREMENT_VERSION_LINK.RELATED_REQUIREMENT_VERSION_ID).as(LINKS))
			.from(REQUIREMENT_VERSION)
			.leftJoin(REQUIREMENT_VERSION_LINK).on(REQUIREMENT_VERSION.RES_ID.eq(REQUIREMENT_VERSION_LINK.REQUIREMENT_VERSION_ID))
			.groupBy(REQUIREMENT_VERSION.RES_ID);
	}

	private SelectHavingStep<?> getCoverages() {
		return DSL.select(REQUIREMENT_VERSION.RES_ID, DSL.count(REQUIREMENT_VERSION_COVERAGE.REQUIREMENT_VERSION_COVERAGE_ID).as(COVERAGES))
			.from(REQUIREMENT_VERSION)
			.leftJoin(REQUIREMENT_VERSION_COVERAGE).on(REQUIREMENT_VERSION.RES_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID))
			.groupBy(REQUIREMENT_VERSION.RES_ID);
	}

	private SelectHavingStep<Record4<Long, Timestamp, Timestamp, String>> getMilestoneDates() {
		// ITEM_ID | MILESTONE_LABELS | MILESTONE_MIN_DATE | MILESTONE_MAX_DATE
		return DSL.select(
			REQUIREMENT_VERSION.RES_ID,
			DSL.min(MILESTONE.END_DATE).as(MILESTONE_MIN_DATE),
			DSL.max(MILESTONE.END_DATE).as(MILESTONE_MAX_DATE),
			DSL.listAgg(MILESTONE.LABEL, ", ").withinGroupOrderBy(MILESTONE.END_DATE.asc()).as(MILESTONE_LABELS)
		)
			.from(REQUIREMENT_VERSION)
			.innerJoin(MILESTONE_REQ_VERSION)
			.on(REQUIREMENT_VERSION.RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
			.innerJoin(MILESTONE).on(MILESTONE.MILESTONE_ID.eq(MILESTONE_REQ_VERSION.MILESTONE_ID))
			.groupBy(
				REQUIREMENT_VERSION.RES_ID,
				MILESTONE_REQ_VERSION.REQ_VERSION_ID
			);
	}
}

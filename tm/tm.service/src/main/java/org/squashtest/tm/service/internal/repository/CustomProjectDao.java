/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import org.squashtest.tm.api.plugin.PluginType;
import org.squashtest.tm.domain.project.LibraryPluginBinding;
import org.squashtest.tm.domain.project.Project;

import java.math.BigInteger;
import java.util.List;


/**
 * @author Gregory Fouquet
 *
 */
public interface CustomProjectDao {
	long countNonFoldersInProject(long projectId);

	long countNonFolderInActionWord(long projectId);

	List<String> findUsersWhoCreatedTestCases(List<Long> projectIds);

	List<String> findUsersWhoModifiedTestCases(List<Long> projectIds);

	List<String> findUsersWhoCreatedRequirementVersions(List<Long> projectIds);

	List<String> findUsersWhoModifiedRequirementVersions(List<Long> projectIds);

	List<String> findUsersWhoExecutedItpi(List<Long> projectIds);

	List<String> findUsersAssignedToItpi(List<Long> projectIds);

	/**
	 * This method returns all projects IDs, excluding project templates
	 * @return list of project ids
	 */
	List<Long> findAllProjectIds();

	/**
	 * This method returns all projects IDs, including project templates
	 * @return list of project and project template ids
	 */
	List<Long> findAllProjectAndTemplateIds();

	/**
	 * Get the ids of readable {@link org.squashtest.tm.domain.project.Project} for the givens {@link org.squashtest.tm.domain.users.Party} ids
	 * Thea goal here is to have the projectIds pre fetched to avoid {@link org.springframework.security.access.prepost.PostFilter} expression.
	 * Performance testing have shown that {@link org.springframework.security.access.prepost.PostFilter} on a big collection has a HUGE performance cost
	 * notably numerous request for fetching libraries and projects, that are necessary to retrieve permissions. And project comme with additional data thus N +1 problem arise
	 * @param partyIds The ids of all concerned parties
	 * @return ths ids of {@link org.squashtest.tm.domain.project.Project} they can read. {@link org.squashtest.tm.domain.project.ProjectTemplate} are excluded.
	 */
	List<Long> findAllProjectIds(List<Long> partyIds);

	/**
	 * Find all project ids for given party ids and permission
	 * @param partyIds The ids of all concerned parties
	 * @param permissionMask The int corresponding to the permission mask
	 * @param classname The full name, with package, of the class which the permission is requested
	 * @return ths ids of {@link org.squashtest.tm.domain.project.Project} with given permission. {@link org.squashtest.tm.domain.project.ProjectTemplate} are excluded.
	 */
	List<Long> findAllProjectIdsByPermissionMaskAndClassname(List<Long> partyIds, int permissionMask, String classname);

	List<Long> findAllProjectIdsForAutomationWriter(List<Long> partyIds);

	Integer countProjectsAllowAutomationWorkflow();

	LibraryPluginBinding findPluginForProject(Long projectId, PluginType pluginType);

	void removeLibraryPluginBindingProperty(Long libraryPluginBindingId);

	BigInteger countActivePluginInProject(long projectId);

	/**
	 * Returns both projects and project templates given a list of potential project managers.
	 * @param partyIds users
	 * @return a list of project and template IDs where at least one of the provided party has a role of project
	 * manager
	 */
	List<Long> findAllManagedProjectIds(List<Long> partyIds);

	Project fetchForAutomatedExecutionCreation(long projectId);

	String findAutomationWorkflowTypeByProjectId(Long projectId);

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import com.google.common.base.CaseFormat;
import com.google.common.base.Converter;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.SelectHavingStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.display.CampaignSearchDisplayDao;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.domain.project.AutomationWorkflowType.NONE;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.jenkins;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.squashAutom;
import static org.squashtest.tm.domain.testcase.TestCaseAutomatable.Y;
import static org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus.AUTOMATED;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITEM_ID;

@Repository
public class CampaignSearchDisplayDaoImpl implements CampaignSearchDisplayDao {

	private static final String MAX_EXECUTION_ORDER = "MAX_EXECUTION_ORDER";
	private static final String ID_ALIAS = "ID";
	private static final String CAMPAIGN_NAME = "CAMPAIGN_NAME";
	private static final String ITERATION_NAME = "ITERATION_NAME";
	private static final String PROJECT_NAME = "PROJECT_NAME";
	private static final String DATASET_NAME = "DATASET_NAME";
	private static final String EXECUTION_MODE = "EXECUTION_EXECUTION_MODE";
	private static final String TEST_SUITES = "TEST_SUITES";
	private static final String UNDEFINED_MODE = "UNDEFINED";
	private static final String LABEL = "LABEL";
	private static final String LATEST_EXECUTION_ID = "LATEST_EXECUTION_ID";

	private DSLContext jooq;

	public CampaignSearchDisplayDaoImpl(DSLContext jooq) {
		this.jooq = jooq;
	}

	@Override
	public GridResponse getRows(List<Long> itpiIds) {
		GridResponse gridResponse = new GridResponse();


		jooq.select(getFields())
			.from(getTable())
			.where(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.in(itpiIds))
			.groupBy(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID,
				CAMPAIGN_LIBRARY_NODE.CLN_ID,
				ITERATION.ITERATION_ID,
				TEST_CASE_LIBRARY_NODE.TCLN_ID,
				TEST_CASE.TCLN_ID,
				DATASET.DATASET_ID,
				PROJECT.PROJECT_ID,
				DSL.field(EXECUTION_MODE))
			.stream().forEach(record -> {
			DataRow dataRow = new DataRow();
			dataRow.setId(record.get(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(ID_ALIAS)).toString());
			dataRow.setProjectId(record.get(PROJECT.PROJECT_ID));
			Map<String, Object> rawData = record.intoMap();
			Map<String, Object> data = new HashMap<>();

			// Using 'Collectors.toMap' won't work for entries with null values
			for (Map.Entry<String, Object> entry : rawData.entrySet()) {
				data.put(convertField(entry.getKey()), entry.getValue());
			}
			dataRow.setData(data);
			gridResponse.addDataRow(dataRow);
		});

		return gridResponse;
	}

	private List<Field<?>> getFields() {
		return Arrays.asList(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(ID_ALIAS),
			ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_BY,
			ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON, ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS,
			CAMPAIGN_LIBRARY_NODE.CLN_ID, CAMPAIGN_LIBRARY_NODE.NAME.as(CAMPAIGN_NAME),
			ITERATION.NAME.as(ITERATION_NAME), ITERATION.ITERATION_ID,
			TEST_CASE_LIBRARY_NODE.NAME.as(LABEL),
			TEST_CASE.IMPORTANCE, TEST_CASE.AUTOMATABLE, TEST_CASE.REFERENCE,
			getAutomationFields().field("EXECUTION_MODE").as(EXECUTION_MODE),
			DATASET.NAME.as(DATASET_NAME),
			PROJECT.PROJECT_ID, PROJECT.NAME.as(PROJECT_NAME),
			DSL.groupConcatDistinct(TEST_SUITE.NAME).as(TEST_SUITES));
	}

	private Table<?> getTable() {

		SelectHavingStep<?> getAutomationFields = getAutomationFields();

		return ITERATION_TEST_PLAN_ITEM
			.innerJoin(ITEM_TEST_PLAN_LIST).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
			.innerJoin(ITERATION).on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
			.leftJoin(TEST_SUITE_TEST_PLAN_ITEM).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
			.leftJoin(TEST_SUITE).on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
			.innerJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
			.innerJoin(PROJECT).on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.leftJoin(TEST_CASE).on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
			.leftJoin(TEST_CASE_LIBRARY_NODE).on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
			.leftJoin(getAutomationFields)
			.on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(getAutomationFields.field(ITEM_ID, Long.class)))
			.leftJoin(DATASET).on(ITERATION_TEST_PLAN_ITEM.DATASET_ID.eq(DATASET.DATASET_ID));

	}

	private String convertField(String fieldName) {
		Converter<String, String> converter = CaseFormat.UPPER_UNDERSCORE.converterTo(CaseFormat.LOWER_CAMEL);
		return converter.convert(fieldName);
	}

	private SelectHavingStep<?> getAutomationFields() {
		return DSL.select(
			ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(ITEM_ID),
			when(
				(PROJECT.AUTOMATION_WORKFLOW_TYPE.eq(NONE.name())
					.or(PROJECT.AUTOMATION_WORKFLOW_TYPE.ne(NONE.name())
						.and(TEST_CASE.AUTOMATABLE.eq(Y.name()))
						.and(AUTOMATION_REQUEST.REQUEST_STATUS.eq(AUTOMATED.name()))))
					// if Jenkins server, TestCase only has to be linked to an automation test
					.and((TEST_AUTOMATION_SERVER.KIND.eq(jenkins.name())
						.and(TEST_CASE.TA_TEST.isNotNull()))
						// if Squash Autom server, then the 3 automation attributes must exist
						.or(TEST_AUTOMATION_SERVER.KIND.eq(squashAutom.name())
							.and(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.isNotNull())
							.and(TEST_CASE.AUTOMATED_TEST_REFERENCE.isNotNull())
							.and(TEST_CASE.SCM_REPOSITORY_ID.isNotNull()))),
				TestCaseExecutionMode.AUTOMATED.name())
				.otherwise(TestCaseExecutionMode.MANUAL.name())
				.as("EXECUTION_MODE"))
			.from(ITERATION_TEST_PLAN_ITEM)
			.innerJoin(TEST_CASE).on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
			.innerJoin(TEST_CASE_LIBRARY_NODE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
			.leftJoin(AUTOMATION_REQUEST).on(AUTOMATION_REQUEST.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
			.innerJoin(PROJECT).on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
			.leftJoin(TEST_AUTOMATION_SERVER).on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(PROJECT.TA_SERVER_ID));
	}
}

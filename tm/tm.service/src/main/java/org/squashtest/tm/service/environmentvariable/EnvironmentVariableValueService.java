/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.environmentvariable;

import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding;

import java.util.Collection;
import java.util.List;

public interface EnvironmentVariableValueService {


	void removeEnvironmentVariableValuesByBindingIds(Collection<Long> bindingIds);

    void editEnvironmentVariableValueFromServer(Long testAutomationServerId, Long environmentVariableId,
												String value);

	void createValueFromServer(EnvironmentVariableBinding binding, Long testAutomationServerId);

	void editEnvironmentVariableValueFromProject(Long projectId, Long environmentVariableId, String value);

	String resetDefaultValue(Long projectId, Long environmentVariableId, String entityType);

	void removeEnvironmentVariableValuesByProjectId(Long projectId);
	void reinitializeEnvironmentVariableValuesByValueAndEvId(List<String> values, Long evId);
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import org.jooq.Field;
import org.jooq.Record5;
import org.jooq.SelectSelectStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.tables.AutomatedExecutionExtender.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.tables.AutomatedSuite.AUTOMATED_SUITE;

public class TestSuiteAutomatedTestSuiteGrid extends AbstractGrid {

	private static final String CREATED_ON = "CREATED_ON";
	private static final String CREATED_BY = "CREATED_BY";
	private static final String EXECUTION_STATUS = "EXECUTION_STATUS";
	private static final String LAST_MODIFIED_ON = "LAST_MODIFIED_ON";
	private static final String SUITE_ID = "SUITE_ID";


	private Long testSuiteId;

	public TestSuiteAutomatedTestSuiteGrid(Long testSuiteId) {
		this.testSuiteId = testSuiteId;
	}

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(DSL.field(CREATED_ON)),
			new GridColumn(DSL.field(CREATED_BY)),
			new GridColumn(DSL.field(EXECUTION_STATUS)),
			new GridColumn(DSL.field(LAST_MODIFIED_ON)),
			new GridColumn(DSL.field(SUITE_ID))
		);
	}

	@Override
	protected Table<?> getTable() {
		return getSelectFields()
			.from(AUTOMATED_SUITE)
			.innerJoin(TEST_SUITE).on(AUTOMATED_SUITE.TEST_SUITE_ID.eq(TEST_SUITE.ID))
			.where(TEST_SUITE.ID.eq(testSuiteId))
			.union(getSelectFields()
				.from(AUTOMATED_SUITE)
				.innerJoin(AUTOMATED_EXECUTION_EXTENDER).on(AUTOMATED_SUITE.SUITE_ID.eq(AUTOMATED_EXECUTION_EXTENDER.SUITE_ID))
				.innerJoin(EXECUTION).on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
				.innerJoin(ITEM_TEST_PLAN_EXECUTION).on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
				.innerJoin(ITERATION_TEST_PLAN_ITEM).on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
				.innerJoin(ITEM_TEST_PLAN_LIST).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
				.innerJoin(TEST_SUITE_TEST_PLAN_ITEM).on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
				.innerJoin(TEST_SUITE).on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
				.innerJoin(ITERATION_TEST_SUITE).on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
				.where(TEST_SUITE.ID.eq(testSuiteId))
			).asTable();
	}

	@Override
	protected Field<?> getIdentifier() {
		return DSL.field(SUITE_ID);
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return null;
	}


	private SelectSelectStep<Record5<Timestamp, String, String, Timestamp, String>> getSelectFields() {
		return DSL.select(AUTOMATED_SUITE.CREATED_ON.as(CREATED_ON),
			AUTOMATED_SUITE.CREATED_BY.as(CREATED_BY),
			AUTOMATED_SUITE.EXECUTION_STATUS.as(EXECUTION_STATUS),
			AUTOMATED_SUITE.LAST_MODIFIED_ON.as(LAST_MODIFIED_ON),
			AUTOMATED_SUITE.SUITE_ID.as(SUITE_ID));
	}
}

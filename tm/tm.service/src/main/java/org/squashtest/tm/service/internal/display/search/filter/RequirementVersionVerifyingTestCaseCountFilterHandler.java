/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search.filter;

import com.google.common.collect.Sets;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.jpa.hibernate.HibernateQuery;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.jpql.ExtendedHibernateQuery;
import org.squashtest.tm.domain.requirement.QRequirement;
import org.squashtest.tm.domain.requirement.QRequirementVersion;
import org.squashtest.tm.domain.testcase.QRequirementVersionCoverage;
import org.squashtest.tm.domain.testcase.QTestCase;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

import java.util.Set;

import static org.squashtest.tm.domain.query.QueryColumnPrototypeReference.REQUIREMENT_VERSION_TCCOUNT_WITH_INDIRECT;
import static org.squashtest.tm.domain.requirement.QRequirementVersion.requirementVersion;

/**
 * This custom filter handler builds the subquery that will retrieve the verifying test cases,
 * direct test cases and indirect ones (the verifying test cases from linked low level requirements)
 */
@Component
public class RequirementVersionVerifyingTestCaseCountFilterHandler implements FilterHandler {

    private final Set<String> handledPrototypes = Sets.newHashSet(REQUIREMENT_VERSION_TCCOUNT_WITH_INDIRECT);

    @Override
    public boolean canHandleFilter(GridFilterValue filter) {
        return this.handledPrototypes.contains(filter.getColumnPrototype());
    }

    @Override
    public void handleFilter (ExtendedHibernateQuery < ? > query, GridFilterValue filter) {
        QRequirement linkedLowLevelReq = new QRequirement("linkedLowLevelReq");
        QRequirementVersion version = new QRequirementVersion("version");
        QRequirement requirement = new QRequirement("requirement");
        QRequirementVersionCoverage coverage = new QRequirementVersionCoverage("coverage");
        QRequirementVersionCoverage directCoverage = new QRequirementVersionCoverage("directCoverage");
        QRequirementVersionCoverage indirectCoverage = new QRequirementVersionCoverage("indirectCoverage");

        HibernateQuery<Integer> subquery = new ExtendedHibernateQuery<>()
                .select(Expressions.ONE)
                .from(version)
                .join(version.requirement, requirement)
                .leftJoin(version.requirementVersionCoverages, directCoverage)
                .leftJoin(linkedLowLevelReq).on(requirement.id.eq(linkedLowLevelReq.highLevelRequirement.id))
                .leftJoin(linkedLowLevelReq.resource.requirementVersionCoverages, indirectCoverage)
                .leftJoin(coverage).on(directCoverage.verifyingTestCase.id.eq(coverage.verifyingTestCase.id)
                        .and(indirectCoverage.verifyingTestCase.id.eq(coverage.verifyingTestCase.id)))
                .where(version.id.eq(requirementVersion.id))
                .groupBy(version.id);
        appendHavingClause(subquery, directCoverage.verifyingTestCase, indirectCoverage.verifyingTestCase, coverage.verifyingTestCase, filter);

        query.where(subquery.exists());
    }

    private void appendHavingClause(HibernateQuery < Integer > subquery, QTestCase directTestCase, QTestCase indirectTestCase, QTestCase duplicatedTestCase, GridFilterValue filter) {
        int min = Integer.parseInt(filter.getValues().get(0));
        NumberExpression<Long> verifyingTestCaseCount = ((directTestCase.id.countDistinct()).add(indirectTestCase.id.countDistinct())).subtract(duplicatedTestCase.id.countDistinct());
        switch (filter.getOperation()) {
            case "BETWEEN":
                int max = Integer.parseInt(filter.getValues().get(1));
                subquery.having(verifyingTestCaseCount.between(min, max));
                break;
            case "GREATER":
                subquery.having(verifyingTestCaseCount.gt(min));
                break;
            case "GREATER_EQUAL":
                subquery.having(verifyingTestCaseCount.goe(min));
                break;
            case "LOWER":
                subquery.having(verifyingTestCaseCount.lt(min));
                break;
            case "LOWER_EQUAL":
                subquery.having(verifyingTestCaseCount.loe(min));
                break;
            case "EQUALS":
                subquery.having(verifyingTestCaseCount.eq((long) min));
                break;
            case "NOT_EQUALS":
                subquery.having(verifyingTestCaseCount.ne((long) min));
                break;
            default:
                throw new IllegalArgumentException("Unknown operation");
        }
    }
}

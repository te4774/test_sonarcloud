/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.workspace.tree;

import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;

import java.util.Set;

public interface TreeBrowser {

	TreeGridResponse getInitialTree(NodeWorkspace workspace, Set<NodeReference> openedNodes, Set<NodeReference> selectedNodes);

	/**
	 * Return a sub-hierarchy in trees.
	 * @param nodeReference The roots nodes.
	 * @param openedNodes The opened nodes. Can be root nodes or nodes in depth. This method ignore illegal nodes (nodes not existing, or not not in hierarchy)
	 * @return A {@link org.squashtest.tm.service.internal.display.grid.GridResponse} containing the nodes.
	 */
	TreeGridResponse findSubHierarchy(Set<NodeReference> nodeReference, Set<NodeReference> openedNodes);

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.automatedexecutionenvironments;

import org.springframework.lang.NonNull;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment;

import javax.annotation.Nullable;
import java.util.List;

public class EnvironmentSelectionPanelDto {
	private EnvironmentsInfo environments;
	private ServerInfo server;
	private ProjectInfo project;

	public EnvironmentSelectionPanelDto() {}

	public EnvironmentSelectionPanelDto(@NonNull EnvironmentsInfo environments,
										@NonNull ServerInfo server,
										@Nullable ProjectInfo project) {
		this.environments = environments;
		this.server = server;
		this.project = project;
	}

	public static EnvironmentSelectionPanelDto forServer(Long testAutomationServerId,
														 List<String> serverTags,
														 List<AutomatedExecutionEnvironment> allAccessibleEnvironments) {
		return new EnvironmentSelectionPanelDto(
			new EnvironmentsInfo(allAccessibleEnvironments),
			new ServerInfo(testAutomationServerId, serverTags, true),
			null
		);
	}

	public static EnvironmentSelectionPanelDto forServerWithoutCredentials(Long testAutomationServerId) {
		return new EnvironmentSelectionPanelDto(
			new EnvironmentsInfo(null),
			new ServerInfo(testAutomationServerId, null, false),
			null
		);
	}

	public static EnvironmentSelectionPanelDto forProject(Long testAutomationServerId,
														  List<String> serverTags,
														  boolean hasServerCredentials,
														  List<AutomatedExecutionEnvironment> allAccessibleEnvironments,
														  Long projectId,
														  boolean hasProjectToken,
														  List<String> projectTags,
														  boolean areProjectTagsInherited) {
		return new EnvironmentSelectionPanelDto(
			new EnvironmentsInfo(allAccessibleEnvironments),
			new ServerInfo(testAutomationServerId, serverTags, hasServerCredentials),
			new ProjectInfo(projectId, hasProjectToken, projectTags, areProjectTagsInherited)
		);
	}

	public static EnvironmentSelectionPanelDto forProjectWithoutCredentials(Long testAutomationServerId,
																			Long projectId) {
		return new EnvironmentSelectionPanelDto(
			new EnvironmentsInfo(null),
			new ServerInfo(testAutomationServerId, null, false),
			new ProjectInfo(projectId, false, null, false)
		);
	}

	public EnvironmentsInfo getEnvironments() {
		return environments;
	}

	public void setEnvironments(EnvironmentsInfo environments) {
		this.environments = environments;
	}

	public ServerInfo getServer() {
		return server;
	}

	public void setServer(ServerInfo server) {
		this.server = server;
	}

	public ProjectInfo getProject() {
		return project;
	}

	public void setProject(ProjectInfo project) {
		this.project = project;
	}
}

class EnvironmentsInfo {
	private List<AutomatedExecutionEnvironment> environments;

	public EnvironmentsInfo() {}

	public EnvironmentsInfo(List<AutomatedExecutionEnvironment> environments) {
		this.environments = environments;
	}

	public List<AutomatedExecutionEnvironment> getEnvironments() {
		return environments;
	}

	public void setEnvironments(List<AutomatedExecutionEnvironment> environments) {
		this.environments = environments;
	}
}

class ServerInfo {
	private Long testAutomationServerId;
	private List<String> defaultTags;
	private boolean hasServerCredentials;

	public ServerInfo() {}

	public ServerInfo(Long testAutomationServerId, List<String> defaultTags, boolean hasServerCredentials) {
		this.testAutomationServerId = testAutomationServerId;
		this.defaultTags = defaultTags;
		this.hasServerCredentials = hasServerCredentials;
	}

	public Long getTestAutomationServerId() {
		return testAutomationServerId;
	}

	public void setTestAutomationServerId(Long testAutomationServerId) {
		this.testAutomationServerId = testAutomationServerId;
	}

	public List<String> getDefaultTags() {
		return defaultTags;
	}

	public void setDefaultTags(List<String> defaultTags) {
		this.defaultTags = defaultTags;
	}

	public boolean isHasServerCredentials() {
		return hasServerCredentials;
	}

	public void setHasServerCredentials(boolean hasServerCredentials) {
		this.hasServerCredentials = hasServerCredentials;
	}
}

class ProjectInfo {
	private Long projectId;
	private boolean hasProjectToken;
	private List<String> projectTags;
	private boolean areProjectTagsInherited;

	public ProjectInfo() {}

	public ProjectInfo(Long projectId, boolean hasProjectToken, List<String> projectTags, boolean areProjectTagsInherited) {
		this.projectId = projectId;
		this.hasProjectToken = hasProjectToken;
		this.projectTags = projectTags;
		this.areProjectTagsInherited = areProjectTagsInherited;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public boolean isHasProjectToken() {
		return hasProjectToken;
	}

	public void setHasProjectToken(boolean hasProjectToken) {
		this.hasProjectToken = hasProjectToken;
	}

	public List<String> getProjectTags() {
		return projectTags;
	}

	public void setProjectTags(List<String> projectTags) {
		this.projectTags = projectTags;
	}

	public boolean isAreProjectTagsInherited() {
		return areProjectTagsInherited;
	}

	public void setAreProjectTagsInherited(boolean areProjectTagsInherited) {
		this.areProjectTagsInherited = areProjectTagsInherited;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import java.util.ArrayList;
import java.util.List;

public class BindMilestoneToProjectDialogData {


	private List<MilestoneDto> globalMilestones = new ArrayList<>();
	private List<MilestoneDto> personalMilestones = new ArrayList<>();
	private List<MilestoneDto> otherMilestones = new ArrayList<>();


	public List<MilestoneDto> getGlobalMilestones() {
		return globalMilestones;
	}

	public void setGlobalMilestones(List<MilestoneDto> globalMilestones) {
		this.globalMilestones = globalMilestones;
	}

	public List<MilestoneDto> getPersonalMilestones() {
		return personalMilestones;
	}

	public void setPersonalMilestones(List<MilestoneDto> personalMilestones) {
		this.personalMilestones = personalMilestones;
	}

	public List<MilestoneDto> getOtherMilestones() {
		return otherMilestones;
	}

	public void setOtherMilestones(List<MilestoneDto> otherMilestones) {
		this.otherMilestones = otherMilestones;
	}
}

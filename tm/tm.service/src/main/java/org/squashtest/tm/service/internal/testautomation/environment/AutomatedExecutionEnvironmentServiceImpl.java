/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.testautomation.environment;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.servers.Credentials;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.service.internal.repository.TestAutomationServerDao;
import org.squashtest.tm.service.internal.testautomation.TestAutomationConnectorRegistry;
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment;
import org.squashtest.tm.service.testautomation.spi.TestAutomationConnector;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AutomatedExecutionEnvironmentServiceImpl implements AutomatedExecutionEnvironmentService {

	private TestAutomationServerDao testAutomationServerDao;

	private TestAutomationConnectorRegistry testAutomationConnectorRegistry;

	@Inject
	public AutomatedExecutionEnvironmentServiceImpl(TestAutomationServerDao testAutomationServerDao, TestAutomationConnectorRegistry testAutomationConnectorRegistry) {
		this.testAutomationServerDao = testAutomationServerDao;
		this.testAutomationConnectorRegistry = testAutomationConnectorRegistry;
	}

	@Override
	public List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(Long testAutomationServerId) {
		Optional<TestAutomationServer> server = testAutomationServerDao.findById(testAutomationServerId);

		if (server.isEmpty()) {
			return Collections.emptyList();
		}

		TestAutomationConnector connector = testAutomationConnectorRegistry.getConnectorForKind(server.get().getKind());
		return connector.getAllAccessibleEnvironments(server.get());
	}

	@Override
	public List<AutomatedExecutionEnvironment> getAllAccessibleEnvironments(Long testAutomationServerId, Credentials credentials) {
		Optional<TestAutomationServer> server = testAutomationServerDao.findById(testAutomationServerId);

		if (server.isEmpty()) {
			return Collections.emptyList();
		}

		TestAutomationConnector connector = testAutomationConnectorRegistry.getConnectorForKind(server.get().getKind());
		return connector.getAllAccessibleEnvironments(server.get(), credentials);
	}

	@Override
	public boolean doesServerSupportAutomatedExecutionEnvironments(long testAutomationServerId) {
		TestAutomationServer server = testAutomationServerDao.getById(testAutomationServerId);
		TestAutomationConnector connector = testAutomationConnectorRegistry.getConnectorForKind(server.getKind());
		return connector.supportsAutomatedExecutionEnvironments();
	}
}

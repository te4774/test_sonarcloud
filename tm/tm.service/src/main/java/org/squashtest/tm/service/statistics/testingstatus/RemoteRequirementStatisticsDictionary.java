/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.testingstatus;

import com.google.common.base.Objects;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * Holds statistics about synchronized requirements. These statistics can take two forms:
 * 	- The "combined" form where each remote key is associated with one or more TM requirements, these
 * 		requirements being themself associated with their statistics data.
 * 	- The "merged" form where each remote key is associated with a single statistics bundle that merges
 * 		the rates from every linked requirement.
 *
 * The actual dictionaries key is composite : it's composed of a server ID and a remote key, assuming that
 * this is sufficient to identify a remote entity.
 */
public class RemoteRequirementStatisticsDictionary {
	public final Map<RemoteKeyAndServerId, Map<Long, RemoteRequirementStatistics>> combinedStats;
	public final Map<RemoteKeyAndServerId, RemoteRequirementStatistics> mergedStats;

	public RemoteRequirementStatisticsDictionary(Map<RemoteKeyAndServerId, Map<Long, RemoteRequirementStatistics>> combinedStats,
												 Map<RemoteKeyAndServerId, RemoteRequirementStatistics> mergedStats) {
		this.combinedStats = combinedStats;
		this.mergedStats = mergedStats;
	}

	public boolean hasStatsForServerAndRemoteKey(Long serverId, String remoteKey) {
		final RemoteKeyAndServerId key = new RemoteKeyAndServerId(remoteKey, serverId);
		return combinedStats.containsKey(key);
	}

	public Map<Long, RemoteRequirementStatistics> getStatsByRequirementId(Long serverId, String remoteKey) {
		final RemoteKeyAndServerId key = new RemoteKeyAndServerId(remoteKey, serverId);
		return combinedStats.get(key);
	}

	public Set<Long> getRequirementIdsForRemote(Long serverId, String remoteKey) {
		if (hasStatsForServerAndRemoteKey(serverId, remoteKey)) {
			return getStatsByRequirementId(serverId, remoteKey).keySet();
		} else {
			return Collections.emptySet();
		}
	}

	public RemoteRequirementStatistics getMergedStatistics(Long serverId, String remoteKey) {
		final RemoteKeyAndServerId key = new RemoteKeyAndServerId(remoteKey, serverId);
		return mergedStats.get(key);
	}

	/**
	 * Composite key to access requirement statistics.
	 */
	public static class RemoteKeyAndServerId {
		public final String remoteKey;
		public final Long serverId;

		public RemoteKeyAndServerId(String remoteKey, Long serverId) {
			this.remoteKey = remoteKey;
			this.serverId = serverId;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			RemoteKeyAndServerId that = (RemoteKeyAndServerId) o;
			return Objects.equal(remoteKey, that.remoteKey) &&
				Objects.equal(serverId, that.serverId);
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(remoteKey, serverId);
		}
	}
}

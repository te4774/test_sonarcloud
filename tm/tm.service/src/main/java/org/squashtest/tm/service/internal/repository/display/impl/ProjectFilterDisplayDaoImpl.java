/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.ProjectFilterDto;
import org.squashtest.tm.service.internal.repository.display.ProjectFilterDisplayDao;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import javax.inject.Inject;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.PROJECT_FILTER;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT_FILTER_ENTRY;

@Repository
public class ProjectFilterDisplayDaoImpl implements ProjectFilterDisplayDao {

	@Inject
	private DSLContext dsl;

	@Override
	public ProjectFilterDto getProjectFilterByUserLogin(String userLogin) {

		Record result = dsl.select(PROJECT_FILTER.PROJECT_FILTER_ID.as(RequestAliasesConstants.ID), PROJECT_FILTER.ACTIVATED,
			PROJECT_FILTER.USER_LOGIN)
			.from(PROJECT_FILTER)
			.where(PROJECT_FILTER.USER_LOGIN.eq(userLogin))
			.fetchOne();
		ProjectFilterDto projectFilterDto = null;
		if(result != null) {
			projectFilterDto = result.into(ProjectFilterDto.class);
		}
		return projectFilterDto;
	}

	@Override
	public List<Long> getProjectIdsByProjectFilter(Long projectFilterId) {
		return dsl.select(PROJECT_FILTER_ENTRY.PROJECT_ID)
			.from(PROJECT_FILTER_ENTRY)
			.where(PROJECT_FILTER_ENTRY.FILTER_ID.eq(projectFilterId))
			.fetch(PROJECT_FILTER_ENTRY.PROJECT_ID);
	}
}

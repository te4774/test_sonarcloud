/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.deletion;

import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CampaignLibraryNodesToDelete {

	private List<Long> folderIds = new ArrayList<>();
	private List<Long> campaignIds = new ArrayList<>();
	private List<Long> iterationIds = new ArrayList<>();
	private List<Long> suiteIds = new ArrayList<>();

	public CampaignLibraryNodesToDelete() {
	}

	public static CampaignLibraryNodesToDelete from(List<String> nodeRefs) {
		List<NodeReference> nodeReferences = nodeRefs.stream().map(NodeReference::fromNodeId).collect(Collectors.toList());
		CampaignLibraryNodesToDelete result = new CampaignLibraryNodesToDelete();

		List<Long> campaignId = extractNodeReferenceIds(nodeReferences, NodeType.CAMPAIGN);
		List<Long> folderId = extractNodeReferenceIds(nodeReferences, NodeType.CAMPAIGN_FOLDER);
		List<Long> iterationIds = extractNodeReferenceIds(nodeReferences, NodeType.ITERATION);
		List<Long> suiteIds = extractNodeReferenceIds(nodeReferences, NodeType.TEST_SUITE);

		result.setFolderIds(folderId);
		result.setCampaignIds(campaignId);
		result.setIterationIds(iterationIds);
		result.setSuiteIds(suiteIds);

		return result;
	}

	private static List<Long> extractNodeReferenceIds(List<NodeReference> nodeReferences, NodeType type) {
		return nodeReferences.stream()
			.filter(nodeReference -> nodeReference.getNodeType().equals(type))
			.map(NodeReference::getId)
			.collect(Collectors.toList());
	}

	public List<Long> getFolderIds() {
		return folderIds;
	}

	public void setFolderIds(List<Long> folderIds) {
		this.folderIds = folderIds;
	}

	public List<Long> getCampaignIds() {
		return campaignIds;
	}

	public void setCampaignIds(List<Long> campaignIds) {
		this.campaignIds = campaignIds;
	}

	public List<Long> getIterationIds() {
		return iterationIds;
	}

	public void setIterationIds(List<Long> iterationIds) {
		this.iterationIds = iterationIds;
	}

	public List<Long> getSuiteIds() {
		return suiteIds;
	}

	public void setSuiteIds(List<Long> suiteIds) {
		this.suiteIds = suiteIds;
	}

	public List<Long> getFolderAndCampaignNodeIds() {
		return Stream.concat(campaignIds.stream(), folderIds.stream()).collect(Collectors.toList());
	}
}

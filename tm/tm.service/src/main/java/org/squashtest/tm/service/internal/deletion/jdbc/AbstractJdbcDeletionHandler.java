/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import org.jooq.DSLContext;
import org.jooq.Param;
import org.jooq.Record3;
import org.jooq.SelectConditionStep;
import org.jooq.SelectSelectStep;
import org.jooq.Table;
import org.jooq.TableField;
import org.slf4j.Logger;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;
import org.squashtest.tm.service.internal.attachment.AttachmentRepositoryVisitor;
import org.squashtest.tm.service.internal.attachment.DatabaseAttachmentRepository;
import org.squashtest.tm.service.internal.attachment.FileSystemAttachmentRepository;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.jooq.impl.DSL.select;
import static org.jooq.impl.DSL.val;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT_LIST;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE_OPTION;
import static org.squashtest.tm.service.internal.deletion.jdbc.delegate.AbstractDelegateDeleteQuery.extractFieldTableNameAsParam;

public abstract class AbstractJdbcDeletionHandler {

	protected final DSLContext dslContext;
	protected final AttachmentRepository attachmentRepository;
	protected final JdbcBatchReorderHelper reorderHelper;
	protected final JdbcWorkingTables workingTables;

	protected final String operationId;
	protected final Param<String> operationIdParam;

	protected final LocalDateTime startDate = LocalDateTime.now();
	protected final EntityManager entityManager;
	protected LocalDateTime lastStageEnd = LocalDateTime.now();

	public AbstractJdbcDeletionHandler(DSLContext dslContext,
									   AttachmentRepository attachmentRepository,
									   JdbcBatchReorderHelper reorderHelper,
									   String operationId,
									   EntityManager entityManager) {
		this.dslContext = dslContext;
		this.attachmentRepository = attachmentRepository;
		this.reorderHelper = reorderHelper;
		this.operationId = operationId;
		operationIdParam = val(operationId);
		workingTables = new JdbcWorkingTables(dslContext, operationId);
		this.entityManager = entityManager;
	}

	protected SelectSelectStep<Record3<Long, String, String>> makeSelectClause(TableField<?, Long> tableField) {
		Param<String> fieldName = extractFieldTableNameAsParam(tableField);
		return makeSelectClause(tableField, fieldName);
	}

	protected SelectSelectStep<Record3<Long, String, String>> makeSelectClause(TableField<?, Long> tableField, Param<String> fieldName) {
		return select(tableField, fieldName, operationIdParam);
	}

	protected SelectConditionStep<Record3<Long, String, String>> makeSelectCustomFieldValues(TableField<?, Long> tableField, BindableEntity bindableEntity) {
		return makeSelectClause(CUSTOM_FIELD_VALUE.CFV_ID)
			.from(CUSTOM_FIELD_VALUE)
			.where(CUSTOM_FIELD_VALUE.BOUND_ENTITY_ID.in(workingTables.makeSelectIds(tableField)))
			.and(CUSTOM_FIELD_VALUE.BOUND_ENTITY_TYPE.eq(bindableEntity.name()));
	}

	protected SelectConditionStep<Record3<Long, String, String>> makeSelectAttachmentList(TableField<?, Long> idField, TableField<?, Long> attachmentListIdField) {
		return makeSelectClause(attachmentListIdField, val(ATTACHMENT_LIST.getName()))
			.from(idField.getTable())
			.where(idField.in(workingTables.makeSelectIds(idField)));
	}

	protected SelectConditionStep<Record3<Long, String, String>> makeSelectOrphanContent() {
		return makeSelectClause(ATTACHMENT_CONTENT.ATTACHMENT_CONTENT_ID, val(ATTACHMENT_CONTENT.getName()))
			.from(ATTACHMENT_CONTENT)
			.leftJoin(ATTACHMENT).on(ATTACHMENT.CONTENT_ID.eq(ATTACHMENT_CONTENT.ATTACHMENT_CONTENT_ID))
			.where(ATTACHMENT.CONTENT_ID.isNull());
	}

	protected void updateTime() {
		lastStageEnd = LocalDateTime.now();
	}

	protected long timeDiff() {
		return lastStageEnd.until(LocalDateTime.now(), ChronoUnit.MILLIS);
	}

	protected void logDelete(Table<?> denormalizedFieldValue) {
		getLogger().debug(String.format("Deleted root table %s and sub tables. Time on this stage : %d ms", denormalizedFieldValue.getName(), timeDiff()));
		updateTime();
	}

	protected void deleteCustomFieldValues() {
		workingTables.delete(CUSTOM_FIELD_VALUE.CFV_ID, CUSTOM_FIELD_VALUE_OPTION.CFV_ID);
		workingTables.delete(CUSTOM_FIELD_VALUE.CFV_ID, CUSTOM_FIELD_VALUE.CFV_ID);
		logDelete(CUSTOM_FIELD_VALUE);
	}

	protected void deleteAttachmentLists() {
		workingTables.delete(ATTACHMENT_LIST.ATTACHMENT_LIST_ID, ATTACHMENT.ATTACHMENT_LIST_ID);
		workingTables.delete(ATTACHMENT_LIST.ATTACHMENT_LIST_ID, ATTACHMENT_LIST.ATTACHMENT_LIST_ID);
		logDelete(ATTACHMENT_LIST);
	}

	// Here we cannot use the methods for delete attachments from our repository because it would require a fetch of
	// attachment list ids. The size can be several od thousands of ids. The (where ... in) sql clause would be nightmare
	// especially for mysql.
	protected void deleteAttachmentContents() {
		AttachmentRepositoryVisitor visitor = new AttachmentRepositoryVisitor() {
			@Override
			public void visit(FileSystemAttachmentRepository fileSystemAttachmentRepository) {
				// No way to erase file system attachments without fetching the whole attachmentList ids in case of external storage.
				// it really sucks, because this list will be gigantic when deleting big bunch of iterations
				List<Long> attachmentLidIds = workingTables.selectIds(ATTACHMENT_LIST.ATTACHMENT_LIST_ID);
				fileSystemAttachmentRepository.deleteContent(attachmentLidIds);
			}

			@Override
			public void visit(DatabaseAttachmentRepository databaseAttachmentRepository) {
				workingTables.addEntity(
					ATTACHMENT_CONTENT.ATTACHMENT_CONTENT_ID,
					() -> makeSelectOrphanContent()
				);
				workingTables.delete(ATTACHMENT_CONTENT.ATTACHMENT_CONTENT_ID, ATTACHMENT_CONTENT.ATTACHMENT_CONTENT_ID);
			}
		};
		attachmentRepository.accept(visitor);
		logDelete(ATTACHMENT_CONTENT);
	}

	protected void logReferenceEntitiesComplete() {
		getLogger().debug("All references are known in WORK_DELETE_ENTITIES table. Time on this stage : " + timeDiff() + " ms");
		updateTime();
	}

	protected void cleanWorkingTable() {
		workingTables.clean();
	}

	protected void clearPersistenceContext() {
		entityManager.flush();
		entityManager.clear();
	}

	protected abstract Logger getLogger();
}

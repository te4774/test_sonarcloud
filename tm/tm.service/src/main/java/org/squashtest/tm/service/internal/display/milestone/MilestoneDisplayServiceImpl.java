/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.milestone;

import org.jooq.DSLContext;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.service.display.milestone.MilestoneDisplayService;
import org.squashtest.tm.service.internal.display.dto.MilestoneAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.MilestoneGrid;
import org.squashtest.tm.service.internal.dto.UserDto;
import org.squashtest.tm.service.internal.repository.MilestoneDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.milestone.MilestoneBindingManagerService;
import org.squashtest.tm.service.milestone.MilestoneManagerService;
import org.squashtest.tm.service.user.UserAccountService;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN_OR_PROJECT_MANAGER;

@Service
@Transactional
public class MilestoneDisplayServiceImpl implements MilestoneDisplayService {

	private DSLContext dsl;
	private MilestoneDisplayDao milestoneDisplayDao;
	private UserAccountService userAccountService;
	private MilestoneManagerService milestoneManagerService;
	private MilestoneBindingManagerService milestoneBindingManagerService;
	private MilestoneDao milestoneDao;

	@Inject
	MilestoneDisplayServiceImpl(DSLContext dsl,
								UserAccountService userAccountService,
								MilestoneDisplayDao milestoneDisplayDao,
								MilestoneManagerService milestoneManagerService,
								MilestoneBindingManagerService milestoneBindingManagerService,
								MilestoneDao milestoneDao) {
		this.userAccountService = userAccountService;
		this.milestoneDisplayDao = milestoneDisplayDao;
		this.dsl = dsl;
		this.milestoneManagerService = milestoneManagerService;
		this.milestoneBindingManagerService = milestoneBindingManagerService;
		this.milestoneDao = milestoneDao;
	}

	@PreAuthorize(HAS_ROLE_ADMIN_OR_PROJECT_MANAGER)
	@Override
	public GridResponse findAll(GridRequest request) {
		UserDto currentUser = userAccountService.findCurrentUserDto();
		Long userId = currentUser.getUserId();

		MilestoneGrid milestoneGrid = new MilestoneGrid(userId, currentUser.isAdmin());
		GridResponse response = milestoneGrid.getRows(request, dsl);
		response.sanitizeField("description");

		return response;
	}

	@PreAuthorize(HAS_ROLE_ADMIN_OR_PROJECT_MANAGER)
	@Override
	public MilestoneAdminViewDto getMilestoneView(long milestoneId) {
		if (canReadMilestone(milestoneId)) {
			List<MilestoneDto> milestones = milestoneDisplayDao.findByIds(Collections.singleton(milestoneId));
			MilestoneAdminViewDto milestoneAdminViewDto = MilestoneAdminViewDto.fromMilestoneDto(milestones.get(0));
			milestoneAdminViewDto.setCanEdit(milestoneManagerService.canEditMilestone(milestoneId));
			milestoneDisplayDao.appendBoundProjectsToMilestoneInformation(milestoneAdminViewDto);
			return milestoneAdminViewDto;
		} else {
			throw new AccessDeniedException("Unauthorized.");
		}

	}

	@Override
	public boolean canReadMilestone(long milestoneId) {
		UserDto currentUser = userAccountService.findCurrentUserDto();

		if (currentUser.isAdmin()) {
			return true;
		} else {
			Long userId = currentUser.getUserId();
			MilestoneGrid milestoneGrid = new MilestoneGrid(userId, currentUser.isAdmin());
			return milestoneGrid.canReadMilestone(milestoneId, dsl);
		}
	}
}

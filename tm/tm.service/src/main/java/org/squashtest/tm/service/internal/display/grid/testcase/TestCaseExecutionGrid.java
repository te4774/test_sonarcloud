/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.testcase;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.SelectHavingStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;

public class TestCaseExecutionGrid extends AbstractGrid {

	private static final String TEST_SUITES_FIELD = "TEST_SUITES";
	private static final String EXECUTION_ID_FIELD = "EXECUTION_ID";
	private static final String NB_ISSUES_FIELD = "NB_ISSUES";


	private Long testCaseId;

	public TestCaseExecutionGrid(Long testCaseId) {
		this.testCaseId = testCaseId;
	}

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(EXECUTION.EXECUTION_ID),
			new GridColumn(EXECUTION.DATASET_LABEL),
			new GridColumn(EXECUTION.EXECUTION_MODE),
			new LevelEnumColumn(ExecutionStatus.class, EXECUTION.EXECUTION_STATUS),
			new GridColumn(EXECUTION.LAST_EXECUTED_BY),
			new GridColumn(EXECUTION.LAST_EXECUTED_ON),
			new GridColumn(ITERATION.NAME.as(RequestAliasesConstants.ITERATION_NAME)),
			new GridColumn(CAMPAIGN_LIBRARY_NODE.NAME.as(RequestAliasesConstants.CAMPAIGN_NAME)),
			new GridColumn(PROJECT.NAME.as(RequestAliasesConstants.PROJECT_NAME)),
			new GridColumn(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER),
			new GridColumn(getTestSuite().field(TEST_SUITES_FIELD)),
			new GridColumn(generateFieldIssues())
		);
	}

	private Field<Integer> generateFieldIssues() {
		return DSL.selectCount()
			.from(EXECUTION_ISSUES_CLOSURE)
			.where(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
			.asField(NB_ISSUES_FIELD);
	}

	private SelectHavingStep<?> getTestSuite() {
		return DSL.select(DSL.groupConcat(TEST_SUITE.NAME).as(TEST_SUITES_FIELD), EXECUTION.EXECUTION_ID)
			.from(EXECUTION)
			.innerJoin(ITEM_TEST_PLAN_EXECUTION).on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
			.innerJoin(TEST_SUITE_TEST_PLAN_ITEM).on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID))
			.innerJoin(TEST_SUITE).on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
			.where(EXECUTION.TCLN_ID.eq(testCaseId))
			.groupBy(EXECUTION.EXECUTION_ID);
	}

	@Override
	protected Table<?> getTable() {
		SelectHavingStep<?> testSuites = getTestSuite();
		return EXECUTION
			.innerJoin(ITEM_TEST_PLAN_EXECUTION).on(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
			.innerJoin(ITERATION_TEST_PLAN_ITEM).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
			.innerJoin(ITEM_TEST_PLAN_LIST).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
			.innerJoin(ITERATION).on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
			.innerJoin(PROJECT).on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.leftJoin(testSuites).on(EXECUTION.EXECUTION_ID.eq(testSuites.field(EXECUTION_ID_FIELD, Long.class)));
	}

	@Override
	protected Field<?> getIdentifier() {
		return EXECUTION.EXECUTION_ID;
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return null;
	}

	@Override
	protected Condition craftInvariantFilter() {
		return EXECUTION.TCLN_ID.eq(testCaseId);
	}


}

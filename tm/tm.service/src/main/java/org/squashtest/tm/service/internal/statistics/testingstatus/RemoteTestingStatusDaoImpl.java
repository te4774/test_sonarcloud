/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.statistics.testingstatus;

import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.statistics.testingstatus.RemoteTestingStatusDao;
import org.squashtest.tm.service.statistics.testingstatus.RequirementsByRemoteKeyByServerId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static org.squashtest.tm.jooq.domain.Tables.REMOTE_SYNCHRONISATION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_SYNC_EXTENDER;

@Repository
public class RemoteTestingStatusDaoImpl implements RemoteTestingStatusDao {

	private final DSLContext dslContext;

	public RemoteTestingStatusDaoImpl(DSLContext dslContext) {
		this.dslContext = dslContext;
	}

	@Override
	public RequirementsByRemoteKeyByServerId findRequirementsByRemoteKeyByServerId(String remoteSynchronisationKind) {
		final Map<Long, Map<String, List<Long>>> result = new HashMap<>();

		dslContext.select(REMOTE_SYNCHRONISATION.SERVER_ID, REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID, REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID)
			.from(REQUIREMENT_SYNC_EXTENDER)
			.innerJoin(REMOTE_SYNCHRONISATION).on(REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.eq(REMOTE_SYNCHRONISATION.REMOTE_SYNCHRONISATION_ID))
			.where(REMOTE_SYNCHRONISATION.KIND.eq(remoteSynchronisationKind))
			.fetch()
			.stream()
			.collect(groupingBy(r -> r.get(REMOTE_SYNCHRONISATION.SERVER_ID)))
			.forEach((serverId, records) -> {
				Map<String, List<Long>> reqIdsByRemoteKey = records.stream().collect(groupingBy(
					r -> r.get(REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID),
					mapping(r -> r.get(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID), toList())
				));
				result.put(serverId, reqIdsByRemoteKey);
			});

		return new RequirementsByRemoteKeyByServerId(result);
	}

	@Override
	public Map<Long, List<String>> findSynchronisedKeysBySynchronisationId(List<Long> remoteSyncIds) {
		final Map<Long, List<String>> remoteKeysBySyncId = new HashMap<>();

		dslContext.select(
			REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID,
			REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID
		)
			.from(REQUIREMENT_SYNC_EXTENDER)
			.where(REQUIREMENT_SYNC_EXTENDER.REMOTE_SYNCHRONISATION_ID.in(remoteSyncIds))
			.fetch().forEach(extenderRecord -> {
			final Long remoteSynchronisationId = extenderRecord.value1();
			final String remoteReqId = extenderRecord.value2();
			remoteKeysBySyncId.computeIfAbsent(remoteSynchronisationId, key -> new ArrayList<>()).add(remoteReqId);
		});

		return remoteKeysBySyncId;
	}
}

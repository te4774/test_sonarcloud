/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.bugtracker;

import org.springframework.stereotype.Component;
import org.squashtest.tm.bugtracker.definition.RemoteIssue;
import org.squashtest.tm.bugtracker.definition.RemotePriority;
import org.squashtest.tm.bugtracker.definition.RemoteStatus;
import org.squashtest.tm.bugtracker.definition.RemoteUser;
import org.squashtest.tm.domain.bugtracker.IssueDetector;
import org.squashtest.tm.domain.bugtracker.IssueOwnership;
import org.squashtest.tm.domain.bugtracker.RemoteIssueDecorator;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.service.bugtracker.BugTrackersLocalService;
import org.squashtest.tm.service.display.issue.IssueEntityType;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.GridResponseBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class BugTrackerServiceHelper {

	private static final String REMOTE_ID = "remoteId";
	private static final String SUMMARY = "summary";
	private static final String PRIORITY = "priority";
	private static final String BT_PROJECT = "btProject";

	private final BugTrackersLocalService service;

	private BugTrackerServiceHelper(BugTrackersLocalService service) {
		this.service = service;
	}

	public GridResponseBuilder<IssueOwnership<RemoteIssueDecorator>> createModelBuilderFor(IssueEntityType entityType) {
		switch (entityType) {
			case EXECUTION_TYPE:
			case EXECUTION_STEP_TYPE:
				return new ExecutionIssuesTableModel();
			case CAMPAIGN_TYPE:
			case ITERATION_TYPE:
			case TEST_SUITE_TYPE:
			case CAMPAIGN_FOLDER_TYPE:
				return new IterationIssuesTableModel();
			default:
				throw new IllegalArgumentException("BugTrackerController : cannot fetch issues for unknown entity type '" + entityType + "'");
		}
	}

	private final class IterationIssuesTableModel extends GridResponseBuilder<IssueOwnership<RemoteIssueDecorator>> {

		@Override
		public DataRow buildItemDataRow(IssueOwnership<RemoteIssueDecorator> ownership) {

			DataRow dataRow = new DataRow();
			RemoteIssue remoteIssue = ownership.getIssue();
			Map<String, Object> data = initializeDataOfDataRow(remoteIssue, ownership.getExecution());
			String url = service.getIssueUrl(remoteIssue.getId(), ownership.getOwner().getBugTracker())
				.toExternalForm();
			data.put("url", url);
			dataRow.setData(data);
			return dataRow;
		}
	}

	private final class ExecutionIssuesTableModel extends GridResponseBuilder<IssueOwnership<RemoteIssueDecorator>> {

		@Override
		public DataRow buildItemDataRow(IssueOwnership<RemoteIssueDecorator> ownership) {

			DataRow dataRow = new DataRow();
			RemoteIssueDecorator remoteIssue = ownership.getIssue();
			Map<String, Object> data = initializeDataOfDataRow(remoteIssue, ownership.getExecution());
			String url = service.getIssueUrl(remoteIssue.getId(), ownership.getOwner().getBugTracker())
				.toExternalForm();
			IssueDetector owner = ownership.getOwner();
			data.put("issueId", remoteIssue.getIssueId());
			data.put("stepOrder", findStepOrder(owner));
			data.put("url", url);
			dataRow.setData(data);
			return dataRow;
		}
	}

	private static String findStepOrder(IssueDetector owner) {
		String stepOrder = "";
		if (owner instanceof ExecutionStep) {
			ExecutionStep step = (ExecutionStep) owner;
			return step.getExecutionStepOrder().toString();
		}

		return stepOrder;
	}

	private static String findSuiteNames(Execution execution) {
		String suiteNames = "";
		List<TestSuite> testSuites = execution.getTestPlan().getTestSuites();
		if (!testSuites.isEmpty()) {
			suiteNames = testSuites.stream().map(TestSuite::getName).collect(Collectors.joining(", "));
		}
		return suiteNames;
	}

	private static String findAssignee(RemoteIssue issue) {
		String assignee = "";
		RemoteUser remoteUser = issue.getAssignee();
		if (remoteUser != null) {
			assignee = remoteUser.getName();
		}
		return assignee;
	}

	private static String findStatus(RemoteIssue issue) {
		String status = "";
		RemoteStatus remoteStatus = issue.getStatus();
		if (remoteStatus != null) {
			status = remoteStatus.getName();
		}
		return status;
	}

	private static String findPriority(RemoteIssue issue) {
		String priority = "";
		RemotePriority remotePriority = issue.getPriority();
		if (remotePriority != null) {
			priority = remotePriority.getName();
		}
		return priority;
	}

	private static Map<String, Object> initializeDataOfDataRow(RemoteIssue issue, Execution execution) {
		Map<String, Object> row = new HashMap<>();
		row.put(REMOTE_ID, issue.getId());
		row.put(SUMMARY, issue.getSummary());
		row.put(PRIORITY, findPriority(issue));
		row.put("status", findStatus(issue));
		row.put("assignee", findAssignee(issue));
		row.put("executionId", execution.getId());
		row.put("executionOrder", execution.getExecutionOrder());
		row.put("executionName", execution.getName());
		row.put("suiteNames", findSuiteNames(execution));
		row.put("remoteKey", issue.getRemoteKey());
		row.put(BT_PROJECT, issue.getProject().getName());
		return row;
	}
}

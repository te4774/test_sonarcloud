/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.test.automation.server;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.service.display.test.automation.server.TestAutomationServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto;
import org.squashtest.tm.service.internal.display.dto.CredentialsDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationServerAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationServerDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.TestAutomationServerGrid;
import org.squashtest.tm.service.internal.repository.TestAutomationServerDao;
import org.squashtest.tm.service.internal.repository.display.EnvironmentVariableDisplayDao;
import org.squashtest.tm.service.internal.repository.display.TestAutomationServerDisplayDao;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.TestAutomationServerCredentialsService;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;


@Service
@Transactional
public class TestAutomationServerDisplayServiceImpl implements TestAutomationServerDisplayService {
	private final DSLContext dsl;
	private final TestAutomationServerDisplayDao testAutomationServerDisplayDao;
	private final StoredCredentialsManager credentialsManager;
	private final TestAutomationServerCredentialsService testAutomationServerCredentialsService;
	private final TestAutomationServerDao testAutomationServerDao;
	private final EnvironmentVariableDisplayDao environmentVariableDisplayDao;

	@Inject
	public TestAutomationServerDisplayServiceImpl(DSLContext dsl,
												  TestAutomationServerDisplayDao testAutomationServerDisplayDao,
												  StoredCredentialsManager credentialsManager,
												  TestAutomationServerCredentialsService testAutomationServerCredentialsService,
												  TestAutomationServerDao testAutomationServerDao, EnvironmentVariableDisplayDao environmentVariableDisplayDao) {
		this.dsl = dsl;
		this.testAutomationServerDisplayDao = testAutomationServerDisplayDao;
		this.credentialsManager = credentialsManager;
		this.testAutomationServerCredentialsService = testAutomationServerCredentialsService;
		this.testAutomationServerDao = testAutomationServerDao;
		this.environmentVariableDisplayDao = environmentVariableDisplayDao;
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public GridResponse getTestAutomationServerGrid(GridRequest request) {
		TestAutomationServerGrid testAutomationServerGrid = new TestAutomationServerGrid();
		return testAutomationServerGrid.getRows(request, dsl);
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public List<TestAutomationServerDto> findAll() {
		return testAutomationServerDisplayDao.findAll();
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public TestAutomationServerAdminViewDto getTestAutomationServerView(long testAutomationServerId) {
		TestAutomationServerAdminViewDto dto = TestAutomationServerAdminViewDto.from(
			testAutomationServerDisplayDao.getTestAutomationServerById(testAutomationServerId));
		ManageableCredentials credentials = credentialsManager.findAppLevelCredentials(testAutomationServerId);
		dto.setSupportsAutomatedExecutionEnvironments(testAutomationServerDisplayDao.supportsAutomatedExecutionEnvironments(dto.getKind()));
		dto.setEnvironmentTags(getDefaultEnvironmentTags(testAutomationServerId));
		dto.setCredentials(CredentialsDto.from(credentials));
		dto.setSupportedAuthenticationProtocols(getSupportedAuthenticationProtocols(testAutomationServerId));
		appendEnvironmentVariables(testAutomationServerId, dto);
		return dto;
	}

	private void appendEnvironmentVariables(long testAutomationServerId, TestAutomationServerAdminViewDto dto) {
		List<BoundEnvironmentVariableDto> environmentVariables = getEnvironmentVariablesDtoByServerId(testAutomationServerId);
		dto.setBoundEnvironmentVariables(environmentVariables);
	}

	private List<String> getSupportedAuthenticationProtocols(long testAutomationServerId) {
		Optional<TestAutomationServer> server = testAutomationServerDao.findById(testAutomationServerId);
		return server.map(testAutomationServer -> Arrays.stream(testAutomationServerCredentialsService.getSupportedProtocols(testAutomationServer))
				.map(Enum::name).collect(Collectors.toList()))
			.orElse(new ArrayList<>());
	}

	@Override
	public List<String> getDefaultEnvironmentTags(long testAutomationServerId) {
		return testAutomationServerDao.getEnvironmentTags(testAutomationServerId);
	}

	@Override
	public boolean hasDefinedCredentials(long testAutomationServerId) {
		return credentialsManager.unsecuredFindAppLevelCredentials(testAutomationServerId) != null;
	}


	@Override
	public List<BoundEnvironmentVariableDto> getEnvironmentVariablesDtoByServerId(Long serverId) {
		List<BoundEnvironmentVariableDto> environmentVariables =
			testAutomationServerDisplayDao.getDefaultBoundEnvironmentVariablesByServerId(serverId);

		environmentVariables.forEach(variable ->
			variable.setOptions(environmentVariableDisplayDao.getEnvironmentVariableOptionListByEvId(variable.getId()))
		);
		return environmentVariables;
	}
}

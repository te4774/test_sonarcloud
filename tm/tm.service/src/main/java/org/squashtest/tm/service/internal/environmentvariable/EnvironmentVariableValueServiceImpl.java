/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableOption;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableValue;
import org.squashtest.tm.domain.environmentvariable.SingleSelectEnvironmentVariable;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableValueService;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableValueDao;
import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class EnvironmentVariableValueServiceImpl implements EnvironmentVariableValueService {

	private final EnvironmentVariableValueDao environmentVariableValueDao;

	private final ProjectDisplayDao projectDisplayDao;

	private final EnvironmentVariableBindingDao environmentVariableBindingDao;
	private final EnvironmentVariableDao environmentVariableDao;

	public EnvironmentVariableValueServiceImpl(EnvironmentVariableValueDao environmentVariableValueDao,
											   ProjectDisplayDao projectDisplayDao,
											   EnvironmentVariableBindingDao environmentVariableBindingDao,
											   EnvironmentVariableDao environmentVariableDao) {
		this.environmentVariableValueDao = environmentVariableValueDao;
		this.projectDisplayDao = projectDisplayDao;
		this.environmentVariableBindingDao = environmentVariableBindingDao;
		this.environmentVariableDao = environmentVariableDao;
	}

	@Override
	public void removeEnvironmentVariableValuesByBindingIds(Collection<Long> bindingIds) {
		Collection<EnvironmentVariableValue> environmentVariableValues = environmentVariableValueDao.findAllByBindingIds(bindingIds);
		if (!environmentVariableValues.isEmpty()) {
			environmentVariableValueDao.deleteAll(environmentVariableValues);
		}
	}

	@Override
	public void editEnvironmentVariableValueFromServer(Long testAutomationServerId, Long environmentVariableId, String value) {
		checkValueIsValidOption(environmentVariableId, value);

		EnvironmentVariableBinding binding = environmentVariableBindingDao
			.findByServerIdAndEvId(testAutomationServerId, environmentVariableId);

		editEnvironmentVariableValue(binding, EVBindableEntity.TEST_AUTOMATION_SERVER, value, testAutomationServerId);
	}

	private void checkValueIsValidOption(Long environmentVariableId, String value) {
		SingleSelectEnvironmentVariable environmentVariable = environmentVariableDao.findSingleSelectEnvironmentVariableById(environmentVariableId);
		if (environmentVariable != null) {
			List<String> values =
				environmentVariable.getOptions().stream().map(EnvironmentVariableOption::getLabel).collect(Collectors.toList());
			if (StringUtils.hasText(value) && !values.contains(value)) {
				throw new IllegalArgumentException();
			}
		}
	}

	@Override
	public void createValueFromServer(EnvironmentVariableBinding binding, Long testAutomationServerId) {
		EnvironmentVariableValue environmentVariableValue =
			new EnvironmentVariableValue(EVBindableEntity.TEST_AUTOMATION_SERVER, testAutomationServerId, binding);
		environmentVariableValueDao.save(environmentVariableValue);
	}

	@Override
	public void editEnvironmentVariableValueFromProject(Long projectId, Long environmentVariableId,
														String value) {

		Long testAutomationServerId = projectDisplayDao.getTaServerIdByProjectId(projectId);

		EnvironmentVariableBinding binding = environmentVariableBindingDao
			.findByServerIdAndEvId(testAutomationServerId, environmentVariableId);

		EnvironmentVariableValue existingProjectValue = environmentVariableValueDao.
			findSingleEnvironmentVariableValue(binding, EVBindableEntity.PROJECT, projectId);
		if (existingProjectValue != null) {
			existingProjectValue.setValue(value);
		} else {
			EnvironmentVariableValue newProjectValue = new EnvironmentVariableValue(EVBindableEntity.PROJECT,
				projectId, binding);
			newProjectValue.setValue(value);
			environmentVariableValueDao.save(newProjectValue);
		}
	}

	/**
	 * Sets the value of an environment variable to the nearest defined value on a higher-level entity.
	 * If no value is defined on the previous-level, the method will return the value of the variable at the
	 * highest-level (server).
	 *
	 * @param projectId
	 * @param environmentVariableId
	 * @param entityType
	 * @return String value
	 */
	@Override
	public String resetDefaultValue(Long projectId, Long environmentVariableId, String entityType) {

		Long testAutomationServerId = projectDisplayDao.getTaServerIdByProjectId(projectId);

		EnvironmentVariableBinding binding = environmentVariableBindingDao
			.findByServerIdAndEvId(testAutomationServerId, environmentVariableId);
		EnvironmentVariableValue existingProjectValue = environmentVariableValueDao.
			findSingleEnvironmentVariableValue(binding, EVBindableEntity.PROJECT, projectId);
		EnvironmentVariableValue defaultEnvironmentVariableValue = environmentVariableValueDao.
			findSingleEnvironmentVariableValue(binding, EVBindableEntity.TEST_AUTOMATION_SERVER, testAutomationServerId);

		if (existingProjectValue != null) {
			if(entityType.equals(EVBindableEntity.PROJECT.name())) {
				environmentVariableValueDao.delete(existingProjectValue);
			} else {
				return existingProjectValue.getValue();
			}
		}
		return defaultEnvironmentVariableValue.getValue();

	}

	@Override
	public void removeEnvironmentVariableValuesByProjectId(Long projectId) {
		List<EnvironmentVariableValue> values =
			environmentVariableValueDao.findAllByBoundEntityIdAndBoundEntityType(projectId, EVBindableEntity.PROJECT);

		if (!values.isEmpty()) {
			environmentVariableValueDao.deleteAll(values);
		}
	}

	@Override
	public void reinitializeEnvironmentVariableValuesByValueAndEvId(List<String> values, Long evId) {
		List<EnvironmentVariableBinding> bindings = environmentVariableBindingDao.findAllByEnvironmentVariable_Id(evId);
		List<EnvironmentVariableValue> environmentVariableValues =
			environmentVariableValueDao.findAllByBindingsAndValues(bindings, values);
		environmentVariableValues.forEach(environmentVariableValue -> environmentVariableValue.setValue(""));
	}

	private void editEnvironmentVariableValue(EnvironmentVariableBinding binding, EVBindableEntity entityType,
											  String value, Long entityId) {
		EnvironmentVariableValue environmentVariableValue =
			environmentVariableValueDao.findSingleEnvironmentVariableValue(binding, entityType, entityId);
			environmentVariableValue.setValue(value);
	}
}

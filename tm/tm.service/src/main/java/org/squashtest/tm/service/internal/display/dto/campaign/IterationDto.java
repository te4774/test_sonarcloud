/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.campaign;

import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.domain.campaign.TestPlanStatistics;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.UserView;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class IterationDto {

	private Long id;
	private String uuid;
	private Long projectId;
	private String name;
	private String reference;
	private String description;
	private String iterationStatus;
	private Date createdOn;
	private String createdBy;
	private Date lastModifiedOn;
	private String lastModifiedBy;
	private TestPlanStatistics testPlanStatistics;
	private Date actualStartDate;
	private Date actualEndDate;
	private Boolean actualStartAuto;
	private Boolean actualEndAuto;
	private Date scheduledStartDate;
	private Date scheduledEndDate;
	private Long attachmentListId;
	private AttachmentListDto attachmentList;
	private List<CustomFieldValueDto> customFieldValues = new ArrayList<>();
	private boolean hasDatasets;
	private HashMap<Long, String> executionStatusMap;
	private int nbIssues;
	private List<UserView> users;
	private List<MilestoneDto> milestones = new ArrayList<>();
	private List<NamedReference> testSuites = new ArrayList<>();
	private long nbAutomatedSuites;
	private JsonCustomReportDashboard dashboard;
	private boolean shouldShowFavoriteDashboard;
	private boolean canShowFavoriteDashboard;
	private Long favoriteDashboardId;
	private int nbTestPlanItems;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAttachmentListId() {
		return attachmentListId;
	}

	public void setAttachmentListId(Long attachmentListId) {
		this.attachmentListId = attachmentListId;
	}

	public AttachmentListDto getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(AttachmentListDto attachmentList) {
		this.attachmentList = attachmentList;
	}

	public List<CustomFieldValueDto> getCustomFieldValues() {
		return customFieldValues;
	}

	public void setCustomFieldValues(List<CustomFieldValueDto> customFieldValues) {
		this.customFieldValues = customFieldValues;
	}

	public boolean isHasDatasets() {
		return hasDatasets;
	}

	public void setHasDatasets(boolean hasDatasets) {
		this.hasDatasets = hasDatasets;
	}

	public HashMap<Long, String> getExecutionStatusMap() {
		return executionStatusMap;
	}

	public void setExecutionStatusMap(HashMap<Long, String> executionStatusMap) {
		this.executionStatusMap = executionStatusMap;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public TestPlanStatistics getTestPlanStatistics() {
		return testPlanStatistics;
	}

	public void setTestPlanStatistics(TestPlanStatistics testPlanStatistics) {
		this.testPlanStatistics = testPlanStatistics;
	}

	public Date getActualStartDate() {
		return actualStartDate;
	}

	public void setActualStartDate(Date actualStartDate) {
		this.actualStartDate = actualStartDate;
	}

	public Date getActualEndDate() {
		return actualEndDate;
	}

	public void setActualEndDate(Date actualEndDate) {
		this.actualEndDate = actualEndDate;
	}

	public Boolean getActualStartAuto() {
		return actualStartAuto;
	}

	public void setActualStartAuto(Boolean actualStartAuto) {
		this.actualStartAuto = actualStartAuto;
	}

	public Boolean getActualEndAuto() {
		return actualEndAuto;
	}

	public void setActualEndAuto(Boolean actualEndAuto) {
		this.actualEndAuto = actualEndAuto;
	}

	public Date getScheduledStartDate() {
		return scheduledStartDate;
	}

	public void setScheduledStartDate(Date scheduledStartDate) {
		this.scheduledStartDate = scheduledStartDate;
	}

	public Date getScheduledEndDate() {
		return scheduledEndDate;
	}

	public void setScheduledEndDate(Date scheduledEndDate) {
		this.scheduledEndDate = scheduledEndDate;
	}

	public String getIterationStatus() {
		return iterationStatus;
	}

	public void setIterationStatus(String iterationStatus) {
		this.iterationStatus = iterationStatus;
	}

	public int getNbIssues() {
		return nbIssues;
	}

	public void setNbIssues(int nbIssues) {
		this.nbIssues = nbIssues;
	}

	public List<UserView> getUsers() {
		return users;
	}

	public void setUsers(List<UserView> users) {
		this.users = users;
	}

	public List<MilestoneDto> getMilestones() {
		return milestones;
	}

	public void setMilestones(List<MilestoneDto> milestones) {
		this.milestones = milestones;
	}

	public List<NamedReference> getTestSuites() {
		return testSuites;
	}

	public void setTestSuites(List<NamedReference> testSuites) {
		this.testSuites = testSuites;
	}

	public long getNbAutomatedSuites() {
		return nbAutomatedSuites;
	}

	public void setNbAutomatedSuites(long nbAutomatedSuites) {
		this.nbAutomatedSuites = nbAutomatedSuites;
	}

	public JsonCustomReportDashboard getDashboard() {
		return dashboard;
	}

	public void setDashboard(JsonCustomReportDashboard dashboard) {
		this.dashboard = dashboard;
	}

	public boolean isShouldShowFavoriteDashboard() {
		return shouldShowFavoriteDashboard;
	}

	public void setShouldShowFavoriteDashboard(boolean shouldShowFavoriteDashboard) {
		this.shouldShowFavoriteDashboard = shouldShowFavoriteDashboard;
	}

	public boolean isCanShowFavoriteDashboard() {
		return canShowFavoriteDashboard;
	}

	public void setCanShowFavoriteDashboard(boolean canShowFavoriteDashboard) {
		this.canShowFavoriteDashboard = canShowFavoriteDashboard;
	}

	public Long getFavoriteDashboardId() {
		return favoriteDashboardId;
	}

	public void setFavoriteDashboardId(Long favoriteDashboardId) {
		this.favoriteDashboardId = favoriteDashboardId;
	}

	public int getNbTestPlanItems() {
		return nbTestPlanItems;
	}

	public void setNbTestPlanItems(int nbTestPlanItems) {
		this.nbTestPlanItems = nbTestPlanItems;
	}
}

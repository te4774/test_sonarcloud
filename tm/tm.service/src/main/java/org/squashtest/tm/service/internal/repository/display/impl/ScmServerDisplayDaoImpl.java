/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.ScmRepositoryDto;
import org.squashtest.tm.service.internal.display.dto.ScmServerAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.ScmServerDto;
import org.squashtest.tm.service.internal.repository.display.ScmServerDisplayDao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.squashtest.tm.jooq.domain.Tables.SCM_REPOSITORY;
import static org.squashtest.tm.jooq.domain.Tables.SCM_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;

@Repository
public class ScmServerDisplayDaoImpl implements ScmServerDisplayDao {

	private DSLContext dslContext;

	public ScmServerDisplayDaoImpl(DSLContext dslContext) {
		this.dslContext = dslContext;
	}


	@Override
	public ScmServerAdminViewDto getScmServerViewById(long scmServerId) {
		ScmServerAdminViewDto dto = dslContext.select(
			SCM_SERVER.SERVER_ID,
			THIRD_PARTY_SERVER.NAME,
			THIRD_PARTY_SERVER.URL,
			SCM_SERVER.KIND,
			SCM_SERVER.COMMITTER_MAIL,
			THIRD_PARTY_SERVER.AUTH_POLICY,
			THIRD_PARTY_SERVER.AUTH_PROTOCOL
		).from(SCM_SERVER)
			.innerJoin(THIRD_PARTY_SERVER).on(SCM_SERVER.SERVER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
			.where(SCM_SERVER.SERVER_ID.eq(scmServerId))
			.fetchOne().into(ScmServerAdminViewDto.class);

		return dto;
	}

	@Override
	public List<ScmServerDto> findAll() {

		List<ScmServerDto> scmServers = new ArrayList<>();
		Map<Record, ? extends Result<? extends Record>> record = dslContext.select(SCM_SERVER.SERVER_ID,
			SCM_SERVER.KIND, SCM_SERVER.COMMITTER_MAIL,
			THIRD_PARTY_SERVER.NAME, THIRD_PARTY_SERVER.URL,
			SCM_REPOSITORY.SERVER_ID, SCM_REPOSITORY.NAME, SCM_REPOSITORY.REPOSITORY_PATH, SCM_REPOSITORY.SCM_REPOSITORY_ID,
			SCM_REPOSITORY.WORKING_BRANCH, SCM_REPOSITORY.WORKING_FOLDER_PATH)
			.from(SCM_SERVER)
			.innerJoin(THIRD_PARTY_SERVER).on(SCM_SERVER.SERVER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
			.innerJoin(SCM_REPOSITORY).on(SCM_SERVER.SERVER_ID.eq(SCM_REPOSITORY.SERVER_ID))
			.orderBy(THIRD_PARTY_SERVER.SERVER_ID, SCM_REPOSITORY.SCM_REPOSITORY_ID)
			.fetch().intoGroups(Arrays.asList(SCM_SERVER.SERVER_ID,
				SCM_SERVER.KIND, SCM_SERVER.COMMITTER_MAIL,
				THIRD_PARTY_SERVER.NAME, THIRD_PARTY_SERVER.URL)
				.toArray(new Field[0]));


		record.entrySet().forEach(recordEntry -> {
			ScmServerDto scmServerDto = new ScmServerDto();
			scmServerDto.setServerId(recordEntry.getKey().get(SCM_SERVER.SERVER_ID));
			scmServerDto.setName(recordEntry.getKey().get(THIRD_PARTY_SERVER.NAME));
			scmServerDto.setCommitterMail(recordEntry.getKey().get(SCM_SERVER.COMMITTER_MAIL));
			scmServerDto.setKind(recordEntry.getKey().get(SCM_SERVER.KIND));
			scmServerDto.setUrl(recordEntry.getKey().get(THIRD_PARTY_SERVER.URL));
			scmServers.add(scmServerDto);
			recordEntry.getValue().forEach(value -> {
				ScmRepositoryDto scmRepositoryDto = new ScmRepositoryDto();
				scmRepositoryDto.setScmRepositoryId(value.get(SCM_REPOSITORY.SCM_REPOSITORY_ID));
				scmRepositoryDto.setServerId(value.get(SCM_REPOSITORY.SERVER_ID));
				scmRepositoryDto.setName(value.get(SCM_REPOSITORY.NAME));
				scmRepositoryDto.setRepositoryPath(value.get(SCM_REPOSITORY.REPOSITORY_PATH));
				scmRepositoryDto.setWorkingBranch(value.get(SCM_REPOSITORY.WORKING_BRANCH));
				scmRepositoryDto.setWorkingFolderPath(value.get(SCM_REPOSITORY.WORKING_FOLDER_PATH));
				scmServerDto.getRepositories().add(scmRepositoryDto);
			});
		});


		return scmServers;
	}

	@Override
	public List<Long> findAllRepositoriesByServerIds(List<Long> scmServerIds) {
		if (scmServerIds.isEmpty()) {
			return Collections.emptyList();
		}

		return dslContext
			.select(SCM_REPOSITORY.SCM_REPOSITORY_ID)
			.from(SCM_REPOSITORY)
			.where(SCM_REPOSITORY.SERVER_ID.in(scmServerIds))
			.fetchInto(Long.class);
	}
}

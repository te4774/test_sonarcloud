/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc.delegate;

import org.jooq.DSLContext;
import org.jooq.Param;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.jooq.TableField;

import static org.jooq.impl.DSL.val;
import static org.squashtest.tm.jooq.domain.tables.WorkDeleteEntities.WORK_DELETE_ENTITIES;

public class AbstractDelegateDeleteQuery {
	protected final TableField<?, Long> originalField;
	protected final DSLContext dslContext;
	protected final String operationId;

	public AbstractDelegateDeleteQuery(TableField<?, Long> originalField, DSLContext dslContext, String operationId) {
		this.originalField = originalField;
		this.dslContext = dslContext;
		this.operationId = operationId;
	}

	public static Param<String> extractFieldTableNameAsParam(TableField<?, Long> field) {
		String entityColumnIdentifier = field.getTable().getName();
		return val(entityColumnIdentifier);
	}

	public SelectConditionStep<Record1<Long>> selectEntityIds() {
		Param<String> nameAsParam = extractFieldTableNameAsParam(originalField);
		return makeSelectEntityIds(nameAsParam, operationId, dslContext);
	}

	public static SelectConditionStep<Record1<Long>> makeSelectEntityIds(Param<String> nameAsParam, String operationId, DSLContext dslContext) {
		return dslContext.select(WORK_DELETE_ENTITIES.ENTITY_ID)
			.from(WORK_DELETE_ENTITIES)
			.where(WORK_DELETE_ENTITIES.ENTITY_TYPE.eq(nameAsParam))
			.and(WORK_DELETE_ENTITIES.OPERATION_ID.eq(operationId));
	}
}

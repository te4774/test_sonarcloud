/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Select;
import org.jooq.SelectConditionStep;
import org.jooq.TableField;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static org.squashtest.tm.jooq.domain.tables.WorkDeleteEntities.WORK_DELETE_ENTITIES;

/**
 * This class represents all WORKING_TABLE available for one operation.
 * It allows performing multiple operation affecting WORKING_TABLE or the real squash tables, in relation to ids stored
 * in WORKING_TABLE.
 * Note that the baking real database table is WORK_DELETE_ENTITIES, and one operation is scoped by operation id. Using only one working table
 * for all operation and all entities was a good compromise between speed and number of tables added to the databases.
 */
public class JdbcWorkingTables {

	private final Map<TableField<?, Long>, JdbcWorkingTable> tables = new HashMap<>();
	private final DSLContext dslContext;
	private final String operationId;

	public JdbcWorkingTables(DSLContext dslContext, String operationId) {
		this.dslContext = requireNonNull(dslContext);
		this.operationId = operationId;
	}

	/**
	 * Add reference to original field, allowing to store Longs(aka ids...) in working table.
	 * @param originalField The field
	 * @param selector A selector, allowing to select ID filtered as needed.
	 */
	public void addEntity(TableField<?, Long> originalField,
						  Supplier<Select<Record3<Long, String, String>>> selector) {
		JdbcWorkingTable jdbcWorkingTable = new JdbcWorkingTable(originalField, selector, dslContext, operationId);
		tables.put(originalField, jdbcWorkingTable);
	}

	public SelectConditionStep<Record1<Long>> makeSelectIds(TableField<?, Long> referencedEntity){
		return tables.get(referencedEntity).selectEntityIds();
	}

	public List<Long> selectIds(TableField<?, Long> referencedEntity){
		return tables.get(referencedEntity)
			.selectEntityIds()
			.fetch(WORK_DELETE_ENTITIES.ENTITY_ID);
	}

	/**
	 * Delete rows using a where clause containing the ids of stored entities in WORK_DELETE_ENTITIES table.
	 * Ex: If some CAMPAIGN.CLN_ID are stored in working table and one want to delete all CAMPAIGN_TEST_PLAN linked to these campaigns,
	 * one must call : delete(CAMPAIGN.CLN_ID, CAMPAIGN_TEST_PLAN.CAMPAIGN_ID)
	 * @param referencedEntity The reference entity in working table
	 * @param toDelete The target column of the foreign key
	 */
	public void delete(TableField<?, Long> referencedEntity, TableField<?, Long> toDelete) {
		tables.get(referencedEntity).delete(toDelete);
	}

	public void nullify(TableField<?, Long> referencedEntity, TableField<?, Long> toNullify) {
		tables.get(referencedEntity).nullify(toNullify);
	}

	/**
	 * Clean all reference from all tables for this operation.
	 */
	public void clean() {
		dslContext.deleteFrom(WORK_DELETE_ENTITIES)
			.where(WORK_DELETE_ENTITIES.OPERATION_ID.eq(operationId))
			.execute();
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Record4;
import org.jooq.SelectHavingStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import static org.jooq.impl.DSL.countDistinct;
import static org.squashtest.tm.domain.milestone.MilestoneStatus.MILESTONE_BLOCKING_STATUSES;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.BOUND_TO_BLOCKING_MILESTONE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITEM_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.USER;

public class CampaignTestPlanGrid extends AbstractGrid {

	private final Long campaignId;

	public CampaignTestPlanGrid(Long campaignId) {
		this.campaignId = campaignId;
	}

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID),
			new GridColumn(TEST_CASE_LIBRARY_NODE.NAME.as("TEST_CASE_NAME"), TEST_CASE_LIBRARY_NODE.NAME),
			new GridColumn(TEST_CASE_LIBRARY_NODE.PROJECT_ID.as("PROJECT_ID")),
			new GridColumn(CAMPAIGN_LIBRARY_NODE.CLN_ID.as("CAMPAIGN_ID")),
			new GridColumn(PROJECT.NAME.as("PROJECT_NAME"), PROJECT.NAME),
			new GridColumn(TEST_CASE.REFERENCE.as("TEST_CASE_REFERENCE"), TEST_CASE.REFERENCE),
			new GridColumn(TEST_CASE.TCLN_ID.as("TEST_CASE_ID")),
			new LevelEnumColumn(TestCaseImportance.class, TEST_CASE.IMPORTANCE),
			new GridColumn(getUser().field(USER)),
			new GridColumn(getUser().field("LOGIN", String.class)), // this field is used only for filters
			new GridColumn(DATASET.NAME.as("DATASET_NAME"), DATASET.NAME),
			new GridColumn(DSL.field("MILESTONE_MIN_DATE")),
			new GridColumn(DSL.field("MILESTONE_MAX_DATE")),
			new GridColumn(DSL.field("MILESTONE_LABELS")),
			new GridColumn(DSL.field(BOUND_TO_BLOCKING_MILESTONE))
		);
	}

	@Override
	protected Table<?> getTable() {
		SelectHavingStep<?> getUser = getUser();
		SelectHavingStep<?> milestoneDates = getMilestoneDates();
		SelectHavingStep<?> boundToLockedMilestone = getBoundToBlockingMilestone();

		return CAMPAIGN_TEST_PLAN_ITEM
			.innerJoin(CAMPAIGN).on(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
			.innerJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN.CLN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
			.leftJoin(TEST_CASE).on(CAMPAIGN_TEST_PLAN_ITEM.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
			.leftJoin(TEST_CASE_LIBRARY_NODE).on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
			.innerJoin(PROJECT).on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.leftJoin(DATASET).on(CAMPAIGN_TEST_PLAN_ITEM.DATASET_ID.eq(DATASET.DATASET_ID))
			.leftJoin(getUser)
			.on(CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID.eq(getUser.field(ITEM_ID, Long.class)))
			.leftJoin(milestoneDates)
			.on(CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID.eq(milestoneDates.field(ITEM_ID, Long.class)))
			.leftJoin(boundToLockedMilestone)
			.on(CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID.eq(boundToLockedMilestone.field(ITEM_ID, Long.class)));
	}

	@Override
	protected Condition craftInvariantFilter() {
		return CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID.eq(this.campaignId);
	}

	@Override
	protected Field<?> getIdentifier() {
		return CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID;
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return CAMPAIGN_LIBRARY_NODE.PROJECT_ID;
	}

	@Override
	protected SortField<?> getDefaultOrder() {
		return CAMPAIGN_TEST_PLAN_ITEM.TEST_PLAN_ORDER.asc();
	}


	/*	Format the user infos as "LASTNAME (LOGIN)" or "FIRSTNAME LASTNAME (LOGIN)" */
	private SelectHavingStep<?> getUser() {
		// ITEM_ID | USER
		return DSL.select(
			CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID.as(ITEM_ID),
			CORE_USER.LOGIN,
			DSL.when(
				CORE_USER.FIRST_NAME.isNotNull().and(CORE_USER.FIRST_NAME.notEqual("")),
				CORE_USER.FIRST_NAME.concat(" "))
				.otherwise("")
				.concat(CORE_USER.LAST_NAME).concat(" (")
				.concat(CORE_USER.LOGIN).concat(")").as(USER)
		)
			.from(CAMPAIGN_TEST_PLAN_ITEM)
			.innerJoin(CORE_USER)
			.on(CAMPAIGN_TEST_PLAN_ITEM.USER_ID.eq(CORE_USER.PARTY_ID));
	}

	private SelectHavingStep<Record4<Long, Timestamp, Timestamp, String>> getMilestoneDates() {
		// ITEM_ID | MILESTONE_LABELS | MILESTONE_MIN_DATE | MILESTONE_MAX_DATE
		return DSL.select(
			CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID.as(ITEM_ID),
			DSL.min(MILESTONE.END_DATE).as("MILESTONE_MIN_DATE"),
			DSL.max(MILESTONE.END_DATE).as("MILESTONE_MAX_DATE"),
			DSL.listAgg(MILESTONE.LABEL, ", ").withinGroupOrderBy(MILESTONE.END_DATE.asc()).as("MILESTONE_LABELS")
		)
			.from(CAMPAIGN_TEST_PLAN_ITEM)
			.innerJoin(MILESTONE_TEST_CASE)
			.on(CAMPAIGN_TEST_PLAN_ITEM.TEST_CASE_ID.eq(MILESTONE_TEST_CASE.TEST_CASE_ID))
			.innerJoin(MILESTONE).on(MILESTONE.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID))
			.groupBy(
				CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID,
				MILESTONE_TEST_CASE.TEST_CASE_ID
			);
	}
	private SelectHavingStep<Record2<Long, Boolean>> getBoundToBlockingMilestone() {
		// ITEM_ID | BOUND_TO_BLOCKING_MILESTONE
		return DSL.select(
				CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID.as(ITEM_ID),
				DSL.field(countDistinct(MILESTONE.MILESTONE_ID).gt(0)).as(BOUND_TO_BLOCKING_MILESTONE)
		)
				.from(CAMPAIGN_TEST_PLAN_ITEM)
				.innerJoin(MILESTONE_CAMPAIGN)
				.on(MILESTONE_CAMPAIGN.CAMPAIGN_ID.eq(CAMPAIGN_TEST_PLAN_ITEM.CAMPAIGN_ID))
				.innerJoin(MILESTONE).on(MILESTONE.MILESTONE_ID.eq(MILESTONE_CAMPAIGN.MILESTONE_ID))
				.where(MILESTONE.STATUS.in(MILESTONE_BLOCKING_STATUSES))
				.groupBy(CAMPAIGN_TEST_PLAN_ITEM.CTPI_ID);
	}

	/* SELECT sh.SHEPHERD_NAME,
        (SELECT (CASE WHEN COUNT(*) = 1 THEN MAX(s.SHEEP_NAME)
                      ELSE LISTAGG(s.SHEEP_ID || ': ' || s.SHEEP_NAME, CHR(10)) WITHIN GROUP (ORDER BY s.SHEEP_ID)
                 END) as SHEEPS
         FROM SHEEPS s
         WHERE s.SHEEP_SHEPHERD_ID = sh.SHEPHERD_ID
       ) as SHEEPS
FROM SHEPHERDS sh; */
}

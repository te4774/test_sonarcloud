/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record2;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.RequirementVersionLinkTypeDto;
import org.squashtest.tm.service.internal.display.dto.RequirementsLinksTypeDto;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionLinkTypeDisplayDao;

import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_LINK_TYPE;

@Repository
public class RequirementVersionLinkTypeDisplayDaoImpl implements RequirementVersionLinkTypeDisplayDao {

	private final DSLContext dsl;
	private static final String LINK_COUNT = "LINK_COUNT";
	private static final String LINK_TYPE_ID = "LINK_TYPE_ID";

	public RequirementVersionLinkTypeDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public List<RequirementVersionLinkTypeDto> findAll() {
		return dsl.select()
			.from(REQUIREMENT_VERSION_LINK_TYPE)
			.fetchInto(RequirementVersionLinkTypeDto.class);
	}

	public List<RequirementsLinksTypeDto> findAllRequirementsLinksType() {
		SelectHavingStep<Record2<Long, Integer>> requirementVersionLinkCount = getRequirementVersionLinkCount();

		return dsl.select(
			REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID,
			REQUIREMENT_VERSION_LINK_TYPE.ROLE_1,
			REQUIREMENT_VERSION_LINK_TYPE.ROLE_1_CODE,
			REQUIREMENT_VERSION_LINK_TYPE.ROLE_2,
			REQUIREMENT_VERSION_LINK_TYPE.ROLE_2_CODE,
			REQUIREMENT_VERSION_LINK_TYPE.IS_DEFAULT,
			requirementVersionLinkCount.field(LINK_COUNT)
		).from(REQUIREMENT_VERSION_LINK_TYPE)
			.leftJoin(requirementVersionLinkCount).on(requirementVersionLinkCount.field(LINK_TYPE_ID, Long.class).eq(REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID))
			.groupBy(REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID, requirementVersionLinkCount.field(LINK_COUNT))
			.orderBy(REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID.asc())
			.fetchInto(RequirementsLinksTypeDto.class);
	}

	private SelectHavingStep<Record2<Long, Integer>> getRequirementVersionLinkCount() {
		return DSL.select(
			REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID.as(LINK_TYPE_ID),
			DSL.count(REQUIREMENT_VERSION_LINK.LINK_ID).as(LINK_COUNT))
			.from(REQUIREMENT_VERSION_LINK_TYPE)
			.leftJoin(REQUIREMENT_VERSION_LINK).on(REQUIREMENT_VERSION_LINK.LINK_TYPE_ID.eq(REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID))
			.groupBy(REQUIREMENT_VERSION_LINK_TYPE.TYPE_ID);
	}
}

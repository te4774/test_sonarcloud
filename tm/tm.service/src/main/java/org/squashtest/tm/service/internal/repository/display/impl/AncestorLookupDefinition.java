/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.Record3;
import org.jooq.SelectConditionStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.TableField;
import org.squashtest.tm.domain.NodeType;

import java.util.HashMap;
import java.util.Map;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.CLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_CONTENT;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.TCLN_RELATIONSHIP_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_FOLDER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_CONTENT;

/**
 * Declarative enum of ancestor relationship existing in tm trees. Used to fetch ancestors from a node to force open selected nodes in trees.
 * For nodes that can be root, as the consumer of these definitions will use closure_tables, please don't forget the 0 depth, aka the relation of a node to itself, that will be used to fetch libraries.
 * For nodes that cannot be root (ex: Iterations, TestSuites) you don't need to provide auto relation, and of course you must not provide library columns to avoid incorrect joins to libraries.
 */
public enum AncestorLookupDefinition {

	REQUIREMENT_FOLDER_ANCESTORS(NodeType.REQUIREMENT_FOLDER, RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID, RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID, REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID, REQUIREMENT_LIBRARY_CONTENT.CONTENT_ID) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.REQUIREMENT_FOLDER, REQUIREMENT_FOLDER.RLN_ID);
			return relations;
		}
	},
	HIGH_LEVEL_REQUIREMENT_ANCESTORS(NodeType.HIGH_LEVEL_REQUIREMENT, RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID, RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID, REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID, REQUIREMENT_LIBRARY_CONTENT.CONTENT_ID) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.REQUIREMENT_FOLDER, REQUIREMENT_FOLDER.RLN_ID);
			relations.put(NodeType.HIGH_LEVEL_REQUIREMENT, HIGH_LEVEL_REQUIREMENT.RLN_ID);
			return relations;
		}
	},
	REQUIREMENT_ANCESTORS(NodeType.REQUIREMENT, RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID, RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID, REQUIREMENT_LIBRARY_CONTENT.LIBRARY_ID, REQUIREMENT_LIBRARY_CONTENT.CONTENT_ID) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.REQUIREMENT_FOLDER, REQUIREMENT_FOLDER.RLN_ID);
			relations.put(NodeType.HIGH_LEVEL_REQUIREMENT, HIGH_LEVEL_REQUIREMENT.RLN_ID);
			relations.put(NodeType.REQUIREMENT, REQUIREMENT.RLN_ID);
			return relations;
		}

		@Override
		public void addAdditionalJoins(SelectOnConditionStep<Record3<String, Long, Long>> initialJoin, NodeType lookedType) {
			if(NodeType.REQUIREMENT.equals(lookedType)){
				initialJoin.leftJoin(HIGH_LEVEL_REQUIREMENT).on(REQUIREMENT.RLN_ID.eq(HIGH_LEVEL_REQUIREMENT.RLN_ID));
			}
		}

		@Override
		public void addAdditionalCondition(SelectConditionStep<Record3<String, Long, Long>> query, NodeType lookedType) {
			if(NodeType.REQUIREMENT.equals(lookedType)){
				query.and(HIGH_LEVEL_REQUIREMENT.RLN_ID.isNull());
			}
		}
	},
	TEST_CASE_FOLDER_ANCESTORS(NodeType.TEST_CASE_FOLDER, TCLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID, TCLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID, TEST_CASE_LIBRARY_CONTENT.LIBRARY_ID, TEST_CASE_LIBRARY_CONTENT.CONTENT_ID) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.TEST_CASE_FOLDER, TEST_CASE_FOLDER.TCLN_ID);
			return relations;
		}
	},
	TEST_CASE_ANCESTORS(NodeType.TEST_CASE, TCLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID, TCLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID, TEST_CASE_LIBRARY_CONTENT.LIBRARY_ID, TEST_CASE_LIBRARY_CONTENT.CONTENT_ID) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.TEST_CASE_FOLDER, TEST_CASE_FOLDER.TCLN_ID);
			// Relation to itself, even if a test case cannot have child.
			relations.put(NodeType.TEST_CASE, TEST_CASE.TCLN_ID);
			return relations;
		}
	},
	CAMPAIGN_FOLDER_ANCESTORS(NodeType.CAMPAIGN_FOLDER, CLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID, CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID, CAMPAIGN_LIBRARY_CONTENT.LIBRARY_ID, CAMPAIGN_LIBRARY_CONTENT.CONTENT_ID) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.CAMPAIGN_FOLDER, CAMPAIGN_FOLDER.CLN_ID);
			return relations;
		}
	},
	CAMPAIGN_ANCESTORS(NodeType.CAMPAIGN, CLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID, CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID, CAMPAIGN_LIBRARY_CONTENT.LIBRARY_ID, CAMPAIGN_LIBRARY_CONTENT.CONTENT_ID) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.CAMPAIGN_FOLDER, CAMPAIGN_FOLDER.CLN_ID);
			relations.put(NodeType.CAMPAIGN, CAMPAIGN.CLN_ID);
			return relations;
		}
	},
	ITERATION_ANCESTORS(NodeType.ITERATION, CAMPAIGN_ITERATION.CAMPAIGN_ID, CAMPAIGN_ITERATION.ITERATION_ID, null, null) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.CAMPAIGN, CAMPAIGN.CLN_ID);
			return relations;
		}
	},
	TEST_SUITES_ANCESTORS(NodeType.TEST_SUITE, ITERATION_TEST_SUITE.ITERATION_ID, ITERATION_TEST_SUITE.TEST_SUITE_ID, null, null) {
		@Override
		public Map<NodeType, TableField<?, Long>> getRelationshipIds() {
			HashMap<NodeType, TableField<?, Long>> relations = new HashMap<>();
			relations.put(NodeType.ITERATION, ITERATION.ITERATION_ID);
			return relations;
		}
	};

	AncestorLookupDefinition(NodeType nodeType, TableField<?, Long> ancestorField, TableField<?, Long> descendantField, TableField<?, Long> libraryField, TableField<?, Long> libraryContentField) {
		this.nodeType = nodeType;
		this.ancestorField = ancestorField;
		this.descendantField = descendantField;
		this.libraryField = libraryField;
		this.libraryContentField = libraryContentField;
	}

	private final NodeType nodeType;
	private final TableField<?, Long> ancestorField;
	private final TableField<?, Long> descendantField;
	private final TableField<?, Long> libraryField;
	private final TableField<?, Long> libraryContentField;

	public NodeType getNodeType() {
		return nodeType;
	}

	public TableField<?, Long> getAncestorField() {
		return ancestorField;
	}

	public TableField<?, Long> getDescendantField() {
		return descendantField;
	}

	abstract public Map<NodeType, TableField<?, Long>> getRelationshipIds();


	public TableField<?, Long> getLibraryField() {
		return libraryField;
	}

	public TableField<?, Long> getLibraryContentField() {
		return libraryContentField;
	}

	public void addAdditionalJoins(SelectOnConditionStep<Record3<String, Long, Long>> initialJoin, NodeType lookedType){
		//NOOP by default
	}

	public void addAdditionalCondition(SelectConditionStep<Record3<String, Long, Long>> query, NodeType key){
		//NOOP by default
	}
}

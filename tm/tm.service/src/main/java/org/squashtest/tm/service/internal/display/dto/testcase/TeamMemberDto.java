/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.testcase;

public class TeamMemberDto {
	private Long partyId;
	private Boolean active;
	private String firstName;
	private String lastName;
	private String login;
	private String fullName;

	public Long getPartyId() { return partyId; }

	public void setPartyId(Long partyId) { this.partyId = partyId; }

	public String getFirstName() { return firstName; }

	public void setFirstName(String firstName) { this.firstName = firstName; }

	public String getLastName() { return lastName; }

	public void setLastName(String lastName) { this.lastName = lastName; }

	public String getLogin() { return login; }

	public void setLogin(String login) { this.login = login; }

	public Boolean getActive() { return active; }

	public void setActive(Boolean active) { this.active = active; }

	public String getFullName() {
		StringBuilder builder = new StringBuilder();
		if (firstName != null) {
			builder.append(firstName);
			builder.append(" ");
			builder.append(lastName);
			builder.append(" (");
			builder.append(login);
			builder.append(")");
		} else {
			builder.append(lastName);
			builder.append(" (");
			builder.append(login);
			builder.append(")");
		}
		fullName = builder.toString();

		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
}

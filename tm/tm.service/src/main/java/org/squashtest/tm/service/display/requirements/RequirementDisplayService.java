/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.requirements;

import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.requirement.AbstractRequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.HighLevelRequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementFolderDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementLibraryDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementMultiSelectionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionBundleStatsDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionLinkDto;
import org.squashtest.tm.service.internal.display.dto.requirement.VerifyingTestCaseDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

import java.util.List;
import java.util.Map;

public interface RequirementDisplayService {
	AbstractRequirementVersionDto findCurrentVersionView(Long requirementId);

    HighLevelRequirementVersionDto findHighLevelCurrentVersionView(Long requirementId);

	AbstractRequirementVersionDto findVersionView(Long requirementVersionId);

	RequirementVersionBundleStatsDto computeRequirementVersionStatistics(Long requirementVersionId);

	List<MilestoneDto> findBindableMilestones(Long requirementVersionId);

	RequirementLibraryDto findLibrary(Long libraryId);

	RequirementFolderDto getRequirementFolderView(long requirementFolderId);

	List<VerifyingTestCaseDto> findVerifyingTestCasesByRequirementVersionId(Long requirementVersionId);

	List<VerifyingTestCaseDto> findVerifyingTestCasesByHighLvlReqIdForCurrentVersion(Long highLevelRequirementId);
	List<VerifyingTestCaseDto> findVerifyingTestCasesByHighLvlReqVersionId(Long highLevelRequirementVersionId);

	List<RequirementVersionLinkDto> findLinkedRequirementsByRequirementVersionId(Long requirementVersionId);

	Map<String, String> findRequirementVersionNamesByRequirementIds(List<Long> requirementIds);

	GridResponse findCurrentVersionModificationHistoryByRequirementVersionId(Long requirementVersionId, GridRequest requestParam);

	GridResponse findVersionsByRequirementId(Long requirementId, GridRequest request);

	RequirementMultiSelectionDto getRequirementMultiView();

	RequirementVersionDto.LinkedHighLevelRequirementDto findLinkedHighLevelRequirement(Long requirementId);

	Integer countIssuesByHighLvlReqId(Long highLevelReqId);

	Integer countIssuesByRequirementVersionId(Long requirementVersionId);
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.testingstatus;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Intermediary representation used when fetching data to build remote requirement statistic bundles.
 * It is not meant to be used outside this package.
 */
public final class RequirementsByRemoteKeyByServerId {
	final Map<Long, Map<String, List<Long>>> dataMap;

	public RequirementsByRemoteKeyByServerId(Map<Long, Map<String, List<Long>>> dataMap) {
		this.dataMap = dataMap;
	}

	public Set<Long> extractAllSyncedRequirementIds() {
		return dataMap.values().stream()
			.map(Map::values)
			.flatMap(Stream::of)
			.flatMap(Collection::stream)
			.flatMap(Collection::stream)
			.collect(Collectors.toSet());
	}

	public Map<String, List<Long>> getRequirementsByRemoteKeyForServerId(Long serverId) {
		return dataMap.get(serverId);
	}
}

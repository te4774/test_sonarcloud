/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;
import org.squashtest.tm.service.internal.repository.CampaignDeletionDao;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;

@Component
public class JdbcCampaignDeletionHandlerFactory {

	private final DSLContext dslContext;
	private final AttachmentRepository attachmentRepository;
	private final JdbcBatchReorderHelper reorderHelper;
	private final CampaignDeletionDao campaignDeletionDao;
	private final JdbcIterationDeletionHandlerFactory iterationDeletionHandlerFactory;
	private final EntityManager entityManager;

	public JdbcCampaignDeletionHandlerFactory(DSLContext dslContext,
											  EntityManager entityManager,
											  AttachmentRepository attachmentRepository,
											  JdbcBatchReorderHelper reorderHelper,
											  CampaignDeletionDao campaignDeletionDao,
											  JdbcIterationDeletionHandlerFactory iterationDeletionHandlerFactory) {
		this.dslContext = dslContext;
		this.attachmentRepository = attachmentRepository;
		this.reorderHelper = reorderHelper;
		this.campaignDeletionDao = campaignDeletionDao;
		this.iterationDeletionHandlerFactory = iterationDeletionHandlerFactory;
		this.entityManager = entityManager;
	}

	public JdbcCampaignDeletionHandler build(Collection<Long> nodeIds) {
		List<Long>[] separatedIds = campaignDeletionDao.separateFolderFromCampaignIds(new ArrayList<>(nodeIds));
		List<Long> folderIds = separatedIds[0];
		List<Long> campaignIds = separatedIds[1];
		String operationId = UUID.randomUUID().toString();
		Set<Long> iterationIds = findIterations(campaignIds);
		JdbcIterationDeletionHandler iterationDeletionHandler = iterationDeletionHandlerFactory.build(iterationIds, operationId);

		return new JdbcCampaignDeletionHandler(
			folderIds,
			campaignIds,
			dslContext,
			entityManager,
			attachmentRepository,
			reorderHelper,
			iterationDeletionHandler,
			operationId);
	}

	private Set<Long> findIterations(Collection<Long> campaignIds) {
		return dslContext.select(CAMPAIGN_ITERATION.ITERATION_ID)
			.from(CAMPAIGN_ITERATION)
			.where(CAMPAIGN_ITERATION.CAMPAIGN_ID.in(campaignIds))
			.fetchSet(CAMPAIGN_ITERATION.ITERATION_ID);

	}
}

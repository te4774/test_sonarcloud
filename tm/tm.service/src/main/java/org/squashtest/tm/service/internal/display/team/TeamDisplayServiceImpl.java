/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.team;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.project.ProjectPermission;
import org.squashtest.tm.service.display.team.TeamDisplayService;
import org.squashtest.tm.service.internal.display.dto.ProjectPermissionDto;
import org.squashtest.tm.service.internal.display.dto.TeamAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TeamMemberDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.TeamGrid;
import org.squashtest.tm.service.internal.repository.display.TeamDisplayDao;
import org.squashtest.tm.service.project.ProjectsPermissionFinder;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

@Service
@Transactional
public class TeamDisplayServiceImpl implements TeamDisplayService {

	final private DSLContext dsl;
	private TeamDisplayDao teamDisplayDao;
	private ProjectsPermissionFinder permissionFinder;


	@Inject
	public TeamDisplayServiceImpl(DSLContext dsl,
								  TeamDisplayDao teamDisplayDao,
								  ProjectsPermissionFinder permissionFinder) {

		this.dsl = dsl;
		this.teamDisplayDao = teamDisplayDao;
		this.permissionFinder = permissionFinder;
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public GridResponse findAll(GridRequest request) {

		TeamGrid teamGrid = new TeamGrid();
		GridResponse response = teamGrid.getRows(request, dsl);
		response.sanitizeField("description");

		return response;
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public TeamAdminViewDto getTeamView(long teamId) {
		TeamAdminViewDto team = teamDisplayDao.getTeamById(teamId);
		team.setProjectPermissions(getProjectPermissions(teamId));
		team.setMembers(teamDisplayDao.getMembersByTeam(teamId));

		return team;
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public List<ProjectPermissionDto> getProjectPermissions(Long teamId) {
		List<ProjectPermission> projectPermissions = permissionFinder.findProjectPermissionByParty(teamId);

		return projectPermissions.stream().map(projectPermission -> {
			ProjectPermissionDto dto = new ProjectPermissionDto();
			dto.setProjectId(projectPermission.getProject().getId());
			dto.setProjectName(projectPermission.getProject().getName());
			dto.setPermissionGroup(projectPermission.getPermissionGroup());
			return dto;
		}).collect(Collectors.toList());
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public List<TeamMemberDto> getTeamMembers(Long teamId) {
		return teamDisplayDao.getMembersByTeam(teamId);
	}
}

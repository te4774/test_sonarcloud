/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.local;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record4;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.squashtest.tm.service.bugtracker.knownissues.local.LocalKnownIssue;

import static org.squashtest.tm.jooq.domain.Tables.BUGTRACKER_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

/**
 * Base class for finders that return issues with the default shape. Subclasses should only provide a match condition.
 */
public abstract class DefaultLocalKnownIssueFinder extends BaseLocalKnownIssueFinder<Record4<Long, Long, String, String>> {

	protected DefaultLocalKnownIssueFinder(DSLContext dsl) {
		super(dsl);
	}

	@Override
	protected LocalKnownIssue buildIssueFromRecord(Record4<Long, Long, String, String> record) {
		return new LocalKnownIssue(
			record.component1(),
			record.component2(),
			record.component3(),
			LocalKnownIssueFinderHelper.parseLongsAndSortDesc(record.component4()));
	}

	@Override
	protected SelectHavingStep<Record4<Long, Long, String, String>> selectKnownIssues(long entityId) {
		return dsl.select(
			PROJECT.PROJECT_ID,
			ISSUE.BUGTRACKER_ID,
			ISSUE.REMOTE_ISSUE_ID,
			DSL.groupConcatDistinct(EXECUTION.EXECUTION_ID))
			.from(getTable())
			.where(getMatchCondition(entityId))
			// Only shows if source project's current bugtracker is the bugtracker the issue was reported with
			.and(ISSUE.BUGTRACKER_ID.eq(BUGTRACKER_BINDING.BUGTRACKER_ID))
			.groupBy(PROJECT.PROJECT_ID, ISSUE.BUGTRACKER_ID, ISSUE.REMOTE_ISSUE_ID);
	}

	protected abstract Condition getMatchCondition(long entityId);
}

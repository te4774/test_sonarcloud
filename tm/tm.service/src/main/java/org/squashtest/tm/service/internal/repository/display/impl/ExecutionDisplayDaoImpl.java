/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.execution.ExecutionView;
import org.squashtest.tm.service.internal.display.dto.execution.ModificationDuringExecutionView.ExecutionStepActionTestStepPair;
import org.squashtest.tm.service.internal.repository.display.ExecutionDisplayDao;

import java.util.List;
import java.util.Set;

import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.TEST_STEP;

@Repository
public class ExecutionDisplayDaoImpl implements ExecutionDisplayDao {

	private DSLContext dsl;

	public ExecutionDisplayDaoImpl(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public ExecutionView findExecutionView(Long executionId) {
		Record record = dsl.select(
				EXECUTION.EXECUTION_ID,
				EXECUTION.NAME,
				EXECUTION.PREREQUISITE,
				EXECUTION.DESCRIPTION,
				EXECUTION.ATTACHMENT_LIST_ID,
				EXECUTION.TCLN_ID,
				EXECUTION.TC_DESCRIPTION,
				EXECUTION.IMPORTANCE,
				EXECUTION.TC_STATUS,
				EXECUTION.TC_NAT_LABEL,
				EXECUTION.TC_NAT_ICON_NAME,
				EXECUTION.TC_TYP_LABEL,
				EXECUTION.TC_TYP_ICON_NAME,
				EXECUTION.DATASET_LABEL,
				EXECUTION.EXECUTION_MODE,
				EXECUTION.LAST_EXECUTED_ON,
				EXECUTION.LAST_EXECUTED_BY,
				EXECUTION.EXECUTION_STATUS,
				ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER,
				CAMPAIGN_LIBRARY_NODE.PROJECT_ID,
				TEST_AUTOMATION_SERVER.KIND,
				AUTOMATED_EXECUTION_EXTENDER.RESULT_URL,
				AUTOMATED_EXECUTION_EXTENDER.RESULT_SUMMARY,
				ITEM_TEST_PLAN_LIST.ITERATION_ID,
				KEYWORD_EXECUTION.EXECUTION_ID,
				SCRIPTED_EXECUTION.EXECUTION_ID,
				ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID
		)
				.from(EXECUTION)
				.naturalJoin(ITEM_TEST_PLAN_EXECUTION)
				.innerJoin(ITERATION_TEST_PLAN_ITEM).using(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID)
				.naturalJoin(ITEM_TEST_PLAN_LIST)
				.innerJoin(ITERATION).using(ITERATION.ITERATION_ID)
				.innerJoin(CAMPAIGN_ITERATION).using(ITERATION.ITERATION_ID)
				.innerJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN_LIBRARY_NODE.CLN_ID.eq(CAMPAIGN_ITERATION.CAMPAIGN_ID))
				.leftJoin(AUTOMATED_EXECUTION_EXTENDER).on(AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
				.leftJoin(PROJECT).on(PROJECT.PROJECT_ID.eq(CAMPAIGN_LIBRARY_NODE.PROJECT_ID))
				.leftJoin(TEST_AUTOMATION_SERVER).on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(PROJECT.TA_SERVER_ID))
				.leftJoin(KEYWORD_EXECUTION).on(KEYWORD_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
				.leftJoin(SCRIPTED_EXECUTION).on(SCRIPTED_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
				.where(EXECUTION.EXECUTION_ID.eq(executionId))
				.fetchOne();

		Long count = dsl.selectCount().from(ITEM_TEST_PLAN_EXECUTION)
			.where(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(record.get(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID)))
			.fetchOneInto(Long.class);

		ExecutionView executionView = new ExecutionView();
		executionView.setId(record.get(EXECUTION.EXECUTION_ID));
		executionView.setName(record.get(EXECUTION.NAME));
		executionView.setExecutionOrder(record.get(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER));
		executionView.setProjectId(record.get(CAMPAIGN_LIBRARY_NODE.PROJECT_ID));
		executionView.setPrerequisite(record.get(EXECUTION.PREREQUISITE));
		executionView.setTcDescription(record.get(EXECUTION.TC_DESCRIPTION));
		executionView.setTcImportance(record.get(EXECUTION.IMPORTANCE));
		executionView.setTcStatus(record.get(EXECUTION.TC_STATUS));
		executionView.setTcNatLabel(record.get(EXECUTION.TC_NAT_LABEL));
		executionView.setTcNatIconName(record.get(EXECUTION.TC_NAT_ICON_NAME));
		executionView.setTcTypeLabel(record.get(EXECUTION.TC_TYP_LABEL));
		executionView.setTcTypeIconName(record.get(EXECUTION.TC_TYP_ICON_NAME));
		executionView.setDatasetLabel(record.get(EXECUTION.DATASET_LABEL));
		executionView.setAttachmentListId(record.get(EXECUTION.ATTACHMENT_LIST_ID));
		executionView.setTestCaseId(record.get(EXECUTION.TCLN_ID));
		executionView.setExecutionMode(record.get(EXECUTION.EXECUTION_MODE));
		executionView.setLastExecutedOn(record.get(EXECUTION.LAST_EXECUTED_ON));
		executionView.setLastExecutedBy(record.get(EXECUTION.LAST_EXECUTED_BY));
		executionView.setExecutionStatus(record.get(EXECUTION.EXECUTION_STATUS));
		executionView.setTestAutomationServerKind(record.get(TEST_AUTOMATION_SERVER.KIND));
		executionView.setAutomatedExecutionResultUrl(record.get(AUTOMATED_EXECUTION_EXTENDER.RESULT_URL));
		executionView.setAutomatedExecutionResultSummary(record.get(AUTOMATED_EXECUTION_EXTENDER.RESULT_SUMMARY));
		// Description = comment in execution...
		executionView.setComment(record.get(EXECUTION.DESCRIPTION));
		executionView.setIterationId(record.get(ITEM_TEST_PLAN_LIST.ITERATION_ID));
		executionView.setKeywordExecutionId(record.get(KEYWORD_EXECUTION.EXECUTION_ID));
		executionView.setScriptedExecutionId(record.get(SCRIPTED_EXECUTION.EXECUTION_ID));
		executionView.setTestPlanItemId(record.get(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID));
		executionView.setExecutionsCount(count);
		return executionView;
	}

	@Override
	public List<ExecutionStepActionTestStepPair> findExecutionStepActionStepPairs(Long executionId) {
		return this.dsl.select(EXECUTION_STEP.EXECUTION_STEP_ID, TEST_STEP.TEST_STEP_ID)
			.from(EXECUTION_EXECUTION_STEPS)
			.innerJoin(EXECUTION_STEP).on(EXECUTION_STEP.EXECUTION_STEP_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID))
			.innerJoin(TEST_STEP).on(EXECUTION_STEP.TEST_STEP_ID.eq(TEST_STEP.TEST_STEP_ID))
			.where(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(executionId))
			.orderBy(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ORDER.asc())
			.fetchInto(ExecutionStepActionTestStepPair.class);
	}

	@Override
	public Set<Long> findAllTestCaseInExecution(Long executionId) {
		return this.dsl.selectDistinct(TEST_CASE_STEPS.TEST_CASE_ID)
			.from(EXECUTION_EXECUTION_STEPS)
			.innerJoin(EXECUTION_STEP).on(EXECUTION_STEP.EXECUTION_STEP_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID))
			.innerJoin(TEST_STEP).on(EXECUTION_STEP.TEST_STEP_ID.eq(TEST_STEP.TEST_STEP_ID))
			.innerJoin(TEST_CASE_STEPS).on(TEST_STEP.TEST_STEP_ID.eq(TEST_CASE_STEPS.STEP_ID))
			.where(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(executionId))
			.fetchSet(TEST_CASE_STEPS.TEST_CASE_ID);
	}
}

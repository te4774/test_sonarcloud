/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.local;

import org.apache.commons.lang3.StringUtils;
import org.jooq.SortField;
import org.squashtest.tm.core.foundation.collection.PagingAndSorting;
import org.squashtest.tm.core.foundation.collection.SortOrder;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.ISSUE;

public abstract class LocalKnownIssueFinderHelper {
	public static List<Long> parseLongsAndSortAsc(String s) {
		List<Long> longs = parseLongs(s);
		longs.sort(Long::compare);
		return longs;
	}

	public static List<Long> parseLongsAndSortDesc(String s) {
		List<Long> longs = parseLongsAndSortAsc(s);
		Collections.reverse(longs);
		return longs;
	}

	public static List<Long> parseLongs(String s) {
		if (StringUtils.isBlank(s)) {
			return Collections.emptyList();
		}

		return Arrays.stream(StringUtils.split(s, ","))
			.map(Long::parseLong)
			.collect(Collectors.toList());
	}

	public static SortField<String> getOrderField(PagingAndSorting paging) {
		return paging.getSortOrder().equals(SortOrder.ASCENDING) ?
			ISSUE.REMOTE_ISSUE_ID.asc() : ISSUE.REMOTE_ISSUE_ID.desc();
	}
}

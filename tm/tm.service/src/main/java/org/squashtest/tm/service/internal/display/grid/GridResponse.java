/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid;

import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;

import java.util.ArrayList;
import java.util.List;

public class GridResponse {

	private long count;
	private String idAttribute;
	private List<DataRow> dataRows = new ArrayList<>();

	public void sanitizeField(String fieldId) {
		dataRows.forEach(dataRow -> {
			Object fieldValue = dataRow.getData().get(fieldId);
			if (fieldValue != null) {
				String sanitized = HTMLCleanupUtils.htmlToTrimmedText(fieldValue.toString());
				dataRow.getData().put(fieldId, sanitized);
			}
		});
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public List<DataRow> getDataRows() {
		return dataRows;
	}

	public void setDataRows(List<DataRow> dataRows) {
		this.dataRows = dataRows;
	}

	public void addDataRow(DataRow dataRow) {
		dataRows.add(dataRow);
	}

	public String getIdAttribute() {
		return idAttribute;
	}

	public void setIdAttribute(String idAttribute) {
		this.idAttribute = idAttribute;
	}
}

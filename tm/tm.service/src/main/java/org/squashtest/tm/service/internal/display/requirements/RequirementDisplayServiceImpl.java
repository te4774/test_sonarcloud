/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.requirements;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.users.PartyPreference;
import org.squashtest.tm.domain.users.preferences.CorePartyPreference;
import org.squashtest.tm.service.customreport.CustomReportDashboardService;
import org.squashtest.tm.service.display.requirements.RequirementDisplayService;
import org.squashtest.tm.service.display.requirements.RequirementPathFinderService;
import org.squashtest.tm.service.display.testcase.TestCasePathFinderService;
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.RequirementVersionKnownIssueFinder;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.dto.requirement.AbstractRequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.HighLevelRequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementFolderDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementLibraryDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementMultiSelectionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionBundleStatsDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionLinkDto;
import org.squashtest.tm.service.internal.display.dto.requirement.VerifyingTestCaseDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.display.AttachmentDisplayDao;
import org.squashtest.tm.service.internal.repository.display.CustomFieldValueDisplayDao;
import org.squashtest.tm.service.internal.repository.display.MilestoneDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ModificationHistoryDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RequirementDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionDisplayDao;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionLinkDisplayDao;
import org.squashtest.tm.service.internal.repository.display.VerifyingTestCaseDisplayDao;
import org.squashtest.tm.service.requirement.HighLevelRequirementService;
import org.squashtest.tm.service.requirement.RequirementHelper;
import org.squashtest.tm.service.requirement.RequirementStatisticsService;
import org.squashtest.tm.service.user.PartyPreferenceService;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_REQUIREMENT_FOLDER_OR_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.READ_REQUIREMENT_OR_ROLE_ADMIN;

@Service
@Transactional(readOnly = true)
public class RequirementDisplayServiceImpl implements RequirementDisplayService {

	private final RequirementVersionDisplayDao requirementVersionDisplayDao;
	private final CustomFieldValueDisplayDao customFieldValueDisplayDao;
	private final AttachmentDisplayDao attachmentDisplayDao;
	private final MilestoneDisplayDao milestoneDisplayDao;
	private final RequirementDisplayDao requirementDisplayDao;
	private final VerifyingTestCaseDisplayDao verifyingTestCaseDisplayDao;
	private final RequirementVersionLinkDisplayDao requirementVersionLinkDisplayDao;
	private final RequirementStatisticsService requirementStatisticsService;
	private final ModificationHistoryDisplayDao modificationHistoryDisplayDao;
	private final RequirementVersionKnownIssueFinder requirementVersionKnownIssueFinder;
	private final CustomReportDashboardService customReportDashboardService;
	private final PartyPreferenceService partyPreferenceService;
	private final HighLevelRequirementService highLevelRequirementService;
	private final TestCasePathFinderService testCasePathService;
	private final RequirementPathFinderService requirementsPathService;
	private final RequirementHelper requirementHelper;
	private final RequirementDao requirementDao;

	public RequirementDisplayServiceImpl(RequirementVersionDisplayDao requirementVersionDisplayDao,
										 CustomFieldValueDisplayDao customFieldValueDisplayDao,
										 AttachmentDisplayDao attachmentDisplayDao,
										 MilestoneDisplayDao milestoneDisplayDao,
										 RequirementDisplayDao requirementDisplayDao,
										 VerifyingTestCaseDisplayDao verifyingTestCaseDisplayDao,
										 RequirementVersionLinkDisplayDao requirementVersionLinkDisplayDao,
										 ModificationHistoryDisplayDao modificationHistoryDisplayDao,
										 RequirementStatisticsService requirementStatisticsService,
										 RequirementVersionKnownIssueFinder requirementVersionKnownIssueFinder,
										 CustomReportDashboardService customReportDashboardService,
										 PartyPreferenceService partyPreferenceService,
										 HighLevelRequirementService highLevelRequirementService,
										 TestCasePathFinderService testCasePathService,
										 RequirementPathFinderService requirementsPathService,
										 RequirementHelper requirementHelper,
										 RequirementDao requirementDao) {
		this.requirementVersionDisplayDao = requirementVersionDisplayDao;
		this.customFieldValueDisplayDao = customFieldValueDisplayDao;
		this.attachmentDisplayDao = attachmentDisplayDao;
		this.milestoneDisplayDao = milestoneDisplayDao;
		this.requirementDisplayDao = requirementDisplayDao;
		this.verifyingTestCaseDisplayDao = verifyingTestCaseDisplayDao;
		this.requirementVersionLinkDisplayDao = requirementVersionLinkDisplayDao;
		this.requirementStatisticsService = requirementStatisticsService;
		this.modificationHistoryDisplayDao = modificationHistoryDisplayDao;
		this.requirementVersionKnownIssueFinder = requirementVersionKnownIssueFinder;
		this.customReportDashboardService = customReportDashboardService;
		this.partyPreferenceService = partyPreferenceService;
		this.highLevelRequirementService = highLevelRequirementService;
		this.testCasePathService = testCasePathService;
		this.requirementsPathService = requirementsPathService;
		this.requirementHelper = requirementHelper;
		this.requirementDao = requirementDao;
	}

	@PreAuthorize(READ_REQUIREMENT_OR_ROLE_ADMIN)
	@Override
	public AbstractRequirementVersionDto findCurrentVersionView(Long requirementId) {
		Long currentVersionId = this.requirementVersionDisplayDao.findCurrentRequirementVersions(requirementId);
		if (requirementVersionIsLinkedToHighLevelRequirement(currentVersionId)) {
			return createHighLevelRequirementVersionDto(currentVersionId);
		} else {
			return createRequirementVersionDto(currentVersionId);
		}
	}

	@PreAuthorize(READ_REQUIREMENT_OR_ROLE_ADMIN)
	@Override
	public HighLevelRequirementVersionDto findHighLevelCurrentVersionView(Long requirementId) {
		Long currentVersionId = this.requirementVersionDisplayDao.findCurrentRequirementVersions(requirementId);
		return createHighLevelRequirementVersionDto(currentVersionId);
	}

	@PreAuthorize("hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion', 'READ')"
		+ OR_HAS_ROLE_ADMIN)
	@Override
	public AbstractRequirementVersionDto findVersionView(Long requirementVersionId) {
		if (requirementVersionIsLinkedToHighLevelRequirement(requirementVersionId)) {
			return createHighLevelRequirementVersionDto(requirementVersionId);
		} else {
			return createRequirementVersionDto(requirementVersionId);
		}
	}

	@PreAuthorize("hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion', 'READ')"
		+ OR_HAS_ROLE_ADMIN)
	@Override
	public RequirementVersionBundleStatsDto computeRequirementVersionStatistics(Long requirementVersionId) {
		if (requirementVersionIsLinkedToHighLevelRequirement(requirementVersionId)) {
			return requirementStatisticsService.findCoveragesStatsByHighLvlReqVersionId(requirementVersionId);
		} else {
			return requirementStatisticsService.findCoveragesStatsByRequirementVersionId(requirementVersionId);
		}
	}

	private boolean requirementVersionIsLinkedToHighLevelRequirement(Long requirementVersionId) {
		return requirementDao.isHighLevelRequirementVersion(requirementVersionId);
	}

	@Override
	public List<VerifyingTestCaseDto> findVerifyingTestCasesByHighLvlReqIdForCurrentVersion(Long highLvlReqId) {
		Long highLvlReqCurrentVersionId = requirementDao.findRequirementCurrentVersionIdFromRequirementId(highLvlReqId);
		return findVerifyingTestCasesByHighLvlReqVersionId(highLvlReqCurrentVersionId);
	}

	@PreAuthorize("hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion', 'READ')"
			+ OR_HAS_ROLE_ADMIN)
	@Override
	public List<VerifyingTestCaseDto> findVerifyingTestCasesByHighLvlReqVersionId(Long requirementVersionId) {
		List<VerifyingTestCaseDto> verifyingTestCaseDtoList = verifyingTestCaseDisplayDao.findByHighLevelRequirementVersionId(requirementVersionId);
		verifyingTestCaseDtoList.forEach(verifyingTestCaseDto ->
			verifyingTestCaseDto.setPath(this.testCasePathService.buildTestCasePath(verifyingTestCaseDto.getId(), verifyingTestCaseDto.getProjectName())));
		return verifyingTestCaseDtoList;
	}

	@Override
	public Integer countIssuesByHighLvlReqId(Long highLevelReqId) {
		Long highLvlReqVersionId = requirementHelper.findRequirementVersionIdFromRequirementId(highLevelReqId);
		return requirementVersionKnownIssueFinder.countKnownIssues(highLvlReqVersionId);
	}

	@Override
	public Integer countIssuesByRequirementVersionId(Long requirementVersionId) {
		return requirementVersionKnownIssueFinder.countKnownIssues(requirementVersionId);
	}

	private RequirementVersionDto createRequirementVersionDto(Long requirementVersionId) {
		RequirementVersionDto requirementVersion = requirementVersionDisplayDao.findRequirementVersion(requirementVersionId);
		appendPathToLinkedHighLevelRequirement(requirementVersion);
		appendRequirementCommonData(requirementVersion);
		requirementVersion.setVerifyingTestCases(findVerifyingTestCasesByRequirementVersionId((requirementVersionId)));
		requirementVersion.setRequirementVersionLinks(findLinkedRequirementsByRequirementVersionId(requirementVersionId));
		Long requirementId = requirementDao.findRequirementIdFromVersionId(requirementVersionId);
		requirementVersion.setChildOfRequirement(requirementDao.checkIfRequirementIsChild(requirementId));
		return requirementVersion;
	}

	private void appendPathToLinkedHighLevelRequirement(RequirementVersionDto requirementVersion) {
		RequirementVersionDto.LinkedHighLevelRequirementDto linkedHighLevelRequirement = requirementVersion.getLinkedHighLevelRequirement();
		if (linkedHighLevelRequirement != null) {
			String path = requirementsPathService.buildRequirementLinkPath(linkedHighLevelRequirement.getRequirementId(), linkedHighLevelRequirement.getProjectName());
			linkedHighLevelRequirement.setPath(path);
		}
		requirementVersion.setLinkedHighLevelRequirement(linkedHighLevelRequirement);
	}

	private HighLevelRequirementVersionDto createHighLevelRequirementVersionDto(Long requirementVersionId) {
		HighLevelRequirementVersionDto requirementVersion = requirementVersionDisplayDao.findHighLevelRequirementVersion(requirementVersionId);
		appendRequirementCommonData(requirementVersion);
		requirementVersion.setLowLevelRequirements(this.highLevelRequirementService.findLinkedLowLevelRequirements(requirementVersion.getRequirementId()));
		requirementVersion.setVerifyingTestCases(findVerifyingTestCasesByHighLvlReqVersionId(requirementVersionId));
		requirementVersion.setRequirementVersionLinks(findLinkedRequirementsByRequirementVersionId(requirementVersionId));
		return requirementVersion;
	}

	private void appendRequirementCommonData(AbstractRequirementVersionDto requirementVersion) {
		Long requirementVersionId = requirementVersion.getId();
		requirementVersion.setCustomFieldValues(customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.REQUIREMENT_VERSION, requirementVersionId));
		requirementVersion.setAttachmentList(attachmentDisplayDao.findAttachmentListById(requirementVersion.getAttachmentListId()));
		requirementVersion.setMilestones(milestoneDisplayDao.getMilestonesByRequirementVersionId(requirementVersionId));
		requirementVersion.setBindableMilestones(milestoneDisplayDao.getMilestonesDtoAssociableToRequirementVersion(requirementVersionId));
	}

	@PreAuthorize("hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion', 'WRITE')"
		+ OR_HAS_ROLE_ADMIN)
	@Override
	public List<MilestoneDto> findBindableMilestones(Long requirementVersionId) {
		return milestoneDisplayDao.getMilestonesDtoAssociableToRequirementVersion(requirementVersionId);
	}

	@Override
	public RequirementLibraryDto findLibrary(Long libraryId) {
		RequirementLibraryDto libraryDto = requirementDisplayDao.findRequirementLibraryDtoById(libraryId);
		libraryDto.setAttachmentList(attachmentDisplayDao.findAttachmentListById(libraryDto.getAttachmentListId()));
		appendFavoriteDashboardInformation(libraryDto);
		return libraryDto;
	}

	private void appendFavoriteDashboardInformation(RequirementLibraryDto libraryDto) {
		libraryDto.setCanShowFavoriteDashboard(customReportDashboardService.canShowDashboardInWorkspace(Workspace.REQUIREMENT));
		libraryDto.setShouldShowFavoriteDashboard(customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.REQUIREMENT));
		PartyPreference preference = partyPreferenceService
			.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_REQUIREMENT.getPreferenceKey());
		if (preference != null) {
			Long dashboardId = Long.valueOf(preference.getPreferenceValue());
			libraryDto.setFavoriteDashboardId(dashboardId);
		}
	}

	@PreAuthorize(READ_REQUIREMENT_FOLDER_OR_ROLE_ADMIN)
	@Override
	public RequirementFolderDto getRequirementFolderView(long requirementFolderId) {
		RequirementFolderDto requirementFolder = requirementDisplayDao.getRequirementFolderDtoById(requirementFolderId);
		requirementFolder.setAttachmentList(attachmentDisplayDao.findAttachmentListById(requirementFolder.getAttachmentListId()));
		requirementFolder.setCustomFieldValues(customFieldValueDisplayDao.findCustomFieldValues(BindableEntity.REQUIREMENT_FOLDER, requirementFolderId));
		appendFavoriteDashboardInformation(requirementFolder);
		return requirementFolder;
	}

	private void appendFavoriteDashboardInformation(RequirementFolderDto requirementFolderDto) {
		requirementFolderDto.setCanShowFavoriteDashboard(customReportDashboardService.canShowDashboardInWorkspace(Workspace.REQUIREMENT));
		requirementFolderDto.setShouldShowFavoriteDashboard(customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.REQUIREMENT));
		PartyPreference preference = partyPreferenceService
			.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_REQUIREMENT.getPreferenceKey());
		if (preference != null) {
			Long dashboardId = Long.valueOf(preference.getPreferenceValue());
			requirementFolderDto.setFavoriteDashboardId(dashboardId);
		}
	}

	@PreAuthorize("hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion', 'READ')"
		+ OR_HAS_ROLE_ADMIN)
	@Override
	public List<VerifyingTestCaseDto> findVerifyingTestCasesByRequirementVersionId(Long requirementVersionId) {
		List<VerifyingTestCaseDto> verifyingTestCasesDto = verifyingTestCaseDisplayDao.findByRequirementVersionIds(new HashSet<>(Collections.singletonList(requirementVersionId)));
		for (VerifyingTestCaseDto testCase : verifyingTestCasesDto) {
			testCase.setPath(this.testCasePathService.buildTestCasePath(testCase.getId(), testCase.getProjectName()));
		}
		return verifyingTestCasesDto;
	}

	@PreAuthorize("hasPermission(#requirementVersionId, 'org.squashtest.tm.domain.requirement.RequirementVersion', 'READ')"
		+ OR_HAS_ROLE_ADMIN)
	@Override
	public List<RequirementVersionLinkDto> findLinkedRequirementsByRequirementVersionId(Long requirementVersionId) {
		List<RequirementVersionLinkDto> requirementsVersionLinkDtos = requirementVersionLinkDisplayDao.findLinksByRequirementVersionId(requirementVersionId);
		for (RequirementVersionLinkDto requirementVersionLinkDto : requirementsVersionLinkDtos) {
			requirementVersionLinkDto.setPath(this.requirementsPathService.buildRequirementLinkPath(requirementVersionLinkDto.getRequirementId(), requirementVersionLinkDto.getProjectName()));
		}
		return requirementsVersionLinkDtos;
	}

	@Override
	public Map<String, String> findRequirementVersionNamesByRequirementIds(List<Long> requirementIds) {
		List<String> reqVersionNames = requirementDisplayDao.findRequirementVersionNamesByRequirementIds(requirementIds);
		Map<String, String> versionNames = new HashMap<>();
		versionNames.put("versionName", String.join(",", reqVersionNames));
		return versionNames;
	}

	@Override
	public GridResponse findCurrentVersionModificationHistoryByRequirementVersionId(Long requirementVersionId, GridRequest request) {
		return modificationHistoryDisplayDao.findGridByRequirementVersionId(requirementVersionId, request);
	}

	@Override
	public GridResponse findVersionsByRequirementId(Long requirementId, GridRequest request) {
		return requirementDisplayDao.findVersionsById(requirementId, request);
	}

	@Override
	public RequirementMultiSelectionDto getRequirementMultiView() {
		RequirementMultiSelectionDto requirementMultiSelectionDto = new RequirementMultiSelectionDto();
		requirementMultiSelectionDto.setCanShowFavoriteDashboard(customReportDashboardService.canShowDashboardInWorkspace(Workspace.REQUIREMENT));
		requirementMultiSelectionDto.setShouldShowFavoriteDashboard(customReportDashboardService.shouldShowFavoriteDashboardInWorkspace(Workspace.REQUIREMENT));
		PartyPreference preference = partyPreferenceService
			.findPreferenceForCurrentUser(CorePartyPreference.FAVORITE_DASHBOARD_REQUIREMENT.getPreferenceKey());
		if (preference != null) {
			Long dashboardId = Long.valueOf(preference.getPreferenceValue());
			requirementMultiSelectionDto.setFavoriteDashboardId(dashboardId);
		}
		return requirementMultiSelectionDto;
	}

	@Override
	@PreAuthorize(READ_REQUIREMENT_OR_ROLE_ADMIN)
	public RequirementVersionDto.LinkedHighLevelRequirementDto findLinkedHighLevelRequirement(Long requirementId) {
		return requirementVersionDisplayDao.findLinkedHighLevelRequirement(requirementId);
	}
}

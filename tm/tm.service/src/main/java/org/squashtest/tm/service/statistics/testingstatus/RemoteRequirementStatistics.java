/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.testingstatus;

import org.squashtest.tm.service.statistics.requirement.RequirementVersionBundleStat;

import java.util.Collection;
import java.util.stream.Collectors;

public class RemoteRequirementStatistics {

	public final Rate redactionRate;
	public final Rate verificationRate;
	public final Rate validationRate;

	public static RemoteRequirementStatistics fromSimpleStats(RequirementVersionBundleStat.SimpleRequirementStats simpleRequirementStats) {
		return new RemoteRequirementStatistics(simpleRequirementStats);
	}

	public static RemoteRequirementStatistics merging(Collection<RemoteRequirementStatistics> statisticsToMerge) {
		return new RemoteRequirementStatistics(statisticsToMerge);
	}

	public static RemoteRequirementStatistics fromRates(Rate redactionRate, Rate verificationRate, Rate validationRate) {
		return new RemoteRequirementStatistics(redactionRate, verificationRate, validationRate);
	}

	private RemoteRequirementStatistics(RequirementVersionBundleStat.SimpleRequirementStats simpleRequirementStats) {
		this.redactionRate = new Rate(simpleRequirementStats.getRedactedTestCase(), simpleRequirementStats.getAllTestCaseCount());
		this.verificationRate = new Rate(simpleRequirementStats.getVerifiedTestCase(), simpleRequirementStats.getPlannedTestCase());
		this.validationRate = new Rate(simpleRequirementStats.getValidatedTestCase(), simpleRequirementStats.getExecutedTestCase());
	}

	private RemoteRequirementStatistics(Collection<RemoteRequirementStatistics> statisticsToMerge) {
		this.redactionRate = Rate.merging(statisticsToMerge.stream().map(stat -> stat.redactionRate).collect(Collectors.toSet()));
		this.verificationRate = Rate.merging(statisticsToMerge.stream().map(stat -> stat.verificationRate).collect(Collectors.toSet()));
		this.validationRate = Rate.merging(statisticsToMerge.stream().map(stat -> stat.validationRate).collect(Collectors.toSet()));
	}

	private RemoteRequirementStatistics(Rate redactionRate, Rate verificationRate, Rate validationRate) {
		this.redactionRate = redactionRate;
		this.verificationRate = verificationRate;
		this.validationRate = validationRate;
	}

	public static class Rate {
		public final int totalCount;
		public final int matchingCount;
		public final int percentRoundedRate;
		public final double normalizedRate;

		public static Rate from(int matchingCount, int totalCount) {
			return new Rate(matchingCount, totalCount);
		}

		public static Rate merging(Collection<Rate> ratesToMerge) {
			int totalSum = ratesToMerge.stream()
				.map(r -> r.totalCount)
				.reduce(0, Integer::sum);

			int matchingSum = ratesToMerge.stream()
				.map(r -> r.matchingCount)
				.reduce(0, Integer::sum);

			return from(matchingSum, totalSum);
		}

		private Rate(int matchingCount, int totalCount) {
			this.totalCount = totalCount;
			this.matchingCount = matchingCount;

			if (totalCount > 0) {
				this.normalizedRate = (float) matchingCount / (float) totalCount;
			} else {
				this.normalizedRate = 0d;
			}
			this.percentRoundedRate = (int)Math.round(this.normalizedRate * 100.0);
		}
	}
}

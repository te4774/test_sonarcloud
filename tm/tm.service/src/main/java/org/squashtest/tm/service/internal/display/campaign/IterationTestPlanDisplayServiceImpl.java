/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.campaign;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.service.display.campaign.IterationTestPlanDisplayService;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.campaign.IterationTestPlanItemExecutionGrid;

import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

@Service
@Transactional(readOnly = true)
public class IterationTestPlanDisplayServiceImpl implements IterationTestPlanDisplayService {

	private static final String CAN_READ_ITERATION_TEST_PLAN_ITEM = "hasPermission(#itemId, 'org.squashtest.tm.domain.campaign.IterationTestPlanItem', 'READ')" + OR_HAS_ROLE_ADMIN;

	private final DSLContext dslContext;

	private final ExecutionSuccessRateCalculator rateCalculator;

	public IterationTestPlanDisplayServiceImpl(DSLContext dslContext,
											   ExecutionSuccessRateCalculator rateCalculator) {
		this.dslContext = dslContext;
		this.rateCalculator = rateCalculator;
	}

	@PreAuthorize(CAN_READ_ITERATION_TEST_PLAN_ITEM)
	@Override
	public GridResponse getITPIExecutions(Long iterationId, Long itemId, GridRequest gridRequest) {
		IterationTestPlanItemExecutionGrid grid = new IterationTestPlanItemExecutionGrid(iterationId, itemId);
		GridResponse gridResponse = grid.getRows(gridRequest, this.dslContext);
		rateCalculator.appendSuccessRate(gridResponse);
		return gridResponse;
	}
}

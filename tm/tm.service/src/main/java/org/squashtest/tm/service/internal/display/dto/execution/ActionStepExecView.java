/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.execution;

import org.squashtest.tm.service.internal.display.dto.AttachmentDto;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.AttachmentListHolderDto;
import org.squashtest.tm.service.internal.display.dto.CufHolderDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;
import org.squashtest.tm.service.internal.display.dto.RequirementVersionCoverageDto;

import java.util.ArrayList;
import java.util.List;

//ACTION_STEP_TEST_CASE_ID

/**
 * DTO for the modif during exec ActionStepView
 */
public class ActionStepExecView implements CufHolderDto, AttachmentListHolderDto {
	private Long id;
	private Long projectId;
	private Long actionStepTestCaseId;
	private String actionStepTestCaseName;
	private String actionStepTestCaseReference;
	private Long executionTestCaseId;
	private String executionTestCaseName;
	private String executionTestCaseReference;
	private String action;
	private String expectedResult;
	private List<CustomFieldValueDto> customFieldValues = new ArrayList<>();
	private AttachmentListDto attachmentList = new AttachmentListDto();
	private List<RequirementVersionCoverageDto> coverages = new ArrayList<>();

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActionStepTestCaseName() {
		return actionStepTestCaseName;
	}

	public void setActionStepTestCaseName(String actionStepTestCaseName) {
		this.actionStepTestCaseName = actionStepTestCaseName;
	}

	public Long getActionStepTestCaseId() {
		return actionStepTestCaseId;
	}

	public void setActionStepTestCaseId(Long actionStepTestCaseId) {
		this.actionStepTestCaseId = actionStepTestCaseId;
	}

	public Long getExecutionTestCaseId() {
		return executionTestCaseId;
	}

	public void setExecutionTestCaseId(Long executionTestCaseId) {
		this.executionTestCaseId = executionTestCaseId;
	}

	public String getExecutionTestCaseName() {
		return executionTestCaseName;
	}

	public void setExecutionTestCaseName(String executionTestCaseName) {
		this.executionTestCaseName = executionTestCaseName;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getExpectedResult() {
		return expectedResult;
	}

	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}

	public List<CustomFieldValueDto> getCustomFieldValues() {
		return customFieldValues;
	}

	@Override
	public void setCustomFieldValues(List<CustomFieldValueDto> customFieldValues) {
		this.customFieldValues = customFieldValues;
	}

	public void setAttachmentListId(Long attachmentListId) {
		this.attachmentList.setId(attachmentListId);
	}

	public Long getAttachmentListId() {
		return this.getAttachmentList().getId();
	}

	public void addAttachments(List<AttachmentDto> attachments) {
		this.attachmentList.addAttachments(attachments);
	}

	public AttachmentListDto getAttachmentList() {
		return attachmentList;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getActionStepTestCaseReference() {
		return actionStepTestCaseReference;
	}

	public void setActionStepTestCaseReference(String actionStepTestCaseReference) {
		this.actionStepTestCaseReference = actionStepTestCaseReference;
	}

	public String getExecutionTestCaseReference() {
		return executionTestCaseReference;
	}

	public void setExecutionTestCaseReference(String executionTestCaseReference) {
		this.executionTestCaseReference = executionTestCaseReference;
	}

	public List<RequirementVersionCoverageDto> getCoverages() {
		return coverages;
	}

	public void setCoverages(List<RequirementVersionCoverageDto> coverages) {
		this.coverages = coverages;
	}
}

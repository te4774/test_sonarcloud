/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableValue;

import java.util.Collection;
import java.util.List;

public interface EnvironmentVariableValueDao extends JpaRepository<EnvironmentVariableValue, Long> {
	@Query("from EnvironmentVariableValue where binding.id in (:bindingIds)")
	Collection<EnvironmentVariableValue> findAllByBindingIds(@Param("bindingIds") Collection<Long> bindingIds);

	@Query("from EnvironmentVariableValue where binding = :binding and boundEntityType = :entityType and boundEntityId = :entityId")
	EnvironmentVariableValue findSingleEnvironmentVariableValue(@Param("binding") EnvironmentVariableBinding binding,
																@Param("entityType") EVBindableEntity entityType,
																@Param("entityId") Long entityId);
	@Query
	List<EnvironmentVariableValue> findAllByBoundEntityIdAndBoundEntityType(Long entityId, EVBindableEntity entityType);
	@Query("from EnvironmentVariableValue where binding in (:bindings) and value in (:values)")
	List<EnvironmentVariableValue> findAllByBindingsAndValues(@Param("bindings") List<EnvironmentVariableBinding> bindings,
															  @Param("values") List<String> values);
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.administration;

import org.jooq.Field;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.service.internal.display.grid.columns.EnumDrivenColumn;

import java.util.HashMap;
import java.util.Map;

public class CustomFieldBoundEntityColumn extends EnumDrivenColumn {
	public CustomFieldBoundEntityColumn(Field<String> field) {
		super(BindableEntity.class, field);
	}

	@Override
	protected Map<String, Integer> getAscSortMap() {
		return new HashMap<String, Integer>() {
			private static final long serialVersionUID = -991447858174232570L;

			{
				put(BindableEntity.REQUIREMENT_FOLDER.toString(), 	1);
				put(BindableEntity.REQUIREMENT_VERSION.toString(), 	2);
				put(BindableEntity.TESTCASE_FOLDER.toString(), 		3);
				put(BindableEntity.TEST_CASE.toString(), 			4);
				put(BindableEntity.TEST_STEP.toString(), 			5);
				put(BindableEntity.CAMPAIGN_FOLDER.toString(), 		6);
				put(BindableEntity.CAMPAIGN.toString(), 			7);
				put(BindableEntity.ITERATION.toString(), 			8);
				put(BindableEntity.TEST_SUITE.toString(), 			9);
				put(BindableEntity.EXECUTION.toString(), 			10);
				put(BindableEntity.EXECUTION_STEP.toString(), 		11);
			}
		};
	}
}

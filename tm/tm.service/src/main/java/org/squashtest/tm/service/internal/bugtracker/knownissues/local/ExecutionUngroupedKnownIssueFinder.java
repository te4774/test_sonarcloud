/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.local;

import org.jooq.DSLContext;
import org.jooq.Record6;
import org.jooq.SelectConditionStep;
import org.jooq.SelectHavingStep;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

/**
 * Unlike the other issue finders, this one won't group remote issues together. Instead,
 * each local issue will get its own row. The generated grid response won't be consumed as-is
 * by TM client. Instead, the rows are dispatched for each execution step and we want to be
 * able to delete a single issue from there. For example, if the same remote issue is attached
 * to multiple steps, we only want to delete the issue linked to the specific step and thus
 * we don't want grouping.
 */
@Repository
@Transactional(readOnly = true)
public class ExecutionUngroupedKnownIssueFinder extends ExecutionKnownIssueFinder {

	public ExecutionUngroupedKnownIssueFinder(DSLContext dsl) {
		super(dsl);
	}

	@Override
	protected SelectHavingStep<Record6<Long, Long, String, String, String, String>> applyGrouping(SelectConditionStep<Record6<Long, Long, String, String, String, String>> select) {
		return select.groupBy(PROJECT.PROJECT_ID, ISSUE.BUGTRACKER_ID, ISSUE.REMOTE_ISSUE_ID, ISSUE.ISSUE_ID);
	}
}

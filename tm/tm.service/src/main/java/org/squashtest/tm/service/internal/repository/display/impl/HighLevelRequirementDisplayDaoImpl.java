/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.SelectConditionStep;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.jooq.domain.tables.RequirementVersion;
import org.squashtest.tm.service.internal.display.dto.requirement.HighLevelRequirementVersionDto.LinkedLowLevelRequirementDto;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.display.HighLevelRequirementDisplayDao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.jooq.domain.Tables.HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;

@Repository
public class HighLevelRequirementDisplayDaoImpl implements HighLevelRequirementDisplayDao {

	private final DSLContext dslContext;
	private final RequirementDao requirementDao;

	private static final String LINKED_LOW_LEVEL_REQ_VERSION_ALIAS = "LINKED_LOW_LEVEL_REQ_VERSION";
	private static final String requirementIdAlias = "REQUIREMENT_ID";
	private static final String requirementVersionIdAlias = "REQUIREMENT_VERSION_ID";
	private static final String projectNameAlias = "PROJECT_NAME";

	private static final String milestoneResIdAlias = "MILESTONE_SUB_SELECT_RES_ID";
	private static final String milestoneLabels = "MILESTONE_LABELS";
	private static final String milestoneMaxDate = "MILESTONE_MAX_DATE";
	private static final String milestoneMinDate = "MILESTONE_MIN_DATE";

	public HighLevelRequirementDisplayDaoImpl(DSLContext dslContext, RequirementDao requirementDao) {
		this.dslContext = dslContext;
		this.requirementDao = requirementDao;
	}

	@Override
	public List<LinkedLowLevelRequirementDto> findLinkedLowLevelRequirements(Long requirementId) {
		Set<Long> linkedRequirementIds = findLinkedLowLevelRequirementIdsByRequirementIds(Collections.singletonList(requirementId));

		// fetch the children ids of all reqs (the looked one and the linked low level reqs)
		Set<Long> ancestorIds = new HashSet<>(linkedRequirementIds);
		ancestorIds.add(requirementId);
		List<Long> descendantIds = requirementDao.findDescendantRequirementIds(new ArrayList<>(ancestorIds));

		Set<Long> requirementToFetch = new HashSet<>(linkedRequirementIds);
		requirementToFetch.addAll(descendantIds);

		// fetch all the req (direct descendant, linked and their descendant)
		return fetchProjections(requirementToFetch, linkedRequirementIds);
	}

	@Override
	public Set<Long> findRequirementIdsByLibraryIds(Collection<Long> readLibIds, boolean isExtendedHighLvlReqScope) {
		SelectConditionStep<Record1<Long>> reqIds = dslContext.select(REQUIREMENT.RLN_ID)
				.from(REQUIREMENT)
				.join(REQUIREMENT_LIBRARY_NODE).on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
				.join(PROJECT).on(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.RL_ID))
				.where(PROJECT.RL_ID.in(readLibIds));

		if (isExtendedHighLvlReqScope) {
			reqIds.union(
				dslContext.select(REQUIREMENT.RLN_ID)
					.from(HIGH_LEVEL_REQUIREMENT)
					.join(REQUIREMENT).on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID))
					.join(REQUIREMENT_LIBRARY_NODE).on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
					.join(PROJECT).on(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.RL_ID))
					.where(PROJECT.RL_ID.in(readLibIds))
			);
		}

		return reqIds.fetchSet(REQUIREMENT.RLN_ID);
	}

	@Override
	public Set<Long> findRequirementIdsByNodeIds(Collection<Long> readNodeIds, boolean isExtendedHighLvlReqScope) {
		SelectConditionStep<Record1<Long>> reqIds = dslContext.select(REQUIREMENT.RLN_ID)
				.from(REQUIREMENT)
				.join(REQUIREMENT_LIBRARY_NODE).on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
				.join(RLN_RELATIONSHIP_CLOSURE).on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
				.where(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.in(readNodeIds));

		if (isExtendedHighLvlReqScope) {
			reqIds.union(
					dslContext.select(REQUIREMENT.RLN_ID)
						.from(HIGH_LEVEL_REQUIREMENT)
						.join(REQUIREMENT).on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID))
						.join(REQUIREMENT_LIBRARY_NODE).on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
						.join(RLN_RELATIONSHIP_CLOSURE).on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID))
						.where(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.in(readNodeIds))
			);
		}

		return reqIds.fetchSet(REQUIREMENT.RLN_ID);
	}

	@Override
	public Set<Long> findStandardRequirementsByHighLvlReqId(Long highLevelRequirementId) {
		return findLinkedLowLevelRequirementIdsByRequirementIds(Collections.singletonList(highLevelRequirementId));
	}

	@Override
	public Set<Long> findStandardRequirementsByRequirementIdsAndProjectIds(List<Long> requirementIds, List<Long> projectIds) {
		return findLinkedLowLevelRequirementIdsByRequirementIdsAndProjectIds(requirementIds, projectIds);
	}

	@Override
	public Set<Long> findLinkedLowLevelReqVersionIdsByReqVersionIdsAndProjectIds(List<Long> requirementVersionIds, List<Long> projectIds) {
		RequirementVersion linkedLowLevelReqVersion = REQUIREMENT_VERSION.as(LINKED_LOW_LEVEL_REQ_VERSION_ALIAS);

		return dslContext.selectDistinct(linkedLowLevelReqVersion.RES_ID)
				.from(HIGH_LEVEL_REQUIREMENT)
				.innerJoin(REQUIREMENT_VERSION).on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
				.innerJoin(REQUIREMENT).on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID))
				.innerJoin(linkedLowLevelReqVersion).on(REQUIREMENT.RLN_ID.eq(linkedLowLevelReqVersion.REQUIREMENT_ID))
				.innerJoin(REQUIREMENT_LIBRARY_NODE).on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
				.innerJoin(PROJECT).on(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
				.where(REQUIREMENT_VERSION.RES_ID.in(requirementVersionIds))
				.and(PROJECT.PROJECT_ID.in(projectIds))
				.fetchSet(linkedLowLevelReqVersion.RES_ID);
	}

	// If high level is not in selection and version id is linked to a high level requirement, it will retrieve it
	@Override
	public Map<Long, List<Long>> findAllLinkedLowLevelReqIdsMappedByHighLevelReqIdFromVersionIds(List<Long> versionIds) {
		Field<Long> highLevelRequirementIdField = when(HIGH_LEVEL_REQUIREMENT.RLN_ID.isNull(), REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID).otherwise(HIGH_LEVEL_REQUIREMENT.RLN_ID);

		return dslContext.selectDistinct(highLevelRequirementIdField, REQUIREMENT.RLN_ID)
				.from(REQUIREMENT_VERSION)
				.leftJoin(HIGH_LEVEL_REQUIREMENT).on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(HIGH_LEVEL_REQUIREMENT.RLN_ID))
				.innerJoin(REQUIREMENT).on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID))
					.or(HIGH_LEVEL_REQUIREMENT.RLN_ID.isNull().and(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID.isNotNull().and(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))))
				.where(REQUIREMENT_VERSION.RES_ID.in(versionIds))
				.fetchGroups(highLevelRequirementIdField, REQUIREMENT.RLN_ID);
	}

	private List<LinkedLowLevelRequirementDto> fetchProjections(Set<Long> requirementsToFetch, Set<Long> linkedRequirementIds) {
		List<LinkedLowLevelRequirementDto> levelRequirements = fetchBaseProjections(requirementsToFetch);

		levelRequirements.forEach(dto -> {
			if (requirementDao.checkIfRequirementIsChild(dto.getRequirementId())) {
				dto.setChildOfRequirement(true);
			}
		});
		return levelRequirements;
	}

	private List<LinkedLowLevelRequirementDto> fetchBaseProjections(Set<Long> requirementsToFetch) {
		SelectHavingStep<Record4<Long, Timestamp, Timestamp, String>> milestoneSubSelect = getMilestoneDates(requirementsToFetch);
		return dslContext.select(
				REQUIREMENT.RLN_ID.as(requirementIdAlias)
				, RESOURCE.NAME, RESOURCE.RES_ID.as(requirementVersionIdAlias)
				, REQUIREMENT_VERSION.REFERENCE, REQUIREMENT_VERSION.VERSION_NUMBER
				, PROJECT.NAME.as(projectNameAlias)
				, field(milestoneLabels) , field(milestoneMinDate), field(milestoneMaxDate) // fields from the milestone date sub select
				, REQUIREMENT_VERSION.REQUIREMENT_STATUS
				, REQUIREMENT_VERSION.CRITICALITY
			).from(REQUIREMENT)
			.innerJoin(REQUIREMENT_LIBRARY_NODE).on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
			.innerJoin(REQUIREMENT_VERSION).on(REQUIREMENT.CURRENT_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
			.innerJoin(RESOURCE).on(RESOURCE.RES_ID.eq(REQUIREMENT_VERSION.RES_ID))
			.innerJoin(PROJECT).on(PROJECT.PROJECT_ID.eq(REQUIREMENT_LIBRARY_NODE.PROJECT_ID))
			.leftJoin(milestoneSubSelect).on(milestoneSubSelect.field(milestoneResIdAlias, Long.class).eq(REQUIREMENT_VERSION.RES_ID))
			.where(REQUIREMENT.RLN_ID.in(requirementsToFetch))
			.fetchInto(LinkedLowLevelRequirementDto.class);
	}

	private Set<Long> findLinkedLowLevelRequirementIdsByRequirementIds(List<Long> requirementIds) {
		return dslContext.selectDistinct(REQUIREMENT.RLN_ID)
			.from(HIGH_LEVEL_REQUIREMENT)
			.innerJoin(REQUIREMENT).on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID))
			.where(HIGH_LEVEL_REQUIREMENT.RLN_ID.in(requirementIds))
			.fetchSet(REQUIREMENT.RLN_ID);
	}

	private Set<Long> findLinkedLowLevelRequirementIdsByRequirementIdsAndProjectIds(List<Long> requirementIds, List<Long> projectIds) {
		return dslContext.selectDistinct(REQUIREMENT.RLN_ID)
			.from(HIGH_LEVEL_REQUIREMENT)
			.innerJoin(REQUIREMENT).on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID))
			.innerJoin(REQUIREMENT_LIBRARY_NODE).on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_LIBRARY_NODE.RLN_ID))
			.innerJoin(PROJECT).on(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.where(HIGH_LEVEL_REQUIREMENT.RLN_ID.in(requirementIds))
				.and(PROJECT.PROJECT_ID.in(projectIds))
			.fetchSet(REQUIREMENT.RLN_ID);
	}

	private SelectHavingStep<Record4<Long, Timestamp, Timestamp, String>> getMilestoneDates(Set<Long> requirementsToFetch) {
		return dslContext.select(
				REQUIREMENT_VERSION.RES_ID.as(milestoneResIdAlias),
				DSL.min(MILESTONE.END_DATE).as(milestoneMinDate),
				DSL.max(MILESTONE.END_DATE).as(milestoneMaxDate),
				DSL.listAgg(MILESTONE.LABEL, ", ").withinGroupOrderBy(MILESTONE.END_DATE.asc()).as(milestoneLabels)
			)
			.from(REQUIREMENT_VERSION)
			.innerJoin(MILESTONE_REQ_VERSION).on(REQUIREMENT_VERSION.RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
			.innerJoin(MILESTONE).on(MILESTONE.MILESTONE_ID.eq(MILESTONE_REQ_VERSION.MILESTONE_ID))
			.where(REQUIREMENT_VERSION.REQUIREMENT_ID.in(requirementsToFetch))
			.groupBy(
				REQUIREMENT_VERSION.RES_ID,
				MILESTONE_REQ_VERSION.REQ_VERSION_ID
			);
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.actionword.ActionWordNodeType;
import org.squashtest.tm.jooq.domain.tables.ActionWord;
import org.squashtest.tm.jooq.domain.tables.ActionWordLibraryNode;
import org.squashtest.tm.service.internal.repository.CustomActionWordDao;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.concat;
import static org.jooq.impl.DSL.count;
import static org.jooq.impl.DSL.min;
import static org.jooq.impl.DSL.val;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_FRAGMENT;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_PARAMETER;
import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_TEXT;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_STEP;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.jooq.domain.tables.KeywordTestCase.KEYWORD_TEST_CASE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.VALUE;

@Repository
@Transactional
public class ActionWordDaoImpl implements CustomActionWordDao {

	@Inject
	private DSLContext dsl;

	@Override
	public void updateActionWordImplInfoFromAutomRequestIds(Collection<Long> automationRequestIds) {
		Map<String, List<Long>> actionWordIdsMappedByImplTechno = findActionWordIdMappedByBddImplTechno(automationRequestIds);
		for(Map.Entry<String, List<Long>> entry : actionWordIdsMappedByImplTechno.entrySet()) {
			String implementationTechnology = entry.getKey();
			Collection<Long> actionWordIds = entry.getValue();
				updateActionWordsImplInfo(actionWordIds, implementationTechnology);
		}
	}

	private Map<String, List<Long>> findActionWordIdMappedByBddImplTechno(Collection<Long> automationRequestIds) {
		return dsl.selectDistinct(PROJECT.BDD_IMPLEMENTATION_TECHNOLOGY, ACTION_WORD.ACTION_WORD_ID)
			.from(AUTOMATION_REQUEST)
			.join(KEYWORD_TEST_CASE).on(KEYWORD_TEST_CASE.TCLN_ID.eq(AUTOMATION_REQUEST.TEST_CASE_ID))
			.join(TEST_CASE_STEPS).on(TEST_CASE_STEPS.TEST_CASE_ID.eq(KEYWORD_TEST_CASE.TCLN_ID))
			.join(KEYWORD_TEST_STEP).on(KEYWORD_TEST_STEP.TEST_STEP_ID.eq(TEST_CASE_STEPS.STEP_ID))
			.join(ACTION_WORD).on(ACTION_WORD.ACTION_WORD_ID.eq(KEYWORD_TEST_STEP.ACTION_WORD_ID))
			.join(PROJECT).on(PROJECT.PROJECT_ID.eq(AUTOMATION_REQUEST.PROJECT_ID))
			.where(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.in(automationRequestIds))
			.fetchGroups(PROJECT.BDD_IMPLEMENTATION_TECHNOLOGY, ACTION_WORD.ACTION_WORD_ID);
	}

	private void updateActionWordsImplInfo(Collection<Long> actionWordIds, String implementationTechnology) {
		dsl.update(ACTION_WORD)
			.set(ACTION_WORD.LAST_IMPLEMENTATION_TECHNOLOGY, implementationTechnology)
			.set(ACTION_WORD.LAST_IMPLEMENTATION_DATE, DSL.currentTimestamp())
			.where(ACTION_WORD.ACTION_WORD_ID.in(actionWordIds))
			.execute();
	}

	@Override
	public List<String> findAllCaseInsensitiveDuplicateActions() {
		// 1. fetch all duplicate tokens selecting the action word with the lowest id
		List<ActionWordDto> duplicateActionWords =
			dsl
				.select(
					min(ACTION_WORD.ACTION_WORD_ID).as("ID"),
					PROJECT.NAME.as("PROJECT_NAME"))
				.from(ACTION_WORD)
				.innerJoin(PROJECT).on(PROJECT.PROJECT_ID.eq(ACTION_WORD.PROJECT_ID))
				.groupBy(
					PROJECT.PROJECT_ID,
					ACTION_WORD.TOKEN.lower())
				.having(count().greaterThan(1))
				.orderBy(PROJECT.PROJECT_ID, ACTION_WORD.TOKEN.lower())
				.fetchInto(ActionWordDto.class);
		if (duplicateActionWords.isEmpty()) {
			return new ArrayList<>();
		}
		// plucking the ids
		List<Long> duplicateActionWordIds =
			duplicateActionWords
				.stream()
				.map(ActionWordDto::getId)
				.collect(Collectors.toList());
		// 2. Fetch all corresponding ActionWordFragments
		List<ActionWordFragmentWithValueDto> actionWordFragments =
			dsl
				.select(
					ACTION_WORD_TEXT.TEXT.as(VALUE),
					ACTION_WORD_FRAGMENT.ACTION_WORD_ID,
					ACTION_WORD_FRAGMENT.FRAGMENT_ORDER
				)
				.from(ACTION_WORD_FRAGMENT)
				.innerJoin(ACTION_WORD_TEXT).on(ACTION_WORD_TEXT.ACTION_WORD_FRAGMENT_ID.eq(ACTION_WORD_FRAGMENT.ACTION_WORD_FRAGMENT_ID))
				.where(ACTION_WORD_FRAGMENT.ACTION_WORD_ID.in(duplicateActionWordIds))
				.union(
					dsl.select(
							concat(val("\""), ACTION_WORD_PARAMETER.NAME, val("\"")).as(VALUE),
							ACTION_WORD_FRAGMENT.ACTION_WORD_ID,
							ACTION_WORD_FRAGMENT.FRAGMENT_ORDER
						)
						.from(ACTION_WORD_FRAGMENT)
						.innerJoin(ACTION_WORD_PARAMETER)
						.on(ACTION_WORD_PARAMETER.ACTION_WORD_FRAGMENT_ID.eq(ACTION_WORD_FRAGMENT.ACTION_WORD_FRAGMENT_ID))
						.where(ACTION_WORD_FRAGMENT.ACTION_WORD_ID.in(duplicateActionWordIds))
				)
				.orderBy(
					ACTION_WORD_FRAGMENT.ACTION_WORD_ID,
					ACTION_WORD_FRAGMENT.FRAGMENT_ORDER)
				.fetch()
				.stream()
				.map(record ->
					new ActionWordFragmentWithValueDto(
						record.get(ACTION_WORD_FRAGMENT.ACTION_WORD_ID),
						record.get(VALUE).toString())
				)
				.collect(Collectors.toList());
		// 3. build the string 'actionword - Project' for each duplicate action word
		return duplicateActionWords
			.stream()
			.map(actionWord -> {
				List<ActionWordFragmentWithValueDto> fragmentList =
					actionWordFragments
						.stream()
						.filter(fragment -> actionWord.getId().equals(fragment.getActionWordId()))
						.collect(Collectors.toList());
				String actionWithParamNames =
					fragmentList
						.stream()
						.map(ActionWordFragmentWithValueDto::getValue)
						.collect(Collectors.joining());
				return actionWithParamNames.toLowerCase() + " - " + actionWord.getProjectName();
			})
			.collect(Collectors.toList());
	}

	@Override
	public Map<Long, Long> findAllWithDuplicateTokenByProjectId(Long projectId) {
		ActionWord duplicateTokenActionWord = ACTION_WORD.as("duplicateTokenActionWord");
		return dsl.select(ACTION_WORD.ACTION_WORD_ID, duplicateTokenActionWord.ACTION_WORD_ID)
			.from(ACTION_WORD)
			.join(duplicateTokenActionWord)
			.on(ACTION_WORD.TOKEN.eq(duplicateTokenActionWord.TOKEN))
			.where(ACTION_WORD.PROJECT_ID.eq(projectId))
			.and(duplicateTokenActionWord.PROJECT_ID.ne(projectId))
			.groupBy(ACTION_WORD.ACTION_WORD_ID, duplicateTokenActionWord.ACTION_WORD_ID)
			.fetchMap(ACTION_WORD.ACTION_WORD_ID, duplicateTokenActionWord.ACTION_WORD_ID);
	}

	@Override
	public Map<Long, Long> findAllWithoutDuplicateTokenByProjectId(Long projectId) {
		ActionWordLibraryNode projectAwln = ACTION_WORD_LIBRARY_NODE.as("projectAwln");
		return dsl.select(ACTION_WORD_LIBRARY_NODE.AWLN_ID, min(projectAwln.AWLN_ID))
			.from(ACTION_WORD)
			.join(KEYWORD_TEST_STEP).on(KEYWORD_TEST_STEP.ACTION_WORD_ID.eq(ACTION_WORD.ACTION_WORD_ID))
			.join(TEST_CASE_STEPS).on(TEST_CASE_STEPS.STEP_ID.eq(KEYWORD_TEST_STEP.TEST_STEP_ID))
			.join(TEST_CASE_LIBRARY_NODE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE_STEPS.TEST_CASE_ID))
			.join(ACTION_WORD_LIBRARY_NODE).on(ACTION_WORD_LIBRARY_NODE.ENTITY_ID.eq(ACTION_WORD.ACTION_WORD_ID)
				.and(ACTION_WORD_LIBRARY_NODE.ENTITY_TYPE.eq(ActionWordNodeType.ACTION_WORD_NAME)))
			.join(PROJECT).on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
			.join(projectAwln).on(projectAwln.ENTITY_ID.eq(PROJECT.AWL_ID)
				.and(projectAwln.ENTITY_TYPE.eq(ActionWordNodeType.LIBRARY_NAME)))
			.where(ACTION_WORD.PROJECT_ID.eq(projectId))
			.and(TEST_CASE_LIBRARY_NODE.PROJECT_ID.ne(projectId))
			.groupBy(ACTION_WORD.ACTION_WORD_ID, ACTION_WORD_LIBRARY_NODE.AWLN_ID)
			.fetchMap(ACTION_WORD_LIBRARY_NODE.AWLN_ID, min(projectAwln.AWLN_ID));
	}

	private static class ActionWordDto {
		private Long id;
		private String projectName;

		public ActionWordDto(Long id, String projectName) {
			this.id = id;
			this.projectName = projectName;
		}

		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}

		public String getProjectName() {
			return projectName;
		}
		public void setProjectName(String projectName) {
			this.projectName = projectName;
		}
	}

	private static class ActionWordFragmentWithValueDto {
		private Long actionWordId;
		private String value;

		public ActionWordFragmentWithValueDto(Long actionWordId, String value) {
			this.actionWordId = actionWordId;
			this.value = value;
		}

		public Long getActionWordId() {
			return actionWordId;
		}
		public void setActionWordId(Long actionWordId) {
			this.actionWordId = actionWordId;
		}

		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
	}
}

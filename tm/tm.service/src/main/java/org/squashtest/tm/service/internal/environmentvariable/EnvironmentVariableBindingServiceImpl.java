/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.environmentvariable.EVInputType;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableValueService;
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto;
import org.squashtest.tm.service.internal.repository.CustomEnvironmentVariableBindingDao;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao;
import org.squashtest.tm.service.internal.repository.TestAutomationServerDao;
import org.squashtest.tm.service.internal.repository.display.EnvironmentVariableDisplayDao;
import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

@Service("squashtest.tm.service.EnvironmentVariableBindingService")
@Transactional
public class EnvironmentVariableBindingServiceImpl implements EnvironmentVariableBindingService {

	private final EnvironmentVariableDao environmentVariableDao;

	private final EnvironmentVariableBindingDao environmentVariableBindingDao;

	private final TestAutomationServerDao serverDao;

	private final ProjectDisplayDao projectDisplayDao;

	private final CustomEnvironmentVariableBindingDao customEnvironmentVariableBindingDao;

	private final EnvironmentVariableDisplayDao environmentVariableDisplayDao;

	private final EnvironmentVariableValueService environmentVariableValueService;

	public EnvironmentVariableBindingServiceImpl(EnvironmentVariableDao environmentVariableDao,
												 EnvironmentVariableBindingDao environmentVariableBindingDao,
												 TestAutomationServerDao serverDao, ProjectDisplayDao projectDisplayDao,
												 CustomEnvironmentVariableBindingDao customEnvironmentVariableBindingDao,
												 EnvironmentVariableDisplayDao environmentVariableDisplayDao,
												 EnvironmentVariableValueService environmentVariableValueService) {
		this.environmentVariableDao = environmentVariableDao;
		this.environmentVariableBindingDao = environmentVariableBindingDao;
		this.serverDao = serverDao;
		this.projectDisplayDao = projectDisplayDao;
		this.customEnvironmentVariableBindingDao = customEnvironmentVariableBindingDao;
		this.environmentVariableDisplayDao = environmentVariableDisplayDao;
		this.environmentVariableValueService = environmentVariableValueService;
	}

	@Override
	@PreAuthorize(HAS_ROLE_ADMIN)
	public void createNewBindings(Long testAutomationServerId, List<Long> environmentVariableIds) {
		TestAutomationServer server = serverDao.findById(testAutomationServerId).orElseThrow();
		List<EnvironmentVariable> environmentVariables = environmentVariableDao.findAllByIdIn(environmentVariableIds);
		for (EnvironmentVariable environmentVariable : environmentVariables) {
			EnvironmentVariableBinding binding = createBinding(server, environmentVariable);
			environmentVariableValueService.createValueFromServer(binding, testAutomationServerId);
		}
	}

	@Override
	@PreAuthorize(HAS_ROLE_ADMIN)
	public void unbind(Long testAutomationServerId, List<Long> evIds) {
		Collection<Long> bindingIds = customEnvironmentVariableBindingDao.findAllBindingIdsByEvIdsAndServerId(evIds, testAutomationServerId);
		environmentVariableValueService.removeEnvironmentVariableValuesByBindingIds(bindingIds);
		environmentVariableBindingDao.deleteAllById(bindingIds);
	}

	@Override
	public void unbindByEnvironmentVariableId(Long evId) {
		List<EnvironmentVariableBinding> bindings = environmentVariableBindingDao.findAllByEnvironmentVariable_Id(evId);
		deleteAllBindings(bindings);
	}

	@Override
	public void unbindAllByServerId(Long serverId) {
		List<EnvironmentVariableBinding> bindings = environmentVariableBindingDao.findAllByTestAutomationServer_Id(serverId);
		deleteAllBindings(bindings);
	}

	@Override
	public List<BoundEnvironmentVariableDto> getAllDefaultBoundEnvironmentVariableDto(Long serverId) {

		List<BoundEnvironmentVariableDto> boundEnvironmentVariableDtos =
			customEnvironmentVariableBindingDao.findAllBoundEnvironmentVariables(serverId, null);
		setOptionValues(boundEnvironmentVariableDtos);
		return boundEnvironmentVariableDtos;
	}

	private void setOptionValues(List<BoundEnvironmentVariableDto> boundEnvironmentVariables) {
		boundEnvironmentVariables.forEach(boundEnvironmentVariableDto -> {
			if(boundEnvironmentVariableDto.getInputType().equals(EVInputType.DROPDOWN_LIST.name())) {
				boundEnvironmentVariableDto
					.setOptions(environmentVariableDisplayDao
						.getEnvironmentVariableOptionListByEvId(boundEnvironmentVariableDto.getId()));
			}
		});
	}

	@Override
	public List<BoundEnvironmentVariableDto> getAllBoundEnvironmentVariableDtoByProjectId(Long projectId) {
		Long taServerId = projectDisplayDao.getTaServerIdByProjectId(projectId);

		if (taServerId != null) {
			List<BoundEnvironmentVariableDto> boundEnvironmentVariableDtos =
				customEnvironmentVariableBindingDao.findAllBoundEnvironmentVariables(taServerId, projectId);
			setOptionValues(boundEnvironmentVariableDtos);
			return boundEnvironmentVariableDtos;
		} else {
			return null;
		}
	}

	private void deleteAllBindings(List<EnvironmentVariableBinding> bindings) {
		List<Long> bindingIds = bindings.stream().map(EnvironmentVariableBinding::getId).collect(Collectors.toList());
		environmentVariableValueService.removeEnvironmentVariableValuesByBindingIds(bindingIds);
		environmentVariableBindingDao.deleteAllById(bindingIds);
	}

	private EnvironmentVariableBinding createBinding(TestAutomationServer server, EnvironmentVariable environmentVariable) {

		EnvironmentVariableBinding environmentVariableBinding = new EnvironmentVariableBinding();
		environmentVariableBinding.setEnvironmentVariable(environmentVariable);
		environmentVariableBinding.setTestAutomationServer(server);

		checkIfBindingExist(server.getId(), environmentVariable.getId());
		return environmentVariableBindingDao.save(environmentVariableBinding);

	}

	private void checkIfBindingExist(Long serverId, Long environmentVariableId) {
		EnvironmentVariableBinding environmentVariableBinding =
			environmentVariableBindingDao.findByServerIdAndEvId(serverId, environmentVariableId);
		if (environmentVariableBinding != null) {
			throw new EntityExistsException("Binding already exists.");
		}
	}

}

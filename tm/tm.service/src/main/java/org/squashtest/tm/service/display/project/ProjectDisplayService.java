/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.project;

import org.squashtest.tm.domain.NamedReference;
import org.squashtest.tm.service.internal.display.dto.BindMilestoneToProjectDialogData;
import org.squashtest.tm.service.internal.display.dto.PartyProjectPermissionDto;
import org.squashtest.tm.service.internal.display.dto.ProjectViewDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

import java.util.List;
import java.util.Map;

public interface ProjectDisplayService {

	GridResponse findAll(GridRequest request);

	List<NamedReference> getTemplateNamedReferences();

	ProjectViewDto getProjectView(long projectId);

	Map<String, Boolean> getProjectStatusesInUse(long projectId);

	boolean hasProjectData(long projectId);

	List<PartyProjectPermissionDto> getPartyProjectPermissions(Long projectId);

	BindMilestoneToProjectDialogData findAvailableMilestones(long projectId);
}

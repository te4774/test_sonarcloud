/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.environmentvariable;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.service.display.environmentvariable.EnvironmentVariableDisplayService;
import org.squashtest.tm.service.internal.display.dto.EnvironmentVariableDto;
import org.squashtest.tm.service.internal.display.dto.EnvironmentVariableOptionDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.administration.EnvironmentVariableGrid;
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao;
import org.squashtest.tm.service.internal.repository.display.EnvironmentVariableDisplayDao;

import javax.transaction.Transactional;

import java.util.List;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

@Service
@Transactional
public class EnvironmentVariableDisplayServiceImpl implements EnvironmentVariableDisplayService {

	private final DSLContext dsl;
	private final EnvironmentVariableDisplayDao dao;

	private final EnvironmentVariableBindingDao bindingDao;

	public EnvironmentVariableDisplayServiceImpl(DSLContext dsl, EnvironmentVariableDisplayDao dao, EnvironmentVariableBindingDao bindingDao) {
		this.dsl = dsl;
		this.dao = dao;
		this.bindingDao = bindingDao;
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public GridResponse findAll(GridRequest request) {
		EnvironmentVariableGrid evGrid = new EnvironmentVariableGrid();
		return evGrid.getRows(request, dsl);
	}

	@Override
	public List<EnvironmentVariableDto> getAllEnvironmentVariableReferences() {
		return dao.getAllEnvironmentVariableDto();
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public EnvironmentVariableDto getEnvironmentVariableView(Long environmentVariableId) {
		EnvironmentVariableDto dto = dao.getEnvironmentVariableById(environmentVariableId);
		dto.setIsBoundToServer(isEnvironmentVariableBound(dto.getId()));
		return dto;
	}

	@PreAuthorize(HAS_ROLE_ADMIN)
	@Override
	public List<EnvironmentVariableOptionDto> getOptionsByEvId(Long environmentVariableId) {
		return dao.getEnvironmentVariableOptionListByEvId(environmentVariableId);
	}

	private boolean isEnvironmentVariableBound(Long environmentVariableId) {
		return !bindingDao.findAllByEnvironmentVariable_Id(environmentVariableId).isEmpty();
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.bugtracker.knownissues.remote;

import org.squashtest.tm.core.foundation.collection.PagingAndSorting;

import java.util.List;

public interface RemoteKnownIssueFinder {

	List<RemoteKnownIssue> findForTestCase(long testCaseId, PagingAndSorting pagingAndSorting);
	int getCountForTestCase(Long testCaseId);

	List<RemoteKnownIssue> findForRequirementVersion(long requirementVersionId, PagingAndSorting pagingAndSorting);
	int getCountForRequirementVersion(Long requirementVersionId);

	List<RemoteKnownIssue> findForIteration(long iterationId, PagingAndSorting pagingAndSorting);
	int getCountForIteration(Long iterationId);

	List<RemoteKnownIssue> findForTestSuite(long testSuiteId, PagingAndSorting pagingAndSorting);
	int getCountForTestSuite(Long testSuiteId);

	List<RemoteKnownIssue> findForCampaign(long campaignId, PagingAndSorting pagingAndSorting);
	int getCountForCampaign(Long campaignId);

	List<RemoteKnownIssue> findForCampaignFolder(long campaignFolderId, PagingAndSorting pagingAndSorting);
	int getCountForCampaignFolder(Long campaignFolderId);

	List<RemoteKnownIssue> findForExecution(long executionId, PagingAndSorting pagingAndSorting);
	int getCountForExecution(Long executionId);

	List<RemoteKnownIssue> findForExecutionStep(long executionStepId, PagingAndSorting pagingAndSorting);
	int getCountForExecutionStep(Long executionStepId);

	List<RemoteKnownIssue> findUngroupedForExecution(Long execId);
}

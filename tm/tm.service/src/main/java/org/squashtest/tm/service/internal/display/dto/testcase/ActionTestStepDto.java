/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto.testcase;

import org.squashtest.tm.service.internal.display.dto.AttachmentDto;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.service.internal.display.dto.AttachmentListHolderDto;
import org.squashtest.tm.service.internal.display.dto.CufHolderDto;
import org.squashtest.tm.service.internal.display.dto.CustomFieldValueDto;

import java.util.List;

public class ActionTestStepDto extends TestStepDto implements CufHolderDto, AttachmentListHolderDto {

	public static final String ACTION_STEP = "action-step";

	private Long id;
	private String action;
	private String expectedResult;
	private Integer verifiedRequirements;
	private List<CustomFieldValueDto> customFieldValues;
	private AttachmentListDto attachmentList = new AttachmentListDto();

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getKind() {
		return ACTION_STEP;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getExpectedResult() {
		return expectedResult;
	}

	public void setExpectedResult(String expectedResult) {
		this.expectedResult = expectedResult;
	}

	public Integer getVerifiedRequirements() {
		return verifiedRequirements;
	}

	public void setVerifiedRequirements(Integer verifiedRequirements) {
		this.verifiedRequirements = verifiedRequirements;
	}

	public List<CustomFieldValueDto> getCustomFieldValues() {
		return customFieldValues;
	}

	public void setCustomFieldValues(List<CustomFieldValueDto> customFieldValues) {
		this.customFieldValues = customFieldValues;
	}

	public void setAttachmentListId(Long attachmentListId) {
		this.attachmentList.setId(attachmentListId);
	}

	public Long getAttachmentListId() {
		return this.getAttachmentList().getId();
	}

	public void addAttachments(List<AttachmentDto> attachments) {
		this.attachmentList.addAttachments(attachments);
	}

	public AttachmentListDto getAttachmentList() {
		return attachmentList;
	}
}

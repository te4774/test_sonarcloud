/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.campaign;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Record4;
import org.jooq.SelectHavingStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.SortField;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import static org.jooq.impl.DSL.countDistinct;
import static org.jooq.impl.DSL.when;
import static org.squashtest.tm.domain.milestone.MilestoneStatus.MILESTONE_BLOCKING_STATUSES;
import static org.squashtest.tm.domain.project.AutomationWorkflowType.NONE;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.jenkins;
import static org.squashtest.tm.domain.testautomation.TestAutomationServerKind.squashAutom;
import static org.squashtest.tm.domain.testcase.TestCaseAutomatable.Y;
import static org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus.AUTOMATED;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATION_REQUEST;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.DATASET;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_SERVER;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.BOUND_TO_BLOCKING_MILESTONE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ITEM_ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TEST_SUITES;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.USER;

public class IterationTestPlanGrid extends AbstractGrid {

	private final Long iterationId;
	private final String LATEST_EXECUTION_ID = "LATEST_EXECUTION_ID";
	private final String MAX_EXECUTION_ORDER = "MAX_EXECUTION_ORDER";

	public IterationTestPlanGrid(Long iterationId) {
		this.iterationId = iterationId;
	}

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID),
			new GridColumn(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON),
			new GridColumn(findItpiWithLatestExecution().field(LATEST_EXECUTION_ID)),
			new GridColumn(ITERATION_TEST_PLAN_ITEM.USER_ID),
			new GridColumn(ITEM_TEST_PLAN_LIST.ITERATION_ID),
			new GridColumn(TEST_CASE_LIBRARY_NODE.TCLN_ID.as("TEST_CASE_ID")),
			new GridColumn(TEST_CASE_LIBRARY_NODE.NAME.as("TEST_CASE_NAME"), TEST_CASE_LIBRARY_NODE.NAME),
			new GridColumn(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.as("PROJECT_ID")),
			new GridColumn(PROJECT.NAME.as("PROJECT_NAME"), PROJECT.NAME),
			new GridColumn(TEST_CASE.REFERENCE.as("TEST_CASE_REFERENCE"), TEST_CASE.REFERENCE),
			new LevelEnumColumn(TestCaseImportance.class, TEST_CASE.IMPORTANCE),
			new GridColumn(getUser().field(USER)),
			new GridColumn(getUser().field("LOGIN", String.class)), // this field is used only for filters
			new GridColumn(DATASET.NAME.as("DATASET_NAME"), DATASET.NAME),
			new GridColumn(getAutomationFields().field("EXECUTION_MODE")),
			new GridColumn(getTestSuites().field(TEST_SUITES)),
			new LevelEnumColumn(ExecutionStatus.class, ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS),
			new GridColumn(DSL.field("MILESTONE_MIN_DATE")),
			new GridColumn(DSL.field("MILESTONE_MAX_DATE")),
			new GridColumn(DSL.field("MILESTONE_LABELS")),
			new GridColumn(DSL.field(BOUND_TO_BLOCKING_MILESTONE))
		);
	}

	@Override
	protected Table<?> getTable() {
		SelectHavingStep<?> getAutomationFields = getAutomationFields();
		SelectHavingStep<?> getUser = getUser();
		SelectHavingStep<?> findItpiWithLatestExecution = findItpiWithLatestExecution();
		SelectHavingStep<?> testSuites = getTestSuites();
		SelectHavingStep<?> milestoneDates = getMilestoneDates();
		SelectHavingStep<?> boundToLockedMilestone = getBoundToBlockingMilestone();

		return ITERATION_TEST_PLAN_ITEM
			.innerJoin(ITEM_TEST_PLAN_LIST)
			.on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
			.leftJoin(TEST_CASE_LIBRARY_NODE).on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
			.leftJoin(ITERATION).on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
			.leftJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.leftJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
			.leftJoin(PROJECT).on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.leftJoin(TEST_CASE).on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
			.leftJoin(getAutomationFields)
			.on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(getAutomationFields.field(ITEM_ID, Long.class)))
			.leftJoin(DATASET).on(ITERATION_TEST_PLAN_ITEM.DATASET_ID.eq(DATASET.DATASET_ID))
			.leftJoin(getUser)
			.on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(getUser.field(ITEM_ID, Long.class)))
			.leftJoin(findItpiWithLatestExecution)
			.on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(findItpiWithLatestExecution.field(ITEM_ID, Long.class)))
			.leftJoin(testSuites)
			.on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(testSuites.field(ITEM_ID, Long.class)))
			.leftJoin(milestoneDates)
			.on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(milestoneDates.field(ITEM_ID, Long.class)))
			.leftJoin(boundToLockedMilestone)
			.on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(boundToLockedMilestone.field(ITEM_ID, Long.class)));
	}

	@Override
	protected Condition craftInvariantFilter() {
		return ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(this.iterationId);
	}

	@Override
	protected Field<?> getIdentifier() {
		return ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID;
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return CAMPAIGN_LIBRARY_NODE.PROJECT_ID;
	}

	@Override
	protected SortField<?> getDefaultOrder() {
		return ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ORDER.asc();
	}

	private SelectOnConditionStep<Record2<Long, Long>> findItpiWithLatestExecution() {

		// ITEM_ID | MAX_EXECUTION_ORDER
		SelectHavingStep<?> findLastExecutionByOrder = findLastExecutionByOrder();

		return DSL.select(
				ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.as(ITEM_ID),
				ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID.as(LATEST_EXECUTION_ID))
			.from(ITEM_TEST_PLAN_EXECUTION)
			.innerJoin(findLastExecutionByOrder)
				.on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID
					.eq(findLastExecutionByOrder.field(ITEM_ID, Long.class)))
				.and(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER
					.eq(findLastExecutionByOrder.field(MAX_EXECUTION_ORDER, Integer.class)));
	}

	private SelectHavingStep<Record2<Long, Integer>> findLastExecutionByOrder() {
		return DSL.select(
				ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.as(ITEM_ID),
				DSL.max(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ORDER).as(MAX_EXECUTION_ORDER))
			.from(ITEM_TEST_PLAN_EXECUTION)
			.innerJoin(ITEM_TEST_PLAN_LIST).on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
			.where(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(iterationId))
			.groupBy(DSL.field(ITEM_ID));
	}

	/* Concatenates test suite names e.g. "test suite 1, test suite 2" */
	private SelectHavingStep<?> getTestSuites() {
		// ITEM_ID | TEST_SUITES
		return DSL.select(
			TEST_SUITE_TEST_PLAN_ITEM.TPI_ID.as(ITEM_ID),
			DSL.listAgg(TEST_SUITE.NAME, ", ").withinGroupOrderBy(TEST_SUITE_TEST_PLAN_ITEM.TEST_PLAN_ORDER).as(TEST_SUITES))
			.from(TEST_SUITE_TEST_PLAN_ITEM)
			.innerJoin(TEST_SUITE).on(TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID.eq(TEST_SUITE.ID))
			.innerJoin(ITERATION_TEST_SUITE).on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID))
			.where(ITERATION_TEST_SUITE.ITERATION_ID.eq(iterationId))
			.groupBy(TEST_SUITE_TEST_PLAN_ITEM.TPI_ID);
	}

	/* Get ITPI ExecutionMode from TestCase.  */
	private SelectHavingStep<?> getAutomationFields() {
		return DSL.select(
			ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(ITEM_ID),
			when(
				(PROJECT.AUTOMATION_WORKFLOW_TYPE.eq(NONE.name())
					.or(PROJECT.AUTOMATION_WORKFLOW_TYPE.ne(NONE.name())
						.and(TEST_CASE.AUTOMATABLE.eq(Y.name()))
						.and(AUTOMATION_REQUEST.REQUEST_STATUS.eq(AUTOMATED.name()))))
					// if Jenkins server, TestCase only has to be linked to an automation test
					.and((TEST_AUTOMATION_SERVER.KIND.eq(jenkins.name())
						.and(TEST_CASE.TA_TEST.isNotNull()))
						// if Squash Autom server, then the 3 automation attributes must exist
						.or(TEST_AUTOMATION_SERVER.KIND.eq(squashAutom.name())
							.and(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.isNotNull())
							.and(TEST_CASE.AUTOMATED_TEST_REFERENCE.isNotNull())
							.and(TEST_CASE.SCM_REPOSITORY_ID.isNotNull()))),
				TestCaseExecutionMode.AUTOMATED.name())
				.otherwise(TestCaseExecutionMode.MANUAL.name())
				.as("EXECUTION_MODE"))
			.from(ITERATION_TEST_PLAN_ITEM)
			.innerJoin(TEST_CASE).on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(TEST_CASE.TCLN_ID))
			.innerJoin(TEST_CASE_LIBRARY_NODE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
			.leftJoin(AUTOMATION_REQUEST).on(AUTOMATION_REQUEST.TEST_CASE_ID.eq(TEST_CASE.TCLN_ID))
			.innerJoin(PROJECT).on(PROJECT.PROJECT_ID.eq(TEST_CASE_LIBRARY_NODE.PROJECT_ID))
			.leftJoin(TEST_AUTOMATION_SERVER).on(TEST_AUTOMATION_SERVER.SERVER_ID.eq(PROJECT.TA_SERVER_ID));
	}

	/*	Format the user infos as "LASTNAME (LOGIN)" or "FIRSTNAME LASTNAME (LOGIN)" */
	private SelectHavingStep<?> getUser() {
		// ITEM_ID | USER
		return DSL.select(
			ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(ITEM_ID),
			CORE_USER.LOGIN,
			DSL.when(
				CORE_USER.FIRST_NAME.isNotNull().and(CORE_USER.FIRST_NAME.notEqual("")),
				CORE_USER.FIRST_NAME.concat(" "))
				.otherwise("")
				.concat(CORE_USER.LAST_NAME).concat(" (")
				.concat(CORE_USER.LOGIN).concat(")").as(USER)
		)
			.from(ITERATION_TEST_PLAN_ITEM)
			.innerJoin(CORE_USER)
			.on(ITERATION_TEST_PLAN_ITEM.USER_ID.eq(CORE_USER.PARTY_ID));
	}

	private SelectHavingStep<Record4<Long, Timestamp, Timestamp, String>> getMilestoneDates() {
		// ITEM_ID | MILESTONE_LABELS | MILESTONE_MIN_DATE | MILESTONE_MAX_DATE
		return DSL.select(
			ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(ITEM_ID),
			DSL.min(MILESTONE.END_DATE).as("MILESTONE_MIN_DATE"),
			DSL.max(MILESTONE.END_DATE).as("MILESTONE_MAX_DATE"),
			DSL.listAgg(MILESTONE.LABEL, ", ").withinGroupOrderBy(MILESTONE.END_DATE.asc()).as("MILESTONE_LABELS")
		)
			.from(ITERATION_TEST_PLAN_ITEM)
			.innerJoin(MILESTONE_TEST_CASE)
				.on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(MILESTONE_TEST_CASE.TEST_CASE_ID))
			.innerJoin(MILESTONE).on(MILESTONE.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID))
			.groupBy(
				ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID,
				MILESTONE_TEST_CASE.TEST_CASE_ID
			);
	}

	private SelectHavingStep<Record2<Long, Boolean>> getBoundToBlockingMilestone() {
		// ITEM_ID | BOUND_TO_BLOCKING_MILESTONE
		return DSL.select(
			ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.as(ITEM_ID),
			DSL.field(countDistinct(MILESTONE.MILESTONE_ID).gt(0)).as(BOUND_TO_BLOCKING_MILESTONE)
		)
			.from(ITERATION_TEST_PLAN_ITEM)
			.innerJoin(ITEM_TEST_PLAN_LIST)
				.on(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
			.innerJoin(CAMPAIGN_ITERATION)
				.on(CAMPAIGN_ITERATION.ITERATION_ID.eq(ITEM_TEST_PLAN_LIST.ITERATION_ID))
			.innerJoin(MILESTONE_CAMPAIGN)
				.on(MILESTONE_CAMPAIGN.CAMPAIGN_ID.eq(CAMPAIGN_ITERATION.CAMPAIGN_ID))
			.innerJoin(MILESTONE).on(MILESTONE.MILESTONE_ID.eq(MILESTONE_CAMPAIGN.MILESTONE_ID))
			.where(MILESTONE.STATUS.in(MILESTONE_BLOCKING_STATUSES))
			.groupBy(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID);
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display;

import org.squashtest.tm.domain.testautomation.TestAutomationServerKind;
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationServerDto;

import java.util.List;
import java.util.Set;

public interface TestAutomationServerDisplayDao {

	List<TestAutomationServerDto> findAll();
	Set<TestAutomationServerKind> findAllAvailableKinds();
	TestAutomationServerDto getTestAutomationServerById(long testAutomationServerId);
	List<BoundEnvironmentVariableDto> getDefaultBoundEnvironmentVariablesByServerId(Long evId);

	/**
	 * Does this kind of connector support automated execution environments selection ?
	 * @param connectorKind connector kind
	 * @return true if connector is present and it supports execution environments
	 */
	boolean supportsAutomatedExecutionEnvironments(TestAutomationServerKind connectorKind);

	/**
	 * Does this kind of connector support automated execution environments selection ?
	 * @param connectorKind connector kind (e.g. "squashAUTOM", "jenkins")
	 * @return true if kind is valid, connector is present, and it supports execution environments
	 */
	boolean supportsAutomatedExecutionEnvironments(String connectorKind);
}

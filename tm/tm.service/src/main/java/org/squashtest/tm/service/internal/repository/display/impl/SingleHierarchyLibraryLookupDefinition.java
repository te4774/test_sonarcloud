/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.Field;
import org.jooq.Table;
import org.jooq.TableField;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.jooq.domain.tables.records.ProjectRecord;

import static org.squashtest.tm.jooq.domain.Tables.ACTION_WORD_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_REPORT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

/**
 * Declarative enum of library and related columns in  SquashTM modern trees.
 */
public enum SingleHierarchyLibraryLookupDefinition {

	CUSTOM_REPORT_LIBRARY_LOOKUP(NodeType.CUSTOM_REPORT_LIBRARY, PROJECT.CRL_ID, CUSTOM_REPORT_LIBRARY_NODE),
	ACTION_WORD_LIBRARY_LOOKUP(NodeType.ACTION_WORD_LIBRARY, PROJECT.ARL_ID, ACTION_WORD_LIBRARY_NODE);

	SingleHierarchyLibraryLookupDefinition(NodeType libraryNodeType, TableField<ProjectRecord, Long> libraryColumnInProject, Table<?> nodeTable) {
		this.libraryNodeType = libraryNodeType;
		this.libraryColumnInProject = libraryColumnInProject;
		this.nodeTable = nodeTable;
	}

	private final NodeType libraryNodeType;
	private final TableField<ProjectRecord, Long> libraryColumnInProject;
	private final Table<?> nodeTable;

	public NodeType getLibraryNodeType() {
		return libraryNodeType;
	}

	public Table<?> getNodeTable() {
		return nodeTable;
	}

	public Field<Long> getEntityIdColumn(){
		Field<?> nodeId = nodeTable.field("ENTITY_ID");
		if(Long.class.equals(nodeId.getDataType().getType())){
			return (Field<Long>) nodeId;
		}
		throw new IllegalArgumentException("No suitable column NODE_ID in " + nodeTable);
	}

	public Field<String> getEntityTypeColumn(){
		Field<?> entityType = nodeTable.field("ENTITY_TYPE");
		if(String.class.equals(entityType.getDataType().getType())){
			return (Field<String>) entityType;
		}
		throw new IllegalArgumentException("No suitable column ENTITY_TYPE in " + nodeTable);
	}

	public TableField<?, Long> getNodeIdColumn() {
		TableField<?, ?> tableField = nodeTable.getPrimaryKey().getFields().get(0);
		boolean isLongPrimaryKey = Long.class.equals(tableField.getDataType().getType());
		if(!isLongPrimaryKey){
			throw new IllegalArgumentException("The primary key of table " + tableField.getTable() + " is not a Long Type ");
		}
		// cast is safe we just checked the type above
		return (TableField<?, Long>) tableField;
	}

	public TableField<ProjectRecord, Long> getLibraryColumnInProject() {
		return libraryColumnInProject;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.statistics.testingstatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;
import org.squashtest.tm.service.internal.repository.RemoteSynchronisationDao;
import org.squashtest.tm.service.requirement.RequirementStatisticsService;
import org.squashtest.tm.service.statistics.requirement.RequirementVersionBundleStat;
import org.squashtest.tm.service.statistics.requirement.RequirementVersionBundleStat.SimpleRequirementStats;
import org.squashtest.tm.service.statistics.testingstatus.RemoteRequirementStatistics;
import org.squashtest.tm.service.statistics.testingstatus.RemoteRequirementStatisticsDictionary;
import org.squashtest.tm.service.statistics.testingstatus.RemoteRequirementStatisticsDictionary.RemoteKeyAndServerId;
import org.squashtest.tm.service.statistics.testingstatus.RemoteTestingStatusDao;
import org.squashtest.tm.service.statistics.testingstatus.RemoteTestingStatusService;
import org.squashtest.tm.service.statistics.testingstatus.RequirementsByRemoteKeyByServerId;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class RemoteTestingStatusServiceImpl implements RemoteTestingStatusService {
	private final RequirementStatisticsService requirementStatisticsService;
	private final RemoteSynchronisationDao remoteSynchronisationDao;
	private final RemoteTestingStatusDao remoteTestingStatusDao;

	@Autowired
	public RemoteTestingStatusServiceImpl(RequirementStatisticsService requirementStatisticsService,
										  RemoteSynchronisationDao remoteSynchronisationDao,
										  RemoteTestingStatusDao remoteTestingStatusDao) {
		this.remoteTestingStatusDao = remoteTestingStatusDao;
		this.requirementStatisticsService = requirementStatisticsService;
		this.remoteSynchronisationDao = remoteSynchronisationDao;
	}

	public RemoteRequirementStatisticsDictionary getRemoteRequirementStatisticsDictionary(String remoteSynchronisationKind) {
		final List<RemoteSynchronisation> synchronisations = remoteSynchronisationDao.findByKind(remoteSynchronisationKind);

		if (!synchronisations.isEmpty()) {
			final RequirementsByRemoteKeyByServerId reqsByRemoteKeyByServerId = remoteTestingStatusDao.findRequirementsByRemoteKeyByServerId(remoteSynchronisationKind);
			final Set<Long> allRequirementIds = reqsByRemoteKeyByServerId.extractAllSyncedRequirementIds();
			final RequirementVersionBundleStat bundleStat = requirementStatisticsService.findSimplifiedCoverageStats(allRequirementIds);
			final Map<Long, RequirementVersionBundleStat.SimpleRequirementStats> reqStatsById = bundleStat.getRequirementStats();
			final Map<Long, List<String>> issueRemoteKeyBySynchronisationId = findAllSynchronisedKeys(synchronisations);

			final Map<RemoteKeyAndServerId, Map<Long, RemoteRequirementStatistics>> combined = getCombinedStats(reqsByRemoteKeyByServerId,
				reqStatsById, synchronisations, issueRemoteKeyBySynchronisationId);
			final Map<RemoteKeyAndServerId, RemoteRequirementStatistics> merged = getMergedStats(combined);

			return new RemoteRequirementStatisticsDictionary(combined, merged);
		} else {
			return new RemoteRequirementStatisticsDictionary(new HashMap<>(), new HashMap<>());
		}
	}

	private Map<RemoteKeyAndServerId, Map<Long, RemoteRequirementStatistics>> getCombinedStats(RequirementsByRemoteKeyByServerId reqsByRemoteKeyByServerId,
																							   Map<Long, SimpleRequirementStats> reqStatsById,
																							   List<RemoteSynchronisation> synchronisations,
																							   Map<Long, List<String>> issueRemoteKeyBySynchronisationId) {
		final Map<RemoteKeyAndServerId, Map<Long, RemoteRequirementStatistics>> combinedStats = new HashMap<>();

		synchronisations.stream()
			.filter(RemoteSynchronisation::isSynchronisationEnable)
			.forEach(remoteSynchronisation -> {
				if (issueRemoteKeyBySynchronisationId.containsKey(remoteSynchronisation.getId())) {
					final Long serverId = remoteSynchronisation.getServer().getId();
					final Map<String, List<Long>> reqIdsByRemoteKey = reqsByRemoteKeyByServerId.getRequirementsByRemoteKeyForServerId(serverId);
					final List<String> issueRemoteKeys = issueRemoteKeyBySynchronisationId.get(remoteSynchronisation.getId());

					issueRemoteKeys.forEach(issueRemoteKey -> {
						final List<Long> reqIds = reqIdsByRemoteKey.getOrDefault(issueRemoteKey, Collections.emptyList());
						final Map<Long, RemoteRequirementStatistics> statsByReqId = reqIds.stream()
							.filter(reqStatsById::containsKey)
							.collect(Collectors.toMap(Function.identity(), key -> asRemoteStats(reqStatsById.get(key))));
						final RemoteKeyAndServerId compositeKey = new RemoteKeyAndServerId(issueRemoteKey, serverId);

						combinedStats
							.computeIfAbsent(compositeKey, key -> new HashMap<>())
							.putAll(statsByReqId);
					});
				}
			});

		return combinedStats;
	}

	private Map<RemoteKeyAndServerId, RemoteRequirementStatistics> getMergedStats(Map<RemoteKeyAndServerId, Map<Long, RemoteRequirementStatistics>> combinedStats) {
		return combinedStats.entrySet().stream()
			.map(entry -> Map.entry(entry.getKey(), flattenStats(entry.getValue())))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	private RemoteRequirementStatistics flattenStats(Map<Long, RemoteRequirementStatistics> stats) {
		return RemoteRequirementStatistics.merging(stats.values());
	}

	private RemoteRequirementStatistics asRemoteStats(SimpleRequirementStats simpleRequirementStats) {
		return RemoteRequirementStatistics.fromSimpleStats(simpleRequirementStats);
	}


	private Map<Long, List<String>> findAllSynchronisedKeys(List<RemoteSynchronisation> remoteSynchronisations) {
		return remoteTestingStatusDao.findSynchronisedKeysBySynchronisationId(remoteSynchronisations.stream()
			.map(RemoteSynchronisation::getId)
			.collect(Collectors.toList())
		);
	}
}

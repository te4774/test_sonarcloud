/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.deletion.jdbc;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Table;
import org.jooq.TableOnConditionStep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldHolderType;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.service.internal.api.repository.HibernateSessionClearing;
import org.squashtest.tm.service.internal.attachment.AttachmentRepository;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.jooq.impl.DSL.select;
import static org.squashtest.tm.jooq.domain.Tables.ATTACHMENT_LIST;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_EXECUTION_EXTENDER;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CUSTOM_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_OPTION;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_RENDERING_LOCATION;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_VALUE_OPTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_EXECUTION_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_STEP;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_SUITE_TEST_PLAN_ITEM;
import static org.squashtest.tm.service.internal.deletion.jdbc.delegate.AbstractDelegateDeleteQuery.extractFieldTableNameAsParam;

/**
 * Delete iterations using fast and optimized sql, allowing to avoid the whole 'hibernate web' used as domain.
 * With a proper domain and some simple database cascade this class shouldn't exist...
 */
public class JdbcIterationDeletionHandler extends AbstractJdbcDeletionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(JdbcIterationDeletionHandler.class);

	private final Set<Long> iterationIds;

	private final TableOnConditionStep<Record> joinToTestSuite = ITERATION
		.innerJoin(ITERATION_TEST_SUITE).on(ITERATION_TEST_SUITE.ITERATION_ID.eq(ITERATION.ITERATION_ID))
		.innerJoin(TEST_SUITE).on(TEST_SUITE.ID.eq(ITERATION_TEST_SUITE.TEST_SUITE_ID));

	private final TableOnConditionStep<Record> joinToIterationTestPlanItem = ITERATION
		.innerJoin(ITEM_TEST_PLAN_LIST).on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
		.innerJoin(ITERATION_TEST_PLAN_ITEM).on(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID));

	private final TableOnConditionStep<Record> joinToExecution = joinToIterationTestPlanItem
		.innerJoin(ITEM_TEST_PLAN_EXECUTION).on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID))
		.innerJoin(Tables.EXECUTION).on(Tables.EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID));

	private final Table<Record> joinToExecutionDfv = joinToExecution
		.innerJoin(DENORMALIZED_FIELD_VALUE).on(DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_ID.eq(Tables.EXECUTION.EXECUTION_ID)
			.and(DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_TYPE.eq(DenormalizedFieldHolderType.EXECUTION.name())));

	private final Table<Record> joinToExecutionStep = joinToExecution
		.innerJoin(EXECUTION_EXECUTION_STEPS).on(EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(Tables.EXECUTION.EXECUTION_ID))
		.innerJoin(EXECUTION_STEP).on(EXECUTION_STEP.EXECUTION_STEP_ID.eq(EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID));

	private final Table<Record> joinToExecutionStepDfv = joinToExecutionStep
		.innerJoin(DENORMALIZED_FIELD_VALUE).on(DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_ID.eq(EXECUTION_STEP.EXECUTION_STEP_ID)
			.and(DENORMALIZED_FIELD_VALUE.DENORMALIZED_FIELD_HOLDER_TYPE.eq(DenormalizedFieldHolderType.EXECUTION_STEP.name())));

	private final Condition inIterationIds;

	public JdbcIterationDeletionHandler(Collection<Long> iterationIds,
										DSLContext dslContext,
										EntityManager entityManager,
										AttachmentRepository attachmentRepository,
										JdbcBatchReorderHelper reorderHelper,
										String operationId) {
		super(dslContext, attachmentRepository, reorderHelper, operationId, entityManager);
		this.iterationIds = new HashSet<>(iterationIds);
		inIterationIds = ITERATION.ITERATION_ID.in(iterationIds);
	}

	@HibernateSessionClearing
	public void deleteIterations() {
		logStartProcess();
		clearPersistenceContext();
		storeEntitiesToDeleteIntoWorkingTable();
		performDeletions();
		reorderCampaigns();
		cleanWorkingTable();
		logEndProcess();
	}

	private void logEndProcess() {
		LOGGER.info(String.format("Deleted Iterations %s. Time elapsed %s", iterationIds, startDate.until(LocalDateTime.now(), ChronoUnit.MILLIS)));
	}

	private void performDeletions() {
		deleteDenormalizedCustomFieldValues();
		deleteExecutionSteps();
		deleteKeywordExecution();
		deleteExecutions();
		deleteTestSuites();
		deleteIterationTestPlanItem();
		deleteIterationEntities();
		deleteIssueLists();
		deleteCustomFieldValues();
		deleteAttachmentLists();
		deleteAttachmentContents();
	}

	private void reorderCampaigns() {
		// Here we fetch the ids, but the list should be rather small, because it's the campaign top object.
		List<Long> campaignIds = workingTables.selectIds(CAMPAIGN.CLN_ID);
		reorderHelper.reorder(
			campaignIds,
			CAMPAIGN_ITERATION,
			CAMPAIGN_ITERATION.CAMPAIGN_ID,
			CAMPAIGN_ITERATION.ITERATION_ID,
			CAMPAIGN_ITERATION.ITERATION_ORDER);

		LOGGER.debug("Reordered campaign content. Time on this stage " + timeDiff() + " ms");
		updateTime();
	}

	private void deleteIssueLists() {
		workingTables.delete(ISSUE_LIST.ISSUE_LIST_ID, ISSUE.ISSUE_LIST_ID);
		workingTables.delete(ISSUE_LIST.ISSUE_LIST_ID, ISSUE_LIST.ISSUE_LIST_ID);
		logDelete(ISSUE_LIST);
	}

	private void deleteIterationEntities() {
		workingTables.delete(ITERATION.ITERATION_ID, AUTOMATED_SUITE.ITERATION_ID);
		workingTables.delete(ITERATION.ITERATION_ID, CAMPAIGN_ITERATION.ITERATION_ID);
		workingTables.delete(ITERATION.ITERATION_ID, ITERATION.ITERATION_ID);
		logDelete(ITERATION);
	}

	private void deleteIterationTestPlanItem() {
		workingTables.delete(ITERATION.ITERATION_ID, ITEM_TEST_PLAN_LIST.ITERATION_ID);
		workingTables.delete(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID, ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID);
		logDelete(ITERATION_TEST_PLAN_ITEM);
	}

	private void deleteTestSuites() {
		workingTables.delete(ITERATION.ITERATION_ID, ITERATION_TEST_SUITE.ITERATION_ID);
		workingTables.delete(TEST_SUITE.ID, TEST_SUITE_TEST_PLAN_ITEM.SUITE_ID);
		workingTables.delete(TEST_SUITE.ID, AUTOMATED_SUITE.TEST_SUITE_ID);
		workingTables.delete(TEST_SUITE.ID, TEST_SUITE.ID);
		logDelete(TEST_SUITE);
	}

	private void deleteExecutions() {
		workingTables.delete(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID, ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID);
		workingTables.delete(EXECUTION.EXECUTION_ID, AUTOMATED_EXECUTION_EXTENDER.MASTER_EXECUTION_ID);
		workingTables.delete(EXECUTION.EXECUTION_ID, SCRIPTED_EXECUTION.EXECUTION_ID);
		workingTables.delete(EXECUTION.EXECUTION_ID, EXECUTION.EXECUTION_ID);
		logDelete(EXECUTION);
	}

	private void deleteExecutionSteps() {
		workingTables.delete(EXECUTION.EXECUTION_ID, EXECUTION_EXECUTION_STEPS.EXECUTION_ID);
		workingTables.delete(EXECUTION_STEP.EXECUTION_STEP_ID, EXECUTION_STEP.EXECUTION_STEP_ID);
		logDelete(EXECUTION_STEP);
	}

	private void deleteKeywordExecution() {
		workingTables.delete(EXECUTION.EXECUTION_ID, KEYWORD_EXECUTION.EXECUTION_ID);
		logDelete(KEYWORD_EXECUTION);
	}

	private void logStartProcess() {
		LOGGER.debug(String.format("Init deletion process of iterations %s. Operation:  %s", iterationIds, operationId));
	}

	private void deleteDenormalizedCustomFieldValues() {
		workingTables.delete(DENORMALIZED_FIELD_VALUE.DFV_ID, DENORMALIZED_FIELD_OPTION.DFV_ID);
		workingTables.delete(DENORMALIZED_FIELD_VALUE.DFV_ID, DENORMALIZED_FIELD_VALUE_OPTION.DFV_ID);
		workingTables.delete(DENORMALIZED_FIELD_VALUE.DFV_ID, DENORMALIZED_FIELD_RENDERING_LOCATION.DFV_ID);
		workingTables.delete(DENORMALIZED_FIELD_VALUE.DFV_ID, DENORMALIZED_FIELD_VALUE.DFV_ID);
		logDelete(DENORMALIZED_FIELD_VALUE);
	}

	private void storeEntitiesToDeleteIntoWorkingTable() {
		addDenormalizedFiledValue();
		addIssueList();
		addCampaign();
		addIteration();
		addTestSuite();
		addIterationTestPlanItem();
		addExecution();
		addExecutionSteps();
		addCustomFieldValues();
		addAttachmentList();
		logReferenceEntitiesComplete();
	}

	private void addAttachmentList() {
		workingTables.addEntity(
			ATTACHMENT_LIST.ATTACHMENT_LIST_ID,
			() -> makeSelectAttachmentList(EXECUTION_STEP.EXECUTION_STEP_ID, EXECUTION_STEP.ATTACHMENT_LIST_ID)
				.union(makeSelectAttachmentList(EXECUTION.EXECUTION_ID, EXECUTION.ATTACHMENT_LIST_ID))
				.union(makeSelectAttachmentList(TEST_SUITE.ID, TEST_SUITE.ATTACHMENT_LIST_ID))
				.union(makeSelectAttachmentList(ITERATION.ITERATION_ID, ITERATION.ATTACHMENT_LIST_ID))
				// For an unknown reason a dev broke a 12 year and 150+ table convention
				// and choose to use a string as primary key, in place of a good old Long/BigInteger...
				// No way to easily fix that for now but maybe one day we will have time for this...
				.union(
					select(AUTOMATED_SUITE.ATTACHMENT_LIST_ID, extractFieldTableNameAsParam(ATTACHMENT_LIST.ATTACHMENT_LIST_ID), operationIdParam)
						.from(AUTOMATED_SUITE)
						.where(AUTOMATED_SUITE.ITERATION_ID.in(iterationIds)))
		);
	}

	private void addCampaign() {
		workingTables.addEntity(
			CAMPAIGN.CLN_ID,
			() -> makeSelectClause(CAMPAIGN.CLN_ID)
				.from(CAMPAIGN)
				.innerJoin(CAMPAIGN_ITERATION).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN.CLN_ID))
				.innerJoin(ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
				.where(inIterationIds)
		);
	}

	private void addIteration() {
		workingTables.addEntity(
			ITERATION.ITERATION_ID,
			() -> makeSelectClause(ITERATION.ITERATION_ID)
				.from(ITERATION)
				.where(inIterationIds)
		);
	}

	private void addCustomFieldValues() {
		workingTables.addEntity(
			CUSTOM_FIELD_VALUE.CFV_ID,
			() -> makeSelectCustomFieldValues(EXECUTION_STEP.EXECUTION_STEP_ID, BindableEntity.EXECUTION_STEP)
				.union(makeSelectCustomFieldValues(EXECUTION.EXECUTION_ID, BindableEntity.EXECUTION))
				.union(makeSelectCustomFieldValues(TEST_SUITE.ID, BindableEntity.TEST_SUITE))
				.union(makeSelectCustomFieldValues(ITERATION.ITERATION_ID, BindableEntity.ITERATION))
		);
	}

	private void addExecutionSteps() {
		workingTables.addEntity(
			EXECUTION_STEP.EXECUTION_STEP_ID,
			() -> makeSelectClause(EXECUTION_STEP.EXECUTION_STEP_ID)
				.from(joinToExecutionStep)
				.where(inIterationIds)
		);
	}

	private void addExecution() {
		workingTables.addEntity(
			EXECUTION.EXECUTION_ID,
			() -> makeSelectClause(EXECUTION.EXECUTION_ID)
				.from(joinToExecution)
				.where(inIterationIds)
		);
	}

	private void addIterationTestPlanItem() {
		workingTables.addEntity(
			ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID,
			() -> makeSelectClause(ITERATION_TEST_PLAN_ITEM.ITEM_TEST_PLAN_ID)
				.from(joinToIterationTestPlanItem)
				.where(inIterationIds));
	}

	private void addTestSuite() {
		workingTables.addEntity(
			TEST_SUITE.ID,
			() -> makeSelectClause(TEST_SUITE.ID)
				.from(joinToTestSuite)
				.where(inIterationIds));
	}

	private void addIssueList() {
		workingTables.addEntity(
			ISSUE_LIST.ISSUE_LIST_ID,
			() -> makeSelectClause(ISSUE_LIST.ISSUE_LIST_ID)
				.from(ISSUE_LIST)
				.innerJoin(joinToExecution).on(EXECUTION.ISSUE_LIST_ID.eq(ISSUE_LIST.ISSUE_LIST_ID))
				.where(inIterationIds).union(
					makeSelectClause(ISSUE_LIST.ISSUE_LIST_ID)
						.from(ISSUE_LIST)
						.innerJoin(joinToExecutionStep).on(EXECUTION_STEP.ISSUE_LIST_ID.eq(ISSUE_LIST.ISSUE_LIST_ID))
						.where(inIterationIds)
				));
	}

	private void addDenormalizedFiledValue() {
		workingTables.addEntity(
			DENORMALIZED_FIELD_VALUE.DFV_ID,
			() -> makeSelectClause(DENORMALIZED_FIELD_VALUE.DFV_ID)
				.from(joinToExecutionStepDfv)
				.where(inIterationIds)
				.union(
					makeSelectClause(DENORMALIZED_FIELD_VALUE.DFV_ID)
						.from(joinToExecutionDfv)
						.where(inIterationIds)
				));
	}

	@Override
	protected Logger getLogger() {
		return LOGGER;
	}
}

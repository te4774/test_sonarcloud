/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.local;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SelectHavingStep;
import org.jooq.SelectSeekStep1;
import org.jooq.Table;
import org.squashtest.tm.core.foundation.collection.PagingAndSorting;
import org.squashtest.tm.service.bugtracker.knownissues.local.LocalKnownIssue;
import org.squashtest.tm.service.bugtracker.knownissues.local.LocalKnownIssueFinder;

import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.BUGTRACKER_BINDING;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.CAMPAIGN_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.EXECUTION_ISSUES_CLOSURE;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_EXECUTION;
import static org.squashtest.tm.jooq.domain.Tables.ITEM_TEST_PLAN_LIST;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;

/**
 * Base KnownIssueFinder implementation for most known issue finders.
 */
public abstract class BaseLocalKnownIssueFinder<RECORD extends Record> implements LocalKnownIssueFinder {

	protected final DSLContext dsl;

	protected BaseLocalKnownIssueFinder(DSLContext dsl) {
		this.dsl = dsl;
	}

	@Override
	public int countKnownIssues(Long entityId) {
		return selectKnownIssues(entityId).fetch().size();
	}

	@Override
	public List<LocalKnownIssue> getPaginatedKnownIssues(Long entityId, PagingAndSorting paging) {
		SelectSeekStep1<RECORD, String> nonPaginatedQuery = selectKnownIssues(entityId)
			.orderBy(LocalKnownIssueFinderHelper.getOrderField(paging));

		Result<RECORD> records;

		if (paging.shouldDisplayAll()) {
			records = nonPaginatedQuery.fetch();
		} else {
			records = nonPaginatedQuery
				.limit(paging.getFirstItemIndex(), paging.getPageSize())
				.fetch();
		}

		return records.map(this::buildIssueFromRecord);
	}

	protected Table<?> getTable() {
		return getIssueToBugtrackerBindingJoin();
	}

	protected static Table<?> getIssueToBugtrackerBindingJoin() {
		return ISSUE
			.innerJoin(EXECUTION_ISSUES_CLOSURE).on(ISSUE.ISSUE_ID.eq(EXECUTION_ISSUES_CLOSURE.ISSUE_ID))
			.innerJoin(EXECUTION).on(EXECUTION.EXECUTION_ID.eq(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID))
			.innerJoin(ITEM_TEST_PLAN_EXECUTION).on(EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
			.innerJoin(ITEM_TEST_PLAN_LIST).on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID))
			.innerJoin(ITERATION).on(ITEM_TEST_PLAN_LIST.ITERATION_ID.eq(ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_ITERATION).on(ITERATION.ITERATION_ID.eq(CAMPAIGN_ITERATION.ITERATION_ID))
			.innerJoin(CAMPAIGN_LIBRARY_NODE).on(CAMPAIGN_ITERATION.CAMPAIGN_ID.eq(CAMPAIGN_LIBRARY_NODE.CLN_ID))
			.innerJoin(PROJECT).on(CAMPAIGN_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.innerJoin(BUGTRACKER_BINDING).on(BUGTRACKER_BINDING.PROJECT_ID.eq(PROJECT.PROJECT_ID));
	}

	abstract protected SelectHavingStep<RECORD> selectKnownIssues(long entityId);
	abstract protected LocalKnownIssue buildIssueFromRecord(RECORD record);
}

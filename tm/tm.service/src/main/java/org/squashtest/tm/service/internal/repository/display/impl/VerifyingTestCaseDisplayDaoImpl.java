/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;


import org.jooq.DSLContext;
import org.jooq.Record2;
import org.jooq.Record4;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.requirement.VerifyingTestCaseDto;
import org.squashtest.tm.service.internal.repository.RequirementDao;
import org.squashtest.tm.service.internal.repository.display.VerifyingTestCaseDisplayDao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.squashtest.tm.jooq.domain.Tables.HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.ITERATION_TEST_PLAN_ITEM;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.ID;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MAX_LAST_EXECUTED_ON;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_LABELS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_MAX_DATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.MILESTONE_MIN_DATE;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.PROJECT_NAME;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.STATUS;
import static org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants.TCLN_ID;

@Repository
public class VerifyingTestCaseDisplayDaoImpl implements VerifyingTestCaseDisplayDao {

	private final RequirementDao requirementDao;

	private final DSLContext dsl;

	public VerifyingTestCaseDisplayDaoImpl(DSLContext dsl,
										   RequirementDao requirementDao) {
		this.dsl = dsl;
		this.requirementDao = requirementDao;
	}

	@Override
	public List<VerifyingTestCaseDto> findByRequirementVersionIds(Set<Long> requirementVersionIds) {
		SelectHavingStep<?> milestoneDates = getMilestoneDates();
		List<VerifyingTestCaseDto> verifyingTestCaseDtos = dsl.select(TEST_CASE.TCLN_ID.as(ID), TEST_CASE.REFERENCE, TEST_CASE.TC_STATUS.as(STATUS), TEST_CASE.IMPORTANCE,
			TEST_CASE_LIBRARY_NODE.NAME, PROJECT.NAME.as(PROJECT_NAME),
			DSL.field(MILESTONE_LABELS), DSL.field(MILESTONE_MIN_DATE), DSL.field(MILESTONE_MAX_DATE))
			.from(TEST_CASE)
			.innerJoin(TEST_CASE_LIBRARY_NODE).on(TEST_CASE.TCLN_ID.eq(TEST_CASE_LIBRARY_NODE.TCLN_ID))
			.innerJoin(PROJECT).on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.innerJoin(REQUIREMENT_VERSION_COVERAGE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID))
			.leftJoin(milestoneDates).on(milestoneDates.field(ID, Long.class).eq(TEST_CASE.TCLN_ID))
			.where(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.in(requirementVersionIds))
			.fetchInto(VerifyingTestCaseDto.class);

		if (!verifyingTestCaseDtos.isEmpty()) {
			appendLastExecutionStatus(verifyingTestCaseDtos);
		}

		return verifyingTestCaseDtos;
	}

	@Override
	public List<VerifyingTestCaseDto> findByHighLevelRequirementVersionId(Long highLevelRequirementVersionId) {
		List<VerifyingTestCaseDto> directlyLinkedVerifyingTestCases = findByRequirementVersionIds(new HashSet<>(Collections.singletonList(highLevelRequirementVersionId)));
		Long highLevelRequirementId = requirementDao.findRequirementIdFromVersionId(highLevelRequirementVersionId);
		Set<Long> linkedRequirementIds = findLinkedLowLevelRequirementIds(highLevelRequirementId);

		if (linkedRequirementIds.isEmpty()) { // stop here if not a high level requirement or has not linked low level requirements
			return directlyLinkedVerifyingTestCases;
		}

		List<Long> descendantIds = requirementDao.findDescendantRequirementIds(new ArrayList<>(linkedRequirementIds));
		linkedRequirementIds.addAll(descendantIds);
		Set<Long> linkedRequirementCurrentVersionIds = requirementDao.findRequirementCurrentVersionIdsFromRequirementIds(linkedRequirementIds);

		List<VerifyingTestCaseDto> lowLevelReqVerifyingTestCases = findByRequirementVersionIds(linkedRequirementCurrentVersionIds);
		lowLevelReqVerifyingTestCases.forEach(testCase -> testCase.setDirectlyLinked(false));
		return Stream.concat(directlyLinkedVerifyingTestCases.stream(), lowLevelReqVerifyingTestCases.stream()).collect(Collectors.toList());
	}

	private Set<Long> findLinkedLowLevelRequirementIds(Long requirementId) {
		return dsl.selectDistinct(REQUIREMENT.RLN_ID)
			.from(HIGH_LEVEL_REQUIREMENT)
			.innerJoin(REQUIREMENT).on(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID))
			.where(HIGH_LEVEL_REQUIREMENT.RLN_ID.eq(requirementId))
			.fetchSet(REQUIREMENT.RLN_ID);
	}

	private SelectHavingStep<Record4<Long, Timestamp, Timestamp, String>> getMilestoneDates() {
		return DSL.select(
			TEST_CASE.TCLN_ID.as(ID),
			DSL.min(MILESTONE.END_DATE).as(MILESTONE_MIN_DATE),
			DSL.max(MILESTONE.END_DATE).as(MILESTONE_MAX_DATE),
			DSL.listAgg(MILESTONE.LABEL, ", ").withinGroupOrderBy(MILESTONE.END_DATE.asc()).as(MILESTONE_LABELS)
		)
			.from(TEST_CASE)
			.innerJoin(MILESTONE_TEST_CASE)
			.on(TEST_CASE.TCLN_ID.eq(MILESTONE_TEST_CASE.TEST_CASE_ID))
			.innerJoin(MILESTONE).on(MILESTONE.MILESTONE_ID.eq(MILESTONE_TEST_CASE.MILESTONE_ID))
			.groupBy(
				TEST_CASE.TCLN_ID,
				MILESTONE_TEST_CASE.TEST_CASE_ID
			);
	}

	// SQUASH-4534 : encountered performance issues doing it directly in the main request (with many executions in database)
	private void appendLastExecutionStatus(List<VerifyingTestCaseDto> verifyingTestCaseDtos) {
		Map<Long, String> lastExecutionStatusByTclnId = getLastExecutionStatusByTclnId(verifyingTestCaseDtos);

		verifyingTestCaseDtos.forEach(verifyingTestCaseDto -> {
			String LastExecutionStatus = lastExecutionStatusByTclnId.get(verifyingTestCaseDto.getId());
			if (LastExecutionStatus != null) {
				verifyingTestCaseDto.setLastExecutionStatus(LastExecutionStatus);
			}
		});
	}

	private SelectHavingStep<Record2<Long, Timestamp>> getLastExecutedOnDate(List<Long> verifyingTestCaseDtoIds) {
		return dsl.select(ITERATION_TEST_PLAN_ITEM.TCLN_ID, DSL.max(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON).as(MAX_LAST_EXECUTED_ON))
				.from(ITERATION_TEST_PLAN_ITEM)
				.where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.in(verifyingTestCaseDtoIds))
				.groupBy(ITERATION_TEST_PLAN_ITEM.TCLN_ID);
	}

	private Map<Long, String> getLastExecutionStatusByTclnId(List<VerifyingTestCaseDto> verifyingTestCaseDtos) {
		List<Long> verifyingTestCaseDtoIds = verifyingTestCaseDtos.stream().map(VerifyingTestCaseDto::getId).collect(Collectors.toList());
		SelectHavingStep<?> lastExecutedOnDate = getLastExecutedOnDate(verifyingTestCaseDtoIds);
		return dsl.selectDistinct(ITERATION_TEST_PLAN_ITEM.TCLN_ID, ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS)
				  .from(ITERATION_TEST_PLAN_ITEM)
				  .join(lastExecutedOnDate).on(ITERATION_TEST_PLAN_ITEM.TCLN_ID.eq(lastExecutedOnDate.field(TCLN_ID, Long.class)))
				  .where(ITERATION_TEST_PLAN_ITEM.TCLN_ID.in(verifyingTestCaseDtoIds)).and(ITERATION_TEST_PLAN_ITEM.LAST_EXECUTED_ON.eq(lastExecutedOnDate.field(MAX_LAST_EXECUTED_ON, Timestamp.class)))
				  .fetchMap(ITERATION_TEST_PLAN_ITEM.TCLN_ID, ITERATION_TEST_PLAN_ITEM.EXECUTION_STATUS);
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid;

import org.springframework.data.domain.Page;
import org.squashtest.tm.core.foundation.collection.PagedCollectionHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class GridResponseBuilder<X> {

	private long currentIndex = 0;

	protected final long getCurrentIndex() {
		return currentIndex;
	}

	public final GridResponse buildGridResponse(PagedCollectionHolder<List<X>> holder, String entityType) {

		GridResponse response = new GridResponse();
		response.setCount(holder.getTotalNumberOfItems());
		response.setDataRows(buildDataRowModel(holder.getPagedItems(), entityType));
		return response;
	}

	public final GridResponse buildGridResponse(Page<X> page, String entityType) {
		GridResponse response = new GridResponse();
		response.setCount(page.getTotalElements());
		response.setDataRows(buildDataRowModel(page.getContent(), entityType));
		return response;
	}

	private List<DataRow> buildDataRowModel(Collection<X> pagedItems, String entityType) {
		List<DataRow> dataRows = new ArrayList<>(pagedItems.size());

		for (X item : pagedItems) {
			DataRow dataRow = buildItemDataRow(item);
			dataRow.setId(entityType + '-' + currentIndex);
			dataRows.add(dataRow);
			currentIndex++;

		}

		return dataRows;
	}

	protected abstract DataRow buildItemDataRow(X item);
}

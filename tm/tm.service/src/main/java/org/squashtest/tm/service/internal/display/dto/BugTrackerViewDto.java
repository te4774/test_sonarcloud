/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.dto;

import org.squashtest.tm.service.servers.ServerAuthConfiguration;

import java.util.List;
import java.util.Set;

public class BugTrackerViewDto extends BugTrackerDto {
    private Set<String> bugTrackerKinds;
    private CredentialsDto credentials;
    private ServerAuthConfiguration authConfiguration;
    private List<String> supportedAuthenticationProtocols;

    public Set<String> getBugTrackerKinds() {
        return bugTrackerKinds;
    }

    public void setBugTrackerKinds(Set<String> bugTrackerKinds) {
        this.bugTrackerKinds = bugTrackerKinds;
    }

    public CredentialsDto getCredentials() {
        return credentials;
    }

    public void setCredentials(CredentialsDto credentials) {
        this.credentials = credentials;
    }

    public ServerAuthConfiguration getAuthConfiguration() {
        return authConfiguration;
    }

    public void setAuthConfiguration(ServerAuthConfiguration authConfiguration) {
        this.authConfiguration = authConfiguration;
    }

    public List<String> getSupportedAuthenticationProtocols() {
        return supportedAuthenticationProtocols;
    }

    public void setSupportedAuthenticationProtocols(List<String> supportedAuthenticationProtocols) {
        this.supportedAuthenticationProtocols = supportedAuthenticationProtocols;
    }
}

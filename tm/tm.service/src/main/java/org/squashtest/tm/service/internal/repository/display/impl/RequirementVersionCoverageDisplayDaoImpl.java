/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.impl;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Record4;
import org.jooq.SelectHavingStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.squashtest.tm.service.internal.display.dto.CoverageStepInfoDto;
import org.squashtest.tm.service.internal.display.dto.RequirementVersionCoverageDto;
import org.squashtest.tm.service.internal.repository.display.RequirementVersionCoverageDisplayDao;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import static java.util.Collections.emptySet;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_REQ_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_VERSION_COVERAGE;
import static org.squashtest.tm.jooq.domain.Tables.RESOURCE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_STEPS;
import static org.squashtest.tm.jooq.domain.Tables.VERIFYING_STEPS;

@Repository
public class RequirementVersionCoverageDisplayDaoImpl implements RequirementVersionCoverageDisplayDao {

	private static final String MILESTONE_LABELS = "MILESTONE_LABELS";
	private static final String MILESTONE_MAX_DATE = "MILESTONE_MAX_DATE";
	private static final String MILESTONE_MIN_DATE = "MILESTONE_MIN_DATE";
	private static final String RES_ID = "RES_ID";

	@Inject
	private DSLContext dsl;

	@Override
	public List<RequirementVersionCoverageDto> findDirectCoverageByTestCaseId(Long testCaseId) {
		return this.findByTestCaseIds(testCaseId, emptySet());
	}

	@Override
	public List<RequirementVersionCoverageDto> findByTestCaseIds(Long testCaseId, Set<Long> calledTestCaseIds) {
		HashSet<Long> allIds = new HashSet<>(calledTestCaseIds);
		allIds.add(testCaseId);
		SelectHavingStep<?> milestoneDates = getMilestoneDates();
		return dsl.select(
			PROJECT.PROJECT_ID, PROJECT.NAME,
			REQUIREMENT_VERSION.RES_ID, REQUIREMENT_VERSION.REQUIREMENT_ID, REQUIREMENT_VERSION.REFERENCE, RESOURCE.NAME, REQUIREMENT_VERSION.CRITICALITY, REQUIREMENT_VERSION.REQUIREMENT_STATUS,
			VERIFYING_STEPS.TEST_STEP_ID, TEST_CASE_STEPS.STEP_ORDER, REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID,
			DSL.field(MILESTONE_LABELS), DSL.field(MILESTONE_MIN_DATE), DSL.field(MILESTONE_MAX_DATE))
			.from(RESOURCE)
			.innerJoin(REQUIREMENT_VERSION).on(REQUIREMENT_VERSION.RES_ID.eq(RESOURCE.RES_ID))
			.innerJoin(REQUIREMENT_VERSION_COVERAGE).on(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
			.innerJoin(REQUIREMENT_LIBRARY_NODE).on(REQUIREMENT_LIBRARY_NODE.RLN_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
			.innerJoin(PROJECT).on(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.leftJoin(VERIFYING_STEPS).on(VERIFYING_STEPS.REQUIREMENT_VERSION_COVERAGE_ID.eq(REQUIREMENT_VERSION_COVERAGE.REQUIREMENT_VERSION_COVERAGE_ID))
			.leftJoin(TEST_CASE_STEPS).on(TEST_CASE_STEPS.STEP_ID.eq(VERIFYING_STEPS.TEST_STEP_ID))
			.leftJoin(milestoneDates)
			.on(REQUIREMENT_VERSION.RES_ID.eq(milestoneDates.field(RES_ID, Long.class)))
			.where(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.in(allIds))
			.orderBy(REQUIREMENT_VERSION.RES_ID)
			.fetch().stream().collect(new CoverageCollector(testCaseId));
	}

	private SelectHavingStep<Record4<Long, Timestamp, Timestamp, String>> getMilestoneDates() {
		return DSL.select(
			REQUIREMENT_VERSION.RES_ID.as(RES_ID),
			DSL.min(MILESTONE.END_DATE).as(MILESTONE_MIN_DATE),
			DSL.max(MILESTONE.END_DATE).as(MILESTONE_MAX_DATE),
			DSL.listAgg(MILESTONE.LABEL, ", ").withinGroupOrderBy(MILESTONE.END_DATE.asc()).as(MILESTONE_LABELS)
		)
			.from(REQUIREMENT_VERSION)
			.innerJoin(MILESTONE_REQ_VERSION)
			.on(REQUIREMENT_VERSION.RES_ID.eq(MILESTONE_REQ_VERSION.REQ_VERSION_ID))
			.innerJoin(MILESTONE).on(MILESTONE.MILESTONE_ID.eq(MILESTONE_REQ_VERSION.MILESTONE_ID))
			.groupBy(
				REQUIREMENT_VERSION.RES_ID,
				MILESTONE_REQ_VERSION.REQ_VERSION_ID
			);
	}

	@Override
	public Map<Long, Integer> countByTestStepId(List<Long> testStepIds) {
		return dsl.select(VERIFYING_STEPS.TEST_STEP_ID, DSL.count())
			.from(VERIFYING_STEPS)
			.where(VERIFYING_STEPS.TEST_STEP_ID.in(testStepIds))
			.groupBy(VERIFYING_STEPS.TEST_STEP_ID)
			.fetch()
			.intoMap(VERIFYING_STEPS.TEST_STEP_ID, DSL.count());
	}

	private static class CoverageCollector implements Collector<Record, Map<Long, RequirementVersionCoverageDto>, List<RequirementVersionCoverageDto>> {

		private Long rootTcId;

		CoverageCollector(Long rootTcId) {
			this.rootTcId = rootTcId;
		}

		@Override
		public Supplier<Map<Long, RequirementVersionCoverageDto>> supplier() {
			return LinkedHashMap::new;
		}

		@Override
		public BiConsumer<Map<Long, RequirementVersionCoverageDto>, Record> accumulator() {
			return (coverageMap, record) -> {
				Long reqVersionId = record.get(REQUIREMENT_VERSION.RES_ID);
				if (coverageMap.containsKey(reqVersionId)) {
					RequirementVersionCoverageDto coverageDto = coverageMap.get(reqVersionId);
					this.updateCoverage(record, coverageDto);
				} else {
					RequirementVersionCoverageDto coverageDto = this.createCoverage(record);
					coverageMap.put(coverageDto.getRequirementVersionId(), coverageDto);
				}
			};
		}

		@Override
		public BinaryOperator<Map<Long, RequirementVersionCoverageDto>> combiner() {
			return (map1, map2) -> {
				throw new UnsupportedOperationException();
			};
		}

		@Override
		public Function<Map<Long, RequirementVersionCoverageDto>, List<RequirementVersionCoverageDto>> finisher() {
			return (coverageMap) -> new ArrayList<>(coverageMap.values());
		}

		@Override
		public Set<Characteristics> characteristics() {
			return emptySet();
		}

		private void updateCoverage(Record record, RequirementVersionCoverageDto coverageDto) {
			Long verifyingStepId = record.get(VERIFYING_STEPS.TEST_STEP_ID);
			Long verifyingTestCaseId = record.get(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID);
			Integer stepIndex = record.get(TEST_CASE_STEPS.STEP_ORDER);
			if (rootTcId.equals(verifyingTestCaseId)) {

				if (Objects.isNull(coverageDto.getVerifyingTestCaseId())) {
					coverageDto.setVerifyingTestCaseId(verifyingTestCaseId);
				}

				if (Objects.nonNull(verifyingStepId)) {
					CoverageStepInfoDto coverageStepInfoDto = new CoverageStepInfoDto();
					coverageStepInfoDto.setId(verifyingStepId);
					coverageStepInfoDto.setIndex(stepIndex);
					coverageDto.addCoverageStepInfo(coverageStepInfoDto);
				}
			} else {
				coverageDto.addCalledVerifyingTestCase(verifyingTestCaseId);
			}
		}

		private RequirementVersionCoverageDto createCoverage(Record record) {
			Long verifyingStepId = record.get(VERIFYING_STEPS.TEST_STEP_ID);
			Integer stepIndex = record.get(TEST_CASE_STEPS.STEP_ORDER);
			Long verifyingTestCaseId = record.get(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID);
			RequirementVersionCoverageDto coverageDto = new RequirementVersionCoverageDto();
			coverageDto.setRequirementVersionId(record.get(REQUIREMENT_VERSION.RES_ID));
			coverageDto.setRequirementId(record.get(REQUIREMENT_VERSION.REQUIREMENT_ID));
			coverageDto.setProjectId(record.get(PROJECT.PROJECT_ID));
			coverageDto.setProjectName(record.get(PROJECT.NAME));
			coverageDto.setReference(record.get(REQUIREMENT_VERSION.REFERENCE));
			coverageDto.setName(record.get(RESOURCE.NAME));
			coverageDto.setCriticality(record.get(REQUIREMENT_VERSION.CRITICALITY));
			coverageDto.setStatus(record.get(REQUIREMENT_VERSION.REQUIREMENT_STATUS));
			coverageDto.setMilestoneLabels((String) record.get(MILESTONE_LABELS));
			coverageDto.setMilestoneMaxDate((Date) record.get(MILESTONE_MAX_DATE));
			coverageDto.setMilestoneMinDate((Date) record.get(MILESTONE_MIN_DATE));
			if (rootTcId.equals(verifyingTestCaseId)) {
				coverageDto.setVerifyingTestCaseId(verifyingTestCaseId);
				if (Objects.nonNull(verifyingStepId)) {
					CoverageStepInfoDto coverageStepInfoDto = new CoverageStepInfoDto();
					coverageStepInfoDto.setId(verifyingStepId);
					coverageStepInfoDto.setIndex(stepIndex);
					coverageDto.addCoverageStepInfo(coverageStepInfoDto);
				}
			} else {
				coverageDto.addCalledVerifyingTestCase(verifyingTestCaseId);
			}
			return coverageDto;
		}
	}
}

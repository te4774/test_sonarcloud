/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.dto;

import java.util.ArrayList;
import java.util.List;

public class HighLevelRequirementExceptionSummary {
    private List<String> requirementWithNotLinkableStatus = new ArrayList<>();
    private List<String> alreadyLinked = new ArrayList<>();
    private List<String> alreadyLinkedToAnotherHighLevelRequirement = new ArrayList<>();
    private List<String> highLevelRequirementsInSelection = new ArrayList<>();
    private List<String> childRequirementsInSelection = new ArrayList<>();

    public List<String> getRequirementWithNotLinkableStatus() {
        return requirementWithNotLinkableStatus;
    }

    public void setRequirementWithNotLinkableStatus(List<String> requirementWithNotLinkableStatus) {
        this.requirementWithNotLinkableStatus = requirementWithNotLinkableStatus;
    }

    public List<String> getAlreadyLinked() {
        return alreadyLinked;
    }

    public void setAlreadyLinked(List<String> alreadyLinked) {
        this.alreadyLinked = alreadyLinked;
    }

    public List<String> getAlreadyLinkedToAnotherHighLevelRequirement() {
        return alreadyLinkedToAnotherHighLevelRequirement;
    }

    public void setAlreadyLinkedToAnotherHighLevelRequirement(List<String> alreadyLinkedToAnotherHighLevelRequirement) {
        this.alreadyLinkedToAnotherHighLevelRequirement = alreadyLinkedToAnotherHighLevelRequirement;
    }

    public List<String> getChildRequirementsInSelection() {
        return childRequirementsInSelection;
    }

    public void setChildRequirementsInSelection(List<String> childRequirementsInSelection) {
        this.childRequirementsInSelection = childRequirementsInSelection;
    }

    public List<String> getHighLevelRequirementsInSelection() {
        return highLevelRequirementsInSelection;
    }

    public void setHighLevelRequirementsInSelection(List<String> highLevelRequirementsInSelection) {
        this.highLevelRequirementsInSelection = highLevelRequirementsInSelection;
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.grid.tf;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.SelectHavingStep;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.domain.project.AutomationWorkflowType;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.service.internal.display.grid.AbstractGrid;
import org.squashtest.tm.service.internal.display.grid.columns.GridColumn;
import org.squashtest.tm.service.internal.display.grid.columns.LevelEnumColumn;

import java.util.Arrays;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_TEST;
import static org.squashtest.tm.jooq.domain.Tables.AUTOMATED_TEST_TECHNOLOGY;
import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.jooq.domain.Tables.KEYWORD_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE;
import static org.squashtest.tm.jooq.domain.Tables.MILESTONE_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.SCM_REPOSITORY;
import static org.squashtest.tm.jooq.domain.Tables.SCRIPTED_TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_AUTOMATION_PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE;
import static org.squashtest.tm.jooq.domain.Tables.TEST_CASE_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.THIRD_PARTY_SERVER;
import static org.squashtest.tm.jooq.domain.tables.AutomationRequest.AUTOMATION_REQUEST;

public class AutomationRequestGrid extends AbstractGrid {

	private static final String PROJECT_NAME = "PROJECT_NAME";
	private static final String TCLN_ID = "TCLN_ID";
	private static final String AUTOMATION_REQUEST_ID = "AUTOMATION_REQUEST_ID";
	private static final String NAME = "NAME";
	private static final String LOGIN = "LOGIN";
	private static final String AUTOMATED_TEST_FULL_NAME = "AUTOMATED_TEST_FULL_NAME";
	private static final String TC_MILESTONE_LOCKED = "TC_MILESTONE_LOCKED";
	private static final String ASSIGNED_USER = "ASSIGNED_USER";

	private List<Long> projectIds;

	private boolean assigneeIsNull;

	public AutomationRequestGrid(List<Long> projectIds) {
		this.projectIds = projectIds;
	}

	public AutomationRequestGrid(List<Long> projectIds, boolean assigneeIsNull) {
		this.projectIds = projectIds;
		this.assigneeIsNull = assigneeIsNull;
	}

	@Override
	protected List<GridColumn> getColumns() {
		return Arrays.asList(
			new GridColumn(PROJECT.PROJECT_ID),
			new GridColumn(TEST_CASE_LIBRARY_NODE.TCLN_ID),
			new GridColumn(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID),
			new GridColumn(PROJECT.NAME.as(PROJECT_NAME), PROJECT.NAME),
			new GridColumn(TEST_CASE_LIBRARY_NODE.NAME.as(NAME), TEST_CASE_LIBRARY_NODE.NAME),
			new GridColumn(AUTOMATION_REQUEST.ASSIGNED_TO),
			new GridColumn(TEST_CASE.REFERENCE),
			new GridColumn(AUTOMATION_REQUEST.AUTOMATION_PRIORITY),
			new LevelEnumColumn(AutomationRequestStatus.class, AUTOMATION_REQUEST.REQUEST_STATUS),
			new GridColumn(AUTOMATION_REQUEST.TRANSMITTED_ON),
			new GridColumn(AUTOMATION_REQUEST.ASSIGNED_ON),
			new GridColumn(SCRIPTED_TEST_CASE.TCLN_ID.as("SCRIPTED_TEST_CASE_ID"), SCRIPTED_TEST_CASE.TCLN_ID),
			new GridColumn(KEYWORD_TEST_CASE.TCLN_ID.as("KEYWORD_TEST_CASE_ID"), KEYWORD_TEST_CASE.TCLN_ID),
			new GridColumn(TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY.as(LOGIN), TEST_CASE_LIBRARY_NODE.LAST_MODIFIED_BY),
			new GridColumn(AUTOMATED_TEST_TECHNOLOGY.AT_TECHNOLOGY_ID),
			new GridColumn(AUTOMATED_TEST_TECHNOLOGY.NAME.as("AUTOMATED_TEST_TECHNOLOGY"), AUTOMATED_TEST_TECHNOLOGY.NAME),
			new GridColumn(SCM_REPOSITORY.SCM_REPOSITORY_ID),
			new GridColumn(TEST_CASE.AUTOMATED_TEST_REFERENCE),
			new GridColumn(AUTOMATION_REQUEST.CONFLICT_ASSOCIATION),
			new GridColumn(TEST_CASE.UUID),
			new GridColumn(getAutomatedTest().field(AUTOMATED_TEST_FULL_NAME)),
			new GridColumn(getAssignedUser().field(ASSIGNED_USER)),
			new GridColumn(getLockedMilestones().field(TC_MILESTONE_LOCKED)),
			new GridColumn(DSL.when(SCRIPTED_TEST_CASE.TCLN_ID.isNotNull(), "GHERKIN").when(KEYWORD_TEST_CASE.TCLN_ID.isNotNull(), "KEYWORD").otherwise("STANDARD").as("KIND")),
			new GridColumn(AUTOMATION_REQUEST.IS_MANUAL),
			new GridColumn(PROJECT.TA_SERVER_ID)
		);
	}

	@Override
	protected Table<?> getTable() {
		SelectHavingStep<?> lockedMilestones = getLockedMilestones();
		SelectHavingStep<?> transmitUser = getTransmitUser();
		SelectHavingStep<?> automatedTest = getAutomatedTest();
		SelectHavingStep<?> assignedUser = getAssignedUser();


		return TEST_CASE_LIBRARY_NODE
			.innerJoin(TEST_CASE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(TEST_CASE.TCLN_ID))
			.innerJoin(AUTOMATION_REQUEST).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(AUTOMATION_REQUEST.TEST_CASE_ID))
			.leftJoin(SCRIPTED_TEST_CASE).on(TEST_CASE.TCLN_ID.eq(SCRIPTED_TEST_CASE.TCLN_ID))
			.leftJoin(KEYWORD_TEST_CASE).on(TEST_CASE.TCLN_ID.eq(KEYWORD_TEST_CASE.TCLN_ID))
			.leftJoin(AUTOMATED_TEST_TECHNOLOGY).on(TEST_CASE.AUTOMATED_TEST_TECHNOLOGY.eq(AUTOMATED_TEST_TECHNOLOGY.AT_TECHNOLOGY_ID))
			.leftJoin(SCM_REPOSITORY).on(TEST_CASE.SCM_REPOSITORY_ID.eq(SCM_REPOSITORY.SCM_REPOSITORY_ID))
			.leftJoin(THIRD_PARTY_SERVER).on(SCM_REPOSITORY.SERVER_ID.eq(THIRD_PARTY_SERVER.SERVER_ID))
			.innerJoin(PROJECT).on(TEST_CASE_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.leftJoin(lockedMilestones).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(lockedMilestones.field(TCLN_ID, Long.class)))
			.leftJoin(automatedTest).on(TEST_CASE.TCLN_ID.eq(automatedTest.field(TCLN_ID, Long.class)))
			.leftJoin(transmitUser).on(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.eq(transmitUser.field(AUTOMATION_REQUEST_ID, Long.class)))
			.leftJoin(assignedUser).on(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.eq(assignedUser.field(AUTOMATION_REQUEST_ID, Long.class)));
	}

	@Override
	protected Field<?> getIdentifier() {
		return TEST_CASE_LIBRARY_NODE.TCLN_ID;
	}

	@Override
	protected Field<?> getProjectIdentifier() {
		return null;
	}

	@Override
	protected Condition craftInvariantFilter() {

		Condition baseCondition = PROJECT.PROJECT_ID.in(this.projectIds)
			.and(PROJECT.ALLOW_AUTOMATION_WORKFLOW.isTrue())
			.and(PROJECT.AUTOMATION_WORKFLOW_TYPE.eq(AutomationWorkflowType.NATIVE.name()))
			.and(TEST_CASE.AUTOMATABLE.eq(TestCaseAutomatable.Y.name()));

		if (assigneeIsNull) {
			baseCondition = baseCondition.and(AUTOMATION_REQUEST.ASSIGNED_TO.isNull());
		}

		return baseCondition;
	}

	private SelectHavingStep<?> getTransmitUser() {
		return DSL.select(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.as(AUTOMATION_REQUEST_ID), CORE_USER.PARTY_ID, CORE_USER.LOGIN.as(LOGIN))
			.from(AUTOMATION_REQUEST).innerJoin(CORE_USER).on(AUTOMATION_REQUEST.TRANSMITTED_BY.eq(CORE_USER.PARTY_ID));
	}

	private SelectHavingStep<?> getAssignedUser() {
		return DSL.select(AUTOMATION_REQUEST.AUTOMATION_REQUEST_ID.as(AUTOMATION_REQUEST_ID), CORE_USER.PARTY_ID, CORE_USER.LOGIN.as(ASSIGNED_USER))
			.from(AUTOMATION_REQUEST).innerJoin(CORE_USER).on(AUTOMATION_REQUEST.ASSIGNED_TO.eq(CORE_USER.PARTY_ID));
	}

	private SelectHavingStep<?> getAutomatedTest() {
		return DSL.select(TEST_CASE.TCLN_ID.as(TCLN_ID), DSL.concat(DSL.val("/"), TEST_AUTOMATION_PROJECT.LABEL, DSL.val("/"), AUTOMATED_TEST.NAME).as(AUTOMATED_TEST_FULL_NAME))
			.from(TEST_CASE)
			.innerJoin(AUTOMATED_TEST).on(TEST_CASE.TA_TEST.eq(AUTOMATED_TEST.TEST_ID))
			.innerJoin(TEST_AUTOMATION_PROJECT).on(AUTOMATED_TEST.PROJECT_ID.eq(TEST_AUTOMATION_PROJECT.TA_PROJECT_ID));
	}

	private SelectHavingStep<?> getLockedMilestones() {
		return DSL.select(TEST_CASE_LIBRARY_NODE.TCLN_ID.as(TCLN_ID), DSL.count(MILESTONE_TEST_CASE.MILESTONE_ID).as(TC_MILESTONE_LOCKED))
			.from(TEST_CASE_LIBRARY_NODE)
			.leftJoin(MILESTONE_TEST_CASE).on(TEST_CASE_LIBRARY_NODE.TCLN_ID.eq(MILESTONE_TEST_CASE.TEST_CASE_ID))
			.leftJoin(MILESTONE).on(MILESTONE_TEST_CASE.MILESTONE_ID.eq(MILESTONE.MILESTONE_ID))
			.where(MILESTONE.STATUS.eq(MilestoneStatus.LOCKED.name()))
			.groupBy(TEST_CASE_LIBRARY_NODE.TCLN_ID, MILESTONE.MILESTONE_ID);
	}
}

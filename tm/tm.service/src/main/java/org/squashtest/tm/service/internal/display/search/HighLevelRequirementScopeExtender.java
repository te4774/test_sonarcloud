/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.search;

import org.jooq.DSLContext;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityReferences;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.jooq.domain.tables.Requirement;
import org.squashtest.tm.service.internal.repository.display.MultipleHierarchyTreeBrowserDao;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.squashtest.tm.jooq.domain.Tables.HIGH_LEVEL_REQUIREMENT;
import static org.squashtest.tm.jooq.domain.Tables.PROJECT;
import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_LIBRARY_NODE;
import static org.squashtest.tm.jooq.domain.Tables.RLN_RELATIONSHIP_CLOSURE;

@Component
public class HighLevelRequirementScopeExtender {
	private final MultipleHierarchyTreeBrowserDao treeBrowserDao;
	private final DSLContext dslContext;
	private static final Requirement LINKED_REQUIREMENT = Tables.REQUIREMENT.as("LINKED_REQUIREMENT");

	public HighLevelRequirementScopeExtender(MultipleHierarchyTreeBrowserDao treeBrowserDao, DSLContext dslContext) {
		this.treeBrowserDao = treeBrowserDao;
		this.dslContext = dslContext;
	}

	public List<EntityReference> extendScope(List<EntityReference> initialScope) {
		NodeReferences nodeReferences = new EntityReferences(initialScope).toNodeReferences();
		Set<NodeReference> libraries = nodeReferences.extractLibraries();
		Set<NodeReference> initialNonLibraries = nodeReferences.extractNonLibraries();

		Set<Long> ids = findPerimeterAsRln(libraries, initialNonLibraries);

		Set<EntityReference> linkedRequirements = findAllLinkedRequirements(ids);

		// Now we must remove the linkedRequirement already in perimeter. We don't want to have already present requirements, it could lead to unexpect behavior
		Set<Long> linkedRequirementIds = linkedRequirements.stream().map(EntityReference::getId).collect(Collectors.toSet());
		Set<EntityReference> alreadyInPerimeterByLibrary = findAlreadyInScopeByLibraries(libraries, linkedRequirementIds);
		Set<EntityReference> alreadyInPerimeterByRLN = findAlreadyInPerimeterByRequirementLibraryNodes(initialNonLibraries, linkedRequirementIds);

		linkedRequirements.removeAll(alreadyInPerimeterByLibrary);
		linkedRequirements.removeAll(alreadyInPerimeterByRLN);

		initialScope.addAll(linkedRequirements);
		return initialScope;
	}

	private Set<EntityReference> findAlreadyInPerimeterByRequirementLibraryNodes(Set<NodeReference> initialNonLibraries, Set<Long> linkedRequirementIds) {
		Set<Long> initialNonLibraryIds = initialNonLibraries.stream().map(NodeReference::getId).collect(Collectors.toSet());
		return dslContext.select(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID)
			.from(RLN_RELATIONSHIP_CLOSURE)
			.where(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.in(initialNonLibraryIds))
			.and(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.in(linkedRequirementIds))
			.stream()
			.map(record -> new EntityReference(EntityType.REQUIREMENT, record.value1()))
			.collect(Collectors.toSet());
	}

	private Set<EntityReference> findAlreadyInScopeByLibraries(Set<NodeReference> libraries, Set<Long> linkedRequirementIds) {
		Set<Long> libraryIds = libraries.stream().map(NodeReference::getId).collect(Collectors.toSet());
		return dslContext.selectDistinct(REQUIREMENT_LIBRARY_NODE.RLN_ID)
			.from(REQUIREMENT_LIBRARY_NODE)
			.innerJoin(PROJECT).on(REQUIREMENT_LIBRARY_NODE.PROJECT_ID.eq(PROJECT.PROJECT_ID))
			.where(PROJECT.RL_ID.in(libraryIds)).and(REQUIREMENT_LIBRARY_NODE.RLN_ID.in(linkedRequirementIds))
			.stream()
			.map(record -> new EntityReference(EntityType.REQUIREMENT, record.value1()))
			.collect(Collectors.toSet());
	}

	private Set<EntityReference> findAllLinkedRequirements(Set<Long> ids) {
		return dslContext.selectDistinct(LINKED_REQUIREMENT.RLN_ID)
			.from(HIGH_LEVEL_REQUIREMENT)
			.innerJoin(RLN_RELATIONSHIP_CLOSURE).on(RLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(HIGH_LEVEL_REQUIREMENT.RLN_ID))
			.innerJoin(LINKED_REQUIREMENT).on(LINKED_REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID.eq(HIGH_LEVEL_REQUIREMENT.RLN_ID))
			.where(RLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.in(ids))
			.stream()
			.map(record -> new EntityReference(EntityType.REQUIREMENT, record.value1()))
			.collect(Collectors.toSet());
	}

	private Set<Long> findPerimeterAsRln(Set<NodeReference> libraries, Set<NodeReference> initialNonLibraries) {
		Set<NodeReference> ancestors = new HashSet<>(initialNonLibraries);
		Collection<NodeReference> librariesChildren = treeBrowserDao.findChildrenReference(libraries).values();
		ancestors.addAll(librariesChildren);
		return ancestors.stream().map(NodeReference::getId).collect(Collectors.toSet());
	}
}

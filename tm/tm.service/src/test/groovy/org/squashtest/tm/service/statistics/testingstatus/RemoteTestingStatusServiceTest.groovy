/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.testingstatus


import org.squashtest.csp.core.bugtracker.domain.BugTracker
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation
import org.squashtest.tm.service.internal.repository.RemoteSynchronisationDao
import org.squashtest.tm.service.internal.statistics.testingstatus.RemoteTestingStatusServiceImpl
import org.squashtest.tm.service.requirement.RequirementStatisticsService
import org.squashtest.tm.service.statistics.requirement.RequirementVersionBundleStat
import spock.lang.Specification

class RemoteTestingStatusServiceTest extends Specification {

	RemoteTestingStatusService service

	RequirementStatisticsService requirementStatisticsService = Mock()
	RemoteSynchronisationDao remoteSynchronisationDao = Mock()
	RemoteTestingStatusDao remoteTestingStatusDao = Mock()

	def setup() {
		service = new RemoteTestingStatusServiceImpl(requirementStatisticsService, remoteSynchronisationDao, remoteTestingStatusDao)
	}

	def "should collect testing statistics for a given synchronisation kind"() {
		given:
		Long syncId = 1L
		Long serverId = 42L
		Long reqId = 123L

		Map<String, List<Long>> reqIdsByRemoteKey = Map.ofEntries(
			Map.entry("key1", Arrays.asList(reqId)),
			Map.entry("key2", Arrays.asList(1L, 4L, 5L)))

		Map<Long, Map<String, List<Long>>> dataMap = Collections.singletonMap(serverId, reqIdsByRemoteKey)
		RequirementsByRemoteKeyByServerId reqByRemoteKeyAndServerId = new RequirementsByRemoteKeyByServerId(dataMap)
		remoteTestingStatusDao.findRequirementsByRemoteKeyByServerId("KIND") >> reqByRemoteKeyAndServerId
		remoteTestingStatusDao.findSynchronisedKeysBySynchronisationId(_) >> Collections.singletonMap(syncId, Arrays.asList("key1", "key2"))

		RequirementVersionBundleStat bundleStat = mockBundleStat(reqId)
		requirementStatisticsService.findSimplifiedCoverageStats(_ as Collection) >> bundleStat

		RemoteSynchronisation sync = Mock()
		sync.isSynchronisationEnable() >> true
		sync.getId() >> syncId

		BugTracker server = Mock()
		server.getId() >> serverId
		sync.getServer() >> server

		remoteSynchronisationDao.findByKind("KIND") >> Collections.singletonList(sync)

		when:
		def stats = service.getRemoteRequirementStatisticsDictionary("KIND")

		then:
		noExceptionThrown()
		stats != null
		stats.getRequirementIdsForRemote(serverId, "key1").size() == 1
		stats.getRequirementIdsForRemote(serverId, "key1").asList().get(0) == reqId
		stats.getMergedStatistics(serverId, "key1") != null
	}

	private RequirementVersionBundleStat mockBundleStat(long reqId) {
		RequirementVersionBundleStat bundleStat = Mock()
		RequirementVersionBundleStat.SimpleRequirementStats simpleStats = mockSimpleStats()
		Map<Long, RequirementVersionBundleStat.SimpleRequirementStats> reqStatsById = Collections.singletonMap(reqId, simpleStats)
		bundleStat.getRequirementStats() >> reqStatsById
		bundleStat
	}

	private RequirementVersionBundleStat.SimpleRequirementStats mockSimpleStats() {
		RequirementVersionBundleStat.SimpleRequirementStats simpleStats = Mock()
		simpleStats.getRedactedTestCase() >> 0
		simpleStats.getAllTestCaseCount() >> 0
		simpleStats.getVerifiedTestCase() >> 0
		simpleStats.getPlannedTestCase() >> 0
		simpleStats.getValidatedTestCase() >> 0
		simpleStats.getExecutedTestCase() >> 0
		simpleStats
	}
}

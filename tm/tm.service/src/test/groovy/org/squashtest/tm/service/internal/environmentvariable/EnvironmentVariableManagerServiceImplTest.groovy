/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable


import org.squashtest.tm.domain.environmentvariable.EVInputType
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableOption
import org.squashtest.tm.domain.environmentvariable.SingleSelectEnvironmentVariable
import org.squashtest.tm.exception.NameAlreadyInUseException
import org.squashtest.tm.exception.RequiredFieldException
import org.squashtest.tm.exception.customfield.CodeAlreadyExistsException
import org.squashtest.tm.exception.customfield.CodeDoesNotMatchesPatternException
import org.squashtest.tm.exception.customfield.OptionAlreadyExistException
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableValueService
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao
import spock.lang.Specification

class EnvironmentVariableManagerServiceImplTest extends Specification {

	EnvironmentVariableManagerServiceImpl service
	EnvironmentVariableDao environmentVariableDao = Mock()
	EnvironmentVariableBindingService environmentVariableBindingService = Mock()
	EnvironmentVariableValueService environmentVariableValueService = Mock()

	def setup() {
		service =
			new EnvironmentVariableManagerServiceImpl(environmentVariableDao,
				environmentVariableBindingService,
				environmentVariableValueService)
	}

	def "should delete environment variable"() {
		given:
		EnvironmentVariable environmentVariable = Mock()
		List<Long> environmentVariableIds = Arrays.asList(1L)
		environmentVariableDao.getById(1L) >> environmentVariable

		when:
		service.deleteEnvironmentVariable(environmentVariableIds)

		then:
		1 * environmentVariableDao.delete(environmentVariable)
		noExceptionThrown()
	}

	def "should change name"() {
		given:
		String newName = "newName"
		String oldName = "oldName"
		EnvironmentVariable environmentVariable = new EnvironmentVariable(oldName, EVInputType.PLAIN_TEXT, "EvCode")
		environmentVariableDao.getById(1L) >> environmentVariable

		when:
		service.changeName(1L, newName)

		then:
		1 * environmentVariableDao.findByName(newName) >> null
		environmentVariable.getName() == newName
		noExceptionThrown()
	}

	def "should fail when change name"() {
		given:
		String newName = "newName"
		String oldName = "oldName"

		EnvironmentVariable existingEnvironmentVariable = new EnvironmentVariable(newName, EVInputType.PLAIN_TEXT, "EvCode2")
		EnvironmentVariable environmentVariable = new EnvironmentVariable(oldName, EVInputType.PLAIN_TEXT, "EvCode")
		environmentVariableDao.getById(1L) >> environmentVariable

		when:
		service.changeName(1L, newName)

		then:
		1 * environmentVariableDao.findByName(newName) >> existingEnvironmentVariable
		thrown(NameAlreadyInUseException)
	}

	def "should change code"() {
		given:
		String newCode = "newCode"
		String oldCode = "oldCode"
		EnvironmentVariable environmentVariable = new EnvironmentVariable("evName", EVInputType.PLAIN_TEXT, oldCode)
		environmentVariableDao.getById(1L) >> environmentVariable

		when:
		service.changeCode(1L, newCode)

		then:
		1 * environmentVariableDao.findByCode(newCode) >> null
		environmentVariable.getCode() == newCode
		noExceptionThrown()
	}

	def "should fail when change code"() {
		given:
		String newCode = "newCode"
		String oldCode = "oldCode"
		EnvironmentVariable existingEnvironmentVariable = new EnvironmentVariable("evNameTest", EVInputType.PLAIN_TEXT, newCode)
		EnvironmentVariable environmentVariable = new EnvironmentVariable("evName", EVInputType.PLAIN_TEXT, oldCode)
		environmentVariableDao.getById(1L) >> environmentVariable

		when:
		service.changeCode(1L, newCode)

		then:
		1 * environmentVariableDao.findByCode(newCode) >> existingEnvironmentVariable
		thrown(CodeAlreadyExistsException)
	}

	def "should change option code"() {
		given:
		String newOptionCode = "newCode"
		String oldOptionCode = "oldCode"
		SingleSelectEnvironmentVariable environmentVariable = new SingleSelectEnvironmentVariable()

		List<EnvironmentVariableOption> options = new ArrayList<>()
		EnvironmentVariableOption option1 = new EnvironmentVariableOption("opt1", oldOptionCode, 0)
		EnvironmentVariableOption option2 = new EnvironmentVariableOption("opt2", "code2", 1)
		options.addAll(option1, option2)
		environmentVariable.setOptions(options)

		environmentVariableDao.findSingleSelectEnvironmentVariableById(1L) >> environmentVariable

		when:
		service.changeOptionCode(1L, "opt1", newOptionCode)

		then:
		environmentVariable.getOptions().get(0).code == newOptionCode
		noExceptionThrown()
	}

	def "should fail when change option code with empty value"() {
		given:
		String newOptionCode = ""

		when:
		service.changeOptionCode(1L, "opt1", newOptionCode)

		then:
		thrown(RequiredFieldException)
	}

	def "should fail change option code when code already exist"() {
		given:
		String newOptionCode = "newCode"
		SingleSelectEnvironmentVariable environmentVariable = new SingleSelectEnvironmentVariable()

		List<EnvironmentVariableOption> options = new ArrayList<>()
		EnvironmentVariableOption option1 = new EnvironmentVariableOption("opt1", newOptionCode)
		EnvironmentVariableOption option2 = new EnvironmentVariableOption("opt2", "code2")
		options.addAll(option1, option2)
		environmentVariable.setOptions(options)

		environmentVariableDao.findSingleSelectEnvironmentVariableById(1L) >> environmentVariable

		when:
		service.changeOptionCode(1L, "opt1", newOptionCode)

		then:
		thrown(CodeAlreadyExistsException)
	}

	def "should fail change option code when code does not match the pattern "() {
		given:
		String newOptionCode = "ne(|@Code"
		SingleSelectEnvironmentVariable environmentVariable = new SingleSelectEnvironmentVariable()

		environmentVariableDao.findSingleSelectEnvironmentVariableById(1L) >> environmentVariable

		when:
		service.changeOptionCode(1L, "opt1", newOptionCode)

		then:
		thrown(CodeDoesNotMatchesPatternException)
	}

	def "should change option label"() {
		given:
		String newOptionLabel = "newLabel"
		String oldOptionLabel = "oldLabel"
		SingleSelectEnvironmentVariable environmentVariable = new SingleSelectEnvironmentVariable()

		List<EnvironmentVariableOption> options = new ArrayList<>()
		EnvironmentVariableOption option1 = new EnvironmentVariableOption(oldOptionLabel, "code1", 0)
		EnvironmentVariableOption option2 = new EnvironmentVariableOption("opt2", "code2", 1)
		options.addAll(option1, option2)
		environmentVariable.setOptions(options)

		environmentVariableDao.findSingleSelectEnvironmentVariableById(1L) >> environmentVariable

		when:
		service.changeOptionLabel(1L, oldOptionLabel, newOptionLabel)

		then:
		environmentVariable.getOptions().get(0).label == newOptionLabel
		noExceptionThrown()
	}

	def "should fail when change option label with empty label"() {
		given:
		String newOptionLabel = ""
		String oldOptionLabel = "oldLabel"

		when:
		service.changeOptionLabel(1L, oldOptionLabel, newOptionLabel)

		then:
		thrown(RequiredFieldException)
	}

	def "should fail change option label when label already exist"() {
		given:
		String existOptionLabel = "existLabel"
		String oldOptionLabel = "existLabel"
		SingleSelectEnvironmentVariable environmentVariable = new SingleSelectEnvironmentVariable()

		List<EnvironmentVariableOption> options = new ArrayList<>()
		EnvironmentVariableOption option1 = new EnvironmentVariableOption(oldOptionLabel, "code1")
		EnvironmentVariableOption option2 = new EnvironmentVariableOption("opt2", "code2")
		options.addAll(option1, option2)
		environmentVariable.setOptions(options)

		environmentVariableDao.findSingleSelectEnvironmentVariableById(1L) >> environmentVariable

		when:
		service.changeOptionLabel(1L, oldOptionLabel, existOptionLabel)

		then:
		thrown(OptionAlreadyExistException)
	}

	def "should change option position"() {
		given:
		Long environmentVariableId = 1L
		Integer position = 2
		List<String> labelToMove = Arrays.asList("option1")

		SingleSelectEnvironmentVariable environmentVariable = new SingleSelectEnvironmentVariable()

		List<EnvironmentVariableOption> options = new ArrayList<>()
		EnvironmentVariableOption option1 = new EnvironmentVariableOption("option1", "option1", 0)
		EnvironmentVariableOption option2 = new EnvironmentVariableOption("option2", "option2", 1)
		EnvironmentVariableOption option3 = new EnvironmentVariableOption("option3", "option3", 2)
		options.addAll(option1, option2, option3)
		environmentVariable.setOptions(options)
		Long indexBeforeChange = environmentVariable.getOptions().indexOf(option1)

		environmentVariableDao.findSingleSelectEnvironmentVariableById(1L) >> environmentVariable

		when:
		service.changeOptionsPosition(environmentVariableId, position, labelToMove)

		then:
		indexBeforeChange == 0
		environmentVariable.getOptions().indexOf(option1) == 2

	}


}

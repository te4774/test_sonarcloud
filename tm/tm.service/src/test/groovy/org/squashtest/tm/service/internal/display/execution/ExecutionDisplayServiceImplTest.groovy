/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.display.execution

import org.jooq.DSLContext
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.execution.ExecutionStep
import org.squashtest.tm.domain.testcase.ActionTestStep
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.exception.execution.ExecutionStepHasNoModifiableStepException
import org.squashtest.tm.exception.execution.ModifDuringExecMissingReadPermissionException
import org.squashtest.tm.exception.execution.ModifDuringExecMissingWritePermissionException
import org.squashtest.tm.service.execution.ExecutionModificationService
import org.squashtest.tm.service.internal.bugtracker.knownissues.local.ExecutionKnownIssueFinder
import org.squashtest.tm.service.internal.repository.display.*
import org.squashtest.tm.service.internal.testcase.TestCaseCallTreeFinder
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.service.testautomation.TestAutomationProjectFinderService
import spock.lang.Specification

import javax.persistence.EntityManager

class ExecutionDisplayServiceImplTest extends Specification {

	ExecutionDisplayDao executionDisplayDao = Mock()
	ExecutionStepDisplayDao executionStepDisplayDao = Mock()
	DenormalizedCustomFieldValueDisplayDao denormalizedCustomFieldValueDisplayDao = Mock()
	CustomFieldValueDisplayDao customFieldValueDisplayDao = Mock()
	AttachmentDisplayDao attachmentDisplayDao = Mock()
	RequirementVersionCoverageDisplayDao requirementVersionCoverageDisplayDao = Mock()
	TestCaseCallTreeFinder testCaseCallTreeFinder = Mock()
	DSLContext dslContext = Mock()
	ExecutionModificationService executionModificationService = Mock()
	TestAutomationProjectFinderService testAutomationProjectFinderService = Mock()
	ExecutionKnownIssueFinder issueFinder = Mock()
	MilestoneDisplayDao milestoneDisplayDao = Mock()
	TestStepDisplayDao testStepDisplayDao = Mock()
	PermissionEvaluationService permissionEvaluationService = Mock()
	EntityManager entityManager = Mock()

	ExecutionDisplayServiceImpl service = new ExecutionDisplayServiceImpl(
		executionDisplayDao,
		executionStepDisplayDao,
		denormalizedCustomFieldValueDisplayDao,
		customFieldValueDisplayDao,
		attachmentDisplayDao,
		requirementVersionCoverageDisplayDao,
		testCaseCallTreeFinder,
		dslContext,
		executionModificationService,
		testAutomationProjectFinderService,
		issueFinder,
		milestoneDisplayDao,
		testStepDisplayDao,
		permissionEvaluationService,
		entityManager
	)

	def "should reject modif step if step doesn't exist anymore"() {
		given:
		entityManager.find(ExecutionStep.class, 1L) >> new ExecutionStep()

		when:
		service.checkPermissionsForModificationDuringExecution(1L, 1L)

		then:
		thrown(ExecutionStepHasNoModifiableStepException.class)
	}

	def "should reject modif step if at least one test case can't be read"() {
		def executionStep = new ExecutionStep(new ActionTestStep(testCase: new TestCase()))
		def execution = new Execution()
		execution.id = 12L
		executionStep.execution = execution


		given:
		entityManager.find(ExecutionStep.class, 1L) >> executionStep
		executionDisplayDao.findAllTestCaseInExecution(12L) >> [1L ,2L, 3L]
		permissionEvaluationService.permissionsOn(TestCase.CLASS_NAME, 1L) >> ['READ','WRITE']
		permissionEvaluationService.permissionsOn(TestCase.CLASS_NAME, 2L) >> ['READ','WRITE']
		permissionEvaluationService.permissionsOn(TestCase.CLASS_NAME, 3L) >> []

		when:
		service.checkPermissionsForModificationDuringExecution(1L, 1L)

		then:
		thrown(ModifDuringExecMissingReadPermissionException.class)
	}

	def "should reject modif step if no test case can be write"() {
		def executionStep = new ExecutionStep(new ActionTestStep(testCase: new TestCase()))
		def execution = new Execution()
		execution.id = 12L
		executionStep.execution = execution


		given:
		entityManager.find(ExecutionStep.class, 1L) >> executionStep
		executionDisplayDao.findAllTestCaseInExecution(12L) >> [1L ,2L, 3L]
		permissionEvaluationService.permissionsOn(TestCase.CLASS_NAME, 1L) >> ['READ']
		permissionEvaluationService.permissionsOn(TestCase.CLASS_NAME, 2L) >> ['READ']
		permissionEvaluationService.permissionsOn(TestCase.CLASS_NAME, 3L) >> ['READ']

		when:
		service.checkPermissionsForModificationDuringExecution(1L, 1L)

		then:
		thrown(ModifDuringExecMissingWritePermissionException.class)
	}

	def "allow modif step if all test case are readable and at least one is writable"() {
		def executionStep = new ExecutionStep(new ActionTestStep(testCase: new TestCase()))
		def execution = new Execution()
		execution.id = 12L
		executionStep.execution = execution


		given:
		entityManager.find(ExecutionStep.class, 1L) >> executionStep
		executionDisplayDao.findAllTestCaseInExecution(12L) >> [1L ,2L, 3L]
		permissionEvaluationService.permissionsOn(TestCase.CLASS_NAME, 1L) >> ['READ','WRITE']
		permissionEvaluationService.permissionsOn(TestCase.CLASS_NAME, 2L) >> ['READ']
		permissionEvaluationService.permissionsOn(TestCase.CLASS_NAME, 3L) >> ['READ']

		when:
		service.checkPermissionsForModificationDuringExecution(1L, 1L)

		then:
		noExceptionThrown()
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable

import org.squashtest.tm.domain.environmentvariable.EVBindableEntity
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableOption
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableValue
import org.squashtest.tm.domain.environmentvariable.SingleSelectEnvironmentVariable
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao
import org.squashtest.tm.service.internal.repository.EnvironmentVariableValueDao
import org.squashtest.tm.service.internal.repository.display.ProjectDisplayDao
import spock.lang.Specification

class EnvironmentVariableValueServiceImplTest extends Specification {

	EnvironmentVariableValueServiceImpl service
	ProjectDisplayDao projectDisplayDao = Mock()
	EnvironmentVariableBindingDao environmentVariableBindingDao = Mock()
	EnvironmentVariableValueDao environmentVariableValueDao = Mock()
	EnvironmentVariableDao environmentVariableDao = Mock()

	def setup() {
		service =
			new EnvironmentVariableValueServiceImpl(environmentVariableValueDao,
				projectDisplayDao,
				environmentVariableBindingDao,
				environmentVariableDao)
	}

	def "should remove environment variable values by binding ids"() {
		given:
		EnvironmentVariableBinding binding = new EnvironmentVariableBinding()
		Collection<Long> bindingIds = Arrays.asList(-1L)
		EnvironmentVariableValue environmentVariableValue = new EnvironmentVariableValue(EVBindableEntity.TEST_AUTOMATION_SERVER,
																-1L, binding)
		Collection<EnvironmentVariableValue> environmentVariableValues = Arrays.asList(environmentVariableValue)
		environmentVariableValueDao.findAllByBindingIds(bindingIds) >> environmentVariableValues

		when:
		service.removeEnvironmentVariableValuesByBindingIds(bindingIds)

		then:
		1 * environmentVariableValueDao.deleteAll(environmentVariableValues)
		noExceptionThrown()
	}

	def "should update environment variable value from server view"() {
		given:
		Long testAutomationServerId = -1L
		Long environmentVariableId = -1L
		EnvironmentVariableBinding binding = new EnvironmentVariableBinding()
		environmentVariableBindingDao.findByServerIdAndEvId(testAutomationServerId, environmentVariableId) >> binding

		EnvironmentVariableValue environmentVariableValue =
			new EnvironmentVariableValue(EVBindableEntity.TEST_AUTOMATION_SERVER, testAutomationServerId, binding)

		when:
		service.editEnvironmentVariableValueFromServer(testAutomationServerId, environmentVariableId, "value")

		then:
		1 * environmentVariableValueDao.
			findSingleEnvironmentVariableValue(binding, EVBindableEntity.TEST_AUTOMATION_SERVER, testAutomationServerId) >> environmentVariableValue
		environmentVariableValue.value == "value"
		noExceptionThrown()
	}

	def "should create environment variable value from server view"() {
		given:
		Long testAutomationServerId = -1L
		EnvironmentVariableBinding binding = new EnvironmentVariableBinding()

		when:
		service.createValueFromServer(binding, testAutomationServerId)

		then:
		1 * environmentVariableValueDao.save(_ as EnvironmentVariableValue)
		noExceptionThrown()
	}

	def "should edit new environment variable value from project view "() {
		given:
		Long projectId= -1L
		Long environmentVariableId = -1L
		Long testAutomationServerId = -1L
		EnvironmentVariableBinding binding = new EnvironmentVariableBinding()
		environmentVariableBindingDao
			.findByServerIdAndEvId(testAutomationServerId, environmentVariableId) >> binding

		environmentVariableValueDao.
			findSingleEnvironmentVariableValue(binding, EVBindableEntity.PROJECT, projectId) >> null

		when:
		service.editEnvironmentVariableValueFromProject(projectId, environmentVariableId, "value")

		then:
		1 * projectDisplayDao.getTaServerIdByProjectId(projectId) >> testAutomationServerId
		1 * environmentVariableValueDao.save(_ as EnvironmentVariableValue)
		noExceptionThrown()
	}

	def "should update environment variable value from project view "() {
		given:
		Long projectId= -1L
		Long environmentVariableId = -1L
		Long testAutomationServerId = -1L
		EnvironmentVariableBinding binding = new EnvironmentVariableBinding()
		EnvironmentVariableValue existingProjectValue = new EnvironmentVariableValue(EVBindableEntity.PROJECT, projectId, binding)

		environmentVariableBindingDao
			.findByServerIdAndEvId(testAutomationServerId, environmentVariableId) >> binding

		environmentVariableValueDao.findSingleEnvironmentVariableValue(binding, EVBindableEntity.PROJECT, projectId) >> existingProjectValue

		when:
		service.editEnvironmentVariableValueFromProject(projectId, environmentVariableId, "value")

		then:
		1 * projectDisplayDao.getTaServerIdByProjectId(projectId) >> testAutomationServerId
		existingProjectValue.value == "value"
		0 * environmentVariableValueDao.save(_ as EnvironmentVariableValue)
		noExceptionThrown()
	}

	def "should throw IllegalArgumentException when edit environment variable value if option value is invalid"() {
		given:
		Long testServerId = -1L
		Long environmentVariableId = -1L
		SingleSelectEnvironmentVariable environmentVariable = new SingleSelectEnvironmentVariable()
		EnvironmentVariableOption option1 = new EnvironmentVariableOption("option1", "opt1")
		EnvironmentVariableOption option2 = new EnvironmentVariableOption("option2", "opt2")
		environmentVariable.setOptions(Arrays.asList(option1, option2))
		environmentVariableDao.findSingleSelectEnvironmentVariableById(environmentVariableId) >> environmentVariable

		when:
		service.editEnvironmentVariableValueFromServer(testServerId, environmentVariableId, "invalidValue")

		then:
		thrown(IllegalArgumentException)
	}

	def "should reset default value from project view"() {
		given:
		Long projectId= -1L
		Long environmentVariableId = -1L
		Long testAutomationServerId = -1L
		EnvironmentVariableBinding binding = new EnvironmentVariableBinding()
		EnvironmentVariableValue existingProjectValue = new EnvironmentVariableValue(EVBindableEntity.PROJECT, projectId, binding)
		EnvironmentVariableValue defaultEnvironmentVariableValue =
			new EnvironmentVariableValue(EVBindableEntity.TEST_AUTOMATION_SERVER, projectId, binding)
		defaultEnvironmentVariableValue.value = "defaultValue"

		environmentVariableBindingDao
			.findByServerIdAndEvId(testAutomationServerId, environmentVariableId) >> binding

		environmentVariableValueDao.findSingleEnvironmentVariableValue(binding,
			EVBindableEntity.PROJECT, projectId) >> existingProjectValue

		environmentVariableValueDao.findSingleEnvironmentVariableValue(binding,
			EVBindableEntity.TEST_AUTOMATION_SERVER, testAutomationServerId) >> defaultEnvironmentVariableValue

		when:
		def result = service.resetDefaultValue(projectId, environmentVariableId, EVBindableEntity.PROJECT.name())

		then:
		1 * projectDisplayDao.getTaServerIdByProjectId(projectId) >> testAutomationServerId
		1 * environmentVariableValueDao.delete(existingProjectValue)
		result == defaultEnvironmentVariableValue.value
		noExceptionThrown()
	}

	def "should remove environment variable values by project id"() {
		given:
		Long projectId= -1L
		EnvironmentVariableBinding binding = new EnvironmentVariableBinding()
		EnvironmentVariableValue environmentVariableValue = new EnvironmentVariableValue(EVBindableEntity.PROJECT, projectId, binding)
		List<EnvironmentVariableValue> values = Arrays.asList(environmentVariableValue)

		environmentVariableValueDao
			.findAllByBoundEntityIdAndBoundEntityType(projectId, EVBindableEntity.PROJECT) >> values

		when:
		service.removeEnvironmentVariableValuesByProjectId(projectId)

		then:
		1 * environmentVariableValueDao.deleteAll(values)
		noExceptionThrown()
	}

	def "should remove environment variable values by value and by environment variable id"() {
		given:
		List<String> values = Arrays.asList("myValue")
		Long evId = -1L
		Long projectId= -1L
		EnvironmentVariableBinding binding = new EnvironmentVariableBinding()
		List<EnvironmentVariableBinding> bindings = Arrays.asList(binding)

		EnvironmentVariableValue environmentVariableValue = new EnvironmentVariableValue(EVBindableEntity.PROJECT, projectId, binding)
		environmentVariableValue.setValue("myValue")
		List<EnvironmentVariableValue> environmentVariableValues = new ArrayList<>()
		environmentVariableValues.add(environmentVariableValue)
		environmentVariableBindingDao.findAllByEnvironmentVariable_Id(evId) >> bindings
		environmentVariableValueDao.findAllByBindingsAndValues(bindings, values) >> environmentVariableValues

		when:
		service.reinitializeEnvironmentVariableValuesByValueAndEvId(values, evId)

		then:
		environmentVariableValue.getValue() == ""
	}
}

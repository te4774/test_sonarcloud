/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.project

import org.jooq.DSLContext
import org.squashtest.tm.api.template.TemplateConfigurablePlugin
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.project.ProjectTemplate
import org.squashtest.tm.service.customfield.CustomFieldModelService
import org.squashtest.tm.service.infolist.InfoListModelService
import org.squashtest.tm.service.internal.repository.GenericProjectDao
import org.squashtest.tm.service.internal.repository.ProjectDao
import org.squashtest.tm.service.internal.repository.ProjectTemplateDao
import org.squashtest.tm.service.milestone.MilestoneModelService
import org.squashtest.tm.service.project.GenericProjectCopyParameter
import org.squashtest.tm.service.project.GenericProjectManagerService
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.service.servers.StoredCredentialsManager
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginBindingService
import org.squashtest.tm.service.templateplugin.TemplateConfigurablePluginService
import org.squashtest.tm.service.user.UserAccountService
import spock.lang.Specification

class CustomProjectModificationServiceImplTest extends Specification {
	ProjectDeletionHandler projectDeletionHandler = Mock()
	ProjectTemplateDao projectTemplateDao = Mock()
	GenericProjectManagerService genericProjectManagerService = Mock()
	ProjectDao projectDao = Mock()
	PermissionEvaluationService permissionEvaluationService = Mock()
	GenericProjectDao genericProjectDao = Mock()
	UserAccountService userAccountService = Mock()
	DSLContext dsl = Mock()
	MilestoneModelService milestoneModelService = Mock()
	CustomFieldModelService customFieldModelService = Mock()
	InfoListModelService infoListModelService = Mock()
	TemplateConfigurablePluginBindingService templateConfigurablePluginBindingService = Mock()
	TemplateConfigurablePluginService templateConfigurablePluginService = Mock()
	StoredCredentialsManager storedCredentialsManager = Mock()

	CustomProjectModificationServiceImpl service = new CustomProjectModificationServiceImpl(
            projectDeletionHandler,
            projectTemplateDao,
            genericProjectManagerService,
            projectDao,
            permissionEvaluationService,
            genericProjectDao,
            userAccountService,
            dsl,
            milestoneModelService,
            customFieldModelService,
            infoListModelService,
            templateConfigurablePluginBindingService,
            templateConfigurablePluginService,
			storedCredentialsManager)

	def "Should add a Projet, bind it to the Template and copy all the settings from it"() {

		given: "The Template"

		ProjectTemplate template = new ProjectTemplate()

		and: "The Project"

		Project project = new Project()

		and:"The Conf Object"

		GenericProjectCopyParameter params = new GenericProjectCopyParameter()

		params.setKeepTemplateBinding(true)

		params.setCopyCUF(false)
		params.setCopyInfolists(false)
		params.setCopyAllowTcModifFromExec(false)
		params.setCopyOptionalExecStatuses(false)

		and:

		projectTemplateDao.getOne(1L) >> template

		when:

		service.addProjectFromTemplate(project, 1L, params)

		then:

		1 * genericProjectManagerService.persist(project)
		1 * genericProjectManagerService.synchronizeGenericProject(project, template, params)

		project.getTemplate() == template

		params.isCopyCUF() == true
		params.isCopyInfolists() == true
		params.isCopyAllowTcModifFromExec() == true
		params.isCopyOptionalExecStatuses() == true
	}

	def "Should add a Projet and copy all the settings from the Template"() {

		given: "The Template"

		ProjectTemplate template = new ProjectTemplate()

		and: "The Project"

		Project project = new Project()

		and:"The Conf Object"

		GenericProjectCopyParameter params = new GenericProjectCopyParameter()

		params.setKeepTemplateBinding(false)

		params.setCopyCUF(false)
		params.setCopyInfolists(false)
		params.setCopyAllowTcModifFromExec(false)
		params.setCopyOptionalExecStatuses(false)

		and:

		projectTemplateDao.getOne(1L) >> template

		when:

		service.addProjectFromTemplate(project, 1L, params)

		then:

		1 * genericProjectManagerService.persist(project)
		1 * genericProjectManagerService.synchronizeGenericProject(project, template, params);

		project.getTemplate() == null

		params.isCopyCUF() == false
		params.isCopyInfolists() == false
		params.isCopyAllowTcModifFromExec() == false
		params.isCopyOptionalExecStatuses() == false
	}

	def "Should add a Project and create template plugin binding"() {

		given: "The Template"

		ProjectTemplate template = Mock()
		template.getId() >> 1L

		and: "The Project"

		Project project = Mock()
		project.getId() >> 2L

		and:"The Conf Object"

		GenericProjectCopyParameter params = new GenericProjectCopyParameter()
		params.setKeepTemplateBinding(true)
		params.getBoundTemplatePlugins().add('my-plugin')

		and:

		projectTemplateDao.getOne(1L) >> template

		and:

		TemplateConfigurablePlugin plugin = Mock()
		templateConfigurablePluginService.findById('my-plugin') >> Optional.of(plugin)
		templateConfigurablePluginBindingService.hasBinding(_, _, _) >> false

		when:

		service.addProjectFromTemplate(project, 1L, params)

		then:

		1 * genericProjectManagerService.persist(project)
		1 * genericProjectManagerService.synchronizeGenericProject(project, template, params)
		0 * plugin.synchroniseTemplateConfiguration(1L, project.id)
		1 * templateConfigurablePluginBindingService.createTemplateConfigurablePluginBinding(template, project, plugin)
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.testingstatus

import org.squashtest.tm.service.statistics.requirement.RequirementVersionBundleStat
import spock.lang.Specification

class RemoteRequirementStatisticsTest extends Specification {
	def "it should create statistics from SimpleRequirementStats"() {
		given:
		RequirementVersionBundleStat.SimpleRequirementStats simpleRequirementStats = new RequirementVersionBundleStat.SimpleRequirementStats(123L)
		simpleRequirementStats.setRate(RequirementVersionBundleStat.SimpleRequirementStats.REDACTION_RATE_KEY,
			50.0, 2, 1)
		simpleRequirementStats.setRate(RequirementVersionBundleStat.SimpleRequirementStats.VERIFICATION_RATE_KEY,
			75.0, 4, 3)
		simpleRequirementStats.setRate(RequirementVersionBundleStat.SimpleRequirementStats.VALIDATION_RATE_KEY,
			33.0, 3, 1)

		when:
		def remoteStats = RemoteRequirementStatistics.fromSimpleStats(simpleRequirementStats)

		then:
		remoteStats.redactionRate.matchingCount == 1
		remoteStats.redactionRate.totalCount == 2
		remoteStats.redactionRate.normalizedRate == 0.5d
		remoteStats.redactionRate.percentRoundedRate == 50

		remoteStats.verificationRate.matchingCount == 3
		remoteStats.verificationRate.totalCount == 4
		remoteStats.verificationRate.normalizedRate == 0.75d
		remoteStats.verificationRate.percentRoundedRate == 75

		remoteStats.validationRate.matchingCount == 1
		remoteStats.validationRate.totalCount == 3
		Math.abs(remoteStats.validationRate.normalizedRate - 0.33d) <= 0.01d
		remoteStats.validationRate.percentRoundedRate == 33
	}

	def "it should create merged statistics"() {
		given:
		List<RemoteRequirementStatistics> toMerge = Arrays.asList(
			RemoteRequirementStatistics.fromRates(
				RemoteRequirementStatistics.Rate.from(1, 2),
				RemoteRequirementStatistics.Rate.from(0, 0),
				RemoteRequirementStatistics.Rate.from(2, 4),
			),
			RemoteRequirementStatistics.fromRates(
				RemoteRequirementStatistics.Rate.from(1, 1),
				RemoteRequirementStatistics.Rate.from(2, 3),
				RemoteRequirementStatistics.Rate.from(3, 6),
			),
		)

		when:
		def remoteStats = RemoteRequirementStatistics.merging(toMerge)

		then:
		remoteStats.redactionRate.matchingCount == 2
		remoteStats.redactionRate.totalCount == 3
		Math.abs(remoteStats.redactionRate.normalizedRate - 0.66d) <= 0.01d
		remoteStats.redactionRate.percentRoundedRate == 67

		remoteStats.verificationRate.matchingCount == 2
		remoteStats.verificationRate.totalCount == 3
		Math.abs(remoteStats.verificationRate.normalizedRate - 0.66d) <= 0.01d
		remoteStats.verificationRate.percentRoundedRate == 67

		remoteStats.validationRate.matchingCount == 5
		remoteStats.validationRate.totalCount == 10
		Math.abs(remoteStats.validationRate.normalizedRate - 0.5d) <= 0.01d
		remoteStats.validationRate.percentRoundedRate == 50

	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.statistics.testingstatus

import org.squashtest.tm.service.statistics.testingstatus.exception.IllegalStatisticRatesException;
import spock.lang.Specification;

class RemoteTestingStatusTest extends Specification {
	def "should compute synthetic status from rates"() {

		given:
		def stats = RemoteRequirementStatistics.fromRates(
			RemoteRequirementStatistics.Rate.from(redactionRate, 100),
			RemoteRequirementStatistics.Rate.from(verificationRate, 100),
			RemoteRequirementStatistics.Rate.from(validationRate, 100),
		)

		when:
		def status = RemoteTestingStatus.findStatus(stats)

		then:
		status == expectedStatus

		where:
		redactionRate   | verificationRate  | validationRate     || expectedStatus
		0               |0                  |0                   || RemoteTestingStatus.NOT_INITIALIZED
		20              |0                  |0                   || RemoteTestingStatus.CONCEPTION
		100             |0                  |0                   || RemoteTestingStatus.TO_EXECUTE
		100             |10                 |100                 || RemoteTestingStatus.VALIDATION_IN_PROGRESS
		50              |10                 |100                 || RemoteTestingStatus.VALIDATION_IN_PROGRESS
		50              |90                 |100                 || RemoteTestingStatus.VALIDATION_IN_PROGRESS
		0               |30                 |100                 || RemoteTestingStatus.VALIDATION_IN_PROGRESS
		30              |30                 |50                  || RemoteTestingStatus.INVALID
		0               |50                 |0                   || RemoteTestingStatus.INVALID
		100             |100                |99                  || RemoteTestingStatus.INVALID
		100             |100                |0                   || RemoteTestingStatus.INVALID
		100             |100                |100                 || RemoteTestingStatus.VALID
		0               |100                |100                 || RemoteTestingStatus.VALID
		10              |100                |100                 || RemoteTestingStatus.VALID
	}

	def "should reject invalid rate values"() {
		given:
		def stats = RemoteRequirementStatistics.fromRates(
			RemoteRequirementStatistics.Rate.from(redactionRate, 100),
			RemoteRequirementStatistics.Rate.from(verificationRate, 100),
			RemoteRequirementStatistics.Rate.from(validationRate, 100),
		)

		when:
		RemoteTestingStatus.findStatus(stats)

		then:
		thrown(IllegalStatisticRatesException.class)

		where:
		redactionRate   | verificationRate  | validationRate
		-10             |0                  |0
		20              |-9		            |0
		100             |0                  |-5
	}
}

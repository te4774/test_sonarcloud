/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.requirement

import org.squashtest.tm.domain.requirement.HighLevelNewRequirementVersionDto
import org.squashtest.tm.domain.requirement.HighLevelRequirement
import org.squashtest.tm.domain.requirement.HighLevelRequirementCreator
import org.squashtest.tm.domain.requirement.NewRequirementVersionDto
import org.squashtest.tm.domain.requirement.Requirement
import spock.lang.Specification

class RequirementFactoryTest extends Specification {

	def "should delegate high level requirement creation"() {
		given:
		def dto = new HighLevelNewRequirementVersionDto()
		dto.name = "req"

		and:
		HighLevelRequirementCreator highLevelRequirementCreator = Stub()
		def createdRequirement = new HighLevelRequirement(dto.toRequirementVersion())
		highLevelRequirementCreator.createRequirement(dto) >> createdRequirement
		RequirementFactory requirementFactory = new RequirementFactory([highLevelRequirementCreator], [])

		when:
		def requirement = requirementFactory.createRequirement(dto)

		then:
		requirement instanceof HighLevelRequirement
		highLevelRequirementCreator.createRequirement(dto)
		requirement == createdRequirement
	}

	def "should not delegate low level requirement creation"() {
		given:
		def dto = new NewRequirementVersionDto()
		dto.name = "req"

		and:
		HighLevelRequirementCreator highLevelRequirementCreator = Mock()
		RequirementFactory requirementFactory = new RequirementFactory([highLevelRequirementCreator], [])

		when:
		def requirement = requirementFactory.createRequirement(dto)

		then:
		requirement instanceof Requirement
		0 * highLevelRequirementCreator.createRequirement(dto)
	}

	def "should throw if try to create high level requirement without dedicated class"() {
		given:
		def dto = new HighLevelNewRequirementVersionDto()
		dto.name = "req"

		and:
		RequirementFactory requirementFactory = new RequirementFactory([], [])

		when:
		requirementFactory.createRequirement(dto)

		then:
		thrown(UnsupportedOperationException)
	}


}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.project

import org.squashtest.tm.api.template.TemplateConfigurablePlugin
import spock.lang.Specification

class TemplateConfigurablePluginBindingTest extends Specification {

    def "Should create a template configurable plugin binding"() {
        given:
        Project project = Mock()
        ProjectTemplate template = Mock()
        TemplateConfigurablePlugin plugin = Mock()
        plugin.id >> "TotoPlugin"

        when:
        def binding = TemplateConfigurablePluginBinding.createBinding(template, project, plugin)

        then:
        binding.pluginId == "TotoPlugin"
        binding.project == project
        binding.projectTemplate == template
    }

    def "Should forbid malformed template configurable plugin binding creation"() {
        given:
        Project project = Mock()
        ProjectTemplate template = Mock()
        TemplateConfigurablePlugin plugin = Mock()

        when:
        TemplateConfigurablePluginBinding.createBinding(null, project, plugin)

        then:
        thrown IllegalArgumentException

        when:
        TemplateConfigurablePluginBinding.createBinding(template, null, plugin)

        then:
        thrown IllegalArgumentException

        when:
        TemplateConfigurablePluginBinding.createBinding(template, project, null)

        then:
        thrown IllegalArgumentException
    }

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.environmentvariable;

import org.squashtest.tm.domain.Sizes;
import org.squashtest.tm.domain.common.ordered.Ordered;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Embeddable
public class EnvironmentVariableOption implements Ordered {

	@NotBlank
	@Size(max = Sizes.LABEL_MAX)
	private String label;

	@NotBlank
	@Size(max = 30)
	@Pattern(regexp = EnvironmentVariable.CODE_REGEXP, message = "{org.squashtest.tm.validation.constraint.onlyStdChars}")
	private String code = "";

	private Integer position;

	/**
	 * For Hibernate.
	 */
	public EnvironmentVariableOption() {
	}

	public EnvironmentVariableOption(String label, String code) {
		this.label = label;
		this.code = code;
	}
	public EnvironmentVariableOption(String label, String code, Integer position) {
		this.label = label;
		this.code = code;
		this.position = position;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	@Override
	public void setOrder(Integer i) {
		setPosition(i);
	}
}

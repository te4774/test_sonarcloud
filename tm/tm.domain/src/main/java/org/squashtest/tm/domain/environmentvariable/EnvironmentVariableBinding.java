/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.environmentvariable;

import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

@Entity
public class EnvironmentVariableBinding implements Identified {

	@Id
	@Column(name = "EVB_ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "environment_variable_binding_evb_id_seq")
	@SequenceGenerator(name = "environment_variable_binding_evb_id_seq", sequenceName = "environment_variable_binding_evb_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = EnvironmentVariable.class)
	@JoinColumn(name = "EV_ID", updatable = false)
	@NotNull
	private EnvironmentVariable environmentVariable;

	@ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = TestAutomationServer.class)
	@JoinColumn(name = "BOUND_SERVER_ID", updatable = false)
	@NotNull
	private TestAutomationServer testAutomationServer;

	public EnvironmentVariableBinding() {
		/* No args constructor for serialization only ! */
	}

	public EnvironmentVariable getEnvironmentVariable() {
		return environmentVariable;
	}

	public void setEnvironmentVariable(EnvironmentVariable environmentVariable) {
		this.environmentVariable = environmentVariable;
	}

	public TestAutomationServer getTestAutomationServer() {
		return testAutomationServer;
	}

	public void setTestAutomationServer(TestAutomationServer testAutomationServer) {
		this.testAutomationServer = testAutomationServer;
	}

	@Override
	public Long getId() {
		return id;
	}
}

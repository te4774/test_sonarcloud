/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.common.ordered;

import java.util.List;
import java.util.Objects;

public class ListIndexValidator<T> {

	private final List<T> list;
	private final Integer index;

	public ListIndexValidator(List<T> list, Integer index) {
		this.list = Objects.requireNonNull(list);
		this.index = index;
	}

	public boolean isValid() {
		return index != null && index < list.size() && index >= 0;
	}

	public Integer coerce() {
		if (index == null || index < 0) {
			return 0;
		}
		if (index > list.size()) {
			return list.size() - 1;
		}
		return index;
	}
}

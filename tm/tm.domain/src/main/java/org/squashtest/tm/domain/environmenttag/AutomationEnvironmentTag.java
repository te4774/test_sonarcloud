/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.environmenttag;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Embeddable
public class AutomationEnvironmentTag {

	@NotBlank
	@Size(max = 255)
	@Column(name = "VALUE")
	private String value;

	@Enumerated(EnumType.STRING)
	@Column(name = "ENTITY_TYPE")
	private AutomationEnvironmentTagHolder entityType;

	public AutomationEnvironmentTag() {
	}

	public AutomationEnvironmentTag(String value, AutomationEnvironmentTagHolder entityType) {
		this.value = value;
		this.entityType = entityType;
	}

	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public AutomationEnvironmentTagHolder getEntityType() {
		return entityType;
	}
	public void setEntityType(AutomationEnvironmentTagHolder entityType) {
		this.entityType = entityType;
	}

}

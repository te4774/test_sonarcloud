/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.requirement;

import org.squashtest.tm.domain.library.NodeVisitor;
import org.squashtest.tm.exception.requirement.ForbiddenHighLevelLinkException;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;

@Entity
@PrimaryKeyJoinColumn(name = "RLN_ID")
public class HighLevelRequirement extends Requirement {

	protected HighLevelRequirement() {
		super();
	}

	/**
	 * Creates a new requirement which "latest version" is the given {@link RequirementVersion}
	 *
	 */
	public HighLevelRequirement(@NotNull RequirementVersion version) {
		super(version);
	}

	@Override
	public boolean isHighLevel() {
		return true;
	}

	@Override
	public void setHighLevelRequirement(HighLevelRequirement highLevelRequirement) {
		throw new ForbiddenHighLevelLinkException();
	}

	/**
	 * Creates a copy usable in a copy / paste operation. The copy is associated to no version, it should be done by the
	 * caller (the latest version might not be eligible for copy / paste).
	 */
	@Override
	public HighLevelRequirement createCopy() {

		HighLevelRequirement copy = new HighLevelRequirement();
		copy.notifyAssociatedWithProject(this.getProject());
		RequirementVersion latestVersionCopy = getCurrentVersion().createPastableCopy(copy);
		copy.addVersion(latestVersionCopy);
		copy.resource = latestVersionCopy;
		return copy;
	}

	@Override
	public void accept(NodeVisitor visitor) {
		visitor.visit(this);
	}
}

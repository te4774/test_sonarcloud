/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.helper;

import org.squashtest.tm.exception.WrongStringSizeException;
import org.squashtest.tm.exception.customfield.CodeDoesNotMatchesPatternException;
import org.squashtest.tm.exception.customfield.OptionAlreadyExistException;

import java.util.List;

public final class EntityOptionHelper {

	public static void checkCodePattern(String code, String codeRegexp, int minCodeSize, int maxCodeSize) {
		if (!code.matches(codeRegexp)) {
			throw new CodeDoesNotMatchesPatternException(code, codeRegexp, "optionCode");
		}
		if (code.length() > maxCodeSize || code.length() < minCodeSize) {
			throw new WrongStringSizeException("code", minCodeSize, maxCodeSize);
		}
	}

	public static void checkLabelAvailability(String label, List<String> optionLabels) {
		if (label.length() > 255 || label.length() < 1) {
			throw new WrongStringSizeException("label", 1, 255);
		}

		if (!isLabelAvailable(label, optionLabels)) {
			throw new OptionAlreadyExistException(label);
		}
	}

	private static boolean isLabelAvailable(String newLabel, List<String> optionLabels) {
		return !optionLabels.contains(newLabel);
	}

	public static boolean isCodeAvailable(String code, List<String> optionCodes) {
		return !optionCodes.contains(code);
	}

}

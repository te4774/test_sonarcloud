/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.environmentvariable;

import org.squashtest.tm.core.foundation.i18n.Internationalizable;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;

public enum EVBindableEntity implements Internationalizable {
	PROJECT() {
		@Override
		public Class<?> getReferencedClass() {
			return Project.class;
		}

	},
	TEST_AUTOMATION_SERVER() {
		@Override
		public Class<?> getReferencedClass() {
			return TestAutomationServer.class;
		}

	};

	private static final String I18N_NAMESPACE = "label.environmentVariable.bindableEntity.";

	@Override
	public String getI18nKey() {
		return I18N_NAMESPACE + name();
	}

	public abstract Class<?> getReferencedClass();

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.project;

import org.squashtest.tm.domain.customfield.BindableEntity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import static org.squashtest.tm.domain.project.Project.PROJECT_TYPE;

@Entity
@DiscriminatorValue(PROJECT_TYPE)
@NamedQueries({
	@NamedQuery(name = "Project.fetchForAutomatedExecutionCreation", query = "select distinct p " +
		"from Project p " +
		"left join fetch p.bugtrackerBinding " +
		"left join fetch p.testAutomationProjects " +
		"left join fetch p.testAutomationServer " +
		"left join fetch p.testCaseNatures " +
		"where p.id=:projectId")
})
public class Project extends GenericProject {

	public static final String PROJECT_TYPE = "P";
	public static final String CLASS_NAME = "org.squashtest.tm.domain.project.Project";
	private static final String SIMPLE_CLASS_NAME = "Project";

	public Project() {
		super();
	}

	/**
	 * @see org.squashtest.tm.domain.project.GenericProject#accept(org.squashtest.tm.domain.project.ProjectVisitor)
	 */
	@Override
	public void accept(ProjectVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public Long getBoundEntityId() {
		return getId();
	}

	@Override
	public BindableEntity getBoundEntityType() {
		return null;
	}

	/**
	 JTH - 2020/08/26 : Dead code here. Project can't implement {@link org.squashtest.tm.domain.customfield.BoundEntity}. However, removing these
	 methods make the generated code by QueryDSL/APT-plugin change. Especially, this method change the name of the static reference in {@link QProject}.
	 It is now named "project1" and all code referencing this produces errors at compile time if i remove this getProject method. Hence the attribute is renamed
	 to the correct name : "project"
	 For plugins if the POM is not up to date, the error is visible only at runtime. So for now we keep that dead code however after merging 1.22 to 2.0 the whole
	 implement of {@link org.squashtest.tm.domain.customfield.BoundEntity} should be removed in {@link GenericProject} and implementation removed in subclass.
	 A nice example of dead code that produce nasty bugs...
	 */
	@Override
	public Project getProject() {
		return this;
	}

	@Override
	public String getClassSimpleName() {
		return SIMPLE_CLASS_NAME;
	}

	@Override
	public String getClassName() {
		return CLASS_NAME;
	}
}

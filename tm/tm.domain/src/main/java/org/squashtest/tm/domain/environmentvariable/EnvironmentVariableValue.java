/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.environmentvariable;

import org.squashtest.tm.domain.Identified;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class EnvironmentVariableValue implements Identified {
	public static final int MAX_SIZE = 255;

	@Id
	@Column(name = "EVV_ID")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "environment_variable_value_evv_id_seq")
	@SequenceGenerator(name = "environment_variable_value_evv_id_seq", sequenceName = "environment_variable_value_evv_id_seq", allocationSize = 1)
	private Long id;

	@Enumerated(EnumType.STRING)
	private EVBindableEntity boundEntityType;

	protected Long boundEntityId;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "EVB_ID")
	protected EnvironmentVariableBinding binding;

	@NotNull
	@Size(min = 0, max = MAX_SIZE)
	private String value = "";

	public EnvironmentVariableValue() {
	}

	public EnvironmentVariableValue(EVBindableEntity boundEntityType, Long boundEntityId,
									EnvironmentVariableBinding binding) {
		this.boundEntityType = boundEntityType;
		this.boundEntityId = boundEntityId;
		this.binding = binding;
	}

	@Override
	public Long getId() {
		return id;
	}

	public EVBindableEntity getBoundEntityType() {
		return boundEntityType;
	}

	public void setBoundEntityType(EVBindableEntity boundEntityType) {
		this.boundEntityType = boundEntityType;
	}

	public Long getBoundEntityId() {
		return boundEntityId;
	}

	public void setBoundEntityId(Long boundEntityId) {
		this.boundEntityId = boundEntityId;
	}

	public EnvironmentVariableBinding getBinding() {
		return binding;
	}

	public void setBinding(EnvironmentVariableBinding binding) {
		this.binding = binding;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.script;

import gherkin.AstBuilder;
import gherkin.Parser;
import gherkin.ParserException;
import gherkin.ast.Background;
import gherkin.ast.Feature;
import gherkin.ast.GherkinDocument;
import gherkin.ast.ScenarioDefinition;
import gherkin.ast.Step;
import org.squashtest.tm.exception.testcase.ScriptParsingException;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class GherkinParser {
	public static GherkinDocument parseDocument(String script) {
		Parser<GherkinDocument> parser = new Parser<>(new AstBuilder());
		GherkinDocument gherkinDocument;

		try {
			gherkinDocument = parser.parse(script);
		} catch (ParserException e) {
			throw new ScriptParsingException(e.getMessage());
		}

		return gherkinDocument;
	}

	public static List<String> extractBackgroundLines(String document) {
		return extractBackgroundLines(parseDocument(document));
	}

	public static List<String> extractBackgroundLines(GherkinDocument gherkinDocument) {
		Feature feature = gherkinDocument.getFeature();

		if (feature == null) {
			return Collections.emptyList();
		}

		List<ScenarioDefinition> scenarioDefinitions = feature.getChildren();

		if (scenarioDefinitions.isEmpty()) {
			return Collections.emptyList();
		}

		// we get the first item of scenario definition witch could be the background
		// by Gherkin specification only the first item can be a background, so we will ignore other ones
		ScenarioDefinition potentialBackground = scenarioDefinitions.get(0);

		if (potentialBackground instanceof Background) {
			Background background = (Background) potentialBackground;
			return background.getSteps().stream().map(Step::getText).collect(Collectors.toList());
		}

		return Collections.emptyList();
	}

	public static boolean hasScenarios(String document) {
		return hasScenarios(parseDocument(document));
	}

	public static boolean hasScenarios(GherkinDocument document) {
		return document != null
			&& document.getFeature() != null
			&& ! document.getFeature().getChildren().isEmpty()
			&& document.getFeature().getChildren().stream().anyMatch(definition -> !(definition instanceof Background));
	}
}

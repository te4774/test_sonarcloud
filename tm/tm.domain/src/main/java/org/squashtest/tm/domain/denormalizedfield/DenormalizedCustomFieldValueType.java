/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.denormalizedfield;

import org.jooq.TableField;

import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_VALUE;
import static org.squashtest.tm.jooq.domain.Tables.DENORMALIZED_FIELD_VALUE_OPTION;

public enum DenormalizedCustomFieldValueType {
	CF(DenormalizedCustomFieldValueDiscriminators.CF, false, DENORMALIZED_FIELD_VALUE.VALUE),
	SSF(DenormalizedCustomFieldValueDiscriminators.SSF, false, DENORMALIZED_FIELD_VALUE.VALUE),
	NUM(DenormalizedCustomFieldValueDiscriminators.NUM, false, DENORMALIZED_FIELD_VALUE.VALUE),
	RTF(DenormalizedCustomFieldValueDiscriminators.RTF, false, DENORMALIZED_FIELD_VALUE.LARGE_VALUE),
	// take care of tags values that are multiple values
	MFV(DenormalizedCustomFieldValueDiscriminators.MFV, true, DENORMALIZED_FIELD_VALUE_OPTION.LABEL);

	private final String discriminatorValue;
	private final boolean isMultiValue;
	private final TableField<?,String> valueColumn;

	DenormalizedCustomFieldValueType(String discriminatorValue, boolean isMultiValue, TableField<?, String> valueColumn) {
		this.discriminatorValue = discriminatorValue;
		this.isMultiValue = isMultiValue;
		this.valueColumn = valueColumn;
	}

	public String getDiscriminatorValue() {
		return discriminatorValue;
	}

	public boolean isMultiValue() {
		return isMultiValue;
	}

	public TableField<?,String> getValueColumn() {
		return this.valueColumn;
	}

	public interface DenormalizedCustomFieldValueDiscriminators {
		 String CF = "CF";
		 String SSF = "SSF";
		 String NUM = "NUM";
		 String RTF = "RTF";
		 String MFV = "MFV";
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain;

/**
 * This interface was created in order to fix permissions which were broken with Hibernate update in 4.0.0 version because
 * it was correctly using fetchType = lazy on {@link org.squashtest.tm.domain.project.GenericProject} libraries
 * and the permissions need the complete object (not a hibernate proxy),
 * then the type was with proxy (...TestCaseLibrary$HibernateProxy... and not just ...TestCaseLibrary)
 * when retrieved by Spring Sec and logically it couldn't find it in the cache.
 *
 * Each primary object, annotated with {@link org.squashtest.tm.security.annotation.AclConstrainedObject} should implement this interface.
 * A force cast is made in AnnotatedPropertyObjectIdentityRetrievalStrategy#getObjectIdentity
 * and we do not let Spring Sec generate the {@link org.springframework.security.acls.model.ObjectIdentity},
 * we bypass it with the {@link Identified#getId()} and {@link SelfClassAware#getClassName()}.
 */
public interface AclPrimaryObject extends Identified, SelfClassAware {

}

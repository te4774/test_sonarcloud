/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain;

import org.apache.commons.lang3.StringUtils;
import org.squashtest.tm.domain.actionword.ActionWordLibrary;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.attachment.AttachmentList;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.bugtracker.Issue;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibrary;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.chart.ChartDefinition;
import org.squashtest.tm.domain.customreport.CustomReportCustomExport;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.domain.customreport.CustomReportFolder;
import org.squashtest.tm.domain.customreport.CustomReportLibrary;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.report.ReportDefinition;
import org.squashtest.tm.domain.requirement.HighLevelRequirement;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedTest;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibrary;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequest;
import org.squashtest.tm.domain.users.User;

import java.util.EnumSet;
import java.util.Objects;
import java.util.Optional;

public enum EntityType {

	// @formatter:off
	PROJECT(Project.class),
	TEST_CASE_LIBRARY(TestCaseLibrary.class, NodeType.TEST_CASE_LIBRARY),
	TEST_CASE_FOLDER(TestCaseFolder.class, NodeType.TEST_CASE_FOLDER),
	TEST_CASE(TestCase.class, NodeType.TEST_CASE),
	TEST_CASE_STEP(TestStep.class),
	REQUIREMENT_LIBRARY(RequirementLibrary.class, NodeType.REQUIREMENT_LIBRARY),
	REQUIREMENT_FOLDER(RequirementFolder.class, NodeType.REQUIREMENT_FOLDER),
	REQUIREMENT(Requirement.class, NodeType.REQUIREMENT),
	REQUIREMENT_VERSION(RequirementVersion.class),
	HIGH_LEVEL_REQUIREMENT(HighLevelRequirement.class, NodeType.HIGH_LEVEL_REQUIREMENT),
	CAMPAIGN_LIBRARY(CampaignLibrary.class, NodeType.CAMPAIGN_LIBRARY),
	CAMPAIGN_FOLDER(CampaignFolder.class, NodeType.CAMPAIGN_FOLDER),
	CAMPAIGN(Campaign.class, NodeType.CAMPAIGN),
	ITERATION(Iteration.class, NodeType.ITERATION),
	EXECUTION(Execution.class),
	TEST_SUITE(TestSuite.class, NodeType.TEST_SUITE),
	EXECUTION_STEP(ExecutionStep.class),
	TEST_STEP(TestStep.class),
	ISSUE(Issue.class),
	ITEM_TEST_PLAN(IterationTestPlanItem.class),
	INFO_LIST_ITEM(InfoListItem.class),
	USER(User.class),
	MILESTONE(Milestone.class),
	AUTOMATED_TEST(AutomatedTest.class),
	AUTOMATED_EXECUTION_EXTENDER(AutomatedExecutionExtender.class),
	ATTACHMENT(Attachment.class),
	ATTACHMENT_LIST(AttachmentList.class),
	AUTOMATION_REQUEST(AutomationRequest.class),
	DATASET(Dataset.class),
	PARAMETER(Parameter.class),
	CUSTOM_REPORT_LIBRARY(CustomReportLibrary.class, NodeType.CUSTOM_REPORT_LIBRARY),
	CUSTOM_REPORT_FOLDER(CustomReportFolder.class, NodeType.CUSTOM_REPORT_FOLDER),
	CUSTOM_REPORT_DASHBOARD(CustomReportDashboard.class, NodeType.CUSTOM_REPORT_DASHBOARD),
	CHART_DEFINITION(ChartDefinition.class, NodeType.CHART_DEFINITION),
	REPORT_DEFINITION(ReportDefinition.class, NodeType.REPORT_DEFINITION),
	CUSTOM_REPORT_CUSTOM_EXPORT(CustomReportCustomExport.class, NodeType.CUSTOM_REPORT_CUSTOM_EXPORT),
	ACTION_WORD_LIBRARY(ActionWordLibrary.class, NodeType.ACTION_WORD_LIBRARY),
	ACTION_WORD(ActionWord.class, NodeType.ACTION_WORD),
	SCM_REPOSITORY(ScmRepository.class);

	// @formatter:on


	EntityType(Class clazz) {
		this.clazz = clazz;
	}

	EntityType(Class clazz, NodeType nodeType) {
		this.clazz = clazz;
		this.nodeType = nodeType;
	}

	private Class clazz;

	private NodeType nodeType;

	public Class getEntityClass() {
		return clazz;
	}

	public String getSimpleClassName() {
		return clazz.getSimpleName();
	}

	public static EntityType fromSimpleName(String simpleClassName) {
		if (StringUtils.isBlank(simpleClassName)) {
			throw new IllegalArgumentException("Simple Name can't be empty");
		}

		EnumSet<EntityType> entityTypes = EnumSet.allOf(EntityType.class);

		Optional<EntityType> type = entityTypes.stream()
			.filter(entityType -> entityType.getSimpleClassName().equals(simpleClassName))
			.findFirst();

		if (type.isPresent()) {
			return type.get();
		}

		throw new IllegalArgumentException("No Entity Type for simple class name : " + simpleClassName);
	}

	public NodeType toNodeType() {
		if(Objects.isNull(nodeType)){
			String message = String.format("Then entity type %s can't be converted into node type", this.name());
			throw new UnsupportedOperationException(message);
		} else {
			return nodeType;
		}
	}
}

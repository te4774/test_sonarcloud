/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.domain.execution;

import org.squashtest.tm.domain.testcase.Dataset;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.squashtest.tm.domain.bdd.ActionWord.ACTION_WORD_CLOSE_GUILLEMET;
import static org.squashtest.tm.domain.bdd.ActionWord.ACTION_WORD_OPEN_GUILLEMET;

/**
 * Helper to perform replacement of parameters by the provided dataset values during execution and execution steps creation.
 * Methods made static to enforce the stateless design of the class. If you do need state, drop the statics and make this
 * a bean.
 */
public final class ExecutionParameterInterpolator {

	private static final String PARAM_PREFIX = "\\Q${\\E";
	private static final String PARAM_SUFFIX = "\\Q}\\E";
	private static final String PARAM_PATTERN = PARAM_PREFIX + "([A-Za-z0-9_-]{1,255})" + PARAM_SUFFIX;
	private static final String NO_PARAM = "&lt;no_value&gt;";
	private static final String KEYWORD_PARAM_PATTERN = ACTION_WORD_OPEN_GUILLEMET + "([A-Za-z0-9_-]{1,255})" + ACTION_WORD_CLOSE_GUILLEMET;

	/**
	 * Builds a registry for parameter values by reference test case id.
	 *
	 * @param dataset the dataset to extract parameter values from
	 * @return the built registry
	 */
	public static CollectedParameters collectParameters(Dataset dataset) {
		if (dataset == null) {
			return CollectedParameters.empty();
		}

		final Map<Long, Map<String, String>> parameterValuesByTestCase = new HashMap<>();
		dataset.getParameterValues().forEach(datasetParamValue -> {
			final Long referenceTestCaseId = datasetParamValue.getParameter().getTestCase().getId();

			// Create entry if it doesn't exist
			if (!parameterValuesByTestCase.containsKey(referenceTestCaseId)) {
				parameterValuesByTestCase.put(referenceTestCaseId, new HashMap<>());
			}

			final String paramName = datasetParamValue.getParameter().getName();
			final String paramValue = datasetParamValue.getParamValue();
			parameterValuesByTestCase.get(referenceTestCaseId).put(paramName, paramValue);
		});

		return new CollectedParameters(parameterValuesByTestCase);
	}

	/**
	 * Perform parameter values interpolation.
	 *
	 * @param source              The text to transform
	 * @param referenceTestCaseId The test case that holds the parameter
	 * @param collectedParameters Parameters that were collected with collectParameters(Dataset).
	 * @return The source string with parameter interpolated
	 */
	public static String replaceParams(String source, Long referenceTestCaseId, CollectedParameters collectedParameters) {
		if (source == null || source.isEmpty()) {
			return source;
		}

		Objects.requireNonNull(referenceTestCaseId);
		Objects.requireNonNull(collectedParameters);

		return doReplaceParams(source, referenceTestCaseId, collectedParameters, PARAM_PATTERN);
	}

	/**
	 * Perform keyword parameter values interpolation.
	 */
	public static String replaceKeywordParams(String source, Long referenceTestCaseId, CollectedParameters collectedParameters) {
		if (source == null || source.isEmpty()) {
			return source;
		}

		Objects.requireNonNull(referenceTestCaseId);
		Objects.requireNonNull(collectedParameters);

		return doReplaceParams(source, referenceTestCaseId, collectedParameters, KEYWORD_PARAM_PATTERN);
	}

	// Code mostly migrated from Execution/ExecutionStep (used to be duplicated).
	private static String doReplaceParams(@NotNull String source,
										  @NotNull Long referenceTestCaseId,
										  @NotNull CollectedParameters collectedParameters,
										  @NotNull String paramPattern) {
		StringBuilder builder = new StringBuilder(source);

		Pattern pattern = Pattern.compile(paramPattern);
		Matcher matcher = pattern.matcher(source);

		// each time a replacement occurs in the stringbuilder deviates
		// a bit further from string scanned by the matcher.
		//
		// Consequently the more the string is modified the more the length might be altered,
		// which leads to inconsistencies in the position of a given substring in the original string
		// and the modified string.
		//
		// Thus, the following variable keeps track
		// of the modifications to adjust the start and end position
		//
		int offset = 0;

		while (matcher.find()) {
			String paramName = matcher.group(1);

			String paramValue = collectedParameters.findValue(referenceTestCaseId, paramName);
			if (paramValue == null || paramValue.isEmpty()) {
				paramValue = NO_PARAM;
			}

			int start = matcher.start();
			int end = matcher.end();

			builder.replace(start + offset, end + offset, paramValue);

			offset += paramValue.length() - (end - start);
		}

		return builder.toString();
	}

	// Groups parameter values by reference test case (the test case in which the parameter was created)
	public static final class CollectedParameters {
		private final Map<Long, Map<String, String>> parameterValuesByTestCase;

		public static CollectedParameters empty() {
			return new CollectedParameters(Collections.emptyMap());
		}

		CollectedParameters(@NotNull Map<Long, Map<String, String>> parameterValuesByTestCase) {
			this.parameterValuesByTestCase = parameterValuesByTestCase;
		}

		public String findValue(@NotNull Long referenceTestCase, @NotNull String parameterName) {
			return parameterValuesByTestCase.getOrDefault(referenceTestCase, Collections.emptyMap())
				.getOrDefault(parameterName, null);
		}
	}
}

# tm-front
## Project overview
This project has two main modules :

1. `sqtm-core`: A module containing the common core for squash-tm main application and plugins.
2. `sqtm-app`: A module containing the main squash tm application.

The Cypress tests (integration and end-to-end tests) are in `./cypress` folder.

### Notes on sqtm-core exports
- For exports to be visible outside the `sqtm-core` module, you have to re-export them in `projects/sqtm-core/src/public_api.ts`.
- From `sqtm-app`, always import from `sqtm-core` and do NOT use relative paths.

```
// NOT OK
import {Identifier} from '../../../../../../../../projects/sqtm-core/src/lib/ui/grid/model/data-row.model';

// OK
import {Identifier} from 'sqtm-core';
```

## Development environment setup

You'll need NodeJS and Yarn installed.

### Installing NodeJS dependencies

To install the dependencies, run `yarn install --frozen-lockfile` from `tm/tmfront`.

The option `--frozen-lockfile` ensures that the same dependency versions are used for all developers.
Only do `yarn install` without the option when changes are made to the `package.json` dependencies.

### Start the development server

The development server allows hot module reloading on both sqtm-core and sqtm-app. Start it with :
```
yarn start
```

The application should be then accessible at `http://localhost:4200/squash`.

### Running the unit tests (Karma)
Both module has its own set of unit tests. You can run them with :

```
ng test sqtm-core
```

```
ng test sqtm-app
```

### Running the integration tests (Cypress)

With the development server started on port 4200, you can launch test integrations tests in interactive mode with :

```
yarn watch-cypress
```

You can also run the tests in headless mode with :

```
yarn integration-tests-cypress-dev
```

### Running the end-to-end tests (Cypress)

To run the E2E tests, you'll need Cypress installed, dedicated databases and set up a few cypress environments variables.

#### Creating a database

The process is the same as for production and development databases. You can create a new PostgreSQL or MariaDB schema and provision it with the database install scripts.
Make sure to dedicate these databases to E2E tests only as they will be cleaned up between each test.

#### Setup the environment variables

The simplest way to provide environment variables for Cypress is to add a `cypress.env.json` file, next to `cypress.json` (the root of the front-end project).
This file should contain the connection infos to your databases:

```
{
  "profiles": {
    "postgres": {
      "database": "squashtm_e2e",
      "port": 5432,
      "user": "postgres",
      "password": "root",
      "host": "localhost"
    },
    "mysql": {
      "database": "squashtm_e2e",
      "port": 3306,
      "user": "root",
      "password": "root",
      "host": "localhost"
    }
  }
}
```

Both `mysql` and `postgres` profiles are required. Just put empty string values if you don't want to use one of these profile.

#### Running the tests

To execute the tests, run one of those commands from the front-end root folder :
- `yarn e2e-cypress-postgres` : runs on PostgreSQL in headless mode
- `yarn e2e-cypress-mysql` : runs on MariaDB in headless mode
- `yarn watch-cypress-e2e-postgres` : runs on PostgreSQL in interactive mode
- `yarn watch-cypress-e2e-mysql` : runs on MariaDB in interactive mode

## Releasing the core
As squashTM.IT.28, we use Angular 11. Publishing ivy libraries is not allowed, so you must rebuild sqtm-core without building sqtm-app before publishing. Building sqtm-app will make ngcc transform sqtm-core module into ivy code, and thus it will reject the publish

### Release procedure:

- yarn build-core -> It will build sqtm-core and create the distribution of sqtm-core into dist/sqtm-core
- navigate to dist/sqtm-core folder. You must release the core distribution and not the root package or the project/sqtm-core package.
- open generated package.json and check the version.
- publish to your npm repository ``` yarn publish --no-git-tag-version ```
- the published version will have version number that you enter the line: question New version
- Increment versions number in the two SOURCE package.json (the root one, and the project/sqtm-core one)
- Note on versions :
  - npm semver do not accept a floating snapshot version. If you need to publish a snapshot, use a version number like "2.0.0-it.28.snapshot.1"
  - If you need to publish an iteration version or a release candidate, use a version number like "2.0.0-it.28" or "2.0.0-rc.1"
  - If you need to publish a release, use a version number like "2.0.0" or "2.0.1"
  - Take care of what REAL version your plugins yarn install will resolve to be sure you have the desired version. Try to avoid dependency to snapshot version once your dev is over.

All this release process should be automated in the future, and the CI should perform release itself. In the meantime we must handle it manually.

However, for local development, it could be necessary to handle snapshot release if a plugin require a modification in sqtm-core...


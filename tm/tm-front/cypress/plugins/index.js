const mysqlExecuteQuery = require('./mysql-execute-query');
const pgExecuteQuery = require('./pg-execute-query');
const {cleanupScript} = require('./cleanup-script');


function getDatabaseProfile(config) {
  const profileName = config.env.profile;
  const accepted = ['mysql', 'postgresql'];

  if (!accepted.includes(profileName)) {
    throw new Error(`Unknown profile ${profileName}. Accepted values are : ${accepted.join(', ')}`);
  }

  return profileName;
}

module.exports = (on, config) => {
  on('task', {
    // in cypress task are identified by their names that must coincide with the function name in task object
    cleanDatabase() {
      const profile = getDatabaseProfile(config);
      return profile === 'postgresql' ?
        pgExecuteQuery.executeQuery(cleanupScript, config)
        : mysqlExecuteQuery.executeQuery(cleanupScript, config);
    },
    executeQuery(query) {
      const profile = getDatabaseProfile(config);
      return profile === 'postgresql' ?
        pgExecuteQuery.executeQuery(query, config) : mysqlExecuteQuery.executeQuery(query, config);
    }
  });
  on('before:browser:launch', (browser, launchOptions) => {
    if (browser.family === 'chromium' && browser.name !== 'electron') {
      launchOptions.preferences.default.intl = {"accept_languages": 'fr'};
      return launchOptions;
    }
  });
};

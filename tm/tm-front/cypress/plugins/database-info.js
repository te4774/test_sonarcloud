/*
For this task to work, you need to provide a custom cypress environment file ("cypress.env.json", living next to
"cypress.json") where you specify you database connection infos as such:

{
  "profiles": {
    "postgres": {
      "database": "squash_e2e",
      "port": 5432,
      "user": "postgres",
      "password": "root",
      "host": "localhost"
    },
    "mysql": {
      "database": "squash_e2e",
      "port": 3306,
      "user": "root",
      "password": "root",
      "host": "localhost"
    }
  }
}

Both 'mysql' and 'postgres' profiles are required. Just put empty strings values if you don't plan to use the profile.
 */

function getDatabaseInfos(config) {
  const profileName = config.env.profile;

  if (!config.env.profiles[profileName]) {
    throw new Error(`Env variables for profile ${profileName} are not set. Please check your database connection infos (cypress.env.json).`);
  }

  const user = config.env.profiles[profileName].user;
  const database = config.env.profiles[profileName].database;
  const password = config.env.profiles[profileName].password;
  const port = config.env.profiles[profileName].port;
  const host = config.env.profiles[profileName].host;

  if (!(database && user && password && host)) {
    throw new Error(`Please check your database connection infos (cypress.env.json).`);
  }

  return {
    user,
    host,
    database,
    password,
    port
  };
}

module.exports = {
  getDatabaseInfos: getDatabaseInfos
};

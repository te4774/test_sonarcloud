const mariadb = require('mariadb');
const {getDatabaseInfos} = require("./database-info");

let pool;

function retrievePool(cypressConfig) {
  if(!pool){
    const poolOptions = {
      ...getDatabaseInfos(cypressConfig),
      connectionLimit: 5,
      multipleStatements: true
    };
    pool = mariadb.createPool(poolOptions);
  }
  return pool;
}

function executeQuery(query, cypressConfig) {
  const pool = retrievePool(cypressConfig);
  return pool.query(query);
}

module.exports = {
  executeQuery: executeQuery
};

export type LevelEnum<K extends string> = { [id in K]: LevelEnumItem<K> };

export interface LevelEnumItem<K> {
  id: K;
  level: number;
  i18nKey: string;
  color?: string;
}

export type MilestoneStatusLevelEnum<K extends string> = { [id in K]: MilestoneStatusLevelEnumItem<K> };

export interface MilestoneStatusLevelEnumItem<K> extends LevelEnumItem<K> {
  isBindableToProject?: boolean;
  isBindableToObject?: boolean;
  allowObjectCreateAndDelete?: boolean;
  allowObjectModification?: boolean;
}


export type ExecutionStatusLevelEnum<K extends string> = { [id in K]: ExecutionStatusLevelEnumItem<K> };

export interface ExecutionStatusLevelEnumItem<K> extends LevelEnumItem<K> {
  color: string;
  textColor: string;
  backgroundColor: string;
  terminal: boolean;
  conclusiveness: LevelEnumItem<ConclusivenessKeys>;
  icon: string;
}


export type TestCaseStatusLevelEnum<K extends string> = { [id in K]: TestCaseStatusLevelEnumItem<K> };

export interface TestCaseStatusLevelEnumItem<K> extends LevelEnumItem<K> {
  color: string;
  icon: string;
}


export type TestCaseExecutionModeLevelEnum<K extends string> = { [id in K]: TestCaseExecutionModeLevelEnumItem<K> };

export interface TestCaseExecutionModeLevelEnumItem<K> extends LevelEnumItem<K> {
  icon: string;
}

export type TestCaseImportanceLevelEnum<K extends string> = { [id in K]: TestCaseImportanceLevelEnumItem<K> };

export interface TestCaseImportanceLevelEnumItem<K> extends LevelEnumItem<K> {
  color: string;
  icon: string;
}


export type TestCaseImportanceKeys = 'LOW' | 'MEDIUM' | 'HIGH' | 'VERY_HIGH';
export type TestCaseStatusKeys = 'WORK_IN_PROGRESS' | 'UNDER_REVIEW' | 'APPROVED' | 'OBSOLETE' | 'TO_BE_UPDATED';
export type RequirementCriticalityKeys = 'CRITICAL' | 'MAJOR' | 'MINOR' | 'UNDEFINED';
export type RequirementStatusKeys = 'WORK_IN_PROGRESS' | 'UNDER_REVIEW' | 'APPROVED' | 'OBSOLETE';
export type RequirementCategoryKeys = 'FUNCTIONAL' | 'NON_FUNCTIONAL' | 'USE_CASE' | 'BUSINESS' | 'TEST_REQUIREMENT' |
                                      'UNDEFINED' | 'ERGONOMICS' | 'PERFORMANCE' | 'TECHNICAL' | 'USER_STORY' | 'SECURITY';
export type CampaignStatusKeys = 'UNDEFINED' | 'PLANNED' | 'IN_PROGRESS' | 'FINISHED' | 'ARCHIVED';
export type MilestoneStatusKeys = 'PLANNED' | 'IN_PROGRESS' | 'FINISHED' | 'LOCKED';
export type MilestoneRangeKeys = 'GLOBAL' | 'RESTRICTED';
export type AutomationRequestStatusKeys =
  'TRANSMITTED'
  | 'AUTOMATION_IN_PROGRESS'
  | 'SUSPENDED'
  | 'REJECTED'
  | 'AUTOMATED'
  | 'READY_TO_TRANSMIT'
  | 'WORK_IN_PROGRESS';

export type TestCaseExecutionModeKeys = 'AUTOMATED' | 'MANUAL' | 'UNDEFINED';

export type ExecutionStatusKeys =
  'READY'
  | 'RUNNING'
  | 'SUCCESS'
  | 'WARNING'
  | 'FAILURE'
  | 'BLOCKED'
  | 'ERROR'
  | 'NOT_RUN'
  | 'UNTESTABLE'
  | 'NOT_FOUND'
  | 'SETTLED';

export type IterationStatusKeys =
  'UNDEFINED'
  | 'PLANNED'
  | 'IN_PROGRESS'
  | 'FINISHED'
  | 'ARCHIVED';

export type TestCaseAutomatableKeys = 'M' | 'Y' | 'N';

export type BddScriptLanguageKeys = 'ENGLISH' | 'FRENCH' | 'GERMAN' | 'SPANISH';

export type BddImplementationTechnologyKeys = 'CUCUMBER_4' | 'CUCUMBER_5_PLUS' | 'ROBOT';

export const TestCaseWeight: TestCaseImportanceLevelEnum<TestCaseImportanceKeys> = {
  LOW: {
    id: 'LOW',
    level: 1,
    i18nKey: 'sqtm-core.entity.test-case.importance.LOW',
    color: '#0885C6',
    icon: 'sqtm-core-test-case:double_down'
  },
  MEDIUM: {
    id: 'MEDIUM',
    level: 2,
    i18nKey: 'sqtm-core.entity.test-case.importance.MEDIUM',
    color: '#F4CF27',
    icon: 'sqtm-core-test-case:down'
  },
  HIGH: {
    id: 'HIGH',
    level: 3,
    i18nKey: 'sqtm-core.entity.test-case.importance.HIGH',
    color: '#FF9719',
    icon: 'sqtm-core-test-case:up'
  },
  VERY_HIGH: {
    id: 'VERY_HIGH',
    level: 4,
    i18nKey: 'sqtm-core.entity.test-case.importance.VERY_HIGH',
    color: '#CE0000',
    icon: 'sqtm-core-test-case:double_up'
  }
};

export const TestCaseStatus: TestCaseStatusLevelEnum<TestCaseStatusKeys> = {
  WORK_IN_PROGRESS: {
    id: 'WORK_IN_PROGRESS',
    level: 1,
    i18nKey: 'sqtm-core.entity.test-case.status.WORK_IN_PROGRESS',
    color: '#F4CF27',
    icon: 'sqtm-core-test-case:status'
  },
  UNDER_REVIEW: {
    id: 'UNDER_REVIEW',
    level: 2,
    i18nKey: 'sqtm-core.entity.test-case.status.UNDER_REVIEW',
    color: '#02A7F0',
    icon: 'sqtm-core-test-case:status'
  },
  APPROVED: {
    id: 'APPROVED',
    level: 3,
    i18nKey: 'sqtm-core.entity.test-case.status.APPROVED',
    color: '#1FBF05',
    icon: 'sqtm-core-test-case:status'
  },
  OBSOLETE: {
    id: 'OBSOLETE',
    level: 4,
    i18nKey: 'sqtm-core.entity.test-case.status.OBSOLETE',
    color: '#7F7F7F',
    icon: 'sqtm-core-test-case:status'
  },
  TO_BE_UPDATED: {
    id: 'TO_BE_UPDATED',
    level: 5,
    i18nKey: 'sqtm-core.entity.test-case.status.TO_BE_UPDATED',
    color: '#F59A23',
    icon: 'sqtm-core-test-case:status'
  },
};

export type RequirementCriticalityLevelEnum<K extends string> = { [id in K]: RequirementCriticalityLevelEnumItem<K> };

export interface RequirementCriticalityLevelEnumItem<K> extends LevelEnumItem<K> {
  color: string;
  icon: string;
}


export const RequirementCriticality: RequirementCriticalityLevelEnum<RequirementCriticalityKeys> = {
  CRITICAL: {
    id: 'CRITICAL',
    level: 1,
    i18nKey: 'sqtm-core.entity.requirement.criticality.CRITICAL',
    color: '#CE0000',
    icon: 'sqtm-core-requirement:double_up'
  },
  MAJOR: {
    id: 'MAJOR',
    level: 2,
    i18nKey: 'sqtm-core.entity.requirement.criticality.MAJOR',
    color: '#FF9719',
    icon: 'sqtm-core-requirement:up'
  },
  MINOR: {
    id: 'MINOR',
    level: 3,
    i18nKey: 'sqtm-core.entity.requirement.criticality.MINOR',
    color: '#0885C6',
    icon: 'sqtm-core-requirement:double_down'
  },
  UNDEFINED: {
    id: 'UNDEFINED',
    level: 4,
    i18nKey: 'sqtm-core.entity.requirement.criticality.UNDEFINED',
    color: '#B5ADAD', icon: 'sqtm-core-requirement:down'

  }
};

export type RequirementStatusLevelEnum<K extends string> = { [id in K]: RequirementStatusLevelEnumItem<K> };

export interface RequirementStatusLevelEnumItem<K> extends LevelEnumItem<K> {
  color: string;
  icon: string;
}

export const RequirementStatus: RequirementStatusLevelEnum<RequirementStatusKeys> = {
  WORK_IN_PROGRESS: {
    id: 'WORK_IN_PROGRESS',
    level: 1,
    i18nKey: 'sqtm-core.entity.requirement.status.WORK_IN_PROGRESS',
    color: '#F4CF27',
    icon: 'sqtm-core-requirement:status'

  },
  UNDER_REVIEW: {
    id: 'UNDER_REVIEW',
    level: 2,
    i18nKey: 'sqtm-core.entity.requirement.status.UNDER_REVIEW',
    color: '#02A7F0',
    icon: 'sqtm-core-requirement:status'
  },
  APPROVED: {
    id: 'APPROVED', level: 3, i18nKey: 'sqtm-core.entity.requirement.status.APPROVED', color: '#1FBF05',
    icon: 'sqtm-core-requirement:status'
  },
  OBSOLETE: {
    id: 'OBSOLETE', level: 4, i18nKey: 'sqtm-core.entity.requirement.status.OBSOLETE', color: '#7F7F7F',
    icon: 'sqtm-core-requirement:status'
  }
};

export const MilestoneStatus: MilestoneStatusLevelEnum<MilestoneStatusKeys> = {
  PLANNED: {
    id: 'PLANNED',
    level: 1,
    i18nKey: 'sqtm-core.entity.milestone.status.PLANNED',
    isBindableToProject: true,
    isBindableToObject: false,
    allowObjectCreateAndDelete: false,
    allowObjectModification: false
  },
  IN_PROGRESS: {
    id: 'IN_PROGRESS',
    level: 2,
    i18nKey: 'sqtm-core.entity.milestone.status.IN_PROGRESS',
    isBindableToProject: true,
    isBindableToObject: true,
    allowObjectCreateAndDelete: true,
    allowObjectModification: true
  },
  FINISHED: {
    id: 'FINISHED',
    level: 3,
    i18nKey: 'sqtm-core.entity.milestone.status.FINISHED',
    isBindableToProject: true,
    isBindableToObject: true,
    allowObjectCreateAndDelete: true,
    allowObjectModification: true
  },
  LOCKED: {
    id: 'LOCKED',
    level: 4,
    i18nKey: 'sqtm-core.entity.milestone.status.LOCKED',
    isBindableToProject: false,
    isBindableToObject: false,
    allowObjectCreateAndDelete: false,
    allowObjectModification: false
  }
};

export const MilestoneRange: LevelEnum<MilestoneRangeKeys> = {
  GLOBAL: {
    id: 'GLOBAL',
    level: 1,
    i18nKey: 'sqtm-core.entity.milestone.range.GLOBAL',
  },
  RESTRICTED: {
    id: 'RESTRICTED',
    level: 2,
    i18nKey: 'sqtm-core.entity.milestone.range.RESTRICTED',
  }
};

export const AutomationRequestStatus: LevelEnum<AutomationRequestStatusKeys> = {
  TRANSMITTED: {id: 'TRANSMITTED', level: 1, i18nKey: 'sqtm-core.entity.automation-request.status.TRANSMITTED'},
  AUTOMATION_IN_PROGRESS: {
    id: 'AUTOMATION_IN_PROGRESS',
    level: 2,
    i18nKey: 'sqtm-core.entity.automation-request.status.AUTOMATION_IN_PROGRESS'
  },
  SUSPENDED: {id: 'SUSPENDED', level: 3, i18nKey: 'sqtm-core.entity.automation-request.status.SUSPENDED'},
  REJECTED: {id: 'REJECTED', level: 4, i18nKey: 'sqtm-core.entity.automation-request.status.REJECTED'},
  AUTOMATED: {id: 'AUTOMATED', level: 5, i18nKey: 'sqtm-core.entity.automation-request.status.AUTOMATED'},
  READY_TO_TRANSMIT: {
    id: 'READY_TO_TRANSMIT',
    level: 6,
    i18nKey: 'sqtm-core.entity.automation-request.status.READY_TO_TRANSMIT'
  },
  WORK_IN_PROGRESS: {
    id: 'WORK_IN_PROGRESS',
    level: 7,
    i18nKey: 'sqtm-core.entity.automation-request.status.WORK_IN_PROGRESS'
  },
};

export const TestCaseExecutionMode: TestCaseExecutionModeLevelEnum<TestCaseExecutionModeKeys> = {
  AUTOMATED: {
    id: 'AUTOMATED',
    level: 1,
    i18nKey: 'sqtm-core.entity.test-case.execution-mode.AUTOMATED',
    icon: 'sqtm-core-test-case:automation'
  },
  MANUAL: {
    id: 'MANUAL',
    level: 2,
    i18nKey: 'sqtm-core.entity.test-case.execution-mode.MANUAL',
    icon: 'sqtm-core-test-case:manual_mode'
  },
  UNDEFINED: {
    id: 'UNDEFINED',
    level: 3,
    i18nKey: 'sqtm-core.entity.test-case.execution-mode.UNDEFINED',
    icon: ''
  },
};

export type ConclusivenessKeys =
  'SUCCESS'
  | 'FAILURE'
  | 'NON_CONCLUSIVE';

export const Conclusiveness: LevelEnum<ConclusivenessKeys> = {
  SUCCESS: {id: 'SUCCESS', level: 1, i18nKey: 'sqtm-core.entity.execution.status.SUCCESS'},
  FAILURE: {id: 'FAILURE', level: 2, i18nKey: 'sqtm-core.entity.execution.status.FAILURE'},
  NON_CONCLUSIVE: {id: 'NON_CONCLUSIVE', level: 3, i18nKey: 'sqtm-core.entity.execution.non-conclusive'},
};


export const ExecutionStatus: ExecutionStatusLevelEnum<ExecutionStatusKeys> = {
  READY: {
    id: 'READY',
    level: 1,
    i18nKey: 'sqtm-core.entity.execution.status.READY',
    color: '#a3b2b8',
    backgroundColor: 'rgba(163,178,184,0.2)',
    textColor: 'black',
    terminal: false,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status'
  },
  RUNNING: {
    id: 'RUNNING',
    level: 2,
    i18nKey: 'sqtm-core.entity.execution.status.RUNNING',
    color: '#0078ae',
    backgroundColor: 'rgba(0,120,174,0.2)',
    textColor: 'white',
    terminal: false,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status'
  },
  SUCCESS: {
    id: 'SUCCESS',
    level: 3,
    i18nKey: 'sqtm-core.entity.execution.status.SUCCESS',
    color: '#006f57',
    backgroundColor: 'rgba(0,111,87,0.2)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.SUCCESS,
    icon: 'sqtm-core-campaign:exec_status'
  },
  WARNING: {
    id: 'WARNING',
    level: 4,
    i18nKey: 'sqtm-core.entity.execution.status.WARNING',
    color: '#FF00FB',
    backgroundColor: 'rgba(255,0,251,0.2)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.SUCCESS,
    icon: 'sqtm-core-campaign:exec_status'
  },
  FAILURE: {
    id: 'FAILURE',
    level: 5,
    i18nKey: 'sqtm-core.entity.execution.status.FAILURE',
    color: '#cb1524',
    backgroundColor: 'rgba(203,21,36,0.2)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.FAILURE,
    icon: 'sqtm-core-campaign:exec_status'
  },
  BLOCKED: {
    id: 'BLOCKED',
    level: 6,
    i18nKey: 'sqtm-core.entity.execution.status.BLOCKED',
    color: '#ffcc00',
    backgroundColor: 'rgba(255,204,0,0.2)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status'
  },
  ERROR: {
    id: 'ERROR',
    level: 7,
    i18nKey: 'sqtm-core.entity.execution.status.ERROR',
    color: '#FF00FB',
    backgroundColor: 'rgba(255,0,251,0.2)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.FAILURE,
    icon: 'sqtm-core-campaign:exec_status'
  },
  NOT_RUN: {
    id: 'NOT_RUN',
    level: 8,
    i18nKey: 'sqtm-core.entity.execution.status.NOT_RUN',
    color: '#FF00FB',
    backgroundColor: 'rgba(255,0,251,0.2)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status'
  },
  UNTESTABLE: {
    id: 'UNTESTABLE',
    level: 9,
    i18nKey: 'sqtm-core.entity.execution.status.UNTESTABLE',
    color: '#3d3d3d',
    backgroundColor: 'rgba(61,61,61,0.2)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status'
  },
  NOT_FOUND: {
    id: 'NOT_FOUND',
    level: 10,
    i18nKey: 'sqtm-core.entity.execution.status.NOT_FOUND',
    color: '#FF00FB',
    backgroundColor: 'rgba(255,0,251,0.2)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.NON_CONCLUSIVE,
    icon: 'sqtm-core-campaign:exec_status'
  },
  SETTLED: {
    id: 'SETTLED',
    level: 11,
    i18nKey: 'sqtm-core.entity.execution.status.SETTLED',
    color: '#05bf71',
    backgroundColor: 'rgba(5,191,113,0.2)',
    textColor: 'white',
    terminal: true,
    conclusiveness: Conclusiveness.SUCCESS,
    icon: 'sqtm-core-campaign:exec_status'
  }
};

export const TerminalExecutionStatus: ExecutionStatusLevelEnumItem<ExecutionStatusKeys>[] =
  Object.entries(ExecutionStatus)
    .map(entry => entry[1])
    .filter((levelEnumItem: ExecutionStatusLevelEnumItem<ExecutionStatusKeys>) => !levelEnumItem.terminal);


export const TestCaseAutomatable: LevelEnum<TestCaseAutomatableKeys> = {
  M: {id: 'M', level: 1, i18nKey: 'sqtm-core.entity.test-case.automatable.M'},
  Y: {id: 'Y', level: 2, i18nKey: 'sqtm-core.entity.test-case.automatable.Y'},
  N: {id: 'N', level: 3, i18nKey: 'sqtm-core.entity.test-case.automatable.N'},
};

export const BddScriptLanguage: LevelEnum<BddScriptLanguageKeys> = {
  ENGLISH: {id: 'ENGLISH', level: 1, i18nKey: 'sqtm-core.administration-workspace.views.project.scripts-language.ENGLISH'},
  SPANISH: {id: 'SPANISH', level: 2, i18nKey: 'sqtm-core.administration-workspace.views.project.scripts-language.SPANISH'},
  GERMAN: {id: 'GERMAN', level: 3, i18nKey: 'sqtm-core.administration-workspace.views.project.scripts-language.GERMAN'},
  FRENCH: {id: 'FRENCH', level: 4, i18nKey: 'sqtm-core.administration-workspace.views.project.scripts-language.FRENCH'}
};

export const BddImplementationTechnology: LevelEnum<BddImplementationTechnologyKeys> = {
  CUCUMBER_5_PLUS: {id: 'CUCUMBER_5_PLUS', level: 1, i18nKey: 'sqtm-core.administration-workspace.views.project.automation.technology.CUCUMBER_5_PLUS'},
  CUCUMBER_4: {id: 'CUCUMBER_4', level: 2, i18nKey: 'sqtm-core.administration-workspace.views.project.automation.technology.CUCUMBER_4'},
  ROBOT: {id: 'ROBOT', level: 3, i18nKey: 'sqtm-core.administration-workspace.views.project.automation.technology.ROBOT'}
};

export const IterationStatus: LevelEnum<IterationStatusKeys> = {
  UNDEFINED: {
    id: 'UNDEFINED',
    level: 0,
    i18nKey: 'sqtm-core.entity.iteration.status.UNDEFINED',
  },
  PLANNED: {
    id: 'PLANNED',
    level: 1,
    i18nKey: 'sqtm-core.entity.iteration.status.PLANNED',
  },
  IN_PROGRESS: {
    id: 'IN_PROGRESS',
    level: 2,
    i18nKey: 'sqtm-core.entity.iteration.status.IN_PROGRESS',
  },
  FINISHED: {
    id: 'FINISHED',
    level: 3,
    i18nKey: 'sqtm-core.entity.iteration.status.FINISHED',
  },
  ARCHIVED: {
    id: 'ARCHIVED',
    level: 4,
    i18nKey: 'sqtm-core.entity.iteration.status.ARCHIVED',
  },
};

export type WorkspaceKeys = 'HOME' | 'TEST_CASE' | 'REQUIREMENT' | 'CAMPAIGN';

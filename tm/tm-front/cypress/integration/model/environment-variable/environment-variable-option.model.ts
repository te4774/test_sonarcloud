export class EnvironmentVariableOption {
  evId: number;
  label: string;
  code: string;
  position: number;
}

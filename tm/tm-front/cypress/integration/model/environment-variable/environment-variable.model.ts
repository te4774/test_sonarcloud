import {EnvironmentVariableOption} from './environment-variable-option.model';

export class EnvironmentVariable {
  id: number;
  name: string;
  code: string;
  inputType: EvInputType;
  boundToServer: boolean;
  options: EnvironmentVariableOption[] = [];
}

export class BoundEnvironmentVariable extends EnvironmentVariable {
  value: string;
}

export enum EvInputType {
  PLAIN_TEXT = 'PLAIN_TEXT',
  DROPDOWN_LIST = 'DROPDOWN_LIST'
}

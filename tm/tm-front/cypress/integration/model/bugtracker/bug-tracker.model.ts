export interface BugTracker {
  id: number;
  name: string;
  url: string;
  kind: string;
  authPolicy: AuthenticationPolicy;
  authProtocol: AuthenticationProtocol;
  iframeFriendly: boolean;
}

export interface BugTrackerReferentialDto extends BugTracker {
  useProjectPaths: boolean;
  projectHelpMessage: string | null;
}

export enum AuthenticationPolicy {
  USER = 'USER',
  APP_LEVEL = 'APP_LEVEL'
}

// fixme: this is a duplicate with ./third-party-server/authentication.model.ts
export enum AuthenticationProtocol {
  BASIC_AUTH = 'BASIC_AUTH',
  OAUTH_1A = 'OAUTH_1A',
  TOKEN_AUTH = 'TOKEN_AUTH',
}

export type BugTrackerMode = 'Automatic' | 'Manual';

import {Credentials} from '../third-party-server/credentials.model';
import {AuthConfiguration} from '../third-party-server/auth-configuration.model';
import {AuthenticationPolicy, AuthenticationProtocol} from './bug-tracker.model';

export interface AdminBugTrackerState {
  id: number;
  name: string;
  url: string;
  kind: string;
  authPolicy: AuthenticationPolicy;
  authProtocol: AuthenticationProtocol;
  iframeFriendly: boolean;
  bugTrackerKinds: string[];
  supportedAuthenticationProtocols: string[];
  authConfiguration?: AuthConfiguration;
  credentials?: Credentials;
}

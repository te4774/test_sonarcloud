import {IterationTestPlanItem} from './iteration-test-plan.item';
import {EntityModel} from '../entity.model';
import {TestPlanStatistics} from './test-plan-statistics';
import {Milestone} from '../milestone/milestone.model';
import {SimpleUser} from '../user/user.model';
import {NamedReference} from '../named-reference';
import {ConclusivenessStatusCount, ExecutionStatusCount, ScheduledIteration} from './campaign-model';
import {TestCaseImportanceKeys} from '../level-enums/level-enum';

export interface IterationModel extends EntityModel {
  name: string;
  description: string;
  reference: string;
  uuid: string;
  iterationStatus: string;
  createdOn: Date;
  createdBy: string;
  lastModifiedOn: Date;
  lastModifiedBy: string;
  testPlanStatistics: TestPlanStatistics;
  actualStartDate: Date;
  actualEndDate: Date;
  actualStartAuto: boolean;
  actualEndAuto: boolean;
  scheduledStartDate: Date;
  scheduledEndDate: Date;
  hasDatasets: boolean;
  executionStatusMap: Map<number, string>;
  itpi: IterationTestPlanItem[];
  milestones: Milestone[];
  users: SimpleUser[];
  testSuites: NamedReference[];
  iterationStatisticsBundle?: IterationStatisticsBundle;
}

export interface IterationProgressionStatistics {
  errors: string[];
  scheduledIteration: ScheduledIteration;
  cumulativeExecutionsPerDate: [string, number][];
}

export interface TestSuiteTestInventoryStatistics {
  testsuiteName: string;
  scheduledStart: string;
  scheduledEnd: string;
  nbTotal: number;
  nbExecuted: number;
  pcProgress: number;
  nbToExecute: number;
  pcSuccess: number;
  pcFailure: number;
  pcPrevProgress: number;
  nbPrevToExecute: number;
  statusStatistics: Partial<ExecutionStatusCount>;
  importanceStatistics: { [K in TestCaseImportanceKeys]: number };
}

export interface IterationStatisticsBundle {
  iterationProgressionStatistics: IterationProgressionStatistics;
  iterationTestCaseStatusStatistics: Partial<ExecutionStatusCount>;
  iterationTestCaseSuccessRateStatistics: { conclusiveness: { [K in TestCaseImportanceKeys]: ConclusivenessStatusCount } };
  iterationNonExecutedTestCaseImportanceStatistics: { [K in TestCaseImportanceKeys]: number };
  testsuiteTestInventoryStatisticsList: TestSuiteTestInventoryStatistics[];
}

export interface TestPlanResumeModel {
  iterationId?: number;
  testSuiteId?: number;
  testPlanItemId: number;
  executionId: number;
  hasNextTestCase: boolean;
  initialStepIndex?: number;
}

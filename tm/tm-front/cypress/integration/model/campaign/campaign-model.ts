import {EntityModel} from '../entity.model';
import {Milestone} from '../milestone/milestone.model';
import {TestPlanStatistics} from './test-plan-statistics';
import {SimpleUser} from '../user/user.model';
import {NamedReference} from '../named-reference';
import {ConclusivenessKeys, ExecutionStatusKeys, TestCaseImportanceKeys} from '../level-enums/level-enum';

export interface CampaignModel extends EntityModel {
  name: string;
  description: string;
  reference: string;
  campaignStatus: string;
  progressStatus: string;
  lastModifiedOn: Date;
  lastModifiedBy: string;
  createdOn: Date;
  createdBy: string;
  milestones: Milestone[];
  testPlanStatistics: TestPlanStatistics;
  nbIssues: number;
  hasDatasets: boolean;
  users: SimpleUser[];
  testSuites: NamedReference[];
  campaignStatisticsBundle?: CampaignStatisticsBundle;
}

export interface IterationPlanning {
  id: number;
  name: string;
  scheduledStartDate: Date;
  scheduledEndDate: Date;
}

export interface ScheduledIteration {
  id: number;
  name: string;
  testplanCount: number;
  scheduledStart: string;
  scheduledEnd: string;
  cumulativeTestsByDate: [string, number][];
}

export interface CampaignProgressionStatistics {
  cumulativeExecutionsPerDate: [string, number][];
  scheduledIterations: ScheduledIteration[];
  errors: string[];
}

export type ExecutionStatusCount = { [K in ExecutionStatusKeys]: number};

export type ConclusivenessStatusCount = { [K in ConclusivenessKeys]: number };

export interface IterationTestInventoryStatistics {
  iterationName: string;
  statistics: Partial<ExecutionStatusCount>;
}

export interface CampaignStatisticsBundle {
  campaignProgressionStatistics: CampaignProgressionStatistics;
  campaignTestCaseStatusStatistics: Partial<ExecutionStatusCount>;
  campaignTestCaseSuccessRateStatistics: { conclusiveness: { [K in TestCaseImportanceKeys]: ConclusivenessStatusCount } };
  campaignNonExecutedTestCaseImportanceStatistics: { [K in TestCaseImportanceKeys]: number };
  iterationTestInventoryStatistics: IterationTestInventoryStatistics[];
}

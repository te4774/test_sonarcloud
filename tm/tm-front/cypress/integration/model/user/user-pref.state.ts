export interface UserPrefState {
  homeWorkspaceContent: 'dashboard' | 'default',
  requirementWorkspaceContent: 'dashboard' | 'default',
  testCaseWorkspaceContent: 'dashboard' | 'default',
  campaignWorkspaceContent: 'dashboard' | 'default',

  homeFavoriteDashboard: number,
  requirementFavoriteDashboard: number,
  testCaseFavoriteDashboard: number,
  campaignFavoriteDashboard: number,

  bugtrackerMode: 'automatic' | 'manual'

}

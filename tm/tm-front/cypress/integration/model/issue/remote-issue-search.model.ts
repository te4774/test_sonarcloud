import {Field} from './remote-issue.model';

export interface RemoteIssueSearchForm {
  fields: Field[];
}

export interface RemoteIssueSearchTerms { [searchTermId: string]: any; }

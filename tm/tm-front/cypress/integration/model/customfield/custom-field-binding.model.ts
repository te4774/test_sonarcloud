import {BindableEntity} from '../bindable-entity.model';

export class CustomFieldBinding {
  id: number;
  customFieldId: number;
  bindableEntity: BindableEntity;
  renderingLocations: RenderingLocation[];
  boundProjectId: number;
  position: number;
}

export enum RenderingLocation {
  LINK_TABLE = 'LINK_TABLE',
  TEST_PLAN = 'TEST_PLAN',
  EXECUTION = 'EXECUTION',
  SEARCH_CRITERIA = 'SEARCH_CRITERIA',
  STEP_TABLE = 'STEP_TABLE'
}

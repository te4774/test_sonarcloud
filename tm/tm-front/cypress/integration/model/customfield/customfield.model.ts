import {CustomFieldOption} from './custom-field-option.model';

export class CustomField {
  id: number;
  code: string;
  label: string;
  name: string;
  defaultValue?: string;
  largeDefaultValue?: string;
  numericDefaultValue?: number;
  inputType: InputType;
  optional: boolean;
  options: CustomFieldOption[] = [];
}

export class CustomFieldData extends CustomField {
  cfvId?: number;
  value: string | string[];
}

// Types used in creation process of entities that hold CustomFieldValues
// Here P is the CUF id, not the CFV id as the CFV doesn't exist at this stage (entity is not created yet)
export interface RawValueMap {
  [P: number]: string | string[];
}

export enum InputType {
  PLAIN_TEXT = 'PLAIN_TEXT',
  CHECKBOX = 'CHECKBOX',
  DROPDOWN_LIST = 'DROPDOWN_LIST',
  RICH_TEXT = 'RICH_TEXT',
  DATE_PICKER = 'DATE_PICKER',
  TAG = 'TAG',
  NUMERIC = 'NUMERIC'
}

export interface AdministrationStatistics {
  projectsNumber: number;
  usersNumber: number;
  requirementsNumber: number;
  testCasesNumber: number;
  campaignsNumber: number;
  iterationsNumber: number;
  executionsNumber: number;
  requirementIndexingDate: Date;
  testcaseIndexingDate: Date;
  campaignIndexingDate: Date;
  databaseSize: number;
}

export interface SystemViewModel {
  statistics: AdministrationStatistics;
  appVersion: string;
  plugins: string[];
  whiteList: string;
  uploadSizeLimit: string;
  importSizeLimit: string;
  callbackUrl: string;
  stackTracePanelIsVisible: boolean;
  stackTraceFeatureIsEnabled: boolean;
  autoconnectOnConnection: boolean;
  caseInsensitiveLogin: boolean;
  caseInsensitiveActions: boolean;
  duplicateLogins: string[];
  duplicateActions: string[];
  welcomeMessage: string;
  loginMessage: string;
  logFiles: string[];
  licenseInfo: LicenseInfo;
  currentActiveUsersCount: number
}

export interface LicenseInfo {
  expirationDate: Date;
  maxUsers: number;
}

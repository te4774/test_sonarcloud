export type Identifier = string | number;

export abstract class DataRow {
  id: Identifier;
  readonly type;
  children?: Identifier[] = [];
  parentRowId?: Identifier;
  state?: DataRowOpenState = DataRowOpenState.closed;
  data: { [K: string]: any };
  abstract readonly allowedChildren: any[];
  abstract readonly allowMoves: boolean;
  projectId: number;
}

export const placeholderDataRowId = 'SQTM-CORE-EMPTY-GRID-MESSAGE';
export const placeholderDataRowType = 'SqtmCorePlaceholderDataRow';
export const dndPlaceholderDataRowType = 'SqtmCoreDndPlaceholderDataRow';


export interface GridResponse {
  count?: number;
  idAttribute?: string;
  dataRows: DataRow[];
}

export interface GridRequest {
  page: number;
  size: number;
  sort?: any[];
}

export enum DataRowOpenState {
  open = 'open',
  closed = 'closed',
  leaf = 'leaf',
}


export enum SquashTmDataRowType {
  Generic = 'Generic',
  // Tree node entities
  TestCaseLibrary = 'TestCaseLibrary',
  TestCaseFolder = 'TestCaseFolder',
  TestCase = 'TestCase',
  RequirementLibrary = 'RequirementLibrary',
  RequirementFolder = 'RequirementFolder',
  HighLevelRequirement = 'HighLevelRequirement',
  Requirement = 'Requirement',
  CampaignLibrary = 'CampaignLibrary',
  CampaignFolder = 'CampaignFolder',
  Campaign = 'Campaign',
  Iteration = 'Iteration',
  TestSuite = 'TestSuite',
  // Non tree node entities
  IterationTestPlanItem = 'IterationTestPlanItem',
  CampaignTestPlanItem = 'CampaignTestPlanItem',
  TAProject = 'TAProject',
  TAFolder = 'TAFolder',
  TATest = 'TATest',
  CustomReportLibrary = 'CustomReportLibrary',
  CustomReportFolder = 'CustomReportFolder',
  ChartDefinition = 'ChartDefinition',
  ReportDefinition = 'ReportDefinition',
  CustomReportDashboard = 'CustomReportDashboard',
  CustomReportCustomExport = 'CustomReportCustomExport',
}

export class Campaign extends DataRow {
  readonly type = SquashTmDataRowType.Campaign;
  allowMoves = true;
  allowedChildren = [];
}

export class Iteration extends DataRow {
  readonly type = SquashTmDataRowType.Iteration;
  allowMoves = true;
  allowedChildren = [];
}

export type GridViewportName = 'leftViewport' | 'mainViewport' | 'rightViewport'
  | 'headerLeftViewport' | 'headerMainViewport' | 'headerRightViewport';

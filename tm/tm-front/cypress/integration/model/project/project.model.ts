import {Bindings} from '../bindable-entity.model';
import {Option} from '../option.model';
import {ProjectPermissions} from '../permissions/permissions.model';
import {BugTrackerBinding} from '../bugtracker/bug-tracker.binding';
import {MilestoneBinding} from '../milestone/milestone-binding.model';
import {ExecutionStatusKeys} from '../level-enums/level-enum';
import {AttachmentListModel} from '../attachment/attachment-list.model';
import {BugTracker} from '../bugtracker/bug-tracker.model';
import {InfoList} from '../infolist/infolist.model';
import {ScmServer} from '../scm-server/scm-server.model';
import {TestAutomationServer} from '../test-automation/test-automation-server.model';
import {TestAutomationProject} from '../test-automation/test-automation-project.model';
import {MilestoneAdminProjectView} from '../milestone/milestone-admin-project-view.model';
import {ProjectPlugin} from '../plugin/project-plugin';

export class Project {
  id: number;
  uri: string;
  name: string;
  label: string;
  testCaseNatureId: number;
  testCaseTypeId: number;
  requirementCategoryId: number;
  allowAutomationWorkflow: boolean;
  customFieldBindings: Bindings;
  permissions: ProjectPermissions;
  bugTrackerBinding: BugTrackerBinding;
  milestoneBindings: MilestoneBinding[];
  taServerId: number;
  automationWorkflowType: string;
  disabledExecutionStatus: ExecutionStatusKeys[];
  description: string;
  keywords: Option[];
  bddScriptLanguage: string;
  bddImplementationTechnology: string;
  allowTcModifDuringExec: boolean;
  activatedPlugins: ActivatedPluginsByWorkspace;
  automatedSuitesLifetime: number;
}

export class ProjectView extends Project {
  availableBugtrackers: BugTracker[];
  bugtrackerProjectNames: string[];
  hasData: boolean;
  allowedStatuses: AllowedStatusesModel;
  statusesInUse: StatusesInUseModel;
  useTreeStructureInScmRepo: boolean;
  scmRepositoryId?: number;
  infoLists: InfoList[];
  templateLinkedToProjects: boolean;
  partyProjectPermissions: PartyProjectPermission[];
  availableScmServers: ScmServer[];
  availableTestAutomationServers: TestAutomationServer[];
  boundMilestonesInformation: MilestoneAdminProjectView[];
  boundTestAutomationProjects: TestAutomationProject[];
  attachmentList: AttachmentListModel;
  createdOn: Date;
  createdBy: string;
  lastModifiedOn: Date;
  lastModifiedBy: string;
  linkedTemplate: string;
  linkedTemplateId: number;
  template: boolean;
  availablePlugins: ProjectPlugin[];
}

export interface AllowedStatusesModel {
  UNTESTABLE: boolean;
  SETTLED: boolean;
}

export interface StatusesInUseModel {
  UNTESTABLE: boolean;
  SETTLED: boolean;
}

export interface PartyProjectPermission {
  projectId: number;
  partyName: string;
  partyId: number;
  permissionGroup: PermissionGroup;
  team: boolean;
}

export interface PermissionGroup {
  id: number;
  qualifiedName: string;
  simpleName: string;
}

export type ActivatedPluginsByWorkspace = { [id in WorkspaceTypeForPlugins]: string[]};

export enum WorkspaceTypeForPlugins {
  TEST_CASE_WORKSPACE = 'TEST_CASE_WORKSPACE',
  REQUIREMENT_WORKSPACE= 'REQUIREMENT_WORKSPACE',
  CAMPAIGN_WORKSPACE = 'CAMPAIGN_WORKSPACE',
}


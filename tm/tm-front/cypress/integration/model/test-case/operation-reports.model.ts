import {TestCaseImportanceKeys} from '../level-enums/level-enum';
import {TestCaseParameterOperationReport} from './change-dataset-param-operation.model';
import {TestStepModel} from './test-step.model';

export class PasteTestStepOperationReport {
  testSteps: TestStepModel[];
  testCaseImportance: TestCaseImportanceKeys;
  operationReport: TestCaseParameterOperationReport;
}

export class AddTestStepOperationReport {
  testStep: TestStepModel;
  operationReport: TestCaseParameterOperationReport;
}

export class DeleteTestStepOperationReport {
  testStepsToDelete: number[];
  operationReport: TestCaseParameterOperationReport;
}

export class TestStepActionWordOperationReport {
  action: string;
  styledAction: string;
  paramOperationReport: TestCaseParameterOperationReport;
}

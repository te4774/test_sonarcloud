import {EntityModel} from '../../entity.model';
import {TestCaseStatistics} from '../test-case-statistics.model';
import {CustomDashboardModel} from '../../custom-report/custom-dashboard.model';

export interface TestCaseFolderModel extends EntityModel {
  name: string;
  description: string;
  statistics: TestCaseStatistics;
  dashboard: CustomDashboardModel;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}

import {RemoteAutomationRequestExtender} from './remote-automation-request-extender.model';

export class AutomationRequest {
  id: number;
  requestStatus: string;
  priority: number;
  transmittedOn: Date;
  extender: RemoteAutomationRequestExtender;
}

export class XlsReport {
  templateOk: ImportLog;
  importFormatFailure: ImportFormatFailure;
}

export interface ImportFormatFailure {
  missingMandatoryColumns: string[];
  duplicateColumns: string[];
  actionValidationError: any;
}

export class ImportLog {
  testCaseSuccesses: number;
  testCaseWarnings: number;
  testCaseFailures: number;
  testStepSuccesses: number;
  testStepWarnings: number;
  testStepFailures: number;
  parameterSuccesses: number;
  parameterWarnings: number;
  parameterFailures: number;
  datasetSuccesses: number;
  datasetWarnings: number;
  datasetFailures: number;
  coverageSuccesses: number;
  coverageWarnings: number;
  coverageFailures: number;
  reportUrl: string;
}

export class ImportSummary {
  total: number;
  success: number;
  renamed: number;
  modified: number;
  failures: number;
  rejected: number;
  milestoneFailures: number;
  milestoneNotActivatedFailures: number;
}

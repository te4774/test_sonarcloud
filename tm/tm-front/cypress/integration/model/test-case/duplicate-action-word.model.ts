export interface DuplicateActionWord {
  projectName: string;
  actionWordId: number;
}

import {Credentials} from '../third-party-server/credentials.model';
import {AuthenticationProtocol} from '../third-party-server/authentication.model';
import {BoundEnvironmentVariable} from '../environment-variable/environment-variable.model';

export class TestAutomationServer {
  id: number;
  baseUrl: string;
  kind: TestAutomationServerKind;
  name: string;
  description: string;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  manualSlaveSelection: boolean;
  authProtocol: AuthenticationProtocol;
  supportsAutomatedExecutionEnvironments: boolean;
  environmentTags: string[];
  boundEnvironmentVariables?: BoundEnvironmentVariable[];
  availableEnvironments: AutomatedExecutionEnvironment[];
  availableEnvironmentTags: string[];
  observerUrl?: string;
}

export class AdminTestAutomationServer extends TestAutomationServer {
  credentials: Credentials;
  supportedAuthenticationProtocols: AuthenticationProtocol[];
}

export enum TestAutomationServerKind {
  jenkins = 'jenkins',
  squashAutom = 'squashAutom',
}

export interface AutomatedExecutionEnvironment {
  name: string;
  tags: string[];
}

import {ExecutionStatusKeys} from '../level-enums/level-enum';

export interface AutomatedSuiteOverview {
  suiteId: string;
  executions: AutomatedExecutionOverview[];
  items: AutomatedItemOverview[];
  percentage: number;
}

export interface AutomatedExecutionOverview {
  id: number;
  name: string;
  status: ExecutionStatusKeys,
  node: string;
  automatedProject: string;
}

export interface AutomatedItemOverview {
  id: number;
  name: string;
  automatedServerName: string;
}

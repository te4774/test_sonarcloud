import {EntityReference} from '../entity.model';

export interface AutomatedSuitePreview {
  manualServerSelection: boolean;
  specification: AutomatedSuiteCreationSpecification;
  projects: TestAutomationProjectPreview[];
  squashAutomProjects: SquashAutomProjectPreview[];
}

export interface AutomatedSuiteCreationSpecification {
  context: EntityReference;
  testPlanSubsetIds: number[];
}

export interface TestAutomationProjectPreview {
  projectId: number;
  label: string;
  server: string;
  nodes: string[];
  testCount: number;
  testList: string[];
}

export interface SquashAutomProjectPreview {
  projectId: number;
  projectName: string;
  serverId: number;
  serverName: string;
  testCases: TestCaseReferenceAndName[];
}

export interface TestCaseReferenceAndName {
  reference: string;
  name: string;
}

export interface ProjectPlugin {
  index: number;
  id: string;
  enabled: boolean;
  type: string;
  name: string;
  status: string;
  configUrl: string;
  hasValidConfiguration: boolean;
}

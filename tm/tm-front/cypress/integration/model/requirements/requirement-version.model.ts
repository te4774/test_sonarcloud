import {EntityModel} from '../entity.model';
import {Milestone} from '../milestone/milestone.model';
import {VerifyingTestCase} from './verifying-test-case';
import {RequirementVersionLink} from './requirement-version.link';
import {RequirementVersionStatsBundle} from './requirement-version-stats-bundle.model';
import {RequirementCriticalityKeys, RequirementStatusKeys} from '../level-enums/level-enum';

export interface RequirementVersionModel extends EntityModel {
  name: string;
  reference: string;
  versionNumber: number;
  category: number;
  criticality: string;
  status: string;
  createdBy: string;
  createdOn: Date;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  milestones: Milestone[];
  description: string;
  requirementId: number;
  bindableMilestones: Milestone[];
  verifyingTestCases: VerifyingTestCase[];
  requirementVersionLinks: RequirementVersionLink[];
  requirementStats: RequirementVersionStatsBundle;
  nbIssues: number;
  highLevelRequirement: boolean;
  lowLevelRequirements?: LinkedLowLevelRequirement[];
  linkedHighLevelRequirement?: LinkedHighLevelRequirement;
  remoteReqPerimeterStatus: RemoteRequirementPerimeterStatus;
  childOfRequirement: boolean;
}

/**
 * This interface describe a high level req linked into a low level req
 */
export interface LinkedHighLevelRequirement {
  requirementId: number;
  requirementVersionId: number;
  name: string;
  reference: string;
}

export type RemoteRequirementPerimeterStatus =
  'UNKNOWN' |
  'IN_CURRENT_PERIMETER' |
  'OUT_OF_CURRENT_PERIMETER'|
  'NOT_FOUND';

/**
 * This interface describe a low level req inside a high level req model.
 * It is the equivalent of RequirementVersionLink inside a HighLevelReq
 */
export interface LinkedLowLevelRequirement {
  requirementId: number;
  requirementVersionId: number;
  name: string;
  reference: string;
  childOfRequirement: boolean;
  projectName: string;
  milestoneLabels: string;
  milestoneMinDate: Date;
  milestoneMaxDate: Date;
  versionNumber: number;
  criticality: RequirementCriticalityKeys;
  requirementStatus: RequirementStatusKeys;
}

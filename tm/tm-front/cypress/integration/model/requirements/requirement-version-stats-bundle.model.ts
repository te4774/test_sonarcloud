import {RequirementVersionStats} from './requirement-version-stats.model';

export interface RequirementVersionStatsBundle {
  currentVersion: RequirementVersionStats;
  children: RequirementVersionStats;
  total: RequirementVersionStats;
  haveChildren: boolean;
  nonObsoleteDescendantsCount: number;
  coveredDescendantsCount: number;
}

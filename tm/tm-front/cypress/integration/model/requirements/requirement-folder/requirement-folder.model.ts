import {EntityModel} from '../../entity.model';
import {RequirementStatistics} from '../requirement-statistics.model';
import {CustomDashboardModel} from '../../custom-report/custom-dashboard.model';

export interface RequirementFolderModel extends EntityModel {
  name: string;
  description: string;
  statistics: RequirementStatistics;
  dashboard: CustomDashboardModel;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  extendHighLvlReqScope: boolean;
}

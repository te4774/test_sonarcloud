import {AttachmentModel} from './attachment.model';

export interface UploadSummary {

  attachmentDto: AttachmentModel;
  status: string;
  iStatus: number;

}

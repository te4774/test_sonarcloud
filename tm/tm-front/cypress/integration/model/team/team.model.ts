import {ProjectPermission} from '../user/user.model';

export interface Team {
  id: number;
  name: string;
  description: string;
  createdOn: Date;
  createdBy: string;
  lastModifiedBy: string;
  lastModifiedOn: Date;
  projectPermissions: ProjectPermission[];
  members: Member[];
}

export interface Member {
  partyId: number;
  firstName: string;
  lastName: string;
  login: string;
  fullName: string;
}



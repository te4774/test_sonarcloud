import {GridViewportName, Identifier} from '../model/grids/data-row.type';
import {HTTP_RESPONSE_STATUS, HttpMockBuilder} from './mocks/request-mock';
import {TextFilterWidgetElement} from '../page-objects/elements/filters/text-filter-widget.element';
import {selectByDataTestComponentId} from './basic-selectors';
import Chainable = Cypress.Chainable;
import PositionType = Cypress.PositionType;

export class GridSelectorBuilder {

  constructor(private readonly gridId: string) {
  }

  viewport(viewportName: GridViewportName = 'mainViewport'): GridViewportSelectorBuilder {
    return new GridViewportSelectorBuilder(this.gridId, viewportName);
  }

  headerRow(viewportName: GridViewportName = 'headerMainViewport'): GridHeaderRowElement {
    return new GridHeaderRowElement(this.gridId, viewportName);
  }

  leftViewport(viewportName: GridViewportName = 'leftViewport'): GridViewportSelectorBuilder {
    return new GridViewportSelectorBuilder(this.gridId, viewportName);
  }

  rightViewport(viewportName: GridViewportName = 'rightViewport'): GridViewportSelectorBuilder {
    return new GridViewportSelectorBuilder(this.gridId, viewportName);
  }

  buildSelector(): string {
    return buildRootGridSelector(this.gridId);
  }
}

export class GridViewportSelectorBuilder {
  constructor(private readonly gridId: string, private readonly viewportName: GridViewportName) {

  }

  row(rowId: Identifier): GridRowElement {
    return new GridRowElement(this.gridId, this.viewportName, rowId);
  }

  findRows(): Chainable<any> {
    return cy
      .get(buildRootGridSelector(this.gridId))
      .should('exist')
      .find(buildViewportSelector(this.viewportName))
      .should('exist')
      .find(buildRowsSelector());

  }

  headerRow(): GridHeaderRowElement {
    return new GridHeaderRowElement(this.gridId, this.viewportName);
  }

  // buildSelector(): string {
  //   return `
  //   ${this.gridId}
  //   [data-test-viewport-name="${this.viewportName}"]
  //   `;
  // }
}

export class GridRowElement {
  constructor(private readonly gridId: string, private readonly viewportName: GridViewportName, private readonly rowId: Identifier) {

  }

  // select() {
  //   // Take the first selectable as there could be many (e.g. projects grid)
  //   this.findRow().first().click();
  // }
  //
  // toggle() {
  //   cy.get('body').type('{ctrl}', {release: false});
  //   this.findRow().first().click();
  //   cy.get('body').type('{ctrl}');
  // }

  assertExist() {
    this.findRow()
      .should('exist');
  }

  assertNotExist() {
    cy
      .get(buildRootGridSelector(this.gridId))
      .should('exist')
      .find(buildViewportSelector(this.viewportName))
      .should('exist')
      .find(buildRowSelector(this.rowId))
      .should('not.exist');
  }

  assertIsSelected() {
    this.findRow()
      .should('exist')
      .should('have.class', 'selected');
  }

  assertIsNotSelected() {
    this.findRow()
      .should('exist')
      .should('not.have.class', 'selected');
  }

  assertIsDndTarget() {
    this.findRow()
      .should('exist')
      .should('have.attr', 'data-test-dnd-container', 'true');
  }

  assertIsNotDndTarget() {
    this.findRow()
      .should('exist')
      .should('have.attr', 'data-test-dnd-container', 'false');
  }

  assertHasParent(expectedParent: Identifier) {
    this.findRow()
      .should('exist')
      .should('have.attr', 'data-test-parent-id', expectedParent);
  }

  cell(cell: string): GridCellSelectorBuilder {
    return new GridCellSelectorBuilder(this.gridId, this.viewportName, this.rowId, cell);
  }

  findRow() {
    return doFindRow(this.gridId, this.viewportName, this.rowId);
  }

  assertDisabled() {
    this.findRow()
      .should('exist')
      .should('have.class', 'disabled');
  }
}

export class GridHeaderRowElement {

  ROW_ID = 'sqtm-core-grid-header-row';

  constructor(private readonly gridId: string, private readonly viewPortName: GridViewportName) {
  }

  findCellId(cellId: string,
             content: string): Chainable {
    return cy.get(this.ROW_ID)
      .contains('span', content)
      .closest('sqtm-core-grid-header-row')
      .find('[data-test-cell-id]')
      .invoke('attr', 'data-test-cell-id');
  }

  cell(cell: string): GridCellSelectorBuilder {
    return new GridCellSelectorBuilder(this.gridId, this.viewPortName, this.ROW_ID, cell);
  }

  header(cell: string): HeaderCellSelectorBuilder {
    return new HeaderCellSelectorBuilder(this.gridId, this.viewPortName, this.ROW_ID, cell);
  }
}

export class HeaderCellSelectorBuilder {
  constructor(
    private readonly gridId: string,
    private readonly viewPortName: GridViewportName,
    private readonly rowId: Identifier,
    private readonly cellId: string) {

  }

  openTextFilter(): TextFilterWidgetElement {
    this.findFilterIcon().should('be.visible');
    // click void as we can have some tooltip visible that prevent correct opening of the widget
    cy.clickVoid();
    this.findFilterIcon().click();
    return new TextFilterWidgetElement();
  }

  private findFilterIcon() {
    return this.findCell().find(selectByDataTestComponentId('filter-icon'));
  }

  private findCell(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }
}

export class GridCellSelectorBuilder {
  constructor(
    private readonly gridId: string,
    private readonly viewPortName: GridViewportName,
    private readonly rowId: Identifier,
    private readonly cellId: string) {

  }

  textRenderer() {
    return new TextRendererSelectorBuilder(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  treeNodeRenderer() {
    return new TreeNodeRendererSelectorBuilder(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  linkRenderer() {
    return new LinkRendererSelectorBuilder(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  iconRenderer() {
    return new IconRendererSelectorBuilder(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  indexRenderer() {
    return new IndexSelectorBuilder(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  selectRenderer() {
    return new SelectRendererSelectorBuilder(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  checkBoxRender() {
    return new CheckBoxRendererSelectorBuilder(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  radioNgZorroRenderer() {
    return new RadioRendererNgZorroSelectorBuilder(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  radioInputRenderer() {
    return new RadioRendererInputSelectorBuilder(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  openCloseRenderer() {
    return new OpenCloseRendererSelectorBuilder(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  assertExist() {
    this.findCell().should('exist');
  }

  assertNotExist() {
    cy
      .get(buildRootGridSelector(this.gridId))
      .should('exist')
      .find(buildViewportSelector(this.viewPortName))
      .should('exist')
      .find(buildRowSelector(this.rowId))
      .should('exist')
      .find(buildCellSelector(this.cellId))
      .should('not.exist');
  }


  findCellTextSpan(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId)
      .find('span');
  }

  findCell(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  // buildSelector(): string {
  //   return `
  //   ${this.rootSelector}
  //   [data-test-cell-id="${this.cellId}"]
  //   `;
  // }
}

export class TextRendererSelectorBuilder {
  constructor(
    private readonly gridId: string,
    private readonly viewPortName: GridViewportName,
    private readonly rowId: Identifier,
    private readonly cellId: string) {

  }

  assertContainText(expected: string) {
    this.findCellTextSpan().should('contain', expected);
  }

  editText(newValue: string, url?: string) {
    if (url) {
      this.editTextError(newValue, url, null);
    } else {
      this.activateEditMode();
      this.typeValue(newValue);
      this.confirm();
    }
  }

  editTextError(newValue: string, url: string, errorResponse?) {
    this.activateEditMode();
    this.typeValue(newValue);
    const mock = this.buildMock(errorResponse, url, null);
    this.confirm();
    mock.wait();
  }

  editTextResponse(newValue: string, url: string, response) {
    this.activateEditMode();
    this.typeValue(newValue);
    const mock = this.buildMock(null, url, response);
    this.confirm();
    mock.wait();
  }

  private buildMock(errorResponse, url: string, response) {
    let mock;
    if (errorResponse) {
      mock = new HttpMockBuilder(url).post().status(HTTP_RESPONSE_STATUS.PRECONDITION_FAIL).responseBody(errorResponse).build();
    } else if (response) {
      mock = new HttpMockBuilder(url).post().responseBody(response).build();
    } else {
      mock = new HttpMockBuilder(url).post().build();
    }
    return mock;
  }

  private confirm() {
    this.findTxtInput().type('{enter}');
  }

  private typeValue(newValue: string) {
    this.findCell().find('input')
      .clear()
      .type(newValue);
  }

  private activateEditMode() {
    this.findCellTextSpan().trigger('click');
  }

  assertIsEditable() {
    this.activateEditMode();
    this.findTxtInput().should('exist');
    this.deactivateEditMode();
  }

  assertIsNotEditable() {
    this.activateEditMode();
    this.findTxtInput().should('not.exist');
  }

  assertDisabled() {
    this.findCellTextSpan().should('have.class', 'disabled-row');
  }

  assertNotDisabled() {
    this.findCellTextSpan().should('not.have.class', 'disabled-row');
  }

  assertErrorDialogExist() {
    cy.get(`[data-test-dialog-id=alert]`)
      .should('exist');
  }

  assertErrorDialogContains(expectedMessage: string) {
    cy.get(`[data-test-dialog-id=alert]`)
      .should('exist')
      .should('contain.text', expectedMessage);
  }

  findCell(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  findCellTextSpan(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId)
      .find('span');
  }

  findTxtInput() {
    return this.findCell().find('input');
  }

  private deactivateEditMode() {
    this.findTxtInput().type('{escape}');
  }
}

export class LinkRendererSelectorBuilder {
  constructor(private readonly gridId: string,
              private readonly viewPortName: GridViewportName,
              private readonly rowId: Identifier,
              private readonly cellId: string) {

  }

  assertContainText(expected: string) {
    this.findCellLink().should('contain', expected);
  }

  findCellLink(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId)
      .find('a');
  }
}

export class IconRendererSelectorBuilder {
  constructor(private readonly gridId: string,
              private readonly viewPortName: GridViewportName,
              private readonly rowId: Identifier,
              private readonly cellId: string) {

  }

  assertContainIcon(className: string) {
    this.findCellIcon().should('have.class', className);
  }

  assertIsVisible() {
    this.findCellIcon().should('be.visible');
  }

  assertNotExist(): void {
    this.findCellIcon().should('not.exist');
  }

  click() {
    this.findCellIcon().click();
  }

  private findCellIcon(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId)
      .find('i');
  }
}

export class IndexSelectorBuilder {
  constructor(private readonly gridId: string,
              private readonly viewPortName: GridViewportName,
              private readonly rowId: Identifier,
              private readonly cellId: string) {

  }

  select() {
    this.findIndex().click();
  }

  toggle() {
    cy.get('body').type('{ctrl}', {release: false});
    this.findIndex().click({force: true});
    cy.get('body').type('{ctrl}');
  }

  clickWithShift() {
    cy.get('body').type('{shift}', {release: false});
    this.findIndex().click();
    cy.get('body').type('{shift}');
  }

  private findIndex(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId)
      .find('span');
  }
}

export class TreeNodeRendererSelectorBuilder {
  constructor(private readonly gridId: string,
              private readonly viewPortName: GridViewportName,
              private readonly rowId: Identifier,
              private readonly cellId: string) {

  }

  toggle() {
    this.findFoldDivSelector().click();
  }

  beginDragAndDrop() {
    doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId)
      .find('div[data-test-grid-selectable="true"]')
      .trigger('mousedown', {button: 0, buttons: 1})
      .trigger('mousemove', -20, -20, {force: true})
      .trigger('mousemove', 50, 50, {force: true});
  }

  assertContainText(expected: string) {
    this.findCellTextSpan().should('contain', expected);
  }

  assertIsOpened() {
    this.findFoldIconSelector().should('have.class', 'anticon-minus');
  }

  getFoldClass(): Chainable {
    return this.findFoldIconSelector().invoke('attr', 'class');
  }


  assertIsClosed() {
    this.findFoldIconSelector().should('have.class', 'anticon-plus');
  }

  assertIsLeaf() {
    this.findFoldDivSelector().should('not.exist');
  }

  assertIsSelected() {
    this.findCell().find('div.selected').should('exist');
  }

  assertIsNotSelected() {
    this.findCell().find('div.selected').should('not.exist');
  }

  dragOverCenter() {
    const position = 'center';
    this.dragOver(position);
  }

  dragOverTopPart() {
    const position = 'top';
    this.dragOver(position);
  }

  dragOverBottomPart() {
    const position = 'bottom';
    this.dragOver(position);
  }

  drop() {
    this.findCell()
      .find('div[data-test-grid-selectable="true"]')
      .trigger('mouseup', {force: true});
  }

  assertIsHighLevelRequirement() {
    return this
        .findCell()
        .find('div')
        .should('have.class', 'high-level-req-name');
  }

  private dragOver(position: PositionType) {
    this
      .findCell()
      .find('div[data-test-grid-selectable="true"]')
      .trigger('mousemove', position, {force: true});
  }

  private findCell(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  private findCellTextSpan(): Chainable<any> {
    return this
      .findCell()
      .find('span');
  }

  private findFoldDivSelector(): Chainable<any> {
    return this
      .findCell()
      .find('[data-test-row-toggle=true]');
  }

  private findFoldIconSelector(): Chainable<any> {
    return this
      .findFoldDivSelector()
      .find('i');
  }

  findRequirementFolderSynchronizationStatusIcon() {
    return this
      .findCell()
      .find('[data-test-icon-id="synchronization-status"]');
  }

  findRemoteReqPerimeterStatusIcon() {
    return this
      .findCell()
      .find('[data-test-icon-id="remote-req-perimeter-status"]');
  }


  // buildTextSelector(): string {
  //   return `
  //   ${this.rootSelector}
  //   span
  //   `;
  // }
  //
  // buildFoldDivSelector(): string {
  //   return `
  //   ${this.rootSelector}
  //   [data-test-row-toggle=true]
  //   `;
  // }
  //
  // buildFoldIconSelector(): string {
  //   return `
  //   ${this.rootSelector}
  //   [data-test-row-toggle=true]
  //   i
  //   `;
  // }
  assertIsNotSelectable() {
    this
      .findCell()
      .should('exist')
      .find('div[data-test-grid-selectable="true"]')
      .should('not.exist');
  }
}

export class SelectRendererSelectorBuilder {
  constructor(private readonly gridId: string,
              private readonly viewPortName: GridViewportName,
              private readonly rowId: Identifier,
              private readonly cellId: string) {

  }

  assertContainText(expected: string) {
    this.findCellTextSpan().should('contain', expected);
  }

  changeValue(itemId: string, url?: string) {
    this.findCellTextSpan().trigger('click');
    if (url) {
      const mock = new HttpMockBuilder(url).post().build();
      cy.get(`[data-test-item-id=${itemId}]`).trigger('click');
      mock.wait();
    } else {
      cy.get(`[data-test-item-id=${itemId}]`).trigger('click');
    }
  }

  private findCell(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  private findCellTextSpan(): Chainable<any> {
    return this
      .findCell()
      .find('span');
  }
}

export class OpenCloseRendererSelectorBuilder {
  constructor(private readonly gridId: string,
              private readonly viewPortName: GridViewportName,
              private readonly rowId: Identifier,
              private readonly cellId: string) {

  }

  toggle() {
    this.findCell().find('i').click();
  }

  private findCell(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

}

export class CheckBoxRendererSelectorBuilder {
  constructor(private readonly gridId: string,
              private readonly viewPortName: GridViewportName,
              private readonly rowId: Identifier,
              private readonly cellId: string) {

  }

  assertIsCheck(shouldBeChecked = true) {
    const chainer = shouldBeChecked ? 'have.class' : 'not.have.class';
    this.findCheckbox().should(chainer, 'ant-checkbox-checked');
  }

  assertIsNotCheck() {
    this.findCheckbox().should('not.have.class', 'ant-checkbox-checked');
  }

  toggleState() {
    this.findCheckbox().find('input').click();
  }

  private findCell(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  findCheckbox(): Chainable<any> {
    return this
      .findCell()
      .find('span.ant-checkbox');
  }
}

export class RadioRendererNgZorroSelectorBuilder {
  constructor(private readonly gridId: string,
              private readonly viewPortName: GridViewportName,
              private readonly rowId: Identifier,
              private readonly cellId: string) {

  }

  private readonly checkboxCheckedClass = 'ant-radio-checked';

  assertIsCheck() {
    this.findRadio().should('have.class', this.checkboxCheckedClass);
  }

  assertIsNotCheck() {
    this.findRadio().should('not.have.class', this.checkboxCheckedClass);
  }

  check() {
    this.findRadio().click();
  }

  private findCell(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  findRadio(): Chainable<any> {
    return this
      .findCell()
      .find('span.ant-radio');
  }
}

export class RadioRendererInputSelectorBuilder {
  constructor(private readonly gridId: string,
              private readonly viewPortName: GridViewportName,
              private readonly rowId: Identifier,
              private readonly cellId: string) {

  }

  assertIsCheck() {
    this.findRadio().should('be.checked');
  }

  assertIsNotCheck() {
    this.findRadio().should('not.be.checked');
  }

  check() {
    this.findRadio().check();
  }

  private findCell(): Chainable<any> {
    return doFindCell(this.gridId, this.viewPortName, this.rowId, this.cellId);
  }

  findRadio(): Chainable<any> {
    return this
      .findCell()
      .find('input');
  }
}

function buildRootGridSelector(gridId: string): string {
  return `
    div[data-test-grid-id="${gridId}"]
    `;
}

function buildViewportSelector(viewportName: GridViewportName): string {
  return `
    div[data-test-viewport-name="${viewportName}"]
    `;
}

function buildRowsSelector(): string {
  return `
    div[data-test-row-id]
    `;
}

function buildRowSelector(rowId: Identifier): string {
  return `
    div[data-test-row-id="${rowId}"]
    `;
}

function doFindRow(gridId: string, viewportName: GridViewportName, rowId: Identifier): Chainable<any> {
  return cy
    .get(buildRootGridSelector(gridId))
    .find(buildViewportSelector(viewportName))
    .find(buildRowSelector(rowId));
}

export function buildCellSelector(cellId: Identifier): string {
  return `
    [data-test-cell-id="${cellId}"]
    `;
}

function doFindCell(gridId: string, viewportName: GridViewportName, rowId: Identifier, cellId: string): Chainable<any> {
  return doFindRow(gridId, viewportName, rowId)
    .find(buildCellSelector(cellId));
}

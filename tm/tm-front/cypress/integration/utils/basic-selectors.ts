export const DataTestComponentId = `data-test-component-id`;

export function selectByDataTestComponentId(id: string): string {
  return `[${DataTestComponentId}="${id}"]`;
}

export const DataTestButtonId = 'data-test-button-id';

export function selectByDataTestButtonId(id: string): string {
  return `[${DataTestButtonId}="${id}"]`;
}

export const DataTestDialogButtonId = 'data-test-dialog-button-id';

export function selectByDataTestDialogButtonId(id: string): string {
  return `[${DataTestDialogButtonId}="${id}"]`;
}

export const DataTestToolbarButtonId = 'data-test-toolbar-button-id';

export function selectByDataTestToolbarButtonId(id: string): string {
  return `[${DataTestToolbarButtonId}="${id}"]`;
}

export const DataTestMenuItemId = 'data-test-menu-item-id';

export function selectByDataTestMenuItemId(id: string): string {
  return `[${DataTestMenuItemId}="${id}"]`;
}

export const DataTestFieldId = 'data-test-field-id';

export function selectByDataTestFieldId(id: string): string {
  return `[${DataTestFieldId}="${id}"]`;
}

export const DataTestItemId = 'data-test-item-id';

export function selectByDataTestItemId(id: string): string {
  return `[${DataTestItemId}="${id}"]`;
}


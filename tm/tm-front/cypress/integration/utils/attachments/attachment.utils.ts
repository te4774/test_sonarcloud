import {UploadSummary} from '../../model/attachment/upload-summary.model';
import {HttpMockBuilder} from '../mocks/request-mock';


export class AttachmentUtils {
  static addAttachments(dropZoneSelector: string, files: File[], attachmentListId?: number, uploadSummary?: UploadSummary[]) {
    const mock = new HttpMockBuilder(`attach-list/${attachmentListId || '*'}/attachments/upload`)
      .post()
      .responseBody(uploadSummary)
      .build();
    cy.get(dropZoneSelector).trigger('drop', {
      dataTransfer: {
        files,
      }
    });
    mock.wait();
  }
}


import {Project, WorkspaceTypeForPlugins} from '../../model/project/project.model';
import {BindableEntity} from '../../model/bindable-entity.model';
import {Permissions} from '../../model/permissions/permissions.model';
import {ReferentialData} from '../../model/referential-data.model';
import {InputType} from '../../model/customfield/customfield.model';

const project1: Project = {
  id: 1,
  milestoneBindings: [
    {
      id: 1,
      milestoneId: 1,
      projectId: 1
    },
    {
      id: 2,
      milestoneId: 2,
      projectId: 1
    },
    {
      id: 3,
      milestoneId: 3,
      projectId: 1
    }
  ],
  requirementCategoryId: 1,
  testCaseNatureId: 2,
  testCaseTypeId: 3,
  customFieldBindings: {
    [BindableEntity.REQUIREMENT_FOLDER]: [],
    [BindableEntity.REQUIREMENT_VERSION]: [],
    [BindableEntity.TESTCASE_FOLDER]: [],
    [BindableEntity.TEST_CASE]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      }
    ],
    [BindableEntity.TEST_STEP]: [],
    [BindableEntity.CAMPAIGN_FOLDER]: [],
    [BindableEntity.CAMPAIGN]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      }
    ],
    [BindableEntity.ITERATION]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      }
    ],
    [BindableEntity.TEST_SUITE]: [],
    [BindableEntity.EXECUTION]: [],
    [BindableEntity.EXECUTION_STEP]: [],
    [BindableEntity.CUSTOM_REPORT_FOLDER]: [],
  },
  permissions: {
    REQUIREMENT_LIBRARY: [Permissions.READ, Permissions.CREATE, Permissions.WRITE, Permissions.LINK, Permissions.EXPORT],
    TEST_CASE_LIBRARY: [Permissions.READ, Permissions.CREATE, Permissions.WRITE, Permissions.LINK],
    CAMPAIGN_LIBRARY: [Permissions.READ, Permissions.CREATE, Permissions.WRITE, Permissions.LINK],
    PROJECT: []
  },
  allowAutomationWorkflow: true,
  disabledExecutionStatus: [],
  keywords: [{label: 'Étant donné que', value: 'GIVEN'}, {label: 'Quand', value: 'WHEN'},
    {label: 'Alors', value: 'THEN'}, {label: 'Et', value: 'AND'}, {label: 'Mais', value: 'BUT'}],
  bddScriptLanguage: 'FRENCH',
  activatedPlugins: {
    [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
  }
} as Project;

const project2: Project = {
  id: 2,
  milestoneBindings: [],
  customFieldBindings: {
    [BindableEntity.REQUIREMENT_FOLDER]: [],
    [BindableEntity.REQUIREMENT_VERSION]: [],
    [BindableEntity.TESTCASE_FOLDER]: [],
    [BindableEntity.TEST_CASE]: [],
    [BindableEntity.TEST_STEP]: [],
    [BindableEntity.CAMPAIGN_FOLDER]: [],
    [BindableEntity.CAMPAIGN]: [],
    [BindableEntity.ITERATION]: [],
    [BindableEntity.TEST_SUITE]: [],
    [BindableEntity.EXECUTION]: [],
    [BindableEntity.EXECUTION_STEP]: [],
    [BindableEntity.CUSTOM_REPORT_FOLDER]: [],
  },
  permissions: {},
  keywords: [{label: 'Étant donné que', value: 'GIVEN'}, {label: 'Quand', value: 'WHEN'},
    {label: 'Alors', value: 'THEN'}, {label: 'Et', value: 'AND'}, {label: 'Mais', value: 'BUT'}],
  bddScriptLanguage: 'FRENCH',
  activatedPlugins: {
    [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
  }
} as Project;

export const createEntityReferentialData: ReferentialData = {
  projects: [
    project1, project2
  ],
  customFields: [
    {
      id: 12,
      code: 'CODE',
      defaultValue: 'a little value',
      inputType: InputType.PLAIN_TEXT,
      label: 'FIELD A',
      name: 'fieldA',
      optional: false,
      largeDefaultValue: null,
      numericDefaultValue: null,
      options: null
    },
    {
      id: 13,
      code: 'CODE_2',
      defaultValue: 'a little value',
      inputType: InputType.NUMERIC,
      label: 'FIELD B',
      name: 'fieldB',
      optional: false,
      largeDefaultValue: null,
      numericDefaultValue: 2,
      options: null
    },
    {
      id: 14,
      code: 'CODE_3',
      defaultValue: 'o1|o2',
      inputType: InputType.TAG,
      label: 'FIELD C',
      name: 'fieldC',
      optional: false,
      largeDefaultValue: null,
      numericDefaultValue: null,
      options: [
        {
          cfId: 14,
          position: 0,
          label: 'o1',
          code: 'OPTION_1',
          colour: ''
        },
        {
          cfId: 14,
          position: 1,
          label: 'o2',
          code: 'OPTION_2',
          colour: ''
        },
        {
          cfId: 14,
          position: 2,
          label: 'o3',
          code: 'OPTION_2',
          colour: ''
        }
      ]
    },
    {
      id: 15,
      code: 'CODE_4',
      defaultValue: 'a little value',
      inputType: InputType.PLAIN_TEXT,
      label: 'FIELD D',
      name: 'fieldD',
      optional: true,
      largeDefaultValue: null,
      numericDefaultValue: null,
      options: null
    },
    {
      id: 16,
      code: 'CODE_5',
      defaultValue: 'a little value',
      inputType: InputType.RICH_TEXT,
      label: 'FIELD E',
      name: 'fieldE',
      optional: false,
      largeDefaultValue: '<p>a little value</p>',
      numericDefaultValue: null,
      options: null
    },
    {
      id: 17,
      code: 'CODE_6',
      defaultValue: '',
      inputType: InputType.DATE_PICKER,
      label: 'Date',
      name: 'date',
      optional: true,
      largeDefaultValue: '2020-02-14',
      numericDefaultValue: null,
      options: null
    },
    {
      id: 18,
      code: 'CODE_7',
      defaultValue: '',
      inputType: InputType.CHECKBOX,
      label: 'Checkbox',
      name: 'checkbox',
      optional: true,
      largeDefaultValue: 'true',
      numericDefaultValue: null,
      options: null
    },
    {
      id: 19,
      code: 'CODE_7',
      defaultValue: '',
      inputType: InputType.DROPDOWN_LIST,
      label: 'Dropdown',
      name: 'dropdown',
      optional: true,
      largeDefaultValue: 'Option C',
      numericDefaultValue: null,
      options: [
        {
          cfId: 19,
          label: 'Option A',
          position: 0,
          colour: 'red',
          code: 'A'
        },
        {
          cfId: 19,
          label: 'Option C',
          position: 2,
          colour: '',
          code: 'C'
        },
        {
          cfId: 19,
          label: 'Option B',
          position: 1,
          colour: '',
          code: 'B'
        }
      ]
    }
  ],
  filteredProjectIds: [],
  infoLists: [
    {
      'id': 1,
      'uri': null,
      'code': 'DEF_REQ_CAT',
      'label': 'infolist.category.default',
      'description': '',
      'items': [
        {
          'id': 1,
          'uri': null,
          'code': 'CAT_FUNCTIONAL',
          'label': 'requirement.category.CAT_FUNCTIONAL',
          'friendlyLabel': null,
          'iconName': 'monitor',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 2,
          'uri': null,
          'code': 'CAT_NON_FUNCTIONAL',
          'label': 'requirement.category.CAT_NON_FUNCTIONAL',
          'friendlyLabel': null,
          'iconName': 'server',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 3,
          'uri': null,
          'code': 'CAT_USE_CASE',
          'label': 'requirement.category.CAT_USE_CASE',
          'friendlyLabel': null,
          'iconName': 'read',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 4,
          'uri': null,
          'code': 'CAT_BUSINESS',
          'label': 'requirement.category.CAT_BUSINESS',
          'friendlyLabel': null,
          'iconName': 'briefcase',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 5,
          'uri': null,
          'code': 'CAT_TEST_REQUIREMENT',
          'label': 'requirement.category.CAT_TEST_REQUIREMENT',
          'friendlyLabel': null,
          'iconName': 'checked_checkbox',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 6,
          'uri': null,
          'code': 'CAT_UNDEFINED',
          'label': 'requirement.category.CAT_UNDEFINED',
          'friendlyLabel': null,
          'iconName': 'indeterminate_checkbox_empty',
          'itemIndex': null,
          'default': true,
          'system': true,
          'denormalized': false
        }, {
          'id': 7,
          'uri': null,
          'code': 'CAT_ERGONOMIC',
          'label': 'requirement.category.CAT_ERGONOMIC',
          'friendlyLabel': null,
          'iconName': 'puzzle',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 8,
          'uri': null,
          'code': 'CAT_PERFORMANCE',
          'label': 'requirement.category.CAT_PERFORMANCE',
          'friendlyLabel': null,
          'iconName': 'dashboard',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 9,
          'uri': null,
          'code': 'CAT_TECHNICAL',
          'label': 'requirement.category.CAT_TECHNICAL',
          'friendlyLabel': null,
          'iconName': 'key',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 10,
          'uri': null,
          'code': 'CAT_USER_STORY',
          'label': 'requirement.category.CAT_USER_STORY',
          'friendlyLabel': null,
          'iconName': 'bookmark',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 11,
          'uri': null,
          'code': 'CAT_SECURITY',
          'label': 'requirement.category.CAT_SECURITY',
          'friendlyLabel': null,
          'iconName': 'protect',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }
      ]
    }, {
      'id': 2,
      'uri': null,
      'code': 'DEF_TC_NAT',
      'label': 'infolist.nature.default',
      'description': '',
      'items': [
        {
          'id': 12,
          'uri': null,
          'code': 'NAT_UNDEFINED',
          'label': 'test-case.nature.NAT_UNDEFINED',
          'friendlyLabel': null,
          'iconName': 'indeterminate_checkbox_empty',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 13,
          'uri': null,
          'code': 'NAT_FUNCTIONAL_TESTING',
          'label': 'test-case.nature.NAT_FUNCTIONAL_TESTING',
          'friendlyLabel': null,
          'iconName': 'monitor',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 14,
          'uri': null,
          'code': 'NAT_BUSINESS_TESTING',
          'label': 'test-case.nature.NAT_BUSINESS_TESTING',
          'friendlyLabel': null,
          'iconName': 'briefcase',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 15,
          'uri': null,
          'code': 'NAT_USER_TESTING',
          'label': 'test-case.nature.NAT_USER_TESTING',
          'friendlyLabel': null,
          'iconName': 'user',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 16,
          'uri': null,
          'code': 'NAT_NON_FUNCTIONAL_TESTING',
          'label': 'test-case.nature.NAT_NON_FUNCTIONAL_TESTING',
          'friendlyLabel': null,
          'iconName': 'server',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 17,
          'uri': null,
          'code': 'NAT_PERFORMANCE_TESTING',
          'label': 'test-case.nature.NAT_PERFORMANCE_TESTING',
          'friendlyLabel': null,
          'iconName': 'dashboard',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 18,
          'uri': null,
          'code': 'NAT_SECURITY_TESTING',
          'label': 'test-case.nature.NAT_SECURITY_TESTING',
          'friendlyLabel': null,
          'iconName': 'protect',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 19,
          'uri': null,
          'code': 'NAT_ATDD',
          'label': 'test-case.nature.NAT_ATDD',
          'friendlyLabel': null,
          'iconName': 'circular_arrows',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }
      ]
    }, {
      'id': 3,
      'uri': null,
      'code': 'DEF_TC_TYP',
      'label': 'infolist.type.default',
      'description': '',
      'items': [
        {
          'id': 20,
          'uri': null,
          'code': 'TYP_UNDEFINED',
          'label': 'test-case.type.TYP_UNDEFINED',
          'friendlyLabel': null,
          'iconName': 'indeterminate_checkbox_empty',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 21,
          'uri': null,
          'code': 'TYP_COMPLIANCE_TESTING',
          'label': 'test-case.type.TYP_COMPLIANCE_TESTING',
          'friendlyLabel': null,
          'iconName': 'task_completed',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 22,
          'uri': null,
          'code': 'TYP_CORRECTION_TESTING',
          'label': 'test-case.type.TYP_CORRECTION_TESTING',
          'friendlyLabel': null,
          'iconName': 'bug',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 23,
          'uri': null,
          'code': 'TYP_EVOLUTION_TESTING',
          'label': 'test-case.type.TYP_EVOLUTION_TESTING',
          'friendlyLabel': null,
          'iconName': 'circled_up_right',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 24,
          'uri': null,
          'code': 'TYP_REGRESSION_TESTING',
          'label': 'test-case.type.TYP_REGRESSION_TESTING',
          'friendlyLabel': null,
          'iconName': 'circular_arrows',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 25,
          'uri': null,
          'code': 'TYP_END_TO_END_TESTING',
          'label': 'test-case.type.TYP_END_TO_END_TESTING',
          'friendlyLabel': null,
          'iconName': 'journey',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }, {
          'id': 26,
          'uri': null,
          'code': 'TYP_PARTNER_TESTING',
          'label': 'test-case.type.TYP_PARTNER_TESTING',
          'friendlyLabel': null,
          'iconName': 'handshake',
          'itemIndex': null,
          'default': false,
          'system': true,
          'denormalized': false
        }
      ]
    }
  ],
  user: {
    username: 'user',
    userId: 1,
    admin: false,
    projectManager: false,
    functionalTester: true,
    automationProgrammer: false,
    firstName: 'firstName',
    lastName: 'lastName'
  },
  projectFilterStatus: false,
  globalConfiguration: {milestoneFeatureEnabled: true, uploadFileExtensionWhitelist: [], uploadFileSizeLimit: 0},
  bugTrackers: [],
  milestones: [
    {
      id: 1,
      label: 'Milestone 1',
      description: '',
      status: 'PLANNED',
      endDate: new Date(Date.now()),
      range: 'GLOBAL',
      ownerFistName: 'John',
      ownerLastName: 'Doe',
      ownerLogin: 'jdoe',
      lastModifiedOn: null,
      lastModifiedBy: null,
      createdBy: 'cypress',
      createdOn: new Date(Date.now()),
    },
    {
      id: 2,
      label: 'Milestone 2',
      description: '',
      status: 'IN_PROGRESS',
      endDate: new Date(Date.now()),
      range: 'GLOBAL',
      ownerFistName: 'John',
      ownerLastName: 'Doe',
      ownerLogin: 'jdoe',
      lastModifiedOn: null,
      lastModifiedBy: null,
      createdBy: 'cypress',
      createdOn: new Date(Date.now()),
    },
    {
      id: 3,
      label: 'Milestone 3',
      description: '',
      status: 'FINISHED',
      endDate: new Date(Date.now()),
      range: 'GLOBAL',
      ownerFistName: 'John',
      ownerLastName: 'Doe',
      ownerLogin: 'jdoe',
      lastModifiedOn: null,
      lastModifiedBy: null,
      createdBy: 'cypress',
      createdOn: new Date(Date.now()),
    },
    {
      id: 4,
      label: 'Milestone 4',
      description: '',
      status: 'LOCKED',
      endDate: new Date(Date.now()),
      range: 'GLOBAL',
      ownerFistName: 'John',
      ownerLastName: 'Doe',
      ownerLogin: 'jdoe',
      lastModifiedOn: null,
      lastModifiedBy: null,
      createdBy: 'cypress',
      createdOn: new Date(Date.now()),
    }
  ],
  automationServers: [],
  milestoneFilterState: {
    milestoneModeEnabled: false,
    selectedMilestoneId: null
  },
  requirementVersionLinkTypes: [],
  licenseInformation: {activatedUserExcess: null, pluginLicenseExpiration: null},
  automatedTestTechnologies: [],
  workspacePlugins: [],
  workspaceWizards: [],
  availableTestAutomationServerKinds: [],
  scmServers: [],
  templateConfigurablePlugins: [],
  premiumPluginInstalled: false,
  documentationLinks: []
};

import {HTTP_RESPONSE_STATUS, HttpMock, HttpMockBuilder} from '../mocks/request-mock';
import {AdminReferentialData} from '../../model/admin-referential-data.model';
import {getDefaultAdminReferentialData} from './admin-referential-data-builder';
import Chainable = Cypress.Chainable;

export class AdminReferentialDataProvider {
  constructor(private httpRequestMock: HttpMock<AdminReferentialData>) {

  }

  wait(): void {
    cy.wait(this.httpRequestMock.alias)
      .its('status')
      .should('eq', HTTP_RESPONSE_STATUS.SUCCESS);
  }

  waitForAdminReferentialData(): Chainable<AdminReferentialData> {
    return cy.wait(this.httpRequestMock.alias)
      .then(xhr => xhr.responseBody as AdminReferentialData);
  }

}

export class AdminReferentialDataProviderBuilder {

  constructor(private adminReferentialData: AdminReferentialData = getDefaultAdminReferentialData()) {
  }

  build(): AdminReferentialDataProvider {
    const httpRequestMock = new HttpMockBuilder<AdminReferentialData>('referential/admin').responseBody(this.adminReferentialData).build();
    return new AdminReferentialDataProvider(httpRequestMock);
  }
}


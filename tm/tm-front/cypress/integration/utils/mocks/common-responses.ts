export const EMPTY_PARAM_RESPONSE: Readonly<any> = {
  parameters: [],
  dataSets: [],
  paramValues: []
};

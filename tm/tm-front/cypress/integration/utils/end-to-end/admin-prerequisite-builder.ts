// tslint:disable:max-line-length
import {isPostgresql} from '../../../utils/testing-context';
import {DatabaseUtils} from '../database.utils';

const AclClass = {
  1: 'org.squashtest.tm.domain.project.Project',
  2: 'org.squashtest.tm.domain.requirement.RequirementLibrary',
  3: 'org.squashtest.tm.domain.testcase.TestCaseLibrary',
  4: 'org.squashtest.tm.domain.campaign.CampaignLibrary',
  5: 'org.squashtest.tm.domain.project.ProjectTemplate',
  6: 'org.squashtest.tm.domain.customreport.CustomReportLibrary',
  7: 'org.squashtest.tm.domain.tf.automationrequest.AutomationRequestLibrary',
  8: 'org.squashtest.tm.domain.actionword.ActionWordLibrary',
};

export type AclGroupName = 'TestEditor'
  | 'ProjectViewer'
  | 'ProjectManager'
  | 'TestRunner'
  | 'TestDesigner'
  | 'AdvanceTester'
  | 'Validator'
  | 'AutomatedTestWriter';

const AclGroup: { [K in AclGroupName]: number } = {
  'TestEditor': 2,
  'ProjectViewer': 4,
  'ProjectManager': 5,
  'TestRunner': 6,
  'TestDesigner': 7,
  'AdvanceTester': 8,
  'Validator': 9,
  'AutomatedTestWriter': 10
};

export class ProjectBuilder {
  private readonly ATTACHMENT_LIST_COUNT = 7;
  private attachmentListSeq: number;
  private index: number;

  private name;

  public withName(name: string) {
    this.name = name;
    return this;
  }

  private insertRequirementLibrary(): string[] {
    const attachmentListId = this.nextAttachmentListId();
    return [
      this.insertAttachmentList(attachmentListId),
      `INSERT INTO REQUIREMENT_LIBRARY(RL_ID, ATTACHMENT_LIST_ID) VALUES (${this.index}, ${attachmentListId})`
    ];
  }

  private insertTestCaseLibrary(): string[] {
    const attachmentListId = this.nextAttachmentListId();
    return [
      this.insertAttachmentList(attachmentListId),
      `INSERT INTO TEST_CASE_LIBRARY(TCL_ID, ATTACHMENT_LIST_ID) VALUES (${this.index}, ${attachmentListId})`
    ];
  }

  private insertCampaignLibrary(): string[] {
    const attachmentListId = this.nextAttachmentListId();
    return [
      this.insertAttachmentList(attachmentListId),
      `INSERT INTO CAMPAIGN_LIBRARY(CL_ID, ATTACHMENT_LIST_ID) VALUES (${this.index}, ${attachmentListId})`
    ];
  }

  private insertCustomReportLibrary(): string[] {
    const attachmentListId = this.nextAttachmentListId();
    return [
      this.insertAttachmentList(attachmentListId),
      `INSERT INTO CUSTOM_REPORT_LIBRARY(CRL_ID, ATTACHMENT_LIST_ID) VALUES (${this.index}, ${attachmentListId})`,
      `INSERT INTO CUSTOM_REPORT_LIBRARY_NODE(CRLN_ID, NAME, ENTITY_TYPE, ENTITY_ID, CRL_ID) VALUES (${this.index}, '${this.getProjectName()}', 'LIBRARY', ${this.index}, ${this.index})`
    ];
  }

  private insertAutomationRequestLibrary(): string[] {
    const attachmentListId = this.nextAttachmentListId();
    return [
      this.insertAttachmentList(attachmentListId),
      `INSERT INTO AUTOMATION_REQUEST_LIBRARY(ARL_ID, ATTACHMENT_LIST_ID) VALUES (${this.index}, ${attachmentListId})`
    ];
  }

  private nextAttachmentListId() {
    this.attachmentListSeq++;
    return this.attachmentListSeq;
  }

  private insertAttachmentList(attachmentListId: number) {
    return `INSERT INTO ATTACHMENT_LIST(ATTACHMENT_LIST_ID) VALUES (${attachmentListId})`;
  }

  build(index: number): string[] {
    this.index = index + 1;
    this.attachmentListSeq = (index) * this.ATTACHMENT_LIST_COUNT;
    return [
      this.insertRequirementLibrary(),
      this.insertTestCaseLibrary(),
      this.insertCampaignLibrary(),
      this.insertCustomReportLibrary(),
      this.insertAutomationRequestLibrary(),
      this.insertActionWordLibrary(),
      this.createProject(),
      this.createAclObjectIdentities()
    ].flat();
  }

  private createProject() {
    const attachmentListId = this.nextAttachmentListId();
    const projectName = this.getProjectName();
    return [
      this.insertAttachmentList(attachmentListId),
      `INSERT INTO PROJECT(PROJECT_ID,NAME, DESCRIPTION, LABEL, CREATED_BY, CREATED_ON,  RL_ID, TCL_ID, CL_ID, CRL_ID, ARL_ID, AWL_ID, ATTACHMENT_LIST_ID)
        VALUES (${this.index},'${projectName}', 'description', '${projectName}', 'multipass', '2019-10-31 14:44:45',${this.index},${this.index},${this.index},${this.index},${this.index},${this.index}, ${attachmentListId})`
    ];
  }

  private getProjectName() {
    return this.name || `project-${this.index}`;
  }

  private createAclObjectIdentities() {
    return Object.entries(AclClass)
      .filter(entry => entry[1] !== 'org.squashtest.tm.domain.project.ProjectTemplate')
      .map(([key, value], classIndex, array) => {
        const objectIdentityPk = ((this.index - 1) * array.length) + classIndex + 1;
        return `INSERT INTO ACL_OBJECT_IDENTITY(ID, IDENTITY, CLASS_ID) VALUES (${objectIdentityPk}, ${this.index},${key})`;
      });
  }

  private insertActionWordLibrary() {
    const attachmentListId = this.nextAttachmentListId();
    return [
      this.insertAttachmentList(attachmentListId),
      `INSERT INTO ACTION_WORD_LIBRARY(AWL_ID, ATTACHMENT_LIST_ID) VALUES (${this.index}, ${attachmentListId})`,
      `INSERT INTO ACTION_WORD_LIBRARY_NODE(AWLN_ID, NAME, ENTITY_TYPE, ENTITY_ID, AWL_ID) VALUES (${this.index}, '${this.getProjectName()}', 'LIBRARY', ${this.index}, ${this.index})`
    ];
  }
}

export class UserBuilder {
  private index: number;

  private login: string;

  projectProfileMap: { [K in number]: AclGroupName } = {};

  private arseSeq;
  private offset: number;

  withLogin(login: string) {
    this.login = login;
    return this;
  }

  withProfileOnProject(projectId: number, profile: AclGroupName) {
    this.projectProfileMap = {...this.projectProfileMap, [projectId]: profile};
    return this;
  }

  build(index: number, offset): string[] {
    // +2 because first admin always present in database
    this.index = index + 2;
    this.arseSeq = offset * this.getProjectAclClasses().length;
    const login = this.login || `user-${this.index}`;
    return [
      this.buildUser(login),
      this.buildPermissions(),
      this.buildPartyAuthorities()
    ].flat();
  }

  private buildUser(login: string) {
    return [
      `INSERT INTO CORE_PARTY(PARTY_ID) VALUES (${this.index})`,
      `INSERT INTO CORE_USER(PARTY_ID, LOGIN, FIRST_NAME, LAST_NAME, EMAIL, CREATED_BY, CREATED_ON, ACTIVE)
        VALUES (${this.index}, '${login}','${login}','${login}','toto@gmail.com','admin','2019-10-31 14:44:45',true)`,
      `INSERT INTO CORE_GROUP_MEMBER(PARTY_ID, GROUP_ID) VALUES (${this.index}, 2)`,
      `INSERT INTO AUTH_USER(LOGIN, PASSWORD, ACTIVE) VALUES ('${login}','d033e22ae348aeb5660fc2140aec35850c4da997',true)`
    ];
  }

  private buildPermissions() {
    return Object.entries(this.projectProfileMap).map(([projectIdStr, aclGroup]) => {
      const projectId = Number.parseInt(projectIdStr, 10);
      return this.getProjectAclClasses()
        .map(([key, value], classIndex, array) => {
          this.arseSeq++;
          const arseId = this.arseSeq;
          const objectIdentityPk = ((projectId - 1) * array.length) + classIndex + 1;
          return `INSERT INTO ACL_RESPONSIBILITY_SCOPE_ENTRY(ID, PARTY_ID, ACL_GROUP_ID, OBJECT_IDENTITY_ID)
                VALUES (${arseId},${this.index},${AclGroup[aclGroup]},${objectIdentityPk})`;
        });
    }).flat();
  }

  private getProjectAclClasses() {
    return Object.entries(AclClass)
      .filter(entry => entry[1] !== 'org.squashtest.tm.domain.project.ProjectTemplate');
  }

  private buildPartyAuthorities() {
    const auths = ['TM_ROLE_USER'];
    const isProjectManager = Object.values(this.projectProfileMap).filter(p => p === 'ProjectManager').length > 0;
    const isAutomatedTestWriter = Object.values(this.projectProfileMap).filter(p => p === 'AutomatedTestWriter').length > 0;
    const isFunctionalTester = Object.values(this.projectProfileMap).filter(p => p !== 'AutomatedTestWriter').length > 0;
    if (isProjectManager) {
      auths.push('ROLE_TM_PROJECT_MANAGER');
    }
    if (isAutomatedTestWriter) {
      auths.push('ROLE_TF_AUTOMATION_PROGRAMMER');
    }
    if (isFunctionalTester) {
      auths.push('ROLE_TF_FUNCTIONAL_TESTER');
    }
    return auths.map(auth => `INSERT INTO CORE_PARTY_AUTHORITY(PARTY_ID, AUTHORITY) VALUES (${this.index}, '${auth}')`);
  }
}


export class AdminPrerequisiteBuilder {

  private userProjectCount = 0;
  private projects: ProjectBuilder[] = [];
  private users: UserBuilder[] = [];

  withProjects(projects: ProjectBuilder[]) {
    this.projects = projects;
    return this;
  }

  withUsers(users: UserBuilder[]) {
    this.users = users;
    return this;
  }

  build(): void {
    const requests = [
      ...this.projects.map((p, i) => p.build(i)).flat(),
      ...this.users.map((user, index) => {
        const strings = user.build(index, this.userProjectCount);
        this.userProjectCount = Object.values(user.projectProfileMap).length + this.userProjectCount;
        return strings;
      }).flat()
    ];
    const flattenRequests = requests.join(';\n');
    console.log(flattenRequests);
    cy.task('executeQuery', flattenRequests).then(() => {
      if (isPostgresql()) {
        console.log('Should reset postgresql sequences after inserts with fixed ids');
        DatabaseUtils.resetSequence('ATTACHMENT_LIST');
        DatabaseUtils.resetSequence('PROJECT');
        DatabaseUtils.resetSequence('REQUIREMENT_LIBRARY', 'RL_ID');
        DatabaseUtils.resetSequence('TEST_CASE_LIBRARY', 'TCL_ID');
        DatabaseUtils.resetSequence('CAMPAIGN_LIBRARY', 'CL_ID');
        DatabaseUtils.resetSequence('CUSTOM_REPORT_LIBRARY', 'CRL_ID');
        DatabaseUtils.resetSequence('CUSTOM_REPORT_LIBRARY_NODE', 'CRLN_ID');
        DatabaseUtils.resetSequence('AUTOMATION_REQUEST_LIBRARY', 'ARL_ID');
        DatabaseUtils.resetSequence('ACTION_WORD_LIBRARY', 'AWL_ID');
        DatabaseUtils.resetSequence('ACTION_WORD_LIBRARY_NODE', 'AWLN_ID');
        DatabaseUtils.resetSequence('ACL_OBJECT_IDENTITY', 'ID');
      }
    });
  }
}

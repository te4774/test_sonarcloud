export class DatabaseUtils {

  static resetSequence(tableName: string, idColumnName?: string, sequenceName?: string) {
    const resolvedIdColumnName = idColumnName || `${tableName}_ID`;
    const resolvedSequenceName = sequenceName || `${tableName}_${resolvedIdColumnName}_seq`.toLowerCase();
    const request = `SELECT setval('${resolvedSequenceName}',
                                   (SELECT MAX(${resolvedIdColumnName})
                                    FROM ${tableName}
                                   ))`;

    console.log(request);

    cy.task('executeQuery', request).then(() => {
      console.log(`Sequence reset with success : ${resolvedSequenceName}`);
    });
  }

  public static cleanDatabase() {
    cy.task('cleanDatabase').then(() => console.log('Database Cleaned...'));
  }

}

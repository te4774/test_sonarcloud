import {combineWithDefaultData} from './data-mocks.utils';
import {Milestone} from '../model/milestone/milestone.model';

export function mockMilestoneModel(customData: Partial<Milestone> = {}): Milestone {
  return combineWithDefaultData( {
    id: 1,
    endDate: new Date(),
    label: '',
    description: '',
    ownerFistName: '',
    ownerLastName: '',
    ownerLogin: '',
    range: 'GLOBAL',
    status: 'IN_PROGRESS',
    createdBy: 'admin',
    createdOn: new Date(),
    lastModifiedBy: 'admin',
    lastModifiedOn: new Date(),
  }, customData);
}

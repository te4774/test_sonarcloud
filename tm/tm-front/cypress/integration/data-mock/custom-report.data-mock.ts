import {CustomReportLibraryModel} from '../model/custom-report/custom-report-librairy/custom-report-library.model';
import {mockEmptyAttachmentListModel} from './generic-entity.data-mock';
import {combineWithDefaultData} from './data-mocks.utils';
import {CampaignFolderModel} from '../model/campaign/campaign-folder/campaign-folder.model';
import {CustomReportFolderModel} from '../model/custom-report/custom-report-folder/custom-report-folder.model';
import {ChartDefinitionModel} from '../model/custom-report/chart-definition.model';
import {CustomDashboardModel} from '../model/custom-report/custom-dashboard.model';
import {DataRow, DataRowOpenState} from '../model/grids/data-row.type';

export function mockCustomReportLibraryModel(customData: Partial<CustomReportLibraryModel> = {}): CustomReportLibraryModel {
  return combineWithDefaultData({
    id: 1,
    projectId: 1,
    name: '',
    attachmentList: mockEmptyAttachmentListModel(),
    customFieldValues: [],
    description: '',
  }, customData);
}

export function mockCustomReportFolderModel(customData: Partial<CampaignFolderModel> = {}): CampaignFolderModel {
  const defaultData: CustomReportFolderModel = {
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    customFieldValues: [],
    name: '',
    description: 'generic description'
  };

  return  combineWithDefaultData(defaultData, customData);
}

export function mockChartDefinitionModel(customData: Partial<ChartDefinitionModel> = {}): ChartDefinitionModel {
  const defaultData: ChartDefinitionModel = {
    id: 1,
    name: 'chart',
    createdOn: new Date(),
    customReportLibraryNodeId: 1,
    projectId: 1,
    type: null,
    measures: [],
    axis: [],
    filters: [],
    abscissa: [],
    scopeType: null,
    scope: [],
    projectScope: ['1'],
    series: {}
  };
  return combineWithDefaultData(defaultData, customData);
}


export function mockCustomReportDashboardModel(customData: Partial<CustomDashboardModel> = {}): CustomDashboardModel {
  const defaultData: CustomDashboardModel = {
    id: 1,
    projectId: 1,
    customReportLibraryNodeId: 1,
    createdBy: '',
    name: 'dashboard',
    chartBindings: [],
    reportBindings: [],
    favoriteWorkspaces: [],
  };
  return combineWithDefaultData(defaultData, customData);
}

export function getSimpleCustomReportLibraryChildNodes() {
  return [
    {
      id: 'CustomReportLibrary-1',
      children: ['CustomReportFolder-2', 'ChartDefinition-3', 'ReportDefinition-4'],
      data: {'NAME': 'International Space Station', 'CHILD_COUNT': '3'},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'CustomReportFolder-2',
      children: [],
      parentRowId: 'CustomReportLibrary-1',
      data: {'NAME': 'Structural Requirements Reports'},
      state: DataRowOpenState.closed
    } as unknown as DataRow,
    {
      id: 'ChartDefinition-3',
      children: [],
      parentRowId: 'CustomReportLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        'CRLN_ID': 3,
        'CHILD_COUNT': 0,
        'NAME': 'Financial Breakdown',
      }
    } as unknown as DataRow,
    {
      id: 'ReportDefinition-4',
      children: [],
      parentRowId: 'CustomReportLibrary-1',
      state: DataRowOpenState.leaf,
      data: {
        'CRLN_ID': 4,
        'CHILD_COUNT': 0,
        'NAME': 'Reg Report',
      }
    } as unknown as DataRow,
  ];
}


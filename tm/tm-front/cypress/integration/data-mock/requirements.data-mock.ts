import {LinkedLowLevelRequirement, RequirementVersionModel} from '../model/requirements/requirement-version.model';
import {mockEmptyAttachmentListModel} from './generic-entity.data-mock';
import {RequirementLibraryModel} from '../model/requirements/requirement-library/requirement-library.model';
import {RequirementFolderModel} from '../model/requirements/requirement-folder/requirement-folder.model';
import {combineWithDefaultData} from './data-mocks.utils';

export function mockRequirementVersionModel(customData: Partial<RequirementVersionModel> = {}): RequirementVersionModel {
  return combineWithDefaultData({
    id: 1,
    projectId: 1,
    attachmentList: mockEmptyAttachmentListModel(),
    bindableMilestones: [],
    category: 0,
    createdBy: '',
    createdOn: undefined,
    criticality: '',
    customFieldValues: [],
    description: '',
    lastModifiedBy: '',
    lastModifiedOn: undefined,
    milestones: [],
    name: '',
    reference: '',
    requirementId: 0,
    requirementStats: undefined,
    requirementVersionLinks: [],
    status: 'OBSOLETE',
    verifyingTestCases: [],
    versionNumber: 0,
    highLevelRequirement: false,
    lowLevelRequirements: [],
    linkedHighLevelRequirement: null,
    remoteReqPerimeterStatus: undefined,
    childOfRequirement: false,
    nbIssues: null
  }, customData);
}

export function mockRequirementLibraryModel(customData: Partial<RequirementFolderModel> = {}): RequirementLibraryModel {
  return combineWithDefaultData({
    id: 1,
    projectId: 1,
    name: '',
    attachmentList: mockEmptyAttachmentListModel(),
    canShowFavoriteDashboard: false,
    customFieldValues: [],
    dashboard: undefined,
    description: '',
    favoriteDashboardId: 0,
    shouldShowFavoriteDashboard: false,
    statistics: undefined,
    extendHighLvlReqScope: false
  }, customData);
}

export function mockRequirementFolderModel(customData: Partial<RequirementFolderModel> = {}): RequirementFolderModel {
  return combineWithDefaultData({
    id: 1,
    projectId: 1,
    name: '',
    attachmentList: mockEmptyAttachmentListModel(),
    canShowFavoriteDashboard: false,
    customFieldValues: [],
    dashboard: undefined,
    description: '',
    favoriteDashboardId: 0,
    shouldShowFavoriteDashboard: false,
    statistics: undefined,
    extendHighLvlReqScope: false
  }, customData);
}

export function mockLinkedLowLevelRequirementModel
(customData: Partial<LinkedLowLevelRequirement> = {}): LinkedLowLevelRequirement {
  return combineWithDefaultData({
    requirementId: null,
    requirementVersionId: null,
    name: '',
    reference: '',
    childOfRequirement: true,
    projectName: '',
    milestoneLabels: '',
    milestoneMinDate: null,
    milestoneMaxDate: null,
    versionNumber: 1,
    criticality: 'MAJOR',
    requirementStatus: 'APPROVED',
  }, customData);
}

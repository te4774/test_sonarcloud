import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {ProjectPrerequisiteCommands} from '../../../../page-objects/scenarios-parts/project/project_e2e_commands';
import {
  AdminWorkspaceProjectsPage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {ProjectViewPage} from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
// tslint:disable-next-line:max-line-length
import {
  ProjectAutomationPanelElement
} from '../../../../page-objects/pages/administration-workspace/project-view/panels/project-automation-panel.element';
import {
  TestCaseViewAutomationPage
} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-automation.page';

describe('Create test case - prerequisite', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.createProject('project-1');
  });

  it('should create test case', () => {
    cy.logInAs('admin', 'admin');
    cy.goHome();
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);

      const createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
      createTestCaseDialog.fillFieldsWithoutCuf('TEST1', 'REF1', 'DESCRIPTION1');
      createTestCaseDialog.clickOnAddButton();

    });
  });
});

  describe('Check automation block conformity', function () {

    beforeEach(function () {
      Cypress.Cookies.debug(true);
    });

    it('should check if there is no automation block', () => {
      cy.logInAs('admin', 'admin');

      const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
      testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
        testCaseWorkspace.tree.openNode(projectId);

        let testCase1ViewPage: TestCaseViewPage;
        testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
          testCase1ViewPage = testCaseWorkspace.tree.selectNode(testCaseId1);
          testCase1ViewPage.assertAutomationBlockExist(false);
        });
      });
    });

    it('should activate the automation workflow', () => {
      cy.logInAs('admin', 'admin');

      const adminWorkspaceProjectsPage = NavBarElement.navigateToAdministration<AdminWorkspaceProjectsPage>('projects');
      adminWorkspaceProjectsPage.grid.findRowId('name', 'project-1').then(projectId => {
        adminWorkspaceProjectsPage.grid.selectRow(projectId);
        const projectView = new ProjectViewPage();
        const projectAutomationPanelElement =  projectView.clickAnchorLink('automation') as ProjectAutomationPanelElement;
        projectAutomationPanelElement.selectAutomationWorkflowType('Squash');
      });
    });

    it('should check if there is an automation block', () => {
      cy.logInAs('admin', 'admin');

      const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
      testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
        testCaseWorkspace.tree.openNode(projectId);

        let testCase1ViewPage: TestCaseViewPage;
        testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
          testCase1ViewPage = testCaseWorkspace.tree.selectNode(testCaseId1);
          testCase1ViewPage.assertAutomationBlockExist(true);
          const testCaseViewAutomationPage = testCase1ViewPage.clickAnchorLink('automation') as TestCaseViewAutomationPage;
          testCaseViewAutomationPage.checkAutomatableState('M');
          testCaseViewAutomationPage.priority.assertNotExist();
          testCaseViewAutomationPage.requestStatus.assertNotExist();
          testCaseViewAutomationPage.uuid.assertNotExist();
          testCaseViewAutomationPage.transmittedOn.assertNotExist();
          cy.get(`${testCase1ViewPage.rootSelector} [data-test-button-id="test-case-automation-transmit"]`)
            .should('not.exist');
        });
      });
    });


    it('should check Eligible option', () => {
      cy.logInAs('admin', 'admin');

      const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
      testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
        testCaseWorkspace.tree.openNode(projectId);

        let testCase1ViewPage: TestCaseViewPage;
        testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
          testCase1ViewPage = testCaseWorkspace.tree.selectNode(testCaseId1);
          testCase1ViewPage.assertAutomationBlockExist(true);
          const testCaseViewAutomationPage = testCase1ViewPage.clickAnchorLink('automation') as TestCaseViewAutomationPage;
          testCaseViewAutomationPage.setAutomatable('Y');
          testCaseViewAutomationPage.checkAutomatableState('Y');
          testCaseViewAutomationPage.priority.assertExist();
          testCaseViewAutomationPage.requestStatus.assertExist();
          testCaseViewAutomationPage.uuid.assertExist();
          testCaseViewAutomationPage.transmittedOn.assertExist();
          cy.get(`${testCase1ViewPage.rootSelector} [data-test-button-id="test-case-automation-transmit"]`)
            .should('exist');
        });
      });
    });

    it('should check Non Eligible option', () => {
      cy.logInAs('admin', 'admin');

      const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
      testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
        testCaseWorkspace.tree.openNode(projectId);

        let testCase1ViewPage: TestCaseViewPage;
        testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
          testCase1ViewPage = testCaseWorkspace.tree.selectNode(testCaseId1);
          testCase1ViewPage.assertAutomationBlockExist(true);
          const testCaseViewAutomationPage = testCase1ViewPage.clickAnchorLink('automation') as TestCaseViewAutomationPage;
          testCaseViewAutomationPage.setAutomatable('N');
          testCaseViewAutomationPage.checkAutomatableState('N');
          testCaseViewAutomationPage.priority.assertNotExist();
          testCaseViewAutomationPage.requestStatus.assertNotExist();
          testCaseViewAutomationPage.uuid.assertNotExist();
          testCaseViewAutomationPage.transmittedOn.assertNotExist();
          cy.get(`${testCase1ViewPage.rootSelector} [data-test-button-id="test-case-automation-transmit"]`)
            .should('not.exist');
        });
      });
    });
  });

import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
// tslint:disable-next-line:max-line-length
import {
  TestCaseViewVerifiedRequirementsPage
} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-verified-requirements.page';
// tslint:disable-next-line:max-line-length
import {
  TestCaseViewCalledTestCasesPage
} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-called-test-cases.page';
import {ProjectPrerequisiteCommands} from '../../../../page-objects/scenarios-parts/project/project_e2e_commands';
import {TestCaseE2eCommands} from '../../../../page-objects/scenarios-parts/test_case/test_case_e2e_commands';

const projectName = 'project-1';
const projectLabel = 'label';
const projectDescription = 'desc1';

const testCaseName = 'TEST1';
const testCaseRef = 'REF1';
const testCaseDescription = 'DESCRIPTION1';

describe('Check Test Case View - Information', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.createProject(projectName);
    cy.logInAs('admin', 'admin');
    cy.goHome();
    TestCaseE2eCommands.createTestCase(testCaseName, testCaseRef, testCaseDescription, projectName );
  });

  it('should check test case information bloc', () => {
    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', projectName).then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', testCaseName).then(testCaseId1 => {
        const tcPage = testCaseWorkspace.tree.selectNode(testCaseId1) as TestCaseViewPage;

        const infoPanel = tcPage.informationPanel;

        // Check basic info
        tcPage.checkData('test-case-status', 'En cours de rédaction');
        tcPage.checkData('test-case-nature', 'Non définie');
        tcPage.checkData('test-case-type', 'Non défini');
        tcPage.checkData('test-case-kind', 'Classique');
        tcPage.checkData('entity-reference', 'REF1');
        infoPanel.descriptionRichField.checkTextContent('DESCRIPTION1');
        // infoPanel.milestonesField.checkMilestones();

        infoPanel.statusSelectField.checkAllOptions(
          ['En cours de rédaction', 'À approuver', 'Approuvé', 'Obsolète', 'À mettre à jour']
        );

        // Importance field : check initial state
        infoPanel.importanceSelectField.checkSelectedOption('Faible');
        infoPanel.importanceSelectField.assertIsEditable();
        infoPanel.importanceAutoCheckBox.checkState(false);


        // Change basic TC fields
        infoPanel.nameTextField.setAndConfirmValue('Mon super test case');
        infoPanel.referenceTextField.setAndConfirmValue('REF');
        infoPanel.statusSelectField.setAndConfirmValueNoButton('Approuvé');
        infoPanel.natureSelectField.setAndConfirmValueNoButton('Fonctionnel');
        infoPanel.typeSelectField.setAndConfirmValueNoButton('Correctif');

        // Importance field : check modifications
        infoPanel.importanceAutoCheckBox.toggleState();
        infoPanel.importanceAutoCheckBox.checkState(true);
        infoPanel.importanceSelectField.checkSelectedOption('Faible');
        infoPanel.importanceAutoCheckBox.toggleState();
        infoPanel.importanceSelectField.setAndConfirmValueNoButton('Moyenne');
        infoPanel.importanceAutoCheckBox.toggleState();
        infoPanel.importanceAutoCheckBox.checkState(true);
        infoPanel.importanceSelectField.checkSelectedOption('Faible');
        infoPanel.importanceSelectField.assertIsNotEditable();

        const descriptionElement = infoPanel.descriptionRichField;
        descriptionElement.enableEditMode();
        descriptionElement.setValue('Hello, World!');
        descriptionElement.confirm('Hello, World!');
        descriptionElement.enableEditMode();
        descriptionElement.clear();
        descriptionElement.confirm(`Cliquer pour renseigner une description.`);

        // Cancel Change basic TC fields

        infoPanel.nameTextField.setValue('Mon NOUVEAU super test case');
        infoPanel.nameTextField.cancel();
        infoPanel.nameTextField.checkContent('Mon super test case');

        infoPanel.referenceTextField.setValue('NEW');
        infoPanel.referenceTextField.cancel();
        infoPanel.referenceTextField.checkContent('REF');

        infoPanel.statusSelectField.setAndCancel('À approuver', 'Approuvé');
        infoPanel.natureSelectField.setAndCancel('Métier', 'Fonctionnel');
        infoPanel.typeSelectField.setAndCancel('Partenaire', 'Correctif');

        infoPanel.nameTextField.setAndConfirmValue('TEST1');
      });
    });
  });

  it('should check test case verified requirements bloc', () => {
    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', projectName).then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', testCaseName).then(testCaseId1 => {
        const tcPage = testCaseWorkspace.tree.selectNode(testCaseId1) as TestCaseViewPage;
        const verifiedRequirementPanel: TestCaseViewVerifiedRequirementsPage = new TestCaseViewVerifiedRequirementsPage(tcPage);
        // verifiedRequirementPanel.checkTableFieldsNames(['#', 'Projet', 'Référence', 'Libellé', 'Criticité', 'Statut', 'Vérifiée par']);
      });
    });
  });

  it('should check test case called test cases bloc', () => {
    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', projectName).then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', testCaseName).then(testCaseId1 => {
        const tcPage = testCaseWorkspace.tree.selectNode(testCaseId1) as TestCaseViewPage;
        const calledTestCasesPanel: TestCaseViewCalledTestCasesPage = new TestCaseViewCalledTestCasesPage(tcPage);
        calledTestCasesPanel.checkTableFieldsNames(['#', 'Projet', 'Référence', 'Cas de test', 'JDD', 'N° PT']);
      });
    });
  });
});

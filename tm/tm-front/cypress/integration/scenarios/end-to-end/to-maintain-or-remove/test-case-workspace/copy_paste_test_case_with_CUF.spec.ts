import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  AdminWorkspaceCustomFieldsPage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-custom-fields.page';
import {
  AdminWorkspaceProjectsPage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {ProjectViewPage} from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import {ProjectPrerequisiteCommands} from '../../../../page-objects/scenarios-parts/project/project_e2e_commands';
// tslint:disable-next-line:max-line-length
import {
  CustomFieldsPanelElement
} from '../../../../page-objects/pages/administration-workspace/project-view/panels/project-custom-fields-panel.element';
import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {TestCaseE2eCommands} from '../../../../page-objects/scenarios-parts/test_case/test_case_e2e_commands';
import {ConfirmInterProjectPaste} from '../../../../page-objects/elements/dialog/confirm-inter-project-paste';


describe('should create CUF / associate CUF / should create test case - prerequisite', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();

  });

  it('should create CUF / associate CUF / should create test case - prerequisite', () => {
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.createProjects(['project-1', 'project-2', 'project-3']);
    cy.logInAs('admin', 'admin');

    const adminWorkspaceCUFPage = NavBarElement.navigateToAdministration<AdminWorkspaceCustomFieldsPage>('entities-customization');

    let createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Case à cocher', 'cocher', 'cuf1', 'Case à cocher', 'Vrai');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createNonOptionalCUF('Date', 'Date', 'cuf3', 'Date');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Tag', 'Tag', 'cuf4', 'Tag');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Numérique', 'Numérique', 'cuf5', 'Numérique', 5);

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Texte Simple', 'Simple', 'cuf6', 'Texte simple', 'texte');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Texte riche', 'riche', 'cuf2', 'Texte riche', 'riche');

  });

// 'Associate CUF '


interface Datatype {
  entity: string;
  cuf: string;
  project: string;
}

const dataSets: Datatype[] =
  [ {entity: 'Cas de test', cuf: 'Case à cocher', project: 'project-1'},
    {entity: 'Cas de test', cuf: 'Date', project: 'project-1'},
    {entity: 'Cas de test', cuf: 'Tag', project: 'project-1'},
    {entity: 'Pas de test', cuf: 'Numérique', project: 'project-1'},
    {entity: 'Pas de test', cuf: 'Texte Simple', project: 'project-1'},
    {entity: 'Pas de test', cuf: 'Texte riche', project: 'project-1'},

    {entity: 'Cas de test', cuf: 'Case à cocher', project: 'project-2'},
    {entity: 'Cas de test', cuf: 'Date', project: 'project-2'},
    {entity: 'Cas de test', cuf: 'Tag', project: 'project-2'},
    {entity: 'Pas de test', cuf: 'Numérique', project: 'project-2'},
    {entity: 'Pas de test', cuf: 'Texte Simple', project: 'project-2'},
    {entity: 'Pas de test', cuf: 'Texte riche', project: 'project-2'},

    {entity: 'Cas de test', cuf: 'Tag', project: 'project-3'}
  ];


dataSets.forEach((data, index) => runTest(data, index));

function runTest(data: Datatype, index: number) {
  it(`Dataset ${index} - For entity : ${data.entity}, cuf: ${data.cuf} - to project : ${data.project}`,
    () => {
      cy.logInAs('admin', 'admin');
      const adminWorkspaceProjectsPage =
        NavBarElement.navigateToAdministration<AdminWorkspaceProjectsPage>('projects');
      adminWorkspaceProjectsPage.grid.findRowId('name', `${data.project}`).then(projectId => {
        adminWorkspaceProjectsPage.grid.selectRow(projectId, 'name');
        const projectView = new ProjectViewPage();
        const customFieldsPanelElement = projectView.clickAnchorLink('custom-fields') as CustomFieldsPanelElement;
        const bindCustomFieldToProjectDialog = customFieldsPanelElement.openCustomFieldBindingDialog();
        bindCustomFieldToProjectDialog.selectEntity(`${data.entity}`);
        bindCustomFieldToProjectDialog.selectCuf(`${data.cuf}`);
        bindCustomFieldToProjectDialog.confirm();
      });
    });
}

it('should create test case', () => {
  cy.logInAs('admin', 'admin');
  const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
  testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
    testCaseWorkspace.tree.selectNode(projectId);
  });
  const createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
  createTestCaseDialog.fillFieldsWithoutCuf('TEST1', 'REF1', 'DESCRIPTION1');
  createTestCaseDialog.addForSuccessOpen();
  testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId => {
    const testCaseViewPage = new TestCaseViewPage(testCaseId);
    TestCaseE2eCommands.findCufId('Tag').then(cufId => {
      cy.log('cufID :' + cufId);
      cy.wrap(cufId).should('eq', 'Tag');
    });
  });
});
});


describe('Copy / Paste test case with CUF', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });

  it('should copy/paste in same project', () => {

    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        testCaseWorkspace.tree.selectNode(testCaseId1);
        testCaseWorkspace.treeMenu.copy();
        cy.clickVoid();
        testCaseWorkspace.tree.selectNode(projectId);
        testCaseWorkspace.pasteForSuccess();
        cy.clickVoid();
        testCaseWorkspace.tree.findRowId('NAME', 'TEST1' + '-Copie1').then(testCaseIdCopy => {
          testCaseWorkspace.tree.selectNode(testCaseIdCopy);
        });
      });
    });
  });

  it('should copy/paste in different project with same cuf', () => {

    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        testCaseWorkspace.tree.selectNode(testCaseId1);
        testCaseWorkspace.treeMenu.copy();
        cy.clickVoid();
        testCaseWorkspace.tree.findRowId('NAME', 'project-2').then(projectId2 => {
          testCaseWorkspace.tree.selectNode(projectId2);
          // testCaseWorkspace.pasteForSuccess();
          testCaseWorkspace.clickPasteButton();
          const confirmInterProjectMove = new ConfirmInterProjectPaste('confirm-inter-project-paste', 'test-case-tree' );
          confirmInterProjectMove.confirm('*');
          cy.clickVoid();
        });
      });
    });
    testCaseWorkspace.tree.findRowId('NAME', 'project-2').then(projectId2 => {
     // testCaseWorkspace.tree.selectNode(projectId2);
      testCaseWorkspace.tree.findRowIdWithProjectName('NAME', 'TEST1', 'mainViewport', 'project-2').then(testCaseId1 => {
        testCaseWorkspace.tree.selectNode(testCaseId1);
      });
    });
  });

  it('should copy/paste in different project with different cuf', () => {

    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        testCaseWorkspace.tree.selectNode(testCaseId1);
        testCaseWorkspace.treeMenu.copy();
        cy.clickVoid();
        testCaseWorkspace.tree.findRowId('NAME', 'project-3').then(projectId3 => {
          testCaseWorkspace.tree.selectNode(projectId3);
          testCaseWorkspace.clickPasteButton();
          const confirmInterProjectMove = new ConfirmInterProjectPaste('confirm-inter-project-paste', 'test-case-tree' );
          confirmInterProjectMove.confirm('*');
          cy.clickVoid();
        });
      });
    });
    testCaseWorkspace.tree.findRowId('NAME', 'project-3').then(projectId3 => {
      // testCaseWorkspace.tree.openNode(projectId3);
      testCaseWorkspace.tree.findRowIdWithProjectName('NAME', 'TEST1', 'mainViewport', 'project-3').then(testCaseId1 => {
        testCaseWorkspace.tree.selectNode(testCaseId1);
      });
    });
  });
});


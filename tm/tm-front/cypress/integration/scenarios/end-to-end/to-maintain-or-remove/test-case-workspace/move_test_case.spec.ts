import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {ProjectPrerequisiteCommands} from '../../../../page-objects/scenarios-parts/project/project_e2e_commands';

describe('should create projects / test cases / folders - prerequisite ', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.createProjects(['project-1', 'project-2']);
  });

  it('test cases / folders', () => {
    cy.logInAs('admin', 'admin');
    let testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
    });
    const createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
    createTestCaseDialog.fillFieldsWithoutCuf('TEST1', 'REF1', 'DESCRIPTION1');
    createTestCaseDialog.clickOnAddAnotherButton();
    createTestCaseDialog.fillFieldsWithoutCuf('TEST2', 'REF2', 'DESCRIPTION2');
    createTestCaseDialog.clickOnAddButton();

    // should create folders

    cy.goHome();

    testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
      let createTestCaseFolderDialog = testCaseWorkspace.treeMenu.openCreateFolder();
      createTestCaseFolderDialog.fillName('folder1');
      createTestCaseFolderDialog.addWithOptions({ parentRowIsClosed: false, addAnother: false });
      testCaseWorkspace.tree.selectNode(projectId);
      createTestCaseFolderDialog = testCaseWorkspace.treeMenu.openCreateFolder();
      createTestCaseFolderDialog.fillName('folder2');
      createTestCaseFolderDialog.addWithOptions({ parentRowIsClosed: false, addAnother: false });
    });

    testCaseWorkspace.tree.findRowId('NAME', 'project-2').then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
      let createTestCaseFolderDialog = testCaseWorkspace.treeMenu.openCreateFolder();
      createTestCaseFolderDialog.fillName('folder12');
      createTestCaseFolderDialog.addWithOptions({ parentRowIsClosed: false, addAnother: false });
      testCaseWorkspace.tree.selectNode(projectId);
      createTestCaseFolderDialog = testCaseWorkspace.treeMenu.openCreateFolder();
      createTestCaseFolderDialog.fillName('folder22');
      createTestCaseFolderDialog.addWithOptions({ parentRowIsClosed: false, addAnother: false });
    });
  });
});
describe('Move test case', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });

  it('should move tc from project root to folder', () => {

    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        testCaseWorkspace.tree.findRowId('NAME', 'folder1').then(folderId1 => {
          testCaseWorkspace.tree.selectNode(testCaseId1);
          testCaseWorkspace.tree.beginDragAndDrop(testCaseId1);
          testCaseWorkspace.tree.dragOverCenter(folderId1);
          testCaseWorkspace.tree.drop(folderId1);
          testCaseWorkspace.tree.openNode(folderId1);
          testCaseWorkspace.tree.assertNodeExist(testCaseId1);
        });
      });
    });
  });

  it('should move tc from folder to another folder', () => {

    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'folder1').then(folderId1 => {
        testCaseWorkspace.tree.openNode(folderId1);
        testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
          testCaseWorkspace.tree.findRowId('NAME', 'folder2').then(folderId2 => {
            testCaseWorkspace.tree.selectNode(testCaseId1);
            testCaseWorkspace.tree.beginDragAndDrop(testCaseId1);
            testCaseWorkspace.tree.dragOverCenter(folderId2);
            testCaseWorkspace.tree.drop(folderId2);
            testCaseWorkspace.tree.openNode(folderId2);
            testCaseWorkspace.tree.assertNodeExist(testCaseId1);
          });
        });
      });
    });
  });

  it('should move tc from folder to project root', () => {

    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'folder2').then(folderId2 => {
        testCaseWorkspace.tree.openNode(folderId2);
        testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
          testCaseWorkspace.tree.selectNode(testCaseId1);
          testCaseWorkspace.tree.beginDragAndDrop(testCaseId1);
          testCaseWorkspace.tree.dragOverCenter(projectId);
          testCaseWorkspace.tree.drop(projectId);
          testCaseWorkspace.tree.assertNodeExist(testCaseId1);
        });
      });
    });
  });

  it('should move tc from 1 project to another project root', () => {
    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        testCaseWorkspace.tree.selectNode(testCaseId1);
        testCaseWorkspace.tree.beginDragAndDrop(testCaseId1);
        testCaseWorkspace.tree.findRowId('NAME', 'project-2').then(project2Id => {
          testCaseWorkspace.tree.dragOverCenter(project2Id);
          const confirmInterProjectMove = testCaseWorkspace.tree.dropIntoOtherProject(project2Id);
          confirmInterProjectMove.confirm(project2Id);
          testCaseWorkspace.tree.openNode(project2Id);
          testCaseWorkspace.tree.assertNodeExist(testCaseId1);
        });
      });
    });
  });

  it('should move tc from folder to another project folder', () => {

    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'folder1').then(folderId1 => {
        testCaseWorkspace.tree.findRowId('NAME', 'project-2').then(project2Id => {
          testCaseWorkspace.tree.openNode(project2Id);
          testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
            testCaseWorkspace.tree.findRowId('NAME', 'folder22').then(folderId22 => {
              testCaseWorkspace.tree.selectNode(testCaseId1);
              testCaseWorkspace.tree.beginDragAndDrop(testCaseId1);
              testCaseWorkspace.tree.dragOverCenter(folderId22);
              testCaseWorkspace.tree.drop(folderId22);
              testCaseWorkspace.tree.openNode(folderId22);
              testCaseWorkspace.tree.assertNodeExist(testCaseId1);
              testCaseWorkspace.tree.beginDragAndDrop(testCaseId1);
              testCaseWorkspace.tree.dragOverCenter(folderId1);
              const confirmInterProjectMove = testCaseWorkspace.tree.dropIntoOtherProject(folderId1);
              confirmInterProjectMove.confirm(folderId1);
              testCaseWorkspace.tree.openNode(folderId1);
              testCaseWorkspace.tree.assertNodeExist(testCaseId1);
            });
          });
        });
      });
    });
  });
});

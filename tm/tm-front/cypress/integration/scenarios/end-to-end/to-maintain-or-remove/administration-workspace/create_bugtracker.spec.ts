import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  AdminWorkspaceBugtrackersPage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-bugtrackers.page';

describe('Create Bugtracker', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
  });

  it('should check error messages ', () => {
    cy.task('cleanDatabase');
    cy.logInAs('admin', 'admin');

    const adminWorkspaceBugtrackersPage = NavBarElement.navigateToAdministration<AdminWorkspaceBugtrackersPage>('servers');
    const createBugtracker = adminWorkspaceBugtrackersPage.openCreateBugtracker();

    createBugtracker.clickOnAddButton();
    createBugtracker.checkIfRequiredErrorMessageIsDisplayed();
    createBugtracker.fillName('BUGtrack');
    createBugtracker.clickOnAddButton();
    createBugtracker.checkIfRequiredErrorMessageIsDisplayed();
    createBugtracker.fillUrl('wrongURL');
    createBugtracker.clickOnAddButton();
    createBugtracker.checkIfMalformedUrlErrorMessageIsDisplayed();

  });

  it('should create bugtracker ', () => {
    cy.task('cleanDatabase');
    cy.logInAs('admin', 'admin');

    let adminWorkspaceBugtrackersPage = NavBarElement.navigateToAdministration<AdminWorkspaceBugtrackersPage>('servers');
    const createBugtracker = adminWorkspaceBugtrackersPage.openCreateBugtracker();

    createBugtracker.createBugtracker('BUGGY', 'mantis', 'http://example.com');

    // should delete bugtracker
    cy.logInAs('admin', 'admin');
    cy.viewport(1600, 900);
    adminWorkspaceBugtrackersPage = NavBarElement.navigateToAdministration<AdminWorkspaceBugtrackersPage>('servers');
    adminWorkspaceBugtrackersPage.deleteRowWithMatchingCellContent('name', 'BUGGY', true);

  });
});

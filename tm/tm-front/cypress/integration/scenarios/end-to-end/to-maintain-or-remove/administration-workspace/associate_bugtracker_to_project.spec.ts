import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  AdminWorkspaceBugtrackersPage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-bugtrackers.page';
import {
  AdminWorkspaceProjectsPage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {ProjectViewPage} from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';
import {ProjectPrerequisiteCommands} from '../../../../page-objects/scenarios-parts/project/project_e2e_commands';

describe('Associate Bugtracker To Project', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.createProject('project-1');
  });

  it('should create / associate bugtracker ', () => {
    cy.logInAs('admin', 'admin');

    const adminWorkspaceBugtrackersPage = NavBarElement.navigateToAdministration<AdminWorkspaceBugtrackersPage>('servers');
    const createBugtracker = adminWorkspaceBugtrackersPage.openCreateBugtracker();

    createBugtracker.createBugtracker('BUGGY', 'mantis', 'http://example.com');

    // associate bugtracker'
    cy.goHome();
    ProjectPrerequisiteCommands.verifyProject('project-1');
    cy.goHome();
    const adminWorkspaceProjectsPage = NavBarElement.navigateToAdministration<AdminWorkspaceProjectsPage>('projects');
    adminWorkspaceProjectsPage.grid.findRowId('name', 'project-1').then(projectId => {
      adminWorkspaceProjectsPage.grid.selectRow(projectId, undefined, 'leftViewport');
      const projectView = new ProjectViewPage();
      projectView.changeBugtracker('BUGGY');
    });
  });
});

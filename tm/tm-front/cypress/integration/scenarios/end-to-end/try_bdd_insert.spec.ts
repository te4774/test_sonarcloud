import {AdminPrerequisiteBuilder, ProjectBuilder, UserBuilder} from '../../utils/end-to-end/admin-prerequisite-builder';

describe('Create test case', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    new AdminPrerequisiteBuilder()
      .withProjects([
        new ProjectBuilder(),
        new ProjectBuilder(),
        new ProjectBuilder()
      ])
      .withUsers([
        new UserBuilder()
          .withProfileOnProject(1, 'ProjectManager')
          .withProfileOnProject(3, 'TestDesigner'),
        new UserBuilder()
          .withProfileOnProject(2, 'AutomatedTestWriter')
      ])
      .build();
  });

  it('should create test case', () => {
    cy.logInAs('user-2', 'admin');
  });
});

import {TestStepsViewPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import {ActionStepElement} from '../../../page-objects/elements/test-steps/action-step.element';
import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {TestCaseViewPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {ProjectPrerequisiteCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';
import {TestCaseE2eCommands} from '../../../page-objects/scenarios-parts/test_case/test_case_e2e_commands';
/// <reference types="../support" />
/// <reference types="cypress" />
// @ts-check

const projectName = 'project-1';
const projectLabel = 'label';
const projectDescription = 'desc1';

const testCaseName = 'TEST1';
const testCaseRef = 'REF1';
const testCaseDescription = 'DESCRIPTION1';

describe('Add test steps to a test case', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.createProject(projectName);
    cy.logInAs('admin', 'admin');
    TestCaseE2eCommands.createTestCase(testCaseName, testCaseRef, testCaseDescription, projectName);
  });

  it('should add step', () => {
    cy.viewport(1200, 720);
    cy.goHome();
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', projectName).then(projectId => {
      testCaseWorkspace.tree.openNodeIfClosed(projectId);
      testCaseWorkspace.tree.findRowId('NAME', testCaseName).then(testCaseId1 => {
        testCaseWorkspace.tree.selectNode(projectId);
        const testCase1ViewPage = testCaseWorkspace.tree.selectNode(testCaseId1) as TestCaseViewPage;
        testCase1ViewPage.checkNameReferenceAndDescription(testCaseName, testCaseRef, testCaseDescription);

        const testCase1testStepPage = testCase1ViewPage.clickAnchorLink('steps') as TestStepsViewPage;

        testCase1testStepPage.fillPrerequisiteFieldAndCancel('A annuler');
        testCase1testStepPage.fillPrerequisiteFieldAndConfirm('Un prérequis');

        const actionStep1: ActionStepElement = testCase1testStepPage.getActionStepByIndex(0);
        actionStep1.fillEmptyStep('FIRST ACTION', 'action');
        actionStep1.fillEmptyStep('FIRST RESULT', 'result');
        actionStep1.clickAddAnother();

        const actionStep2: ActionStepElement = testCase1testStepPage.getActionStepByIndex(1);
        actionStep2.fillEmptyStep('2nd ACTION', 'action');
        actionStep2.fillEmptyStep('2nd RESULT', 'result');
        actionStep2.clickAdd();

        actionStep2.clickActionStepButtonAdd();
        const actionStep3: ActionStepElement = testCase1testStepPage.getActionStepByIndex(2);
        actionStep3.fillEmptyStep('3rd ACTION', 'action');
        actionStep3.fillEmptyStep('3rd RESULT', 'result');
        actionStep3.clickAdd();
        actionStep3.modifyFilledStep('MODIFIED ACTION', 'action');
        actionStep3.modifyFilledStep('MODIFIED RESULT', 'result');

        actionStep3.clickActionStepButtonAdd();
        const actionStep4 = testCase1testStepPage.getActionStepByIndex(3);
        actionStep4.fillEmptyStep('to cancel', 'action');
        actionStep4.fillEmptyStep('to cancel', 'result');
        actionStep4.clickCancel();

        actionStep3.clickActionStepButtonCopy();
        actionStep3.clickActionStepButtonPaste();
        actionStep3.clickActionStepButtonCopy();
        actionStep3.clickActionStepButtonPaste();

        actionStep3.clickActionStepButtonDeleteAndCancel();
        actionStep3.clickActionStepButtonDeleteAndConfirm();

        actionStep3.toggleStepSelection();
        actionStep3.clickActionStepButtonDeleteAndConfirm();

        actionStep1.checkIsExpended();
        actionStep1.collapseStep();
        actionStep1.extendStep();
      });
    });
  });
});

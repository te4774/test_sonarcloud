import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  CreateCampaignDialog
} from '../../../page-objects/pages/campaign-workspace/dialogs/create-campaign-dialog.element';
import {
  CreateIterationDialog
} from '../../../page-objects/pages/campaign-workspace/dialogs/create-iteration-dialog.element';
import {IterationViewPage} from '../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import {IterationTestPlanPage} from '../../../page-objects/pages/campaign-workspace/iteration/iteration-test-plan.page';
import {ProjectPrerequisiteCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';

describe('Create / Associate Test case - prerequisite', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.createProject('project-1');
  });

  it('should create test case', () => {
    cy.logInAs('admin', 'admin');
    cy.goHome();
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
    });
    const createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
    createTestCaseDialog.fillFieldsWithoutCuf('TEST1', 'REF1', 'DESCRIPTION1');
    createTestCaseDialog.clickOnAddAnotherButton();
    createTestCaseDialog.fillFieldsWithoutCuf('TEST2', 'REF2', 'DESCRIPTION2');
    createTestCaseDialog.clickOnAddAnotherButton();
    createTestCaseDialog.fillFieldsWithoutCuf('TEST3', 'REF3', 'DESCRIPTION3');
    createTestCaseDialog.clickOnAddButton();


    // should create an iteration and associate a test case'
    const TEST1_NAME = 'TEST1';
    cy.logInAs('admin', 'admin');

    const campaignWorkspace = NavBarElement.navigateToCampaignWorkspace();
    campaignWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      campaignWorkspace.tree.selectNode(projectId);
    });
    const createCampaignDialog: CreateCampaignDialog = campaignWorkspace.openCreateCampaign();

    createCampaignDialog.fillName('CAMPAIGN1');
    createCampaignDialog.addWithOptions({
      addAnother: false,
      parentRowIsClosed: false,
      parentRowRef: '',
    });

    campaignWorkspace.tree.findRowId('NAME', 'CAMPAIGN1').then(campaignId => {
      const createIterationDialog: CreateIterationDialog = campaignWorkspace.openCreateIteration();
      createIterationDialog.fillName('ITERATION');
      createIterationDialog.addWithOptions({
        parentRowIsClosed: true,
      });

      campaignWorkspace.tree.findRowId('NAME', 'ITERATION').then(iterationId => {
        campaignWorkspace.tree.selectNode(campaignId);
        const iteration1viewPage =  campaignWorkspace.tree.selectNode(iterationId) as IterationViewPage;
        let iterationTestPlanPage: IterationTestPlanPage;
        iterationTestPlanPage = iteration1viewPage.clickAnchorLink('plan-exec') as IterationTestPlanPage;
        const drawerTreeElement = iterationTestPlanPage.openTestCaseDrawer();
        cy.clickVoid();
        drawerTreeElement.findRowId('NAME', 'project-1').then(projectId => {
          drawerTreeElement.openNode(projectId);
          drawerTreeElement.findRowId('NAME', TEST1_NAME).then(testCaseId => {
            drawerTreeElement.selectRows([testCaseId], 'NAME');
            drawerTreeElement.beginDragAndDrop(testCaseId);
            iterationTestPlanPage.dropIntoTestPlanNoMock();
            iterationTestPlanPage.closeTestCaseDrawer();
          });
        });
      });
    });
  });
});

describe('Delete test case', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });

  it('should delete test case present in iteration', () => {

    cy.logInAs('admin', 'admin');

    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId => {
        testCaseWorkspace.tree.selectNode(testCaseId);
        testCaseWorkspace.clickDeleteButton(false);
        cy.get('[data-test-toolbar-button-id="delete-button"]')
          .find('i')
          .click();
        cy.get('[data-test-dialog-id="confirm-delete"]')
          .contains('li', 'est référencé dans au moins une itération. Après sa suppression, il ne pourra plus être exécuté.');
      });
    });
  });

  it('should delete multiple test cases', () => {

    cy.logInAs('admin', 'admin');

    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        testCaseWorkspace.tree.selectNode(testCaseId1);
        testCaseWorkspace.clickDeleteButton(true);
      });
    });
    testCaseWorkspace.tree.findRowId('NAME', 'TEST2').then(testCaseId2 => {
      testCaseWorkspace.tree.findRowId('NAME', 'TEST3').then(testCaseId3 => {
        testCaseWorkspace.tree.selectNode(testCaseId2);
        testCaseWorkspace.tree.addNodeToSelection(testCaseId3);
        testCaseWorkspace.clickDeleteButton(true);
      });
    });
  });
});

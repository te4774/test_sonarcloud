import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {TestCaseViewPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {ProjectPrerequisiteCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';
import {EMPTY_FIELD_ERROR} from '../../../page-objects/elements/forms/error.message';

describe('Create test case', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.createProject('project-1');
  });

  it('should create test case', () => {
    cy.logInAs('admin', 'admin');
    cy.goHome();
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    new NavBarElement().toggle();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);

      const createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
      createTestCaseDialog.checkCreateTestCaseDialogButtons();

      createTestCaseDialog.checkIfFormIsEmpty(false);

      createTestCaseDialog.clickOnAddButton();
      createTestCaseDialog.assertNameHasError(EMPTY_FIELD_ERROR);

      createTestCaseDialog.clickOnAddAnotherButton();
      createTestCaseDialog.assertNameHasError(EMPTY_FIELD_ERROR);

      createTestCaseDialog.fillFieldsWithoutCuf('TEST1', 'REF1', 'DESCRIPTION1');
      createTestCaseDialog.clickOnAddAnotherButton();
      createTestCaseDialog.checkIfFormIsEmpty(false);

      createTestCaseDialog.fillFieldsWithoutCuf('TEST2', 'REF2', 'DESCRIPTION2');
      createTestCaseDialog.clickOnAddButton();

      let testCase1ViewPage: TestCaseViewPage;
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        testCase1ViewPage = testCaseWorkspace.tree.selectNode(testCaseId1);
        testCase1ViewPage.checkNameReferenceAndDescription('TEST1', 'REF1', 'DESCRIPTION1');
        testCaseWorkspace.tree.selectNode(projectId);
        let testCase2ViewPage: TestCaseViewPage;
        testCaseWorkspace.tree.findRowId('NAME', 'TEST2').then(testCaseId2 => {
          testCase2ViewPage = testCaseWorkspace.tree.selectNode(testCaseId2);
          testCase2ViewPage.checkNameReferenceAndDescription('TEST2', 'REF2', 'DESCRIPTION2');
        });
      });
    });
  });
});

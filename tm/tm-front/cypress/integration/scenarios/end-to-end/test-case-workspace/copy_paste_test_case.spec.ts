import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  TestCaseFolderViewPage
} from '../../../page-objects/pages/test-case-workspace/test-case-folder/test-case-folder-view.page';
import {
  TestCaseLibraryViewPage
} from '../../../page-objects/pages/test-case-workspace/test-case/test-case-library-view.page';
import {ProjectPrerequisiteCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';

describe('Create test case / folder - prerequisite', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.createProject('project-1');
  });

  it('should create test case and folders', () => {
    cy.logInAs('admin', 'admin');
    let testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
    });
    const createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
    createTestCaseDialog.fillFieldsWithoutCuf('TEST1', 'REF1', 'DESCRIPTION1');
    createTestCaseDialog.clickOnAddAnotherButton();
    createTestCaseDialog.fillFieldsWithoutCuf('TEST2', 'REF2', 'DESCRIPTION2');
    createTestCaseDialog.clickOnAddButton();

    cy.goHome();

    testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
      let createTestCaseFolderDialog = testCaseWorkspace.treeMenu.openCreateFolder();
      createTestCaseFolderDialog.fillName('folder1');
      createTestCaseFolderDialog.addWithOptions({ parentRowIsClosed: false, addAnother: false });
      testCaseWorkspace.tree.selectNode(projectId);
      createTestCaseFolderDialog = testCaseWorkspace.treeMenu.openCreateFolder();
      createTestCaseFolderDialog.fillName('folder2');
      createTestCaseFolderDialog.addWithOptions({ parentRowIsClosed: false, addAnother: false });
    });
  });
});

describe('Copy / Paste test case', function () {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });

  it('should copy / paste test cases', () => {

    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        testCaseWorkspace.tree.selectNode(testCaseId1);
        testCaseWorkspace.treeMenu.copy();
        cy.clickVoid();
        testCaseWorkspace.tree.selectNode(projectId);
        testCaseWorkspace.pasteForSuccess();
        cy.clickVoid();
        testCaseWorkspace.tree.findRowId('NAME', 'TEST1' + '-Copie1').then(testCaseIdCopy => {
          testCaseWorkspace.tree.selectNode(testCaseIdCopy);
        });
      });
    });
  });

  it('should copy / paste test cases in a folder', () => {

    cy.logInAs('admin', 'admin');

    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId1 => {
        testCaseWorkspace.tree.selectNode(testCaseId1);
        testCaseWorkspace.clickCopyButton();
        cy.clickVoid();
        testCaseWorkspace.tree.findRowId('NAME', 'folder1').then(folderId => {
          testCaseWorkspace.tree.selectNode(folderId);
          testCaseWorkspace.pasteForSuccess();
          cy.clickVoid();
          cy.reload();
          const testCaseFolderViewPage = testCaseWorkspace.tree.selectNode(folderId) as TestCaseFolderViewPage;
          testCaseFolderViewPage.checkNumberOfTestCases(1);
          const testCaseLibraryView =  testCaseWorkspace.tree.selectNode(projectId) as TestCaseLibraryViewPage;
          testCaseLibraryView.checkNumberOfTestCases(4);
        });
      });
    });
  });
});

import {TestCaseWorkspacePage} from '../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {TestCaseSearchPage} from '../../../page-objects/pages/test-case-workspace/research/test-case-search-page';
import {TextFilterWidgetElement} from '../../../page-objects/elements/filters/text-filter-widget.element';
import {TestStepsViewPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import {TestCaseViewPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {GroupedMultiListElement} from '../../../page-objects/elements/filters/grouped-multi-list.element';
import {ProjectPrerequisiteCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';

describe('Research a test case', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.createProject('project-1');
  });

  it('should create test cases', () => {
    cy.logInAs('admin', 'admin');

    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
    });
    let createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
    createTestCaseDialog.fillFieldsWithoutCuf('T&ST1', 'REF1', 'D&$CR|PTION_1');
    createTestCaseDialog.clickOnAddButton();

    createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
    createTestCaseDialog.fillFieldsWithoutCuf('TEST2', 'REF2', 'DESCRIPTION2');
    createTestCaseDialog.clickOnAddButton();

    createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
    createTestCaseDialog.fillFieldsWithoutCuf('TEST3', 'REF3', 'DESCRIPTION3');
    createTestCaseDialog.clickOnAddButton();

    testCaseWorkspace.tree.findRowId('NAME', 'T&ST1').then(testCaseId1 => {
      const testCase1ViewPage = testCaseWorkspace.tree.selectNode(testCaseId1) as TestCaseViewPage;
      const testCase1testStepPage = testCase1ViewPage.clickAnchorLink('steps') as TestStepsViewPage;
      testCase1testStepPage.fillPrerequisiteFieldAndConfirm('LE prérequis');
    });

    // should research test cases
    cy.goHome();

    let testCaseWorkspacePage: TestCaseWorkspacePage;
    testCaseWorkspacePage = NavBarElement.navigateToTestCaseWorkspace() as TestCaseWorkspacePage;
    const testCaseResearchPage = testCaseWorkspacePage.clickSearchButton() as TestCaseSearchPage;

    // search by Name
    testCaseResearchPage.clickAddCriteria();
    testCaseResearchPage.selectCriteriaByLabel('Nom', false);
    let textFilterWidgetElement = new TextFilterWidgetElement() ;
    textFilterWidgetElement.assertExist();
    textFilterWidgetElement.typeAndConfirm('T&ST1');
    testCaseResearchPage.selectRowsWithMatchingCellContent('name', ['T&ST1']);

    // search by Reference
    testCaseResearchPage.clickAddCriteria();
    testCaseResearchPage.selectCriteriaByLabel('Référence', false);
    textFilterWidgetElement = new TextFilterWidgetElement() ;
    textFilterWidgetElement.assertExist();
    textFilterWidgetElement.typeAndConfirm('REF1');
    testCaseResearchPage.selectRowsWithMatchingCellContent('reference', ['REF1']);

    // search by Pre requisite
    testCaseResearchPage.clickAddCriteria();
    testCaseResearchPage.selectCriteriaByLabel('Prérequis', false);
    textFilterWidgetElement = new TextFilterWidgetElement() ;
    textFilterWidgetElement.assertExist();
    textFilterWidgetElement.typeAndConfirm('LE prérequis');
    testCaseResearchPage.selectRowsWithMatchingCellContent('reference', ['REF1']);

    // search by Format
    testCaseResearchPage.clickAddCriteria();
    testCaseResearchPage.selectCriteriaByLabel('Format', false);
    let groupedMultiListElement = new GroupedMultiListElement() ;
    groupedMultiListElement.assertExist();
    groupedMultiListElement.toggleOneItem('Classique');
    cy.get('body').click();
    testCaseResearchPage.selectRowsWithMatchingCellContent('reference', ['REF1']);

    // search by Statut
    testCaseResearchPage.clickAddCriteria();
    testCaseResearchPage.selectCriteriaByLabel('Statut$', true);
    const groupedMultiListElement2 = new GroupedMultiListElement() ;
    groupedMultiListElement2.assertExist();
    groupedMultiListElement2.toggleOneItem('En cours de rédaction');
    cy.get('body').click();
    testCaseResearchPage.selectRowsWithMatchingCellContent('reference', ['REF1']);

    // search by Description
    testCaseResearchPage.clickAddCriteria();
    testCaseResearchPage.selectCriteriaByLabel('Description', false);
    textFilterWidgetElement = new TextFilterWidgetElement() ;
    textFilterWidgetElement.assertExist();
    textFilterWidgetElement.typeAndConfirm('D&$CR|PTION_1');
    testCaseResearchPage.selectRowsWithMatchingCellContent('reference', ['REF1']);


    // search by Nature
    testCaseResearchPage.clickAddCriteria();
    testCaseResearchPage.selectCriteriaByLabel('Nature', false);
    groupedMultiListElement = new GroupedMultiListElement() ;
    groupedMultiListElement.assertExist();
    groupedMultiListElement.toggleOneItem('Non définie');
    cy.get('body').click();
    testCaseResearchPage.selectRowsWithMatchingCellContent('reference', ['REF1']);

    // search by Importance
    testCaseResearchPage.clickAddCriteria();
    testCaseResearchPage.selectCriteriaByLabel('Importance', false);
    groupedMultiListElement = new GroupedMultiListElement() ;
    groupedMultiListElement.assertExist();
    groupedMultiListElement.toggleOneItem('Faible');
    cy.get('body').click();
    testCaseResearchPage.selectRowsWithMatchingCellContent('reference', ['REF1']);

    // search by Type
    testCaseResearchPage.clickAddCriteria();
    testCaseResearchPage.selectCriteriaByLabel('Type', false);
    groupedMultiListElement = new GroupedMultiListElement() ;
    groupedMultiListElement.assertExist();
    groupedMultiListElement.toggleOneItem('Non défini');
    cy.get('body').click();
    testCaseResearchPage.selectRowsWithMatchingCellContent('reference', ['REF1']);

    // Deleting criteria
    testCaseResearchPage.deleteCriteriaByLabel('Nom');
    testCaseResearchPage.deleteCriteriaByLabel('Référence');
    testCaseResearchPage.deleteCriteriaByLabel('Prérequis');
    testCaseResearchPage.deleteCriteriaByLabel('Format');
    testCaseResearchPage.deleteCriteriaByLabel('Description');
    testCaseResearchPage.deleteCriteriaByLabel('Statut');
    testCaseResearchPage.deleteCriteriaByLabel('Nature');
    testCaseResearchPage.deleteCriteriaByLabel('Importance');
    testCaseResearchPage.deleteCriteriaByLabel('Type');

    testCaseResearchPage.selectRowsWithMatchingCellContent('reference', ['REF1']);
    testCaseResearchPage.selectRowsWithMatchingCellContent('reference', ['REF2']);
    testCaseResearchPage.selectRowsWithMatchingCellContent('reference', ['REF3']);

  });
});

// TODO Deal with Automatician profile
import {TestCaseMenuItemIds} from '../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {TestCaseViewPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {ProjectPrerequisiteCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';

describe('Create Permissions / test cases - prerequisite', () => {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.addAllPermissionsToProject('project-1');
  });

  it('should create test cases - prerequisite', () => {

    cy.logInAs('admin', 'admin');
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(id => {
      testCaseWorkspace.tree.selectNode(id);
      const createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
      createTestCaseDialog.fillFieldsWithoutCuf('TEST1', 'REF1', 'DESCRIPTION1');
      createTestCaseDialog.clickOnAddAnotherButton();
      createTestCaseDialog.checkIfFormIsEmpty(false);

      createTestCaseDialog.fillFieldsWithoutCuf('TEST2', 'REF2', 'DESCRIPTION2');
      createTestCaseDialog.clickOnAddButton();
    });

    let testCase1ViewPage: TestCaseViewPage;
    testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(id => {
      testCase1ViewPage = testCaseWorkspace.tree.selectNode(id);
      testCase1ViewPage.checkNameReferenceAndDescription('TEST1', 'REF1', 'DESCRIPTION1');
    });
  });
});

describe('Check Create / Delete / Copy permissions', () => {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });

  interface Datatype {
    login: string;
    password: string;
    createDeletePossible: boolean;
  }

  const dataSets: Datatype[] =
    [ {login: 'admin', password: 'admin', createDeletePossible: true},
      {login: 'Chef de projet', password: 'admin', createDeletePossible: true},
      {login: 'Designer de tests', password: 'admin', createDeletePossible: true},
      { login: 'Testeur référent', password: 'admin', createDeletePossible: true},

      {login: 'Testeur avancé', password: 'admin', createDeletePossible: false},
      {login: 'Testeur', password: 'admin', createDeletePossible: false},
      {login: 'Invité', password: 'admin', createDeletePossible: false},
      { login: 'Valideur', password: 'admin', createDeletePossible: false}
    ];


  dataSets.forEach((data, index) => runTest(data, index));

  function runTest(data: Datatype, index: number) {
    it(`Dataset ${index} - For profile : ${data.login}, should allow test case creation/deletion/copy: ${data.createDeletePossible}`,
      () => {
        cy.logInAs(data.login, data.password);
        const testCaseWorkspacePage = NavBarElement.navigateToTestCaseWorkspace();
        testCaseWorkspacePage.tree.findRowId('NAME', 'project-1').then(projectId => {
          testCaseWorkspacePage.tree.openNode(projectId);
          testCaseWorkspacePage.tree.selectNode(projectId);
          if (data.createDeletePossible) {
            testCaseWorkspacePage.treeMenu.assertDialogIsActive(TestCaseMenuItemIds.NEW_TEST_CASE);
            testCaseWorkspacePage.treeMenu.assertDialogIsActive(TestCaseMenuItemIds.NEW_FOLDER);
            cy.hideMenu('create-menu');
          } else {
            testCaseWorkspacePage.treeMenu.assertCreateButtonIsDisabled();
          }
          testCaseWorkspacePage.tree.findRowId('NAME', 'TEST1').then(testCaseId => {
            const tc = testCaseWorkspacePage.tree.selectNode(testCaseId) as TestCaseViewPage;
            tc.checkData('entity-name', 'TEST1');
            if (data.createDeletePossible) {
              testCaseWorkspacePage.clickDeleteButton(false);
              testCaseWorkspacePage.treeMenu.copy();
              cy.clickVoid();
              testCaseWorkspacePage.tree.selectNode(projectId);
              testCaseWorkspacePage.pasteForSuccess();
              cy.clickVoid();
            } else {
              testCaseWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
            }

          });
        });
      });
  }
});

describe('Check Modify permissions', () => {
  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });
  interface Datatype {
    login: string;
    password: string;
    modifyRight: boolean;
  }

  const dataSets: Datatype[] =
    [ {login: 'admin', password: 'admin', modifyRight: true},
      {login: 'Chef de projet', password: 'admin', modifyRight: true},
      {login: 'Designer de tests', password: 'admin', modifyRight: true},
      { login: 'Testeur référent', password: 'admin', modifyRight: true},
      {login: 'Testeur avancé', password: 'admin', modifyRight: true},
      { login: 'Valideur', password: 'admin', modifyRight: true},

      {login: 'Testeur', password: 'admin', modifyRight: false},
      {login: 'Invité', password: 'admin', modifyRight: false}

    ];


  dataSets.forEach((data, index) => runTest(data, index));

  function runTest(data: Datatype, index: number) {
    it(`Dataset ${index} - For profile : ${data.login}, should allow test case creation/deletion/copy: ${data.modifyRight}`,
      () => {
        cy.logInAs(data.login, data.password);
        const testCaseWorkspacePage = NavBarElement.navigateToTestCaseWorkspace();
        testCaseWorkspacePage.tree.findRowId('NAME', 'project-1').then(projectId => {
          testCaseWorkspacePage.tree.openNode(projectId);
          testCaseWorkspacePage.tree.selectNode(projectId);
          testCaseWorkspacePage.tree.findRowId('NAME', 'TEST1').then(testCaseId => {
            const tc = testCaseWorkspacePage.tree.selectNode(testCaseId) as TestCaseViewPage;
            if (data.modifyRight) {
              tc.informationPanel.nameTextField.setValue('test');
              tc.informationPanel.nameTextField.cancel();
            } else {
              tc.informationPanel.nameTextField.checkEditMode(false);
            }
          });
        });
      });
  }
});

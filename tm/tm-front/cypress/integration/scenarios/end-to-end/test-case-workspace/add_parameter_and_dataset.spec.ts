import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {TestCaseViewPage} from '../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {
  TestCaseViewParametersPage
} from '../../../page-objects/pages/test-case-workspace/test-case/test-case-view-parameters.page';
import {TestCaseWorkspacePage} from '../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {ProjectPrerequisiteCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';
import {TestCaseE2eCommands} from '../../../page-objects/scenarios-parts/test_case/test_case_e2e_commands';

describe('Add / Modify parameter', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.createProject('project-1');
  });

  it('should add / dataset & parameter', () => {
    cy.logInAs('admin', 'admin');
    let testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
    });

    const createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
    createTestCaseDialog.checkCreateTestCaseDialogButtons();

    createTestCaseDialog.fillFieldsWithoutCuf('TEST1', 'REF1', 'DESCRIPTION1');
    createTestCaseDialog.addWithOptions({ addAnother: false, parentRowIsClosed: false });


    // should add dataset & parameter
    cy.goHome();
    testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace() as TestCaseWorkspacePage;
    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNodeIfClosed(projectId);
      testCaseWorkspace.tree.selectNode(projectId);
    });
    testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId => {
      const testCaseViewPage = testCaseWorkspace.tree.selectNode(testCaseId) as TestCaseViewPage;
      const testCaseViewParametersPage = testCaseViewPage.clickAnchorLink('parameters') as TestCaseViewParametersPage;
      let createDatasetDialogElement = testCaseViewParametersPage.openAddDataSetDialog();

      createDatasetDialogElement.fillName('IAMADATASET');
      createDatasetDialogElement.addDataSet();
      testCaseViewParametersPage.checkExistingDataSet('IAMADATASET');

      cy.goHome();
      NavBarElement.navigateToTestCaseWorkspace();
      createDatasetDialogElement = testCaseViewParametersPage.openAddDataSetDialog();
      createDatasetDialogElement.fillName('IAM2DATASET');
      createDatasetDialogElement.addDataSet();
      testCaseViewParametersPage.checkExistingDataSet('IAM2DATASET');

      cy.goHome();
      TestCaseE2eCommands.addAParameterToTestCase('project-1', 'TEST1', 'IAMPARAM');
    });

    testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
      testCaseWorkspace.tree.openNodeIfClosed(projectId);
      testCaseWorkspace.tree.selectNode(projectId);
      testCaseWorkspace.tree.findRowId('NAME', 'TEST1').then(testCaseId => {
        const testCaseViewPage = testCaseWorkspace.tree.selectNode(testCaseId) as TestCaseViewPage;
        const testCaseViewParametersPage = testCaseViewPage.clickAnchorLink('parameters') as TestCaseViewParametersPage;
        testCaseViewParametersPage.renameParam('IAMPARAM', 'NEWPARAM');
        testCaseViewParametersPage.renameDataSet('IAMADATASET', 'NEWDATASET');
        testCaseViewParametersPage.changeParamValue('NEWDATASET', 'NEWPARAM', 'NEWVALUE');
      });
    });
  });
});

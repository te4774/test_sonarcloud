import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  AdminWorkspaceCustomFieldsPage
} from '../../../page-objects/pages/administration-workspace/admin-workspace-custom-fields.page';
// tslint:disable-next-line:max-line-length
import {
  AdminWorkspaceRequirementsLinksPage
} from '../../../page-objects/pages/administration-workspace/admin-workspace-requirements-links.page';
import {EMPTY_FIELD_ERROR} from '../../../page-objects/elements/forms/error.message';

describe('Create requirement links', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
  });


  it('should check error messages', () => {
    cy.task('cleanDatabase');
    cy.logInAs('admin', 'admin');

    const adminWorkspaceCUFPage = NavBarElement.navigateToAdministration<AdminWorkspaceCustomFieldsPage>('entities-customization');

    const adminWorkspaceRequirementsLinksPage =
      adminWorkspaceCUFPage.clickAnchor('requirements-links', 'requirements-links') as AdminWorkspaceRequirementsLinksPage;

    const createRequirementsLinkDialog = adminWorkspaceRequirementsLinksPage.openCreateRequirementsLink();

    createRequirementsLinkDialog.clickOnAddButton();
    createRequirementsLinkDialog.assertRole1HasError(EMPTY_FIELD_ERROR);
    createRequirementsLinkDialog.assertCode1HasError(EMPTY_FIELD_ERROR);
    createRequirementsLinkDialog.assertRole2HasError(EMPTY_FIELD_ERROR);
    createRequirementsLinkDialog.assertCode2HasError(EMPTY_FIELD_ERROR);
    createRequirementsLinkDialog.fillRole1('role1');
    createRequirementsLinkDialog.clickOnAddButton();
    createRequirementsLinkDialog.assertRole1HasNoError();
    createRequirementsLinkDialog.assertCode1HasError(EMPTY_FIELD_ERROR);
    createRequirementsLinkDialog.assertRole2HasError(EMPTY_FIELD_ERROR);
    createRequirementsLinkDialog.assertCode2HasError(EMPTY_FIELD_ERROR);
    createRequirementsLinkDialog.fillRole2('role2');
    createRequirementsLinkDialog.clickOnAddButton();
    createRequirementsLinkDialog.assertCode1HasError(EMPTY_FIELD_ERROR);
    createRequirementsLinkDialog.assertCode2HasError(EMPTY_FIELD_ERROR);
    createRequirementsLinkDialog.fillRole1Code('CR1');
    createRequirementsLinkDialog.clickOnAddButton();
    createRequirementsLinkDialog.assertCode2HasError(EMPTY_FIELD_ERROR);
    createRequirementsLinkDialog.fillRole2Code('CR2');
  });

  it('should create and delete links', () => {
    cy.task('cleanDatabase');
    cy.logInAs('admin', 'admin');

    const adminWorkspaceCUFPage = NavBarElement.navigateToAdministration<AdminWorkspaceCustomFieldsPage>('entities-customization');

    const adminWorkspaceRequirementsLinksPage =
      adminWorkspaceCUFPage.clickAnchor('requirements-links', 'requirements-links') as AdminWorkspaceRequirementsLinksPage;

    let createRequirementsLinkDialog = adminWorkspaceRequirementsLinksPage.openCreateRequirementsLink();
    createRequirementsLinkDialog.createLink('role1', 'role1Code', 'role2', 'role2Code');

    createRequirementsLinkDialog = adminWorkspaceRequirementsLinksPage.openCreateRequirementsLink();
    createRequirementsLinkDialog.createLink('role12', 'role12Code', 'role22', 'role22Code');

    createRequirementsLinkDialog = adminWorkspaceRequirementsLinksPage.openCreateRequirementsLink();
    createRequirementsLinkDialog.createLink('role13', 'role13Code', 'role23', 'role23Code');

    adminWorkspaceRequirementsLinksPage.grid.findRowId('role', 'Relatif').then(id => {
      adminWorkspaceRequirementsLinksPage.assertLinkIsDefault(id);
    });

    adminWorkspaceRequirementsLinksPage.grid.findRowId('role', 'role1').then(id => {
      adminWorkspaceRequirementsLinksPage.assertLinkIsNotDefault(id);
      adminWorkspaceRequirementsLinksPage.makeLinkDefault(id);
      adminWorkspaceRequirementsLinksPage.assertLinkIsDefault(id);
    });

    adminWorkspaceRequirementsLinksPage.deleteLinkByName(
      'role', 'role1', false, true, 'Le type sélectionné est le type par défaut. Il ne peut donc pas être supprimé.'
    );

    adminWorkspaceRequirementsLinksPage.grid.findRowId('role', 'Relatif').then(id => {
      adminWorkspaceRequirementsLinksPage.assertLinkIsNotDefault(id);
      adminWorkspaceRequirementsLinksPage.makeLinkDefault(id);
      adminWorkspaceRequirementsLinksPage.assertLinkIsDefault(id);
    });

    adminWorkspaceRequirementsLinksPage.selectLinksByContent('role', ['role1', 'role12']);
    adminWorkspaceRequirementsLinksPage.clickOnMultipleDeleteButton(false);

    adminWorkspaceRequirementsLinksPage.deleteLinkByName('role', 'role13', true);

    adminWorkspaceRequirementsLinksPage.selectLinksByContent('role', ['role1', 'role12']);
    adminWorkspaceRequirementsLinksPage.clickOnMultipleDeleteButton(true);

  });

});

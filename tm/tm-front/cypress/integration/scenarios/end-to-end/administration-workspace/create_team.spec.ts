import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {AdminWorkspaceUsersPage} from '../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import {AdminE2eCommands} from '../../../page-objects/scenarios-parts/administration/admin_e2e_commands';
import {NAME_ALREADY_USED_ERROR} from '../../../page-objects/elements/forms/error.message';

describe('Create Team', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
  });

  it('should create team', () => {
    cy.task('cleanDatabase');
    cy.logInAs('admin', 'admin');

    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration('users') as AdminWorkspaceUsersPage;
    const adminWorkspaceTeamsPage = adminWorkspaceUsersPage.goToTeamAnchor();
    let createTeamDialog = adminWorkspaceTeamsPage.openCreateTeam();
    createTeamDialog.createTeam('LEQUIPE', 'first team description');
    createTeamDialog = adminWorkspaceTeamsPage.openCreateTeam();
    createTeamDialog.fillName('LEQUIPE');
    createTeamDialog.addWithServerSideFailure();
    createTeamDialog.assertNameHasError(NAME_ALREADY_USED_ERROR);
    createTeamDialog.cancel();
    createTeamDialog = adminWorkspaceTeamsPage.openCreateTeam();
    createTeamDialog.createTeam('LEQUIPE2', '2nd team description');

    // Leave time to the grid to refresh before selecting the rows
    adminWorkspaceTeamsPage.grid.assertRowCount(2);
    adminWorkspaceTeamsPage.grid.findRowId('name', 'LEQUIPE').then(teamId1 => {
      adminWorkspaceTeamsPage.grid.findRowId('name', 'LEQUIPE2').then(teamId2 => {
        adminWorkspaceTeamsPage.grid.selectRows([teamId1, teamId2], '#', 'leftViewport');
      });
    });
  });
});

describe('Delete Team', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    AdminE2eCommands.createTeam('TEAM1', 'team desc');
    AdminE2eCommands.createTeam('TEAM2', 'team desc');
  });

  it('should delete team', () => {
    cy.logInAs('admin', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration('users') as AdminWorkspaceUsersPage;
    const adminWorkspaceTeamsPage = adminWorkspaceUsersPage.goToTeamAnchor();
    adminWorkspaceTeamsPage.grid.findRowId('name', 'TEAM1').then(teamId1 => {
      adminWorkspaceTeamsPage.grid.findRowId('name', 'TEAM2').then(teamId2 => {
        adminWorkspaceTeamsPage.grid.selectRows([teamId1, teamId2], '#', 'leftViewport');
        adminWorkspaceTeamsPage.clickOnMultipleDeleteButton(false);
        adminWorkspaceTeamsPage.deleteTeamByName('TEAM2', true);
        adminWorkspaceTeamsPage.selectTeamsByName(['TEAM1']);
        adminWorkspaceTeamsPage.clickOnMultipleDeleteButton(true);
      });

    });

  });

});

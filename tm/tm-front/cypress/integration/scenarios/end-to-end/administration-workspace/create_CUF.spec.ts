import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  AdminWorkspaceCustomFieldsPage
} from '../../../page-objects/pages/administration-workspace/admin-workspace-custom-fields.page';

describe('Create CUF', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
  });

  it('should check personalization anchors', () => {
    cy.task('cleanDatabase');
    cy.logInAs('admin', 'admin');
    const adminWorkspaceCUFPage = NavBarElement.navigateToAdministration<AdminWorkspaceCustomFieldsPage>('entities-customization');

  adminWorkspaceCUFPage.clickAnchor('info-lists', 'info-lists');

  adminWorkspaceCUFPage.clickAnchor('custom-fields', 'custom-fields');

  adminWorkspaceCUFPage.clickAnchor('requirements-links', 'requirements-links');

  });

  it('should create CUF', () => {
    cy.task('cleanDatabase');
    cy.logInAs('admin', 'admin');

    const adminWorkspaceCUFPage = NavBarElement.navigateToAdministration<AdminWorkspaceCustomFieldsPage>('entities-customization');

    let createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Case à cocher', 'cuf1', 'cuf1', 'Case à cocher', 'Vrai');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createNonOptionalCUF('Date', 'cuf3', 'cuf3', 'Date');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Tag', 'cuf4', 'cuf4', 'Tag', 'tag');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Numérique', 'cuf5', 'cuf5', 'Numérique', '42');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Texte Simple', 'cuf6', 'cuf6', 'Texte simple');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Texte riche', 'cuf2', 'cuf2', 'Texte riche');

  });

  it('should delete CUF', () => {
    cy.logInAs('admin', 'admin');
    const adminWorkspaceCUFPage = NavBarElement.navigateToAdministration<AdminWorkspaceCustomFieldsPage>('entities-customization');
    adminWorkspaceCUFPage.deleteCUFByName('Date', true);
    adminWorkspaceCUFPage.deleteCUFByName('Tag', true);
    adminWorkspaceCUFPage.deleteCUFByName('Numérique', true);
    adminWorkspaceCUFPage.deleteCUFByName('Texte riche', true);
    adminWorkspaceCUFPage.deleteCUFByName('Case à cocher', true);
    adminWorkspaceCUFPage.deleteCUFByName('Texte Simple', true);

  });


});

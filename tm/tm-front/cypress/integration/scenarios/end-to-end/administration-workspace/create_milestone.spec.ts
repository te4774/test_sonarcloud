import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  AdminWorkspaceMilestonesPage
} from '../../../page-objects/pages/administration-workspace/admin-workspace-milestones.page';
import {SystemE2eCommands} from '../../../page-objects/scenarios-parts/system/system_e2e_commands';
import {DatabaseUtils} from '../../../utils/database.utils';
import {format, startOfDay} from 'date-fns';

describe('Create Milestone', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
  });

  it('should check error Messages ', () => {
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    cy.logInAs('admin', 'admin');

    const adminWorkspaceMilestonesPage = NavBarElement.navigateToAdministration<AdminWorkspaceMilestonesPage>('milestones');
    const createMilestoneDialog = adminWorkspaceMilestonesPage.openCreateMilestone();

    createMilestoneDialog.clickOnAddButton();
    createMilestoneDialog.checkIfRequiredErrorMessageIsDisplayed();
    createMilestoneDialog.fillName('JALON');
    createMilestoneDialog.clickOnAddButton();
    createMilestoneDialog.checkIfRequiredErrorMessageIsDisplayed();

  });

  it('should create Milestone ', () => {
    DatabaseUtils.cleanDatabase();
    SystemE2eCommands.setMilestoneActivated();
    cy.logInAs('admin', 'admin');

    const adminWorkspaceMilestonesPage = NavBarElement.navigateToAdministration<AdminWorkspaceMilestonesPage>('milestones');
    const createMilestoneDialog = adminWorkspaceMilestonesPage.openCreateMilestone();

    const todayAsString = format(startOfDay(new Date()), 'dd/MM/yyyy');
    createMilestoneDialog.createMilestone('Mon JALON');
    adminWorkspaceMilestonesPage.grid.findRowId('label', 'Mon JALON').then(rowId => {
      adminWorkspaceMilestonesPage.grid
        .assertRowExist(rowId, 'leftViewport');

      adminWorkspaceMilestonesPage.grid
        .getRow(rowId)
        .cell('status')
        .textRenderer()
        .assertContainText('Planifié');

      adminWorkspaceMilestonesPage.grid
        .getRow(rowId)
        .cell('endDate')
        .textRenderer()
        .assertContainText(todayAsString);
    });
  });

});

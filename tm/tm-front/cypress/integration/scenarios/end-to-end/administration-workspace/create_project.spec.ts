import {ProjectPrerequisiteCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';
import {
  AdminWorkspaceProjectsPage
} from '../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {NavBarAdminElement} from '../../../page-objects/elements/nav-bar/nav-bar-admin.element';

describe('Create Project', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
  });

  it.skip('should create project', () => {
    cy.logInAs('admin', 'admin');
    const projectsPage = AdminWorkspaceProjectsPage.initTestAtPageProjects();
    const createProjectDialog = projectsPage.openCreateProject();
    createProjectDialog.fillName('project-1');
    createProjectDialog.clickOnAddButton();
    new NavBarAdminElement().toggle();
    cy.goHome();
    ProjectPrerequisiteCommands.verifyProject('project-1');
  });
});

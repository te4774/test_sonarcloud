import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {AdminWorkspaceUsersPage} from '../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import {LoginPage} from '../../../page-objects/pages/login/login-page';
import {UserViewPage} from '../../../page-objects/pages/administration-workspace/user-view/user-view.page';
import {ProjectPrerequisiteCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';
import {AdminE2eCommands} from '../../../page-objects/scenarios-parts/administration/admin_e2e_commands';
import {DatabaseUtils} from '../../../utils/database.utils';
import {EMPTY_FIELD_ERROR, LOGIN_ALREADY_USED_ERROR} from '../../../page-objects/elements/forms/error.message';

const projectName = 'project-1';
const projectLabel = 'label';
const projectDescription = 'desc1';


describe('Create User', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    DatabaseUtils.cleanDatabase();
  });

  it('should create User', () => {
    cy.logInAs('admin', 'admin');

    let adminWorkspaceUsersPage = NavBarElement.navigateToAdministration<AdminWorkspaceUsersPage>('users');
    let createUserDialog = adminWorkspaceUsersPage.openCreateUser();
    createUserDialog.createUser('2User', 'firstName', 'lastName', 'email@email.com', 'Utilisateur', 'password', 'password');
    createUserDialog = adminWorkspaceUsersPage.openCreateUser();
    createUserDialog.addWithClientSideFailure();
    createUserDialog.assertLoginFieldHasError(EMPTY_FIELD_ERROR);
    createUserDialog.assertLastNameFieldHasError(EMPTY_FIELD_ERROR);
    createUserDialog.assertPasswordFieldHasError(EMPTY_FIELD_ERROR);
    createUserDialog.assertPasswordConfirmationFieldHasError(EMPTY_FIELD_ERROR);
    createUserDialog.fillLogin('2User');
    createUserDialog.addWithClientSideFailure();
    createUserDialog.assertLoginFieldIsValid();
    createUserDialog.assertLastNameFieldHasError(EMPTY_FIELD_ERROR);
    createUserDialog.assertPasswordFieldHasError(EMPTY_FIELD_ERROR);
    createUserDialog.assertPasswordConfirmationFieldHasError(EMPTY_FIELD_ERROR);
    createUserDialog.fillName('NewName');
    createUserDialog.addWithClientSideFailure();
    createUserDialog.assertLoginFieldIsValid();
    createUserDialog.assertLastNameFieldIsValid();
    createUserDialog.assertPasswordFieldHasError(EMPTY_FIELD_ERROR);
    createUserDialog.assertPasswordConfirmationFieldHasError(EMPTY_FIELD_ERROR);
    createUserDialog.fillPassword('password');
    createUserDialog.addWithClientSideFailure();
    createUserDialog.assertLoginFieldIsValid();
    createUserDialog.assertLastNameFieldIsValid();
    createUserDialog.assertPasswordFieldIsValid();
    createUserDialog.assertPasswordConfirmationFieldHasError(EMPTY_FIELD_ERROR);
    createUserDialog.fillConfirmPassword('password');
    createUserDialog.addWithClientSideFailure();
    createUserDialog.assertLastNameFieldIsValid();
    createUserDialog.assertPasswordFieldIsValid();
    createUserDialog.assertPasswordConfirmationFieldIsValid();
    createUserDialog.assertLoginFieldHasError(LOGIN_ALREADY_USED_ERROR);
    createUserDialog.selectGroup('Utilisateur');
    createUserDialog.fillLogin('NewUser');
    createUserDialog.clickOnAddAnotherButton();
    createUserDialog.cancel();
    cy.logOut();
    cy.logInAs('2User', 'password');

    // should deactivate User
    cy.logOut();
    cy.logInAs('admin', 'admin');
    adminWorkspaceUsersPage = NavBarElement.navigateToAdministration('users') as AdminWorkspaceUsersPage;
    adminWorkspaceUsersPage.grid.findRowId('login', '2User').then(userID => {
      adminWorkspaceUsersPage.grid.toggleRow(userID, 'active');
    });

    // should check deactivated User cannot login
    cy.logOut();
    const loginPage = LoginPage.navigateTo();
    loginPage.assertExist();
    loginPage.loginFail('2User', 'password');
    loginPage.assertLoginFailedWarningIsVisible();

    // should reactivate User
    cy.logInAs('admin', 'admin');
    adminWorkspaceUsersPage = NavBarElement.navigateToAdministration('users') as AdminWorkspaceUsersPage;
    adminWorkspaceUsersPage.grid.findRowId('login', '2User').then(userID => {
      adminWorkspaceUsersPage.grid.toggleRow(userID, 'active');
    });

    // should login as created User
    cy.logOut();
    cy.logInAs('2User', 'password');
  });
});

describe('Modify User', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    AdminE2eCommands.createUser('MyUser', 'MyUser', 'MyUser', 'MyUser@MyUser', 'Utilisateur', 'admin', 'admin');
  });

  it('modify User', () => {
    cy.logInAs('admin', 'admin');
    cy.viewport(1600, 900);
    let adminWorkspaceUsersPage = NavBarElement.navigateToAdministration('users') as AdminWorkspaceUsersPage;
    adminWorkspaceUsersPage.grid.findRowId('login', 'MyUser').then(userId => {
      adminWorkspaceUsersPage.grid.selectRowWithStickyIndexColumn(userId);
      const userViewPage = new UserViewPage();
      userViewPage.modifyData('login', 'NEWWW');
    });

    // should login as modified User
    cy.logOut();
    cy.logInAs('NEWWW', 'admin');

    // should change User information
    cy.logOut();
    cy.logInAs('admin', 'admin');
    cy.viewport(1600, 900);
    adminWorkspaceUsersPage = NavBarElement.navigateToAdministration('users') as AdminWorkspaceUsersPage;
    adminWorkspaceUsersPage.grid.findRowId('login', 'NEWWW').then(userId => {
      adminWorkspaceUsersPage.grid.selectRowWithStickyIndexColumn(userId);
      const userViewPage = new UserViewPage();
      userViewPage.modifyData('user-first-name', 'Prénom');
      userViewPage.modifyData('user-last-name', 'Nom');
      userViewPage.modifyData('user-email', 'Email');
      userViewPage.modifyData('user-group', 'Administrateur');
      userViewPage.modifyData('user-group', 'Utilisateur');
      // TODO Add when reset password is on again
      // const resetPasswordDialog = userViewPage.openResetPasswordDialog() as ResetPasswordDialog;
      // resetPasswordDialog.fillPassword('newpass');
      // resetPasswordDialog.fillConfirmPassword('newpass');
      // resetPasswordDialog.confirm('newpass');
    });
  });
});
//
//   it.skip('should check login not possible with old password', () => {
//     const loginPage = LoginPage.navigateTo();
//     loginPage.assertExist();
//     loginPage.loginFail('NEWWW', 'password');
//     loginPage.assertLoginFailedWarningIsVisible();
//   });
//
//   it.skip('should login with modified password', () => {
//     cy.logInAs('NEWWW', 'newpass');
//   });
// });
//

describe('Change User Permission', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    cy.viewport(1600, 900);
    ProjectPrerequisiteCommands.createProject(projectName);
    AdminE2eCommands.createUser('MyUser', 'MyUser', 'MyUser', 'MyUser@MyUser', 'Utilisateur', 'admin', 'admin');
  });

  it('Change User Permission', () => {
    cy.logInAs('admin', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration('users') as AdminWorkspaceUsersPage;
    adminWorkspaceUsersPage.grid.findRowId('login', 'MyUser').then(userId => {
      adminWorkspaceUsersPage.grid.selectRowWithStickyIndexColumn(userId);
      const userViewPage = new UserViewPage();
      const addUserAuthorisationsDialog = userViewPage.authorisationsPanel.clickOnAddPermissionButton();
      addUserAuthorisationsDialog.selectProjects(projectName);
      addUserAuthorisationsDialog.selectProfile('Testeur');
      addUserAuthorisationsDialog.confirm();
    });
  });
});

describe('Delete User', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    cy.viewport(1600, 900);
    AdminE2eCommands.createUsers(['MyUser1', 'MyUser2']);
    ProjectPrerequisiteCommands.createProject(projectName);

  });

  it('should delete User', () => {
    cy.logInAs('admin', 'admin');
    cy.viewport(1600, 900);
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration('users') as AdminWorkspaceUsersPage;
    adminWorkspaceUsersPage.deleteUserByLogin('MyUser1', true);
    adminWorkspaceUsersPage.grid.findRowId('login', 'MyUser2').then(userId => {
      adminWorkspaceUsersPage.grid.selectRowWithStickyIndexColumn(userId);
      adminWorkspaceUsersPage.clickOnMultipleDeleteButton(false);
      adminWorkspaceUsersPage.clickOnMultipleDeleteButton(true);
    });
    cy.logOut();

// should check deleted User cannot login
    const loginPage = LoginPage.navigateTo();
    loginPage.assertExist();
    loginPage.loginFail('MyUser1', 'admin');
    loginPage.assertLoginFailedWarningIsVisible();

  });
});


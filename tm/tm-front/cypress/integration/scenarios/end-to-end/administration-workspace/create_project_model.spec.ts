import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  AdminWorkspaceProjectsPage
} from '../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {ProjectPrerequisiteCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';
import {EMPTY_FIELD_ERROR, NAME_ALREADY_USED_ERROR} from '../../../page-objects/elements/forms/error.message';
import {NavBarAdminElement} from '../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';

describe('Create Project Model', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    cy.viewport(1440, 900);
    ProjectPrerequisiteCommands.createProject('project-1');
  });

  it('should check disabled option', () => {
    cy.logInAs('admin', 'admin');
    const adminWorkspaceProjectsPage =
      NavBarElement.navigateToAdministration('projects') as AdminWorkspaceProjectsPage;
    adminWorkspaceProjectsPage.checkIfCreateFromProjectDisabled(true);
  });

  it('should create a model', () => {
    cy.logInAs('admin', 'admin');
    let adminWorkspaceProjectsPage = NavBarElement.navigateToAdministration('projects') as AdminWorkspaceProjectsPage;
    new NavBarAdminElement().toggle();
    let createTemplateDialog = adminWorkspaceProjectsPage.openCreateTemplate();

    // TODO : this should ideally be hidden behind a page-object method... Throwing this to quickly fix flaky test.
    const projectView = new HttpMockBuilder('project-view/*').build();
    createTemplateDialog.createTemplate('my template', 'template label', 'template description');
    projectView.wait();

    adminWorkspaceProjectsPage.grid.findRowId('name', 'my template').then(templateId => {
      // selecting the row will unselect it after creation that already selected the line...
      adminWorkspaceProjectsPage.grid.selectRowWithStickyIndexColumn(templateId);
      adminWorkspaceProjectsPage.assertIsProjectModel(templateId);
    });

    createTemplateDialog = adminWorkspaceProjectsPage.openCreateTemplate();
    createTemplateDialog.fillDescription('template description');
    createTemplateDialog.fillLabel('template label');
    createTemplateDialog.clickOnAddButton();
    createTemplateDialog.assertNameFieldHasError(EMPTY_FIELD_ERROR);
    createTemplateDialog.fillName('my template');
    createTemplateDialog.clickOnAddButton();
    createTemplateDialog.assertNameFieldHasError(NAME_ALREADY_USED_ERROR);

    createTemplateDialog.cancel();

    // should create model from project'
    cy.goHome();
    adminWorkspaceProjectsPage = NavBarElement.navigateToAdministration('projects') as AdminWorkspaceProjectsPage;
    adminWorkspaceProjectsPage.grid.findRowId('name', 'project-1').then(projectId => {
      adminWorkspaceProjectsPage.grid.selectRowWithStickyIndexColumn(projectId);
    });
    const createTemplateFromProjectDialog = adminWorkspaceProjectsPage.openCreateTemplateFromProject();

    createTemplateFromProjectDialog.fillDescription('template description');
    createTemplateFromProjectDialog.fillLabel('template label');
    createTemplateFromProjectDialog.clickOnAddButton();
    createTemplateFromProjectDialog.assertNameFieldHasError(EMPTY_FIELD_ERROR);
    createTemplateFromProjectDialog.fillName('my template');
    createTemplateFromProjectDialog.clickOnAddButton();
    createTemplateFromProjectDialog.assertNameFieldHasError(NAME_ALREADY_USED_ERROR);
    createTemplateFromProjectDialog.cancel();

    cy.goHome();
    adminWorkspaceProjectsPage = NavBarElement.navigateToAdministration('projects') as AdminWorkspaceProjectsPage;
    adminWorkspaceProjectsPage.grid.findRowId('name', 'project-1').then(projectId => {
      adminWorkspaceProjectsPage.assertIsNotProjectModel(projectId);
    });
    adminWorkspaceProjectsPage.grid.findRowId('name', 'my template').then(projectId => {
      adminWorkspaceProjectsPage.assertIsProjectModel(projectId);
    });
  });
});

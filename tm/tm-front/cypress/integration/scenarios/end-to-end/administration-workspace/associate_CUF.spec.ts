import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  AdminWorkspaceCustomFieldsPage
} from '../../../page-objects/pages/administration-workspace/admin-workspace-custom-fields.page';
import {ProjectPrerequisiteCommands} from '../../../page-objects/scenarios-parts/project/project_e2e_commands';
import {
  AdminWorkspaceProjectsPage
} from '../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {ProjectViewPage} from '../../../page-objects/pages/administration-workspace/project-view/project-view.page';
// tslint:disable-next-line:max-line-length
import {
  CustomFieldsPanelElement
} from '../../../page-objects/pages/administration-workspace/project-view/panels/project-custom-fields-panel.element';
import {TestCaseE2eCommands} from '../../../page-objects/scenarios-parts/test_case/test_case_e2e_commands';


describe('Create CUF - prerequisite', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    cy.task('cleanDatabase');
    ProjectPrerequisiteCommands.createProject('project-1');
  });

  it('should create CUF', () => {

    cy.logInAs('admin', 'admin');

    const adminWorkspaceCUFPage = NavBarElement.navigateToAdministration<AdminWorkspaceCustomFieldsPage>('entities-customization');

    let createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Case à cocher', 'cuf1', 'cuf1', 'Case à cocher', 'Vrai');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createNonOptionalCUF('Date', 'cuf3', 'cuf3', 'Date');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Tag', 'cuf4', 'cuf4', 'Tag');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Numérique', 'cuf5', 'cuf5', 'Numérique', 5);

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Texte Simple', 'cuf6', 'cuf6', 'Texte simple', 'texte');

    createCustomFieldDialog = adminWorkspaceCUFPage.openCreateCustomField();
    createCustomFieldDialog.createOptionalCUF('Texte riche', 'cuf2', 'cuf2', 'Texte riche');

  });
});


describe('Associate CUF', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
    //cy.server();
  });

  interface Datatype {
    entity: string;
    cuf: string;
    mandatory: boolean;
  }

  const dataSets: Datatype[] =
    [ {entity: 'Dossiers d\'exigences', cuf: 'Case à cocher', mandatory: false},
      {entity: 'Exigences', cuf: 'Date', mandatory: false},
      {entity: 'Dossiers de cas de test', cuf: 'Tag', mandatory: false},
      {entity: 'Cas de test', cuf: 'Numérique', mandatory: true},
      {entity: 'Pas de test', cuf: 'Texte Simple', mandatory: true},
      {entity: 'Dossiers de campagnes', cuf: 'Texte riche', mandatory: false},
      {entity: 'Campagnes', cuf: 'Case à cocher', mandatory: false},
      {entity: 'Itérations', cuf: 'Date', mandatory: false},
      {entity: 'Suites de tests', cuf: 'Tag', mandatory: false},
      {entity: 'Exécutions', cuf: 'Numérique', mandatory: true},
      {entity: 'Pas d\'exécution', cuf: 'Texte Simple', mandatory: true}
    ];


  dataSets.forEach((data, index) => runTest(data, index));

  function runTest(data: Datatype, index: number) {
    it(`Dataset ${index} - For entity : ${data.entity}, cuf: ${data.cuf} - is mandatory : ${data.mandatory}`,
      () => {
        cy.logInAs('admin', 'admin');
        const adminWorkspaceProjectsPage =
          NavBarElement.navigateToAdministration<AdminWorkspaceProjectsPage>('projects');
        adminWorkspaceProjectsPage.grid.findRowId('name', 'project-1').then(projectId => {
          adminWorkspaceProjectsPage.grid.selectRow(projectId, 'name');
          const projectView = new ProjectViewPage();
          const customFieldsPanelElement = projectView.clickAnchorLink('custom-fields') as CustomFieldsPanelElement;
          const bindCustomFieldToProjectDialog = customFieldsPanelElement.openCustomFieldBindingDialog();
          bindCustomFieldToProjectDialog.selectEntity(`${data.entity}`);
          bindCustomFieldToProjectDialog.selectCuf(`${data.cuf}`);
          bindCustomFieldToProjectDialog.confirm();
        });
      });
  }

});

describe('Check CUF', function () {

  beforeEach(function () {
    Cypress.Cookies.debug(true);
  });

  it('check CUF',
    () => {
      cy.logInAs('admin', 'admin');
      const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
      testCaseWorkspace.tree.findRowId('NAME', 'project-1').then(projectId => {
        testCaseWorkspace.tree.selectNode(projectId);

        // TODO Requirements
        // TODO Requirements Folder


        // check 'cas de test' cuf
        const createTestCaseDialog = testCaseWorkspace.treeMenu.openCreateTestCase();
        createTestCaseDialog.fillFieldsWithoutCuf('TEST1', 'REF1', 'DESCRIPTION1');
        createTestCaseDialog.addForSuccessOpen();
        TestCaseE2eCommands.findCufLabelId('cuf5').then(cufLabelId => {
          cy.log('cufID : ' + cufLabelId);
        });


        // TODO Campaign
        // TODO Campaign Folder
        // TODO Iterations
        // TODO Test Suites
        // TODO Execution
        // TODO Execution Step
      });
    });
});




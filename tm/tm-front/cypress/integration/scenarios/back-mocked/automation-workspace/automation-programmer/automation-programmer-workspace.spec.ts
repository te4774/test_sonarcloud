import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {
  AutomationProgrammerAssigneePage
} from '../../../../page-objects/pages/automation-workspace/automation-programmer/automation-programmer-assignee.page';
import {
  AutomationProgrammerTreatmentPage
} from '../../../../page-objects/pages/automation-workspace/automation-programmer/automation-programmer-treatment.page';
import {
  AutomationProgrammerGlobalPage
} from '../../../../page-objects/pages/automation-workspace/automation-programmer/automation-programmer-global.page';

describe('Automation Workspace - assignee', function () {
  it('should display assignee grid', () => {
    const page = AutomationProgrammerAssigneePage.initTestAtPage(initialNodes);
    const grid = page.grid;
    const toolBar = page.gridToolBarElement;
    toolBar.button('unassign').assertExist();
    toolBar.button('unassign').assertIsDisabled();

    toolBar.button('automated').assertExist();
    toolBar.button('automated').assertIsDisabled();
    grid.assertRowCount(2);


    grid.getRow(1).cell('name').textRenderer().assertContainText('TestCase 1');
    grid.getRow(2).cell('name').textRenderer().assertContainText('TestCase 2');

  });

});

describe('Automation Workspace - treatment', function () {
  it('should display treatment grid', () => {
    const page = AutomationProgrammerTreatmentPage.initTestAtPage(initialNodes);
    const grid = page.grid;
    grid.assertRowCount(2);


    grid.getRow(1).cell('name').textRenderer().assertContainText('TestCase 1');
    grid.getRow(2).cell('name').textRenderer().assertContainText('TestCase 2');

  });

});

describe('Automation Workspace - global', function () {
  it('should display global grid', () => {
    const page = AutomationProgrammerGlobalPage.initTestAtPage(initialNodes);
    const grid = page.grid;

    const toolBar = page.gridToolBarElement;
    toolBar.button('unassign').assertExist();
    toolBar.button('unassign').assertIsDisabled();
    toolBar.button('assign').assertExist();
    toolBar.button('assign').assertIsDisabled();
    toolBar.button('automated').assertExist();
    toolBar.button('automated').assertIsDisabled();
    toolBar.buttonMenu('more', 'action-menu').assertExist();
    grid.assertRowCount(2);


    grid.getRow(1).cell('name').textRenderer().assertContainText('TestCase 1');
    grid.getRow(2).cell('name').textRenderer().assertContainText('TestCase 2');

  });

});

const todayDate = new Date();

const initialNodes: GridResponse = {
  count: 2,
  dataRows: [{
    id: 1,
    projectId: 1,
    data: {
      projectId: 1,
      tclnId: 1,
      automationRequestId: 1,
      projectName: 'Project - 001',
      name: 'TestCase 1',
      assignedTo: 1,
      reference: '001',
      automationPriority: 4,
      requestStatus: 'AUTOMATED',
      transmittedOn: '2021-03-16',
      assignedOn: '2021-03-15',
      scriptedTestCaseId: null,
      keywordTestCaseId: null,
      login: 'admin',
      atTechnologyId: 2,
      sourceCodeRepositoryUrl: '',
      automatedTestReference: '',
      conflictAssociation: '',
      uuid: 'aaa-eez-zez',
      automatedTestFullName: '',
      tcMilestoneLocked: 0,
      kind: 'STANDARD'
    }
  } as unknown as DataRow,
    {
      id: 2,
      projectId: 1,
      data: {
        projectId: 1,
        tclnId: 2,
        automationRequestId: 2,
        projectName: 'Project - 001',
        name: 'TestCase 2',
        assignedTo: 1,
        reference: '001',
        automationPriority: 4,
        requestStatus: 'AUTOMATED',
        transmittedOn: '2021-03-16',
        assignedOn: '2021-03-15',
        scriptedTestCaseId: null,
        keywordTestCaseId: null,
        login: 'admin',
        atTechnologyId: 2,
        sourceCodeRepositoryUrl: '',
        automatedTestReference: '',
        conflictAssociation: '',
        uuid: 'aaa-eez-zez',
        automatedTestFullName: '',
        tcMilestoneLocked: 0,
        kind: 'STANDARD'
      }
    } as unknown as DataRow]
};

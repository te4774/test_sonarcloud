import {
  FunctionalTesterReadyToTransmitPage
} from '../../../../page-objects/pages/automation-workspace/functional-tester-workspace/functional-tester-ready-to-transmit.page';
import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {
  FunctionalTesterToBeValidatedPage
} from '../../../../page-objects/pages/automation-workspace/functional-tester-workspace/functional-tester-to-be-validated.page';
import {
  FunctionalTesterGlobalPage
} from '../../../../page-objects/pages/automation-workspace/functional-tester-workspace/functional-tester-global.page';

describe('Functional Tester - Ready to transmit view', () => {
  it('should display ready to transmit grid', () => {
    const page = FunctionalTesterReadyToTransmitPage.initTestAtPage(initialNodes);
    const grid = page.grid;
    const toolBar = page.gridToolBarElement;

    toolBar.button('transmit').assertExist();
    toolBar.button('transmit').assertIsDisabled();

    grid.getRow(1).cell('name').textRenderer().assertContainText('TestCase 1');
    grid.getRow(2).cell('name').textRenderer().assertContainText('TestCase 2');
    grid.getRow(3).cell('name').textRenderer().assertContainText('TestCase 3');
  });
});

describe('Functional Tester - To be validated view', () => {
  it('should display to be validated grid', () => {
    const page = FunctionalTesterToBeValidatedPage.initTestAtPage(initialNodes);
    const grid = page.grid;
    const toolBar = page.gridToolBarElement;

    toolBar.button('validate').assertExist();
    toolBar.button('validate').assertIsDisabled();

    grid.getRow(1).cell('name').textRenderer().assertContainText('TestCase 1');
    grid.getRow(2).cell('name').textRenderer().assertContainText('TestCase 2');
    grid.getRow(3).cell('name').textRenderer().assertContainText('TestCase 3');
  });
});

describe('Functional Tester - Global view', () => {
  it('should display global automation requests grid', () => {
    const page = FunctionalTesterGlobalPage.initTestAtPage(initialNodes);
    const grid = page.grid;
    const toolBar = page.gridToolBarElement;

    toolBar.button('transmit').assertExist();
    toolBar.button('transmit').assertIsDisabled();

    grid.getRow(1).cell('name').textRenderer().assertContainText('TestCase 1');
    grid.getRow(2).cell('name').textRenderer().assertContainText('TestCase 2');
    grid.getRow(3).cell('name').textRenderer().assertContainText('TestCase 3');
  });
});

const initialNodes: GridResponse = {
  count: 2,
  dataRows: [{
    id: 1,
    projectId: 1,
    data: {
      projectId: 1,
      tclnId: 1,
      automationRequestId: 1,
      projectName: 'Project - 001',
      name: 'TestCase 1',
      assignedTo: null,
      reference: '001',
      automationPriority: 4,
      requestStatus: 'READY_TO_TRANSMIT',
      transmittedOn: null,
      assignedOn: '2021-03-15',
      scriptedTestCaseId: null,
      keywordTestCaseId: null,
      login: 'admin',
      atTechnologyId: 2,
      sourceCodeRepositoryUrl: '',
      automatedTestReference: '',
      conflictAssociation: '',
      uuid: 'aaa-eez-zez',
      automatedTestFullName: '',
      tcMilestoneLocked: null,
      kind: 'STANDARD'
    }
  } as unknown as DataRow,
    {
      id: 2,
      projectId: 1,
      data: {
        projectId: 1,
        tclnId: 2,
        automationRequestId: 2,
        projectName: 'Project - 001',
        name: 'TestCase 2',
        assignedTo: null,
        reference: '001',
        automationPriority: 4,
        requestStatus: 'READY_TO_TRANSMIT',
        transmittedOn: '2021-03-16',
        assignedOn: '2021-03-15',
        scriptedTestCaseId: null,
        keywordTestCaseId: null,
        login: 'admin',
        atTechnologyId: 2,
        sourceCodeRepositoryUrl: '',
        automatedTestReference: '',
        conflictAssociation: '',
        uuid: 'aaa-eez-zez',
        automatedTestFullName: '',
        tcMilestoneLocked: null,
        kind: 'STANDARD'
      }
    } as unknown as DataRow,
    {
      id: 3,
      projectId: 1,
      data: {
        projectId: 1,
        tclnId: 3,
        automationRequestId: 3,
        projectName: 'Project - 001',
        name: 'TestCase 3',
        assignedTo: null,
        reference: '001',
        automationPriority: 4,
        requestStatus: 'READY_TO_TRANSMIT',
        transmittedOn: '2021-03-16',
        assignedOn: '2021-03-15',
        scriptedTestCaseId: null,
        keywordTestCaseId: null,
        login: 'admin',
        atTechnologyId: 2,
        sourceCodeRepositoryUrl: '',
        automatedTestReference: '',
        conflictAssociation: '',
        uuid: 'aaa-eez-zez',
        automatedTestFullName: '',
        tcMilestoneLocked: 1,
        kind: 'STANDARD'
      }
    } as unknown as DataRow]
};

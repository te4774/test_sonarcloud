import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
// tslint:disable-next-line:max-line-length
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
// tslint:disable-next-line:max-line-length
import {
  TestCaseForCoverageSearchPage
} from '../../../../page-objects/pages/test-case-workspace/research/test-case-for-coverage-search-page';
import {AlertDialogElement} from '../../../../page-objects/elements/dialog/alert-dialog.element';


function assertRedirectionToRequirementWorkspaceDone() {
  cy.url().should('equal', `${Cypress.config().baseUrl}/requirement-workspace`);
}

describe('Test Case for coverage search page', function () {

  beforeEach(() => {
    //cy.server();
    cy.viewport(1200, 720);
  });

  describe('Test Case search table', () => {
    const testCase = {
      id: '1',
      type: 'TestCase',
      projectId: 1,
      data: {
        'name': 'Test 1',
        'id': 1,
        'reference': 'ref1',
        'projectName': 'project 1',
        'attachments': 0,
        'items': 0,
        'steps': 0,
        'nature': 14,
        'type': 16,
        'automatable': 'Y',
        'status': 'WORK_IN_PROGRESS',
        'importance': 'LOW',
        'createdBy': 'admin',
        'lastModifiedBy': 'admin',
        'tcMilestoneLocked': 0,
        'reqMilestoneLocked': 0
      }
    } as unknown as DataRow;


    it('should show links buttons and activate according to user selection', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase]
      };
      const forCoverageSearchPage = TestCaseForCoverageSearchPage.initTestAtPage('1', createEntityReferentialData, gridResponse);
      const gridElement = forCoverageSearchPage.grid;
      new NavBarElement().toggle();
      forCoverageSearchPage.foldFilterPanel();
      forCoverageSearchPage.assertLinkSelectionButtonExist();
      forCoverageSearchPage.assertLinkSelectionButtonIsNotActive();
      forCoverageSearchPage.assertLinkAllButtonExist();
      forCoverageSearchPage.assertLinkAllButtonIsActive();
      forCoverageSearchPage.assertNavigateBackButtonExist();
      forCoverageSearchPage.assertNavigateBackButtonIsActive();
      gridElement.selectRow('1', '#', 'leftViewport');
      forCoverageSearchPage.assertLinkSelectionButtonIsActive();
      gridElement.toggleRow('1', '#', 'leftViewport');
      forCoverageSearchPage.assertLinkSelectionButtonIsNotActive();
    });

    it('should link a test Case', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase]
      };
      const forCoverageSearchPage = TestCaseForCoverageSearchPage.initTestAtPage('1', createEntityReferentialData, gridResponse);
      const gridElement = forCoverageSearchPage.grid;
      new NavBarElement().toggle();
      gridElement.selectRow('1', '#', 'leftViewport');
      const testCaseTreeMock = buildRequirementTreeMock();
      forCoverageSearchPage.linkSelection('1');
      testCaseTreeMock.wait();
      assertRedirectionToRequirementWorkspaceDone();
    });

    it('should link all test cases', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase]
      };
      const forCoverageSearchPage = TestCaseForCoverageSearchPage.initTestAtPage('1', createEntityReferentialData, gridResponse);
      new NavBarElement().toggle();
      const testCaseTreeMock = buildRequirementTreeMock();
      forCoverageSearchPage.linkAll('1');
      testCaseTreeMock.wait();
      assertRedirectionToRequirementWorkspaceDone();
    });

    it('should link a test case and show error', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [testCase]
      };
      const forCoverageSearchPage = TestCaseForCoverageSearchPage.initTestAtPage('1', createEntityReferentialData, gridResponse);
      const gridElement = forCoverageSearchPage.grid;
      new NavBarElement().toggle();
      gridElement.selectRow('1', '#', 'leftViewport');
      forCoverageSearchPage.linkSelection('1', {
        verifyingTestCases: [], summary: {
          alreadyVerifiedRejections: true,
          notLinkableRejections: true
        }
      });
      const alertDialogElement = new AlertDialogElement('coverage-report');
      alertDialogElement.assertExist();
      alertDialogElement.assertHasMessage(
        'Au moins un des cas de test sélectionné est déjà associé à cette version ou à une autre version de cette' +
        ' exigence. Ce cas de test n\'a pas été associé à cette version.');
      alertDialogElement.assertHasMessage(
        'Au moins un des cas de test sélectionnés n\'a pas été ajouté à l\'exigence car son statut ne le permet pas.');
      alertDialogElement.close();
    });
  });
});

function buildRequirementTreeMock() {
  const requirementTree: GridResponse = {count: 0, dataRows: []};
  return new HttpMockBuilder(`/requirement-tree`).post().responseBody(requirementTree).build();
}

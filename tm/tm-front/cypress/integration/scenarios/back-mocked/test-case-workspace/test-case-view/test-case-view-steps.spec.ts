import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {ActionStepModel, CallStepModel, TestStepModel} from '../../../../model/test-case/test-step.model';
import {TestStepsViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {Parameter} from '../../../../model/test-case/parameter.model';
import {
  TestCaseViewParametersPage
} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-parameters.page';
import {Dataset} from '../../../../model/test-case/dataset.model';
import {DatasetParamValue} from '../../../../model/test-case/dataset-param-value';

describe('Test Case View - Steps', function () {
  it('should display test case steps', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickAnchorLink('steps') as TestStepsViewPage;
    testStepPage.checkOrder([2, 4, 1, 3]);

    // checking that prerequisite is expended
    testStepPage.checkPrerequisiteIsExpended();

    // checking that all actions steps are expended
    [0, 2, 3].forEach(stepIndex => {
      const actionStepElement = testStepPage.getActionStepByIndex(stepIndex);
      actionStepElement.checkIndexLabel();
      actionStepElement.checkIsExpended();
    });

    // checking that all call steps are expended
    const callStepElement = testStepPage.getCallStepByIndex(1);
    callStepElement.checkIsCollapsed();

  });

  it('should collapse/expend all test case steps', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickAnchorLink('steps') as TestStepsViewPage;
    testStepPage.collapseAll();
    testStepPage.checkPrerequisiteIsCollapsed();

    [0, 2, 3].forEach(stepIndex => {
      const actionStep = testStepPage.getActionStepByIndex(stepIndex);
      actionStep.checkIsCollapsed();
    });

    const callStepElement = testStepPage.getCallStepByIndex(1);
    callStepElement.checkIsCollapsed();

    testStepPage.expendAll();
    [0, 2, 3].forEach(stepIndex => {
      const actionStep = testStepPage.getActionStepByIndex(stepIndex);
      actionStep.checkIsExpended();
    });

    callStepElement.checkIsExpended();
  });

  it('should select multiple steps', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickAnchorLink('steps') as TestStepsViewPage;
    const step1 = testStepPage.getActionStepByIndex(0);
    const step2 = testStepPage.getCallStepByIndex(1);
    const step3 = testStepPage.getActionStepByIndex(2);
    const step4 = testStepPage.getActionStepByIndex(3);

    step1.singleSelectStep();
    step1.assertIsSelected();
    step2.assertIsNotSelected();
    step3.assertIsNotSelected();
    step4.assertIsNotSelected();

    step2.singleSelectStep();
    step2.assertIsSelected();
    step1.assertIsNotSelected();

    // adding step 4 to selection
    step4.toggleStepSelection();
    step1.assertIsNotSelected();
    step2.assertIsSelected();
    step3.assertIsNotSelected();
    step4.assertIsSelected();

    // adding step 3
    step3.toggleStepSelection();
    step1.assertIsNotSelected();
    step2.assertIsSelected();
    step3.assertIsSelected();
    step4.assertIsSelected();

    // checking that collapse doesn't change selection
    testStepPage.collapseAll();
    step1.assertIsNotSelected();
    step2.assertIsSelected();
    step3.assertIsSelected();
    step4.assertIsSelected();

    // removing step 3
    step3.toggleStepSelection();
    step1.assertIsNotSelected();
    step2.assertIsSelected();
    step3.assertIsNotSelected();
    step4.assertIsSelected();

    // force select step 4 and deselect all
    step4.singleSelectStep();
    step1.assertIsNotSelected();
    step2.assertIsNotSelected();
    step3.assertIsNotSelected();
    step4.assertIsSelected();

    // unselect without ctrl
    step4.singleSelectStep();
    step1.assertIsNotSelected();
    step2.assertIsNotSelected();
    step3.assertIsNotSelected();
    step4.assertIsNotSelected();
  });

  it.skip('should create param in test step', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickAnchorLink('steps') as TestStepsViewPage;
    const step1 = testStepPage.getActionStepByIndex(0);
    const response = {parameters: [{id: 1, name: 'param_42'} as Parameter], paramValues: [], dataSets: []};
    step1.modifyFilledStepWithParam('Action ${param_42}', 'action', 'test-steps/*', response);
    const parameterPage = page.clickAnchorLink('parameters') as TestCaseViewParametersPage;
    const parameterTable = parameterPage.parametersTable;
    const cell = parameterTable.getHeaderRow().cell('PARAM_COLUMN_1');
    cell.assertExist();
    cell.textRenderer().assertContainText('param_42');
  });

  it.skip('should forbid update test step if parameter pattern name is wrong', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickAnchorLink('steps') as TestStepsViewPage;
    const step1 = testStepPage.getActionStepByIndex(0);
    const error = {
      squashTMError: {
        actionValidationError: {
          i18nKey: 'sqtm-core.error.test-step.parameter.name'
        },
        kind: 'ACTION_ERROR'
      }
    };
    step1.modifyFilledStepOnErrorWithParam('wrong param ${param 42} of ${23}', 'action',
      'test-steps/*', error);
    const alertDialog = testStepPage.parameterNameAlertDialog();
    alertDialog.assertExist();
    alertDialog.assertMessage();
  });

  it.skip('should show delegate param dialog', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickAnchorLink('steps') as TestStepsViewPage;
    const callStep = testStepPage.getCallStepByIndex(1);
    const dataSetLink = callStep.getLinkElement('dataset');
    dataSetLink.containText('Choisir un jeu de données');

    const dataSets = [{'id': 1, 'name': 'JDD'}];
    const dialog = callStep.showDelegateParamDialog(dataSets);
    dialog.shouldExist();
    dialog.radioIsSelected('available-dataset');
    dialog.radioNotSelected('no-dataset');
    const dataSetField = dialog.getSelectField();
    dataSetField.checkSelectedOption('Aucun');
    dataSetField.checkAllOptions(['Aucun', 'JDD']);
    dataSetField.setValue('JDD');
    dataSetField.checkSelectedOption('JDD');

    const response = {
      parameters: [] as Parameter[],
      dataSets: [] as Dataset[],
      paramValues: [] as DatasetParamValue[],
    };
    dialog.confirm(response);
    dataSetLink.containText('JDD');
  });

  it('should change delegate param', () => {
    const page = navigateToTestCase();
    new NavBarElement().toggle();
    page.toggleTree();
    const testStepPage = page.clickAnchorLink('steps') as TestStepsViewPage;
    const callStep = testStepPage.getCallStepByIndex(1);
    const dataSetLink = callStep.getLinkElement('dataset');
    dataSetLink.containText('Choisir un jeu de données');
    const dialog = callStep.showDelegateParamDialog([]);
    dialog.shouldExist();
    dialog.selectRadio('no-dataset');
    dialog.confirm({
      parameters: [{id: 1, name: 'Parameter', sourceTestCaseId: 4} as Parameter],
      dataSets: [],
      paramValues: []
    });
    dataSetLink.containText('(paramètres délégués)');
    const parameterPage = page.clickAnchorLink('parameters') as TestCaseViewParametersPage;
    const parameterTable = parameterPage.parametersTable;
    const cell = parameterTable.getHeaderRow().cell('PARAM_COLUMN_1');
    cell.assertExist();
    cell.textRenderer().assertContainText('Parameter');
  });

  it('should create call test case', () => {
    const workspacePage = initTestCaseWorkspacePage();
    const testCaseModel = createTestCaseModel([]);
    const testCaseViewPage = workspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', testCaseModel);
    const testStepPage = testCaseViewPage.clickAnchorLink('steps') as TestStepsViewPage;

    workspacePage.tree.beginDragAndDrop('TestCase-4');
    testStepPage.enterIntoTestCase();
    const testSteps = {
      testSteps:
        [
          {
            'id': 1,
            'stepOrder': 0,
            'testCaseId': 3,
            'calledTcId': 4,
            'calledTcName': 'Launch Atlantis 2',
            'calledDatasetId': null,
            'calledDatasetName': null,
            'delegateParam': false,
            'calledTestCaseSteps': [],
            'kind': 'call-step'
          }],
      'testCaseImportance': 'LOW'
    };
    testStepPage.dropCalledTestCase(3, testSteps);
    const callStep = testStepPage.getCallStepByIndex(0);
    callStep.getLinkElement('calledTestCase').containText('Launch Atlantis 2');
    callStep.getLinkElement('dataset').containText('Choisir un jeu de données');
  });
})
;

function initTestCaseWorkspacePage(): TestCaseWorkspacePage {
  const initialNodes: GridResponse = {
    count: 3,
    dataRows: [
      {
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3', 'TestCase-4'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 2},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'TestCase-3',
        children: [],
        projectId: 1,
        parentRowId: 'TestCaseLibrary-1',
        state: DataRowOpenState.leaf,
        data: {
          'NAME': 'Launch Atlantis',
          'CHILD_COUNT': 0,
          'TC_STATUS': 'APPROVED',
          'TC_KIND': 'STANDARD',
          'IMPORTANCE': 'LOW'
        }
      } as unknown as DataRow,
      {
        id: 'TestCase-4',
        children: [],
        projectId: 1,
        parentRowId: 'TestCaseLibrary-1',
        state: DataRowOpenState.leaf,
        data: {
          'NAME': 'Launch Atlantis 2',
          'CHILD_COUNT': 0,
          'TC_STATUS': 'APPROVED',
          'TC_KIND': 'STANDARD',
          'IMPORTANCE': 'LOW'
        }
      } as unknown as DataRow]
  };
  return TestCaseWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
}

function navigateToTestCase(): TestCaseViewPage {
  const testCaseWorkspacePage = initTestCaseWorkspacePage();
  const actionStep1: ActionStepModel = {
    id: 2,
    projectId: 1,
    stepOrder: 0,
    customFieldValues: [],
    attachmentList: {id: 2, attachments: []},
    kind: 'action-step',
    action: 'check main engines glimbal',
    expectedResult: 'main engines ok to handle corrections'
  };

  const callStep1: CallStepModel = {
    id: 4,
    stepOrder: 1,
    projectId: 1,
    calledTcId: 12,
    calledTcName: 'main engines sequence start',
    kind: 'call-step',
    calledDatasetId: null,
    calledTestCaseSteps: [],
    delegateParam: false
  };

  const actionStep2: ActionStepModel = {
    id: 1,
    projectId: 1,
    stepOrder: 2,
    customFieldValues: [],
    attachmentList: {id: 3, attachments: []},
    kind: 'action-step',
    action: 'solid rocket booster ignition',
    expectedResult: ' big petards are running'
  };

  const actionStep3: ActionStepModel = {
    id: 3,
    projectId: 1,
    stepOrder: 3,
    customFieldValues: [],
    attachmentList: {id: 3, attachments: []},
    kind: 'action-step',
    action: 'release fixation arms',
    expectedResult: '... and we have lift off. Space shuttle Atlantis is returning to the ISS'
  };

  const testSteps: TestStepModel[] = [actionStep1, callStep1, actionStep2, actionStep3];
  const model = createTestCaseModel(testSteps);
  return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
}

function createTestCaseModel(testSteps: TestStepModel[]): TestCaseModel {
  return {
    id: 3,
    projectId: 1,
    name: 'Launch Atlantis',
    customFieldValues: [],
    attachmentList: {
      id: 1,
      attachments: []
    },
    reference: 'STS 135',
    description: '',
    uuid: '',
    type: 20,
    testSteps,
    status: 'WORK_IN_PROGRESS',
    prerequisite: '',
    parameters: [],
    nbIssues: 0,
    nature: 12,
    milestones: [],
    lastModifiedOn: new Date('2020-03-09 10:30'),
    lastModifiedBy: 'admin',
    kind: 'STANDARD',
    importanceAuto: false,
    importance: 'VERY_HIGH',
    executions: [],
    datasets: [],
    datasetParamValues: [],
    createdOn: new Date('2020-03-09 10:30'),
    createdBy: 'admin',
    coverages: [],
    automationRequest: null,
    automatable: 'M',
    calledTestCases: [],
    lastExecutionStatus: 'SUCCESS',
    script: '',
    actionWordLibraryActive: false
  };
}

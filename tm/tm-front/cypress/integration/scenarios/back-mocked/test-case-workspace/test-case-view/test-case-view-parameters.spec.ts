import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {Parameter} from '../../../../model/test-case/parameter.model';
import {Dataset} from '../../../../model/test-case/dataset.model';
import {DatasetParamValue} from '../../../../model/test-case/dataset-param-value';
import {
  TestCaseViewParametersPage
} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-parameters.page';
import {TestCaseParameterOperationReport} from '../../../../model/test-case/change-dataset-param-operation.model';
import {mockTestCaseModel} from '../../../../data-mock/test-case.data-mock';

describe('Test Case View - Parameters', function () {
  it('should display table', () => {
    const testCaseViewPage = navigateToTestCase();
    const table = testCaseViewPage.parametersTable;
    const firstRow = table.getRow(1, 'leftViewport');
    firstRow.cell('name').textRenderer().assertContainText('Dataset');

  });

  it('should add parameter', () => {
    const testCaseViewPage = navigateToTestCase();
    const dialog = testCaseViewPage.openAddParameterDialog();

    dialog.fillName('Param_2');
    dialog.fillDescription('Description');

    const operation: TestCaseParameterOperationReport = {
      dataSets: [{id: 1, name: 'Dataset'}, {id: 2, name: 'Dataset 2'}],
      paramValues: [
        ...initializeDataSetParamValue(),
        {id: 7, parameterId: 4, datasetId: 1, value: ''},
        {id: 8, parameterId: 4, datasetId: 2, value: ''}
      ],
      parameters: [...initializeParameters(), {
        id: 4,
        name: 'Param_2',
        description: 'Description',
        sourceTestCaseId: 3,
        sourceTestCaseName: '',
        sourceTestCaseProjectName: '',
        sourceTestCaseReference: ''
      }]
    };
    dialog.addParam(operation);
    const table = testCaseViewPage.parametersTable;
    const headerRow = table.getHeaderRow();
    headerRow.cell('PARAM_COLUMN_4').assertExist();
  });


  it('should add datasets', () => {
    const testCaseViewPage = navigateToTestCase();
    const dialog = testCaseViewPage.openAddDataSetDialog();
    dialog.checkDialogButtons();
    dialog.fillName('New dataset');
    dialog.fillParameter(1, 'p245');


    const operation: TestCaseParameterOperationReport = {
      dataSets: [{id: 1, name: 'Dataset'}, {id: 2, name: 'Dataset 2'}, {id: 3, name: 'New dataset'}],
      paramValues: [
        {id: 1, parameterId: 1, datasetId: 1, value: ''},
        {id: 2, parameterId: 1, datasetId: 2, value: ''},
        {id: 3, parameterId: 1, datasetId: 3, value: 'p245'},
        {id: 4, parameterId: 2, datasetId: 1, value: ''},
        {id: 5, parameterId: 2, datasetId: 2, value: ''},
        {id: 6, parameterId: 2, datasetId: 3, value: ''},
        {id: 7, parameterId: 3, datasetId: 1, value: ''},
        {id: 8, parameterId: 3, datasetId: 2, value: ''},
        {id: 9, parameterId: 3, datasetId: 3, value: ''}
      ],
      parameters: [...initializeParameters()]
    };
    dialog.addDataSet(operation);

    const table = testCaseViewPage.parametersTable;
    table.assertRowCount(3);
    const newRow = table.getRow('3', 'leftViewport');
    newRow.cell('name').textRenderer().assertContainText('New dataset');
  });

  it('should remove dataset', () => {
    const testCaseViewPage = navigateToTestCase();
    const dialog = testCaseViewPage.openDeleteDataSetDialog(2);
    const operation: TestCaseParameterOperationReport = {
      dataSets: [{id: 1, name: 'Dataset'}],
      paramValues: [
        {id: 1, parameterId: 1, datasetId: 1, value: ''},
        {id: 3, parameterId: 2, datasetId: 1, value: ''},
        {id: 5, parameterId: 3, datasetId: 1, value: ''},
      ],
      parameters: [...initializeParameters()]
    };
    dialog.deleteForSuccess(operation);

    const table = testCaseViewPage.parametersTable;
    table.assertRowCount(1);
  });

  it('should remove a parameter', () => {
    const testCaseViewPage = navigateToTestCase();
    testCaseViewPage.openDeleteParamDialog('PARAM_COLUMN_1', {'PARAMETER_USED': false});

    const removeDialog = testCaseViewPage.paramRemoveDialog;

    const operation: TestCaseParameterOperationReport = {
      dataSets: [{id: 1, name: 'Dataset'}, {id: 2, name: 'Dataset 2'}],
      paramValues: [
        {id: 3, parameterId: 2, datasetId: 1, value: ''},
        {id: 4, parameterId: 2, datasetId: 2, value: ''},
        {id: 5, parameterId: 3, datasetId: 1, value: ''},
        {id: 6, parameterId: 3, datasetId: 2, value: ''}
      ],
      parameters: [{
        id: 2,
        name: 'param2',
        description: '',
        sourceTestCaseId: 5,
        sourceTestCaseName: 'TestCase_5',
        sourceTestCaseProjectName: 'Project 42',
        sourceTestCaseReference: ''
      }, {
        id: 3,
        name: 'param3',
        description: '',
        sourceTestCaseId: 6,
        sourceTestCaseName: 'TestCase_6',
        sourceTestCaseProjectName: 'Project 42',
        sourceTestCaseReference: '006'
      }]
    };

    removeDialog.deleteForSuccess(operation);

    const table = testCaseViewPage.parametersTable;
    table.assertRowCount(2);
    table.assertColumnCount(4);
  });

  it('should update paramValue', () => {
    const testCaseViewPage = navigateToTestCase();
    const table = testCaseViewPage.parametersTable;
    const row = table.getRow(1);
    const cell = row.cell('PARAM_COLUMN_1');
    cell.textRenderer().editText('paramValue', 'dataset-param-values');
    cell.assertExist();
    cell.textRenderer().assertContainText('paramValue');

  });

  it('should update parameter name', () => {
    const testCaseViewPage = navigateToTestCase();
    const table = testCaseViewPage.parametersTable;
    const headerRow = table.getHeaderRow();
    const cell = headerRow.cell('PARAM_COLUMN_1');
    cell.textRenderer().editTextResponse('ParamDeTest', 'parameters/*/rename', {testStepList: [], prerequisite: ''});
    cell.assertExist();
    cell.textRenderer().assertContainText('ParamDeTest');
  });

  it('should rename parameter by dialog', () => {
    const testCaseViewPage = navigateToTestCase();
    const table = testCaseViewPage.parametersTable;
    const informationDialog = testCaseViewPage.openInformationParamDialog('PARAM_COLUMN_1');
    const nameField = informationDialog.getNameField('parameters/*/rename');
    nameField.checkContent('param1');
    nameField.setAndConfirmValue('param_1', {testStepList: [], prerequisite: ''});
    nameField.checkContent('param_1');
    informationDialog.closeDialog();
    const headerRow = table.getHeaderRow();
    headerRow.cell('PARAM_COLUMN_1').textRenderer().assertContainText('param_1');
  });

  it('should change description of parameter', () => {

    const testCaseViewPage = navigateToTestCase();
    const informationDialog = testCaseViewPage.openInformationParamDialog('PARAM_COLUMN_1');
    const descriptionField = informationDialog.getDescriptionField('parameters/*/description');
    descriptionField.setAndConfirmValue('description');
    descriptionField.checkTextContent('description');
    informationDialog.closeDialog();
  });

  it('should display test case source link', () => {
    const testCaseViewPage = navigateToTestCase();
    const informationDialog = testCaseViewPage.openInformationParamDialog('PARAM_COLUMN_2');
    informationDialog.testCaseSourceLink.containText('TestCase_5 (Project 42)');
    informationDialog.closeDialog();
  });

  function navigateToTestCase(): TestCaseViewParametersPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'TestCaseLibrary-1',
        children: [],
        data: {'NAME': 'Project1'}
      } as unknown as DataRow]
    };
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
    const libraryRefreshAtOpen = [
      {
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'TestCase-3',
        children: [],
        projectId: 1,
        data: {'NAME': 'TestCase3', 'CHILD_COUNT': 0, 'TC_STATUS': 'APPROVED', 'TC_KIND': 'STANDARD', 'IMPORTANCE': 'LOW'},
        parentRowId: 'TestCaseLibrary-1',
      } as unknown as DataRow];

    testCaseWorkspacePage.tree.openNode('TestCaseLibrary-1', libraryRefreshAtOpen);
    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: []
      },
      reference: '',
      description: '',
      uuid: '',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: initializeParameters(),
      nbIssues: 0,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30'),
      lastModifiedBy: 'admin',
      kind: 'STANDARD',
      importanceAuto: false,
      importance: 'LOW',
      executions: [],
      datasets: initializeDataSets(),
      datasetParamValues: initializeDataSetParamValue(),
      createdOn: new Date('2020-03-09 10:30'),
      createdBy: 'admin',
      coverages: [],
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [],
      lastExecutionStatus: 'SUCCESS',
      script: ''
    });
    const testCaseViewPage = testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
    return testCaseViewPage.clickAnchorLink('parameters');
  }

  function initializeParameters(): Parameter[] {
    return [{
      id: 1,
      name: 'param1',
      description: '',
      sourceTestCaseId: 3,
      sourceTestCaseName: '',
      sourceTestCaseProjectName: '',
      sourceTestCaseReference: ''
    }, {
      id: 2,
      name: 'param2',
      description: '',
      sourceTestCaseId: 5,
      sourceTestCaseName: 'TestCase_5',
      sourceTestCaseProjectName: 'Project 42',
      sourceTestCaseReference: ''
    }, {
      id: 3,
      name: 'param3',
      description: '',
      sourceTestCaseId: 6,
      sourceTestCaseName: 'TestCase_6',
      sourceTestCaseProjectName: 'Project 42',
      sourceTestCaseReference: '006'
    }];
  }

  function initializeDataSets(): Dataset[] {
    return [
      {id: 1, name: 'Dataset'},
      {id: 2, name: 'Dataset 2'}
    ];
  }

  function initializeDataSetParamValue(): DatasetParamValue[] {
    return [
      {id: 1, parameterId: 1, datasetId: 1, value: ''},
      {id: 2, parameterId: 1, datasetId: 2, value: ''},
      {id: 3, parameterId: 2, datasetId: 1, value: ''},
      {id: 4, parameterId: 2, datasetId: 2, value: ''},
      {id: 5, parameterId: 3, datasetId: 1, value: ''},
      {id: 6, parameterId: 3, datasetId: 2, value: ''}
    ];
  }
});

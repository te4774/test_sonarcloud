import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {defaultReferentialData} from '../../../../utils/referential/default-referential-data.const';
import {InputType} from '../../../../model/customfield/customfield.model';
import {EditableTextFieldElement} from '../../../../page-objects/elements/forms/editable-text-field.element';
import {EditableRichTextFieldElement} from '../../../../page-objects/elements/forms/editable-rich-text-field.element';
import {EditableNumericFieldElement} from '../../../../page-objects/elements/forms/editable-numeric-field.element';
import {EditableTagFieldElement} from '../../../../page-objects/elements/forms/editable-tag-field.element';
import {TestStepsViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-steps-view.page';
import {ActionStepModel, TestStepModel} from '../../../../model/test-case/test-step.model';
import {mockTestCaseModel} from '../../../../data-mock/test-case.data-mock';


const initialTestCaseRow: DataRow = {
  id: 'TestCase-3',
  children: [],
  projectId: 1,
  parentRowId: 'TestCaseLibrary-1',
  data: {'NAME': 'TestCase3', 'CHILD_COUNT': 0, 'TC_STATUS': 'APPROVED', 'TC_KIND': 'STANDARD', 'IMPORTANCE': 'HIGH'},
  state: DataRowOpenState.leaf
} as unknown as DataRow;

describe('Test Case View With Milestone Locked', function () {
  it('should forbid modifications in information panel', () => {
    const page = navigateToTestCaseBoundToLockedMilestone();
    const informationPanel = page.informationPanel;
    informationPanel.nameTextField.assertIsNotEditable();
    informationPanel.importanceSelectField.assertIsNotEditable();
    informationPanel.descriptionRichField.assertIsNotEditable();
    const fieldA = page.getCustomField('fieldA', InputType.PLAIN_TEXT) as EditableTextFieldElement;
    fieldA.assertIsNotEditable();
    const fieldB = page.getCustomField('fieldB', InputType.NUMERIC) as EditableNumericFieldElement;
    fieldB.assertIsNotEditable();
    const fieldC = page.getCustomField('fieldC', InputType.TAG) as EditableTagFieldElement;
    fieldC.assertIsNotEditable();
    fieldC.checkTagsWhenFieldIsNotEditable('o1', 'o2');
    const fieldD = page.getCustomField('fieldD', InputType.PLAIN_TEXT) as EditableTextFieldElement;
    fieldD.assertIsNotEditable();
    const fieldE = page.getCustomField('fieldE', InputType.RICH_TEXT) as EditableRichTextFieldElement;
    fieldE.assertIsNotEditable();
  });

  it('should forbid test step creation', () => {
    const page = navigateToTestCaseBoundToLockedMilestone();
    const stepPanel = page.clickAnchorLink<TestStepsViewPage>('steps');
    stepPanel.assertIsEmpty();
  });

  it('should forbid test step modification', () => {
    const testSteps: ActionStepModel[] = [
      {
        id: 1,
        kind: 'action-step',
        action: 'action',
        expectedResult: 'reaction',
        attachmentList: {
          id: 1,
          attachments: []
        },
        customFieldValues: [],
        projectId: 1,
        stepOrder: 0
      }
    ];
    const page = navigateToTestCaseBoundToLockedMilestone(testSteps);
    const stepPanel = page.clickAnchorLink<TestStepsViewPage>('steps');
    const actionStep = stepPanel.getActionStepByIndex(0);
    actionStep.getActionField().assertIsNotEditable();
    actionStep.getExpectedResultField().assertIsNotEditable();
  });

  it('should show milestone locked warning', () => {
    const page = navigateToTestCaseBoundToLockedMilestone();
    page.assertMilestoneLockedWarningExist();
  });

  function setupTestCaseWorkspace(referentialData = defaultReferentialData) {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'TestCaseLibrary-1',
        children: [],
        data: {'NAME': 'Project1'}
      } as unknown as DataRow]
    };
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes, referentialData);
    const libraryRefreshAtOpen = [
      {
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {...initialTestCaseRow}];

    testCaseWorkspacePage.tree.openNode('TestCaseLibrary-1', libraryRefreshAtOpen);
    return testCaseWorkspacePage;
  }

  function createTestCaseModel() {
    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      customFieldValues: [
        {
          id: 0,
          value: 'a little value',
          cufId: 0,
          fieldType: 'CF'
        },
        {
          id: 1,
          value: '2',
          cufId: 1,
          fieldType: 'NUM'
        },
        {
          id: 2,
          value: 'o1|o2',
          cufId: 2,
          fieldType: 'TAG'
        },
        {
          id: 3,
          value: 'a little value',
          cufId: 3,
          fieldType: 'CF'
        },
        {
          id: 4,
          value: '<p>a little value</p>',
          cufId: 4,
          fieldType: 'RTF'
        },
        {
          id: 5,
          value: '2020-02-14',
          cufId: 5,
          fieldType: 'CF'
        },
        {
          id: 6,
          value: 'true',
          cufId: 6,
          fieldType: 'CF'
        },
        {
          id: 7,
          value: 'Option C',
          cufId: 7,
          fieldType: 'CF'
        }
      ],
      attachmentList: {
        id: 1,
        attachments: []
      },
      reference: '',
      description: '',
      uuid: '',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: [],
      nbIssues: 0,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30'),
      lastModifiedBy: 'admin',
      kind: 'STANDARD',
      importanceAuto: true,
      importance: 'LOW',
      executions: [],
      datasets: [],
      datasetParamValues: [],
      createdOn: new Date('2020-03-09 10:30'),
      createdBy: 'admin',
      coverages: [],
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [],
      lastExecutionStatus: 'SUCCESS',
      script: ''
    });
    return model;
  }

  function navigateToTestCaseBoundToLockedMilestone(testSteps: TestStepModel[] = []): TestCaseViewPage {
    const testCaseWorkspacePage = setupTestCaseWorkspace();
    const model: TestCaseModel = {
      ...createTestCaseModel(),
      prerequisite: 'prerequisite',
      testSteps,
      milestones: [{
        id: 1,
        label: 'locked',
        status: 'LOCKED',
        description: '',
        endDate: new Date(),
        ownerFistName: '',
        ownerLastName: '',
        ownerLogin: '',
        range: 'GLOBAL',
        createdBy: '',
        createdOn: new Date(),
        lastModifiedBy: '',
        lastModifiedOn: new Date()
      }, {
        id: 2,
        label: 'progress',
        status: 'IN_PROGRESS',
        description: '',
        endDate: new Date(),
        ownerFistName: '',
        ownerLastName: '',
        ownerLogin: '',
        range: 'GLOBAL',
        createdBy: '',
        createdOn: new Date(),
        lastModifiedBy: '',
        lastModifiedOn: new Date()
      }]
    };
    return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
  }
});

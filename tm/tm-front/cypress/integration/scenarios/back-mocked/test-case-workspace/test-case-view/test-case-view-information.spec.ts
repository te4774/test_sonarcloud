import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {defaultReferentialData} from '../../../../utils/referential/default-referential-data.const';
import {InputType} from '../../../../model/customfield/customfield.model';
import {EditableTextFieldElement} from '../../../../page-objects/elements/forms/editable-text-field.element';
import {EditableRichTextFieldElement} from '../../../../page-objects/elements/forms/editable-rich-text-field.element';
import {CheckBoxElement} from '../../../../page-objects/elements/forms/check-box.element';
import {EditableSelectFieldElement} from '../../../../page-objects/elements/forms/editable-select-field.element';
import {EditableNumericFieldElement} from '../../../../page-objects/elements/forms/editable-numeric-field.element';
import {EditableDateFieldElement} from '../../../../page-objects/elements/forms/editable-date-field.element';
import {EditableTagFieldElement} from '../../../../page-objects/elements/forms/editable-tag-field.element';
import {mockTestCaseModel} from '../../../../data-mock/test-case.data-mock';

const initialTestCaseRow: DataRow = {
  id: 'TestCase-3',
  children: [],
  projectId: 1,
  parentRowId: 'TestCaseLibrary-1',
  data: {'NAME': 'TestCase3', 'CHILD_COUNT': 0, 'TC_STATUS': 'APPROVED', 'TC_KIND': 'STANDARD', 'IMPORTANCE': 'HIGH'}
} as unknown as DataRow;

describe('Test Case View - Information', function () {
  it('should display test case information', () => {
    const tcPage = navigateToTestCase();
    const infoPanel = tcPage.informationPanel;

    // Check basic info
    tcPage.checkData('test-case-created', '09/03/2020 10:30 (admin)');
    tcPage.checkData('test-case-status', 'En cours de rédaction');
    tcPage.checkData('test-case-lastModified', '09/03/2020 10:30 (admin)');
    tcPage.checkData('test-case-nature', 'Non définie');
    tcPage.checkData('test-case-type', 'Non défini');
    tcPage.checkData('test-case-kind', 'Classique');
    tcPage.checkData('test-case-id', '3');
    tcPage.checkData('entity-reference', '');
    infoPanel.descriptionRichField.checkTextContent('Cliquer pour renseigner une description.');
    infoPanel.milestonesField.checkMilestones();

    infoPanel.statusSelectField.checkAllOptions(
      ['En cours de rédaction', 'À approuver', 'Approuvé', 'Obsolète', 'À mettre à jour']
    );

    // Importance field : check initial state
    infoPanel.importanceSelectField.checkSelectedOption('Faible');
    infoPanel.importanceSelectField.assertIsNotEditable();
    infoPanel.importanceAutoCheckBox.checkState(true);

    // Check CUF values
    tcPage.getTextCustomField('fieldA').checkContent('a little value');
    tcPage.getNumericCustomField('fieldB').checkContent(2);
    tcPage.getTagCustomField('fieldC').checkTags('o1', 'o2');
    tcPage.getTextCustomField('fieldD').checkContent('a little value');
    tcPage.getRichTextCustomField('fieldE').checkTextContent('a little value');
    tcPage.getDateCustomField('date').checkContent('14/02/2020', true);
    tcPage.getCheckboxCustomField('checkbox').checkState(true);
    tcPage.getSelectCustomField('dropdown').checkSelectedOption('Option C');
  });

  it('should allow modifications on TC infos', () => {
    const tcPage = navigateToTestCase();
    const page = tcPage.informationPanel;

    // Change basic TC fields
    page.rename('Mon super test case', {
      dataRows: [{
        ...initialTestCaseRow,
        data: {...initialTestCaseRow.data, 'NAME': 'Mon super test case'}
      }]
    });
    page.changeReference('REF', {
      dataRows: [{
        ...initialTestCaseRow,
        data: {...initialTestCaseRow.data, 'NAME': 'REF-Mon super test case'}
      }]
    });
    // All subsequent change that induce workspace tree refresh are covered by the mock defined in changeName and changeReference
    page.statusSelectField.setAndConfirmValueNoButton('Approuvé');
    page.natureSelectField.setAndConfirmValueNoButton('Fonctionnel');
    page.typeSelectField.setAndConfirmValueNoButton('Correctif');

    // Importance field : check modifications
    page.importanceAutoCheckBox.toggleState('{"importance":"LOW"}');
    page.importanceAutoCheckBox.checkState(false);
    page.importanceSelectField.checkSelectedOption('Faible');
    page.importanceSelectField.setAndConfirmValueNoButton('Moyenne');
    page.importanceAutoCheckBox.toggleState('{"importance":"LOW"}');
    page.importanceAutoCheckBox.checkState(true);
    page.importanceSelectField.checkSelectedOption('Faible');
    page.importanceSelectField.assertIsNotEditable();

    const descriptionElement = page.descriptionRichField;
    descriptionElement.enableEditMode();
    descriptionElement.setValue('Hello, World!');
    descriptionElement.confirm('Hello, World!');
    descriptionElement.enableEditMode();
    descriptionElement.clear();
    descriptionElement.confirm(`Cliquer pour renseigner une description`);

    const milestonesElement = page.milestonesField;

    // const multipleMilestonePicker = milestonesElement.openMilestoneDialog();
    // multipleMilestonePicker.selectMilestone(2);
    // multipleMilestonePicker.assertMilestoneIsSelected(2);
    // multipleMilestonePicker.confirm();
  });

  /*
  it('should cancel modifications on TC infos', () => {
    const page = navigateToTestCase();

    page.nameTextField.setValue('Mon super test case');
    page.nameTextField.cancel();
    page.nameTextField.checkContent('TestCase3', true);

    page.referenceTextField.setValue('REF');
    page.referenceTextField.cancel();
    page.referenceTextField.checkContent('Référence du cas de test', true);

    page.statusSelectField.setAndCancel('À approuver', 'En cours de rédaction');
    page.natureSelectField.setAndCancel('Métier', 'Non définie');
    page.typeSelectField.setAndCancel('Partenaire', 'Non défini');
    page.importanceSelectField.setAndCancel('Très haute', 'Faible');
  });
  */

  it('should allow modifications on TC custom fields', () => {
    const page = navigateToTestCase();

    // Set cufs values
    const fieldA = page.getCustomField('fieldA', InputType.PLAIN_TEXT) as EditableTextFieldElement;
    const fieldB = page.getCustomField('fieldB', InputType.NUMERIC) as EditableNumericFieldElement;
    const fieldC = page.getCustomField('fieldC', InputType.TAG) as EditableTagFieldElement;
    const fieldD = page.getCustomField('fieldD', InputType.PLAIN_TEXT) as EditableTextFieldElement;
    const fieldE = page.getCustomField('fieldE', InputType.RICH_TEXT) as EditableRichTextFieldElement;
    const date = page.getCustomField('date', InputType.DATE_PICKER) as EditableDateFieldElement;
    const checkbox = page.getCustomField('checkbox', InputType.CHECKBOX) as CheckBoxElement;
    const dropdown = page.getCustomField('dropdown', InputType.DROPDOWN_LIST) as EditableSelectFieldElement;

    fieldA.setAndConfirmValue('a new value');
    fieldB.setAndConfirmValue(3);
    fieldD.setAndConfirmValue('a new value');
    fieldE.setAndConfirmValue('a new value');

    // Date field
    const localeToday = EditableDateFieldElement.dateToDisplayString(new Date(Date.now()));
    date.enableEditMode();
    date.setToTodayAndConfirm();
    date.checkContent(localeToday, true);
    checkbox.toggleState();
    checkbox.checkState(false);
    dropdown.setAndConfirmValueNoButton('Option A');

    // Tags
    fieldC.toggleTagFieldOptions('o1', 'o3');
    fieldC.typeNewTag('Hello, World!');
    fieldC.checkTags('o2', 'o3', 'Hello, World!');
  });

  it('should not show milestone locked warning', () => {
    const page = navigateToTestCase();
    page.assertMilestoneLockedWarningNotExist();
  });

  function navigateToTestCase(): TestCaseViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'TestCaseLibrary-1',
        children: [],
        data: {'NAME': 'Project1'}
      } as unknown as DataRow]
    };
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes, defaultReferentialData);
    const libraryRefreshAtOpen = [
      {
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {...initialTestCaseRow}];

    testCaseWorkspacePage.tree.openNode('TestCaseLibrary-1', libraryRefreshAtOpen);
    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      customFieldValues: [
        {
          id: 0,
          value: 'a little value',
          cufId: 0,
          fieldType: 'CF'
        },
        {
          id: 1,
          value: '2',
          cufId: 1,
          fieldType: 'NUM'
        },
        {
          id: 2,
          value: 'o1|o2',
          cufId: 2,
          fieldType: 'TAG'
        },
        {
          id: 3,
          value: 'a little value',
          cufId: 3,
          fieldType: 'CF'
        },
        {
          id: 4,
          value: '<p>a little value</p>',
          cufId: 4,
          fieldType: 'RTF'
        },
        {
          id: 5,
          value: '2020-02-14',
          cufId: 5,
          fieldType: 'CF'
        },
        {
          id: 6,
          value: 'true',
          cufId: 6,
          fieldType: 'CF'
        },
        {
          id: 7,
          value: 'Option C',
          cufId: 7,
          fieldType: 'CF'
        }
      ],
      attachmentList: {
        id: 1,
        attachments: []
      },
      reference: '',
      description: '',
      uuid: '',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: [],
      nbIssues: 0,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30'),
      lastModifiedBy: 'admin',
      kind: 'STANDARD',
      importanceAuto: true,
      importance: 'LOW',
      executions: [],
      datasets: [],
      datasetParamValues: [],
      createdOn: new Date('2020-03-09 10:30'),
      createdBy: 'admin',
      coverages: [],
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [],
      lastExecutionStatus: 'SUCCESS',
      script: '',
      actionWordLibraryActive: false
    });
    return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
  }
});

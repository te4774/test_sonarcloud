import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {defaultReferentialData} from '../../../../utils/referential/default-referential-data.const';
import {
  TestCaseViewScriptPage
} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view-script.page';
import {mockTestCaseModel} from '../../../../data-mock/test-case.data-mock';

const initialTestCaseRow: DataRow = {
  id: 'TestCase-3',
  children: [],
  projectId: 1,
  parentRowId: 'TestCaseLibrary-1',
  data: {'NAME': 'TestCase3', 'CHILD_COUNT': 0, 'TC_STATUS': 'APPROVED', 'TC_KIND': 'STANDARD', 'IMPORTANCE': 'HIGH'}
} as unknown as DataRow;

describe('Test Case View - Script', function () {
  it('should display gherkin test case script', () => {
    const scriptPage = navigateToTestCase();
    scriptPage.assertAceEditorExist();
  });

  it('should insert snippet', () => {
    const scriptPage = navigateToTestCase();
    scriptPage.showEditButtons();
    scriptPage.showSnippetPanel();
  });

  it.skip('should display help', () => {
    const scriptPage = navigateToTestCase();
    scriptPage.toggleHelp();
    scriptPage.assertHelpExist();
  });

  function navigateToTestCase(): TestCaseViewScriptPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'TestCaseLibrary-1',
        children: [],
        data: {'NAME': 'Project1'}
      } as unknown as DataRow]
    };
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes, defaultReferentialData);
    const libraryRefreshAtOpen = [
      {
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {...initialTestCaseRow}];

    testCaseWorkspacePage.tree.openNode('TestCaseLibrary-1', libraryRefreshAtOpen);
    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: []
      },
      reference: '',
      description: '',
      uuid: '',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: [],
      nbIssues: 0,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30'),
      lastModifiedBy: 'admin',
      kind: 'GHERKIN',
      importanceAuto: true,
      importance: 'LOW',
      executions: [],
      datasets: [],
      datasetParamValues: [],
      createdOn: new Date('2020-03-09 10:30'),
      createdBy: 'admin',
      coverages: [],
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [],
      lastExecutionStatus: 'SUCCESS',
      script: '# language: fr\n' +
        'Fonctionnalité: hfghfghfgjhgj'
    });
    const tcPage = testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
    return tcPage.clickAnchorLink('script') as TestCaseViewScriptPage;

  }
});

import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {Permissions, ProjectPermissions} from '../../../../model/permissions/permissions.model';
import {ReferentialData} from '../../../../model/referential-data.model';
import {TestCaseParameterOperationReport} from '../../../../model/test-case/change-dataset-param-operation.model';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {KeywordStepModel, TestStepModel} from '../../../../model/test-case/test-step.model';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {COMMENT, DATATABLE, DOCSTRING} from '../../../../page-objects/elements/test-steps/keyword-step.element';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {
  KeywordTestStepsViewPage
} from '../../../../page-objects/pages/test-case-workspace/test-case/keyword-test-step-view-page';
import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';
import {GridElement} from '../../../../page-objects/elements/grid/grid.element';
import * as _ from 'lodash';

describe('Keyword Test Case View - Steps', function () {
  it('should display keyword test case steps', () => {
    const testStepPage = navigationToKeywordTestStepsPage();
    testStepPage.checkStepsOrder([2, 1, 5]);
    [0, 1, 2].forEach(index => {
      const stepElement = testStepPage.getStepByIndex(index);
      stepElement.checkIndexLabel();
    });
    [
      { index: 0, label: 'Étant donné que', action: `go to <span style="color: blue;">engine</span> room`, datatable: '', docstring: '', comment: '' },
      { index: 1, label: 'Quand', action: 'check main engines gimbal', datatable: createDefaultDatatable(), docstring: createDefaultDocstring(), comment: createDefaultComment() },
      { index: 2, label: 'Alors', action: 'everything is great', datatable: '', docstring: '', comment: '' }
    ].forEach(({ index, label, action , datatable, docstring, comment }) => {
      testStepPage.checkExistenceAndContentOfStepByIndex(index, label, action, datatable, docstring, comment);
    });
  });

  it('should add a new keyword test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage();
    testStepPage.chooseKeyword('Alors');
    testStepPage.fillAction(`I eat <number> apples.`);
    testStepPage.addKeywordStep(
      false,
      {
      testStep: {
        id: 7,
        projectId: 1,
        stepOrder: 3,
        kind: 'keyword-step',
        keyword: 'THEN',
        action: `I eat <number> apples.`,
        styledAction: `I eat <span style="color: blue;">&lt;number&gt;</span> apples.`
      } as KeywordStepModel,
      operationReport: {
        parameters: [],
        dataSets: [],
        paramValues: []
      }});

    testStepPage.checkExistenceAndContentOfStepByIndex(
      3,
      'Alors',
      `I eat <span style="color: blue;">&lt;number&gt;</span> apples.`);
  });

  it('should not be able to add a keyword test step if not permitted', () => {
    const testStepPage = navigationToKeywordTestStepsPage(createReferentialDataWithoutWritePermissions());
    testStepPage.checkKeywordSelectFieldNotVisible();
    testStepPage.checkActionTextFieldNotVisible();
  });

  it('should delete single keyword test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage();
    const step = testStepPage.getStepByIndex(0);
    step.selectStep();
    step.deleteStep(generateEmptyDeleteTestStepReport());

    testStepPage.checkStepsOrder([1, 5]);
  });

  it('should delete multiple keyword test steps', () => {
    const testStepPage = navigationToKeywordTestStepsPage();
    const step1 = testStepPage.getStepByIndex(0);
    const step3 = testStepPage.getStepByIndex(2);

    step1.toggleStepSelection();
    step3.toggleStepSelection();

    step3.deleteStep(generateEmptyDeleteTestStepReport());

    testStepPage.checkStepsOrder([1]);
  });

  it('should not be able to delete a keyword test step if not permitted', () => {
    const testStepPage = navigationToKeywordTestStepsPage(createReferentialDataWithoutWritePermissions());
    const step1 = testStepPage.getStepByIndex(0);
    step1.selectStep();
    step1.checkDeleteButtonNotVisible();
  });

  it('should add keyword step by selecting with mouse a suggested action word in autocomplete text field', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    testStepPage.chooseKeyword('Et');
    testStepPage.fillAutocompleteAction('je', { actionList: [
        `je me connecte`,
        `je renseigne "nomUtilisateur" dans le champ 'Nom d'utilisateur'.`,
        `je renseigne "motDePasse" dans le champ 'Mot de passe'.`
      ] });
    testStepPage.checkAutocompletionMenuIsVisible();
    testStepPage.checkAutocompletionOptions([
      `je me connecte`,
      `je renseigne "nomUtilisateur" dans le champ 'Nom d'utilisateur'.`,
      `je renseigne "motDePasse" dans le champ 'Mot de passe'.`
    ]);
    testStepPage.chooseAutocompletionOptionWithClick('je me connecte');
    testStepPage.addKeywordStep(
      true,
      {
      testStep: {
        id: 7,
        projectId: 1,
        stepOrder: 3,
        kind: 'keyword-step',
        keyword: 'AND',
        action: `je me connecte`,
        styledAction: `je me connecte`
      } as KeywordStepModel,
      operationReport: {
        parameters: [],
        dataSets: [],
        paramValues: []
      }});

    testStepPage.checkExistenceAndContentOfStepByIndex(
      3,
      'Et',
      `je me connecte`);
  });

  it('should add keyword step by selecting suggested multi-project action word and selecting its project', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    testStepPage.chooseKeyword('Et');
    testStepPage.fillAutocompleteAction('je', { actionList: [
        `je me connecte`,
        `je renseigne "nomUtilisateur" dans le champ 'Nom d'utilisateur'.`,
        `je renseigne "motDePasse" dans le champ 'Mot de passe'.`
      ] });
    testStepPage.checkAutocompletionMenuIsVisible();
    testStepPage.checkAutocompletionOptions([
      `je me connecte`,
      `je renseigne "nomUtilisateur" dans le champ 'Nom d'utilisateur'.`,
      `je renseigne "motDePasse" dans le champ 'Mot de passe'.`
    ]);
    testStepPage.chooseAutocompletionOptionWithClick('je me connecte');
    testStepPage.addKeywordStepWithActionWordProjectChoice(
      'project_2',
      {
        testStep: {
          id: 7,
          projectId: 1,
          stepOrder: 3,
          kind: 'keyword-step',
          keyword: 'AND',
          action: `je me connecte`,
          styledAction: `je me connecte`
        } as KeywordStepModel,
        operationReport: {
          parameters: [],
          dataSets: [],
          paramValues: []
        }},
      { 'project_1': 78, 'project_2': 12, 'project_3': 99 });
    testStepPage.checkExistenceAndContentOfStepByIndex(
      3,
      'Et',
      `je me connecte`);
  });

  it('should add keyword step by selecting with keyboard a suggested action word in autocomplete text field', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    testStepPage.chooseKeyword('Et');
    testStepPage.fillAutocompleteAction('je', { actionList: [
        `je me connecte`,
        `je renseigne "nomUtilisateur" dans le champ 'Nom d'utilisateur'.`,
        `je renseigne "motDePasse" dans le champ 'Mot de passe'.`
      ] });
    testStepPage.checkAutocompletionMenuIsVisible();
    testStepPage.checkAutocompletionOptions([
      `je me connecte`,
      `je renseigne "nomUtilisateur" dans le champ 'Nom d'utilisateur'.`,
      `je renseigne "motDePasse" dans le champ 'Mot de passe'.`
    ]);
    testStepPage.chooseAutocompletionOptionAtIndexWithKeyboard(0, 'je me connecte');
    testStepPage.addKeywordStep(
      true,
      {
      testStep: {
        id: 7,
        projectId: 1,
        stepOrder: 3,
        kind: 'keyword-step',
        keyword: 'AND',
        action: `je me connecte`,
        styledAction: `je me connecte`
      } as KeywordStepModel,
      operationReport: {
        parameters: [],
        dataSets: [],
        paramValues: []
      }});

    testStepPage.checkExistenceAndContentOfStepByIndex(
      3,
      'Et',
      `je me connecte`);
  });

  it('should change keyword of a test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(0);
    const keywordSelectField = step.getKeywordField();
    keywordSelectField.setAndConfirmValueNoButton('Et');
    keywordSelectField.setAndConfirmValueNoButton('Étant donné que');
  });

  it('should change action of a test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createDefaultTestCaseModel());
    const step = testStepPage.getStepByIndex(0);
    const actionTextField = step.getActionField();
    actionTextField.setAndCancel('go to "office" room');
    actionTextField.setAndConfirm(
      'go to "rest" room and take 1 coffee',
      false,
      {
        action: 'go to "rest" room and take 1 coffee',
        styledAction: 'go to go to <span style="color: blue;">rest</span> room and take <span style="color: blue;">1</span> coffee',
        paramOperationReport: { parameters: [], dataSets: [], paramValues: [] }
      });
  });

  it('should change action of a test step using mouse in autocomplete menu', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(0);
    const actionTextField = step.getActionField();
    actionTextField.fillInput('je', { actionList: [
        `je me connecte`,
        `je renseigne "nomUtilisateur" dans le champ 'Nom d'utilisateur'.`,
        `je renseigne "motDePasse" dans le champ 'Mot de passe'.`
      ]});
    actionTextField.checkAutocompletionMenuIsVisible();
    actionTextField.checkAutocompletionOptions([
      `je me connecte`,
      `je renseigne "nomUtilisateur" dans le champ 'Nom d'utilisateur'.`,
      `je renseigne "motDePasse" dans le champ 'Mot de passe'.`
    ]);
    actionTextField.chooseAutocompletionOptionWithClick('je me connecte');
    actionTextField.confirm(
      true,
      {
      action: 'je me connecte',
      styledAction: 'je me connecte',
      paramOperationReport: { parameters: [], dataSets: [], paramValues: [] }
    });
    actionTextField.checkContent('je me connecte');
  });

  it('should change action of test step by selecting a suggested multi-project action word autocomplete menu', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(0);
    const actionTextField = step.getActionField();
    actionTextField.fillInput('je', { actionList: [
        `je me connecte`,
        `je renseigne "nomUtilisateur" dans le champ 'Nom d'utilisateur'.`,
        `je renseigne "motDePasse" dans le champ 'Mot de passe'.`
      ]});
    actionTextField.checkAutocompletionMenuIsVisible();
    actionTextField.checkAutocompletionOptions([
      `je me connecte`,
      `je renseigne "nomUtilisateur" dans le champ 'Nom d'utilisateur'.`,
      `je renseigne "motDePasse" dans le champ 'Mot de passe'.`
    ]);
    actionTextField.chooseAutocompletionOptionWithClick('je me connecte');
    actionTextField.confirmWithActionWordProjectChoice(
      'project_2',
      {
        action: 'je me connecte',
        styledAction: 'je me connecte',
        paramOperationReport: { parameters: [], dataSets: [], paramValues: [] }
      },
      { 'project_1': 78, 'project_2': 12, 'project_3': 99 });
    actionTextField.checkContent('je me connecte');
  });

  it('should change action of a test step using keyboard in autocomplete menu', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(0);
    const actionTextField = step.getActionField();
    actionTextField.fillInput('je', { actionList: [
        `je me connecte`,
        `je renseigne "nomUtilisateur" dans le champ 'Nom d'utilisateur'.`,
        `je renseigne "motDePasse" dans le champ 'Mot de passe'.`
      ]});
    actionTextField.checkAutocompletionMenuIsVisible();
    actionTextField.checkAutocompletionOptions([
      `je me connecte`,
      `je renseigne "nomUtilisateur" dans le champ 'Nom d'utilisateur'.`,
      `je renseigne "motDePasse" dans le champ 'Mot de passe'.`
    ]);
    actionTextField.chooseAutocompletionOptionAtIndexWithKeyboard(0, 'je me connecte');
    actionTextField.confirm(
      true,
      {
      action: 'je me connecte',
      styledAction: 'je me connecte',
      paramOperationReport: { parameters: [], dataSets: [], paramValues: [] }
    });
    actionTextField.checkContent('je me connecte');
  });

  it('should create datatable of a test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(0);
    step.createSection(DATATABLE);
    step.assertSectionExist(DATATABLE);
    const datatableTextAreaField = step.getTextAreaField(DATATABLE);
    datatableTextAreaField.checkEditMode(true);
    datatableTextAreaField.checkEditContent(`| produit | prix |
| Expresso | 0.40 |`);
    datatableTextAreaField.confirm();
    datatableTextAreaField.checkEditMode(false);
    datatableTextAreaField.checkDisplayContent(`| produit | prix |
| Expresso | 0.40 |`);
  });

  it('should create docstring of a test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(0);
    step.createSection(DOCSTRING);
    step.assertSectionExist(DOCSTRING);
    const docstringTextAreaField = step.getTextAreaField(DOCSTRING);
    docstringTextAreaField.checkEditMode(true);
    docstringTextAreaField.checkEditContent('');
    docstringTextAreaField.typeValue(`Abc
Def
Ghi`);
    docstringTextAreaField.confirm();
    docstringTextAreaField.checkEditMode(false);
    docstringTextAreaField.checkDisplayContent(`Abc
Def
Ghi`);
  });

  it('should create comment of a test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(0);
    step.createSection(COMMENT);
    step.assertSectionExist(COMMENT);
    const commentTextAreaField = step.getTextAreaField(COMMENT);
    commentTextAreaField.checkEditMode(true);
    commentTextAreaField.checkEditContent('');
    commentTextAreaField.typeValue(`Comment
#1
#2`);
    commentTextAreaField.confirm();
    commentTextAreaField.checkEditMode(false);
    commentTextAreaField.checkDisplayContent(`Comment
#1
#2`);
  });

  it('should change datatable of a test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(1);
    const datatableTextAreaField = step.getTextAreaField(DATATABLE);
    datatableTextAreaField.setAndCancel(`| name | age | mail |
| jean`,
      createDefaultDatatable());
    datatableTextAreaField.setAndConfirm(
`| name | age | mail |
| jane | 26 | jane@mail.com |
| jack | 19 | jack@mail.fr |`);
  });

  it('should change docstring of a test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(1);
    const docstringTextAreaField = step.getTextAreaField(DOCSTRING);
    docstringTextAreaField.setAndCancel(`Bonjour
Wor`,
      createDefaultDocstring());
    docstringTextAreaField.setAndConfirm(`Hello
World
!`);
  });

  it('should change comment of a test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(1);
    const commentTextAreaField = step.getTextAreaField(COMMENT);
    commentTextAreaField.setAndCancel(`Not
Bad`,
      createDefaultComment());
    commentTextAreaField.setAndConfirm(`Very
Well
!`);
  });

  it('should delete datatable of a test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(1);
    step.assertSectionDeleteIconAppears(DATATABLE);
    step.deleteSection(DATATABLE);
    step.assertSectionNotExist(DATATABLE);
  });

  it('should delete docstring of a test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(1);
    step.assertSectionDeleteIconAppears(DOCSTRING);
    step.deleteSection(DOCSTRING);
    step.assertSectionNotExist(DOCSTRING);
  });

  it('should delete comment of a test step', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(1);
    step.assertSectionDeleteIconAppears(COMMENT);
    step.deleteSection(COMMENT);
    step.assertSectionNotExist(COMMENT);
  });

  it('should not suggest existing action words in autocomplete text field if autocompletion is inactive', () => {
    const testStepPage = navigationToKeywordTestStepsPage();
    testStepPage.fillAction('je');
    testStepPage.checkActionTextFieldVisible();
  });

  it('should show the generated script preview in a popup', () => {
    const testStepPage = navigationToKeywordTestStepsPage();
    testStepPage.checkPreviewIconIsVisible();
    testStepPage.showScriptPreview({ script: createScriptContent() });
    testStepPage.checkScriptPreviewPopupContent(createScriptContent());
  });

  it('should move a step using arrows', () => {
    const testStepPage = navigationToKeywordTestStepsPage();
    let step = testStepPage.getStepByIndex(0);
    step.moveStepDown();
    step = testStepPage.getStepByIndex(1);
    step.moveStepDown();
    step = testStepPage.getStepByIndex(2);
    step.moveStepUp();
    testStepPage.checkStepsOrder([1, 2, 5]);
  });

  it('should move steps using drag and drop', () => {
    cy.viewport(1000, 800);
    const testStepPage = navigationToKeywordTestStepsPage();
    testStepPage.moveStepWithDragAndDrop(0, true);
    testStepPage.moveStepWithDragAndDrop(2, false, 1);
    testStepPage.checkStepsOrder([1, 2, 5]);

    const step1 = testStepPage.getStepByIndex(0);
    const step3 = testStepPage.getStepByIndex(2);

    step1.toggleStepSelection();
    step3.toggleStepSelection();

    testStepPage.moveStepWithDragAndDrop(0, true);

    testStepPage.checkStepsOrder([2, 1, 5]);
  });

  it('should navigate from keyword step to action word', () => {
    const testStepPage = navigationToKeywordTestStepsPage(null, createTestCaseModelWithAutocompleteActive());
    const step = testStepPage.getStepByIndex(0);
    step.navigateToActionWord();
  });

  it('should update project filter', () => {
    const page = navigationToKeywordTestStepsPage(initReferentialData(), createTestCaseModelWithAutocompleteActive());
    const projectPicker: GridElement = page.openProjectScopePicker();
    projectPicker.findRowId('name', 'Project2')
      .then((id) => projectPicker.getCell(id, 'select-row-column').checkBoxRender().toggleState());
    page.confirmSelectedProject();
    page.getScopeFieldValue('Project1, Project2');
  });

  /* Actions in Page */

  function navigationToKeywordTestStepsPage(referentialData?: ReferentialData, testCaseModel?: TestCaseModel) {
    const page = navigateToTestCase(referentialData, testCaseModel);
    new NavBarElement().toggle();
    page.toggleTree();
    return page.clickAnchorLink('keywordSteps') as KeywordTestStepsViewPage;
  }

  function navigateToTestCase(referentialData?: ReferentialData, testCaseModel?: TestCaseModel): TestCaseViewPage {
    const testCaseWorkspacePage = initTestCaseWorkspacePage(referentialData);
    let newTestCaseModel = createDefaultTestCaseModel();
    if (testCaseModel != null) {
      newTestCaseModel = testCaseModel;
    }
    return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', newTestCaseModel);
  }

  function initTestCaseWorkspacePage(referentialData?: ReferentialData): TestCaseWorkspacePage {
    let newReferentialData = createEntityReferentialData;
    if (referentialData != null) {
      newReferentialData = referentialData;
    }
    const initialNodes: GridResponse = createDefaultInitialNodes();
    return TestCaseWorkspacePage.initTestAtPage(initialNodes, newReferentialData);
  }

  /* Data Mock */

  function createReferentialDataWithoutWritePermissions() {
    return new ReferentialDataMockBuilder()
      .withProjects({
        name: 'project 1',
        permissions: {
          TEST_CASE_LIBRARY: [Permissions.READ],
        } as ProjectPermissions
      })
      .build();
  }

  function createDefaultInitialNodes(): GridResponse {
    return {
      count: 3,
      dataRows: [
        {
          id: 'TestCaseLibrary-1',
          children: ['TestCase-3', 'TestCase-4'],
          data: {'NAME': 'Project1', 'CHILD_COUNT': 2},
          state: DataRowOpenState.open
        } as unknown as DataRow,
        {
          id: 'TestCase-3',
          children: [],
          projectId: 1,
          parentRowId: 'TestCaseLibrary-1',
          state: DataRowOpenState.leaf,
          data: {
            'NAME': 'Launch Atlantis',
            'CHILD_COUNT': 0,
            'TC_STATUS': 'APPROVED',
            'TC_KIND': 'STANDARD',
            'IMPORTANCE': 'LOW'
          }
        } as unknown as DataRow,
        {
          id: 'TestCase-4',
          children: [],
          projectId: 1,
          parentRowId: 'TestCaseLibrary-1',
          state: DataRowOpenState.leaf,
          data: {
            'NAME': 'Launch Atlantis 2',
            'CHILD_COUNT': 0,
            'TC_STATUS': 'APPROVED',
            'TC_KIND': 'STANDARD',
            'IMPORTANCE': 'LOW'
          }
        } as unknown as DataRow]
    };
  }

  function createDefaultTestCaseModel(): TestCaseModel {
    return {
      id: 3,
      projectId: 1,
      name: 'Launch Atlantis',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: []
      },
      reference: 'STS 135',
      description: '',
      uuid: '',
      type: 20,
      testSteps: createDefaultTestStepModels(),
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: [],
      nbIssues: 0,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30'),
      lastModifiedBy: 'admin',
      kind: 'KEYWORD',
      importanceAuto: false,
      importance: 'VERY_HIGH',
      executions: [],
      datasets: [],
      datasetParamValues: [],
      createdOn: new Date('2020-03-09 10:30'),
      createdBy: 'admin',
      coverages: [],
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [],
      lastExecutionStatus: 'SUCCESS',
      script: '',
      actionWordLibraryActive: false
    };
  }

  function createTestCaseModelWithAutocompleteActive() {
    const basicTestCaseModel = createDefaultTestCaseModel();
    return {
      ...basicTestCaseModel,
      actionWordLibraryActive: true
    };
  }

  function createDefaultTestStepModels(): TestStepModel[] {
    const keywordStep1: KeywordStepModel = {
      id: 2,
      projectId: 1,
      stepOrder: 0,
      kind: 'keyword-step',
      keyword: 'GIVEN',
      actionWordId: 3,
      action: 'go to engine room',
      styledAction: `go to <span style="color: blue;">engine</span> room`,
      datatable: '',
      docstring: '',
      comment: ''
    };
    const keywordStep2: KeywordStepModel = {
      id: 1,
      projectId: 1,
      stepOrder: 1,
      kind: 'keyword-step',
      keyword: 'WHEN',
      actionWordId: 7,
      action: 'check main engines gimbal',
      styledAction: 'check main engines gimbal',
      datatable: createDefaultDatatable(),
      docstring: createDefaultDocstring(),
      comment: createDefaultComment()

    };
    const keywordStep3: KeywordStepModel = {
      id: 5,
      projectId: 1,
      stepOrder: 2,
      kind: 'keyword-step',
      keyword: 'THEN',
      actionWordId: 15,
      action: 'everything is great',
      styledAction: 'everything is great',
      datatable: '',
      docstring: '',
      comment: ''
    };
    return [keywordStep1, keywordStep2, keywordStep3];
  }

  function createDefaultDatatable(): string {
    return `| alpha | beta  | gamma |
| 12°3' | 55°7' | 8°1' |
| 2°2' | 42°0' | 30°3' |`;
  }

  function createDefaultDocstring(): string {
    return`Made in England,
Manchester,
1935`;
  }

  function createDefaultComment(): string {
    return `This step checks:
- if previous operation is a success
- if no side effect appeared`;
  }

  function generateEmptyDeleteTestStepReport() {
    return {
      parameters: [],
      dataSets: [],
      paramValues: []
    } as TestCaseParameterOperationReport;
  }

  function createScriptContent() {
    return `
    # language: en
    Feature: CT_BDD

      Scenario: CT_BDD
        Given je suis sur la page "connexion"
        When j'écris "login" dans le champ <login>
        And j'écris "password" dans le champ <password>
        And je clique sur le bouton "Connexion"
        Then je suis connecté !
    `;
  }

  function initReferentialData(): ReferentialData {
    return new ReferentialDataMockBuilder(_.cloneDeep(createEntityReferentialData))
      .withProjects(
        {name: 'Project1', permissions: ALL_PROJECT_PERMISSIONS},
        {name: 'Project2', permissions: ALL_PROJECT_PERMISSIONS},
        {name: 'Project3', permissions: ALL_PROJECT_PERMISSIONS}
      )
      .withFilterProjectIndices([0])
      .withProjectFilterEnabled()
      .build();
  }

});

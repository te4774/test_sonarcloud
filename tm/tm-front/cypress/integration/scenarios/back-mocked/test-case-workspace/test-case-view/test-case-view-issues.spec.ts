import {TestCaseViewPage} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {ReferentialDataMockBuilder} from '../../../../utils/referential/referential-data-builder';
import {AuthenticationProtocol} from '../../../../model/bugtracker/bug-tracker.model';
import {mockTestCaseModel} from '../../../../data-mock/test-case.data-mock';

const initialTestCaseRow: DataRow = {
  id: 'TestCase-3',
  children: [],
  projectId: 1,
  parentRowId: 'TestCaseLibrary-1',
  data: {
    'NAME': 'TestCase3', 'CHILD_COUNT': 0, 'TC_STATUS': 'APPROVED', 'TC_KIND': 'STANDARD', 'IMPORTANCE': 'LOW'
  }
} as unknown as DataRow;

const referentialData = new ReferentialDataMockBuilder().withProjects({
  name: 'Project_Issues',
  bugTrackerBinding: {id: 1, bugTrackerId: 1, projectId: 3}
}).withBugTrackers({
  name: 'bugtracker',
  authProtocol: AuthenticationProtocol.BASIC_AUTH
}).build();


describe('Test Case View - Issues', () => {
  it('should display connection page if user not connected to bugtracker', () => {
    const testCaseViewPage = navigateToTestCase();
    const issuePage = testCaseViewPage.showIssuesWithoutBindingToBugTracker();
    const connectionDialog = issuePage.openConnectionDialog();
    connectionDialog.fillUserName('admin');
    connectionDialog.fillPassword('admin');
    connectionDialog.connection();
  });

  it('should display table issues', () => {
    const modelResponse = {
      'entityType': 'test-case',
      'bugTrackerStatus': 'AUTHENTICATED',
      'projectName': '["LELprojet","test","LELprojetclassique","LELclassique","Projet_Test_Arnaud"]',
      'projectId': 328,
      'delete': '',
      'oslc': false
    };
    const testCaseViewPage = navigateToTestCase();
    const gridResponse = {
      dataRows: [{
        data: {}
      }]
    } as GridResponse;
    const issuePage = testCaseViewPage.showIssuesIfBindedToBugTracker(modelResponse, gridResponse);
  });

  function navigateToTestCase(): TestCaseViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'TestCaseLibrary-1',
        children: [],
        data: {'NAME': 'Project_Issues'}
      } as unknown as DataRow]
    };
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes, referentialData);
    const libraryChildren = [
      {
        id: 'TestCaseLibrary-1',
        children: ['TestCase-3'],
        data: {'NAME': 'Project_Issues', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      initialTestCaseRow];

    testCaseWorkspacePage.tree.openNode('TestCaseLibrary-1', libraryChildren);
    const model: TestCaseModel = mockTestCaseModel({
      id: 3,
      projectId: 1,
      name: 'TestCase3',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: []
      },
      reference: '',
      description: '',
      uuid: '',
      type: 20,
      testSteps: [],
      status: 'WORK_IN_PROGRESS',
      prerequisite: '',
      parameters: [],
      nbIssues: 2,
      nature: 12,
      milestones: [],
      lastModifiedOn: new Date('2020-03-09 10:30'),
      lastModifiedBy: 'admin',
      kind: 'STANDARD',
      importanceAuto: false,
      importance: 'LOW',
      executions: [],
      datasets: [],
      datasetParamValues: [],
      createdOn: new Date('2020-03-09 10:30'),
      createdBy: 'admin',
      coverages: [],
      automationRequest: null,
      automatable: 'M',
      calledTestCases: [],
      lastExecutionStatus: 'SUCCESS',
      script: ''
    });
    return testCaseWorkspacePage.tree.selectNode<TestCaseViewPage>('TestCase-3', model);
  }

});

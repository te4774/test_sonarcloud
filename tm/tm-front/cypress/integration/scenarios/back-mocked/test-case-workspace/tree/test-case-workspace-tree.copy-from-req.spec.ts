import {
  ALL_PROJECT_PERMISSIONS,
  NO_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {
  RequirementWorkspacePage
} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';

describe('Test Case Workspace Tree : add TC from requirements', () => {

  function referentialData() {
    return new ReferentialDataMockBuilder()
      .withProjects({
          name: 'Project 1',
          permissions: ALL_PROJECT_PERMISSIONS,
        },
        {
          name: 'Project 2',
          permissions: NO_PROJECT_PERMISSIONS,
        })
      .withUser({functionalTester: true})
      .build();
  }

  const initialRequirementNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'RequirementLibrary-1',
      projectId: 1,
      children: [],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '1'}
    } as unknown as DataRow,
      {
        id: 'RequirementLibrary-2',
        projectId: 2,
        children: [],
        data: {'NAME': 'Project2', 'CHILD_COUNT': '1'}
      } as unknown as DataRow]
  };

  const requirementLibraryRefreshAtOpen = [
    {
      id: 'RequirementLibrary-1',
      projectId: 1,
      children: ['Requirement-3'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '1'},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'Requirement-3',
      children: [],
      projectId: 1,
      parentRowId: 'RequirementLibrary-1',
      data: {
        'NAME': 'a nice requirement',
        'HAS_DESCRIPTION': true,
        'REQUIREMENT_STATUS': 'WORK_IN_PROGRESS',
        'CRITICALITY': 'MAJOR'
      }
    } as unknown as DataRow,
  ];

  const initialTestCaseNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'TestCaseLibrary-1',
      projectId: 1,
      children: [],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '1'}
    } as unknown as DataRow,
      {
        id: 'TestCaseLibrary-2',
        projectId: 2,
        children: [],
        data: {'NAME': 'Project2', 'CHILD_COUNT': '1'}
      } as unknown as DataRow]
  };

  const tcLibraryRefreshAtOpen = [
    {
      id: 'TestCaseLibrary-1',
      projectId: 1,
      children: ['TestCase-3'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '1'},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'TestCase-3',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      data: {'NAME': 'a nice test', 'TC_KIND': 'STANDARD', 'TC_STATUS': 'APPROVED', 'IMPORTANCE': 'HIGH'}
    } as unknown as DataRow
  ];

  const refreshedNodes = [
    {
      id: 'TestCaseLibrary-1',
      projectId: 1,
      children: ['TestCase-3', 'TestCase-4'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '2'},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'TestCase-3',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      data: {'NAME': 'a nice test', 'TC_KIND': 'STANDARD', 'TC_STATUS': 'APPROVED', 'IMPORTANCE': 'HIGH'}
    } as unknown as DataRow,
    {
      id: 'TestCase-4',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      data: {'NAME': 'copied test from requirement', 'TC_KIND': 'STANDARD', 'TC_STATUS': 'APPROVED', 'IMPORTANCE': 'HIGH'}
    } as unknown as DataRow
  ];

  it('should not display link in button menu if no permission to create test case on target', () => {

    const firstRequirementNode = initialRequirementNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialRequirementNodes, referentialData());
    const reqTree = requirementWorkspacePage.tree;
    reqTree.openNode(firstRequirementNode.id, requirementLibraryRefreshAtOpen);
    reqTree.selectNode('Requirement-3');
    requirementWorkspacePage.treeMenu.copy();


    const tcWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialTestCaseNodes, referentialData());
    tcWorkspacePage.tree.selectNode('TestCaseLibrary-2');
    tcWorkspacePage.treeMenu.assertCopyFromReqMenuDisabled();
  });

  it('should not display link in button menu if no req nodes copied', () => {

    const tcWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialTestCaseNodes, referentialData());
    const firstTcNode = initialTestCaseNodes.dataRows[0];
    const tcTree = tcWorkspacePage.tree;
    tcTree.openNode(firstTcNode.id, tcLibraryRefreshAtOpen);
    tcTree.selectNode('TestCaseLibrary-1');
    tcWorkspacePage.treeMenu.assertCopyFromReqMenuDisabled();
  });

  it('should create tc from requirement', () => {

    const firstRequirementNode = initialRequirementNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialRequirementNodes, referentialData());
    const reqTree = requirementWorkspacePage.tree;
    reqTree.openNode(firstRequirementNode.id, requirementLibraryRefreshAtOpen);
    reqTree.selectNode('Requirement-3');
    requirementWorkspacePage.treeMenu.copy();

    const tcWorkspacePage = NavBarElement.navigateToTestCaseWorkspace(initialTestCaseNodes, referentialData());
    const tcTree = tcWorkspacePage.tree;
    tcTree.selectNode('TestCaseLibrary-1');

    const addTcDialog = tcWorkspacePage.treeMenu.showCopyFromRequirementDialog();

    addTcDialog.getField('format').selectValue('Classique');

    addTcDialog.confirm('TestCaseLibrary-1', 'STANDARD', refreshedNodes);
    tcTree.assertNodeExist('TestCase-4');
  });

});

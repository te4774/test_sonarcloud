import {DataRow, DataRowOpenState} from '../../../../model/grids/data-row.type';
import {Permissions} from '../../../../model/permissions/permissions.model';
import {AlertDialogElement} from '../../../../page-objects/elements/dialog/alert-dialog.element';
import {
  ExportableScriptType
} from '../../../../page-objects/pages/test-case-workspace/dialogs/export-script-dialog.element';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';

describe('Keyword Test Case Scripts Import', function () {
  [
    { scriptType: 'gherkin', label: 'Gherkin' },
    { scriptType: 'bdd', label: 'BDD' }
  ].forEach(({ scriptType, label}: { scriptType: ExportableScriptType, label: string }) => {

      it(`should show ${label} script export dialog`, () => {
        const testCaseWorkspacePage: TestCaseWorkspacePage = navigateToTestCaseWorkspace();
        testCaseWorkspacePage.navBar.toggle();
        testCaseWorkspacePage.selectMultipleTreeNodesByName(
          ['Standard Test Case', 'Gherkin Test Case', 'Keyword Test Case'],
          'testCase',
          true);
        const exportScriptDialog = testCaseWorkspacePage.treeMenu.openExportScriptDialog(scriptType);
        exportScriptDialog.assertExist();
        exportScriptDialog.checkTitle(`Exporter les scripts ${label}`);
        exportScriptDialog.checkDownloadFileName('export-cas-de-test_');
        exportScriptDialog.checkExportScriptDialogButtons();
        exportScriptDialog.cancelScriptExport();
      });

    it(`should show information dialog because no ${label} test case was selected`, () => {
      const testCaseWorkspacePage: TestCaseWorkspacePage = navigateToTestCaseWorkspace();
      testCaseWorkspacePage.navBar.toggle();
      testCaseWorkspacePage.selectTreeNodeByNameSimpleClick('Standard Test Case', 'testCase', true);
      const exportScriptDialog = testCaseWorkspacePage.treeMenu.openExportScriptDialog(scriptType);
      exportScriptDialog.assertNotExist();
      const alertDialog = new AlertDialogElement(`no-${scriptType}-test-case-selected`);
      alertDialog.assertExist();
      alertDialog.assertHasMessage(`Veuillez sélectionner un objet à exporter (projet, dossier ou au moins un cas de test ${label})`);
      alertDialog.close();
    });
    }
  );




  function navigateToTestCaseWorkspace() {
    return TestCaseWorkspacePage.initTestAtPage(
      createDefaultInitialNodes(),
      createEntityReferentialDataWithExportPermission);
  }

  function createDefaultInitialNodes() {
    return {
      count: 4,
      dataRows: [
        {
          id: 'TestCaseLibrary-1',
          children: ['TestCase-3', 'TestCase-4', 'TestCase-5'],
          data: {
            'NAME': 'Export Project',
            'CHILD_COUNT': 3
          },
          state: DataRowOpenState.open
        } as unknown as DataRow,
        {
          id: 'TestCase-3',
          children: [],
          projectId: 1,
          parentRowId: 'TestCaseLibrary-1',
          state: DataRowOpenState.leaf,
          data: {
            'NAME': 'Keyword Test Case',
            'CHILD_COUNT': 0,
            'TC_STATUS': 'APPROVED',
            'TC_KIND': 'KEYWORD',
            'IMPORTANCE': 'HIGH'
          }
        } as unknown as DataRow,
        {
          id: 'TestCase-4',
          children: [],
          projectId: 1,
          parentRowId: 'TestCaseLibrary-1',
          state: DataRowOpenState.leaf,
          data: {
            'NAME': 'Standard Test Case',
            'CHILD_COUNT': 0,
            'TC_STATUS': 'WORK_IN_PROGRESS',
            'TC_KIND': 'STANDARD',
            'IMPORTANCE': 'MEDIUM'
          }
        } as unknown as DataRow,
        {
          id: 'TestCase-5',
          children: [],
          projectId: 1,
          parentRowId: 'TestCaseLibrary-1',
          state: DataRowOpenState.leaf,
          data: {
            'NAME': 'Gherkin Test Case',
            'CHILD_COUNT': 0,
            'TC_STATUS': 'WORK_IN_PROGRESS',
            'TC_KIND': 'GHERKIN',
            'IMPORTANCE': 'LOW'
          }
        } as unknown as DataRow
      ]};
  }

  function createEntityReferentialDataWithExportPermission() {
    const referentialDataWithExportPermission = {...createEntityReferentialData};
    referentialDataWithExportPermission.projects[0].permissions.TEST_CASE_LIBRARY.push(Permissions.EXPORT);
    return referentialDataWithExportPermission;
  }

});

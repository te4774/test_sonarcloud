import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {
  mockTestCaseFolderModel,
  mockTestCaseLibraryModel,
  mockTestCaseModel
} from '../../../../data-mock/test-case.data-mock';

describe('TestCase Workspace Tree Delete', function () {
  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'TestCaseLibrary-1',
      projectId: 1,
      children: [],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'}
    } as unknown as DataRow,
      {
        id: 'TestCaseLibrary-2',
        projectId: 2,
        children: [],
        data: {'NAME': 'Project2', 'CHILD_COUNT': '1'}
      } as unknown as DataRow]
  };

  const libraryRefreshAtOpen = [
    {
      id: 'TestCaseLibrary-1',
      projectId: 1,
      children: ['TestCaseFolder-1', 'TestCase-3', 'TestCaseFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'TestCaseFolder-1',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      data: {'NAME': 'folder1'}
    } as unknown as DataRow,
    {
      id: 'TestCase-3',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      data: {'NAME': 'a nice test', 'TC_KIND': 'STANDARD', 'TC_STATUS': 'APPROVED', 'IMPORTANCE': 'HIGH'}
    } as unknown as DataRow,
    {
      id: 'TestCaseFolder-2',
      children: [],
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
      data: {'NAME': 'folder2'}
    } as unknown as DataRow
  ];

  it('should activate or deactivate delete button according to user selection', () => {
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    testCaseWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('TestCaseFolder-1', mockTestCaseFolderModel());
    testCaseWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    tree.selectNode('TestCase-3', mockTestCaseModel());
    testCaseWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    tree.selectNode('TestCaseLibrary-1', mockTestCaseLibraryModel());
    testCaseWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
  });

  it('should show server warnings when deleting', () => {
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    testCaseWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('TestCaseFolder-1');
    testCaseWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    const confirmDialog = testCaseWorkspacePage.treeMenu.initDeletion('test-case-tree', [1], ['warning_milestone', 'warning_called']);
    confirmDialog.checkWarningMessages(['warning_milestone', 'warning_called']);
  });

  function performDeletion(useShortcut = false) {
    const libRefreshed = {
      id: 'TestCaseLibrary-1',
      projectId: 1,
      children: ['TestCaseFolder-1', 'TestCase-3', 'TestCaseFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
      state: DataRowOpenState.open
    } as unknown as DataRow;
    const tcf1 = {
      id: 'TestCaseFolder-1',
      children: [],
      data: {'NAME': 'folder1'},
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
    } as unknown as DataRow;
    const tc = {
      id: 'TestCase-3',
      children: [],
      data: {'NAME': 'a nice test', 'TC_KIND': 'STANDARD', 'TC_STATUS': 'APPROVED', 'IMPORTANCE': 'HIGH'},
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
    } as unknown as DataRow;
    const tcf2 = {
      id: 'TestCaseFolder-2',
      children: [],
      data: {'NAME': 'folder2'},
      projectId: 1,
      parentRowId: 'TestCaseLibrary-1',
    } as unknown as DataRow;
    const childNodes = [
      libRefreshed,
      tcf1,
      tc,
      tcf2
    ];
    const firstNode = initialNodes.dataRows[0];
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
    const tree = testCaseWorkspacePage.tree;
    tree.openNode(firstNode.id, childNodes);
    testCaseWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('TestCaseFolder-1');
    testCaseWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    const confirmDialog = testCaseWorkspacePage.treeMenu.initDeletion('test-case-tree', [1], [], useShortcut);
    const libRefreshedAfterDelete = {
      id: 'TestCaseLibrary-1',
      projectId: 1,
      children: ['TestCase-3', 'TestCaseFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '2'},
      state: DataRowOpenState.open
    } as unknown as DataRow;
    const refreshedContent = [
      libRefreshedAfterDelete, tc, tcf2
    ];
    const selectedParentMock = new HttpMockBuilder('/test-case-library-view/1?**')
      .responseBody(mockTestCaseLibraryModel()).build();
    confirmDialog.deleteNodes([1], ['TestCaseLibrary-1'], refreshedContent);
    selectedParentMock.wait();
    testCaseWorkspacePage.tree.assertNodeNotExist('TestCaseFolder-1');
    testCaseWorkspacePage.tree.assertNodeExist('TestCaseFolder-2');
    testCaseWorkspacePage.tree.assertNodeExist('TestCase-3');
  }

  it('should delete node and remove it from tree', () => {
    performDeletion();
  });

  it('should delete node with shortcut and remove it from tree', () => {
    performDeletion(true);
  });
});

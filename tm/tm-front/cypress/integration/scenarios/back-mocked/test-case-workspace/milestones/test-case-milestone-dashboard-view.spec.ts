import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {ReferentialDataMockBuilder} from '../../../../utils/referential/referential-data-builder';
import {
  TestCaseStatisticPanelElement
} from '../../../../page-objects/pages/test-case-workspace/panels/test-case-statistic-panel.element';
import {
  MilestoneInformationPanel
} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-milestone-view.page';

const statistics = {
  selectedIds: [4, 5, 7, 9, 12, 78],
  sizeStatistics: {zeroSteps: 1, between0And10Steps: 2, between11And20Steps: 2, above20Steps: 1},
  importanceStatistics: {low: 1, medium: 2, high: 1, veryHigh: 2},
  statusesStatistics: {workInProgress: 2, underReview: 1, approved: 2, toBeUpdated: 1, obsolete: 0},
  boundRequirementsStatistics: {zeroRequirements: 1, oneRequirement: 4, manyRequirements: 1}
};

const endDate = new Date(2020, 6, 28);

const ORBITAL_RDV = 'orbital rendez-vous';

function buildReferentialData() {
  const referentialDataMock = new ReferentialDataMockBuilder()
    .withProjects(
      {name: 'Apollo', label: 'Apollo'}, {name: 'Gemini', label: 'Gemini'}
    ).withMilestones(
      {
        label: ORBITAL_RDV,
        description: '',
        endDate,
        status: 'IN_PROGRESS',
        boundProjectIndexes: [0, 1],
        range: 'GLOBAL',
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
      },
      {
        label: 'saturn 5 tests',
        description: '',
        endDate: new Date(),
        status: 'IN_PROGRESS',
        boundProjectIndexes: [1],
        range: 'GLOBAL',
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
      })
    .withUser({functionalTester: true})
    .build();
  referentialDataMock.globalConfiguration.milestoneFeatureEnabled = true;
  return referentialDataMock;
}

const MILESTONE_LABEL_CRITERIA = 'Nom du jalon';
describe('Test Case Milestone Dashboard View', function () {
  it('should change milestone in menu above tree', () => {
    const testCaseWorkspacePage: TestCaseWorkspacePage = navigateToTestCaseWorkspace();
    const milestoneSelector = testCaseWorkspacePage.navBar.openMilestoneSelector();
    milestoneSelector.selectMilestone(ORBITAL_RDV);
    milestoneSelector.confirm();
    testCaseWorkspacePage.treeMenu.assertMilestoneButtonIsVisible();
    const tcMilestoneSelector = testCaseWorkspacePage.treeMenu.showMilestoneSelector();
    tcMilestoneSelector.assertMilestoneIsSelected(ORBITAL_RDV);
    tcMilestoneSelector.selectMilestone('saturn 5 tests');
    tcMilestoneSelector.confirm();
    testCaseWorkspacePage.treeMenu.showMilestoneSelector();
    tcMilestoneSelector.assertMilestoneIsSelected('saturn 5 tests');
    tcMilestoneSelector.cancel();
    testCaseWorkspacePage.navBar.assertMilestoneModeIsActive();
    testCaseWorkspacePage.navBar.openMilestoneSelector();
    milestoneSelector.assertMilestoneIsSelected('saturn 5 tests');
  });

  it('should display test case milestone dashboard', () => {
    const testCaseWorkspacePage = navigateToTestCaseWorkspace();
    testCaseWorkspacePage.assertExist();
    const milestoneSelector = testCaseWorkspacePage.navBar.openMilestoneSelector();
    milestoneSelector.selectMilestone(ORBITAL_RDV);
    milestoneSelector.confirm();
    const milestonePage = testCaseWorkspacePage.treeMenu.showMilestoneDashboard({statistics: statistics});
    milestonePage.assertNameEquals(ORBITAL_RDV);
    const informationPanel = milestonePage.clickAnchorLink<MilestoneInformationPanel>('information');
    informationPanel.assertStatusEquals('En cours');
    informationPanel.assertEndDateEquals('28/07/2020');
    const dashboardPanel = milestonePage.clickAnchorLink<TestCaseStatisticPanelElement>('dashboard');
    dashboardPanel.assertTitleExist('Tableau de bord');
    dashboardPanel.coverageChart.assertChartExist();
    dashboardPanel.coverageChart.assertHasTitle('Associations aux exigences');
    dashboardPanel.statusChart.assertChartExist();
    dashboardPanel.statusChart.assertHasTitle('Statut');
    dashboardPanel.importanceChart.assertChartExist();
    dashboardPanel.importanceChart.assertHasTitle('Importance');
    dashboardPanel.sizeChart.assertChartExist();
    dashboardPanel.sizeChart.assertHasTitle('Pas de test');
    dashboardPanel.assertFooterContains('Total des cas de test : 6');
  });

  it('should go to research page when clicking on chart legend', () => {
    const testCaseWorkspacePage = navigateToTestCaseWorkspace();
    testCaseWorkspacePage.assertExist();
    const milestoneSelector = testCaseWorkspacePage.navBar.openMilestoneSelector();
    milestoneSelector.selectMilestone(ORBITAL_RDV);
    milestoneSelector.confirm();
    const milestonePage = testCaseWorkspacePage.treeMenu.showMilestoneDashboard({statistics: statistics});
    milestonePage.assertNameEquals(ORBITAL_RDV);
    const dashboardPanel = milestonePage.clickAnchorLink<TestCaseStatisticPanelElement>('dashboard');
    dashboardPanel.coverageChart.assertChartExist();
    const testCaseResearchPage = dashboardPanel.coverageChart.goToResearchPage(2, {...buildReferentialData()});
    testCaseResearchPage.grid.filterPanel.assertCriteriaIsActive(MILESTONE_LABEL_CRITERIA);
    // for now research do a toLowerCase on values
    testCaseResearchPage.grid.filterPanel.assertCriteriaHasValue(MILESTONE_LABEL_CRITERIA, ORBITAL_RDV);
    testCaseResearchPage.grid.filterPanel.assertPerimeterHasValue('Apollo, Gemini');
  });

  function navigateToTestCaseWorkspace(): TestCaseWorkspacePage {
    const referentialDataMock = buildReferentialData();

    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'TestCaseLibrary-1',
        children: [],
        data: {'NAME': 'Project 1', 'MILESTONES': [1, 2]}
      } as unknown as DataRow]
    };
    return TestCaseWorkspacePage.initTestAtPage(initialNodes, referentialDataMock);
  }
});

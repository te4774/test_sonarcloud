import {defaultReferentialData} from '../../../../utils/referential/default-referential-data.const';
import {
  TestCaseModifyDuringExecProloguePage
} from '../../../../page-objects/pages/test-case-workspace/test-case/test-case-modify-during-exec-prologue.page';
import {mockTestCaseModel} from '../../../../data-mock/test-case.data-mock';

describe('Test Case Modify during exec', function () {
  it('should display modify during prologue', () => {
    const modifyDuringExecPage = TestCaseModifyDuringExecProloguePage.initTestAtPage(mockTestCaseModel(), 1, defaultReferentialData);
    modifyDuringExecPage.assertExist();
    const alertDialogElement = modifyDuringExecPage.goBackToExec();
    alertDialogElement.assertExist();
    alertDialogElement.assertHasMessage('L\'exécution ne peut pas être lancée car il n\'y a aucun pas à exécuter');
  });
});

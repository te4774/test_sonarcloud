import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseWorkspacePage} from '../../../../page-objects/pages/test-case-workspace/test-case-workspace.page';
import {defaultReferentialData} from '../../../../utils/referential/default-referential-data.const';
import {
  TestCaseFolderViewPage
} from '../../../../page-objects/pages/test-case-workspace/test-case-folder/test-case-folder-view.page';
import {TestCaseFolderModel} from '../../../../model/test-case/test-case-folder/test-case-folder.model';
import {FolderInformationPanelElement} from '../../../../page-objects/elements/panels/folder-information-panel.element';
import {
  TestCaseStatisticPanelElement
} from '../../../../page-objects/pages/test-case-workspace/panels/test-case-statistic-panel.element';
import {TestCaseStatistics} from '../../../../model/test-case/test-case-statistics.model';
import {getFavoriteDashboard, getStatistics} from '../test-case-workspace-mock-data';
import {CustomDashboardModel} from '../../../../model/custom-report/custom-dashboard.model';

const description = `<p>a nice description</p>
<table><tbody><tr><th>Col1</th><th>Col2</th></tr><tr><td>Col1L1</td><td>Col2L1</td></tr></tbody></table>`;

describe('Test Case Folder View', function () {
  it('should display test case folder page and information', () => {
    const tcFolderPage = navigateToTestCaseFolder();
    tcFolderPage.assertExist();
    const informationPanel = tcFolderPage.clickAnchorLink<FolderInformationPanelElement>('information');
    informationPanel.descriptionRichField.checkHtmlContent(description);
  });

  it('should display test case folder dashboard', () => {
    const tcFolderPage = navigateToTestCaseFolder();
    tcFolderPage.assertExist();
    const dashboardPanel = tcFolderPage.clickAnchorLink<TestCaseStatisticPanelElement>('dashboard');
    dashboardPanel.assertTitleExist('Tableau de bord');
    dashboardPanel.coverageChart.assertChartExist();
    dashboardPanel.coverageChart.assertHasTitle('Associations aux exigences');
    dashboardPanel.statusChart.assertChartExist();
    dashboardPanel.statusChart.assertHasTitle('Statut');
    dashboardPanel.importanceChart.assertChartExist();
    dashboardPanel.importanceChart.assertHasTitle('Importance');
    dashboardPanel.sizeChart.assertChartExist();
    dashboardPanel.sizeChart.assertHasTitle('Pas de test');
    dashboardPanel.assertFooterContains('Total des cas de test : 6');
  });

  it('should refresh test case folder dashboard', () => {
    const tcFolderPage = navigateToTestCaseFolder();
    tcFolderPage.assertExist();
    const dashboardPanel = tcFolderPage.clickAnchorLink<TestCaseStatisticPanelElement>('dashboard');
    dashboardPanel.assertFooterContains('Total des cas de test : 6');
    const updatedStatistics: TestCaseStatistics = {...getStatistics(), selectedIds: [4, 5, 7, 9, 12, 13, 48, 78]};
    dashboardPanel.refreshStatistics(updatedStatistics);
    dashboardPanel.assertFooterContains('Total des cas de test : 8');
  });

  it('should display test case folder favorite dashboard', () => {
    const tcFolderPage = navigateToTestCaseFolderWithFavoriteDashboard();
    tcFolderPage.assertExist();
    const dashboardPanel = tcFolderPage.clickAnchorLink<TestCaseStatisticPanelElement>('dashboard');
    dashboardPanel.assertCustomDashboardExist();
    dashboardPanel.assertTitleExist('Favorite Dashboard');
  });

  function getTestCaseWorkspaceMocked(): TestCaseWorkspacePage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'TestCaseLibrary-1',
        children: [],
        data: {'NAME': 'Project1'}
      } as unknown as DataRow]
    };
    const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes, defaultReferentialData);
    const libraryChildren = [
      {
        id: 'TestCaseLibrary-1',
        children: ['TestCaseFolder-3'],
        state: DataRowOpenState.open,
        data: {'NAME': 'Project1', CHILD_COUNT: 1}
      } as unknown as DataRow,
      {
        id: 'TestCaseFolder-3',
        children: [],
        projectId: 1,
        data: {'NAME': 'TestCaseFolder-3', 'CHILD_COUNT': 6},
        parentRowId: 'TestCaseLibrary-1',
      } as unknown as DataRow];

    testCaseWorkspacePage.tree.openNode('TestCaseLibrary-1', libraryChildren);
    return testCaseWorkspacePage;
  }

  function navigateToTestCaseFolder(): TestCaseFolderViewPage {
    const testCaseWorkspacePage = getTestCaseWorkspaceMocked();
    const model: TestCaseFolderModel = createFolderModel(getStatistics(), null);
    return testCaseWorkspacePage.tree.selectNode<TestCaseFolderViewPage>('TestCaseFolder-3', model);
  }

  function navigateToTestCaseFolderWithFavoriteDashboard(): TestCaseFolderViewPage {
    const testCaseWorkspacePage = getTestCaseWorkspaceMocked();
    const model: TestCaseFolderModel = createFolderModel(null, getFavoriteDashboard());
    return testCaseWorkspacePage.tree.selectNode<TestCaseFolderViewPage>('TestCaseFolder-3', model);
  }

  function createFolderModel(statistics: TestCaseStatistics, dashboard: CustomDashboardModel): TestCaseFolderModel {
    return {
      id: 3,
      projectId: 1,
      name: 'TestCaseFolder3',
      customFieldValues: [
        {
          id: 0,
          value: 'a little value',
          cufId: 0,
          fieldType: 'CF'
        },
        {
          id: 1,
          value: '2',
          cufId: 1,
          fieldType: 'NUM'
        },
        {
          id: 2,
          value: 'o1|o2',
          cufId: 2,
          fieldType: 'TAG'
        },
        {
          id: 3,
          value: 'a little value',
          cufId: 3,
          fieldType: 'CF'
        },
        {
          id: 4,
          value: '<p>a little value</p>',
          cufId: 4,
          fieldType: 'RTF'
        },
        {
          id: 5,
          value: '2020-02-14',
          cufId: 5,
          fieldType: 'CF'
        },
        {
          id: 6,
          value: 'true',
          cufId: 6,
          fieldType: 'CF'
        },
        {
          id: 7,
          value: 'Option C',
          cufId: 7,
          fieldType: 'CF'
        }
      ],
      attachmentList: {
        id: 1,
        attachments: []
      },
      description,
      statistics: statistics,
      dashboard: dashboard,
      canShowFavoriteDashboard: Boolean(dashboard),
      favoriteDashboardId: 1,
      shouldShowFavoriteDashboard: Boolean(dashboard),
    };
  }
});

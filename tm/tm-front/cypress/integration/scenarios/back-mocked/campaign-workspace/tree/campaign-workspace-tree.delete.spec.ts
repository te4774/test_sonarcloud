import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {CampaignWorkspacePage} from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

describe('Campaign Workspace Tree Delete', function () {

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'CampaignLibrary-1',
      projectId: 1,
      children: [],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'}
    } as unknown as DataRow,
      {
        id: 'CampaignLibrary-2',
        projectId: 2,
        children: [],
        data: {'NAME': 'Project2', 'CHILD_COUNT': '1'}
      } as unknown as DataRow]
  };

  const libraryRefreshAtOpen = [
    {
      id: 'CampaignLibrary-1',
      projectId: 1,
      children: ['CampaignFolder-1', 'Campaign-3', 'CampaignFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'CampaignFolder-1',
      children: [],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      data: {'NAME': 'folder1'}
    } as unknown as DataRow,
    {
      id: 'Campaign-3',
      children: [],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      data: {'NAME': 'a nice test'}
    } as unknown as DataRow,
    {
      id: 'CampaignFolder-2',
      children: [],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      data: {'NAME': 'folder2'}
    } as unknown as DataRow
  ];

  it('should activate or deactivate delete button according to user selection', () => {
    const firstNode = initialNodes.dataRows[0];
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
    const tree = campaignWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    const statFolderBundleMock = campaignWorkspacePage.mockCustomReportFolderStatisticsRequest();
    tree.selectNode('CampaignFolder-1');
    statFolderBundleMock.wait();
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    tree.selectNode('Campaign-3');
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    tree.selectNode('CampaignLibrary-1');
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
  });

  it('should show server warnings when deleting', () => {
    const firstNode = initialNodes.dataRows[0];
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
    const tree = campaignWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    const statFolderBundleMock = campaignWorkspacePage.mockCustomReportFolderStatisticsRequest();
    tree.selectNode('CampaignFolder-1');
    statFolderBundleMock.wait();
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    const confirmDialog = campaignWorkspacePage.treeMenu.initCampaignWorkspaceDeletion(['CampaignFolder-1'],
      ['warning_milestone', 'warning_called']);
    confirmDialog.checkWarningMessages(['warning_milestone', 'warning_called']);
  });

  it('should delete node and remove it from tree', () => {
    const libRefreshed = {
      id: 'CampaignLibrary-1',
      projectId: 1,
      children: ['CampaignFolder-1', 'Campaign-3', 'CampaignFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
      state: DataRowOpenState.open
    } as unknown as DataRow;
    const cf1 = {
      id: 'CampaignFolder-1',
      children: [],
      data: {'NAME': 'folder1'},
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
    } as unknown as DataRow;
    const c = {
      id: 'Campaign-3',
      children: [],
      data: {'NAME': 'a nice test'},
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
    } as unknown as DataRow;
    const cf2 = {
      id: 'CampaignFolder-2',
      children: [],
      data: {'NAME': 'folder2'},
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
    } as unknown as DataRow;
    const childNodes = [
      libRefreshed,
      cf1,
      c,
      cf2
    ];
    const firstNode = initialNodes.dataRows[0];
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes);
    const tree = campaignWorkspacePage.tree;
    tree.openNode(firstNode.id, childNodes);
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    const statFolderBundleMock = campaignWorkspacePage.mockCustomReportFolderStatisticsRequest();
    tree.selectNode('CampaignFolder-1');
    statFolderBundleMock.wait();
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    const confirmDialog = campaignWorkspacePage.treeMenu.initCampaignWorkspaceDeletion(['CampaignFolder-1']);
    const libRefreshedAfterDelete = {
      id: 'CampaignLibrary-1',
      projectId: 1,
      children: ['Campaign-3', 'CampaignFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '2'},
      state: DataRowOpenState.open
    } as unknown as DataRow;
    const refreshedContent = [
      libRefreshedAfterDelete, c, cf2
    ];
    const selectedParentMock = new HttpMockBuilder('/campaign-library-view/1?**').responseBody({}).build();
    confirmDialog.deleteNodes(['CampaignFolder-1'], ['CampaignLibrary-1'], refreshedContent);
    selectedParentMock.wait();
    campaignWorkspacePage.tree.assertNodeNotExist('CampaignFolder-1');
    campaignWorkspacePage.tree.assertNodeExist('CampaignFolder-2');
    campaignWorkspacePage.tree.assertNodeExist('Campaign-3');
  });

});

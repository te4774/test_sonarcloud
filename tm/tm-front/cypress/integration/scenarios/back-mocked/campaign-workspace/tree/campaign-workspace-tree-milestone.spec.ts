import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {CampaignWorkspacePage} from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';


describe('Campaign workspace tree in milestone mode', () => {
  const referentialDataMock = new ReferentialDataMockBuilder()
    .withProjects(
      {name: 'Project 1', label: 'Etiquette', permissions: ALL_PROJECT_PERMISSIONS},
      {name: 'Project 2', label: 'Etiquette 2', permissions: ALL_PROJECT_PERMISSIONS}
    ).withMilestones(
      {
        label: 'milestone1',
        description: '',
        endDate: new Date(),
        status: 'IN_PROGRESS',
        boundProjectIndexes: [0],
        range: 'GLOBAL',
      },
      {
        label: 'milestone2',
        description: '',
        endDate: new Date(),
        status: 'FINISHED',
        boundProjectIndexes: [0],
        range: 'GLOBAL',
      },
      {
        label: 'milestone3',
        description: '',
        endDate: new Date(),
        status: 'LOCKED',
        boundProjectIndexes: [1],
        range: 'GLOBAL',
      })
    .withUser({functionalTester: true})
    .build();
  referentialDataMock.globalConfiguration.milestoneFeatureEnabled = true;

  const campaignLibrary1 = {
    id: 'CampaignLibrary-1',
    children: ['CampaignFolder-1', 'Campaign-3', 'Campaign-4', 'CampaignFolder-2'],
    projectId: 1,
    data: {'NAME': 'Project 1', 'CHILD_COUNT': '3', 'MILESTONES': [1, 2]},
    state: DataRowOpenState.open
  } as unknown as DataRow;
  const campaignLibrary2 = {
    id: 'CampaignLibrary-2',
    children: ['Campaign-5'],
    projectId: 2,
    data: {'NAME': 'Project 2', 'CHILD_COUNT': '0', 'MILESTONES': [3]},
    state: DataRowOpenState.open
  } as unknown as DataRow;

  const campaignFolder1 = {
    id: 'CampaignFolder-1',
    children: [],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: {'NAME': 'folder1', 'MILESTONES': [1, 2]}
  } as unknown as DataRow;
  const campaignFolder2 = {
    id: 'CampaignFolder-2',
    children: [],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: {'NAME': 'folder2', 'MILESTONES': [1, 2]}
  } as unknown as DataRow;

  const campaign3 = {
    id: 'Campaign-3',
    children: ['Iteration-1'],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: {
      'NAME': 'campaign number 3',
      'MILESTONES': [1],
    },
    state: DataRowOpenState.open
  } as unknown as DataRow;
  const campaign4 = {
    id: 'Campaign-4',
    children: [],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: {
      'NAME': 'campaign number 4',
      'MILESTONES': [2]
    }
  } as unknown as DataRow;
  const campaign5 = {
    id: 'Campaign-5',
    children: ['Iteration-2'],
    projectId: 1,
    parentRowId: 'CampaignLibrary-2',
    data: {
      'NAME': 'campaign number 5',
      'MILESTONES': [3],
    },
    state: DataRowOpenState.open
  } as unknown as DataRow;

  const iteration1 = {
    id: 'Iteration-1',
    children: ['TestSuite-1'],
    projectId: 1,
    parentRowId: 'Campaign-3',
    data: {
      'NAME': 'iteration 1',
      'MILESTONES': [1]
    },
    state: DataRowOpenState.open
  } as unknown as DataRow;
  const iteration2 = {
    id: 'Iteration-2',
    children: ['TestSuite-2'],
    projectId: 1,
    parentRowId: 'Campaign-5',
    data: {
      'NAME': 'iteration 2',
      'MILESTONES': [3]
    },
    state: DataRowOpenState.open
  } as unknown as DataRow;

  const allNodes: GridResponse = {
    count: 9,
    dataRows: [campaignLibrary1, campaignLibrary2, campaignFolder1, campaignFolder2,
      campaign3, campaign4, campaign5, iteration1, iteration2],
  };

  it('should display a tree in milestone mode', () => {
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(allNodes, referentialDataMock);
    const navbar = campaignWorkspacePage.navBar;
    const tree = campaignWorkspacePage.tree;

    // enable Milestone mode on milestone1
    const milestonePicker = navbar.openMilestoneSelector();
    milestonePicker.selectMilestone('milestone1');
    milestonePicker.confirm();
    tree.assertNodeExist('CampaignLibrary-1');
    tree.assertNodeExist('CampaignFolder-1');
    tree.assertNodeExist('CampaignFolder-2');
    tree.assertNodeExist('Campaign-3');
    tree.assertNodeNotExist('Campaign-4');
    tree.assertNodeNotExist('CampaignLibrary-2');

    // disable Milestone Mode
    navbar.disableMilestoneMode();
    tree.assertNodeExist('CampaignLibrary-1');
    tree.assertNodeExist('CampaignFolder-1');
    tree.assertNodeExist('CampaignFolder-2');
    tree.assertNodeExist('Campaign-3');
    tree.assertNodeExist('Campaign-4');
    tree.assertNodeExist('CampaignLibrary-2');

    // enable Milestone mode on milestone2
    navbar.openMilestoneSelector();
    milestonePicker.selectMilestone('milestone2');
    milestonePicker.confirm();
    tree.assertNodeExist('CampaignLibrary-1');
    tree.assertNodeExist('CampaignFolder-1');
    tree.assertNodeExist('CampaignFolder-2');
    tree.assertNodeNotExist('Campaign-3');
    tree.assertNodeExist('Campaign-4');
    tree.assertNodeNotExist('CampaignLibrary-2');
  });

  it('should create or delete entities in milestone mode', () => {
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(allNodes, referentialDataMock);
    const navbar = campaignWorkspacePage.navBar;
    const tree = campaignWorkspacePage.tree;
    navbar.toggle();

    // milestone mode, no locked milestone
    const milestonePicker = navbar.openMilestoneSelector();
    milestonePicker.selectMilestone(1);
    milestonePicker.confirm();

    tree.selectNode('CampaignLibrary-1');
    campaignWorkspacePage.treeMenu.performAllChecksForCampaignLibrary();
    tree.selectNode('Campaign-3');
    campaignWorkspacePage.treeMenu.performAllChecksForCampaign();
    tree.selectNode('Iteration-1');
    campaignWorkspacePage.treeMenu.performAllChecksForIteration();

    // milestone mode, locked milestone
    const milestonePicker2 = navbar.openMilestoneSelector();
    milestonePicker2.selectMilestone(3);
    milestonePicker2.confirm();

    tree.selectNode('CampaignLibrary-2');
    campaignWorkspacePage.treeMenu.assertAllMenuItemsDisabled();
    campaignWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    campaignWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    tree.selectNode('Campaign-5');
    campaignWorkspacePage.treeMenu.assertAllMenuItemsDisabled();
    campaignWorkspacePage.treeMenu.assertCopyButtonIsActive();
    campaignWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    tree.selectNode('Iteration-2');
    campaignWorkspacePage.treeMenu.assertAllMenuItemsDisabled();
    campaignWorkspacePage.treeMenu.assertCopyButtonIsActive();
    campaignWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();

  });
});

import {mockDataRow, mockGridResponse} from '../../../../data-mock/grid.data-mock';
import {initialTestCaseLibraries, projectOneChildren} from '../../../../data-mock/test-case-simple-tree';
import {TestSuiteModel} from '../../../../model/campaign/test-suite.model';
import {EntityReference, EntityType} from '../../../../model/entity.model';
import {DataRow, DataRowOpenState, GridResponse, Identifier} from '../../../../model/grids/data-row.type';
import {
  AutomatedSuitePreview,
  TestAutomationProjectPreview
} from '../../../../model/test-automation/automated-suite-preview.model';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {CampaignWorkspacePage} from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  TestSuiteTestPlanPage
} from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-test-plan.page';
import {TestSuiteViewPage} from '../../../../page-objects/pages/campaign-workspace/test-suite/test-suite-view.page';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';

describe('Test Suite Test Plan View', () => {
  it('should display a simple test plan', () => {
    const testSuiteViewPage = navigateToTestSuite();
    const testPlan = mockTestPlan([
      mockITPIDataRow('1'),
      mockITPIDataRow('2'),
    ]);

    const testPlanPage = testSuiteViewPage.clickAnchorLink<TestSuiteTestPlanPage>('plan-exec', testPlan);

    testPlanPage.testPlan.assertRowExist('1');
    testPlanPage.testPlan.assertRowExist('2');

    testPlanPage.testPlan.getRow('1').cell('testCaseName').linkRenderer().assertContainText('Test-Case 1');
    testPlanPage.testPlan.getRow('2').cell('testCaseName').linkRenderer().assertContainText('Test-Case 2');
  });

  it('should display and filter a test plan', () => {
    const testSuiteViewPage = navigateToTestSuite();
    new NavBarElement().toggle();
    testSuiteViewPage.toggleTree();
    const testPlan = mockTestPlan([
      mockITPIDataRow('1'),
      mockITPIDataRow('2'),
    ]);

    const testPlanPage = testSuiteViewPage.clickAnchorLink<TestSuiteTestPlanPage>('plan-exec', testPlan);

    testPlanPage.testPlan.assertRowExist('1');
    testPlanPage.testPlan.assertRowExist('2');

    testPlanPage.testPlan.declareRefreshData({...testPlan, dataRows: [testPlan.dataRows[0]]});
    testPlanPage.testPlan.filterByTextColumn('testCaseName', 'value', {...testPlan, dataRows: [testPlan.dataRows[0]]});

    testPlanPage.testPlan.assertRowExist('1');
    testPlanPage.testPlan.assertRowNotExist('2');
  });

  it('should apply a fastpass on item', () => {
    cy.viewport(1200, 700);
    const testSuiteViewPage = navigateToTestSuite();
    new NavBarElement().toggle();
    testSuiteViewPage.toggleTree();

    const testPlan = {
      'count': 1,
      'idAttribute': null,
      'dataRows': [
        {
          id: '1',
          data: {
            itemTestPlanId: 1,
            testCaseName: 'Test-Case 1',
            testCaseId: 1,
            projectId: 1,
            iterationId: 1,
            lastExecutedOn: null,
            executionStatus: 'RUNNING',
            user: 'gilbert',
            hasExecutions: false,
            datasetName: 'JDD'
          },
          projectId: 1
        } as any
      ]
    };
    const testPlanPage = testSuiteViewPage.clickAnchorLink<TestSuiteTestPlanPage>('plan-exec', testPlan);
    testPlanPage.foldGrid();

    cy.get(`[data-test-cell-id=execution-status-cell]`).should('have.css', 'background-color', 'rgb(0, 120, 174)');
    testPlanPage.applyFastpassOnItpi('SUCCESS');
    cy.get(`[data-test-cell-id=execution-status-cell]`).should('have.css', 'background-color', 'rgb(0, 111, 87)');
  });

  it('should mass edit ITPIs', () => {
    const testSuiteViewPage = navigateToTestSuite();
    new NavBarElement().toggle();
    testSuiteViewPage.toggleTree();

    const dataRows = [
      mockITPIDataRow(1),
      mockITPIDataRow(2),
      mockITPIDataRow(3),
      mockITPIDataRow(4),
    ];
    const testPlan = mockGridResponse('itemTestPlanId', dataRows);

    const testPlanPage = testSuiteViewPage.clickAnchorLink<TestSuiteTestPlanPage>('plan-exec', testPlan);
    testPlanPage.testPlan.selectRowsWithStickyIndexColumn([1, 2]);

    const massEditDialog = testPlanPage.openMassEditDialog();
    massEditDialog.assertExist();

    massEditDialog.toggleAssigneeEdition();
    massEditDialog.selectAssignee('Joe Ni (jawny)');
    massEditDialog.toggleStatusEdition();
    massEditDialog.selectStatus('En cours');

    massEditDialog.confirm(dataRows);
  });

  it('should update dataset', () => {
    cy.viewport(1200, 700);
    const testSuiteViewPage = navigateToTestSuite();
    const testPlan = {
      'count': 1,
      'idAttribute': null,
      'dataRows': [
        {
          id: '1',
          data: {
            itemTestPlanId: 1,
            testCaseName: 'Test-Case 1',
            projectId: 1,
            iterationId: 1,
            datasetName: 'JDD',
            lastExecutedOn: null,
            executionStatus: 'RUNNING',
            user: 'gilbert',
            hasExecutions: false,
            testCaseId: 1,
            availableDatasets: [{id: 1, name: 'JDD'}, {id: 2, name: 'Dataset'}],
          },
          projectId: 1
        } as any
      ]
    };
    const testPlanPage = testSuiteViewPage.clickAnchorLink<TestSuiteTestPlanPage>('plan-exec', testPlan);

    cy.get(`[data-test-cell-id=dataset-cell]`).should('contain.text', 'JDD');
    testPlanPage.changeDataset('item-2');
    cy.get(`[data-test-cell-id=dataset-cell]`).should('contain.text', 'Dataset');
  });

  it('should update assigned user', () => {
    cy.viewport(1200, 700);
    const testSuiteViewPage = navigateToTestSuite();
    const testPlan = {
      'count': 1,
      'idAttribute': null,
      'dataRows': [
        {
          id: '1',
          data: {
            itemTestPlanId: 1,
            testCaseName: 'Test-Case 1',
            projectId: 1,
            iterationId: 1,
            lastExecutedOn: null,
            executionStatus: 'RUNNING',
            user: 'gilbert',
            hasExecutions: false,
            datasetName: 'JDD'
          },
          projectId: 1
        } as any
      ]
    };
    const testPlanPage = testSuiteViewPage.clickAnchorLink<TestSuiteTestPlanPage>('plan-exec', testPlan);

    testPlanPage.testPlan.scrollPosition('right', 'mainViewport');
    const row = testPlanPage.testPlan.getRow(1);
    const assignedUserCell = row.cell('user').selectRenderer();
    assignedUserCell.changeValue('item-2', 'test-plan-item/*/assign-user');
    assignedUserCell.assertContainText('Joe Ni (jawny)');
  });

  it('should reorder items with drag and drop', () => {
    const testSuiteViewPage = navigateToTestSuite();
    const testPlan = mockGridResponse('itemTestPlanId', [
      mockITPIDataRow(1),
      mockITPIDataRow(2),
      mockITPIDataRow(3),
      mockITPIDataRow(4),
    ]);
    const testPlanPage = testSuiteViewPage.clickAnchorLink<TestSuiteTestPlanPage>('plan-exec', testPlan);

    testPlanPage.testPlan.assertRowCount(4);
    testPlanPage.moveItemWithServerResponse('1', '3', {});
  });

  it('should forbid items reordering if grid is filtered', () => {
    const testSuiteViewPage = navigateToTestSuite();
    const testPlan = mockGridResponse('itemTestPlanId', [
      mockITPIDataRow(1),
      mockITPIDataRow(2),
      mockITPIDataRow(3),
      mockITPIDataRow(4),
    ]);
    const testPlanPage = testSuiteViewPage.clickAnchorLink<TestSuiteTestPlanPage>('plan-exec', testPlan);

    testPlanPage.testPlan.filterByTextColumn('projectName', 'P', testPlan);

    testPlanPage.assertCannotMoveItem('1');
  });

  it('should detach or remove test cases from test suite', () => {
    cy.viewport(1200, 700);
    const testSuiteViewPage = navigateToTestSuite();
    const testPlan = mockGridResponse('itemTestPlanId', [
      mockITPIDataRow(1, {lastExecutedOn: new Date('2020-03-09 10:30')}),
      mockITPIDataRow(2, {lastExecutedOn: new Date('2020-03-09 10:30')}),
      mockITPIDataRow(3, {lastExecutedOn: null}),
      mockITPIDataRow(4, {lastExecutedOn: null}),
    ]);
    const testPlanPage = testSuiteViewPage.clickAnchorLink<TestSuiteTestPlanPage>('plan-exec', testPlan);
    testPlanPage.testPlan.scrollPosition('right', 'mainViewport');

    const testPlanAfterRefresh1 = mockGridResponse('itemTestPlanId', [
      mockITPIDataRow(2, {lastExecutedOn: new Date('2020-03-09 10:30')}),
      mockITPIDataRow(3, {lastExecutedOn: null}),
      mockITPIDataRow(4, {lastExecutedOn: null}),
    ]);
    const detachHasExecDialog = testPlanPage.showDeleteConfirmDialog(5, 1, testPlanAfterRefresh1);
    detachHasExecDialog.assertExist();
    detachHasExecDialog.assertItemHasExecutions();
    detachHasExecDialog.detachFromTestSuite();
    detachHasExecDialog.assertNotExist();

    const testPlanAfterRefresh2 = mockGridResponse('itemTestPlanId', [
      mockITPIDataRow(3, {lastExecutedOn: null}),
      mockITPIDataRow(4, {lastExecutedOn: null}),
    ]);
    const removeHasExecDialog = testPlanPage.showDeleteConfirmDialog(5, 2, testPlanAfterRefresh2);
    removeHasExecDialog.assertExist();
    removeHasExecDialog.assertItemHasExecutions();
    removeHasExecDialog.removeFromTestSuiteAndIteration();
    removeHasExecDialog.assertNotExist();

    const testPlanAfterRefresh3 = mockGridResponse('itemTestPlanId', [
      mockITPIDataRow(4, {lastExecutedOn: null}),
    ]);
    const detachNoExecDialog = testPlanPage.showDeleteConfirmDialog(5, 3, testPlanAfterRefresh3);
    detachNoExecDialog.assertExist();
    detachNoExecDialog.assertItemHasNoExecutions();
    detachNoExecDialog.detachFromTestSuite();
    detachNoExecDialog.assertNotExist();

    const testPlanAfterRefresh4 = mockGridResponse('itemTestPlanId', []);
    const removeNoExecDialog = testPlanPage.showDeleteConfirmDialog(5, 4, testPlanAfterRefresh4);
    removeNoExecDialog.assertExist();
    removeNoExecDialog.assertItemHasNoExecutions();
    removeNoExecDialog.removeFromTestSuiteAndIteration();
    removeNoExecDialog.assertNotExist();

  });

  it('should mass detach or remove test cases from test suite', () => {
    cy.viewport(1200, 700);
    const testSuiteViewPage = navigateToTestSuite();

    const testPlan = mockGridResponse('itemTestPlanId', [
      mockITPIDataRow(1, {lastExecutedOn: new Date('2020-03-09 10:30')}),
      mockITPIDataRow(2, {lastExecutedOn: new Date('2020-03-09 10:30')}),
      mockITPIDataRow(3, {lastExecutedOn: null}),
      mockITPIDataRow(4, {lastExecutedOn: null}),
      mockITPIDataRow(5, {lastExecutedOn: null})
    ]);
    const testPlanPage = testSuiteViewPage.clickAnchorLink<TestSuiteTestPlanPage>('plan-exec', testPlan);

    testPlanPage.testPlan.selectRowsWithStickyIndexColumn([2, 3]);
    const testPlanAfterRefresh1 = mockGridResponse('itemTestPlanId', [
      mockITPIDataRow(1, {lastExecutedOn: new Date('2020-03-09 10:30')}),
      mockITPIDataRow(4, {lastExecutedOn: null}),
      mockITPIDataRow(5, {lastExecutedOn: null})
    ]);
    const removeHasExecDialog = testPlanPage.showMassDeleteConfirmDialog(5, [2, 3], testPlanAfterRefresh1);

    removeHasExecDialog.assertExist();
    removeHasExecDialog.assertItemHasExecutions();
    removeHasExecDialog.removeFromTestSuiteAndIteration();
    removeHasExecDialog.assertNotExist();

    testPlanPage.testPlan.selectRowsWithStickyIndexColumn([4, 5]);
    const testPlanAfterRefresh2 = mockGridResponse('itemTestPlanId', [
      mockITPIDataRow(1, {lastExecutedOn: new Date('2020-03-09 10:30')})]);
    const removeNoExecDialog = testPlanPage.showMassDeleteConfirmDialog(5, [4, 5], testPlanAfterRefresh2);

    removeNoExecDialog.assertExist();
    removeNoExecDialog.assertItemHasNoExecutions();
    removeNoExecDialog.removeFromTestSuiteAndIteration();
    removeNoExecDialog.assertNotExist();
  });

  it('should add a test case to test suite', () => {
    const testSuiteViewPage = navigateToTestSuite();
    new NavBarElement().toggle();
    testSuiteViewPage.toggleTree();

    const testPlan = mockTestPlan([]);

    const testPlanPage = testSuiteViewPage.clickAnchorLink<TestSuiteTestPlanPage>('plan-exec', testPlan);
    new NavBarElement().toggle();
    testSuiteViewPage.toggleTree();

    const testCaseTreePicker = testPlanPage.openTestCaseDrawer(initialTestCaseLibraries);
    testCaseTreePicker.openNode('TestCaseLibrary-1', projectOneChildren);
    testCaseTreePicker.beginDragAndDrop('TestCase-3');

    const refreshedTestPlan = mockGridResponse('itemTestPlanId', [mockITPIDataRow(1)]);

    testPlanPage.enterIntoTestPlan();
    testPlanPage.assertColoredBorderIsVisible();
    testPlanPage.dropIntoTestPlan(5, [17], refreshedTestPlan);
    testPlanPage.verifyTestPlanItem({id: 1, name: 'Test-Case 1', showsAsLink: true});
    testPlanPage.assertColoredBorderIsNotVisible();
    testPlanPage.closeTestCaseDrawer();
  });

  it('should launch automated tests', () => {
    const testSuiteViewPage = navigateToTestSuite();
    new NavBarElement().toggle();
    testSuiteViewPage.toggleTree();
    const testPlan = mockTestPlan([
      mockITPIDataRow('1', {executionMode: 'MANUAL'}),
      mockITPIDataRow('2', {executionMode: 'AUTOMATED'}),
    ]);
    const testPlanPage = testSuiteViewPage.clickAnchorLink<TestSuiteTestPlanPage>('plan-exec', testPlan);
    testPlanPage.assertLaunchAutomatedMenuExist();
    const executionDialog = testPlanPage.openAutoTestExecDialogViaLaunchAllButton(mockAutomatedSuitePreview());
    executionDialog.checkAgentPhaseContentForJenkinsProjects(mockAutomationProjects());
  });
});

function navigateToTestSuite(): TestSuiteViewPage {

  const refData = new ReferentialDataMockBuilder().withProjects(
    {
      allowAutomationWorkflow: true,
      permissions: ALL_PROJECT_PERMISSIONS,
    }
  ).build();
  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'CampaignLibrary-1',
      children: [],
      data: {'NAME': 'Project1'},
    } as unknown as DataRow]
  };
  const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes, refData);
  const libraryChildren = [
    {
      id: 'CampaignLibrary-1',
      children: ['Campaign-3'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'Campaign-3',
      children: [],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      data: {'NAME': 'campaign3', 'CHILD_COUNT': 1}
    } as unknown as DataRow];
  const campaignChildren = [
    {
      id: 'Campaign-3',
      children: ['Iteration-4'],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      state: DataRowOpenState.open,
      data: {'NAME': 'campaign3', 'CHILD_COUNT': 1, 'MILESTONE_STATUS': 'IN_PROGRESS'}
    } as unknown as DataRow,
    {
      id: 'Iteration-4',
      children: [],
      projectId: 1,
      parentRowId: 'Campaign-3',
      data: {'NAME': 'iteration-4', 'CHILD_COUNT': 0, 'MILESTONE_STATUS': 'IN_PROGRESS'}
    } as unknown as DataRow];
  const iterationChildren = [
    {
      id: 'Campaign-3',
      children: ['Iteration-4'],
      projectId: 1,
      parentRowId: 'CampaignLibrary-1',
      state: DataRowOpenState.open,
      data: {'NAME': 'campaign3', 'CHILD_COUNT': 1, 'MILESTONE_STATUS': 'IN_PROGRESS'}
    } as unknown as DataRow,
    {
      id: 'Iteration-4',
      children: ['TestSuite-5'],
      projectId: 1,
      parentRowId: 'Campaign-3',
      state: DataRowOpenState.open,
      data: {'NAME': 'iteration-4', 'CHILD_COUNT': 1, 'MILESTONE_STATUS': 'IN_PROGRESS'}
    } as unknown as DataRow,
    {
      id: 'TestSuite-5',
      children: [],
      projectId: 1,
      parentRowId: 'Iteration-4',
      state: DataRowOpenState.leaf,
      data: {'NAME': 'testSuite-5', 'CHILD_COUNT': 0, 'MILESTONE_STATUS': 'IN_PROGRESS', 'EXECUTION_STATUS': 'RUNNING'}
    } as unknown as DataRow
  ];

  campaignWorkspacePage.tree.openNode('CampaignLibrary-1', libraryChildren);
  campaignWorkspacePage.tree.openNode('Campaign-3', campaignChildren);
  campaignWorkspacePage.tree.openNode('Iteration-4', iterationChildren);

  return campaignWorkspacePage.tree.selectNode<TestSuiteViewPage>('TestSuite-5', buildDefaultTestSuiteModel());
}

function buildDefaultTestSuiteModel(): TestSuiteModel {
  return {
    id: 5,
    projectId: 1,
    name: 'testSuite-5',
    description: 'this is a wonderful test suite',
    uuid: '3e448aca-436a-11eb-a12b-5c80b64fb103',
    executionStatus: 'RUNNING',
    createdOn: new Date('2020-03-09 10:30'),
    createdBy: 'Bébert',
    lastModifiedOn: new Date('2020-03-09 11:30'),
    lastModifiedBy: 'Riton',
    testPlanStatistics: {
      status: 'RUNNING',
      progression: 50,
      nbTestCases: 2,
      nbDone: 1,
      nbReady: 1,
      nbRunning: 0,
      nbUntestable: 0,
      nbBlocked: 0,
      nbFailure: 0,
      nbSettled: 0,
      nbSuccess: 1
    },
    customFieldValues: [],
    attachmentList: {
      id: 1,
      attachments: []
    },
    hasDatasets: true,
    users: [
      {id: 1, login: 'raowl', firstName: 'Ra', lastName: 'Oul'},
      {id: 2, login: 'jawny', firstName: 'Joe', lastName: 'Ni'}
    ],
    executionStatusMap: new Map<number, string>()
  };
}

function mockTestPlan(dataRows: DataRow[]): GridResponse {
  return mockGridResponse('itemTestPlanId', dataRows);
}

function mockITPIDataRow(itemTestPlanId: Identifier, customData?: any): DataRow {
  return mockDataRow({
    id: itemTestPlanId.toString(),
    projectId: 1,
    data: {
      itemTestPlanId,
      testCaseName: 'Test-Case ' + itemTestPlanId,
      testCaseId: 1,
      projectId: 1,
      projectName: 'P1',
      iterationId: 1,
      ...customData,
    },
    allowMoves: true,
  });
}

function mockAutomatedSuitePreview(): AutomatedSuitePreview {
  return {
    manualServerSelection: true,
    specification: {
      context: new EntityReference(1, EntityType.ITERATION),
      testPlanSubsetIds: []
    },
    projects: [{
      projectId: 4,
      label: 'Job4',
      server: 'Jenkins',
      nodes: ['Agent1', 'Agent2', 'Agent3'],
      testCount: 3,
      testList: ['Job4/test_Ok.ta', 'Job4/test_KO.ta', 'Job4_Blocked.ta']
    },
      {
        projectId: 7,
        label: 'Job7',
        server: 'Jenkins',
        nodes: ['Agent1', 'Agent5'],
        testCount: 1,
        testList: ['Job7/connection.feature']
      }],
    squashAutomProjects: []
  };
}

function mockAutomationProjects(): TestAutomationProjectPreview[] {
  return mockAutomatedSuitePreview().projects
    .map((automationProject: TestAutomationProjectPreview) => ({
      ...automationProject,
      nodes: [automationProject.server, ...automationProject.nodes, 'Indifférent']
    }));
}

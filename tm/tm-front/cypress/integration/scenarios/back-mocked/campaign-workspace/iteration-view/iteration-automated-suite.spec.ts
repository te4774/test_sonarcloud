import {CampaignWorkspacePage} from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {IterationViewPage} from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {IterationModel} from '../../../../model/campaign/iteration-model';
import {mockDataRow, mockGridResponse} from '../../../../data-mock/grid.data-mock';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';
import {getEmptyIterationStatisticsBundle, mockIterationModel} from '../../../../data-mock/iteration.data-mock';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {
  IterationAutomatedSuitePage
} from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-automated-suite.page';

describe('Iteration View', function () {
  it('should display a simple test plan', () => {
    const iterationViewPage = navigateToIteration();
    const today = new Date();
    const testPlan = mockTestPlan([
      mockAutomatedSuiteDataRow('456', today),
      mockAutomatedSuiteDataRow('123', today, {executionStatus: 'RUNNING'}),
      mockAutomatedSuiteDataRow('az47', today, {hasExecution: true, hasResultUrl: true}),
      mockAutomatedSuiteDataRow('sdfds58', today),
    ]);
    const automatedSuitePage = iterationViewPage.clickAnchorLink('automated-suite', testPlan) as unknown as IterationAutomatedSuitePage;
    automatedSuitePage.testPlan.assertRowCount(4);
    automatedSuitePage.testPlan.assertRowExist('456');
    const row2 = automatedSuitePage.testPlan.getRow('123');
    row2.assertExist();
    row2.cell('executionDetails').iconRenderer().assertNotExist();
    row2.cell('executionReport').iconRenderer().assertNotExist();
    const row3 = automatedSuitePage.testPlan.getRow('az47');
    row3.assertExist();
    row3.cell('executionDetails').iconRenderer().assertContainIcon('anticon-sqtm-core-campaign:exec_details');
    row3.cell('executionReport').iconRenderer().assertContainIcon('anticon-sqtm-core-campaign:exec_report');

    automatedSuitePage.testPlan.assertRowExist('sdfds58');
  });


  function navigateToIteration(items: any[] = []): IterationViewPage {
    const refData = new ReferentialDataMockBuilder().withProjects(
      {
        allowAutomationWorkflow: true,
        permissions: ALL_PROJECT_PERMISSIONS,
      }
    ).build();

    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'CampaignLibrary-1',
        children: [],
        data: {'NAME': 'Project1'},
      } as unknown as DataRow]
    };
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes, refData);
    const libraryChildren = [
      {
        id: 'CampaignLibrary-1',
        children: ['Campaign-3'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'Campaign-3',
        children: [],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1}
      } as unknown as DataRow];

    const campaignChildren = [
      {
        id: 'Campaign-3',
        children: ['Iteration-1'],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        state: DataRowOpenState.open,
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1}
      } as unknown as DataRow,
      {
        id: 'Iteration-1',
        children: [],
        projectId: 1,
        parentRowId: 'Campaign-3',
        data: {'NAME': 'iteration-1', 'CHILD_COUNT': 0}
      } as unknown as DataRow];
    campaignWorkspacePage.tree.openNode('CampaignLibrary-1', libraryChildren);
    campaignWorkspacePage.tree.openNode('Campaign-3', campaignChildren);
    const model: IterationModel = mockIterationModel({
      id: 1,
      projectId: 1,
      name: 'iteration-1',
      itpi: items,
      iterationStatus: 'PLANNED',
      uuid: 'b368',
      testPlanStatistics: {
        status: 'DONE',
        progression: 100,
        nbTestCases: 3,
        nbDone: 3,
        nbReady: 0,
        nbRunning: 0,
        nbUntestable: 0,
        nbBlocked: 0,
        nbFailure: 0,
        nbSettled: 0,
        nbSuccess: 0
      },
      hasDatasets: true,
      users: [
        {id: 1, login: 'raowl', firstName: 'Ra', lastName: 'Oul'},
        {id: 2, login: 'jawny', firstName: 'Joe', lastName: 'Ni'}
      ],
      testSuites: [
        {id: 1, name: 'suite01'},
        {id: 2, name: 'suite02'},
      ]
    });
    new HttpMockBuilder(`iteration-view/${model.id}/statistics`)
      .responseBody(getEmptyIterationStatisticsBundle())
      .build();
    return campaignWorkspacePage.tree.selectNode<IterationViewPage>('Iteration-1', model);
  }
});

function mockAutomatedSuiteDataRow(suiteId: string, createdOn: Date, customData?: any): DataRow {
  return mockDataRow({
    id: suiteId, projectId: 1,
    data: {
      suiteId: suiteId,
      createdBy: 'admin',
      createdOn: createdOn,
      lastModifiedOn: null,
      executionStatus: 'SUCCESS',
      hasExecution: false,
      hasResultUrl: false,
      ...customData
    },
    allowMoves: true,
  });
}

function mockTestPlan(dataRows: DataRow[]): GridResponse {
  return mockGridResponse('suiteId', dataRows);
}

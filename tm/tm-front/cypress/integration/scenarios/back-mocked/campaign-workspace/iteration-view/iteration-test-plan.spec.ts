import {mockDataRow, mockGridResponse} from '../../../../data-mock/grid.data-mock';
import {getEmptyIterationStatisticsBundle, mockIterationModel} from '../../../../data-mock/iteration.data-mock';
import {initialTestCaseLibraries, projectOneChildren} from '../../../../data-mock/test-case-simple-tree';
import {IterationModel} from '../../../../model/campaign/iteration-model';
import {EntityReference, EntityType} from '../../../../model/entity.model';
import {ExecutionModel, ExecutionStepModel} from '../../../../model/execution/execution.model';
import {DataRow, DataRowOpenState, GridResponse, Identifier} from '../../../../model/grids/data-row.type';
import {ExecutionStatusKeys} from '../../../../model/level-enums/level-enum';
import {AutomatedSuiteOverview} from '../../../../model/test-automation/automated-suite-overview.model';
import {
  AutomatedSuitePreview,
  SquashAutomProjectPreview,
  TestAutomationProjectPreview
} from '../../../../model/test-automation/automated-suite-preview.model';
import {
  EnvironmentSelectionPanelDto
} from '../../../../page-objects/elements/automated-execution-environments/environment-selection-panel.element';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {CampaignWorkspacePage} from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  IterationTestPlanPage
} from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-test-plan.page';
import {IterationViewPage} from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';

describe('Iteration View', function () {
  it('should display a simple test plan', () => {
    const iterationViewPage = navigateToIteration();

    const testPlan = mockTestPlan([
      mockITPIDataRow('1'),
      mockITPIDataRow('2'),
    ]);

    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);
    testPlanPage.testPlan.assertRowExist('1');
    testPlanPage.testPlan.assertRowExist('2');

    // should display and filter a test plan
    testPlanPage.testPlan.declareRefreshData(mockTestPlan([testPlan.dataRows[0]]));
    testPlanPage.testPlan.filterByTextColumn('testCaseName', 'value', mockTestPlan([testPlan.dataRows[0]]));

    testPlanPage.testPlan.assertRowExist('1');
    testPlanPage.testPlan.assertRowNotExist('2');
  });

  it('should add and remove test cases to iteration', () => {
    const iterationViewPage = navigateToIteration();

    const testPlan = mockTestPlan([]);

    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    const testCaseTreePicker = testPlanPage.openTestCaseDrawer(initialTestCaseLibraries);
    testCaseTreePicker.openNode('TestCaseLibrary-1', projectOneChildren);
    testCaseTreePicker.beginDragAndDrop('TestCase-3');

    // We add as many rows as we need to delete them later. We don't care about realism :)
    const fullTestPlan = [
      mockITPIDataRow(1),
      mockITPIDataRow(2),
      mockITPIDataRow(3),
    ];

    testPlanPage.enterIntoTestPlan();
    testPlanPage.assertColoredBorderIsVisible();
    testPlanPage.dropIntoTestPlan(1, [1], mockTestPlan(fullTestPlan));
    testPlanPage.verifyTestPlanItem({id: 1, name: 'Test-Case 1', showsAsLink: true});
    testPlanPage.assertColoredBorderIsNotVisible();
    testPlanPage.closeTestCaseDrawer();

    // should remove a single test case from iteration
    const confirmDialogComponent = testPlanPage.showDeleteConfirmDialog(1, 1);

    confirmDialogComponent.assertExist();
    confirmDialogComponent.deleteForSuccess(mockTestPlan(fullTestPlan.slice(1)));
    confirmDialogComponent.assertNotExist();

    // should mass remove test cases from iteration
    testPlanPage.testPlan.selectRowsWithStickyIndexColumn([2, 3]);
    const confirmMultiDialogComponent = testPlanPage.showMassDeleteConfirmDialog(1, [2, 3]);
    confirmMultiDialogComponent.assertExist();
    confirmMultiDialogComponent.deleteForSuccess(mockTestPlan([]));
    confirmMultiDialogComponent.assertNotExist();
  });

  it('should allow to update data from grid', () => {
    const iterationViewPage = navigateToIteration();


    const testPlan = mockTestPlan([mockITPIDataRow(1, {
      itemTestPlanId: 1,
      testCaseName: 'Test-Case 1',
      projectId: 1,
      iterationId: 1,
      lastExecutedOn: null,
      executionStatus: 'RUNNING',
      user: 'gilbert',
      hasExecutions: false,
      datasetName: 'JDD',
      availableDatasets: [{id: 1, name: 'JDD'}, {id: 2, name: 'Dataset'}],
    })]);
    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);
    testPlanPage.foldGrid();

    // Apply fast pass
    cy.get(`[data-test-cell-id=execution-status-cell]`).should('have.css', 'background-color', 'rgb(0, 120, 174)');
    testPlanPage.applyFastpassOnItpi('SUCCESS');
    cy.get(`[data-test-cell-id=execution-status-cell]`).should('have.css', 'background-color', 'rgb(0, 111, 87)');

    // should update dataset
    cy.get(`[data-test-cell-id=dataset-cell]`).should('contain.text', 'JDD');
    testPlanPage.changeDataset('item-2');
    cy.get(`[data-test-cell-id=dataset-cell]`).should('contain.text', 'Dataset');

    // should update assigned user
    const row = testPlanPage.testPlan.getRow(1);
    const assignedUserCell = row.cell('user').selectRenderer();
    assignedUserCell.changeValue('item-2', 'test-plan-item/*/assign-user');
    assignedUserCell.assertContainText('Joe Ni (jawny)');
  });

  it('should create execution in page mode', () => {
    const iterationViewPage = navigateToIteration();

    const testPlan = mockTestPlan([mockITPIDataRow(1)]);

    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);
    testPlanPage.clickOnPlayButton(1);
    testPlanPage.checkPlayMenu();
    const executionModel: ExecutionModel = getExecutionModel();
    const executionPage = testPlanPage.launchExecutionInPageMode(1, 1, 1, undefined, executionModel);
    executionPage.checkName('REF4 - Test Case');
  });

  it('should create an item automated execution in dialog', () => {
    const iterationViewPage = navigateToIteration();
    const testPlan = mockTestPlan([mockITPIDataRow(1, { executionMode: 'AUTOMATED' })]);
    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);
    testPlanPage.foldGrid();
    testPlanPage.clickOnPlayButton(1);
    testPlanPage.checkPlayMenu(4);
    const automatedExecutionDialog = testPlanPage.openAutoTestExecDialogViaLaunchAllButton({
      manualServerSelection: false,
      specification: {
        context: new EntityReference(1, EntityType.ITERATION),
        testPlanSubsetIds: []
      },
      projects: [],
      squashAutomProjects: []
    });
    automatedExecutionDialog.assertExist();
  });

  it('should open execution history of an itpi', () => {
    const iterationViewPage = navigateToIteration();

    const testPlan = mockTestPlan([mockITPIDataRow(1)]);
    const testPlanExecution = {
      'count': 3,
      'idAttribute': null,
      'dataRows': [
        {
          index: 1,
          id: '48',
          data: {
            executionOrder: 2,
            executionReference: '',
            successRate: 0,
            importance: 'HIGH',
            executionStatus: 'READY',
            datasetName: 'JDD',
            executionMode: 'MANUAL',
            itemTestPlanId: 5,
            issueCount: 0,
            executionId: 48,
            executionName: 'TC2',
            lastExecutedOn: null,
            iterationId: 1,
            projectId: 1,
            user: 'admin'
          },
          projectId: 1
        },
        {
          index: 2,
          id: '35',
          data: {
            executionOrder: 1,
            executionReference: '',
            successRate: 0,
            importance: 'HIGH',
            executionStatus: 'BLOCKED',
            datasetName: 'JDD',
            executionMode: 'MANUAL',
            itemTestPlanId: 5,
            issueCount: 1,
            executionId: 35,
            executionName: 'TC2',
            lastExecutedOn: '2021-01-19T15:39:12.000+0000',
            iterationId: 1,
            projectId: 1,
            user: 'admin'
          },
          projectId: 1
        },
        {
          index: 3,
          id: '14',
          data: {
            executionOrder: 0,
            executionReference: '',
            successRate: 0,
            importance: 'HIGH',
            executionStatus: 'BLOCKED',
            datasetName: 'JDD',
            executionMode: 'MANUAL',
            itemTestPlanId: 5,
            issueCount: 2,
            executionId: 14,
            executionName: 'TC2',
            lastExecutedOn: '2021-01-19T18:19:42.000+0000',
            iterationId: 1,
            projectId: 1,
            user: 'admin'
          },
          projectId: 1
        }
      ]
    };

    const testPlanExecutionAfterMultiDeletion = {
      'count': 1,
      'idAttribute': null,
      'dataRows': [
        {
          index: 1,
          id: '48',
          data: {
            executionOrder: 2,
            executionReference: '',
            successRate: 0,
            importance: 'HIGH',
            executionStatus: 'READY',
            datasetName: 'JDD',
            executionMode: 'MANUAL',
            itemTestPlanId: 5,
            issueCount: 0,
            executionId: 48,
            executionName: 'TC2',
            lastExecutedOn: null,
            iterationId: 1,
            projectId: 1,
            user: 'admin'
          },
          projectId: 1
        }
      ]
    };

    const testPlanExecutionAfterSingleDeletion = {
      'count': 0,
      'idAttribute': null,
      'dataRows': []
    };

    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);
    testPlanPage.foldGrid();
    testPlanPage.clickOnPlayButton(1);
    testPlanPage.checkPlayMenu();
    const executionDialog = testPlanPage.openExecutionHistoryDialog(testPlanExecution);
    executionDialog.checkFirstIdInExecutionHistoryDialog(2, 48);

    executionDialog.deleteMultiSelectedExecution([35, 14], testPlanExecutionAfterMultiDeletion);
    executionDialog.gridElement.assertRowCount(1);

    executionDialog.deleteSelectedExecution(48, testPlanExecutionAfterSingleDeletion);
    executionDialog.gridElement.assertRowCount(0);
  });

  it('should reorder items with drag and drop', () => {
    const iterationViewPage = navigateToIteration();
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    const testPlan = mockGridResponse('itemTestPlanId', [
      mockITPIDataRow(1),
      mockITPIDataRow(2),
      mockITPIDataRow(3),
      mockITPIDataRow(4),
    ]);

    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);
    testPlanPage.testPlan.assertRowCount(4);
    testPlanPage.moveItemWithServerResponse('1', '3', {});

    // should forbid items reordering if grid is filtered
    testPlanPage.testPlan.filterByTextColumn('projectName', 'P', testPlan);
    testPlanPage.assertCannotMoveItem('1');
  });

  it('should mass edit ITPIs', () => {
    const iterationViewPage = navigateToIteration();
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    const dataRows = [
      mockITPIDataRow(1),
      mockITPIDataRow(2),
      mockITPIDataRow(3),
      mockITPIDataRow(4),
    ];
    const testPlan = mockGridResponse('itemTestPlanId', dataRows);

    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);
    testPlanPage.testPlan.selectRowsWithStickyIndexColumn([1, 2]);

    const massEditDialog = testPlanPage.openMassEditDialog();
    massEditDialog.assertExist();

    massEditDialog.toggleTestSuiteEdition();
    massEditDialog.selectTestSuites(['suite01']);
    massEditDialog.toggleAssigneeEdition();
    massEditDialog.selectAssignee('Joe Ni (jawny)');
    massEditDialog.toggleStatusEdition();
    massEditDialog.selectStatus('En cours');

    massEditDialog.confirm(dataRows);
  });

  it('should create a test suite', () => {
    const iterationViewPage = navigateToIteration();
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    const dataRows = [
      mockITPIDataRow(1),
      mockITPIDataRow(2),
      mockITPIDataRow(3),
      mockITPIDataRow(4),
    ];
    const testPlan = mockGridResponse('itemTestPlanId', dataRows);

    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);
    testPlanPage.testPlan.selectRowsWithStickyIndexColumn([1]);

    const createTestSuiteDialog = testPlanPage.openCreateTestSuiteDialog();
    createTestSuiteDialog.assertExist();

    createTestSuiteDialog.nameField.fill('TS01');
    createTestSuiteDialog.descriptionField.fill('test suite 01');

    const addedTS = {data: {ID: 1}} as unknown as DataRow;
    createTestSuiteDialog.confirm(addedTS);
  });

  it('should not allows to create execution if TC was deleted', () => {
    const iterationViewPage = navigateToIteration();
    const deletedTestCaseDataRow = mockITPIDataRow(1, {testCaseId: null});
    const testPlan = mockTestPlan([deletedTestCaseDataRow]);
    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);

    testPlanPage.testPlan.getRow('1').cell('testCaseName').textRenderer().assertContainText('(supprimé)');

    testPlanPage.clickOnPlayButton(1);

    testPlanPage.checkPlayMenuDontAllowNewExecution();
  });

  it('should refuse to launch execution plan on error', () => {
    const iterationViewPage = navigateToIteration();
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    const dataRows = [
      mockITPIDataRow(1),
      mockITPIDataRow(2),
      mockITPIDataRow(3),
      mockITPIDataRow(4),
    ];
    const testPlan = mockGridResponse('itemTestPlanId', dataRows);

    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);
    const resumeAlertDialog = testPlanPage.launchExecutionWithError('1');
    resumeAlertDialog.assertExist();
    resumeAlertDialog.assertHasMessage('L\'itération ne peut pas être exécutée parce que le plan de test est vide.');
  });

  it('should launch automated tests', () => {
    const iterationViewPage = navigateToIteration();
    const testPlan = mockTestPlan([
      mockITPIDataRow(1, {testCaseName: 'TestCase_OK', executionMode: 'AUTOMATED'}),
      mockITPIDataRow(2, {testCaseName: 'TestCase_KO', executionMode: 'MANUAL'}),
      mockITPIDataRow(3, {testCaseName: 'TestCase_BLOCKED', executionMode: 'MANUAL'}),
      mockITPIDataRow(4, {testCaseName: 'TestCase_Connection', executionMode: 'AUTOMATED'}),
      mockITPIDataRow(5, {testCaseName: 'Test_SquashAutom_Login', executionMode: 'AUTOMATED'}),
      mockITPIDataRow(6, {testCaseName: 'Test_SquashAutom_Welcome', executionMode: 'AUTOMATED'}),
      mockITPIDataRow(7, {testCaseName: 'Test_SquashAutom_Welcome', executionMode: 'AUTOMATED'}),
    ]);
    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);
    testPlanPage.assertLaunchAutomatedMenuExist();
    const executionDialog = testPlanPage.openAutoTestExecDialogViaLaunchAllButton(
      mockAutomatedSuitePreview(),
      [mockEnvironmentPanelResponse()]);
    executionDialog.displayEnvironments(2);

    executionDialog.addEnvironmentTag(2, 'ssh');

    executionDialog.checkAgentPhaseContentForSquashAutomProjects(mockSquashAutomProjects());
    executionDialog.checkAgentPhaseContentForJenkinsProjects(mockAutomationProjects());
    executionDialog.changeAgent(4, 'Jenkins');
    executionDialog.changeAgent(7, 'Agent5');
    executionDialog.confirmAgentPhase(mockAutomatedSuiteOverviewStart());
    executionDialog.checkExecutionPhaseStaticContent();
    executionDialog.checkExecutionPhaseDynamicContentAtStart(mockAutomatedSuiteOverviewStart());
    executionDialog.closeBeforeEnd(mockAutomatedSuiteOverviewStart());
    executionDialog.checkWarningPhaseContent();
    executionDialog.cancel();
    executionDialog.checkExecutionPhaseDynamicContentProceeding(mockAutomatedSuiteOverviewEnd());
    executionDialog.close();
  });

  it('should reach execution phase with only jenkins automated tests', () => {
    const iterationViewPage = navigateToIteration();
    const testPlan = mockTestPlan([
      mockITPIDataRow(1, {testCaseName: 'TestCase_1', executionMode: 'AUTOMATED'}),
      mockITPIDataRow(2, {testCaseName: 'TestCase_2', executionMode: 'AUTOMATED'})
    ]);
    const testPlanPage = iterationViewPage.clickAnchorLink<IterationTestPlanPage>('plan-exec', testPlan);
    const executionDialog = testPlanPage.openAutoTestExecDialogViaLaunchAllButton(mockAutomatedSuitePreviewForJenkinsScenario());
    testPlanPage.assertLaunchAutomatedMenuIsNotVisible();
    executionDialog.checkAgentPhaseContentForJenkinsProjects(mockAutomationProjectsForJenkinsScenario());
    executionDialog.confirmAgentPhase(mockAutomatedSuiteOverviewStartForJenkinsScenario());
    executionDialog.checkExecutionPhaseStaticContent(true, false);
  });

  function navigateToIteration(): IterationViewPage {
    const refData = new ReferentialDataMockBuilder().withProjects(
      {
        allowAutomationWorkflow: true,
        permissions: ALL_PROJECT_PERMISSIONS,
      }
    ).build();

    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'CampaignLibrary-1',
        children: [],
        data: {'NAME': 'Project1'},
      } as unknown as DataRow]
    };
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes, refData);
    const libraryChildren = [
      {
        id: 'CampaignLibrary-1',
        children: ['Campaign-3'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'Campaign-3',
        children: [],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1}
      } as unknown as DataRow];

    const campaignChildren = [
      {
        id: 'Campaign-3',
        children: ['Iteration-1'],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        state: DataRowOpenState.open,
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1}
      } as unknown as DataRow,
      {
        id: 'Iteration-1',
        children: [],
        projectId: 1,
        parentRowId: 'Campaign-3',
        data: {'NAME': 'iteration-1', 'CHILD_COUNT': 0}
      } as unknown as DataRow];
    campaignWorkspacePage.tree.openNode('CampaignLibrary-1', libraryChildren);
    campaignWorkspacePage.tree.openNode('Campaign-3', campaignChildren);

    const model: IterationModel = mockIterationModel(buildDefaultIterationCustomData());
    new HttpMockBuilder(`iteration-view/${model.id}/statistics`)
      .responseBody(getEmptyIterationStatisticsBundle())
      .build();
    const iterationViewPage = campaignWorkspacePage.tree.selectNode<IterationViewPage>('Iteration-1', model);

    // Full screen
    new NavBarElement().toggle();
    iterationViewPage.toggleTree();

    return iterationViewPage;
  }
});

function buildDefaultIterationCustomData(): Partial<IterationModel> {
  return {
    id: 1,
    projectId: 1,
    name: 'iteration-1',
    itpi: [],
    iterationStatus: 'PLANNED',
    uuid: 'b368',
    testPlanStatistics: {
      status: 'DONE',
      progression: 100,
      nbTestCases: 3,
      nbDone: 3,
      nbReady: 0,
      nbRunning: 0,
      nbUntestable: 0,
      nbBlocked: 0,
      nbFailure: 0,
      nbSettled: 0,
      nbSuccess: 0
    },
    hasDatasets: true,
    users: [
      {id: 1, login: 'raowl', firstName: 'Ra', lastName: 'Oul'},
      {id: 2, login: 'jawny', firstName: 'Joe', lastName: 'Ni'}
    ],
    testSuites: [
      {id: 1, name: 'suite01'},
      {id: 2, name: 'suite02'},
    ]
  };
}

function mockITPIDataRow(itemTestPlanId: Identifier, customData?: any): DataRow {
  return mockDataRow({
    id: itemTestPlanId.toString(), projectId: 1,
    data: {
      itemTestPlanId,
      testCaseName: 'Test-Case ' + itemTestPlanId,
      testCaseId: 1,
      projectId: 1,
      projectName: 'P1',
      iterationId: 1,
      ...customData,
    },
    allowMoves: true,
  });
}

function mockTestPlan(dataRows: DataRow[]): GridResponse {
  return mockGridResponse('itemTestPlanId', dataRows);
}

function getExecutionModel(): ExecutionModel {
  return {
    id: 1,
    projectId: 1,
    executionOrder: 2,
    datasetLabel: '',
    executionsCount: 0,
    testCaseId: 0,
    testPlanItemId: 0,
    name: 'REF4 - Test Case',
    prerequisite: '',
    attachmentList: {id: 1, attachments: []},
    customFieldValues: [],
    tcImportance: 'LOW',
    tcNatLabel: 'test-case.nature.NAT_BUSINESS_TESTING',
    tcNatIconName: 'noicon',
    tcStatus: 'APPROVED',
    tcTypeLabel: 'test-case.type.TYP_COMPLIANCE_TESTING',
    tcTypeIconName: 'noicon',
    tcDescription: 'description',
    comment: '',
    denormalizedCustomFieldValues: [],
    executionStepViews: [
      {
        id: 1,
        order: 0,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel,
      {
        id: 2,
        order: 1,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel,
      {
        id: 3,
        order: 2,
        executionStatus: 'READY',
        attachmentList: {
          id: 1, attachments: []
        }
      } as ExecutionStepModel
    ],
    coverages: [],
    executionMode: 'MANUAL',
    lastExecutedOn: null,
    lastExecutedBy: 'admin',
    executionStatus: 'READY',
    automatedJobUrl: null,
    testAutomationServerKind: null,
    automatedExecutionResultUrl: null,
    automatedExecutionResultSummary: null,
    nbIssues: 0,
    iterationId: -1,
    kind: 'STANDARD',
    milestones: []
  };
}

function mockAutomatedSuitePreview(): AutomatedSuitePreview {
  return {
    manualServerSelection: true,
    specification: {
      context: new EntityReference(1, EntityType.ITERATION),
      testPlanSubsetIds: []
    },
    projects: [{
      projectId: 4,
      label: 'Job4',
      server: 'Jenkins',
      nodes: ['Agent1', 'Agent2', 'Agent3'],
      testCount: 3,
      testList: ['Job4/test_ok.ta', 'Job4/test_ko.ta', 'Job4/test_blocked.ta']
    },
      {
        projectId: 7,
        label: 'Job7',
        server: 'Jenkins',
        nodes: ['Agent1', 'Agent5'],
        testCount: 1,
        testList: ['Job7/connection.feature']
      }],
    squashAutomProjects: [{
      projectId: 2,
      projectName: 'project 2',
      serverId: 9,
      serverName: 'orchestrator',
      testCases: [{
        reference: 'LOGIN_1',
        name: 'Test_SquashAutom_Login'
      }, {
        reference: 'WELCOME_3',
        name: 'Test_SquashAutom_Welcome'
      }, {
        reference: 'WELCOME_3',
        name: 'Test_SquashAutom_Welcome'
      }]
    }]
  };
}

function mockAutomatedSuitePreviewForJenkinsScenario(): AutomatedSuitePreview {
  return {
    manualServerSelection: true,
    specification: {
      context: new EntityReference(1, EntityType.ITERATION),
      testPlanSubsetIds: []
    },
    projects: [{
      projectId: 1,
      label: 'Job1',
      server: 'Jenkins',
      nodes: ['Agent1', 'Agent2'],
      testCount: 2,
      testList: ['TestCase_1', 'TestCase_2']
    }],
    squashAutomProjects: []
  };
}

function mockEnvironmentPanelResponse(): EnvironmentSelectionPanelDto {
  return {
    environments: {
      environments: [{
        name: 'robot-agent',
        tags: ['robotframework', 'ssh']
      }]
    },
    server: {
      testAutomationServerId: 9,
      defaultTags: [],
      hasServerCredentials: true
    },
    project: {
      projectId: 2,
      projectTags: ['robotframework'],
      areProjectTagsInherited: false,
      hasProjectToken: false,
    }
  };
}


function mockAutomationProjects(): TestAutomationProjectPreview[] {
  return extractProjectPreviewsFromSuitePreview(mockAutomatedSuitePreview());
}

function mockAutomationProjectsForJenkinsScenario(): TestAutomationProjectPreview[] {
  return extractProjectPreviewsFromSuitePreview(mockAutomatedSuitePreviewForJenkinsScenario());
}

function extractProjectPreviewsFromSuitePreview(automatedSuitePreview: AutomatedSuitePreview): TestAutomationProjectPreview[] {
  return automatedSuitePreview.projects
    .map((automationProject: TestAutomationProjectPreview) => ({
      ...automationProject,
      nodes: [automationProject.server, ...automationProject.nodes, 'Indifférent']
    }));
}

function mockAutomatedSuiteOverviewStart(): AutomatedSuiteOverview {
  return {
    suiteId: '4028b88178ef5c290178ef6e8347j0g0',
    executions: [{
      id: 4,
      name: 'TestCase_OK',
      status: 'RUNNING',
      node: 'master',
      automatedProject: 'Job4'
    }, {
      id: 5,
      name: 'TestCase_KO',
      status: 'READY',
      node: 'master',
      automatedProject: 'Job4'
    }, {
      id: 6,
      name: 'TestCase_BLOCKED',
      status: 'READY',
      node: 'master',
      automatedProject: 'Job4'
    }, {
      id: 7,
      name: 'TestCase_Connection',
      status: 'READY',
      node: 'Agent5',
      automatedProject: 'Job7'
    }],
    items: [{
      id: 56,
      name: 'Test_SquashAutom_Login',
      automatedServerName: 'Orchestrator'
    }, {
      id: 57,
      name: 'Test_SquashAutom_Welcome',
      automatedServerName: 'Orchestrator'
    }, {
      id: 58,
      name: 'Test_SquashAutom_Welcome',
      automatedServerName: 'Orchestrator'
    }],
    percentage: 0
  };
}


function mockAutomatedSuiteOverviewStartForJenkinsScenario(): AutomatedSuiteOverview {
  return {
    suiteId: '4028b88178ef5c290178ef6e8347j0g0',
    executions: [{
      id: 4,
      name: 'TestCase_1',
      status: 'RUNNING',
      node: 'Agent1',
      automatedProject: 'Job1'
    }, {
      id: 5,
      name: 'TestCase_2',
      status: 'RUNNING',
      node: 'Agent1',
      automatedProject: 'Job1'
    }],
    items: [],
    percentage: 0
  };
}

function mockAutomatedSuiteOverviewEnd(): AutomatedSuiteOverview {
  const endOverview = mockAutomatedSuiteOverviewStart();
  ['BLOCKED', 'SUCCESS', 'FAILURE', 'UNTESTABLE'].forEach((value: ExecutionStatusKeys, index) => {
    endOverview.executions[index].status = value;
  });
  endOverview.percentage = 100;
  return endOverview;
}

function mockSquashAutomProjects(): SquashAutomProjectPreview[] {
  return mockAutomatedSuitePreview().squashAutomProjects;
}


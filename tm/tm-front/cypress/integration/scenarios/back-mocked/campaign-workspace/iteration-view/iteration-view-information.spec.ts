import {IterationViewPage} from '../../../../page-objects/pages/campaign-workspace/iteration/iteration-view.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {CampaignWorkspacePage} from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {IterationModel} from '../../../../model/campaign/iteration-model';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {getEmptyIterationStatisticsBundle, mockIterationModel} from '../../../../data-mock/iteration.data-mock';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

describe('Iteration - Information', () => {
  it('should display iteration informations', () => {
    const iterationViewPage = navigateToIteration();
    const infoPanel = iterationViewPage.informationPanel;
    iterationViewPage.clickOnAnchorLink('information');

    iterationViewPage.checkData('iteration-id', '1');
    iterationViewPage.checkData('iteration-created', '09/03/2020 10:30 (toto)');
    iterationViewPage.checkData('iteration-lastModified', '09/03/2020 11:30 (gilbert)');
    iterationViewPage.checkData('iteration-status', 'Planifié');
    iterationViewPage.checkData('iteration-progress-state', 'Terminé');
    infoPanel.descriptionRichField.checkTextContent('this is description');
  });

  it('should allow modifications on iteration informations', () => {
    const iterationViewPage = navigateToIteration();
    const infoPanel = iterationViewPage.informationPanel;
    iterationViewPage.clickOnAnchorLink('information');

    infoPanel.rename('GREAT ITERATION ! NO CHEATING !', {
      dataRows: [{
        ...initialIterationRow,
        data: {...initialIterationRow.data, 'NAME': 'GREAT ITERATION ! NO CHEATING !'}
      }]
    });
    infoPanel.statusSelectField.setAndConfirmValueNoButton('Archivé');

    const descriptionElement = infoPanel.descriptionRichField;
    descriptionElement.enableEditMode();
    descriptionElement.setValue('Hello, World!');
    descriptionElement.confirm('Hello, World!');
    descriptionElement.enableEditMode();
    descriptionElement.clear();
    descriptionElement.confirm(`Cliquer pour renseigner une description`);
  });


  function navigateToIteration(items: any[] = []): IterationViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'CampaignLibrary-1',
        children: [],
        data: {'NAME': 'Project1'},
      } as unknown as DataRow]
    };

    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
    const libraryChildren = [
      {
        id: 'CampaignLibrary-1',
        children: ['Campaign-3'],
        data: {'NAME': 'Project1', 'CHILD_COUNT': 1},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'Campaign-3',
        children: [],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1}
      } as unknown as DataRow];

    const campaignChildren = [
      {
        id: 'Campaign-3',
        children: ['Iteration-1'],
        projectId: 1,
        parentRowId: 'CampaignLibrary-1',
        state: DataRowOpenState.open,
        data: {'NAME': 'campaign3', 'CHILD_COUNT': 1, 'MILESTONE_STATUS': 'IN_PROGRESS'}
      } as unknown as DataRow,
      {
        id: 'Iteration-1',
        children: [],
        projectId: 1,
        parentRowId: 'Campaign-3',
        data: {'NAME': 'iteration-1', 'CHILD_COUNT': 0, 'MILESTONE_STATUS': 'IN_PROGRESS'}
      } as unknown as DataRow];
    campaignWorkspacePage.tree.openNode('CampaignLibrary-1', libraryChildren);
    campaignWorkspacePage.tree.openNode('Campaign-3', campaignChildren);
    const model: IterationModel = mockIterationModel({
      id: 1,
      projectId: 1,
      name: 'iteration-1',
      itpi: items,
      reference: 'REF 1',
      description: 'this is description',
      createdBy: 'toto',
      createdOn: new Date('2020-03-09 10:30'),
      lastModifiedBy: 'gilbert',
      lastModifiedOn: new Date('2020-03-09 11:30'),
      scheduledStartDate: new Date('2020-04-10 10:30'),
      scheduledEndDate: new Date('2020-05-11 10:30'),
      actualStartDate: new Date('2020-04-10 10:30'),
      actualEndDate: new Date('2020-05-11 10:30'),
      actualStartAuto: false,
      actualEndAuto: false,
      iterationStatus: 'PLANNED',
      uuid: 'b368',
    });
    new HttpMockBuilder(`iteration-view/${model.id}/statistics`)
      .responseBody(getEmptyIterationStatisticsBundle())
      .build();
    return campaignWorkspacePage.tree.selectNode<IterationViewPage>('Iteration-1', model);
  }

  const initialIterationRow: DataRow = {
    id: 'Iteration-1',
    children: [],
    projectId: 1,
    parentRowId: 'Campaign-3',
    data: {'NAME': 'iteration-1', 'CHILD_COUNT': 0}
  } as unknown as DataRow;
});

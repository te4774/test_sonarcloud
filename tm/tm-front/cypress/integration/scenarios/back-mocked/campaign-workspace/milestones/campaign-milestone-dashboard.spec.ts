import {getDefaultCampaignStatisticsBundle} from '../../../../data-mock/campaign.data-mock';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {CampaignWorkspacePage} from '../../../../page-objects/pages/campaign-workspace/campaign-workspace.page';
import {
  CampaignStatisticsPanelElement
} from '../../../../page-objects/pages/campaign-workspace/campaign/campaign-statistics-panel.element';

describe('Campaign MilestoneDashboard', () => {
  const referentialDataMock = new ReferentialDataMockBuilder()
    .withProjects(
      {name: 'Project 1', label: 'Etiquette', permissions: ALL_PROJECT_PERMISSIONS},
      {name: 'Project 2', label: 'Etiquette 2', permissions: ALL_PROJECT_PERMISSIONS}
    ).withMilestones(
      {
        label: 'milestone1',
        description: '',
        endDate: new Date(),
        status: 'IN_PROGRESS',
        boundProjectIndexes: [0],
        range: 'GLOBAL',
      },
      {
        label: 'milestone2',
        description: '',
        endDate: new Date(),
        status: 'FINISHED',
        boundProjectIndexes: [0],
        range: 'GLOBAL',
      },
      {
        label: 'milestone3',
        description: '',
        endDate: new Date(),
        status: 'LOCKED',
        boundProjectIndexes: [1],
        range: 'GLOBAL',
      })
    .withUser({functionalTester: true})
    .build();

  const campaignLibrary1 = {
    id: 'CampaignLibrary-1',
    children: ['CampaignFolder-1', 'Campaign-3', 'Campaign-4', 'CampaignFolder-2'],
    projectId: 1,
    data: {'NAME': 'Project 1', 'CHILD_COUNT': '3', 'MILESTONES': [1, 2]},
    state: DataRowOpenState.open
  } as unknown as DataRow;
  const campaignLibrary2 = {
    id: 'CampaignLibrary-2',
    children: ['Campaign-5'],
    projectId: 2,
    data: {'NAME': 'Project 2', 'CHILD_COUNT': '0', 'MILESTONES': [3]},
    state: DataRowOpenState.open
  } as unknown as DataRow;

  const campaignFolder1 = {
    id: 'CampaignFolder-1',
    children: [],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: {'NAME': 'folder1', 'MILESTONES': [1, 2]}
  } as unknown as DataRow;
  const campaignFolder2 = {
    id: 'CampaignFolder-2',
    children: [],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: {'NAME': 'folder2', 'MILESTONES': [1, 2]}
  } as unknown as DataRow;

  const campaign3 = {
    id: 'Campaign-3',
    children: ['Iteration-1'],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: {
      'NAME': 'campaign number 3',
      'MILESTONES': [1],
    },
    state: DataRowOpenState.open
  } as unknown as DataRow;
  const campaign4 = {
    id: 'Campaign-4',
    children: [],
    projectId: 1,
    parentRowId: 'CampaignLibrary-1',
    data: {
      'NAME': 'campaign number 4',
      'MILESTONES': [2]
    }
  } as unknown as DataRow;
  const campaign5 = {
    id: 'Campaign-5',
    children: ['Iteration-2'],
    projectId: 1,
    parentRowId: 'CampaignLibrary-2',
    data: {
      'NAME': 'campaign number 5',
      'MILESTONES': [3],
    },
    state: DataRowOpenState.open
  } as unknown as DataRow;

  const iteration1 = {
    id: 'Iteration-1',
    children: ['TestSuite-1'],
    projectId: 1,
    parentRowId: 'Campaign-3',
    data: {
      'NAME': 'iteration 1',
      'MILESTONES': [1]
    },
    state: DataRowOpenState.open
  } as unknown as DataRow;
  const iteration2 = {
    id: 'Iteration-2',
    children: ['TestSuite-2'],
    projectId: 1,
    parentRowId: 'Campaign-5',
    data: {
      'NAME': 'iteration 2',
      'MILESTONES': [3]
    },
    state: DataRowOpenState.open
  } as unknown as DataRow;

  const allNodes: GridResponse = {
    count: 9,
    dataRows: [campaignLibrary1, campaignLibrary2, campaignFolder1, campaignFolder2,
      campaign3, campaign4, campaign5, iteration1, iteration2],
  };

  it('should select milestone above tree buttons', () => {
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(allNodes, referentialDataMock);
    const navbar = campaignWorkspacePage.navBar;

    const milestonePicker = navbar.openMilestoneSelector();
    milestonePicker.selectMilestone(1);
    milestonePicker.confirm();

    campaignWorkspacePage.treeMenu.assertMilestoneButtonIsVisible();
    const campaignMilestoneSelector = campaignWorkspacePage.treeMenu.showMilestoneSelector();
    campaignMilestoneSelector.assertMilestoneIsSelected(1);
    campaignMilestoneSelector.selectMilestone(2);
    campaignMilestoneSelector.confirm();
    campaignWorkspacePage.treeMenu.showMilestoneSelector();
    campaignMilestoneSelector.assertMilestoneIsSelected(2);
    campaignMilestoneSelector.cancel();
    campaignWorkspacePage.navBar.assertMilestoneModeIsActive();
    campaignWorkspacePage.navBar.openMilestoneSelector();
    campaignMilestoneSelector.assertMilestoneIsSelected(2);
  });

  it('should display campaign milestone dashboard', () => {
    const campaignWorkspacePage = CampaignWorkspacePage.initTestAtPage(allNodes, referentialDataMock);
    const navbar = campaignWorkspacePage.navBar;
    const milestonePicker = navbar.openMilestoneSelector();
    milestonePicker.selectMilestone(1);
    milestonePicker.confirm();

    const milestonePage = campaignWorkspacePage.treeMenu
      .showCampaignMilestoneDashboard({statistics: getDefaultCampaignStatisticsBundle()});
    milestonePage.assertNameEquals('milestone1');

    const dashboardPanel = milestonePage.clickAnchorLink<CampaignStatisticsPanelElement>('dashboard');
    dashboardPanel.assertTitleExist('Statistiques des campagnes du jalon');
    dashboardPanel.importanceChart.assertChartExist();
    dashboardPanel.importanceChart.assertHasTitle('Importance des tests jamais exécutés');
    dashboardPanel.conclusivenessChart.assertChartExist();
    dashboardPanel.statusChart.assertChartExist();
    dashboardPanel.statusChart.assertHasTitle('Statut des tests');

  });
});

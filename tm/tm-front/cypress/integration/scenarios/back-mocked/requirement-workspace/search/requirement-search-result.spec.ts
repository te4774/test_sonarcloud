import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {
  RequirementSearchPage
} from '../../../../page-objects/pages/requirement-workspace/search/requirement-search-page';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {Permissions} from '../../../../model/permissions/permissions.model';
import {Project, WorkspaceTypeForPlugins} from '../../../../model/project/project.model';
import {BindableEntity} from '../../../../model/bindable-entity.model';
import {MilestoneMassEdit} from '../../../../model/milestone/milestone.model';
import {
  NoPermissionToBindMilestoneDialog
} from '../../../../page-objects/pages/requirement-workspace/dialogs/NoPermissionToBindMilestoneDialog';
import {ReferentialData} from '../../../../model/referential-data.model';

describe('Requirement Search Results', function () {

  beforeEach(() => {
    cy.viewport(1200, 720);
  });

  describe('Requirement search table', () => {

    it('should display row in table', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [requirementVersionBasic]
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(createEntityReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;
      new NavBarElement().toggle();
      requirementSearchPage.foldFilterPanel();

      gridElement.assertRowExist(1);
      const row = gridElement.getRow(1);
      row.cell('projectName').textRenderer().assertContainText('project 1');
      row.cell('reference').textRenderer().assertContainText('ref1');
      row.cell('name').textRenderer().assertContainText('Exigence 1');
      row.cell('category').selectRenderer().assertContainText('Fonctionnelle');
      row.cell('createdBy').textRenderer().assertContainText('admin');
      row.cell('lastModifiedBy').textRenderer().assertContainText('hello');
      row.cell('lastModifiedBy').textRenderer().assertContainText('hello');
      row.cell('milestones').textRenderer().assertContainText('0');
      row.cell('coverages').textRenderer().assertContainText('3');
      row.cell('attachments').textRenderer().assertContainText('2');
      row.cell('versionsCount').textRenderer().assertContainText('3');
      row.cell('versionNumber').textRenderer().assertContainText('2');
    });

    it('should edit cell in table', () => {
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [requirementVersionBasic]
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(createEntityReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;

      gridElement.assertRowExist(1);
      const row = gridElement.getRow(1);
      const refCell = row.cell('reference').textRenderer();
      refCell.editText('ref02', 'requirement-version/1/reference');
      refCell.assertContainText('ref02');

      const categoryCell = row.cell('category').selectRenderer();
      categoryCell.changeValue('item-4', 'requirement-version/*/category');
      categoryCell.assertContainText('Métier');

      const httpError = {
        squashTMError: {
          kind: 'FIELD_VALIDATION_ERROR',
          fieldValidationErrors: [
            {
              fieldName: 'name',
              i18nKey: 'sqtm-core.error.generic.duplicate-name'
            }
          ]
        },
      };
      const nameCell = row.cell('name').textRenderer();
      nameCell.editTextError('Requirement-42', 'requirement-version/*/name', httpError);
      nameCell.assertErrorDialogContains('Un élément avec ce nom existe déjà à cet emplacement');
    });

    it('forbid edition for requirement with status approved or obsolete', () => {
      const approvedRequirementVersion: DataRow = {
        ...requirementVersionBasic, data: {
          ...requirementVersionBasic.data,
          status: 'APPROVED'
        }
      };
      const gridResponse: GridResponse = {
        count: 1,
        dataRows: [approvedRequirementVersion]
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(createEntityReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;

      gridElement.assertRowExist(1);
      const row = gridElement.getRow(1);
      const refCell = row.cell('reference').textRenderer();
      refCell.assertIsNotEditable();
      const nameCell = row.cell('name').textRenderer();
      nameCell.assertIsNotEditable();
    });
  });

  describe('Requirement export dialog', () => {

    it('show export dialog', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionBasic,
          requirementVersionUnderReview
        ]
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(createEntityReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;
      gridElement.assertRowExist(1);

      const alertDialogElement = requirementSearchPage.showExportDialog();
      alertDialogElement.assertExist();
      alertDialogElement.close();
      alertDialogElement.assertNotExist();
    });
  });

  describe('Requirement mass edit dialog', () => {

    it('should mass edit rows', () => {

      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionBasic,
          requirementVersionUnderReview
        ]
      };
      const editResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionBasic,
          {
            id: '2',
            type: 'Requirement',
            projectId: 1,
            data: {
              'name': 'Exigence 2',
              'id': 1,
              'reference': 'ref1',
              'projectName': 'project 1',
              'attachments': 2,
              'status': 'UNDER_REVIEW',
              'criticality': 'MINOR',
              'category': 1,
              'createdBy': 'admin',
              'lastModifiedBy': 'hello',
              'reqMilestoneLocked': 0,
              'milestones': 0,
              'coverages': 3,
              'versionsCount': 3,
              'versionNumber': 2,
              'requirementId': 1,
            }
          } as unknown as DataRow
        ]
      };

      const requirementSearchPage = RequirementSearchPage.initTestAtPage(createEntityReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;
      // gridElement.selectRows([1, 2]);
      gridElement.selectRow(2, '#', 'leftViewport');
      const massEditRequirementDialog = requirementSearchPage.showMassEditDialog();
      massEditRequirementDialog.assertExist();

      const categoryField = massEditRequirementDialog.getOptionalField('category');
      categoryField.check();
      categoryField.selectValue('Fonctionnelle');

      const reqVersionIds = [];
      reqVersionIds.push(gridResponse.dataRows[1].data['id']);
      massEditRequirementDialog.confirm(editResponse, reqVersionIds);

      const row = gridElement.getRow(2);
      const categoryCell = row.cell('category').selectRenderer();
      categoryCell.assertContainText('Fonctionnelle');
    });

    it('should only edit status if obsolete', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionBasic,
          requirementVersionObsolete
        ]
      };

      const requirementSearchPage = RequirementSearchPage.initTestAtPage(createEntityReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;
      gridElement.selectRows([1, 3], '#', 'leftViewport');
      const massEditRequirementDialog = requirementSearchPage.showMassEditDialog();
      massEditRequirementDialog.assertExist();
      massEditRequirementDialog.assertExistCanOnlyEditStatusMessage();
    });

    it('should not mass edit if different categories lists', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionBasic,
          requirementVersionOtherProject
        ]
      };

      const differentCategoriesReferentialData: ReferentialData = {
        ...createEntityReferentialData, projects : [
          ...createEntityReferentialData.projects, projectWithDifferentCategories
        ]
      };

      const requirementSearchPage = RequirementSearchPage.initTestAtPage(differentCategoriesReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;
      gridElement.selectRows([1, 4], '#', 'leftViewport');
      const massEditRequirementDialog = requirementSearchPage.showMassEditDialog();
      massEditRequirementDialog.assertExist();
      massEditRequirementDialog.assertExistDifferentInfoListMessage();
    });

    it('should not display mass edit dialog if no rights on any row', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionLockedMilestone,
          requirementVersionCantWrite
        ]
      };
      const projectsWithDifferentPermissionsReferentialData: ReferentialData = {
        ...createEntityReferentialData, projects : [
          ...createEntityReferentialData.projects, projectWithoutWritingPermissions
        ]
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(projectsWithDifferentPermissionsReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;
      gridElement.selectRows([5, 6], '#', 'leftViewport');
      const massEditRequirementDialog = requirementSearchPage.showMassEditDialog();
      massEditRequirementDialog.assertNotExist();
      massEditRequirementDialog.assertExistNoWritingRightsDialog();
    });

    it('should mass edit only editable rows', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionBasic,
          requirementVersionLockedMilestone
        ]
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(createEntityReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;
      gridElement.selectRows([1, 6], '#', 'leftViewport');
      const massEditRequirementDialog = requirementSearchPage.showMassEditDialog();
      massEditRequirementDialog.assertExist();
      massEditRequirementDialog.assertExistNoWritingRightsMessage();
    });
  });

  describe('Requirement to Milestone mass binding', () => {

    it('should not display dialog if no permissions on project', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionLockedMilestone,
          requirementVersionCantWrite
        ]
      };

      const projectsWithDifferentPermissionsReferentialData: ReferentialData = {
        ...createEntityReferentialData, projects : [
          ...createEntityReferentialData.projects, projectWithoutWritingPermissions
        ]
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(projectsWithDifferentPermissionsReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;
      gridElement.selectRows([5], '#', 'leftViewport');
      const noPermissionsDialog: NoPermissionToBindMilestoneDialog = requirementSearchPage.getNoPermissionToBindMilestoneDialog();
      noPermissionsDialog.assertExist();

    });

    it('should display dialog if permissions OK', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionBasic,
          requirementVersionUnderReview
        ]
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(createEntityReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;
      gridElement.selectRows([1, 2], '#', 'leftViewport');
      const massBindingMilestone: MilestoneMassEdit = {
        milestoneIds: [1, 2],
        checkedIds: [],
        samePerimeter: true,
        boundObjectIds: []
      };
      const massBindingMilestoneDialog = requirementSearchPage.showMassBindingMilestoneDialog(massBindingMilestone);
      massBindingMilestoneDialog.assertExist();
      // massBindingMilestoneDialog.confirmForReq(gridResponse, false);
    });

    it('should display alert if milestone is already bound to requirement', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionBasic,
          requirementVersionUnderReview
        ]
      };

      const requirementSearchPage = RequirementSearchPage.initTestAtPage(createEntityReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;
      gridElement.selectRows([1, 2], '#', 'leftViewport');
      const massBindingMilestone: MilestoneMassEdit = {
        milestoneIds: [1, 2],
        checkedIds: [1],
        samePerimeter: true,
        boundObjectIds: [1]
      };
      const massBindingMilestoneDialog = requirementSearchPage.showMassBindingMilestoneDialog(massBindingMilestone);
      massBindingMilestoneDialog.assertExist();
      massBindingMilestoneDialog.confirmForReq(gridResponse, true);
      const milestoneAlreadyBoundDialog = requirementSearchPage.getMilestoneAlreadyBoundDialog();
      milestoneAlreadyBoundDialog.assertMessage();
    });

    it('should display message if selection has different perimeter', () => {
      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionBasic,
          requirementVersionUnderReview
        ]
      };
      const requirementSearchPage = RequirementSearchPage.initTestAtPage(createEntityReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;
      gridElement.selectRows([1, 2], '#', 'leftViewport');
      const massBindingMilestone: MilestoneMassEdit = {
        milestoneIds: [1, 2],
        checkedIds: [],
        samePerimeter: false,
        boundObjectIds: []
      };
      const massBindingMilestoneDialog = requirementSearchPage.showMassBindingMilestoneDialog(massBindingMilestone);
      massBindingMilestoneDialog.assertExist();
      massBindingMilestoneDialog.assertNoSamePerimeterMessage();
    });

    it('should display alert if selected requirements projects share no milestones', () => {

      const gridResponse: GridResponse = {
        count: 2,
        dataRows: [
          requirementVersionBasic,
          requirementVersionOtherProject
        ]
      };

      const differentCategoriesReferentialData: ReferentialData = {
        ...createEntityReferentialData, projects : [
          ...createEntityReferentialData.projects, projectWithDifferentCategories
        ]
      };

      const requirementSearchPage = RequirementSearchPage.initTestAtPage(differentCategoriesReferentialData, gridResponse);
      const gridElement = requirementSearchPage.grid;
      gridElement.selectRows([1, 4], '#', 'leftViewport');
      const massBindingMilestone: MilestoneMassEdit = {
        milestoneIds: [],
        checkedIds: [],
        samePerimeter: false,
        boundObjectIds: []
      };
      const massBindingMilestoneDialog = requirementSearchPage.showMassBindingMilestoneDialog(massBindingMilestone);
      massBindingMilestoneDialog.assertNotExist();
      const noMilestoneSharedDialog = requirementSearchPage.getNoMilestoneSharedDialog();
      noMilestoneSharedDialog.assertMessage();
    });
  });
});

const requirementVersionBasic = {
  id: '1',
  type: 'Requirement',
  projectId: 1,
  data: {
    'name': 'Exigence 1',
    'id': 1,
    'reference': 'ref1',
    'projectName': 'project 1',
    'attachments': 2,
    'status': 'WORK_IN_PROGRESS',
    'criticality': 'CRITICAL',
    'category': 1,
    'createdBy': 'admin',
    'lastModifiedBy': 'hello',
    'reqMilestoneLocked': 0,
    'milestones': 0,
    'coverages': 3,
    'versionsCount': 3,
    'versionNumber': 2,
    'requirementId': 1,
  }
} as unknown as DataRow;

const requirementVersionUnderReview: DataRow = {
  ...requirementVersionBasic, id: '2', data: {
    ...requirementVersionBasic.data,
    id: 2,
    reference: 'ref2',
    status: 'UNDER_REVIEW',
    requirementId: '2'
  }
};

const requirementVersionObsolete: DataRow = {
  ...requirementVersionBasic, id: '3', data: {
    ...requirementVersionBasic.data,
    id: 3,
    reference: 'ref3',
    status: 'OBSOLETE',
    requirementId: '3'
  }
};

const requirementVersionOtherProject: DataRow = {
  ...requirementVersionBasic, id: '4', projectId: 3, data: {
    ...requirementVersionBasic.data,
    id: 4,
    reference: 'ref4',
    category: 2,
    requirementId: '4'
  }
};

const requirementVersionCantWrite: DataRow = {
  ...requirementVersionBasic, id: '5', projectId: 4, data: {
    ...requirementVersionBasic.data,
    id: 5,
    reference: 'ref5',
    requirementId: '5'
  }
};

const requirementVersionLockedMilestone: DataRow = {
  ...requirementVersionBasic, id: '6', data: {
    ...requirementVersionBasic.data,
    id: 6,
    reference: 'ref6',
    reqMilestoneLocked: 1,
    requirementId: '6'
  }
};

const requirementPreviousVersion: DataRow = {
  ...requirementVersionBasic, id: '7', data: {
    ...requirementVersionBasic.data,
    id: 7,
    reference: 'ref7',
    versionNumber: '1',
    requirementId: '7'
  }
};

const projectWithDifferentCategories: Project = {
  id: 3,
  milestoneBindings: [
    {
      id: 4,
      milestoneId: 4,
      projectId: 1
    },
  ],
  requirementCategoryId: 2,
  testCaseNatureId: 2,
  testCaseTypeId: 3,
  customFieldBindings: {
    [BindableEntity.REQUIREMENT_FOLDER]: [],
    [BindableEntity.REQUIREMENT_VERSION]: [],
    [BindableEntity.TESTCASE_FOLDER]: [],
    [BindableEntity.TEST_CASE]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      }
    ],
    [BindableEntity.TEST_STEP]: [],
    [BindableEntity.CAMPAIGN_FOLDER]: [],
    [BindableEntity.CAMPAIGN]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      }
    ],
    [BindableEntity.ITERATION]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      }
    ],
    [BindableEntity.TEST_SUITE]: [],
    [BindableEntity.EXECUTION]: [],
    [BindableEntity.EXECUTION_STEP]: [],
    [BindableEntity.CUSTOM_REPORT_FOLDER]: [],
  },
  permissions: {
    REQUIREMENT_LIBRARY: [Permissions.READ, Permissions.CREATE, Permissions.WRITE],
    TEST_CASE_LIBRARY: [Permissions.READ, Permissions.CREATE, Permissions.WRITE],
    CAMPAIGN_LIBRARY: [Permissions.READ, Permissions.CREATE, Permissions.WRITE],
    PROJECT: []
  },
  allowAutomationWorkflow: true,
  activatedPlugins: {
    [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
  }
} as Project;

const projectWithoutWritingPermissions: Project = {
  id: 4,
  milestoneBindings: [
    {
      id: 4,
      milestoneId: 4,
      projectId: 1
    },
  ],
  requirementCategoryId: 2,
  testCaseNatureId: 2,
  testCaseTypeId: 3,
  customFieldBindings: {
    [BindableEntity.REQUIREMENT_FOLDER]: [],
    [BindableEntity.REQUIREMENT_VERSION]: [],
    [BindableEntity.TESTCASE_FOLDER]: [],
    [BindableEntity.TEST_CASE]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.TEST_CASE
      }
    ],
    [BindableEntity.TEST_STEP]: [],
    [BindableEntity.CAMPAIGN_FOLDER]: [],
    [BindableEntity.CAMPAIGN]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.CAMPAIGN
      }
    ],
    [BindableEntity.ITERATION]: [
      {
        boundProjectId: 1,
        customFieldId: 12,
        id: 1,
        position: 0,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 13,
        id: 1,
        position: 2,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 14,
        id: 1,
        position: 1,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 15,
        id: 1,
        position: 3,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 16,
        id: 1,
        position: 4,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 17,
        id: 1,
        position: 5,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 18,
        id: 1,
        position: 6,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      },
      {
        boundProjectId: 1,
        customFieldId: 19,
        id: 1,
        position: 7,
        renderingLocations: [],
        bindableEntity: BindableEntity.ITERATION
      }
    ],
    [BindableEntity.TEST_SUITE]: [],
    [BindableEntity.EXECUTION]: [],
    [BindableEntity.EXECUTION_STEP]: [],
    [BindableEntity.CUSTOM_REPORT_FOLDER]: [],
  },
  permissions: {
    REQUIREMENT_LIBRARY: [Permissions.READ],
    TEST_CASE_LIBRARY: [Permissions.READ],
    CAMPAIGN_LIBRARY: [Permissions.READ],
    PROJECT: []
  },
  allowAutomationWorkflow: true,
  activatedPlugins: {
    [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
    [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
  }
} as Project;


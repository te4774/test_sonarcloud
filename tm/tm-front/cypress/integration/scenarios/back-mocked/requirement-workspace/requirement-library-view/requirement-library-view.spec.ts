import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {defaultReferentialData} from '../../../../utils/referential/default-referential-data.const';
import {FolderInformationPanelElement} from '../../../../page-objects/elements/panels/folder-information-panel.element';
// tslint:disable-next-line:max-line-length
import {
  RequirementLibraryViewPage
} from '../../../../page-objects/pages/requirement-workspace/requirement-library/requirement-library-view.page';
import {
  RequirementWorkspacePage
} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {RequirementLibraryModel} from '../../../../model/requirements/requirement-library/requirement-library.model';
import {
  RequirementStatisticPanelElement
} from '../../../../page-objects/pages/requirement-workspace/panels/requirement-statistic-panel.element';
import {RequirementStatistics} from '../../../../model/requirements/requirement-statistics.model';
import {getFavoriteDashboard, getStatistics} from '../requirement-workspace-mock-data';
import {CustomDashboardModel} from '../../../../model/custom-report/custom-dashboard.model';


const description = `<p>a nice description</p>
<table><tbody><tr><th>Col1</th><th>Col2</th></tr><tr><td>Col1L1</td><td>Col2L1</td></tr></tbody></table>`;


describe('Requirement Library View', function () {
  it('should display requirement library page and information', () => {
    const requirementLibraryViewPage = navigateToRequirementLibrary();
    requirementLibraryViewPage.assertExist();
    const informationPanel = requirementLibraryViewPage.clickAnchorLink<FolderInformationPanelElement>('information');
    informationPanel.descriptionRichField.checkHtmlContent(description);
  });

  it('should display requirement library dashboard', () => {
    const requirementLibraryViewPage = navigateToRequirementLibrary();
    requirementLibraryViewPage.assertExist();
    const dashboardPanel = requirementLibraryViewPage.clickAnchorLink<RequirementStatisticPanelElement>('dashboard');
    dashboardPanel.assertTitleExist('Tableau de bord');
    dashboardPanel.orphanRequirementChart.assertChartExist();
    dashboardPanel.orphanRequirementChart.assertHasTitle('Couverture par les cas de test');
    dashboardPanel.statusChart.assertChartExist();
    dashboardPanel.statusChart.assertHasTitle('Statut');
    dashboardPanel.criticalityChart.assertChartExist();
    dashboardPanel.criticalityChart.assertHasTitle('Criticité');
    dashboardPanel.descriptionChart.assertChartExist();
    dashboardPanel.descriptionChart.assertHasTitle('Description');
    dashboardPanel.coverageByCriticalityChart.assertChartExist();
    dashboardPanel.validationByCriticalityChart.assertChartExist();
    dashboardPanel.assertFooterContains('Total des exigences : 10');
  });

  it('should refresh requirement library dashboard', () => {
    const requirementLibraryViewPage = navigateToRequirementLibrary();
    requirementLibraryViewPage.assertExist();
    const dashboardPanel = requirementLibraryViewPage.clickAnchorLink<RequirementStatisticPanelElement>('dashboard');
    dashboardPanel.assertFooterContains('Total des exigences : 10');
    const updatedStatistics: RequirementStatistics = {...getStatistics(), selectedIds: [4, 5, 7, 9, 12, 13, 48, 78]};
    dashboardPanel.refreshStatistics(updatedStatistics);
    dashboardPanel.assertFooterContains('Total des exigences : 8');
  });

  it('should display requirement library favorite dashboard', () => {
    const requirementLibraryViewPage = navigateToRequirementLibraryWithFavoriteDashboard();
    requirementLibraryViewPage.assertExist();
    const dashboardPanel = requirementLibraryViewPage.clickAnchorLink<RequirementStatisticPanelElement>('dashboard');
    dashboardPanel.assertTitleExist('Favorite Dashboard');
    dashboardPanel.assertCustomDashboardExist();
  });

  function navigateToRequirementLibrary(): RequirementLibraryViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'RequirementLibrary-1',
        children: [],
        data: {'NAME': 'Project1'}
      } as unknown as DataRow]
    };
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, defaultReferentialData);

    const model: RequirementLibraryModel = createRequirementLibraryModel({...getStatistics()}, null);
    return requirementWorkspacePage.tree.selectNode<RequirementLibraryViewPage>('RequirementLibrary-1', model);
  }

  function navigateToRequirementLibraryWithFavoriteDashboard(): RequirementLibraryViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'RequirementLibrary-1',
        children: [],
        data: {'NAME': 'Project1'}
      } as unknown as DataRow]
    };
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, defaultReferentialData);

    const model: RequirementLibraryModel = createRequirementLibraryModel(null, {...getFavoriteDashboard()});
    return requirementWorkspacePage.tree.selectNode<RequirementLibraryViewPage>('RequirementLibrary-1', model);
  }

  function createRequirementLibraryModel(statistics: RequirementStatistics, dashboard: CustomDashboardModel): RequirementLibraryModel {
    return {
      canShowFavoriteDashboard: Boolean(dashboard),
      favoriteDashboardId: 1,
      shouldShowFavoriteDashboard: Boolean(dashboard),
      extendHighLvlReqScope: false,
      id: 3,
      projectId: 1,
      name: 'Requirement Library 1',
      customFieldValues: [],
      attachmentList: {
        id: 1,
        attachments: []
      },
      description,
      dashboard: dashboard,
      statistics: statistics
    };
  }
});

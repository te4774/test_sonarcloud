import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {ReferentialDataMockBuilder} from '../../../../utils/referential/referential-data-builder';
import {
  RequirementVersionViewPage
} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view.page';
import {
  RequirementWorkspacePage
} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  RequirementViewPage
} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import {RequirementVersionModel} from '../../../../model/requirements/requirement-version.model';
import {AuthenticationProtocol} from '../../../../model/bugtracker/bug-tracker.model';

const referentialData = new ReferentialDataMockBuilder().withProjects({
  name: 'Project_Issues',
  bugTrackerBinding: {id: 1, bugTrackerId: 1, projectId: 3}
}).withBugTrackers({
  name: 'bugtracker',
  authProtocol: AuthenticationProtocol.BASIC_AUTH
}).build();


describe('Requirement View - Issues', () => {
  it('should display connection page if user not connected to bugtracker', () => {
    const testCaseViewPage = navigateToRequirementVersion();
    const issuePage = testCaseViewPage.showIssuesWithoutBindingToBugTracker();
    const connectionDialog = issuePage.openConnectionDialog();
    connectionDialog.fillUserName('admin');
    connectionDialog.fillPassword('admin');
    connectionDialog.connection();
  });

  it('should display table issues', () => {
    const modelResponse = {
      'entityType': 'requirement-version',
      'bugTrackerStatus': 'AUTHENTICATED',
      'projectName': '["LELprojet","test","LELprojetclassique","LELclassique","Projet_Test_Arnaud"]',
      'projectId': 328,
      'delete': '',
      'oslc': false
    };
    const requirementVersionViewPage = navigateToRequirementVersion();
    const gridResponse = {
      dataRows: [{
        id: 1,
        data: {
          remoteId: 'LE-01',
          btProject: 'Project bt',
          summary: 'summary',
          priority: '4',
          status: 'OK',
          assignee: 'Paul',
          verifiedRequirementVersions: [
            {id: 1, name: 'Requirement 2'},
          ],
        }
      }]
    } as unknown as GridResponse;
    const issuePage = requirementVersionViewPage.showIssuesIfBindedToBugTracker(modelResponse, gridResponse);
    const grid = issuePage.issueGrid;
    const row = grid.getRow(1);
    row.cell('remoteId').linkRenderer().assertContainText('LE-01');
    row.cell('btProject').textRenderer().assertContainText('Project bt');
    row.cell('summary').textRenderer().assertContainText('summary');
    row.cell('priority').textRenderer().assertContainText('4');
    row.cell('status').textRenderer().assertContainText('OK');
    row.cell('assignee').textRenderer().assertContainText('Paul');
    row.cell('verifiedRequirementVersions').linkRenderer().assertContainText('Requirement 2');
  });

  function navigateToRequirementVersion(): RequirementVersionViewPage {
    const initialNodes: GridResponse = {
      count: 1,
      dataRows: [{
        id: 'RequirementLibrary-1',
        children: ['Requirement-3'],
        data: {'NAME': 'Project_Issues'},
        state: DataRowOpenState.open
      } as unknown as DataRow,
        {
          id: 'Requirement-3',
          children: [],
          parentRowId: 'RequirementLibrary-1',
          state: DataRowOpenState.leaf,
          data: {
            'RLN_ID': 3,
            'CHILD_COUNT': 0,
            'NAME': 'M4 - Build Cupola',
            CRITICALITY: 'CRITICAL',
            REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
            HAS_DESCRIPTION: true,
            REQ_CATEGORY_ICON: 'briefcase',
            REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
            REQ_CATEGORY_TYPE: 'SYS',
            COVERAGE_COUNT: 0,
            IS_SYNCHRONIZED: false
          }
        } as unknown as DataRow]
    };
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, referentialData);
    new NavBarElement().toggle();
    const tree = requirementWorkspacePage.tree;
    const requirementPage: RequirementViewPage = tree.selectNode('Requirement-3', {...req3Model});
    requirementPage.toggleTree();
    return requirementPage.currentVersion;
  }

  const req3Model: RequirementVersionModel = {
    id: 3,
    projectId: 1,
    name: 'Build Cupola',
    reference: 'M4',
    attachmentList: {id: 1, attachments: []},
    customFieldValues: [],
    category: 2,
    createdBy: 'admin',
    createdOn: new Date('2020-09-30 10:30'),
    criticality: 'MAJOR',
    description: '',
    lastModifiedBy: '',
    lastModifiedOn: null,
    milestones: [],
    requirementId: 3,
    status: 'UNDER_REVIEW',
    versionNumber: 1,
    bindableMilestones: [],
    verifyingTestCases: [],
    requirementVersionLinks: [],
    requirementStats: {
      children: {
        allTestCaseCount: 0,
        executedTestCase: 0,
        plannedTestCase: 0,
        verifiedTestCase: 0,
        redactedTestCase: 0,
        validatedTestCases: 0

      },
      total: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1

      },
      currentVersion: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1

      },
      haveChildren: false,
    },
    remoteReqPerimeterStatus: null
  };
});

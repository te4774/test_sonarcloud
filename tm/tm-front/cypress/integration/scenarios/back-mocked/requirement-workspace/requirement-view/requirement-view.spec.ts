import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
// tslint:disable-next-line:max-line-length
import {
  RequirementWorkspacePage
} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {
  LinkedHighLevelRequirement,
  RequirementVersionModel
} from '../../../../model/requirements/requirement-version.model';
import {
  RequirementViewPage
} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {defaultReferentialData} from '../../../../utils/referential/default-referential-data.const';
// tslint:disable-next-line:max-line-length
import {
  RequirementVersionViewInformationPage
} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view-information.page';
// tslint:disable-next-line:max-line-length
import {mockRequirementVersionModel} from '../../../../data-mock/requirements.data-mock';
import {mockMilestoneModel} from '../../../../data-mock/milestone.data-mock';


function getRequirementLibraryChildNodes() {
    return [
        {
            id: 'RequirementLibrary-1',
            children: ['RequirementFolder-1', 'Requirement-3', 'RequirementFolder-2', 'HighLevelRequirement-1', 'HighLevelRequirement-2', 'Requirement-4'],
            data: {'NAME': 'International Space Station', 'CHILD_COUNT': '6'},
            state: DataRowOpenState.open,
            projectId: 1
        } as unknown as DataRow,
        {
            id: 'RequirementFolder-1',
            children: [],
            parentRowId: 'RequirementLibrary-1',
            data: {'NAME': 'Structural Requirements'},
            state: DataRowOpenState.closed,
            projectId: 1
        } as unknown as DataRow,
        {
            id: 'Requirement-3',
            children: [],
            parentRowId: 'RequirementLibrary-1',
            state: DataRowOpenState.leaf,
            data: {
                'RLN_ID': 3,
                'CHILD_COUNT': 0,
                'NAME': 'M4 - Build Cupola',
                CRITICALITY: 'CRITICAL',
                REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
                HAS_DESCRIPTION: true,
                REQ_CATEGORY_ICON: 'briefcase',
                REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
                REQ_CATEGORY_TYPE: 'SYS',
                COVERAGE_COUNT: 0,
                IS_SYNCHRONIZED: false
            },
            projectId: 1
        } as unknown as DataRow,
        {
            id: 'Requirement-4',
            children: ['Requirement-5'],
            parentRowId: 'RequirementLibrary-1',
            state: DataRowOpenState.open,
            data: {
                'RLN_ID': 4,
                'CHILD_COUNT': 1,
                'NAME': 'Requirement 4',
                CRITICALITY: 'CRITICAL',
                REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
                HAS_DESCRIPTION: true,
                REQ_CATEGORY_ICON: 'briefcase',
                REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
                REQ_CATEGORY_TYPE: 'SYS',
                COVERAGE_COUNT: 0,
                IS_SYNCHRONIZED: false
            },
            projectId: 1
        } as unknown as DataRow,
        {
            id: 'Requirement-5',
            children: [],
            parentRowId: 'Requirement-4',
            state: DataRowOpenState.leaf,
            data: {
                'RLN_ID': 5,
                'CHILD_COUNT': 0,
                'NAME': 'Child Requirement 5',
                CRITICALITY: 'CRITICAL',
                REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
                HAS_DESCRIPTION: true,
                REQ_CATEGORY_ICON: 'briefcase',
                REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
                REQ_CATEGORY_TYPE: 'SYS',
                COVERAGE_COUNT: 0,
                IS_SYNCHRONIZED: false
            },
            projectId: 1
        } as unknown as DataRow,
        {
            id: 'RequirementFolder-2',
            children: [],
            parentRowId: 'RequirementLibrary-1',
            data: {'NAME': 'Functional Requirements'},
            state: DataRowOpenState.closed,
            projectId: 1
        } as unknown as DataRow,
        {
            id: 'HighLevelRequirement-1',
            children: [],
            state: DataRowOpenState.leaf,
            data: {
                RLN_ID: 1,
                projectId: 1,
                NAME: 'hl 001',
                HAS_DESCRIPTION: false,
                REFERENCE: '',
                CRITICALITY: 'MINOR',
                REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
                REQ_CATEGORY_ICON: 'indeterminate_checkbox_empty',
                REQ_CATEGORY_LABEL: 'requirement.category.CAT_UNDEFINED',
                REQ_CATEGORY_TYPE: 'SYS',
                CHILD_COUNT: 0,
                COVERAGE_COUNT: 0,
                IS_SYNCHRONIZED: false,
                REMOTE_REQ_PERIMETER_STATUS: null,
                REMOTE_SYNCHRONISATION_ID: null,
                BOUND_TO_BLOCKING_MILESTONE: false
            },
            projectId: 1,
            parentRowId: 'RequirementLibrary-1'
        } as unknown as DataRow,
        {
            id: 'HighLevelRequirement-2',
            children: [],
            state: DataRowOpenState.leaf,
            data: {
                RLN_ID: 2,
                projectId: 1,
                NAME: 'hl 002',
                HAS_DESCRIPTION: false,
                REFERENCE: '',
                CRITICALITY: 'MINOR',
                REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
                REQ_CATEGORY_ICON: 'indeterminate_checkbox_empty',
                REQ_CATEGORY_LABEL: 'requirement.category.CAT_UNDEFINED',
                REQ_CATEGORY_TYPE: 'SYS',
                CHILD_COUNT: 0,
                COVERAGE_COUNT: 0,
                IS_SYNCHRONIZED: false,
                REMOTE_REQ_PERIMETER_STATUS: null,
                REMOTE_SYNCHRONISATION_ID: null,
                BOUND_TO_BLOCKING_MILESTONE: false
            },
            projectId: 1,
            parentRowId: 'RequirementLibrary-1'
        } as unknown as DataRow
    ];
}

function updateRowData(beforeUpdateDataRow: DataRow, dataProperty: string, value: string): DataRow {
    const data = {...beforeUpdateDataRow.data};
    data[dataProperty] = value;
    return {
        ...beforeUpdateDataRow,
        data
    };
}

describe('Requirement View Display', function () {

    const initialNodes: GridResponse = {
        count: 1,
        dataRows: [
            {
                id: 'RequirementLibrary-1',
                children: [],
                data: {'NAME': 'International Space Station', 'CHILD_COUNT': '3'},
                projectId: 1
            } as unknown as DataRow,
            {
                id: 'RequirementLibrary-2',
                children: [],
                data: {'NAME': 'STS - Shuttle', 'CHILD_COUNT': '3'},
                projectId: 2
            } as unknown as DataRow
        ]
    };

    it('should display a requirement', () => {

        const requirementPage = initAtRequirementViewPage(initReq3Model());
        const currentVersion = requirementPage.currentVersion;
        currentVersion.assertExist();
        currentVersion.assertNameContains('Build Cupola');
        currentVersion.assertReferenceContains('M4');
        const informationPage: RequirementVersionViewInformationPage = currentVersion.clickAnchorLink<RequirementVersionViewInformationPage>('information');
        informationPage.assertIsLowLevelRequirement();
    });

    it('should change information of requirement version', () => {
        let initialReqRowForTest = initialReqRow;
        const requirementPage = initAtRequirementViewPage(initReq3Model());
        const currentVersion = requirementPage.currentVersion;
        const informationPage = currentVersion.clickAnchorLink('information') as RequirementVersionViewInformationPage;

        initialReqRowForTest = updateRowData(initialReqRowForTest, 'NAME', 'Rebuild Cupola');
        informationPage.rename('Rebuild Cupola', {dataRows: [initialReqRowForTest]});
        currentVersion.assertNameContains('Rebuild Cupola');
        informationPage.changeCategory('Métier');
        informationPage.changeCriticality('Mineure');
        informationPage.bindMilestone(1, []);
        const milestoneTagElement = informationPage.milestoneTagElement;
        milestoneTagElement.checkMilestones('Milestone');
        informationPage.changeDescription('Ma nouvelle description');
        informationPage.descriptionElement.checkTextContent('Ma nouvelle description');

        initialReqRowForTest = updateRowData(initialReqRowForTest, 'REQUIREMENT_STATUS', 'APPROVED');
        informationPage.changeStatus('Approuvée', {dataRows: [initialReqRowForTest]});

    });

    it('should bind to high level requirement', () => {
        const requirementPage = initAtRequirementViewPage(initReq3Model(), true);
        const currentVersion = requirementPage.currentVersion;
        const informationPage = currentVersion.clickAnchorLink('information') as RequirementVersionViewInformationPage;

        const dialog = informationPage.openBindHighLevelRequirementSelectorDialog(initialNodes);
        const tree = dialog.requirementTree;
        const firstNode = initialNodes.dataRows[0];
        tree.openNode(firstNode.id, getRequirementLibraryChildNodes());
        tree.pickNode('HighLevelRequirement-1');
        dialog.confirm(linkedHighLevelRequirement1);
        informationPage.checkHighLevelRequirementName('hl 001');
    });

    it('should replace bound high level requirement', () => {
        const requirementPage = initAtRequirementViewPage(initReq3Model(linkedHighLevelRequirement1), true);
        const currentVersion = requirementPage.currentVersion;
        const informationPage = currentVersion.clickAnchorLink('information') as RequirementVersionViewInformationPage;

        const dialog = informationPage.openChangeHighLevelRequirementSelectorDialog(initialNodes);
        const tree = dialog.requirementTree;
        const firstNode = initialNodes.dataRows[0];
        tree.openNode(firstNode.id, getRequirementLibraryChildNodes());
        tree.pickNode('HighLevelRequirement-2');
        dialog.confirm(linkedHighLevelRequirement2);
        informationPage.checkHighLevelRequirementName('hl 002');
    });

    it('should display disclaimer for user without premium plugin', () => {
        const requirementPage = initAtRequirementViewPage(initReq3Model());
        const currentVersion = requirementPage.currentVersion;
        const informationPage = currentVersion.clickAnchorLink('information') as RequirementVersionViewInformationPage;

        informationPage.openBindHighLevelRequirementSelectorDialog(initialNodes);
        informationPage.checkPremiumPluginDisclaimerMessageExists();
    });

    it('should not display disclaimer for user without premium plugin', () => {
        const requirementPage = initAtRequirementViewPage(initReq3Model(linkedHighLevelRequirement1), true);
        const currentVersion = requirementPage.currentVersion;
        const informationPage = currentVersion.clickAnchorLink('information') as RequirementVersionViewInformationPage;

        informationPage.openChangeHighLevelRequirementSelectorDialog(initialNodes);
        informationPage.checkPremiumPluginDisclaimerMessageDoesNotExist();
    });

    it('should unbind to high level requirement', () => {
        const requirementPage = initAtRequirementViewPage(initReq3Model(linkedHighLevelRequirement1));
        const currentVersion = requirementPage.currentVersion;
        const informationPage = currentVersion.clickAnchorLink('information') as RequirementVersionViewInformationPage;

        informationPage.unlinkHighLevelRequirementSelectorDialog();
        informationPage.checkHighLevelRequirementNameDoesNotExist();
    });

    it('should not display high level requirement selector if requirement is child', () => {
        const requirementPage = initAtRequirementViewPage(initReq5Model());
        const currentVersion = requirementPage.currentVersion;
        const informationPage = currentVersion.clickAnchorLink('information') as RequirementVersionViewInformationPage;

        informationPage.assertBindHighLevelRequirementSelectorButtonsDoNotExist();
    });

    function initAtRequirementViewPage(requirementVersionModel: RequirementVersionModel,
                                       premiumPluginInstalled?: boolean): RequirementViewPage {
        const firstNode = initialNodes.dataRows[0];
        const referentialData = {...defaultReferentialData};
        referentialData.premiumPluginInstalled = premiumPluginInstalled;
        const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, referentialData);
        new NavBarElement().toggle();
        const tree = requirementWorkspacePage.tree;
        tree.assertNodeExist(firstNode.id);
        tree.assertNodeTextContains(firstNode.id, firstNode.data['NAME']);
        tree.openNode(firstNode.id, getRequirementLibraryChildNodes());
        tree.assertNodeIsOpen(firstNode.id);
        const requirementPage: RequirementViewPage =
            tree.selectNode(`Requirement-${requirementVersionModel.id}`, requirementVersionModel);
        requirementPage.assertExist();
        return requirementPage;
    }

});


const initialReqRow: DataRow = {
    'id': 'Requirement-3',
    'children': [],
    'state': 'leaf',
    'data': {
        'RLN_ID': 3,
        'NAME': 'M4 - Build Cupola',
        'HAS_DESCRIPTION': true,
        'REFERENCE': 'M4',
        'CRITICALITY': 'CRITICAL',
        'REQUIREMENT_STATUS': 'WORK_IN_PROGRESS',
        'REQ_CATEGORY_ICON': 'briefcase',
        'REQ_CATEGORY_LABEL': 'requirement.category.CAT_BUSINESS',
        'REQ_CATEGORY_TYPE': 'SYS',
        'CHILD_COUNT': 0,
        'COVERAGE_COUNT': 0,
        'IS_SYNCHRONIZED': false
    }
} as unknown as DataRow;

const linkedHighLevelRequirement1: LinkedHighLevelRequirement = {
    name: 'hl 001',
    reference: null,
    requirementId: 1,
    requirementVersionId: 1
}

const linkedHighLevelRequirement2: LinkedHighLevelRequirement = {
    name: 'hl 002',
    reference: null,
    requirementId: 2,
    requirementVersionId: 1
}

function initReq3Model(linkedHighLevelRequirement?: LinkedHighLevelRequirement): RequirementVersionModel {
    return mockRequirementVersionModel({
        id: 3,
        projectId: 1,
        name: 'Build Cupola',
        reference: 'M4',
        category: 2,
        createdBy: 'admin',
        createdOn: new Date('2020-09-30 10:30'),
        criticality: 'MAJOR',
        requirementId: 3,
        status: 'UNDER_REVIEW',
        versionNumber: 1,
        bindableMilestones: [
            mockMilestoneModel({
                label: 'Milestone',
                endDate: new Date('2020-09-30 10:30'),
                createdOn: new Date('2020-09-30 10:30'),
                lastModifiedOn: new Date('2020-09-30 10:30'),
            })
        ],
        requirementStats: {
            children: {
                allTestCaseCount: 0,
                executedTestCase: 0,
                plannedTestCase: 0,
                verifiedTestCase: 0,
                redactedTestCase: 0,
                validatedTestCases: 0

            },
            total: {
                allTestCaseCount: 5,
                executedTestCase: 2,
                plannedTestCase: 2,
                verifiedTestCase: 1,
                redactedTestCase: 2,
                validatedTestCases: 1

            },
            currentVersion: {
                allTestCaseCount: 5,
                executedTestCase: 2,
                plannedTestCase: 2,
                verifiedTestCase: 1,
                redactedTestCase: 2,
                validatedTestCases: 1

            },
            haveChildren: false,
        },
        remoteReqPerimeterStatus: null,
        linkedHighLevelRequirement: linkedHighLevelRequirement ? linkedHighLevelRequirement : null,
        childOfRequirement: false
    });
}


function initReq5Model(): RequirementVersionModel {
    return mockRequirementVersionModel({
        id: 5,
        projectId: 1,
        name: 'Child Requirement 5',
        reference: '',
        category: 2,
        createdBy: 'admin',
        createdOn: new Date('2020-09-30 10:30'),
        criticality: 'MAJOR',
        requirementId: 5,
        status: 'UNDER_REVIEW',
        versionNumber: 1,
        bindableMilestones: null,
        requirementStats: {
            children: {
                allTestCaseCount: 0,
                executedTestCase: 0,
                plannedTestCase: 0,
                verifiedTestCase: 0,
                redactedTestCase: 0,
                validatedTestCases: 0

            },
            total: {
                allTestCaseCount: 5,
                executedTestCase: 2,
                plannedTestCase: 2,
                verifiedTestCase: 1,
                redactedTestCase: 2,
                validatedTestCases: 1

            },
            currentVersion: {
                allTestCaseCount: 5,
                executedTestCase: 2,
                plannedTestCase: 2,
                verifiedTestCase: 1,
                redactedTestCase: 2,
                validatedTestCases: 1

            },
            haveChildren: false,
        },
        remoteReqPerimeterStatus: null,
        linkedHighLevelRequirement: null,
        childOfRequirement: true
    });
}

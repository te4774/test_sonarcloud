import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {
  RequirementWorkspacePage
} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {RequirementVersionModel} from '../../../../model/requirements/requirement-version.model';
import {
  RequirementViewPage
} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-view.page';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  RequirementVersionViewInformationPage
} from '../../../../page-objects/pages/requirement-workspace/requirement/requirement-version-view-information.page';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';

describe('Requirement Multi Version View Display', function () {
  const initialNodes: GridResponse = {
    count: 2,
    dataRows: [
      {
        id: 'RequirementLibrary-1',
        children: ['Requirement-3'],
        data: {'NAME': 'International Space Station', 'CHILD_COUNT': '3'},
        state: DataRowOpenState.open
      } as unknown as DataRow,
      {
        id: 'Requirement-3',
        children: [],
        parentRowId: 'RequirementLibrary-1',
        state: 'leaf',
        data: {
          'RLN_ID': 3,
          'NAME': 'M4 - Build Cupola',
          'HAS_DESCRIPTION': true,
          'REFERENCE': 'M4',
          'CRITICALITY': 'CRITICAL',
          'REQUIREMENT_STATUS': 'WORK_IN_PROGRESS',
          'REQ_CATEGORY_ICON': 'briefcase',
          'REQ_CATEGORY_LABEL': 'requirement.category.CAT_BUSINESS',
          'REQ_CATEGORY_TYPE': 'SYS',
          'CHILD_COUNT': 0,
          'COVERAGE_COUNT': 0,
          'IS_SYNCHRONIZED': false
        }
      } as unknown as DataRow
    ]
  };

  const req3Model: RequirementVersionModel = {
    id: 3,
    projectId: 1,
    name: 'Build Cupola',
    reference: 'M4',
    attachmentList: {id: 1, attachments: []},
    customFieldValues: [],
    category: 2,
    createdBy: 'admin',
    createdOn: new Date('2020-09-30 10:30'),
    criticality: 'MAJOR',
    description: '',
    lastModifiedBy: '',
    lastModifiedOn: null,
    milestones: [],
    requirementId: 3,
    status: 'UNDER_REVIEW',
    versionNumber: 1,
    bindableMilestones: [],
    verifyingTestCases: [],
    requirementVersionLinks: [],
    requirementStats: {
      children: {
        allTestCaseCount: 0,
        executedTestCase: 0,
        plannedTestCase: 0,
        verifiedTestCase: 0,
        redactedTestCase: 0,
        validatedTestCases: 0

      },
      total: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1

      },
      currentVersion: {
        allTestCaseCount: 5,
        executedTestCase: 2,
        plannedTestCase: 2,
        verifiedTestCase: 1,
        redactedTestCase: 2,
        validatedTestCases: 1

      },
      haveChildren: false,
    },
    childOfRequirement: false,
    lowLevelRequirements: [],
    linkedHighLevelRequirement: null,
    highLevelRequirement: null,
    remoteReqPerimeterStatus: 'UNKNOWN'
  };

  const gridResponse = {
    count: 2, dataRows: [
      {
        id: 2, data: {
          projectId: 1,
          rlnId: 1,
          resId: 2,
          versionNumber: 2,
          reference: '002',
          name: 'Version 2',
          category: 2,
          criticality: 'CRITICAL',
          status: 'WORK_IN_PROGRESS',
          links: 4,
          coverages: 2,
          milestoneMinDate: null,
          milestoneMaxDate: null,
          milestoneLabels: ''
        },
        projectId: 1
      } as unknown as DataRow,
      {
        id: 1, data: {
          projectId: 1,
          rlnId: 1,
          resId: 1,
          versionNumber: 1,
          reference: '001',
          name: 'Version 1',
          category: 2,
          criticality: 'CRITICAL',
          status: 'WORK_IN_PROGRESS',
          links: 0,
          coverages: 1,
          milestoneMinDate: null,
          milestoneMaxDate: null,
          milestoneLabels: ''
        },
        projectId: 1
      } as unknown as DataRow
    ]
  } as GridResponse;

  it('should display grid with versions of requirement', () => {
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
    new NavBarElement().toggle();
    const requirementPage: RequirementViewPage = requirementWorkspacePage.tree.selectNode('Requirement-3', {...req3Model});
    const currentVersion = requirementPage.currentVersion;
    currentVersion.toggleTree();
    const informationPanel = currentVersion.clickAnchorLink('information') as RequirementVersionViewInformationPage;
    const requirementMultiVersionViewPage = informationPanel.clickOnVersionLink(gridResponse, createEntityReferentialData);
    const grid = requirementMultiVersionViewPage.grid;

    grid.assertRowCount(2);
    const row1 = grid.getRow(2);
    row1.cell('name').textRenderer().assertContainText('Version 2');

    const row2 = grid.getRow(1);
    row2.cell('name').textRenderer().assertContainText('Version 1');
  });


});

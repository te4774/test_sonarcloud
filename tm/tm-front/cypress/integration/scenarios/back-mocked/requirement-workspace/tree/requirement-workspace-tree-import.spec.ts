import {
  RequirementWorkspacePage
} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {ReferentialDataMockBuilder} from '../../../../utils/referential/referential-data-builder';
import {XlsReport} from '../../../../model/requirements/import/import-requirement.model';

function buildReferentialData() {
  return new ReferentialDataMockBuilder()
    .withUser({
      userId: 1,
      admin: true,
      username: 'admin',
      projectManager: false,
      functionalTester: false,
      automationProgrammer: false
    })
    .withProjects(
      {name: 'Apollo', label: 'Apollo'}, {name: 'Gemini', label: 'Gemini'}
    ).build();
}

describe('Requirement import', () => {
  function navigateToRequirementWorkspace() {
    const referentialDataMock = buildReferentialData();
    return RequirementWorkspacePage.initTestAtPage(initialNodes, referentialDataMock);
  }

  const xlsReport: XlsReport = {
    templateOk: {
      requirementVersionSuccesses: 1,
      requirementVersionWarnings: 0,
      requirementVersionFailures: 0,
      coverageSuccesses: 0,
      coverageWarnings: 0,
      coverageFailures: 0,
      reqlinksSuccesses: 0,
      reqlinksWarnings: 0,
      reqlinksFailures: 0,
      reportUrl: 'requirement/import-logs/requirement-import-log-9999.xls'
    },
    importFormatFailure: null
  };

  it('Should open import requirement dialog then close it with Cancel button', () => {
    const requirementWorkspacePage: RequirementWorkspacePage = navigateToRequirementWorkspace();
    requirementWorkspacePage.navBar.toggle();
    const importDialog = requirementWorkspacePage.treeMenu.openImportRequirementDialog();
    importDialog.assertExist();
    importDialog.clickCancel();
    importDialog.assertNotExist();
  });

  it('Should display wrong format error while simulate a requirement from a not-Excel-file', () => {
    const requirementWorkspacePage: RequirementWorkspacePage = navigateToRequirementWorkspace();
    requirementWorkspacePage.navBar.toggle();
    const importDialog = requirementWorkspacePage.treeMenu.openImportRequirementDialog();
    importDialog.assertExist();
    importDialog.chooseImportFile('requirement_import.pdf', 'application/pdf');
    importDialog.clickSimulateWithoutSendingRequest();
    importDialog.assertErrorMessageContent('Veuillez sélectionner un fichier Excel à importer xls,xlsx,xlsm');
  });

  it('Should display wrong format error while import a requirement from a not-Excel-file', () => {
    const requirementWorkspacePage: RequirementWorkspacePage = navigateToRequirementWorkspace();
    requirementWorkspacePage.navBar.toggle();
    const importDialog = requirementWorkspacePage.treeMenu.openImportRequirementDialog();
    importDialog.assertExist();
    importDialog.chooseImportFile('requirement_import.pdf', 'application/pdf');
    importDialog.clickImport();
    importDialog.assertErrorMessageContent('Veuillez sélectionner un fichier Excel à importer xls,xlsx,xlsm');
  });

  it('Should simulate then confirm importing a requirement from Excel file', () => {
    const requirementWorkspacePage: RequirementWorkspacePage = navigateToRequirementWorkspace();
    requirementWorkspacePage.navBar.toggle();
    const importDialog = requirementWorkspacePage.treeMenu.openImportRequirementDialog();
    importDialog.assertExist();
    importDialog.chooseImportFile('requirement_import.xls', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    importDialog.clickSimulate(xlsReport);
    importDialog.assertSimulationTableContent();
    importDialog.assertBilanLinkName('Télécharger le bilan (xls)');
    importDialog.assertBilanLinkRef('/backend/requirement/import-logs/requirement-import-log-9999.xls');
    importDialog.clickImport();
    importDialog.assertConfirmImportTableContent('requirement_import.xls');
    importDialog.clickConfirmAfterSimulate({dataRows: []} as GridResponse);
    importDialog.assertReportImportTableContent();
    importDialog.close();
  });

  it('Should import a requirement from Excel file', () => {
    const requirementWorkspacePage: RequirementWorkspacePage = navigateToRequirementWorkspace();
    requirementWorkspacePage.navBar.toggle();
    const importDialog = requirementWorkspacePage.treeMenu.openImportRequirementDialog();
    importDialog.assertExist();
    importDialog.chooseImportFile('requirement_import.xls', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    importDialog.clickImport();
    importDialog.assertConfirmImportTableContent('requirement_import.xls');
    importDialog.clickConfirm({dataRows: []} as GridResponse, xlsReport);
    importDialog.assertReportImportTableContent();
    importDialog.assertBilanLinkName('Télécharger le bilan (xls)');
    importDialog.assertBilanLinkRef('/backend/requirement/import-logs/requirement-import-log-9999.xls');
    importDialog.close();
  });

  it('should display Error Import report', () => {
    const requirementWorkspacePage: RequirementWorkspacePage = navigateToRequirementWorkspace();
    requirementWorkspacePage.navBar.toggle();
    const importDialog = requirementWorkspacePage.treeMenu.openImportRequirementDialog();
    importDialog.chooseImportFile('requirement_import.xls', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    importDialog.clickImport();
    importDialog.assertConfirmImportTableContent('requirement_import.xls');

    const errorXlsReport: XlsReport = {
      templateOk: null,
      importFormatFailure: {
        duplicateColumns: ['REQUIREMENT_VERSION_NAME'],
        actionValidationError: null,
        missingMandatoryColumns: ['PROJECT_NAME']
      }
    };

    importDialog.clickConfirm({dataRows: []} as GridResponse, errorXlsReport);
    importDialog.assertReportExist('xls-report-ko');
    importDialog.assertButtonExist('cancel');
  });

  const initialNodes: GridResponse = {
    count: 4,
    dataRows: [
      {
        id: 'RequirementLibrary-1',
        children: [],
        data: {'NAME': 'International Space Station', 'CHILD_COUNT': '1'}
      } as unknown as DataRow
    ]
  };
});

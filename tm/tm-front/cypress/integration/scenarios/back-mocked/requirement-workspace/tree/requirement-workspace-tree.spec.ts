import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {
  RequirementWorkspacePage
} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';
import {createEntityReferentialData} from '../../../../utils/referential/create-entity-referential.const';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {getSimpleRequirementLibraryChildNodes} from '../../../../data-mock/requirement-tree.mock';

function getRequirementFolder1ChildNodes() {
  return [
    {
      id: 'RequirementFolder-1',
      parentRowId: 'RequirementLibrary-1',
      data: {'NAME': 'Structural Requirements'},
      children: ['Requirement-4', 'Requirement-5'],
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'Requirement-4',
      children: [],
      parentRowId: 'RequirementFolder-1',
      state: DataRowOpenState.leaf,
      data: {
        'RLN_ID': 4,
        'CHILD_COUNT': 0,
        'NAME': 'Build Soyouz deck',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false
      }
    } as unknown as DataRow,
    {
      id: 'Requirement-5',
      children: [],
      parentRowId: 'RequirementFolder-1',
      state: DataRowOpenState.closed,
      data: {
        'RLN_ID': 4,
        'CHILD_COUNT': 1,
        'NAME': 'Build modules',
        CRITICALITY: 'MAJOR',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false
      }
    } as unknown as DataRow,
  ];
}

function getRequirement5ChildNodes() {
  return [
    {
      id: 'Requirement-5',
      children: ['Requirement-6', 'Requirement-7'],
      parentRowId: 'RequirementFolder-1',
      state: DataRowOpenState.open,
      data: {
        'RLN_ID': 4,
        'CHILD_COUNT': 1,
        'NAME': 'Build modules',
        CRITICALITY: 'MAJOR',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false
      }
    } as unknown as DataRow,
    {
      id: 'Requirement-6',
      children: [],
      parentRowId: 'Requirement-5',
      state: DataRowOpenState.leaf,
      data: {
        'RLN_ID': 5,
        'CHILD_COUNT': 0,
        'NAME': 'Build Central Module',
        CRITICALITY: 'MAJOR',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false
      }
    } as unknown as DataRow,
    {
      id: 'Requirement-7',
      children: [],
      parentRowId: 'Requirement-5',
      state: DataRowOpenState.leaf,
      data: {
        'RLN_ID': 5,
        'CHILD_COUNT': 0,
        'NAME': 'Build Cupola',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'briefcase',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false
      }
    } as unknown as DataRow,
  ];
}

describe('Requirement Workspace Tree Display', function () {

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'RequirementLibrary-1',
      children: [],
      data: {'NAME': 'International Space Station', 'CHILD_COUNT': '3'}
    } as unknown as DataRow,
      {
        id: 'RequirementLibrary-2',
        children: [],
        data: {'NAME': 'STS - Shuttle', 'CHILD_COUNT': '3'}
      } as unknown as DataRow]
  };

  it('should display a simple tree', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes);
    const tree = requirementWorkspacePage.tree;
    tree.assertNodeExist(firstNode.id);
    tree.assertNodeTextContains(firstNode.id, firstNode.data['NAME']);
  });

  it('should open and close various nodes', () => {
    const childNodes = getSimpleRequirementLibraryChildNodes();
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes);
    const tree = requirementWorkspacePage.tree;
    tree.assertNodeExist('RequirementLibrary-1');
    tree.assertNodeTextContains('RequirementLibrary-1', 'International Space Station');
    tree.assertNodeExist('RequirementLibrary-2');
    tree.assertNodeTextContains('RequirementLibrary-2', 'STS - Shuttle');
    tree.assertNodeIsClosed('RequirementLibrary-1');
    tree.assertNodeIsClosed('RequirementLibrary-2');
    tree.openNode(firstNode.id, childNodes);
    tree.assertNodeIsOpen(firstNode.id);
    tree.assertNodeExist('RequirementLibrary-1');
    tree.assertNodeTextContains('RequirementLibrary-1', 'International Space Station');
    tree.assertNodeExist('RequirementFolder-1');
    tree.assertNodeIsClosed('RequirementFolder-1');
    tree.assertNodeTextContains('RequirementFolder-1', 'Structural Requirements');
    tree.assertNodeExist('Requirement-3');
    tree.assertNodeTextContains('Requirement-3', 'Find lot\'s of bucks');
    tree.assertNodeIsLeaf('Requirement-3');
    tree.assertNodeExist('RequirementFolder-2');
    tree.assertNodeTextContains('RequirementFolder-2', 'Functional Requirements');
    tree.assertNodeIsClosed('RequirementFolder-2');
    tree.openNode('RequirementFolder-1', getRequirementFolder1ChildNodes());
    tree.assertNodeIsOpen('RequirementFolder-1');
    tree.assertNodeExist('Requirement-4');
    tree.assertNodeTextContains('Requirement-4', 'Build Soyouz deck');
    tree.assertNodeIsLeaf('Requirement-4');
    tree.assertNodeExist('Requirement-5');
    tree.assertNodeTextContains('Requirement-5', 'Build modules');
    tree.assertNodeIsClosed('Requirement-5');
    tree.openNode('Requirement-5', getRequirement5ChildNodes());
    tree.assertNodeIsOpen('Requirement-5');
    tree.assertNodeExist('Requirement-6');
    tree.assertNodeTextContains('Requirement-6', 'Build Central Module');
    tree.assertNodeIsLeaf('Requirement-6');
    tree.assertNodeExist('Requirement-7');
    tree.assertNodeTextContains('Requirement-7', 'Build Cupola');
    tree.assertNodeIsLeaf('Requirement-7');
    tree.closeNode('Requirement-5');
    tree.assertNodeIsClosed('Requirement-5');
    tree.assertNodeNotExist('Requirement-6');
    tree.assertNodeNotExist('Requirement-7');
    tree.closeNode('RequirementFolder-1');
    tree.assertNodeNotExist('Requirement-4');
    tree.assertNodeNotExist('Requirement-5');
    tree.closeNode('RequirementLibrary-1');
    tree.assertNodeNotExist('Requirement-3');
    tree.assertNodeNotExist('RequirementFolder-1');
    tree.assertNodeNotExist('RequirementFolder-2');
  });
  //
  // it('should do various selections', () => {
  //   const childNodes = getChildNodes();
  //   const firstNode = initialNodes.dataRows[0];
  //   const testCaseWorkspacePage = TestCaseWorkspacePage.initTestAtPage(initialNodes);
  //   const tree = testCaseWorkspacePage.tree;
  //   tree.openNode(firstNode.id, childNodes);
  //   tree.assertNodeIsOpen(firstNode.id);
  //   tree.selectNode('TestCase-3');
  //   tree.assertNodeIsSelected('TestCase-3');
  //   tree.expendSelectionToNode('TestCaseFolder-2');
  //   tree.assertNodeIsSelected('TestCaseFolder-1');
  //   tree.assertNodeIsSelected('TestCaseFolder-2');
  //   tree.selectNode('TestCaseFolder-2');
  //   tree.expendSelectionToNode('TestCase-3');
  //   tree.assertNodeIsSelected('TestCase-3');
  //   tree.assertNodeIsSelected('TestCaseFolder-1');
  //   tree.assertNodeIsSelected('TestCaseFolder-2');
  //   tree.expendSelectionToNode('TestCaseFolder-1');
  //   tree.assertNodeIsNotSelected('TestCaseFolder-1');
  //   tree.assertNodeIsNotSelected('TestCase-3');
  //   tree.assertNodeIsSelected('TestCaseFolder-2');
  // });
});

describe('Requirement Workspace Add Nodes', function () {

  const openLib1 = {
    id: 'RequirementLibrary-1',
    children: ['RequirementFolder-1', 'Requirement-3', 'RequirementFolder-2'],
    data: {'NAME': 'International Space Station', 'CHILD_COUNT': '3'},
    state: DataRowOpenState.open,
    projectId: 1
  } as unknown as DataRow;
  const folder1 = {
    id: 'RequirementFolder-1',
    children: [],
    parentRowId: 'RequirementLibrary-1',
    data: {'NAME': 'Structural Requirements'},
    state: DataRowOpenState.closed,
    projectId: 1
  } as unknown as DataRow;
  const req3 = {
    id: 'Requirement-3',
    children: [],
    parentRowId: 'RequirementLibrary-1',
    state: DataRowOpenState.leaf,
    projectId: 1,
    data: {
      'RLN_ID': 3,
      'CHILD_COUNT': 0,
      'NAME': 'Find lot\'s of bucks',
      CRITICALITY: 'CRITICAL',
      REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
      HAS_DESCRIPTION: true,
      REQ_CATEGORY_ICON: 'briefcase',
      REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
      REQ_CATEGORY_TYPE: 'SYS',
      COVERAGE_COUNT: 0,
      IS_SYNCHRONIZED: false
    }
  } as unknown as DataRow;
  const folder2 = {
    id: 'RequirementFolder-2',
    children: [],
    parentRowId: 'RequirementLibrary-1',
    data: {'NAME': 'Functional Requirements'},
    state: DataRowOpenState.closed,
    projectId: 1
  } as unknown as DataRow;

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [
      {
        id: 'RequirementLibrary-1',
        children: [],
        data: {'NAME': 'International Space Station', 'CHILD_COUNT': '3'},
        projectId: 1
      } as unknown as DataRow,
      {
        id: 'RequirementLibrary-2',
        children: [],
        data: {'NAME': 'STS - Shuttle', 'CHILD_COUNT': '3'},
        projectId: 2
      } as unknown as DataRow
    ]
  };

  it('should show create buttons according to selection', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
    new NavBarElement().toggle();
    const tree = requirementWorkspacePage.tree;
    tree.assertNodeExist(firstNode.id);
    tree.assertNodeTextContains(firstNode.id, firstNode.data['NAME']);
    tree.openNode(firstNode.id, getSimpleRequirementLibraryChildNodes());
    requirementWorkspacePage.treeMenu.assertCreateButtonIsDisabled();
    tree.selectNode(firstNode.id);
    requirementWorkspacePage.treeMenu.assertCreateFolderButtonIsEnabled();
    requirementWorkspacePage.treeMenu.assertCreateRequirementButtonIsEnabled();
    requirementWorkspacePage.treeMenu.assertCreateHighLevelRequirementButtonIsEnabled();
    requirementWorkspacePage.treeMenu.hideCreateMenu();
    tree.selectNode('RequirementFolder-1');
    requirementWorkspacePage.treeMenu.assertCreateFolderButtonIsEnabled();
    requirementWorkspacePage.treeMenu.assertCreateRequirementButtonIsEnabled();
    requirementWorkspacePage.treeMenu.assertCreateHighLevelRequirementButtonIsEnabled();
    requirementWorkspacePage.treeMenu.hideCreateMenu();
    tree.selectNode('Requirement-3', {});
    requirementWorkspacePage.treeMenu.assertCreateFolderButtonIsDisabled();
    requirementWorkspacePage.treeMenu.assertCreateRequirementButtonIsEnabled();
    requirementWorkspacePage.treeMenu.assertCreateHighLevelRequirementButtonIsDisabled();
    requirementWorkspacePage.treeMenu.hideCreateMenu();
    // No perm no creation
    tree.selectNode('RequirementLibrary-2');
    requirementWorkspacePage.treeMenu.assertCreateButtonIsDisabled();
    requirementWorkspacePage.treeMenu.hideCreateMenu();
  });

  it('should create requirement folders', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
    new NavBarElement().toggle();
    const tree = requirementWorkspacePage.tree;
    tree.assertNodeExist(firstNode.id);
    tree.assertNodeTextContains(firstNode.id, firstNode.data['NAME']);
    tree.selectNode(firstNode.id);
    const createFolderDialog = requirementWorkspacePage.treeMenu.openCreateFolderDialog();
    createFolderDialog.clickOnAddButton();
    createFolderDialog.checkIfRequiredErrorMessageIsDisplayed();
    createFolderDialog.fillName('Other Requirements');
    const addedFolder = {
      id: 'RequirementFolder-4',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      data: {'NAME': 'Other Requirements'},
      state: DataRowOpenState.closed,
      projectId: 1
    } as unknown as DataRow;
    const refreshedLibrary = {...openLib1, children: [...openLib1.children, addedFolder.id]};
    const response = {dataRows: [refreshedLibrary, folder1, req3, folder2, addedFolder]};
    createFolderDialog.addWithOptions({
      addAnother: true,
      parentRowIsClosed: true,
      addedId: 4,
      createResponse: addedFolder,
      children: response,
      parentRowRef: 'RequirementLibrary-1',
      entityModel: {}
    });
    createFolderDialog.fillName('Regulatory Requirements');
    const anotherAddedFolder = {
      id: 'RequirementFolder-5',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      data: {'NAME': 'Regulatory Requirements'},
      state: DataRowOpenState.closed,
      projectId: 1
    } as unknown as DataRow;
    createFolderDialog.addWithOptions({
      addAnother: false,
      parentRowIsClosed: false,
      addedId: 5,
      createResponse: anotherAddedFolder,
      parentRowRef: 'RequirementLibrary-1',
      entityModel: {}
    });
    tree.assertNodeExist(addedFolder.id);
    tree.assertNodeTextContains(addedFolder.id, addedFolder.data['NAME']);
    tree.assertNodeExist(anotherAddedFolder.id);
    tree.assertNodeTextContains(anotherAddedFolder.id, anotherAddedFolder.data['NAME']);
    tree.assertNodeIsSelected(anotherAddedFolder.id);
  });

  it('should create requirements', () => {
    // absorbing all requests from current version, not the object of this test
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, createEntityReferentialData);
    new NavBarElement().toggle();
    const tree = requirementWorkspacePage.tree;
    tree.selectNode(firstNode.id);
    const createRequirementDialog = requirementWorkspacePage.treeMenu.openCreateRequirementDialog();
    createRequirementDialog.criticalitySelectBox.checkSelectedOption('Mineure');
    createRequirementDialog.categorySelectBox.checkSelectedOption('Non définie');
    createRequirementDialog.clickOnAddButton();
    createRequirementDialog.checkIfRequiredErrorMessageIsDisplayed();
    createRequirementDialog.fillName('Anti Collision System');
    createRequirementDialog.fillReference('ISS-12');
    createRequirementDialog.fillDescription('Avoid space junks using STS engines');
    createRequirementDialog.criticalitySelectBox.selectValue('Critique');
    createRequirementDialog.categorySelectBox.selectValue('Sécurité');

    const addedRequirement = {
      id: 'Requirement-4',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      projectId: 1,
      data: {
        'RLN_ID': 4,
        'CHILD_COUNT': 0,
        'NAME': 'Anti Collision System',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'protect',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_SECURITY',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false
      }
    } as unknown as DataRow;
    const refreshedLibrary = {...openLib1, children: [...openLib1.children, addedRequirement.id]};
    const response = {dataRows: [refreshedLibrary, folder1, req3, folder2, addedRequirement]};
    createRequirementDialog.addWithOptions({
      addAnother: true,
      parentRowIsClosed: true,
      addedId: 4,
      createResponse: addedRequirement,
      children: response,
      parentRowRef: 'RequirementLibrary-1',
      entityModel: {}
    });
    createRequirementDialog.criticalitySelectBox.checkSelectedOption('Mineure');
    createRequirementDialog.categorySelectBox.checkSelectedOption('Non définie');
    createRequirementDialog.checkIfFormIsEmpty(false);
    createRequirementDialog.fillName('Build Cupola');
    createRequirementDialog.fillReference('ISS-13');
    createRequirementDialog.fillDescription('Build a cupola to take nice photos');
    createRequirementDialog.criticalitySelectBox.selectValue('Majeure');
    createRequirementDialog.categorySelectBox.selectValue('Ergonomique');
    const anotherAddedRequirement = {
      id: 'Requirement-5',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      projectId: 1,
      data: {
        'RLN_ID': 4,
        'CHILD_COUNT': 0,
        'NAME': 'Build Cupola',
        CRITICALITY: 'MAJOR',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'puzzle',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_ERGONOMIC',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false
      }
    } as unknown as DataRow;
    createRequirementDialog.addWithOptions({
      addAnother: false,
      parentRowIsClosed: false,
      addedId: 5,
      createResponse: anotherAddedRequirement,
      parentRowRef: 'RequirementLibrary-1',
      entityModel: {}
    });
    tree.assertNodeExist(addedRequirement.id);
    tree.assertNodeTextContains(addedRequirement.id, addedRequirement.data['NAME']);
    tree.assertNodeExist(anotherAddedRequirement.id);
    tree.assertNodeTextContains(anotherAddedRequirement.id, anotherAddedRequirement.data['NAME']);
    tree.assertNodeIsSelected(anotherAddedRequirement.id);
  });

  it('should create high level requirements', () => {
    // absorbing all requests from current version, not the object of this test
    const firstNode = initialNodes.dataRows[0];
    const refData = createEntityReferentialData;
    refData.premiumPluginInstalled = true;
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, refData);
    new NavBarElement().toggle();
    const tree = requirementWorkspacePage.tree;
    tree.selectNode(firstNode.id);
    const createRequirementDialog = requirementWorkspacePage.treeMenu.openCreateHighLevelRequirementDialog();
    createRequirementDialog.criticalitySelectBox.checkSelectedOption('Mineure');
    createRequirementDialog.categorySelectBox.checkSelectedOption('Non définie');
    createRequirementDialog.clickOnAddButton();
    createRequirementDialog.checkIfRequiredErrorMessageIsDisplayed();
    createRequirementDialog.fillName('Anti Collision System');
    createRequirementDialog.fillReference('ISS-12');
    createRequirementDialog.fillDescription('Avoid space junks using STS engines');
    createRequirementDialog.criticalitySelectBox.selectValue('Critique');
    createRequirementDialog.categorySelectBox.selectValue('Sécurité');

    const addedRequirement = {
      id: 'HighLevelRequirement-4',
      children: [],
      parentRowId: 'RequirementLibrary-1',
      state: DataRowOpenState.leaf,
      projectId: 1,
      data: {
        'RLN_ID': 4,
        'CHILD_COUNT': 0,
        'NAME': 'Anti Collision System',
        CRITICALITY: 'CRITICAL',
        REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
        HAS_DESCRIPTION: true,
        REQ_CATEGORY_ICON: 'protect',
        REQ_CATEGORY_LABEL: 'requirement.category.CAT_SECURITY',
        REQ_CATEGORY_TYPE: 'SYS',
        COVERAGE_COUNT: 0,
        IS_SYNCHRONIZED: false,
        IS_HIGH_LEVEL: true
      }
    } as unknown as DataRow;
    const refreshedLibrary = {...openLib1, children: [...openLib1.children, addedRequirement.id]};
    const response = {dataRows: [refreshedLibrary, folder1, req3, folder2, addedRequirement]};
    createRequirementDialog.addWithOptions({
      addAnother: false,
      parentRowIsClosed: true,
      addedId: 4,
      createResponse: addedRequirement,
      children: response,
      parentRowRef: 'RequirementLibrary-1',
      entityModel: {}
    });
    tree.assertNodeExist(addedRequirement.id);
    tree.assertNodeTextContains(addedRequirement.id, addedRequirement.data['NAME']);
    tree.getRow(addedRequirement.id).cell('NAME').treeNodeRenderer().assertIsHighLevelRequirement();
  });
});

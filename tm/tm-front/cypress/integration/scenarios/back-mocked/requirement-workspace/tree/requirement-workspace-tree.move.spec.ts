import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {
  ALL_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';
import {NavBarElement} from '../../../../page-objects/elements/nav-bar/nav-bar.element';
import {
  RequirementWorkspacePage
} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';

describe('Requirement Workspace Tree Move', function () {

  function referentialData() {
    return new ReferentialDataMockBuilder()
      .withProjects({
          name: 'International Space Station',
          permissions: ALL_PROJECT_PERMISSIONS,
        },
        {
          name: 'STS - Shuttle',
          permissions: ALL_PROJECT_PERMISSIONS,
        })
      .build();
  }

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'RequirementLibrary-1',
      projectId: 1,
      children: [],
      data: {'NAME': 'International Space Station', 'CHILD_COUNT': '3'},
    } as unknown as DataRow,
      {
        id: 'RequirementLibrary-2',
        projectId: 2,
        children: [],
        data: {'NAME': 'STS - Shuttle', 'CHILD_COUNT': '3'},
      } as unknown as DataRow]
  };

  const reqLib1 = {
    id: 'RequirementLibrary-1',
    projectId: 1,
    children: ['RequirementFolder-1', 'Requirement-3', 'RequirementFolder-2'],
    data: {'NAME': 'International Space Station', 'CHILD_COUNT': '3'},
    state: DataRowOpenState.open
  } as unknown as DataRow;

  const reqFolder1 = {
    id: 'RequirementFolder-1',
    children: [],
    projectId: 1,
    parentRowId: 'RequirementLibrary-1',
    data: {'NAME': 'Structural Requirements'},
  } as unknown as DataRow;

  const req3 = {
    id: 'Requirement-3',
    children: [],
    parentRowId: 'RequirementLibrary-1',
    state: DataRowOpenState.leaf,
    projectId: 1,
    data: {
      'RLN_ID': 3,
      'CHILD_COUNT': 0,
      'NAME': 'Find lot\'s of bucks',
      CRITICALITY: 'CRITICAL',
      REQUIREMENT_STATUS: 'WORK_IN_PROGRESS',
      HAS_DESCRIPTION: true,
      REQ_CATEGORY_ICON: 'briefcase',
      REQ_CATEGORY_LABEL: 'requirement.category.CAT_BUSINESS',
      REQ_CATEGORY_TYPE: 'SYS',
      COVERAGE_COUNT: 0,
      IS_SYNCHRONIZED: false
    }
  } as unknown as DataRow;

  const reqFolder2 = {
    id: 'RequirementFolder-2',
    children: [],
    projectId: 1,
    parentRowId: 'RequirementLibrary-1',
    data: {'NAME': 'Functional Requirements'},
  } as unknown as DataRow;

  const libraryRefreshAtOpen = [
    reqLib1,
    reqFolder1,
    req3,
    reqFolder2
  ];

  it('should init move and show target according to destination', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    requirementWorkspacePage.treeMenu.sortTreePositional();
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop(reqFolder1.id);
    tree.assertNodeNotExist(reqFolder1.id);
    tree.dragOverTopPart(reqFolder2.id);
    tree.assertDragAndDropTargetIsVisible(2);
    tree.dragOverCenter(reqFolder2.id);
    tree.assertContainerIsDndTarget(reqFolder2.id);
    tree.dragOverBottomPart(reqFolder2.id);
    tree.assertDragAndDropTargetIsVisible(3);
    tree.cancelDnd();
    tree.assertNodeExist(reqFolder1.id);
  });

  it('should drop into folder', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    requirementWorkspacePage.treeMenu.sortTreePositional();
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop(reqFolder1.id);
    tree.assertNodeNotExist(reqFolder1.id);
    tree.dragOverCenter(reqFolder2.id);
    tree.assertContainerIsDndTarget(reqFolder2.id);
    const refreshedRows = [
      {...reqLib1, children: [req3.id, reqFolder2.id]},
      {...reqFolder1, parentRowId: reqFolder2.id},
      {...req3},
      {...reqFolder2, children: [reqFolder1.id], state: DataRowOpenState.open},
    ];
    tree.drop(reqFolder2.id, 'RequirementLibrary-1,RequirementFolder-2', refreshedRows);
    tree.assertRowHasParent(reqFolder1.id, reqFolder2.id);
  });


  it('should suspend drag and resume when coming back into tree', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    requirementWorkspacePage.treeMenu.sortTreePositional();
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop(reqFolder1.id);
    tree.assertNodeNotExist(reqFolder1.id);
    tree.dragOverTopPart(reqFolder2.id);
    tree.assertDragAndDropTargetIsVisible(2);
    tree.suspendDnd();
    tree.assertDragAndDropTargetIsNotVisible();
    tree.assertNodeExist(reqFolder1.id);
    tree.dragOverCenter(reqFolder2.id);
    tree.assertContainerIsDndTarget(reqFolder2.id);
    tree.assertNodeNotExist(reqFolder1.id);
    tree.suspendDnd();
    tree.assertNodeExist(reqFolder1.id);
    tree.assertContainerIsNotDndTarget(reqFolder2.id);
    tree.cancelDnd();
  });

  it('should show rows when dragging multiple rows', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes);
    new NavBarElement().toggle();
    requirementWorkspacePage.treeMenu.sortTreePositional();
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode(req3.id, {});
    tree.addNodeToSelection(reqFolder1.id, {});
    tree.beginDragAndDrop(req3.id);
    tree.assertNodeNotExist(req3.id);
    tree.assertNodeNotExist(reqFolder1.id);
    tree.assertDndPlaceholderContains(req3.id, 'Find lot\'s of bucks');
    tree.assertDndPlaceholderContains(reqFolder1.id, 'Structural Requirements');
  });

  it('should show warning when dropping into another project', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, referentialData());
    new NavBarElement().toggle();
    requirementWorkspacePage.treeMenu.sortTreePositional();
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.beginDragAndDrop(req3.id);
    tree.dragOverCenter('RequirementLibrary-2');
    const confirmInterProjectMove = tree.dropIntoOtherProject('RequirementLibrary-2');
    confirmInterProjectMove.assertExist();
    confirmInterProjectMove.cancel();
    tree.assertNodeExist(req3.id);
    tree.assertRowHasParent(req3.id, reqLib1.id);
  });

});

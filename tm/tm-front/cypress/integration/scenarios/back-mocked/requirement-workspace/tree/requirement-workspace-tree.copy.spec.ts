import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {
  ALL_PROJECT_PERMISSIONS,
  NO_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';
import {
  RequirementWorkspacePage
} from '../../../../page-objects/pages/requirement-workspace/requirement-workspace.page';

describe('Requirement Workspace Tree Copy', function () {

  function referentialData() {
    return new ReferentialDataMockBuilder()
      .withProjects({
          name: 'Project 1',
          permissions: ALL_PROJECT_PERMISSIONS,
        },
        {
          name: 'Project 2',
          permissions: NO_PROJECT_PERMISSIONS,
        })
      .build();
  }

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'RequirementLibrary-1',
      projectId: 1,
      children: [],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'}
    } as unknown as DataRow,
      {
        id: 'RequirementLibrary-2',
        projectId: 2,
        children: [],
        data: {'NAME': 'Project2', 'CHILD_COUNT': '1'}
      } as unknown as DataRow]
  };

  const libraryRefreshAtOpen = [
    {
      id: 'RequirementLibrary-1',
      projectId: 1,
      children: ['RequirementFolder-1', 'Requirement-3', 'RequirementFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'RequirementFolder-1',
      children: [],
      projectId: 1,
      parentRowId: 'RequirementLibrary-1',
      data: {'NAME': 'folder1'}
    } as unknown as DataRow,
    {
      id: 'Requirement-3',
      children: [],
      projectId: 1,
      parentRowId: 'RequirementLibrary-1',
      data: {
        'NAME': 'a nice requirement',
        'HAS_DESCRIPTION': true,
        'REQUIREMENT_STATUS': 'WORK_IN_PROGRESS',
        'CRITICALITY': 'MAJOR'
      }
    } as unknown as DataRow,
    {
      id: 'RequirementFolder-2',
      children: [],
      projectId: 1,
      parentRowId: 'RequirementLibrary-1',
      data: {'NAME': 'folder2'}
    } as unknown as DataRow
  ];

  it('should activate or deactivate copy button according to user selection', () => {

    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes);
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode(firstNode.id);
    requirementWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    requirementWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    // // the component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('RequirementFolder-1', {});
    requirementWorkspacePage.treeMenu.assertCopyButtonIsActive();
    requirementWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('Requirement-3', {});
    requirementWorkspacePage.treeMenu.assertCopyButtonIsActive();
    requirementWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('RequirementLibrary-1', {});
    requirementWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    requirementWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
  });

  it('should activate or deactivate paste button according to destination', () => {
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, referentialData());
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode(firstNode.id);
    requirementWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    requirementWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    // The component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('Requirement-3', {});
    requirementWorkspacePage.treeMenu.assertCopyButtonIsActive();
    requirementWorkspacePage.treeMenu.copy();
    requirementWorkspacePage.treeMenu.assertPasteButtonIsActive();
    tree.selectNode('RequirementFolder-1', {});
    requirementWorkspacePage.treeMenu.assertPasteButtonIsActive();
    tree.selectNode('RequirementLibrary-1', {});
    requirementWorkspacePage.treeMenu.assertPasteButtonIsActive();
    tree.selectNode('Requirement-3', {});
    requirementWorkspacePage.treeMenu.assertPasteButtonIsActive();
    // Testing permissions
    tree.selectNode('RequirementLibrary-2', {});
    requirementWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
  });

  it('should copy paste a node', () => {
    const refreshedNodes = [
      {
        id: 'RequirementFolder-1',
        projectId: 1,
        children: ['Requirement-4'],
        parentRowId: 'RequirementLibrary-1',
        state: DataRowOpenState.open,
        data: {'NAME': 'folder1', CHILD_COUNT: 1}
      } as unknown as DataRow,
      {
        id: 'Requirement-4',
        projectId: 1,
        children: [],
        parentRowId: 'RequirementFolder-1',
        data: {
          'NAME': 'a nice requirement',
          'HAS_DESCRIPTION': true,
          'REQUIREMENT_STATUS': 'WORK_IN_PROGRESS',
          'CRITICALITY': 'MAJOR'
        }
      } as unknown as DataRow,
    ];
    const firstNode = initialNodes.dataRows[0];
    const requirementWorkspacePage = RequirementWorkspacePage.initTestAtPage(initialNodes, referentialData());
    const tree = requirementWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode('Requirement-3', {});
    requirementWorkspacePage.treeMenu.copy();
    tree.selectNode('RequirementFolder-1', {});
    requirementWorkspacePage.treeMenu.paste({dataRows: refreshedNodes}, 'requirement-tree', 'RequirementFolder-1');
    tree.assertNodeExist('Requirement-4');
    tree.assertNodeIsOpen('RequirementFolder-1');
  });
});

import {DataRow, GridResponse} from 'cypress/integration/model/grids/data-row.type';
import {ExecutionPage} from '../../../page-objects/pages/execution/execution-page';
import {ExecutionModel} from '../../../model/execution/execution.model';
import {InputType} from '../../../model/customfield/customfield.model';
import {EditableDateFieldElement} from '../../../page-objects/elements/forms/editable-date-field.element';
import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';

const todayDate = new Date();

describe('Execution page - history', function () {

  const initialNodes: GridResponse = {
    count: 3,
    idAttribute: null,
    dataRows: [
      {
        id: 3,
        children: [],
        state: 'leaf',
        data: {
          executionReference: '',
          successRate: 100.0,
          importance: 'LOW',
          executionStatus: 'SUCCESS',
          datasetName: null,
          executionMode: 'MANUAL',
          itemTestPlanId: 1,
          issueCount: 0,
          executionOrder: 2,
          executionId: 3,
          executionName: 'Exec 3',
          boundToBlockingMilestone: false,
          lastExecutedOn: todayDate,
          iterationId: 1,
          projectId: 1,
          user: 'admin'
        },
        projectId: 1,
        parentRowId: null
      } as unknown as DataRow,
      {
        id: 2,
        children: [],
        state: 'leaf',
        data: {
          executionReference: '',
          successRate: 0.0,
          importance: 'LOW',
          executionStatus: 'FAILURE',
          datasetName: null,
          executionMode: 'MANUAL',
          itemTestPlanId: 1,
          issueCount: 0,
          executionOrder: 1,
          executionId: 2,
          executionName: 'Exec 2',
          boundToBlockingMilestone: false,
          lastExecutedOn: todayDate,
          iterationId: 1,
          projectId: 1,
          user: 'admin'
        },
        projectId: 1,
        parentRowId: null
      } as unknown as DataRow,
      {
        id: 1,
        children: [],
        state: 'leaf',
        data: {
          executionReference: '',
          successRate: 0.0,
          importance: 'LOW',
          executionStatus: 'READY',
          datasetName: null,
          executionMode: 'MANUAL',
          itemTestPlanId: 41,
          issueCount: 0,
          executionOrder: 0,
          executionId: 1,
          executionName: 'Exec 1',
          boundToBlockingMilestone: false,
          lastExecutedOn: null,
          iterationId: 1,
          projectId: 1,
          user: null
        },
        projectId: 1,
        parentRowId: null
      } as unknown as DataRow,
    ]
  };
  const oneDeleteNodes: GridResponse = {
    count: 3,
    idAttribute: null,
    dataRows: [
      {
        id: 3,
        children: [],
        state: 'leaf',
        data: {
          executionReference: '',
          successRate: 100.0,
          importance: 'LOW',
          executionStatus: 'SUCCESS',
          datasetName: null,
          executionMode: 'MANUAL',
          itemTestPlanId: 1,
          issueCount: 0,
          executionOrder: 1,
          executionId: 3,
          executionName: 'Exec 3',
          boundToBlockingMilestone: false,
          lastExecutedOn: todayDate,
          iterationId: 1,
          projectId: 1,
          user: 'admin'
        },
        projectId: 1,
        parentRowId: null
      } as unknown as DataRow,
      {
        id: 2,
        children: [],
        state: 'leaf',
        data: {
          executionReference: '',
          successRate: 0.0,
          importance: 'LOW',
          executionStatus: 'FAILURE',
          datasetName: null,
          executionMode: 'MANUAL',
          itemTestPlanId: 1,
          issueCount: 0,
          executionOrder: 0,
          executionId: 2,
          executionName: 'Exec 2',
          boundToBlockingMilestone: false,
          lastExecutedOn: todayDate,
          iterationId: 1,
          projectId: 1,
          user: 'admin'
        },
        projectId: 1,
        parentRowId: null
      } as unknown as DataRow,
    ]
  };
  const twoDeletesNodes: GridResponse = {
    count: 3,
    idAttribute: null,
    dataRows: [
      {
        id: 3,
        children: [],
        state: 'leaf',
        data: {
          executionReference: '',
          successRate: 100.0,
          importance: 'LOW',
          executionStatus: 'SUCCESS',
          datasetName: null,
          executionMode: 'MANUAL',
          itemTestPlanId: 1,
          issueCount: 0,
          executionOrder: 0,
          executionId: 3,
          executionName: 'Exec 3',
          boundToBlockingMilestone: false,
          lastExecutedOn: todayDate,
          iterationId: 1,
          projectId: 1,
          user: 'admin'
        },
        projectId: 1,
        parentRowId: null
      } as unknown as DataRow,
    ]
  };

  it('should display correct information', () => {
    const executionPage = ExecutionPage.initTestAtPage(3, getExecutionModel(), null, initialNodes);
    executionPage.clickAnchorLink('history');
    executionPage.historyPanel.assertExist();
    const grid = executionPage.historyGrid;
    new NavBarElement().toggle();

    grid.assertRowExist(2);
    const row = grid.getRow(2);
    row.cell('executionMode').iconRenderer().assertContainIcon('anticon-sqtm-core-administration:user');
    row.cell('importance').iconRenderer().assertContainIcon('anticon-sqtm-core-test-case:double_down');
    row.cell('user').textRenderer().assertContainText('admin');
    row.cell('lastExecutedOn').textRenderer()
      .assertContainText(EditableDateFieldElement.dateToDisplayString(todayDate));


    grid.assertRowExist(3);
    grid.getRow(3).cell('delete').iconRenderer().assertContainIcon('anticon-sqtm-core-generic:delete');
  });

  it('should delete an other execution', () => {
    const executionPage = ExecutionPage.initTestAtPage(3, getExecutionModel(), null, initialNodes);
    executionPage.clickAnchorLink('history');
    executionPage.historyPanel.assertExist();
    new NavBarElement().toggle();
    const grid = executionPage.historyGrid;
    executionPage.deleteExectuion(1, oneDeleteNodes);
    grid.assertRowCount(2);
  });

  it('should delete multiple executions', () => {
    const executionPage = ExecutionPage.initTestAtPage(3, getExecutionModel(), null, initialNodes);
    executionPage.clickAnchorLink('history');
    executionPage.historyPanel.assertExist();
    new NavBarElement().toggle();
    const grid = executionPage.historyGrid;
    executionPage.deleteMultipleExecutions([1, 2], twoDeletesNodes);
    grid.assertRowCount(1);
  });

});

function getExecutionModel(): ExecutionModel {
  return {
    id: 3,
    name: 'Exec 3',
    projectId: 1,
    testCaseId: 1,
    executionOrder: 2,
    prerequisite: '',
    tcNatLabel: 'test-case.nature.NAT_UNDEFINED',
    tcNatIconName: 'indeterminate_checkbox_empty',
    tcTypeLabel: 'test-case.type.TYP_UNDEFINED',
    tcTypeIconName: 'indeterminate_checkbox_empty',
    tcStatus: 'APPROVED',
    tcImportance: 'LOW',
    tcDescription: '',
    datasetLabel: null,
    comment: null,
    executionMode: 'MANUAL',
    lastExecutedOn: todayDate,
    lastExecutedBy: 'admin',
    executionStatus: 'SUCCESS',
    executionStepViews: [
      {
        id: 159,
        order: 0,
        executionStatus: 'SUCCESS',
        action: '',
        expectedResult: '',
        comment: '',
        attachmentList: {
          id: 542,
          attachments: []
        },
        denormalizedCustomFieldValues: [],
        customFieldValues: [],
        lastExecutedOn: todayDate,
        lastExecutedBy: 'admin',
        projectId: 2,
      }
    ],
    denormalizedCustomFieldValues: [
      {
        id: 1,
        label: 'dnz-cuf-1',
        inputType: InputType.PLAIN_TEXT,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: 'dnz cuf value 1'
      },
      {
        id: 2,
        label: 'dnz-cuf-2',
        inputType: InputType.DATE_PICKER,
        denormalizedFieldHolderId: 1,
        fieldType: 'CF',
        value: '2020-02-25'
      }
    ],
    customFieldValues: [],
    coverages: [],
    attachmentList: {
      id: 541,
      attachments: []
    },
    automatedExecutionResultUrl: null,
    automatedExecutionResultSummary: null,
    automatedJobUrl: null,
    nbIssues: 0,
    iterationId: 1,
    milestones: [],
    testPlanItemId: 1,
    executionsCount: 3,
    kind: 'STANDARD',
    testAutomationServerKind: null,
  };
}




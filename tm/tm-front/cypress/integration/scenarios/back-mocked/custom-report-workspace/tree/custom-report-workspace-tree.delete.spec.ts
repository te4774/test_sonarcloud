import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';
import {
  CustomReportWorkspacePage
} from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

describe('Custom Report Workspace Tree Delete', function () {

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: [],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'}
    } as unknown as DataRow,
      {
        id: 'CustomReportLibrary-2',
        projectId: 2,
        children: [],
        data: {'NAME': 'Project2', 'CHILD_COUNT': '1'}
      } as unknown as DataRow]
  };

  const libraryRefreshAtOpen = [
    {
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: ['CustomReportFolder-1', 'ChartDefinition-2', 'CustomReportFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'CustomReportFolder-1',
      children: [],
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
      data: {'NAME': 'folder1'}
    } as unknown as DataRow,
    {
      id: 'ChartDefinition-2',
      children: [],
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
      data: {'NAME': 'a nice chart'}
    } as unknown as DataRow,
    {
      id: 'CustomReportFolder-2',
      children: [],
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
      data: {'NAME': 'folder2'}
    } as unknown as DataRow
  ];

  it('should activate or deactivate delete button according to user selection', () => {
    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    customReportWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('CustomReportFolder-1');
    customReportWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    tree.selectNode('ChartDefinition-2');
    customReportWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    tree.selectNode('CustomReportLibrary-1');
    customReportWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
  });

  it('should show server warnings when deleting', () => {
    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    customReportWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('CustomReportFolder-1');
    customReportWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    const confirmDialog = customReportWorkspacePage.treeMenu.initDeletion('custom-report-tree', [1], ['warning_called']);
    confirmDialog.checkWarningMessages(['warning_called']);
  });

  it('should delete node and remove it from tree', () => {
    const libRefreshed = {
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: ['CustomReportFolder-1', 'ChartDefinition-2', 'CustomReportFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
      state: DataRowOpenState.open
    } as unknown as DataRow;
    const crf1 = {
      id: 'CustomReportFolder-1',
      children: [],
      data: {'NAME': 'folder1'},
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
    } as unknown as DataRow;
    const c = {
      id: 'ChartDefinition-2',
      children: [],
      data: {'NAME': 'a nice chart'},
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
    } as unknown as DataRow;
    const crf2 = {
      id: 'CustomReportFolder-2',
      children: [],
      data: {'NAME': 'folder2'},
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
    } as unknown as DataRow;
    const childNodes = [
      libRefreshed,
      crf1,
      c,
      crf2
    ];
    const firstNode = initialNodes.dataRows[0];
    const campaignWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    const tree = campaignWorkspacePage.tree;
    tree.openNode(firstNode.id, childNodes);
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('CustomReportFolder-1');
    campaignWorkspacePage.treeMenu.assertDeleteButtonIsActive();
    const confirmDialog = campaignWorkspacePage.treeMenu.initDeletion('custom-report-tree', [1]);
    const libRefreshedAfterDelete = {
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: ['ChartDefinition-2', 'CustomReportFolder-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '2'},
      state: DataRowOpenState.open
    } as unknown as DataRow;
    const refreshedContent = [
      libRefreshedAfterDelete, c, crf2
    ];
    const selectedParentMock = new HttpMockBuilder('/custom-report-library-view/1?**').responseBody({}).build();
    confirmDialog.deleteNodes([1], ['CustomReportLibrary-1'], refreshedContent);
    selectedParentMock.wait();
    campaignWorkspacePage.tree.assertNodeNotExist('CustomReportFolder-1');
    campaignWorkspacePage.tree.assertNodeExist('CustomReportFolder-2');
    campaignWorkspacePage.tree.assertNodeExist('ChartDefinition-2');
  });

});

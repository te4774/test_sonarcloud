import {
  ALL_PROJECT_PERMISSIONS,
  NO_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';
import {
  CustomReportWorkspacePage
} from '../../../../page-objects/pages/custom-report-workspace/custom-report-workspace.page';
import {DataRow, DataRowOpenState, GridResponse} from '../../../../model/grids/data-row.type';

describe('Custom Report Workspace Tree Copy', function () {

  function referentialData() {
    return new ReferentialDataMockBuilder()
      .withProjects({
          name: 'Project 1',
          permissions: ALL_PROJECT_PERMISSIONS,
        },
        {
          name: 'Project 2',
          permissions: NO_PROJECT_PERMISSIONS,
        })
      .build();
  }

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: [],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'}
    } as unknown as DataRow,
      {
        id: 'CustomReportLibrary-2',
        projectId: 2,
        children: [],
        data: {'NAME': 'Project2', 'CHILD_COUNT': '1'}
      } as unknown as DataRow]
  };

  const libraryRefreshAtOpen = [
    {
      id: 'CustomReportLibrary-1',
      projectId: 1,
      children: ['CustomReportFolder-1', 'CustomReportFolder-2', 'ChartDefinition-2'],
      data: {'NAME': 'Project1', 'CHILD_COUNT': '3'},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'CustomReportFolder-1',
      children: ['ChartDefinition-3'],
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
      data: {'NAME': 'folder1'},
      state: DataRowOpenState.open
    } as unknown as DataRow,
    {
      id: 'ChartDefinition-3',
      children: [],
      projectId: 1,
      parentRowId: 'CustomReportFolder-1',
      data: {'NAME': 'a nice chart'}
    } as unknown as DataRow,
    {
      id: 'CustomReportFolder-2',
      children: [],
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
      data: {'NAME': 'folder2'},
    } as unknown as DataRow,
    {
      id: 'ChartDefinition-2',
      children: [],
      projectId: 1,
      parentRowId: 'CustomReportLibrary-1',
      data: {'NAME': 'a chart'}
    } as unknown as DataRow,
  ];

  it('should activate or deactivate copy button according to user selection', () => {

    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes);
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode(firstNode.id);
    customReportWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    // the component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('CustomReportFolder-1');
    customReportWorkspacePage.treeMenu.assertCopyButtonIsActive();
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('ChartDefinition-2');
    customReportWorkspacePage.treeMenu.assertCopyButtonIsActive();
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('CustomReportLibrary-1');
    customReportWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
  });

  it('should activate or deactivate paste button according to destination', () => {
    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes, referentialData());
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode(firstNode.id);
    customReportWorkspacePage.treeMenu.assertCopyButtonIsDisabled();
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    // The component should never render with empty model, but we don't care, we are just testing the menu
    tree.selectNode('ChartDefinition-2');
    customReportWorkspacePage.treeMenu.assertCopyButtonIsActive();
    customReportWorkspacePage.treeMenu.copy();
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    tree.selectNode('CustomReportFolder-1');
    customReportWorkspacePage.treeMenu.assertPasteButtonIsActive();
    tree.selectNode('CustomReportLibrary-1');
    customReportWorkspacePage.treeMenu.assertPasteButtonIsActive();
    tree.selectNode('ChartDefinition-3');
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
    // Testing permissions
    tree.selectNode('CustomReportLibrary-2');
    customReportWorkspacePage.treeMenu.assertPasteButtonIsDisabled();
  });

  it('should copy paste a node', () => {
    const refreshedNodes = [
      {
        id: 'CustomReportFolder-2',
        projectId: 1,
        children: ['ChartDefinition-4'],
        parentRowId: 'CustomReportLibrary-1',
        state: DataRowOpenState.open,
        data: {'NAME': 'folder2', CHILD_COUNT: 1}
      } as unknown as DataRow,
      {
        id: 'ChartDefinition-4',
        projectId: 1,
        children: [],
        parentRowId: 'CustomReportFolder-2',
        data: {'NAME': 'a nice chart'}
      } as unknown as DataRow,
    ];
    const firstNode = initialNodes.dataRows[0];
    const customReportWorkspacePage = CustomReportWorkspacePage.initTestAtPage(initialNodes, referentialData());
    const tree = customReportWorkspacePage.tree;
    tree.openNode(firstNode.id, libraryRefreshAtOpen);
    tree.selectNode('ChartDefinition-3');
    customReportWorkspacePage.treeMenu.copy();
    tree.selectNode('CustomReportFolder-2');
    customReportWorkspacePage.treeMenu.paste({dataRows: refreshedNodes}, 'custom-report-tree', 'CustomReportFolder-2');
    tree.assertNodeExist('ChartDefinition-4');
    tree.assertNodeIsOpen('CustomReportFolder-2');
  });
});

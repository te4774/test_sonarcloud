import {
  basicWorkbenchData,
  CreateChartViewPage
} from '../../../page-objects/pages/custom-report-workspace/create-chart-view.page';
import {NavBarElement} from '../../../page-objects/elements/nav-bar/nav-bar.element';
import {ChartType} from '../../../model/custom-report/chart-definition.model';
import {getSimpleChartDefinition, getThreeAxisModel} from '../../../data-mock/custom-chart.mocks';
import {getSimpleCustomReportLibraryChildNodes} from '../../../data-mock/custom-report.data-mock';

const PIE = 'Répartition';
const BAR = 'Histogramme';
const CUMULATIVE = 'Évolution';
const TREND = 'Tendance';
const COMPARATIVE = 'Comparaison';

describe('Create Chart View', function () {
  it('should create a pie chart', () => {
    const createChartViewPage = navigateToChartWorkbench();
    new NavBarElement().toggle();
    createChartViewPage.assertExist();
    createChartViewPage.axisSelector.assertNoAttributeSelected();
    createChartViewPage.axisSelector.openAttributeSelector();
    createChartViewPage.axisSelector.addAttributeAndRenderPreview(4, '1', getSimpleChartDefinition(ChartType.PIE));
    createChartViewPage.assertChartIsRendered();
    const customReportWorkspacePage = createChartViewPage.saveChart('1', 3, getSimpleCustomReportLibraryChildNodes());
    customReportWorkspacePage.tree.assertNodeIsSelected('ChartDefinition-3');
  });

  it('should display all required options', () => {
    const createChartViewPage = navigateToChartWorkbench();
    new NavBarElement().toggle();
    createChartViewPage.assertExist();
    createChartViewPage.chartTypeSelector.checkAllOptions([
      PIE,
      BAR,
      CUMULATIVE,
      TREND,
      COMPARATIVE
    ]);
  });

  it('should create a bar chart', () => {
    const createChartViewPage = navigateToChartWorkbench();
    new NavBarElement().toggle();
    createChartViewPage.assertExist();
    createChartViewPage.assertHintMessageIsVisible();
    createChartViewPage.chartTypeSelector.selectValue(BAR);
    createChartViewPage.measureSelector.assertNoAttributeSelected();
    createChartViewPage.measureSelector.openAttributeSelector();
    createChartViewPage.measureSelector.addAttributeToChart(1);
    createChartViewPage.assertMissingAxesMessageIsVisible();
    createChartViewPage.axisSelector.assertNoAttributeSelected();
    createChartViewPage.axisSelector.openAttributeSelector();
    createChartViewPage.axisSelector.addAttributeAndRenderPreview(4, '1', getSimpleChartDefinition(ChartType.BAR));
    createChartViewPage.assertChartIsRendered();
    const customReportWorkspacePage = createChartViewPage.saveChart('1', 3, getSimpleCustomReportLibraryChildNodes());
    customReportWorkspacePage.tree.assertNodeIsSelected('ChartDefinition-3');
  });

  it('should create a cumulative chart', () => {
    const createChartViewPage = navigateToChartWorkbench();
    new NavBarElement().toggle();
    createChartViewPage.assertExist();
    createChartViewPage.assertHintMessageIsVisible();
    createChartViewPage.chartTypeSelector.selectValue(CUMULATIVE);
    createChartViewPage.measureSelector.assertNoAttributeSelected();
    createChartViewPage.measureSelector.openAttributeSelector();
    createChartViewPage.measureSelector.addAttributeToChart(2);
    createChartViewPage.assertMissingAxesMessageIsVisible();
    createChartViewPage.axisSelector.assertNoAttributeSelected();
    createChartViewPage.axisSelector.openAttributeSelector();
    createChartViewPage.axisSelector.addAttributeAndRenderPreview(12, '1', getSimpleChartDefinition(ChartType.CUMULATIVE));
    createChartViewPage.assertChartIsRendered();
    const customReportWorkspacePage = createChartViewPage.saveChart('1', 3, getSimpleCustomReportLibraryChildNodes());
    customReportWorkspacePage.tree.assertNodeIsSelected('ChartDefinition-3');
  });

  it('should create a comparative chart', () => {
    const createChartViewPage = navigateToChartWorkbench();
    new NavBarElement().toggle();
    createChartViewPage.assertExist();
    createChartViewPage.assertHintMessageIsVisible();
    createChartViewPage.chartTypeSelector.selectValue(COMPARATIVE);
    createChartViewPage.measureSelector.assertNoAttributeSelected();
    createChartViewPage.measureSelector.openAttributeSelector();
    createChartViewPage.measureSelector.addAttributeToChart(2);
    createChartViewPage.assertMissingAxesMessageIsVisible();
    createChartViewPage.seriesSelector.assertNoAttributeSelected();
    createChartViewPage.seriesSelector.openAttributeSelector();
    createChartViewPage.seriesSelector.addAttributeToChart(13);
    createChartViewPage.assertMissingAxesMessageIsVisible();
    createChartViewPage.axisSelector.assertNoAttributeSelected();
    createChartViewPage.axisSelector.openAttributeSelector();
    createChartViewPage.axisSelector.addAttributeAndRenderPreview(14, '1', getThreeAxisModel(ChartType.COMPARATIVE));
    createChartViewPage.assertChartIsRendered();
    const customReportWorkspacePage = createChartViewPage.saveChart('1', 3, getSimpleCustomReportLibraryChildNodes());
    customReportWorkspacePage.tree.assertNodeIsSelected('ChartDefinition-3');
  });

  it('should create a trend chart', () => {
    const createChartViewPage = navigateToChartWorkbench();
    new NavBarElement().toggle();
    createChartViewPage.assertExist();
    createChartViewPage.assertHintMessageIsVisible();
    createChartViewPage.chartTypeSelector.selectValue(TREND);
    createChartViewPage.measureSelector.assertNoAttributeSelected();
    createChartViewPage.measureSelector.openAttributeSelector();
    createChartViewPage.measureSelector.addAttributeToChart(2);
    createChartViewPage.assertMissingAxesMessageIsVisible();
    createChartViewPage.seriesSelector.assertNoAttributeSelected();
    createChartViewPage.seriesSelector.openAttributeSelector();
    createChartViewPage.seriesSelector.addAttributeToChart(10);
    createChartViewPage.assertMissingAxesMessageIsVisible();
    createChartViewPage.axisSelector.assertNoAttributeSelected();
    createChartViewPage.axisSelector.openAttributeSelector();
    createChartViewPage.axisSelector.addAttributeAndRenderPreview(15, '1', getThreeAxisModel(ChartType.TREND));
    createChartViewPage.assertChartIsRendered();
    const customReportWorkspacePage = createChartViewPage.saveChart('1', 3, getSimpleCustomReportLibraryChildNodes());
    customReportWorkspacePage.tree.assertNodeIsSelected('ChartDefinition-3');
  });

  it('should toggle attributes', () => {
    const createChartViewPage = navigateToChartWorkbench();
    new NavBarElement().toggle();
    createChartViewPage.assertExist();
    createChartViewPage.axisSelector.assertNoAttributeSelected();
    const attributeTree = createChartViewPage.axisSelector.openAttributeSelector();
    // checking only a few nodes for testing generation... can't test the whole hundreds of nodes
    attributeTree.assertNodeIsOpen('REQUIREMENT');
    attributeTree.assertNodeIsOpen('REQUIREMENT_VERSION');
    attributeTree.assertNodeIsOpen('TEST_CASE');
    attributeTree.assertNodeExist(1);
    attributeTree.assertNodeIsLeaf(1);
    attributeTree.assertNodeTextContains(1, 'ID de l\'exigence');
    attributeTree.assertNodeExist(2);
    attributeTree.assertNodeIsLeaf(2);
    attributeTree.assertNodeTextContains(2, 'Nombre de versions');
    attributeTree.closeAllNodes();
    attributeTree.assertNodeIsClosed('REQUIREMENT');
    attributeTree.assertNodeIsClosed('REQUIREMENT_VERSION');
    attributeTree.assertNodeIsClosed('TEST_CASE');
    attributeTree.assertNodeNotExist(1);
    attributeTree.assertNodeNotExist(2);
    attributeTree.openAllNodes();
    attributeTree.assertNodeIsOpen('REQUIREMENT');
    attributeTree.assertNodeIsOpen('REQUIREMENT_VERSION');
    attributeTree.assertNodeIsOpen('TEST_CASE');
    attributeTree.assertNodeExist(1);
    attributeTree.assertNodeExist(2);
  });

  it('should filter attributes', () => {
    const createChartViewPage = navigateToChartWorkbench();
    new NavBarElement().toggle();
    createChartViewPage.assertExist();
    createChartViewPage.axisSelector.assertNoAttributeSelected();
    const attributeTree = createChartViewPage.axisSelector.openAttributeSelector();
    // checking only a few nodes for testing generation... can't test the whole hundreds of nodes
    attributeTree.assertNodeExist('REQUIREMENT');
    attributeTree.assertNodeExist('REQUIREMENT_VERSION');
    attributeTree.assertNodeExist('TEST_CASE');
    attributeTree.assertNodeExist(1);
    attributeTree.assertNodeExist(2);
    attributeTree.filterAttribute('ID de l\'exi');
    attributeTree.assertNodeExist('REQUIREMENT');
    attributeTree.assertNodeNotExist('REQUIREMENT_VERSION');
    attributeTree.assertNodeNotExist('TEST_CASE');
    attributeTree.assertNodeExist(1);
    attributeTree.assertNodeNotExist(2);
  });

  function navigateToChartWorkbench(): CreateChartViewPage {
    return CreateChartViewPage.initTestAtPage(undefined, basicWorkbenchData, '1');
  }
});


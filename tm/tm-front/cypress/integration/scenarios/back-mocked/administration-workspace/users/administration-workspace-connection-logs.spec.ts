import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {EditableDateFieldElement} from '../../../../page-objects/elements/forms/editable-date-field.element';
import {
  AdminWorkspaceConnectionLogsPage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-connection-logs.page';
import {assertAccessDenied} from '../../../../utils/assert-access-denied.utils';

describe('Administration Workspace - Connection logs', function () {
  const todayDate = new Date();

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [{
      id: 'ConnectionAttemptLog-1',
      children: [],
      data: {
        login: 'admin',
        connectionDate: todayDate,
        success: true

      }
    } as unknown as DataRow]
  };

  it('should forbid access to non-admin users', () => {
    assertAccessDenied('administration-workspace/projects');
  });

  it('should display connection logs grid', () => {
    const page = AdminWorkspaceConnectionLogsPage.initTestAtPageUsersConnexionLogs(initialNodes);
    const grid = page.grid;

    grid.assertRowExist('ConnectionAttemptLog-1');
    const row = grid.getRow('ConnectionAttemptLog-1');
    row.cell('login').textRenderer().assertContainText('admin');
    row.cell('connectionDate').textRenderer().assertContainText(EditableDateFieldElement.dateToDisplayString(todayDate));
    row.cell('success').textRenderer().assertContainText('oui');
  });

  it('should open export dialog', () => {
    const page = AdminWorkspaceConnectionLogsPage.initTestAtPageUsersConnexionLogs(initialNodes);
    const exportDialog = page.openExportDialog();
    exportDialog.assertExists();
    exportDialog.confirm();
  });
});

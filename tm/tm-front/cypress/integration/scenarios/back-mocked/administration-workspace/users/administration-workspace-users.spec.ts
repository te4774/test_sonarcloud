import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {EditableDateFieldElement} from '../../../../page-objects/elements/forms/editable-date-field.element';
import {
  RemoveUserDialogElement
} from '../../../../page-objects/pages/administration-workspace/dialogs/remove-user-dialog.element';
import {
  AdminWorkspaceUsersPage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-users.page';
import {assertAccessDenied} from '../../../../utils/assert-access-denied.utils';
import {selectByDataTestToolbarButtonId} from '../../../../utils/basic-selectors';
import {mockFieldValidationError} from '../../../../data-mock/http-errors.data-mock';
import {
  ProjectWithoutPermission
} from '../../../../page-objects/pages/administration-workspace/user-view/dialogs/add-user-authorisations.dialog';
import {NavBarAdminElement} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import {User} from '../../../../model/user/user.model';
import {AclGroup} from '../../../../model/permissions/permissions.model';
import {
  TeamAssociation
} from '../../../../page-objects/pages/administration-workspace/user-view/dialogs/associate-team-to-user.dialog';
import {makeTeamData, makeUserData} from '../../../../data-mock/administration-views.data-mock';
import {
  TeamViewDetailPage
} from '../../../../page-objects/pages/administration-workspace/team-view/team-view-detail.page';
import {HttpMockBuilder} from "../../../../utils/mocks/request-mock";
import {UserViewPage} from "../../../../page-objects/pages/administration-workspace/user-view/user-view.page";


describe('Administration workspace users', function () {
  it('should forbid access to non-admin users', () => {
    assertAccessDenied('administration-workspace/projects');
  });

  it('should display users grid', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('login').textRenderer().assertContainText('admin');
    grid.getRow(2).cell('active').iconRenderer().assertContainIcon('anticon-sqtm-core-administration:user');
    row.cell('userGroup').textRenderer().assertContainText('Administrateur');
    row.cell('firstName').textRenderer().assertContainText('adminFirstName');
    row.cell('lastName').textRenderer().assertContainText('adminLastName');
    row.cell('email').textRenderer().assertContainText('admin@henix.fr');
    row.cell('createdOn').textRenderer().assertContainText(EditableDateFieldElement.dateToDisplayString(todayDate));
    row.cell('createdBy').textRenderer().assertContainText('superAdmin');
    grid.scrollPosition('right', 'mainViewport');
    row.cell('habilitationCount').textRenderer().assertContainText('3');
    row.cell('teamCount').textRenderer().assertContainText('2');
    row.cell('lastConnectedOn').textRenderer().assertContainText(EditableDateFieldElement.dateToDisplayString(todayDate));
  });

  it('should add user', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const httpMockView = new HttpMockBuilder('user-view/4?*')
      .get()
      .responseBody({id: 3,
        login: 'jSmith',
        firstName: 'John',
        lastName: 'Smith',
        email: 'jsmith@email.com',
        active: false,
        createdOn: new Date(),
        createdBy: 'admin',
        lastModifiedBy: 'admin',
        lastModifiedOn: new Date(),
        lastConnectedOn: new Date(),
        usersGroupBinding: 2,
        usersGroups: getUserDto().usersGroups,
        projectPermissions: getUserDto().projectPermissions,
        teams: getUserDto().teams
      })
      .build();

    const dialog = page.openCreateUser();
    dialog.assertExist();
    dialog.fillLogin('jSmith');
    dialog.fillName('Smith');
    dialog.fillFirstName('John');
    dialog.fillEmail('jsmith@email.com');
    dialog.selectGroup('Utilisateur');
    dialog.fillPassword('password');
    dialog.fillConfirmPassword('password');

    dialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 4},
      gridResponse: addUserResponse
    });

    httpMockView.wait();
    const view = new UserViewPage();
    view.assertExist();
    view.emailTextField.checkContent('jsmith@email.com');

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(2);
    grid.assertRowExist(3);
    grid.assertRowExist(4);

    // should add another user
    const dialogAnother = page.openCreateUser();
    dialogAnother.assertExist();
    dialogAnother.fillLogin('j');
    dialogAnother.fillName('s');
    dialogAnother.fillFirstName('j');
    dialogAnother.selectGroup('Utilisateur');
    dialogAnother.fillPassword('password');
    dialogAnother.fillConfirmPassword('password');

    dialogAnother.addWithOptions({
      addAnother: true,
      createResponse: {id: 4},
      gridResponse: addUserResponse
    });

    dialogAnother.assertExist();
    dialogAnother.checkIfFormIsEmpty();
    dialogAnother.cancel();
    dialogAnother.assertNotExist();
  });

  it('should validate creation form', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);

    const dialog = page.openCreateUser();
    dialog.assertExist();

    // should forbid to add a user with an empty login
    dialog.fillLogin('');
    dialog.fillName('s');
    dialog.fillFirstName('j');
    dialog.fillEmail('@');
    dialog.selectGroup('Utilisateur');
    dialog.fillPassword('password');
    dialog.fillConfirmPassword('password');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a user if login already exists
    const loginHttpError = mockFieldValidationError('login', 'sqtm-core.error.generic.name-already-in-use');
    dialog.fillLogin('a');
    dialog.addWithServerSideFailure(loginHttpError);

    // should forbid to add a user with an empty name
    dialog.fillName('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a user with a password containing less than 6 characters
    dialog.fillName('a');
    dialog.fillPassword('pass');
    dialog.fillConfirmPassword('pass');
    dialog.clickOnAddButton();
    dialog.checkIfInsufficientCharsInPasswordErrorMessageIsDisplayed();

    // should forbid to add a user with an empty password
    dialog.fillPassword('');
    dialog.fillConfirmPassword('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a user with a not matching password confirmation
    dialog.fillPassword('password');
    dialog.fillConfirmPassword('passwoooord');
    dialog.clickOnAddButton();
    dialog.checkIfNotMatchingErrorMessageIsDisplayed();
  });

  it('should allow user removal', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const grid = page.grid;

    // Show dialog
    grid.scrollPosition('right', 'mainViewport');

    const iconRenderer = grid.getRow(3).cell('delete').iconRenderer();
    iconRenderer.assertContainIcon('anticon-sqtm-core-generic:delete');
    iconRenderer.click();

    // Confirm
    const dialog = new RemoveUserDialogElement([3]);
    dialog.deleteUserForSuccess({
      count: 2,
      dataRows: [initialNodes.dataRows[0], initialNodes.dataRows[1]]
    });
  });

  it('should delete multiple users', () => {
    cy.viewport(1200, 700);
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);

    page.grid.selectRows(['2', '3'], '#', 'leftViewport');

    // Show dialog
    page.grid.scrollPosition('right', 'mainViewport');

    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    // Confirm
    const dialog = new RemoveUserDialogElement([2, 3]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0]]
    });
  });
});

const todayDate = new Date();

const initialNodes: GridResponse = {
  count: 3,
  dataRows: [
    {
      id: '1',
      children: [],
      data: {
        partyId: 1,
        active: true,
        login: 'admin',
        userGroup: 'squashtest.authz.group.core.Admin',
        firstName: 'adminFirstName',
        lastName: 'adminLastName',
        email: 'admin@henix.fr',
        createdOn: todayDate,
        createdBy: 'superAdmin',
        habilitationCount: '3',
        teamCount: '2',
        lastConnectedOn: todayDate,
      }
    } as unknown as DataRow,
    {
      id: '2',
      children: [],
      data: {
        partyId: 2,
        active: true,
        login: 'jimmy',
        userGroup: 'squashtest.authz.group.tm.User',
        firstName: 'jimmy',
        lastName: 'ymmij',
        email: 'jj@henix.fr',
        createdOn: todayDate,
        createdBy: 'superAdmin',
        habilitationCount: '0',
        teamCount: '1',
      }
    } as unknown as DataRow,
    {
      id: '3',
      children: [],
      data: {
        partyId: 3,
        active: true,
        login: 'bobby',
        userGroup: 'squashtest.authz.group.tm.User',
        firstName: 'bobby',
        lastName: 'xyz',
        email: 'bx@henix.fr',
        createdOn: todayDate,
        createdBy: 'superAdmin',
        habilitationCount: '0',
        teamCount: '1',
      }
    } as unknown as DataRow,
  ]
};

const addUserResponse: GridResponse = {
  count: 4,
  dataRows: [
    ...initialNodes.dataRows,
    {
      id: '4',
      children: [],
      data: {'name': 'User4'}
    } as unknown as DataRow,
  ]
};

describe('Administration Workspace - Users - Authorisations', function () {
  it('should add project permissions to user', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExist();
    userViewPage.authorisationsPanel.grid.assertExist();

    const projectsWithoutPermissions: ProjectWithoutPermission[] =
      [
        {
          id: '-1',
          name: 'Alpha project',
        },
        {
          id: '-2',
          name: 'Beta Project',
        }
      ];

    const addUserAuthorisationsDialog = userViewPage.authorisationsPanel.clickOnAddPermissionButton(projectsWithoutPermissions);

    addUserAuthorisationsDialog.selectProjects('Alpha project', 'Beta Project');
    addUserAuthorisationsDialog.selectProfile('Testeur référent');

    addUserAuthorisationsDialog.confirm([]);
  });


  it('should remove user authorisation', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExist();
    userViewPage.authorisationsPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    userViewPage.foldGrid();

    userViewPage.authorisationsPanel.deleteOne('Alpha Project');
  });

  it('should remove multiple authorisations', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExist();
    userViewPage.authorisationsPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    userViewPage.foldGrid();

    userViewPage.authorisationsPanel.deleteMultiple(['Alpha Project', 'Beta Project']);
  });

  it('should prevent adding authorisations with empty fields', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExist();
    userViewPage.authorisationsPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    userViewPage.foldGrid();

    const projectsWithoutPermissions: ProjectWithoutPermission[] = [];

    const addUserAuthorisationsDialog = userViewPage.authorisationsPanel.clickOnAddPermissionButton(projectsWithoutPermissions);

    addUserAuthorisationsDialog.clickOnConfirmButton();
    cy.get('.sqtm-core-error-message').should('have.length', 2);
  });
});

describe('Administration Workspace - Users - Teams', function () {
  it('should associate user to team', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExist();
    userViewPage.teamsPanel.grid.assertExist();

    const unassociatedTeams: TeamAssociation[] =
      [
        {
          partyId: '-1',
          name: 'Team A'
        },
        {
          partyId: '-2',
          name: 'Team B'
        }
      ];

    const associateUserToTeamDialog = userViewPage.teamsPanel.clickOnAssociateTeamToUserButton(unassociatedTeams);

    associateUserToTeamDialog.selectTeams('Team A', 'Team B');

    associateUserToTeamDialog.confirm([]);
  });

  it('should remove team association', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExist();
    userViewPage.teamsPanel.grid.assertExist();

    const navBar = new NavBarAdminElement();
    navBar.toggle();

    userViewPage.teamsPanel.deleteOne('Team A');
  });

  it('should remove multiple team associations', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExist();
    userViewPage.teamsPanel.grid.assertExist();

    const navBar = new NavBarAdminElement();
    navBar.toggle();

    userViewPage.teamsPanel.deleteMultiple(['Team A', 'Team B']);
  });

  it('should prevent adding team if none is selected', () => {
    const page = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = page.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.assertExist();
    userViewPage.teamsPanel.grid.assertExist();

    const unassociatedTeams: TeamAssociation[] = [];

    const associateUserToTeamDialog = userViewPage.teamsPanel.clickOnAssociateTeamToUserButton(unassociatedTeams);

    associateUserToTeamDialog.clickOnConfirmButton();
    cy.get('.sqtm-core-error-message').should('have.length', 1);
  });

  it('should navigate to team detail page and back', () => {
    const usersWorkspace = AdminWorkspaceUsersPage.initTestAtPage(initialNodes);
    const userViewPage = usersWorkspace.selectUserByLogin(initialNodes.dataRows[0].data.login, getUserDto());

    userViewPage.teamsPanel.showTeamDetail('Team A', makeTeamData());
    const teamViewDetail = new TeamViewDetailPage();
    teamViewDetail.assertExist();

    teamViewDetail.clickBackButton();
    usersWorkspace.assertExist();
  });
});

function getUserDto(): User {
  return makeUserData({
    id: 3,
    login: '007',
    firstName: 'James',
    lastName: 'Bond',
    email: '007@secret-agent.co.uk',
    active: true,
    createdOn: new Date(),
    createdBy: 'admin',
    lastModifiedBy: 'admin',
    lastModifiedOn: new Date(),
    lastConnectedOn: new Date(),
    usersGroupBinding: 2,
    usersGroups:
      [
        {id: 1, qualifiedName: 'squashtest.authz.group.core.Admin'},
        {id: 4, qualifiedName: 'squashtest.authz.group.tm.TestAutomationServer'},
        {id: 2, qualifiedName: 'squashtest.authz.group.tm.User'}
      ],
    projectPermissions: [
      {
        projectId: -1,
        projectName: 'Alpha Project',
        permissionGroup: {
          id: 1,
          simpleName: 'PROJECT_MANAGER',
          qualifiedName: AclGroup.PROJECT_MANAGER
        }
      },
      {
        projectId: -2,
        projectName: 'Beta Project',
        permissionGroup: {
          id: 2,
          simpleName: 'TEST_RUNNER',
          qualifiedName: AclGroup.TEST_RUNNER
        }
      }
    ],
    teams: [
      {
        partyId: -1,
        name: 'Team A'
      },
      {
        partyId: -2,
        name: 'Team B'
      }
    ]
  });
}

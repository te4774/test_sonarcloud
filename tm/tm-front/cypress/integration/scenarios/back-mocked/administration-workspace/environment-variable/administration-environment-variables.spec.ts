import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {
  AdminWorkspaceEnvironmentVariablePage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-environment-variable.page';
import {
  RemoveEnvironmentVariableElement
} from '../../../../page-objects/pages/administration-workspace/dialogs/remove-environment-variable.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

describe('Administration workspace -Environment Variable', function () {
  const initialNodes: GridResponse = {
    count: 2,
    dataRows: [
      {
        id: '1',
        children: [],
        data: {
          evId: 1,
          name: 'EnvironmentVariable1',
          code: '123',
          inputType: 'PLAIN_TEXT',
        }
      } as unknown as DataRow,
      {
        id: '2',
        children: [],
        data: {
          evId: 2,
          name: 'EnvironmentVariable2',
          code: '345',
          inputType: 'DROPDOWN_LIST',
        }
      } as unknown as DataRow,
    ]
  };

  it('should display environment variable grid', () => {
    const page = AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('name').textRenderer().assertContainText('EnvironmentVariable1');
    row.cell('code').textRenderer().assertContainText('123');
    row.cell('inputType').textRenderer().assertContainText('Texte simple');
  });

  it('should add environment variable', () => {
    const page = AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);
    const httpMockView = new HttpMockBuilder('environment-variable-view/3?*')
      .get()
      .responseBody({
        evId: 3,
        name: 'EV',
        code: 'EV1',
        inputType: 'PLAIN_TEXT',
      })
      .build();

    const dialog = page.openCreateEnvironmentVariable();
    dialog.assertExist();

    dialog.fillName('EV');
    dialog.fillCodeField('EV1');
    dialog.selectTypeField('Texte simple');
    dialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 3},
      gridResponse: {
        count: initialNodes.count + 1,
        dataRows: [
          ...initialNodes.dataRows,
          {
            id: 3,
            children: [],
            data: {
              evId: 3,
              name: 'EV',
              code: 'EV1',
              inputType: 'PLAIN_TEXT',
            }
          } as unknown as DataRow,
        ]
      }
    });
    httpMockView.wait();
  });

  it('should add environment variable with dropdown list', () => {
    const page = AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);

    const httpMockView = new HttpMockBuilder('environment-variable-view/4?*')
      .get()
      .responseBody({
        evId: 4,
        name: 'EVlist',
        code: 'EVlist',
        inputType: 'DROPDOWN_LIST',
      })
      .build();

    const dialog = page.openCreateEnvironmentVariable();
    dialog.assertExist();

    dialog.fillName('EVlist');
    dialog.fillCodeField('EVlist');
    dialog.selectTypeField('Liste déroulante');
    dialog.addDropdownListOption('option1', 'code1');
    dialog.dropdownListOptionsGrid.assertRowCount(1);
    const row = dialog.dropdownListOptionsGrid.getRow('code1');
    row.cell('label').textRenderer().assertContainText('option1');
    row.cell('code').textRenderer().assertContainText('code1');
    dialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 4},
      gridResponse: {
        count: initialNodes.count + 1,
        dataRows: [
          ...initialNodes.dataRows,
          {
            id: 4,
            children: [],
            data: {
              evId: 4,
              name: 'EVlist',
              code: 'EVlist',
              inputType: 'DROPDOWN_LIST',
            }
          } as unknown as DataRow,
        ]
      }
    });
    httpMockView.wait();
  });

  it('should search on name column', () => {
    const page = AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);
    const grid = page.grid;
    grid.assertRowCount(2);

    page.fillSearchInput('Variable2', {
      count: 1,
      dataRows: [{
        id: '2',
        children: [],
        data: {
          evId: 2,
          name: 'EnvironmentVariable2',
          code: '345',
          inputType: 'DROPDOWN_LIST',
        }
      } as unknown as DataRow]
    });

    grid.assertRowCount(1);
  });

  it('should validate creation form', () => {
    const page = AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);
    const dialog = page.openCreateEnvironmentVariable();
    dialog.assertExist();

    dialog.fillName('EVerror');
    dialog.fillCodeField('EVerror');
    dialog.selectTypeField('Liste déroulante');

    dialog.addDropdownListOption('name', 'code2');
    dialog.addDropdownListOption('name', 'code3');
    dialog.checkIfOptionNameAlreadyExistErrorIsDisplayed();

    dialog.addDropdownListOption('name1', 'code2');
    dialog.checkIfOptionCodeAlreadyExistErrorIsDisplayed();
  });

  it('should delete environment variable', () => {
    const page = AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);
    page.grid.assertExist();
    page.deleteSingleEnvironmentVariableInGrid(2);

    const deleteDialog = new RemoveEnvironmentVariableElement([2]);
    deleteDialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0]]
    });

    page.grid.assertRowCount(1);
  });

  it('should delete multiple environment variable', () => {
    const page = AdminWorkspaceEnvironmentVariablePage.initTestAtPageEnvironmentVariables(initialNodes);

    page.grid.selectRows([1, 2]);
    page.clickOnMultipleDeleteButton(true);
  });

});

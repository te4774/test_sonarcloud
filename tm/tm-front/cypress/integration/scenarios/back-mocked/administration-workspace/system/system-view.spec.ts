import {SystemViewModel} from '../../../../model/system/system-view.model';
import {makeSystemViewData} from '../../../../data-mock/administration-views.data-mock';
import {
  SystemViewDownloadsPage
} from '../../../../page-objects/pages/administration-workspace/system-view/system-view-downloads.page';
import {
  SystemViewInformationPage
} from '../../../../page-objects/pages/administration-workspace/system-view/system-view-information.page';
import {
  SystemViewMessagesPage
} from '../../../../page-objects/pages/administration-workspace/system-view/system-view-messages.page';
import {
  SystemViewSettingsPage
} from '../../../../page-objects/pages/administration-workspace/system-view/system-view-settings.page';

describe('Administration workspace - System downloads', function () {
  it('should show log file download links', () => {
    const viewData: SystemViewModel = makeSystemViewData({
      logFiles: ['logfile1', 'squash-tm.log', 'logfile2'],
    });

    const page = SystemViewDownloadsPage.navigateToPage(viewData);
    page.assertExist();
    page.assertLatestLogLinkExists();
    page.assertLinkExists('logfile1');
    page.assertLinkExists('logfile2');
    page.assertLinkDoesNotExist('squash-tm.log');
  });

  it('should not show previous links section if there\'s no previous', () => {
    const viewData: SystemViewModel = makeSystemViewData({
      logFiles: ['squash-tm.log'],
    });

    const page = SystemViewDownloadsPage.navigateToPage(viewData);
    page.assertExist();
    page.assertLatestLogLinkExists();
    page.assertPreviousLinksAreNotVisible();
  });
});

describe('Administration workspace - System information', function () {
  it('should display information about the instance', () => {
    const viewData: SystemViewModel = makeSystemViewData({
      appVersion: 'VERSION',
      plugins: ['PLUGIN1', 'PLUGIN2'],
      statistics: {
        testcaseIndexingDate: null,
        requirementIndexingDate: null,
        campaignIndexingDate: null,
        projectsNumber: 1,
        usersNumber: 12,
        requirementsNumber: 123,
        testCasesNumber: 1234,
        campaignsNumber: 12345,
        iterationsNumber: 321,
        executionsNumber: 2,
        databaseSize: 10000,
      }
    });

    const page = SystemViewInformationPage.navigateToPage(viewData);
    page.assertExist();
    page.checkVersion(viewData.appVersion);
    page.checkStatistics(viewData.statistics);
    page.checkPlugins(viewData.plugins);
  });
});

describe('Administration workspace - System messages', function () {
  it('should display login and welcome messages', () => {
    const viewData: SystemViewModel = makeSystemViewData({
      loginMessage: 'hello',
      welcomeMessage: 'world',
    });

    const page = SystemViewMessagesPage.navigateToPage(viewData);
    page.assertExist();

    page.loginMessageField.checkTextContent('hello');
    page.welcomeMessageField.checkTextContent('world');
  });

  it('should change login and welcome messages', () => {
    const viewData: SystemViewModel = makeSystemViewData({
      loginMessage: 'hello',
      welcomeMessage: 'world',
    });

    const page = SystemViewMessagesPage.navigateToPage(viewData);

    page.loginMessageField.setAndConfirmValue('&');
    page.loginMessageField.checkTextContent('&');
    page.welcomeMessageField.setAndConfirmValue('$');
    page.welcomeMessageField.checkTextContent('$');
  });
});

describe('Administration workspace - System settings', function () {
  it('should display settings of the instance', () => {
    const viewData: SystemViewModel = makeSystemViewData({
      whiteList: 'txt, pdf',
      uploadSizeLimit: '100000',
      importSizeLimit: '100000',
      callbackUrl: 'http://192.168.0.1:8080/squash',
      stackTraceFeatureIsEnabled: true,
      stackTracePanelIsVisible: true,
    });

    const page = SystemViewSettingsPage.navigateToPage(viewData);
    page.assertExist();
    page.checkWhiteList(viewData.whiteList);
    page.checkUploadSizeLimit(viewData.uploadSizeLimit);
    page.checkImportSizeLimit(viewData.importSizeLimit);
    page.checkCallbackUrl(viewData.callbackUrl);
  });

  it('should set white list', () => {
    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSettingsPage.navigateToPage(viewData);
    page.assertExist();
    page.whiteListField.setAndConfirmValue('pdf');
    page.whiteListField.checkContent('pdf');
  });

  it('should set upload size limit', () => {
    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSettingsPage.navigateToPage(viewData);
    page.assertExist();
    page.uploadSizeLimitField.setAndConfirmValue('200000');
    page.uploadSizeLimitField.checkContent('200000');
  });

  it('should set import size limit', () => {
    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSettingsPage.navigateToPage(viewData);
    page.assertExist();
    page.importSizeLimitField.setAndConfirmValue('200000');
    page.importSizeLimitField.checkContent('200000');
  });

  it('should set callback url', () => {
    const viewData: SystemViewModel = makeSystemViewData();
    const page = SystemViewSettingsPage.navigateToPage(viewData);
    page.assertExist();
    page.callbackUrlField.setAndConfirmValue('http://192.168.0.1:8080/test');
    page.callbackUrlField.checkContent('http://192.168.0.1:8080/test');
  });

  it('should toggle stacktrace feature', () => {
    const viewData: SystemViewModel = makeSystemViewData({
      stackTraceFeatureIsEnabled: true,
      stackTracePanelIsVisible: true,
    });
    const page = SystemViewSettingsPage.navigateToPage(viewData);
    page.assertExist();
    page.stackTraceFeatureSwitch.toggle();
    page.stackTraceFeatureSwitch.checkValue(false);
  });

  it('should toggle case insensitive login feature', () => {
    const viewData: SystemViewModel = makeSystemViewData({
      caseInsensitiveLogin: true,
    });
    const page = SystemViewSettingsPage.navigateToPage(viewData);
    page.assertExist();
    page.caseInsensitiveLoginSwitch.toggle();
    page.caseInsensitiveLoginSwitch.checkValue(false);
  });

  it('should toggle case insensitive actions feature', () => {
    const viewData: SystemViewModel = makeSystemViewData({
      caseInsensitiveActions: true,
    });
    const page = SystemViewSettingsPage.navigateToPage(viewData);
    page.assertExist();
    page.caseInsensitiveActionsSwitch.toggle();
    page.caseInsensitiveActionsSwitch.checkValue(false);
  });

  it('should toggle auto-connect feature', () => {
    const viewData: SystemViewModel = makeSystemViewData({
      autoconnectOnConnection: true,
    });
    const page = SystemViewSettingsPage.navigateToPage(viewData);
    page.assertExist();
    page.autoconnectOnConnectionSwitch.checkValue(true);
    page.autoconnectOnConnectionSwitch.toggle();
    page.autoconnectOnConnectionSwitch.checkValue(false);
  });
});

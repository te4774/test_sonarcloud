import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {
  RemoveTestAutomationServerDialogElement
} from '../../../../page-objects/pages/administration-workspace/dialogs/remove-test-automation-server-dialog.element';
import {
  AdminWorkspaceAutomationServersPage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-automation-servers.page';
import {mockFieldValidationError} from '../../../../data-mock/http-errors.data-mock';
import {
  TestAutomationServerViewPage
} from '../../../../page-objects/pages/administration-workspace/test-automation-server-view/test-automation-server-view.page';
import {
  AdminTestAutomationServer,
  AutomatedExecutionEnvironment,
  TestAutomationServer,
  TestAutomationServerKind
} from '../../../../model/test-automation/test-automation-server.model';
import {makeTAServerViewData} from '../../../../data-mock/administration-views.data-mock';
import {AdminReferentialDataMockBuilder} from '../../../../utils/referential/admin-referential-data-builder';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {AuthenticationProtocol} from '../../../../model/third-party-server/authentication.model';
import {TokenAuthCredentials} from '../../../../model/third-party-server/credentials.model';
import {
  EnvironmentSelectionPanelDto
} from '../../../../page-objects/elements/automated-execution-environments/environment-selection-panel.element';
import {EnvironmentVariableOption} from '../../../../model/environment-variable/environment-variable-option.model';
import {
  BoundEnvironmentVariable,
  EvInputType
} from '../../../../model/environment-variable/environment-variable.model';

describe('Administration workspace - Test Automation Servers', function () {
  function initialisePage(): AdminWorkspaceAutomationServersPage {
    const refData = new AdminReferentialDataMockBuilder()
      .withAvailableTestAutomationServerKinds([TestAutomationServerKind.jenkins, TestAutomationServerKind.squashAutom])
      .build();
    return AdminWorkspaceAutomationServersPage.initTestAtPageTestAutomationServers(initialNodes, refData);
  }

  it('should display  the grid of test automation servers', () => {
    const page = initialisePage();
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('name').textRenderer().assertContainText('TestAutomationServer1');
    row.cell('baseUrl').linkRenderer().assertContainText('http:192.168.0.03:9090/testServer1');
  });

  it('should add test automation servers', () => {
    const page = initialisePage();
    const httpMockView = new HttpMockBuilder('test-automation-server-view/4?*')
      .get()
      .responseBody({
        id: 73,
        baseUrl: 'u',
        kind: 'jenkins',
        name: 't',
        description: 'd'
      })
      .build();

    const dialog = page.openCreateTestAutomationServer();
    dialog.assertExist();
    dialog.fillName('t');
    dialog.fillUrl('u');
    dialog.fillDescription('d');
    dialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 4},
      gridResponse: addTestAutomationServerResponse
    });

    const grid = page.grid;

    grid.assertRowExist('1');
    grid.assertRowExist('2');
    grid.assertRowExist('3');
    grid.assertRowExist('4');

    httpMockView.wait();
    const view = new TestAutomationServerViewPage();
    view.entityNameField.checkContent('t');

    // should add another test automation server
    const dialogAnother = page.openCreateTestAutomationServer();
    dialogAnother.assertExist();
    dialogAnother.fillName('t');
    dialogAnother.fillUrl('u');
    dialogAnother.fillDescription('d');

    dialogAnother.addWithOptions({
      addAnother: true,
      createResponse: {id: 4},
      gridResponse: addTestAutomationServerResponse
    });

    dialogAnother.assertExist();
    dialogAnother.checkIfFormIsEmpty();
    dialogAnother.cancel();
    dialogAnother.assertNotExist();

  });

  it('should validate creation form', () => {
    const page = initialisePage();

    const dialog = page.openCreateTestAutomationServer();
    dialog.assertExist();
    dialog.fillName('t');
    dialog.fillUrl('u');
    dialog.fillDescription('d');

    // should forbid to add a test automation server if name already exists
    const nameHttpError = mockFieldValidationError('name', 'sqtm-core.error.generic.name-already-in-use');
    dialog.addWithServerSideFailure(nameHttpError);
    dialog.assertExist();
    dialog.checkIfNameAlreadyInUseErrorMessageIsDisplayed();

    // should forbid to add a test automation server with an empty name
    dialog.fillName('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a test automation server with an empty url
    dialog.fillName('n');
    dialog.fillUrl('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a test automation server with a malformed url
    const urlHttpError = mockFieldValidationError('baseUrl', 'sqtm-core.exception.wrong-url');
    dialog.fillUrl('http');
    dialog.addWithServerSideFailure(urlHttpError);
    dialog.assertExist();
    dialog.checkIfMalformedUrlErrorMessageIsDisplayed();
  });

  it('should allow test automation removal without bound projects', () => {
    const page = initialisePage();

    // Show dialog
    page.grid.getRow(3).cell('delete').iconRenderer().click();

    // Confirm
    const dialog = new RemoveTestAutomationServerDialogElement([3]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0]]
    });
  });

  it('should allow test automation removal with bound projects', () => {
    const page = initialisePage();

    // Show dialog
    page.grid.getRow(2).cell('delete').iconRenderer().click();

    // Confirm
    const dialog = new RemoveTestAutomationServerDialogElement([2]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0]]
    });
  });

  it('should allow multiple test automation servers removal', () => {
    const page = initialisePage();

    // Select elements to delete
    page.grid.selectRows([1, 2, 3], '#', 'leftViewport');

    // Show dialog
    page.clickOnMultipleDeleteButton(true, {
      count: 0,
      dataRows: [],
      idAttribute: '',
    });
  });

  const initialNodes: GridResponse = {
    count: 3,
    dataRows: [
      {
        id: '1',
        children: [],
        data: {
          serverId: 1,
          name: 'TestAutomationServer1',
          baseUrl: 'http:192.168.0.03:9090/testServer1',
          executionCount: 0,
          createdBy: '',
          createdOn: '',
          lastModifiedBy: null,
          lastModifiedOn: null,
          kind: TestAutomationServerKind.jenkins,
        }
      } as unknown as DataRow,
      {
        id: '2',
        children: [],
        data: {
          serverId: 2,
          name: 'TestAutomationServer2',
          baseUrl: 'http:192.168.0.03:9090/testServer2',
          executionCount: 2,
          createdBy: '',
          createdOn: '',
          lastModifiedBy: null,
          lastModifiedOn: null,
          kind: TestAutomationServerKind.jenkins,
        }
      } as unknown as DataRow,
      {
        id: '3',
        children: [],
        data: {
          serverId: 3,
          name: 'TestAutomationServer3',
          baseUrl: 'http:192.168.0.03:9090/testServer3',
          executionCount: 0,
          createdBy: '',
          createdOn: '',
          lastModifiedBy: null,
          lastModifiedOn: null,
          kind: TestAutomationServerKind.jenkins,
        }
      } as unknown as DataRow,
    ]
  };

  const addTestAutomationServerResponse: GridResponse = {
    count: 4,
    dataRows: [
      ...initialNodes.dataRows,
      {
        id: '4',
        children: [],
        data: {'name': 'TestAutomationServer4'}
      } as unknown as DataRow,
    ]
  };
});


describe('Administration Workspace - Test automation server - Authentication policy', function () {
  it('should send credential form', () => {
    const page = showTestAutomationServerView();

    page.authPolicyPanel.assertSendButtonDisabled();
    page.authPolicyPanel.usernameField.fill('username');
    page.authPolicyPanel.passwordField.fill('password');
    page.authPolicyPanel.assertSendButtonEnabled();

    page.authPolicyPanel.sendCredentialsForm();
    page.authPolicyPanel.assertSaveSuccessMessageVisible();
  });
});


describe('Administration Workspace - Test automation server - Authentication protocol panel', function () {
  it('should display auth protocol panel', () => {
    const page = showTestAutomationServerView();
    page.authProtocolPanel.protocolField.checkSelectedOption('basic authentication');
  });
});


describe('Administration Workspace - Test automation server - Information panel', function () {
  it('should set test automation server name', () => {
    const page = showTestAutomationServerView();
    page.entityNameField.setAndConfirmValue('NEW');
    page.entityNameField.checkContent('NEW');
  });

  it('should set test automation server url', () => {
    const page = showTestAutomationServerView();
    page.informationPanel.urlField.setAndConfirmValue('http://192.168.0.1:8080/jenkins');
    page.informationPanel.urlField.checkContent('http://192.168.0.1:8080/jenkins');
  });

  it('should set test automation server description', () => {
    const page = showTestAutomationServerView();
    page.informationPanel.descriptionField.setAndConfirmValue('A description');
    page.informationPanel.descriptionField.checkTextContent('A description');
  });

  it('should check test automation server manual slave selection', () => {
    const page = showTestAutomationServerView();
    page.informationPanel.manualSlaveSelection.toggleState();
    page.informationPanel.manualSlaveSelection.checkState(true);
  });
});

function showTestAutomationServerView(initialNodes = getInitialNodes(),
                                      envMockData?: EnvironmentSelectionPanelDto): TestAutomationServerViewPage {
  const page = AdminWorkspaceAutomationServersPage.initTestAtPageTestAutomationServers(initialNodes);
  const firstRowName = initialNodes.dataRows[0].data.name;

  return page.selectAutomationServerByName([firstRowName],
    initialNodes.dataRows[0].data as AdminTestAutomationServer,
    envMockData);
}

function getInitialNodes(automationServer = getInitialModel()): GridResponse {
  return {
    count: 1,
    dataRows: [
      {
        id: automationServer.id.toString(),
        children: [],
        data: automationServer,
        allowMoves: true,
        allowedChildren: [],
        type: 'Generic'
      } as unknown as DataRow,
    ],
  };
}

function getInitialModel(): AdminTestAutomationServer {
  return makeTAServerViewData({
    id: 1,
    name: 'auto1',
    baseUrl: 'http://test',
    createdBy: 'admin',
    createdOn: new Date(),
    description: '',
    lastModifiedBy: 'admin',
    lastModifiedOn: new Date(),
  });
}

describe('Administration Workspace - Test automation server - Environments panel', function () {

  it('should not show environment panel for a Jenkins server', () => {
    const page = showTestAutomationServerView();
    page.environmentPanel.assertNotExist();
  });

  it('should display environments panel', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(getInitialNodes(taServer), mockEnvDataFromServer(taServer));
    page.fetchAvailableEnvironmentsMock.wait();

    const environmentPanel = page.environmentPanel.environmentSelectionPanel;

    // Only visible on project's page
    environmentPanel.tokenOverrideField.assertNotExist();

    environmentPanel.assertSelectedTagsContainAll(['robot', 'chrome']);
    environmentPanel.availableEnvironmentsGrid.assertExist();
    environmentPanel.availableEnvironmentsGrid.findRowId('name', 'dummy').then(rowId =>
      environmentPanel.availableEnvironmentsGrid.getRow(rowId)
        .cell('joinedTags').textRenderer()
        .assertContainText('chrome, robot, windows'));
  });

  it('should modify environment tags', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(getInitialNodes(taServer), mockEnvDataFromServer(taServer));
    page.fetchAvailableEnvironmentsMock.wait();

    const environmentPanel = page.environmentPanel.environmentSelectionPanel;

    environmentPanel.assertSelectedTagsContainAll(['robot', 'chrome']);

    environmentPanel.assertTagsOptionsContainAll(['chrome', 'firefox', 'linux', 'robot', 'windows']);

    environmentPanel.addTag('linux', ['robot', 'chrome', 'linux']);
    environmentPanel.removeTag('robot', ['chrome', 'linux']);

    environmentPanel.assertSelectedTagsContainAll(['chrome', 'linux']);
  });

  it('should display missing credentials message', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(getInitialNodes(taServer), {
      server: {hasServerCredentials: false, defaultTags: null, testAutomationServerId: null},
      environments: {environments: null},
    });
    page.fetchAvailableEnvironmentsMock.wait();

    page.environmentPanel.environmentSelectionPanel.assertMissingCredentialsMessageExists();
  });

  it('should display environments fetch error message', () => {
    const taServer = makeTAServerWithEnvironments();
    const availableEnvMockBuilder = new HttpMockBuilder('test-automation-servers/*/automated-execution-environments/all?*')
      .status(500)
      .build();

    const page = showTestAutomationServerView(getInitialNodes(taServer), null);
    availableEnvMockBuilder.wait();

    page.environmentPanel.environmentSelectionPanel.assertLoadEnvironmentsErrorMessageExists();
  });

  it('should filter available environments based on selected tags', () => {
    const taServer = makeTAServerWithEnvironments();
    const page = showTestAutomationServerView(getInitialNodes(taServer), mockEnvDataFromServer(taServer));
    page.fetchAvailableEnvironmentsMock.wait();
    const environmentPanel = page.environmentPanel.environmentSelectionPanel;

    environmentPanel.removeTag('robot', ['chrome']);
    environmentPanel.removeTag('chrome', []);
    environmentPanel.availableEnvironmentsGrid.assertRowCount(2);

    environmentPanel.addTag('linux', ['linux']);
    environmentPanel.availableEnvironmentsGrid.assertRowCount(1);

    environmentPanel.addTag('windows', ['linux', 'windows']);
    environmentPanel.assertNoMatchingEnvironmentMessageExists();
  });
});

describe('Administration Workspace - Test automation server - SquashAUTOM fields', function () {
  it('should not show SquashAUTOM fields for a Jenkins server', () => {
    const page = showTestAutomationServerView();
    page.informationPanel.assertSquashAUTOMFieldsAreHidden();
  });

  it('should show SquashAUTOM fields', () => {
    const taServer = makeTAServerViewData({
      kind: TestAutomationServerKind.squashAutom,
      supportsAutomatedExecutionEnvironments: true
    });
    const page = showTestAutomationServerView(getInitialNodes(taServer), mockEnvDataFromServer(taServer));
    page.fetchAvailableEnvironmentsMock.wait();
    page.informationPanel.assertSquashAUTOMFieldsAreVisible();
  });

  it('should modify Observer URL', () => {
    const taServer = makeTAServerViewData({
      kind: TestAutomationServerKind.squashAutom,
      supportsAutomatedExecutionEnvironments: true
    });
    const page = showTestAutomationServerView(getInitialNodes(taServer), mockEnvDataFromServer(taServer));
    page.fetchAvailableEnvironmentsMock.wait();

    const observerUrlField = page.informationPanel.observerUrlField;

    observerUrlField.checkCustomPlaceholder(taServer.baseUrl);
    observerUrlField.setAndConfirmValue('http://my.url/');
    observerUrlField.checkContent('http://my.url/');
  });
});

describe('Administration Workspace - Test automation server - Environment variable panel', function () {
  it('should not show environment variable panel for a Jenkins server', () => {
    const page = showTestAutomationServerView();
    page.environmentVariablePanel.assertNotExist();
  });

  it('should display environment variable panel', () => {
    const taServer = makeTAServerWithEnvironmentVariables();
    const page = showTestAutomationServerView(getInitialNodes(taServer), mockEnvDataFromServer(taServer));
    const environmentPanel = page.environmentVariablePanel.environmentSelectionPanel;
    page.showEnvironmentVariablePanel('environment-variables');
    environmentPanel.boundEnvironmentVariablesGrid.assertExist();
    environmentPanel.boundEnvironmentVariablesGrid.findRowId('name', 'environmentVariable1').then(rowId =>
      environmentPanel.boundEnvironmentVariablesGrid.getRow(rowId)
        .cell('value').textRenderer()
        .assertContainText('ev-value'));
  });

  it('should modify environment variable', () => {
    const taServer = makeTAServerWithEnvironmentVariables();
    const page = showTestAutomationServerView(getInitialNodes(taServer), mockEnvDataFromServer(taServer));


    const environmentVariablePanel = page.environmentVariablePanel.environmentSelectionPanel;
    page.showEnvironmentVariablePanel('environment-variables');

    environmentVariablePanel.updateSimpleEnvironmentVariableValue('simple update', 'environmentVariable1',
      'value', 'test-automation-servers/1/environment-variables/value/1');

    environmentVariablePanel.showEnvironmentVariableOptionsList(2, 'environmentVariable2', 'value');
    environmentVariablePanel.selectUpdateEnvironmentVariableOption('option1');

    environmentVariablePanel.checkCellValue('simple update', 'environmentVariable1', 'value');
    environmentVariablePanel.checkCellValue('option1', 'environmentVariable2', 'value');
  });

  it('should delete one environment variable binding', () => {
    const taServer = makeTAServerWithEnvironmentVariables();
    const page = showTestAutomationServerView(getInitialNodes(taServer), mockEnvDataFromServer(taServer));


    const environmentVariablePanel = page.environmentVariablePanel.environmentSelectionPanel;
    page.showEnvironmentVariablePanel('environment-variables');

    environmentVariablePanel.unbindEnvironmentVariable('environmentVariable1');
  });

  it('should delete multiple environment variable bindings', () => {
    const taServer = makeTAServerWithEnvironmentVariables();
    const page = showTestAutomationServerView(getInitialNodes(taServer), mockEnvDataFromServer(taServer));


    const environmentVariablePanel = page.environmentVariablePanel.environmentSelectionPanel;
    page.showEnvironmentVariablePanel('environment-variables');

    environmentVariablePanel.boundEnvironmentVariablesGrid
      .selectRowsWithStickyIndexColumn(['environmentVariable1', 'environmentVariable2']);
    environmentVariablePanel.unbindMultipleEnvironmentVariable(['environmentVariable1', 'environmentVariable2']);
  });

  it('should add environment variable bindings', () => {
    const taServer = makeTAServerWithEnvironmentVariables();
    const page = showTestAutomationServerView(getInitialNodes(taServer), mockEnvDataFromServer(taServer));


    const environmentVariablePanel = page.environmentVariablePanel.environmentSelectionPanel;
    page.showEnvironmentVariablePanel('environment-variables');

    environmentVariablePanel.bindEnvironmentVariable();
  });
});


function mockEnvDataFromServer(taServer: TestAutomationServer): EnvironmentSelectionPanelDto {
  return {
    server: {
      testAutomationServerId: taServer.id,
      defaultTags: taServer.environmentTags,
      hasServerCredentials: true,
    },
    environments: {
      environments: taServer.availableEnvironments
    },
  };
}

function makeTAServerWithEnvironmentVariables() {
  const environmentVariable1: BoundEnvironmentVariable = {
    id: 1,
    name: 'environmentVariable1',
    code: 'code1',
    inputType: EvInputType.PLAIN_TEXT,
    boundToServer: false,
    options: [],
    value: 'ev-value'
  };

  const option1: EnvironmentVariableOption = {
    evId: 2,
    label: 'option1',
    code: 'code1',
    position: 0,
  };

  const option2: EnvironmentVariableOption = {
    evId: 2,
    label: 'option2',
    code: 'code2',
    position: 1,
  };

  const environmentVariable2: BoundEnvironmentVariable = {
    id: 2,
    name: 'environmentVariable2',
    code: 'code2',
    inputType: EvInputType.DROPDOWN_LIST,
    boundToServer: false,
    options: [option1, option2],
    value: ''
  };

  return makeTAServerViewData({
      supportsAutomatedExecutionEnvironments: true,
      boundEnvironmentVariables: [environmentVariable1, environmentVariable2],
      kind: TestAutomationServerKind.squashAutom
    }
  );
}

function makeTAServerWithEnvironments() {
  const dummyEnvironment1: AutomatedExecutionEnvironment = {
    name: 'dummy',
    tags: ['robot', 'firefox', 'linux'],
  };

  const dummyEnvironment2: AutomatedExecutionEnvironment = {
    name: 'dummy2',
    tags: ['windows', 'chrome', 'robot'],
  };

  const allTags = [...dummyEnvironment1.tags];
  dummyEnvironment2.tags.forEach(tag => {
    if (!allTags.includes(tag)) {
      allTags.push(tag);
    }
  });

  return makeTAServerViewData({
    supportsAutomatedExecutionEnvironments: true,
    availableEnvironments: [dummyEnvironment1, dummyEnvironment2],
    availableEnvironmentTags: allTags,
    environmentTags: ['robot', 'chrome'],
    credentials: makeTokenCredentials(),
  });
}

function makeTokenCredentials(): TokenAuthCredentials {
  return {
    type: AuthenticationProtocol.TOKEN_AUTH,
    implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
    token: 'whatever, dude',
  };
}

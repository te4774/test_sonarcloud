import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {
  RemoveScmServerDialogElement
} from '../../../../page-objects/pages/administration-workspace/dialogs/remove-scm-server-dialog.element';
import {
  AdminWorkspaceScmServersPage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-scm-servers.page';
import {
  NoneScmPluginInstalledAlert
} from '../../../../page-objects/pages/administration-workspace/dialogs/none-scm-plugin-installed-alert.element';
import {selectByDataTestToolbarButtonId} from '../../../../utils/basic-selectors';
import {mockFieldValidationError} from '../../../../data-mock/http-errors.data-mock';
import {
  ScmServerViewPage
} from '../../../../page-objects/pages/administration-workspace/scm-server-view/scm-server-view.page';
import {AdminScmServer} from '../../../../model/scm-server/scm-server.model';
import {AuthenticationPolicy, AuthenticationProtocol} from '../../../../model/bugtracker/bug-tracker.model';
import {NavBarAdminElement} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import {HttpMock, HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {
  BugTrackerViewPage
} from '../../../../page-objects/pages/administration-workspace/bug-tracker-view/bug-tracker-view.page';

function mockAreServersUsedRequest(): HttpMock<Object> {
  return new HttpMockBuilder('scm-server-view/*/are-used-by-test-cases')
    .responseBody({areUsed: false})
    .get()
    .build();
}

describe('Administration Workspace - SCM servers', function () {
  it('should display SCM servers grid', () => {
    const page = AdminWorkspaceScmServersPage.initTestAtPageScmServers(initialNodes);
    const grid = page.grid;

    const firstRow = grid.getRow(1);
    firstRow.cell('name').textRenderer().assertContainText('Github');
    firstRow.cell('kind').textRenderer().assertContainText('git');
    firstRow.cell('url').linkRenderer().assertContainText('https://github.com/SquashRecette');

    const secondRow = grid.getRow(2);
    secondRow.cell('name').textRenderer().assertContainText('Github2');
    secondRow.cell('kind').textRenderer().assertContainText('git');
    secondRow.cell('url').linkRenderer().assertContainText('https://github.com/SquashRecette');

    const thirdRow = grid.getRow(3);
    thirdRow.cell('name').textRenderer().assertContainText('Gitlab');
    thirdRow.cell('kind').textRenderer().assertContainText('git');
    thirdRow.cell('url').linkRenderer().assertContainText('https://gitlab.com/SquashRecette');
  });

  it('should add scm server', () => {
    const page = AdminWorkspaceScmServersPage.initTestAtPageScmServers(initialNodes);
    const httpMockView = new HttpMockBuilder('scm-server-view/4?*')
      .get()
      .responseBody({
        serverId: 4,
        name: 'Github3',
        kind: 'Github',
        url: 'http://localhost:9090',
        authProtocol: AuthenticationProtocol.BASIC_AUTH,
        authPolicy: AuthenticationPolicy.USER,
        committerMail: 'jdoe@test.fr',
        repositories: [
          {
            scmRepositoryId: 1,
            serverId: 1,
            name: 'Repo 1',
            workingBranch: 'master',
            repositoryPath: '/test/repo1',
            workingFolderPath: 'work/repo1'
          }]
      })
      .build();

    const dialog = page.openCreateScmServer();
    dialog.assertExist();
    dialog.fillName('Github3');
    dialog.fillKindField('Github');
    dialog.fillUrl('http://localhost:9090');


    dialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 4},
      gridResponse: addScmServerResponse
    });
    const view = new BugTrackerViewPage();
    httpMockView.wait();
    view.entityNameField.checkContent('Github3');
    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(2);
    grid.assertRowExist(3);
    grid.assertRowExist(4);

    // add another

    const dialogAnother = page.openCreateScmServer();
    dialogAnother.assertExist();
    dialogAnother.fillName('Github3');
    dialogAnother.fillKindField('Github');
    dialogAnother.fillUrl('http://localhost:9090');

    dialogAnother.addWithOptions({
      addAnother: true,
      createResponse: {id: 4},
      gridResponse: addScmServerResponse
    });

    dialogAnother.assertExist();
    dialogAnother.checkIfFormIsEmpty();
    dialogAnother.cancel();
    dialogAnother.assertNotExist();
  });

  it('should validate creation form', () => {
    const page = AdminWorkspaceScmServersPage.initTestAtPageScmServers(initialNodes);

    const dialog = page.openCreateScmServer();
    dialog.assertExist();
    dialog.fillName('Github');
    dialog.fillKindField('Github');
    dialog.fillUrl('http://localhost:9090');

    // should forbid to add a scm server if name already exists
    const nameHttpError = mockFieldValidationError('name', 'sqtm-core.error.generic.name-already-in-use');
    dialog.addWithServerSideFailure(nameHttpError);
    dialog.assertExist();
    dialog.checkIfNameAlreadyInUseErrorMessageIsDisplayed();

    // should forbid to add a scm server with an empty name
    dialog.fillName('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a scm server with an empty url
    dialog.fillName('a');
    dialog.fillUrl('');
    dialog.clickOnAddButton();
    dialog.checkIfRequiredErrorMessageIsDisplayed();

    // should forbid to add a bugtracker with a malformed url
    dialog.fillUrl('www');
    const urlHttpError = mockFieldValidationError('url', 'sqtm-core.exception.wrong-url');
    dialog.addWithServerSideFailure(urlHttpError);
    dialog.assertExist();
    dialog.checkIfMalformedUrlErrorMessageIsDisplayed();
  });

  it('should allow removal of unused SCM server from the grid', () => {
    const page = AdminWorkspaceScmServersPage.initTestAtPageScmServers(initialNodes);
    mockAreServersUsedRequest();
    const firstRow = page.grid.getRow(1);
    firstRow.cell('delete').assertExist();
    firstRow.cell('delete').iconRenderer().click();
    const dialog = new RemoveScmServerDialogElement([1]);
    dialog.assertMessageWhenUnused();
    dialog.deleteForSuccess({
      count: 2,
      dataRows: [initialNodes.dataRows[1], initialNodes.dataRows[2]]
    });
  });

  it('should allow removal of SCM server used by project from the grid', () => {
    const page = AdminWorkspaceScmServersPage.initTestAtPageScmServers(initialNodes);
    mockAreServersUsedRequest();

    const firstRow = page.grid.getRow(3);
    firstRow.cell('delete').assertExist();
    firstRow.cell('delete').iconRenderer().click();
    const dialog = new RemoveScmServerDialogElement([3]);
    dialog.assertMessageWhenScmServerBoundToProject();
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[0], initialNodes.dataRows[1]]
    });
  });

  it('should allow multiple scm servers removal', () => {
    const page = AdminWorkspaceScmServersPage.initTestAtPageScmServers(initialNodes);
    mockAreServersUsedRequest();

    // Select elements to delete
    page.grid.selectRows([1, 2], '#', 'leftViewport');

    // Show dialog
    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    // Confirm
    const dialog = new RemoveScmServerDialogElement([1, 2]);
    dialog.deleteForSuccess({
      count: 1,
      dataRows: [initialNodes.dataRows[2]]
    });
  });

  it('should allow removal of multiple SCM servers used by project from the grid', () => {
    const page = AdminWorkspaceScmServersPage.initTestAtPageScmServers(initialNodes);
    mockAreServersUsedRequest();

    // Select elements to delete
    page.grid.selectRows([1, 2, 3], '#', 'leftViewport');

    // Show dialog
    cy.get(selectByDataTestToolbarButtonId('delete-button')).click();

    // Confirm
    const dialog = new RemoveScmServerDialogElement([1, 2, 3]);

    dialog.assertMessageWhenScmServersBoundToProject();
    dialog.deleteForSuccess({
      count: 0,
      dataRows: []
    });
  });

  it('should inform user that none scm plugin is installed', () => {
    AdminWorkspaceScmServersPage.initTestAtPageScmServersWithoutScmPluginInstalled(initialNodes);

    cy.get(selectByDataTestToolbarButtonId('create-button')).click();

    const alert = new NoneScmPluginInstalledAlert();
    alert.assertMessage();
    alert.close();
  });

  const initialNodes: GridResponse = {
    count: 3,
    dataRows: [
      {
        id: 1,
        children: [],
        data: {
          serverId: 1,
          name: 'Github',
          kind: 'git',
          url: 'https://github.com/SquashRecette',
          projectCount: 0,
        }
      } as unknown as DataRow,
      {
        id: 2,
        children: [],
        data: {
          serverId: 2,
          name: 'Github2',
          kind: 'git',
          url: 'https://github.com/SquashRecette',
          projectCount: 0,
        }
      } as unknown as DataRow,
      {
        id: 3,
        children: [],
        data: {
          serverId: 3,
          name: 'Gitlab',
          kind: 'git',
          url: 'https://gitlab.com/SquashRecette',
          projectCount: 1
        }
      } as unknown as DataRow
    ]
  };

  const addScmServerResponse: GridResponse = {
    count: 4,
    dataRows: [
      ...initialNodes.dataRows,
      {
        id: 4,
        children: [],
        data: {'name': 'Gitlab2'}
      } as unknown as DataRow,
    ]
  };
});


describe('Administration Workspace - Scm server - Information panel', function () {
  it('should set scm server name', () => {
    const page = showScmServerView();
    page.entityNameField.setAndConfirmValue('NEW');
    page.entityNameField.checkContent('NEW');
  });


  it('should set scm server url', () => {
    const page = showScmServerView();
    page.informationPanel.urlField.setAndConfirmValue('http://test');
    page.informationPanel.urlField.checkContent('http://test');
  });
});


describe('Administration Workspace - Scm server - Auth policy panel', function () {
  it('should send credential form', () => {
    const page = showScmServerView();

    page.authPolicyPanel.assertSendButtonDisabled();
    page.authPolicyPanel.usernameField.fill('username');
    page.authPolicyPanel.passwordField.fill('password');
    page.authPolicyPanel.assertSendButtonEnabled();

    page.authPolicyPanel.sendCredentialsForm();
    page.authPolicyPanel.assertSaveSuccessMessageVisible();
  });
});

describe('Administration Workspace - Scm server - Commit policy panel', function () {
  it('should set committer email', () => {
    const page = showScmServerView();
    page.commitPolicyPanel.committerMailField.setAndConfirmValue('admin@test.fr');
    page.commitPolicyPanel.committerMailField.checkContent('admin@test.fr');
  });
});

describe('Administration Workspace - Scm servers - Repositories', function () {
  it('should add a repository', () => {
    const page = showScmServerView();
    page.repositoriesPanel.grid.assertExist();
    const addScmRepositoryDialog = page.repositoriesPanel.clickOnAddRepositoryButton();

    addScmRepositoryDialog.openCreateScmRepository('Repo 3', 'master', '/test/repo3', 'work/repo3');
  });


  it('should delete repository', () => {
    const page = showScmServerView();
    page.repositoriesPanel.grid.assertExist();

    const navBar = new NavBarAdminElement();
    navBar.toggle();
    page.foldGrid();

    page.repositoriesPanel.deleteOne('Repo 1');
  });

  it('should delete multiple repositories', () => {
    const page = showScmServerView();
    page.repositoriesPanel.grid.assertExist();

    const navBar = new NavBarAdminElement();
    navBar.toggle();
    page.foldGrid();

    page.repositoriesPanel.deleteMultiple(['Repo 1', 'Repo 2']);
  });
});


function showScmServerView(): ScmServerViewPage {
  const initialNodes = getInitialNodes();
  const page = AdminWorkspaceScmServersPage.initTestAtPageScmServers(initialNodes);
  return page.selectScmServerByName([initialNodes.dataRows[0].data.name],
    initialNodes.dataRows[0].data as AdminScmServer);
}

function getInitialNodes(): GridResponse {
  const scmServer = getInitialModel();

  return {
    count: 1,
    dataRows: [
      {
        id: scmServer.serverId.toString(), children: [], data: scmServer, allowMoves: true, allowedChildren: [], type: 'Generic'
      } as unknown as DataRow,
    ],
  };
}

function getInitialModel(): AdminScmServer {
  return {
    serverId: 1,
    authProtocol: AuthenticationProtocol.BASIC_AUTH,
    authPolicy: AuthenticationPolicy.USER,
    kind: 'git',
    url: 'http://some.url',
    name: 'github',
    committerMail: 'jdoe@test.fr',
    repositories: [
      {
        scmRepositoryId: 1,
        serverId: 1,
        name: 'Repo 1',
        workingBranch: 'master',
        repositoryPath: '/test/repo1',
        workingFolderPath: 'work/repo1'
      },
      {
        scmRepositoryId: 2,
        serverId: 2,
        name: 'Repo 2',
        workingBranch: 'master',
        repositoryPath: '/test/repo2',
        workingFolderPath: 'work/repo2'
      },
    ],
  };
}

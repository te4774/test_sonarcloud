import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {EditableDateFieldElement} from '../../../../page-objects/elements/forms/editable-date-field.element';
import {
  NO_PROJECT_PERMISSIONS,
  ReferentialDataMockBuilder
} from '../../../../utils/referential/referential-data-builder';
import {ReferentialDataProviderBuilder} from '../../../../utils/referential/referential-data.provider';
import {Permissions} from '../../../../model/permissions/permissions.model';
import {
  Project,
  ProjectView,
} from '../../../../model/project/project.model';
import {
  AdminWorkspaceProjectsPage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {HttpMock, HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {
  RemoveProjectDialog
} from '../../../../page-objects/pages/administration-workspace/dialogs/remove-project-dialog.element';
import {
  CannotRemoveProjectAlert
} from '../../../../page-objects/pages/administration-workspace/dialogs/cannot-remove-project-alert.element';
import {AdminReferentialDataMockBuilder} from '../../../../utils/referential/admin-referential-data-builder';
import {AdminReferentialDataProviderBuilder} from '../../../../utils/referential/admin-referential-data.provider';
import {assertAccessDenied} from '../../../../utils/assert-access-denied.utils';
import {makeProjectViewData} from '../../../../data-mock/administration-views.data-mock';
import {
  selectByDataTestButtonId,
  selectByDataTestFieldId,
  selectByDataTestMenuItemId
} from '../../../../utils/basic-selectors';
import {AdminReferentialData} from '../../../../model/admin-referential-data.model';
import {GridElement} from '../../../../page-objects/elements/grid/grid.element';
import {ProjectViewPage} from '../../../../page-objects/pages/administration-workspace/project-view/project-view.page';

describe('Administration Workspace - Projects', function () {
  const todayDate = new Date();

  const initialNodes: GridResponse = {
    count: 2,
    dataRows: [
      {
        id: '1',
        children: [],
        data: {
          projectId: 1,
          name: 'Project1',
          label: 'label',
          isTemplate: false,
          createdOn: todayDate,
          createdBy: 'JP01',
          lastModifiedOn: todayDate,
          lastModifiedBy: 'JP01',
          hasPermissions: true,
          bugtrackerName: 'BT JIRA',
          executionServer: 'TA server'
        }
      } as unknown as DataRow,
      {
        id: '2',
        children: [],
        data: {
          projectId: 2,
          name: 'Template1',
          label: 'label',
          isTemplate: true,
          createdOn: todayDate,
          createdBy: 'admin',
          lastModifiedOn: todayDate,
          lastModifiedBy: 'JP01',
          hasPermissions: true,
          bugtrackerName: 'BT JIRA',
          executionServer: 'TA server'
        }
      } as unknown as DataRow
    ]
  };

  const addProjectResponse: GridResponse = {
    count: 3,
    dataRows: [
      ...initialNodes.dataRows,
      {
        children: [],
        id: '4',
        data: {
          'name': 'Project 45',
          projectId: 4
        }
      } as unknown as DataRow
    ]
  };

  const addTemplateResponse: GridResponse = {
    count: 2,
    dataRows: [
      ...initialNodes.dataRows,
      {
        id: '4',
        children: [],
        data: {
          projectId: 4,
          'name': 'Template',
          isTemplate: true,
        }
      } as unknown as DataRow
    ]
  };

  it('should forbid access to non-admin users', () => {
    assertAccessDenied('administration-workspace/projects');
  });

  it('should display managed projects for a project manager', () => {
    const refData = new ReferentialDataMockBuilder().withUser({
      admin: false,
      username: 'gerard',
      userId: 99,
      projectManager: true,
      functionalTester: false,
      automationProgrammer: false,
    }).withProjects(
      {
        name: 'project with permissions',
        permissions: {...NO_PROJECT_PERMISSIONS, PROJECT: [Permissions.MANAGEMENT]}
      },
      {
        name: 'project without permissions',
        permissions: NO_PROJECT_PERMISSIONS
      }).build();

    const response = getProjectsAsGridResponse(refData.projects[0]);
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(response, refData);
    page.grid.assertRowExist(1);
  });

  it('should forbid access for project manager without managed project', () => {
    const adminRefData = new AdminReferentialDataMockBuilder().withUser({
      admin: false,
      username: 'gerard',
      userId: 99,
      projectManager: false,
      functionalTester: false,
      automationProgrammer: false,
      firstName: '',
      lastName: ''
    }).build();

    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(adminRefData).build();

    const refData = new ReferentialDataMockBuilder().withUser({
      admin: false,
      username: 'gerard',
      userId: 99,
      projectManager: true,
      functionalTester: false,
      automationProgrammer: false,
    }).withProjects(
      {
        name: 'project without permissions',
        permissions: NO_PROJECT_PERMISSIONS
      },
      {
        name: 'project without permissions2',
        permissions: NO_PROJECT_PERMISSIONS
      }).build();

    const referentialDataProvider = new ReferentialDataProviderBuilder(refData).build();
    const homeWorkspaceMock = new HttpMockBuilder('/home-workspace').build();
    cy.visit(`administration-workspace/projects`);
    adminReferentialDataProvider.wait();
    referentialDataProvider.wait();
    homeWorkspaceMock.wait();
    cy.location('pathname').should('contain', '/home-workspace');
  });

  it('should display projects grid', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const grid = page.grid;

    grid.assertRowExist(1);
    const row = grid.getRow(1);
    row.cell('createdOn').textRenderer()
      .assertContainText(EditableDateFieldElement.dateToDisplayString(todayDate));
    row.cell('createdBy').textRenderer().assertContainText('JP01');
    grid.scrollPosition('right', 'mainViewport');
    row.cell('lastModifiedOn').textRenderer()
      .assertContainText(EditableDateFieldElement.dateToDisplayString(todayDate));
    row.cell('lastModifiedBy').textRenderer().assertContainText('JP01');
    row.cell('hasPermissions').textRenderer().assertContainText('oui');
    row.cell('bugtrackerName').textRenderer().assertContainText('BT JIRA');
    row.cell('executionServer').textRenderer().assertContainText('TA server');


    grid.assertRowExist(2);
    grid.getRow(2).cell('isTemplate').iconRenderer().assertContainIcon('anticon-sqtm-core-administration:template');
  });

  it('should add project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const genericProject = getGenericProject(4, 'Project 45', 'A description',
    'My label',  1, '', false);

    const mock = new HttpMockBuilder<any>('project-view/4?frontEndErrorIsHandled=true')
      .get()
      .responseBody(genericProject)
      .build();

    const statusRequestMock = new HttpMockBuilder('project-view/*/statuses-in-use')
      .get()
      .responseBody(genericProject.statusesInUse)
      .build();

    const dialog = page.openCreateProject();
    dialog.assertExist();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');

    dialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 4},
      gridResponse: addProjectResponse,
    });

    mock.wait();
    statusRequestMock.wait();

    const view = new ProjectViewPage();
    view.assertExist();

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(4);
  });

  it('should add another project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const genericProject = getGenericProject(4, 'Project 45', 'A description',
      'My label',  1, '', false);
    const dialog = page.openCreateProject();
    dialog.assertExist();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');

    const mock = new HttpMockBuilder<any>('project-view/4?frontEndErrorIsHandled=true')
      .get()
      .responseBody(genericProject)
      .build();

    const statusRequestMock = new HttpMockBuilder('project-view/*/statuses-in-use')
      .get()
      .responseBody(genericProject.statusesInUse)
      .build();

    dialog.addWithOptions({
      addAnother: true,
      createResponse: {id: 4},
      gridResponse: addProjectResponse,
    });

    dialog.checkIfFormIsEmpty();

    dialog.cancel();
    dialog.assertNotExist();

    mock.wait();
    statusRequestMock.wait();

    const view = new ProjectViewPage();
    view.assertExist();

    page.grid.assertRowExist(4);
  });

  it('should add project from a template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);

    const genericProject = getGenericProject(4, 'Project 45', 'A description',
      'My label',  1, 'Template1', false);

    const mock = new HttpMockBuilder<any>('project-view/4?frontEndErrorIsHandled=true')
      .get()
      .responseBody(genericProject)
      .build();

    const statusRequestMock = new HttpMockBuilder('project-view/*/statuses-in-use')
      .get()
      .responseBody(genericProject.statusesInUse)
      .build();

    const dialog = page.openCreateProject([{
      id: 2,
      name: 'Template1',
    }]);

    dialog.assertExist();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');
    dialog.selectTemplate('Template1');

    dialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 4},
      gridResponse: addProjectResponse,
    });

    mock.wait();
    statusRequestMock.wait();

    const view = new ProjectViewPage();
    view.assertExist();

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(4);
  });

  it('should forbid to add a project with an empty name', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const dialog = page.openCreateProject();
    dialog.assertExist();
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');

    dialog.clickOnAddButton();

    dialog.checkIfRequiredErrorMessageIsDisplayed();
  });

  it('should forbid to add a project if name already exists', () => {
    const httpError = {
      squashTMError: {
        kind: 'FIELD_VALIDATION_ERROR',
        fieldValidationErrors: [
          {
            fieldName: 'name',
            i18nKey: 'sqtm-core.error.generic.name-already-in-use'
          }
        ]
      },
    };
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const dialog = page.openCreateProject();
    dialog.assertExist();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');

    dialog.addWithServerSideFailure(httpError);
    dialog.assertExist();
    dialog.checkIfNameAlreadyInUseErrorMessageIsDisplayed();
  });

  it('should add template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);

    const genericProject = getGenericProject(4, 'Project 45', 'A description',
      'My label',  1, 'Template1', true);

    const mock = new HttpMockBuilder<any>('project-view/4?frontEndErrorIsHandled=true')
      .get()
      .responseBody(genericProject)
      .build();

    const statusRequestMock = new HttpMockBuilder('project-view/*/statuses-in-use')
      .get()
      .responseBody(genericProject.statusesInUse)
      .build();

    const dialog = page.openCreateTemplate();
    dialog.assertExist();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');

    dialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 4},
      gridResponse: addTemplateResponse,
    });

    mock.wait();
    statusRequestMock.wait();

    const view = new ProjectViewPage();
    view.assertExist();

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(4);
  });

  it('should add template from project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);

    const viewMock: HttpMock<any> = new HttpMockBuilder('project-view/*').build();
    page.grid.selectRow(1, '#', 'leftViewport');
    viewMock.wait();

    const dialog = page.openCreateTemplateFromProject();
    dialog.assertExist();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');

    dialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 4},
      gridResponse: addTemplateResponse,
    });

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(4);
  });

  it('should delete a project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectView = page.selectProjectByName('Project1', makeProjectViewData());

    projectView.clickOnDeleteProjectMenuItem();

    const removeProjectDialog = new RemoveProjectDialog(4);
    removeProjectDialog.deleteForSuccess({
      dataRows: [],
      count: 0,
    });

    page.grid.assertRowCount(0);
  });

  it('should forbid delete a project with data', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectDto = makeProjectViewData();
    projectDto.hasData = true;

    const projectView = page.selectProjectByName('Project1', projectDto);

    projectView.clickOnDeleteProjectMenuItem();

    const alert = new CannotRemoveProjectAlert();
    alert.assertExist();
    alert.clickOnCloseButton();

    page.grid.assertRowCount(2);
  });

  it('should rename a project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectView = page.selectProjectByName('Project1', makeProjectViewData());

    projectView.nameTextField.setAndConfirmValue('New name');
  });

  it('should associate a project to a template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectView = page.selectProjectByName('Project1', makeProjectViewData());

    cy.get(selectByDataTestFieldId('linked-template'))
      .should('contain.text', 'Template1');

    projectView.disassociateProjectFromTemplate();

    cy.get(selectByDataTestFieldId('linked-template'))
      .should('contain.text', 'Aucun');

    const dialog = projectView.openAssociateToTemplateDialog(['Template1', 'Template2']);
    dialog.selectTemplateAndConfirm('Template1');

    cy.get(selectByDataTestFieldId('linked-template'))
      .should('contain.text', 'Template1');
  });

  it('should associate a project to a template with plugin binding', () => {
    const referentialData: AdminReferentialData = new AdminReferentialDataMockBuilder()
      .withTemplateConfigurablePlugins([{
        id: 'my-plugin',
        name: 'My Plugin',
        type: 'my-plugin-type',
      }, {
        id: 'my-plugin2',
        name: 'My Plugin2',
        type: 'my-plugin-type',
      }])
      .build();

    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, referentialData);
    const projectView = page.selectProjectByName('Project1', makeProjectViewData());

    cy.get(selectByDataTestFieldId('linked-template'))
      .should('contain.text', 'Template1');

    projectView.disassociateProjectFromTemplate();

    cy.get(selectByDataTestFieldId('linked-template'))
      .should('contain.text', 'Aucun');

    const dialog = projectView.openAssociateToTemplateDialog(['Template1', 'Template2']);

    dialog.assertBindTemplatePluginConfigurationCheckboxExists('my-plugin');
    dialog.assertBindTemplatePluginConfigurationCheckboxExists('my-plugin2');
  });

  it('should transform a project into a template', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectView = page.selectProjectByName('Project1', makeProjectViewData());

    cy.get(selectByDataTestFieldId('linked-template'))
      .should('contain.text', 'Template1');

    projectView.transformProjectIntoTemplate();

    cy.get(selectByDataTestFieldId('linked-template'))
      .should('not.exist');

    // Check that menu item isn't accessible anymore
    cy.get(selectByDataTestButtonId('menu-button'))
      .should('exist')
      .click()
      .get(selectByDataTestMenuItemId('transform-into-template'))
      .should('not.exist');
  });

  it('should search on multi columns', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const grid = page.grid;
    grid.assertRowCount(2);
    page.fillSearchInput('Temp', {
      count: 1,
      dataRows: [{
        id: '2',
        children: [],
        data: {
          projectId: 2,
          name: 'Template1',
          label: 'label',
          isTemplate: true,
          createdOn: todayDate,
          createdBy: 'admin',
          lastModifiedOn: todayDate,
          lastModifiedBy: 'JP01',
          hasPermissions: true,
          bugtrackerName: 'BT JIRA',
          executionServer: 'TA server'
        }
      } as unknown as DataRow]
    });
    grid.assertRowCount(1);
  });


  it('should add project and bind/copy template plugin configuration', () => {

    const referentialData: AdminReferentialData = new AdminReferentialDataMockBuilder()
      .withTemplateConfigurablePlugins([{
        id: 'my-plugin',
        name: 'My Plugin',
        type: 'my-plugin-type',
      }])
      .build();

    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes, referentialData);

    const genericProject = getGenericProject(4, 'Project 45', 'A description',
      'My label',  2, 'Template', true);

    const mock = new HttpMockBuilder<any>('project-view/4?frontEndErrorIsHandled=true')
      .get()
      .responseBody(genericProject)
      .build();

    const statusRequestMock = new HttpMockBuilder('project-view/*/statuses-in-use')
      .get()
      .responseBody(genericProject.statusesInUse)
      .build();

    const dialog = page.openCreateProject([{
      id: 2, name: 'Template',
    }]);
    dialog.assertExist();
    dialog.fillName('Project 45');
    dialog.fillDescription('A description');
    dialog.fillLabel('My label');
    dialog.selectTemplate('Template');

    dialog.assertCopyTemplatePluginConfigurationCheckboxIsDisabled('my-plugin');
    dialog.getKeepTemplatePluginConfigurationBindingCheckbox('my-plugin').click();
    dialog.assertCopyTemplatePluginConfigurationCheckboxIsEnabled('my-plugin');

    dialog.getCopyTemplatePluginConfigurationCheckbox('my-plugin').click();
    dialog.getKeepTemplatePluginConfigurationBindingCheckbox('my-plugin').click();
    dialog.assertCopyTemplatePluginConfigurationCheckboxIsDisabled('my-plugin');
    dialog.assertCopyTemplatePluginConfigurationCheckboxIsChecked('my-plugin');

    dialog.addWithOptions({
      addAnother: false,
      createResponse: {id: 4},
      gridResponse: addProjectResponse,
    });

    mock.wait();
    statusRequestMock.wait();

    const view = new ProjectViewPage();
    view.assertExist();

    const grid = page.grid;

    grid.assertRowExist(1);
    grid.assertRowExist(4);
  });

  it('should navigate to a contextual view with a given anchor group', () => {
    const {view, workspace} = visitContextualViewWithURL(initialNodes, makeProjectViewData({id: 1}));
    view.assertExist();
    workspace.grid.getRow('1').assertIsSelected();
    view.pluginPanel.assertExist();
  });

  function visitContextualViewWithURL(gridResponse: GridResponse, viewResponse: any)
    : { workspace: AdminWorkspaceProjectsPage, view: ProjectViewPage } {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder().build();
    const gridElement = GridElement.createGridElement('projects', 'generic-projects', gridResponse);
    const view = new ProjectViewPage();
    const viewMock = new HttpMockBuilder('project-view/*')
      .responseBody(viewResponse)
      .build();

    // visit page
    cy.visit(`administration-workspace/projects/${viewResponse.id}/plugins`);
    adminReferentialDataProvider.wait();
    gridElement.waitInitialDataFetch();
    viewMock.waitResponseBody().then(() => {
      view.waitInitialDataFetch();
    });

    return {
      workspace: new AdminWorkspaceProjectsPage(gridElement),
      view,
    };
  }

  function getProjectsAsGridResponse(...projects: Project[]): GridResponse {
    return {
      count: projects.length,
      dataRows: projects.map(project => {
        return {
          id: project.id.toString(),
          children: [],
          data: {
            name: project.name,
            label: project.label,
            isTemplate: false,
            createdOn: todayDate,
            createdBy: 'admin',
            lastModifiedOn: todayDate,
            lastModifiedBy: 'admin',
            hasPermissions: true,
            bugtrackerName: 'BT JIRA',
            executionServer: 'TA server'
          }
        } as unknown as DataRow;
      })
    };
  }

  function getGenericProject(id: number, name: string, description: string,
                             label: string, templateId: number, template: string, isTemplate: boolean): ProjectView {
    return {
      id: id,
      uri: '',
      name: name,
      label: label,
      testCaseNatureId: null,
      testCaseTypeId: null,
      requirementCategoryId: null,
      allowAutomationWorkflow: false,
      customFieldBindings: null,
      permissions: null,
      bugTrackerBinding: null,
      milestoneBindings: null,
      taServerId: null,
      automationWorkflowType: 'NONE',
      disabledExecutionStatus: [],
      description: description,
      keywords: [],
      bddScriptLanguage: 'ENGLISH',
      bddImplementationTechnology: 'CUCUMBER_5_PLUS',
      allowTcModifDuringExec: false,
      activatedPlugins: {TEST_CASE_WORKSPACE: [], CAMPAIGN_WORKSPACE: [], REQUIREMENT_WORKSPACE: []},
      automatedSuitesLifetime: null,
      availableBugtrackers: [],
      bugtrackerProjectNames: null,
      hasData: true,
      allowedStatuses: {SETTLED: false, UNTESTABLE: true},
      statusesInUse: {SETTLED: false, UNTESTABLE: false},
      useTreeStructureInScmRepo: true,
      scmRepositoryId: null,
      infoLists: [],
      templateLinkedToProjects: false,
      partyProjectPermissions: [],
      availableScmServers: [],
      availableTestAutomationServers: [],
      boundMilestonesInformation: [],
      boundTestAutomationProjects: [],
      attachmentList: {id: 7, attachments: []},
      createdOn: new Date(),
      createdBy: 'admin',
      lastModifiedOn: null,
      lastModifiedBy: null,
      linkedTemplate: template,
      linkedTemplateId: templateId,
      template: isTemplate,
      availablePlugins: [],
    };
  }
});



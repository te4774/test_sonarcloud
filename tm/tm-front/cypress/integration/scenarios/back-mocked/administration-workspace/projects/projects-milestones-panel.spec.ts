import {DataRow, GridResponse} from '../../../../model/grids/data-row.type';
import {
  AdminWorkspaceProjectsPage
} from '../../../../page-objects/pages/administration-workspace/admin-workspace-projects.page';
import {ProjectView} from '../../../../model/project/project.model';
import {
  MilestonesResponse
} from '../../../../page-objects/pages/administration-workspace/project-view/dialogs/bind-milestone-to-project.dialog';
import {NavBarAdminElement} from '../../../../page-objects/elements/nav-bar/nav-bar-admin.element';
import {makeMilestoneViewData, makeProjectViewData} from '../../../../data-mock/administration-views.data-mock';
import {
  MilestoneViewDetailPage
} from '../../../../page-objects/pages/administration-workspace/milestone-view/milestone-view-detail.page';

describe('Administration Workspace - Projects - Milestones', function () {
  const todayDate = new Date();

  const initialNodes: GridResponse = {
    count: 1,
    dataRows: [
      {
        id: '1',
        children: [],
        data: {
          projectId: 1,
          name: 'Project1',
          label: 'label',
          isTemplate: false,
          createdOn: todayDate,
          createdBy: 'JP01',
          lastModifiedOn: todayDate,
          lastModifiedBy: 'JP01',
          hasPermissions: true,
          bugtrackerName: 'BT JIRA',
          executionServer: 'TA server'
        }
      } as unknown as DataRow,
    ],
  };

  const availableMilestones: MilestonesResponse = {
    globalMilestones: [
      {
        id: 3,
        label: 'milestone3',
        description: '',
        endDate: todayDate,
        range: 'GLOBAL',
        status: 'PLANNED',
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
        lastModifiedBy: null, lastModifiedOn: null, createdOn: null, createdBy: null,
      },
      {
        id: 4,
        label: 'milestone4',
        description: '',
        endDate: todayDate,
        range: 'GLOBAL',
        status: 'PLANNED',
        ownerFistName: 'admin',
        ownerLastName: 'admin',
        ownerLogin: 'admin',
        lastModifiedBy: null, lastModifiedOn: null, createdOn: null, createdBy: null,
      }
    ],
    personalMilestones: [],
    otherMilestones: [
      {
        id: 5,
        label: 'milestone5',
        description: '',
        endDate: todayDate,
        range: 'RESTRICTED',
        status: 'IN_PROGRESS',
        ownerFistName: 'John',
        ownerLastName: 'Doe',
        ownerLogin: 'admin',
        lastModifiedBy: null, lastModifiedOn: null, createdOn: null, createdBy: null,
      }
    ]
  };

  it('should show milestones count', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto());
    projectViewPage.assertMilestoneCount(2);
  });

  it('should bind milestones to a project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto());
    projectViewPage.assertExist();
    projectViewPage.clickAnchorLink('milestones');
    projectViewPage.milestonesPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const bindMilestoneToProjectDialog = projectViewPage.milestonesPanel.openMilestoneBindingDialog(availableMilestones);
    bindMilestoneToProjectDialog.selectMilestones(['milestone3', 'milestone4'], 'global');
    bindMilestoneToProjectDialog.confirm();
  });


  it('should unbind milestone from project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto());
    projectViewPage.assertExist();
    projectViewPage.clickAnchorLink('milestones');
    projectViewPage.milestonesPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.milestonesPanel.unbindOne('milestone1');
  });

  it('should unbind multiple milestones from project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto());
    projectViewPage.assertExist();
    projectViewPage.clickAnchorLink('milestones');
    projectViewPage.milestonesPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    projectViewPage.milestonesPanel.unbindMultiple(['milestone1', 'milestone2']);
  });

  it('should create and bind milestone to project', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto());
    projectViewPage.assertExist();
    projectViewPage.clickAnchorLink('milestones');
    projectViewPage.milestonesPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const createBindMilestoneDialog = projectViewPage.milestonesPanel.openMilestoneCreateAndBindDialog();
    createBindMilestoneDialog.confirmCreateAndBindToProject('milestone test', {
      milestone: {
        milestone: {
          id: -1,
          label: 'milestone test',
          status: 'PLANNED',
          range: 'GLOBAL',
          endDate: todayDate,
          description: '',
          ownerFistName: 'admin',
          ownerLastName: 'admin',
          ownerLogin: 'admin',
          lastModifiedBy: null, lastModifiedOn: null, createdOn: null, createdBy: null,
        },
        milestoneBoundToOneObjectOfProject: false,
      }
    });
  });

  it('should create and bind milestone to project and objects', () => {
    const page = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = page.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto());
    projectViewPage.assertExist();
    projectViewPage.clickAnchorLink('milestones');
    projectViewPage.milestonesPanel.grid.assertExist();
    new NavBarAdminElement().toggle();
    projectViewPage.foldGrid();

    const createBindMilestoneDialog = projectViewPage.milestonesPanel.openMilestoneCreateAndBindDialog();
    createBindMilestoneDialog.confirmCreateAndBindToProjectAndObjects('milestone test',
      {
        milestone: {
          milestone: {
            id: -1,
            label: 'milestone test',
            status: 'IN_PROGRESS',
            range: 'GLOBAL',
            endDate: todayDate,
            description: '',
            ownerFistName: 'admin',
            ownerLastName: 'admin',
            ownerLogin: 'admin',
            createdBy: null, createdOn: null, lastModifiedOn: null, lastModifiedBy: null,
          },
          milestoneBoundToOneObjectOfProject: true
        }
      }
    );
  });

  it('should navigate to milestone detail page and back', () => {
    const workspacePage = AdminWorkspaceProjectsPage.initTestAtPageProjects(initialNodes);
    const projectViewPage = workspacePage.selectProjectByName(initialNodes.dataRows[0].data.name, getProjectDto());
    projectViewPage.clickAnchorLink('milestones');

    projectViewPage.milestonesPanel.showMilestoneDetail('milestone1', makeMilestoneViewData());
    const milestoneViewDetailPage = new MilestoneViewDetailPage();
    milestoneViewDetailPage.assertExist();

    milestoneViewDetailPage.clickBackButton();
    workspacePage.assertExist();
  });
});

function getProjectDto(): ProjectView {
  return makeProjectViewData({
    boundMilestonesInformation: [
      {
        milestone: {
          id: 1,
          label: 'milestone1',
          description: '',
          endDate: new Date(),
          range: 'GLOBAL',
          status: 'PLANNED',
          ownerFistName: 'admin',
          ownerLastName: 'admin',
          ownerLogin: 'admin',
          createdBy: null, createdOn: null, lastModifiedOn: null, lastModifiedBy: null,
        },
        milestoneBoundToOneObjectOfProject: false
      },
      {
        milestone: {
          id: 2,
          label: 'milestone2',
          description: '',
          endDate: new Date(),
          range: 'GLOBAL',
          status: 'PLANNED',
          ownerFistName: 'admin',
          ownerLastName: 'admin',
          ownerLogin: 'admin',
          createdBy: null, createdOn: null, lastModifiedOn: null, lastModifiedBy: null,
        },
        milestoneBoundToOneObjectOfProject: false
      }
    ],
  });
}

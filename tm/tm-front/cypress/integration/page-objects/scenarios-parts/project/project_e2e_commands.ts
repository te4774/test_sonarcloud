import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {AdminWorkspaceProjectsPage} from '../../pages/administration-workspace/admin-workspace-projects.page';
import {
  AdminPrerequisiteBuilder,
  ProjectBuilder,
  UserBuilder
} from '../../../utils/end-to-end/admin-prerequisite-builder';

export class ProjectPrerequisiteCommands {
  public static createProject(name: string) {
    new AdminPrerequisiteBuilder()
      .withProjects([
        new ProjectBuilder().withName(name)
      ])
      .build();
  }

  public static createProjects(names: string[]) {
    new AdminPrerequisiteBuilder()
      .withProjects(ProjectPrerequisiteCommands.createProjectsBuildersFromNames(names))
      .build();
  }

  private static createProjectsBuildersFromNames(names: string[]): ProjectBuilder[] {
    const projectBuilders = [];
    names.forEach(name => {
      projectBuilders.push(new ProjectBuilder().withName(name));
    });
    return projectBuilders;
  }


  public static verifyProject(name: string) {
    const testCaseWorkspace = NavBarElement.navigateToTestCaseWorkspace();
    testCaseWorkspace.tree.findRowId('NAME', name).then(projectId => {
      testCaseWorkspace.tree.selectNode(projectId);
    });
    const campaignWorkspace = NavBarElement.navigateToCampaignWorkspace();
    campaignWorkspace.tree.findRowId('NAME', name).then(projectId => {
      campaignWorkspace.tree.selectNode(projectId);
    });
    const adminWorkspaceProjectsPage = NavBarElement.navigateToAdministration<AdminWorkspaceProjectsPage>('projects');
    adminWorkspaceProjectsPage.grid.findRowId('name', name).then(projectId => {
      adminWorkspaceProjectsPage.grid.selectRow(projectId, undefined, 'leftViewport');
    });
    cy.goHome();
  }

  // work with 1 project only, projectId is fixed
  public static addAllPermissionsToProject(projectName: string) {
    new AdminPrerequisiteBuilder()
      .withProjects([
        new ProjectBuilder().withName(projectName)
      ])
      .withUsers([
        new UserBuilder().withLogin('Chef de projet').withProfileOnProject(1, 'ProjectManager'),
        new UserBuilder().withLogin('Testeur').withProfileOnProject(1, 'TestRunner'),
        new UserBuilder().withLogin('Automaticien').withProfileOnProject(1, 'AutomatedTestWriter'),
        new UserBuilder().withLogin('Designer de tests').withProfileOnProject(1, 'TestDesigner'),
        new UserBuilder().withLogin('Invité').withProfileOnProject(1, 'ProjectViewer'),
        new UserBuilder().withLogin('Testeur avancé').withProfileOnProject(1, 'AdvanceTester'),
        new UserBuilder().withLogin('Testeur référent').withProfileOnProject(1, 'TestEditor'),
        new UserBuilder().withLogin('Valideur').withProfileOnProject(1, 'Validator'),
      ])
      .build();
  }
}

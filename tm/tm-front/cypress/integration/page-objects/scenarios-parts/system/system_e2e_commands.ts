export class SystemE2eCommands {
  public static setMilestoneActivated() {
    cy.task('executeQuery', 'delete from CORE_CONFIG where STR_KEY LIKE \'feature.milestone.enabled\';').then(
      () => cy.task('executeQuery', 'insert into CORE_CONFIG values (\'feature.milestone.enabled\',\'true\')')
      );
  }
}

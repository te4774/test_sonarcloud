import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {AdminWorkspaceUsersPage} from '../../pages/administration-workspace/admin-workspace-users.page';
import {AdminPrerequisiteBuilder, UserBuilder} from '../../../utils/end-to-end/admin-prerequisite-builder';

export class AdminE2eCommands {
  public static createTeam(name: string, description: string) {
    cy.logInAs('admin', 'admin');
    const adminWorkspaceUsersPage = NavBarElement.navigateToAdministration('users') as AdminWorkspaceUsersPage;
    const adminWorkspaceTeamsPage = adminWorkspaceUsersPage.goToTeamAnchor();
    const createTeamDialog = adminWorkspaceTeamsPage.openCreateTeam();
    createTeamDialog.createTeam(name, description);
    cy.logOut();
  }

  public static createUser(
    login: string, firstName: string, lastName: string, email: string, group: string, password: string, confirmPassword: string) {

    new AdminPrerequisiteBuilder()
      .withUsers([new UserBuilder().withLogin(login)])
      .build();
  }

  public static createUsers(logins: string[]) {
    new AdminPrerequisiteBuilder()
      .withUsers(AdminE2eCommands.createUsersBuildersFromLogin(logins))
      .build();
  }

  private static createUsersBuildersFromLogin(logins: string[]): UserBuilder[] {
    const userBuilders = [];
    logins.forEach(login => {
      userBuilders.push(new UserBuilder().withLogin(login));
    });
    return userBuilders;
  }


}

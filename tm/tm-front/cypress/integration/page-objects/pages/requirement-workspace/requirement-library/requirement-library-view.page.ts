import {EntityViewPage} from '../../page';
import {FolderInformationPanelElement} from '../../../elements/panels/folder-information-panel.element';
import {apiBaseUrl} from '../../../../utils/mocks/request-mock';
import {RequirementStatisticPanelElement} from '../panels/requirement-statistic-panel.element';

export class RequirementLibraryViewPage extends EntityViewPage {

  constructor(private libraryId: number | '*') {
    super('sqtm-app-requirement-library-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/requirement-library-view/${this.libraryId}?**`;
    cy.wait(`@${url}`);
  }

  clickAnchorLink<T>(linkId: RequirementLibraryAnchorLinks, data?: any): T {
    let element: T;
    switch (linkId) {
      case 'information':
        element = this.showInformationPanel(linkId) as unknown as T;
        break;
      case 'dashboard':
        element = this.showDashboard(linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    return element;
  }

  private showInformationPanel<T>(linkId: 'information'): FolderInformationPanelElement {
    this.clickOnAnchorLink(linkId);
    return new FolderInformationPanelElement(this.libraryId);
  }

  private showDashboard(linkId: 'dashboard') {
    this.clickOnAnchorLink(linkId);
    return new RequirementStatisticPanelElement();
  }

  private clickOnAnchorLink(linkId: string) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

}

export type RequirementLibraryAnchorLinks = 'information' | 'dashboard';

import {RequirementStatisticPanelElement} from '../panels/requirement-statistic-panel.element';
import {RequirementStatistics} from '../../../../model/requirements/requirement-statistics.model';
import {Page} from '../../page';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';


export class RequirementMultiSelectionPage extends Page {

  public requirementStatisticPanelElement = new RequirementStatisticPanelElement();
  public statistics: RequirementStatistics;

  constructor(statistics: RequirementStatistics) {
    super('sqtm-app-requirement-workspace-multi-select-view');
    this.statistics = statistics;
  }

  refreshStatistics() {
    const response = {
      'statistics': this.statistics
    };
    const httpMock = new HttpMockBuilder('requirement-workspace-multi-view').post().responseBody(response).build();
    cy.get(`[data-test-refresh-button-id=refresh-button]`).click();
    httpMock.waitResponseBody();
    return new RequirementStatisticPanelElement();
  }
}

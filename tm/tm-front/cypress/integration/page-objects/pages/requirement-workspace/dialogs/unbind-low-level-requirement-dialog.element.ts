import {DeleteConfirmDialogElement} from '../../../elements/dialog/delete-confirm-dialog.element';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

export class UnbindLowLevelRequirementDialogElement extends DeleteConfirmDialogElement {
  highLevelRequirementVersionId: number;
  linkedLowLevelRequirementIds: number[];


  constructor(highLevelRequirementVersionId: number, linkedLowLevelRequirementIds: number[]) {
    super('confirm-delete');
    this.highLevelRequirementVersionId = highLevelRequirementVersionId;
    this.linkedLowLevelRequirementIds = linkedLowLevelRequirementIds;
  }

  deleteForFailure(response: any) {
  }

  deleteForSuccess(response?: any, refreshNodeResponse?: GridResponse) {
    // tslint:disable-next-line:max-line-length
    const removeMock = new HttpMockBuilder<any>(`high-level-requirement/${this.highLevelRequirementVersionId}/unbind-requirement-from-high-level-requirement/${[this.linkedLowLevelRequirementIds]}`)
      .delete().responseBody(response).build();
    const refreshNodeMock = new HttpMockBuilder<any>(`requirement-tree/refresh`)
      .post().responseBody(refreshNodeResponse).build();
    this.clickOnConfirmButton();
    removeMock.wait();
    if (response ) {
      refreshNodeMock.wait();
    }
  }
}

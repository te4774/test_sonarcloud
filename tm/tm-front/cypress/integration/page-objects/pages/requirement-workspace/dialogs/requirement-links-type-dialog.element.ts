import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {selectByDataTestComponentId} from '../../../../utils/basic-selectors';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {ChangeLinkedRequirementOperationReport} from '../../../../model/change-coverage-operation-report';
import {RequirementVersionLink} from '../../../../model/requirements/requirement-version.link';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';
import {GridResponse} from '../../../../model/grids/data-row.type';

export class RequirementLinksTypeDialogElement {

  dialogId = 'requirement-version-link';

  constructor() {
  }

  get selectField() {
    return new SelectFieldElement(CommonSelectors.fieldName('type'));
  }

  assertRequirementNameIs(reqName: string) {
    cy.get(selectByDataTestComponentId('reqVersionName')).should('contain.text', reqName);
  }

  assertRelatedVersionNamesIs(relatedVersionNames: string) {
    cy.get(selectByDataTestComponentId('relatedVersionName')).should('contain.text', relatedVersionNames);
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  confirmLink(response?: ChangeLinkedRequirementOperationReport, refreshNodeResponse?: GridResponse) {
    const mock = new HttpMockBuilder('requirement-version/*/linked-requirement-versions')
      .post()
      .responseBody(response)
      .build();
    const refreshNodeMock = new HttpMockBuilder<any>(`requirement-tree/refresh`)
      .post().responseBody(refreshNodeResponse).build();
    cy.get('[data-test-dialog-button-id="confirm"]').click({force: true});
    mock.wait();
    refreshNodeMock.wait();
  }

  updateLink(requirementVersionLinks: RequirementVersionLink[]) {
    const mock = new HttpMockBuilder('requirement-version/*/linked-requirement-versions/update')
      .post()
      .responseBody(requirementVersionLinks)
      .build();
    cy.get('[data-test-dialog-button-id="confirm"]').click();
    mock.wait();
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }
}

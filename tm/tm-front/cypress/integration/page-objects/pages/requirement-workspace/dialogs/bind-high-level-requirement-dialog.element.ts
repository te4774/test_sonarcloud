import {TreeElement} from '../../../elements/grid/grid.element';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {LinkedHighLevelRequirement} from '../../../../model/requirements/requirement-version.model';

export class BindHighLevelRequirementDialogElement {

    dialogId = 'bind-high-level-requirement-dialog';
    requirementTree : TreeElement;

    constructor(treeResponse: GridResponse) {
        console.log('treeResponse :', treeResponse);
        this.requirementTree = TreeElement.createTreeElement('high-level-requirement-tree-picker', 'requirement-tree', treeResponse);
        console.log('requirementTree :', this.requirementTree);
    }

    assertExist() {
        cy.get(this.buildSelector()).should('exist');
    }

    buildSelector(): string {
        return `[data-test-dialog-id=${this.dialogId}]`;
    }

    confirm(response: LinkedHighLevelRequirement) {
        const mockBuilder = new HttpMockBuilder('high-level-requirement/*/link/*').responseBody(response).post().build();
        cy.get('[data-test-dialog-button-id="confirm"]').click();
        mockBuilder.wait();
    }

    cancel() {
        cy.get('[data-test-dialog-button-id="cancel"]').click();
    }
}

import {EntityViewPage, Page} from '../../page';
import {TestCaseAnchorLinks, TestCaseViewPage} from './test-case-view.page';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {ReferentialData} from '../../../../model/referential-data.model';
import {ReferentialDataProviderBuilder} from '../../../../utils/referential/referential-data.provider';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {NavBarElement} from '../../../elements/nav-bar/nav-bar.element';
import {AlertDialogElement} from '../../../elements/dialog/alert-dialog.element';

export class TestCaseModifyDuringExecProloguePage extends EntityViewPage {

  private testCaseViewPage: TestCaseViewPage;

  constructor(public readonly testCaseId: number | '*') {
    super('sqtm-app-test-case-view-modif-during-exec');
    this.testCaseViewPage = new TestCaseViewPage(testCaseId);
  }

  public checkDataFetched() {
    this.testCaseViewPage.checkDataFetched();
  }

  clickAnchorLink<T extends Page>(linkId: TestCaseAnchorLinks, data?: any): T {
    return this.testCaseViewPage.clickAnchorLink(linkId, data);
  }

  public static initTestAtPage(testCaseModel: TestCaseModel,
                               executionId = 1,
                               referentialData?: ReferentialData): TestCaseModifyDuringExecProloguePage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const url = `test-case-workspace/test-case/modification-during-exec/${testCaseModel.id}/execution/${executionId}`;
    const testCaseModelUrl = `test-case-view/${testCaseModel.id}?**`;
    const mock = new HttpMockBuilder(testCaseModelUrl).responseBody(testCaseModel).build();
    cy.visit(url);
    referentialDataProvider.wait();
    mock.wait();
    new NavBarElement().toggle();
    return new TestCaseModifyDuringExecProloguePage(testCaseModel.id);
  }

  goBackToExec(): AlertDialogElement {
    cy.get(this.selectByComponentId('back-to-execution')).click();
    return new AlertDialogElement();
  }
}

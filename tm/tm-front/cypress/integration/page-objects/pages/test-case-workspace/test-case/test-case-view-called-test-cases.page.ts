import {TestCaseViewPage} from './test-case-view.page';
import {Page} from '../../page';
import {GridElement} from '../../../elements/grid/grid.element';

export class TestCaseViewCalledTestCasesPage extends Page {
  constructor(private parentPage: TestCaseViewPage) {
    super('sqtm-app-called-test-case');
  }

  get testCaseId(): number|string {
    return this.parentPage.testCaseId;
  }

  get callingTestTable(): GridElement {
    return new GridElement('test-case-view-called-test-case');
  }
  checkTableFieldsNames(names: Array<string>) {
    const checkedNames = Array<string>();
    names.push('');
    cy.get(`
    ${this.rootSelector}
    div.sqtm-grid-header-viewport
    `)
      .find('span')
      .each(($el, index, $lis) => {
        checkedNames.push($el.text());
        assert(checkedNames[index] === names[index]);
      });

  }

  checkExistingCallingTest(testName: string) {
    const gridElement = this.callingTestTable as GridElement;
    gridElement.findRowIdNoWithLink('name', testName).then(testId => {
      gridElement.selectRow(testId, '#');
    });
  }

}

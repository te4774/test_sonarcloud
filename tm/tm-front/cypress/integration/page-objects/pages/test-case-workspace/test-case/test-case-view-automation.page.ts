import {Page} from '../../page';
import {TestCaseAutomatableKeys} from '../../../../model/level-enums/level-enum';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {AutomationRequest} from '../../../../model/test-case/automation-request-model';
import {GenericTextFieldElement} from '../../../elements/forms/generic-text-field.element';
import {EditableSelectFieldElement} from '../../../elements/forms/editable-select-field.element';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {TestCaseViewPage} from './test-case-view.page';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import Chainable = Cypress.Chainable;

export class TestCaseViewAutomationPage extends Page {
  constructor(private readonly parentPage: TestCaseViewPage) {
    super('sqtm-app-test-case-view-automation-panel');
    this.assertExist();
  }

  checkAutomatableState(expectedState: TestCaseAutomatableKeys) {
    this.getAutomatableOption(expectedState).find('.ant-radio-checked').should('exist');
  }

  setAutomatable(newState: TestCaseAutomatableKeys, response?: AutomationRequest) {
    const mock = new HttpMockBuilder('test-case/*/automatable')
      .post().responseBody(response).build();

    this.getAutomatableOption(newState).click();

    mock.wait();
  }

  get priority(): EditableTextFieldElement {
    return new EditableTextFieldElement('test-case-automation-priority', 'test-case/*/automation-request/priority');
  }

  get uuid(): GenericTextFieldElement {
    return new GenericTextFieldElement('test-case-automation-uuid');
  }

  get transmittedOn(): GenericTextFieldElement {
    return new GenericTextFieldElement('test-case-automation-transmitted-on');
  }

  get requestStatus(): EditableSelectFieldElement {
    return new EditableSelectFieldElement('test-case-automation-request-status');
  }

  get remoteStatus(): GenericTextFieldElement {
    return new GenericTextFieldElement('test-case-automation-remote-status');
  }

  get syncErrorMessage(): GenericTextFieldElement {
    return new GenericTextFieldElement('tc-automation-sync-error-message');
  }

  get remoteAssignedTo(): GenericTextFieldElement {
    return new GenericTextFieldElement('test-case-automation-assigned-to');
  }

  get remoteUrl(): GenericTextFieldElement {
    return new GenericTextFieldElement('test-case-automation-remote-request-url');
  }

  get automated(): GenericTextFieldElement {
    return new GenericTextFieldElement('test-case-automation-automated');
  }

  transmit(response?: TestCaseModel[]): void {
    const mock = response ?
      new HttpMockBuilder<TestCaseModel[]>('automation-requests/*/status')
        .responseBody(response)
        .post()
        .build()
      : null;

    cy.get(`${this.parentPage.rootSelector} [data-test-button-id="test-case-automation-transmit"]`).click();

    if (mock !== null) {
      mock.wait();
    }
  }

  private getAutomatableOption(automatable: TestCaseAutomatableKeys): Chainable<any> {
    return cy.get(`
      ${this.rootSelector}
      [data-test-field-id="test-case-automatable"]
      label.ant-radio-wrapper`)
      .eq(this.getOptionIndexForAutomatable(automatable));
  }

  private getOptionIndexForAutomatable(expectedState: TestCaseAutomatableKeys) {
    switch (expectedState) {
      case 'M':
        return 0;
      case 'Y':
        return 1;
      case 'N':
        return 2;
    }
  }
}

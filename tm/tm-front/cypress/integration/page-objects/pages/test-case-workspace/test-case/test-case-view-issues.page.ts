import {IssuePage} from '../../../elements/issues/issue.page';

export class TestCaseViewIssuesPage extends IssuePage {
  constructor() {
    super('sqtm-app-issues');
  }

}

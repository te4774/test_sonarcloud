import {DeleteConfirmDialogElement} from '../../../elements/dialog/delete-confirm-dialog.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {DataRow} from '../../../../model/grids/data-row.type';

export class DeleteTreeNodesDialogElement extends DeleteConfirmDialogElement {

  constructor(private treeUrl: string) {
    super('confirm-delete');
  }

  deleteForFailure(response: any) {
  }

  deleteForSuccess(response: any) {
  }

  deleteNodes(deletedNodeIds: number[], parentIds: string[], refreshedNodes: DataRow[]) {
    const refreshMock = new HttpMockBuilder(`${this.treeUrl}/refresh`)
      .post()
      .responseBody({dataRows: refreshedNodes})
      .build();
    const deleteMock = new HttpMockBuilder(`/${this.treeUrl}/${deletedNodeIds.join(',')}`).delete().build();
    this.clickOnConfirmButton();
    deleteMock.wait();
    refreshMock.wait();
  }
}

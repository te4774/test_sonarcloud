import {CreateEntityDialog} from '../../create-entity-dialog.element';
import {ProjectData} from '../../../../model/project/project-data.model';
import {BindableEntity} from '../../../../model/bindable-entity.model';

export class CreateTestCaseFolderDialog extends CreateEntityDialog {

  constructor(project?: ProjectData, domain?: BindableEntity) {
    super({
      treePath: 'test-case-tree',
      viewPath: 'test-case-folder-view',
      newEntityPath: 'new-folder'
    }, project, domain);
  }
}

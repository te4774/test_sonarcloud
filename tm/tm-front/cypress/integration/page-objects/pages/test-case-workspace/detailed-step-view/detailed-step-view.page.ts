import {selectByDataTestComponentId} from '../../../../utils/basic-selectors';
import {TestCaseModel} from '../../../../model/test-case/test-case.model';
import {ReferentialData} from '../../../../model/referential-data.model';
import {ReferentialDataProviderBuilder} from '../../../../utils/referential/referential-data.provider';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {CoverageTable, TreeElement} from '../../../elements/grid/grid.element';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {ToolbarMenuButtonElement} from '../../../elements/workspace-common/toolbar.element';
import {SimpleDeleteConfirmDialogElement} from '../../../elements/dialog/simple-delete-confirm-dialog.element';
import {StepCounterElement} from '../../../elements/execution-runner/step-counter.element';
import {EditableRichTextFieldElement} from '../../../elements/forms/editable-rich-text-field.element';
import {NavBarElement} from '../../../elements/nav-bar/nav-bar.element';
import {EntityViewPage, Page} from '../../page';
import {EMPTY_PARAM_RESPONSE} from '../../../../utils/mocks/common-responses';
import {UploadSummary} from '../../../../model/attachment/upload-summary.model';
import {AttachmentUtils} from '../../../../utils/attachments/attachment.utils';
import {AttachmentCompactListElement} from '../../../elements/attachments/attachment-compact-list.element';
import {CreateActionStepFormElement} from '../../../elements/test-steps/create-action-step-form.element';
import {ChangeCoverageOperationReport} from '../../../../model/change-coverage-operation-report';
import {navigateToRequirementSearchForCoverage} from '../../requirement-workspace/search/search-page-utils';
import Chainable = Cypress.Chainable;

export class DetailedStepViewPage extends EntityViewPage {

  readonly coverageTable: CoverageTable;
  readonly stepCounter: StepCounterElement;
  private readonly actionField: EditableRichTextFieldElement;
  private readonly resultField: EditableRichTextFieldElement;
  attachmentCompactList: AttachmentCompactListElement;


  constructor(private testCaseId: string | '*' = '*') {
    super('sqtm-app-detailed-test-step-view');
    this.coverageTable = new CoverageTable();
    this.stepCounter = new StepCounterElement();
    this.actionField = new EditableRichTextFieldElement('action');
    this.resultField = new EditableRichTextFieldElement('result');
    this.attachmentCompactList = new AttachmentCompactListElement();
  }

  public static initTestAtPage(stepIndex: number, testCaseModel: TestCaseModel, referentialData?: ReferentialData): DetailedStepViewPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const url = `detailed-test-step/${testCaseModel.id}/step/${stepIndex}`;
    const testCaseModelUrl = `detailed-test-step-view/${testCaseModel.id}?**`;
    const mock = new HttpMockBuilder(testCaseModelUrl).responseBody(testCaseModel).build();
    cy.visit(url);
    referentialDataProvider.wait();
    mock.wait();
    new NavBarElement().toggle();
    return new DetailedStepViewPage(testCaseModel.id.toString());
  }

  clickAnchorLink(linkId: string): Page {
    throw new Error('Method not implemented.');
  }

  checkTestCaseName(expectedName: string) {
    cy.get(this.rootSelector)
      .find(selectByDataTestComponentId('test-case-name'))
      .should('contain.text', expectedName);
  }

  showAddRequirementCoverageDrawer(response?: GridResponse) {
    const requirementTree = TreeElement.createTreeElement('requirement-tree-picker', 'requirement-tree', response);
    cy.get(this.rootSelector)
      .find(selectByDataTestComponentId('add-coverages'))
      .click()
      // removing nzTooltip by mouse leave
      .trigger('mouseleave');
    requirementTree.waitInitialDataFetch();
    return requirementTree;

  }

  enterIntoRequirementDropZone() {
    this.getRequirementDropZone()
      .trigger('mouseenter', {force: true})
      .should('have.class', 'drop-requirement');
  }

  private getRequirementDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(selectByDataTestComponentId(DETAILED_VIEW_REQ_DROP_ZONE_ID));
  }

  dropRequirement(testCaseId?: number, testStepId?: number, operationReport?: ChangeCoverageOperationReport) {
    const testCaseIdAsString = testCaseId ? testCaseId.toString() : '*';
    const testStepIdAsString = testStepId ? testStepId.toString() : '*';
    const url = `test-cases/${testCaseIdAsString}/steps/${testStepIdAsString}/verified-requirements`;
    const mock = new HttpMockBuilder(url)
      .post()
      .responseBody(operationReport)
      .build();
    this.getRequirementDropZone().trigger('mouseup', {force: true});
    mock.wait();
    this.getRequirementDropZone().should('not.have.class', 'drop-requirement');
  }

  closeRequirementDrawer() {
    cy.get('.ant-drawer-body .anticon-close').click();
  }


  deleteOneCoverageFromTestStep(requirementVersionId: number, stepId = '*', coverages = []) {
    const mock = new HttpMockBuilder(`test-cases/${this.testCaseId}/steps/${stepId}/verified-requirement-versions/${requirementVersionId}`)
      .delete()
      .responseBody({coverages})
      .build();
    this.coverageTable.getRow(requirementVersionId).cell('linkedToCurrentStep').checkBoxRender().findCheckbox().click();
    mock.wait();
  }

  linkRequirementVersionToCurrentStep(requirementVersionId: number, stepId = '*') {
    const mock = new HttpMockBuilder(`test-steps/${stepId}/verified-requirement-versions/${requirementVersionId}`)
      .post()
      .build();
    this.coverageTable.getRow(requirementVersionId).cell('linkedToCurrentStep').checkBoxRender().findCheckbox().click();
    mock.wait();
  }

  deleteSelectedCoverageFromTestCase(ids: number[], response = {coverages: []}) {
    this.coverageTable.selectRows(ids);
    const toolbarMenuButtonElement = new ToolbarMenuButtonElement(
      selectByDataTestComponentId('coverages-actions'),
      'remove-coverages',
      'remove-coverages');

    toolbarMenuButtonElement.showMenu().item('delete-requirement-coverage-from-test-case').click();
    const idsAsString = ids.length > 0 ? ids.join(',') : '*';
    const mock = new HttpMockBuilder(`test-cases/${this.testCaseId}/verified-requirement-versions/${idsAsString}`)
      .delete()
      .responseBody(response)
      .build();
    new SimpleDeleteConfirmDialogElement().clickOnConfirmButton();
    mock.wait();
  }

  deleteSelectedCoverageFromTestStep(ids: number[], stepId = '*', response = {coverages: []}) {
    this.coverageTable.selectRows(ids);
    const toolbarMenuButtonElement = new ToolbarMenuButtonElement(
      selectByDataTestComponentId('coverages-actions'),
      'remove-coverages',
      'remove-coverages');

    toolbarMenuButtonElement.showMenu().item('delete-requirement-coverage-from-test-step').click();
    const idsAsString = ids.length > 0 ? ids.join(',') : '*';
    const mock = new HttpMockBuilder(`test-cases/${this.testCaseId}/steps/${stepId}/verified-requirement-versions/${idsAsString}`)
      .delete()
      .responseBody(response)
      .build();
    new SimpleDeleteConfirmDialogElement().clickOnConfirmButton();
    mock.wait();
  }

  assertActionContains(expectedAction: string) {
    this.actionField.checkHtmlContent(expectedAction);
  }

  assertResultContains(expectedResult: string) {
    this.resultField.checkHtmlContent(expectedResult);
  }

  assertRequirementVersionIsCoveredByTestCase(requirementVersionId: number) {
    this.coverageTable.getRow(requirementVersionId).assertExist();
  }

  assertRequirementVersionIsNotCoveredByTestCase(requirementVersionId: number) {
    this.coverageTable.getRow(requirementVersionId).assertNotExist();
  }

  assertRequirementVersionIsCoveredByCurrentStep(requirementVersionId: number) {
    this.getCoveredByCurrentStepCell(requirementVersionId).assertIsCheck();
  }

  assertRequirementVersionIsNotCoveredByCurrentStep(requirementVersionId: number) {
    this.getCoveredByCurrentStepCell(requirementVersionId).assertIsNotCheck();
  }

  changeAction(action: string, testStepId?: number) {
    const mock = new HttpMockBuilder(`test-steps/${testStepId || '*'}`)
      .post()
      .responseBody(EMPTY_PARAM_RESPONSE)
      .build();
    this.actionField.setAndConfirmValue(action);
    mock.wait();
  }

  changeResult(expectedResult: string, testStepId?: number) {
    const mock = new HttpMockBuilder(`test-steps/${testStepId || '*'}`)
      .post()
      .responseBody(EMPTY_PARAM_RESPONSE)
      .build();
    this.resultField.setAndConfirmValue(expectedResult);
    mock.wait();
  }

  private getCoveredByCurrentStepCell(requirementVersionId: number) {
    return this.coverageTable.getRow(requirementVersionId).cell('linkedToCurrentStep').checkBoxRender();
  }

  addAttachments(files: File[], attachmentListId?: number, uploadSummary?: UploadSummary[]) {
    AttachmentUtils.addAttachments(selectByDataTestComponentId('attachment-drop-zone'), files, attachmentListId, uploadSummary);
  }

  showCreateActionStepForm(): CreateActionStepFormElement {
    cy.get(selectByDataTestComponentId('show-create-action-step-form')).click();
    return new CreateActionStepFormElement();
  }

  assertAddStepButtonNotVisible() {
    cy.get(selectByDataTestComponentId('show-create-action-step-form')).should('not.exist');
  }

  returnToPreviousPage() {
    cy.get(selectByDataTestComponentId('return-to-previous-page-button')).click();
  }

  // /backend/test-cases/3/steps/1
  deleteCurrentStep(testCaseId?: number, stepId?: number) {
    cy.get(selectByDataTestComponentId('remove-step')).click();
    const deleteConfirmDialogElement = new SimpleDeleteConfirmDialogElement();
    const httpMock = new HttpMockBuilder(`/test-cases/${testCaseId || '*'}/steps/${stepId || '*'}`)
      .delete()
      .responseBody(EMPTY_PARAM_RESPONSE)
      .build();
    deleteConfirmDialogElement.clickOnConfirmButton();
    httpMock.wait();
  }

  navigateToRequirementSearchForCoverage() {
    return navigateToRequirementSearchForCoverage();
  }
}

const DETAILED_VIEW_REQ_DROP_ZONE_ID = 'requirement-drop-zone';





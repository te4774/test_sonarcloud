import {EntityViewPage, Page} from '../../page';
import {ReferentialData} from '../../../../model/referential-data.model';
import {ReferentialDataProviderBuilder} from '../../../../utils/referential/referential-data.provider';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {NavBarElement} from '../../../elements/nav-bar/nav-bar.element';
import {AlertDialogElement} from '../../../elements/dialog/alert-dialog.element';
import {
  ActionStepExecViewModel,
  ModifDuringExecModel
} from '../../../../model/modif-during-exec/modif-during-exec.model';
import {mockActionStepExecViewModel, mockModifDuringExecModel} from '../../../../data-mock/modif-during-exec.data-mock';

export class TestStepModifyDuringExecPage extends EntityViewPage {

  constructor() {
    super('sqtm-app-modif-during-exec-step-view');
  }

  public checkDataFetched() {
    throw Error('unsupported operation exception');
  }

  clickAnchorLink<T extends Page>(linkId: any, data?: any): T {
    throw Error('unsupported operation exception');
  }

  assertTestCaseNameContains(expectedName: string) {
    cy.get(this.selectByComponentId('test-case-name')).should('contain.text', expectedName);
  }

  assertCalledTestCaseNameContains(expectedName: string) {
    cy.get(this.selectByComponentId('called-test-case-name')).should('contain.text', expectedName);
  }

  assertCalledTestCaseNameNotVisible() {
    cy.get(this.selectByComponentId('called-test-case-name')).should('not.exist');
  }

  public static initTestAtPage(modifDuringExecModel: Partial<ModifDuringExecModel>,
                               actionStepExecViewModel: Partial<ActionStepExecViewModel>,
                               executionId = 1,
                               executionStepId = 1,
                               referentialData?: ReferentialData): TestStepModifyDuringExecPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const modifDuringExecModelMockUrl = `execution/${executionId}/modification-during-execution`;
    const actionStepExecViewModelMockUrl = `execution/${executionId}/modification-during-execution/action-step/${executionStepId}`;
    const pageUrl = `modif-during-exec/execution/${executionId}/step/${executionStepId}`;
    const modifDuringExecModelMock = new HttpMockBuilder(modifDuringExecModelMockUrl)
      .responseBody(mockModifDuringExecModel(modifDuringExecModel))
      .build();

    const actionStepExecViewModelMock = new HttpMockBuilder(actionStepExecViewModelMockUrl)
      .responseBody(mockActionStepExecViewModel(actionStepExecViewModel))
      .build();

    cy.visit(pageUrl);
    referentialDataProvider.wait();
    modifDuringExecModelMock.wait();
    actionStepExecViewModelMock.wait();
    new NavBarElement().toggle();
    return new TestStepModifyDuringExecPage();
  }

  goBackToExec(): AlertDialogElement {
    cy.get(this.selectByComponentId('back-to-execution')).click();
    return new AlertDialogElement();
  }
}

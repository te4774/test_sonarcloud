import {Page} from '../../page';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {ReferentialData} from '../../../../model/referential-data.model';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder
} from '../../../../utils/referential/referential-data.provider';
import {GridElement} from '../../../elements/grid/grid.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {ToggleIconElement} from '../../../elements/workspace-common/toggle-icon.element';
import {ChangeVerifyingTestCaseOperationReport} from '../../../../model/change-coverage-operation-report';
import {TestCaseSearchModel} from './test-case-search-model';

export class TestCaseForCoverageSearchPage extends Page {

  private linkSelectionButton = new ToggleIconElement('link-selection-button');
  private linkAllButton = new ToggleIconElement('link-all-button');
  private cancelButton = new ToggleIconElement('cancel-button');

  constructor(public grid: GridElement) {
    super('sqtm-app-test-case-for-coverage-search-page');
  }

  public static initTestAtPage(
    requirementVersionId: string,
    referentialData: ReferentialData = basicReferentialData,
    initialRows: GridResponse = {dataRows: []},
    requirementSearchModel: TestCaseSearchModel = {
      usersWhoCreatedTestCases: [],
      usersWhoModifiedTestCases: []
    },
    queryString: string = ''): TestCaseForCoverageSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = new HttpMockBuilder<TestCaseSearchModel>('search/test-case').responseBody(requirementSearchModel).build();
    const grid = GridElement.createGridElement('test-case-search', 'search/test-case', initialRows);
    // visit page
    cy.visit(`search/test-case/coverage/${requirementVersionId}?${queryString}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new TestCaseForCoverageSearchPage(grid);
  }

  clickAddCriteria() {
    cy.get('div[data-test-component-id="grid-filter-manager-add-criteria"]')
      .click();
  }

  selectRowsWithMatchingCellContent(cellId: string, contents: string[]) {
    this.grid.selectRowsWithMatchingCellContent(cellId, contents);
  }

  selectRowWithMatchingCellContent(cellId: string, content: string) {
    this.grid.selectRowWithMatchingCellContent(cellId, content);
  }

  foldFilterPanel() {
    cy.get(this.selectByComponentId('fold-filter-panel-button')).click();
  }

  assertLinkSelectionButtonExist() {
    this.linkSelectionButton.assertExist();
  }

  assertLinkSelectionButtonIsActive() {
    this.linkSelectionButton.assertIsActive();
  }

  assertLinkSelectionButtonIsNotActive() {
    this.linkSelectionButton.assertIsNotActive();
  }

  assertLinkAllButtonExist() {
    this.linkAllButton.assertExist();
  }

  assertLinkAllButtonIsActive() {
    this.linkAllButton.assertIsActive();
  }

  assertNavigateBackButtonExist() {
    this.cancelButton.assertExist();
  }

  assertNavigateBackButtonIsActive() {
    this.cancelButton.assertIsActive();
  }

  linkSelection(
    requirementVersionId = '*',
    response: ChangeVerifyingTestCaseOperationReport = {
      verifyingTestCases: [],
      summary: {}
    }) {
    const mock = this.buildLinkRequestMock(requirementVersionId, response);
    this.linkSelectionButton.click();
    mock.wait();
  }

  linkAll(
    testCaseId = '*',
    response: ChangeVerifyingTestCaseOperationReport = {
      verifyingTestCases: [],
      summary: {}
    }) {
    const mock = this.buildLinkRequestMock(testCaseId, response);
    this.linkAllButton.click();
    mock.wait();
  }

  private buildLinkRequestMock(testCaseId: string, response: ChangeVerifyingTestCaseOperationReport) {
    return new HttpMockBuilder(`/requirement-version/${testCaseId}/verifying-test-cases`).post()
      .responseBody(response)
      .build();
  }
}

import {Page} from '../../page';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {ReferentialData} from '../../../../model/referential-data.model';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder
} from '../../../../utils/referential/referential-data.provider';
import {GridElement} from '../../../elements/grid/grid.element';
import {MassEditTestCaseDialog} from './dialogs/mass-edit-test-case-dialog.element';
import {NoWritingRightsDialogElement} from './dialogs/no-writing-rights-dialog.element';
import {NoLineWritingRightsDialogElement} from './dialogs/no-line-writing-rights-dialog.element';
import {EditMilestonesDialog} from './dialogs/edit-milestones-dialog';
import {MilestoneMassEdit} from '../../../../model/milestone/milestone.model';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {TestCaseSearchModel} from './test-case-search-model';
import {selectByDataTestToolbarButtonId} from '../../../../utils/basic-selectors';

export class TestCaseSearchPage extends Page {

  constructor(public grid: GridElement) {
    super('sqtm-app-test-case-search-page');
  }

  public static initTestAtPage
  (
    referentialData: ReferentialData = basicReferentialData,
    initialNodes: GridResponse = {dataRows: []},
    testCaseResearchModel: TestCaseSearchModel = {usersWhoCreatedTestCases: [], usersWhoModifiedTestCases: []},
    queryString: string = ''
  ): TestCaseSearchPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = new HttpMockBuilder<TestCaseSearchModel>('search/test-case').responseBody(testCaseResearchModel).build();
    const grid = GridElement.createGridElement('test-case-search', 'search/test-case', initialNodes);
    // visit page
    cy.visit(`search/test-case?${queryString}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    pageDataMock.wait();
    // wait for initial tree data request to fire
    return new TestCaseSearchPage(grid);
  }

  showMassEditDialog(): MassEditTestCaseDialog {
    const editButton = cy.get(`${this.buildToolBarSelector()} ${selectByDataTestToolbarButtonId('mass-edit-button')}`);
    editButton.should('exist');
    editButton.click();
    return new MassEditTestCaseDialog();
  }

  showEditMilestonesDialog(milestoneMassEdit?: MilestoneMassEdit): EditMilestonesDialog {
    const mock = new HttpMockBuilder('search/milestones/test-case/*').responseBody(milestoneMassEdit).build();
    const milestoneButton = cy.get(`${this.buildToolBarSelector()} ${selectByDataTestToolbarButtonId('edit-milestones-button')}`);
    milestoneButton.should('exist');
    milestoneButton.click();
    mock.wait();
    return new EditMilestonesDialog();
  }

  getNoWritingRightsDialog(): NoWritingRightsDialogElement {
    return new NoWritingRightsDialogElement();
  }

  getNoLineWritingRightsDialog(): NoLineWritingRightsDialogElement {
    return new NoLineWritingRightsDialogElement();
  }

  buildToolBarSelector(): string {
    return '[data-test-toolbar-id=test-case-research-toolbar]';
  }

  clickAddCriteria() {
    cy.get('div[data-test-component-id="grid-filter-manager-add-criteria"]')
      .click();
  }

  deleteCriteriaByLabel(label: string) {
    cy.get('div[data-test-component-id="grid-filter-field"] > div > sqtm-core-default-value-renderer')
      .contains(label)
      .closest('sqtm-core-filter-field')
      .children('div')
      .find('span')
      .find('svg')
      .click({force: true});
  }

  selectCriteriaByLabel(label: string, regEx: boolean, gridResponse?: GridResponse) {
    this.grid.declareRefreshData(gridResponse);

    if (regEx) {
      const newLabel: RegExp = new RegExp(label);
      cy.get('sqtm-core-select-filters div[data-test-component-id="item"]')
        .contains('span', newLabel)
        .as('element');
    } else {
      cy.get('sqtm-core-select-filters div[data-test-component-id="item"]')
        .contains('span', label)
        .as('element');
    }

    cy.get('@element')
      .siblings('sqtm-core-simple-checkbox')
      .find('span.ant-checkbox')
      .click()
      .then(() => {
        this.grid.waitForRefresh();
      });
  }


  selectRowsWithMatchingCellContent(cellId: string, contents: string[]) {
    this.grid.selectRowsWithMatchingCellContent(cellId, contents);
  }

  selectRowWithMatchingCellContent(cellId: string, content: string) {
    this.grid.selectRowWithMatchingCellContent(cellId, content);
  }
}

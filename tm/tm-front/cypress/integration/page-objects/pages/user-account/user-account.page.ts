import {Page} from '../page';
import {HttpMock, HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {UserAccount} from '../../../model/user/user-account.model';
import {GridElement} from '../../elements/grid/grid.element';
import Chainable = Cypress.Chainable;

export class UserAccountPage extends Page {

  readonly permissionsGrid: GridElement;

  private initialMock: HttpMock<UserAccount>;

  constructor() {
    super('sqtm-app-user-account-page');

    this.permissionsGrid = GridElement.createGridElement('project-permissions');
  }

  declareInitialData(userAccount: UserAccount): void {
    this.initialMock = new HttpMockBuilder<UserAccount>('user-account')
      .responseBody(userAccount)
      .build();
  }

  checkDataFetched(): void {
    this.initialMock.wait();
  }

  assertName(expected: string): void {
    this.getField('name').should('contain.text', expected);
  }

  assertEmail(expected: string): void {
    this.getField('email').should('contain.text', expected);
  }

  assertGroup(expected: string): void {
    this.getField('group').should('contain.text', expected);
  }

  assertBugTrackerLogin(expected: string): void {
    this.getField('credentials-username')
      .find('input')
      .should('have.value', expected);
  }

  assertBugTrackerRevokeButtonVisible(): void {
    cy.get(`${this.rootSelector} [data-test-component-id="revoke-credentials-button"`)
      .should('be.visible');
  }

  private getField(fieldName: string): Chainable {
    return cy.get(`${this.rootSelector} [data-test-field-id=${fieldName}]`);
  }

  selectEditedBugTracker(bugTrackerName: string): void {
    this.getField('edited-bugtracker')
      .click()
      .get('.ant-select-item-option-content')
      .contains(bugTrackerName)
      .click();
  }
}

import {Page} from '../page';
import {ReferentialData} from '../../../model/referential-data.model';
import {ReferentialDataProviderBuilder} from '../../../utils/referential/referential-data.provider';
import {ExecutionModel} from '../../../model/execution/execution.model';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {GridResponse} from '../../../model/grids/data-row.type';
import {InputType} from '../../../model/customfield/customfield.model';
import {EditableRichTextFieldElement} from '../../elements/forms/editable-rich-text-field.element';
import {GridElement} from '../../elements/grid/grid.element';
import {ExecutionScenarioPanelElement} from './panels/execution-scenario-panel.element';
import {selectByDataTestToolbarButtonId} from '../../../utils/basic-selectors';
import {ExecutionHistoryPanelElement} from './panels/execution-history-panel.element';

export class ExecutionPage extends Page {

  public readonly commentField: EditableRichTextFieldElement;
  public readonly automatedExecutionResultSummaryField: EditableRichTextFieldElement;
  public coverageGrid: GridElement;
  public issueGrid: GridElement;
  public historyGrid: GridElement;
  public readonly scenarioPanel: ExecutionScenarioPanelElement;
  public readonly historyPanel: ExecutionHistoryPanelElement;

  constructor() {
    super('sqtm-app-execution-page');

    const urlPrefix = 'execution/*/';
    this.commentField = new EditableRichTextFieldElement('execution-comment', urlPrefix + 'comment');
    this.automatedExecutionResultSummaryField = new EditableRichTextFieldElement('automated-execution-result-summary');
    this.coverageGrid = new GridElement('execution-view-coverages');
    this.issueGrid = new GridElement('execution-view-issues');
    this.historyGrid = new GridElement('execution-page-history');
    this.scenarioPanel = new ExecutionScenarioPanelElement();
    this.historyPanel = new ExecutionHistoryPanelElement();
  }

  public static initTestAtPage(executionId: number,
                               executionModel?: ExecutionModel,
                               issuesGridResponse?: GridResponse,
                               historyGridResponse?: GridResponse,
                               referentialData?: ReferentialData) {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const executionModelMock = new HttpMockBuilder<ExecutionModel>(`execution/${executionId}?*`).responseBody(executionModel).build();
    const getIssuePanelMock = new HttpMockBuilder('issues/execution/*?frontEndErrorIsHandled=true')
      .responseBody(getAuthenticatedIssuePanelResponse())
      .build();

    const getKnownIssuesMock = new HttpMockBuilder('issues/execution/*/known-issues')
      .post()
      .responseBody(issuesGridResponse)
      .build();


    if (historyGridResponse) {
      new HttpMockBuilder('iteration/*/test-plan/*/executions')
        .post()
        .responseBody(historyGridResponse)
        .build();
    }

    cy.visit(`execution/${executionId}`);

    referentialDataProvider.wait();
    executionModelMock.wait();
    if (issuesGridResponse) {
      getIssuePanelMock.wait();
      getKnownIssuesMock.wait();
    }

    return new ExecutionPage();
  }

  checkExecutionMode(executionMode: string) {
    cy.get('[data-test-component-id="execution-mode"]').should('contain.text', executionMode);
  }

  checkExecutionStatus(executionStatus: string) {
    cy.get('[data-test-component-id="execution-status"]').should('contain.text', executionStatus);
  }

  checkJobUrl(jobUrl: string) {
    cy.get('[data-test-component-id="job-url"]').should('contain.text', jobUrl);
  }

  checkJobResult(jobResult: string) {
    cy.get('[data-test-component-id="job-result"]').should('contain.text', jobResult);
  }

  assertJobUrlCapsuleNotExists() {
    cy.get('[data-test-component-id="job-url"]').should('not.exist');
  }

  assertJobResultCapsuleNotExists() {
    cy.get('[data-test-component-id="job-result"]').should('not.exist');
  }

  checkName(executionName: string) {
    cy.get('[data-test-component-id="execution-title"]').should('contain.text', executionName);
  }

  checkStatus(expectedStatus: string) {
    cy.get('sqtm-app-denormalized-status').should('contain.text', expectedStatus);
  }

  checkImportance(expectedImportance: string) {
    cy.get('sqtm-app-denormalized-importance').should('contain.text', expectedImportance);
  }

  checkNature(expectedNature: string) {
    cy.get('span[data-test-component-id="execution-nature"]').should('contain.text', expectedNature);
  }

  checkNatureIcon(iconName: string): void {
    cy.get('span[data-test-component-id="execution-nature"]')
      .siblings('.sqtm-core-infolist-icon')
      .should('have.class', `anticon-sqtm-core-infolist-item:${iconName}`);
  }

  checkType(expectedType: string) {
    cy.get('span[data-test-component-id="execution-type"]').should('contain.text', expectedType);
  }

  checkTypeIcon(iconName: string): void {
    cy.get('span[data-test-component-id="execution-type"]')
      .siblings('.sqtm-core-infolist-icon')
      .should('have.class', `anticon-sqtm-core-infolist-item:${iconName}`);
  }

  checkDataSet(expectedDataSet: string) {
    cy.get('span[data-test-component-id="execution-dataset"]').should('contain.text', expectedDataSet);
  }

  checkTcDescription(expectedDescription: string) {
    cy.get('div[data-test-component-id="execution-tc-description"]').should('have.html', expectedDescription);
  }

  checkDenormalizedCustomField(index: number, expectedLabel: string, expectedValue: any, inputType?: InputType) {
    cy.get(`label[data-test-component-id="execution-dnz-label-${index}"]`).should('contain.text', expectedLabel);
    if (!Boolean(inputType)) {
      cy.get(`sqtm-app-denormalized-custom-field[data-test-component-id="execution-dnz-value-${index}"]`)
        .should('contain.text', expectedValue);
    } else if (inputType === InputType.CHECKBOX) {
      this.checkDenormalizedCheckBoxCustomField(index, expectedValue);
    } else if (inputType === InputType.RICH_TEXT) {
      cy.get(`sqtm-app-denormalized-custom-field[data-test-component-id="execution-dnz-value-${index}"]`)
        .should('contain.html', expectedValue);
    } else if (inputType === InputType.TAG) {
      this.checkDenormalizedTagCustomField(index, expectedValue);
    }
  }

  private checkDenormalizedCheckBoxCustomField(index: number, expectedValue: boolean) {
    const selector = `
    sqtm-app-denormalized-custom-field[data-test-component-id="execution-dnz-value-${index}"]
    .ant-checkbox
    `;
    if (expectedValue) {
      cy.get(selector).should('have.class', 'ant-checkbox-checked');
    } else {
      cy.get(selector).should('not.have.class', 'ant-checkbox-checked');
    }
  }

  private checkDenormalizedTagCustomField(cufIndex: number, expectedValues: string[]) {
    const selector = `
    sqtm-app-denormalized-custom-field[data-test-component-id="execution-dnz-value-${cufIndex}"]
    nz-tag
    `;
    expectedValues.forEach((expectedValue, index) => {
      cy.get(selector).then(elements => {
        cy.wrap(elements.eq(index)).should('contain.text', expectedValue);
      });
    });
  }

  unbindOneIssue(remoteIssueId: string, issuesGridResponse: GridResponse) {
    this.issueGrid.findRowIdNoWithLink('remoteId', remoteIssueId).then((id) => {
      this.issueGrid
        .getRow(id, 'rightViewport')
        .cell('delete')
        .iconRenderer()
        .click();

      const deleteMock = new HttpMockBuilder('issues/*')
        .delete()
        .build();

      const issueCountMock = new HttpMockBuilder('execution/*/issue-count')
        .get()
        .responseBody({issueCount: issuesGridResponse.count})
        .build()

      const getKnownIssuesMock = new HttpMockBuilder('issues/execution/*/known-issues')
        .post()
        .responseBody(issuesGridResponse)
        .build();

      this.clickConfirmDeleteButton();

      deleteMock.wait();
      issueCountMock.wait();
      getKnownIssuesMock.wait();
    });
  }

  unbindMultipleIssues(remoteIssueIds: string[], issuesGridResponse: GridResponse) {
    this.issueGrid.selectRowsWithMatchingCellContent('remoteId', remoteIssueIds);

    const deleteMock = new HttpMockBuilder('issues/*')
      .delete()
      .build();

    const issueCountMock = new HttpMockBuilder('execution/*/issue-count')
      .get()
      .responseBody({issueCount: issuesGridResponse.count})
      .build()

    const getKnownIssuesMock = new HttpMockBuilder('issues/execution/*/known-issues')
      .post()
      .responseBody(issuesGridResponse)
      .build();

    this.clickOnDeleteButton();
    this.clickConfirmDeleteButton();

    deleteMock.wait();
    issueCountMock.wait();
    getKnownIssuesMock.wait();
  }

  deleteExectuion(rowId: number, newHistoryGrid: GridResponse) {
    const row = this.historyGrid.getRow(rowId);
    row.cell('delete').iconRenderer().click();

    const deleteMock = new HttpMockBuilder('iteration/*/test-plan/execution/*')
      .delete()
      .build();

    const getHistoryMock = new HttpMockBuilder('iteration/*/test-plan/*/executions')
      .post()
      .responseBody(newHistoryGrid)
      .build();

    this.clickConfirmDeleteButton();
    deleteMock.wait();
    getHistoryMock.wait();
  }

  deleteMultipleExecutions(rowIds: number[], newHistoryGrid: GridResponse) {
    rowIds.map(id => {
      const row = this.historyGrid.getRow(id);
    row.cell('#').indexRenderer().clickWithShift();
    });

    const deleteMock = new HttpMockBuilder('iteration/*/test-plan/execution/*')
      .delete()
      .build();

    const getHistoryMock = new HttpMockBuilder('iteration/*/test-plan/*/executions')
      .post()
      .responseBody(newHistoryGrid)
      .build();

    this.clickOnMultipleDeleteButton();
    this.clickConfirmDeleteButton();
    deleteMock.wait();
    getHistoryMock.wait();
  }

  private clickOnDeleteButton() {
    cy.get(selectByDataTestToolbarButtonId('remove-issues'))
      .should('exist')
      .click();
  }

  private clickOnMultipleDeleteButton() {
    cy.get('sqtm-app-execution-page-history')
      .find('[data-test-button-id="mass-delete-button"]')
      .click();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }

  clickAnchorLink<T extends Page>(linkId: ExecutionPageAnchorLinks, knowIssues?: GridResponse): T {
    let page: T;
    switch (linkId) {
      case 'information':
        page = this.showInformationPanel(linkId) as unknown as T;
        break;
      case 'scenario':
        page = this.showScenarioPanel(linkId, knowIssues) as unknown as T;
        break;
      case 'history':
        page = this.showHistoryPanel(linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    page.assertExist();
    return page;
  }

  private showInformationPanel<T>(linkId: 'information'): this {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
    return this;
  }

  private showHistoryPanel<T>(linkId: 'history'): ExecutionHistoryPanelElement {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
    return this.historyPanel;
  }

  private showScenarioPanel<T>(linkId: 'scenario', knowIssues?: GridResponse): ExecutionScenarioPanelElement {
    const getBugTrackerAuthenticatedMock = new HttpMockBuilder('issues/execution/*?frontEndErrorIsHandled=true')
      .responseBody(getAuthenticatedIssuePanelResponse())
      .build();

    const getKnownIssuesMock = new HttpMockBuilder('issues/execution/*/all-known-issues')
      .post()
      .responseBody(knowIssues)
      .build();
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');

    if (knowIssues) {
      getBugTrackerAuthenticatedMock.wait();
      getKnownIssuesMock.wait();
    }
    cy.removeNzTooltip();
    return this.scenarioPanel;
  }

}

function getAuthenticatedIssuePanelResponse(): any {
  return {
    entityType: 'execution',
    bugTrackerStatus: 'AUTHENTICATED',
    projectName: '["P1"]',
    projectId: 1,
    delete: '',
    oslc: false,
  };
}

export type ExecutionPageAnchorLinks =
  'information'
  | 'bugtracker'
  | 'scenario'
  | 'history'
  ;

import {DeleteConfirmDialogElement} from '../../../elements/dialog/delete-confirm-dialog.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {DataRow} from '../../../../model/grids/data-row.type';

export class DeleteCampaignTreeNodesDialog extends DeleteConfirmDialogElement {

  constructor(private treeUrl: string) {
    super('confirm-delete');
  }

  deleteForFailure(response: any) {
  }

  deleteForSuccess(response: any) {
  }

  deleteNodes(deletedNodeIds: string[], parentIds: string[], refreshedNodes: DataRow[], removeFromIter: boolean = false) {
    const refreshMock = new HttpMockBuilder(`${this.treeUrl}/refresh`)
      .post()
      .responseBody({dataRows: refreshedNodes})
      .build();
    const deleteMock = new HttpMockBuilder(`${this.treeUrl}/${deletedNodeIds.join(',')}?remove_from_iter=${removeFromIter}`).delete().build();
    this.clickOnConfirmButton();
    deleteMock.wait();
    refreshMock.wait();
  }
}

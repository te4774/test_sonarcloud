import Chainable = Cypress.Chainable;
import {ExecutionStatusKeys} from '../../../../model/level-enums/level-enum';
import {
  AutomatedExecutionOverview,
  AutomatedItemOverview,
  AutomatedSuiteOverview
} from '../../../../model/test-automation/automated-suite-overview.model';
import {
  SquashAutomProjectPreview,
  TestAutomationProjectPreview
} from '../../../../model/test-automation/automated-suite-preview.model';
import {GridRowElement} from '../../../../utils/grid-selectors.builder';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';
import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {GridElement} from '../../../elements/grid/grid.element';

export class AutomatedTestsExecutionSupervisionDialogElement {

  public readonly stringSelector: string = '[data-test-dialog-id="automated-tests-execution-supervision"]';

  constructor() {
  }

  get selector(): Chainable {
    return cy.get(this.stringSelector);
  }

  displayEnvironments(tmProjectId: number) {
    this.getSquashAutomSectionWithTmProjectId(tmProjectId)
      .find('sqtm-app-environment-selection-panel')
      .find('i[nztype="caret-right"]')
      .click();
  }

  addEnvironmentTag(tmProjectId: number, newTag: string) {
    const listRefreshMock = new HttpMockBuilder('test-automation/*/available-tags').build();
    this.getSquashAutomSectionWithTmProjectId(tmProjectId)
      .find('sqtm-app-environment-selection-panel')
      .find('sqtm-core-editable-tag-list-field')
      .find('nz-select-search input')
      .click({force: true})
      .type(newTag)
      .type('{enter}');
    listRefreshMock.wait();
  }

  getSquashAutomSectionWithTmProjectId(tmProjectId: number) {
    return this.selector.find(`[data-test-component-id="squash-autom-${tmProjectId}"]`);
  }

  checkAgentPhaseContentForJenkinsProjects(expectedAutomationProjects: TestAutomationProjectPreview[]) {
    this.getAutomationProjectSections()
      .then(automProjectSections => cy.wrap(automProjectSections.length)
        .should('equal', expectedAutomationProjects.length));
    expectedAutomationProjects.forEach(automProject => this.checkAutomationProjectPreviewContent(automProject));
  }

  checkAgentPhaseContentForSquashAutomProjects(expectedSquashAutomProjects: SquashAutomProjectPreview[]) {
    this.getSquashAutomProjectSections()
      .then(squashAutomProjectSections => cy.wrap(squashAutomProjectSections.length)
        .should('equal', expectedSquashAutomProjects.length));
    expectedSquashAutomProjects.forEach(squashAutomProject => this.checkSquashAutomProjectPreviewContent(squashAutomProject));
  }

  changeAgent(automationProjectId: number, option: string) {
    const agentSelectField = new SelectFieldElement(CommonSelectors.fieldName(automationProjectId.toString()));
    agentSelectField.selectValue(option);
  }

  confirmAgentPhase(response?: AutomatedSuiteOverview) {
    const createSuiteAndExecute =
      new HttpMockBuilder('automated-suites/create-and-execute').post().responseBody(response).build();
    this.selector.find('[data-test-dialog-button-id="confirm"]').click();
    createSuiteAndExecute.wait();
  }

  checkExecutionPhaseStaticContent(checkJenkinsTable: boolean = true, checkSquashAutomTable: boolean = true) {
    if (checkJenkinsTable) {
      this.checkTfTableTitle();
    }
    if (checkSquashAutomTable) {
      this.checkSquashAutomTableTitle();
    }
    this.selector.find('[data-test-dialog-button-id="close"]').should('exist');
  }

  checkExecutionPhaseDynamicContentAtStart(automatedSuiteOverview: AutomatedSuiteOverview) {
    this.checkTfGridContent(automatedSuiteOverview.executions);
    this.checkSquashAutomGridContent(automatedSuiteOverview.items);
    this.checkProgressBar(automatedSuiteOverview.percentage);
  }

  checkExecutionPhaseDynamicContentProceeding(automatedSuiteOverview: AutomatedSuiteOverview) {
    new HttpMockBuilder('automated-suites/*/executions')
      .get().responseBody(automatedSuiteOverview).build()
      .wait();
    this.checkTfGridStatuses(automatedSuiteOverview.executions);
    this.checkProgressBar(automatedSuiteOverview.percentage);
  }

  checkWarningPhaseContent() {
    this.selector.find('i').should('exist').should('have.length', 1);
    this.selector.find('.close-warning-content').should('exist');
  }

  close() {
    this.selector.find('[data-test-dialog-button-id="close"]').click();
  }

  closeBeforeEnd(automatedSuiteOverview: AutomatedSuiteOverview) {
    new HttpMockBuilder('automated-suites/*/executions').get().responseBody(automatedSuiteOverview).build().wait();
    this.close();
  }

  cancel() {
    this.selector.find('[data-test-dialog-button-id="cancel"]').click();
  }

  private checkTfGridContent(executionOverviews: AutomatedExecutionOverview[]) {
    const gridElement: GridElement = GridElement.createGridElement('tf-automated-test-plan-execution');
    gridElement.assertExist();
    gridElement.assertColumnCount(4);
    gridElement.assertRowCount(executionOverviews.length);
    executionOverviews.forEach((overview: AutomatedExecutionOverview) => {
      gridElement.assertRowExist(overview.id);
      const gridRowElement: GridRowElement = gridElement.getRow(overview.id);
      this.checkCellContainsText(gridRowElement, 'automatedProject', overview.automatedProject);
      this.checkCellContainsText(gridRowElement, 'name', overview.name);
      this.checkCellContainsText(gridRowElement, 'node', overview.node);
      this.checkStatusCell(gridRowElement, overview.status);
    });
  }

  private checkTfGridStatuses(executionOverviews: AutomatedExecutionOverview[]) {
    const gridElement: GridElement = GridElement.createGridElement('tf-automated-test-plan-execution');
    executionOverviews.forEach((overview: AutomatedExecutionOverview) => {
      const gridRowElement: GridRowElement = gridElement.getRow(overview.id);
      this.checkStatusCell(gridRowElement, overview.status);
    });
  }

  private checkSquashAutomGridContent(squashAutomItemOverviews: AutomatedItemOverview[]) {
    const gridElement: GridElement = GridElement.createGridElement('squash-autom-automated-test-plan-execution');
    gridElement.assertExist();
    gridElement.assertColumnCount(2);
    gridElement.assertRowCount(squashAutomItemOverviews.length);
    squashAutomItemOverviews.forEach((itemOverview: AutomatedItemOverview) => {
      gridElement.assertRowExist(itemOverview.id);
      const gridRowElement: GridRowElement = gridElement.getRow(itemOverview.id);
      this.checkCellContainsText(gridRowElement, 'name', itemOverview.name);
      this.checkCellContainsText(gridRowElement, 'automatedServerName', itemOverview.automatedServerName);
    });
  }

  checkProgressBar(progress: number) {
    if (progress !== 100) {
      this.selector.find('nz-progress span').should('contain.text', progress.toString());
    } else {
      this.selector.find('nz-progress i').should('exist');
    }
  }

  private checkCellContainsText(
    gridRowElement: GridRowElement, cellName: string, expectedContent: string) {
    gridRowElement.cell(cellName).findCellTextSpan()
      .should('contain.text', expectedContent);
  }

  private checkStatusCell(
    gridRowElement: GridRowElement, expectedStatus: ExecutionStatusKeys) {
    gridRowElement.cell('status').findCell().find('.status-badge')
      .should('have.css', 'background-color', this.convertStatusColorToRgb(expectedStatus));
  }

  /* Cypress gives background-color as rgb(_, _, _). */
  private convertStatusColorToRgb(status: ExecutionStatusKeys) {
    switch (status) {
      case 'READY': return 'rgb(163, 178, 184)';
      case 'RUNNING': return 'rgb(0, 120, 174)';
      case 'SUCCESS': return 'rgb(0, 111, 87)';
      case 'FAILURE': return 'rgb(203, 21, 36)';
      case 'BLOCKED': return 'rgb(255, 204, 0)';
      case 'UNTESTABLE': return 'rgb(61, 61, 61)';
      default: throw new Error('This ExecutionStatus is not supported.');
    }
  }

  private checkTfTableTitle() {
    this.selector.find('sqtm-app-tf-execution-table .section-title')
      .should('exist')
      .should('contain.text', 'Tests exécutés via Squash TF');
  }

  private checkSquashAutomTableTitle() {
    this.selector.find('sqtm-app-squash-autom-execution-table .section-title')
      .should('exist')
      .should('contain.text', 'Tests exécutés via l\'orchestrateur Squash AUTOM');
  }

  assertExist() {
    this.selector.should('exist');
  }

  getAutomationProjectSections() {
    return this.selector.find('sqtm-app-automation-project-menu');
  }

  getSquashAutomProjectSections() {
    return this.selector.find('sqtm-app-squash-autom-project-preview');
  }

  checkAutomationProjectPreviewContent(expectedAutomationProject: TestAutomationProjectPreview) {
    this.checkJenkinsProjectSectionMessage(expectedAutomationProject);
    this.checkNodeOptions(expectedAutomationProject);
    this.checkTestList(expectedAutomationProject);
  }

  checkSquashAutomProjectPreviewContent(expectedSquashAutomProject: SquashAutomProjectPreview) {
    this.checkSquashAutomSectionMessage(expectedSquashAutomProject);
    this.checkSquashAutomPreviewTestGrid(expectedSquashAutomProject);
  }

  checkJenkinsProjectSectionMessage(expectedAutomationProject: TestAutomationProjectPreview) {
    const automationProjectSection = this.getAutomationProjectSectionWithId(expectedAutomationProject.projectId);
    automationProjectSection.find('.section-title')
      .should('contain.text', expectedAutomationProject.label)
      .should('contain.text', expectedAutomationProject.server);
  }

  checkSquashAutomSectionMessage(expecteSquashAutomProject: SquashAutomProjectPreview) {
    const squashAutomProjectSection = this.getSquashAutomPreviewSectionWithId(expecteSquashAutomProject.projectId);
    squashAutomProjectSection.find('.section-title')
      .should('contain.text', expecteSquashAutomProject.projectName)
      .should('contain.text', expecteSquashAutomProject.serverName);
  }

  private checkNodeOptions(expectedAutomationProject: TestAutomationProjectPreview) {
    const agentSelectField =
      new SelectFieldElement(CommonSelectors.fieldName(expectedAutomationProject.projectId.toString()));
    agentSelectField.checkSelectedOption('Indifférent');
    agentSelectField.checkAllOptions(expectedAutomationProject.nodes);
  }

  private checkTestList(expectedAutomationProject: TestAutomationProjectPreview) {
    const automationProjectId: number = expectedAutomationProject.projectId;
    const testList: string[] = expectedAutomationProject.testList;

    const testListRequest = new HttpMockBuilder('automated-suites/preview/test-list?auto-project-id=*')
      .responseBody(testList).post().build();
    cy.get(`nz-collapse-panel[data-test-component-id="${automationProjectId}"] .label-color`)
      .should('exist')
      .should('contain.text', `Liste des cas de test automatisés (${testList.length} cas de test)`)
      .click();
    testListRequest.wait();
    cy.get(`nz-collapse-panel[data-test-component-id="${automationProjectId}"] ul > li`)
      .should('have.length', testList.length);
    testList.forEach(
      test => cy.contains(
        `nz-collapse-panel[data-test-component-id="${automationProjectId}"] ul > li`,
        test)
        .should('exist'));
  }

  getAutomationProjectSectionWithId(automationProjectId: number) {
    return this.selector.find(`[data-test-component-id="${automationProjectId}"]`);
  }

  getSquashAutomPreviewSectionWithId(projectId: number) {
    return this.selector.find(`[data-test-component-id="squash-autom-${projectId}"]`);
  }

  checkSquashAutomPreviewTestGrid(expectedSquashAutomProject: SquashAutomProjectPreview) {
    const gridElement: GridElement = GridElement.createGridElement('squash-autom-test-case-list');
    gridElement.assertExist();
    gridElement.assertColumnCount(3);
    gridElement.assertRowCount(expectedSquashAutomProject.testCases.length);
  }
}

import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {DataRow} from '../../../../model/grids/data-row.type';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';

export class CtpiMultiEditDialogElement {
  public readonly dialogId = 'ctpi-multi-edit';

  public readonly assigneeSelect: SelectFieldElement;

  constructor() {
    this.assigneeSelect = new SelectFieldElement(CommonSelectors.fieldName('assignee'));
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  confirm(gridRefreshResponse: DataRow[]): void {
    const updateMock = new HttpMockBuilder('campaign/test-plan/*/mass-update').post().build();
    const testPlanMock = new HttpMockBuilder('campaign/*/test-plan')
      .post().responseBody({dataRows: gridRefreshResponse})
      .build();

    const refreshTreeMock = new HttpMockBuilder('campaign-tree/refresh')
      .post().responseBody({dataRows: []})
      .build();

    this.clickOnConfirmButton();

    updateMock.wait();
    testPlanMock.wait();
    refreshTreeMock.wait();
  }

  toggleAssigneeEdition(): void {
    this.clickLabel('Assignation');
  }

  selectAssignee(assignee: string): void {
    this.assigneeSelect.selectValue(assignee);
  }

  private clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
    cy.get(this.buildSelector()).should('not.exist');
  }

  private clickLabel(labelContent: string): void {
    cy.get(this.buildSelector())
      .contains('span', labelContent)
      .click();
  }

  private buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }
}

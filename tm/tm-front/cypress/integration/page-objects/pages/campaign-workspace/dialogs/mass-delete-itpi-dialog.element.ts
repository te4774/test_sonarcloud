import {DeleteConfirmDialogElement} from '../../../elements/dialog/delete-confirm-dialog.element';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';

export class MassDeleteItpiDialogElement extends DeleteConfirmDialogElement {
  iterationId: number;
  itpiIds: number[];

  constructor(iterationId: number, itpiIds: number[]) {
    super('confirm-delete');
    this.iterationId = iterationId;
    this.itpiIds = itpiIds;
  }

  deleteForFailure(response: any) {
  }

  deleteForSuccess(response?: GridResponse) {
    const removeMock = new HttpMockBuilder<{ nbIssues: number }>(`iteration/${this.iterationId}/test-plan/${[this.itpiIds]}`)
      .delete().responseBody({nbIssues: 0}).build();
    const mock = new HttpMockBuilder<GridResponse>(`iteration/${this.iterationId}/test-plan`).post().responseBody(response).build();
    this.clickOnConfirmButton();
    removeMock.wait();
    return mock.waitResponseBody();
  }

}

import {AutomatedSuitePreview} from '../../../../model/test-automation/automated-suite-preview.model';
import {Page} from '../../page';
import {GridElement, TreeElement} from '../../../elements/grid/grid.element';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {
  AutomatedTestsExecutionSupervisionDialogElement
} from '../dialogs/automated-tests-execution-supervision-dialog.element';
import {RemoveOrDetachTestSuiteItpiDialogElement} from '../dialogs/remove-or-detach-test-suite-itpi-dialog.element';
import {VerifyItemTestPlanOptions} from '../iteration/iteration-test-plan.page';
import {TestSuiteTpiMultiEditDialogElement} from '../dialogs/test-suite-tpi-multi-edit-dialog.element';
import Chainable = Cypress.Chainable;

export class TestSuiteTestPlanPage extends Page {

  private constructor(rootSelector: string, public readonly testPlan: GridElement, private testSuiteId: number | string) {
    super(rootSelector);
  }

  static navigateTo(testSuiteId: number | string, testPlan: GridResponse): TestSuiteTestPlanPage {
    const url = `test-suite/${testSuiteId}/test-plan`;
    const gridElement = GridElement.createGridElement('test-suite-test-plan', url, testPlan);
    return new TestSuiteTestPlanPage('sqtm-app-test-suite-test-plan-execution', gridElement, testSuiteId);
  }

  changeDataset(itemId: string) {
    cy.get(`[data-test-cell-id=dataset-cell]`).click();
    const updatedDatasetMock = new HttpMockBuilder('test-plan-item/*/dataset').post().build();
    cy.get(`[data-test-item-id=${itemId}]`).click();
    updatedDatasetMock.wait();
  }

  moveItemWithServerResponse(itemToMove: string, itemToDropOnto: string, response: any): void {
    const mock = new HttpMockBuilder('test-suite/*/test-plan/*/position/*')
      .responseBody(response)
      .post().build();

    this.startDrag(itemToMove);
    this.getDraggedContent().should('be.visible')
      .find('[data-test-component-id="cannot-drag-icon"]')
      .should('not.exist');
    this.internalDropOnto(itemToDropOnto);

    mock.wait();
  }

  private startDrag(itemToDrag: string): void {
    this.testPlan.getRow(itemToDrag, 'leftViewport').cell('#').findCell()
      .find('span')
      .trigger('mousedown', {button: 0})
      .trigger('mousemove', -20, -20, {force: true})
      .trigger('mousemove', 50, 50, {force: true});
  }

  private internalDropOnto(itemToDropOnto: string): void {
    this.testPlan.getRow(itemToDropOnto, 'leftViewport').cell('#').findCell()
      .trigger('mousemove', 'bottom', {force: true})
      .trigger('mouseup', {force: true});
  }

  private getDraggedContent() {
    return cy.get('sqtm-app-test-suite-test-plan-dragged-content');
  }

  assertCannotMoveItem(itemToMove: string): void {
    this.startDrag(itemToMove);
    this.getDraggedContent()
      .find('[data-test-component-id="cannot-drag-icon"]')
      .should('exist');
    this.internalDropOnto(itemToMove);
  }

  showDeleteConfirmDialog(testSuiteId: number, itemTestPlanId: number, refreshedTestPlan: GridResponse)
    : RemoveOrDetachTestSuiteItpiDialogElement {
    this.testPlan.scrollPosition('right', 'mainViewport');
    cy.get(`[data-test-row-id=${itemTestPlanId}] [data-test-cell-id="delete"] i`).click();
    return new RemoveOrDetachTestSuiteItpiDialogElement(testSuiteId, [itemTestPlanId], refreshedTestPlan);
  }

  showMassDeleteConfirmDialog(testSuiteId: number, itemTestPlanIds: number[], refreshedTestPlan: GridResponse)
    : RemoveOrDetachTestSuiteItpiDialogElement {
    cy.get('[data-test-button-id="show-confirm-mass-delete-dialog"]').click();
    return new RemoveOrDetachTestSuiteItpiDialogElement(testSuiteId, itemTestPlanIds, refreshedTestPlan);
  }

  openTestCaseDrawer(response?: GridResponse): TreeElement {
    const treeElement = TreeElement.createTreeElement('test-case-tree-picker', 'test-case-tree', response);
    cy.get('[data-test-button-id=show-test-case-picker]').click();
    treeElement.waitInitialDataFetch();
    return treeElement;
  }

  closeTestCaseDrawer() {
    cy.get('.ant-drawer-body .anticon-close').click();
  }

  enterIntoTestPlan() {
    this.getDropZone()
      .trigger('mouseenter', 'left', {force: true, buttons: 1});
  }

  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(`[data-test-element-id=${TEST_SUITE_TEST_PLAN_DROP_ZONE_ID}]`);
  }

  assertColoredBorderIsVisible() {
    this.getDropZone().should('have.class', 'drop-test-case');
  }

  assertColoredBorderIsNotVisible() {
    this.getDropZone().should('not.have.class', 'drop-test-case');
  }

  dropIntoTestPlan(testSuiteId: number | string, createdTestPlanItemIds?: number[], refreshedTestPlan?: GridResponse) {
    const addTcMock = new HttpMockBuilder<{ itemTestPlanIds: number[] }>(`test-suite/${testSuiteId}/test-plan-items`).post()
      .responseBody({itemTestPlanIds: createdTestPlanItemIds}).build();
    new HttpMockBuilder('test-suite/*/test-plan').post().responseBody(refreshedTestPlan).build();
    this.testPlan.declareRefreshData(refreshedTestPlan);
    this.getDropZone().trigger('mouseup', {force: true});
    return addTcMock.waitResponseBody().then(responseBody => {
      this.testPlan.waitForRefresh();
      return cy.wrap(responseBody.itemTestPlanIds);
    });

  }

  verifyTestPlanItem(options: VerifyItemTestPlanOptions) {
    this.testPlan.assertRowExist(options.id);
    const row = this.testPlan.getRow(options.id);
    if (options.name) {
      if (options.showsAsLink) {
        row.cell('testCaseName').linkRenderer().assertContainText(options.name);
      } else {
        row.cell('testCaseName').textRenderer().assertContainText(options.name);
      }
    }
  }

  foldGrid() {
    cy.get(`[data-test-component-id="fold-tree-button"]`).click();
  }

  applyFastpassOnItpi(itemId: string) {
    cy.get(`[data-test-cell-id=execution-status-cell]`).click();

    const mock = new HttpMockBuilder('test-plan-item/*/execution-status').post().build();
    const refreshMock = new HttpMockBuilder('campaign-tree/refresh').post().build();
    cy.get(`[data-test-item-id=item-${itemId}]`).trigger('click');
    mock.wait();
    refreshMock.wait();
  }

  openMassEditDialog(): TestSuiteTpiMultiEditDialogElement {
    cy.get('[data-test-button-id=mass-edit]').click();
    return new TestSuiteTpiMultiEditDialogElement();
  }

  assertLaunchAutomatedMenuExist() {
    cy.get('[data-test-button-id="launch-automated"]')
      .should('exist')
      .click();
    cy.get('a.launch-automated-link').should('have.length', 2);
  }

  openAutoTestExecDialogViaLaunchAllButton(automatedSuitePreview: AutomatedSuitePreview): AutomatedTestsExecutionSupervisionDialogElement {
    this.clickOnLaunchAutoTestsIcon();
    cy.get('a.launch-automated-link').should('have.length', 2);

    const resolveTaScriptsRequest = new HttpMockBuilder('automation-requests/associate-TA-script?testSuiteId=*').post().build();
    const automatedSuitePreviewRequest = new HttpMockBuilder('automated-suites/preview').post()
      .responseBody(automatedSuitePreview).build();

    cy.get('a.launch-automated-link').contains('Lancer tous les tests automatisés').should('exist').click();

    resolveTaScriptsRequest.wait();
    automatedSuitePreviewRequest.wait();

    const executionDialog = new AutomatedTestsExecutionSupervisionDialogElement();
    executionDialog.assertExist();
    return executionDialog;
  }

  private clickOnLaunchAutoTestsIcon() {
    cy.get('[data-test-button-id="launch-automated"]').should('exist').click();
  }
}

const TEST_SUITE_TEST_PLAN_DROP_ZONE_ID = 'testSuiteTestPlanDropZone';

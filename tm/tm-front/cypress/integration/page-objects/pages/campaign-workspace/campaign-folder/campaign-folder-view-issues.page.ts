import {GridElement} from '../../../elements/grid/grid.element';
import {IssuePage} from '../../../elements/issues/issue.page';

export class CampaignFolderViewIssuesPage extends IssuePage {
  constructor() {
    super('sqtm-app-campaign-folder-issues');
  }

  get issueGrid() {
    return new GridElement('campaign-folder-view-issue');
  }

}

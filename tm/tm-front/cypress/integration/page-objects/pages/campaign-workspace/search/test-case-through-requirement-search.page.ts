import {Page} from '../../page';
import {ToggleIconElement} from '../../../elements/workspace-common/toggle-icon.element';
import {GridElement} from '../../../elements/grid/grid.element';
import {ReferentialData} from '../../../../model/referential-data.model';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder
} from '../../../../utils/referential/referential-data.provider';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {TestCaseSearchModel} from '../../test-case-workspace/research/test-case-search-model';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {ChangeItemTestPlanIdsReport} from '../../../../model/change-coverage-operation-report';

export class TestCaseThroughRequirementSearchPage extends Page {

  private linkSelectionButton = new ToggleIconElement('link-selection-button');
  private linkAllButton = new ToggleIconElement('link-all-button');
  private cancelButton = new ToggleIconElement('cancel-button');

  constructor(public grid: GridElement, public entityName: CampaignWorkspaceEntityName) {
    super('sqtm-app-test-case-by-requirement-search-page');
  }

  public static initTestAtPage(entityName: CampaignWorkspaceEntityName,
                               entityId: string,
                               referentialData: ReferentialData = basicReferentialData,
                               initialRows: GridResponse = {dataRows: []},
                               testCaseSearchModel: TestCaseSearchModel = {
                                 usersWhoCreatedTestCases: [],
                                 usersWhoModifiedTestCases: []
                               },
                               queryString: string = ''): TestCaseThroughRequirementSearchPage {

    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const pageDataMock = new HttpMockBuilder<TestCaseSearchModel>('search/test-case').responseBody(testCaseSearchModel).build();
    const grid = GridElement.createGridElement('tc-by-requirement', 'search/test-case/by-requirement', initialRows);

    cy.visit(`search/test-case/${entityName}/${entityId}/coverage?${queryString}`);

    referentialDataProvider.wait();
    pageDataMock.wait();

    return new TestCaseThroughRequirementSearchPage(grid, entityName);
  }

  clickAddCriteria() {
    cy.get('div[data-test-component-id="grid-filter-manager-add-criteria"]')
      .click();
  }

  selectRowsWithMatchingCellContent(cellId: string, contents: string[]) {
    this.grid.selectRowsWithMatchingCellContent(cellId, contents);
  }

  selectRowWithMatchingCellContent(cellId: string, content: string) {
    this.grid.selectRowWithMatchingCellContent(cellId, content);
  }

  foldFilterPanel() {
    cy.get(this.selectByComponentId('fold-filter-panel-button')).click();
  }

  assertLinkSelectionButtonExist() {
    this.linkSelectionButton.assertExist();
  }

  assertLinkSelectionButtonIsActive() {
    this.linkSelectionButton.assertIsActive();
  }

  assertLinkSelectionButtonIsNotActive() {
    this.linkSelectionButton.assertIsNotActive();
  }

  assertLinkAllButtonExist() {
    this.linkAllButton.assertExist();
  }

  assertLinkAllButtonIsActive() {
    this.linkAllButton.assertIsActive();
  }

  assertNavigateBackButtonExist() {
    this.cancelButton.assertExist();
  }

  assertNavigateBackButtonIsActive() {
    this.cancelButton.assertIsActive();
  }

  linkSelection(
    entityId = '*',
    response: ChangeItemTestPlanIdsReport = {
      itemTestPlanIds: [],
      summary: {}
    }) {
    const mock = this.buildLinkRequestMock(this.entityName, entityId, response);
    this.linkSelectionButton.click();
    mock.wait();
  }

  linkAll(
    entityId = '*',
    response: ChangeItemTestPlanIdsReport = {
      itemTestPlanIds: [],
      summary: {}
    }) {
    const mock = this.buildLinkRequestMock(this.entityName, entityId, response);
    this.linkAllButton.click();
    mock.wait();
  }

  private buildLinkRequestMock(entityName: CampaignWorkspaceEntityName, entityId: string, response: ChangeItemTestPlanIdsReport) {
    return new HttpMockBuilder(`/${entityName}/${entityId}/test-plan-items`).post()
      .responseBody(response)
      .build();
  }
}

export type CampaignWorkspaceEntityName = 'campaign' | 'iteration' | 'test-suite';

import {EntityViewPage} from '../../page';
import {CampaignStatisticsPanelElement} from './campaign-statistics-panel.element';
import {selectByDataTestComponentId} from '../../../../utils/basic-selectors';


export class CampaignMilestoneViewPage extends EntityViewPage {

  public constructor() {
    super('sqtm-app-campaign-milestone-view');
  }

  clickAnchorLink<T>(linkId: CampaignMilestoneViewAnchorLinks, data?: any): T {
    let element: T;
    switch (linkId) {
      case 'information':
        element = this.showInformationPanel(linkId) as unknown as T;
        break;
      case 'dashboard':
        element = this.showDashboard(linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    return element;
  }

  private clickOnAnchorLink(linkId: CampaignMilestoneViewAnchorLinks) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

  private showInformationPanel(linkId: 'information') {
    return undefined;
  }

  private showDashboard(linkId: 'dashboard') {
    this.clickOnAnchorLink(linkId);
    return new CampaignStatisticsPanelElement();
  }

  assertNameEquals(expectedName: string) {
    cy.get(this.rootSelector)
      .find(selectByDataTestComponentId('milestone-label'))
      .should('contain.text', expectedName);
  }
}

export type CampaignMilestoneViewAnchorLinks = 'dashboard' | 'information';

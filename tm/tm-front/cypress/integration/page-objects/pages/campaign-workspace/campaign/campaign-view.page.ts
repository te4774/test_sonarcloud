import {EntityViewPage, Page} from '../../page';
import {EditableRichTextFieldElement} from '../../../elements/forms/editable-rich-text-field.element';
import {apiBaseUrl, HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {CampaignViewInformationPage} from './campaign-view-information.page';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import {CampaignViewStatisticsPage} from './campaign-view-statistics.page';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {CampaignViewIssuesPage} from './campaign-view-issues.page';
import {CampaignTestPlanPage} from './campaign-test-plan.page';
import {CampaignViewDashboardPage} from './campaign-view-dashboard.page';

export class CampaignViewPage extends EntityViewPage {

  constructor(public campaignId: number | string) {
    super('sqtm-app-campaign-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/campaign-view/${this.campaignId}?**`;
    cy.wait(`@${url}`);
  }

  clickAnchorLink<T extends Page>(linkId: CampaignViewAnchorLinks, data?: any): T {
    let element: T;
    switch (linkId) {
      case 'information':
        element = this.showInformationPanel(linkId) as unknown as T;
        break;
      case 'dashboard':
        element = this.showDashboardPage(data) as unknown as T;
        break;
      case 'statistics':
        element = this.showStatisticsPanel(linkId) as unknown as T;
        break;
      case 'plan-exec':
        element = this.showTestPlan(data, linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    return element;
  }

  get nameTextField(): EditableTextFieldElement {
    return new EditableTextFieldElement('entity-name');
  }

  checkName(value: string) {
    this.nameTextField.checkContent(value);
  }

  updateName() {

  }

  get descriptionRichField(): EditableRichTextFieldElement {
    return new EditableRichTextFieldElement('campaign-description');
  }

  checkDescription(value: string) {
    this.descriptionRichField.checkTextContent(value);
  }

  public showInformationPanel<T>(linkId: 'information'): CampaignViewInformationPage {
    this.clickOnAnchorLink(linkId);
    return new CampaignViewInformationPage(this);
  }

  private clickOnAnchorLink(linkId: string) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

  public showStatisticsPanel(linkId: 'statistics'): CampaignViewStatisticsPage {
    this.clickOnAnchorLink(linkId);
    return new CampaignViewStatisticsPage(this);
  }

  checkData(fieldId: string, value: string) {
    cy.get(`[data-test-field-id=${fieldId}] span`).should('contain.text', value);
  }

  showIssuesWithoutBindingToBugTracker(response?: any): CampaignViewIssuesPage {
    const httpMock = new HttpMockBuilder('issues/campaign/*?frontEndErrorIsHandled=true').responseBody(response).build();
    cy.get(`[anchor-link-id="issues"]`).click().trigger('mouseleave');
    httpMock.wait();
    cy.removeNzTooltip();
    return new CampaignViewIssuesPage();
  }

  showIssuesIfBindedToBugTracker(modelResponse?: any, gridResponse?: GridResponse): CampaignViewIssuesPage {
    const bugTrackerModel = new HttpMockBuilder('issues/campaign/*?frontEndErrorIsHandled=true')
      .responseBody(modelResponse).build();
    const issues = new HttpMockBuilder('issues/campaign/*/known-issues').responseBody(gridResponse).post().build();
    cy.get(`[anchor-link-id="issues"]`).click().trigger('mouseleave');
    bugTrackerModel.wait();
    issues.wait();
    cy.removeNzTooltip();
    return new CampaignViewIssuesPage();
  }

  private showTestPlan(data: any, linkId: 'plan-exec'): CampaignTestPlanPage {
    const campaignTestPlanPage = CampaignTestPlanPage.navigateTo(this.campaignId, data);
    cy.get(`[anchor-link-id=${linkId}]`).click();
    campaignTestPlanPage.testPlan.waitInitialDataFetch();
    return campaignTestPlanPage;
  }

  private showDashboardPage(data?: any) {
    this.clickOnAnchorLink('dashboard');
    return new CampaignViewDashboardPage(this.campaignId);
  }
}

export type CampaignViewAnchorLinks = 'dashboard' | 'information' | 'statistics' | 'plan-exec';

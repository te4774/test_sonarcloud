import {Page} from '../../page';
import {CampaignViewPage} from './campaign-view.page';


export class CampaignViewStatisticsPage extends Page {
  private REFRESH_URL = 'campaign-tree/Campaign-*/refresh';

  constructor(private parentPage: CampaignViewPage) {
    super('sqtm-app-campaign-statistics-panel');
  }

  get campaignId() {
    return this.parentPage.campaignId;
  }
}

import {Page} from '../../page';
import {CampaignViewPage} from './campaign-view.page';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import {MilestonesTagFieldElement} from '../../../elements/forms/milestones-tag-field.element';
import {EditableRichTextFieldElement} from '../../../elements/forms/editable-rich-text-field.element';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {Milestone} from '../../../../model/milestone/milestone.model';

export class CampaignViewInformationPage extends Page {

  private REFRESH_URL = 'campaign-tree/Campaign-*/refresh';

  constructor(private parentPage: CampaignViewPage) {
    super('sqtm-app-campaign-information-panel');
  }

  get campaignId() {
    return this.parentPage.campaignId;
  }

  get nameTextField(): EditableTextFieldElement {
    const url = `${this.getRootModificationUrl()}/name`;
    return new EditableTextFieldElement('entity-name', url);
  }

  get milestoneTagElement() {
    return new MilestonesTagFieldElement('campaign-milestones', 'campaign/*/milestones/*');
  }

  get descriptionElement() {
    return new EditableRichTextFieldElement('campaign-description');
  }

  rename(newValue: string, refreshedRow: GridResponse = {dataRows: []}) {
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    this.nameTextField.setValue(newValue);
    this.nameTextField.confirm();
    refreshMock.wait();
  }

  private getRefreshTreeMock(refreshedRow: GridResponse) {
    return new HttpMockBuilder(this.REFRESH_URL).post().responseBody(refreshedRow).build();
  }

  bindMilestone(milestoneId: number, bindableMilestones?: Milestone[]) {
    const milestoneTagElement = this.milestoneTagElement;
    const milestonePickerDialog = milestoneTagElement.openMilestoneDialog();
    milestonePickerDialog.selectMilestone(milestoneId);
    const mock = new HttpMockBuilder('campaign/*/milestones/*').post().responseBody(bindableMilestones).build();
    milestonePickerDialog.confirm();
    mock.wait();
  }

  changeDescription(description: string, refreshedRow: GridResponse = {dataRows: []}) {
    const mock = new HttpMockBuilder(`${this.getRootModificationUrl()}/description`).post().build();
    const refreshMock = this.getRefreshTreeMock(refreshedRow);
    const descriptionElement = this.descriptionElement;
    descriptionElement.assertExist();
    descriptionElement.enableEditMode();
    descriptionElement.assertIsEditable();
    descriptionElement.setValue(description);
    descriptionElement.confirm();
    mock.wait();
    refreshMock.wait();
  }

  private getRootModificationUrl() {
    return `campaign/${this.campaignId}`;
  }
}

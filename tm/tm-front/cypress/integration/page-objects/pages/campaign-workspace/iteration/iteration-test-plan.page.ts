import {ExecutionModel} from '../../../../model/execution/execution.model';
import {GridResponse, Identifier} from '../../../../model/grids/data-row.type';
import {ReferentialData} from '../../../../model/referential-data.model';
import {AutomatedSuitePreview} from '../../../../model/test-automation/automated-suite-preview.model';
import {HttpMock, HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {ReferentialDataProviderBuilder} from '../../../../utils/referential/referential-data.provider';
import {
  EnvironmentSelectionPanelDto
} from '../../../elements/automated-execution-environments/environment-selection-panel.element';
import {AlertDialogElement} from '../../../elements/dialog/alert-dialog.element';
import {ConfirmDialogElement} from '../../../elements/dialog/confirm-dialog.element';
import {GridElement, TreeElement} from '../../../elements/grid/grid.element';
import {ExecutionPage} from '../../execution/execution-page';
import {Page} from '../../page';
import {
  AutomatedTestsExecutionSupervisionDialogElement
} from '../dialogs/automated-tests-execution-supervision-dialog.element';
import {CreateBindTestSuiteDialog} from '../dialogs/create-bind-test-suite.dialog';
import {ExecutionHistoryDialog} from '../dialogs/itpi-execution-history-dialog.element';
import {ItpiMultiEditDialogElement} from '../dialogs/itpi-multi-edit-dialog.element';
import {MassDeleteItpiDialogElement} from '../dialogs/mass-delete-itpi-dialog.element';
import {RemoveItpiDialogElement} from '../dialogs/remove-itpi-dialog.element';
import Chainable = Cypress.Chainable;
import {EvInputType} from '../../../../model/environment-variable/environment-variable.model';


export class IterationTestPlanPage extends Page {

  private constructor(rootSelector: string, public readonly testPlan: GridElement, private iterationId: number | string) {
    super(rootSelector);
  }

  static navigateTo(iterationId: number | string, testPlan: GridResponse): IterationTestPlanPage {
    const url = `iteration/${iterationId}/test-plan`;
    const gridElement = GridElement.createGridElement('iteration-test-plan', url, testPlan);
    return new IterationTestPlanPage('sqtm-app-iteration-test-plan-execution', gridElement, iterationId);
  }

  playTestCaseByName(name: string, executionOption: string) {
    cy.get('sqtm-core-grid-cell[data-test-cell-id="testCaseName"]')
      .contains('span', name)
      .closest('sqtm-core-grid-cell')
      .siblings('sqtm-core-grid-cell[data-test-cell-id="startExecution"]')
      .find('svg')
      .click({force: true});

    cy.get('a').should('contain', ' ... avec la popup');
    cy.get('a').should('contain', ' ... avec l\'IEO');
    cy.get('a').should('contain', ' ... nouvelle exécution');

    switch (executionOption) {
      case 'popup':
        cy.window().then((win) => {

        });

        cy.get('a')
          .contains(' ... avec la popup')
          .click();
        // cy.get('@windowOpen').should('be.calledWith');
        // cy.visit('@windowOpen');
        break;

      case 'ieo':
        cy.get('a')
          .contains(' ... avec l\'IEO')
          .click();
        break;

      case 'newExecution':
        cy.get('a')
          .contains(' ... nouvelle exécution')
          .click();
        break;
    }

  }

  openTestCaseDrawer(response?: GridResponse): TreeElement {
    const treeElement = TreeElement.createTreeElement('test-case-tree-picker', 'test-case-tree', response);
    cy.get('[data-test-button-id=show-test-case-picker]').click();
    treeElement.waitInitialDataFetch();
    return treeElement;
  }

  closeTestCaseDrawer() {
    cy.get('.ant-drawer-body .anticon-close').click();
  }


  openProjectNodeInDrawerByName(name: string, isLeaf: boolean, contentResponse: any = null) {

    let contentMock = null;
    // If the node being double clicked is not a leaf, it will open so we need to wait for its content
    if (!isLeaf) {
      const contentUrl = 'test-case-tree/*/content';
      contentMock = new HttpMockBuilder<GridResponse>(contentUrl).responseBody(contentResponse).build();
    }

    this.getDrawerNodeByName(name).dblclick();

    if (contentMock !== null) {
      contentMock.wait();
    }

  }

  chooseTestCaseInDrawerByName(names: Array<string>, isLeaf: boolean, treeElement: TreeElement, contentResponse: any = null) {

    let contentMock = null;
    // If the node being double clicked is not a leaf, it will open so we need to wait for its content
    if (!isLeaf) {
      const contentUrl = 'test-case-tree/*/content';
      contentMock = new HttpMockBuilder<GridResponse>(contentUrl).responseBody(contentResponse).build();
    }

    const firstName = names[0];
    const remainingNames = names.slice(1);

    this.getDrawerNodeByName(firstName).click();

    for (const name of remainingNames) {
      cy.get('body')
        .type('{ctrl}', {release: false})
        .get('sqtm-core-test-case-picker')
        .find('sqtm-core-test-case-tree-node > div > div ')
        .find('span.tree-node-name')
        .contains(name)
        .click()
      ;
    }
    // this.getDrawerNodeByName(firstName).trigger('mousedown', { which: 1 });
    //   treeElement.findRowId('NAME', firstName, treeElement.beginDragAndDrop);
    treeElement.findRowId('NAME', firstName).then((id) => treeElement.beginDragAndDrop(id));

    // cy.get('[anchor-link-id="plan-exec"]').trigger('mouseover');

    if (contentMock !== null) {
      contentMock.wait();
    }
  }

  getDrawerNodeByName(name: string): Chainable<any> {
    return cy.get('sqtm-core-test-case-picker')
      .find('sqtm-core-test-case-tree-node > div > div ')
      .find('span.tree-node-name')
      .contains(name);
  }

  showDeleteConfirmDialog(testPlanId: number, iterationId: number): RemoveItpiDialogElement {
    this.testPlan.scrollPosition('right', 'mainViewport');
    cy.get(`[data-test-row-id=${testPlanId}] [data-test-cell-id="delete"] i`).click();
    return new RemoveItpiDialogElement(testPlanId, iterationId);
  }

  showMassDeleteConfirmDialog(iterationId: number, itpiIds: number[]) {
    cy.get('[data-test-button-id=show-confirm-mass-delete-dialog]').click();
    return new MassDeleteItpiDialogElement(iterationId, itpiIds);
  }

  assertDeleteIconHasClass(rowId: Identifier, name: string) {
    const deleteIcon = cy.get(`[data-test-row-id=${rowId}] [data-test-cell-id="delete"] [nz-icon].${name}`);
    deleteIcon.should('exist');
    deleteIcon.should('have.class', name);
  }

  enterIntoTestPlan() {
    this.getDropZone()
      .trigger('mouseenter', 'left', {force: true, buttons: 1});
  }

  dropIntoTestPlanNoMock() {
    // cy.get(`[data-test-grid-row-dragged-content]`);
    this.enterIntoTestPlan();
    this.getDropZone()
      .trigger('mouseover', {force: true})
      .trigger('mouseup', {force: true});
  }

  dropIntoTestPlan(iterationId: number | string, createdTestPlanItemIds?: number[], refreshedTestPlan?: GridResponse): Chainable<number[]> {
    const url = `iteration/${iterationId}/test-plan-items`;
    const mock = new HttpMockBuilder<{ itemTestPlanIds: number[] }>(url)
      .post()
      .responseBody({itemTestPlanIds: createdTestPlanItemIds})
      .build();
    this.testPlan.declareRefreshData(refreshedTestPlan);
    this.getDropZone().trigger('mouseup', {force: true});
    return mock.waitResponseBody().then(responseBody => {
      this.testPlan.waitForRefresh();
      return cy.wrap(responseBody.itemTestPlanIds);
    });
  }


  private getDropZone(): Chainable<JQuery<HTMLDivElement>> {
    return cy.get(`[data-test-element-id=${ITERATION_TEST_PLAN_DROP_ZONE_ID}]`);
  }

  verifyTestPlanItem(options: VerifyItemTestPlanOptions) {
    this.testPlan.assertRowExist(options.id);
    const row = this.testPlan.getRow(options.id);
    if (options.name) {
      if (options.showsAsLink) {
        row.cell('testCaseName').linkRenderer().assertContainText(options.name);
      } else {
        row.cell('testCaseName').textRenderer().assertContainText(options.name);
      }
    }
  }

  assertColoredBorderIsVisible() {
    this.getDropZone().should('have.class', 'drop-test-case');
  }

  assertColoredBorderIsNotVisible() {
    this.getDropZone().should('not.have.class', 'drop-test-case');
  }

  clickOnPlayButton(itemTestPlanId: number) {
    this.testPlan.scrollPosition('right', 'mainViewport');
    const row = this.testPlan.getRow(itemTestPlanId, 'rightViewport');
    row.cell('startExecution').iconRenderer().click();
  }

  checkPlayMenu(numberOfOptions: number = 3) {
    cy.get('.launch-execution-link').should('have.length', numberOfOptions);
  }

  checkPlayMenuDontAllowNewExecution(): void {
    cy.get('.launch-execution-link').should('have.length', 3);
    cy.get('.launch-execution-link.disabled').should('have.length', 2);
  }

  applyFastpassOnItpi(itemId: string) {
    cy.get(`[data-test-cell-id=execution-status-cell]`).click();

    const mock = new HttpMockBuilder('test-plan-item/*/execution-status').post().build();
    const refreshMock = new HttpMockBuilder('campaign-tree/refresh').post().build();
    cy.get(`[data-test-item-id=item-${itemId}]`).trigger('click');
    mock.wait();
    refreshMock.wait();
  }

  changeDataset(itemId: string) {
    cy.get(`[data-test-cell-id=dataset-cell]`).click();

    const updatedDatasetMock = new HttpMockBuilder('test-plan-item/*/dataset').post().build();
    cy.get(`[data-test-item-id=${itemId}]`).click();
    updatedDatasetMock.wait();
  }


  openExecutionHistoryDialog(data?: any): ExecutionHistoryDialog {
    const executionListMock = new HttpMockBuilder('iteration/*/test-plan/*/executions')
      .responseBody(data).post().build();
    cy.get(`[data-test-link-id=show-execution-history]`).click();
    executionListMock.waitResponseBody();
    return new ExecutionHistoryDialog(data);
  }


  // faking dialog open by clicking on new page execution
  launchExecutionInPageMode(
    iterationId: number,
    testPlanItemId: number,
    newExecutionId?: number,
    referentialData?: ReferentialData,
    executionModel?: ExecutionModel
  ): ExecutionPage {
    const url = `iteration/${iterationId}/test-plan/${testPlanItemId}/executions/new-manual`;
    const createExecutionMock = new HttpMockBuilder<{ executionId: number }>(url)
      .post()
      .responseBody({executionId: newExecutionId}).build();
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const executionModelMock = new HttpMockBuilder<ExecutionModel>(`execution/*`)
      .responseBody(executionModel).build();
    cy.get('[data-test-link-id=manual-execution]').click();
    createExecutionMock.waitResponseBody();
    referentialDataProvider.wait();
    executionModelMock.wait();
    return new ExecutionPage();
  }

  moveItemWithServerResponse(itemToMove: string, itemToDropOnto: string, response: any): void {
    const mock = new HttpMockBuilder('iteration/*/test-plan/*/position/*')
      .responseBody(response)
      .post().build();

    this.startDrag(itemToMove);
    this.getDraggedContent().should('be.visible')
      .find('[data-test-component-id="cannot-drag-icon"]')
      .should('not.exist');
    this.internalDropOnto(itemToDropOnto);

    mock.wait();
  }

  assertCannotMoveItem(itemToMove: string): void {
    this.startDrag(itemToMove);
    this.getDraggedContent()
      .find('[data-test-component-id="cannot-drag-icon"]')
      .should('exist');
    this.internalDropOnto(itemToMove);
  }

  private startDrag(itemToDrag: string): void {
    this.testPlan.getRow(itemToDrag, 'leftViewport').cell('#').findCell()
      .find('span')
      .trigger('mousedown', {button: 0})
      .trigger('mousemove', -20, -20, {force: true})
      .trigger('mousemove', 50, 50, {force: true});
  }

  private internalDropOnto(itemToDropOnto: string): void {
    this.testPlan.getRow(itemToDropOnto, 'leftViewport').cell('#').findCell()
      .trigger('mousemove', 'bottom', {force: true})
      .trigger('mouseup', {force: true});
  }

  private getDraggedContent() {
    return cy.get('sqtm-app-iteration-test-plan-dragged-content');
  }

  foldGrid() {
    cy.get(`[data-test-component-id="fold-tree-button"]`).click();
  }

  openMassEditDialog(): ItpiMultiEditDialogElement {
    cy.get('[data-test-button-id=mass-edit]').click();
    return new ItpiMultiEditDialogElement();
  }

  openCreateTestSuiteDialog(): CreateBindTestSuiteDialog {
    cy.get('[data-test-button-id=create-test-suite]').click();
    return new CreateBindTestSuiteDialog();
  }

  launchExecutionWithError(iterationId = '*'): AlertDialogElement {
    const mock = new HttpMockBuilder(`iteration/${iterationId}/test-plan/resume`)
      .post()
      .status(412)
      .responseBody(this.getEmptyTestPlanError())
      .build();
    cy.get('[data-test-button-id=launch-execution]').click();
    mock.wait();
    return new AlertDialogElement();
  }

  private getEmptyTestPlanError() {
    return {
      'squashTMError': {
        'kind': 'ACTION_ERROR',
        'actionValidationError': {
          'exception': 'EmptyIterationTestPlanException',
          'i18nKey': 'sqtm-core.error.test-plan.empty',
          'i18nParams': []
        }
      }
    };
  }

  relaunchExecutionWithError(iterationId = '*'): AlertDialogElement {
    const mock = new HttpMockBuilder(`iteration/${iterationId}/test-plan/relaunch`)
      .post()
      .status(412)
      .responseBody(this.getEmptyTestPlanError()).build();
    cy.get('[data-test-button-id=relaunch-execution]').click();
    const confirmDelete = new ConfirmDialogElement('confirm-delete-all-execution');
    confirmDelete.assertExist();
    confirmDelete.clickOnConfirmButton();
    mock.wait();
    return new AlertDialogElement();
  }

  assertLaunchAutomatedMenuExist() {
    cy.get('[data-test-button-id="launch-automated"]')
      .should('exist')
      .click();
    cy.get('a.launch-automated-link').should('have.length', 2);
  }

  assertLaunchAutomatedMenuIsNotVisible() {
    cy.get('[data-test-button-id="launch-automated"]')
      .find('a.launch-automated-link')
      .should('not.exist');
  }

  openAutoTestExecDialogViaLaunchAllButton(
    automatedSuitePreview: AutomatedSuitePreview,
    environmentPanelResponses?: EnvironmentSelectionPanelDto[]): AutomatedTestsExecutionSupervisionDialogElement {
    this.clickOnLaunchAutoTestsIcon();
    cy.get('a.launch-automated-link').should('have.length', 2);

    const resolveTaScriptsRequest = new HttpMockBuilder('automation-requests/associate-TA-script?iterationId=*').post().build();
    const automatedSuitePreviewRequest = new HttpMockBuilder('automated-suites/preview').post()
      .responseBody(automatedSuitePreview).build();

    let loadEnvironmentMocks: HttpMock<EnvironmentSelectionPanelDto>[];
    if (environmentPanelResponses) {
      loadEnvironmentMocks =
        environmentPanelResponses
          .map(panel => new HttpMockBuilder<EnvironmentSelectionPanelDto>(`test-automation/${panel.project.projectId}/automated-execution-environments/all?frontEndErrorIsHandled=true`)
            .get()
            .responseBody(panel)
            .build());
    } else {
      loadEnvironmentMocks = [];
    }

    let environmentVariablePanels: HttpMock<any>[];
    if (automatedSuitePreview.squashAutomProjects.length > 0) {
      environmentVariablePanels = automatedSuitePreview.squashAutomProjects
        .map(project => new HttpMockBuilder('bound-environment-variables/*/project')
          .get()
          .responseBody(this.getAutomatedEnvironmentPanelResponse())
          .build());
    } else {
      environmentVariablePanels = [];
    }

    cy.get('a.launch-automated-link').contains('Lancer tous les tests automatisés').should('exist').click();

    resolveTaScriptsRequest.wait();
    automatedSuitePreviewRequest.wait();
    loadEnvironmentMocks.forEach(mock => mock.wait());
    environmentVariablePanels.forEach(evMock => evMock.wait());

    const executionDialog = new AutomatedTestsExecutionSupervisionDialogElement();
    executionDialog.assertExist();
    return executionDialog;
  }

  private clickOnLaunchAutoTestsIcon() {
    cy.get('[data-test-button-id="launch-automated"]').should('exist').click();
  }

  private getAutomatedEnvironmentPanelResponse() {
    return {
      boundEnvironmentVariables: [{
        id: 1,
        name: 'Simple EV1',
        inputType: EvInputType.PLAIN_TEXT,
        boundToServer: true,
        value: 'toto',
        options: [],
        code: 'Simple EV1'
      }, {
        id: 2,
        name: 'Dropdown EV1',
        code: 'SSEV1',
        inputType: EvInputType.DROPDOWN_LIST,
        boundToServer: true,
        value: 'OPT1',
        options: [{
          label: 'OPT1',
          code: 'OPT1',
          evId: 2,
          position: 0
        }, {
          label: 'OPT2',
          code: 'OPT2',
          evId: 2,
          position: 1
        }],
      }]
    };
  }

}

export interface VerifyItemTestPlanOptions {
  id: number;
  name?: string;
  showsAsLink: boolean;
}

const ITERATION_TEST_PLAN_DROP_ZONE_ID = 'iterationTestPlanDropZone';


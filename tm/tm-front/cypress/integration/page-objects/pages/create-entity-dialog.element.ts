import {CreateDialogOptions, CreationDialogElement} from '../elements/dialog/creation-dialog.element';
import {HttpMockBuilder} from '../../utils/mocks/request-mock';
import {DataRow, GridResponse} from '../../model/grids/data-row.type';
import {TestCaseModel} from '../../model/test-case/test-case.model';
import {ProjectData} from '../../model/project/project-data.model';
import {BindableEntity} from '../../model/bindable-entity.model';
import {TextFieldElement} from '../elements/forms/TextFieldElement';
import {RichTextFieldElement} from '../elements/forms/RichTextFieldElement';
import {CustomFieldForm} from '../elements/custom-fields/custom-field-form';
import {I18nError} from '../elements/forms/error.message';
import Chainable = Cypress.Chainable;

/**
 * This element serves as a specialisation of creation dialogs for entities that
 * live in a tree (e.g. test case, campaign, requirement workspaces entities).
 */
export class CreateEntityDialog extends CreationDialogElement<CreateEntityOptions> {
  private readonly nameField: TextFieldElement;
  private readonly referenceField: TextFieldElement;
  private readonly descriptionField: RichTextFieldElement;
  private customFieldForm: CustomFieldForm;

  constructor(private entityPath: CreateEntityDialogPath, project?: ProjectData, domain?: BindableEntity) {
    super('new-entity');
    this.nameField = new TextFieldElement('name');
    this.referenceField = new TextFieldElement('reference');
    this.descriptionField = new RichTextFieldElement('description');
    this.customFieldForm = new CustomFieldForm(project, domain);
  }

  public fillName(name: string) {
    this.nameField.fill(name);
  }

  public assertNameHasError(error: I18nError) {
    this.nameField.assertErrorContains(error);
  }

  public fillReference(reference: string) {
    this.referenceField.fill(reference);
  }

  public checkReferenceContent(expectedContent: string) {
    this.referenceField.checkContent(expectedContent);
  }

  public fillDescription(description: string) {
    this.descriptionField.fill(description);
  }

  public fillCustomField(cufId: number, value: any) {
    this.customFieldForm.fillCustomField(cufId, value);
  }

  public toggleTagFieldOptions(cufId: number, ...indexes: number[]) {
    this.customFieldForm.toggleTagFieldOptions(cufId, ...indexes);
  }

  public clearCustomField(cufId: number) {
    this.customFieldForm.clearCustomField(cufId);
  }

  public assertCustomFieldHasNoError(cufId: number) {
    this.customFieldForm.assertHasNoError(cufId);
  }

  public assertCustomFieldErrorExist(cufId: number, errorKey: string) {
    this.customFieldForm.assertHasError(cufId, errorKey);
  }

  public assertDuplicateNameErrorExist() {
    this.nameField.assertErrorExist('sqtm-core.error.generic.duplicate-name');
  }

  public checkIfFormIsEmpty(hasCustomField: boolean) {
    if (!hasCustomField) {
      this.nameField.checkContent('');
      this.referenceField.checkContent('');
      this.descriptionField.checkContent('');
    } else {
      // TODO implement this part
      console.log('CUF NOT CHECKED');
    }
  }
  checkCreateEntityDialogButtons() {
    cy.get('button[data-test-dialog-button-id="add-another"]').should('exist');
    cy.get('button[data-test-dialog-button-id="add"]').should('exist');
    cy.get('button[data-test-dialog-button-id="cancel"]').should('exist');
  }

  // Implementation of CreateDialogElement

  addWithOptions(options: CreateEntityOptions): Chainable<any> {
    return this.addForSuccess(options.parentRowIsClosed, options.addAnother, options.parentRowRef, options.addedId,
      options.createResponse, options.children, options.entityModel);
  }

  addForSuccessOpen(parentId?: string, addedNodeId?: number, createResponse?: DataRow,
                    children?: GridResponse, entityModel?: any): Chainable<any> {
    return this.addForSuccess(false, false, parentId, addedNodeId, createResponse, children, entityModel);
  }

  private addForSuccess(parentIsClosed: boolean, addAnother: boolean, parentId?: string, addedNodeId?: number, createResponse?: DataRow,
                        children?: GridResponse, entityModel?: any): Chainable<any> {
    const mock = new HttpMockBuilder<DataRow>(this.getNewEntityUrl())
      .post()
      .responseBody(createResponse || {} as DataRow)
      .build();

    let openRowMock = null;

    if (parentIsClosed) {
      openRowMock = new HttpMockBuilder<GridResponse>(
        `${this.entityPath.treePath}/${parentId == null ? '*' : parentId}/content`
      ).responseBody(children).build();
    }

    const viewUrl = `${this.entityPath.viewPath}/${addedNodeId === undefined ? '*' : addedNodeId}?**`;
    const viewMock = new HttpMockBuilder<TestCaseModel>(viewUrl).responseBody(entityModel).build();

    if (addAnother) {
      this.clickOnAddAnotherButton();
    } else {
      this.clickOnAddButton();
    }

    mock.wait();

    if (openRowMock) {
      openRowMock.wait();
    }

    return viewMock.waitResponseBody();
  }

  protected getNewEntityUrl(): string {
    return `${this.entityPath.treePath}/${this.entityPath.newEntityPath}`;
  }
}

export interface CreateEntityOptions extends CreateDialogOptions {
  parentRowIsClosed: boolean;
  parentRowRef?: string;
  children?: GridResponse;
  entityModel?: any;

  /**
   * The server's response after creation (in a mock situation).
   */
  createResponse?: DataRow;
}

export interface CreateEntityDialogPath {
  treePath: string;
  viewPath: string;
  newEntityPath: string;
}




import {GridElement} from '../elements/grid/grid.element';
import {selectByDataTestComponentId} from '../../utils/basic-selectors';
import {EditableTextFieldElement} from '../elements/forms/editable-text-field.element';
import {InputType} from '../../model/customfield/customfield.model';
import {EditableRichTextFieldElement} from '../elements/forms/editable-rich-text-field.element';
import {EditableNumericFieldElement} from '../elements/forms/editable-numeric-field.element';
import {EditableDateFieldElement} from '../elements/forms/editable-date-field.element';
import {EditableTagFieldElement} from '../elements/forms/editable-tag-field.element';
import {EditableSelectFieldElement} from '../elements/forms/editable-select-field.element';
import {CheckBoxElement} from '../elements/forms/check-box.element';
import {AbstractFormFieldElement, CommonSelectors} from '../elements/forms/abstract-form-field.element';

export type PageFactory<T extends Page> = (...args: any[]) => T;

export abstract class Page {

  protected constructor(public readonly rootSelector: string) {
  }

  /**
   * Returns a Cypress selector string for a component ID (attribute 'data-test-component-id').
   * Beware that this method will NOT return a scoped selector for this page.
   * @param id: component ID to look for
   */
  selectByComponentId(id: string): string {
    return selectByDataTestComponentId(id);
  }

  assertExist() {
    cy.get(this.rootSelector).should('have.length', 1);
  }

  checkDataFetched() {
  }
}

export abstract class WorkspaceWithGridPage extends Page {

  protected constructor(public readonly grid: GridElement, rootSelector: string) {
    super(rootSelector);
  }

  waitInitialDataFetch() {
    this.grid.waitInitialDataFetch();
  }

  assertExist() {
    super.assertExist();
    this.grid.assertExist();
  }
}

export abstract class EntityViewPage extends Page {
  protected constructor(rootSelector: string) {
    super(rootSelector);
  }

  abstract clickAnchorLink(linkId: string): Page;

  assertNameContains(expected: string) {
    cy.get('[data-test-field-id="entity-name"] span').should('contain.text', expected);
  }

  assertReferenceContains(expected: string) {
    if (expected == null || expected === '') {
      // For an empty reference, the selector won't work as the span isn't rendered.
      return;
    }

    cy.get('[data-test-field-id="entity-reference"] span').should('contain.text', expected);
  }

  toggleTree() {
    cy.get(selectByDataTestComponentId('fold-tree-button')).click({force: true});
  }


  getTextCustomField(customFieldName: string): EditableTextFieldElement {
    return this.getCustomField(customFieldName, InputType.PLAIN_TEXT) as EditableTextFieldElement;
  }

  getRichTextCustomField(customFieldName: string): EditableRichTextFieldElement {
    return this.getCustomField(customFieldName, InputType.RICH_TEXT) as EditableRichTextFieldElement;
  }

  getNumericCustomField(customFieldName: string): EditableNumericFieldElement {
    return this.getCustomField(customFieldName, InputType.NUMERIC) as EditableNumericFieldElement;
  }

  getDateCustomField(customFieldName: string): EditableDateFieldElement {
    return this.getCustomField(customFieldName, InputType.DATE_PICKER) as EditableDateFieldElement;
  }

  getTagCustomField(customFieldName: string): EditableTagFieldElement {
    return this.getCustomField(customFieldName, InputType.TAG) as EditableTagFieldElement;
  }

  getSelectCustomField(customFieldName: string): EditableSelectFieldElement {
    return this.getCustomField(customFieldName, InputType.DROPDOWN_LIST) as EditableSelectFieldElement;
  }

  getCheckboxCustomField(customFieldName: string): CheckBoxElement {
    return this.getCustomField(customFieldName, InputType.CHECKBOX) as CheckBoxElement;
  }

  getCustomField(customFieldName: string, inputType: InputType): AbstractFormFieldElement {
    const singleValueUrl = 'custom-fields/values/*/single-value';
    const multiValueUrl = 'custom-fields/values/*/multi-value';
    const cufSelector = CommonSelectors.customField(customFieldName);

    switch (inputType) {
      case InputType.DROPDOWN_LIST:
        return new EditableSelectFieldElement(cufSelector, singleValueUrl);
      case InputType.CHECKBOX:
        return new CheckBoxElement(cufSelector, singleValueUrl);
      case InputType.DATE_PICKER:
        return new EditableDateFieldElement(cufSelector, singleValueUrl);
      case InputType.NUMERIC:
        return new EditableNumericFieldElement(cufSelector, singleValueUrl);
      case InputType.PLAIN_TEXT:
        return new EditableTextFieldElement(cufSelector, singleValueUrl);
      case InputType.RICH_TEXT:
        return new EditableRichTextFieldElement(cufSelector, singleValueUrl);
      case InputType.TAG:
        return new EditableTagFieldElement(cufSelector, multiValueUrl);
    }
  }
}

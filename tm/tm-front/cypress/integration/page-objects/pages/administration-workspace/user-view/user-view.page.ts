import {Page} from '../../page';
import {UserAuthorisationsPanelElement} from './panels/user-authorisations-panel.element';
import {UserTeamsPanelElement} from './panels/user-teams-panel.element';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import {EditableSelectFieldElement} from '../../../elements/forms/editable-select-field.element';
import {ResetPasswordDialog} from './dialogs/reset-password-dialog.dialog';

export class UserViewPage extends Page {

  public readonly authorisationsPanel: UserAuthorisationsPanelElement;
  public readonly teamsPanel: UserTeamsPanelElement;

  public readonly loginTextField: EditableTextFieldElement;
  public readonly firstNameTextField: EditableTextFieldElement;
  public readonly lastNameTextField: EditableTextFieldElement;
  public readonly emailTextField: EditableTextFieldElement;
  public readonly groupSelectField: EditableSelectFieldElement;

  constructor(selector: string = 'sqtm-app-user-view > sqtm-core-entity-view-wrapper > div') {
    super(selector);
    this.authorisationsPanel = new UserAuthorisationsPanelElement();
    this.teamsPanel = new UserTeamsPanelElement();

    this.loginTextField = new EditableTextFieldElement(
      'entity-name',
      'users/*/login'
    );

    this.firstNameTextField = new EditableTextFieldElement(
      'user-first-name',
      'users/*/first-name'
    );

    this.lastNameTextField = new EditableTextFieldElement(
      'user-last-name',
      'users/*/last-name'
    );

    this.emailTextField = new EditableTextFieldElement(
      'user-email',
      'users/*/email'
    );

    this.groupSelectField = new EditableSelectFieldElement(
      'user-group',
      'users/*/change-group/*'
    );
  }

  foldGrid() {
    cy.get(`[data-test-component-id="fold-tree-button"]`).click();
  }

  modifyData(fieldId: string, value: string) {
    switch (fieldId) {
      case 'login':
        this.loginTextField.setAndConfirmValue(value);
        break;
      case 'user-first-name':
        // cy.get('[data-test-field-id="project-description"] p').click({force: true});
        this.firstNameTextField.setAndConfirmValue(value);
        break;
      case 'user-last-name':
        this.lastNameTextField.setAndConfirmValue(value);
        break;
      case 'user-email':
        this.emailTextField.setAndConfirmValue(value);
        break;
      case 'user-group':
        this.groupSelectField.setAndConfirmValueNoButton(value);
        break;
      case undefined :
        cy.log('fieldId does not exist');
        break;
    }
  }

  openResetPasswordDialog(): ResetPasswordDialog {
    cy.get('.reset-button').click();
    return new ResetPasswordDialog();
  }

  waitInitialDataFetch() {
    this.authorisationsPanel.waitInitialDataFetch();
    this.teamsPanel.waitInitialDataFetch();
  }
}

import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {RichTextFieldElement} from '../../../elements/forms/RichTextFieldElement';
import {CreateAdministrationEntityDialog} from '../create-administration-entity-dialog';
import {GridResponse} from '../../../../model/grids/data-row.type';
import {HttpResponseStatus} from '../../../../utils/mocks/request-mock';
import {I18nError} from '../../../elements/forms/error.message';

export class CreateTemplateDialog extends CreateAdministrationEntityDialog {

  private readonly nameField: TextFieldElement;
  private readonly labelField: TextFieldElement;
  private readonly descriptionField: RichTextFieldElement;

  constructor() {
    super('new-entity', 'projects/new-template', 'generic-projects');

    this.nameField = new TextFieldElement('name');
    this.labelField = new TextFieldElement('label');
    this.descriptionField = new RichTextFieldElement('description');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillLabel(label: string) {
    this.labelField.fill(label);
  }

  fillDescription(description: string) {
    this.descriptionField.fill(description);
  }

  assertNameFieldHasError(error: I18nError) {
    this.nameField.assertErrorContains(error);
  }

  createTemplate(name: string, label: string, description: string) {
    this.fillName(name);
    this.fillLabel(label);
    this.fillDescription(description);
    this.addForSuccess(false, null, null, 201);
  }

  protected addForSuccess(addAnother: boolean,
                          createResponse?: any,
                          gridResponse?: GridResponse,
                          createResponseStatus?: HttpResponseStatus): Cypress.Chainable<any> {
    return super.addForSuccess(addAnother, createResponse, gridResponse, 201);
  }

  checkIfFormIsEmpty() {
    this.nameField.checkContent('');
    this.labelField.checkContent('');
    this.descriptionField.checkContent('');
  }

}

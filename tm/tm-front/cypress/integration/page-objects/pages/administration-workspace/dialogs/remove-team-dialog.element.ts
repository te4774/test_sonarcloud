import {Identifier} from '../../../../model/grids/data-row.type';
import {RemoveAdministrationEntityDialog} from '../remove-administration-entity-dialog';

export class RemoveTeamDialogElement extends RemoveAdministrationEntityDialog {
  constructor(public readonly teamIds: Identifier[]) {
    super('teams', 'teams', teamIds);
  }
}

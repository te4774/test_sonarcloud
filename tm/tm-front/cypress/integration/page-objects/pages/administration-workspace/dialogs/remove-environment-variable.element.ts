import {RemoveAdministrationEntityDialog} from '../remove-administration-entity-dialog';

export class RemoveEnvironmentVariableElement extends RemoveAdministrationEntityDialog {
  constructor(evIds: number[]) {
    super('environment-variables', 'environment-variables', evIds);
  }
}

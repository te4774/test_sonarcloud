import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {CreateAdministrationEntityDialog} from '../create-administration-entity-dialog';
import {RequirementVersionLinkType} from '../../../../model/requirements/requirement-version-link-type.model';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {I18nError} from '../../../elements/forms/error.message';

export class CreateRequirementsLinkDialog extends CreateAdministrationEntityDialog {


  private readonly role1Field: TextFieldElement;
  private readonly role1CodeField: TextFieldElement;
  private readonly role2Field: TextFieldElement;
  private readonly role2CodeField: TextFieldElement;

  constructor() {
    super('new-requirements-link', 'requirements-links/new', 'requirements-links');

    this.role1Field = new TextFieldElement('role1');
    this.role1CodeField = new TextFieldElement('role1Code');
    this.role2Field = new TextFieldElement('role2');
    this.role2CodeField = new TextFieldElement('role2Code');

  }

  fillRole1(role1: string) {
    this.role1Field.fill(role1);
  }

  assertRole1HasError(error: I18nError) {
    this.role1Field.assertErrorContains(error);
  }

  assertRole1HasNoError() {
    this.role1Field.assertNoErrorIsDisplayed();
  }

  fillRole1Code(role1Code: string) {
    this.role1CodeField.fill(role1Code);
  }

  assertCode1HasError(error: I18nError) {
    this.role1CodeField.assertErrorContains(error);
  }

  fillRole2(role2: string) {
    this.role2Field.fill(role2);
  }

  assertRole2HasError(error: I18nError) {
    this.role2Field.assertErrorContains(error);
  }

  fillRole2Code(role2Code: string) {
    this.role2CodeField.fill(role2Code);
  }

  assertCode2HasError(error: I18nError) {
    this.role2CodeField.assertErrorContains(error);
  }

  addRequirementLink(addedRequirementLinkId: number, requirementsLinks: RequirementVersionLinkType[], addAnother: boolean) {
    const mockAdd = new HttpMockBuilder('requirements-links/new').responseBody({id: addedRequirementLinkId}).post().build();
    const mockAddedResponse = new HttpMockBuilder('requirements-links').responseBody({requirementLinks: requirementsLinks}).post().build();
    if (addAnother) {
      this.clickOnAddAnotherButton();
    } else {
      this.clickOnAddButton();
    }
    return mockAdd.waitResponseBody().then( () => {
      mockAddedResponse.waitResponseBody();
    });
  }

  createLink(role1: string, role1Code: string, role2: string, role2Code: string) {
    this.fillRole1(role1);
    this.fillRole1Code(role1Code);
    this.fillRole2(role2);
    this.fillRole2Code(role2Code);
    this.addForSuccess(false);
  }

  checkIfFormIsEmpty() {
    this.role1Field.checkContent('');
    this.role1CodeField.checkContent('');
    this.role2Field.checkContent('');
    this.role2CodeField.checkContent('');
  }

  checkIfExistingTypeCodeIsDisplayed() {
    const errorMessage = 'Ce code est déjà utilisé pour un type de lien existant.';
    cy.get('span[data-test-error-key="sqtm-core.exception.requirements-links.link-type-code-already-exists"]')
      .should('contain.text', errorMessage);
  }

}

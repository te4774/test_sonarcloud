import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {RichTextFieldElement} from '../../../elements/forms/RichTextFieldElement';
import {CreateAdministrationEntityDialog} from '../create-administration-entity-dialog';
import {I18nError} from '../../../elements/forms/error.message';

export class CreateTeamDialog extends CreateAdministrationEntityDialog {

  private readonly nameField: TextFieldElement;
  private readonly descriptionField: RichTextFieldElement;

  constructor() {
    super('new-team', 'teams/new', 'teams');

    this.nameField = new TextFieldElement('name');
    this.descriptionField = new RichTextFieldElement('description');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  assertNameHasError(error: I18nError) {
    this.nameField.assertErrorContains(error);
  }

  fillDescription(description: string) {
    this.descriptionField.fill(description);
  }

  createTeam(name: string, description: string) {
    this.fillName(name);
    this.fillDescription(description);
    this.addForSuccess(false);
  }

  checkIfFormIsEmpty() {
    this.nameField.checkContent('');
    this.descriptionField.checkContent('');
  }

}

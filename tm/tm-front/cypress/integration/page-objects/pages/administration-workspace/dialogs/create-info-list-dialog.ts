import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {CreateAdministrationEntityDialog} from '../create-administration-entity-dialog';
import {RichTextFieldElement} from '../../../elements/forms/RichTextFieldElement';
import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {GridElement} from '../../../elements/grid/grid.element';

export class CreateInfoListDialog extends CreateAdministrationEntityDialog {

  public readonly labelField: TextFieldElement;
  public readonly codeField: TextFieldElement;
  public readonly descriptionField: RichTextFieldElement;

  public readonly infoListOptionLabelField: TextFieldElement;
  public readonly infoListOptionCodeField: TextFieldElement;
  public readonly infoListOptionsGrid: GridElement;


  constructor() {
    super('new-entity', 'info-lists/new', 'info-lists');
    this.labelField = new TextFieldElement('label');
    this.codeField = new TextFieldElement('code');
    this.descriptionField = new RichTextFieldElement('description');


    this.infoListOptionLabelField = new TextFieldElement('infoListOptionLabel');
    this.infoListOptionCodeField = new TextFieldElement('infoListOptionCode');
    this.infoListOptionsGrid = GridElement.createGridElement('info-list-options');
  }


  fillLabel(label: string) {
    this.labelField.fill(label);
  }

  fillCodeField(codeField: string) {
    this.codeField.fill(codeField);
  }

  fillDescriptionField(description: string) {
    this.descriptionField.fill(description);
  }

  fillInfoListOptionLabel(infoListOptionLabel: string) {
    this.infoListOptionLabelField.fill(infoListOptionLabel);
  }

  fillInfoListOptionCode(infoListOptionCode: string) {
    this.infoListOptionCodeField.fill(infoListOptionCode);
  }

  removeOptionWithLabel(label: string): void {
    this.infoListOptionsGrid.findRowId('label', label).then(id => {
      this.infoListOptionsGrid.getCell(id, 'delete').iconRenderer().click();
    });
  }

  addInfoListOption(infoListOptionLabel: string, infoListOptionCode: string) {
    this.fillInfoListOptionLabel(infoListOptionLabel);
    this.fillInfoListOptionCode(infoListOptionCode);

    const mock = new HttpMockBuilder('info-lists/check-if-item-code-already-exists/' + infoListOptionCode).get().build();
    cy.get('button[data-test-dialog-button-id="add-option"').click();
    mock.wait();
  }

  clickOnAddOptionButton() {
    cy.get('button[data-test-dialog-button-id="add-option"').click();
  }

  clickOnAddButton() {
    cy.get('button[data-test-dialog-button-id="add"').click();
  }

  checkIfOptionNameAlreadyExistErrorIsDisplayed() {
    const errorMessage = 'Le nom de l\'option existe déjà.';
    cy.get('span[data-test-error-key="sqtm-core.validation.errors.optionNameAlreadyExists"]').should('contain.text', errorMessage);
  }

  checkIfOptionCodeAlreadyExistErrorIsDisplayed() {
    const errorMessage = 'Le code de l\'option existe déjà.';
    cy.get('span[data-test-error-key="sqtm-core.validation.errors.optionCodeAlreadyExists"]').should('contain.text', errorMessage);
  }

  checkIfOptionDefaultValueRequiredErrorIsDisplayed() {
    const errorMessage = ' Une option par défaut doit être définie';
    cy.get('span[data-test-error-key="default-option-required"]').should('contain.text', errorMessage);
  }

  checkIfInvalidCodePatternErrorIsDisplayed() {
    const errorMessage = 'Ne doit contenir que des lettres, nombres ou underscores.';
    cy.get('span[data-test-error-key="sqtm-core.validation.errors.invalidCodePattern"]').should('contain.text', errorMessage);
  }
}

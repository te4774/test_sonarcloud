import {CreateAdministrationEntityDialog} from '../create-administration-entity-dialog';
import {TextFieldElement} from '../../../elements/forms/TextFieldElement';
import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {GridElement} from '../../../elements/grid/grid.element';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';
import {
  OPTION_CODE_ALREADY_EXISTS_ERROR,
  OPTION_NAME_ALREADY_EXISTS_ERROR
} from '../../../elements/forms/error.message';

export class CreateEnvironmentVariableDialog extends CreateAdministrationEntityDialog {
  public readonly nameField: TextFieldElement;
  public readonly codeField: TextFieldElement;
  public readonly inputTypeField: SelectFieldElement;

  public readonly dropdownListOptionNameField: TextFieldElement;
  public readonly dropdownListOptionCodeField: TextFieldElement;
  public readonly dropdownListOptionsGrid: GridElement;

  constructor() {
    super('new-entity', 'environment-variables/new', 'environment-variables');
    this.nameField = new TextFieldElement('name');
    this.codeField = new TextFieldElement('code');
    this.inputTypeField = new SelectFieldElement(CommonSelectors.fieldName('evInputType'));
    this.dropdownListOptionNameField = new TextFieldElement('dropdownListOptionLabel');
    this.dropdownListOptionCodeField = new TextFieldElement('dropdownListOptionCode');
    this.dropdownListOptionsGrid = GridElement.createGridElement('environment-variable-options');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillCodeField(codeField: string) {
    this.codeField.fill(codeField);
  }

  selectTypeField(inputTypeField: string) {
    this.inputTypeField.selectValue(inputTypeField);
  }

  fillDropdownListOptionName(dropdownListOptionName: string) {
    this.dropdownListOptionNameField.fill(dropdownListOptionName);
  }

  fillDropdownListOptionCode(dropdownListOptionCode: string) {
    this.dropdownListOptionCodeField.fill(dropdownListOptionCode);
  }

  addDropdownListOption(dropdownListOptionName: string, dropdownListOptionCode: string) {
    this.fillDropdownListOptionName(dropdownListOptionName);
    this.fillDropdownListOptionCode(dropdownListOptionCode);
    cy.get('button[data-test-dialog-button-id="add-option"').click();
  }

  checkIfOptionNameAlreadyExistErrorIsDisplayed() {
   this.dropdownListOptionNameField.assertErrorContains(OPTION_NAME_ALREADY_EXISTS_ERROR);
  }

  checkIfOptionCodeAlreadyExistErrorIsDisplayed() {
    this.dropdownListOptionCodeField.assertErrorContains(OPTION_CODE_ALREADY_EXISTS_ERROR);
  }
}

import {HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {SelectFieldElement} from '../../../elements/forms/select-field.element';
import {CommonSelectors} from '../../../elements/forms/abstract-form-field.element';

export class ChangeExecutionStatusInUseDialog {


  private readonly statusSelectionField: SelectFieldElement;

  constructor() {
    this.statusSelectionField = new SelectFieldElement(CommonSelectors.fieldName('status'));
  }


  fillStatusSelectionField(status: string) {
    this.statusSelectionField.selectValue(status);
  }


  editStatusInUse(oldStatus: 'UNTESTABLE' | 'SETTLED', newStatus: string) {
    this.fillStatusSelectionField(newStatus);
    new HttpMockBuilder<any>('generic-projects/4/disable-and-replace-execution-status-within-project/' + oldStatus)
      .post()
      .build();

    cy.get('[data-test-dialog-button-id="confirm"]').click();
  }
}

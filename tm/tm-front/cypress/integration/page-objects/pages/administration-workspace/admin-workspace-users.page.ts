import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {GridElement} from '../../elements/grid/grid.element';
import {AdministrationWorkspacePage} from './administration-workspace.page';
import {PageFactory} from '../page';
import {GridResponse} from '../../../model/grids/data-row.type';
import {AdminWorkspaceTeamsPage} from './admin-workspace-teams.page';
import {CreateUserDialog} from './dialogs/create-user-dialog.element';
import {HttpMock, HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {UserViewPage} from './user-view/user-view.page';
import {User} from '../../../model/user/user.model';
import {AdminReferentialDataProviderBuilder} from '../../../utils/referential/admin-referential-data.provider';
import {AdminReferentialData} from '../../../model/admin-referential-data.model';

export class AdminWorkspaceUsersPage extends AdministrationWorkspacePage {
  public readonly navBar = new NavBarElement();

  private usersGroupsMock: HttpMock<any>;

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-user-workspace');

    this.usersGroupsMock = new HttpMockBuilder('users/get-users-groups')
      .responseBody({
        usersGroups:
          [
            {id: 1, qualifiedName: 'squashtest.authz.group.core.Admin'},
            {id: 2, qualifiedName: 'squashtest.authz.group.tm.User'},
            {id: 4, qualifiedName: 'squashtest.authz.group.tm.TestAutomationServer'}
          ]
      })
      .build();
  }

  public static initTestAtPage: PageFactory<AdminWorkspaceUsersPage> =
    (initialNodes: GridResponse = {dataRows: []}, referentialData?: AdminReferentialData) => {

      const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(referentialData).build();

      const gridElement = GridElement.createGridElement('users', 'users', initialNodes);
      const page = new AdminWorkspaceUsersPage(gridElement);

      // visit page
      cy.visit(`administration-workspace/${page.getPageUrl()}`);

      // Check page initialisation
      page.assertExist();

      // wait for ref data request to fire
      adminReferentialDataProvider.wait();

      // wait for initial grid data and additional requests to fire
      page.waitInitialDataFetch();

      return page;
    }

  public waitInitialDataFetch() {
    super.waitInitialDataFetch();
  }

  public assertExist() {
    super.assertExist();
  }


  goToTeamAnchor(initialGridData?: GridResponse): AdminWorkspaceTeamsPage {
    cy.get('div[data-test-anchor-id="team"]').click();
    cy.get('[data-test-grid-id="teams"]').should('exist');
    const gridElement = GridElement.createGridElement('teams', 'teams', initialGridData);
    const page = new AdminWorkspaceTeamsPage(gridElement);
    page.assertExist();
    return page;
  }


  selectUserByLogin(name: string,
                    viewResponse?: User): UserViewPage {

    const userView = new UserViewPage();
    const mock = new HttpMockBuilder('user-view/*')
      .responseBody(viewResponse)
      .build();

    this.selectRowWithMatchingCellContent('login', name);

    mock.waitResponseBody().then(() => {
      userView.waitInitialDataFetch();
      userView.assertExist();
    });

    return userView;
  }

  selectUsersByLogin(names: string[]) {
    this.selectRowsWithMatchingCellContent('login', names);
  }

  deleteUserByLogin(login: string, confirm: boolean) {
    this.deleteRowWithMatchingCellContent('login', login, confirm);
  }

  openCreateUser(): CreateUserDialog {
    this.clickCreateButton();
    return new CreateUserDialog;
  }

  protected getPageUrl(): string {
    return 'users/manage';
  }

  protected getDeleteUrl(): string {
    return 'users';
  }
}

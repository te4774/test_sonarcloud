import {DeleteConfirmDialogElement} from '../../elements/dialog/delete-confirm-dialog.element';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {GridResponse, Identifier} from '../../../model/grids/data-row.type';

export class RemoveAdministrationEntityDialog extends DeleteConfirmDialogElement {
  constructor(public readonly deleteUrl: string,
              public readonly gridUrl: string,
              public readonly entityIds: Identifier[]) {
    super('confirm-delete');
  }

  deleteForFailure(response?: any) {
    // TODO: implement me !
  }

  deleteForSuccess(response?: GridResponse) {
    const removeMock = new HttpMockBuilder<any>(this.getDeleteUrl())
      .delete()
      .responseBody('')
      .build();

    const mock = new HttpMockBuilder<GridResponse>(this.gridUrl)
      .post()
      .responseBody(response)
      .build();

    this.clickOnConfirmButton();

    removeMock.wait();
    return mock.waitResponseBody();
  }

  deleteUserForSuccess(response: GridResponse) {
  const removeMock = new HttpMockBuilder<any>(this.getDeleteUrl())
    .delete()
    .responseBody('')
    .build();

  const syncsMock = new HttpMockBuilder<any>('/users/*/synchronisations')
    .get()
    .responseBody([])
    .build();

  const mock = new HttpMockBuilder<GridResponse>(this.gridUrl)
    .post()
    .responseBody(response)
    .build();

    this.clickOnConfirmButton();
    syncsMock.wait();
    removeMock.wait();

    return mock.waitResponseBody();
  }

  protected getDeleteUrl() {
    if (this.entityIds == null) {
      return `${this.deleteUrl}/*`;
    }

    return `${this.deleteUrl}/${this.entityIds.join(',')}`;
  }
}

import {GridElement} from '../../../../elements/grid/grid.element';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {BindableProject, BindProjectsToMilestoneDialog} from '../dialogs/bind-projects-to-milestone.dialog';
import {ProjectView} from '../../../../../model/project/project.model';
import {selectByDataTestToolbarButtonId} from '../../../../../utils/basic-selectors';


export class MilestoneProjectsPanelElement {
  public readonly grid: GridElement;

  constructor() {
    this.grid = GridElement.createGridElement('milestone-projects');
  }

  waitInitialDataFetch() {
  }

  unbindOne(projectName: string) {
    this.grid.findRowId('projectName', projectName).then((id) => {
      this.grid
        .getRow(id)
        .cell('delete')
        .iconRenderer()
        .click();

      const deleteMock = new HttpMockBuilder('milestone-binding/*/unbind-projects/*')
        .delete()
        .build();

      this.clickConfirmDeleteButton();

      deleteMock.wait();
    });
  }

  unbindMultiple(projectNames: string[]) {
    this.grid.selectRowsWithMatchingCellContent('projectName', projectNames);

    const deleteMock = new HttpMockBuilder('milestone-binding/*/unbind-projects/*')
      .delete()
      .build();

    this.clickOnUnbindProjectsButton();
    this.clickConfirmDeleteButton();

    deleteMock.wait();
  }

  clickOnBindProjectsButton(bindableProjects?: BindableProject[]): BindProjectsToMilestoneDialog {
    const dialog = new BindProjectsToMilestoneDialog(this.grid, bindableProjects);

    cy.get('[data-test-button-id="bind-projects"]')
      .should('exist')
      .click();


    dialog.waitInitialDataFetch();
    dialog.assertExist();

    return dialog;
  }


  private clickOnUnbindProjectsButton() {
    cy.get(selectByDataTestToolbarButtonId('unbind-projects'))
      .should('exist')
      .click();
  }

  private clickConfirmDeleteButton() {
    cy.get('sqtm-core-confirm-delete-dialog')
      .find('[data-test-dialog-button-id="confirm"]')
      .click()
      // Then
      .get('sqtm-core-confirm-delete-dialog')
      .should('not.exist');
  }

  showProjectDetail(projectName: string, response: ProjectView): void {
    const mock = new HttpMockBuilder('project-view/*').responseBody(response).build();
    const statusesInUseMock =  new HttpMockBuilder('project-view/*/statuses-in-use')
      .responseBody(response.statusesInUse)
      .build();

    this.grid.findRowId('projectName', projectName)
      .then((id) => {
        this.grid
          .getRow(id)
          .cell('projectName')
          .linkRenderer().findCellLink().click();
      });

    mock.wait();
    statusesInUseMock.wait();
  }
}

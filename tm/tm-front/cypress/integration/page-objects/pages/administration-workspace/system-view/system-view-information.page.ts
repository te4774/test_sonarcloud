import {Page} from '../../page';
import {AdminReferentialDataProviderBuilder} from '../../../../utils/referential/admin-referential-data.provider';
import {AdminReferentialData} from '../../../../model/admin-referential-data.model';
import {HttpMock, HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {AdministrationStatistics, SystemViewModel} from '../../../../model/system/system-view.model';
import {SystemViewMessagesPage} from './system-view-messages.page';
import {SystemViewDownloadsPage} from './system-view-downloads.page';
import {SystemViewSettingsPage} from './system-view-settings.page';
import Chainable = Cypress.Chainable;

type SystemWorkspaceAnchorLink = 'information' | 'messages' | 'downloads' | 'settings';

export class SystemViewInformationPage extends Page {
  private initialDataMock: HttpMock<SystemViewModel>;

  constructor(viewData: SystemViewModel) {
    super('sqtm-app-system-view-information > div');
    this.initialDataMock = new HttpMockBuilder<SystemViewModel>('system-view').responseBody(viewData).build();
  }

  static navigateToPage(viewData: SystemViewModel, referentialData?: AdminReferentialData): SystemViewInformationPage {
    const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(referentialData).build();
    const page = new SystemViewInformationPage(viewData);
    cy.visit('administration-workspace/system/information');
    adminReferentialDataProvider.wait();
    return page;
  }

  waitInitialDataFetch() {
    this.initialDataMock.wait();
  }

  checkVersion(expected: string): void {
    this.getField('appVersion').should('contain.text', expected);
  }

  checkStatistics(statistics: AdministrationStatistics): void {
    const fieldsToCheck: (keyof AdministrationStatistics)[] = ['testCasesNumber', 'campaignsNumber', 'iterationsNumber',
      'executionsNumber', 'requirementsNumber', 'usersNumber', 'projectsNumber', 'databaseSize'];

    fieldsToCheck.forEach(fieldName => this.getField(fieldName).should('contain.text', statistics[fieldName]));
  }

  private getField(fieldName: string): Chainable {
    return cy.get(`${this.rootSelector} [data-test-field-id="${fieldName}"]`);
  }

  checkPlugins(plugins: string[]): void {
    plugins.forEach(plugin => cy.get(`${this.rootSelector} sqtm-app-system-plugins-panel`).should('contain.text', plugin));
  }

  clickMessagesAnchor(): SystemViewMessagesPage {
    this.doClickAnchor('messages');
    return new SystemViewMessagesPage();
  }

  clickDownloadsAnchor(): SystemViewDownloadsPage {
    this.doClickAnchor('downloads');
    return new SystemViewDownloadsPage();
  }

  clickSettingsAnchor(): SystemViewSettingsPage {
    this.doClickAnchor('settings');
    return new SystemViewSettingsPage();
  }

  private doClickAnchor(linkId: SystemWorkspaceAnchorLink): void {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }
}

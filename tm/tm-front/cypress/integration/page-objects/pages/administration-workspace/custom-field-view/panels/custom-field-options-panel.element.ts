import {GridElement} from '../../../../elements/grid/grid.element';
import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {CustomField} from '../../../../../model/customfield/customfield.model';
import {AddCustomFieldOptionDialogElement} from '../dialogs/add-custom-field-option-dialog.element';

export class CustomFieldOptionsPanelElement {
  public readonly grid: GridElement;

  constructor() {
    this.grid = GridElement.createGridElement('options');
  }

  assertDeleteIconIsVisible(optionLabel: string): void {
    this.grid.findRowId('label', optionLabel)
      .then(id => {
        this.grid.getRow(id).cell('delete').iconRenderer().assertIsVisible();
      });
  }

  assertDeleteIconIsHidden(optionLabel: string): void {
    this.grid.findRowId('label', optionLabel)
      .then(id => this.grid.getRow(id).cell('delete').iconRenderer().assertNotExist());
  }

  toggleDefaultOption(optionLabel: string): void {
    const mock = new HttpMockBuilder('custom-fields/*/default-value').post().build();

    this.grid.findRowId('label', optionLabel)
      .then(id => {
        this.grid.getRow(id).cell('default').checkBoxRender().toggleState();
        mock.wait();
      });
  }

  moveOption(optionToMove: string, optionToDropOnto: string, response: CustomField): void {
    const mock = new HttpMockBuilder('custom-fields/*/options/positions')
      .responseBody(response)
      .post().build();

    this.grid.getRow(optionToMove, 'leftViewport').cell('#').findCell()
      .trigger('mousedown', {button: 0})
      .trigger('mousemove', -20, -20, {force: true, buttons: 1})
      .trigger('mousemove', 50, 50, {force: true, buttons: 1});

    this.grid.getRow(optionToDropOnto, 'leftViewport').cell('#').findCell()
      .trigger('mousemove', 'bottom', {force: true, buttons: 1})
      .trigger('mouseup', {force: true});

    mock.wait();
  }

  openAddOptionDialog(): AddCustomFieldOptionDialogElement {
    const dialog = new AddCustomFieldOptionDialogElement();
    this.clickAddButton();
    return dialog;
  }

  clickAddButton(): void {
    cy.get('[data-test-button-id="add-option"]').click();
  }
}

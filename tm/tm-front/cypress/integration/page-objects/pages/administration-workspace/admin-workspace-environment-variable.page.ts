import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {GridElement} from '../../elements/grid/grid.element';
import {PageFactory} from '../page';
import {GridResponse, Identifier} from '../../../model/grids/data-row.type';
import {AdministrationWorkspacePage} from './administration-workspace.page';
import {ReferentialData} from '../../../model/referential-data.model';
import {AdminReferentialDataProviderBuilder} from '../../../utils/referential/admin-referential-data.provider';
import {CreateEnvironmentVariableDialog} from './dialogs/create-environment-variable-dialog';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';

export class AdminWorkspaceEnvironmentVariablePage extends AdministrationWorkspacePage {
  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-custom-workspace');
  }

  public static initTestAtPageEnvironmentVariables: PageFactory<AdminWorkspaceEnvironmentVariablePage> =
    (initialNodes: GridResponse = {dataRows: []}, referentialData?: ReferentialData) => {
      return AdminWorkspaceEnvironmentVariablePage.initTestAtPage(initialNodes, 'environmentVariables',
        'environment-variables', 'entities-customization/environment-variables', referentialData);
    }

    public static initTestAtPage: PageFactory<AdminWorkspaceEnvironmentVariablePage> =
      (initialNodes: GridResponse = {dataRows: []}, gridId: string, gridUrl: string, pageUrl: string,
       referentialData?: ReferentialData) => {
        const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(referentialData).build();
        const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);
        // visit page
        cy.visit(`administration-workspace/${pageUrl}`);
        // wait for ref data request to fire
        adminReferentialDataProvider.wait();
        // wait for initial tree data request to fire
        gridElement.waitInitialDataFetch();
        return new AdminWorkspaceEnvironmentVariablePage(gridElement);
      }

  protected getPageUrl(): string {
    return 'environment-variables';
  }

  protected getDeleteUrl(): string {
    return 'environment-variables';
  }

  openCreateEnvironmentVariable(): CreateEnvironmentVariableDialog {
    this.clickCreateButton();
    return new CreateEnvironmentVariableDialog();
  }

  deleteSingleEnvironmentVariableInGrid(id: Identifier) {
    const iconRenderer = this.grid.getRow(id).cell('delete').iconRenderer();
    iconRenderer.assertIsVisible();
    iconRenderer.click();
  }

  fillSearchInput(value: string, gridResponse?: GridResponse) {
    const mock = new HttpMockBuilder('environment-variables').post().responseBody(gridResponse).build();
    cy.get(selectByDataTestComponentId('environment-variable-filter-field')).find('input').type(value);
    mock.wait();
  }
}

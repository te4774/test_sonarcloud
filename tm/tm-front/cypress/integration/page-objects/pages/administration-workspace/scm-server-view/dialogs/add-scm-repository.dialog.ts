import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {TextFieldElement} from '../../../../elements/forms/TextFieldElement';
import {ScmRepository} from '../../../../../model/scm-server/scm-server.model';


export class AddScmRepositoryDialog  {

  private readonly dialogId = 'add-scm-repository-dialog';

  private readonly nameField: TextFieldElement;
  private readonly branchField: TextFieldElement;
  private readonly repositoryPathField: TextFieldElement;
  private readonly workingFolderPathField: TextFieldElement;

  constructor() {

    this.nameField = new TextFieldElement('scm');
    this.branchField = new TextFieldElement('branch');
    this.repositoryPathField = new TextFieldElement('path');
    this.workingFolderPathField = new TextFieldElement('workingFolderPath');
  }

  fillName(name: string) {
    this.nameField.fill(name);
  }

  fillBranchField(group: string) {
    this.branchField.fill(group);
  }

  fillRepositoryPathField(url: string) {
    this.repositoryPathField.fill(url);
  }

  fillWorkingFolderPathField(url: string) {
    this.workingFolderPathField.fill(url);
  }


  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }

  openCreateScmRepository(name: string, branch: string, repositoryPath: string, workingFolderPath: string) {
    this.fillName(name);
    this.fillBranchField(branch);
    this.fillRepositoryPathField(repositoryPath);
    this.fillWorkingFolderPathField(workingFolderPath);
    this.confirm();
  }

  confirm(updatedRepositories?: ScmRepository[]) {
    const mock = new HttpMockBuilder('scm-repositories/*/new')
      .responseBody({repositories: updatedRepositories})
      .post()
      .build();

    this.clickOnConfirmButton();

    mock.wait();
  }

  cancel() {
    const buttonSelector = this.buildButtonSelector('cancel');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('add');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
  }

  private buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }
}

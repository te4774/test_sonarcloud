import {NavBarElement} from '../../elements/nav-bar/nav-bar.element';
import {GridElement} from '../../elements/grid/grid.element';
import {AdministrationWorkspacePage} from './administration-workspace.page';
import {PageFactory} from '../page';
import {GridResponse} from '../../../model/grids/data-row.type';
import {ReferentialData} from '../../../model/referential-data.model';
import {CreateTeamDialog} from './dialogs/create-team-dialog.element';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {TeamViewPage} from './team-view/team-view.page';
import {Team} from '../../../model/team/team.model';
import {AdminReferentialDataProviderBuilder} from '../../../utils/referential/admin-referential-data.provider';


export class AdminWorkspaceTeamsPage extends AdministrationWorkspacePage {

  public readonly navBar = new NavBarElement();

  constructor(public readonly grid: GridElement) {
    super(grid, 'sqtm-app-main-user-workspace');
  }

  public static initTestAtPageTeams: PageFactory<AdminWorkspaceTeamsPage> =
    (initialNodes: GridResponse = {dataRows: []}, referentialData?: ReferentialData) => {
      return AdminWorkspaceTeamsPage.initTestAtPage(initialNodes, 'teams', 'teams', 'users/teams', referentialData);
    }

  public static initTestAtPage: PageFactory<AdminWorkspaceTeamsPage> =
    (initialNodes: GridResponse = {dataRows: []}, gridId: string, gridUrl: string, pageUrl: string,
     referentialData?: ReferentialData) => {
      const adminReferentialDataProvider = new AdminReferentialDataProviderBuilder(referentialData).build();
      const gridElement = GridElement.createGridElement(gridId, gridUrl, initialNodes);

      // visit page
      cy.visit(`administration-workspace/${pageUrl}`);

      // wait for ref data request to fire
      adminReferentialDataProvider.wait();

      // wait for initial tree data request to fire
      const page = new AdminWorkspaceTeamsPage(gridElement);

      page.waitInitialDataFetch();

      return page;
    }

  openCreateTeam(): CreateTeamDialog {
    this.clickCreateButton();
    return new CreateTeamDialog();
  }

  selectTeamByName(name: string,
                    viewResponse?: Team): TeamViewPage {

    const teamView = new TeamViewPage();
    const mock = new HttpMockBuilder('team-view/*')
      .responseBody(viewResponse)
      .build();

    this.selectRowWithMatchingCellContent('name', name);

    mock.waitResponseBody().then(() => {
      teamView.waitInitialDataFetch();
      teamView.assertExist();
    });

    return teamView;
  }

  selectTeamsByName(names: string[]) {
    this.selectRowsWithMatchingCellContent('name', names);
  }

  deleteTeamByName(name: string, confirm: boolean) {
    this.deleteRowWithMatchingCellContent('name', name, confirm);
  }

  protected getPageUrl(): string {
    return 'teams';
  }

  protected getDeleteUrl(): string {
    return 'teams';
  }
}

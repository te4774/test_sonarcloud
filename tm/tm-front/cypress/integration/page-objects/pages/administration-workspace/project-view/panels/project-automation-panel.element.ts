import {HttpMockBuilder} from '../../../../../utils/mocks/request-mock';
import {Page} from '../../../page';
import {EditableSelectFieldElement} from '../../../../elements/forms/editable-select-field.element';
import {SwitchFieldElement} from '../../../../elements/forms/switch-field.element';
import {GridElement} from '../../../../elements/grid/grid.element';
import {TAUsageStatus, TestAutomationProject} from '../../../../../model/test-automation/test-automation-project.model';
import {AddJobDialogElement} from '../dialogs/add-job-dialog.element';
import {EditableNumericFieldElement} from '../../../../elements/forms/editable-numeric-field.element';

export class ProjectAutomationPanelElement extends Page {

  public readonly workflowTypeField: EditableSelectFieldElement;
  public readonly scmServerSelectField: EditableSelectFieldElement;
  public readonly scmRepositorySelectField: EditableSelectFieldElement;
  public readonly executionServerField: EditableSelectFieldElement;
  public readonly useTreeStructureField: SwitchFieldElement;
  public readonly taServerJobsGrid: GridElement;
  public readonly automatedSuitesLifetimeField: EditableNumericFieldElement;
  public readonly bddTechnologyField: EditableSelectFieldElement;
  public readonly bddScriptLanguageField: EditableSelectFieldElement;

  constructor() {
    super('sqtm-app-project-automation-panel');

    this.workflowTypeField = new EditableSelectFieldElement(
      'automation-workflow',
      'generic-projects/*/automation-workflow-type');

    this.scmServerSelectField = new EditableSelectFieldElement(
      'automation-scm-server',
      'generic-projects/*/scm-repository-id',
    );

    this.automatedSuitesLifetimeField = new EditableNumericFieldElement(
      'automated-suites-lifetime',
      'generic-projects/*/automated-suites-lifetime'
    );

    this.bddTechnologyField = new EditableSelectFieldElement(
      'bdd-implementation-technology',
      'generic-projects/*/bdd-implementation-technology'
    );

    this.bddScriptLanguageField = new EditableSelectFieldElement(
      'bdd-script-language',
      'generic-projects/*/bdd-script-language'
    );

    this.scmRepositorySelectField = new EditableSelectFieldElement(
      'automation-scm-repository',
      'generic-projects/*/scm-repository-id',
    );

    this.useTreeStructureField = new SwitchFieldElement(
      'use-tree-structure',
      'generic-projects/*/use-tree-structure-in-scm-repo',
    );

    this.executionServerField = new EditableSelectFieldElement(
      'automation-execution-server',
      'generic-projects/*/ta-server-id',
    );

    this.taServerJobsGrid = GridElement.createGridElement('project-ta-jobs');
  }

  waitInitialDataFetch() {
  }

  selectAutomationWorkflowType(type: 'Aucun' | 'Squash' | 'Serveur distant') {
    this.workflowTypeField.setAndConfirmValueNoButton(type);
  }

  checkScmServerBlockVisibility(shouldBeVisible: boolean) {
    cy.get(`[data-test-component-id="scm-block"]`).should(shouldBeVisible ? 'be.visible' : 'not.exist');
  }

  checkScmRepositoryFieldVisibility(shouldBeVisible: boolean) {
    cy.get(`[data-test-field-id="automation-scm-repository"]`).should(shouldBeVisible ? 'be.visible' : 'not.exist');
  }

  checkTaJobsBlockVisibility(shouldBeVisible: boolean) {
    cy.get(`[data-test-component-id="ta-jobs-block"]`).should(shouldBeVisible ? 'be.visible' : 'not.exist');
  }

  assertEnvironmentsBlockIsHidden(): void {
    cy.get(`[data-test-component-id="environments-block"]`).should('not.exist');
  }

  assertEnvironmentsBlockIsVisible(): void {
    cy.get(`[data-test-component-id="environments-block"]`).should('be.visible');
  }

  selectScmServer(serverName: string) {
    this.scmServerSelectField.setAndConfirmValueNoButton(serverName);
  }

  deleteSingleJob(jobName: string, hasExecutedTests?: boolean, updatedJobs?: TestAutomationProject[]) {
    this.taServerJobsGrid.findRowId('label', jobName).then((id) => {
      const usageStatusMock = new HttpMockBuilder<{ usageStatus: TAUsageStatus }>('test-automation-projects/*/usage-status')
        .responseBody({
          usageStatus: {
            hasExecutedTests,
            hasBoundProject: false,
          }
        }).build();

      this.taServerJobsGrid
        .getRow(id)
        .cell('delete')
        .iconRenderer()
        .click();

      usageStatusMock.wait();
      cy.get('sqtm-core-confirm-delete-dialog').should('exist');

      const deleteMock = new HttpMockBuilder('test-automation-projects/*')
        .delete()
        .responseBody({taProjects: updatedJobs})
        .build();

      cy.get('button[data-test-dialog-button-id="confirm"]').click();
      deleteMock.wait();
    });
  }

  openAddJobDialog(availableJobs: TestAutomationProject[]): AddJobDialogElement {
    const dialog = new AddJobDialogElement(this.taServerJobsGrid, availableJobs);

    this.clickAddJobButton();

    dialog.waitForInitialDataFetch();
    return dialog;
  }

  openAddJobDialogWithError(): AddJobDialogElement {
    const dialog = new AddJobDialogElement(this.taServerJobsGrid, null);
    const availableJobsMock = new HttpMockBuilder('project-view/4/available-ta-projects')
      .status(412).build();

    this.clickAddJobButton();
    availableJobsMock.wait();

    dialog.checkConnectionError(true);
    dialog.checkConfirmButtonVisibility(false);

    return dialog;
  }

  changeJobLabel(remoteName: string, newLabel: string, response?: TestAutomationProject[]) {
    const mock = new HttpMockBuilder('test-automation-projects/*/label')
      .post()
      .responseBody({taProjects: response || []})
      .build();

    this.taServerJobsGrid.findRowId('remoteName', remoteName).then((id) => {
      this.taServerJobsGrid.getRow(id)
        .cell('label')
        .textRenderer()
        .editText(newLabel);
    });

    mock.wait();
  }

  toggleJobCanRunBdd(remoteName: string, response?: TestAutomationProject[]) {
    const mock = new HttpMockBuilder('test-automation-projects/*/can-run-bdd')
      .post()
      .responseBody({taProjects: response || []})
      .build();

    this.taServerJobsGrid.findRowId('remoteName', remoteName).then((id) => {
      this.taServerJobsGrid.getRow(id)
        .cell('canRunBdd')
        .checkBoxRender()
        .toggleState();

      mock.wait();
    });
  }

  checkJobCanRunBdd(remoteName: string, canRunBdd: boolean) {
    this.taServerJobsGrid.findRowId('remoteName', remoteName).then((id) => {
      this.taServerJobsGrid.getRow(id)
        .cell('canRunBdd')
        .checkBoxRender()
        .assertIsCheck(canRunBdd);
    });
  }

  private clickAddJobButton() {
    cy.get('[data-test-button-id="add-job"]').click();
  }
}

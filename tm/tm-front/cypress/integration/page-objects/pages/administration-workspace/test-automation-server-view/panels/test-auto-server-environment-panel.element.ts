import {Page} from '../../../page';
import {
  EnvironmentSelectionPanelElement
} from '../../../../elements/automated-execution-environments/environment-selection-panel.element';

export class TestAutoServerEnvironmentElement extends Page {

  readonly environmentSelectionPanel = new EnvironmentSelectionPanelElement();

  constructor() {
    super('sqtm-app-test-automation-server-environment-panel');
  }

  assertNotExist() {
    cy.get(this.rootSelector).should('not.exist');
  }

}

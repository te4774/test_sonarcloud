import {Page} from '../../../page';
import {EditableTextFieldElement} from '../../../../elements/forms/editable-text-field.element';
import {EditableRichTextFieldElement} from '../../../../elements/forms/editable-rich-text-field.element';
import {CheckBoxElement} from '../../../../elements/forms/check-box.element';

export class TestAutoServerInfoPanelElement extends Page {
  urlField: EditableTextFieldElement;
  loginField: EditableTextFieldElement;
  passwordField: EditableTextFieldElement;
  descriptionField: EditableRichTextFieldElement;
  manualSlaveSelection: CheckBoxElement;
  observerUrlField: EditableTextFieldElement;

  constructor() {
    super('sqtm-app-test-automation-server-information-panel');

    const urlPrefix = 'test-automation-servers/*/';
    this.urlField = new EditableTextFieldElement('test-automation-server-url', urlPrefix + 'base-url');
    this.loginField = new EditableTextFieldElement('test-automation-server-login', urlPrefix + 'login');
    this.passwordField = new EditableTextFieldElement('test-automation-server-password', urlPrefix + 'password');
    this.descriptionField = new EditableRichTextFieldElement('test-automation-server-description', urlPrefix + 'description');
    this.manualSlaveSelection = new CheckBoxElement('manualSlaveSelection', urlPrefix + 'manual-selection');
    this.observerUrlField = new EditableTextFieldElement('squash-autom-server-observer-url', urlPrefix + 'observer-url');
  }

  assertSquashAUTOMFieldsAreHidden(): void {
    this.observerUrlField.assertNotExist();
  }

  assertSquashAUTOMFieldsAreVisible(): void {
    this.observerUrlField.assertExist();
  }
}

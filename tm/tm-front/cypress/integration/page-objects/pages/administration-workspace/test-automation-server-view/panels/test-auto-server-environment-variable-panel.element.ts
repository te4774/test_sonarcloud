import {Page} from '../../../page';
import {
  EnvironmentVariablePanelElement
} from '../../../../elements/environment-variables/environment-variable-panel.element';

export class TestAutoServerEnvironmentVariableElement extends Page {

  readonly environmentSelectionPanel = new EnvironmentVariablePanelElement();

  constructor() {
    super('sqtm-app-tas-environment-variables-panel');
  }

  assertNotExist() {
    cy.get(this.rootSelector).should('not.exist');
  }

}

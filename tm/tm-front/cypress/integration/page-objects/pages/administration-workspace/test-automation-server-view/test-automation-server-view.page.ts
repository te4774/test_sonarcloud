import {HttpMock, HttpMockBuilder} from '../../../../utils/mocks/request-mock';
import {EditableTextFieldElement} from '../../../elements/forms/editable-text-field.element';
import {Page} from '../../page';
import {TestAutoServerAuthPolicyElement} from './panels/test-auto-server-auth-policy.element';
import {TestAutoServerAuthProtocolElement} from './panels/test-auto-server-auth-protocol.element';
import {TestAutoServerEnvironmentElement} from './panels/test-auto-server-environment-panel.element';
import {TestAutoServerInfoPanelElement} from './panels/test-auto-server-info-panel.element';
import {
  ProjectEnvironmentPanelResponse
} from '../../../elements/automated-execution-environments/environment-selection-panel.element';
import {TestAutoServerEnvironmentVariableElement} from './panels/test-auto-server-environment-variable-panel.element';

export class TestAutomationServerViewPage extends Page {

  readonly entityNameField: EditableTextFieldElement;

  readonly informationPanel = new TestAutoServerInfoPanelElement();
  readonly authPolicyPanel = new TestAutoServerAuthPolicyElement();
  readonly authProtocolPanel = new TestAutoServerAuthProtocolElement();
  readonly environmentPanel = new TestAutoServerEnvironmentElement();
  readonly environmentVariablePanel = new TestAutoServerEnvironmentVariableElement();

  readonly fetchAvailableEnvironmentsMock: HttpMock<any>;

  constructor(mockData?: ProjectEnvironmentPanelResponse) {
    super('sqtm-app-test-automation-server-view');

    this.entityNameField = new EditableTextFieldElement('entity-name', 'test-automation-servers/*/name');

    if (mockData != null) {
      this.fetchAvailableEnvironmentsMock = new HttpMockBuilder('test-automation-servers/*/automated-execution-environments/all?*')
        .responseBody(mockData)
        .build();
    }
  }

  waitInitialDataFetch() {
  }

  foldGrid() {
    cy.get(`[data-test-component-id="fold-tree-button"]`).click();
  }

  private clickOnAnchorLink(linkId: TestAutomationServerViewAnchorLinks) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }
  public showEnvironmentVariablePanel(linkId: 'environment-variables') {
    this.clickOnAnchorLink(linkId);
  }
}

export type TestAutomationServerViewAnchorLinks = 'environment-variables' | 'information';


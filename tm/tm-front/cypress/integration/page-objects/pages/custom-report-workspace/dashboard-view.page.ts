import {EntityViewPage} from '../page';
import {apiBaseUrl, HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {ChartBinding, ReportBinding} from '../../../model/custom-report/custom-dashboard.model';
import {ToolbarElement} from '../../elements/workspace-common/toolbar.element';

class ChartInformationPanelElement {
  constructor(private customReportLibraryNodeId: number | '*') {
  }
}

class ChartPanelElement {
  constructor(customReportLibraryNodeId: number | '*') {
  }
}

export class DashboardViewPage extends EntityViewPage {

  constructor(private customReportLibraryNodeId: number | '*') {
    super('sqtm-app-dashboard-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/dashboard-view/${this.customReportLibraryNodeId}?**`;
    cy.wait(`@${url}`);
  }

  clickAnchorLink<T>(linkId: DashboardAnchorLinks, data?: any): T {
    let element: T;
    switch (linkId) {
      case 'dashboard':
        element = this.showDashboardPanel(linkId) as unknown as T;
        break;
      case 'information':
        element = this.showInformationPanel(linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    return element;
  }

  private showDashboardPanel<T>(linkId: 'dashboard'): ChartPanelElement {
    this.clickOnAnchorLink(linkId);
    return new ChartPanelElement(this.customReportLibraryNodeId);
  }

  private showInformationPanel<T>(linkId: 'information'): ChartInformationPanelElement {
    this.clickOnAnchorLink(linkId);
    return new ChartInformationPanelElement(this.customReportLibraryNodeId);
  }

  private clickOnAnchorLink(linkId: string) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

  showFavoriteList(): ToolbarElement {
    return new ToolbarElement('favorite-icon');
  }

  assertBindingIsRendered(bindingId: number) {
    cy.get(this.rootSelector).find(this.getBindingSelector(bindingId)).should('exist');
  }

  assertBindingIsNotRendered(bindingId: number) {
    cy.get(this.rootSelector).find(this.getBindingSelector(bindingId)).should('not.exist');
  }

  private getBindingSelector(bindingId: number | string) {
    return this.selectByComponentId(`custom-dashboard-binding-${bindingId}`);
  }

  assertChartBindingIsRendered(bindingId: number) {
    cy.get(this.rootSelector)
      .find(this.getBindingSelector(bindingId))
      .find('.js-plotly-plot .main-svg')
      .should('exist');
  }

  assertReportBindingIsRendered(bidingId: number, reportDefinitionId: number) {
    cy.get(this.rootSelector)
      .find(this.getBindingSelector(bidingId))
      .find(this.selectByComponentId(`report-binding-${reportDefinitionId}`))
      .should('exist');
  }

  assertEmptyDropzoneIsVisible() {
    cy.get(this.rootSelector).find(this.selectByComponentId('empty-dashboard-dropzone')).should('be.visible');
  }

  assertEmptyDropzoneIsNotVisible() {
    cy.get(this.rootSelector).find(this.selectByComponentId('empty-dashboard-dropzone')).should('not.exist');
  }

  assertDropzoneIsVisible() {
    cy.get(this.rootSelector).find(this.selectByComponentId('dashboard-dropzone')).should('be.visible');
  }

  dropChartIntoEmptyZone(dashboardId = '*', createdBiding?: ChartBinding) {
    const httpMock = new HttpMockBuilder(`dashboard/${dashboardId}/chart-binding`).post().responseBody(createdBiding).build();
    cy.get(this.rootSelector).find(this.selectByComponentId('empty-dashboard-dropzone')).trigger('mouseup', {force: true});
    httpMock.wait();
  }

  dropChart(dashboardId = '*', createdBinding?: ChartBinding) {
    const httpMock = new HttpMockBuilder(`dashboard/${dashboardId}/chart-binding`).post().responseBody(createdBinding).build();
    cy.get(this.rootSelector).find(this.selectByComponentId('dashboard-dropzone')).trigger('mouseup', {force: true});
    httpMock.wait();
  }

  dropReportIntoEmptyZone(dashboardId = '*', createdBinding?: ReportBinding) {
    const httpMock = new HttpMockBuilder(`dashboard/${dashboardId}/report-binding`).post().responseBody(createdBinding).build();
    cy.get(this.rootSelector).find(this.selectByComponentId('empty-dashboard-dropzone')).trigger('mouseup', {force: true});
    httpMock.wait();
  }

  dropReport(dashboardId = '*', createdBinding?: ReportBinding) {
    const httpMock = new HttpMockBuilder(`dashboard/${dashboardId}/report-binding`).post().responseBody(createdBinding).build();
    cy.get(this.rootSelector).find(this.selectByComponentId('dashboard-dropzone')).trigger('mouseup', {force: true});
    httpMock.wait();
  }

  removeChart(bindingId: number) {
    const httpMock = new HttpMockBuilder(`dashboard/chart-binding/${bindingId ? bindingId : '*'}`).delete().build();
    cy.get(this.rootSelector)
      .find(this.getBindingSelector(bindingId))
      .find(this.selectByComponentId('custom-dashboard-remove-binding'))
      .click({force: true});
    httpMock.wait();
  }

  removeReport(bindingId: number) {
    const httpMock = new HttpMockBuilder(`dashboard/report-binding/${bindingId ? bindingId : '*'}`).delete().build();
    cy.get(this.rootSelector)
      .find(this.getBindingSelector(bindingId))
      .find(this.selectByComponentId('custom-dashboard-remove-binding'))
      .click({force: true});
    httpMock.wait();
  }

  dropForSwappingChart(bindingId = '*', newChartNodeId = '*', createdBinding?: ChartBinding) {
    const httpMock = new HttpMockBuilder(`dashboard/chart-binding/${bindingId}/swap/${newChartNodeId}`)
      .post()
      .responseBody(createdBinding)
      .build();

    cy.get(this.rootSelector)
      .find(this.getBindingSelector(bindingId))
      .find('div')
      .first()
      .trigger('mouseup', {force: true});

    httpMock.wait();
  }
}

export type DashboardAnchorLinks = 'dashboard' | 'information';

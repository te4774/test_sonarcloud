import {Page} from '../page';
import {ReferentialData} from '../../../model/referential-data.model';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder
} from '../../../utils/referential/referential-data.provider';
import {ChartDefinitionModel, ChartWorkbenchData} from '../../../model/custom-report/chart-definition.model';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {AxisSelectorElement} from '../../elements/chart-workbench/axis-selector.element';
import {SimpleSelectFieldElement} from '../../elements/workspace-common/simple-select-field.element';
import {DataRow} from '../../../model/grids/data-row.type';
import {CustomReportWorkspacePage} from './custom-report-workspace.page';
import {TreeElement} from '../../elements/grid/grid.element';

export class ModifyChartViewPage extends Page {

  measureSelector = new AxisSelectorElement('Measure');
  axisSelector = new AxisSelectorElement('Axis');
  seriesSelector = new AxisSelectorElement('Series');
  chartTypeSelector = new SimpleSelectFieldElement('chart-type-selector');

  constructor() {
    super('sqtm-app-modify-chart-view');
  }

  public static initTestAtPage(
    chartDefinition: Partial<ChartDefinitionModel>,
    referentialData: ReferentialData = basicReferentialData,
    chartWorkbenchData: ChartWorkbenchData,
    nodeReference: string = '*'): ModifyChartViewPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const mock = new HttpMockBuilder(`chart-workbench/${nodeReference}`).responseBody(chartWorkbenchData).build();

    const chartDefinitionMock = new HttpMockBuilder(`chart-definition-view/${nodeReference}?**`).responseBody(chartDefinition).build();
    const chartPreviewMock = new HttpMockBuilder(`chart-workbench/preview/${chartDefinition.projectId}`)
      .post()
      .responseBody(chartDefinition)
      .build();

    // visit page
    cy.visit(`custom-report-workspace/modify-chart-definition/${nodeReference}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    mock.wait();
    chartDefinitionMock.wait();
    chartPreviewMock.wait();
    return new ModifyChartViewPage();
  }

  assertChartIsRendered() {
    cy.get('.js-plotly-plot .main-svg').should('exist');
    cy.get(this.selectByComponentId('hint-message')).should('not.exist');
  }

  saveChartModifications(containerId: string = '*',
                         initialNodes?: DataRow[],
                         model?: Partial<ChartDefinitionModel>,
                         referentialData?: ReferentialData)
    : CustomReportWorkspacePage {
    const modifyChartMock = new HttpMockBuilder(`chart-workbench/update/${containerId}`).post().build();
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement('custom-report-workspace-main-tree', 'custom-report-tree', {dataRows: initialNodes});
    const chartDefMock = new HttpMockBuilder(`chart-definition-view/${model.customReportLibraryNodeId}?**`).responseBody(model).build();
    cy.get(this.rootSelector).find(this.selectByComponentId('modify-chart')).click();
    modifyChartMock.wait();
    referentialDataProvider.wait();
    tree.waitInitialDataFetch();
    chartDefMock.wait();
    return new CustomReportWorkspacePage(tree, 'sqtm-app-custom-report-workspace');
  }
}

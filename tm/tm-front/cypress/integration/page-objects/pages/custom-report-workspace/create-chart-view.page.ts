import {Page} from '../page';
import {ReferentialData} from '../../../model/referential-data.model';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder
} from '../../../utils/referential/referential-data.provider';
import {ChartBuildingBlocks, ChartWorkbenchData} from '../../../model/custom-report/chart-definition.model';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {TreeElement} from '../../elements/grid/grid.element';
import {AxisSelectorElement} from '../../elements/chart-workbench/axis-selector.element';
import {DataRow} from '../../../model/grids/data-row.type';
import {CustomReportWorkspacePage} from './custom-report-workspace.page';
import {SimpleSelectFieldElement} from '../../elements/workspace-common/simple-select-field.element';

export class CreateChartViewPage extends Page {

  measureSelector = new AxisSelectorElement('Measure');
  axisSelector = new AxisSelectorElement('Axis');
  seriesSelector = new AxisSelectorElement('Series');
  chartTypeSelector = new SimpleSelectFieldElement('chart-type-selector');

  constructor() {
    super('sqtm-app-create-chart-view');
  }

  public static initTestAtPage(
    referentialData: ReferentialData = basicReferentialData,
    chartWorkbenchData: ChartWorkbenchData = basicWorkbenchData,
    nodeReference: string = '*'): CreateChartViewPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const mock = new HttpMockBuilder(`chart-workbench/${nodeReference}`).responseBody(chartWorkbenchData).build();
    // visit page
    cy.visit(`custom-report-workspace/create-chart-definition/${nodeReference}`);
    // wait for ref data request to fire
    referentialDataProvider.wait();
    mock.wait();
    return new CreateChartViewPage();
  }

  assertChartIsRendered() {
    cy.get('.js-plotly-plot .main-svg').should('exist');
    cy.get(this.selectByComponentId('hint-message')).should('not.exist');
  }

  assertHintMessageIsVisible() {
    cy.get('.js-plotly-plot .main-svg').should('not.exist');
    cy.get(this.selectByComponentId('hint-message')).should('exist');
  }

  assertMissingAxesMessageIsVisible() {
    cy.get('.js-plotly-plot .main-svg').should('not.exist');
    cy.get(this.selectByComponentId('missing-axes-message')).should('exist');
  }

  saveChart(containerId: string = '*', createdChartId?: number, initialNodes?: DataRow[], model?, referentialData?: ReferentialData)
    : CustomReportWorkspacePage {
    const createChartMock = new HttpMockBuilder(`chart-workbench/new/${containerId}`).post().responseBody({id: createdChartId}).build();
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement('custom-report-workspace-main-tree', 'custom-report-tree', {dataRows: initialNodes});
    const chartDefMock = new HttpMockBuilder(`chart-definition-view/${createdChartId ? createdChartId : '*'}?**`)
      .responseBody(model).build();
    cy.get(this.rootSelector).find(this.selectByComponentId('add-chart')).click();
    createChartMock.wait();
    referentialDataProvider.wait();
    tree.waitInitialDataFetch();
    chartDefMock.wait();
    return new CustomReportWorkspacePage(tree, 'sqtm-app-custom-report-workspace');
  }

}

// I swear i cannot do nothing about this 3k lines madness... Will not try to abbreviate the whole thing...
// it's just a massive amount of fix server data, the same at each chart creation...
// if it change someday, just copy paste the whole json bunch sent from server...
const chartBuildingBlocks: ChartBuildingBlocks = {
  'columnPrototypes': {
    'REQUIREMENT': [{
      'id': 1,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_ID',
      'specializedType': {
        'entityType': 'REQUIREMENT',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'id',
      'business': true
    }, {
      'id': 2,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 1,
        'name': 'REQUIREMENT_NB_VERSIONS_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'INNER_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 1,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 1,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'REQUIREMENT',
            'entityRole': null
          },
          'entityType': 'REQUIREMENT',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'REQUIREMENT_VERSION',
            'entityRole': null
          },
          'entityType': 'REQUIREMENT_VERSION',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 1,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 1,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'REQUIREMENT',
            'entityRole': null
          },
          'entityType': 'REQUIREMENT',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'REQUIREMENT_VERSION',
            'entityRole': null
          },
          'entityType': 'REQUIREMENT_VERSION',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'REQUIREMENT_NB_VERSIONS',
      'specializedType': {
        'entityType': 'REQUIREMENT',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(requirementVersionCoverages)',
      'business': true
    }, {
      'id': 4,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_CRITICALITY',
      'specializedType': {
        'entityType': 'REQUIREMENT',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'LEVEL_ENUM',
      'attributeName': 'resource.criticality',
      'business': true
    }, {
      'id': 5,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_STATUS',
      'specializedType': {
        'entityType': 'REQUIREMENT',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'REQUIREMENT_STATUS',
      'attributeName': 'resource.status',
      'business': true
    }, {
      'id': 6,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_CATEGORY',
      'specializedType': {
        'entityType': 'REQUIREMENT',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'INFO_LIST_ITEM',
      'attributeName': 'resource.category.code',
      'business': true
    }
    ],
    'REQUIREMENT_VERSION': [{
      'id': 7,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_ID',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'id',
      'business': true
    }, {
      'id': 8,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_REFERENCE',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'reference',
      'business': true
    }, {
      'id': 9,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_CATEGORY',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'INFO_LIST_ITEM',
      'attributeName': 'category.code',
      'business': true
    }, {
      'id': 10,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_CRITICALITY',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'LEVEL_ENUM',
      'attributeName': 'criticality',
      'business': true
    }, {
      'id': 11,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_STATUS',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'REQUIREMENT_STATUS',
      'attributeName': 'status',
      'business': true
    }, {
      'id': 12,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_CREATED_BY',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'audit.createdBy',
      'business': true
    }, {
      'id': 13,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_CREATED_ON',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'audit.createdOn',
      'business': true
    }, {
      'id': 14,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_MODIFIED_BY',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'audit.lastModifiedBy',
      'business': true
    }, {
      'id': 15,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_MODIFIED_ON',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'audit.lastModifiedOn',
      'business': true
    }, {
      'id': 16,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_VERS_NUM',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': null
      },
      'role': ['MEASURE', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'versionNumber',
      'business': true
    }, {
      'id': 17,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 2,
        'name': 'REQUIREMENT_VERSION_TCCOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'REQUIREMENT_VERSION',
            'entityRole': null
          },
          'entityType': 'REQUIREMENT_VERSION',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'REQUIREMENT_VERSION',
            'entityRole': null
          },
          'entityType': 'REQUIREMENT_VERSION',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'REQUIREMENT_VERSION_TCCOUNT',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(requirementVersionCoverages)',
      'business': true
    }, {
      'id': 18,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 3,
        'name': 'REQUIREMENT_VERSION_MILCOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'REQUIREMENT_VERSION',
            'entityRole': null
          },
          'entityType': 'REQUIREMENT_VERSION',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 89,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_MILESTONE_ID',
            'specializedType': {
              'entityType': 'MILESTONE',
              'entityRole': 'REQUIREMENT_VERSION_MILESTONE'
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 89,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_MILESTONE_ID',
            'specializedType': {
              'entityType': 'MILESTONE',
              'entityRole': 'REQUIREMENT_VERSION_MILESTONE'
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'MILESTONE',
            'entityRole': 'REQUIREMENT_VERSION_MILESTONE'
          },
          'entityType': 'MILESTONE',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'REQUIREMENT_VERSION',
            'entityRole': null
          },
          'entityType': 'REQUIREMENT_VERSION',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 89,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_MILESTONE_ID',
            'specializedType': {
              'entityType': 'MILESTONE',
              'entityRole': 'REQUIREMENT_VERSION_MILESTONE'
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 89,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_MILESTONE_ID',
            'specializedType': {
              'entityType': 'MILESTONE',
              'entityRole': 'REQUIREMENT_VERSION_MILESTONE'
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'MILESTONE',
            'entityRole': 'REQUIREMENT_VERSION_MILESTONE'
          },
          'entityType': 'MILESTONE',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'REQUIREMENT_VERSION_MILCOUNT',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(milestones)',
      'business': true
    }, {
      'id': 95,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_CUF_TEXT',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 96,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_CUF_CHECKBOX',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'BOOLEAN_AS_STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 97,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_CUF_LIST',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'LIST',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 98,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_CUF_DATE',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'DATE_AS_STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 99,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_CUF_TAG',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'TAG',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 100,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'REQUIREMENT_VERSION_CUF_NUMERIC',
      'specializedType': {
        'entityType': 'REQUIREMENT_VERSION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'value',
      'business': true
    }
    ],
    'TEST_CASE': [{
      'id': 19,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'TEST_CASE_ID',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'id',
      'business': true
    }, {
      'id': 20,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'TEST_CASE_REFERENCE',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'reference',
      'business': true
    }, {
      'id': 21,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'TEST_CASE_IMPORTANCE',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'LEVEL_ENUM',
      'attributeName': 'importance',
      'business': true
    }, {
      'id': 22,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'TEST_CASE_NATURE',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'INFO_LIST_ITEM',
      'attributeName': 'nature.code',
      'business': true
    }, {
      'id': 23,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'TEST_CASE_TYPE',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'INFO_LIST_ITEM',
      'attributeName': 'type.code',
      'business': true
    }, {
      'id': 24,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'TEST_CASE_STATUS',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'LEVEL_ENUM',
      'attributeName': 'status',
      'business': true
    }, {
      'id': 25,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'TEST_CASE_CREATED_BY',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'audit.createdBy',
      'business': true
    }, {
      'id': 26,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'TEST_CASE_CREATED_ON',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'audit.createdOn',
      'business': true
    }, {
      'id': 27,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'TEST_CASE_MODIFIED_BY',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'audit.lastModifiedBy',
      'business': true
    }, {
      'id': 28,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'TEST_CASE_MODIFIED_ON',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'audit.lastModifiedOn',
      'business': true
    }, {
      'id': 30,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 4,
        'name': 'TEST_CASE_VERSCOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'REQUIREMENT_VERSION',
            'entityRole': null
          },
          'entityType': 'REQUIREMENT_VERSION',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 7,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'REQUIREMENT_VERSION_ID',
            'specializedType': {
              'entityType': 'REQUIREMENT_VERSION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'REQUIREMENT_VERSION',
            'entityRole': null
          },
          'entityType': 'REQUIREMENT_VERSION',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'TEST_CASE_VERSCOUNT',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(requirementVersionCoverages)',
      'business': true
    }, {
      'id': 31,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 5,
        'name': 'TEST_CASE_CALLSTEPCOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [{
          'column': {
            'id': 134,
            'columnType': 'ENTITY',
            'subQuery': null,
            'label': 'TEST_CASE_STEP_ENTITY',
            'specializedType': {
              'entityType': 'TEST_CASE_STEP',
              'entityRole': null
            },
            'role': [],
            'dataType': 'ENTITY',
            'attributeName': null,
            'business': false
          },
          'operation': 'IS_CLASS',
          'cufId': null,
          'values': ['org.squashtest.tm.domain.testcase.CallTestStep'],
          'specializedType': {
            'entityType': 'TEST_CASE_STEP',
            'entityRole': null
          },
          'entityType': 'TEST_CASE_STEP',
          'dataType': 'ENTITY'
        }
        ],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 79,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_STEP_ID',
            'specializedType': {
              'entityType': 'TEST_CASE_STEP',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 79,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_STEP_ID',
            'specializedType': {
              'entityType': 'TEST_CASE_STEP',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'TEST_CASE_STEP',
            'entityRole': null
          },
          'entityType': 'TEST_CASE_STEP',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 79,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_STEP_ID',
            'specializedType': {
              'entityType': 'TEST_CASE_STEP',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 79,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_STEP_ID',
            'specializedType': {
              'entityType': 'TEST_CASE_STEP',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'TEST_CASE_STEP',
            'entityRole': null
          },
          'entityType': 'TEST_CASE_STEP',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'TEST_CASE_CALLSTEPCOUNT',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(steps[class=\'CallTestStep\'])',
      'business': true
    }, {
      'id': 32,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 6,
        'name': 'TEST_CASE_STEPCOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 79,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_STEP_ID',
            'specializedType': {
              'entityType': 'TEST_CASE_STEP',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 79,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_STEP_ID',
            'specializedType': {
              'entityType': 'TEST_CASE_STEP',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'TEST_CASE_STEP',
            'entityRole': null
          },
          'entityType': 'TEST_CASE_STEP',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 79,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_STEP_ID',
            'specializedType': {
              'entityType': 'TEST_CASE_STEP',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 79,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_STEP_ID',
            'specializedType': {
              'entityType': 'TEST_CASE_STEP',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'TEST_CASE_STEP',
            'entityRole': null
          },
          'entityType': 'TEST_CASE_STEP',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'TEST_CASE_STEPCOUNT',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(steps)',
      'business': true
    }, {
      'id': 33,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 7,
        'name': 'TEST_CASE_VERSION_MILCOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 87,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_MILESTONE_ID',
            'specializedType': {
              'entityType': 'MILESTONE',
              'entityRole': 'TEST_CASE_MILESTONE'
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 87,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_MILESTONE_ID',
            'specializedType': {
              'entityType': 'MILESTONE',
              'entityRole': 'TEST_CASE_MILESTONE'
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'MILESTONE',
            'entityRole': 'TEST_CASE_MILESTONE'
          },
          'entityType': 'MILESTONE',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 87,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_MILESTONE_ID',
            'specializedType': {
              'entityType': 'MILESTONE',
              'entityRole': 'TEST_CASE_MILESTONE'
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 87,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_MILESTONE_ID',
            'specializedType': {
              'entityType': 'MILESTONE',
              'entityRole': 'TEST_CASE_MILESTONE'
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'MILESTONE',
            'entityRole': 'TEST_CASE_MILESTONE'
          },
          'entityType': 'MILESTONE',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'TEST_CASE_MILCOUNT',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(milestones)',
      'business': true
    }, {
      'id': 34,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 8,
        'name': 'TEST_CASE_ITERCOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITERATION',
            'entityRole': null
          },
          'entityType': 'ITERATION',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITERATION',
            'entityRole': null
          },
          'entityType': 'ITERATION',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'TEST_CASE_ITERCOUNT',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(iterations)',
      'business': true
    }, {
      'id': 35,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 9,
        'name': 'TEST_CASE_EXECOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'EXECUTION',
            'entityRole': null
          },
          'entityType': 'EXECUTION',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'EXECUTION',
            'entityRole': null
          },
          'entityType': 'EXECUTION',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'TEST_CASE_EXECOUNT',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(executions)',
      'business': true
    }, {
      'id': 36,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 10,
        'name': 'TEST_CASE_HASAUTOSCRIPT_SUBQUERY',
        'strategy': 'INLINED',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 93,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'AUTOMATED_TEST_ID',
            'specializedType': {
              'entityType': 'AUTOMATED_TEST',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'label': null,
          'operation': 'NOT_NULL',
          'cufId': null,
          'column': {
            'id': 93,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'AUTOMATED_TEST_ID',
            'specializedType': {
              'entityType': 'AUTOMATED_TEST',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'AUTOMATED_TEST',
            'entityRole': null
          },
          'entityType': 'AUTOMATED_TEST',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 93,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'AUTOMATED_TEST_ID',
            'specializedType': {
              'entityType': 'AUTOMATED_TEST',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NOT_NULL',
          'column': {
            'id': 93,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'AUTOMATED_TEST_ID',
            'specializedType': {
              'entityType': 'AUTOMATED_TEST',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'AUTOMATED_TEST',
            'entityRole': null
          },
          'entityType': 'AUTOMATED_TEST',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'TEST_CASE_HASAUTOSCRIPT',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'BOOLEAN',
      'attributeName': 'notnull(automatedTest)',
      'business': true
    }, {
      'id': 101,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'TEST_CASE_CUF_TEXT',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 102,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'TEST_CASE_CUF_CHECKBOX',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'BOOLEAN_AS_STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 103,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'TEST_CASE_CUF_LIST',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'LIST',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 104,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'TEST_CASE_CUF_DATE',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'DATE_AS_STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 105,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'TEST_CASE_CUF_TAG',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'TAG',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 106,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'TEST_CASE_CUF_NUMERIC',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 186,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'TEST_CASE_AUTOMATABLE',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'LEVEL_ENUM',
      'attributeName': 'automatable',
      'business': true
    }, {
      'id': 203,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 28,
        'name': 'TEST_CASE_HAS_BOUND_SCM_REPOSITORY_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 201,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'SCM_REPOSITORY_ID',
            'specializedType': {
              'entityType': 'SCM_REPOSITORY',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'label': null,
          'operation': 'NOT_NULL',
          'cufId': null,
          'column': {
            'id': 201,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'SCM_REPOSITORY_ID',
            'specializedType': {
              'entityType': 'SCM_REPOSITORY',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'SCM_REPOSITORY',
            'entityRole': null
          },
          'entityType': 'SCM_REPOSITORY',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'TEST_CASE_HAS_BOUND_SCM_REPOSITORY',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'BOOLEAN',
      'attributeName': 'notnull(scmRepository)',
      'business': true
    }, {
      'id': 204,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 29,
        'name': 'TEST_CASE_HAS_BOUND_AUTOMATED_TEST_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 202,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_AUTOMATED_TEST_REFERENCE',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'automatedTestReference',
            'business': false
          },
          'label': null,
          'operation': 'NOT_NULL',
          'cufId': null,
          'column': {
            'id': 202,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_AUTOMATED_TEST_REFERENCE',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'automatedTestReference',
            'business': false
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'STRING'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'TEST_CASE_HAS_BOUND_AUTOMATED_TEST_REFERENCE',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'BOOLEAN',
      'attributeName': 'notnull(automatedTestReference)',
      'business': true
    }, {
      'id': 205,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'TEST_CASE_AUTOMATED_TEST_TECHNOLOGY',
      'specializedType': {
        'entityType': 'TEST_CASE',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'AUTOMATED_TEST_TECHNOLOGY',
      'attributeName': 'automatedTestTechnology.name',
      'business': true
    }
    ],
    'CAMPAIGN': [{
      'id': 37,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'CAMPAIGN_ID',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'id',
      'business': true
    }, {
      'id': 39,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'CAMPAIGN_REFERENCE',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': null
      },
      'role': ['MEASURE', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'reference',
      'business': true
    }, {
      'id': 40,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'CAMPAIGN_SCHED_START',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'scheduledPeriod.scheduledStartDate',
      'business': true
    }, {
      'id': 41,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'CAMPAIGN_SCHED_END',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'scheduledPeriod.scheduledEndDate',
      'business': true
    }, {
      'id': 42,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'CAMPAIGN_ACTUAL_START',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'actualPeriod.actualStartDate',
      'business': true
    }, {
      'id': 43,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'CAMPAIGN_ACTUAL_END',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'actualPeriod.actualEndDate',
      'business': true
    }, {
      'id': 44,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 11,
        'name': 'CAMPAIGN_ITERCOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 37,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'CAMPAIGN_ID',
            'specializedType': {
              'entityType': 'CAMPAIGN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 37,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'CAMPAIGN_ID',
            'specializedType': {
              'entityType': 'CAMPAIGN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'CAMPAIGN',
            'entityRole': null
          },
          'entityType': 'CAMPAIGN',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITERATION',
            'entityRole': null
          },
          'entityType': 'ITERATION',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 37,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'CAMPAIGN_ID',
            'specializedType': {
              'entityType': 'CAMPAIGN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 37,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'CAMPAIGN_ID',
            'specializedType': {
              'entityType': 'CAMPAIGN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'CAMPAIGN',
            'entityRole': null
          },
          'entityType': 'CAMPAIGN',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITERATION',
            'entityRole': null
          },
          'entityType': 'ITERATION',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'CAMPAIGN_ITERCOUNT',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(iterations)',
      'business': true
    }, {
      'id': 45,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 12,
        'name': 'CAMPAIGN_ISSUECOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 37,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'CAMPAIGN_ID',
            'specializedType': {
              'entityType': 'CAMPAIGN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 37,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'CAMPAIGN_ID',
            'specializedType': {
              'entityType': 'CAMPAIGN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'CAMPAIGN',
            'entityRole': null
          },
          'entityType': 'CAMPAIGN',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'specializedType': {
            'entityType': 'ISSUE',
            'entityRole': null
          },
          'entityType': 'ISSUE',
          'dataType': 'STRING'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 37,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'CAMPAIGN_ID',
            'specializedType': {
              'entityType': 'CAMPAIGN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 37,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'CAMPAIGN_ID',
            'specializedType': {
              'entityType': 'CAMPAIGN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'CAMPAIGN',
            'entityRole': null
          },
          'entityType': 'CAMPAIGN',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'specializedType': {
            'entityType': 'ISSUE',
            'entityRole': null
          },
          'entityType': 'ISSUE',
          'dataType': 'STRING'
        }
        ]
      },
      'label': 'CAMPAIGN_ISSUECOUNT',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(issues)',
      'business': true
    }, {
      'id': 107,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'CAMPAIGN_CUF_TEXT',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 108,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'CAMPAIGN_CUF_CHECKBOX',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'BOOLEAN_AS_STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 109,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'CAMPAIGN_CUF_LIST',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'LIST',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 110,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'CAMPAIGN_CUF_DATE',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'DATE_AS_STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 111,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'CAMPAIGN_CUF_TAG',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'TAG',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 112,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'CAMPAIGN_CUF_NUMERIC',
      'specializedType': {
        'entityType': 'CAMPAIGN',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'value',
      'business': true
    }
    ],
    'ITERATION': [{
      'id': 46,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITERATION_ID',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'id',
      'business': true
    }, {
      'id': 47,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITERATION_REFERENCE',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': null
      },
      'role': ['MEASURE', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'reference',
      'business': true
    }, {
      'id': 48,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITERATION_SCHED_START',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'scheduledPeriod.scheduledStartDate',
      'business': true
    }, {
      'id': 49,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITERATION_SCHED_END',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'scheduledPeriod.scheduledEndDate',
      'business': true
    }, {
      'id': 50,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITERATION_ACTUAL_START',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'actualPeriod.actualStartDate',
      'business': true
    }, {
      'id': 51,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITERATION_ACTUAL_END',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'actualPeriod.actualEndDate',
      'business': true
    }, {
      'id': 52,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 13,
        'name': 'ITERATION_ITEMCOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITERATION',
            'entityRole': null
          },
          'entityType': 'ITERATION',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITEM_TEST_PLAN',
            'entityRole': null
          },
          'entityType': 'ITEM_TEST_PLAN',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITERATION',
            'entityRole': null
          },
          'entityType': 'ITERATION',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITEM_TEST_PLAN',
            'entityRole': null
          },
          'entityType': 'ITEM_TEST_PLAN',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'ITERATION_ITEMCOUNT',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(testPlans)',
      'business': true
    }, {
      'id': 53,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 14,
        'name': 'ITERATION_ISSUECOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITERATION',
            'entityRole': null
          },
          'entityType': 'ITERATION',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'specializedType': {
            'entityType': 'ISSUE',
            'entityRole': null
          },
          'entityType': 'ISSUE',
          'dataType': 'STRING'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 46,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITERATION_ID',
            'specializedType': {
              'entityType': 'ITERATION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITERATION',
            'entityRole': null
          },
          'entityType': 'ITERATION',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'specializedType': {
            'entityType': 'ISSUE',
            'entityRole': null
          },
          'entityType': 'ISSUE',
          'dataType': 'STRING'
        }
        ]
      },
      'label': 'ITERATION_ISSUECOUNT',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(issues)',
      'business': true
    }, {
      'id': 113,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'ITERATION_CUF_TEXT',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 114,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'ITERATION_CUF_CHECKBOX',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'BOOLEAN_AS_STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 115,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'ITERATION_CUF_LIST',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'LIST',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 116,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'ITERATION_CUF_DATE',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'DATE_AS_STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 117,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'ITERATION_CUF_TAG',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'TAG',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 118,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'ITERATION_CUF_NUMERIC',
      'specializedType': {
        'entityType': 'ITERATION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'value',
      'business': true
    }
    ],
    'ITEM_TEST_PLAN': [{
      'id': 54,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITEM_TEST_PLAN_ID',
      'specializedType': {
        'entityType': 'ITEM_TEST_PLAN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'id',
      'business': true
    }, {
      'id': 55,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITEM_TEST_PLAN_LABEL',
      'specializedType': {
        'entityType': 'ITEM_TEST_PLAN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'label',
      'business': true
    }, {
      'id': 56,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITEM_TEST_PLAN_STATUS',
      'specializedType': {
        'entityType': 'ITEM_TEST_PLAN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'EXECUTION_STATUS',
      'attributeName': 'executionStatus',
      'business': true
    }, {
      'id': 57,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITEM_TEST_PLAN_LASTEXECON',
      'specializedType': {
        'entityType': 'ITEM_TEST_PLAN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'lastExecutedOn',
      'business': true
    }, {
      'id': 58,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITEM_TEST_PLAN_DATASET_LABEL',
      'specializedType': {
        'entityType': 'ITEM_TEST_PLAN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'referencedDataset.name',
      'business': true
    }, {
      'id': 59,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITEM_TEST_PLAN_TESTER',
      'specializedType': {
        'entityType': 'ITEM_TEST_PLAN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'user.login',
      'business': true
    }, {
      'id': 60,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'ITEM_TEST_PLAN_TC_ID',
      'specializedType': {
        'entityType': 'ITEM_TEST_PLAN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'referencedTestCase.id',
      'business': true
    }, {
      'id': 61,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 15,
        'name': 'ITEM_TEST_PLAN_TCDELETED_SUBQUERY',
        'strategy': 'INLINED',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITEM_TEST_PLAN',
            'entityRole': null
          },
          'entityType': 'ITEM_TEST_PLAN',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'IS_NULL',
          'cufId': null,
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITEM_TEST_PLAN',
            'entityRole': null
          },
          'entityType': 'ITEM_TEST_PLAN',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'IS_NULL',
          'column': {
            'id': 19,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'TEST_CASE_ID',
            'specializedType': {
              'entityType': 'TEST_CASE',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'TEST_CASE',
            'entityRole': null
          },
          'entityType': 'TEST_CASE',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'ITEM_TEST_PLAN_TC_DELETED',
      'specializedType': {
        'entityType': 'ITEM_TEST_PLAN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'BOOLEAN',
      'attributeName': 'isnull(referencedTestCase)',
      'business': true
    }, {
      'id': 62,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 16,
        'name': 'ITEM_TEST_PLAN_ISEXECUTED_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITEM_TEST_PLAN',
            'entityRole': null
          },
          'entityType': 'ITEM_TEST_PLAN',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NOT_NULL',
          'cufId': null,
          'column': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'EXECUTION',
            'entityRole': null
          },
          'entityType': 'EXECUTION',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITEM_TEST_PLAN',
            'entityRole': null
          },
          'entityType': 'ITEM_TEST_PLAN',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NOT_NULL',
          'column': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'EXECUTION',
            'entityRole': null
          },
          'entityType': 'EXECUTION',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'ITEM_TEST_PLAN_IS_EXECUTED',
      'specializedType': {
        'entityType': 'ITEM_TEST_PLAN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'BOOLEAN',
      'attributeName': 'notnull(executions)',
      'business': true
    }, {
      'id': 63,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 17,
        'name': 'ITEM_TEST_PLAN_MANEXCOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITEM_TEST_PLAN',
            'entityRole': null
          },
          'entityType': 'ITEM_TEST_PLAN',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [{
          'column': {
            'id': 94,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'AUTOMATED_EXECUTION_EXTENDER_ID',
            'specializedType': {
              'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'operation': 'NOT_NULL',
          'cufId': null,
          'values': ['FALSE'],
          'specializedType': {
            'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
            'entityRole': null
          },
          'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
          'dataType': 'NUMERIC'
        }
        ],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'EXECUTION',
            'entityRole': null
          },
          'entityType': 'EXECUTION',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITEM_TEST_PLAN',
            'entityRole': null
          },
          'entityType': 'ITEM_TEST_PLAN',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'EXECUTION',
            'entityRole': null
          },
          'entityType': 'EXECUTION',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'ITEM_TEST_PLAN_MANEXCOUNT',
      'specializedType': {
        'entityType': 'ITEM_TEST_PLAN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(executions[auto=\'false\'])',
      'business': true
    }, {
      'id': 64,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 18,
        'name': 'ITEM_TEST_PLAN_AUTOEXCOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITEM_TEST_PLAN',
            'entityRole': null
          },
          'entityType': 'ITEM_TEST_PLAN',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [{
          'column': {
            'id': 94,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'AUTOMATED_EXECUTION_EXTENDER_ID',
            'specializedType': {
              'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'operation': 'NOT_NULL',
          'cufId': null,
          'values': ['TRUE'],
          'specializedType': {
            'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
            'entityRole': null
          },
          'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
          'dataType': 'NUMERIC'
        }
        ],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'EXECUTION',
            'entityRole': null
          },
          'entityType': 'EXECUTION',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITEM_TEST_PLAN',
            'entityRole': null
          },
          'entityType': 'ITEM_TEST_PLAN',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'EXECUTION',
            'entityRole': null
          },
          'entityType': 'EXECUTION',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'ITEM_TEST_PLAN_AUTOEXCOUNT',
      'specializedType': {
        'entityType': 'ITEM_TEST_PLAN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(executions[auto=\'true\'])',
      'business': true
    }, {
      'id': 65,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 19,
        'name': 'ITEM_TEST_PLAN_ISSUECOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITEM_TEST_PLAN',
            'entityRole': null
          },
          'entityType': 'ITEM_TEST_PLAN',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'specializedType': {
            'entityType': 'ISSUE',
            'entityRole': null
          },
          'entityType': 'ISSUE',
          'dataType': 'STRING'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 54,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ITEM_TEST_PLAN_ID',
            'specializedType': {
              'entityType': 'ITEM_TEST_PLAN',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'ITEM_TEST_PLAN',
            'entityRole': null
          },
          'entityType': 'ITEM_TEST_PLAN',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'specializedType': {
            'entityType': 'ISSUE',
            'entityRole': null
          },
          'entityType': 'ISSUE',
          'dataType': 'STRING'
        }
        ]
      },
      'label': 'ITEM_TEST_PLAN_ISSUECOUNT',
      'specializedType': {
        'entityType': 'ITEM_TEST_PLAN',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(issues)',
      'business': true
    }
    ],
    'EXECUTION': [{
      'id': 66,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'EXECUTION_ID',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'id',
      'business': true
    }, {
      'id': 67,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'EXECUTION_LABEL',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'name',
      'business': true
    }, {
      'id': 68,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'EXECUTION_DS_LABEL',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'datasetLabel',
      'business': true
    }, {
      'id': 69,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'EXECUTION_LASTEXEC',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'lastExecutedOn',
      'business': true
    }, {
      'id': 70,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'EXECUTION_TESTER_LOGIN',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': null
      },
      'role': ['AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'lastExecutedBy',
      'business': true
    }, {
      'id': 71,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'EXECUTION_STATUS',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'EXECUTION_STATUS',
      'attributeName': 'executionStatus',
      'business': true
    }, {
      'id': 72,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 20,
        'name': 'EXECUTION_ISAUTO_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'EXECUTION',
            'entityRole': null
          },
          'entityType': 'EXECUTION',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 94,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'AUTOMATED_EXECUTION_EXTENDER_ID',
            'specializedType': {
              'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'label': null,
          'operation': 'NOT_NULL',
          'cufId': null,
          'column': {
            'id': 94,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'AUTOMATED_EXECUTION_EXTENDER_ID',
            'specializedType': {
              'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
            'entityRole': null
          },
          'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
          'dataType': 'NUMERIC'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'EXECUTION',
            'entityRole': null
          },
          'entityType': 'EXECUTION',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 94,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'AUTOMATED_EXECUTION_EXTENDER_ID',
            'specializedType': {
              'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NOT_NULL',
          'column': {
            'id': 94,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'AUTOMATED_EXECUTION_EXTENDER_ID',
            'specializedType': {
              'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
              'entityRole': null
            },
            'role': [],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': false
          },
          'specializedType': {
            'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
            'entityRole': null
          },
          'entityType': 'AUTOMATED_EXECUTION_EXTENDER',
          'dataType': 'NUMERIC'
        }
        ]
      },
      'label': 'EXECUTION_ISAUTO',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'BOOLEAN',
      'attributeName': 'notnull(automatedExecutionExtender)',
      'business': true
    }, {
      'id': 73,
      'columnType': 'CALCULATED',
      'subQuery': {
        'id': 21,
        'name': 'EXECUTION_ISSUECOUNT_SUBQUERY',
        'strategy': 'SUBQUERY',
        'joinStyle': 'LEFT_JOIN',
        'aggregationColumns': [{
          'columnPrototype': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'label': null,
          'operation': 'NONE',
          'cufId': null,
          'column': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'EXECUTION',
            'entityRole': null
          },
          'entityType': 'EXECUTION',
          'dataType': 'NUMERIC'
        }
        ],
        'filterColumns': [],
        'projectionColumns': [{
          'columnPrototype': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'label': null,
          'operation': 'COUNT',
          'cufId': null,
          'column': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'specializedType': {
            'entityType': 'ISSUE',
            'entityRole': null
          },
          'entityType': 'ISSUE',
          'dataType': 'STRING'
        }
        ],
        'orderingColumns': [{
          'columnPrototype': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'NONE',
          'column': {
            'id': 66,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'EXECUTION_ID',
            'specializedType': {
              'entityType': 'EXECUTION',
              'entityRole': null
            },
            'role': ['MEASURE', 'AXIS', 'FILTER'],
            'dataType': 'NUMERIC',
            'attributeName': 'id',
            'business': true
          },
          'specializedType': {
            'entityType': 'EXECUTION',
            'entityRole': null
          },
          'entityType': 'EXECUTION',
          'dataType': 'NUMERIC'
        }, {
          'columnPrototype': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'order': 'ASC',
          'cufId': null,
          'operation': 'COUNT',
          'column': {
            'id': 75,
            'columnType': 'ATTRIBUTE',
            'subQuery': null,
            'label': 'ISSUE_REMOTE_ID',
            'specializedType': {
              'entityType': 'ISSUE',
              'entityRole': null
            },
            'role': [],
            'dataType': 'STRING',
            'attributeName': 'remoteIssueId',
            'business': false
          },
          'specializedType': {
            'entityType': 'ISSUE',
            'entityRole': null
          },
          'entityType': 'ISSUE',
          'dataType': 'STRING'
        }
        ]
      },
      'label': 'EXECUTION_ISSUECOUNT',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'count(issues)',
      'business': true
    }, {
      'id': 119,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'EXECUTION_CUF_TEXT',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 120,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'EXECUTION_CUF_CHECKBOX',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'BOOLEAN_AS_STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 121,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'EXECUTION_CUF_LIST',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'LIST',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 122,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'EXECUTION_CUF_DATE',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'DATE_AS_STRING',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 123,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'EXECUTION_CUF_TAG',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'TAG',
      'attributeName': 'value',
      'business': true
    }, {
      'id': 124,
      'columnType': 'CUF',
      'subQuery': null,
      'label': 'EXECUTION_CUF_NUMERIC',
      'specializedType': {
        'entityType': 'EXECUTION',
        'entityRole': 'CUSTOM_FIELD'
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'value',
      'business': true
    }
    ],
    'AUTOMATION_REQUEST': [{
      'id': 179,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'AUTOMATION_REQUEST_STATUS',
      'specializedType': {
        'entityType': 'AUTOMATION_REQUEST',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'LEVEL_ENUM',
      'attributeName': 'requestStatus',
      'business': true
    }, {
      'id': 206,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'AUTOMATION_REQUEST_TRANSMISSION_DATE',
      'specializedType': {
        'entityType': 'AUTOMATION_REQUEST',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'DATE',
      'attributeName': 'transmissionDate',
      'business': true
    }, {
      'id': 207,
      'columnType': 'ATTRIBUTE',
      'subQuery': null,
      'label': 'AUTOMATION_REQUEST_AUTOMATION_PRIORITY',
      'specializedType': {
        'entityType': 'AUTOMATION_REQUEST',
        'entityRole': null
      },
      'role': ['MEASURE', 'AXIS', 'FILTER'],
      'dataType': 'NUMERIC',
      'attributeName': 'automationPriority',
      'business': true
    }
    ]
  },
  'columnRoles': {
    'AXIS': ['NONE', 'BY_DAY', 'BY_WEEK', 'BY_MONTH', 'BY_YEAR'],
    'MEASURE': ['MIN', 'MAX', 'AVG', 'SUM', 'COUNT'],
    'FILTER': ['GREATER', 'GREATER_EQUAL', 'LOWER', 'LOWER_EQUAL', 'BETWEEN', 'EQUALS', 'IN', 'LIKE', 'NOT_EQUALS']
  },
  'dataTypes': {
    'NUMERIC': ['NONE', 'GREATER', 'GREATER_EQUAL', 'LOWER', 'LOWER_EQUAL', 'BETWEEN', 'EQUALS', 'MIN', 'MAX', 'AVG', 'SUM', 'COUNT', 'NOT_EQUALS'],
    'STRING': ['NONE', 'EQUALS', 'LIKE', 'COUNT'],
    'DATE': ['GREATER', 'GREATER_EQUAL', 'LOWER', 'LOWER_EQUAL', 'BETWEEN', 'EQUALS', 'COUNT', 'NOT_EQUALS', 'BY_DAY', 'BY_WEEK', 'BY_MONTH', 'BY_YEAR'],
    'DATE_AS_STRING': ['GREATER', 'GREATER_EQUAL', 'LOWER', 'LOWER_EQUAL', 'BETWEEN', 'EQUALS', 'COUNT', 'NOT_EQUALS', 'BY_DAY', 'BY_MONTH', 'BY_YEAR'],
    'EXISTENCE': ['IS_NULL', 'NOT_NULL'],
    'BOOLEAN': ['NONE', 'EQUALS', 'COUNT'],
    'BOOLEAN_AS_STRING': ['NONE', 'EQUALS', 'COUNT'],
    'LEVEL_ENUM': ['NONE', 'EQUALS', 'IN', 'COUNT'],
    'REQUIREMENT_STATUS': ['NONE', 'EQUALS', 'IN', 'COUNT'],
    'AUTOMATED_TEST_TECHNOLOGY': ['NONE', 'EQUALS', 'IN', 'COUNT'],
    'EXECUTION_STATUS': ['NONE', 'EQUALS', 'IN', 'COUNT'],
    'LIST': ['NONE', 'EQUALS', 'IN', 'COUNT'],
    'INFO_LIST_ITEM': ['NONE', 'EQUALS', 'IN', 'COUNT'],
    'TAG': ['NONE', 'EQUALS', 'IN', 'COUNT'],
    'ENUM': ['NONE', 'EQUALS', 'IN', 'COUNT'],
    'ENTITY': ['IS_CLASS'],
    'TEXT': ['NONE', 'LIKE', 'FULLTEXT']
  },
  'entityTypes': {
    'REQUIREMENT': 'icon-chart-requirement',
    'REQUIREMENT_VERSION': 'icon-chart-requirement-version',
    'TEST_CASE': 'icon-chart-test-case',
    'CAMPAIGN': 'icon-chart-campaign',
    'ITERATION': 'icon-chart-iteration',
    'ITEM_TEST_PLAN': 'icon-chart-item-test-plan',
    'EXECUTION': 'icon-chart-execution'
  }
}as unknown as ChartBuildingBlocks;

export const basicWorkbenchData: ChartWorkbenchData = {
  chartBuildingBlocks: chartBuildingBlocks,
  chartDefinition: null,
  projectId: 1,
  containerId: 1
};


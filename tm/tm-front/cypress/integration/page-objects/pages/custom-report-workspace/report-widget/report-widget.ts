import {ReportInputType} from '../../../../model/custom-report/report-definition.model';
import {selectByDataTestComponentId} from '../../../../utils/basic-selectors';
import {ProjectPickerDialogElement} from '../../../elements/grid/project-picker-dialog.element';
import {TreeElement} from '../../../elements/grid/grid.element';
import {GridResponse, Identifier} from '../../../../model/grids/data-row.type';
import {MilestonePickerDialogElement} from '../../../elements/grid/milestone-picker-dialog.element';
import Chainable = Cypress.Chainable;

export class ReportWidget {

  constructor(private inputId: string) {
  }

  protected select(): Chainable<any> {
    return cy.get(selectByDataTestComponentId(this.inputId));
  }

  assertCriteriaHasTitle(expectedName: string) {
    this.select()
      .find(selectByDataTestComponentId('criteria-name'))
      .should('contain.text', expectedName);
  }

  private selectErrors(): Chainable<any> {
    return this.select()
      .find(selectByDataTestComponentId('criteria-errors'));
  }

  assertRequiredErrorMessageExist() {
    this.selectErrors()
      .should('exist')
      .should('contain.text', 'Ce champ ne peut pas être vide.');
  }

  assertRequiredErrorMessageNotExist() {
    this.selectErrors()
      .should('not.exist');
  }

  // the option is either the option name if simple radio or the giveAccessTo attribute if composite input.
  createFormProjectWidget(optionId: string = 'projectIds'): ProjectPickerOption {
    this.selectOption(optionId).should('exist');
    return new ProjectPickerOption(optionId);
  }

  createFormTreePicker(optionId: string, kind: 'requirement'): TreePickerOption {
    this.selectOption(optionId).should('exist');
    return new TreePickerOption(optionId, kind);
  }

  createFormMilestonePicker(optionId: string = 'milestones'): MilestonePickerOption {
    this.selectOption(optionId).should('exist');
    return new MilestonePickerOption(optionId);
  }

  protected selectOption(optionId: string): Chainable<any> {
    return this.select()
      .find(`[data-test-option-id="${optionId}"]`);
  }

  public changeSelectOption(optionId: string): Chainable<any> {
    return this.selectOption(optionId).click();
  }

  public static create(inputId: string, inputType: ReportInputType): ReportWidget {
    switch (inputType) {
      case ReportInputType.RADIO_BUTTONS_GROUP:
        return new MultiRadioWidget(inputId);
      default:
        throw Error('Unknown type : ' + inputType);
    }
  }
}

export class MultiRadioWidget extends ReportWidget {
  constructor(inputId: string) {
    super(inputId);
  }

  assertOptionIsSelected(optionId: string) {
    this.selectOption(optionId).find('.ant-radio-checked').should('exist');
  }

  assertOptionIsNotSelected(optionId: string) {
    this.selectOption(optionId)
      .should('exist')
      .find('.ant-radio-checked')
      .should('not.exist');
  }
}

export abstract class ReportOptionWidget {
  protected constructor(protected optionId: string) {
  }

  abstract assertExist();
}

export class ProjectPickerOption extends ReportOptionWidget {
  constructor(optionId: string) {
    super(optionId);
  }

  private select(): Chainable<any> {
    return cy.get(`[data-test-option-id="${this.optionId}"]`)
      .find('sqtm-app-form-project-picker');
  }

  private selectLink(): Chainable<any> {
    return this.select().find('a');
  }

  assertExist() {
    cy.get(`[data-test-option-id="${this.optionId}"]`)
      .find('sqtm-app-form-project-picker')
      .should('exist');
  }

  assertSelectedProjectsContains(expectedNames: string) {
    this.selectLink().should('contain.text', expectedNames);
  }

  assertNoProjectAreSelected() {
    this.selectLink().should('contain.text', 'Choisir');
  }

  openProjectSelector(): ProjectPickerDialogElement {
    this.selectLink().click();
    return new ProjectPickerDialogElement();
  }
}

export class MilestonePickerOption extends ReportOptionWidget {
  constructor(optionId: string) {
    super(optionId);
  }

  private select(): Chainable<any> {
    return cy.get(`[data-test-option-id="${this.optionId}"]`)
      .find('sqtm-app-form-milestone-picker');
  }

  private selectLink(): Chainable<any> {
    return this.select().find('a');
  }

  assertExist() {
    cy.get(`[data-test-option-id="${this.optionId}"]`)
      .find('sqtm-app-milestone-project-picker')
      .should('exist');
  }

  assertSelectedMilestoneContains(expectedNames: string) {
    this.selectLink().should('contain.text', expectedNames);
  }

  assertNoMilestonesAreSelected() {
    this.selectLink().should('contain.text', 'Choisir');
  }

  openMilestoneSelector(): MilestonePickerDialogElement {
    this.selectLink().click();
    return new MilestonePickerDialogElement();
  }
}

export class TreePickerOption extends ReportOptionWidget {

  private tree: TreeElement;

  constructor(optionId: string, private kind: 'requirement') {
    super(optionId);
  }

  private select(): Chainable<any> {
    return cy.get(`[data-test-option-id="${this.optionId}"]`)
      .find(`sqtm-app-form-${this.kind}-tree-picker`);
  }

  private selectLink(): Chainable<any> {
    return this.select().find('a');
  }

  assertExist() {
    this.select().should('exist');
  }

  assertHowManyNodesAreSelected(count: number) {
    this.selectLink().should('contain.text', `${count} élément(s) sélectionné(s)`);
  }

  assertNoNodesAreSelected() {
    this.selectLink().should('contain.text', 'Choisir');
  }

  openTreeNodeSelector(initialNodes: GridResponse = {dataRows: []}) {
    this.tree = TreeElement.createTreeElement(`${this.kind}-tree-picker`, `${this.kind}-tree`, initialNodes);
    this.selectLink().click();
    this.tree.waitInitialDataFetch();
  }

  pickNodes(...ids: Identifier[]) {
    this.tree.pickNode(ids[0]);
    this.tree.selectRows(ids.slice(1), 'NAME');
  }

  confirm() {
    this.selectButton('confirm').click();
  }

  private selectButton(buttonId: string) {
    return cy.get(`[data-test-dialog-button-id="${buttonId}"]`);
  }

  cancel() {
    this.selectButton('cancel').click();
  }

  // assert node selection in tree picker... witch should be opened of course
  assertNodesAreSelected(...ids: Identifier[]) {
    ids.forEach(id => {
      this.tree.assertNodeIsSelected(id);
    });
  }
}

import {EntityViewPage} from '../page';
import {apiBaseUrl} from '../../../utils/mocks/request-mock';
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';

export class ReportInformationPanelElement {
  constructor(private customReportLibraryNodeId: number | '*') {
  }

  assertSummaryContains(expectedSummary: string) {
    cy.get(selectByDataTestComponentId('report-summary'))
      .should('contain.text', expectedSummary);
  }

  assertAttributesContains(attributeName: string, attributeValue: string) {
    cy.get(selectByDataTestComponentId('report-attributes'))
      .find(`[data-test-attribute-name="${attributeName}"]`)
      .should('contain.text', attributeName);
    cy.get(selectByDataTestComponentId('report-attributes'))
      .find(`[data-test-attribute-value="${attributeName}"]`)
      .should('contain.text', attributeValue);
  }
}

export class ReportDefinitionViewPage extends EntityViewPage {

  constructor(private customReportLibraryNodeId: number | '*') {
    super('sqtm-app-report-definition-view');
  }

  public checkDataFetched() {
    const url = `${apiBaseUrl()}/report-definition-view/${this.customReportLibraryNodeId}`;
    cy.wait(`@${url}`);
  }

  clickAnchorLink<T>(linkId: ChartDefinitionAnchorLinks, data?: any): T {
    let element: T;
    switch (linkId) {
      case 'information':
        element = this.showInformationPanel(linkId) as unknown as T;
        break;
      default :
        throw Error(`Unknown linkId : ${linkId}`);
    }
    return element;
  }

  private showInformationPanel<T>(linkId: 'information'): ReportInformationPanelElement {
    this.clickOnAnchorLink(linkId);
    return new ReportInformationPanelElement(this.customReportLibraryNodeId);
  }

  private clickOnAnchorLink(linkId: string) {
    cy.get(`[anchor-link-id=${linkId}]`).click().trigger('mouseleave');
    cy.removeNzTooltip();
  }

}

export type ChartDefinitionAnchorLinks = 'information';

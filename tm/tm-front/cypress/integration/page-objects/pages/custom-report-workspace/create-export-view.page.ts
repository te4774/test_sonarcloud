import {Page} from '../page';
import {ReferentialData} from '../../../model/referential-data.model';
import {
  basicReferentialData,
  ReferentialDataProviderBuilder
} from '../../../utils/referential/referential-data.provider';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {TreeElement} from '../../elements/grid/grid.element';
import {CustomExportModel, CustomExportWorkbenchData} from '../../../model/custom-report/custom-export.model';
import {GridResponse} from '../../../model/grids/data-row.type';
import {CommonSelectors} from '../../elements/forms/abstract-form-field.element';
import {TextFieldElement} from '../../elements/forms/TextFieldElement';
import {CustomExportViewPage} from './custom-export-view.page';
import Chainable = Cypress.Chainable;

export class CreateExportViewPage extends Page {

  public readonly columnTree: TreeElement;
  public readonly nameField: TextFieldElement;

  private readonly selectedAttributesDropZoneSelector = this.selectByComponentId('custom-export-workbench-attributes-drop-zone');
  private readonly perimeterFieldSelector = 'sqtm-app-custom-export-scope-selector';

  constructor() {
    super('sqtm-app-create-custom-export-view');
    this.columnTree = TreeElement.createTreeElement('custom-export-column-selector');
    this.nameField = new TextFieldElement('name');
  }

  assertExist(): void {
    super.assertExist();
    this.columnTree.assertExist();
  }

  public static initTestAtPage(
    referentialData: ReferentialData = basicReferentialData,
    workbenchData: CustomExportWorkbenchData = basicWorkbenchData,
    nodeReference: string = '*'): CreateExportViewPage {
    const referentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const mock = new HttpMockBuilder(`custom-exports/workbench/${nodeReference}`).responseBody(workbenchData).build();
    cy.visit(`custom-report-workspace/create-custom-export/${nodeReference}`);
    referentialDataProvider.wait();
    mock.wait();
    return new CreateExportViewPage();
  }

  changeName(newName: string): void {
    this.nameField.fill(newName);
  }

  openScopeSelector(gridResponse: GridResponse): CustomExportScopeSelectorElement {
    new HttpMockBuilder('campaign-tree').post().responseBody(gridResponse).build();
    cy.get(this.perimeterFieldSelector).click();
    return new CustomExportScopeSelectorElement();
  }

  confirm(crlnId: number, customReportTreeResponse: GridResponse, customExportViewResponse: CustomExportModel): CustomExportViewPage {
    new HttpMockBuilder('custom-exports/new/*').post().responseBody({id: crlnId}).build();
    new HttpMockBuilder('custom-report-tree').post().responseBody(customReportTreeResponse).build();
    new HttpMockBuilder('custom-report-custom-export-view/*').responseBody(customExportViewResponse).build();
    const thisSelector = () => cy.get(this.rootSelector);
    CommonSelectors.childComponentId(thisSelector, 'add-custom-export')().click();
    return new CustomExportViewPage(crlnId);
  }

  dragAndDropAttribute(localizedColumnName: string): void {
    this.findRowIdByLabel(localizedColumnName)
      .then(rowId => this.columnTree.beginDragAndDrop(rowId))
      .get(this.selectedAttributesDropZoneSelector)
      .trigger('mouseenter')
      .trigger('mousemove')
      .trigger('mouseup');
  }

  assertMilestoneColumnsExist(): void {
    this.columnTree.assertRowExist('CAMPAIGN_MILESTONE');
    // We won't check this one because it's not in the DOM because of virtual scroll
    // this.columnTree.assertRowExist('TEST_CASE_MILESTONE');
  }

  assertMilestoneColumnsNotExist(): void {
    this.columnTree.assertRowNotExist('CAMPAIGN_MILESTONE');
    this.columnTree.assertRowNotExist('TEST_CASE_MILESTONE');
  }

  // This method match cells by locale name. This means we can only get the first matching cell in case of duplicates labels.
  private findRowIdByLabel(localizedColumnName: string): Chainable {
    return this.columnTree.findRowId('NAME', localizedColumnName);
  }
}

class CustomExportScopeSelectorElement {
  readonly tree: TreeElement;

  constructor() {
    this.tree = TreeElement.createTreeElement('campaign-tree-picker');
  }

  selectNode(nodeName: string): void {
    this.tree.findRowId('NAME', nodeName).then(id => this.tree.addNodeToSelection(id));
  }

  confirm(): void {
    const thisSelector = () => cy.get('sqtm-app-custom-export-scope-selector-overlay');
    CommonSelectors.childComponentId(thisSelector, 'confirm')().click();
  }
}

const basicWorkbenchData: CustomExportWorkbenchData = {
  containerId: 1,
  projectId: 1,
};

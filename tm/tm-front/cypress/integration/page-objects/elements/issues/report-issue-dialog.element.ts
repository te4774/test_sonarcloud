import {HTTP_RESPONSE_STATUS, HttpMock, HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {RemoteIssue} from '../../../model/issue/remote-issue.model';
import {TextFieldElement} from '../forms/TextFieldElement';
import {IssueBindableEntity} from '../../../model/bugtracker/issue-bindable-entity.model';
import {RemoteAutocompleteFieldElement} from '../forms/remote-autocomplete-field.element';
import {TextAreaFieldElement} from '../forms/text-area-field.element';
import {SelectFieldElement} from '../forms/select-field.element';
import {DatePickerElement} from '../forms/date-picker.element';
import {CommonSelectors} from '../forms/abstract-form-field.element';
import {CascadingSelectFieldElement} from '../forms/cascading-select-field.element';
import {CheckboxListFieldElement} from '../forms/checkbox-list-field.element';
import {RemoteAttachmentFieldElement} from '../forms/remote-attachment-field-element';
import {RemoteIssueSearchForm} from '../../../model/issue/remote-issue-search.model';
import {CheckBoxElement} from '../forms/check-box.element';

export class ReportIssueDialogElement {
  public readonly selector = '[data-test-dialog-id=remote-issue-dialog]';

  private readonly remoteProjectNamesMock: HttpMock<any>;
  private readonly reportIssueModelMock: HttpMock<any>;
  private readonly remoteIssueSearchFormMock: HttpMock<RemoteIssueSearchForm>;

  constructor(public readonly mocks: ReportIssueDialogMocks) {
    this.remoteProjectNamesMock = new HttpMockBuilder('issues/projects/*/bugtracker')
      .responseBody({projectNames: mocks.remoteProjectNames})
      .build();

    this.reportIssueModelMock = new HttpMockBuilder('issues/execution-step/*/new-issue?project-name=*')
      .responseBody(mocks.remoteIssue)
      .build();

    this.remoteIssueSearchFormMock = new HttpMockBuilder<RemoteIssueSearchForm>('issues/issue-search-form')
      .post()
      .responseBody(mocks.searchForm)
      .build();
  }

  assertExists(): void {
    cy.get(this.selector).should('exist');
  }

  assertNotExists(): void {
    cy.get(this.selector).should('not.exist');
  }

  waitForInitialRequests() {
    if (! this.mocks.attachMode) {
      this.remoteProjectNamesMock.wait();
      this.reportIssueModelMock.wait();
    } else {
      this.remoteProjectNamesMock.wait();
    }
  }

  fillTextField(fieldId: string, content: string): void {
    new TextFieldElement(fieldId).fill(content);
  }

  getAutocompleteTextField(fieldId: string): RemoteAutocompleteFieldElement {
    return new RemoteAutocompleteFieldElement(fieldId);
  }

  getTextAreaField(fieldId: string): TextAreaFieldElement {
    return new TextAreaFieldElement(fieldId);
  }

  getDropdownList(fieldId: string): SelectFieldElement {
    return new SelectFieldElement(CommonSelectors.fieldName(fieldId));
  }

  getFreeTagList(fieldId: string): SelectFieldElement {
    return new SelectFieldElement(CommonSelectors.fieldName(fieldId));
  }

  getTagList(fieldId: string): SelectFieldElement {
    return this.getFreeTagList(fieldId);
  }

  getDatePicker(fieldId: string): DatePickerElement {
    return new DatePickerElement(CommonSelectors.fieldName(fieldId));
  }

  getMultiSelect(fieldId: string): SelectFieldElement {
    return this.getFreeTagList(fieldId);
  }

  getCascadingSelect(fieldId: string): CascadingSelectFieldElement {
    return new CascadingSelectFieldElement(CommonSelectors.fieldName(fieldId));
  }

  getCheckboxList(fieldId: string): CheckboxListFieldElement {
    return new CheckboxListFieldElement(CommonSelectors.fieldName(fieldId));
  }

  getRemoteAttachmentField(fieldId: string): RemoteAttachmentFieldElement {
    return new RemoteAttachmentFieldElement(CommonSelectors.fieldName(fieldId));
  }

  getCheckbox(fieldId: string): CheckBoxElement {
    return new CheckBoxElement(CommonSelectors.fieldName(fieldId));
  }

  // We assume all searches are text-based
  searchForIssue(searchTerms: {[fieldName: string]: string}, result: RemoteIssue): void {
    const mock = new HttpMockBuilder('issues/search-issue')
      .post()
      .responseBody(result)
      .build();

    Object.entries(searchTerms).forEach(([fieldName, value]) => {
      cy.get(this.buildInnerComponentSelector(fieldName)).type(value);
    });

    cy.get(this.buildInnerComponentSelector('search-button')).click();

    mock.wait();
  }

  confirmWithoutMock(): void {
    cy.get(this.buildInnerComponentSelector('confirm')).click();
  }

  confirmWithErrorResponse(error: any): void {
    const mock = new HttpMockBuilder(this.buildPostUrl())
      .post()
      .status(HTTP_RESPONSE_STATUS.PRECONDITION_FAIL)
      .responseBody(error)
      .build();

    this.confirmWithoutMock();
    mock.wait();
  }

  confirmWithSuccess(): void {
    const submitIssueMock = new HttpMockBuilder(this.buildPostUrl())
      .post()
      .responseBody({issueId: 'ISSUE-ID', url: 'http://some.url/bt'})
      .build();

    this.confirmWithoutMock();
    submitIssueMock.wait();
  }

  confirmWithAttachments(): void {
    const attachmentsUrl = 'issues/*/*/attachments';
    const submitAttachmentsMock = new HttpMockBuilder(attachmentsUrl)
      .post()
      .build();

    this.confirmWithSuccess();
    submitAttachmentsMock.wait();
  }

  assertHasRequiredError(fieldId: string): void {
    new TextFieldElement(fieldId).assertErrorExist('sqtm-core.validation.errors.required');
  }

  assertHasModalErrorMessage(message: string): void {
    cy.get(this.buildInnerComponentSelector('modal-error-message')).should('contain.text', message);
  }

  private buildInnerComponentSelector(componentId: string) {
    return `${this.selector} [data-test-component-id=${componentId}]`;
  }

  private buildPostUrl(): string {
    let url = 'issues/';

    switch (this.mocks.bindableEntity) {
      case 'EXECUTION_TYPE':
        url += 'execution/';
        break;
      case 'EXECUTION_STEP_TYPE':
        url += 'execution-step/';
        break;
      default:
        throw new Error('not implemented');
    }

    url += '*/';

    switch (this.mocks.issueType) {
      case 'standard':
        url += 'new-issue';
        break;
      case 'advanced':
        url += 'new-advanced-issue';
        break;
      case 'oslc':
        url += 'new-oslc-issue';
        break;
      default:
        throw new Error('not implemented');
    }

    return url;
  }
}

export interface ReportIssueDialogMocks {
  remoteProjectNames: string[];
  remoteIssue: RemoteIssue;
  bindableEntity: IssueBindableEntity;
  issueType: 'standard' | 'advanced' | 'oslc';
  attachMode: boolean;
  searchForm: RemoteIssueSearchForm;
}


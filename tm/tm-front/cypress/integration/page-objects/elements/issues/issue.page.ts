import {Page} from '../../pages/page';
import {BugTrackerConnectionDialog} from './bugtracker-connection-dialog.element';

export class IssuePage extends Page {

  constructor(selector: string) {
    super(selector);
  }

  openConnectionDialog(): BugTrackerConnectionDialog {
    cy.get(`button[data-test-button-id="login"]`).click();
    return new BugTrackerConnectionDialog();
  }

  exportAllLines() {
    cy.get(`i[data-test-toolbar-button-id="export-issue-button"]`).click();
    cy.get(`li[data-test-menu-item-id="export-visible"]`).should('exist').click();
  }

  exportSelectedLines() {
    cy.get(`[data-test-row-id="1"]`).first().click();
    cy.get(`i[data-test-toolbar-button-id="export-issue-button"]`).click();
    cy.get(`li[data-test-menu-item-id="export-selected"]`).click();
  }
}

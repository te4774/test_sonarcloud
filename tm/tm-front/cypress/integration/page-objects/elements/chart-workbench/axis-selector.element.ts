import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {CustomReportAttributeSelector} from '../grid/grid.element';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {ChartDefinitionModel} from '../../../model/custom-report/chart-definition.model';

export class AxisSelectorElement {

  private readonly treeElement = new CustomReportAttributeSelector('overlay-column-prototype-selector');

  constructor(public id = 'axis') {
  }

  assertExist() {
    this.getBaseDomNode().should('exist');
  }

  private getBaseDomNode() {
    return cy.get(`sqtm-app-axis-selector [data-test-component-id="${this.id}"]`);
  }

  assertNoAttributeSelected() {
    this.getBaseDomNode().should('contain.text', 'Pas d\'attribut');
  }

  openAttributeSelector(): CustomReportAttributeSelector {
    this.getBaseDomNode()
      .find(selectByDataTestComponentId('columnField'))
      .click();
    this.treeElement.assertExist();
    return this.treeElement;
  }

  addAttributeAndRenderPreview(id: number, projectId: string = '*', chartDefinition?: Partial<ChartDefinitionModel>): void {
    const httpMock = new HttpMockBuilder(`chart-workbench/preview/${projectId}`).post().responseBody(chartDefinition).build();
    this.treeElement.assertExist();
    this.treeElement.selectRow(id, 'NAME');
    httpMock.wait();
  }

  addAttributeToChart(id: number): void {
    this.treeElement.assertExist();
    this.treeElement.selectRow(id, 'NAME');
  }

}

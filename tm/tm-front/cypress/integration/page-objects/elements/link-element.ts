export class LinkElement {
  constructor(private fieldId: string) {
  }

  containText(value: string) {
    cy.get(`a${this.buildSelector()}`).should('contain.text', value);
  }

  assertExist() {
    cy.get(`a${this.buildSelector()}`).should('exist');
  }

  buildSelector() {
    return `[data-test-field-id="${this.fieldId}"]`;
  }
}

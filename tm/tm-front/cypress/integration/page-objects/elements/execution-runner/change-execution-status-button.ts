import {ExecutionStatusKeys} from '../../../model/level-enums/level-enum';
import {ExecutionRunnerStepPage} from '../../pages/execution/execution-runner-step-page';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {TestPlanResumeModel} from '../../../model/campaign/test-plan-resume.model';
import {ExecutionModel} from '../../../model/execution/execution.model';
import {ExecutionRunnerProloguePage} from '../../pages/execution/execution-runner-prologue-page';

export class ChangeExecutionStatusButton {

  constructor(private executionStatus: ExecutionStatusKeys) {
  }

  assertExist() {
    this.getButton()
      .should('exist');
  }

  private getButton() {
    return cy.get(`
    sqtm-app-execution-runner-status-button
    [data-test-execution-status="${this.executionStatus}"]
    `);
  }

  changeStatus(executionStepId: number, executionId: number): ExecutionRunnerStepPage {
    const mock = new HttpMockBuilder(`execution-step/${executionStepId}/status?executionId=${executionId}`)
      .post()
      .build();
    this.getButton().click();
    mock.wait();
    return new ExecutionRunnerStepPage();
  }

  changeStatusAndForward(executionStepId: number, executionId: number, iterationId= '*',
                         testPlanId = '*',
                         nextExecutionId = '*',
                         testPlanResume?: TestPlanResumeModel,
                         nextExecutionModel?: ExecutionModel): ExecutionRunnerProloguePage {
    const changeStatusMock = new HttpMockBuilder(`execution-step/${executionStepId}/status?executionId=${executionId}`)
      .post()
      .build();
    const resumeMock = new HttpMockBuilder(`iteration/${iterationId}/test-plan/${testPlanId}/next-execution`)
      .post()
      .responseBody(testPlanResume)
      .build();
    const executionMock = new HttpMockBuilder(`execution/${nextExecutionId}?*`).responseBody(nextExecutionModel).build();
    this.getButton().click();
    changeStatusMock.wait();
    resumeMock.wait();
    executionMock.wait();
    return new ExecutionRunnerProloguePage();
  }
}

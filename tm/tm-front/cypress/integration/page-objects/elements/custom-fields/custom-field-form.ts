import {CustomFieldBindingData, ProjectData} from '../../../model/project/project-data.model';
import {BindableEntity} from '../../../model/bindable-entity.model';
import {CustomField, InputType} from '../../../model/customfield/customfield.model';
import {RichTextFieldElement} from '../forms/RichTextFieldElement';
import Chainable = Cypress.Chainable;

export class CustomFieldForm {

  constructor(private project?: ProjectData, private domain?: BindableEntity) {
  }

  // here we only validate presence and order of cuf inputs. The behavior of each input must be checked as unit test level not ITs.
  validateFormStructure() {
    // check labels is a good way to ensure cuf presence
    this.checkLabels();
    // check orders
    this.checkOrder();
  }

  fillCustomField(cufId: number, value: any) {
    this.typeInCustomField(cufId, value);
  }

  clearCustomField(cufId: number) {
    const customField = this.getCustomFields().find(value => value.id === cufId);
    switch (customField.inputType) {
      case InputType.PLAIN_TEXT:
        this.clearTextField(cufId);
        break;
      case InputType.NUMERIC:
        this.clearNumericField(cufId);
        break;
      case InputType.TAG:
        this.clearTagField(cufId);
        break;
      case InputType.RICH_TEXT:
        this.clearRichField(cufId);
        break;
      default:
        throw new Error(`Non clearable field ${customField.name} of type ${customField.inputType}`);
    }
  }

  toggleTagFieldOptions(cufId: number, ...indexes: number[]) {
    // Show options
    this.getTagField(cufId).click();
    // Get options and click them
    this.getTagFieldOptions().then((children) => {
      indexes.forEach((optionIndex) => children[optionIndex].click());
    }).then(() => {
      // Click again to hide overlay menu
      // We need to force because cypress may complain about the element being covered by the dropdown menu...
      this.getTagField(cufId).click({force: true});
    }).then(() => {
      cy.clickVoid();
    });
  }

  assertHasError(cufId: number, expectedError: string) {
    cy.get(`[data-test-component-id="custom-field-form"]`)
      .should('exist')
      .find(`div[data-test-cuf-id-error="${cufId}"]`)
      .should('exist')
      .should('have.attr', 'data-test-error-key', expectedError);
  }

  assertHasNoError(cufId: number) {
    cy.get(`
      [data-test-component-id="custom-field-form"]
      [data-test-cuf-id-error="${cufId}"]
    `).should('not.exist');
  }

  private clearTextField(cufId: number) {
    this.getTextField(cufId).clear();
  }

  private getTextField(cufId: number): Chainable<any> {
    return cy.get(`
    [data-test-component-id="custom-field-form"]
    [data-test-cuf-text-input-id="${cufId}"]
    `);
  }

  private clearNumericField(cufId: number) {
    this.getNumericField(cufId).clear();
  }

  private getNumericField(cufId: number): Chainable<any> {
    return cy.get(`
    [data-test-component-id="custom-field-form"]
    [data-test-cuf-numeric-input-id="${cufId}"]
    input
    `);
  }

  private clearTagField(cufId: number) {
    this.getTagFieldRemoveButtons(cufId).then(elements => {
      elements.each((index, element) => {
        element.click();
      });
    });
  }

  private getTagField(cufId: number): Chainable<any> {
    return cy.get(`
      [data-test-component-id="custom-field-form"]
      [data-test-cuf-tag-input-id="${cufId}"]
    `);
  }

  private getTagFieldOptions(): Chainable<any> {
    // This is a modal overlay so we should only have one dropdown...
    return cy.get(`
      nz-option-item
    `);
  }

  private getTagFieldRemoveButtons(cufId: number): Chainable<any> {
    // This will fail if the field has already been cleared !
    return cy.get(`
      [data-test-component-id="custom-field-form"]
      [data-test-cuf-tag-input-id="${cufId}"]
      .ant-select-selection-item-remove
    `);
  }

  private clearRichField(cufId: number) {
    this.getRichField(cufId).clear();
  }

  private typeInCustomField(cufId: number, value: any) {
    const customField = this.getCustomFields().find(cuf => cuf.id === cufId);
    switch (customField.inputType) {
      case InputType.PLAIN_TEXT:
        this.getTextField(cufId).type(value);
        break;
      case InputType.NUMERIC:
        this.getNumericField(cufId).type(value);
        break;
      case InputType.TAG:
        this.toggleTagFieldOptions(cufId, value);
        break;
      case InputType.RICH_TEXT:
        this.getRichField(cufId).fill(value);
        break;
      default:
        throw new Error(`Unhandled type ${customField.inputType} for field ${customField.name}`);
    }
  }

  private getRichField(cufId: number): RichTextFieldElement {
    return new RichTextFieldElement(cufId.toString());
  }

  private checkLabels() {
    const customFields: CustomField[] = this.getCustomFields();
    customFields.filter(value => !value.optional)
      .forEach(value => {
        this.checkLabel(value.id, value.label);
      });

    customFields.filter(value => value.optional)
      .forEach(value => {
        this.checkLabelIsNotPresent(value.id);
      });
  }

  private checkLabel(cufId: number, expectedLabel: string) {
    cy.get(`
      [data-test-component-id="custom-field-form"]
      [data-test-cuf-label-id="${cufId}"]`)
      .should('contain', expectedLabel);
  }

  private getCustomFields(): CustomField[] {
    if (this.domain && this.project) {
      return this.getCustomFieldBindings().map(value => value.customField);
    }
    return [];
  }

  private getCustomFieldBindings(): CustomFieldBindingData[] {
    if (this.domain && this.project) {
      return this.project.customFieldBinding[this.domain];
    }
    return [];
  }

  private checkOrder() {
    const sortedBindings = this.getCustomFieldBindings()
      .sort((a, b) => a.position - b.position)
      .filter(value => !value.customField.optional);
    cy.get(`[data-test-component-id="custom-field-form"] .label`)
      .should('have.length', sortedBindings.length);

    cy.get(`[data-test-component-id="custom-field-form"] .label`).then((elements) => {
      elements.each((index, element) => {
        expect(element.innerText).to.eql(sortedBindings[index].customField.label);
      });
    });
  }

  private checkLabelIsNotPresent(cufId: number) {
    cy.get(`[data-test-cuf-label-id="${cufId}"]`).should('not.exist');
  }
}

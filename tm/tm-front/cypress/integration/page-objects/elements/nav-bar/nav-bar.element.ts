import {AdministrationWorkspacePage} from '../../pages/administration-workspace/administration-workspace.page';
import {TestCaseWorkspacePage} from '../../pages/test-case-workspace/test-case-workspace.page';
import {
  ReferentialDataProvider,
  ReferentialDataProviderBuilder
} from '../../../utils/referential/referential-data.provider';
import {GridElement, TreeElement} from '../grid/grid.element';
import {CampaignWorkspacePage} from '../../pages/campaign-workspace/campaign-workspace.page';
import {GridResponse} from '../../../model/grids/data-row.type';
import {ReferentialData} from '../../../model/referential-data.model';
import {MenuElement} from '../../../utils/menu.element';
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {AdminWorkspaceProjectsPage} from '../../pages/administration-workspace/admin-workspace-projects.page';
import {AdminWorkspaceUsersPage} from '../../pages/administration-workspace/admin-workspace-users.page';
import {AdminWorkspaceTeamsPage} from '../../pages/administration-workspace/admin-workspace-teams.page';
import {AdminWorkspaceCustomFieldsPage} from '../../pages/administration-workspace/admin-workspace-custom-fields.page';
import {ProjectFilterDialog} from '../dialog/project-filter-dialog.element';
import {AdminWorkspaceMilestonesPage} from '../../pages/administration-workspace/admin-workspace-milestones.page';
import {AdminWorkspaceBugtrackersPage} from '../../pages/administration-workspace/admin-workspace-bugtrackers.page';
import {SingleMilestonePickerDialogElement} from '../dialog/single-milestone-picker-dialog.element';
import {LogoutPage} from '../../pages/login/logout-page';
import {
  AdminReferentialDataProvider,
  AdminReferentialDataProviderBuilder
} from '../../../utils/referential/admin-referential-data.provider';
import {UserAccountPage} from '../../pages/user-account/user-account.page';
import {UserAccount} from '../../../model/user/user-account.model';
import {getDefaultAdminReferentialData} from '../../../utils/referential/admin-referential-data-builder';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {AdminReferentialData} from '../../../model/admin-referential-data.model';
import Chainable = Cypress.Chainable;

export class NavBarElement {

  constructor() {
  }

  private readonly rootSelector = 'sqtm-core-nav-bar';

  private readonly BUGTRACKER_BUTTON_ID = 'bugtrackers';
  private readonly BUGTRACKER_MENU_ID = 'bugtrackers-menu';

  private readonly MILESTONE_BUTTON_ID = 'milestone-filter';
  private readonly MILESTONE_MENU_ID = 'milestone-filter-menu';
  private readonly MILESTONE_MENU_ITEM_ENABLE = 'filtered-by-milestone-mode';
  private readonly MILESTONE_MENU_ITEM_DISABLE = 'referential-milestone-mode';

  public static navigateToAdministration<T extends AdministrationWorkspacePage>(subMenu: string, initialNodes?: GridResponse,
                                                                                referentialData?: ReferentialData): T {
    // Determine which page we're about to navigate to
    let page: any;
    let grid: GridElement;

    switch (subMenu) {
      case ('projects'):
        grid = GridElement.createGridElement('projects', 'generic-projects', initialNodes);
        page = new AdminWorkspaceProjectsPage(grid);
        break;
      case ('users'):
        grid = GridElement.createGridElement('users', 'users', initialNodes);
        page = new AdminWorkspaceUsersPage(grid);
        break;
      case ('teams'):
        grid = GridElement.createGridElement('teams', 'teams', initialNodes);
        page = new AdminWorkspaceTeamsPage(grid);
        break;
      case ('entities-customization'):
        grid = GridElement.createGridElement('customFields', 'custom-fields', initialNodes);
        page = new AdminWorkspaceCustomFieldsPage(grid);
        break;
      case ('milestones'):
        grid = GridElement.createGridElement('milestones', 'milestones', initialNodes);
        page = new AdminWorkspaceMilestonesPage(grid);
        break;
      case ('servers'):
        grid = GridElement.createGridElement('bugtrackers', 'bugtrackers', initialNodes);
        page = new AdminWorkspaceBugtrackersPage(grid);
        break;
      default:
        throw new Error(`Unknown page type for menu item : ${subMenu}`);
    }

    // Do navigate
    const adminReferentialDataProvider: AdminReferentialDataProvider = new AdminReferentialDataProviderBuilder(referentialData).build();
    const menu = this.showSubMenu('administration', 'administration-menu');
    const menuItem = menu.item(subMenu);
    menuItem.click();

    // Check page and grid initialization
    page.assertExist();

    // Wait for referential data
    adminReferentialDataProvider.wait();

    return page;
  }

  public static navigateToTestCaseWorkspace(initialNodes?: GridResponse, referentialData?: ReferentialData): TestCaseWorkspacePage {
    // creating stubs/routes before the click
    const referentialDataProvider: ReferentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement('test-case-workspace-main-tree', 'test-case-tree', initialNodes);
    // now that routes are initialized we can click and thus angular will fire the requests
    NavBarElement.clickWorkspaceLink('test-case-workspace');
    // wait for ref data request to fire
    referentialDataProvider.wait();
    // wait for initial tree data request to fire
    tree.waitInitialDataFetch();
    // return properly initialized page
    const page = new TestCaseWorkspacePage(tree, 'sqtm-app-test-case-workspace');
    page.assertExist();
    return page;
  }

  public static clickWorkspaceLink(workspaceName: WorkspaceName) {
    cy.get(`.${workspaceName}-link.ant-menu-item`).click();
  }

  public static showSubMenu(fieldId: string, menuId: string): MenuElement {
    cy.get(`[data-test-navbar-field-id=${fieldId}]`).trigger('mouseenter');
    cy.get(`[data-test-navbar-field-id=${fieldId}]`).trigger('mouseover');
    const menuElement = new MenuElement(menuId);
    menuElement.assertExist();
    return menuElement;
  }

  public static navigateToCampaignWorkspace(initialNodes?: GridResponse, referentialData?: ReferentialData) {
    // creating stubs/routes before the click
    const referentialDataProvider: ReferentialDataProvider = new ReferentialDataProviderBuilder(referentialData).build();
    const tree = TreeElement.createTreeElement('campaign-workspace-main-tree', 'campaign-tree', initialNodes);
    // now that routes are initialized we can click and thus angular will fire the requests
    NavBarElement.clickWorkspaceLink('campaign-workspace');
    // wait for ref data request to fire
    referentialDataProvider.wait();
    // wait for initial tree data request to fire
    tree.waitInitialDataFetch();
    // return properly initialized page
    const page = new CampaignWorkspacePage(tree);
    page.assertExist();
    return page;
  }

  public static openUserAccount(data: UserAccount): UserAccountPage {
    const adminRefDataMock = new HttpMockBuilder<AdminReferentialData>('referential/admin')
      .responseBody(getDefaultAdminReferentialData())
      .build();
    const referentialDataProvider: ReferentialDataProvider = new ReferentialDataProviderBuilder().build();
    const menu = this.showSubMenu('user-menu', 'user-menu');
    const menuItem = menu.item('user-account');
    const page = new UserAccountPage();
    page.declareInitialData(data);
    menuItem.click();
    referentialDataProvider.wait();
    page.checkDataFetched();
    page.assertExist();
    return page;
  }

  public static logout() {
    const menu = this.showSubMenu('user-menu', 'user-menu');
    const menuItem = menu.item('logout');
    menuItem.click();
    const page = new LogoutPage();
    page.assertExist();
    return page;
  }

  public static openProjectFilter() {
    const menu = this.showSubMenu('project-filter', 'project-filter-menu');
    const menuItem = menu.item('multi-projects');
    menuItem.click();
    return new ProjectFilterDialog();
  }

  public openMilestoneSelector() {
    const menu = this.showMilestoneMenu();
    const menuItem = menu.item('filtered-by-milestone-mode');
    menuItem.click();
    return new SingleMilestonePickerDialogElement();
  }


  public assertMilestoneModeIsDisabled() {
    const menu = this.showMilestoneMenu();
    const referentialModeItem = menu.item(this.MILESTONE_MENU_ITEM_DISABLE);
    referentialModeItem.assertIsChecked();
    const milestoneModeItem = menu.item(this.MILESTONE_MENU_ITEM_ENABLE);
    milestoneModeItem.assertIsNotChecked();
  }

  public assertMilestoneModeIsActive() {
    const menu = this.showMilestoneMenu();
    const referentialModeItem = menu.item(this.MILESTONE_MENU_ITEM_DISABLE);
    referentialModeItem.assertIsNotChecked();
    const milestoneModeItem = menu.item(this.MILESTONE_MENU_ITEM_ENABLE);
    milestoneModeItem.assertIsChecked();
  }


  private showMilestoneMenu() {
    return this.showSubMenu(this.MILESTONE_BUTTON_ID, this.MILESTONE_MENU_ID);
  }

  public toggle() {
    cy.get(this.rootSelector).find(selectByDataTestComponentId('sqtm-main-nav-bar-toggle-button')).click();
  }

  public showSubMenu(fieldId: string, menuId: string): MenuElement {
    cy.get(`[data-test-navbar-field-id=${fieldId}]`).trigger('mouseenter');
    const menuElement = new MenuElement(menuId);
    menuElement.assertExist();
    return menuElement;
  }

  assertExist() {
    cy.get(this.rootSelector).should('have.length', 1);
  }

  assertMilestoneMenuExist() {
    this.getMilestoneButton().should('exist');
  }

  assertMilestoneMenuNotExist() {
    this.getMilestoneButton().should('not.exist');
  }

  assertWorkspacePluginLinkExist(pluginId: string, expectedText: string) {
    cy.get(this.rootSelector).find(selectByDataTestComponentId(pluginId))
      .should('be.visible')
      .should('contain.text', expectedText);
    cy.get(this.rootSelector).find(selectByDataTestComponentId('fake-action-word')).should('not.exist');
  }

  assertWorkspacePluginLinkNotExist(pluginId: string) {
    cy.get(this.rootSelector).find(selectByDataTestComponentId(pluginId))
      .should('not.exist');
  }

  assertFakeActionWordNotExist() {
    cy.get(this.rootSelector).find(selectByDataTestComponentId('fake-action-word'))
      .should('not.exist');
  }

  assertFakeActionWordExist() {
    cy.get(this.rootSelector).find(selectByDataTestComponentId('fake-action-word'))
      .should('be.visible')
      .should('contain.text', 'Action');
  }

  assertWorkspaceLinkExist(id: string, expectedText: string, expectedUrl: string) {
    const baseAppUrl = Cypress.env('appBaseUrl');
    const url = baseAppUrl + expectedUrl;
    cy.get(this.rootSelector).find(selectByDataTestComponentId(id))
      .find('a')
      .should('be.visible')
      .should('contain.text', expectedText)
      .should('have.attr', 'href', url);
  }

  private getMilestoneButton(): Chainable<any> {
    return cy.get(this.rootSelector).find(`[data-test-navbar-field-id="${this.MILESTONE_BUTTON_ID}"]`);
  }

  disableMilestoneMode() {
    return this.showMilestoneMenu().item(this.MILESTONE_MENU_ITEM_DISABLE).click();
  }

  enableMilestoneMode() {
    return this.showMilestoneMenu().item(this.MILESTONE_MENU_ITEM_ENABLE).click();
  }

  assertBugTrackerMenuExist() {
    this.getBugTrackerButton().should('exist');
  }

  assertBugTrackerMenuNotExist() {
    this.getBugTrackerButton().should('not.exist');
  }

  private getBugTrackerButton(): Chainable<any> {
    return cy.get(this.rootSelector).find(`[data-test-navbar-field-id="${this.BUGTRACKER_BUTTON_ID}"]`);
  }

  assertAvatarHaveText(text: string) {
    const avatar = cy.get(this.rootSelector)
      .find(`[data-test-navbar-field-id="user-menu"]`)
      .find('nz-avatar')
      .find('span');
    avatar.should('have.text', text);
  }

  assertNavBarIsFold() {
    cy.get(this.rootSelector).find('.ant-menu-inline-collapsed').should('be.visible');
  }
}

type WorkspaceName = 'administration-workspace' | 'test-case-workspace' | 'campaign-workspace';

import {EditableTextFieldElement} from '../forms/editable-text-field.element';
import {selectByDataTestFieldId, selectByDataTestToolbarButtonId} from '../../../utils/basic-selectors';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {EditableSelectFieldElement} from '../forms/editable-select-field.element';

export class EnvironmentVariableProjectPanelElement {

  private readonly rootSelector = 'sqtm-app-automated-execution-environment-variable-panel';

  public checkSelectFieldValue(expectedValue: string, evID: number) {
    cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('environment-variable-dropdown-value=' + evID))
      .should('contain.text', expectedValue);
  }

  public checkTextFieldValue(expectedValue: string, evID: number) {
    this.getEditableTextFieldElement(evID).checkContent(expectedValue);
  }

  public editTextfieldValue(value: string, evID: number) {
    const mock = new HttpMockBuilder('bound-environment-variables/*/project/*/value')
      .post()
      .build();

    const textField = this.getEditableTextFieldElement(evID);
    textField.assertIsEditable();
    textField.setValue(value);
    textField.selector.type('{enter}');

    mock.wait();
  }

  public editSelectFieldValue(value: string, evID: number) {
    const mock = new HttpMockBuilder('bound-environment-variables/*/project/*/value')
      .post()
      .build();

    this.getEditableSelectFieldElement(evID).setAndConfirmValueNoButton(value);

    mock.wait();
  }

  public resetValue(evId: number, defaultValue: string) {
    const mock = new HttpMockBuilder('bound-environment-variables/*/project/*/reset')
      .post()
      .responseBody({defaultValue: defaultValue})
      .build();

    cy.get(this.rootSelector).find(selectByDataTestToolbarButtonId('reset-ev-value-button=' + evId)).click();

    mock.wait();
  }

  private getEditableTextFieldElement(evID: number): EditableTextFieldElement {
    return new EditableTextFieldElement(() => cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('environment-variable-text-value=' + evID)));
  }
  private getEditableSelectFieldElement(evID: number): EditableSelectFieldElement {
    return new EditableSelectFieldElement(() => cy.get(this.rootSelector)
      .find(selectByDataTestFieldId('environment-variable-dropdown-value=' + evID)));
  }


}

import {GridDialogPicker} from './grid-dialog-picker';

export class MilestonePickerDialogElement extends GridDialogPicker {

  constructor() {
    super('single-milestones-picker-grid');
  }

  assertMilestoneAreSelected(...ids: number[]) {
    this.assertRowsAreSelected(ids);
  }

  selectMilestone(id: number) {
    this.grid.getCell(id, 'single-select-row-column').radioNgZorroRenderer().check();
  }

}

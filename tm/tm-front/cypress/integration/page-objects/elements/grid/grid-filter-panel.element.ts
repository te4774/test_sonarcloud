import {AsyncGroupedMultiListElement, GroupedMultiListElement} from '../filters/grouped-multi-list.element';
import {TextFilterWidgetElement} from '../filters/text-filter-widget.element';
import {GridResponse} from '../../../model/grids/data-row.type';
import {HttpMock, HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {NumericFilterWidgetElement} from '../filters/numeric-filter-widget.element';
import {DateFilterWidgetElement} from '../filters/date-filter-widget.element';
import {GridTestCaseScopeElement} from './grid-test-case-scope.element';
import {NumericSetFilterWidgetElement} from '../filters/numeric-set-filter-widget.element';

export class GridFilterPanelElement {

  constructor(private gridId: string, private gridUrl?: string) {
  }

  private selectByComponentId(id: string) {
    return `
    sqtm-core-filter-manager
    [data-test-component-id="${id}"]
    `;
  }

  assertPerimeterIsActive() {
    cy.get(this.selectByComponentId(`grid-scope-field`)).should('exist');
    cy.get(this.selectByComponentId(`grid-scope-field`)).should('contain.text', 'Périmètre');
  }

  assertCriteriaIsActive(label: string) {
    cy.get(this.selectByComponentId(`grid-filter-field`)).contains(label).should('exist');
  }

  assertCriteriaIsInactive(label: string) {
    cy.get(this.selectByComponentId(`grid-filter-field`)).contains(label).should('not.exist');
  }

  openProjectScopeSelector(): AsyncGroupedMultiListElement {
    cy.get(this.selectByComponentId(`grid-scope-field`)).click();
    const groupedMultiListElement = new AsyncGroupedMultiListElement(this.gridUrl);
    groupedMultiListElement.assertExist();
    return groupedMultiListElement;
  }

  openCustomScopeSelector(nodes: GridResponse = {dataRows: []}): GridTestCaseScopeElement {
    const testCaseScope = new GridTestCaseScopeElement(nodes, this.gridUrl);
    cy.get(this.selectByComponentId(`grid-scope-field`)).click();
    testCaseScope.tree.waitInitialDataFetch();
    testCaseScope.tree.assertExist();
    return testCaseScope;
  }

  changeToCustomScope(nodes: GridResponse = {dataRows: []}): GridTestCaseScopeElement {
    const testCaseScope = new GridTestCaseScopeElement(nodes, this.gridUrl);
    cy.get(selectByDataTestComponentId(`custom-scope`)).click();
    testCaseScope.tree.waitInitialDataFetch();
    testCaseScope.tree.assertExist();
    return testCaseScope;
  }

  assertPerimeterHasValue(expectedValue: string) {
    cy.get(this.selectByComponentId(`grid-scope-value`)).should('contain.text', expectedValue);
  }

  assertCriteriaHasValue(id: string, expectedValue: string) {
    cy.get(this.selectByComponentId(`grid-filter-field`))
      .contains(id)
      .parent()
      .find(selectByDataTestComponentId('grid-filter-field-value'))
      .should('contain.text', expectedValue);
  }

  openExistingCriteria(label: string) {
    cy.get(this.selectByComponentId(`grid-filter-field`)).contains(label).click();
  }

  inactivateCriteria(criteriaLabel: string, response: GridResponse = {dataRows: []}) {
    let mock: HttpMock<GridResponse>;
    if (this.gridUrl) {
      mock = new HttpMockBuilder<GridResponse>(this.gridUrl).post().responseBody(response).build();
    }
    cy.get(this.selectByComponentId(`grid-filter-field`))
      .then((elements) => {
        return elements.filter((index, element) => element.textContent.includes(criteriaLabel));
      })
      .find('[data-test-component-id="inactivate"]')
      .click();
    if (mock) {
      mock.wait();
    }
  }

  assertAddCriteriaLinkIsPresent() {
    cy.get(this.selectByComponentId(`grid-filter-manager-add-criteria`)).should('exist');
    cy.get(this.selectByComponentId(`grid-filter-manager-add-criteria`)).should('contain.text', 'Ajouter un critère');
  }

  openCriteriaList(): GroupedMultiListElement {
    cy.get(this.selectByComponentId(`grid-filter-manager-add-criteria`)).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExist();
    return groupedMultiListElement;
  }

  selectTextCriteria(filterId: string): TextFilterWidgetElement {
    cy.get(this.selectByComponentId(`grid-filter-manager-add-criteria`)).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExist();
    groupedMultiListElement.toggleOneItem(filterId);
    groupedMultiListElement.assertNotExist();
    const textFilterWidgetElement = new TextFilterWidgetElement();
    textFilterWidgetElement.assertExist();
    return textFilterWidgetElement;
  }

  selectNumericCriteria(filterId: string, withOutOperations?: boolean): NumericFilterWidgetElement {
    cy.get(this.selectByComponentId(`grid-filter-manager-add-criteria`)).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExist();
    groupedMultiListElement.toggleOneItem(filterId);
    groupedMultiListElement.assertNotExist();
    const numericFilterWidgetElement = new NumericFilterWidgetElement(this.gridUrl);
    if (withOutOperations) {
      numericFilterWidgetElement.assertExistWithOutOperationSelector();
    } else {
      numericFilterWidgetElement.assertExist();
    }
    return numericFilterWidgetElement;
  }

  selectDateCriteria(filterId: string): DateFilterWidgetElement {
    cy.get(this.selectByComponentId(`grid-filter-manager-add-criteria`)).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExist();
    groupedMultiListElement.toggleOneItem(filterId);
    groupedMultiListElement.assertNotExist();
    const dateFilterWidgetElement = new DateFilterWidgetElement(this.gridUrl);
    dateFilterWidgetElement.assertExist();
    return dateFilterWidgetElement;
  }

  selectMultiListCriteria(filterId: string): AsyncGroupedMultiListElement {
    cy.get(this.selectByComponentId(`grid-filter-manager-add-criteria`)).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExist();
    groupedMultiListElement.toggleOneItem(filterId);
    return new AsyncGroupedMultiListElement(this.gridUrl);
  }

  selectNumericSetCriteria(filterId: string): NumericSetFilterWidgetElement {
    cy.get(this.selectByComponentId(`grid-filter-manager-add-criteria`)).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExist();
    groupedMultiListElement.toggleOneItem(filterId);
    groupedMultiListElement.assertNotExist();
    return new NumericSetFilterWidgetElement(this.gridUrl);
  }

  fillTextCriteria(filterLabel: string, input: string, response: GridResponse = {dataRows: []}): void {
    cy.get(this.selectByComponentId(`grid-filter-manager-add-criteria`)).click();
    const groupedMultiListElement = new GroupedMultiListElement();
    groupedMultiListElement.assertExist();
    groupedMultiListElement.toggleOneItem(filterLabel);
    const textFilterWidgetElement = new TextFilterWidgetElement();
    let mock: HttpMock<GridResponse>;
    if (this.gridUrl) {
      mock = new HttpMockBuilder<GridResponse>(this.gridUrl).post().responseBody(response).build();
    }
    textFilterWidgetElement.typeAndConfirm(input);
    if (mock) {
      mock.wait();
    }
    textFilterWidgetElement.assertNotExist();
  }
}

export abstract class AbstractStepElement {

  protected rootSelector: string;

  protected constructor(protected stepId: string, protected stepIndex: number) {
    if (this.stepId) {
      this.rootSelector = `sqtm-app-test-step
    div[data-test-component-id="test-step"][data-test-test-step-id="${stepId}"]
    `;
    } else {
      // step index + 1 because we have the prerequisite block just above the steps
      this.rootSelector = `sqtm-app-test-step:nth-of-type(${this.stepIndex + 1})
      div[data-test-component-id="test-step"]
    `;
    }
  }

  checkIndexLabel() {
    this.getIndexLabel().should('contain.text', (this.stepIndex + 1).toString());
  }

  // select the step in single mode aka deselect all other steps
  singleSelectStep() {
    this.getIndexLabel().click();
  }

  // select toggle aka with ctrl
  toggleStepSelection() {
    cy.get('body').type('{ctrl}', {release: false});
    this.getIndexLabel().click();
    cy.get('body').type('{ctrl}');
  }

  assertIsSelected() {
    this.getIndexLabel().should('have.class', 'selected');
  }

  assertIsNotSelected() {
    this.getIndexLabel().should('not.have.class', 'selected');
  }

  assertActionButtonBarIsHidden() {
    this.getActionButtonBar().should('not.be.visible');
  }

  assertActionButtonBarIsVisible() {
    this.getActionButtonBar().should('be.visible');
  }

  simulateHoverIn() {
    this.singleSelectStep();
  }

  private getActionButtonBar() {
    return cy.get(`
    ${this.rootSelector}
    .step-list-element-buttons
    `)
      .invoke('trigger');
  }

  private getIndexLabel() {
    return cy.get(`
    ${this.rootSelector}
    .index-typography
    `);
  }

   clickActionStepButtonAdd() {
     cy.get(`
      ${this.rootSelector}
      div[data-test-element-id="button-area"] > div > div`)
      .find('i[data-test-button-id="addActionStepAfter"]')
      .click({ force: true });

  }

  clickActionStepButtonCopy() {
    cy.get(`
      ${this.rootSelector}
      div[data-test-element-id="button-area"] > div > div`)
      .find('i[data-test-button-id="copy"]')
      .click({ force: true });

  }

  clickActionStepButtonPaste() {
    cy.get(`
      ${this.rootSelector}
      div[data-test-element-id="button-area"] > div > div`)
      .find('i[data-test-button-id="paste"]')
      .click({ force: true });

  }

  clickActionStepButtonDeleteAndConfirm() {
    cy.get(`
      ${this.rootSelector}
      div[data-test-element-id="button-area"] > div > div`)
      .find('i[data-test-button-id="delete"]')
      .click({ force: true });

    cy.get('sqtm-core-confirm-delete-dialog').should('exist');
    this.confirmDelete();
  }

  clickActionStepButtonDeleteAndCancel() {
    cy.get(`
      ${this.rootSelector}
      div[data-test-element-id="button-area"] > div > div`)
      .find('i[data-test-button-id="delete"]')
      .click({ force: true });

    cy.get('sqtm-core-confirm-delete-dialog').should('exist');
    this.cancelDelete();
  }

  private confirmDelete() {
    cy.get('sqtm-core-confirm-delete-dialog ' +
      'button[data-test-dialog-button-id="confirm"]')
      .should('exist')
      .click();
  }

  private cancelDelete() {
    cy.get('sqtm-core-confirm-delete-dialog ' +
      'button[data-test-dialog-button-id="cancel"]')
      .should('exist')
      .click();
  }
}

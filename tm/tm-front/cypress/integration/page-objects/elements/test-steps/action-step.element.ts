import {AbstractStepElement} from './abstract-step.element';
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {EditableRichTextFieldElement} from '../forms/editable-rich-text-field.element';
import {ToolbarMenuButtonElement} from '../workspace-common/toolbar.element';
import {TestCaseModel} from '../../../model/test-case/test-case.model';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {DetailedStepViewPage} from '../../pages/test-case-workspace/detailed-step-view/detailed-step-view.page';
import {TestCaseViewPage} from '../../pages/test-case-workspace/test-case/test-case-view.page';
import {CommonSelectors, ElementSelectorFactory} from '../forms/abstract-form-field.element';

export type StepSectionType = 'action' | 'result';

export class ActionStepElement extends AbstractStepElement {

  constructor(stepId: string, stepIndex: number) {
    super(stepId, stepIndex);
  }

  checkIsExpended() {
    this.getCollapseArrow().should('exist');
    this.getExpendArrow().should('not.exist');
  }

  checkIsCollapsed() {
    this.getExpendArrow().should('exist');
    this.getCollapseArrow().should('not.exist');
  }

  collapseStep() {
    this.getCollapseArrow().click();
    this.checkIsCollapsed();
  }

  extendStep() {
    this.getExpendArrow().click();
    this.checkIsExpended();
  }

  getActionField() {
    return new EditableRichTextFieldElement('action');
  }

  getExpectedResultField() {
    return new EditableRichTextFieldElement('result');
  }

  private getCollapseArrow() {
    return cy.get(`
    ${this.rootSelector}
    ${selectByDataTestComponentId('collapse-step')}
    `);
  }

  private getExpendArrow() {
    return cy.get(`
    ${this.rootSelector}
    ${selectByDataTestComponentId('expend-step')}
    `);
  }

  checkCalledTestCaseStep(calledTestCaseName: string) {
    cy.get('[data-test-field-id="calledTestCase"]')
      .contains(calledTestCaseName);
  }

  clickCalledTestCaseStep(calledTestCaseName: string): TestCaseViewPage {
    cy.get('[data-test-field-id="calledTestCase"]')
      .contains(calledTestCaseName)
      .click();
    return new TestCaseViewPage('*');
  }

  fillEmptyStep(value: string, stepSectionType: StepSectionType) {
    cy.get(`${this.rootSelector}> div > div > sqtm-app-create-step-form > form > div > div.${stepSectionType} ckeditor p`)
      .should('exist')
      .click({force: true})
      .as('a');

    cy.get('@a').type(value);

  }

  clickAdd() {
    const button = cy.get(`
        ${this.rootSelector}
         button[data-test-button-id='addStep']
         `);

    button.click();
    // Cypress guard for next commands
    button.should('not.exist');
  }

  clickAddAnother() {
    cy.get(`
        ${this.rootSelector}
         button[data-test-button-id='addAnotherStep']
         `)
      .click();
  }

  clickCancel() {
    cy.get(`
        ${this.rootSelector}
         button[data-test-button-id='cancel']
         `)
      .click();
  }

  modifyFilledStep(value: string, stepSectionType: StepSectionType) {
    cy.get(`
    ${this.rootSelector}
    div.${stepSectionType}
    `).click()
      .then(() => {
        const actionForm: EditableRichTextFieldElement = new EditableRichTextFieldElement(this.getFieldSelector(stepSectionType));
        actionForm.setAndConfirmValue(value);
      });
  }

  modifyFilledStepWithParam(value: string,
                            stepSectionType: StepSectionType,
                            url: string,
                            response?: any
  ) {

    cy.get(this.rootSelector)
      .find(`div.${stepSectionType}`).click();
    const fieldSelector = this.getFieldSelector(stepSectionType);
    const actionForm: EditableRichTextFieldElement = new EditableRichTextFieldElement(fieldSelector, url);
    actionForm.enableEditMode();
    const newValue = value.replace(/(\$)(\{)(\w*\s*\w*)(})/gi, '$1{$2}$3{$4}');
    actionForm.setValue(newValue);
    actionForm.confirmValue(response);
    actionForm.checkTextContent(value);
  }

  modifyFilledStepOnErrorWithParam(value: string,
                                   stepSectionType: StepSectionType,
                                   url: string,
                                   squashError?: any
  ) {
    cy.get(this.rootSelector)
      .find(`div.${stepSectionType}`).click();

    const actionForm: EditableRichTextFieldElement = new EditableRichTextFieldElement(this.getFieldSelector(stepSectionType), url);
    actionForm.enableEditMode();
    const newValue = value.replace(/(\$)(\{)(\w*\s*\w*)(})/gi, '$1{$2}$3{$4}');
    actionForm.setValue(newValue);
    actionForm.confirmOnError(squashError);
  }

  navigateToStepDetails(model?: TestCaseModel) {
    const actionMenu = new ToolbarMenuButtonElement(this.rootSelector, 'step-action-button', 'step-action-menu');
    const testCaseId = model ? model.id.toString() : '*';
    const url = `detailed-test-step-view/${testCaseId}?**`;
    const mock = new HttpMockBuilder(url).responseBody(model).build();
    actionMenu.showMenu(true).item('navigate-to-details').click();
    mock.wait();
    return new DetailedStepViewPage(testCaseId);
  }

  private getFieldSelector(stepSectionType: StepSectionType): ElementSelectorFactory {
    return CommonSelectors.childFieldName(() => cy.get(this.rootSelector), stepSectionType);
  }

}

import {RichTextFieldElement} from '../forms/RichTextFieldElement';
import {selectByDataTestComponentId} from '../../../utils/basic-selectors';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';

export class CreateActionStepFormElement {
  private readonly actionField: RichTextFieldElement;
  private readonly expectedResultField: RichTextFieldElement;

  constructor() {
    this.actionField = new RichTextFieldElement('action');
    this.expectedResultField = new RichTextFieldElement('expectedResult');
  }

  public assertFormIsReady() {
    this.actionField.assertIsReady();
    this.expectedResultField.assertIsReady();
  }

  public fillAction(name: string) {
    this.actionField.fillWithString(name);
  }

  public fillExpectedResult(name: string) {
    this.expectedResultField.fillWithString(name);
  }

  // public fillCustomField(cufId: number, value: any) {
  //   this.customFieldForm.fillCustomField(cufId, value);
  // }

  public confirmAddStep(testCaseId?: number, response?: any) {
    const mock = new HttpMockBuilder(`/test-cases/${testCaseId || '*'}/steps/add`)
      .post()
      .responseBody(response)
      .build();
    cy.get(selectByDataTestComponentId('confirm-add-test-step')).click();
    mock.wait();
  }

  public cancelAddStep() {
    cy.get(selectByDataTestComponentId('cancel-add-test-step')).click();
  }

  public assertCancelAddStepButtonDisabled() {
    cy.get(selectByDataTestComponentId('cancel-add-test-step')).should('be.disabled');
  }
}

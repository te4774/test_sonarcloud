import {TestCaseParameterOperationReport} from '../../../model/test-case/change-dataset-param-operation.model';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {CommonSelectors} from '../forms/abstract-form-field.element';
import {EditableActionTextFieldElement} from '../forms/editable-action-text-field.element';
import {EditableSelectFieldElement} from '../forms/editable-select-field.element';
import {EditableTextAreaFieldElement} from '../forms/editable-text-area-field.element';

export class KeywordStepElement {

  rootSelector: string;

  constructor(private stepId: number, private stepIndex: number) {
    if (this.stepId) {
      this.rootSelector = `sqtm-app-keyword-test-step
    div[data-test-component-id="keyword-test-step"][data-test-test-step-id="${stepId}"]
    `;
    } else {
      // step index + 1 because we have the prerequisite block just above the steps
      this.rootSelector = `sqtm-app-keyword-step:nth-of-type(${this.stepIndex + 1})
      div[data-test-component-id="keyword-test-step"]
    `;
    }
  }

  assertExist() {
    cy.get(this.rootSelector).should('exist');
  }

  checkIndexLabel() {
    this.getIndexLabel().should('contain.text', (this.stepIndex + 1).toString());
  }

  selectStep() {
    this.getIndexLabel().click();
  }

  getKeywordField(): EditableSelectFieldElement {
    const parentSelector = () => cy.get(this.rootSelector);
    return new EditableSelectFieldElement(
      CommonSelectors.childFieldName(parentSelector, 'keyword'),
      'test-steps/*/keyword');
  }

  getActionField(): EditableActionTextFieldElement {
    const parentSelector = () => cy.get(this.rootSelector);
    return new EditableActionTextFieldElement(
      CommonSelectors.childFieldName(parentSelector, 'action'),
      'test-steps/*/action-word');
  }

  navigateToActionWord() {
    cy.intercept('/plugin', (interceptedRequest) => {
      expect(interceptedRequest.url).to.contain('/plugin/actionword/action-word-workspace/action-word/detail');
      interceptedRequest.url = '/squash/test-case-workspace';
    });
    this.getNavigateToActionButton().click();
  }

  deleteStep(response?: TestCaseParameterOperationReport) {
    new HttpMockBuilder('test-cases/*/verified-requirements').responseBody({}).build();
    const mock =
      new HttpMockBuilder('test-cases/*/steps/*')
        .delete()
        .responseBody(response)
        .build();
    this.getDeleteButton().click({force: true});
    this.confirmDelete();
    mock.wait();
  }

  checkDeleteButtonNotVisible() {
    this.getDeleteButton().should('not.exist');
  }

  // select toggle aka with ctrl
  toggleStepSelection() {
    cy.get('body').type('{ctrl}', {release: false});
    this.getIndexLabel().click();
    cy.get('body').type('{ctrl}');
  }

  moveStepUp() {
    const mock = new HttpMockBuilder('test-cases/*/steps/move').post().build();
    this.getArrowUp().click();
    mock.wait();
  }

  moveStepDown() {
    const mock = new HttpMockBuilder('test-cases/*/steps/move').post().build();
    this.getArrowDown().click();
    mock.wait();
  }

  getDraggableArea() {
    return cy.get(this.rootSelector)
      .find('.dnd-handler')
      .invoke('show');
  }

  private getIndexLabel() {
    return cy.get(`
    ${this.rootSelector}
    .index-typography
    `);
  }

  private getDeleteButton() {
    return cy.get(`
    ${this.rootSelector}
    div[data-test-element-id="button-area"]`)
      .find('i[data-test-button-id="delete"]');
  }

  private getNavigateToActionButton() {
    this.getMoreButton().click();
    return cy.get(`li[data-test-menu-item-id="navigate-to-action"]`);
  }

  private getMoreButton() {
    cy.get(this.rootSelector)
      .find('.step-list-element-buttons')
      .invoke('show');
    return cy.get(this.rootSelector)
      .find('i[data-test-button-id="more"]');
  }

  private confirmDelete() {
    cy.get('sqtm-core-confirm-delete-dialog ' +
      'button[data-test-dialog-button-id="confirm"]')
      .should('exist')
      .click();
  }

  private getArrowUp() {
    return cy.get(this.rootSelector)
      .find('.arrow-sup')
      .invoke('show');
  }

  private getArrowDown() {
    return cy.get(this.rootSelector)
      .find('.arrow-down')
      .invoke('show');
  }

  /* Sub-sections management */

  getTextAreaField(fieldName: KeywordStepSectionName): EditableTextAreaFieldElement {
    const parentSelector = () => cy.get(this.rootSelector);
    return new EditableTextAreaFieldElement(
      CommonSelectors.childFieldName(parentSelector, fieldName),
      `test-steps/*/${fieldName}`);
  }

  assertSectionExist(sectionName: KeywordStepSectionName) {
    this.getSection(sectionName).should('exist');
  }

  assertSectionNotExist(sectionName: KeywordStepSectionName) {
    this.getSection(sectionName).should('not.exist');
  }

  assertSectionDeleteIconAppears(sectionName: KeywordStepSectionName) {
    this.getSectionDeleteIcon(sectionName).should('be.visible');
  }

  createSection(sectionName: KeywordStepSectionName) {
    this.getMoreButton().click();
    this.getSectionCreateButton(sectionName).click();
  }

  deleteSection(sectionName: KeywordStepSectionName) {
    const mock = new HttpMockBuilder( `test-steps/*/${sectionName}`).post().build();
    this.getSectionDeleteIcon(sectionName).click();
    mock.wait();
  }

  private getSection(sectionName: KeywordStepSectionName) {
    return cy.get(this.rootSelector).find(`.${sectionName}-section`);
  }

  private getSectionCreateButton(sectionName: KeywordStepSectionName) {
    cy.get('ul[data-test-menu-id="menu"]').invoke('show');
    return cy.get(`li[data-test-menu-item-id="create-${sectionName}"]`);
  }

  private getSectionDeleteIcon(sectionName: KeywordStepSectionName) {
    return this.getSection(sectionName)
      .find('i[data-test-button-id="delete"]')
      .invoke('show');
  }

}

export const DATATABLE = 'datatable';
export const DOCSTRING = 'docstring';
export const COMMENT = 'comment';
export type KeywordStepSectionName = 'datatable' | 'docstring' | 'comment';

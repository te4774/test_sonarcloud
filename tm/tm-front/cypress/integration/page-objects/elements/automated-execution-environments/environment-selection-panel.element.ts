import {GridElement} from '../grid/grid.element';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {CommonSelectors} from '../forms/abstract-form-field.element';
import {AutomatedExecutionEnvironment} from '../../../model/test-automation/test-automation-server.model';
import {selectByDataTestComponentId, selectByDataTestToolbarButtonId} from '../../../utils/basic-selectors';
import {EditableTextFieldElement} from '../forms/editable-text-field.element';
import Chainable = Cypress.Chainable;

export class EnvironmentSelectionPanelElement {

  public readonly availableEnvironmentsGrid: GridElement = GridElement.createGridElement(selectors.AVAILABLE_ENVIRONMENTS_GRID);

  public readonly tokenOverrideField: EditableTextFieldElement = new EditableTextFieldElement(
    () => cy.get(this.rootSelector).find(selectByDataTestComponentId(selectors.TOKEN_FIELD)),
    urls.PROJECT_TOKEN);

  private readonly rootSelector = 'sqtm-app-environment-selection-panel';

  assertTagsOptionsContainAll(options: string[]) {
    this.openEnvironmentTagSelectList();
    cy.get('nz-option-item')
      .should('have.length', options.length)
      .each(($el, index) => {
        expect($el).to.contain(options[index]);
      });
    this.closeEnvironmentTagSelectList();
  }

  assertSelectedTagsContainAll(tags: string[]) {
    this.findByScopedComponentId(selectors.SELECTED_ENVIRONMENT_TAGS)
      .find('nz-select-item > div')
      .should('have.length', tags.length)
      .each(($el, index) => {
        expect($el).to.contain(tags[index]);
      });
  }

  addTag(tag: string, response: string[]) {
    const mockUpdateTags = new HttpMockBuilder(urls.SERVER_TAGS)
      .post()
      .responseBody(response)
      .build();

    this.openEnvironmentTagSelectList();

    cy.get('nz-option-item')
      .contains(tag)
      .click();

    mockUpdateTags.wait();

    this.closeEnvironmentTagSelectList();
  }

  removeTag(tag: string, response: string[]) {
    const mockUpdateTags = new HttpMockBuilder(urls.SERVER_TAGS)
      .post()
      .responseBody(response)
      .build();
    this.clickOnTagCross(tag);
    mockUpdateTags.wait();
  }

  assertMissingCredentialsMessageExists(): void {
    this.findByScopedComponentId(selectors.MISSING_CREDENTIALS_MESSAGE).should('exist');
  }

  assertLoadEnvironmentsErrorMessageExists(): void {
    this.findByScopedComponentId(selectors.LOAD_ENVIRONMENTS_ERROR_MESSAGE).should('exist');
  }

  assertNoMatchingEnvironmentMessageExists(): void {
    this.findByScopedComponentId(selectors.NO_MATCHING_ENVIRONMENT).should('exist');
  }

  clearTagOverrides(): void {
    const mock = new HttpMockBuilder(urls.PROJECT_TAGS).delete().build();

    cy.get(this.rootSelector)
      .find(selectByDataTestToolbarButtonId(selectors.RESET_TAGS_BUTTON))
      .click();

    mock.wait();
  }

  clearTokenOverride(): void {
    const mock = new HttpMockBuilder(urls.PROJECT_TOKEN).delete().build();
    this.getResetTokenButton().click();
    mock.wait();
  }

  assertResetTokenButtonIsHidden(): void {
    this.getResetTokenButton().should('not.exist');
  }

  assertResetTokenButtonIsVisible(): void {
    this.getResetTokenButton().should('be.visible');
  }

  setTokenOverride(newValue: string) {
    this.tokenOverrideField.setAndConfirmValue(newValue);
  }

  private clickOnTagCross(tag: string) {
    this.findByScopedComponentId(selectors.SELECTED_ENVIRONMENT_TAGS)
      .contains('nz-select-item', tag)
      .find('span')
      .click();
  }

  private openEnvironmentTagSelectList() {
    const httpMock = new HttpMockBuilder(urls.SERVER_AVAILABLE_TAGS).build();
    this.findByScopedComponentId(selectors.SELECTED_ENVIRONMENT_TAGS)
      .find('nz-select')
      .click();
    httpMock.wait();
  }

  private closeEnvironmentTagSelectList() {
    cy.clickVoid();
  }

  private findByScopedComponentId(id: string): Chainable {
    const thisPageSelectorFactory = () => cy.get(this.rootSelector);
    return CommonSelectors.childComponentId(thisPageSelectorFactory, id)();
  }

  private getResetTokenButton(): Chainable {
    return cy.get(this.rootSelector)
      .find(selectByDataTestToolbarButtonId(selectors.RESET_TOKEN_BUTTON));
  }
}

export interface EnvironmentSelectionPanelDto {
  environments: {
    environments?: AutomatedExecutionEnvironment[];
  };

  server: {
    testAutomationServerId: number;
    defaultTags: string[];
    hasServerCredentials: boolean;
  };

  project?: {
    projectId: number;
    hasProjectToken: boolean;
    projectTags: string[];
    areProjectTagsInherited: boolean;
  };
}

const SERVER_URL_PREFIX = 'test-automation-servers/*/';
const PROJECT_URL_PREFIX = 'generic-projects/*/';

const urls = {
  SERVER_TAGS: SERVER_URL_PREFIX + 'environment-tags',
  SERVER_AVAILABLE_TAGS: SERVER_URL_PREFIX + 'available-tags',
  PROJECT_TOKEN: PROJECT_URL_PREFIX + 'automated-execution-environments/tokens/*',
  PROJECT_TAGS: PROJECT_URL_PREFIX + 'environment-tags',
};

const selectors = {
  TOKEN_FIELD: 'token-field',
  AVAILABLE_ENVIRONMENTS_GRID: 'available-execution-environments',
  SELECTED_ENVIRONMENT_TAGS: 'selected-environment-tags',
  MISSING_CREDENTIALS_MESSAGE: 'missing-credentials-message',
  LOAD_ENVIRONMENTS_ERROR_MESSAGE: 'load-environments-error-message',
  NO_MATCHING_ENVIRONMENT: 'no-matching-environment',
  RESET_TOKEN_BUTTON: 'reset-token-button',
  RESET_TAGS_BUTTON: 'reset-tags-button',
};

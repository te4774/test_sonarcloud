export class ConfirmDialogElement {
  constructor(protected dialogId: string) {
  }

  assertNotExist() {
    cy.get(this.buildSelector()).should('not.exist');
  }

  assertExist() {
    cy.get(this.buildSelector()).should('exist');
  }


  buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  assertHasMessage(expected: string) {
    return cy.get(this.buildMessageSelector()).should('contain.text', expected);
  }

  clickOnConfirmButton() {
    const buttonSelector = this.buildButtonSelector('confirm');
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click();
    cy.get(this.buildSelector()).should('not.exist');
  }

  clickOnCancelButton(): void {
    cy.get(this.buildButtonSelector('cancel'))
      .should('exist')
      .click()
      .get(this.buildSelector())
      .should('not.exist');
  }

  buildMessageSelector(): string {
    return `${this.buildSelector()} [data-test-dialog-message]`;
  }

  protected buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }
}

import {GridElement} from '../grid/grid.element';

export class SingleMilestonePickerDialogElement {

  private readonly dialogId = 'single-milestone-picker-dialog';

  private readonly labelCellId = 'label';
  private readonly selectCellId = 'single-select-row-column';

  grid: GridElement;

  constructor() {
    this.grid = new GridElement('single-milestones-picker-grid');
  }

  fillSearchInput(value: string) {
    cy.get(this.dialogId).find('nz-input-group input')
      .should('exist')
      .clear()
      .type(value);
  }

  confirm() {
    this.clickButton('confirm');
    this.assertNotExist();
  }

  cancel() {
    this.clickButton('cancel');
    this.assertNotExist();
  }

  clickButton(buttonId: string) {
    cy.get(`[data-test-dialog-button-id=${buttonId}]`).click();
  }

  assertExist() {
    cy.get('[data-test-dialog-id=project-filter]').should('exist');
  }

  assertNotExist() {
    cy.get('[data-test-dialog-id=project-filter]').should('not.exist');
  }

  selectMilestone(id: number | string) {
    if (typeof id === 'string') {
      this.grid
        .findRowId(this.labelCellId, id)
        .then(numId => this.grid.getCell(numId, this.selectCellId).radioNgZorroRenderer().check());
    } else {
      this.grid.getCell(id, this.selectCellId).radioNgZorroRenderer().check();
    }
  }

  assertMilestoneIsSelected(id: number | string) {
    if (typeof id === 'string') {
      this.grid
        .findRowId(this.labelCellId, id)
        .then(numId => this.grid.getCell(numId, this.selectCellId).radioNgZorroRenderer().assertIsCheck());
    } else {
      this.grid.getCell(id, this.selectCellId).radioNgZorroRenderer().assertIsCheck();
    }
  }

}

import {GridElement} from '../grid/grid.element';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';

export class ProjectFilterDialog {

  grid: GridElement;

  constructor() {
    this.grid = new GridElement('project-filter');
  }


  fillSearchInput(value: string) {
    const searchInput = cy.get('nz-input-group input');
    searchInput.should('exist');
    searchInput.clear().type(value);
  }


  confirm() {
    const mockBuilder = new HttpMockBuilder('global-filter/filter').post().build();
    this.clickButton('apply');
    mockBuilder.wait();
    this.assertNotExist();
  }

  confirmWithEnterKey() {
    const mockBuilder = new HttpMockBuilder('global-filter/filter').post().build();
    cy.get('body').type('{enter}');
    mockBuilder.wait();
    this.assertNotExist();
  }

  cancel() {
    this.clickButton('cancel');
    this.assertNotExist();
  }

  cancelWithEscapeKey() {
    cy.get('body').type('{esc}');
    this.assertNotExist();
  }

  clickButton(buttonId: string) {
    cy.get(`[data-test-dialog-button-id=${buttonId}]`).click();
  }

  assertExist() {
    cy.get('[data-test-dialog-id=project-filter]').should('exist');
  }

  assertNotExist() {
    cy.get('[data-test-dialog-id=project-filter]').should('not.exist');
  }

  assertMilestoneWarningIsVisible() {
    cy.get('[data-test-component-id=milestone-warning]')
      .should('exist');
  }

  assertMilestoneWarningIsNotVisible() {
    cy.get('[data-test-component-id=milestone-warning]')
      .should('not.exist');
  }

}

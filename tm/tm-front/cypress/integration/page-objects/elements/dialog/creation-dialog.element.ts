import {HTTP_RESPONSE_STATUS, HttpMockBuilder} from '../../../utils/mocks/request-mock';
import Chainable = Cypress.Chainable;

export abstract class AbstractCreationDialogElement {
  protected constructor(protected dialogId: string) {
  }

  public assertExist(): void {
    cy.get(this.buildSelector()).should('exist');
  }

  public assertNotExist(): void {
    cy.get(this.buildSelector()).should('not.exist');
  }

  public buildSelector(): string {
    return `[data-test-dialog-id=${this.dialogId}]`;
  }

  public buildButtonSelector(buttonId: string) {
    return `${this.buildSelector()} [data-test-dialog-button-id=${buttonId}]`;
  }

  protected clickButton(buttonId: string): void {
    const buttonSelector = this.buildButtonSelector(buttonId);
    cy.get(buttonSelector).should('exist');
    cy.get(buttonSelector).click({force: true});
  }

  public cancel(): void {
    this.clickButton('cancel');
    cy.get('button[data-test-dialog-button-id="cancel"').should('not.exist');
  }

  public clickOnAddAnotherButton(): void {
    this.clickButton('add-another');
  }

  public clickOnAddButton(): void {
    this.clickButton('add');
  }
}

/**
 *  Base class for creation dialogs.
 */
export abstract class CreationDialogElement<T extends CreateDialogOptions> extends AbstractCreationDialogElement {
  constructor(protected dialogId: string) {
    super(dialogId);
  }

  // maybe we should ask for a specific fieldName...
  // -> [JTH 16/02/2022] How can you just say "maybe" ?
  // Of course, you MUST check in witch field the error message is displayed
  // and by doing this error in all methods here, we have now dozen of imprecise tests, that will be hard to maintain...
  // And btw, the best place to implement this code is directly in text-field element.
  /**
   * @deprecated Use I18Error instance instead, and target the field ! See TextFieldElement to have exemple.
   */
  public checkIfRequiredErrorMessageIsDisplayed(): void {
    const errorMessage = 'Ce champ ne peut pas être vide.';
    cy.get('span[data-test-error-key="sqtm-core.validation.errors.required"]').should('contain.text', errorMessage);
  }

  /**
   * @deprecated Use I18Error instance instead, and target the field ! See TextFieldElement to have exemple.
   */
  public checkIfDuplicateNameErrorMessageIsDisplayed(): void {
    const errorMessage = 'Un élément avec ce nom existe déjà à cet emplacement';
    cy.get('span[data-test-error-key="sqtm-core.error.generic.duplicate-name"]').should('contain.text', errorMessage);
  }

  /**
   * @deprecated Use I18Error instance instead, and target the field ! See TextFieldElement to have exemple.
   */
  public checkIfNameAlreadyInUseErrorMessageIsDisplayed(): void {
    const errorMessage = 'Ce nom est déjà utilisé.';
    cy.get('span[data-test-error-key="sqtm-core.error.generic.name-already-in-use"]').should('contain.text', errorMessage);
  }

  /**
   * @deprecated Use I18Error instance instead, and target the field ! See TextFieldElement to have exemple.
   */
  public checkIfMalformedUrlErrorMessageIsDisplayed(): void {
    const errorMessage = 'URL non valide';
    cy.get('span[data-test-error-key="sqtm-core.exception.wrong-url"]').should('contain.text', errorMessage);
  }

  public abstract addWithOptions(options: T): Chainable<any>;

  public addWithServerSideFailure(response: any = {}) {
    const mock = new HttpMockBuilder<any>(this.getNewEntityUrl())
      .post()
      .status(HTTP_RESPONSE_STATUS.PRECONDITION_FAIL)
      .responseBody(response)
      .build();
    this.clickOnAddButton();
    return mock.waitResponseBody(HTTP_RESPONSE_STATUS.PRECONDITION_FAIL);
  }

  public addWithClientSideFailure() {
    this.clickOnAddButton();
  }

  /**
   * The server end point used to create a new entity
   */
  protected abstract getNewEntityUrl(): string;
}

/**
 * Options to give to the 'addWithOptions' method. This can be specialized for more
 * specific needs.
 */
export interface CreateDialogOptions {
  /**
   * Should the 'add another' button be clicked ?
   */
  addAnother?: boolean;

  /**
   * The ID of the element to add (in a mock situation).
   */
  addedId?: number;

  /**
   * The server's response after creation (in a mock situation).
   */
  createResponse?: any;
}

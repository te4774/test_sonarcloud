import {GridElement} from '../grid/grid.element';

export class MultipleMilestonePickerDialogElement {

  private readonly dialogId = 'multiple-milestone-picker-dialog';

  private readonly labelCellId = 'label';
  private readonly selectCellId = 'select-row-column';

  grid: GridElement;

  constructor() {
    this.grid = new GridElement('multiple-milestones-picker-grid');
  }

  fillSearchInput(value: string) {
    cy.get(this.dialogId).find('nz-input-group input')
      .should('exist')
      .clear()
      .type(value);
  }

  confirm() {
    this.clickButton('confirm');
    this.assertNotExist();
  }

  cancel() {
    this.clickButton('cancel');
    this.assertNotExist();
  }

  clickButton(buttonId: string) {
    cy.get(`[data-test-dialog-button-id=${buttonId}]`).click();
  }

  assertExist() {
    cy.get('[data-test-dialog-id=multiple-milestone-picker-dialog]').should('exist');
  }

  assertNotExist() {
    cy.get('[data-test-dialog-id=multiple-milestone-picker-dialog]').should('not.exist');
  }

  selectMilestone(id: number | string) {
    if (typeof id === 'string') {
      this.grid
        .findRowId(this.labelCellId, id)
        .then(numId => this.grid.getCell(numId, this.selectCellId).checkBoxRender().toggleState());
    } else {
      this.grid.getCell(id, this.selectCellId).checkBoxRender().toggleState();
    }
  }

  assertMilestoneIsSelected(id: number | string) {
    if (typeof id === 'string') {
      this.grid
        .findRowId(this.labelCellId, id)
        .then(numId => this.grid.getCell(numId, this.selectCellId).checkBoxRender().assertIsCheck());
    } else {
      this.grid.getCell(id, this.selectCellId).checkBoxRender().assertIsCheck();
    }
  }

}

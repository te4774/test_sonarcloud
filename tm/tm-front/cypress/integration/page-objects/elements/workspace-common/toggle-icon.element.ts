import Chainable = Cypress.Chainable;

export class ToggleIconElement {

  private readonly tagName = 'sqtm-core-toggle-icon';

  constructor(private attributeValue: string, private attributeName = 'data-test-component-id') {
  }

  assertExist() {
    this.select().should('exist');
  }

  assertNotExist() {
    this.select().should('not.exist');
  }

  assertIsActive() {
    this.selectSpanWrapper().should('have.class', 'current-workspace-button');
  }

  assertIsNotActive() {
    this.selectSpanWrapper().should('not.have.class', 'current-workspace-button');
  }


  private select(): Chainable<any> {
    return cy.get(`${this.tagName} i[${this.attributeName}='${this.attributeValue}']`);
  }

  private selectSpanWrapper(): Chainable<any> {
    return this.select().parent();
  }

  click() {
    this.select().click();
  }
}

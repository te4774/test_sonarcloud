import {ToolbarButtonElement, ToolbarElement, ToolbarMenuButtonElement} from './toolbar.element';
import {GridResponse, Identifier} from '../../../model/grids/data-row.type';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {DeleteTreeNodesDialogElement} from '../../pages/test-case-workspace/dialogs/delete-tree-nodes-dialog.element';
import {SingleMilestonePickerDialogElement} from '../dialog/single-milestone-picker-dialog.element';
import {TestCaseStatistics} from '../../../model/test-case/test-case-statistics.model';
import {TestCaseMilestoneViewPage} from '../../pages/test-case-workspace/test-case/test-case-milestone-view.page';
import {
  RequirementMilestoneViewPage
} from '../../pages/requirement-workspace/requirement/requirement-milestone-view.page';
import {
  DeleteCampaignTreeNodesDialog
} from '../../pages/campaign-workspace/dialogs/delete-campaign-tree-nodes-dialog.element';
import {MilestoneRequirementDashboardModel} from '../../../model/requirements/milestone-requirement-dashboard.model';
import {CampaignStatisticsBundle} from '../../../model/campaign/campaign-model';
import {CampaignMilestoneViewPage} from '../../pages/campaign-workspace/campaign/campaign-milestone-view.page';
import {KeyboardShortcuts} from './keyboard-shortcuts';

export class TreeToolbarElement extends ToolbarElement {
  constructor(protected readonly toolbarId: string) {
    super(toolbarId);
  }

  protected copyButton(): ToolbarButtonElement {
    const createButtonElement = this.button('copy-button');
    createButtonElement.assertExist();
    return createButtonElement;
  }

  protected pasteButton(): ToolbarButtonElement {
    const createButtonElement = this.button('paste-button');
    createButtonElement.assertExist();
    return createButtonElement;
  }

  protected deleteButton(): ToolbarButtonElement {
    const buttonElement = this.button('delete-button');
    buttonElement.assertExist();
    return buttonElement;
  }

  assertCopyButtonIsActive() {
    const copyButtonElement = this.copyButton();
    copyButtonElement.assertIsActive();
  }


  assertCopyButtonIsDisabled() {
    const copyButtonElement = this.copyButton();
    copyButtonElement.assertIsDisabled();
  }

  copy(useShortcut = false) {
    if (useShortcut) {
      new KeyboardShortcuts().performCopy();
    } else {
      const copyButtonElement = this.copyButton();
      copyButtonElement.click();
    }
  }

  assertPasteButtonIsActive() {
    const pasteButton = this.pasteButton();
    pasteButton.assertIsActive();
  }

  assertPasteButtonIsDisabled() {
    const pasteButton = this.pasteButton();
    pasteButton.assertIsDisabled();
  }

  assertCreateButtonIsDisabled() {
    const createButton = this.createButton();
    createButton.assertIsDisabled();
  }

  assertImportButtonIsDisabled() {
    const importButton = this.importExportButton();
    importButton.assertIsDisabled();
  }

  paste(refreshedNodes: GridResponse = {dataRows: []}, treeUrl: string, destinationId: Identifier, useShortcut = false) {
    const refreshUrl = `${treeUrl}/refresh`;
    const pasteUrl = `${treeUrl}/${destinationId}/content/paste`;
    const pasteMock = new HttpMockBuilder(pasteUrl).post().build();
    const refreshDestinationMock = new HttpMockBuilder(refreshUrl).post().responseBody(refreshedNodes).build();
    this.performPasteCommand(useShortcut);
    pasteMock.wait();
    refreshDestinationMock.wait();
  }

  private performPasteCommand(useShortcut: boolean) {
    if (useShortcut) {
      new KeyboardShortcuts().performPaste();
    } else {
      const pasteButton = this.pasteButton();
      pasteButton.click();
    }
  }

  assertDeleteButtonIsActive() {
    const deleteButton = this.deleteButton();
    deleteButton.assertIsActive();
  }

  assertDeleteButtonIsDisabled() {
    const deleteButton = this.deleteButton();
    deleteButton.assertIsDisabled();
  }

  createButton(): ToolbarMenuButtonElement {
    const createButtonElement = this.buttonMenu('create-button', 'create-menu');
    createButtonElement.assertExist();
    return createButtonElement;
  }

  importExportButton(): ToolbarMenuButtonElement {
    const importExportButtonElement = this.buttonMenu('import-export', 'import-export-menu');
    importExportButtonElement.assertExist();
    return importExportButtonElement;
  }

  initDeletion(treeUrl: string, deleteNodes: any[], warnings: string[] = [], useShortcut = false): DeleteTreeNodesDialogElement {
    const deleteButton = this.deleteButton();
    const pathParam = deleteNodes.map(id => id.toString()).join(',');
    const simulationUrl = `${treeUrl}/deletion-simulation/${pathParam}`;
    const simulationMock = new HttpMockBuilder(simulationUrl).responseBody({messageCollection: warnings}).build();
    deleteButton.click();
    simulationMock.wait();
    return new DeleteTreeNodesDialogElement(treeUrl);
  }

  // deleted node is identifier[]
  initCampaignWorkspaceDeletion(deleteNodes: any[], warnings: string[] = []): DeleteCampaignTreeNodesDialog {
    const deleteButton = this.deleteButton();
    const pathParam = deleteNodes.map(id => id.toString()).join(',');
    const simulationUrl = `campaign-tree/deletion-simulation/${pathParam}`;
    const simulationMock = new HttpMockBuilder(simulationUrl).responseBody({messageCollection: warnings}).build();
    deleteButton.click();
    simulationMock.wait();
    return new DeleteCampaignTreeNodesDialog('campaign-tree');
  }

  sortTreePositional() {
    const sortButton = this.buttonMenu('settings-button', 'sort-menu');
    sortButton.showMenu().item('sort-positional').click();
    sortButton.hideMenu();
  }

  sortTreeAlphabetical() {
    const sortButton = this.buttonMenu('settings-button', 'sort-menu');
    sortButton.showMenu().item('sort-alphabetical').click();
    sortButton.hideMenu();
  }

  assertMilestoneButtonIsVisible() {
    this.buttonMenu('milestone-button', 'milestone-menu').assertExist();
  }

  showMilestoneSelector(): SingleMilestonePickerDialogElement {
    const milestoneButton = this.buttonMenu('milestone-button', 'milestone-menu');
    milestoneButton.showMenu().item('change-active-milestone').click();
    return new SingleMilestonePickerDialogElement();
  }

  showMilestoneDashboard(statistics: { statistics: TestCaseStatistics }): TestCaseMilestoneViewPage {
    const milestoneButton = this.buttonMenu('milestone-button', 'milestone-menu');
    const mock = new HttpMockBuilder('test-case-milestone-dashboard').responseBody(statistics).build();
    milestoneButton.showMenu().item('show-active-milestone-dashboard').click();
    mock.wait();
    const testCaseMilestoneViewPage = new TestCaseMilestoneViewPage();
    testCaseMilestoneViewPage.assertExist();
    return testCaseMilestoneViewPage;
  }

  showCampaignMilestoneDashboard(statistics: { statistics: CampaignStatisticsBundle }): CampaignMilestoneViewPage {
    const milestoneButton = this.buttonMenu('milestone-button', 'milestone-menu');
    const mock = new HttpMockBuilder('campaign-milestone-dashboard').responseBody(statistics).build();
    milestoneButton.showMenu().item('show-active-milestone-dashboard').click();
    mock.wait();
    const campaignMilestoneViewPage = new CampaignMilestoneViewPage();
    campaignMilestoneViewPage.assertExist();
    return campaignMilestoneViewPage;
  }

  showRequirementMilestoneDashboard(statistics: MilestoneRequirementDashboardModel): RequirementMilestoneViewPage {
    const milestoneButton = this.buttonMenu('milestone-button', 'milestone-menu');
    const mock = new HttpMockBuilder('requirement-milestone-dashboard').responseBody(statistics).build();
    milestoneButton.showMenu().item('show-active-milestone-dashboard').click();
    mock.wait();
    const requirementMilestoneViewPage = new RequirementMilestoneViewPage();
    requirementMilestoneViewPage.assertExist();
    return requirementMilestoneViewPage;
  }
}

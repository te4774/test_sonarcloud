export enum Locales {
  EN = 'EN', FR = 'FR', DE = 'DE', ES = 'ES',
}

export type I18nMessage = { [Locales.FR]: string } & { [K in Locales]?: string };

export class I18nError {
  readonly key: string;
  private readonly messages: I18nMessage;

  constructor(key: string, messages: string | I18nMessage) {
    this.key = key;
    if (typeof messages === 'string') {
      messages = {FR: messages};
    }
    this.messages = messages;
  }

  get message() {
    const envLocale = Cypress.env('locale') || Locales.FR;
    return this.messages[Locales[envLocale]];
  }
}

const EMPTY_FIELD_ERROR_MESSAGE_FR = 'Ce champ ne peut pas être vide.';
const EMPTY_FIELD_ERROR_MESSAGE_KEY = 'sqtm-core.validation.errors.required';
export const EMPTY_FIELD_ERROR = new I18nError(EMPTY_FIELD_ERROR_MESSAGE_KEY, EMPTY_FIELD_ERROR_MESSAGE_FR);

const LOGIN_ALREADY_USED_ERROR_MESSAGE_FR = 'Ce login est déjà utilisé.';
const LOGIN_ALREADY_USED_ERROR_MESSAGE_KEY = 'sqtm-core.error.generic.login-already-in-use';
export const LOGIN_ALREADY_USED_ERROR = new I18nError(LOGIN_ALREADY_USED_ERROR_MESSAGE_KEY, LOGIN_ALREADY_USED_ERROR_MESSAGE_FR);

const NAME_ALREADY_USED_ERROR_MESSAGE_FR = 'Ce nom est déjà utilisé.';
const NAME_ALREADY_USED_ERROR_MESSAGE_KEY = 'sqtm-core.error.generic.name-already-in-use';
export const NAME_ALREADY_USED_ERROR = new I18nError(NAME_ALREADY_USED_ERROR_MESSAGE_KEY, NAME_ALREADY_USED_ERROR_MESSAGE_FR);

const OPTION_NAME_ALREADY_EXISTS_ERROR_MESSAGE_FR = 'Le nom de l\'option existe déjà.';
const OPTION_NAME_ALREADY_EXISTS_ERROR_MESSAGE_KEY = 'sqtm-core.validation.errors.optionNameAlreadyExists';
export const OPTION_NAME_ALREADY_EXISTS_ERROR = new I18nError(OPTION_NAME_ALREADY_EXISTS_ERROR_MESSAGE_KEY, OPTION_NAME_ALREADY_EXISTS_ERROR_MESSAGE_FR);

const OPTION_CODE_ALREADY_EXISTS_ERROR_MESSAGE_FR = 'Le code de l\'option existe déjà.';
const OPTION_CODE_ALREADY_EXISTS_ERROR_MESSAGE_KEY = 'sqtm-core.validation.errors.optionCodeAlreadyExists';
export const OPTION_CODE_ALREADY_EXISTS_ERROR = new I18nError(OPTION_CODE_ALREADY_EXISTS_ERROR_MESSAGE_KEY, OPTION_CODE_ALREADY_EXISTS_ERROR_MESSAGE_FR);

import {I18nError} from './error.message';

export class TextFieldElement {

  constructor(protected readonly fieldName: string) {
  }

  get selector(): string {
    return `
    sqtm-core-text-field
    [data-test-field-name="${this.fieldName}"]`;
  }

  get inputSelector() {
    return `
    ${this.selector}
    input
    `;
  }

  fill(value: string) {
    cy.get(this.inputSelector).clear();

    if (value !== '') {
      cy.get(this.inputSelector).type(value);
    }
  }

  checkContent(expectedContent: string) {
    cy.get(this.inputSelector).should('have.value', expectedContent);
  }

  assertErrorExist(i18nKey: string) {
    const errorSelector = this.makeErrorSelector(i18nKey);
    cy.get(errorSelector).should('have.length', 1);
  }

  assertErrorContains(error: I18nError) {
    const errorSelector = this.makeErrorSelector(error.key);
    cy.get(errorSelector).should('contain.text', error.message);
  }

  private makeErrorSelector(i18nKey: string) {
    return `
    ${this.selector}
    .sqtm-core-error-message[data-test-error-key="${i18nKey}"]
    `;
  }

  assertNoErrorIsDisplayed() {
    const errorSelector = `
    ${this.selector}
    .sqtm-core-error-message
    `;
    cy.get(errorSelector).should('not.exist');
  }

  assertExist() {
    cy.get(this.selector).should('exist');
  }

  assertNotExist() {
    cy.get(this.selector).should('not.exist');
  }

  clearContent(): void {
    this.fill('');
  }
}

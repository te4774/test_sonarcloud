export class IconFieldElement {

  constructor(private readonly fieldName: string) {
  }

  get selector(): string {
    return `
    sqtm-core-icon-picker-field[data-test-field-name="${this.fieldName}"]`;
  }

  assertExists(): void {
    cy.get(this.selector).should('exist');
  }

  assertHasNoIcon(): void {
    cy.get(this.selector).should('contain.text', 'Aucune');
  }

  assertHasIcon(iconQualifiedName: string): void {
    cy.get(`${this.selector}`)
      .find('.anticon-' + iconQualifiedName)
      .should('exist');
  }

  selectIcon(iconQualifiedName: string): void {
    cy.get(`${this.selector}>div`).click();
    cy.get(this.iconPickerSelector)
      .should('exist')
      .find('.anticon-' + iconQualifiedName)
      .should('exist')
      .click();
  }

  private get iconPickerSelector(): string {
    return 'sqtm-core-icon-picker';
  }
}

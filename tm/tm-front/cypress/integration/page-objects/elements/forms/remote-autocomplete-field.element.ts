import {TextFieldElement} from './TextFieldElement';
import {HttpMock, HttpMockBuilder} from '../../../utils/mocks/request-mock';

export class RemoteAutocompleteFieldElement extends TextFieldElement {

  constructor(protected readonly fieldName: string) {
    super(fieldName);
  }

  get selector(): string {
    return `
    sqtm-app-remote-autocomplete-text-field
    [data-test-field-name="${this.fieldName}"]`;
  }

  fillWithAutocomplete(content: string, autocompleteResponse: any): void {
    const commandMock = autocompleteResponse ? this.mockCommand<any>(autocompleteResponse) : null;

    this.fill(content);

    if (commandMock) {
      commandMock.wait();
    }
  }

  assertHasAutocompleteChoices(choices: string[]): void {
    choices.forEach((choice) => {
      cy.contains('nz-auto-option', choice).should('exist');
    });
  }

  clickAutocompleteChoice(value: string): void {
    cy.contains('nz-auto-option', value)
      .should('exist')
      .click();
  }

  private mockCommand<T>(response: T): HttpMock<T> {
    return new HttpMockBuilder<T>('issues/*/command')
      .post()
      .responseBody(response)
      .build();
  }
}

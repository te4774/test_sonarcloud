import Chainable = Cypress.Chainable;
import {selectByDataTestComponentId, selectByDataTestFieldId} from '../../../utils/basic-selectors';

export type ElementSelectorFactory = () => Chainable<JQuery>;

/**
 * Common interface for form fields elements. It mainly encapsulates the HTML element query based on the selection strategy
 * passed to the constructor.
 *
 * Most of the time, you'll want to pass a string to the constructor so that it matches `[attr.data-test-field-id=MY_SELECTOR]`.
 * If you want to use another selector, pass a selector factory function instead.
 *
 * @see CommonSelectors
 */
export class AbstractFormFieldElement {

  public selectorFactory: ElementSelectorFactory;

  /**
   * @param selectorOrFieldId either an ElementSelectorFactory or a string. In the second case, the selector
   *        factory used is `CommonSelectors.fieldName`
   */
  constructor(selectorOrFieldId: ElementSelectorFactory | string) {
    this.selectorFactory = typeof selectorOrFieldId === 'string' ? CommonSelectors.fieldId(selectorOrFieldId)
      : selectorOrFieldId;
  }

  get selector(): Chainable<any> {
    return this.selectorFactory();
  }

  assertExist() {
    this.selector.should('exist');
  }

  assertNotExist() {
    this.selector.should('not.exist');
  }
}

/**
 * Helpers for commonly used form field element selectors.
 *
 * These functions are high-order functions which can lead to unusual syntax such as
 * `CommonSelectors.fieldName('password')().should('be.visible')` (note the empty parenthesis after the fieldName call).
 */
export const CommonSelectors = {
  fieldName: (fieldName: string) =>
    () => cy.get(`[data-test-field-name="${fieldName}"]`),

  customField: (fieldName: string) =>
    () => cy.get(`[data-test-customfield-name="${fieldName}"]`).siblings().first(),

  fieldId: (fieldId: string) =>
    () => cy.get(selectByDataTestFieldId(fieldId)),

  componentId: (componentId: string) =>
    () => cy.get(selectByDataTestComponentId(componentId)),

  childFieldName: (parent: ElementSelectorFactory, fieldId: string) =>
    () => parent().find(selectByDataTestFieldId(fieldId)),

  childComponentId: (parent: ElementSelectorFactory, componentId: string) =>
    () => parent().find(selectByDataTestComponentId(componentId)),
};


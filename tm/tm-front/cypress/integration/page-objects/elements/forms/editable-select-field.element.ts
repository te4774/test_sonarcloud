import Chainable = Cypress.Chainable;
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {AbstractFormFieldElement, ElementSelectorFactory} from './abstract-form-field.element';

export class EditableSelectFieldElement extends AbstractFormFieldElement {

  constructor(selectorOrFieldId: ElementSelectorFactory | string,
              private readonly url?: string) {
    super(selectorOrFieldId);
  }

  setAndConfirmValueNoButton(newValue: string) {
    this.selectValueNoButton(newValue);
    this.checkSelectedOption(newValue);
  }

  setAndConfirmValueWithServerResponse(newValue: string, serverResponse: any) {
    this.selectValue(newValue);
    this.confirmOptionWithServerResponse(serverResponse);
  }

  setAndConfirmValueWithServerResponseNoButton(newValue: string, serverResponse: any) {
    this.selectValueWithServerResponseNoButton(newValue, serverResponse);
  }

  // Activate edit mode, select an option and cancel
  setAndCancel(newValue?: string, oldValue?: string) {
    this.selectValue(newValue);
    this.cancelOption(oldValue);
  }

  confirm() {
    cy.get('[data-test-button-id="confirm"]').click();
  }

  assertIsEditable() {
    this.selector.find('div').should('have.class', 'editable');
  }

  assertIsNotEditable() {
    this.selector.find('div').should('not.have.class', 'editable');
  }

  // Show dropdown and check that the available options names match the specified string array
  checkAllOptions(options: string[]) {
    this.selector.find('span').click();
    cy.get(`nz-option-item`).each(function (value, index) {
      cy.wrap(value).should('contain.text', options[index]);
    });

    cy.clickVoid();
  }

  // Checks that the selected option (in non-edit mode) matches the specified text
  checkSelectedOption(expected: string) {
    this.selector.find('span').should('contain.text', expected);
  }

  // Returns the Chainable for an option of the dropdown menu
  // This won't work if the dropdown isn't shown.
  private getOption(optionName: string): Chainable<any> {
    return cy.contains(`nz-option-item`, optionName);
  }

  // Shows the dropdown and click on an option
  selectValue(newValue: string) {
    this.selector.find('span').click();
    this.getOption(newValue).click();
  }

  selectValueNoButton(newValue: string) {
    this.selector.find('span').click();
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().build();
      this.getOption(newValue).click();
      mock.wait();
    } else {
      this.getOption(newValue).click();
    }
  }

  selectValueWithServerResponseNoButton(newValue: string, response?: any) {
    this.selector.find('span').click();
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().responseBody(response).build();
      this.getOption(newValue).click();
      mock.wait();
    } else {
      this.getOption(newValue).click();
    }
  }

  private confirmOption(): void {
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().build();
      this.clickConfirmButton();
      mock.wait();
    } else {
      this.clickConfirmButton();
    }
  }

  private confirmOptionWithServerResponse(response?: any): void {
    if (this.url != null) {
      const mock = new HttpMockBuilder(this.url).post().responseBody(response).build();
      this.clickConfirmButton();
      mock.wait();
    } else {
      this.clickConfirmButton();
    }
  }

  // Click on cancel button and optionally check the result (selected option text)
  private cancelOption(oldValue?: string): void {
    cy.get('[data-test-button-id="cancel"]').click();

    if (oldValue != null) {
      this.checkSelectedOption(oldValue);
    }
  }

  private clickConfirmButton() {
    cy.get('[data-test-button-id="confirm"]').click();
  }
}

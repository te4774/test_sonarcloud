import {DuplicateActionWord} from '../../../model/test-case/duplicate-action-word.model';
import {TestStepActionWordOperationReport} from '../../../model/test-case/operation-reports.model';
import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {
  DuplicateActionWordDialogElement
} from '../../pages/test-case-workspace/dialogs/duplicate-action-word-dialog.element';
import {AbstractFormFieldElement, ElementSelectorFactory} from './abstract-form-field.element';

export class EditableActionTextFieldElement extends AbstractFormFieldElement {

  constructor(selectorOrFieldId: ElementSelectorFactory | string,
              private readonly url: string) {
    super(selectorOrFieldId);
  }

  checkContent(value: string): void {
    this.selector.find('div > div').should('contain.html', value);
  }

  setAndConfirm(value: string, isAutocompleteActive: boolean, response?: TestStepActionWordOperationReport) {
    this.setValue(value);
    this.confirm(isAutocompleteActive, response);
  }

  setAndCancel(value: string) {
    this.setValue(value);
    this.cancel();
  }

  setValue(value: string) {
    this.selector.find('div > div').click();
    this.selector.find('input').clear();
    this.selector.find('input').type(value);
  }

  confirm(isAutocompleteActive: boolean, response?: TestStepActionWordOperationReport): void {
    if (this.url) {
      const mockCheckDuplicate =
        new HttpMockBuilder('keyword-test-cases/duplicated-action')
          .post()
          .responseBody({})
          .build();
      const mockChangeAction =
        new HttpMockBuilder(this.url)
          .post()
          .responseBody(response)
          .build();
      this.selector.find('button').first().click();
      if (isAutocompleteActive) {
        mockCheckDuplicate.wait();
      }
      mockChangeAction.wait();
    } else {
      this.selector.find('button').first().click();
    }
    this.checkEditMode(false);
    if (response) {
      this.checkContent(response.styledAction);
    }
  }

  confirmWithActionWordProjectChoice(
    projectNameToSelect: string,
    response?: TestStepActionWordOperationReport,
    duplicateActionWords?: any) {

    const mockCheckDuplicate =
      new HttpMockBuilder('keyword-test-cases/duplicated-action')
        .post()
        .responseBody(duplicateActionWords)
        .build();
    this.selector.find('button').first().click();
    mockCheckDuplicate.wait();

    const mockChangeAction =
      new HttpMockBuilder(this.url)
        .post()
        .responseBody(response)
        .build();
      const duplicateActionWordDialog = new DuplicateActionWordDialogElement();
      duplicateActionWordDialog.assertExist();
      duplicateActionWordDialog.checkContent(
        this.convertDuplicateActionWords(duplicateActionWords));
      duplicateActionWordDialog.chooseProject(projectNameToSelect);
      duplicateActionWordDialog.confirm();
    mockChangeAction.wait();
  }

  private convertDuplicateActionWords(duplicateActionWords: any): DuplicateActionWord[] {
    return Object
      .keys(duplicateActionWords)
      .map(key => ({ projectName: key, actionWordId: duplicateActionWords[key] }));
  }

  cancel(): void {
    this.selector.find('button').last().click();
    this.checkEditMode(false);
  }

  checkEditMode(shouldBeInEditMode: boolean) {
    const chainer = shouldBeInEditMode ? 'exist' : 'not.exist';
    this.selector.find('button').should(chainer);
  }

  fillInput(action: string, response?: {actionList: string[]}) {
    const mock =
      new HttpMockBuilder('keyword-test-cases/autocomplete')
        .post()
        .responseBody(response)
        .build();
    this.setValue(action);
    mock.wait();
  }

  checkAutocompletionMenuIsVisible() {
    this.getAutocompleteMenu().should('exist');
  }

  getAutocompleteMenu() {
    return cy.get('div.autocomplete-menu');
  }

  checkAutocompletionOptions(options: string[]) {
    this.getOptions().then(autocompleteOptions => {
      cy.wrap(autocompleteOptions.length).should('equal', options.length);
      autocompleteOptions.each((index, element) => {
        cy.wrap(element).should('contain.text', options[index]);
      });
    });
  }

  getOptions() {
    return this.getAutocompleteMenu().find('sqtm-core-action-autocomplete-field-option');
  }

  chooseAutocompletionOptionWithClick(option: string) {
    this.getOptions().contains(option).click();
  }

  chooseAutocompletionOptionAtIndexWithKeyboard(index: number, option: string) {
    for (let i=0; i < index + 1; i++) {
      this.selector.find('input').type('{downarrow}');
    }
    this.selector.find('input').type('{downarrow}');
    this.selector.find('input').type('{uparrow}');
    this.selector.find('input').type('{enter}');
  }
}

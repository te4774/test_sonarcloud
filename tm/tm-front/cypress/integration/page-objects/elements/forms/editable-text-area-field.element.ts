import {HttpMockBuilder} from '../../../utils/mocks/request-mock';
import {AbstractFormFieldElement, ElementSelectorFactory} from './abstract-form-field.element';

export class EditableTextAreaFieldElement extends AbstractFormFieldElement {

  constructor(selectorOrFieldId: ElementSelectorFactory | string,
              private readonly url?: string) {
    super(selectorOrFieldId);
  }

  checkDisplayContent(value: string): void {
    this.selector.find('.field-value').should('contain.html', value);
  }

  checkEditContent(value: string): void {
    this.selector.find('textarea').should('have.value', value);
  }

  setAndCancel(value: string, oldValue?: string) {
    this.setValue(value);
    this.cancel(oldValue);
  }

  setAndConfirm(value: string) {
    this.setValue(value);
    this.confirm();
  }

  setValue(value: string) {
    this.selector.find('div > div').click();
    this.selector.find('textarea').clear();
    this.typeValue(value);
  }

  typeValue(value: string) {
    this.selector.find('textarea').type(value);
  }

  cancel(oldValue?: string) {
    this.selector.find('button').last().click();
    this.checkEditMode(false);
    if (oldValue) {
      this.checkDisplayContent(oldValue);
    }
  }

  checkEditMode(shouldBeInEditMode: boolean) {
    const chainer = shouldBeInEditMode ? 'exist' : 'not.exist';
    this.selector.find('button').should(chainer);
  }

  confirm(): void {
    if (this.url) {
      const mock = new HttpMockBuilder(this.url).post().build();
      this.selector.find('button').first().click();
      mock.wait();
    } else {
      this.selector.find('button').first().click();
    }
    this.checkEditMode(false);
  }
}

export function isBackEndMocked(): boolean {
  const envMocked = Cypress.env('backend');
  console.log(`envMocked : ${envMocked}`);
  return envMocked === 'mocked';
}

export function isPostgresql(): boolean {
  const profile = Cypress.env('profile');
  console.log(`is postgresql : ${profile}`);
  return profile === 'postgresql';
}

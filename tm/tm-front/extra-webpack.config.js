const TerserPlugin = require('terser-webpack-plugin')

// little hack to know if we are building in prod mode...
// i have not found any tag in process.env or any other way to spot the build mode from here...
const isProd = process.argv.indexOf('production') !== -1;

const prodPlugins = [
  new TerserPlugin({
    terserOptions: {
      compress: false,
    },
  }),
];

const devPlugins = [];

const plugins = isProd ? prodPlugins : devPlugins;

console.log(`Building sqtm-app in ${isProd ? 'production' : 'development'} mode. See angular.json and extra-webpack.config.js.`)

module.exports = {
  node:{
    global: true
  },
  plugins,
  resolve: {
    fallback: {
      "fs": false,
      "path": false,
      "assert": false,
      "stream": require.resolve("stream-browserify")
    },
  }
};

import {
  GridDefinitionBuilder,
  pagination,
  PaginationConfigBuilder,
  StyleDefinitionBuilder
} from '../../ui/grid/model/grid-definition.builder';
import {convertSqtmLiterals} from './data-row.type';
import {GridType} from '../../ui/grid/model/grid-definition.model';

export function grid(id: string) {
  return new GridDefinitionBuilder(id, GridType.GRID)
    .defaultPagination()
    .withStyle(new StyleDefinitionBuilder().showLines());
}

export function bigGrid(id: string) {
  return new GridDefinitionBuilder(id, GridType.GRID)
    .defaultPagination()
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withPagination(new PaginationConfigBuilder()
      .allowPageSizes(25, 50, 100, 500, 1000));
}

export function searchGrid(id: string) {
  return new GridDefinitionBuilder(id, GridType.GRID)
    .defaultPagination()
    .disableRightToolBar()
    .withRowHeight(35)
    .withPagination(new PaginationConfigBuilder().initialSize(50))
    .withStyle(new StyleDefinitionBuilder().showLines());
}

export function smallGrid(id: string) {
  return new GridDefinitionBuilder(id, GridType.GRID)
    .enableDynamicHeight()
    .disableRightToolBar()
    .withPagination(
      new PaginationConfigBuilder()
        .fixPaginationSize(10)
    );
}

// DEPRECATED
export function treeGrid(id: string) {
  return new GridDefinitionBuilder(id, GridType.TREE)
    .disableHeaders()
    .withRowConverter(convertSqtmLiterals)
    .withPagination(pagination().inactive());
}

export function tree(id: string) {
  return new GridDefinitionBuilder(id, GridType.TREE)
    .withRowHeight(30)
    .disableHeaders()
    .disableRightToolBar()
    .disableUnselectRowOnSimpleClick()
    .withStyle(
      new StyleDefinitionBuilder()
        .switchOffSelectedRows()
        .switchOffHoveredRows()
        .enableInitialLoadAnimation())
    .withRowConverter(convertSqtmLiterals)
    .withPagination(pagination().inactive())
    .enableDrag()
    .enableInternalDrop();
}

export function treePicker(id: string) {
  return new GridDefinitionBuilder(id, GridType.TREE)
    .withRowHeight(30)
    .disableHeaders()
    .disableRightToolBar()
    .disableUnselectRowOnSimpleClick()
    .withStyle(
      new StyleDefinitionBuilder()
        .switchOffSelectedRows()
        .switchOffHoveredRows()
        .enableInitialLoadAnimation())
    .withRowConverter(convertSqtmLiterals)
    .withPagination(pagination().inactive());
}

export function table(id: string) {
  return new GridDefinitionBuilder(id, GridType.TABLE)
    .withRowHeight(30)
    .disableRightToolBar()
    .withPagination(
      new PaginationConfigBuilder()
        .fixPaginationSize(10)
    );
}

import {ProjectPermission} from '../user/user.model';

export interface Team {
  id: number;
  name: string;
  description: string;
  createdOn: string;
  createdBy: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  projectPermissions: ProjectPermission[];
  members: Member[];
}

export interface Member {
  partyId: number;
  active: boolean;
  firstName: string;
  lastName: string;
  login: string;
  fullName: string;
}

import {RequirementCriticalityKeys} from '../level-enums/level-enum';

export class RequirementVersionCoverage {
  requirementVersionId: number;
  projectName: string;
  reference: string;
  name: string;
  criticality: RequirementCriticalityKeys;
  status: string;
  verifiedBy: string;
  stepIndex: number;
  directlyVerified: boolean;
  unDirectlyVerified: boolean;
  verifyingCalledTestCaseIds: number[];
  coverageStepInfos: CoverageTestStepInfo[];
  verifyingTestCaseId: number;
  milestoneLabels: string;
  milestoneMaxDate: string;
  milestoneMinDate: string;
}

export class CoverageTestStepInfo {
  id: number;
  index?: number;
}


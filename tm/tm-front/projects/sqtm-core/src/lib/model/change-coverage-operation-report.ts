import {RequirementVersionCoverage} from './test-case/requirement-version-coverage-model';
import {VerifyingTestCase} from './requirement/verifying-test-case';
import {RequirementVersionLink} from './requirement/requirement-version.link';
import {LinkedLowLevelRequirement} from './requirement/requirement-version.model';
import {RequirementVersionStatsBundle} from './requirement/requirement-version-stats-bundle.model';


export interface ChangeOperationReport {
  summary: SummaryExceptions;
}

export interface ChangeCoverageOperationReport extends ChangeOperationReport {
  coverages: RequirementVersionCoverage[];
}

export interface ChangeVerifyingTestCaseOperationReport extends ChangeOperationReport {
  verifyingTestCases: VerifyingTestCase[];
  requirementStats: RequirementVersionStatsBundle;
  nbIssues: number;
}

export interface ChangeLinkedRequirementOperationReport {
  summary: RequirementLinksSummaryExceptions;
  requirementVersionLinks: RequirementVersionLink[];
}


export interface BindRequirementToHighLevelRequirementOperationReport {
  linkedLowLevelRequirements: LinkedLowLevelRequirement[];
  verifyingTestCases: VerifyingTestCase[];
  requirementStats: RequirementVersionStatsBundle;
  nbIssues: number;
  summary: BindRequirementToHighLevelRequirementExceptions;
}

class BindRequirementToHighLevelRequirementExceptions {
  requirementWithNotLinkableStatus: string[];
  alreadyLinked: string [];
  alreadyLinkedToAnotherHighLevelRequirement: string[];
  highLevelRequirementsInSelection: string[];
  childRequirementsInSelection: string[];
}

export class SummaryExceptions {
  alreadyVerifiedRejections: any;
  notLinkableRejections: any;
  noVerifiableVersionRejections: any;
}

export class RequirementLinksSummaryExceptions {
  notLinkableRejections: boolean;
  alreadyLinkedRejections: boolean;
  sameRequirementRejections: boolean;
}

export class AutomatedTestTechnology {
  id: number;
  name: string;
  actionProviderKey: string;
  premium: boolean;
}

export function buildTechnologyLabel(technology: AutomatedTestTechnology): string {
  return technology.premium ? `${technology.name} (Premium)` : technology.name;
}

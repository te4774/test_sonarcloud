import {FilterValueModel} from '../../ui/filters/state/filter.state';
import {EntityReference} from '../entity.model';

export interface AutomatedSuitePreview {
  manualServerSelection: boolean;
  specification: AutomatedSuiteCreationSpecification;
  projects: TestAutomationProjectPreview[];
  squashAutomProjects: SquashAutomProjectPreview[];
}

export interface AutomatedSuiteCreationSpecification {
  context: EntityReference;
  testPlanSubsetIds: number[];
  filterValues?: FilterValueModel[];
  executionConfigurations?: AutomatedSuiteExecutionConfiguration[];
  squashAutomExecutionConfigurations?: SquashAutomExecutionConfiguration[];
}

export interface TestAutomationProjectPreview {
  projectId: number;
  label: string;
  server: string;
  nodes: string[];
  testCount: number;
}

export interface SquashAutomProjectPreview {
  projectId: number;
  projectName: string;
  serverId: number;
  serverName: string;
  testCases: TestCaseReferenceAndName[];
}

export interface AutomatedSuiteExecutionConfiguration {
  projectId: number;
  node: string;
}

export interface SquashAutomExecutionConfiguration {
  projectId: number;
  namespaces: string[];
  environmentTags: string[];
  environmentVariables: { [key: string]: string };
}

export interface TestCaseReferenceAndName {
  reference: string;
  name: string;
}

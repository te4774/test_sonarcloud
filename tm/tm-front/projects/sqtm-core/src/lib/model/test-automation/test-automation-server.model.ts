import {Credentials} from '../third-party-server/credentials.model';
import {I18nEnum} from '../level-enums/level-enum';
import {AuthenticationProtocol} from '../third-party-server/authentication.model';
import {BoundEnvironmentVariable} from '../environment-variable/environment-variable.model';

export class TestAutomationServer {
  id: number;
  baseUrl: string;
  kind: TestAutomationServerKind;
  name: string;
  description: string;
  createdBy: string;
  createdOn: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  manualSlaveSelection: boolean;
  authProtocol: AuthenticationProtocol;
  boundEnvironmentVariables: BoundEnvironmentVariable[];
  supportsAutomatedExecutionEnvironments: boolean;
  environmentTags: string[];
  availableEnvironments: AutomatedExecutionEnvironment[];
  availableEnvironmentTags: string[];
  observerUrl: string;
}

export class AdminTestAutomationServer extends TestAutomationServer {
  credentials: Credentials;
  supportedAuthenticationProtocols: AuthenticationProtocol[];
}

export enum TestAutomationServerKind {
  jenkins = 'jenkins',
  squashAutom = 'squashAutom',
}

export const TestAutomationServerKindI18nEnum: I18nEnum<TestAutomationServerKind> = {
  [TestAutomationServerKind.jenkins]: {
    id: TestAutomationServerKind.jenkins,
    i18nKey: 'sqtm-core.entity.execution-server.kind.jenkins',
  },
  [TestAutomationServerKind.squashAutom]: {
    id: TestAutomationServerKind.squashAutom,
    i18nKey: 'sqtm-core.entity.execution-server.kind.squashAutom',
  },
};

export function getTestAutomationServerKindI18nKey(kind: TestAutomationServerKind): string {
  if (Object.keys(TestAutomationServerKindI18nEnum).includes(kind)) {
    return TestAutomationServerKindI18nEnum[kind].i18nKey;
  }

  return kind;
}

export interface AutomatedExecutionEnvironment {
  name: string;
  namespaces: string[];
  tags: string[];
}

export enum AutomationEnvironmentTagHolder {
  PROJECT = 'PROJECT',
  TEST_AUTOMATION_SERVER = 'TEST_AUTOMATION_SERVER',
  EXECUTION_DIALOG = 'EXECUTION_DIALOG'
}

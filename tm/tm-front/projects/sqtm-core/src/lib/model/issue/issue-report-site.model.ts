import {TranslateService} from '@ngx-translate/core';

// Maps to KnownIssuesGridResponseBuilder.KnownIssueReportSite server-side
export interface IssueReportSite {
  executionId: number;
  executionOrder: number; // 0-based order
  executionName: string;
  suiteNames: string[];
}

export function getIssueReportSiteAsString(reportSite: IssueReportSite,
                                           translateService: TranslateService): string {
  const {
    executionOrder,
    executionName,
    suiteNames
  } = reportSite;

  const oneBasedOrder = executionOrder + 1;

  if (suiteNames.length > 0) {
    const testSuite = translateService.instant('sqtm-core.entity.test-suite.label.singular').toLowerCase();
    return `${executionName} (${testSuite} '${suiteNames}', exe. #${oneBasedOrder})`;
  } else {
    return `${executionName} (exe. #${oneBasedOrder})`;
  }
}

import {Milestone} from '../milestone/milestone.model';
import {TestPlanStatistics} from './test-plan-statistics';
import {EntityModel} from '../../core/services/entity-view/entity-view.state';
import {SimpleUser} from '../user/user.model';
import {NamedReference} from '../named-reference';
import {ConclusivenessKeys, ExecutionStatusKeys, TestCaseImportanceKeys} from '../level-enums/level-enum';
import {CustomDashboardModel} from '../custom-report/custom-dashboard.model';

export interface CampaignModel extends EntityModel {
  name: string;
  description: string;
  reference: string;
  campaignStatus: string;
  progressStatus: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  createdOn: string;
  createdBy: string;
  milestones: Milestone[];
  testPlanStatistics: TestPlanStatistics;
  actualStartDate: Date;
  actualEndDate: Date;
  actualStartAuto: boolean;
  actualEndAuto: boolean;
  scheduledStartDate: Date;
  scheduledEndDate: Date;
  nbIssues: number;
  hasDatasets: boolean;
  users: SimpleUser[];
  testSuites: NamedReference[];
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  campaignStatisticsBundle?: CampaignStatisticsBundle;
  dashboard?: CustomDashboardModel;
  nbTestPlanItems: number;
}

export interface IterationPlanning {
  id: number;
  name: string;
  scheduledStartDate: Date;
  scheduledEndDate: Date;
}

export interface ScheduledIteration {
  id: number;
  name: string;
  testplanCount: number;
  scheduledStart: string;
  scheduledEnd: string;
  cumulativeTestsByDate: [string, number][];
}

export interface CampaignProgressionStatistics {
  cumulativeExecutionsPerDate: [string, number][];
  scheduledIterations: ScheduledIteration[];
  errors: string[];
}

export type ExecutionStatusCount = Partial<{ [K in ExecutionStatusKeys]: number }>;

export type ConclusivenessStatusCount = { [K in ConclusivenessKeys]: number };

export type TestCaseImportanceCount = { [K in TestCaseImportanceKeys]: number };

export interface IterationTestInventoryStatistics {
  iterationName: string;
  statistics: ExecutionStatusCount;

}

export interface CampaignStatisticsBundle {
  campaignProgressionStatistics: CampaignProgressionStatistics;
  campaignTestCaseStatusStatistics: ExecutionStatusCount;
  campaignTestCaseSuccessRateStatistics: { conclusiveness: { [K in TestCaseImportanceKeys]: ConclusivenessStatusCount } };
  campaignNonExecutedTestCaseImportanceStatistics: TestCaseImportanceCount;
  iterationTestInventoryStatistics: IterationTestInventoryStatistics[];
}

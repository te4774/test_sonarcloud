import {Attachment, AttachmentModel} from './attachment.model';

export interface AttachmentListModel {
  id: number;
  attachments: AttachmentModel[];
}

export interface AttachmentList {
  id: number;
  attachments: Attachment[];
}

export function initialAttachmentList(): Readonly<AttachmentList> {
  return {
    id: null,
    attachments: []
  };
}

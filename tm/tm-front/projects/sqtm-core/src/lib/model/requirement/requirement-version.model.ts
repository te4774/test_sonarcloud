import {Milestone} from '../milestone/milestone.model';
import {RequirementCriticalityKeys, RequirementStatusKeys} from '../level-enums/level-enum';
import {VerifyingTestCase} from './verifying-test-case';
import {RequirementVersionLink} from './requirement-version.link';
import {RequirementVersionStatsBundle} from './requirement-version-stats-bundle.model';
import {EntityModel} from '../../core/services/entity-view/entity-view.state';

export interface RequirementVersionModel extends EntityModel {
  name: string;
  reference: string;
  versionNumber: number;
  category: number;
  criticality: RequirementCriticalityKeys;
  status: RequirementStatusKeys;
  createdBy: string;
  createdOn: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  milestones: Milestone[];
  description: string;
  requirementId: number;
  hasExtender: boolean;
  bindableMilestones: Milestone[];
  verifyingTestCases: VerifyingTestCase[];
  requirementVersionLinks: RequirementVersionLink[];
  lowLevelRequirements?: LinkedLowLevelRequirement[];
  requirementStats: RequirementVersionStatsBundle;
  linkedHighLevelRequirement?: LinkedHighLevelRequirement;
  nbIssues: number;
  remoteReqUrl: string;
  remoteReqId: string;
  syncStatus: string;
  remoteReqPerimeterStatus: RemoteRequirementPerimeterStatus;
  highLevelRequirement: boolean;
  childOfRequirement: boolean;
}

/**
 * This interface describe a high level req linked into a low level req
 */
export interface LinkedHighLevelRequirement {
  requirementId: number;
  requirementVersionId: number;
  name: string;
  reference: string;
  path: string;
  projectName: string;
}

/**
 * This interface describe a low level req inside a high level req model.
 * It is the equivalent of RequirementVersionLink inside a HighLevelReq
 */
export interface LinkedLowLevelRequirement {
  requirementId: number;
  requirementVersionId: number;
  name: string;
  reference: string;
  childOfRequirement: boolean;
  projectName: string;
  milestoneLabels: string;
  milestoneMinDate: Date;
  milestoneMaxDate: Date;
  versionNumber: number;
  criticality: RequirementCriticalityKeys;
  requirementStatus: RequirementStatusKeys;
}

export type RemoteRequirementPerimeterStatus =
  'UNKNOWN' |
  'IN_CURRENT_PERIMETER' |
  'OUT_OF_CURRENT_PERIMETER'|
  'NOT_FOUND';


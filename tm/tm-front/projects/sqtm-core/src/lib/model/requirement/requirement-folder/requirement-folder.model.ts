import {RequirementStatistics} from '../requirement-statistics.model';
import {EntityModel} from '../../../core/services/entity-view/entity-view.state';
import {CustomDashboardModel} from '../../custom-report/custom-dashboard.model';

export interface RequirementFolderModel extends EntityModel {
  name: string;
  description: string;
  statistics: RequirementStatistics;
  dashboard: CustomDashboardModel;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  extendHighLvlReqScope: boolean;
}

export interface RequirementVersionLinkPatch {
  requirementNodesIds: number[];
  reqVersionLinkTypeId: number;
  reqVersionLinkTypeDirection: boolean;
  isRelatedIdANodeId?: boolean;
}

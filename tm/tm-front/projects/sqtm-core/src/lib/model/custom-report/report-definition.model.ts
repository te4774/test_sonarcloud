import {BindableEntity} from '../bindable-entity.model';

export enum ReportInputType {
  TEXT = 'TEXT',
  PASSWORD = 'PASSWORD',
  LABEL = 'LABEL',
  DATE = 'DATE',
  DROPDOWN_LIST = 'DROPDOWN_LIST',
  RADIO_BUTTONS_GROUP = 'RADIO_BUTTONS_GROUP',
  CHECKBOX = 'CHECKBOX',
  CHECKBOXES_GROUP = 'CHECKBOXES_GROUP',
  TREE_PICKER = 'TREE_PICKER',
  PROJECT_PICKER = 'PROJECT_PICKER',
  MILESTONE_PICKER = 'MILESTONE_PICKER',
  TAG_PICKER = 'TAG_PICKER',
  INPUTS_GROUP = 'INPUTS_GROUP'
}

export enum NodeType {
  CAMPAIGN = 'CAMPAIGN',
  TEST_CASE = 'TEST_CASE',
  REQUIREMENT = 'REQUIREMENT'
}

export interface ReportInput {
  name: string;
  label: string;
  type: ReportInputType;
  required: boolean;
  helpMessage?: string;
  jsTreeNodeType?: string;
}

export interface ReportOption {
  name: string;
  label: string;
  value: string;
  defaultSelected: boolean;
  givesAccessTo: string;
  composite: boolean;
  contentType: ReportInputType;
  bindableEntity?: BindableEntity;
  nodeType?: NodeType;
  disabledBy: string;
}

export function isReportOptionGroup(input: ReportInput): boolean {
  return [ReportInputType.CHECKBOXES_GROUP, ReportInputType.RADIO_BUTTONS_GROUP, ReportInputType.DROPDOWN_LIST].includes(input.type);
}

export interface ReportOptionGroup extends ReportInput {
  type: ReportInputType.CHECKBOXES_GROUP | ReportInputType.RADIO_BUTTONS_GROUP | ReportInputType.DROPDOWN_LIST;
  options: ReportOption[];
}

export interface ReportInputsGroup extends ReportInput {
  type: ReportInputType.INPUTS_GROUP;
  inputs: ReportInput[];
}

export interface ReportDefinitionModel {
  id: number;
  name: string;
  description: string;
  summary: string;
  label?: string;
  pluginNamespace: string;
  parameters: any;
  createdOn?: Date;
  createdBy?: string;
  lastModifiedOn?: Date;
  lastModifiedBy?: string;
  missingPlugin?: boolean;
}

export interface ReportView {
  label: string;
  formats: string[];
  reportFileName: string;
}

export interface Report {
  id: string;
  label: string;
  description: string;
  isDocxTemplate: boolean;
  isDirectDownloadableReport: boolean;
  category: StandardReportCategory;
  inputs: ReportInput[];
  views: ReportView[];
}

export interface UnInstallReport {
  id: string;
  labelKey: string;
  messageKey: string;
}

export interface ReportWorkbenchData {
  availableReports: Report[];
  reportDefinition: ReportDefinitionModel;
  projectId: number;
  containerId?: number;
}

export enum StandardReportCategory {
  EXECUTION_PHASE = 'EXECUTION_PHASE',
  PREPARATION_PHASE = 'PREPARATION_PHASE',
  VARIOUS = 'VARIOUS'
}

export interface ReportDefinitionViewModel extends ReportDefinitionModel {
  customReportLibraryNodeId: number;
  projectId: number;
  attributes: { [K in string]: string[] };
  reportLabel?: string;
  missingPlugin: boolean;
  report?: Report;
}

export interface ReportCheckboxInput extends ReportInput {
  type: ReportInputType.CHECKBOX;
  defaultSelected: boolean;
}

export interface ReportTreePickerInput extends ReportInput {
  type: ReportInputType.TREE_PICKER;
  jsTreeNodeType: 'campaign' | 'iteration';
}

import {EntityReference, EntityType} from '../entity.model';
import * as _ from 'lodash';

export enum ChartType {
  PIE = 'PIE',
  BAR = 'BAR',
  CUMULATIVE = 'CUMULATIVE',
  TREND = 'TREND',
  COMPARATIVE = 'COMPARATIVE'
}

export enum ChartOperation {
  NONE = 'NONE',
  GREATER = 'GREATER',
  GREATER_EQUAL = 'GREATER_EQUAL',
  LOWER = 'LOWER',
  LOWER_EQUAL = 'LOWER_EQUAL',
  BETWEEN = 'BETWEEN',
  EQUALS = 'EQUALS',
  IN = 'IN',
  LIKE = 'LIKE',
  MATCHES = 'MATCHES',
  MIN = 'MIN',
  MAX = 'MAX',
  AVG = 'AVG',
  SUM = 'SUM',
  COUNT = 'COUNT',
  IS_NULL = 'IS_NULL',
  NOT_NULL = 'NOT_NULL',
  NOT_EQUALS = 'NOT_EQUALS',
  BY_DAY = 'BY_DAY',
  BY_WEEK = 'BY_WEEK',
  BY_MONTH = 'BY_MONTH',
  BY_YEAR = 'BY_YEAR',
  FULLTEXT = 'FULLTEXT',
}

export enum ChartColumnType {
  ENTITY = 'ENTITY',
  CUF = 'CUF',
  ATTRIBUTE = 'ATTRIBUTE',
  CALCULATED = 'CALCULATED',
}

export enum ChartScopeType {
  // The perimeter will be the current project of the chart if user look just the chart or
  // the dashboard's project id the chart is looked into a dashboard
  DEFAULT = 'DEFAULT',
  // The perimeter will be a fix selection of project
  PROJECTS = 'PROJECTS',
  // The perimeter is a custom selection of entities.
  CUSTOM = 'CUSTOM'
}

export enum ChartDataType {

  // @formatter:off
  NUMERIC = 'NUMERIC',
  STRING = 'STRING',
  DATE = 'DATE',
  DATE_AS_STRING = 'DATE_AS_STRING',
  EXISTENCE = 'EXISTENCE',
  BOOLEAN = 'BOOLEAN',
  BOOLEAN_AS_STRING = 'BOOLEAN_AS_STRING',
  LEVEL_ENUM = 'LEVEL_ENUM',
  REQUIREMENT_STATUS = 'REQUIREMENT_STATUS',
  EXECUTION_STATUS = 'EXECUTION_STATUS',
  LIST = 'LIST',
  INFO_LIST_ITEM = 'INFO_LIST_ITEM',
  TAG = 'TAG',
  ENUM = 'ENUM',
  AUTOMATED_TEST_TECHNOLOGY = 'AUTOMATED_TEST_TECHNOLOGY',

  // type ENTITY means that columns of that datatype represent the entity itself rather than one of its attributes.
  ENTITY = 'ENTITY',
  // @formatter:on

  TEXT = 'TEXT'
}

const chartOperationByDataType: { [P in ChartDataType]: ChartOperation[] } = {
  NUMERIC: [
    ChartOperation.AVG,
    ChartOperation.BETWEEN,
    ChartOperation.COUNT,
    ChartOperation.EQUALS,
    ChartOperation.GREATER,
    ChartOperation.GREATER_EQUAL,
    ChartOperation.LOWER,
    ChartOperation.LOWER_EQUAL,
    ChartOperation.MAX,
    ChartOperation.MIN,
    ChartOperation.SUM,
    ChartOperation.NONE,
    ChartOperation.NOT_EQUALS],
  STRING: [ChartOperation.EQUALS, ChartOperation.LIKE, ChartOperation.COUNT, ChartOperation.NONE],
  DATE: [
    ChartOperation.BETWEEN,
    ChartOperation.COUNT,
    ChartOperation.EQUALS,
    ChartOperation.GREATER,
    ChartOperation.GREATER_EQUAL,
    ChartOperation.LOWER,
    ChartOperation.LOWER_EQUAL,
    ChartOperation.BY_DAY,
    ChartOperation.BY_WEEK,
    ChartOperation.BY_MONTH,
    ChartOperation.BY_YEAR,
    ChartOperation.NOT_EQUALS],
  DATE_AS_STRING: [
    ChartOperation.BETWEEN,
    ChartOperation.COUNT,
    ChartOperation.EQUALS,
    ChartOperation.GREATER,
    ChartOperation.GREATER_EQUAL,
    ChartOperation.LOWER,
    ChartOperation.LOWER_EQUAL,
    ChartOperation.BY_DAY,
    ChartOperation.BY_MONTH,
    ChartOperation.BY_YEAR,
    ChartOperation.NOT_EQUALS],
  EXISTENCE: [ChartOperation.NOT_NULL, ChartOperation.IS_NULL],
  BOOLEAN: [ChartOperation.EQUALS, ChartOperation.COUNT, ChartOperation.NONE],
  BOOLEAN_AS_STRING: [ChartOperation.EQUALS, ChartOperation.COUNT, ChartOperation.NONE],
  LEVEL_ENUM: [ChartOperation.EQUALS, ChartOperation.IN, ChartOperation.COUNT, ChartOperation.NONE],
  REQUIREMENT_STATUS: [ChartOperation.EQUALS, ChartOperation.IN, ChartOperation.COUNT, ChartOperation.NONE],
  EXECUTION_STATUS: [ChartOperation.EQUALS, ChartOperation.IN, ChartOperation.COUNT, ChartOperation.NONE],
  LIST: [ChartOperation.EQUALS, ChartOperation.IN, ChartOperation.COUNT, ChartOperation.NONE],
  INFO_LIST_ITEM: [ChartOperation.EQUALS, ChartOperation.IN, ChartOperation.COUNT, ChartOperation.NONE],
  TAG: [ChartOperation.EQUALS, ChartOperation.IN, ChartOperation.COUNT, ChartOperation.NONE],
  ENUM: [ChartOperation.EQUALS, ChartOperation.IN, ChartOperation.COUNT, ChartOperation.NONE],
  ENTITY: [ChartOperation.EQUALS, ChartOperation.IN, ChartOperation.COUNT, ChartOperation.NONE],
  TEXT: [ChartOperation.LIKE, ChartOperation.FULLTEXT, ChartOperation.NONE],
  AUTOMATED_TEST_TECHNOLOGY: [ChartOperation.EQUALS, ChartOperation.IN, ChartOperation.COUNT, ChartOperation.NONE]
};

export enum ChartColumnRole {
  AXIS = 'AXIS',
  MEASURE = 'MEASURE',
  FILTER = 'FILTER'
}

const chartOperationByColumnRole: { [P in ChartColumnRole]: ChartOperation[] } = {
  AXIS: [ChartOperation.BY_DAY, ChartOperation.BY_WEEK, ChartOperation.BY_MONTH, ChartOperation.BY_YEAR, ChartOperation.NONE],
  MEASURE: [ChartOperation.COUNT, ChartOperation.AVG, ChartOperation.MIN, ChartOperation.MAX, ChartOperation.SUM],
  FILTER: [
    ChartOperation.LIKE,
    ChartOperation.IN,
    ChartOperation.GREATER,
    ChartOperation.GREATER_EQUAL,
    ChartOperation.LOWER,
    ChartOperation.LOWER_EQUAL,
    ChartOperation.BETWEEN,
    ChartOperation.EQUALS,
    ChartOperation.NOT_EQUALS
  ]
};

export function filterAvailableChartOperations(role: ChartColumnRole, dataType: ChartDataType): ChartOperation[] {
  return _.intersection<ChartOperation>(chartOperationByColumnRole[role], chartOperationByDataType[dataType]);
}

export interface SpecializedEntityType {
  entityType: EntityType;
}

export interface ChartColumnPrototype {
  id: number;
  columnType: ChartColumnType;
  label: string;
  specializedType: SpecializedEntityType;
  dataType: ChartDataType;
}


export interface MeasureColumn {
  cufId?: number;
  label: string;
  column: ChartColumnPrototype;
  operation: ChartOperation;
}

// for now AxisColumn seems to be like Measure columns.
export type AxisColumn = MeasureColumn;

export interface ChartFilter {
  id: number;
  cufId?: number;
  values: string[];
  column: ChartColumnPrototype;
  operation: ChartOperation;
}

export interface ChartDefinitionModel {
  id: number;
  name: string;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  customReportLibraryNodeId: number;
  projectId: number;
  type: ChartType;
  measures: MeasureColumn[];
  axis: AxisColumn[];
  filters: ChartFilter[];
  abscissa: any[][];
  scopeType: ChartScopeType;
  scope: EntityReference[];
  // ids of projects in perimeter. Don't know why it's string type, however it's like that...
  projectScope: string[];
  series: { [K in string]: number[] };
  colours: string[];
}

export type ChartColumnPrototypesByEntityType = { [K in CustomChartEntityType]: ChartColumnPrototype[] };

export interface ChartBuildingBlocks {
  columnPrototypes: ChartColumnPrototypesByEntityType;
  columnRoles: { [K in string]: any[] };
  dataTypes: { [K in ChartDataType]: ChartOperation[] };
}

export interface ChartWorkbenchData {
  chartBuildingBlocks: ChartBuildingBlocks;
  chartDefinition: ChartDefinitionModel;
  projectId: number;
  containerId: number;
}

export enum CustomChartEntityType {
  REQUIREMENT = 'REQUIREMENT',
  REQUIREMENT_VERSION = 'REQUIREMENT_VERSION',
  TEST_CASE = 'TEST_CASE',
  CAMPAIGN = 'CAMPAIGN',
  ITERATION = 'ITERATION',
  ITEM_TEST_PLAN = 'ITEM_TEST_PLAN',
  EXECUTION = 'EXECUTION'
}

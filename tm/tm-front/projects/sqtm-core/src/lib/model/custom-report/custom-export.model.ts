import {CustomExportEntityType} from './custom-export-columns.model';

/**
 * Response shape when fetching an existing CustomReportCustomExport.
 * Maps to CustomReportCustomExportDto server-side.
 */
export interface CustomExportModel {
  id: number;
  customReportLibraryNodeId: number;
  projectId: number;
  name: string;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  scopeNodeId: number;
  scopeNodeName: string;
  scopeProjectId: number;
  columns: CustomExportColumn[];
}

/**
 * Maps to CustomReportCustomExportColumnDto server-side.
 */
export interface CustomExportColumn {
  entityType: CustomExportEntityType;
  columnName: string;
  cufId: number;
}

/**
 * Payload used when creating a new custom export. Maps to CustomExportController#NewCustomExport server-side.
 */
export interface NewCustomExportModel {
  /**
   * Name for the new export.
   */
  name: string;

  /**
   * Node ID for the export's scope (e.g. Campaign-3)
   */
  scope: string;

  /**
   * Serializable attributes for the export.
   */
  columns: NewCustomExportColumn[];
}

export interface NewCustomExportColumn {
  /**
   * This should correspond to the enumerated CustomExportColumnNames server-side (e.g. TEST_CASE_CUF).
   */
  label: string;

  /**
   * The ID of the corresponding custom field, when this column does correspond to a custom field.
   */
  cufId: number;
}

/**
 * Initial payload for the workbench page.
 */
export interface CustomExportWorkbenchData {
  /**
   * The project ID of the CRLN we want to add a new report to.
   */
  projectId: number;

  /**
   * The node ID of the CRLN we want to add a new report to.
   */
  containerId: number;

  /**
   * Custom export to edit when any.
   */
  customExport: CustomExportModel;
}

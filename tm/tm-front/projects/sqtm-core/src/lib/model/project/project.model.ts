import {Option} from '../option.model';
import {ProjectPermissions} from '../permissions/permissions.model';
import {BugTrackerBinding} from '../bugtracker/bug-tracker.binding';
import {MilestoneBinding} from '../milestone/milestone-binding.model';
import {Bindings} from '../bindable-entity.model';
import {BddImplementationTechnologyKeys, BddScriptLanguageKeys, ExecutionStatusKeys} from '../level-enums/level-enum';
import {AdminEntityModel} from '../../core/services/genric-entity-view/generic-entity-view-state';
import {AttachmentListModel} from '../attachment/attachment-list.model';
import {BugTracker} from '../bugtracker/bug-tracker.model';
import {InfoList} from '../infolist/infolist.model';
import {ScmServer} from '../scm-server/scm-server.model';
import {TestAutomationServer} from '../test-automation/test-automation-server.model';
import {TestAutomationProject} from '../test-automation/test-automation-project.model';
import {MilestoneAdminProjectView} from '../milestone/milestone-admin-project-view.model';
import {ProjectPlugin} from '../plugin/project-plugin';
import {ActivatedPluginsByWorkspace} from './project-data.model';

export class Project implements AdminEntityModel, InfoListInProject {
  id: number;
  uri: string;
  name: string;
  label: string;
  testCaseNatureId: number;
  testCaseTypeId: number;
  requirementCategoryId: number;
  allowAutomationWorkflow: boolean;
  customFieldBindings: Bindings;
  permissions: ProjectPermissions;
  bugTrackerBinding: BugTrackerBinding;
  milestoneBindings: MilestoneBinding[];
  taServerId: number;
  automationWorkflowType: string;
  disabledExecutionStatus: ExecutionStatusKeys[];
  attachmentList: AttachmentListModel;
  createdOn: string;
  createdBy: string;
  lastModifiedOn: string;
  lastModifiedBy: string;
  linkedTemplate: string;
  linkedTemplateId: number;
  template: boolean;
  description: string;
  bddScriptLanguage: string;
  keywords: Option[];
  allowTcModifDuringExec: boolean;
  activatedPlugins: ActivatedPluginsByWorkspace;
  automatedSuitesLifetime: number;
  hasTemplateConfigurablePluginBinding: boolean;
}

export class ProjectView extends Project {
  availableBugtrackers: BugTracker[];
  bugtrackerProjectNames: string[];
  hasData: boolean;
  allowedStatuses: AllowedStatusesModel;
  statusesInUse: StatusesInUseModel;
  useTreeStructureInScmRepo: boolean;
  scmRepositoryId?: number;
  infoLists: InfoList[];
  templateLinkedToProjects: boolean;
  partyProjectPermissions: PartyProjectPermission[];
  availableScmServers: ScmServer[];
  availableTestAutomationServers: TestAutomationServer[];
  boundMilestonesInformation: MilestoneAdminProjectView[];
  boundTestAutomationProjects: TestAutomationProject[];
  availablePlugins: ProjectPlugin[];
  bddScriptLanguage: BddScriptLanguageKeys;
  bddImplementationTechnology: BddImplementationTechnologyKeys;
}

export interface AllowedStatusesModel {
  UNTESTABLE: boolean;
  SETTLED: boolean;
}

export interface StatusesInUseModel {
  UNTESTABLE: boolean;
  SETTLED: boolean;
}

export interface PartyProjectPermission {
  projectId: number;
  partyName: string;
  partyId: number;
  active: boolean;
  permissionGroup: PermissionGroup;
  team: boolean;
}

export interface PermissionGroup {
  id: number;
  qualifiedName: string;
  simpleName: string;
}

export type InfoListAttribute = 'testCaseNatureId' | 'testCaseTypeId' | 'requirementCategoryId';

export type InfoListInProject = { [K in InfoListAttribute]: number };

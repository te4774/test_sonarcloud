export class ProjectFilter {
  id: number;
  name: string;
  label: string;
  activated: boolean;
}

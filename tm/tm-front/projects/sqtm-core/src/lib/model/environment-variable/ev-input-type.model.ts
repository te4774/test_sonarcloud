export enum EvInputType {
  PLAIN_TEXT = 'PLAIN_TEXT',
  DROPDOWN_LIST = 'DROPDOWN_LIST'
}

export function getEvInputTypeI18n(inputTypeKey: EvInputType): string {
  return 'sqtm-core.entity.environment-variable.' + inputTypeKey;
}

export class EnvironmentVariableOption {
  evId: number;
  label: string;
  code: string;
  position: number;
}

export function isEVOptionCodePatternValid(code: string) {
  return ((!code.match(/\s+/)) && code
    .match(/^[A-Za-z0-9_]*$/));
}

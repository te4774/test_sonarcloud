import {compareLevelEumItems, TestCaseWeight} from './level-enum';

describe('LevelEnum', () => {
  it('should sort two items', () => {
    const sortedIds = Object.values(TestCaseWeight)
      .sort(compareLevelEumItems)
      .map(w => w.id);

    expect(sortedIds).toEqual(['VERY_HIGH', 'HIGH', 'MEDIUM', 'LOW']);
  });
});

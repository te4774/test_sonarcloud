import {validateNamespace} from './logger.utils';

describe('LoggerUtils', () => {

  describe('Reject Invalid Namespace', () => {
    interface DataType {
      namespace: string;
    }

    const dataSets: DataType[] = [
      {namespace: 'invli@d'},
      {namespace: ''},
      {namespace: '..'},
      {namespace: 'invalid.'},
      {namespace: ' invalid '},
      {namespace: 'inv lid'},
    ];
    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - It should reject invalid ns ${data.namespace}`, () => {
        const valid = validateNamespace(data.namespace);
        expect(valid).toBeFalsy();
      });
    }
  });

});

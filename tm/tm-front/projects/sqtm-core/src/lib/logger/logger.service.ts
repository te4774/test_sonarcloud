import {
  getLoggingLevel,
  isUnset,
  LoggerConfiguration,
  LoggingConfiguration,
  LoggingLevel,
  LoggingLevelKey,
  NamespaceConfiguration,
  WarningLoggingLevel
} from './logger.configuration';
import {NamespaceTree} from './namespace-tree';
import {validateNamespace} from './logger.utils';

let LOGGER_SERVICE;

export class LoggerService {

  private namespaceTree = new NamespaceTree();

  private alwaysShowTrace = false;

  private defaultLoggingLevel: LoggingLevel = new WarningLoggingLevel();

  private constructor() {
  }

  public static getLoggerService(): LoggerService {
    if (!LOGGER_SERVICE) {
      console.log('INSTANTIATE SINGLETON LOG SERVICE');
      LOGGER_SERVICE = new LoggerService();
    }
    return LOGGER_SERVICE;
  }

  loadConfiguration(configuration: LoggingConfiguration): void {
    this.loadLoggerConfiguration(configuration.loggers);
    this.loadTraceConfiguration(configuration.alwaysShowTrace);
  }

  private loadLoggerConfiguration(configuration: LoggerConfiguration) {
    if (configuration.default) {
      this.defaultLoggingLevel = getLoggingLevel(configuration.default);
    }
    if (configuration.namespaces) {
      Object.entries(configuration.namespaces).forEach(([namespace, loggingLevelKey]: [string, LoggingLevelKey]) => {
        this.addNamespace(namespace, loggingLevelKey);
      });
    }
  }

  addNamespace(namespace: string, loggingLevelKey: LoggingLevelKey) {
    const nsConfiguration: NamespaceConfiguration = {level: getLoggingLevel(loggingLevelKey)};
    const valid = validateNamespace(namespace);
    if (valid) {
      this.namespaceTree.addPath(namespace, nsConfiguration);
    } else {
      throw Error(`Only A-Za-z0-9_- are allowed in namespaces. No space allowed. Invalid char in : ${namespace}`);
    }
  }

  isEnabled(namespace: string, requiredLevel: LoggingLevel) {
    const loggingLevel = this.getLoggingLevel(namespace);
    return requiredLevel.level <= loggingLevel.level;
  }

  log(message: string, namespace: string, requiredLevel: LoggingLevel, objects?: Object[]) {
    const loggingLevel = this.getLoggingLevel(namespace);
    if (requiredLevel.level <= loggingLevel.level) {
      this.doLog(namespace, requiredLevel, message, objects);
    }
  }

  private doLog(namespace: string, requiredLevel: LoggingLevel, message: string | Object, objects: Object[]) {
    const formattedNameSpace = this.formatNameSpace(namespace);
    const str = `${requiredLevel.name.toLocaleUpperCase().padEnd(6)} ${formattedNameSpace} : ${message}`;
    switch (requiredLevel.name) {
      case 'error':
        console.error(str);
        break;
      case 'warn':
        console.warn(str);
        break;
      case 'info':
        // tslint:disable-next-line:no-console
        console.info(str);
        break;
      case 'debug':
        console.log(str);
        break;
      case 'trace':
        console.log(str);
        break;
    }
    if (objects && objects.length > 0) {
      this.logObjects(objects);
    }
    if (this.alwaysShowTrace) {
      // tslint:disable-next-line:no-console
      console.trace();
    }
  }

  private logObjects(objects: Object[]) {
    objects.forEach(object => {
      console.log(object);
    });
  }

  private getLoggingLevel(namespace: string) {
    let loggingLevel = this.namespaceTree.getLoggingLevel(namespace);
    if (isUnset(loggingLevel)) {
      loggingLevel = this.defaultLoggingLevel;
    }
    return loggingLevel;
  }

  private formatNameSpace(namespace: string): string {
    const split = namespace.split('.');
    let formatted = '';
    while (split.length > 1) {
      const ns = split.shift();
      formatted = formatted.concat(ns.substring(0, 1));
      formatted = formatted.concat('.');
    }
    formatted = formatted.concat(split.shift());
    return formatted;
  }

  private loadTraceConfiguration(alwaysShowTrace: boolean) {
    this.alwaysShowTrace = alwaysShowTrace;
  }
}

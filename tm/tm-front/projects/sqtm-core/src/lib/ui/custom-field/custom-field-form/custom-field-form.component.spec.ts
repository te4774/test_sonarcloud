import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CustomFieldFormComponent} from './custom-field-form.component';
import {FormGroup, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {ComponentTester} from 'ngx-speculoos';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzDatePickerModule} from 'ng-zorro-antd/date-picker';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzInputNumberModule} from 'ng-zorro-antd/input-number';
import {NzPopoverModule} from 'ng-zorro-antd/popover';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzTagModule} from 'ng-zorro-antd/tag';
import {CommonModule} from '@angular/common';
import {CKEditorModule} from 'ckeditor4-angular';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {CustomField} from '../../../model/customfield/customfield.model';
import {InputType} from '../../../model/customfield/input-type.model';
import {WorkspaceCommonModule} from '../../workspace-common/workspace-common.module';
import {generateRandomString} from '../../testing-utils/test-data-generator';

class CustomFieldFormComponentTester extends ComponentTester<CustomFieldFormComponent> {

  getLabel(cufId: number) {
    return this.element(`[data-test-cuf-label-id="${cufId}"]`).nativeElement.textContent;
  }

  getTextInputValue(cufId: number) {
    return this.input(`[data-test-cuf-text-input-id="${cufId}"]`).value;
  }

  setInputValue(customField: CustomField, value: string) {
    this.input(`[data-test-cuf-text-input-id="${customField.id}"]`).fillWith(value);
  }

  getNumericInputValue(cufId: number) {
    const inputElement = this.input(`[data-test-cuf-numeric-input-id="${cufId}"] .ant-input-number-input`);
    // cannot get the value directly from DOM it seems that ng-zorro lag a bit and make test crash
    return inputElement.attr('ng-reflect-model');
  }

  getDropdownValue(cufId: number) {
    return this.element(`[data-test-cuf-dropdown-input-id="${cufId}"]  nz-select-item`).textContent;
  }

  getTagInputValue(cufId: number) {
    const elements = this.elements(`[data-test-cuf-tag-input-id="${cufId}"] nz-select-item`);
    // cannot get the value directly from DOM it seems that ng-zorro lag a bit and make test crash
    return elements.map(element => element.textContent);
  }

  isCheckboxChecked(cufId: number) {
    const checkBox = this.element(`[data-test-cuf-checkbox-input-id="${cufId}"] > span`);
    return checkBox.classes.includes('ant-checkbox-checked');
  }

  getErrors(cudId: number): string[] {
    const errorElements = this.elements(`[data-test-cuf-id-error="${cudId}"]`);
    return errorElements.map(value => value.textContent);
  }
}

describe('CustomFieldFormComponent', () => {
  let tester: CustomFieldFormComponentTester;
  let fixture: ComponentFixture<CustomFieldFormComponent>;
  const customFields = [
    {
      id: 12,
      code: 'CODE',
      defaultValue: 'default',
      inputType: InputType.PLAIN_TEXT,
      label: 'FIELD A',
      name: 'fieldA',
      optional: false,
      largeDefaultValue: null,
      numericDefaultValue: null,
      options: null
    },
    {
      id: 13,
      code: 'CODE_13',
      defaultValue: 'true',
      inputType: InputType.CHECKBOX,
      label: 'FIELD B',
      name: 'fieldB',
      optional: false,
      largeDefaultValue: null,
      numericDefaultValue: null,
      options: null
    },
    {
      id: 14,
      code: 'CODE_14',
      defaultValue: 'Option - 2',
      inputType: InputType.DROPDOWN_LIST,
      label: 'FIELD C',
      name: 'fieldC',
      optional: false,
      largeDefaultValue: null,
      numericDefaultValue: null,
      options: [
        {
          cfId: 14,
          code: 'OPTION_1',
          colour: '',
          label: 'Option - 1',
          position: 0
        },
        {
          cfId: 14,
          code: 'OPTION_2',
          colour: '',
          label: 'Option - 2',
          position: 1
        }
      ]
    },
    {
      id: 15,
      code: 'CODE_15',
      defaultValue: '1245',
      inputType: InputType.NUMERIC,
      label: 'FIELD D',
      name: 'fieldD',
      optional: false,
      largeDefaultValue: null,
      numericDefaultValue: 1245,
      options: null
    },
    {
      id: 16,
      code: 'CODE_16',
      defaultValue: 'Option - 1|Option - 2',
      inputType: InputType.TAG,
      label: 'FIELD E',
      name: 'fieldE',
      optional: false,
      largeDefaultValue: null,
      numericDefaultValue: null,
      options: [
        {
          cfId: 16,
          code: 'OPTION_1',
          colour: '',
          label: 'Option - 1',
          position: 0
        },
        {
          cfId: 16,
          code: 'OPTION_2',
          colour: '',
          label: 'Option - 2',
          position: 1
        },
        {
          cfId: 16,
          code: 'OPTION_3',
          colour: '',
          label: 'Option - 3',
          position: 2
        }
      ]
    }
  ];
  const customFieldsMap = customFields.reduce((map, cuf) => {
    map[cuf.id] = cuf;
    return map;
  }, {});

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        TranslateModule.forRoot(),
        NzIconModule,
        NzButtonModule,
        NzInputModule,
        TranslateModule,
        ReactiveFormsModule,
        NzSelectModule,
        FormsModule,
        CKEditorModule,
        NzDatePickerModule,
        NzCheckboxModule,
        NzTagModule,
        NzPopoverModule,
        WorkspaceCommonModule,
        NzInputNumberModule,
        NoopAnimationsModule,
        NzDropDownModule
      ],
      declarations: [CustomFieldFormComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(CustomFieldFormComponent);
      tester = new CustomFieldFormComponentTester(fixture);
      tester.componentInstance.parentFormGroup = new FormGroup({});
      tester.componentInstance.customFields = customFields;
    });
  }));

  it('should create', () => {
    expect(tester.componentInstance).toBeTruthy();
  });

  it('should create various custom field widget', () => {
    tester.detectChanges();
    expect(tester.getLabel(12)).toEqual('FIELD A');
    expect(tester.getTextInputValue(12)).toEqual('default');

    expect(tester.getLabel(13)).toEqual('FIELD B');
    expect(tester.isCheckboxChecked(13)).toBeTruthy();

    expect(tester.getLabel(14)).toEqual('FIELD C');
    expect(tester.getDropdownValue(14)).toEqual('Option - 2');

    expect(tester.getLabel(15)).toEqual('FIELD D');
    expect(tester.getNumericInputValue(15)).toEqual('1245');

    expect(tester.getLabel(16)).toEqual('FIELD E');
    expect(tester.getTagInputValue(16)).toEqual(['Option - 1', 'Option - 2']);
  });

  describe('Reject Invalid Cuf Values', () => {
    interface DataType {
      cufId: number;
      value: any;
      expectedErrors: string[];
    }

    const dataSet: DataType[] = [
      {
        cufId: 12,
        value: '',
        expectedErrors: ['sqtm-core.validation.errors.required']
      },
      {
        cufId: 12,
        value: '     ',
        expectedErrors: ['sqtm-core.validation.errors.pattern']
      },
      {
        cufId: 12,
        value: generateRandomString(256),
        expectedErrors: ['sqtm-core.validation.errors.maxlength']
      }
    ];

    dataSet.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - It should reject invalid value : ${data.value} for cuf ${data.cufId}`, () => {
        tester.detectChanges();
        const cuf = customFieldsMap[data.cufId] as CustomField;
        tester.setInputValue(cuf, data.value);
        tester.componentInstance.showClientSideErrors();
        console.log(tester.componentInstance.parentFormGroup);
        const errors = tester.getErrors(data.cufId);
        // expect(errors.length).toEqual(data.expectedErrors.length);
        expect(errors).toEqual(data.expectedErrors);
      });
    }

  });


})
;

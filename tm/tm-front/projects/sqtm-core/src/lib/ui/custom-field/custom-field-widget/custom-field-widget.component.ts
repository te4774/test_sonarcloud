import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  ElementRef,
  EventEmitter,
  Host,
  Injector,
  Input,
  isDevMode,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  Type,
  ViewContainerRef
} from '@angular/core';
import {CustomField} from '../../../model/customfield/customfield.model';
import {TranslateService} from '@ngx-translate/core';
import {
  EditableTextFieldComponent
} from '../../workspace-common/components/editables/editable-text-field/editable-text-field.component';
import {InputType} from '../../../model/customfield/input-type.model';
import {SqtmEntityState} from '../../../core/services/entity-view/entity-view.state';
import {SimplePermissions} from '../../../model/permissions/simple-permissions';
import {EditableCustomField} from '../editable-custom-field';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import {
  EditableSelectFieldComponent
} from '../../workspace-common/components/editables/editable-select-field/editable-select-field.component';
import {
  EditableRichTextComponent
} from '../../workspace-common/components/editables/editable-rich-text/editable-rich-text.component';
// tslint:disable-next-line:max-line-length
import {
  EditableNumericFieldComponent
} from '../../workspace-common/components/editables/editable-numeric-field/editable-numeric-field.component';
import {
  EditableDateFieldComponent
} from '../../workspace-common/components/editables/editable-date-field/editable-date-field.component';
import {format, parse} from 'date-fns';
import {
  EditableTagFieldComponent
} from '../../workspace-common/components/editables/editable-tag-field/editable-tag-field.component';
import {
  EditableCheckBoxComponent
} from '../../workspace-common/components/editables/editable-check-box/editable-check-box.component';
import {ValidatorFn, Validators} from '@angular/forms';
import {CUF_DATE_FORMAT} from '../custom-field-value.utils';
import {customFieldLogger} from '../custom-field.logger';

const logger = customFieldLogger.compose('CustomFieldWidgetComponent');


@Component({
  selector: 'sqtm-core-custom-field-widget',
  templateUrl: './custom-field-widget.component.html',
  styleUrls: ['./custom-field-widget.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomFieldWidgetComponent<S extends SqtmEntityState, T extends string, P extends SimplePermissions>
  implements OnInit, OnDestroy, AfterViewChecked {

  private _customField: CustomField;
  private _value: string | string[];
  private componentRef: ComponentRef<EditableCustomField>;

  @Input()
  set customField(customField: CustomField) {
    if (!Boolean(this._customField)) {
      this._customField = customField;
      this.initializeWidget();
      this.initializeTestData();
    }
  }

  get customField() {
    return this._customField;
  }

  @Input()
  set value(value: string | string[] | number) {
    if (this.componentRef) {
      this.setValueInEditable(value);
    }
  }

  get value() {
    return this._value;
  }

  @Input()
  set editable(editable: boolean) {
    this.componentRef.instance.editable = editable;
  }

  placeHolder: string;

  @Output()
  readonly confirmEvent = new EventEmitter<string | string[]>();

  private unsub$ = new Subject<void>();

  constructor(private translateService: TranslateService, private viewContainerRef: ViewContainerRef,
              private componentFactoryResolver: ComponentFactoryResolver, private injector: Injector,
              @Host() private host: ElementRef, private renderer: Renderer2) {
  }

  ngAfterViewChecked(): void {
    if (isDevMode()) {
      logger.debug('checking cuf widget ' + this.customField);
    }
  }


  ngOnInit() {
    this.placeHolder = this.translateService.instant('sqtm-core.generic.editable.placeholder');
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  beginAsync() {
    this.componentRef.instance.beginAsync();
  }

  private setValueInEditable(value: string | string[] | number) {
    switch (this.customField.inputType) {
      case InputType.DATE_PICKER:
        this.setValueInDateField(value as string);
        break;
      case InputType.CHECKBOX:
        this.setValueInCheckBoxField(value as string);
        break;
      case InputType.DROPDOWN_LIST:
        this.setValueInDropdownListSelectField(value as string);
        break;
      default:
        this.componentRef.instance.value = value;
    }
    this.componentRef.instance.markForCheck();
  }

  private setValueInDateField(value: string) {
    if (this.customField.optional && !Boolean(value)) {
      this.componentRef.instance.value = '';
    } else {
      this.componentRef.instance.value = parse(value as string, CUF_DATE_FORMAT, new Date());
    }
  }

  private initializeWidget() {
    const inputType = this.customField.inputType;
    switch (inputType) {
      case InputType.PLAIN_TEXT:
        this.initializePlainTextField();
        break;
      case InputType.DROPDOWN_LIST:
        this.initializeDropdownList();
        break;
      case InputType.RICH_TEXT:
        this.initializeRichTextField();
        break;
      case InputType.NUMERIC:
        this.initializeNumericField();
        break;
      case InputType.DATE_PICKER:
        this.initializeDatePickerField();
        break;
      case InputType.TAG:
        this.initializeTagField();
        break;
      case InputType.CHECKBOX:
        this.initializeCheckBoxField();
        break;
      default:
        throw Error(`Unable to create custom field widget for cuf ${this.customField.name} of type ${this.customField.inputType}`);
    }
  }

  private initializePlainTextField() {
    const editableTextField = this.instantiateComponent(EditableTextFieldComponent).instance;
    const validators: ValidatorFn[] = [];
    validators.push(Validators.maxLength(255));
    editableTextField.layout = 'compact';
    editableTextField.validators = validators;
    editableTextField.trimValue = true;
    editableTextField.required = !this.customField.optional;
    editableTextField.confirmEvent.pipe(
      takeUntil(this.unsub$)
    ).subscribe(newValue => this.confirmEvent.next(newValue));
  }

  private initializeDropdownList() {
    const selectField = this.instantiateComponent(EditableSelectFieldComponent).instance;
    selectField.options = this.customField.options.map(cufOption => {
      return {
        value: cufOption.label,
        label: cufOption.label,
      };
    });
    selectField.allowEmptyValue = this.customField.optional;
    selectField.layout = 'no-buttons';
    selectField.confirmEvent.pipe(
      takeUntil(this.unsub$)
    ).subscribe(newValue => {
      if (this.customField.optional && !Boolean(newValue)) {
        this.confirmEvent.next('');
      } else {
        this.confirmEvent.next(newValue.value);
      }
    });
  }

  private instantiateComponent<C extends EditableCustomField>(component: Type<C>): ComponentRef<C> {
    this.viewContainerRef.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
    const componentRef = this.viewContainerRef.createComponent(componentFactory, this.viewContainerRef.length, this.injector);
    this.componentRef = componentRef;
    return componentRef;
  }


  private initializeRichTextField() {
    const editableRichTextComponent = this.instantiateComponent(EditableRichTextComponent).instance;
    editableRichTextComponent.required = !this.customField.optional;
    editableRichTextComponent.focus = true;
    this.componentRef.instance.confirmEvent.pipe(
      takeUntil(this.unsub$)
    ).subscribe(newValue => this.confirmEvent.next(newValue));
  }

  private initializeNumericField() {
    const numericField = this.instantiateComponent(EditableNumericFieldComponent).instance;
    numericField.layout = 'compact';
    numericField.required = !this.customField.optional;
    numericField.confirmEvent.pipe(
      takeUntil(this.unsub$),
    ).subscribe(newValue => this.confirmEvent.next(newValue));
  }

  private initializeDatePickerField() {
    const dateField = this.instantiateComponent(EditableDateFieldComponent);
    dateField.instance.layout = 'compact';
    dateField.instance.allowEmptyValue = this.customField.optional;
    dateField.instance.confirmEvent.pipe(
      takeUntil(this.unsub$),
    ).subscribe(newValue => {
      if (newValue) {
        const formattedValue = format(newValue, CUF_DATE_FORMAT);
        this.confirmEvent.next(formattedValue);
      } else {
        this.confirmEvent.next('');
      }
    });
  }

  private initializeTagField() {
    const tagField = this.instantiateComponent(EditableTagFieldComponent).instance;
    tagField.options = this.customField.options.map(cufOption => {
      return cufOption.label;
    });
    tagField.optional = this.customField.optional;
    tagField.confirmEvent.pipe(
      takeUntil(this.unsub$)
    ).subscribe(newValue => this.confirmEvent.next(newValue));
  }

  private initializeCheckBoxField() {
    const checkbox = this.instantiateComponent(EditableCheckBoxComponent).instance;
    checkbox.confirmEvent.pipe(
      takeUntil(this.unsub$)
    ).subscribe(newValue => this.confirmEvent.next(newValue.toString()));
  }

  private setValueInCheckBoxField(value: string) {
    this.componentRef.instance.value = value === 'true';
  }

  private setValueInDropdownListSelectField(value: string) {
    const findOption = this.customField.options.find(option => option.label === value);

    if (findOption == null) {
      const dropdownList = this.componentRef.instance as EditableSelectFieldComponent;
      dropdownList.options = [...dropdownList.options, {value: value, label: value, hide: true}];
    }

    this.componentRef.instance.value = value;
  }

  private initializeTestData() {
    this.renderer.setAttribute(this.host.nativeElement, 'data-test-customfield-name', this._customField.name);
  }
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomFieldWidgetComponent} from './custom-field-widget/custom-field-widget.component';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzDatePickerModule} from 'ng-zorro-antd/date-picker';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzInputNumberModule} from 'ng-zorro-antd/input-number';
import {NzPopoverModule} from 'ng-zorro-antd/popover';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzTagModule} from 'ng-zorro-antd/tag';
import {WorkspaceCommonModule} from '../workspace-common/workspace-common.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {EntityCustomFieldDirective} from './directives/entity-custom-field.directive';
import {CKEditorModule} from 'ckeditor4-angular';
import {CustomFieldFormComponent} from './custom-field-form/custom-field-form.component';

@NgModule({
  declarations: [
    CustomFieldWidgetComponent,
    EntityCustomFieldDirective,
    CustomFieldFormComponent,
  ],
  imports: [
    CommonModule,
    NzIconModule,
    NzButtonModule,
    NzInputModule,
    TranslateModule.forChild(),
    ReactiveFormsModule,
    NzSelectModule,
    FormsModule,
    CKEditorModule,
    NzDatePickerModule,
    NzCheckboxModule,
    NzTagModule,
    NzPopoverModule,
    WorkspaceCommonModule,
    NzInputNumberModule,
  ],
  exports: [
    CustomFieldWidgetComponent,
    EntityCustomFieldDirective,
    CustomFieldFormComponent,
  ],
})
export class CustomFieldModule {
}

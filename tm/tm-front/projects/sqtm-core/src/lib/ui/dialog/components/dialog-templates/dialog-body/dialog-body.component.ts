import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-core-dialog-body',
  template: `
    <div class="full-height full-width dialog-body">
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./dialog-body.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogBodyComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}

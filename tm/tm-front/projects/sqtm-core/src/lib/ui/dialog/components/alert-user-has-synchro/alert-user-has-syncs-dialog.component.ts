import {ChangeDetectionStrategy, Component, HostListener} from '@angular/core';
import {AlertUserHasSyncsConfiguration} from './alert-user-has-syncs-configuration';
import {DialogReference} from '../../model/dialog-reference';
import {KeyNames} from '../../../utils/key-names';

@Component({
  selector: 'sqtm-core-alert-user-has-syncs-dialog',
  templateUrl: './alert-user-has-syncs-dialog.component.html',
  styleUrls: ['./alert-user-has-syncs-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlertUserHasSyncsDialogComponent  {

  configuration: AlertUserHasSyncsConfiguration;

  constructor(private dialogReference: DialogReference<AlertUserHasSyncsConfiguration, boolean>) {
    this.configuration = dialogReference.data;
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.ENTER || event.key === KeyNames.ESCAPE) {
      this.dialogReference.close();
    }
  }
}

import {ChangeDetectionStrategy, Component, HostListener, OnInit} from '@angular/core';
import {DialogReference} from '../../../model/dialog-reference';
import {ConfirmDeleteConfiguration} from './confirm-delete-configuration';
import {KeyNames} from '../../../../utils/key-names';

@Component({
  selector: 'sqtm-core-confirm-delete-dialog',
  templateUrl: './confirm-delete-dialog.component.html',
  styleUrls: ['./confirm-delete-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmDeleteDialogComponent implements OnInit {

  configuration: ConfirmDeleteConfiguration;

  constructor(private dialogReference: DialogReference<ConfirmDeleteConfiguration, boolean>) {
    this.configuration = dialogReference.data;
  }

  ngOnInit() {
  }

  confirmDeletion() {
    this.dialogReference.result = true;
    this.dialogReference.close();
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.ENTER) {
      this.confirmDeletion();
    } else if (event.key === KeyNames.ESCAPE) {
      this.dialogReference.close();
    }
  }
}

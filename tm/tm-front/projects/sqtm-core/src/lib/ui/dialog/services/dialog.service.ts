import {ComponentFactoryResolver, Inject, Injectable, Injector, isDevMode} from '@angular/core';
import {createEntityAdapter} from '@ngrx/entity';
import {createStore} from '../../../core/store/store';
import {ComponentPortal, PortalInjector} from '@angular/cdk/portal';
import {Overlay, OverlayConfig} from '@angular/cdk/overlay';
import {filter, map, share, take, takeUntil} from 'rxjs/operators';
import {
  DEFAULT_DIALOG_CONFIGURATION,
  DialogClosePayload,
  DialogConfiguration,
  DialogFeatureState
} from './dialog-feature.state';
import {DialogComponent} from '../components/dialog/dialog.component';
import {DOCUMENT} from '@angular/common';
import {Observable, Subject} from 'rxjs';
import {DialogReference} from '../model/dialog-reference';
import {OVERLAY_REF} from '../dialog.constants';
import {
  ConfirmDeleteDialogComponent
} from '../components/dialog-templates/confirm-delete-dialog/confirm-delete-dialog.component';
import {
  ConfirmDeleteConfiguration,
  defaultDeleteConfirmConfiguration
} from '../components/dialog-templates/confirm-delete-dialog/confirm-delete-configuration';
import {
  CreationDialogConfiguration
} from '../components/dialog-templates/creation-dialog/creation-dialog.configuration';
import {GridService} from '../../grid/services/grid.service';
import {DataRow} from '../../grid/model/data-row.model';
import {AlertDialogComponent} from '../components/dialog-templates/alert-dialog/alert-dialog.component';
import {
  AlertConfiguration,
  defaultAlertConfiguration
} from '../components/dialog-templates/alert-dialog/alert-configuration';
import {
  ConfirmConfiguration,
  defaultConfirmConfiguration
} from '../components/dialog-templates/confirm-dialog/confirm-configuration';
import {ConfirmDialogComponent} from '../components/dialog-templates/confirm-dialog/confirm-dialog.component';
import {EntityCreationDialogData} from '../components/create-entity-dialog/entity-creation-dialog-data';
import {
  HttpErrorDialogConfiguration
} from '../components/dialog-templates/http-error-dialog/http-error-dialog-configuration';
import {HttpErrorDialogComponent} from '../components/dialog-templates/http-error-dialog/http-error-dialog.component';

/**
 * Service used for creating our resizable and draggable dialogs.
 * Heavily inspired by angular material (https://github.com/angular/material) and ngModal (https://github.com/mazdik/ng-modal)
 */
/** @dynamic */
@Injectable({
  providedIn: 'root'
})
export class DialogService {

  dialogsOpened$: Observable<string>;
  dialogsClosed$: Observable<DialogClosePayload<any>>;

  private _dialogsOpened = new Subject<string>();
  private _dialogsClosed = new Subject<DialogClosePayload<any>>();

  private adapter = createEntityAdapter<DialogReference>();

  private store = createStore<DialogFeatureState>({
    dialogs: {ids: [], entities: {}}
  });

  constructor(private overlay: Overlay, @Inject(DOCUMENT) private document: Document, private rootInjector: Injector) {
    this.dialogsOpened$ = this._dialogsOpened.asObservable().pipe(share());
    this.dialogsClosed$ = this._dialogsClosed.asObservable().pipe(share());
  }

  private commit(state: DialogFeatureState) {
    this.store.commit(state);
  }

  openDialog<D, R>(dialogDefinition: DialogConfiguration<D>): DialogReference<D, R> {
    const overlayConfig = this.createConfig(dialogDefinition);
    const injector = this.getInjector(dialogDefinition);
    // Creating the overlay
    const overlayRef = this.overlay.create(overlayConfig);

    // Creating a custom injector for the container
    const containerInjector = new PortalInjector(injector, new WeakMap<any, any>(
      [[OVERLAY_REF, overlayRef]]
    ));

    // create and attach the container
    const containerPortal = new ComponentPortal<DialogComponent>(
      DialogComponent,
      undefined,
      containerInjector,
      injector.get(ComponentFactoryResolver));

    const containerRef = overlayRef.attach(containerPortal);

    // now we can create the reference witch is responsible for manipulating and closing the dialog.
    const dialogReference: DialogReference<D, R> = new DialogReference(
      dialogDefinition.id, containerRef, overlayRef, dialogDefinition.data);

    // creating the internal component injector with the dialog reference and the custom data
    const contentInjector = new PortalInjector(injector, new WeakMap<any, any>(
      [
        [DialogReference, dialogReference],
      ]
    ));

    // creating and attaching the user component. Injecting the DialogReference across a custom injector.
    const contentPortal = new ComponentPortal(dialogDefinition.component, undefined, contentInjector);
    containerRef.instance.attachComponentPortal(contentPortal);

    this.connectToServiceOutput<D, R>(dialogReference);
    return dialogReference;
  }

  openDeletionConfirm(configuration?: Partial<ConfirmDeleteConfiguration>, width?: number):
    DialogReference<ConfirmDeleteConfiguration, boolean> {
    const dialogConfiguration: DialogConfiguration = {
      id: 'confirm-delete',
      component: ConfirmDeleteDialogComponent,
      data: configuration ? {...defaultDeleteConfirmConfiguration, ...configuration} : {...defaultDeleteConfirmConfiguration},
      width: width || 550,
    };

    return this.openDialog<ConfirmDeleteConfiguration, boolean>(dialogConfiguration);
  }

  openConfirm(configuration?: Partial<ConfirmConfiguration>, width?: number): DialogReference<ConfirmConfiguration, boolean> {
    const dialogConfiguration: DialogConfiguration = {
      id: 'confirm',
      component: ConfirmDialogComponent,
      data: configuration ? {...defaultConfirmConfiguration, ...configuration} : {...defaultConfirmConfiguration},
      width: width || 550,
    };

    return this.openDialog(dialogConfiguration);
  }

  openAlert(configuration?: Partial<AlertConfiguration>, width?: number): DialogReference<AlertConfiguration, boolean> {
    const dialogConfiguration: DialogConfiguration = {
      id: 'alert',
      component: AlertDialogComponent,
      data: configuration ? {...defaultAlertConfiguration, ...configuration} : {...defaultAlertConfiguration},
      width: width || 400,
    };

    return this.openDialog(dialogConfiguration);
  }

  openHttpErrorDialog(configuration: Partial<HttpErrorDialogConfiguration>, width?: number):
    DialogReference<HttpErrorDialogConfiguration, boolean> {
    const dialogConfiguration: DialogConfiguration = {
      id: 'server-error',
      component: HttpErrorDialogComponent,
      data: configuration,
      width: width || 400,
    };

    return this.openDialog(dialogConfiguration);
  }

  openEntityCreationDialog<D extends EntityCreationDialogData, R extends DataRow>(
    configuration: CreationDialogConfiguration<D>,
    gridService: GridService,
    position?: number
  ): DialogReference<D, R> {
    const dialogConfiguration: DialogConfiguration<D> = {
      id: configuration.data.id,
      component: configuration.formComponent,
      data: {...configuration.data},
      viewContainerReference: configuration.viewContainerReference,
      width: 800
    };

    const dialogReference: DialogReference<D, R> = this.openDialog<D, R>(dialogConfiguration);

    dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      filter((addedRow) => addedRow != null),
    ).subscribe((addedRow: DataRow) => {
      gridService.addAndSelectRow(addedRow, dialogReference.data['parentEntityReference'], position);
    });

    return dialogReference;
  }

  private getInjector(dialogDefinition: DialogConfiguration) {
    let injector: Injector;
    if (Boolean(dialogDefinition.injector)) {
      injector = dialogDefinition.injector;
    } else if (Boolean(dialogDefinition.viewContainerReference)) {
      injector = dialogDefinition.viewContainerReference.injector;
    } else {
      if (isDevMode()) {
        console.warn(`Using default root injector for dialog ${dialogDefinition.id}.
      It could lead to 'No component factory found...' or 'No provider for ...' errors because the injector of the dialog is
      son of the root injector and NOT son of the component that opened the dialog.
      If such error occurs, provide a reference to the ViewContainerRef or the Injector itself, in the dialog definition.
      By providing an injector (or the vcr, that contain a reference to the injector), the injector of dialog will be the son of the
      provided injector, and thus, the DI system will be able to provide non root stuff`);
      }
      injector = this.rootInjector;
    }
    return injector;
  }

  private createConfig(dialogConfiguration: DialogConfiguration): OverlayConfig {
    const configuration = {...DEFAULT_DIALOG_CONFIGURATION, ...dialogConfiguration};

    const positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically();

    if (dialogConfiguration.top) {
      positionStrategy.top(dialogConfiguration.top);
    }

    return {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      width: configuration.width,
      height: configuration.height,
      minHeight: configuration.minHeight,
      minWidth: configuration.minWidth,
      maxWidth: configuration.maxWidth,
      maxHeight: configuration.maxHeight,
    };
  }

  private forwardReferenceToStore<D, R>(dialogReference: DialogReference<D, R>) {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const dialogs = this.adapter.addOne(dialogReference, state.dialogs);
        return {...state, dialogs};
      })
    ).subscribe((state: DialogFeatureState) => this.commit(state));
  }

  private removeReferenceFromStore<D, R>(dialogReference: DialogReference<D, R>) {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const dialogs = this.adapter.removeOne(dialogReference.id, state.dialogs);
        return {...state, dialogs};
      })
    ).subscribe((state: DialogFeatureState) => this.commit(state));
  }

  private connectToServiceOutput<D, R>(dialogReference: DialogReference<D, R>) {
    dialogReference.dialogOpened$.subscribe(() => {
      this.forwardReferenceToStore(dialogReference);
      this._dialogsOpened.next(dialogReference.id);
    });

    dialogReference.dialogClosed$.subscribe(
      (result: R) => {
        this.removeReferenceFromStore(dialogReference);
        this._dialogsClosed.next({
          id: dialogReference.id,
          result: result
        });
      }
    );
  }
}

import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {CreationDialogData} from './creation-dialog.configuration';
import {DialogReference} from '../../../model/dialog-reference';
import {dialogLogger} from '../../../dialog.logger';
import {KeyNames} from '../../../../utils/key-names';

const creationDialogLogger = dialogLogger.compose('CreationDialogComponent');

@Component({
  selector: 'sqtm-core-creation-dialog',
  templateUrl: './creation-dialog.component.html',
  styleUrls: ['./creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreationDialogComponent implements OnInit {

  data: CreationDialogData;

  @Input()
  asyncTaskRunning = false;

  @ViewChild('dialog')
  dialog: ElementRef;

  @Output()
  addAnother = new EventEmitter<void>();

  @Output()
  add = new EventEmitter<void>();

  constructor(private dialogReference: DialogReference<CreationDialogData, any>) {
    this.data = dialogReference.data;
  }

  ngOnInit() {
    creationDialogLogger.debug(`Creation dialog, data: ${JSON.stringify(this.data)}`);
  }

  handleAdd() {
    this.add.emit();
  }

  handleAddAnother() {
    this.addAnother.emit();
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (event.key === KeyNames.ENTER) {
      this.handleAddAnother();
    } else if (event.key === KeyNames.ESCAPE) {
      this.dialogReference.close();
    }
  }
}

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import {DialogFooterComponent} from '../dialog-footer/dialog-footer.component';
import {fromEvent, Subject} from 'rxjs';
import {KeyNames} from '../../../../utils/key-names';
import {DialogReference} from '../../../model/dialog-reference';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-core-customisable-dialog',
  template: `
    <div class="full-width full-height flex-column" style="justify-content: space-evenly;"
         [attr.data-test-dialog-id]="dialogId">
      <div class="flex-fixed-size m-t-10 m-l-10 m-r-10" sqtmCoreDialogHeader>
        <ng-container *ngIf="titleKey">
          <span class="sqtm-core-prevent-selection font-dialog-title">{{titleKey | translate | capitalize}}</span>
        </ng-container>
        <ng-content select="sqtm-core-dialog-header"></ng-content>
      </div>
      <nz-divider class="sqtm-core-dialog-divider"></nz-divider>
      <div class="m-t-5 m-l-10 m-r-10 dialog-body-wrapper full-height">
        <ng-content select="sqtm-core-dialog-body"></ng-content>
      </div>
      <div style="justify-content: flex-end" class="flex-row flex-fixed-size m-b-8 m-r-10">
        <ng-content select="sqtm-core-dialog-footer"></ng-content>
        <ng-container *ngIf="!projectedFooter">
          <button
            [attr.data-test-dialog-button-id]="'confirm'"
            nz-button
            nzType="primary"
            [disabled]="!allowConfirm"
            (click)="handleConfirm()"
          >{{confirmLabelKey | translate | capitalize}}</button>
          <button
            [attr.data-test-dialog-button-id]="'cancel'"
            style="margin-left: 5px"
            nz-button
            nzType="default"
            sqtmCoreDialogClose
          >{{'sqtm-core.generic.label.cancel' | translate}}</button>
        </ng-container>
      </div>
    </div>
  `,
  styleUrls: ['./customisable-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomisableDialogComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input()
  dialogId: string;

  @Input()
  titleKey: string;

  @Input()
  allowConfirm = true;

  @Input()
  confirmLabelKey = 'sqtm-core.generic.label.confirm';

  @ContentChild(DialogFooterComponent)
  projectedFooter: DialogFooterComponent;

  @Output()
  confirm = new EventEmitter<void>();

  private unsub$ = new Subject<void>();

  constructor(private dialogReference: DialogReference) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    if (this.projectedFooter == null) {
      fromEvent(window, 'keyup')
        .pipe(takeUntil(this.unsub$))
        .subscribe((event: KeyboardEvent) => {
          if (event.key === KeyNames.ENTER) {
            this.handleConfirm();
          } else if (event.key === KeyNames.ESCAPE) {
            this.dialogReference.result = null;
            this.dialogReference.close();
          }
        });
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }


  handleConfirm() {
    this.confirm.next();
  }
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractFilterWidget} from '../abstract-filter-widget';
import {ListItem} from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import {Filter, isMultiBooleanValue} from '../../state/filter.state';
import {Workspaces} from '../../../ui-manager/theme.model';
import {BooleanI18nEnum} from '../../../../model/level-enums/level-enum';

@Component({
  selector: 'sqtm-core-boolean-filter',
  templateUrl: './boolean-filter.component.html',
  styleUrls: ['./boolean-filter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BooleanFilterComponent extends AbstractFilterWidget  implements OnInit {

  items: ListItem[];
  workspace: Workspaces;

  private filterValue: boolean[];

  constructor(protected cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  ngOnInit(): void {
  }

  setFilter(filter: Filter) {
    const filterValue = filter.value;
    if (isMultiBooleanValue(filterValue)) {
      this._filter = filter;
      const booleanValues = filterValue.value.map(value => value);
      this.items = Object.values(BooleanI18nEnum).map(levelEnumItem => {
        return {
          id: levelEnumItem.id,
          selected: booleanValues.includes(levelEnumItem.id.toString().toLowerCase() === 'true'),
          i18nLabelKey: levelEnumItem.i18nKey
        };
      });
      this.filterValue = filterValue.value;
      this.workspace = filter.uiOptions?.workspace;
      this.cdRef.detectChanges();
    } else {
      throw Error('Not handled kind ' + filter.value.kind);
    }
  }

  changeSelection($event: ListItem) {
    const i18nEnumItem = BooleanI18nEnum[$event.id];
    let nextValue: boolean[] = [...this.filterValue];
    if ($event.selected) {
      nextValue = this.filterValue.concat(i18nEnumItem.id.toString().toLowerCase() === 'true');
    } else {
      const index = this.filterValue.findIndex(v => v.toString().toLowerCase() === $event.id.toString().toLowerCase());
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: { kind: 'multiple-boolean-value', value: nextValue }
    });
  }
}

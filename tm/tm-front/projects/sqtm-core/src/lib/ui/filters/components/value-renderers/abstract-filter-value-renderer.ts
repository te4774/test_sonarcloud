import {ChangeDetectorRef, Directive, EventEmitter, Input, Output} from '@angular/core';
import {
  DateFilterValue,
  Filter,
  FilterOperation,
  MultiDateFilterValue,
  MultiDiscreteFilterValue,
  MultipleBooleanFilterValue,
  MultipleNumericValue,
  MultipleStringFilterValue,
  StringFilterValue
} from '../../state/filter.state';
import {TranslateService} from '@ngx-translate/core';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractFilterValueRenderer {

  protected _filter: Filter;

  get filter(): Filter {
    return this._filter;
  }

  @Input()
  set filter(value: Filter) {
    this._filter = value;
  }

  @Output()
  filterInactivated = new EventEmitter<void>();

  protected constructor(protected cdRef: ChangeDetectorRef, protected translateService: TranslateService) {
  }

  public detectChanges() {
    this.cdRef.detectChanges();
  }

  getDisplayValue(): string {
    const filterValue = this.filter.value;
    if (Boolean(filterValue)) {
      switch (filterValue.kind) {
        case 'single-string-value':
          return (filterValue as StringFilterValue).value || '';
        case 'single-date-value':
          return this.buildDateValue(filterValue as DateFilterValue, this.filter.operation);
        case 'multiple-date-value':
          return this.buildMultiDateValue(filterValue as MultiDateFilterValue, this.filter.operation);
        case 'multiple-discrete-value':
          return this.buildDiscreteValue(filterValue as MultiDiscreteFilterValue);
        case 'multiple-string-value':
          return this.buildMultiStringValue(filterValue as MultipleStringFilterValue, this.filter.operation);
        case 'multiple-boolean-value':
          return this.buildMultiBooleanValue(filterValue as MultipleBooleanFilterValue, this.filter.operation);
        case 'multiple-numeric-value':
          return this.buildMultipleNumericValue(filterValue as MultipleNumericValue, this.filter.operation);
        default:
          throw Error('unknown filter value kind ' + filterValue.kind);
      }
    }
    return '';
  }

  protected buildDiscreteValue(filterValue: MultiDiscreteFilterValue): string {
    if (filterValue.value.length === 0) {
      return '';
    }
    const parts: String[] = filterValue.value.map(value => {
      if (Boolean(value.label)) {
        return value.label;
      } else if (Boolean(value.i18nLabelKey)) {
        return this.translateService.instant(value.i18nLabelKey);
      } else {
        throw Error('Cannot display a value with no label and no i18nKey ' + value.id);
      }
    });
    return parts
      .sort()
      .join(', ');
  }

  protected buildMultiStringValue(filterValue: MultipleStringFilterValue, operation: FilterOperation): string {
    if (filterValue.value.length === 0) {
      return '';
    }
    return filterValue.value.join(', ');
  }

  protected buildMultipleNumericValue(filterValue: MultipleNumericValue, operation: FilterOperation): string {
    if (filterValue.value.length === 0) {
      return '';
    }
    return filterValue.value.join(', ');
  }

  protected buildDateValue(filterValue: DateFilterValue, operation: FilterOperation) {
    if (filterValue.value.length === 0) {
      return '';
    }
    return filterValue.value;
  }

  protected buildMultiDateValue(filterValue: MultipleStringFilterValue, operation: FilterOperation): string {
    if (filterValue.value.length === 0) {
      return '';
    }
    return filterValue.value.join(', ');
  }

  protected inactivateFilter() {
    this.filterInactivated.emit();
  }

  private buildMultiBooleanValue(filterValue: MultipleBooleanFilterValue, operation: FilterOperation) {
    if (filterValue.value.length === 0) {
      return '';
    }
    return filterValue.value.join(', ');
  }
}

import {
  AutomationRequestStatus,
  BooleanI18nEnum,
  ExecutionStatus,
  I18nEnum,
  RequirementCriticality,
  RequirementStatus,
  SearchableMilestoneStatus,
  TestCaseAutomatable,
  TestCaseExecutionMode,
  TestCaseStatus,
  TestCaseWeight,
  ToBeTreatAutomationRequestStatus,
  ToBeValidatedAutomationRequestStatus,
} from '../../../model/level-enums/level-enum';
import {TestCaseClasses} from '../../../model/level-enums/test-case/test-case-kind';
import {
  RequirementBoundToHighLvlReqFilter,
  RequirementCurrentVersionFilter,
  RequirementKindFilter,
  RequirementVersionHasChildrenFilter,
  RequirementVersionHasDescriptionFilter,
  RequirementVersionHasParentFilter
} from '../../../model/level-enums/requirement/requirement-version-filter';
import {ResearchColumnPrototype} from './filter.state';

// @dynamic
export class SearchColumnPrototypeUtils {

  private static getEnumMap(): Partial<{ [K in ResearchColumnPrototype]: I18nEnum<any> }> {
    return {
      [ResearchColumnPrototype.REQUIREMENT_CRITICALITY]: RequirementCriticality,
      [ResearchColumnPrototype.REQUIREMENT_STATUS]: RequirementStatus,
      [ResearchColumnPrototype.REQUIREMENT_VERSION_CRITICALITY]: RequirementCriticality,
      [ResearchColumnPrototype.REQUIREMENT_VERSION_STATUS]: RequirementStatus,
      [ResearchColumnPrototype.REQUIREMENT_VERSION_MILESTONE_STATUS]: SearchableMilestoneStatus,
      [ResearchColumnPrototype.TEST_CASE_STATUS]: TestCaseStatus,
      [ResearchColumnPrototype.TEST_CASE_IMPORTANCE]: TestCaseWeight,
      [ResearchColumnPrototype.TEST_CASE_AUTOMATABLE]: TestCaseAutomatable,
      [ResearchColumnPrototype.AUTOMATION_REQUEST_STATUS]: AutomationRequestStatus,
      [ResearchColumnPrototype.TO_BE_VALIDATED_AUTOMATION_REQUEST_STATUS]: ToBeValidatedAutomationRequestStatus,
      [ResearchColumnPrototype.TO_BE_TREAT_AUTOMATION_REQUEST_STATUS]: ToBeTreatAutomationRequestStatus,
      [ResearchColumnPrototype.TEST_CASE_KIND]: TestCaseClasses,
      [ResearchColumnPrototype.TEST_CASE_MILESTONE_STATUS]: SearchableMilestoneStatus,
      [ResearchColumnPrototype.REQUIREMENT_KIND]: RequirementKindFilter,
      [ResearchColumnPrototype.REQUIREMENT_BOUND_TO_HIGH_LEVEL_REQUIREMENT]: RequirementBoundToHighLvlReqFilter,
      [ResearchColumnPrototype.REQUIREMENT_VERSION_HAS_DESCRIPTION]: RequirementVersionHasDescriptionFilter,
      [ResearchColumnPrototype.REQUIREMENT_VERSION_HAS_CHILDREN]: RequirementVersionHasChildrenFilter,
      [ResearchColumnPrototype.REQUIREMENT_VERSION_HAS_PARENT]: RequirementVersionHasParentFilter,
      [ResearchColumnPrototype.REQUIREMENT_VERSION_CURRENT_VERSION]: RequirementCurrentVersionFilter,
      [ResearchColumnPrototype.TEST_CASE_HASAUTOSCRIPT]: BooleanI18nEnum,
      [ResearchColumnPrototype.ITEM_TEST_PLAN_STATUS]: ExecutionStatus,
      [ResearchColumnPrototype.ITEM_TEST_PLAN_TC_DELETED]: BooleanI18nEnum,
      [ResearchColumnPrototype.ITEM_TEST_PLAN_IS_EXECUTED]: BooleanI18nEnum,
      [ResearchColumnPrototype.EXECUTION_ISAUTO]: BooleanI18nEnum,
      [ResearchColumnPrototype.EXECUTION_ISSUECOUNT]: BooleanI18nEnum,
      [ResearchColumnPrototype.EXECUTION_STATUS]: ExecutionStatus,
      [ResearchColumnPrototype.EXECUTION_EXECUTION_MODE]: TestCaseExecutionMode,
      [ResearchColumnPrototype.CAMPAIGN_MILESTONE_STATUS]: SearchableMilestoneStatus,
      [ResearchColumnPrototype.TEST_CASE_HAS_BOUND_SCM_REPOSITORY]: BooleanI18nEnum,
      [ResearchColumnPrototype.TEST_CASE_HAS_BOUND_AUTOMATED_TEST_REFERENCE]: BooleanI18nEnum,
    };
  }

  public static findEnum(columnPrototype: ResearchColumnPrototype): I18nEnum<any> {
    const i18nEnum = SearchColumnPrototypeUtils.getEnumMap()[columnPrototype];
    if (Boolean(i18nEnum)) {
      return i18nEnum;
    }
    throw Error(`No enum for column prototype ${columnPrototype}`);
  }
}

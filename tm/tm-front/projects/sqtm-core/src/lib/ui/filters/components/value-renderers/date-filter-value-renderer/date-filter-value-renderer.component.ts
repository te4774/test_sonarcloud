import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractFilterValueRenderer} from '../abstract-filter-value-renderer';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';
import {DateFilterValue, FilterOperation, MultipleStringFilterValue} from '../../../state/filter.state';
import {getSupportedBrowserLang} from '../../../../../core/utils/browser-langage.utils';

@Component({
  selector: 'sqtm-core-date-filter-value-renderer',
  templateUrl: './date-filter-value-renderer.component.html',
  styleUrls: ['./date-filter-value-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateFilterValueRendererComponent extends AbstractFilterValueRenderer implements OnInit {

  constructor(protected cdRef: ChangeDetectorRef, protected translateService: TranslateService,
              protected datePipe: DatePipe) {
    super(cdRef, translateService);
  }

  ngOnInit() {
  }

  protected buildDateValue(filterValue: DateFilterValue, operation: FilterOperation): string {

    if (filterValue.value.length === 0) {
      return '';
    }

    return this.datePipe.transform(filterValue.value, 'shortDate', '', getSupportedBrowserLang(this.translateService));
  }

  protected buildMultiDateValue(filterValue: MultipleStringFilterValue, operation: FilterOperation): string {

    if (filterValue.value.length === 0) {
      return '';
    }
    const values = filterValue.value.map(value => this.datePipe.transform(
      value, 'shortDate', '',
      getSupportedBrowserLang(this.translateService)));
    return values.join(', ');
  }

}

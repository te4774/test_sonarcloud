import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractFilterWidget} from '../abstract-filter-widget';
import {ListItem} from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import {DiscreteFilterValue, Filter, MultiDiscreteFilterValue, Scope} from '../../state/filter.state';
import {take} from 'rxjs/operators';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {AutomatedTestTechnology} from '../../../../model/automation/automated-test-technology.model';
import {Identifier} from '../../../../model/entity.model';


@Component({
  selector: 'sqtm-core-filter-automated-test-technology',
  templateUrl: './filter-automated-test-technology.component.html',
  styleUrls: ['./filter-automated-test-technology.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterAutomatedTestTechnologyComponent extends AbstractFilterWidget  implements OnInit {

  items: ListItem[];
  private filterValue: DiscreteFilterValue[];

  constructor(protected cdRef: ChangeDetectorRef, private referentialDataService: ReferentialDataService) {
    super(cdRef);
  }

  ngOnInit(): void {
  }

  setFilter(filter: Filter, scope: Scope) {
    this.referentialDataService.automatedTestTechnologies$.pipe(
      take(1)
    ).subscribe(automatedTestTechnologies => {
      const selectedItemIds = (filter.value as MultiDiscreteFilterValue).value.map(v => v.id);
      this.items = this.convertToListItems(automatedTestTechnologies, selectedItemIds);
      this.filterValue = (filter.value as MultiDiscreteFilterValue).value;
      this.cdRef.detectChanges();
    });
  }

  private convertToListItems(automatedTestTechnologies: AutomatedTestTechnology[], selectedItemIds: Identifier[]) {
    const listItems = automatedTestTechnologies.map(technology => {
      const listItem = {id: technology.name, label: technology.name};
      return {
        ...listItem, selected: selectedItemIds.includes(listItem.id)
      };
    });

    listItems.sort((first, second) => first.label.localeCompare(second.label));
    return listItems;
  }

  changeSelection($event: ListItem) {
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if ($event.selected) {
      nextValue = this.filterValue.concat({id: $event.id, label: $event.label});
    } else {
      const index = this.filterValue.findIndex(v => v.id === $event.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: {
        kind: 'multiple-discrete-value',
        value: nextValue
      }
    });
  }

}

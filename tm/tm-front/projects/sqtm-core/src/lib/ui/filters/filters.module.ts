import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilterFieldComponent} from './components/filter-field/filter-field.component';
import {TranslateModule} from '@ngx-translate/core';
import {WorkspaceCommonModule} from '../workspace-common/workspace-common.module';
import {EditFilterTextComponent} from './components/edit-filter-text/edit-filter-text.component';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzDatePickerModule} from 'ng-zorro-antd/date-picker';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzInputNumberModule} from 'ng-zorro-antd/input-number';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzTabsModule} from 'ng-zorro-antd/tabs';
import {FormsModule} from '@angular/forms';
import {FilterProjectComponent} from './components/filter-project/filter-project.component';
import {
  MultiValueCufFilterComponent
} from './components/custom-fields/multi-value-cuf-filter/multi-value-cuf-filter.component';
import {NumericFilterComponent} from './components/numeric-filter/numeric-filter.component';
import {OperationSelectorComponent} from './components/operation-selector/operation-selector.component';
import {
  DefaultValueRendererComponent
} from './components/value-renderers/default-value-renderer/default-value-renderer.component';
// tslint:disable-next-line:max-line-length
import {
  WithOperationFilterValueRendererComponent
} from './components/value-renderers/with-operation-filter-value-renderer/with-operation-filter-value-renderer.component';
import {DateFilterComponent} from './components/date-filter/date-filter.component';
// tslint:disable-next-line:max-line-length
import {
  DateFilterValueRendererComponent
} from './components/value-renderers/date-filter-value-renderer/date-filter-value-renderer.component';
// tslint:disable-next-line:max-line-length
import {
  FilterTestCaseMilestoneLabelComponent
} from './components/filter-test-case-milestone-label/filter-test-case-milestone-label.component';
import {CheckboxCufFilterComponent} from './components/custom-fields/checkbox-cuf-filter/checkbox-cuf-filter.component';
import {FilterEnumComponent} from './components/filter-enum/filter-enum.component';
import {
  FilterEnumSingleSelectComponent
} from './components/filter-enum-single-select/filter-enum-single-select.component';
import {FilterInfolistComponent} from './components/filter-infolist/filter-infolist.component';
import {FilterUserHistoryComponent} from './components/filter-user-history/filter-user-history.component';
import {UiManagerModule} from '../ui-manager/ui-manager.module';
import {NumericSetFilterComponent} from './components/numeric-set-filter/numeric-set-filter.component';
import {FilterUserAssigneeComponent} from './components/filter-user-assignee/filter-user-assignee.component';
import {BooleanFilterComponent} from './components/boolean-filter/boolean-filter.component';
import {FilterMultiValueComponent} from './components/filter-multi-value/filter-multi-value.component';
import {FilterExecutionStatusComponent} from './components/filter-execution-status/filter-execution-status.component';
import {NzSwitchModule} from 'ng-zorro-antd/switch';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {
  FilterAutomatedTestTechnologyComponent
} from './components/filter-automated-test-technology/filter-automated-test-technology.component';


@NgModule({
    imports: [
        CommonModule,
        TranslateModule.forChild(),
        WorkspaceCommonModule,
        NzInputModule,
        NzButtonModule,
        FormsModule,
        NzIconModule,
        NzSelectModule,
        NzInputNumberModule,
        NzDatePickerModule,
        NzTabsModule,
        UiManagerModule,
        NzSwitchModule,
        NzToolTipModule,
    ],
    declarations: [
        FilterFieldComponent,
        EditFilterTextComponent,
        FilterProjectComponent,
        MultiValueCufFilterComponent,
        NumericFilterComponent,
        OperationSelectorComponent,
        DefaultValueRendererComponent,
        WithOperationFilterValueRendererComponent,
        DateFilterComponent,
        DateFilterValueRendererComponent,
        FilterTestCaseMilestoneLabelComponent,
        CheckboxCufFilterComponent,
        FilterEnumComponent,
        FilterEnumSingleSelectComponent,
        FilterInfolistComponent,
        FilterUserHistoryComponent,
        NumericSetFilterComponent,
        FilterUserAssigneeComponent,
        BooleanFilterComponent,
        FilterMultiValueComponent,
        FilterExecutionStatusComponent,
        FilterAutomatedTestTechnologyComponent,
    ],
    exports: [
        FilterFieldComponent,
        EditFilterTextComponent,
        FilterProjectComponent,
        MultiValueCufFilterComponent,
        NumericFilterComponent,
        WithOperationFilterValueRendererComponent,
        FilterTestCaseMilestoneLabelComponent,
        CheckboxCufFilterComponent,
        FilterEnumComponent,
        FilterUserHistoryComponent,
        OperationSelectorComponent,
        DateFilterComponent,
        BooleanFilterComponent,
        FilterMultiValueComponent
    ]
})
export class FiltersModule {
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {
  UserListElement
} from '../../../workspace-common/components/forms/user-list-selector/user-list-selector.component';
import {
  DiscreteFilterValue,
  Filter,
  MultiDiscreteFilterValue,
  NULL_VALUE_IN_FILTER,
  Scope
} from '../../state/filter.state';
import {UserHistorySearchProvider} from '../../user-history-search-provider.service';
import {take} from 'rxjs/operators';
import {AbstractFilterWidget} from '../abstract-filter-widget';

@Component({
  selector: 'sqtm-core-filter-user-assignee',
  templateUrl: './filter-user-assignee.component.html',
  styleUrls: ['./filter-user-assignee.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterUserAssigneeComponent extends AbstractFilterWidget implements OnInit {

  private readonly UNASSIGNED: UserListElement = {
    login: NULL_VALUE_IN_FILTER,
    selected: false
  };

  users: UserListElement[];

  private filterValue: DiscreteFilterValue[];

  constructor(protected cdRef: ChangeDetectorRef, private userProvider: UserHistorySearchProvider) {
    super(cdRef);
  }

  ngOnInit() {
  }

  setFilter(filter: Filter, scope: Scope): any {
    this.userProvider.provideUserList(filter.columnPrototype).pipe(
      take(1),
    ).subscribe(providedUsers => {
      const selectedUserIds = (filter.value as MultiDiscreteFilterValue).value.map(v => v.id);
      this.users = providedUsers.filter(user => user.login !== null).map(user => ({
        ...user, selected: selectedUserIds.includes(user.login)
      }));
      this.users = [{...this.UNASSIGNED, selected: selectedUserIds.includes(NULL_VALUE_IN_FILTER)}, ...this.users];
      this.filterValue = (filter.value as MultiDiscreteFilterValue).value;
      this.cdRef.detectChanges();
    });
  }

  changeFilterValue($event: UserListElement) {
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if ($event.selected) {
      nextValue = this.filterValue.concat({id: $event.login, label: $event.login});
    } else {
      const index = this.filterValue.findIndex(v => v.id === $event.login);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({
      filterValue: {
        kind: 'multiple-discrete-value',
        value: nextValue
      }
    });
  }
}

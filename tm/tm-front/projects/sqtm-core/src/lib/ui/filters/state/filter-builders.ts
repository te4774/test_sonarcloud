import {Type} from '@angular/core';
import {DiscreteFilterValue, FilterOperation, FilterValue, ResearchColumnPrototype} from './filter.state';
import {AbstractFilterWidget} from '../components/abstract-filter-widget';
import {GridFilter} from '../../grid/model/state/filter.state';
import {EditFilterTextComponent} from '../components/edit-filter-text/edit-filter-text.component';
import {NumericFilterComponent} from '../components/numeric-filter/numeric-filter.component';
import {AbstractFilterValueRenderer} from '../components/value-renderers/abstract-filter-value-renderer';
import {
  WithOperationFilterValueRendererComponent
} from '../components/value-renderers/with-operation-filter-value-renderer/with-operation-filter-value-renderer.component';
import {DateFilterComponent} from '../components/date-filter/date-filter.component';
import {
  DateFilterValueRendererComponent
} from '../components/value-renderers/date-filter-value-renderer/date-filter-value-renderer.component';
import {FilterEnumComponent} from '../components/filter-enum/filter-enum.component';
import {I18nEnumItem} from '../../../model/level-enums/level-enum';
import {
  FilterEnumSingleSelectComponent
} from '../components/filter-enum-single-select/filter-enum-single-select.component';
import {FilterInfolistComponent} from '../components/filter-infolist/filter-infolist.component';
import {FilterUserHistoryComponent} from '../components/filter-user-history/filter-user-history.component';
import {NumericSetFilterComponent} from '../components/numeric-set-filter/numeric-set-filter.component';
import {FilterUserAssigneeComponent} from '../components/filter-user-assignee/filter-user-assignee.component';
import {BooleanFilterComponent} from '../components/boolean-filter/boolean-filter.component';
import {FilterMultiValueComponent} from '../components/filter-multi-value/filter-multi-value.component';
import {FilterExecutionStatusComponent} from '../components/filter-execution-status/filter-execution-status.component';
import {
  FilterAutomatedTestTechnologyComponent
} from '../components/filter-automated-test-technology/filter-automated-test-technology.component';

export class FilterBuilder {
  private availableOperations: FilterOperation[] = [];
  private operation: FilterOperation;
  private initialValue: FilterValue;
  private widget: Type<AbstractFilterWidget>;
  private valueRenderer: Type<AbstractFilterValueRenderer>;
  private columnPrototype: ResearchColumnPrototype;
  private groupId: string;
  private i18nLabelKey: string;
  private tiedToPerimeter = false;
  private _alwaysActive = false;
  private preventOpening = false;

  constructor(private id: string) {
  }

  alwaysActive() {
    this._alwaysActive = true;
    this.preventOpening = true;
    return this;
  }

  withAvailableOperations(availableOperations: FilterOperation[]): FilterBuilder {
    this.availableOperations = availableOperations;
    return this;
  }

  withOperations(operation: FilterOperation): FilterBuilder {
    this.operation = operation;
    return this;
  }

  withInitialValue(initialValue: FilterValue): FilterBuilder {
    this.initialValue = initialValue;
    return this;
  }

  withInitialTextValue(initialTextValue: string): FilterBuilder {
    this.initialValue = {kind: 'single-string-value', value: initialTextValue};
    return this;
  }

  withInitialNumberListValue(initialList: number[]): FilterBuilder {
    this.initialValue = {kind: 'multiple-numeric-value', value: initialList};
    return this;
  }

  withWidget(widget: Type<AbstractFilterWidget>): FilterBuilder {
    this.widget = widget;
    return this;
  }

  withValueRenderer(renderer: Type<AbstractFilterValueRenderer>): FilterBuilder {
    this.valueRenderer = renderer;
    return this;
  }


  withColumnPrototype(columnPrototype: ResearchColumnPrototype): FilterBuilder {
    this.columnPrototype = columnPrototype;
    return this;
  }

  withGroupId(groupId: string): FilterBuilder {
    this.groupId = groupId;
    return this;
  }

  withI18nKey(i18nLabelKey: string): FilterBuilder {
    this.i18nLabelKey = i18nLabelKey;
    return this;
  }

  withTiedToPerimeter(): FilterBuilder {
    this.tiedToPerimeter = true;
    return this;
  }

  build(): GridFilter {
    return {
      id: this.id,
      i18nLabelKey: this.i18nLabelKey,
      availableOperations: this.availableOperations,
      operation: this.operation,
      initialValue: this.initialValue,
      columnPrototype: this.columnPrototype,
      widget: this.widget,
      active: this._alwaysActive,
      groupId: this.groupId,
      tiedToPerimeter: this.tiedToPerimeter,
      valueRenderer: this.valueRenderer,
      alwaysActive: this._alwaysActive,
      preventOpening: this.preventOpening,
    };
  }
}

export function buildFilters(filters: FilterBuilder[]): GridFilter[] {
  return filters.map(filter => filter.build());
}

export function textResearchFilter(id: string, columnPrototype: ResearchColumnPrototype): FilterBuilder {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withAvailableOperations([FilterOperation.LIKE])
    .withOperations(FilterOperation.LIKE)
    .withInitialTextValue('')
    .withWidget(EditFilterTextComponent);
}

export function numericResearchFilter(id: string, columnPrototype: ResearchColumnPrototype): FilterBuilder {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withAvailableOperations([
      FilterOperation.EQUALS,
      FilterOperation.GREATER,
      FilterOperation.GREATER_EQUAL,
      FilterOperation.LOWER,
      FilterOperation.LOWER_EQUAL,
      FilterOperation.BETWEEN,
      FilterOperation.NOT_EQUALS])
    .withOperations(FilterOperation.EQUALS)
    .withInitialTextValue(null)
    .withWidget(NumericFilterComponent)
    .withValueRenderer(WithOperationFilterValueRendererComponent);
}

export function idResearchFilter(id: string, columnPrototype: ResearchColumnPrototype): FilterBuilder {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withAvailableOperations([FilterOperation.EQUALS])
    .withOperations(FilterOperation.EQUALS)
    .withInitialTextValue(null)
    .withWidget(NumericFilterComponent);
}

export function idListSearchFilter(id: string, columnPrototype: ResearchColumnPrototype): FilterBuilder {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withAvailableOperations([FilterOperation.IN])
    .withOperations(FilterOperation.IN)
    .withInitialNumberListValue([])
    .withWidget(NumericSetFilterComponent);
}

export function dateFilter(id: string, columnPrototype: ResearchColumnPrototype): FilterBuilder {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withAvailableOperations([
      FilterOperation.EQUALS,
      FilterOperation.GREATER,
      FilterOperation.GREATER_EQUAL,
      FilterOperation.LOWER,
      FilterOperation.LOWER_EQUAL,
      FilterOperation.BETWEEN])
    .withOperations(FilterOperation.EQUALS)
    .withInitialValue({kind: 'single-date-value', value: ''})
    // .withInitialTextValue(null)
    .withWidget(DateFilterComponent)
    .withValueRenderer(DateFilterValueRendererComponent);
}

export function infoListResearchFilter(
  id: string, columnPrototype: ResearchColumnPrototype): FilterBuilder {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withWidget(FilterInfolistComponent)
    .withAvailableOperations([FilterOperation.IN])
    .withOperations(FilterOperation.IN)
    .withInitialValue({kind: 'multiple-discrete-value', value: []})
    .withGroupId('attributes')
    .withTiedToPerimeter();
}

export function multipleListResearchFilter(
  id: string, columnPrototype: ResearchColumnPrototype, widget: Type<AbstractFilterWidget>): FilterBuilder {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withWidget(widget)
    .withAvailableOperations([FilterOperation.IN])
    .withOperations(FilterOperation.IN)
    .withInitialValue({kind: 'multiple-discrete-value', value: []});
}

export function userHistoryResearchFilter(id: string, columnPrototype: ResearchColumnPrototype): FilterBuilder {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withWidget(FilterUserHistoryComponent)
    .withAvailableOperations([FilterOperation.IN])
    .withOperations(FilterOperation.IN)
    .withInitialValue({kind: 'multiple-discrete-value', value: []});
}

export function assigneeFilter(id: string, columnPrototype: ResearchColumnPrototype): FilterBuilder {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withWidget(FilterUserAssigneeComponent)
    .withAvailableOperations([FilterOperation.IN])
    .withOperations(FilterOperation.IN)
    .withInitialValue({kind: 'multiple-discrete-value', value: []});
}

export function i18nEnumResearchFilter(
  id: string, columnPrototype: ResearchColumnPrototype): FilterBuilder {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withWidget(FilterEnumComponent)
    .withAvailableOperations([FilterOperation.IN])
    .withOperations(FilterOperation.IN)
    .withInitialValue({kind: 'multiple-discrete-value', value: []});
}

export function i18nEnumSingleSelectionResearchFilter(
  id: string, columnPrototype: ResearchColumnPrototype, initialValue?: I18nEnumItem<any>): FilterBuilder {
  let value: DiscreteFilterValue[] = [];
  if (Boolean(initialValue)) {
    value = [{
      id: initialValue.id,
      i18nLabelKey: initialValue.i18nKey
    }];
  }

  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withWidget(FilterEnumSingleSelectComponent)
    .withAvailableOperations([FilterOperation.IN])
    .withOperations(FilterOperation.IN)
    .withInitialValue({kind: 'multiple-discrete-value', value});
}

export function fullTextResearchFilter(id: string, columnPrototype: ResearchColumnPrototype) {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withWidget(EditFilterTextComponent)
    .withInitialTextValue('')
    .withOperations(FilterOperation.FULLTEXT)
    .withAvailableOperations([FilterOperation.FULLTEXT]);
}

export function serverBackedGridTextFilter(id: string) {
  return new FilterBuilder(id)
    .alwaysActive()
    .withWidget(EditFilterTextComponent)
    .withInitialTextValue('')
    .withOperations(FilterOperation.LIKE)
    .withAvailableOperations([FilterOperation.LIKE]);
}


export function serverBackedGridDateRangeFilter(id: string): FilterBuilder {
  return new FilterBuilder(id)
    .alwaysActive()
    .withAvailableOperations([FilterOperation.BETWEEN])
    .withOperations(FilterOperation.BETWEEN)
    .withInitialValue({kind: 'multiple-date-value', value: []})
    .withWidget(DateFilterComponent)
    .withValueRenderer(DateFilterValueRendererComponent);
}

export function serverBackedGridEqualFilter(id: string) {
  return new FilterBuilder(id)
    .alwaysActive()
    .withWidget(EditFilterTextComponent)
    .withInitialTextValue('')
    .withOperations(FilterOperation.EQUALS)
    .withAvailableOperations([FilterOperation.EQUALS]);
}

export function serverBackedGridMultiValueFilter(id: string) {
  return new FilterBuilder(id)
    .alwaysActive()
    .withWidget(FilterMultiValueComponent)
    .withAvailableOperations([FilterOperation.IN])
    .withOperations(FilterOperation.IN)
    .withInitialValue({kind: 'multiple-discrete-value', value: []});
}

export function booleanFilter(
  id: string, initialValue?: boolean[]): FilterBuilder {
  let value: boolean[] = [];
  if (initialValue != null) {
    value = initialValue;
  }

  return new FilterBuilder(id)
    .withWidget(BooleanFilterComponent)
    .withAvailableOperations([FilterOperation.IN])
    .withOperations(FilterOperation.IN)
    .withInitialValue({kind: 'multiple-boolean-value', value});
}

export function executionStatusFilter(id: string, columnPrototype: ResearchColumnPrototype): FilterBuilder {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withWidget(FilterExecutionStatusComponent)
    .withAvailableOperations([FilterOperation.IN])
    .withOperations(FilterOperation.IN)
    .withInitialValue({kind: 'multiple-discrete-value', value: []});
}

export function automatedTestTechnologyResearchFilter(id: string, columnPrototype: ResearchColumnPrototype): FilterBuilder {
  return new FilterBuilder(id)
    .withColumnPrototype(columnPrototype)
    .withWidget(FilterAutomatedTestTechnologyComponent)
    .withAvailableOperations([FilterOperation.IN])
    .withOperations(FilterOperation.IN)
    .withInitialValue({kind: 'multiple-discrete-value', value: []});
}

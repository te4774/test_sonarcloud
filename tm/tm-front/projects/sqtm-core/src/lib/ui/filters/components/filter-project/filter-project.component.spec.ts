import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {FilterProjectComponent} from './filter-project.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';

describe('FilterProjectComponent', () => {
  let component: FilterProjectComponent;
  let fixture: ComponentFixture<FilterProjectComponent>;

  const referentialDataServiceMock = jasmine.createSpyObj(['load']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FilterProjectComponent],
      providers: [
        {provide: ReferentialDataService, useValue: referentialDataServiceMock}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {AbstractFilterWidget} from '../abstract-filter-widget';
import {Filter, FilterOperation, isMultiNumericValue} from '../../state/filter.state';
import {Workspaces} from '../../../ui-manager/theme.model';
import {KeyCodes} from '../../../utils/key-codes';

@Component({
  selector: 'sqtm-core-numeric-set-filter',
  templateUrl: './numeric-set-filter.component.html',
  styleUrls: ['./numeric-set-filter.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NumericSetFilterComponent extends AbstractFilterWidget implements OnInit, OnDestroy {

  @ViewChild('input', {static: true}) input: ElementRef;

  integersOnly = true;

  stringValue = '';
  operation: FilterOperation;
  workspace: Workspaces;

  constructor(protected cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  ngOnInit() {
    this.input.nativeElement.focus();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  setFilter(filter: Filter) {
    this._filter = filter;
    this.operation = filter.operation;
    const filterValue = filter.value;
    this.workspace = filter.uiOptions?.workspace;
    if (isMultiNumericValue(filterValue)) {
      this.stringValue = filterValue.value.join(', ');
      this.cdRef.detectChanges();
    } else {
      throw Error('Not handled kind ' + filterValue.kind);
    }
  }

  private parseNumericSet(value: string): number[] {
    if (!(typeof value === 'string')) {
      return null;
    }

    const set: number[] = [];

    value.split(',')
      .map(str => str?.trim())
      .filter(str => Boolean(str))  // Empty strings are falsy
      .map(str => this.convertToNumber(str))
      .filter(numOrNull => !isNaN(numOrNull))
      .forEach(num => {
        if (!set.includes(num)) {
          set.push(num);
        }
      });

    // Default compare won't work !! e.g "3 > 12"
    set.sort((a, b) => a - b);
    return set;
  }

  private convertToNumber(str: string): number {
    if (this.integersOnly) {
      return Number.parseInt(str, 10);
    } else {
      let parsedValue: number;
      if (str.includes('.')) {
        parsedValue = Number.parseFloat(str);
      } else {
        parsedValue = Number.parseInt(str, 10);
      }
      return parsedValue;
    }
  }

  closeTextField() {
    this.close.next();
  }

  changeValue() {
    if (this.stringValue != null) {
      this.filteringChanged.next({
        filterValue: {
          kind: 'multiple-numeric-value',
          value: this.parseNumericSet(this.input.nativeElement.value),
        },
        operation: this.operation
      });
    }
    this.closeTextField();
  }

  changeOperation(newOperation: FilterOperation) {
    this.operation = newOperation;
    this.cdRef.markForCheck();
  }

  handleKeyboardInput(event: KeyboardEvent) {
    if (event.keyCode === KeyCodes.ENTER) {
      this.changeValue();
      this.closeTextField();
    } else if (event.keyCode === KeyCodes.ESC) {
      this.closeTextField();
    }
    return false;
  }
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractFilterValueRenderer} from '../abstract-filter-value-renderer';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-core-default-value-renderer',
  templateUrl: './default-value-renderer.component.html',
  styleUrls: ['./default-value-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefaultValueRendererComponent extends AbstractFilterValueRenderer implements OnInit {

  constructor(protected cdRef: ChangeDetectorRef, protected translateService: TranslateService) {
    super(cdRef, translateService);
  }

  ngOnInit() {
  }

}

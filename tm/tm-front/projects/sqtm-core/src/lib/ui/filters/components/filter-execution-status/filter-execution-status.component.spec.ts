import {ComponentFixture, TestBed} from '@angular/core/testing';

import {FilterExecutionStatusComponent} from './filter-execution-status.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';

describe('FilterExecutionStatusComponent', () => {
  let component: FilterExecutionStatusComponent;
  let fixture: ComponentFixture<FilterExecutionStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [ FilterExecutionStatusComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterExecutionStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractFilterWidget} from '../abstract-filter-widget';
import {ListItem} from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';
import {Workspaces} from '../../../ui-manager/theme.model';
import {DiscreteFilterValue, Filter, isDiscreteValue} from '../../state/filter.state';
import {
  CanonicalExecutionStatus,
  ExecutionStatus,
  ExecutionStatusLevelEnumItem,
  I18nEnumItem
} from '../../../../model/level-enums/level-enum';

@Component({
  selector: 'sqtm-core-filter-execution-status',
  templateUrl: './filter-execution-status.component.html',
  styleUrls: ['./filter-execution-status.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterExecutionStatusComponent extends AbstractFilterWidget {

  showAutomatedStatuses = false;
  items: ListItem[];
  workspace: Workspaces;

  protected filterValue: DiscreteFilterValue[];

  constructor(protected cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  get i18nEnumItems(): I18nEnumItem<any>[] {
    return this.showAutomatedStatuses ? Object.values(ExecutionStatus) : CanonicalExecutionStatus;
  }

  changeShowAutomatedStatuses(shouldShowAutomatedStatuses: boolean): void {
    this.showAutomatedStatuses = shouldShowAutomatedStatuses;

    const currentIds = this.filterValue.map(value => value.id);
    this.items = this.i18nEnumItems.map((levelEnumItem: ExecutionStatusLevelEnumItem<any>) => ({
      id: levelEnumItem.id,
      selected: currentIds.includes(levelEnumItem.id),
      i18nLabelKey: levelEnumItem.i18nKey
    }));

    // Deselect items that are not visible
    const nextValue = this.filterValue.filter(value => this.items.map(item => item.id).includes(value.id));
    if (this.filterValue.length !== nextValue.length) {
      this.filteringChanged.next({filterValue: {kind: 'multiple-discrete-value', value: nextValue}});
    }

    this.cdRef.detectChanges();
  }

  changeSelection($event: ListItem) {
    const i18nEnumItem = this.i18nEnumItems.find(item => item.id === $event.id);
    let nextValue: DiscreteFilterValue[] = [...this.filterValue];
    if ($event.selected) {
      nextValue = this.filterValue.concat({id: i18nEnumItem.id, i18nLabelKey: i18nEnumItem.i18nKey});
    } else {
      const index = this.filterValue.findIndex(v => v.id === $event.id);
      if (index >= 0) {
        nextValue.splice(index, 1);
      }
    }
    this.filteringChanged.next({filterValue: {kind: 'multiple-discrete-value', value: nextValue}});
  }

  setFilter(filter: Filter) {
    const filterValue = filter.value;
    if (isDiscreteValue(filterValue)) {
      this._filter = filter;
      const ids = filterValue.value.map(value => value.id);
      this.items = this.i18nEnumItems.map(levelEnumItem => {
        return {
          id: levelEnumItem.id,
          selected: ids.includes(levelEnumItem.id),
          i18nLabelKey: levelEnumItem.i18nKey
        };
      });
      this.filterValue = filterValue.value;
      this.workspace = filter.uiOptions?.workspace;

      // Set showAutomatedStatus to 'true' if current value contains non-canonical statuses
      const canonicalIds = CanonicalExecutionStatus.map(status => status.id);
      const currentIds = this.filterValue.map(value => value.id);
      const areAutomatedStatusesVisible = currentIds.some(id => ! canonicalIds.includes(id as any));
      if (areAutomatedStatusesVisible) {
        this.changeShowAutomatedStatuses(true);
      }

      this.cdRef.detectChanges();
    } else {
      throw Error('Not handled kind ' + filter.value.kind);
    }
    this.cdRef.detectChanges();
  }
}

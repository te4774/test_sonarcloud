import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {OperationSelectorComponent} from './operation-selector.component';
import {FormsModule} from '@angular/forms';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('OperationSelectorComponent', () => {
  let component: OperationSelectorComponent;
  let fixture: ComponentFixture<OperationSelectorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, NzSelectModule, TestingUtilsModule, NoopAnimationsModule],
      declarations: [OperationSelectorComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {ResearchColumnPrototype} from './state/filter.state';
import {UserListElement} from '../workspace-common/components/forms/user-list-selector/user-list-selector.component';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable()
export abstract class UserHistorySearchProvider {
  abstract provideUserList(columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]>;
}

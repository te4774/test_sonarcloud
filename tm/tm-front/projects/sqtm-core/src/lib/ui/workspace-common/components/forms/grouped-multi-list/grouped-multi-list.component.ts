import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef
} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Workspaces} from '../../../../ui-manager/theme.model';

export interface ListItem {
  id: string | number;
  selected?: boolean;
  i18nLabelKey?: string;
  groupId?: string;
  label?: string;
  customTemplate?: TemplateRef<void>;
}

export interface ListGroup {
  id: string;
  i18nLabelKey?: string;
  label?: string;
}

interface ListItemDisplay {
  id: string | number;
  label: string;
  groupId?: string;
  selected: boolean;
  customTemplate?: TemplateRef<void>;
}

interface ListGroupDisplay {
  id: string;
  label: string;
  groupId?: string;
  items: ListItemDisplay[];
}

interface GroupedListItem extends ListGroup {
  items: ListItem[];
}

export interface GroupedMultiListOptions {
  filterable: boolean;
}

@Component({
  selector: 'sqtm-core-grouped-multi-list',
  templateUrl: './grouped-multi-list.component.html',
  styleUrls: ['./grouped-multi-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupedMultiListComponent implements OnInit, OnChanges {

  readonly rowHeight = 29;
  readonly spacerHeight = 10;
  filteredUngroupedItems: ListItemDisplay[] = [];
  filteredGroupDisplay: ListGroupDisplay[] = [];

  allItemDisplays: ListItemDisplay[] = [];
  allGroupDisplay: ListGroupDisplay[] = [];
  selectedItemsToDisplay: ListItemDisplay[] = [];
  private mustUpdateList = false;

  @Input()
  set listGroups(listGroups: ListGroup[]) {
    this._listGroups = listGroups;
    this.markAsDirty();
  }

  @Input()
  set listItems(listItems: ListItem[]) {
    this._listItems = listItems;
    this.markAsDirty();
  }

  @Input()
  width = 250;

  @Input()
  height = 500;

  // If true ungrouped elements will be put in a virtual scroll view port
  // Grouped items WIL NOT BE RENDERED
  // To do that we should flat the tree into a simple list of rendered items, like rows in grid
  // Not for today...
  @Input()
  virtualScroll = false;

  @Input()
  boxShadow = '1px 1px 1px 1px rgba(0, 0, 0, 0.1)';

  private _separateSelectedItems = false;

  get separateSelectedItems(): boolean {
    return this._separateSelectedItems;
  }


  @Input()
  set separateSelectedItems(value: boolean) {
    this._separateSelectedItems = value;
    this.markAsDirty();
  }

  @Input()
  searchable = true;

  @Input()
  numberOfSelectedItemsToDisplay = 5;

  @Input()
  numberOfUngroupedItemsToDisplay = 5;

  @Output()
  itemSelectionChanged = new EventEmitter<ListItem>();

  @Input()
  workspace: Workspaces;

  private _options: GroupedMultiListOptions;

  private _listItems: ListItem[] = [];

  private _listGroups: ListGroup[] = [];

  researchValue: string;



  get minSelectedItemsHeight(): number {
    const nbItemMin = Math.min(this.selectedItemsToDisplay.length, this.numberOfSelectedItemsToDisplay);
    return nbItemMin * this.rowHeight + this.spacerHeight;
  }

  get minAvailableItemsHeight(): number {
    const nbItemMin = Math.min(this.filteredUngroupedItems.length, this.numberOfUngroupedItemsToDisplay);
    return nbItemMin * this.rowHeight;
  }

  get hasSelectItemsToShow() {
    return this.separateSelectedItems && this.selectedItemsToDisplay && this.selectedItemsToDisplay.length > 0;
  }

  constructor(private translateService: TranslateService, private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  private updateBaseList() {
    // create the definitive list of items (aka translated...)
    this.allItemDisplays = this._listItems
      .map(listItem => {
        const label = listItem.i18nLabelKey ? this.translateService.instant(listItem.i18nLabelKey) : listItem.label;
        const itemDisplay: ListItemDisplay = {
          ...listItem,
          selected: listItem.selected,
          label,
        };
        return itemDisplay;
      });

    this.allGroupDisplay = this._listGroups
      .map(listGroup => {
        const label = listGroup.i18nLabelKey ? this.translateService.instant(listGroup.i18nLabelKey) : listGroup.label;
        const itemsForGroup = this.allItemDisplays.filter(item => item.groupId === listGroup.id);
        const groupDisplay: ListGroupDisplay = {
          ...listGroup,
          label,
          items: itemsForGroup
        };
        return groupDisplay;
      });

    if (this.virtualScroll && this.allGroupDisplay && this.allGroupDisplay.length > 0) {
      throw Error('Cannot use virtual scroll with groups');
    }

    this.updateFilteredList();
  }

  private updateFilteredList() {
    let filteredItems: ListItemDisplay[];
    if (Boolean(this.researchValue)) {
      filteredItems = this.allItemDisplays
        .filter(item => item.label.toLowerCase().includes(this.researchValue.toLowerCase()));
    } else {
      filteredItems = this.allItemDisplays;
    }
    this.dispatchItems(filteredItems);
  }

  private dispatchItems(filteredItems: ListItemDisplay[]) {
    let itemsToDisplay = filteredItems;
    if (this._separateSelectedItems) {
      itemsToDisplay = filteredItems.filter(item => !item.selected);
      this.selectedItemsToDisplay = filteredItems.filter(item => item.selected);
    }
    this.filteredUngroupedItems = itemsToDisplay.filter(item => item.groupId == null);
    this.filteredGroupDisplay = this.allGroupDisplay.map(group => {
      const groupItems = itemsToDisplay.filter(item => item.groupId === group.id);
      return {...group, items: groupItems};
    }).filter(group => group.items.length > 0);
  }

  handleKeyboardInput() {
    this.updateFilteredList();
    this.cdRef.detectChanges();
  }

  toggleItem(id: string) {
    const listItem = this._listItems.find(item => item.id === id);
    this.itemSelectionChanged.next({...listItem, selected: !listItem.selected});
  }

  trackItemById(index: number, item: ListItem) {
    return item.id;
  }

  private markAsDirty() {
    this.mustUpdateList = true;
  }

  private markAsUpdated() {
    this.mustUpdateList = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.mustUpdateList) {
      this.updateBaseList();
      this.markAsUpdated();
    }
  }
}

import {FormGroup} from '@angular/forms';

export function passwordLengthValidator(passwordFieldName: string): any {
  return (group: FormGroup) => {
    const password = group.get(passwordFieldName).value;

    if (password.length > 0 && password.length < 6 || password.length > 255) {
      group.controls[passwordFieldName].setErrors({passwordLength: true});
    }

    return null;
  };
}

export function passwordConfirmationValidator(passwordFieldName: string, confirmFieldName: string): any {
  return (group: FormGroup) => {
    const password = group.get(passwordFieldName).value;
    const confirmPassword = group.get(confirmFieldName).value;
    if (confirmPassword.length > 0 && password !== confirmPassword) {
      group.controls[confirmFieldName].setErrors({passwordsNotMatching: true});
    }
    return null;
  };
}

import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';


@Component({
  selector: 'sqtm-core-editable-draggable-tag-field',
  templateUrl: './editable-draggable-tag-field.component.html',
  styleUrls: ['./editable-draggable-tag-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableDraggableTagFieldComponent {

  inputVisible = false;
  inputValue = '';

  @Input()
  options: string[] = [];

  @Input()
  placeholder: string;

  _required =  false;

  @Output()
  editTagEvent = new EventEmitter<string[]>();

  @ViewChild('inputElement', { static: false }) inputElement?: ElementRef;

  constructor() {
  }

  @Input()
  set required(isRequired: boolean)  {
    this._required = isRequired;
  }

  get required() {
    return this._required;
  }

  public getNzMode(): 'default' | 'closeable' {
    if (this.required) {
      return this.options.length === 1 ? 'default' : 'closeable';
    } else {
      return 'closeable';
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.options, event.previousIndex, event.currentIndex);
    this.editTagEvent.emit(this.options);
  }

  handleClose(removedTag: {}): void {
    this.options = this.options.filter(option => option !== removedTag);
    this.editTagEvent.emit(this.options);
  }

  showInput(): void {
    this.inputVisible = true;
    setTimeout(() => {
      this.inputElement?.nativeElement.focus();
    }, 10);
  }

  handleInputConfirm(): void {
    if (this.inputValue && this.options.indexOf(this.inputValue) === -1) {
      this.options = [...this.options, this.inputValue];
      this.editTagEvent.emit(this.options);
    }

    this.inputValue = '';
    this.inputVisible = false;
  }
}

import {AfterViewInit, Directive, Host, HostListener, Input, OnDestroy, OnInit} from '@angular/core';
import {SqtmEntityState} from '../../../core/services/entity-view/entity-view.state';
import {Subject} from 'rxjs';
import {EntityViewService} from '../../../core/services/entity-view/entity-view.service';
import {distinctUntilChanged, filter, map, pluck, takeUntil} from 'rxjs/operators';
import {SimplePermissions} from '../../../model/permissions/simple-permissions';
import {InfoListItem} from '../../../model/infolist/infolistitem.model';
import {
  EditableSelectInfolistFieldComponent
} from '../components/editables/editable-select-infolist-field/editable-select-infolist-field.component';


// Note about change detection :
// We need to marksForCheck child decorated component because father component is not always updated when XhrRequest came back from server.
// As the father component is often a complex view, with no @Input, angular stop change detection.
// However, as this directive is meant to be used in a XhrRequest context, a change detection cycle will be automatically triggered,
// When all callback have executed. One of this call back set new value in componentData$.
// We just need to indicate to Angular that this component must be checked.
@Directive({
  selector: '[sqtmCoreEntityViewEditableInfolistDirective]'
})
export class EntityViewEditableInfolistDirective<E extends SqtmEntityState, T extends string, P extends SimplePermissions>
  implements OnInit, AfterViewInit, OnDestroy {

  @Input('sqtmCoreEntityViewEditableInfolistDirective')
  fieldName: keyof E;

  unsub$ = new Subject<void>();

  constructor(private entityViewService: EntityViewService<E, T, P>, @Host() private editable: EditableSelectInfolistFieldComponent) {

  }

  @HostListener('confirmEvent', ['$event'])
  onConfirm(infoListItem: InfoListItem) {
    this.editable.beginAsync();
    this.entityViewService.update(this.fieldName, infoListItem.id as any); // cannot use type system on dynamic inputs...
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.entityViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => componentData[componentData.type]),
      pluck(this.fieldName as string),
      filter(value => Boolean(value)),
      distinctUntilChanged()
    ).subscribe((infolistItemId: number) => {
      this.editable.selectedItem = infolistItemId;
      this.editable.markForCheck();
    });

    this.entityViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      filter(componentData => Boolean(componentData.permissions)),
      map(componentData => componentData.permissions.canWrite && componentData.milestonesAllowModification),
      distinctUntilChanged()
    ).subscribe((editionAllowed: boolean) => {
      this.editable.editable = editionAllowed && this.editable.editable;
      this.editable.markForCheck();
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {EditableCustomField} from '../../../../custom-field/editable-custom-field';

@Component({
  selector: 'sqtm-core-editable-check-box',
  templateUrl: './editable-check-box.component.html',
  styleUrls: ['./editable-check-box.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableCheckBoxComponent implements OnInit, EditableCustomField {

  autoAsync = false;

  model: boolean;

  private _value;

  @Output()
  confirmEvent = new EventEmitter<boolean>();

  @Input()
  set value(checked: boolean) {
    this._value = checked;
    this.model = checked;
    this.cdRef.detectChanges();
  }

  get value() {
    return this._value;
  }

  @Input()
  editable = true;

  constructor(public cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  beginAsync() {
  }

  cancel() {
  }

  confirm() {
    this.confirmEvent.emit(this.model);
  }

  disableEditMode() {
  }

  enableEditMode() {
  }

  endAsync() {
  }

  markForCheck() {
    this.cdRef.markForCheck();
  }

  getFakeBoxClass() {
    return this._value ? ['ant-checkbox-checked'] : [];
  }


}

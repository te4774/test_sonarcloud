import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldValidationError} from '../../../../../model/error/error.model';
import {AbstractFormField} from '../abstract-form-field';

@Component({
  selector: 'sqtm-core-numeric-field',
  templateUrl: './numeric-field.component.html',
  styleUrls: ['./numeric-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NumericFieldComponent extends AbstractFormField {

  @Input() formGroup: FormGroup;
  @Input() fieldName: string;
  @Input() step = 1; // Default value for NgZorro InputNumber. Do NOT set undefined/null !
  @Input() precision = 3;

  @Input()
  set serverSideFieldValidationError(fieldsValidationErrors: FieldValidationError[]) {
    this.showServerSideError(fieldsValidationErrors);
  }

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }
}

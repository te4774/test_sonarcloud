import {Directive, Host, HostListener, Input} from '@angular/core';
import {GenericEntityViewService} from '../../../core/services/genric-entity-view/generic-entity-view.service';
import {SqtmGenericEntityState} from '../../../core/services/genric-entity-view/generic-entity-view-state';
import {EditableDateFieldComponent} from '../components/editables/editable-date-field/editable-date-field.component';
import {AbstractGenericViewEditableField} from './abstract-generic-view-editable-field';
import {format, parse} from 'date-fns';
import {CUF_DATE_FORMAT} from '../../custom-field/custom-field-value.utils';

// Note : this directive assumes we're working with dates following the same formatting
// as custom field (that is yyyy-MM-dd format)

@Directive({
  selector: '[sqtmCoreGenericViewEditableDateCufValue]'
})
export class GenericViewEditableDateCufValueDirective<E extends SqtmGenericEntityState, T extends string>
  extends AbstractGenericViewEditableField<EditableDateFieldComponent, E, T> {

  @Input('sqtmCoreGenericViewEditableDateCufValue')
  fieldName: keyof E;

  constructor(protected genericEntityViewService: GenericEntityViewService<E, T>,
              @Host() protected editable: EditableDateFieldComponent) {
    super(genericEntityViewService, editable);
  }

  protected showExternalErrorMessages(errorMessages: string[]): void {
  }

  protected setValue(newValue: any): void {
    if (typeof newValue === 'string' && newValue.trim() !== '') {
      this.editable.value = parse(newValue, CUF_DATE_FORMAT, new Date());
    } else {
      this.editable.value = newValue ?? '';
    }
  }

  @HostListener('confirmEvent', ['$event'])
  onConfirm(value: Date) {
    const asString = value ? format(value, CUF_DATE_FORMAT) : '';
    this.editable.beginAsync();
    this.genericViewService.update(this.fieldName as any, asString);
  }
}

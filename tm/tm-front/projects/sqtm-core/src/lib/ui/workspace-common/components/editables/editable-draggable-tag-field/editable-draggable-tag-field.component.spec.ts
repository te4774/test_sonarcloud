import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {EditableDraggableTagFieldComponent} from './editable-draggable-tag-field.component';

describe('EditableDraggableTagFieldComponent', () => {
  let component: EditableDraggableTagFieldComponent;
  let fixture: ComponentFixture<EditableDraggableTagFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EditableDraggableTagFieldComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableDraggableTagFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

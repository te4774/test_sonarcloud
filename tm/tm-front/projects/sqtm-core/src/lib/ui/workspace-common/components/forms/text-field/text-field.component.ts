import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldValidationError} from '../../../../../model/error/error.model';
import {AbstractFormField} from '../abstract-form-field';

@Component({
  selector: 'sqtm-core-text-field',
  templateUrl: './text-field.component.html',
  styleUrls: ['./text-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextFieldComponent extends AbstractFormField implements AfterViewInit {

  @Input()
  formGroup: FormGroup;

  @Input()
  fieldName: string;

  @Input()
  set serverSideFieldValidationError(fieldsValidationErrors: FieldValidationError[]) {
    this.showServerSideError(fieldsValidationErrors);
  }

  /** If set to true, it will give the focus to this component at page initialisation */
  @Input()
  initialFocus = false;

  @Input()
  inputType = 'text';

  @Input()
  placeholder = '';

  @ViewChild('inputField', {static: true, read: ElementRef})
  inputField: ElementRef;

  @Output()
  inputFocused = new EventEmitter<FocusEvent>();

  @Output()
  inputBlurred = new EventEmitter<FocusEvent>();

  constructor(cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  ngAfterViewInit(): void {
    if (this.initialFocus) {
      this.focusInputField();
    }
  }

  grabFocus() {
    this.focusInputField();
  }

  private focusInputField() {
    const inputFieldNativeElement = this.inputField.nativeElement as HTMLInputElement;
    inputFieldNativeElement.focus();
  }

  handleFocus($event: FocusEvent) {
    this.inputFocused.next($event);
  }

  handleBlur($event: FocusEvent) {
    this.inputBlurred.next($event);
  }
}

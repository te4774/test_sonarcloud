import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from '@angular/core';
import {NzSwitchComponent} from 'ng-zorro-antd/switch';

/*
 * Switch widget with asynchronous behaviour. You have to react to click events (confirmEvent output)
 * and either set the value or discard the async mode.
 */
@Component({
  selector: 'sqtm-core-switch-field',
  templateUrl: './switch-field.component.html',
  styleUrls: ['./switch-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwitchFieldComponent {

  @ViewChild(NzSwitchComponent) nzSwitch: NzSwitchComponent;

  @Input()
  set value(newValue: boolean) {
    this.internalValue = newValue;
    this.endAsyncMode();
  }

  get value(): boolean { return this.internalValue; }

  internalValue: boolean;

  @Output()
  confirmEvent = new EventEmitter<boolean>();

  pending: boolean;

  constructor(private cdRef: ChangeDetectorRef) { }

  public endAsyncMode(): void {
    this.pending = false;
    this.cdRef.detectChanges();
  }

  handleClick() {
    this.pending = true;
    this.confirmEvent.emit(!this.internalValue);
  }
}

import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-core-compact-collapse-panel',
  templateUrl: './compact-collapse-panel.component.html',
  styleUrls: ['./compact-collapse-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompactCollapsePanelComponent implements OnInit {

  @Input()
  active = true;

  @Input()
  i18nTitleKey = 'PROVIDE AN I18N TITLE KEY';

  constructor() {
  }

  ngOnInit() {
  }

  toggle() {
    this.active = !this.active;
  }

}

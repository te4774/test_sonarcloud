import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {STACK_TRACE_PARAM} from '../../../../core/services/rest.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'sqtm-core-entity-not-found',
  templateUrl: './entity-not-found.component.html',
  styleUrls: ['./entity-not-found.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EntityNotFoundComponent {
  @Input() httpError: HttpErrorResponse;

  public get isStackTraceEnabled() {
    return this.httpError?.headers?.get(STACK_TRACE_PARAM) === 'enable';
  }

  public openStackTracePopup() {
    if (this.isStackTraceEnabled && this.stackTrace) {
      const popup = window.open(
        'about:blank',
        'error_details',
        'resizable=yes, scrollbars=yes, status=no, menubar=no, toolbar=no, dialog=yes, location=no');

      popup.document.write(this.stackTrace);
    }
  }

  private get stackTrace() {
    return this.httpError?.error?.trace;
  }
}

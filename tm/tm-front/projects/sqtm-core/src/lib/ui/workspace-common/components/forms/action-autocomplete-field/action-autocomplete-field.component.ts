import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {FormGroup} from '@angular/forms';

import {fromEvent, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import {FieldValidationError} from '../../../../../model/error/error.model';
import {AbstractActionWordFieldComponent} from '../action-word-field/abstract-action-word-field.component';
import {
  ActionAutocompleteFieldOptionComponent
} from './action-autocomplete-field-option/action-autocomplete-field-option.component';

@Component({
  selector: 'sqtm-core-action-autocomplete-field',
  templateUrl: './action-autocomplete-field.component.html',
  styleUrls: ['./action-autocomplete-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionAutocompleteFieldComponent extends AbstractActionWordFieldComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() formGroup: FormGroup;
  @Input() fieldName: string;
  @Input() serverSideFieldValidationError: FieldValidationError[];

  _options: string[];

  get options() {
    return this._options;
  }

  @Input()
  set options(paramOptions: string[]) {
    this.errors = [];
    this._options = paramOptions;
    if (paramOptions.length > 0) {
      this.openOptionMenu();
    } else {
      this.closeOptionMenu();
    }
  }

  private _isMenuOpen: boolean = false;

  get isMenuOpen(): boolean {
    return this._isMenuOpen;
  }

  set isMenuOpen(value: boolean) {
    this._isMenuOpen = value;
  }

  private _value: string;

  get value(): string {
    return this._value;
  }

  set value(value: string) {
    this._value = value;
  }

  @Input() placeholder = '';

  @Output() valueChanged = new EventEmitter<any>();

  @Output() confirmValueEvent = new EventEmitter<any>();

  private unsub$ = new Subject<void>();

  @ViewChild('textInput')
  textInput: ElementRef;

  @ViewChildren(ActionAutocompleteFieldOptionComponent)
  optionElements: QueryList<ActionAutocompleteFieldOptionComponent>;

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    fromEvent(this.textInput.nativeElement, 'input')
      .pipe(
        takeUntil(this.unsub$),
        debounceTime(500),
        map((event: any) => event.target.value),
        distinctUntilChanged()
      ).subscribe(value => this.valueChanged.emit(value));
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  confirmValue(event: KeyboardEvent) {
    this.confirmValueEvent.emit(event);
  }

  handleKeyupEnter(event: KeyboardEvent) {
    if (this.isMenuOpen && this.isOneOptionActive()) {
      this.selectOptionWithEnter();
    } else {
      this.confirmValue(event);
    }
  }

  selectOptionWithMouse(event, option: string) {
    this.formGroup.get(this.fieldName).setValue(option);
    this.closeOptionMenu();
    this.focusInput();
    event.preventDefault();
  }

  closeOptionMenu() {
    if (this.isMenuOpen) {
      this.isMenuOpen = false;
      this.deactivateOptions();
    }
  }

  openOptionMenu() {
    if (!this.isMenuOpen && this.options.length > 0) {
      this.isMenuOpen = true;
    }
  }

  activateOption(paramIndex: number) {
    if (paramIndex === undefined) {
      return;
    }
    this.deactivateOptions();
    const activeOption = this.optionElements
      .find((element, index) => index === paramIndex);
    if (activeOption) {
      activeOption.isActive = true;
    }
  }

  focusInput() {
    const textInputNativeElement = this.textInput.nativeElement as HTMLInputElement;
    textInputNativeElement.focus();
  }

  blurInput() {
    const inputFieldNativeElement = this.textInput.nativeElement as HTMLInputElement;
    inputFieldNativeElement.blur();
  }

  private deactivateOptions() {
    const activeOption = this.optionElements.find(option => option.isActive);
    if (activeOption) {
      activeOption.isActive = false;
    }
  }

  private isOneOptionActive(): boolean {
    return this.optionElements.filter(option => option.isActive).length > 0;
  }

  /* Keyboard navigation in menu */

  selectOptionWithEnter() {
    const option = this.getActiveOptionValue();
    this.formGroup.get(this.fieldName).setValue(option);
    this.closeOptionMenu();
    this.focusInput();
  }

  activateOptionBelow(event) {
    event.preventDefault();
    this.openOptionMenu();
    this.activateOption(
      this.getBelowOptionIndex(
        this.getActiveOptionIndex()));
  }

  activateOptionAbove(event) {
    event.preventDefault();
    this.openOptionMenu();
    this.activateOption(
      this.getAboveOptionIndex(
        this.getActiveOptionIndex()));
  }

  private getActiveOptionValue(): string {
    return this.optionElements.find(option => option.isActive).value;
  }

  private getActiveOptionIndex(): number {
    return this.optionElements.toArray().findIndex(option => option.isActive);
  }

  private getBelowOptionIndex(index: number) {
    const optionLength = this.optionElements.length;
    if (optionLength === 0) {
      return undefined;
    } else if (index === -1 || index === undefined) {
      return 0;
    } else if (index === optionLength - 1) {
      return 0;
    } else {
      return index + 1;
    }
  }

  private getAboveOptionIndex(index: number) {
    const optionLength = this.optionElements.length;
    if (optionLength === 0) {
      return undefined;
    } else if (index === -1 || index === undefined) {
      return optionLength - 1;
    } else if (index === 0) {
      return optionLength - 1;
    } else {
      return index - 1;
    }
  }

}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {EntityNotFoundComponent} from './entity-not-found.component';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';

describe('EntityNotFoundComponent', () => {
  let component: EntityNotFoundComponent;
  let fixture: ComponentFixture<EntityNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EntityNotFoundComponent ],
      imports: [TestingUtilsModule],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntityNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));
});

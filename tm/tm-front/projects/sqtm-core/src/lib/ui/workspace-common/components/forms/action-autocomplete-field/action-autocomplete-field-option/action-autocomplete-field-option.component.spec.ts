import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ActionAutocompleteFieldOptionComponent} from './action-autocomplete-field-option.component';

describe('ActionAutocompleteFieldOptionComponent', () => {
  let component: ActionAutocompleteFieldOptionComponent;
  let fixture: ComponentFixture<ActionAutocompleteFieldOptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActionAutocompleteFieldOptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionAutocompleteFieldOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

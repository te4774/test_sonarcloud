import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {EditableSelectFieldComponent} from './editable-select-field.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {By} from '@angular/platform-browser';
import {Option} from '../../../../../model/option.model';
import {TranslateModule} from '@ngx-translate/core';

describe('EditableSelectComponent', () => {
  let component: EditableSelectFieldComponent;
  let fixture: ComponentFixture<EditableSelectFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, TranslateModule.forRoot()],
      declarations: [EditableSelectFieldComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(EditableSelectFieldComponent);
        component = fixture.componentInstance;
        component.options = [{label: 'Label', value: 'Value'}, {label: 'Label2', value: 'Value2'}, {
          label: 'Label3',
          value: 'Value3'
        }];
        component.value = 'Value3';
        component.editable = true;
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('It should show initial data', () => {
    const element = fixture.nativeElement;
    const span = element.querySelector('span');
    expect(span.textContent).toContain('Label3');
  });

  it('should pass on edit mode when clicking on it if edition is allowed', () => {
    activateEditMode();
    const select = fixture.debugElement.query(By.css('nz-select'));
    expect(select).toBeTruthy();
    expect(select.properties['ngModel']).toBe('Value3');
    expect(select.nativeElement.children.length).toBe(3);
  });

  it('should not pass in edit mode when clicking on it if edition is forbidden', () => {
    component.editable = false;
    fixture.detectChanges();
    activateEditMode();
    const select = fixture.debugElement.query(By.css('div[data-test-component-id="select-field-wrapper"]'));
    expect(select.classes['hide']).toBeTruthy();
  });

  it('should emit new value in confirm output', () => {
    let outputValue: Option;
    component.confirmEvent.asObservable().subscribe(
      value => outputValue = value
    );
    activateEditMode();
    component.transientValue = 'Value';
    component.confirm();
    expect(outputValue.value).toEqual('Value');
    expect(fixture.debugElement.queryAll(By.css('button')).length).toBe(2);
  });

  function activateEditMode() {
    const span = fixture.debugElement.query(By.css('span'));
    span.triggerEventHandler('click', null);
    fixture.detectChanges();
  }
});

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-core-autocomplete-option',
  templateUrl: './autocomplete-option.component.html',
  styleUrls: ['./autocomplete-option.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutocompleteOptionComponent implements OnInit {

  @Input()
  value: string;

  private _isActive: boolean;

  get isActive(): boolean {
    return this._isActive;
  }

  set isActive(value: boolean) {
    this._isActive = value;
    this.cdr.detectChanges();
  }

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

}

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {EditableCustomField} from '../../../../custom-field/editable-custom-field';
import {ValidatorFn} from '@angular/forms';
import {AbstractEditableField, EditableField, EditableLayout} from '../abstract-editable-field';
import {createCkEditorConfig, overrideCKEditorDefaultLinkTarget} from '../../../../utils/ck-editor-config';
import {CKEditor4} from 'ckeditor4-angular';
import {SessionPingService} from '../../../../../core/services/session-ping/session-ping.service';
import {getSupportedBrowserLang} from '../../../../../core/utils/browser-langage.utils';
import EditorType = CKEditor4.EditorType;

@Component({
  selector: 'sqtm-core-editable-rich-text',
  templateUrl: './editable-rich-text.component.html',
  styleUrls: ['./editable-rich-text.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableRichTextComponent extends AbstractEditableField
  implements OnInit, AfterViewInit, OnDestroy, EditableField, EditableCustomField {

  private readonly REQUIRED_KEY = 'sqtm-core.validation.errors.required';

  dirty = false;

  externalErrors: string[] = [];

  editorModel: { editorData: string } = {editorData: ''};

  type = EditorType.DIVAREA;

  @Input()
  set value(value: string) {
    this.updateAndClose(value);
  }

  @Input()
  focus = false;

  get value(): string {
    return this.editorModel.editorData;
  }

  @Input()
  required = false;

  @Input()
  placeholder;

  edit = false;

  @Input()
  size: 'small' | 'default' | 'large' = 'default';

  @Input()
  border = false;

  @Input()
  layout: EditableLayout = 'default';

  @Input()
  set validators(validators: ValidatorFn[]) {
    throw Error('Validators are not handled in Rich Text Field.');
  }

  pending = false;

  config = {};

  // initial value when editor is active. Allow to restore previous value if user press cancel.
  initialValue;

  @Output()
  readonly confirmEvent = new EventEmitter<string>();

  @Output()
  readonly cancelEvent = new EventEmitter<void>();

  constructor(cdRef: ChangeDetectorRef,
              private ngxTranslateService: TranslateService,
              private readonly sessionPingService: SessionPingService) {
    super(cdRef);
    if (!this.placeholder) {
      this.placeholder = this.ngxTranslateService.instant('sqtm-core.generic.editable.placeholder');
    }
  }

  ngOnInit() {
    this.config = createCkEditorConfig(getSupportedBrowserLang(this.ngxTranslateService));
  }

  ngAfterViewInit(): void {
    if (this.focus) {
      this.focusInputField();
    }
  }

  ngOnDestroy(): void {
    if (this.edit) {
      this.sessionPingService.endLongTermEdition();
    }
  }

  handleCKEditorReady(): void {
    overrideCKEditorDefaultLinkTarget();
  }

  enableEditMode() {
    if (!this.edit && this.editable) {
      this.dirty = false;
      this.edit = true;
      this.initialValue = this.editorModel.editorData;
      this.cdRef.detectChanges();

      this.sessionPingService.beginLongTermEdition();
    }
  }

  disableEditMode() {
    if (this.edit) {
      this.edit = false;
      this.pending = false;
      this.cdRef.detectChanges();

      this.sessionPingService.endLongTermEdition();
    }
  }

  handleClickOut(event) {
    const isFullScreen: boolean = event.editor.getCommand('maximize').state == 1;
    if (!isFullScreen) {
      if (this.editorModel.editorData === this.initialValue) {
        this.cancel();
      } else {
        this.confirm();
      }
    }
  }

  cancel() {
    this.editorModel.editorData = this.initialValue;
    this.disableEditMode();
    this.cancelEvent.emit();
  }

  confirm() {
    this.dirty = true;
    this.removeExternalErrors();
    this.editorModel.editorData = this.extractImageHeightWidthAsHtmlAttributes(this.editorModel.editorData);

    if (this.required && this.isBlank(this.editorModel.editorData)) {
      this.externalErrors.push(this.ngxTranslateService.instant(this.REQUIRED_KEY));
      this.cdRef.detectChanges();
    } else {
      this.executeAutoAsync();
      this.confirmEvent.emit(this.editorModel.editorData);
    }
  }

  /**
   * Extracts an image's height value and width value from the inline style set by ckEditor if measured in px,
   * and adds the values as html height and width attributes within the image tag.
   * Regex : selects the number values between " width:" and "px" and "height:" and "px"
   * @param value string containing the html content of an rtf
   * @private
   */
  private extractImageHeightWidthAsHtmlAttributes(value: string): string {

    if(value.includes("<img") && value.includes("(?<=\\swidth:)([0-9]+)(?=px)")
      && value.includes("(?<=height:)([0-9]+)(?=px)")) {

      let extractedWidth = value.match("(?<=\\swidth:)([0-9]+)(?=px)");
      const width = extractedWidth.toString().split(",")[1];
      const widthAttribute = "width=\"" + width + "\" ";

      let extractedHeight = value.match("(?<=height:)([0-9]+)(?=px)");
      const height = extractedHeight.toString().split(",")[1];
      const heightAttribute = " height=\"" + height + "\" ";

      const splitValues = value.split(" style");
      return splitValues[0] + heightAttribute + widthAttribute + "style" + splitValues[1];

    } else {
      return value;
    }
  }


  beginAsync() {
    this.pending = true;
  }

  endAsync() {
    this.pending = false;
    this.cdRef.detectChanges();
  }

  updateAndClose(value: string) {
    this.endAsync();
    this.disableEditMode();
    this.initialValue = value;
    this.editorModel.editorData = value;
    this.cdRef.detectChanges();
  }

  private removeExternalErrors() {
    this.externalErrors = [];
  }

  private isBlank(text: string) {
    // Extract text
    const div = document.createElement('div');
    div.innerHTML = text;
    return (div.textContent || div.innerText).trim().length === 0;
  }

  private focusInputField() {
    this.config['startupFocus'] = true;
  }
}

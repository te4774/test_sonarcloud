import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';

import {EditableTagListFieldComponent} from './editable-tag-list-field.component';

describe('EditableTagListFieldComponent', () => {
  let component: EditableTagListFieldComponent;
  let fixture: ComponentFixture<EditableTagListFieldComponent>;

  const initialOptions = ['linux', 'ssh', 'firefox'];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule, TestingUtilsModule],
      declarations: [ EditableTagListFieldComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableTagListFieldComponent);
    component = fixture.componentInstance;

    component.options = initialOptions;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  const dataSets = [
    { inputValues: [], expectedValuesNotInOptions: [] },
    { inputValues: ['linux', 'ssh', 'firefox'], expectedValuesNotInOptions: [] },
    { inputValues: ['linux', 'ssh', 'chrome'], expectedValuesNotInOptions: ['chrome'] },
  ];

  dataSets.forEach(dataset => {
    it('should display correct options', () => {
      component.value = dataset.inputValues;
      fixture.detectChanges();

      initialOptions.forEach(option => expect(component.options).toEqual(initialOptions));
      expect(countDisplayedOptions()).toEqual(initialOptions.length);

      expect(component.selectedTagsNotInOptions).toEqual(dataset.expectedValuesNotInOptions);
    });
  });

  function countDisplayedOptions(): number {
    const selectElement = fixture.nativeElement.querySelector('nz-select');
    return selectElement.children.length;
  }

});

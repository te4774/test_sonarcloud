import {Directive, Host, Input} from '@angular/core';
import {GenericEntityViewService} from '../../../core/services/genric-entity-view/generic-entity-view.service';
import {SqtmGenericEntityState} from '../../../core/services/genric-entity-view/generic-entity-view-state';
import {
  EditableNumericFieldComponent
} from '../components/editables/editable-numeric-field/editable-numeric-field.component';
import {AbstractGenericViewEditableField} from './abstract-generic-view-editable-field';

@Directive({
  selector: '[sqtmCoreGenericViewEditableNumericField]'
})
export class GenericViewEditableNumericFieldDirective<E extends SqtmGenericEntityState, T extends string>
  extends AbstractGenericViewEditableField<EditableNumericFieldComponent, E, T> {

  @Input('sqtmCoreGenericViewEditableNumericField')
  fieldName: keyof E;

  constructor(protected genericEntityViewService: GenericEntityViewService<E, T>,
              @Host() protected editable: EditableNumericFieldComponent) {
    super(genericEntityViewService, editable);
  }

  protected showExternalErrorMessages(errorMessages: string[]): void {
    this.editable.showExternalErrorMessage(errorMessages);
  }

  protected setValue(newValue: any): void {
    this.editable.value = newValue;
  }
}

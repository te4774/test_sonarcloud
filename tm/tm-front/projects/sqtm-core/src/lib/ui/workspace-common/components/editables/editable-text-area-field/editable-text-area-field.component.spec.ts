import {DebugElement, NO_ERRORS_SCHEMA} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {EditableLayout} from '../abstract-editable-field';

import {EditableTextAreaFieldComponent} from './editable-text-area-field.component';

describe('EditableTextAreaFieldComponent', () => {
  let component: EditableTextAreaFieldComponent;
  let fixture: ComponentFixture<EditableTextAreaFieldComponent>;
  let el: DebugElement;

  const initialValue =
`| First name  | Last name | email                   |
| Pierre      | Dupond    | pierre.dupond@mail.com  |
| Jean        | Dupont    | jean@dupont@mail        |
| Paul        | Dupres    | paul.dupres@mail.com    |`;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ TestingUtilsModule ],
      declarations: [ EditableTextAreaFieldComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableTextAreaFieldComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement;

    component.value = initialValue;
    fixture.detectChanges();
  });

  it('should display initial data', () => {
    expect(getConfirmButton()).toBeFalsy();
    expect(getCancelButton()).toBeFalsy();
    expect(getInput()).toBeFalsy();
    expect(getDisplayDivContent()).toBe(initialValue);
  });

  it('should switch to editable mode on click', () => {
    expect(getConfirmButton()).toBeFalsy();
    expect(getCancelButton()).toBeFalsy();
    expect(getDisplayDiv()).toBeTruthy();
    getDisplayClickableDiv().triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(getDisplayDiv()).toBeFalsy();
    expect(getConfirmButton()).toBeTruthy();
    expect(getCancelButton()).toBeTruthy();
    expect(getInput()).toBeTruthy();
  });

  interface DataSet {
    layout: EditableLayout;
    shouldShowButtons: boolean;
    shouldShowButtonLabels: boolean;
  }

  const dataSets: DataSet[] = [
    { layout: 'default', shouldShowButtons: true, shouldShowButtonLabels: true },
    { layout: 'compact', shouldShowButtons: true, shouldShowButtonLabels: false },
    { layout: 'no-buttons', shouldShowButtons: false, shouldShowButtonLabels: false }];

  dataSets.forEach((dataset) => {
    it('should show buttons according to layout configuration', () => {
      component.layout = dataset.layout;
      component.edit = true;
      fixture.detectChanges();

      const confirmButton = getConfirmButton();
      const cancelButton = getCancelButton();

      if (dataset.shouldShowButtons) {
        expect(confirmButton).toBeTruthy();
        expect(cancelButton).toBeTruthy();
      } else {
        expect(confirmButton).toBeFalsy();
        expect(cancelButton).toBeFalsy();
      }

      const confirmButtonLabel = getButtonLabel(confirmButton);
      const cancelButtonLabel = getButtonLabel(cancelButton);

      if (dataset.shouldShowButtonLabels) {
        expect(confirmButtonLabel).toBeTruthy();
        expect(cancelButtonLabel).toBeTruthy();
      } else {
        expect(confirmButtonLabel).toBeFalsy();
        expect(cancelButtonLabel).toBeFalsy();
      }

    });
  });

  it('should cancel editing', () => {
    spyOn(component.cancelEvent, 'emit').and.callThrough();

    component.edit = true;
    fixture.detectChanges();

    const editedValue =
      `edited
       value`;

    setInputContent(editedValue);
    expect(getInputContent()).toBe(editedValue);

    getCancelButton().triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(getCancelButton()).toBeFalsy();
    expect(getDisplayDivContent()).toBe(initialValue);

    expect(component.cancelEvent.emit).toHaveBeenCalled();
  });

  it('should confirm editing', () => {
    spyOn(component.confirmEvent, 'emit').and.callThrough();

    component.edit = true;
    fixture.detectChanges();

    const editedValue =
      `edited
       value`;

    setInputContent(editedValue);
    expect(getInputContent()).toBe(editedValue);

    getConfirmButton().triggerEventHandler('click', null);

    fixture.detectChanges();

    expect(component.confirmEvent.emit).toHaveBeenCalled();
  })

  function getConfirmButton(): DebugElement {
    return el.query(By.css('button:first-child'));
  }

  function getCancelButton(): DebugElement {
    return el.query(By.css('button:last-child'));
  }

  function getButtonLabel(button: DebugElement) {
    if (button) {
      return button.nativeElement.textContent;
    } else {
      return null;
    }
  }

  function getInput(): DebugElement {
    return el.query(By.css('textarea'));
  }

  function getInputContent(): string {
    return getInput().nativeElement.textContent;
  }

  function setInputContent(value: string) {
    getInput().nativeElement.textContent = value;
  }

  function getDisplayClickableDiv(): DebugElement {
    return el.query(By.css('div.text-area-border'));
  }

  function getDisplayDiv(): DebugElement {
    return el.query(By.css('.field-value'));
  }

  function getDisplayDivContent(): string {
    return getDisplayDiv().nativeElement.textContent;
  }

});

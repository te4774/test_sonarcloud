import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldValidationError} from '../../../../../model/error/error.model';
import {AbstractFormField} from '../abstract-form-field';
import {DateFormatUtils} from '../../../../../core/utils/date-format.utils';
import {TranslateService} from '@ngx-translate/core';
import {getSupportedBrowserLang} from '../../../../../core/utils/browser-langage.utils';

@Component({
  selector: 'sqtm-core-date-picker',
  template: `
    <div class="full-width" [formGroup]="formGroup" [attr.data-test-field-name]="fieldName">
      <nz-date-picker
          class="full-width"
          [formControlName]="fieldName"
          [nzAllowClear]="allowEmptyValue"
          [nzShowTime]="showTime"
          [nzFormat]="format"
          sqtmCoreKeyupStopPropagation>
      </nz-date-picker>
      <div class="has-error" *ngFor="let error of errors">
        <span
          [attr.data-test-error-key]="error.provideI18nKey()"
          class="sqtm-core-error-message">
          {{error.provideI18nKey()| translate}}
        </span>
      </div>
    </div>`,
  styleUrls: ['./date-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatePickerComponent extends AbstractFormField {

  @Input()
  formGroup: FormGroup;

  @Input()
  fieldName: string;

  @Input()
  allowEmptyValue = true;

  @Input()
  showTime = false;

  get format() {
    const browserLang = getSupportedBrowserLang(this.translateService);
    return this.showTime ? DateFormatUtils.short(browserLang) : DateFormatUtils.shortDate(browserLang);
  }

  @Input()
  set serverSideFieldValidationError(fieldsValidationErrors: FieldValidationError[]) {
    this.showServerSideError(fieldsValidationErrors);
  }

  constructor(cdr: ChangeDetectorRef, public readonly translateService: TranslateService) {
    super(cdr);
  }
}

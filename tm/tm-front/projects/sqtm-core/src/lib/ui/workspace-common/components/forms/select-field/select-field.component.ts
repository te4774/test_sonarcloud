import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FieldValidationError} from '../../../../../model/error/error.model';
import {DisplayOption} from '../display-option';
import {AbstractFormField} from '../abstract-form-field';
import {NzSelectModeType} from 'ng-zorro-antd/select';

@Component({
  selector: 'sqtm-core-select-field',
  template: `
    <div class="full-width" [formGroup]="formGroup" [attr.data-test-field-name]="fieldName"
         sqtmCoreKeyupStopPropagation>
      <nz-select class="full-width"
                 [nzShowArrow]="mode !== 'tags'"
                 [nzShowSearch]="showSearch"
                 [nzDisabled]="disabled"
                 [formControlName]="fieldName"
                 [nzMode]="mode"
                 [nzPlaceHolder]="placeHolder">
        <nz-option
          *ngFor="let option of options" [nzValue]="option.id"
          [nzLabel]="option.label | translate">
        </nz-option>
      </nz-select>
      <div class="has-error" *ngFor="let error of errors">
        <span [attr.data-test-error-key]="error.provideI18nKey()"
              class="sqtm-core-error-message">
          {{error.provideI18nKey()| translate}}
        </span>
      </div>
    </div>`,
  styleUrls: ['./select-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectFieldComponent extends AbstractFormField {

  @Input()
  formGroup: FormGroup;

  @Input()
  fieldName: string;

  @Input()
  options: DisplayOption[] = [];

  @Input()
  placeHolder: string;

  @Input()
  showSearch: boolean;

  @Input()
  set serverSideFieldValidationError(fieldsValidationErrors: FieldValidationError[]) {
    this.showServerSideError(fieldsValidationErrors);
  }

  @Input()
  set disabled(isDisabled: boolean) {
    this._disabled = isDisabled;
  }

  get disabled(): boolean {
    return this._disabled;
  }

  @Input()
  set mode(mode: NzSelectModeType) {
    this._mode = mode;
  }

  get mode(): NzSelectModeType {
    return this._mode;
  }

  private _mode: NzSelectModeType = 'default';

  private _disabled: boolean;

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }
}

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {Option} from '../../../../../model/option.model';
import {
  AutomationRequestStatus,
  BddImplementationTechnology,
  BddScriptLanguage,
  CampaignStatus,
  ExecutionStatus,
  LevelEnum,
  LevelEnumItem,
  MilestoneRange,
  MilestoneStatus,
  RequirementCriticality,
  RequirementStatus,
  TestCaseStatus,
  TestCaseWeight
} from '../../../../../model/level-enums/level-enum';
import {TranslateService} from '@ngx-translate/core';
import {EditableSelectFieldComponent} from '../editable-select-field/editable-select-field.component';

@Component({
  selector: 'sqtm-core-editable-select-level-enum-field',
  templateUrl: './editable-select-level-enum-field.component.html',
  styleUrls: ['./editable-select-level-enum-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableSelectLevelEnumFieldComponent implements OnInit, AfterViewInit {

  @ViewChild('child')
  child: EditableSelectFieldComponent;

  @Input()
  edit = false;

  @Input()
  pending = false;

  @Input()
  size: 'small' | 'default' | 'large' = 'default';

  @Input()
  layout: 'compact' | 'no-buttons' | 'default' = 'default';

  private _selectedEnumItem: string;

  get selectedEnumItem(): string {
    return this._selectedEnumItem;
  }

  @Input()
  set selectedEnumItem(selectedEnumItem: string) {
    this._selectedEnumItem = selectedEnumItem;
    this.updateChildValue();
  }

  @Input()
  enumType: 'TestCaseStatus'
    | 'TestCaseWeight'
    | 'AutomationRequestStatus'
    | 'RequirementStatus'
    | 'RequirementCriticality'
    | 'MilestoneStatus'
    | 'MilestoneRange'
    | 'CampaignStatus'
    | 'ExecutionStatus'
    | 'BddImplementationTechnology'
    | 'BddScriptLanguage';

  @Input()
  excludedKeys: any[] = [];

  @Input()
  editable = true;

  @Input()
  showIcon = false;

  @Output()
  readonly confirmEvent = new EventEmitter<LevelEnumItem<any>>();

  @Output()
  readonly toggleEditEvent = new EventEmitter<boolean>();

  levelEnum: LevelEnum<any>;

  options: Option[];

  constructor(private translate: TranslateService,
              private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    switch (this.enumType) {
      case 'TestCaseStatus':
        this.levelEnum = TestCaseStatus;
        break;
      case 'TestCaseWeight':
        this.levelEnum = TestCaseWeight;
        break;
      case 'AutomationRequestStatus':
        this.levelEnum = AutomationRequestStatus;
        break;
      case 'RequirementStatus':
        this.levelEnum = RequirementStatus;
        break;
      case 'RequirementCriticality':
        this.levelEnum = RequirementCriticality;
        break;
      case 'MilestoneStatus':
        this.levelEnum = MilestoneStatus;
        break;
      case 'MilestoneRange':
        this.levelEnum = MilestoneRange;
        break;
      case 'CampaignStatus':
        this.levelEnum = CampaignStatus;
        break;
      case 'ExecutionStatus':
        this.levelEnum = ExecutionStatus;
        break;
      case 'BddImplementationTechnology':
        this.levelEnum = BddImplementationTechnology;
        break;
      case 'BddScriptLanguage':
        this.levelEnum = BddScriptLanguage;
        break;
    }
    this.options = this.convertToOption(this.levelEnum);
  }

  ngAfterViewInit(): void {
    this.updateChildValue();
  }

  beginAsync() {
    this.child.beginAsync();
  }

  endAsync() {
    this.child.endAsync();
  }

  disableEditMode() {
    this.child.disableEditMode();
  }

  confirm(event: Option) {
    this.confirmEvent.emit(this.levelEnum[event.value]);
  }

  convertToOption(levelEnum: LevelEnum<any>): Option[] {
    const options = [];
    for (const levelEnumKey in levelEnum) {
      if (levelEnum.hasOwnProperty(levelEnumKey)) {
        const levelEnumItem = levelEnum[levelEnumKey];
        options.push(this.levelEnumItemToOption(levelEnumItem));
      }
    }
    return options;
  }

  levelEnumItemToOption(levelEnumItem: LevelEnumItem<any>): Option {
    const option = new Option();
    option.label = this.translate.instant(levelEnumItem.i18nKey);
    option.value = levelEnumItem.id;
    return option;
  }

  toggleEdit($event: boolean) {
    this.toggleEditEvent.emit($event);
  }

  markForCheck() {
    this.cdRef.markForCheck();
    if (this.child) {
      this.child.cdRef.markForCheck();
    }
  }

  private updateChildValue() {
    if (this.child) {
      this.child.value = this._selectedEnumItem;
    }
  }
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {EditableSearchableSelectFieldComponent} from './editable-searchable-select-field.component';

describe('EditableSearchableSelectFieldComponent', () => {
  let component: EditableSearchableSelectFieldComponent;
  let fixture: ComponentFixture<EditableSearchableSelectFieldComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EditableSearchableSelectFieldComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableSearchableSelectFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

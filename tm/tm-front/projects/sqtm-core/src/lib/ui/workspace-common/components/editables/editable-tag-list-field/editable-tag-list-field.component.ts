import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'sqtm-core-editable-tag-list-field',
  templateUrl: './editable-tag-list-field.component.html',
  styleUrls: ['./editable-tag-list-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableTagListFieldComponent {

  @Input()
  set options(options: string[]) {
    this._options = options;
    this._selectedTagsNotInOptions = this.findSelectedTagsNotInOptions();
  }

  @Input()
  set value(value: string[]) {
    this._value = value;
    this.model = value;
    this._selectedTagsNotInOptions = this.findSelectedTagsNotInOptions();
  }

  @Input()
  disabled = false;

  @Output()
  confirmEvent = new EventEmitter<string[]>();

  @Output()
  nzFocus = new EventEmitter<void>();

  model: string[];

  private _value: string[] = [];

  private _options: string[] = [];

  private _selectedTagsNotInOptions: string[] = [];

  constructor() {
  }

  get value(): string[] {
    return this._value;
  }

  get options(): string[] {
    return this._options;
  }

  get selectedTagsNotInOptions(): string[] {
    return this._selectedTagsNotInOptions;
  }

  findSelectedTagsNotInOptions() {
    return this._value.filter(tag => !this._options.includes(tag));
  }

  confirm(): any {
    this.confirmEvent.emit(this.model);
  }

  handleFocus(): void {
    this.nzFocus.emit();
  }
}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {AbstractEditableField, EditableLayout} from '../abstract-editable-field';

@Component({
  selector: 'sqtm-core-editable-text-area-field',
  templateUrl: './editable-text-area-field.component.html',
  styleUrls: ['./editable-text-area-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableTextAreaFieldComponent extends AbstractEditableField implements OnInit {

  @Input()
  set value(value: string) {
    this.updateAndClose(value);
  }

  @Input()
  set edit(isEdit: boolean) {
    this._edit = isEdit;
    this.cdRef.detectChanges();
  }

  @Input()
  layout: EditableLayout = 'default';

  @Output()
  confirmEvent = new EventEmitter<string>();

  @Output()
  cancelEvent = new EventEmitter<string>();

  @ViewChild('input')
  input: ElementRef;

  editorData: string = '';

  initialValue: string;

  pending = false;

  private _edit = false;

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  ngOnInit(): void {
  }

  enableEditMode() {
    if (this.editable && !this.edit) {
        this.edit = true;
        this.initialValue = this.editorData;
        this.cdRef.detectChanges();
    }
  }

  disableEditMode() {
    this.edit = false;
    this.pending = false;
  }

  confirm() {
    this.executeAutoAsync();
    this.confirmEvent.emit(this.editorData);
  }

  cancel() {
    this.editorData = this.initialValue;
    this.disableEditMode();
    this.cancelEvent.emit();
  }

  beginAsync() {
    this.pending = true;
  }

  endAsync() {
    this.pending = false;
  }

  focus() {
    this.input.nativeElement.focus();
  }

  getComponentClasses() {
    const cssClass = [];
    if (this.edit) {
      cssClass.push('edit');
    } else {
      cssClass.push('read');
      if (this.editable) {
        cssClass.push('editable');
      }
    }
    return cssClass;
  }

  private updateAndClose(value: string) {
    this.endAsync();
    this.disableEditMode();
    this.initialValue = value;
    this.editorData = value;
    this.cdRef.detectChanges();
  }

  get value(): string {
    return this.editorData;
  }

  get edit(): boolean {
    return this._edit;
  }
}

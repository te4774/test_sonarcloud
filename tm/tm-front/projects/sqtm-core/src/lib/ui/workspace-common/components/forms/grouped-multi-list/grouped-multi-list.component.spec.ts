import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {GroupedMultiListComponent} from './grouped-multi-list.component';
import {TranslateModule} from '@ngx-translate/core';
import {TestElement} from 'ngx-speculoos';
import {FormsModule} from '@angular/forms';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzDividerModule} from 'ng-zorro-antd/divider';
import {NzInputModule} from 'ng-zorro-antd/input';
import {OnPushComponentTester} from '../../../../testing-utils/on-push-component-tester';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';

export class GroupedMultiListComponentTester extends OnPushComponentTester<GroupedMultiListComponent> {
  selectItem(id: string) {
    return this.getElementByDataTestComponentId(`item`);
  }

  selectGroup(id: string) {
    return this.getElementByDataTestComponentId(`group`);
  }

  selectGroupLabel(id: string) {
    return this.getElementByDataTestComponentId(`group-${id}`).element('div:nth-of-type(1)');
  }

  selectGroupItems(id: string): TestElement<any>[] {
    return this.getElementByDataTestComponentId(`group-${id}`).elements('div').slice(1);
  }

  search(token: string) {
    this.input('input').fillWith(token);
  }
}

const listItems = [
  {id: '1', label: 'Atlantys', groupId: 'STS', selected: true},
  {id: '2', label: 'Endavour', groupId: 'STS'},
  {id: '3', label: 'Discovery', groupId: 'STS'},
  {id: '4', label: 'Saturn 5', groupId: 'BFR'},
  {id: '5', label: 'Proton', groupId: 'BFR'},
  {id: '6', label: 'Ariane 6', groupId: 'BFR'},
  {id: '7', label: 'Falcon Heavy', groupId: 'BFR'},
  {id: '8', label: 'Sputnik'},
];

const listGroups = [
  {id: 'STS', label: 'Space Shuttles'},
  {id: 'BFR', label: 'Big F... Rockets'},
  {id: 'Noop', label: 'Nothing Here'},
];

describe('GroupedMultiListComponent', () => {
  let tester: GroupedMultiListComponentTester;
  let fixture: ComponentFixture<GroupedMultiListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), FormsModule, NzCheckboxModule, NzInputModule, NzDividerModule, TestingUtilsModule],
      declarations: [GroupedMultiListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedMultiListComponent);
    tester = new GroupedMultiListComponentTester(fixture);
    fixture.detectChanges();
  });

  // The whole thing is tested in cypress tests
  // No need to maintain that anymore

  // it('should create a no grouped list', fakeAsync(() => {
  //   tester.componentInstance.listItems = [
  //     {id: 'item-1', label: 'item 1 label'},
  //     {id: 'item-2', label: 'item 2 label'},
  //     {id: 'item-3', label: 'item 3 label'},
  //   ];
  //
  //   tester.detectChanges();
  //   tester.componentInstance.ngOnChanges({});
  //   tester.detectChanges();
  //   tick();
  //   expect(tester).toBeTruthy();
  //   const item1 = tester.selectItem('item-1');
  //   expect(item1.textContent).toContain('item 1 label');
  //   const item2 = tester.selectItem('item-2');
  //   expect(item2.textContent).toContain('item 2 label');
  //   const item3 = tester.selectItem('item-3');
  //   expect(item3.textContent).toContain('item 3 label');
  // }));
  //
  // it('should create a grouped list', fakeAsync(() => {
  //   tester.componentInstance.listItems = listItems;
  //   tester.componentInstance.listGroups = listGroups;
  //   tester.componentInstance.ngOnChanges({});
  //   tester.detectChanges();
  //   tick();
  //   expect(tester).toBeTruthy();
  //   const item1 = tester.selectItem('8');
  //   expect(item1.textContent).toContain('Sputnik');
  //   expect(tester.selectGroup('STS')).toBeTruthy();
  //   expect(tester.selectGroupLabel('STS').textContent).toContain('Space Shuttles');
  //   expect(tester.selectGroupItems('STS').length).toEqual(3);
  //   expect(tester.selectGroupItems('STS')[0].textContent).toContain('Atlantys');
  //   expect(tester.selectGroupItems('STS')[1].textContent).toContain('Endavour');
  //   expect(tester.selectGroupItems('STS')[2].textContent).toContain('Discovery');
  //
  //   expect(tester.selectGroup('BFR')).toBeTruthy();
  //   expect(tester.selectGroupLabel('BFR').textContent).toContain('Big F... Rockets');
  //   expect(tester.selectGroupItems('BFR').length).toEqual(4);
  //   expect(tester.selectGroupItems('BFR')[0].textContent).toContain('Saturn 5');
  //   expect(tester.selectGroupItems('BFR')[1].textContent).toContain('Proton');
  //   expect(tester.selectGroupItems('BFR')[2].textContent).toContain('Ariane 6');
  //   expect(tester.selectGroupItems('BFR')[3].textContent).toContain('Falcon Heavy');
  //   expect(tester.selectGroup('Noop')).toBeFalsy();
  // }));

});

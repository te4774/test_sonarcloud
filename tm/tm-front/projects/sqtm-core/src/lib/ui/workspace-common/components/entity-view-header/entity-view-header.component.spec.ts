import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {EntityViewHeaderComponent} from './entity-view-header.component';
import {CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform} from '@angular/core';
import {TranslatePipe} from '@ngx-translate/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

@Pipe({name: 'translate'})
class TranslatePipeMock implements PipeTransform {
  transform(): any {
    return 'mock-translate-pipe';
  }
}

describe('EntityViewHeaderComponent', () => {
  let component: EntityViewHeaderComponent;
  let fixture: ComponentFixture<EntityViewHeaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [NoopAnimationsModule],
      declarations: [EntityViewHeaderComponent, TranslatePipeMock],
      providers: [
        {provide: TranslatePipe, useClass: TranslatePipeMock}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntityViewHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

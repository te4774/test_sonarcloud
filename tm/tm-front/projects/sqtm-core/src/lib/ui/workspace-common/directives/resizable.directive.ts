///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Directive, ElementRef, EventEmitter, Inject, NgZone, OnDestroy, OnInit, Output, Renderer2} from '@angular/core';
import {fromEvent, Subject} from 'rxjs';
import {DOCUMENT} from '@angular/common';
import {take, takeUntil, tap} from 'rxjs/operators';

/** @dynamic */
@Directive({
  selector: '[sqtmCoreResizable]'
})
export class ResizableDirective implements OnInit, OnDestroy {

  private unSub$ = new Subject<void>();

  @Output()
  resize = new EventEmitter<number>();

  @Output()
  endResize = new EventEmitter<number>();

  constructor(private el: ElementRef, private zone: NgZone, private renderer: Renderer2, @Inject(DOCUMENT) private document: Document) {

  }

  ngOnInit(): void {
    this.zone.runOutsideAngular(() => {
      fromEvent(this.el.nativeElement, 'mousedown')
        .pipe(takeUntil(this.unSub$))
        .subscribe(($event: MouseEvent) => {
          this.renderer.addClass(this.document.body, 'sqtm-core-prevent-selection');
          this.dragEventHandler($event.clientX);
        });
    });
  }

  private dragEventHandler(startX: number) {
    this.zone.runOutsideAngular(() => {
      const stopListenDnd = new Subject<void>();
      let currentX = startX;

      fromEvent(this.document.documentElement, 'mousemove').pipe(
        takeUntil(stopListenDnd),
      ).subscribe(($event: MouseEvent) => {
        this.resize.emit($event.clientX - currentX);
        currentX = $event.clientX;
      });

      fromEvent(this.document.documentElement, 'mouseup').pipe(
        take(1),
        tap(() => {
          stopListenDnd.next();
          stopListenDnd.complete();
        })
      ).subscribe(($event: MouseEvent) => {
        this.renderer.removeClass(this.document.body, 'sqtm-core-prevent-selection');
        this.endResize.emit($event.clientX - startX);
      });
      return false;
    });
  }

  ngOnDestroy(): void {
    this.unSub$.next();
    this.unSub$.complete();
  }


}

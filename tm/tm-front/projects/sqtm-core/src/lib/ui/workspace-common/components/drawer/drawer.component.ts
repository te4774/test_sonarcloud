import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import {NzDrawerComponent} from 'ng-zorro-antd/drawer';
import {Subject} from 'rxjs';
import {DOCUMENT} from '@angular/common';
import {wsCommonLogger} from '../../workspace.common.logger';
import {Workspaces} from '../../../ui-manager/theme.model';

const logger = wsCommonLogger.compose('DrawerComponent');

/** @dynamic */
@Component({
  selector: 'sqtm-core-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DrawerComponent implements OnInit, OnDestroy {

  @ViewChild(NzDrawerComponent)
  nzDrawerComponent: NzDrawerComponent;

  @Input()
  titleKey: string;

  @Input()
  localTheme: Workspaces;

  @Input()
  backgroundColor: 'white' | 'neutral' = 'white';

  @Output()
  close = new EventEmitter<void>();

  private _isVisible = false;

  get isVisible(): boolean {
    return this._isVisible;
  }

  @Input()
  set isVisible(value: boolean) {
    logger.debug(`set visibility to ${value}`);
    this._isVisible = value;
    const that = this;
    if (this._isVisible) {
      setTimeout(() => {
        that.restorePointerEvents();
      });
    }

  }

  unsub$ = new Subject<void>();

  constructor(private cdRef: ChangeDetectorRef, private renderer: Renderer2, @Inject(DOCUMENT) private document: Document) {
  }

  ngOnInit() {
  }

  doClose() {
    this.close.emit();
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  restorePointerEvents() {
    logger.debug(`Try to fix NgZorroDrawer style to allow proper interaction with backdrop. This is a native js hack and it could break.`);
    this.setOverlayPointerEventsToNone();
    this.setDrawerContentPointerEventsToAll();
  }

  private setDrawerContentPointerEventsToAll() {
    const elements = this.document.getElementsByClassName('ant-drawer-content-wrapper');
    const length = elements.length;
    for (let i = 0; i < length; i++) {
      const divElement = elements.item(i) as HTMLDivElement;
      if (divElement) {
        logger.debug(`Try to fix NgZorroDrawer : Setting pointerEvents of NgZorroDrawer content wrapper to all.`, [divElement]);
        divElement.style.pointerEvents = 'all';
      }
    }
  }

  private setOverlayPointerEventsToNone() {
    const elements = this.document.getElementsByClassName('ant-drawer-open');
    const length = elements.length;
    for (let i = 0; i < length; i++) {
      const divElement = elements.item(i) as HTMLDivElement;
      if (divElement) {
        logger.debug(`Try to fix NgZorroDrawer : Setting pointerEvents of NgZorroDrawer overlay to none.`, [divElement]);
        divElement.style.pointerEvents = 'none';
      }
    }
  }

  getNzDrawerBodyStyle() {
    const backgroundColor = this.backgroundColor === 'white' ? 'white' : 'var(--background-color)';
    return {padding: '0px', height: '100%', 'background-color': backgroundColor};
  }
}

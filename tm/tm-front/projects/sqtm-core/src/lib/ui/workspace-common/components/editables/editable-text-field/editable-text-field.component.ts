import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {Subject, timer} from 'rxjs';
import {filter, takeUntil, tap} from 'rxjs/operators';
import {KeyCodes} from '../../../../utils/key-codes';
import {FormControl, ValidationErrors, ValidatorFn} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {EditableCustomField} from '../../../../custom-field/editable-custom-field';
import {AbstractEditableField, EditableField, EditableLayout} from '../abstract-editable-field';

@Component({
  selector: 'sqtm-core-editable-text-field',
  templateUrl: './editable-text-field.component.html',
  styleUrls: ['./editable-text-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableTextFieldComponent extends AbstractEditableField implements OnInit, EditableField, EditableCustomField {

  public readonly obfuscatedText = '••••••';

  private readonly REQUIRED_KEY = 'sqtm-core.validation.errors.required';

  edit = false;

  dirty = false;

  pending = false;

  @Input()
  required = false;

  @Input()
  trimValue = false;

  @Input()
  placeholder: string;

  @Input()
  size: 'small' | 'default' | 'large' = 'default';

  @Input()
  displayInGrid: boolean;

  @ViewChild('input')
  input: ElementRef;

  formControl: FormControl;

  @Input()
  set validators(validators: ValidatorFn[]) {
    this.formControl.setValidators(validators);
    this.formControl.updateValueAndValidity();
  }

  @Input()
  layout: EditableLayout = 'default';

  @Input()
  showPlaceHolder = true;

  @Input()
  isObfuscated = false;

  @Output()
  keyupEvent = new EventEmitter<string>();

  @Output()
  cancelEvent = new EventEmitter<string>();

  @Output()
  confirmEvent = new EventEmitter<string>();

  @Output()
  validatorErrorEvent = new EventEmitter();

  private _value: string;

  @Input()
  set value(value: string) {
    this.updateAndClose(value);
  }

  get value() {
    return this._value;
  }

  // Errors that are not handled by NG form control.
  // Note that the 'required' property is treated as an external error because the error is shown only on confirm
  // and not at each value change
  externalErrors: string[] = [];

  constructor(cdRef: ChangeDetectorRef, private translateService: TranslateService) {
    super(cdRef);
    this.formControl = new FormControl();
  }

  ngOnInit() {
    // this.formControl.setValidators(this.validators);
  }

  enableEditMode() {
    if (this.editable) {
      this.formControl.reset();

      if (! this.isObfuscated) {
        this.formControl.setValue(this.value);
      }

      this.formControl.updateValueAndValidity();
      this.formControl.enable();
      this.dirty = false;
      this.removeExternalErrors();
      if (!this.edit) {
        this.edit = true;
        // kind of pulling for waiting that input is displayed and active, so we can focus into.
        const unsub = new Subject<void>();
        timer(1, 10).pipe(
          takeUntil(unsub),
          filter(() => Boolean(this.input)),
          tap(() => this.input.nativeElement.focus()),
          // auto unsub, so we don't have wild timers that run into app after an edition
          tap(() => {
            unsub.next();
            unsub.complete();
          })
        ).subscribe();
      }
    }
    return false;
  }

  disableEditMode() {
    this.edit = false;
    this.pending = false;
  }

  handleBlur(event: FocusEvent) {
    if (this.checkIfConfirmOrCancelButtonHaveNotBeenClicked(event)) {
      this.confirm();
    }
  }

  checkIfConfirmOrCancelButtonHaveNotBeenClicked(event: FocusEvent) {
    // We don't want the blur event to have any impact when we click on the confirm and cancel buttons, so we check that
    // the related target (where the click occurs when the focus is lost) does not have the two CSS classes below.
    // If the click is outside the editable-field component, the related target of focus event is always null
    return event.relatedTarget == null ||
      (!(event.relatedTarget as HTMLElement).className.includes('editable-text-field-cancel-button') &&
        !(event.relatedTarget as HTMLElement).className.includes('editable-text-field-confirm-button'));
  }

  // MS Edge doesn't support key properly so we rely on deprecated keyCode.
  handleKeyboardInput(key: number)  {
    if (key === KeyCodes.ENTER) {
      // In Chrome when we pressed ENTER the blur event was triggered as well as the keyup event. The confirm method
      // was executed twice and two requests were shot (it was working fine in Firefox). To avoid this problem,
      // instead of executing confirm() method when ENTER key is pressed, we just loose focus on input element so that it
      // is the confirm method of handleBlur() event that does the job and only one request is fired.
      this.input.nativeElement.blur();
    } else if (key === KeyCodes.ESC) {
      this.cancel();
    } else {
      this.keyupEvent.emit(this.getInputFieldValue());
    }
    return false;
  }

  cancel() {
    this.cancelEvent.emit(this.getInputFieldValue());
    this.disableEditMode();
  }

  confirm() {
    this.dirty = true;
    this.removeExternalErrors();

    if (this.trimValue) {
      this.formControl.setValue(this.formControl.value.trim());
    }

    // Check if value actually changed
    if (this._value === this.getInputFieldValue()) {
      this.cancel();
      return;
    }

    // Checking if value changed is not enough for obfuscated inputs as we programmatically clear the form value before going
    // into edit mode.
    if (this.isObfuscated && ! this.formControl.dirty) {
      this.cancel();
      return;
    }

    // First check if required and blank
    if (this.required && this.isBlank(this.formControl.value)) {
      this.externalErrors.push(this.translateService.instant(this.REQUIRED_KEY));
      // Reset input to remove whitespaces
      this.formControl.setValue('');
    } else if (!this.formControl.errors) {
      this.executeAutoAsync();
      this.confirmEvent.emit(this.getInputFieldValue());
    } else {
      this.validatorErrorEvent.emit(this.formControl.errors);
    }
  }

  beginAsync() {
    this.pending = true;
    this.formControl.disable();
  }

  endAsync() {
    this.pending = false;
    this.formControl.enable();
    this.cdRef.detectChanges();
  }

  private updateAndClose(value: string) {
    this.endAsync();
    this.disableEditMode();
    this._value = value;
    this.cdRef.detectChanges();
  }

  private getInputFieldValue() {
    return this.formControl.value;
  }

  getErrors(errors: ValidationErrors): string [] {
    const messages = [];
    if (errors) {
      Object.keys(errors).forEach(errorKey => {
        messages.push(this.translateService.instant('sqtm-core.validation.errors.' + errorKey));
      });
    }
    return messages;
  }

  showExternalErrorMessage(externalErrors: string[]) {
    this.endAsync();
    this.externalErrors = externalErrors;
    this.cdRef.detectChanges();
  }

  getComponentClasses() {
    const cssClass = [];
    if (this.edit) {
      cssClass.push('edit');
    } else {
      cssClass.push('read');
      if (this.editable) {
        cssClass.push('editable');
      }
    }
    return cssClass;
  }

  private removeExternalErrors() {
    this.externalErrors = [];
  }

  private isBlank(value: string): boolean {
    return value.trim() === '';
  }

  getEditableIcon() {
    if (this.displayInGrid) {
      return '';
    } else {
      return 'edit';
    }
  }

  // This prevents undesired behaviors such as grid row drag when interacting with the input
  stopMouseEventPropagation($event: MouseEvent): void {
    $event.stopPropagation();
  }
}


import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'sqtm-core-icon-picker',
  templateUrl: './icon-picker.component.html',
  styleUrls: ['./icon-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconPickerComponent implements OnInit {

  @Input()
  icons: string[] = [];

  @Input()
  selectedIcon: string;

  @Output()
  confirmEvent = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  handleClick($event: string) {
    this.confirmEvent.emit($event);
  }
}

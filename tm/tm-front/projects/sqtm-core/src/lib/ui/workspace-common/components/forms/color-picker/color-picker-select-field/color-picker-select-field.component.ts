import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EmbeddedViewRef,
  EventEmitter,
  Input,
  Output,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {TemplatePortal} from '@angular/cdk/portal';

@Component({
  selector: 'sqtm-core-color-picker-select-field',
  templateUrl: './color-picker-select-field.component.html',
  styleUrls: ['./color-picker-select-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColorPickerSelectFieldComponent {

  private _color: string;

  @Input()
  set color(newColor: string) {
    this._color = newColor;
  }

  get color() {
    return this._color;
  }

  @Input()
  cpPosition: 'auto' | 'top' | 'bottom' | 'left' | 'right' |
    'top-left' | 'top-right'| 'bottom-left'| 'bottom-right' = 'top';

  @Output()
  colorChanged = new EventEmitter<string>();

  @ViewChild('field')
  colorSelectField: ElementRef;

  @ViewChild('template')
  templateRef: TemplateRef<any>;

  private overlayRef: OverlayRef;

  private componentRef: EmbeddedViewRef<any>;

  constructor(private overlay: Overlay, private viewContainerRef: ViewContainerRef, public readonly cdRef: ChangeDetectorRef) {

  }

  get selectedColor(): string {
    return this.color;
  }

  set selectedColor(newColor: string) {
    this.color = newColor;

    this.cdRef.detectChanges();
  }

  edit(): void {

    const positionStrategy = this.overlay.position()
      .flexibleConnectedTo(this.colorSelectField)
      .withPositions([
        {originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'center'},
      ]);

    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };

    this.overlayRef = this.overlay.create(overlayConfig);
    const templatePortal = new TemplatePortal(this.templateRef, this.viewContainerRef);
    this.componentRef = this.overlayRef.attach(templatePortal);

    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  handleColorSelectionChange(newColor: string) {
    this.color = newColor;

    this.colorChanged.emit(this.selectedColor);
    this.cdRef.detectChanges();
  }

  getBackgroundColor() {
    const checkerBoard = {
      'background-color': 'white',
      'background': 'border-box',
      'background-size': '10px 10px',
      'background-position' : '0 0, 5px 5px',
      'background-image': 'linear-gradient(45deg, #cccccc 25%, transparent 25%, transparent 75%, #cccccc 75%, #cccccc),' +
      'linear-gradient(45deg, #cccccc 25%, transparent 25%, transparent 75%, #cccccc 75%, #cccccc)'
    };
    return  this.color == null ? checkerBoard : {'background-color': this.color};
  }

  private close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }

    this.cdRef.detectChanges();
  }
}

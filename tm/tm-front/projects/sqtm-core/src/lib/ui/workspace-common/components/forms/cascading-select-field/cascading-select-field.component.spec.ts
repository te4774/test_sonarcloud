import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CascadingSelectFieldComponent} from './cascading-select-field.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('CascadingSelectFieldComponent', () => {
  let component: CascadingSelectFieldComponent;
  let fixture: ComponentFixture<CascadingSelectFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CascadingSelectFieldComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CascadingSelectFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

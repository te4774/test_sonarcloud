import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {AbstractEditableField, EditableLayout} from '../abstract-editable-field';
import {DisplayOption} from '../../forms/display-option';

/**
 * Editable field that renders as a dropdown list with a text input to filter options.
 */
@Component({
  selector: 'sqtm-core-editable-searchable-select-field',
  templateUrl: './editable-searchable-select-field.component.html',
  styleUrls: ['./editable-searchable-select-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableSearchableSelectFieldComponent extends AbstractEditableField {
  @Input()
  options: DisplayOption[];

  @Input()
  layout: EditableLayout = 'default';

  @Input()
  size: 'small' | 'default' | 'large' = 'default';

  @Input()
  placeholder = '';

  @Input()
  set value(value: string) {
    this.updateAndClose(value);
  }

  get value(): string {
    return this._value;
  }

  @Output()
  readonly confirmEvent = new EventEmitter<DisplayOption>();

  edit = false;

  pending = false;

  private _value: string;

  private _transientValue: string;

  get displayValue(): string {
    if (! Boolean(this.options)) {
      return this.placeholder;
    }
    return this.getOptionFromValue(this._value).label;
  }

  get transientValue(): string {
    return this._transientValue;
  }

  constructor(public readonly cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  beginAsync() {
    this.pending = true;
  }

  cancel() {
    this._transientValue = this.value;
    this.disableEditMode();
  }

  confirm() {
    this.executeAutoAsync();
    let selectedOption: DisplayOption = null;
    try {
      selectedOption = this.getOptionFromLabel(this._transientValue);
    } catch (e) {
      // Ignore unknown values
      this.endAsync();
      this.cancel();
    }

    if (selectedOption != null) {
      this.confirmEvent.emit(selectedOption);
    }
  }

  disableEditMode() {
    this.edit = false;
  }

  enableEditMode() {
    if (!this.edit && this.editable) {
      this._transientValue = this.getOptionFromValue(this.value).label;
      this.edit = true;
    }
  }

  endAsync() {
    this.pending = false;
  }

  getComponentClasses() {
    const cssClass = [];
    if (this.edit) {
      cssClass.push('edit');
    } else {
      cssClass.push('read');
      if (this.editable) {
        cssClass.push('editable');
      }
    }
    return cssClass;
  }

  private updateAndClose(value: string) {
    this.endAsync();
    this.disableEditMode();
    this._value = value;
    this._transientValue = value;
    this.cdRef.detectChanges();
  }

  private getOptionFromLabel(label: string): DisplayOption {
    const optionFound = this.options.find(option => option.label === label);
    if (Boolean(optionFound)) {
      return optionFound;
    }
    throw Error(`Unable to find option with label "${label}" inside available options : ${JSON.stringify(this.options)}`);
  }

  private getOptionFromValue(value: string): DisplayOption {
    const optionFound = this.options.find(option => option.id === value);
    if (Boolean(optionFound)) {
      return optionFound;
    }
    throw Error(`Unable to find option ${value} inside available options : ${JSON.stringify(this.options)}`);
  }

  handleValueChanged($event: any): void {
    this._transientValue = $event;
  }
}

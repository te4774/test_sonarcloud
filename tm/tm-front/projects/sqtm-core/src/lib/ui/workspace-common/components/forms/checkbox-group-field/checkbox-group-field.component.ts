import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {FieldValidationError} from '../../../../../model/error/error.model';
import {DisplayOption} from '../display-option';
import {Identifier} from '../../../../../model/entity.model';
import {BehaviorSubject} from "rxjs";

/**
 * A list of checkboxes that can be used inside a formGroup.
 */
@Component({
  selector: 'sqtm-core-checkbox-group-field',
  templateUrl: './checkbox-group-field.component.html',
  styleUrls: ['./checkbox-group-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: CheckboxGroupFieldComponent,
    multi: true
  }],
})
export class CheckboxGroupFieldComponent implements ControlValueAccessor {
  @Input() options: DisplayOption[];
  @Input() serverSideFieldValidationError: FieldValidationError[];

  onChange: Function;

  value$: BehaviorSubject<any> = new BehaviorSubject<any>({});
  _internalValue = {};

  public get value(): Identifier[] {
    return Object.entries(this._internalValue)
      .filter(([, value]) => value)
      .map(([key]) => key);
  }

  constructor(public readonly changeDetectorRef: ChangeDetectorRef) {
  }

  isChecked(optionId: Identifier): boolean {
    return this.value.includes(optionId);
  }

  setChecked(optionId: Identifier, checked: boolean): void {
    if (checked !== this.isChecked(optionId)) {
      this._internalValue[optionId] = checked;
      this.value$.next(this._internalValue);
      if (this.onChange) {
        this.onChange(this.value);
      }
    }
  }

  handleWrapperClicked(option: DisplayOption): void {
    this.setChecked(option.id, !this.isChecked(option.id));
  }

  writeValue(value: Identifier[]) {
    if (Array.isArray(value)) {
      this._internalValue = {};
      value.forEach(optionId => this.setChecked(optionId, true));
      this.changeDetectorRef.detectChanges();
    } else {
      this._internalValue = {};
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }
}

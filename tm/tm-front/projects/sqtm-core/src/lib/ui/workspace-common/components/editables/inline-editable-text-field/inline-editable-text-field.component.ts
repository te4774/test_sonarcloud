import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {EditableField} from '../abstract-editable-field';
import {FormControl} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {Subject, timer} from 'rxjs';
import {filter, takeUntil, tap} from 'rxjs/operators';
import {KeyCodes} from '../../../../utils/key-codes';

@Component({
  selector: 'sqtm-core-inline-editable-text-field',
  templateUrl: './inline-editable-text-field.component.html',
  styleUrls: ['./inline-editable-text-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InlineEditableTextFieldComponent implements OnInit, EditableField {

  private _editable = true;

  @Input()
  autoAsync = true;

  private keyUp = false;

  @Input()
  set editable(editable: boolean) {
    this._editable = editable;
    this.cdRef.detectChanges();
  }

  get editable(): boolean {
    return this._editable;
  }

  edit = false;

  async = false;

  @Input()
  trimValue = false;

  @Input()
  placeholder: string;

  @ViewChild('input')
  input: ElementRef;

  formControl: FormControl;

  @Input()
  showPlaceHolder = true;

  @Output()
  keyupEvent = new EventEmitter<string>();

  @Output()
  cancelEvent = new EventEmitter<string>();

  @Output()
  confirmEvent = new EventEmitter<string>();

  private _value: string;

  @Input()
  set value(value: string) {
    this.updateAndClose(value);
  }

  get value() {
    return this._value;
  }


  constructor(private cdRef: ChangeDetectorRef, private translateService: TranslateService) {
    this.formControl = new FormControl();
  }

  ngOnInit() {
    // this.formControl.setValidators(this.validators);
  }

  enableEditMode() {
    if (this.editable) {
      this.formControl.reset();
      this.formControl.setValue(this.value);
      this.formControl.updateValueAndValidity();
      this.formControl.enable();
      if (!this.edit) {
        this.edit = true;
        // kind of pulling for waiting that input is displayed and active, so we can focus into.
        const unsub = new Subject<void>();
        timer(1, 10).pipe(
          takeUntil(unsub),
          filter(() => Boolean(this.input)),
          tap(() => this.input.nativeElement.focus()),
          // auto unsub, so we don't have wild timers that run into app after an edition
          tap(() => {
            unsub.next();
            unsub.complete();
          })
        ).subscribe();
      }
    }
    return false;
  }

  disableEditMode() {
    this.edit = false;
    this.async = false;
  }

  // MS Edge doesn't support key properly so we rely on deprecated keyCode.
  handleKeyboardInput(event: KeyboardEvent) {
    const key = event.keyCode;
    if (key === KeyCodes.ENTER) {
      this.confirm();
    } else if (key === KeyCodes.ESC) {
      this.cancel();
    } else {
      this.keyupEvent.emit(this.getInputFieldValue());
    }
    return false;
  }

  cancel() {
    this.cancelEvent.emit(this.getInputFieldValue());
    this.disableEditMode();
  }

  confirm() {
    this.trimFormValue();
    if (this.formValueIsSameAsInitialValue()) {
      this.cancel();
    } else {
      this.keyUp = true;
      this.executeAutoAsync();
      this.confirmEvent.emit(this.getInputFieldValue());
    }
  }

  private formValueIsSameAsInitialValue() {
    return this.formControl.value === this.value;
  }

  private trimFormValue() {
    if (this.trimValue) {
      this.formControl.setValue(this.formControl.value.trim());
    }
  }

  beginAsync() {
    this.async = true;
    this.formControl.disable();
  }

  endAsync() {
    this.async = false;
    this.formControl.enable();
  }

  private updateAndClose(value: string) {
    this.endAsync();
    this.disableEditMode();
    this._value = value;
    this.cdRef.detectChanges();
  }

  private getInputFieldValue() {
    return this.formControl.value;
  }

  handleBlur() {
    if (!this.keyUp) {
      this.cancel();
    }
    this.keyUp = false;
  }

  getComponentClasses() {
    const cssClass = [];
    if (this.edit) {
      cssClass.push('edit');
    } else {
      cssClass.push('read');
      if (this.editable) {
        cssClass.push('editable');
      }
    }
    return cssClass;
  }

  markForCheck() {
    this.cdRef.markForCheck();
  }


  protected executeAutoAsync() {
    if (this.autoAsync) {
      this.beginAsync();
    }
  }

  private isBlank(value: string): boolean {
    return value.trim() === '';
  }
}

import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {I18nEnum, I18nEnumItem, LevelEnumItem} from '../../../../../model/level-enums/level-enum';

@Component({
  selector: 'sqtm-core-i18n-enum-icon',
  template: `
    <i nz-icon
       class="table-icon-size"
       [nzType]="resolvedItem.icon"
       [style.color]="resolvedItem.color || 'var(--current-workspace-main-color)'"
       nz-tooltip [nzTooltipTrigger]="nzTooltipTrigger" [nzTooltipTitle]="resolvedItem.i18nKey | translate">
    </i>
  `,
  styleUrls: ['./i18n-enum-icon.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class I18nEnumIconComponent<K extends string> implements OnInit {

  @Input()
  set i18nEnumItem(value: LevelEnumItem<K> | K) {
    if (typeof value === 'string') {
      this._itemKey = value;
      this.setItemByKeyInEnum();
    } else {
      this._resolvedItem = value;
    }
  }

  _i18nEnum: I18nEnum<K>;

  @Input()
  set i18nEnum(value: I18nEnum<K>) {
    this._i18nEnum = value;
    this.setItemByKeyInEnum();
  }

  _resolvedItem: I18nEnumItem<K>;

  @Input()
  disableTooltip = false;

  get resolvedItem(): I18nEnumItem<K> {
    if (!Boolean(this._resolvedItem)) {
      throw Error(`Unable to retrieve enum item. You must provide either an I18nEnumItem or a valid couple key/enum.`);
    }
    return this._resolvedItem;
  }

  _itemKey: K;

  get nzTooltipTrigger(): null | 'hover' {
    return this.disableTooltip ? null : 'hover';
  }

  constructor() {
  }

  ngOnInit(): void {
  }

  private setItemByKeyInEnum() {
    if (Boolean(this._i18nEnum) && Boolean(this._itemKey)) {
      this._resolvedItem = this._i18nEnum[this._itemKey];
    }
  }
}

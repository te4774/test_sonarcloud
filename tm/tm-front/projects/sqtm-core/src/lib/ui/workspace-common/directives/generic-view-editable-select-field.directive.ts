import {AfterViewInit, Directive, Host, HostListener, Input, OnDestroy, OnInit} from '@angular/core';
import {distinctUntilChanged, map, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {SqtmGenericEntityState} from '../../../core/services/genric-entity-view/generic-entity-view-state';
import {
  EditableSelectFieldComponent
} from '../components/editables/editable-select-field/editable-select-field.component';
import {Option} from '../../../model/option.model';
import {GenericEntityViewService} from '../../../core/services/genric-entity-view/generic-entity-view.service';

@Directive({
  selector: '[sqtmCoreGenericViewEditableSelectField]'
})
export class GenericViewEditableSelectFieldDirective<E extends SqtmGenericEntityState, T extends string>
  implements OnInit, OnDestroy, AfterViewInit {

  @Input('sqtmCoreGenericViewEditableSelectField')
  fieldName: keyof E;

  unsub$ = new Subject<void>();

  constructor(private genericEntityViewService: GenericEntityViewService<E, T>, @Host() private editable: EditableSelectFieldComponent) {
  }

  @HostListener('confirmEvent', ['$event'])
  onConfirm(option: Option) {
    const value = option?.value as any; // Option may be null if allowEmpty is set to true
    this.editable.beginAsync();
    this.genericEntityViewService.update(this.fieldName, value); // cannot use type system on dynamic inputs...
  }

  ngAfterViewInit(): void {
    this.genericEntityViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => componentData[componentData.type]),
      // If value has changed in store, the update is a success.
      // If there is any error server side, the error stream will emit, and the value will not be changed.
      distinctUntilChanged()
    ).subscribe((entity) => {
      let value = entity[this.fieldName as string];

      // For selected placeholder options, value must stay undefined so that the dropdown list menu works properly.
      if (value === null) {
        value = undefined;
      }

      this.editable.value = typeof value === 'number' ? value.toString() : value;
      this.editable.markForCheck();
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}

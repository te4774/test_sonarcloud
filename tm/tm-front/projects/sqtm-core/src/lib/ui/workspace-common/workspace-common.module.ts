import {DragDropModule} from '@angular/cdk/drag-drop';
import {OverlayModule} from '@angular/cdk/overlay';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {CKEditorModule} from 'ckeditor4-angular';
import {NzAutocompleteModule} from 'ng-zorro-antd/auto-complete';
import {NzBadgeModule} from 'ng-zorro-antd/badge';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzNoAnimationModule} from 'ng-zorro-antd/core/no-animation';
import {NzDatePickerModule} from 'ng-zorro-antd/date-picker';
import {NzDividerModule} from 'ng-zorro-antd/divider';
import {NzDrawerModule} from 'ng-zorro-antd/drawer';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzInputNumberModule} from 'ng-zorro-antd/input-number';
import {NzPopoverModule} from 'ng-zorro-antd/popover';
import {NzRadioModule} from 'ng-zorro-antd/radio';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzSwitchModule} from 'ng-zorro-antd/switch';
import {NzTagModule} from 'ng-zorro-antd/tag';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {ColorPickerModule} from 'ngx-color-picker';
import {SvgModule} from '../svg/svg.module';
import {UiManagerModule} from '../ui-manager/ui-manager.module';
import {
  CapsuleInformationComponent
} from './components/capsule-information/capsule-information/capsule-information.component';
import {CapsuleLinkComponent} from './components/capsule-information/capsule-link/capsule-link-component';
import {
  CapsuleTestCaseLastExecutionStatusComponent
} from './components/capsule-information/capsule-test-case-last-execution-status/capsule-test-case-last-execution-status.component';
import {DrawerComponent} from './components/drawer/drawer.component';
import {
  AutocompleteOptionComponent
} from './components/editables/autocomplete-field/autocomplete-option/autocomplete-option.component';
import {AutocompleteComponent} from './components/editables/autocomplete-field/autocomplete.component';
import {
  EditableActionTextFieldComponent
} from './components/editables/editable-action-text-field/editable-action-text-field.component';
import {
  EditableAutocompleteFieldComponent
} from './components/editables/editable-autocomplete-field/editable-autocomplete-field.component';
import {EditableCheckBoxComponent} from './components/editables/editable-check-box/editable-check-box.component';
import {EditableDateFieldComponent} from './components/editables/editable-date-field/editable-date-field.component';
import {
  EditableDraggableTagFieldComponent
} from './components/editables/editable-draggable-tag-field/editable-draggable-tag-field.component';
import {
  EditableNumericFieldComponent
} from './components/editables/editable-numeric-field/editable-numeric-field.component';
import {
  EditableRadioButtonComponent
} from './components/editables/editable-radio-button/editable-radio-button.component';
import {EditableRichTextComponent} from './components/editables/editable-rich-text/editable-rich-text.component';
import {
  EditableSearchableSelectFieldComponent
} from './components/editables/editable-searchable-select-field/editable-searchable-select-field.component';
import {
  EditableSelectFieldComponent
} from './components/editables/editable-select-field/editable-select-field.component';
import {
  EditableSelectInfolistFieldComponent
} from './components/editables/editable-select-infolist-field/editable-select-infolist-field.component';
import {
  EditableSelectLevelEnumFieldComponent
} from './components/editables/editable-select-level-enum-field/editable-select-level-enum-field.component';
import {EditableTagFieldComponent} from './components/editables/editable-tag-field/editable-tag-field.component';
import {
  EditableTextAreaFieldComponent
} from './components/editables/editable-text-area-field/editable-text-area-field.component';
import {EditableTextFieldComponent} from './components/editables/editable-text-field/editable-text-field.component';
import {
  InlineEditableTextFieldComponent
} from './components/editables/inline-editable-text-field/inline-editable-text-field.component';
import {EntityViewHeaderComponent} from './components/entity-view-header/entity-view-header.component';
import {
  ActionAutocompleteFieldOptionComponent
} from './components/forms/action-autocomplete-field/action-autocomplete-field-option/action-autocomplete-field-option.component';
import {
  ActionAutocompleteFieldComponent
} from './components/forms/action-autocomplete-field/action-autocomplete-field.component';
import {
  ActionTextFieldComponent
} from './components/forms/action-word-field/action-text-field/action-text-field.component';
import {AuditableFieldComponent} from './components/forms/auditable-field/auditable-field.component';
import {AutocompleteFieldComponent} from './components/forms/autocomplete-field/autocomplete-field.component';
import {
  CascadingSelectFieldComponent
} from './components/forms/cascading-select-field/cascading-select-field.component';
import {CheckboxGroupFieldComponent} from './components/forms/checkbox-group-field/checkbox-group-field.component';
import {
  ColorPickerSelectFieldComponent
} from './components/forms/color-picker/color-picker-select-field/color-picker-select-field.component';
import {ColorPickerComponent} from './components/forms/color-picker/color-picker/color-picker.component';
import {DatePickerComponent} from './components/forms/date-picker/date-picker.component';
import {
  GroupedMultiListFieldComponent
} from './components/forms/grouped-multi-list-field/grouped-multi-list-field.component';
import {GroupedMultiListComponent} from './components/forms/grouped-multi-list/grouped-multi-list.component';
import {IconPickerFieldComponent} from './components/forms/icon-picker/icon-picker-field/icon-picker-field.component';
import {IconPickerComponent} from './components/forms/icon-picker/icon-picker/icon-picker.component';
import {LabelComponent} from './components/forms/label/label.component';
import {ListPanelComponent} from './components/forms/list-panel/list-panel.component';
import {MultipleRadioButtonComponent} from './components/forms/multiple-radio-button/multiple-radio-button.component';
import {
  OptionalGroupedMultiListComponent
} from './components/forms/optional-grouped-multi-list/optional-grouped-multi-list.component';
import {OptionalSelectFieldComponent} from './components/forms/optional-select-field/optional-select-field.component';
import {RadioSelectFieldComponent} from './components/forms/radio-select-field/radio-select-field.component';
import {RichTextFieldComponent} from './components/forms/rich-text-field/rich-text-field.component';
import {SelectFieldComponent} from './components/forms/select-field/select-field.component';
import {SimpleCheckboxComponent} from './components/forms/simple-checkbox/simple-checkbox.component';
import {SwitchFieldComponent} from './components/forms/switch-field/switch-field.component';
import {TextAreaFieldComponent} from './components/forms/text-area-field/text-area-field.component';
import {TextFieldComponent} from './components/forms/text-field/text-field.component';
import {TextResearchFieldComponent} from './components/forms/text-research-field/text-research-field.component';
import {
  ThirdPartyCredentialsFormComponent
} from './components/forms/third-party-credentials-form/third-party-credentials-form.component';
import {UserListSelectorComponent} from './components/forms/user-list-selector/user-list-selector.component';
import {FullPageLoadingIconComponent} from './components/icons/full-page-loading-icon/full-page-loading-icon.component';
import {I18nEnumIconComponent} from './components/icons/i18n-enum-icon/i18n-enum-icon.component';
import {ToggleIconComponent} from './components/icons/toggle-icon/toggle-icon.component';
import {
  CompactCollapsePanelComponent
} from './components/layout/compact-collapse-panel/compact-collapse-panel.component';
import {
  LicenseInformationBannerComponent
} from './components/license-information-banner/license-information-banner.component';
import {StepNavigatorComponent} from './components/step-navigator/step-navigator.component';
import {DynamicComponentDirective} from './directives/dynamic-component.directive';
import {EntityViewEditableCheckboxDirective} from './directives/entity-view-editable-checkbox.directive';
import {EntityViewEditableInfolistDirective} from './directives/entity-view-editable-infolist.directive';
import {EntityViewEditableRichTextFieldDirective} from './directives/entity-view-editable-rich-text-field.directive';
import {
  EntityViewEditableSelectLevelEnumFieldDirective
} from './directives/entity-view-editable-select-level-enum-field.directive';
import {EntityViewEditableTextFieldDirective} from './directives/entity-view-editable-text-field.directive';
import {GenericViewEditableCheckboxFieldDirective} from './directives/generic-view-editable-checkbox-field.directive';
import {GenericViewEditableDateCufValueDirective} from './directives/generic-view-editable-date-cuf-value.directive';
import {GenericViewEditableDateFieldDirective} from './directives/generic-view-editable-date-field.directive';
import {GenericViewEditableNumericFieldDirective} from './directives/generic-view-editable-numeric-field.directive';
import {GenericViewEditableRichTextFieldDirective} from './directives/generic-view-editable-rich-text-field.directive';
import {GenericViewEditableSelectFieldDirective} from './directives/generic-view-editable-select-field.directive';
import {
  GenericViewEditableSelectLevelEnumFieldDirective
} from './directives/generic-view-editable-select-level-enum-field.directive';
import {GenericViewEditableTextFieldDirective} from './directives/generic-view-editable-text-field.directive';
import {GenericViewSwitchFieldDirective} from './directives/generic-view-switch-field.directive';
import {KeyupStopPropagationDirective} from './directives/keyup-stop-propagation.directive';
import {LabelTooltipDirective} from './directives/label-tooltip.directive';
import {ResizableDirective} from './directives/resizable.directive';
import {CapitalizePipe} from './pipes/capitalize.pipe';
import {ConvertFileSizePipe} from './pipes/convert-file-size.pipe';
import {SafeRichContentPipe} from './pipes/safe-rich-content.pipe';
import {HistoryBackButtonComponent} from './components/layout/history-back-button/history-back-button.component';
import {BackButtonComponent} from './components/layout/back-button/back-button.component';
import {RefreshOnResizeDirective} from './directives/refresh-on-resize.directive';
import {NumericFieldComponent} from './components/forms/numeric-field/numeric-field.component';
import {EntityNotFoundComponent} from './components/entity-not-found/entity-not-found.component';
import {
  EditableTagListFieldComponent
} from './components/editables/editable-tag-list-field/editable-tag-list-field.component';

@NgModule({

  declarations: [
    ActionAutocompleteFieldComponent,
    ActionAutocompleteFieldOptionComponent,
    ActionTextFieldComponent,
    AuditableFieldComponent,
    AutocompleteComponent,
    AutocompleteFieldComponent,
    AutocompleteOptionComponent,
    BackButtonComponent,
    CapitalizePipe,
    CapsuleInformationComponent,
    CapsuleLinkComponent,
    CapsuleTestCaseLastExecutionStatusComponent,
    CascadingSelectFieldComponent,
    CheckboxGroupFieldComponent,
    ColorPickerComponent,
    ColorPickerSelectFieldComponent,
    CompactCollapsePanelComponent,
    ConvertFileSizePipe,
    DatePickerComponent,
    DrawerComponent,
    DynamicComponentDirective,
    EditableActionTextFieldComponent,
    EditableAutocompleteFieldComponent,
    EditableCheckBoxComponent,
    EditableDateFieldComponent,
    EditableDraggableTagFieldComponent,
    EditableNumericFieldComponent,
    EditableRadioButtonComponent,
    EditableRichTextComponent,
    EditableSearchableSelectFieldComponent,
    EditableSelectFieldComponent,
    EditableSelectInfolistFieldComponent,
    EditableSelectLevelEnumFieldComponent,
    EditableTagFieldComponent,
    EditableTagListFieldComponent,
    EditableTextAreaFieldComponent,
    EditableTextFieldComponent,
    EntityViewEditableCheckboxDirective,
    EntityViewEditableInfolistDirective,
    EntityViewEditableRichTextFieldDirective,
    EntityViewEditableSelectLevelEnumFieldDirective,
    EntityViewEditableTextFieldDirective,
    EntityViewHeaderComponent,
    FullPageLoadingIconComponent,
    GenericViewEditableCheckboxFieldDirective,
    GenericViewEditableDateCufValueDirective,
    GenericViewEditableDateFieldDirective,
    GenericViewEditableNumericFieldDirective,
    GenericViewEditableRichTextFieldDirective,
    GenericViewEditableSelectFieldDirective,
    GenericViewEditableSelectLevelEnumFieldDirective,
    GenericViewEditableTextFieldDirective,
    GenericViewSwitchFieldDirective,
    GroupedMultiListComponent,
    GroupedMultiListFieldComponent,
    HistoryBackButtonComponent,
    I18nEnumIconComponent,
    IconPickerComponent,
    IconPickerFieldComponent,
    InlineEditableTextFieldComponent,
    KeyupStopPropagationDirective,
    LabelComponent,
    LabelTooltipDirective,
    LicenseInformationBannerComponent,
    ListPanelComponent,
    MultipleRadioButtonComponent,
    OptionalGroupedMultiListComponent,
    OptionalSelectFieldComponent,
    RadioSelectFieldComponent,
    RefreshOnResizeDirective,
    ResizableDirective,
    RichTextFieldComponent,
    SafeRichContentPipe,
    SelectFieldComponent,
    SimpleCheckboxComponent,
    StepNavigatorComponent,
    SwitchFieldComponent,
    TextAreaFieldComponent,
    TextFieldComponent,
    TextResearchFieldComponent,
    ThirdPartyCredentialsFormComponent,
    ToggleIconComponent,
    UserListSelectorComponent,
    NumericFieldComponent,
    EntityNotFoundComponent,
  ],
  imports: [
    CommonModule,
    SvgModule,
    NzIconModule,
    NzBadgeModule,
    NzButtonModule,
    NzDrawerModule,
    NzInputModule,
    TranslateModule.forChild(),
    ReactiveFormsModule,
    FormsModule,
    OverlayModule,
    PortalModule,
    CKEditorModule,
    NzDividerModule,
    NzDatePickerModule,
    NzCheckboxModule,
    NzTagModule,
    NzPopoverModule,
    NzInputNumberModule,
    NzRadioModule,
    UiManagerModule,
    NzToolTipModule,
    ScrollingModule,
    DragDropModule,
    NzSwitchModule,
    ColorPickerModule,
    NzAutocompleteModule,
    NzNoAnimationModule,
    NzSelectModule,
  ],
  exports: [
    ActionAutocompleteFieldComponent,
    ActionAutocompleteFieldOptionComponent,
    ActionTextFieldComponent,
    AuditableFieldComponent,
    AutocompleteFieldComponent,
    BackButtonComponent,
    CapitalizePipe,
    CapsuleInformationComponent,
    CapsuleLinkComponent,
    CapsuleTestCaseLastExecutionStatusComponent,
    CascadingSelectFieldComponent,
    CheckboxGroupFieldComponent,
    ColorPickerSelectFieldComponent,
    CompactCollapsePanelComponent,
    ConvertFileSizePipe,
    DatePickerComponent,
    DrawerComponent,
    DynamicComponentDirective,
    EditableActionTextFieldComponent,
    EditableAutocompleteFieldComponent,
    EditableCheckBoxComponent,
    EditableDateFieldComponent,
    EditableDraggableTagFieldComponent,
    EditableNumericFieldComponent,
    EditableRadioButtonComponent,
    EditableRichTextComponent,
    EditableSearchableSelectFieldComponent,
    EditableSelectFieldComponent,
    EditableSelectInfolistFieldComponent,
    EditableSelectLevelEnumFieldComponent,
    EditableTagFieldComponent,
    EditableTagListFieldComponent,
    EditableTextAreaFieldComponent,
    EditableTextFieldComponent,
    EntityNotFoundComponent,
    EntityViewEditableCheckboxDirective,
    EntityViewEditableInfolistDirective,
    EntityViewEditableRichTextFieldDirective,
    EntityViewEditableSelectLevelEnumFieldDirective,
    EntityViewEditableTextFieldDirective,
    EntityViewHeaderComponent,
    FullPageLoadingIconComponent,
    GenericViewEditableCheckboxFieldDirective,
    GenericViewEditableDateCufValueDirective,
    GenericViewEditableDateFieldDirective,
    GenericViewEditableNumericFieldDirective,
    GenericViewEditableRichTextFieldDirective,
    GenericViewEditableSelectFieldDirective,
    GenericViewEditableSelectLevelEnumFieldDirective,
    GenericViewEditableTextFieldDirective,
    GenericViewSwitchFieldDirective,
    GroupedMultiListComponent,
    GroupedMultiListFieldComponent,
    HistoryBackButtonComponent,
    I18nEnumIconComponent,
    IconPickerComponent,
    IconPickerFieldComponent,
    InlineEditableTextFieldComponent,
    KeyupStopPropagationDirective,
    LabelComponent,
    LabelTooltipDirective,
    LicenseInformationBannerComponent,
    ListPanelComponent,
    MultipleRadioButtonComponent,
    NumericFieldComponent,
    OptionalGroupedMultiListComponent,
    OptionalSelectFieldComponent,
    RadioSelectFieldComponent,
    RefreshOnResizeDirective,
    ResizableDirective,
    RichTextFieldComponent,
    SafeRichContentPipe,
    SelectFieldComponent,
    SimpleCheckboxComponent,
    StepNavigatorComponent,
    SwitchFieldComponent,
    TextAreaFieldComponent,
    TextFieldComponent,
    TextResearchFieldComponent,
    ThirdPartyCredentialsFormComponent,
    ToggleIconComponent,
    UserListSelectorComponent,
  ],
})
export class WorkspaceCommonModule {
}

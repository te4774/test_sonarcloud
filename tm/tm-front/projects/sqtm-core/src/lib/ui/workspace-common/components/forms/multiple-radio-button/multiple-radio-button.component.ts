import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ListItem} from '../grouped-multi-list/grouped-multi-list.component';

@Component({
  selector: 'sqtm-core-multiple-radio-button',
  templateUrl: './multiple-radio-button.component.html',
  styleUrls: ['./multiple-radio-button.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultipleRadioButtonComponent implements OnInit {

  private _listItems: ListItem[] = [];

  get listItems(): ListItem[] {
    return this._listItems;
  }

  @Input()
  set listItems(items: ListItem[]) {
    this._listItems = items;
    this.updateSelectedOption();
  }

  @Input()
  selectionRequired = true;

  @Output()
  itemSelectionChanged = new EventEmitter<ListItem>();

  selectedItemId: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  private updateSelectedOption() {
    const selectedItems = this._listItems
      .filter(item => item.selected);
    if (selectedItems.length !== 1 && this.selectionRequired) {
      throw Error('You must provide a list of items with one item selected');
    }
    if (selectedItems.length > 0) {
      this.selectedItemId = selectedItems[0].id.toString();
    }
  }

  changeSelection() {
    const selectedItem = this._listItems.find(item => item.id === this.selectedItemId);
    this.itemSelectionChanged.next(selectedItem);
  }
}

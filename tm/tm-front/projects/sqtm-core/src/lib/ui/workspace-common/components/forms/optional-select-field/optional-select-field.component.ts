import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DisplayOption} from '../display-option';
import {EditableSelectFieldComponent} from '../../editables/editable-select-field/editable-select-field.component';

@Component({
  selector: 'sqtm-core-optional-select-field',
  template: `
    <div [attr.data-test-field-id]="fieldName" class="flex-row p-r-5 optionals-center">
      <label [style]="computedLabelStyle" nz-checkbox [nzChecked]="check" (nzCheckedChange)="editField($event)"
             [nzDisabled]="disable" [attr.data-test-component-id]="'activate-checkbox'">
        {{i18nKey | translate}}
      </label>
      <nz-select #selectField
                 [style]="computedSelectStyle"
                 [ngModel]="selectedValue"
                 (ngModelChange)="change($event)"
                 [nzDisabled]="!selectable()"
                 [nzPlaceHolder]="selectPlaceHolder">
        <nz-option *ngFor="let option of displayOptions" [nzValue]="option.id"
                   [nzLabel]="option.label">{{option.label}}</nz-option>
      </nz-select>
    </div>
  `,
  styleUrls: ['./optional-select-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionalSelectFieldComponent implements OnInit {

  @Input()
  displayOptions: DisplayOption[] = [];

  @Input()
  selectedValue: string | number;

  @Input()
  disable: boolean;

  @Input()
  fieldName: string;

  @Input()
  i18nKey: string;

  @Input()
  selectPlaceHolder = '';

  check = false;

  @Input()
  select = true;

  // That's probably bad design to have styling inputs but I couldn't find a way around without
  //  splitting this component in two
  @Input()
  labelWidthRatio = 0.5;

  @Output()
  checkEvent = new EventEmitter();

  @ViewChild('selectField')
  selectField: EditableSelectFieldComponent;

  get computedLabelStyle(): string {
    return `width: ${this.labelWidthRatio * 100}%`;
  }

  get computedSelectStyle(): string {
    return `width: ${(1 - this.labelWidthRatio) * 100}%`;
  }

  constructor() {
  }

  ngOnInit() {
  }

  change($event) {
    this.selectedValue = $event;
  }

  editField($event) {
    this.check = $event;
    this.checkEvent.emit($event);
  }

  selectable() {
    return this.check && this.select;
  }

}

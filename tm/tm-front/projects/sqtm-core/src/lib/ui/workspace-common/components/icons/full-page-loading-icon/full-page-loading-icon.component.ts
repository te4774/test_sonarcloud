import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-core-full-page-loading-icon',
  templateUrl: './full-page-loading-icon.component.html',
  styleUrls: ['./full-page-loading-icon.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FullPageLoadingIconComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

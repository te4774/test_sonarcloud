import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {AbstractCapsuleInformationDirective} from '../abstract-capsule-information';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-core-capsule-link',
  templateUrl: './capsule-link-component.html',
  styleUrls: ['./capsule-link-component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CapsuleLinkComponent  extends AbstractCapsuleInformationDirective implements OnInit {

  constructor(translateService: TranslateService) {
    super(translateService);
  }

  ngOnInit(): void {
  }

}

import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-core-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LabelComponent implements OnInit {
  @Input()
  text: string;

  constructor() {}

  ngOnInit() {
  }
}

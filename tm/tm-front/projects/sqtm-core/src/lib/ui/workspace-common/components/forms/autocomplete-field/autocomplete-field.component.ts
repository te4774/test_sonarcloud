import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {DisplayOption} from '../display-option';

@Component({
  selector: 'sqtm-core-autocomplete-field',
  templateUrl: './autocomplete-field.component.html',
  styleUrls: ['./autocomplete-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutocompleteFieldComponent implements OnInit {
  @Input() options: DisplayOption[] = [];
  @Input() placeholder = '';
  @Input() value: string;
  @Input() size: 'small' | 'default' | 'large' = 'default';

  @Output() valueChanged = new EventEmitter<any>();

  @ViewChild('textInput') textInput: ElementRef;

  private _filteredOptions: DisplayOption[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  get showClearButton(): boolean {
    return Boolean(this.value)
      && this.value.length > 0;
  }

  get visibleOptionLabels(): string[] {
    return this._filteredOptions.map(option => option.label);
  }

  onInput(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    this._filteredOptions = this.options.filter(option => option.label.includes(value));
  }

  handleModelChange($event: any): void {
    this.valueChanged.emit($event);
  }

  clearInputAndShowList(): void {
    this.value = '';
    this.valueChanged.emit('');
    this.textInput.nativeElement.focus();
  }
}

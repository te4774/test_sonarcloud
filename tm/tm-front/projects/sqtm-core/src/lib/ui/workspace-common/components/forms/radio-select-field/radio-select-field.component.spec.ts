import {ComponentFixture, TestBed} from '@angular/core/testing';

import {RadioSelectFieldComponent} from './radio-select-field.component';

describe('RadioSelectFieldComponent', () => {
  let component: RadioSelectFieldComponent;
  let fixture: ComponentFixture<RadioSelectFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadioSelectFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioSelectFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Directive, ElementRef, Input, OnDestroy, OnInit} from '@angular/core';
import {fromEvent, Subject} from 'rxjs';
import {filter, takeUntil, withLatestFrom} from 'rxjs/operators';
import {NzTooltipDirective} from 'ng-zorro-antd/tooltip';
import {DragAndDropService} from '../../drag-and-drop/drag-and-drop.service';

@Directive({
  selector: '[sqtmCoreLabelTooltip]'
})
export class LabelTooltipDirective implements OnInit, OnDestroy {

  @Input('sqtmCoreLabelTooltip')
  title: string;

  @Input('ellipsis')
  set ellipsis(showEllipsis: boolean) {
    const native = this.element.nativeElement as HTMLElement;

    if (showEllipsis) {
      native.classList.add('txt-ellipsis');
    } else {
      native.classList.remove('txt-ellipsis');
    }
  }

  unsub$ = new Subject<void>();

  constructor(private element: ElementRef, private nzToolTipDirective: NzTooltipDirective, private dndService: DragAndDropService) {
  }

  ngOnInit(): void {
    fromEvent(this.element.nativeElement, 'mouseenter').pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.dndService.dragAndDrop$),
      // tap(([, dragging]) => console.log(`mouseenter dragging is ${dragging}. mouseenter overflow is ${this.hasOverflow()}`)),
      filter(() => this.hasOverflow()),
      filter(([, dragging]) => !dragging),
    ).subscribe(() => {
      // Since ng-zorro 9.1.0, changing the directive's title won't work so we need to also set the inner component's nzTitle
      this.nzToolTipDirective.component.nzTitle = this.title || this.element.nativeElement.innerText;
      this.nzToolTipDirective.show();
    });

    fromEvent(this.element.nativeElement, 'mouseleave').pipe(
      takeUntil(this.unsub$),
      filter(() => this.nzToolTipDirective.elementRef !== null)
    ).subscribe(() => {
      this.nzToolTipDirective.component.nzTitle = '';
      this.nzToolTipDirective.hide();
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private hasOverflow(): boolean {
    return this.element.nativeElement.offsetWidth < this.element.nativeElement.scrollWidth;
  }

}

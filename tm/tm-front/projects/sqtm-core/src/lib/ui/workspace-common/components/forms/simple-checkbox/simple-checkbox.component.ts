import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'sqtm-core-simple-checkbox',
  templateUrl: './simple-checkbox.component.html',
  styleUrls: ['./simple-checkbox.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleCheckboxComponent implements OnInit {

  @Input()
  value = false;

  @Input()
  disabled = false;

  @Output()
  clickedInActiveMode = new EventEmitter<MouseEvent>();

  constructor() {
  }

  ngOnInit(): void {
  }

  handleClick($event: MouseEvent) {
    if (!this.disabled) {
      this.clickedInActiveMode.emit($event);
    }
  }

}

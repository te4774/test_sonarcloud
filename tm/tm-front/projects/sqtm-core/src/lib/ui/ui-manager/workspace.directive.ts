///
///     This file is part of the Squashtest platform.
///     Copyright (C) Henix, henix.fr
///
///     See the NOTICE file distributed with this work for additional
///     information regarding copyright ownership.
///
///     This is free software: you can redistribute it and/or modify
///     it under the terms of the GNU Lesser General Public License as published by
///     the Free Software Foundation, either version 3 of the License, or
///     (at your option) any later version.
///
///     this software is distributed in the hope that it will be useful,
///     but WITHOUT ANY WARRANTY; without even the implied warranty of
///     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
///     GNU Lesser General Public License for more details.
///
///     You should have received a copy of the GNU Lesser General Public License
///     along with this software.  If not, see <http://www.gnu.org/licenses/>.
///

import {Directive, ElementRef, Host, Inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {Subject} from 'rxjs';
import {take, takeUntil} from 'rxjs/operators';
import {CssProperties, Theme, Workspaces, WorkspaceTheme} from './theme.model';
import {UiManagerService} from '../ui-manager/ui-manager.service';
import {uiManagerLogger} from './ui-manager.logger';

const logger = uiManagerLogger.compose('WorkspaceDirective');

/** @dynamic */
@Directive({
  selector: '[sqtmCoreWorkspace]'
})
export class WorkspaceDirective implements OnInit, OnDestroy, OnChanges {

  @Input('sqtmCoreWorkspace')
  workspaceName: string;

  // tslint:disable-next-line:no-input-rename
  @Input('sqtmCoreWorkspaceTheme')
  workspaceTheme: string;

  @Input()
  localTheme = false;

  private unsub$ = new Subject<void>();

  constructor(private uiManager: UiManagerService,
              @Inject(DOCUMENT) private document: any,
              @Host() private host: ElementRef) {
  }

  ngOnInit(): void {
    this.uiManager.selectedTheme$
      .pipe(takeUntil(this.unsub$))
      .subscribe(
        (theme: Theme) => {
          this.applyTheme(theme);
        }
      );

    if (this.isGlobal()) {
      this.uiManager.changeCurrentWorkspace(this.workspaceName);
    }
  }

  private isGlobal() {
    return !this.localTheme;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }


  private applyTheme(theme: Theme) {
    if (this.isGlobal()) {
      this.applyGlobalTheme(theme);
    }
    if (this.workspaceName) {
      this.applyCurrentWorkspaceTheme(theme);
    }
  }

  private applyGlobalTheme(theme: Theme) {
    const globalTheme = theme['global-theme'];
    this.updateCssVariables(globalTheme);
    this.extractWorkspaceColors(theme);
  }

  private applyCurrentWorkspaceTheme(theme: Theme) {
    const currentWorkspaceTheme = this.findCurrentWorkspaceName();
    const workspaceTheme: WorkspaceTheme = theme[currentWorkspaceTheme];
    if (workspaceTheme) {
      logger.debug(`Found current workspace theme : ${currentWorkspaceTheme}`, [workspaceTheme]);
      const cssProps: CssProperties = {};
      for (const key of Object.keys(workspaceTheme)) {
        cssProps['current-workspace-' + key] = workspaceTheme[key];
      }
      // overriding the wavew of ng zorro. Aka the little flash of color around objects when clicking
      cssProps['antd-wave-shadow-color'] = workspaceTheme['brightest-color'];
      this.updateCssVariables(cssProps);
    } else {
      throw new Error(`No key ${currentWorkspaceTheme} defined in theme ${theme.id}`);
    }
  }

  private findCurrentWorkspaceName() {
    return this.workspaceTheme || this.workspaceName;
  }

  private updateCssVariables(properties) {
    for (const key of Object.keys(properties)) {
      const cssVariableName = `--${key}`;
      const cssVariableValue = properties[key];
      this.setCssVariable(cssVariableName, cssVariableValue);
    }
  }

  private setCssVariable(cssVariableName: string, cssVariableValue) {
    if (this.localTheme) {
      logger.trace(`Setting ${cssVariableName} ${cssVariableValue}`);
      const element = (this.host.nativeElement) as HTMLElement;
      element.style.setProperty(cssVariableName, cssVariableValue);
    } else {
      this.document.documentElement.style.setProperty(cssVariableName, cssVariableValue);
    }
  }

  private extractWorkspaceColors(theme: Theme) {
    const properties: CssProperties = {};
    for (const key of Object.keys(Workspaces)) {
      const workspaceTheme: WorkspaceTheme = theme[key];
      const colorKey = `${key}-main-color`;
      properties[colorKey] = workspaceTheme['main-color'];
    }
    this.updateCssVariables(properties);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.uiManager.selectedTheme$.pipe(
      take(1)
    ).subscribe(theme => this.applyTheme(theme));
  }


}

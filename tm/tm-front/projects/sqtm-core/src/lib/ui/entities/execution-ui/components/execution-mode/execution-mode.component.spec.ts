import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ExecutionModeComponent} from './execution-mode.component';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestCaseExecutionMode} from '../../../../../model/level-enums/level-enum';

describe('ExecutionModeComponent', () => {
  let component: ExecutionModeComponent;
  let fixture: ComponentFixture<ExecutionModeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ExecutionModeComponent],
      imports: [TranslateModule.forRoot()],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionModeComponent);
    component = fixture.componentInstance;
    component.executionMode = TestCaseExecutionMode.MANUAL;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';
import {InfoListItem} from '../../../../../model/infolist/infolistitem.model';
import {Observable, of} from 'rxjs';
import {take} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-core-category-label',
  templateUrl: './category-label.component.html',
  styleUrls: ['./category-label.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryLabelComponent implements OnInit {

  infoListItem$: Observable<InfoListItem>;

  @Input()
  set category(value: number | InfoListItem) {
    if (typeof value === 'number') {
      this.infoListItem$ = this.referentialDataService.findInfoListItem(value).pipe(take(1));
    } else {
      this.infoListItem$ = of(value);
    }
  }

  constructor(private referentialDataService: ReferentialDataService, private translateService: TranslateService) {
  }

  ngOnInit(): void {
  }

  getLabel(infoListItem: InfoListItem): string {
    let label = infoListItem.label;
    if (infoListItem.system) {
      label = this.translateService.instant(`sqtm-core.entity.${infoListItem.label}`);
    }
    return label;
  }

  getIcon(infoListItem: InfoListItem): string {
    return `sqtm-core-infolist-item:${infoListItem.iconName}`;
  }
}

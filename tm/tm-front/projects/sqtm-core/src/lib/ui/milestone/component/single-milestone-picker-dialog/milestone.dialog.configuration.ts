import type {Milestone} from '../../../../model/milestone/milestone.model';
import {SingleMilestonePickerDialogComponent} from './single-milestone-picker-dialog.component';
import type {DialogConfiguration} from '../../../dialog/services/dialog-feature.state';
import type {Identifier} from '../../../../model/entity.model';

export interface MilestoneDialogConfiguration {
  id: string;
  titleKey: string;
  milestones: Milestone[];
  disableColumns?: Identifier[];
}

export interface SingleSelectMilestoneDialogConfiguration extends MilestoneDialogConfiguration {
  selectedMilestone?: number;
}

export interface MultipleSelectMilestoneDialogConfiguration extends MilestoneDialogConfiguration {
  selectedMilestones: number[];
  warningMessageKey: string;
}

/**
 * Build a standard single milestone dialog picker
 * @param milestones The Milestones to display
 * @param selectedMilestone The previously selected milestone
 */
export function buildSingleMilestonePickerDialogDefinition(milestones: Milestone[], selectedMilestone: Pick<Milestone, 'id'> = {id: null}):
  DialogConfiguration<SingleSelectMilestoneDialogConfiguration> {
  const configuration: SingleSelectMilestoneDialogConfiguration = {
    id: 'single-milestone-picker-dialog',
    titleKey: 'sqtm-core.dialog.milestone.picker.single',
    milestones,
    selectedMilestone: selectedMilestone.id,
    disableColumns: ['range']
  };
  return {
    id: 'milestone-filter',
    component: SingleMilestonePickerDialogComponent,
    height: 500,
    width: 800,
    data: configuration,
  };
}

import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {GridDefinition} from '../../../grid/model/grid-definition.model';
import {grid} from '../../../../model/grids/grid-builders';
import {Extendable, Fixed} from '../../../grid/model/column-definition.model';
import {MilestoneRange, MilestoneStatus} from '../../../../model/level-enums/level-enum';
import {sortDate} from '../../../grid/components/cell-renderers/date-cell-renderer/date-cell-renderer.component';
import {PaginationConfigBuilder} from '../../../grid/model/grid-definition.builder';
import {GridService} from '../../../grid/services/grid.service';
import {gridServiceFactory} from '../../../grid/grid.service.provider';
import {Milestone} from '../../../../model/milestone/milestone.model';
import {map, takeUntil} from 'rxjs/operators';
import {GridFilter} from '../../../grid/model/state/filter.state';
import {Subject} from 'rxjs';
import {
  ToggleSelectionHeaderRendererComponent
} from '../../../grid/components/header-renderers/toggle-selection-header-renderer/toggle-selection-header-renderer.component';
import {Identifier} from '../../../../model/entity.model';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {RestService} from '../../../../core/services/rest.service';
import {
  dateColumn,
  richTextColumn,
  selectRowColumn,
  textColumn
} from '../../../grid/model/common-column-definition.builders';

export function multipleMilestonePickerDefinition(): GridDefinition {
  return grid(null)
    .withColumns(
      [
        selectRowColumn()
          .withHeaderRenderer(ToggleSelectionHeaderRendererComponent)
          .changeWidthCalculationStrategy(new Fixed(40)),
        textColumn('label')
          .withI18nKey('sqtm-core.entity.generic.name.label')
          .changeWidthCalculationStrategy(new Extendable(100, 0.8)),
        textColumn('status')
          .withI18nKey('sqtm-core.entity.milestone.status.label')
          .withEnumRenderer(MilestoneStatus, false, true)
          .isEditable(false),
        dateColumn('endDate')
          .withI18nKey('sqtm-core.entity.milestone.term.label')
          .withSortFunction(sortDate),
        textColumn('range')
          .withI18nKey('sqtm-core.entity.milestone.range.label')
          .withEnumRenderer(MilestoneRange, false, true)
          .isEditable(false),
        richTextColumn('description')
          .disableSort()
          .withI18nKey('sqtm-core.entity.milestone.description')
          .changeWidthCalculationStrategy(new Extendable(100, 1))
      ]
    )
    .withPagination(
      new PaginationConfigBuilder().inactive()
    )
    .disableRightToolBar()
    .enableMultipleColumnsFiltering(['label'])
    .build();
}

export const MULTIPLE_MILESTONES_PICKER_GRID = 'multiple-milestones-picker-grid';

@Component({
  selector: 'sqtm-core-multiple-milestone-picker',
  templateUrl: './multiple-milestone-picker.component.html',
  styleUrls: ['./multiple-milestone-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridDefinition,
      useFactory: multipleMilestonePickerDefinition,
      deps: []
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, GridDefinition, ReferentialDataService]
    }
  ]
})
export class MultipleMilestonePickerComponent implements OnInit, OnDestroy {

  @Input()
  milestones: Milestone[];

  @Input()
  initiallySelectedMilestones: number[];

  @Input()
  disableColumns: Identifier[] = [];

  @Input()
  gridId = MULTIPLE_MILESTONES_PICKER_GRID;

  @Input()
  warningMessageKey: string;

  @Output()
  selectedMilestone: EventEmitter<Milestone[]> = new EventEmitter<Milestone[]>();

  private unsub$ = new Subject<void>();

  constructor(private gridService: GridService) {
  }

  ngOnInit(): void {
    this.initializeGridId();
    this.initializeData();
    this.initializeFilters();
    this.initializeOutput();
    this.toggleColumnsVisibility();
  }

  private initializeData() {
    if (Boolean(this.initiallySelectedMilestones)) {
      this.gridService.loadInitialData(this.milestones, this.milestones.length, 'id', this.initiallySelectedMilestones);
    } else {
      this.gridService.loadInitialData(this.milestones, this.milestones.length);
    }
  }

  private initializeOutput() {
    this.gridService.selectedRows$.pipe(
      takeUntil(this.unsub$),
      map(rows => rows.map(row => row.data as Milestone))
    ).subscribe((milestones) => this.selectedMilestone.emit(milestones));
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initializeFilters() {
    const filters: GridFilter[] = [{
      id: 'label', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false
    }];
    this.gridService.addFilters(filters);
  }

  handleResearchInput($event: string) {
    this.gridService.applyMultiColumnsFilter($event);
  }

  private toggleColumnsVisibility() {
    this.disableColumns.forEach(columnId => {
      this.gridService.setColumnVisibility(columnId, false);
    });
  }

  private initializeGridId() {
    this.gridService.defineId(this.gridId);
  }
}

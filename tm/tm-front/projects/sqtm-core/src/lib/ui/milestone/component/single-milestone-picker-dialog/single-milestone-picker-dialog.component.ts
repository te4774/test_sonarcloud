import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {SingleSelectMilestoneDialogConfiguration} from './milestone.dialog.configuration';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {milestoneLogger} from '../../milestone.logger';
import {Milestone} from '../../../../model/milestone/milestone.model';

const logger = milestoneLogger.compose('SingleMilestonePickerDialogComponent');

@Component({
  selector: 'sqtm-core-single-milestone-picker-dialog',
  templateUrl: './single-milestone-picker-dialog.component.html',
  styleUrls: ['./single-milestone-picker-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SingleMilestonePickerDialogComponent implements OnInit, AfterViewInit, OnDestroy {

  private unsub$ = new Subject<void>();

  configuration: SingleSelectMilestoneDialogConfiguration;

  constructor(private dialogReference: DialogReference<SingleSelectMilestoneDialogConfiguration, Milestone>) {
    this.configuration = this.dialogReference.data;
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
  }

  confirm() {
    this.dialogReference.close();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  changeMilestoneSelection(milestone: Milestone) {
    this.dialogReference.result = milestone;
  }
}



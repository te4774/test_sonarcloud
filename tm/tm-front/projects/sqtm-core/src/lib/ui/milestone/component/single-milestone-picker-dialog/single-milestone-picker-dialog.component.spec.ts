import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SingleMilestonePickerDialogComponent} from './single-milestone-picker-dialog.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {TranslateModule} from '@ngx-translate/core';

const dialogReference = jasmine.createSpyObj(['close']);
dialogReference['data'] = {};

describe('SingleMilestonePickerDialogComponent', () => {
  let component: SingleMilestonePickerDialogComponent;
  let fixture: ComponentFixture<SingleMilestonePickerDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [SingleMilestonePickerDialogComponent],
      providers: [{
        provide: DialogReference,
        useValue: dialogReference
      }],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleMilestonePickerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

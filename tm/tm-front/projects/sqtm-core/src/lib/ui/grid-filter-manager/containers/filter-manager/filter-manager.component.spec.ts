import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {FilterManagerComponent} from './filter-manager.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridTestingModule} from '../../../grid/grid-testing/grid-testing.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';

describe('FilterManagerComponent', () => {
  let component: FilterManagerComponent;
  let fixture: ComponentFixture<FilterManagerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, TestingUtilsModule, OverlayModule],
      declarations: [FilterManagerComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // skip as long as FilterManagerComponent is not extracted
  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});

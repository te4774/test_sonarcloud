import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilterManagerComponent} from './containers/filter-manager/filter-manager.component';
import {GridModule} from '../grid/grid.module';
import {TranslateModule} from '@ngx-translate/core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {WorkspaceCommonModule} from '../workspace-common/workspace-common.module';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {FiltersModule} from '../filters/filters.module';


@NgModule({
  declarations: [FilterManagerComponent],
  exports: [FilterManagerComponent],
  imports: [
    CommonModule,
    GridModule,
    TranslateModule.forChild(),
    NzButtonModule,
    NzIconModule,
    WorkspaceCommonModule,
    NzToolTipModule,
    FiltersModule,
  ]
})
export class GridFilterManagerModule {
}

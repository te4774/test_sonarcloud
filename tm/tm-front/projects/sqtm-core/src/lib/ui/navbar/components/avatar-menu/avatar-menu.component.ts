import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-core-avatar-menu',
  template: `
    <ul nz-menu nzTheme="dark">
      <li class="ant-menu-item">Mon compte</li>
      <li class="ant-menu-item">Deconnexion</li>
    </ul>
  `,
  styleUrls: ['./avatar-menu.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarMenuComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {HorizontalLogoutBarComponent} from './horizontal-logout-bar.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {TranslateModule} from "@ngx-translate/core";

describe('HorizontalLogoutBarComponent', () => {
  let component: HorizontalLogoutBarComponent;
  let fixture: ComponentFixture<HorizontalLogoutBarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        OverlayModule,
        TranslateModule.forRoot()
      ],
      declarations: [HorizontalLogoutBarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorizontalLogoutBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

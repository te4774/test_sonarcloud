import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  QueryList,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewChildren,
  ViewContainerRef
} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Overlay, OverlayRef} from '@angular/cdk/overlay';
import {TemplatePortal} from '@angular/cdk/portal';
import {combineLatest, fromEvent, Observable, Subject} from 'rxjs';
import {delay, filter, map, pluck, take, takeUntil, tap, throttleTime} from 'rxjs/operators';
import {ProjectFilterComponent} from '../project-filter/project-filter.component';
import {UiManagerService} from '../../../ui-manager/ui-manager.service';
import {NzSubMenuComponent} from 'ng-zorro-antd/menu';
import {ReferentialDataService} from '../../../../core/referential/services/referential-data.service';
import {DialogService} from '../../../dialog/services/dialog.service';
import {AuthenticatedUser} from '../../../../model/user/authenticated-user.model';
import {select} from '@ngrx/store';
import {Milestone} from '../../../../model/milestone/milestone.model';
import {
  buildSingleMilestonePickerDialogDefinition,
  SingleSelectMilestoneDialogConfiguration
} from '../../../milestone/component/single-milestone-picker-dialog/milestone.dialog.configuration';
import {MilestoneModeData} from '../../../../core/referential/state/milestone-filter.state';
import {AboutDialogComponent} from '../../../dialog/components/about-dialog/about-dialog.component';
import {BugTracker} from '../../../../model/bugtracker/bug-tracker.model';
import {RestService} from '../../../../core/services/rest.service';
import {selectMilestoneModeData} from '../../../../core/referential/state/referential-data.state';
import {WorkspacePlugin} from '../../../../core/referential/state/referential-data.model';
import {ApiDocumentationLink} from '../../../../core/referential/state/api-documentation-link';
import {BACKEND_CONTEXT_PATH} from '../../../../core/sqtm-core.tokens';
import {getSupportedBrowserLang} from '../../../../core/utils/browser-langage.utils';
import {
  DOC_URL_EN,
  DOC_URL_FR,
  SQUASH_AUTOM_DEVOPS_DOC_URL_EN,
  SQUASH_AUTOM_DEVOPS_DOC_URL_FR
} from '../../nav-bar.constants';

// TODO try to change custom avatar menu into a ngZorro component
@Component({
  selector: 'sqtm-core-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavBarComponent implements OnInit, OnDestroy, AfterViewInit {

  isManagerOrAdmin$: Observable<boolean>;

  avatarColor = '#11a67c';

  @ViewChild('avatar', {read: ElementRef})
  private avatar: ElementRef;

  @ViewChild('templatePortalContent')
  private userMenuTemplateRef: TemplateRef<any>;

  @ViewChildren(NzSubMenuComponent)
  nzSubMenus: QueryList<NzSubMenuComponent>;

  private overlayAvatarMenuRef: OverlayRef;

  avatarMenuOpened = false;

  private avatarMouseOverState = false;

  private unsub$ = new Subject<void>();

  milestoneNavBarState$: Observable<MilestoneModeData>;

  bugTracker$: Observable<BugTracker[]>;

  currentUser$: Observable<AuthenticatedUser>;

  showMilestoneWorkspaceMenuItem$: Observable<boolean>;

  workspacePlugins$: Observable<WorkspacePlugin[]>;

  documentationUrl: string;
  squashAutomDevopsDocumentationUrl: string;

  apiDocumentationLinks$: Observable<ApiDocumentationLink[]>;

  toggleButtonTooltipIsVisible: boolean;

  callbackUrl$: Observable<string | undefined>;

  constructor(private translateService: TranslateService,
              private renderer: Renderer2,
              private overlay: Overlay,
              private vcRef: ViewContainerRef,
              private dialogService: DialogService,
              public uiManager: UiManagerService,
              private referentialDataService: ReferentialDataService,
              private restService: RestService,
              @Inject(BACKEND_CONTEXT_PATH) private serverServletContext: string
  ) {
  }

  ngOnInit() {
    this.isManagerOrAdmin$ = this.referentialDataService.isAdminOrProjectManager().pipe(
      takeUntil(this.unsub$)
    );

    this.milestoneNavBarState$ = this.referentialDataService.referentialData$.pipe(
      takeUntil(this.unsub$),
      select(selectMilestoneModeData)
    );

    this.currentUser$ = this.referentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$)
    );

    this.workspacePlugins$ = this.referentialDataService.referentialData$.pipe(
      takeUntil(this.unsub$),
      map(d => d.workspacePlugins)
    );

    this.bugTracker$ = this.referentialDataService.bugTrackersBindToProject$.pipe(
      takeUntil(this.unsub$)
    );

    this.showMilestoneWorkspaceMenuItem$ = combineLatest([
      this.referentialDataService.globalConfiguration$,
      this.referentialDataService.isAdmin(),
    ]).pipe(
      takeUntil(this.unsub$),
      map(([globalConfig, isAdmin]) => globalConfig.milestoneFeatureEnabled || isAdmin));

    this.initDocumentationUrls();
    this.initDocumentationLinks();
  }

  private initDocumentationUrls() {
    if (getSupportedBrowserLang(this.translateService) === 'fr') {
      this.documentationUrl = DOC_URL_FR;
      this.squashAutomDevopsDocumentationUrl = SQUASH_AUTOM_DEVOPS_DOC_URL_FR;
    } else {
      this.documentationUrl = DOC_URL_EN;
      this.squashAutomDevopsDocumentationUrl = SQUASH_AUTOM_DEVOPS_DOC_URL_EN;
    }
  }

  private initDocumentationLinks() {
    this.callbackUrl$ = this.referentialDataService.referentialData$.pipe(
      takeUntil(this.unsub$),
      pluck('callbackUrl'));

    this.apiDocumentationLinks$ = this.referentialDataService.referentialData$.pipe(
      takeUntil(this.unsub$),
      pluck('documentationLinks'),
      map(links => {
        links.sort((first, second) => first.label.localeCompare(second.label));
        return links;
      }));
  }

  ngOnDestroy(): void {
    this.closeAvatarMenu();
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleCollapsed(): void {
    this.uiManager.toggleNavBar();
    this.toggleButtonTooltipIsVisible = false;
  }

  getClasses(workspace: string): string [] {
    const classes: string[] = [];
    classes.push(`${workspace}-workspace-link`);
    classes.push('sqtm-core-menu-item');
    return classes;
  }

  getWorkspaceName(workspace: string): string {
    const key = `sqtm-core.${workspace}-workspace.label.short`;
    return this.translateService.instant(key);
  }

  getWorkspaceTitle(workspace: string) {
    const key = `sqtm-core.${workspace}-workspace.label.full`;
    return this.translateService.instant(key);
  }

  getAvatarStyle() {
    return {
      'background-color': this.avatarColor,
    };
  }

  ngAfterViewInit(): void {
    fromEvent(this.avatar.nativeElement, 'mouseover').pipe(
      takeUntil(this.unsub$),
      throttleTime(50),
      tap(() => this.avatarMouseOverState = true),
      delay(100),
      filter(() => this.avatarMouseOverState && !this.avatarMenuOpened))
      .subscribe(() => {
        this.avatarMenuOpened = true;
        this.openAvatarMenu();
      });

    fromEvent(this.avatar.nativeElement, 'mouseleave').pipe(
      takeUntil(this.unsub$),
      tap(() => this.avatarMouseOverState = false),
      delay(100),
      filter(() => !this.avatarMouseOverState && this.avatarMenuOpened))
      .subscribe(() => {
        this.avatarMenuOpened = false;
        this.closeAvatarMenu();
      });
  }

  openAvatarMenu() {
    const position = this.overlay.position()
      .flexibleConnectedTo(this.avatar)
      .withDefaultOffsetX(5)
      .withDefaultOffsetY(-5).withPositions(
        [{
          originX: 'end',
          originY: 'bottom',
          overlayX: 'start',
          overlayY: 'bottom'
        }]
      );

    this.overlayAvatarMenuRef = this.overlay.create({positionStrategy: position});
    const avatarMenuPortal = new TemplatePortal(this.userMenuTemplateRef, this.vcRef);
    this.overlayAvatarMenuRef.attach(avatarMenuPortal);

    fromEvent(this.overlayAvatarMenuRef.overlayElement, 'mouseover').pipe(
      takeUntil(this.unsub$),
      filter(() => !this.avatarMouseOverState)).subscribe(
      () => {
        this.avatarMouseOverState = true;
      });

    fromEvent(this.overlayAvatarMenuRef.overlayElement, 'mouseleave').pipe(
      takeUntil(this.unsub$),
      tap(() => this.avatarMouseOverState = false),
      delay(200),
      filter(() => !this.avatarMouseOverState)).subscribe(
      () => {
        this.avatarMenuOpened = false;
        this.closeAvatarMenu();
      });
  }

  closeAvatarMenu() {
    if (this.overlayAvatarMenuRef) {
      this.overlayAvatarMenuRef.detach();
      this.overlayAvatarMenuRef.dispose();
    }
  }

  deactivateProjectFilter() {
    this.referentialDataService.deactivateProjectFilter();
  }

  modifyProjectFilter() {
    this.dialogService.openDialog({
      id: 'global-project-filter',
      component: ProjectFilterComponent,
      viewContainerReference: this.vcRef,
      height: 500,
      width: 700,
    });
  }

  disableMilestoneMode($event: MouseEvent) {
    this.referentialDataService.disableMilestoneMode();
  }

  openMilestoneSelect($event: MouseEvent) {
    this.milestoneNavBarState$.pipe(
      take(1),
    ).subscribe(({milestones, selectedMilestone}) => {
      const dialogDefinition = buildSingleMilestonePickerDialogDefinition(milestones, selectedMilestone);
      dialogDefinition.viewContainerReference = this.vcRef;
      const dialogReference = this.dialogService.openDialog<SingleSelectMilestoneDialogConfiguration, Milestone>(dialogDefinition);
      dialogReference.dialogClosed$.pipe(
        filter(nextSelectedMilestone => Boolean(nextSelectedMilestone))
      ).subscribe(nextSelectedMilestone => this.referentialDataService.enableMilestoneMode(nextSelectedMilestone.id));
    });
  }

  showAboutDialog() {
    this.restService.get<SquashInformation>(['information/version']).subscribe(
      squashInformation => {
        this.dialogService.openDialog({
          id: 'squash-about',
          viewContainerReference: this.vcRef,
          component: AboutDialogComponent,
          data: squashInformation.version,
          width: 600
        });
      }
    );
  }

  getUserText(currentUser: AuthenticatedUser) {
    const firstName = currentUser.firstName;
    const lastName = currentUser.lastName;
    let userText = '';

    if (firstName != null && firstName !== '') {
      userText = firstName.charAt(0);
    }

    if (lastName != null && lastName !== '') {
      userText = userText + lastName.charAt(0);
    }
    return userText.toUpperCase();
  }

  canNavigate(currentUser: AuthenticatedUser) {
    return currentUser.functionalTester || currentUser.admin;
  }

  showAdvertisingDialog() {
    this.dialogService.openAlert({
      titleKey: 'sqtm-core.generic.label.information.short',
      messageKey: 'sqtm-core.action-word-workspace.messages.advertising',
      level: 'INFO'
    }, 700);
  }

  buildDocumentationLink(url: string, callbackUrl: string): string {
    let urlStart = callbackUrl || this.serverServletContext;

    if (!urlStart.endsWith('/')) {
      urlStart += '/';
    }

    if (url.startsWith('/')) {
      url = url.replace('/', '');
    }

    return `${urlStart}${url}`;
  }
}

export interface SquashInformation {
  version: string;
}

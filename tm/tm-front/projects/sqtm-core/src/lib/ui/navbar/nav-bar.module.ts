import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {SvgModule} from '../svg/svg.module';
import {TranslateModule} from '@ngx-translate/core';
import {WorkspaceCommonModule} from '../workspace-common/workspace-common.module';
import {NavBarComponent} from './containers/nav-bar/nav-bar.component';
import {NzAvatarModule} from 'ng-zorro-antd/avatar';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzDividerModule} from 'ng-zorro-antd/divider';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {NzModalModule} from 'ng-zorro-antd/modal';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {AvatarMenuComponent} from './components/avatar-menu/avatar-menu.component';
import {ProjectFilterComponent} from './containers/project-filter/project-filter.component';
import {GridModule} from '../grid/grid.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {DialogModule} from '../dialog/dialog.module';
import {FormsModule} from '@angular/forms';
import {PlatformNavigationModule} from '../platform-navigation/platform-navigation.module';
import {MilestoneModule} from '../milestone/milestone.module';
import {HorizontalLogoutBarComponent} from './components/horizontal-logout-bar/horizontal-logout-bar.component';
import {NavBarAdminComponent} from './containers/nav-bar-admin/nav-bar-admin.component';
import {UiManagerModule} from '../ui-manager/ui-manager.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule.forChild(),
        SvgModule,
        WorkspaceCommonModule,
        PlatformNavigationModule,
        NzMenuModule,
        NzToolTipModule,
        NzIconModule,
        NzDividerModule,
        NzAvatarModule,
        NzModalModule,
        NzButtonModule,
        GridModule,
        OverlayModule,
        DialogModule,
        NzInputModule,
        FormsModule,
        MilestoneModule,
        UiManagerModule
    ],
    declarations: [NavBarComponent, AvatarMenuComponent, ProjectFilterComponent, HorizontalLogoutBarComponent, NavBarAdminComponent],
    exports: [NavBarComponent, HorizontalLogoutBarComponent, NavBarAdminComponent]
})
export class NavBarModule {
  constructor() {
  }
}

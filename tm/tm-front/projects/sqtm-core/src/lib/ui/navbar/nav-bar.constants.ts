export const DOC_URL_FR = 'https://tm-fr.doc.squashtest.com/';
export const DOC_URL_EN = 'https://tm-en.doc.squashtest.com/';

export const SQUASH_AUTOM_DEVOPS_DOC_URL_FR = 'https://autom-devops-fr.doc.squashtest.com/';
export const SQUASH_AUTOM_DEVOPS_DOC_URL_EN = 'https://autom-devops-en.doc.squashtest.com/';

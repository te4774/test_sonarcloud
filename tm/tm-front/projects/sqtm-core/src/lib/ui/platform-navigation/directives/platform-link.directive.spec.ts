import {PlatformLinkDirective} from './platform-link.directive';

const platformNavigationServiceMock = jasmine.createSpyObj(['navigate']);


describe('PlatformLinkDirective', () => {
  it('should create an instance', () => {
    const directive = new PlatformLinkDirective(platformNavigationServiceMock, null, null);
    expect(directive).toBeTruthy();
  });
});

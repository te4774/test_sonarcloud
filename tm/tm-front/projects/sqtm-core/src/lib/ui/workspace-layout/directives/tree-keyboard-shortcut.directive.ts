import {Directive, ElementRef, Host, HostListener, Inject, OnDestroy, OnInit} from '@angular/core';
import {wsLayoutLogger} from '../workspace.layout.logger';
import {DOCUMENT} from '@angular/common';
import {GridService} from '../../grid/services/grid.service';
import {filter, take} from 'rxjs/operators';

const logger = wsLayoutLogger.compose('TreeKeyboardShortcutDirective');

/** @dynamic */
@Directive({
  selector: '[sqtmCoreTreeKeyboardShortcut]'
})
export class TreeKeyboardShortcutDirective implements OnInit, OnDestroy {

  constructor(@Host() private host: ElementRef, @Inject(DOCUMENT) private document: Document, private grid: GridService) {
  }

  @HostListener('document:keyup', ['$event'])
  clicked(event: KeyboardEvent) {
    const activeElement = this.document.activeElement;
    if (activeElement === this.document.body) {
      this.handleBodyEvent(event);
    }
  }

  private handleBodyEvent(event: KeyboardEvent) {
    logger.debug('Handling keyboard stroke in TreeKeyboardShortcutDirective : ', [event]);
    if (event.ctrlKey) {
      this.handleCtrlKeyPressedEvents(event);
    } else {
      this.handleSimpleKeyEvents(event);
    }
  }

  private handleCtrlKeyPressedEvents(event: KeyboardEvent) {
    switch (event.key) {
      case 'c':
        this.copyNodes();
        break;
      case 'v':
        this.pasteNodes();
        break;
    }
  }

  private copyNodes() {
    logger.debug('Keyboard copy shortcut activated');
    this.grid.canCopy$.pipe(
      take(1),
      filter(canCopy => canCopy)
    ).subscribe(() => {
      this.grid.copy();
    });
  }

  private pasteNodes() {
    logger.debug('Keyboard paste shortcut activated');
    this.grid.canPaste$.pipe(
      take(1),
      filter(canPaste => canPaste)
    ).subscribe(() => {
      this.grid.paste();
    });
  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
  }

  private handleSimpleKeyEvents(event: KeyboardEvent) {
    if (event.key === 'Delete') {
      this.grid.canDelete$.pipe(
        take(1),
        filter(canDelete => canDelete)
      ).subscribe(() => {
        this.grid.deleteRows();
      });
    }
  }
}

import {ChangeDetectionStrategy, Component} from '@angular/core';
import {GenericEntityViewService} from '../../../../core/services/genric-entity-view/generic-entity-view.service';
import {SqtmGenericEntityState} from '../../../../core/services/genric-entity-view/generic-entity-view-state';

@Component({
  selector: 'sqtm-core-entity-view-wrapper',
  templateUrl: './entity-view-wrapper.component.html',
  styleUrls: ['./entity-view-wrapper.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EntityViewWrapperComponent<S extends SqtmGenericEntityState, T extends string> {
  constructor(public readonly viewService: GenericEntityViewService<S, T>) {
  }
}

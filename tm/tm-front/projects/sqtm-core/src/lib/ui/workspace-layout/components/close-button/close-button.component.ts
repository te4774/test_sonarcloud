import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {distinctUntilChanged, filter, takeUntil} from 'rxjs/operators';
import {UiManagerService} from '../../../ui-manager/ui-manager.service';
import {WorkspaceDirective} from '../../../ui-manager/workspace.directive';
import {WorkspaceLayoutState} from '../../../ui-manager/ui-manager-state';

@Component({
  selector: 'sqtm-core-close-button',
  templateUrl: './close-button.component.html',
  styleUrls: ['./close-button.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CloseButtonComponent implements OnInit , OnDestroy {

  private unsub$ = new Subject<void>();
  workspaceLayout$: Observable<WorkspaceLayoutState>;

  constructor(private uiManager: UiManagerService, private workspaceDirective: WorkspaceDirective) {
    this.workspaceLayout$ = this.uiManager.createWorkspaceLayoutObservable(this.workspaceDirective.workspaceName).pipe(
      takeUntil(this.unsub$),
      filter(workspaceLayoutState => Boolean(workspaceLayoutState)),
      distinctUntilChanged()
    );
  }

  ngOnInit(): void {
  }

  closeContextualContent() {
    this.uiManager.closeContextualContent(this.workspaceDirective.workspaceName);
  }


  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}

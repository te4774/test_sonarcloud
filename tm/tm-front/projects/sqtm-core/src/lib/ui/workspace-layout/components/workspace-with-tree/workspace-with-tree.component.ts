import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import {ResizableDirective} from '../../../workspace-common/directives/resizable.directive';
import {combineLatest, Subject} from 'rxjs';
import {OverlayRef} from '@angular/cdk/overlay';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {wsLayoutLogger} from '../../workspace.layout.logger';
import {distinctUntilChanged, filter, map, pluck, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {GridService} from '../../../grid/services/grid.service';
import decamelize from 'decamelize';
import {UiManagerService} from '../../../ui-manager/ui-manager.service';
import {NavBarComponent} from '../../../navbar/containers/nav-bar/nav-bar.component';
import {Identifier} from '../../../../model/entity.model';
import {EntityRowReference, toEntityRowReference} from '../../../../model/grids/data-row.type';
import {DataRow} from '../../../grid/model/data-row.model';
import {LicenseInformationState} from '../../../../core/referential/state/license-information.state';
import {ResizableWorkspaceLayoutDelegate, WorkspaceLikeComponent} from '../../resizable-workspace-layout-delegate';
import {LocalPersistenceService} from '../../../../core/services/local-persistence.service';

const logger = wsLayoutLogger.compose('WorkspaceWithTreeComponent');

@Component({
  selector: 'sqtm-core-workspace-with-tree',
  templateUrl: './workspace-with-tree.component.html',
  styleUrls: ['./workspace-with-tree.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkspaceWithTreeComponent implements WorkspaceLikeComponent, OnInit, OnDestroy, AfterViewInit {

  @Input()
  name: string;

  private _theme: string;

  get theme(): string {
    return this._theme || this.name;
  }

  @Input()
  set theme(value: string) {
    this._theme = value;
  }

  @Input()
  licenseInformation: LicenseInformationState;

  @Input()
  isLoggedAsAdmin: boolean;

  @ViewChild('tree', {read: ElementRef, static: true})
  treeReference: ElementRef;

  @ViewChild(ResizableDirective, {read: ElementRef, static: true})
  resizeHandle: ElementRef;

  @ViewChild(NavBarComponent, {read: ElementRef, static: true})
  navBar: ElementRef;

  @ViewChild('contextualContent', {read: ElementRef, static: true})
  contextualContent: ElementRef;

  @ViewChild('workspace', {read: ElementRef, static: true})
  workspace: ElementRef;

  // Implements WorkspaceLikeComponent
  get gridReference(): ElementRef {
    return this.treeReference;
  }

  // unsub$
  public readonly unsub$ = new Subject<void>();

  private overlayRef: OverlayRef;

  private readonly layoutDelegate: ResizableWorkspaceLayoutDelegate;

  constructor(private router: Router,
              private activeRoute: ActivatedRoute,
              public gridService: GridService,
              public renderer: Renderer2,
              private uiManager: UiManagerService,
              private persistenceService: LocalPersistenceService) {
    this.layoutDelegate = new ResizableWorkspaceLayoutDelegate(this, this.persistenceService);
  }

  ngOnInit() {
    logger.debug(`Initialize ${this.name}`);
    this.layoutDelegate.applyMinimalSizes();
    this.layoutDelegate.reactToWindowResized();
    this.initializeNavigation();
    this.initializeFolding();
  }

  ngAfterViewInit(): void {
    this.layoutDelegate.initializeWidthBoundary();
    this.layoutDelegate.showContextualContent();
    this.reactToRouteChanges();
  }

  ngOnDestroy(): void {
    logger.debug('Destroying WorkspaceWithTreeComponent');
    this.unsub$.next();
    this.unsub$.complete();
    if (this.overlayRef) {
      this.overlayRef.dispose();
    }
  }

  requireNodeRefresh(ids: Identifier[]): void {
    logger.debug(`Refresh subtrees required on nodes : `, [ids]);
    this.gridService.refreshSubTree(ids);
  }

  /**
   * Refresh the container of current selected node, if any. If a node selection is required, this method will wait for the row to
   * be available and select as soon as possible.
   */
  requireCurrentContainerRefresh(selectNodeAfterRefresh?: Identifier): void {
    logger.debug(`Refresh subtrees required on current container : `);
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length === 1),
      map(rows => rows[0]),
      filter(row => Boolean(row.parentRowId)),
      tap(row => this.gridService.refreshSubTree([row.parentRowId])),
      // switching to another observable of all rows. As soon as the required selected row comme back from server,
      // we can actually select it, if exist...
      filter(() => Boolean(selectNodeAfterRefresh)),
      switchMap(() => this.gridService.dataRows$.pipe(
        takeUntil(this.unsub$),
        map(allRows => allRows[selectNodeAfterRefresh]),
        filter(row => Boolean(row)),
        take(1),
      ))
    ).subscribe((row) => {
      this.gridService.selectSingleRow(row.id);
    });
  }

  private initializeFolding(): void {
    this.uiManager.registerWorkspace(this.name);
    this.uiManager.createWorkspaceLayoutObservable(this.name).pipe(
      takeUntil(this.unsub$),
      filter(workspaceLayoutState => Boolean(workspaceLayoutState)),
      pluck('treeFolded'),
      distinctUntilChanged()
    ).subscribe((treeFolded: boolean) => {
      if (treeFolded) {
        this.layoutDelegate.foldTree();
      } else {
        this.layoutDelegate.unfoldTree();
      }
    });
  }

  private initializeNavigation(): void {
    combineLatest([this.gridService.selectedRowIds$, this.gridService.loaded$]).pipe(
      filter(([, loaded]) => loaded),
      map(([ids]) => ids),
      takeUntil(this.unsub$),
      map((ids: Identifier[]) => ids.map((id) => toEntityRowReference(id))),
      distinctUntilChanged((selectionA: EntityRowReference[], selectionB: EntityRowReference[]) => {
        let same = selectionA.length === selectionB.length;
        if (same) {
          let i = 0;
          while (i < selectionA.length && same) {
            const rowA = selectionA[i];
            const rowB = selectionB[i];
            if (rowA.id !== rowB.id || rowA.entityType !== rowB.entityType) {
              same = false;
            }
            i++;
          }
        }
        return same;
      }),
      distinctUntilChanged((selectionA: EntityRowReference[], selectionB: EntityRowReference[]) => {
        // no navigation if selection is from multi to multi
        return selectionA.length > 1 && selectionB.length > 1;
      }),
    ).subscribe((rows: EntityRowReference[]) => {
      logger.debug('Load Rows ', [rows]);
      if (rows.length === 0) {
        this.handleNoSelection();
      }
      if (rows.length === 1) {
        this.handleSingleSelection(rows[0]);
      }
      if (rows.length > 1) {
        this.handleMultiSelection();
      }
    });
  }

  private handleSingleSelection(row: EntityRowReference): void {
    const desiredRoute = `${decamelize(row.entityType, '-')}/${row.id}`;
    const isAlreadyOnDesiredRoute = this.router.url.includes(desiredRoute);

    if (!isAlreadyOnDesiredRoute) {
      this.router.navigate([`./${desiredRoute}`], {relativeTo: this.activeRoute});
    }
  }

  private handleNoSelection(): void {
    if (!this.isOnMilestoneDashboard()) {
      this.router.navigate([`/${this.name}`]);
    }
  }

  handleResize(widthDelta: number): void {
    this.layoutDelegate.handleResize(widthDelta);
  }

  private handleMultiSelection(): void {
    this.layoutDelegate.showContextualContent();
    this.router.navigate([`./${this.name}-multi`],
      {
        relativeTo: this.activeRoute
      });
  }

  // Allow to adapt layout when navigating without selecting in the grid (e.g. the browser's "Previous" and "Next"
  // buttons)
  private reactToRouteChanges(): void {
    this.router.events.pipe(
      takeUntil(this.unsub$),
      filter(e => e instanceof NavigationEnd)
    ).subscribe((e: NavigationEnd) => {
      this.parseUrlAndAdaptLayout(e.urlAfterRedirects);
    });
  }

  private parseUrlAndAdaptLayout(url: string): void {
    if (url.startsWith('/')) {
      url = url.slice(1);
    }

    const urlParts = url.split('/');
    const entityId = urlParts[2];

    if (entityId != null || url.endsWith('multi')) {
      this.layoutDelegate.showContextualContent();
    } else {
      this.uiManager.unfoldTree(this.name);
      this.layoutDelegate.unfoldTree();
    }
  }

  private isOnMilestoneDashboard(): boolean {
    return this.router.url.split('/').includes('milestone-dashboard');
  }
}

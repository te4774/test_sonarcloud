import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EntityViewWrapperComponent} from './entity-view-wrapper.component';
import {GenericEntityViewService} from '../../../../core/services/genric-entity-view/generic-entity-view.service';

class ConcreteWrapperComponent extends EntityViewWrapperComponent<any, any> {
}

describe('EntityViewWrapperComponent', () => {
  let component: ConcreteWrapperComponent;
  let fixture: ComponentFixture<ConcreteWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConcreteWrapperComponent ],
      providers: [
        {provide: GenericEntityViewService, useValue: {}},
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcreteWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  RequirementTreeDialogPickerComponent
} from './components/requirement-tree-dialog-picker/requirement-tree-dialog-picker.component';
import {DialogModule} from '../dialog/dialog.module';
import {GridModule} from '../grid/grid.module';
import {UiManagerModule} from '../ui-manager/ui-manager.module';
import {
  TestCaseTreeDialogPickerComponent
} from './components/test-case-tree-dialog-picker/test-case-tree-dialog-picker.component';
import {
  CampaignTreeDialogPickerComponent
} from './components/campaign-tree-dialog-picker/campaign-tree-dialog-picker.component';


@NgModule({
  declarations: [RequirementTreeDialogPickerComponent, TestCaseTreeDialogPickerComponent, CampaignTreeDialogPickerComponent],
  imports: [
    CommonModule,
    DialogModule,
    GridModule,
    UiManagerModule
  ]
})
export class DialogPickersModule { }

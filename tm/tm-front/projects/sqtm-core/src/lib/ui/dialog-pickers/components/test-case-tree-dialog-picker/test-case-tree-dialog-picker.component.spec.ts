import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TestCaseTreeDialogPickerComponent} from './test-case-tree-dialog-picker.component';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('TestCaseTreeDialogPickerComponent', () => {
  let component: TestCaseTreeDialogPickerComponent;
  let fixture: ComponentFixture<TestCaseTreeDialogPickerComponent>;
  const dialogReference = jasmine.createSpyObj(['close']);
  dialogReference['data'] = {};
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestCaseTreeDialogPickerComponent],
      providers: [{
        provide: DialogReference,
        useValue: dialogReference
      }],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseTreeDialogPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

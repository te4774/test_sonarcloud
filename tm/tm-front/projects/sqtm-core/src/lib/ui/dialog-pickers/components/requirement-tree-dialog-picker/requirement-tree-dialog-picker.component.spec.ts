import {ComponentFixture, TestBed} from '@angular/core/testing';

import {RequirementTreeDialogPickerComponent} from './requirement-tree-dialog-picker.component';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RequirementTreeDialogPickerComponent', () => {
  let component: RequirementTreeDialogPickerComponent;
  let fixture: ComponentFixture<RequirementTreeDialogPickerComponent>;

  const dialogReference = jasmine.createSpyObj(['close']);
  dialogReference['data'] = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequirementTreeDialogPickerComponent ],
      providers: [{
        provide: DialogReference,
        useValue: dialogReference
      }],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementTreeDialogPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

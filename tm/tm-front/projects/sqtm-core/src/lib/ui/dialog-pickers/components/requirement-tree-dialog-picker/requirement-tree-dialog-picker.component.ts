import {ChangeDetectionStrategy, Component} from '@angular/core';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {TreePickerDialogConfiguration} from '../../picker.dialog.configuration';
import {DataRow} from '../../../grid/model/data-row.model';
import {AbstractTreeDialogPicker} from '../abstract-tree-dialog-picker';

@Component({
  selector: 'sqtm-core-requirement-tree-dialog-picker',
  templateUrl: './requirement-tree-dialog-picker.component.html',
  styleUrls: ['./requirement-tree-dialog-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementTreeDialogPickerComponent extends AbstractTreeDialogPicker {

  constructor(dialogReference: DialogReference<TreePickerDialogConfiguration, DataRow[]>) {
    super(dialogReference);
  }

}

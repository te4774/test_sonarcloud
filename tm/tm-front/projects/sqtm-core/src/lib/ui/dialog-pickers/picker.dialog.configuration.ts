import {DialogConfiguration} from '../dialog/services/dialog-feature.state';
import {
  RequirementTreeDialogPickerComponent
} from './components/requirement-tree-dialog-picker/requirement-tree-dialog-picker.component';
import {Identifier} from '../../model/entity.model';
import {
  TestCaseTreeDialogPickerComponent
} from './components/test-case-tree-dialog-picker/test-case-tree-dialog-picker.component';
import {
  CampaignTreeDialogPickerComponent
} from './components/campaign-tree-dialog-picker/campaign-tree-dialog-picker.component';

export interface TreePickerDialogConfiguration {
  titleKey: string;
  selectedNodes: Identifier[];
  enabledMultiSelection: boolean;
  requiredRowType?: string;
}

export type TreePickerDialogType = 'requirement-picker-dialog' | 'test-case-picker-dialog' | 'campaign-picker-dialog';


function findComponentType(pickerType: TreePickerDialogType) {
  let component;

  switch (pickerType) {
    case 'requirement-picker-dialog':
      component = RequirementTreeDialogPickerComponent;
      break;
    case 'test-case-picker-dialog':
      component = TestCaseTreeDialogPickerComponent;
      break;
    case 'campaign-picker-dialog':
      component = CampaignTreeDialogPickerComponent;
      break;
    default:
      throw Error('Unknown TreePickerDialogType: ' + pickerType);
  }
  return component;
}

export function buildTreePickerDialogDefinition(pickerType: TreePickerDialogType, selectedNodes: Identifier[] = [],
                                                enabledMultiSelection: boolean = true, requiredRowType?: string):
  DialogConfiguration<TreePickerDialogConfiguration> {

  const configuration: TreePickerDialogConfiguration = {
    titleKey: 'sqtm-core.dialog.choose-elements',
    selectedNodes,
    enabledMultiSelection,
    requiredRowType
  };

  const component = findComponentType(pickerType);

  return {
    id: 'tree-picker-dialog',
    component,
    height: 800,
    width: 600,
    data: configuration,
  };
}

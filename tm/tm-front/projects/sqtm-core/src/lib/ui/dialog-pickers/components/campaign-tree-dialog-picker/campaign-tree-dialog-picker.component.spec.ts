import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CampaignTreeDialogPickerComponent} from './campaign-tree-dialog-picker.component';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('CampaignTreeDialogPickerComponent', () => {
  let component: CampaignTreeDialogPickerComponent;
  let fixture: ComponentFixture<CampaignTreeDialogPickerComponent>;
  const dialogReference = jasmine.createSpyObj(['close']);
  dialogReference['data'] = {};
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CampaignTreeDialogPickerComponent ],
      providers: [{
        provide: DialogReference,
        useValue: dialogReference
      }],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignTreeDialogPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {ChangeDetectionStrategy, Component, Inject, InjectionToken, OnDestroy, OnInit} from '@angular/core';
import {map, take, takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {GridDefinition} from '../../../../grid/model/grid-definition.model';
import {GridService} from '../../../../grid/services/grid.service';
import {tree} from '../../../../../model/grids/grid-builders';
import {
  TreeNodeCellRendererComponent
} from '../../../../cell-renderer-common/tree-node-cell-renderer/tree-node-cell-renderer.component';
import {Extendable} from '../../../../grid/model/column-definition.model';
import {gridServiceFactory} from '../../../../grid/grid.service.provider';
import {RestService} from '../../../../../core/services/rest.service';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';
import {DialogReference} from '../../../../dialog/model/dialog-reference';
import {DataRow, GridResponse} from '../../../../grid/model/data-row.model';
import {SquashTmDataRowType, TaFolder, TaProject, TaTest} from '../../../../../model/grids/data-row.type';
import {ReadOnlyPermissions} from '../../../../../model/permissions/simple-permissions';
import {column} from '../../../../grid/model/common-column-definition.builders';


export const TCW_TA_TEST_TREE_CONF = new InjectionToken<GridDefinition>('Grid config for the ta test tree of test case view');
export const TCW_TA_TEST_TREE = new InjectionToken<GridService>('Grid service instance for the ta test tree of test case view');

export function taProjectTreeConfigFactory(): GridDefinition {
  const gridDefinition = tree('ta-projects')
    .withColumns([
      column('NAME')
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent)
    ])
    .disableMultiSelection()
    .withRowConverter(taLiteralConverter)
    .build();
  return gridDefinition;
}

@Component({
  selector: 'sqtm-core-test-automation-dialog',
  templateUrl: './test-automation-dialog.component.html',
  styleUrls: ['./test-automation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TCW_TA_TEST_TREE_CONF,
      useFactory: taProjectTreeConfigFactory,
      deps: []
    },
    {
      provide: TCW_TA_TEST_TREE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_TA_TEST_TREE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: TCW_TA_TEST_TREE
    }]
})
export class TestAutomationDialogComponent implements OnInit, OnDestroy {

  disabled$: Observable<boolean>;

  unsub$ = new Subject<void>();

  constructor(private dialogReference: DialogReference,
              @Inject(TCW_TA_TEST_TREE) private gridService: GridService,
              private restService: RestService) {
    this.disabled$ = this.gridService.selectedRows$.pipe(
      takeUntil(this.unsub$),
      map((rows: DataRow[]) => {
        if (rows.length > 0) {
          return rows[0].type !== SquashTmDataRowType.TATest;
        }
        return true;
      })
    );
  }

  ngOnInit(): void {
    this.restService.get<GridResponse>([`test-case/${this.dialogReference.data.testCaseId}/test-automation/tests`])
      .subscribe(response => {
        this.gridService.loadInitialDataRows(response.dataRows, response.dataRows.length);
      });
  }

  chooseScript() {
    this.gridService.selectedRows$.pipe(
      take(1),
    ).subscribe(
      rows => {
        const row = rows[0];
        this.dialogReference.result = `/${row.id}`;
        this.dialogReference.close();
      }
    );
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

}

export function taLiteralConverter(literals: Partial<DataRow>[]): DataRow[] {
  return literals.reduce((datarows, literal) => {

    let dataRow;
    if (literal.data['TYPE'] === 'ta-project') {
      dataRow = new TaProject();
    } else if (literal.data['TYPE'] === 'ta-folder') {
      dataRow = new TaFolder();
    } else {
      dataRow = new TaTest();
    }
    dataRow.simplePermissions = new ReadOnlyPermissions();
    Object.assign(dataRow, literal);
    datarows.push(dataRow);
    return datarows;
  }, []);
}

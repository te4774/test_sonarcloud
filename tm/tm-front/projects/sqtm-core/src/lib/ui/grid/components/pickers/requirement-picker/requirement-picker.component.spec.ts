import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {RequirementPickerComponent} from './requirement-picker.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {RouterTestingModule} from '@angular/router/testing';

describe('RequirementPickerComponent', () => {
  let component: RequirementPickerComponent;
  let fixture: ComponentFixture<RequirementPickerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule, OverlayModule, RouterTestingModule],
      declarations: [RequirementPickerComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequirementPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {Identifier} from '../../../../model/entity.model';
import {DataRowFilterFunction} from '../data-row.model';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {Filter, FilterGroup, FilterValue, Scope} from '../../../filters/state/filter.state';
import {
  ListGroup,
  ListItem
} from '../../../workspace-common/components/forms/grouped-multi-list/grouped-multi-list.component';

export interface GridFilter extends Filter {
  // does the filter is active. User selected it, so it must be applied.
  active: boolean;
  // By default, all filters open when activated. this could lead to filter widget popping if filters are set at grid initialization.
  // However, deactivate filters will override this setting and put it to false, so if user reselect them, they will auto open.
  preventOpening?: boolean;
  // If true the filter will always be displayed and user cannot remove it from filter list.
  alwaysActive?: boolean;
  // If true the filter value will be locked to it's initial value, and the value editors couldn't be open
  lockedValue?: boolean;
  // Custom function called to perform filter. Used only for filtering client side.
  filterFunction?: DataRowFilterFunction;
  // initial value, mandatory for new research function
  initialValue: FilterValue;
  // if true, filter will be reset and inactivated when perimeter change to avoid stale values in filters
  tiedToPerimeter: boolean;
}

export interface GridFilters extends EntityState<GridFilter> {
  // Contains the rows ids that match filters.
  // It's a sort of denorm that allow us to have better performance, better control on pagination state...
  // However, as it is a denorm it must be kept in phase with DataRowState if rows are added/removed or modified
  matchingRowIds: Identifier[];

  // Contains the rows ids that are parent of rows that match filters.
  // It's a sort of denorm that allow us to filter in tree mode and show parents, and thus really filter in hierarchy
  // However, as it is a denorm it must be kept in phase with DataRowState if rows are added/removed or modified
  ancestorMatchingRowIds: Identifier[];
}

export interface GridFilterGroups extends EntityState<FilterGroup> {

}

export interface FilterState {
  filters: GridFilters;
  filterGroups: GridFilterGroups;
  scope: Scope;
  extendedHighLvlReqScope: boolean;
}

export const filterEntityAdapter = createEntityAdapter<GridFilter>();
export const filterGroupEntityAdapter = createEntityAdapter<FilterGroup>();

type GridFiltersStateReadOnly = Readonly<GridFilters>;

export function initialFilterState(): Readonly<FilterState> {
  return {
    filters: initialGridFiltersState(),
    filterGroups: filterGroupEntityAdapter.getInitialState(),
    scope: {kind: 'project', value: [], initialValue: [], initialKind: 'project', active: false},
    extendedHighLvlReqScope: true
  };
}

export function initialGridFiltersState(): GridFiltersStateReadOnly {
  return filterEntityAdapter.getInitialState({
    matchingRowIds: [],
    ancestorMatchingRowIds: []
  });
}

export const selectFilterState = createFeatureSelector<FilterState>('filterState');
export const selectFilters = createSelector(selectFilterState, (state) => state.filters);
export const selectFilterGroups = createSelector(selectFilterState, (state) => state.filterGroups);
export const selectScope = createSelector(selectFilterState, (state) => state.scope);
export const selectExtendedHighLvlReqScope = createSelector(selectFilterState, (state) => state.extendedHighLvlReqScope);
export const selectAllFilters = createSelector(selectFilters, filterEntityAdapter.getSelectors().selectAll);
export const selectAllFilterGroups = createSelector(selectFilterGroups, filterGroupEntityAdapter.getSelectors().selectAll);
export const selectActiveFilters = createSelector(selectAllFilters, filters => filters.filter(f => f.active));

export const selectFilterData =
  createSelector(selectAllFilters, selectAllFilterGroups, selectScope, selectExtendedHighLvlReqScope,
    (filters, filterGroups, scope: Scope, extendedHighLvlReqScope: boolean) => ({
    filters, filterGroups, scope, extendedHighLvlReqScope
  }));

export const selectFiltersAndGroupsForMultiList = createSelector(selectAllFilters, selectAllFilterGroups, (filters, filterGroups) => {
  const items: ListItem[] = filters
    .filter(f => !f.alwaysActive) // always active filters mustn't be shown in filter list
    .map(f => {
      return {
        id: f.id.toString(),
        label: f.label,
        i18nLabelKey: f.i18nLabelKey,
        selected: f.active,
        groupId: f.groupId
      };
    });
  const itemGroups: ListGroup[] = filterGroups.map(group => {
    return {id: group.id.toString(), i18nLabelKey: group.i18nLabelKey};
  });
  return {items, itemGroups};
});

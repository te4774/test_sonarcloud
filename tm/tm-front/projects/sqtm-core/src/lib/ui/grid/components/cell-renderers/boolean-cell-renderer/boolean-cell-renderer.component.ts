import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractCellRendererComponent} from '../abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../../services/grid.service';

@Component({
  selector: 'sqtm-core-boolean-cell-renderer',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
        <span style="margin: auto 0;" class="sqtm-grid-cell-txt-renderer boolean-cell-renderer"
              [ngClass]="textClass">
          {{i18nKey | translate}}
        </span>
      </div>
    </ng-container>`,
  styleUrls: ['./boolean-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BooleanCellRendererComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  get i18nKey(): string {
    const data = this.row.data[this.columnDisplay.id];
    return Boolean(data) ? 'sqtm-core.generic.label.yes' : 'sqtm-core.generic.label.no';
  }

  get textClass(): string {
    return 'align-' + this.columnDisplay.contentPosition;
  }

}

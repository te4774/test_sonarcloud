import {
  BooleanCellRendererComponent
} from '../components/cell-renderers/boolean-cell-renderer/boolean-cell-renderer.component';
import {
  DateCellRendererComponent,
  DateTimeCellRendererComponent
} from '../components/cell-renderers/date-cell-renderer/date-cell-renderer.component';
import {
  buildLevelEnumKeySort,
  I18nEnum,
  LevelEnum,
  TestCaseAutomatable,
  TestCaseStatus,
  TestCaseWeight
} from '../../../model/level-enums/level-enum';
import {isTestCaseEditable, isTestCaseImportanceEditable} from '../../cell-renderer-common/editable-functions';
import {
  EditableDateCellRendererComponent
} from '../components/cell-renderers/editable-date-cell-renderer/editable-date-cell-renderer.component';
import {
  NumericCellRendererComponent
} from '../components/cell-renderers/numeric-cell-renderer/numeric-cell-renderer.component';
import {
  EditableTextCellRendererComponent
} from '../components/cell-renderers/editable-text-cell-renderer/editable-text-cell-renderer.component';
import {
  TextCellerWithToolTipRenderComponent
} from '../components/cell-renderers/text-cell-with-tool-tip-renderer/text-celler-with-tool-tip-render.component';
import {
  IndexCellRendererComponent
} from '../components/cell-renderers/index-cell-renderer/index-cell-renderer.component';
import {Fixed} from './column-definition.model';
import {
  EditableRichTextRendererComponent
} from '../components/cell-renderers/editable-rich-text-renderer/editable-rich-text-renderer.component';
import {Type} from '@angular/core';
import {TextCellRendererComponent} from '../components/cell-renderers/text-cell-renderer/text-cell-renderer.component';
import {
  CheckBoxCellRendererComponent
} from '../components/cell-renderers/check-box-cell-renderer/check-box-cell-renderer.component';
import {
  EditableNumericCellRendererComponent
} from '../components/cell-renderers/editable-numeric-cell-renderer/editable-numeric-cell-renderer.component';
import {RichTextRendererComponent} from '../components/cell-renderers/rich-text-renderer/rich-text-renderer.component';
import {
  RadioCellRendererComponent
} from '../components/cell-renderers/radio-cell-renderer/radio-cell-renderer.component';
import {
  SelectableNumericCellRendererComponent
} from '../components/cell-renderers/selectable-numeric-cell-renderer/selectable-numeric-cell-renderer.component';
import {ColumnDefinitionBuilder} from './column-definition.builder';
import {
  BooleanValueRenderer,
  DateExportValueRenderer,
  DateTimeExportValueRenderer
} from '../../grid-export/value-renderer/value-renderer';

export function column(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id);
}

export function textColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id)
        .withRenderer(TextCellRendererComponent);

}

export function textCellWithToolTipColumn(id: string, toolTipText: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id)
        .withToolTipText(toolTipText)
        .withRenderer(TextCellerWithToolTipRenderComponent);
}

export function centredTextColumn(id: string): ColumnDefinitionBuilder {
    return textColumn(id)
        .withHeaderPosition('center')
        .withContentPosition('center');
}

export function editableTextColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id)
        .withRenderer(EditableTextCellRendererComponent);
}

export function checkboxColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id).withRenderer(CheckBoxCellRendererComponent);
}

export function booleanColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id).withRenderer(BooleanCellRendererComponent).withExportValueRenderer(BooleanValueRenderer);
}

export function editableDateColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id).withRenderer(EditableDateCellRendererComponent);
}

export function dateColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id).withRenderer(DateCellRendererComponent).withExportValueRenderer(DateExportValueRenderer);
}

export function dateTimeColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id).withRenderer(DateTimeCellRendererComponent).withExportValueRenderer(DateTimeExportValueRenderer);
}

export function editableNumericColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id).withRenderer(EditableNumericCellRendererComponent);
}

export function numericColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id).withRenderer(NumericCellRendererComponent).withHeaderPosition('center');
}

export function indexColumn(): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder('#')
        .withHeaderPosition('center')
        .withRenderer(IndexCellRendererComponent)
        .changeWidthCalculationStrategy(new Fixed(50))
        .disableSort();
}

export function editableRichTextColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id)
        .withRenderer(EditableRichTextRendererComponent);
}

export function richTextColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id)
        .withRenderer(RichTextRendererComponent);
}

export function selectRowColumn(): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder('select-row-column')
        .changeWidthCalculationStrategy(new Fixed(40))
        .disableSort()
        .enableForceRenderOnNoValue()
        .withRenderer(CheckBoxCellRendererComponent);
}

export function singleSelectRowColumn(): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder('single-select-row-column')
        .changeWidthCalculationStrategy(new Fixed(40))
        .disableSort()
        .enableForceRenderOnNoValue()
        .disableHeader()
        .withRenderer(RadioCellRendererComponent);
}

export function deleteColumn(renderer: Type<any>, id = 'delete', label = ''): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id)
        .withRenderer(renderer)
        .withLabel(label)
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(40));
}

export function testCaseImportanceColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id)
        .withEnumRenderer(TestCaseWeight)
        .isEditable(isTestCaseImportanceEditable)
        .withHeaderPosition('center');
}

export function testCaseStatusColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id)
        .withEnumRenderer(TestCaseStatus)
        .isEditable(isTestCaseEditable)
        .withHeaderPosition('center');
}

export function testCaseAutomatableColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id)
        .withEnumRenderer(TestCaseAutomatable)
        .isEditable(isTestCaseEditable)
        .withHeaderPosition('center');
}

export function i18nEnumColumn(id: string, i18nEnum: I18nEnum<any>): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id)
        .withEnumRenderer(i18nEnum)
        .withHeaderPosition('center');
}

export function levelEnumColumn(id: string, levelEnum: LevelEnum<any>): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id)
        .withEnumRenderer(levelEnum)
        .withHeaderPosition('center')
        .withSortFunction(buildLevelEnumKeySort(levelEnum));
}

export function selectableNumericColumn(id: string): ColumnDefinitionBuilder {
    return new ColumnDefinitionBuilder(id).withRenderer(SelectableNumericCellRendererComponent);
}

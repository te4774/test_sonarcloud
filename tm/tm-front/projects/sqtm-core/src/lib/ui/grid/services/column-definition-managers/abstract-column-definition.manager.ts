import {Observable, of} from 'rxjs';
import {ColumnDefinition, Sort, SortedColumn, toggleColumnSortState} from '../../model/column-definition.model';
import {ColumnState, GridViewportName, gridViewportNames, ViewportState} from '../../model/state/column.state';
import {MoveColumnAction} from '../../model/actions/move-column.action';
import {GridState} from '../../model/state/grid.state';
import {createEntityAdapter} from '@ngrx/entity';
import {shallowClone} from '../../../../core/store/store';
import {Identifier} from '../../../../model/entity.model';

export abstract class AbstractColumnDefinitionManager {

  protected adapter = createEntityAdapter<ColumnDefinition>();

  public toggleSortColumn(id: Identifier, state: GridState): Observable<GridState> {

    const columnState: ColumnState = {...state.columnState};
    const sortedColumns = columnState.shouldResetSorts ? [] : [...columnState.sortedColumns];

    const toggledColumn: SortedColumn = sortedColumns.find(column => column.id === id);
    // if column was already sorted, we cycle sort
    if (toggledColumn) {
      const index = sortedColumns.indexOf(toggledColumn);
      const sort = toggleColumnSortState(toggledColumn.sort);
      // we must strip the now unsorted column from sorted columns
      if (sort === Sort.NO_SORT) {
        sortedColumns.splice(index, 1);
      } else if (sort === Sort.ASC) {
        sortedColumns.splice(index, 1, {id, sort: Sort.ASC});
      } else {
        sortedColumns.splice(index, 1, {id, sort: Sort.DESC});
      }
      columnState.sortedColumns = sortedColumns;
    } else {
      // if column was not already sorted, we sort ASC and put the column in last position in sort order
      sortedColumns.push({id, sort: Sort.ASC});
      columnState.sortedColumns = sortedColumns;
    }
    columnState.shouldResetSorts = false;
    return of({...state, columnState});
  }

  setSortColumn(sortedColumns: SortedColumn[], state: GridState): Observable<GridState> {
    const columnState: ColumnState = {...state.columnState};
    columnState.sortedColumns = [...sortedColumns];
    columnState.shouldResetSorts = false;
    return of({...state, columnState});
  }

  public toggleColumnVisibility(id: Identifier, state: GridState): Observable<GridState> {
    let columnState = {...state.columnState};
    const show = !columnState.entities[id].show;
    columnState = this.adapter.updateOne({id: id as any, changes: {show}}, columnState);
    return of({...state, columnState});
  }

  setColumnVisibility(show: boolean, id: Identifier, state: GridState): Observable<GridState> {
    let columnState = {...state.columnState};
    columnState = this.adapter.updateOne({id: id as any, changes: {show}}, columnState);
    return of({...state, columnState});
  }

  public moveColumn(moveColumn: MoveColumnAction, state: GridState): Observable<GridState> {
    const columnState: ColumnState = {...state.columnState};
    const viewport = findViewport(moveColumn.id, columnState);
    const previousIndex = columnState[viewport].order.indexOf(moveColumn.id);
    columnState[viewport].order.splice(previousIndex, 1);
    columnState[moveColumn.moveToViewport].order.splice(moveColumn.index, 0, moveColumn.id);
    return of({...state, columnState});
  }

  resizeColumn(id: Identifier, offset: number, state: GridState): Observable<GridState> {
    const column = state.columnState.entities[id];
    if (column.resizable && column.widthCalculationStrategy.isResizable) {
      const strategy = shallowClone(column.widthCalculationStrategy);
      strategy.resize(offset);
      const columnState = this.adapter.updateOne({
        id: column.id as any,
        changes: {widthCalculationStrategy: strategy}
      }, state.columnState);
      return of({...state, columnState});
    } else {
      return of(state);
    }
  }

  renameColumn(columnId: Identifier, state: GridState, newLabel: string): Observable<GridState> {
    let columnState = {...state.columnState};
    columnState = this.adapter.updateOne({id: columnId as any, changes: {label: newLabel}}, columnState);
    return of({...state, columnState});
  }
}

function findViewport(id: Identifier, columnState: ColumnState): GridViewportName {
  let result: GridViewportName;
  let found = false;

  for (const viewportName of gridViewportNames) {
    const viewportState: ViewportState = columnState[viewportName];
    if (viewportState.order.includes(id)) {
      found = true;
      result = viewportName;
    }
  }

  if (!Boolean(result)) {
    throw Error(`No column found with id ${id}`);
  }
  return result;
}

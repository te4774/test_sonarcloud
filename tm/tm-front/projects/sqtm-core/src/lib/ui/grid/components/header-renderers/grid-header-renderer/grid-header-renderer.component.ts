import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ElementRef,
  Inject,
  Input,
  NgZone,
  OnDestroy,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {GridService} from '../../../services/grid.service';
import {fromEvent, Subject} from 'rxjs';
import {ColumnDisplay, ColumnWithFilter} from '../../../model/column-display.model';
import {Sort} from '../../../model/column-definition.model';
import {GridDisplay} from '../../../model/grid-display.model';
import {takeUntil, throttleTime} from 'rxjs/operators';
import {DOCUMENT} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {AbstractHeaderRendererComponent} from '../abstract-header-renderer/abstract-hearder-renderer.component';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {ComponentPortal} from '@angular/cdk/portal';
import {AbstractFilterWidget} from '../../../../filters/components/abstract-filter-widget';
import {FilteringChange} from '../../../../filters/state/filter.state';
import {Identifier} from '../../../../../model/entity.model';
import {GridFilterUtils} from '../../../services/filter-managers/grid-filter-utils';

/** @dynamic */
@Component({
  selector: 'sqtm-core-grid-header',
  template: `
    <ng-container *ngIf="gridDisplay">
      <div #header class="full-width flex-row grid-header" [style.height]="calculateRowHeight(gridDisplay)"
           style="justify-content: space-between">
        <div #resizeHandler *ngIf="shouldShowLeftHandler()"
             class="full-height m-l-5 flex-fixed-size resize-handler-left __hover_col_resize">
        </div>
        <div *ngIf="columnDisplay.showHeader" nz-tooltip [nzTooltipTitle]="getTranslatedTitleHeader(columnDisplay)"
             class="full-width flex-row"
             [style.height]="calculateRowHeight(gridDisplay)"
             [class.__hover_pointer]="columnDisplay.sortable"
             [style.justify-content]="getHeaderPosition(columnDisplay)"
             style="overflow-x: hidden; align-items: center"
             (click)="toggleSort()">
          <ng-container *ngIf="hasTitle(columnDisplay); else defaultTooltip">
            <span style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;"
                  class="header-color">{{getTranslatedHeader(columnDisplay)}}</span>
          </ng-container>
          <ng-template #defaultTooltip>
          <span style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;"
                [sqtmCoreLabelTooltip]="getTranslatedHeader(columnDisplay)"
                class="header-color">{{getTranslatedHeader(columnDisplay)}}</span>
          </ng-template>
          <span class="m-l-5" *ngIf="shouldDisplaySortIcon(columnDisplay)">
            <i nz-icon nzTheme="outline" [nzType]="iconName(columnDisplay)"
               class="font-10-px"></i>
          </span>
          <span #filterButton class="m-l-5" *ngIf="shouldDisplayFilterIcon(columnDisplay)" (click)="showFilterWidget($event)"
                [attr.data-test-component-id]="'filter-icon'">
            <ng-container *ngIf="filterIsActiveAndValid(); else filterEmpty">
              <i nz-icon nzTheme="fill" [nzType]="'filter'"
                 class="font-12-px"></i>
            </ng-container>
            <ng-template #filterEmpty>
              <i nz-icon nzTheme="outline" [nzType]="'filter'"
                 class="font-12-px"></i>
            </ng-template>
          </span>
        </div>
        <div #resizeHandler *ngIf="shouldShowRightHandler()"
             class="full-height  m-r-5 flex-fixed-size resize-handler __hover_col_resize">
          <div class="resize-indicator"></div>
        </div>
      </div>
    </ng-container>
  `,
  styleUrls: ['./grid-header-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridHeaderRendererComponent extends AbstractHeaderRendererComponent implements AfterViewInit, OnDestroy {

  protected _columnDisplay: ColumnWithFilter;

  public get columnDisplay(): ColumnWithFilter {
    return this._columnDisplay;
  }

  @Input()
  public set columnDisplay(value: ColumnWithFilter) {
    this._columnDisplay = value;
    if (this.componentRef) {
      this.componentRef.instance.setFilter(value.filter, this.gridDisplay.scope);
    }
    this.initResize();
  }

  @ViewChild('resizeHandler', {read: ElementRef})
  resizeHandler: ElementRef;

  @ViewChild('header', {read: ElementRef})
  private header: ElementRef;

  @ViewChild('filterButton', {read: ElementRef})
  private filterButton: ElementRef;

  private overlayRef: OverlayRef;

  private componentRef: ComponentRef<AbstractFilterWidget>;

  private unsub$ = new Subject<void>();

  private resizeInitialized = false;

  constructor(public grid: GridService,
              private zone: NgZone,
              @Inject(DOCUMENT) private document: Document,
              private translate: TranslateService,
              public cdRef: ChangeDetectorRef,
              private overlay: Overlay,
              private vcr: ViewContainerRef) {
    super(grid, cdRef);
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      this.initResize();
    });
  }

  private initResize() {
    if (this._columnDisplay.resizable && this.resizeHandler && !this.resizeInitialized) {
      this.zone.runOutsideAngular(() => {
        this.resizeInitialized = true;
        fromEvent(this.resizeHandler.nativeElement, 'mousedown')
          .pipe(takeUntil(this.unsub$))
          .subscribe(($event: MouseEvent) => this.dragEventHandler($event.clientX));
      });
    }
  }

  ngOnDestroy(): void {
    this.closeFilterWidget();
    this.unsub$.next();
    this.unsub$.complete();
  }

  toggleSort() {
    if (this._columnDisplay.sortable) {
      this.grid.toggleColumnSort(this._columnDisplay.id);
    }
  }

  iconName(column: ColumnDisplay) {
    return column.sort === Sort.ASC ? 'arrow-up' : 'arrow-down';
  }

  shouldDisplaySortIcon(column: ColumnDisplay) {
    return column.sort !== Sort.NO_SORT;
  }

  hasTitle(column: ColumnDisplay): boolean {
    return Boolean(column.titleI18nKey);
  }

  calculateRowHeight(gridDisplay: GridDisplay): string {
    return `${gridDisplay.rowHeight}px`;
  }

  private dragEventHandler(startX: number) {
    const stopListenDnd = new Subject<void>();
    let currentX = startX;

    fromEvent(this.document.documentElement, 'mousemove').pipe(
      takeUntil(stopListenDnd),
      throttleTime(60)
    ).subscribe(($event: MouseEvent) => {
      this.offsetColumnWidth($event.clientX - currentX);
      // console.log(`Resize : Client X ${$event.clientX} - CurrentX ${currentX}`);
      currentX = $event.clientX;
    });

    fromEvent(this.document.documentElement, 'mouseup').pipe(
      takeUntil(stopListenDnd),
    ).subscribe(($event: MouseEvent) => {
      this.offsetColumnWidth($event.clientX - currentX);
      stopListenDnd.next();
      stopListenDnd.complete();
    });
    return false;
  }

  private offsetColumnWidth(offset: number) {
    this.zone.run(() => {
      // if rightViewport, we resize by left side, so we must invert the offset
      // if (this.viewportName === 'rightViewport') {
      //   this.grid.offsetColumnWidth(this.columnDisplay.id, -1 * offset);
      // } else {
      this.grid.offsetColumnWidth(this._columnDisplay.id, offset);
      // }
    });
  }

  shouldShowRightHandler() {
    return this._columnDisplay.resizable && this.viewportName !== 'rightViewport';
  }

  shouldShowLeftHandler() {
    return this._columnDisplay.resizable && this.viewportName === 'rightViewport';
  }

  shouldShowDummyHandler() {
    return !this._columnDisplay.resizable;
  }

  getTranslatedHeader(column: ColumnDisplay) {
    if (Boolean(column.i18nKey)) {
      return this.translate.instant(column.i18nKey);
    }

    if (column.label != null) {
      return column.label;
    }
    return column.id;
  }

  getTranslatedTitleHeader(column: ColumnDisplay) {
    if (Boolean(column.titleI18nKey)) {
      return this.translate.instant(column.titleI18nKey);
    }
  }

  getHeaderPosition(column: ColumnDisplay) {

    let justifyContent = 'flex-start';

    switch (column.headerPosition) {
      case 'center':
        justifyContent = 'center';
        break;
      case 'right':
        justifyContent = 'flex-end';
        break;
      default:
        break;
    }

    return justifyContent;
  }

  shouldDisplayFilterIcon(columnDisplay: ColumnWithFilter) {
    return Boolean(columnDisplay.filter);
  }

  showFilterWidget($event: MouseEvent) {
    $event.stopPropagation();
    const positionStrategy = this.overlay.position().flexibleConnectedTo(this.filterButton)
      .withPositions([
        {originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: 10},
        {originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'center', offsetX: 10},
        {originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: -10},
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const componentPortal = new ComponentPortal(this._columnDisplay.filter.widget, this.vcr);
    this.componentRef = this.overlayRef.attach(componentPortal);
    this.componentRef.instance.setFilter(this._columnDisplay.filter, this.gridDisplay.scope);
    this.componentRef.instance.filteringChanged.pipe(
      takeUntil(this.unsub$)
    ).subscribe(value => this.changeFiltering(this._columnDisplay.filter.id, value));
    this.componentRef.instance.close.pipe(
      takeUntil(this.unsub$)
    ).subscribe(() => this.closeFilterWidget());
    this.overlayRef.backdropClick().subscribe(() => this.closeFilterWidget());
  }

  closeFilterWidget() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }
  }

  private changeFiltering(filterId: Identifier, filteringChange: FilteringChange) {
    this.grid.changeFilterValue({id: filterId, ...filteringChange});
  }

  filterIsActiveAndValid() {
    return this._columnDisplay.filter && GridFilterUtils.mustIncludeFilter(this._columnDisplay.filter);
  }
}

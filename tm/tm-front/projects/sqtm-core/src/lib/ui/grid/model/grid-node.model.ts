import type {Identifier} from '../../../model/entity.model';
import type {DataRow} from './data-row.model';
import type {Type} from '@angular/core';
import type {RowRenderer} from '../components/row-renderers/RowRenderer';

export interface GridNodeState {
  id: Identifier;
  // Depth inside the hierarchy. Mainly used for hierarchical cell renderer.
  depth?: number;
  // // This object contains the size of each column used in auto size.
  // // K is the column id, V is the number of chars inside the cell.
  // size?: { [K: string]: number };
  // Position of the row in table
  index: number;
  selected?: boolean;
  // If true the row must be shown as a parent of filtered row but is not in the filters itself.
  showAsFilteredParent?: boolean;
  dataRow?: DataRow;
  rowComponent?: Type<RowRenderer>;
  isLast?: boolean;
  //  the depthMap map says if the ancestors of rows are last in their own container. Used to draw hierarchy lines in trees
  //  or other hierarchy related stuff.
  depthMap?: boolean[];
}

/**
 * Type for displaying GridNode. It's GridNodeState with some additional properties
 */
export interface GridNode extends GridNodeState {
  // If true the node is currently the valid container targeted for dnd operation.
  showAsDndTarget: boolean;
}

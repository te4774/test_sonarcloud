import {InjectionToken} from '@angular/core';
import type {GridNodeGenerator} from './services/node-generators/grid-node-generator';
import type {DataRowLoader} from './services/data-row-loaders/data-row-loader';
import type {Identifier} from '../../model/entity.model';
import type {GridService} from './services/grid.service';

export const GRID_NODE_GENERATOR = new InjectionToken<GridNodeGenerator>(
  'A service used by the grid to transform the internal row data in grid node objects, ready to be drawn by the component');

export const DATA_ROW_LOADER = new InjectionToken<DataRowLoader>(
  'A service used by the grid to load rows, add rows, updates them...');

export const COLUMN_ID = new InjectionToken<Identifier>('Token used to inject column id in Portal Component');

export const GRID_SERVICE_TOKEN = new InjectionToken<GridService>('Token used to inject grid service to resolve circular dependencies');

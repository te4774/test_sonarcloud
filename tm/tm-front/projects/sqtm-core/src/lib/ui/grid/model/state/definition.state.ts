import type {DataRow} from '../data-row.model';
import {GridDataProvider, GridType} from '../grid-definition.model';
import type {Type} from '@angular/core';
import type {ProjectDataMap} from '../../../../model/project/project-data.model';
import type {BindableEntity} from '../../../../model/bindable-entity.model';
import type {DraggedContentRenderer} from '../../../drag-and-drop/dragged-content-renderer';

export interface DefinitionState {
  id: string;
  rowHeight: number;
  dataProvider: GridDataProvider;
  serverRootUrl?: string[];
  serverModificationUrl?: string[];
  gridType: GridType;
  allowShowAll: boolean;
  enableHeaders: boolean;
  enableRightToolBar: boolean;
  enableMultiRowSelection: boolean;
  // if true, the grid must unselect row on simple click if this row is the only selected row
  // of course it's disabled in tree or hierarchical grids because it interfere with double clicks
  unselectRowOnSimpleClick: boolean;
  dynamicHeight: boolean;
  literalDataRowConverter: LiteralDataRowConverter;
  contextualMenuComponent?: Type<any>;
  // does the grid is allowed to start dnd operation
  enableDrag: boolean;
  // does the grid is allowed to perform internal dnd, aka moving nodes into grid.
  enableInternalDrop: boolean;
  draggedContentRenderer: Type<DraggedContentRenderer>;
  // Bindable entity used for the display of filters for custom fields. Decoupled from columns cuf because some grids could
  // filters on one domain and display another one (aka research on test-case and display requirements)
  addFilterCufFrom: BindableEntity;
  // does initial data has been loaded
  loaded: boolean;
  // Does the grid allow modification. Default is true
  // Allow to lock grids from the outside when user has no permissions or a business rules forbid modifications (Milestones...)
  // The exact effect can be different in different grids because rowRenderers, cellRenderers
  // and all configurable stuff can ignore this attribute...
  allowModifications: boolean;
  style: GridStyleDefinition;
  multipleColumnsFilter: string[];
  scopeDefinition: GridScopeDefinition;
}

export interface GridStyleDefinition {
  showLines: boolean;
  showOverlayBeforeLoad: boolean;
  highlightSelectedRows: boolean;
  highlightHoveredRows: boolean;
}

export interface GridScopeDefinition {
  allowCustomScope: boolean;
  customScopeKind?: CustomScopeKind;
}

export type CustomScopeKind = 'requirement' | 'test-case' | 'campaign';

export type LiteralDataRowConverter = (rows: Partial<DataRow>[], projectData: ProjectDataMap) => DataRow[];

export type DefinitionStateReadOnly = Readonly<DefinitionState>;

export function initialDefinitionState(): DefinitionStateReadOnly {
  return {
    id: null,
    rowHeight: 25,
    serverRootUrl: [],
    serverModificationUrl: [],
    gridType: GridType.GRID,
    dataProvider: GridDataProvider.CLIENT,
    allowShowAll: false,
    enableHeaders: true,
    enableRightToolBar: true,
    enableMultiRowSelection: true,
    dynamicHeight: false,
    literalDataRowConverter: null,
    contextualMenuComponent: null,
    addFilterCufFrom: null,
    enableDrag: false,
    enableInternalDrop: false,
    unselectRowOnSimpleClick: true,
    loaded: false,
    allowModifications: true,
    style: {
      showLines: false,
      showOverlayBeforeLoad: false,
      highlightSelectedRows: true,
      highlightHoveredRows: true,
    },
    scopeDefinition: {
      allowCustomScope: false
    },
    multipleColumnsFilter: [],
    draggedContentRenderer: null,
  };
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {RowRenderer} from '../RowRenderer';
import {GridDisplay, ViewportDisplay} from '../../../model/grid-display.model';
import {GridNode} from '../../../model/grid-node.model';

@Component({
  selector: 'sqtm-core-empty-grid-message',
  template: `
    <div *ngIf="gridDisplay?.loaded && viewport.name ==='mainViewport'"
         class="full-width full-height">{{"sqtm-core.grid.empty-grid.label"|translate}}</div>
  `,
  styleUrls: ['./empty-grid-message.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmptyGridMessageComponent implements OnInit, RowRenderer {

  gridDisplay: GridDisplay;
  gridNode: GridNode;
  viewport: ViewportDisplay;

  constructor(public cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
  }


}

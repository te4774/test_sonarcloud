import type {DataRow, DataRowSortFunction} from './data-row.model';
import type {Type} from '@angular/core';
import type {I18nEnum} from '../../../model/level-enums/level-enum';
import type {Identifier} from '../../../model/entity.model';
import type {GridNode} from './grid-node.model';
import type {POSITION} from './column-definition.builder';
import type {ResearchColumnPrototype} from '../../filters/state/filter.state';
import type {GridViewportName} from './state/column.state';
import type {ColumnDisplay} from './column-display.model';
import type {ProjectData} from '../../../model/project/project-data.model';
import {ValueRenderer} from '../../grid-export/value-renderer/value-renderer';

export class ColumnDefinition {
  // Identifier of the column. Must be unique across a grid instance.
  id: string;
  // Key of the column header
  i18nKey?: string;
  // Label of the column's header. Will have precedence over i18nKey.
  label?: string;
  // Does the column is resizable by user
  // Will have effect only if widthCalculationStrategy is a kind of resizable one.
  resizable = true;
  widthCalculationStrategy: WidthCalculationStrategy;
  sortable = true;
  // Does the cells of this column can be drag and dropped
  draggable = false;
  // Show
  show = true;
  // Class of component used to render the cells for this column.
  // This component must implement the AbstractCellRenderer Class
  cellRenderer?: Type<any>;
  // Will render the cell content even if there is no value for this cell in the DataRow
  forceRenderOnNoValue = false;
  // Custom function called to perform sort. Used only for sorts client side
  sortFunction?: DataRowSortFunction;
  // An optional column proto for grids backed by the custom chart engine.
  // That column proto will be transmitted to backend, for sorts or other operation that require it.
  // Do not use that and standard filters at the same time...
  columnPrototype?: ResearchColumnPrototype;
  // associate the column to the filter identified by the value of this field
  // do not use that and columnPrototype in the same grid
  associateToFilter?: Identifier;
  // Class of component used to render the header of this column.
  headerRenderer?: Type<any>;
  cufId?: number;
  titleI18nKey?: string;
  headerPosition: POSITION = 'left';
  contentPosition: POSITION = 'left';
  editable?: boolean | ((columnDisplay: ColumnDisplay, row: DataRow) => boolean);
  showHeader = true;
  viewportName: GridViewportName = 'mainViewport';
  options?: ColumnDefinitionOption;
  exportValueRenderer?: Type<ValueRenderer>;
}

export class SortedColumn {
  id: Identifier;
  sort: Sort;
}

export enum Sort {
  'NO_SORT' = 'NO_SORT', ASC = 'ASC', DESC = 'DESC'
}

export type WidthCalculationStrategy = ExtensibleWidthCalculationStrategy | InvariableWidthCalculationStrategy;

interface WidthCalculationStrategyBase {
  readonly isResizable: boolean;
  width: number;

  calculateInvariantWidth(columnId: string, gridNodes: GridNode[]): number;

  resize(offset: number);
}

export function isExtensible(strategy: WidthCalculationStrategy): strategy is ExtensibleWidthCalculationStrategy {
  return strategy.isExtensible;
}

export interface ExtensibleWidthCalculationStrategy extends WidthCalculationStrategyBase {
  readonly isExtensible: true;
  fraction: number;
}

export interface InvariableWidthCalculationStrategy extends WidthCalculationStrategyBase {
  readonly isExtensible: false;
}

export class Fixed implements InvariableWidthCalculationStrategy {
  readonly width: number;
  readonly isResizable = false;
  readonly isExtensible = false;

  constructor(width: number) {
    this.width = width;
  }

  calculateInvariantWidth(): number {
    return this.width;
  }

  resize(offset: number) {
    throw new Error('This strategy does not allow resize');
  }
}

export class Limited implements InvariableWidthCalculationStrategy {
  readonly isResizable = true;
  readonly isExtensible = false;
  readonly minWidth: number;
  readonly maxWidth: number;
  width: number;

  constructor(width: number, minWidth: number = 60, maxWidth: number = 600) {
    this.minWidth = minWidth;
    this.maxWidth = maxWidth;
    this.width = width;
  }

  calculateInvariantWidth(): number {
    return this.width;
  }

  resize(offset: number) {
    let calculatedWidth = this.width + offset;

    if (calculatedWidth < this.minWidth) {
      calculatedWidth = this.minWidth;
    }

    if (calculatedWidth > this.maxWidth) {
      calculatedWidth = this.maxWidth;
    }

    this.width = calculatedWidth;
  }
}

// default auto resize by length and depth for string data
// should be enough for our needs, but it's always possible to create a new custom strategy if needed
export class ContentDriven implements InvariableWidthCalculationStrategy {
  readonly isResizable = false;
  readonly width: number;
  readonly isExtensible = false;

  constructor(width: number = 0) {
    this.width = width;
  }

  calculateInvariantWidth(columnId: string, gridNodes: GridNode[]): number {
    let maxLength = 0;
    let maxDepth = 0;
    const charWidth = 10;
    const depthWidth = 15;
    gridNodes.forEach(gridNode => {
      if (Boolean(gridNode.dataRow) && Boolean(gridNode.dataRow.data) && gridNode.dataRow.data[columnId]) {
        const value = gridNode.dataRow.data[columnId];
        if (typeof value === 'string' && value.length > maxLength) {
          maxLength = value.length;
        }
        if (gridNode.depth > maxLength) {
          maxDepth = gridNode.depth;
        }
      }
    });
    // hard coded for now, char size is 14 by default. 10px seems to give the better result without cutting content
    const calculatedWidth = maxDepth * depthWidth + maxLength * charWidth;
    return calculatedWidth < this.width ? this.width : calculatedWidth;
  }

  resize(offset: number) {
    throw new Error('This strategy does not allow resize');
  }
}


export class Extendable implements ExtensibleWidthCalculationStrategy {
  readonly isResizable = false;
  readonly isExtensible = true;
  readonly width: number;
  private _fraction: number;

  constructor(width: number = 100, fraction: number = 1) {
    this.width = width;
    this.fraction = fraction;
  }

  get fraction(): number {
    return this._fraction;
  }

  set fraction(value: number) {
    if (value <= 0) {
      throw Error('Fraction must be a positive number');
    }
    this._fraction = value;
  }

  calculateInvariantWidth(): number {
    return this.width;
  }

  resize(offset: number) {
    throw new Error('This strategy does not allow resize');
  }
}


export function toggleColumnSortState(columnSort: Sort): Sort {
  if (!columnSort) {
    return Sort.ASC;
  }
  switch (columnSort) {
    case Sort.NO_SORT :
      return Sort.ASC;
    case Sort.ASC:
      return Sort.DESC;
    case Sort.DESC:
      return Sort.NO_SORT;
  }
}

export function columnSortMultiplier(columnSort: Sort): number {
  if (columnSort === Sort.ASC) {
    return 1;
  } else if (columnSort === Sort.DESC) {
    return -1;
  }
  throw new Error(`No multiplier for sort : ${columnSort}`);
}

export type ColumnDefinitionOptionKind = 'link' | 'iconLink' | 'i18nEnum' | 'infoList' | 'dataset' | 'treeNodeRendererDisplay';

export interface ColumnDefinitionOption {
  kind: ColumnDefinitionOptionKind;
}

export class DatasetOptions implements ColumnDefinitionOption {
  itemIdType: string;
  kind: ColumnDefinitionOptionKind = 'dataset';
}

export class LinkOptions implements ColumnDefinitionOption {
  baseUrl?: string;
  columnParamId?: string;
  createUrlFunction?: (row: DataRow, columnDisplay: ColumnDisplay) => string | string[];
  kind: ColumnDefinitionOptionKind = 'link';
  saveGridStateBeforeNavigate?: boolean;
}

export class IconLinkOptions extends LinkOptions {
  iconName: string;
  kind: ColumnDefinitionOptionKind = 'iconLink';
}

export class InfoListOptions implements ColumnDefinitionOption {
  infolist: ('testCaseNature' | 'testCaseType' | 'requirementCategory') & keyof ProjectData;
  kind: ColumnDefinitionOptionKind = 'infoList';
}

export class I18nEnumOptions implements ColumnDefinitionOption {
  kind: ColumnDefinitionOptionKind = 'i18nEnum';
  i18nEnum: I18nEnum<any>;
  showIcon: boolean;
  showLabel: boolean;
}


export class TreeNodeRendererDisplayOptions implements ColumnDefinitionOption {
  ellipsisOnLeft: boolean;
  kind: 'treeNodeRendererDisplay';
}

export function isDatasetOptions(options: ColumnDefinitionOption): options is DatasetOptions {
  return options.kind === 'dataset';
}

export function isLinkOptions(options: ColumnDefinitionOption): options is LinkOptions {
  return options.kind === 'link';
}

export function isIconLinkOptions(options: ColumnDefinitionOption): options is IconLinkOptions {
  return options.kind === 'iconLink';
}

export function isI18nEnumOptions(options: ColumnDefinitionOption): options is I18nEnumOptions {
  return options.kind === 'i18nEnum';
}

export function isInfoListOptions(options: ColumnDefinitionOption): options is InfoListOptions {
  return options.kind === 'infoList';
}

export function isTreeNodeRendererDisplayOptions(options: ColumnDefinitionOption): options is TreeNodeRendererDisplayOptions {
  return options.kind === 'treeNodeRendererDisplay';
}



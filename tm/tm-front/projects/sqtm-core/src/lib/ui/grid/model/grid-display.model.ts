import type {ColumnWithFilter} from './column-display.model';
import type {GridType} from './grid-definition.model';
import type {GridScopeDefinition, GridStyleDefinition} from './state/definition.state';
import type {Scope} from '../../filters/state/filter.state';
import type {DraggedContentRenderer} from '../../drag-and-drop/dragged-content-renderer';
import type {Type} from '@angular/core';
import type {GridViewportName} from './state/column.state';

export class GridDisplay {
  id: string;
  rowHeight: number;
  gridType: GridType;
  allowShowAll: boolean;
  enableHeaders: boolean;
  enableRightToolBar: boolean;
  dynamicHeight: boolean;
  mainViewport: ViewportDisplay;
  leftViewport: ViewportDisplay;
  rightViewport: ViewportDisplay;
  // will only be calculated if grid has dynamicHeight set to true
  gridHeight?: number;
  enableDrag: boolean;
  enableInternalDrop: boolean;
  draggedContentRenderer: Type<DraggedContentRenderer>;
  loaded: boolean;
  allowModifications: boolean;
  style: GridStyleDefinition;
  scopeDefinition: GridScopeDefinition;
  scope: Scope;
}

export class ViewportDisplay {
  columnDisplays: ColumnWithFilter[];
  totalWidth: number;
  name: GridViewportName;
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AbstractCellRendererComponent} from '../abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../../services/grid.service';
import {filter, takeUntil, tap} from 'rxjs/operators';
import {Subject, timer} from 'rxjs';
import {KeyCodes} from '../../../../utils/key-codes';
import {TableValueChange} from '../../../model/actions/table-value-change';

@Component({
  selector: 'sqtm-core-editable-text-cell-renderer',
  template: `
    <div *ngIf="columnDisplay"
         (click)="activateEditMode()">
      <div *ngIf="row && !edit">
        <span>{{row.data[columnDisplay.id]}}</span>
      </div>
      <input #input type="text" *ngIf="row && edit" (blur)="handleBlur()" (keyup)="handleKeyboardInput($event)">
    </div>
  `,
  styleUrls: ['./editable-text-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableTextCellRendererComponent extends AbstractCellRendererComponent implements OnInit {

  edit = false;

  @ViewChild('input')
  input: ElementRef;

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  ngOnInit() {
  }

  activateEditMode() {
    if (!this.edit) {
      this.edit = true;
      // kind of pulling for waiting that input is displayed and active.
      const unsub = new Subject<void>();
      timer(1, 10).pipe(
        takeUntil(unsub),
        filter(() => Boolean(this.input)),
        tap(() => this.input.nativeElement.focus()),
        tap(() => {
          const input = this.input.nativeElement as HTMLInputElement;
          input.value = this.row.data[this.columnDisplay.id];
        }),
        // auto unsub, so we don't have wild timers that run into app after an edition
        tap(() => {
          unsub.next();
          unsub.complete();
        })
      ).subscribe();
    }
    return false;
  }

  handleBlur() {
    this.disableEditMode();
  }

  disableEditMode() {
    if (this.edit) {
      this.edit = false;
    }
  }

  handleKeyboardInput($event: KeyboardEvent) {
    // MS Edge doesn't support key so we rely on deprecated keyCode.
    const key = $event.keyCode;
    if (key === KeyCodes.ENTER) {
      const inputElement = this.input.nativeElement as HTMLInputElement;
      const changedValue: TableValueChange = {columnId: this.columnDisplay.id, value: inputElement.value};
      this.disableEditMode();
      this.grid.editRows([this.row.id], [changedValue]);
    } else if (key === KeyCodes.ESC) {
      this.disableEditMode();
    }
    return false;
  }
}

import {GridPartialConfiguration} from '../model/grid-partial-configuration.model';
import {GridState} from '../model/state/grid.state';
import {Identifier} from '../../../model/entity.model';


function restorePagination(nextState: GridState, conf: GridPartialConfiguration) {
  if (nextState.paginationState.allowedPageSizes.includes(conf.pagination.size) && conf.pagination.page >= 0) {
    nextState.paginationState = {...nextState.paginationState, ...conf.pagination};
  }
}

export function checkAndSetInitialConfiguration(conf: GridPartialConfiguration, state: GridState) {
  const nextState: GridState = {...state};
  if (conf.pagination) {
    restorePagination(nextState, conf);
  }
  const sortedColumns = conf.sortedColumns;
  if (sortedColumns?.length > 0) {
    const existingColumns: Identifier[] = nextState.columnState.ids;
    const validSortedColumns = sortedColumns.filter(sortedColumn => existingColumns.includes(sortedColumn.id));
    nextState.columnState = {...nextState.columnState, sortedColumns: validSortedColumns};
  }
  if (conf.filters?.length > 0) {
    conf.filters.forEach(filterSnapshot => {
      const gridFilters = state.filterState.filters;
      const ids: Identifier[] = gridFilters.ids;
      if (ids.includes(filterSnapshot.id)) {
        gridFilters.entities[filterSnapshot.id].value = filterSnapshot.value;
        gridFilters.entities[filterSnapshot.id].active = true;
      }
    });
  }
  return nextState;
}

import {TestBed} from '@angular/core/testing';

import {GridService} from './grid.service';
import {RestService} from '../../../core/services/rest.service';
import {gridServiceFactory} from '../grid.service.provider';
import {PaginationDisplay} from '../model/pagination-display.model';
import {initialGridState} from '../model/state/grid.state';
import {GridDefinition} from '../model/grid-definition.model';
import {createColumn} from '../grid-testing/grid-testing-utils';
import {grid} from '../../../model/grids/grid-builders';
import {computeInvariableViewport} from './grid-service-impl';
import {ReferentialDataService} from '../../../core/referential/services/referential-data.service';
import {Fixed, Limited} from '../model/column-definition.model';
import {createStore} from '../../../core/store/store';
import {pluck, shareReplay, take} from 'rxjs/operators';

describe('GridService', () => {

  const gridConfig = grid('grid-test').build();
  const restService = {};
  const referentialDataService = {};

  beforeEach(function () {
      return TestBed.configureTestingModule({
        providers: [
          {
            provide: GridDefinition,
            useValue: gridConfig
          },
          {
            provide: GridService,
            useFactory: gridServiceFactory,
            deps: [RestService, GridDefinition, ReferentialDataService]
          },
          {
            provide: RestService,
            useValue: restService
          }
        ]
      });
    }
  );

  it('should change pagination size', () => {
    const service: GridService = TestBed.inject(GridService);
    let pagination: PaginationDisplay = {
      active: true,
      allowedPageSizes: [],
      count: 0,
      maxPage: 1,
      page: 1,
      size: 25,
      showPaginationFooter: true
    };
    service.paginationDisplay$.subscribe(
      value => {
        pagination = value;
      }
    );

    expect(service).toBeTruthy();
    const gridState = {...initialGridState()};
    const dataRowState = {ids: [], entities: {}, count: 255, rootRowIds: [], selectedRows: []};
    service['store'].commit({...gridState, dataRowState});
    expect(pagination.count).toEqual(255);

    service.changePaginationSize(50);
    expect(pagination.size).toEqual(50);
    expect(pagination.maxPage).toEqual(5);

    service.paginateNextPage();
    expect(pagination.page).toEqual(1);

    service.changePaginationSize(12);
    expect(pagination.size).toEqual(50);
    expect(pagination.maxPage).toEqual(5);

    expect(pagination.page).toEqual(1);

    service.changePaginationSize(10);
    expect(pagination.size).toEqual(10);
    expect(pagination.maxPage).toEqual(25);
    expect(pagination.page).toEqual(0);

  });

  it('should change page', () => {
    const service: GridService = TestBed.inject(GridService);
    let pagination: PaginationDisplay;
    service.paginationDisplay$.subscribe(
      value => pagination = value
    );

    expect(service).toBeTruthy();
    const gridState = {...initialGridState()};
    gridState.dataRowState = {
      ids: [],
      entities: {},
      count: 52,
      rootRowIds: [],
      selectedRows: [],
      lastToggledRowId: null
    };
    service['store'].commit(gridState);
    expect(pagination.count).toEqual(52);

    // we are at page 0 so paginatePreviousPage should be without effect
    service.paginatePreviousPage();
    expect(pagination.page).toEqual(0);

    service.paginateNextPage();
    expect(pagination.page).toEqual(1);

    // navigate back to page 0
    service.paginatePreviousPage();
    expect(pagination.page).toEqual(0);

    // navigate to page 2 witch should be max page
    service.paginateNextPage();
    service.paginateNextPage();
    expect(pagination.page).toEqual(2);

    // no more paginate next should be possible
    service.paginateNextPage();
    expect(pagination.page).toEqual(2);
  });

  it('should compute invariable ViewportDisplay', () => {
    const gridState = {...initialGridState()};
    const columnState = gridState.columnState;
    columnState.mainViewport = {
      order: ['column-7', 'column-2', 'column-5', 'column-12'],
    };
    columnState.leftViewport = {
      order: ['column-1', 'column-3']
    };
    columnState.ids = ['column-2', 'column-5', 'column-7', 'column-12'];
    columnState.entities = {
      'column-2': {...createColumn('column-2', false), widthCalculationStrategy: new Fixed(200)},
      'column-5': {...createColumn('column-5', false), widthCalculationStrategy: new Limited(75)},
      'column-7': {...createColumn('column-7', false), show: false},
      'column-12': {...createColumn('column-12', false), widthCalculationStrategy: new Limited(50)},
    };
    const mainViewportColumnDisplay = computeInvariableViewport(
      gridState.columnState,
      gridState.definitionState,
      [],
      'mainViewport',
      gridState.filterState);
    expect(mainViewportColumnDisplay.columnDisplays.length).toEqual(4);
    expect(mainViewportColumnDisplay.totalWidth).toEqual(325);
  });

  it('should connect to observable datasource and preserve selected rows', async (done) => {
    const service: GridService = TestBed.inject(GridService);

    interface Data {
      id: number;
      data: string;
    }

    interface State {
      things: Data[];
    }

    const initialState: State = {
      things: [
        {id: 1, data: 'Bonjour'},
        {id: 2, data: 'Aurevoir'},
        {id: 3, data: 'A bientôt'},
      ]
    };

    const store = createStore<State>(initialState);
    const datas$ = store.state$.pipe(pluck('things'), shareReplay(1));

    service.connectToDatasource(datas$, 'id');
    service.selectAllRows();

    store.commit({
      things: [
        {id: 1, data: 'Hey'},
        {id: 4, data: 'Salut'}
      ],
    });

    service.selectedRowIds$.pipe(take(1)).subscribe(rows => {
      expect(rows.length).toBe(1);
      expect(rows[0]).toBe(1);
      done();
    });
  });
});

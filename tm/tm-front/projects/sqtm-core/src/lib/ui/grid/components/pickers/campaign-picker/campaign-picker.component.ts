import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewContainerRef
} from '@angular/core';
import {GridDefinition} from '../../../model/grid-definition.model';
import {treePicker} from '../../../../../model/grids/grid-builders';
import {CAMPAIGN_TREE_PICKER_CONFIG, CAMPAIGN_TREE_PICKER_ID} from '../../../tree-pickers.constant';
import {Extendable} from '../../../model/column-definition.model';
import {
  TreeNodeCellRendererComponent
} from '../../../../cell-renderer-common/tree-node-cell-renderer/tree-node-cell-renderer.component';
import {GridService} from '../../../services/grid.service';
import {gridServiceFactory} from '../../../grid.service.provider';
import {DataRow} from '../../../model/data-row.model';
import {Subject} from 'rxjs';
import {Identifier} from '../../../../../model/entity.model';
import {DialogService} from '../../../../dialog/services/dialog.service';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {TreeWithStatePersistence} from '../../../../workspace-common/components/tree/tree-with-state-persistence';
import {RestService} from '../../../../../core/services/rest.service';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';
import {GridPersistenceService} from '../../../../../core/services/grid-persistence/grid-persistence.service';
import {SquashTmDataRowType} from '../../../../../model/grids/data-row.type';
import {column} from '../../../model/common-column-definition.builders';

export function campaignTreePickerConfigFactory(): GridDefinition {
  return treePicker(CAMPAIGN_TREE_PICKER_ID)
    .server()
    .withServerUrl(['campaign-tree'])
    .withColumns([
      column('NAME')
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent)
    ])
    .enableDrag()
    .build();
}


@Component({
  selector: 'sqtm-core-campaign-picker',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./campaign-picker.component.less'],
  providers: [
    {
      provide: CAMPAIGN_TREE_PICKER_CONFIG,
      useFactory: campaignTreePickerConfigFactory
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, CAMPAIGN_TREE_PICKER_CONFIG, ReferentialDataService]
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignPickerComponent extends TreeWithStatePersistence implements OnInit, OnDestroy {

  @Input()
  set pickerPersistenceKey(persistenceKey) {
    if (!Boolean(this.persistenceKey)) {
      this.persistenceKey = persistenceKey;
    } else {
      throw Error('Cannot change the persistence key dynamically ' + this.persistenceKey);
    }
  }

  @Input()
  initialSelectedNodes: Identifier[] = [];

  @Input()
  restrictedDataRowType: SquashTmDataRowType[] = [];

  @Output()
  selectedRows = new EventEmitter<DataRow[]>();

  @Input()
  set enableMultiRowSelection(shouldBeEnabled: boolean) {
    this.tree.setMultiSelectionEnabled(shouldBeEnabled);
  }

  unsub$ = new Subject<void>();

  constructor(public tree: GridService, protected referentialDataService: ReferentialDataService, protected restService: RestService,
              private cdRef: ChangeDetectorRef, protected dialogService: DialogService, protected vcr: ViewContainerRef,
              protected gridPersistenceService: GridPersistenceService, protected router: Router) {
    super(tree, referentialDataService, gridPersistenceService, restService, router, dialogService, vcr, null, 'campaign-tree');
  }

  ngOnInit() {
    this.tree.selectedRows$.pipe(
      takeUntil(this.unsub$)
    ).subscribe((rows) => {
      this.selectedRows.next(rows);
    });

    this.initData({selectedNodes: this.initialSelectedNodes});
    if (Boolean(this.persistenceKey)) {
      this.registerStatePersistence();
    }
  }

  ngOnDestroy(): void {
    if (this.persistenceKey) {
      this.unregisterStatePersistence();
    }
    this.tree.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

}

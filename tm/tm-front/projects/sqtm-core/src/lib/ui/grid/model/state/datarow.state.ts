import {Identifier} from '../../../../model/entity.model';
import {DataRow, DataRowOpenState} from '../data-row.model';
import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {createSelector} from '@ngrx/store';

export interface DataRowState extends EntityState<DataRow> {
  count: number;
  rootRowIds: Identifier[];
  selectedRows: Identifier[];
  lastToggledRowId: Identifier;
}

export type DataRowStateReadOnly = Readonly<DataRowState>;

export function initialDataRowState(): DataRowStateReadOnly {
  return dataRowEntityAdapter.getInitialState({
    count: 0,
    rootRowIds: [],
    selectedRows: [],
    lastToggledRowId: null,
  });
}

export const dataRowEntityAdapter = createEntityAdapter<DataRow>();

export const selectOpenedRowIds = createSelector(dataRowEntityAdapter.getSelectors().selectAll, dataRows => {
  return dataRows
    .filter(dataRow => dataRow.state === DataRowOpenState.open)
    .map(dataRow => dataRow.id);
});

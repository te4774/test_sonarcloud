import {Update} from '@ngrx/entity';
import {GridNode} from '../../model/grid-node.model';
import {Identifier} from '../../../../model/entity.model';
import {GridState} from '../../model/state/grid.state';
import {Observable, of} from 'rxjs';
import {
  DataRow,
  dndPlaceholderDataRowId,
  DragAndDropPlaceHolderDataRow,
  PlaceholderDataRow
} from '../../model/data-row.model';
import {getDescendantIds, getDraggedRows} from '../data-row-loaders/data-row-utils';
import {GridNodesState, gridNodeStateAdapter} from '../../model/state/grid-nodes.state';
import {GridNodeGenerator} from './grid-node-generator';
import {DragAndDropTarget} from '../../../drag-and-drop/events';

export abstract class AbstractNodeGenerator implements GridNodeGenerator {

  updateNodesData(ids: Identifier[], state: Readonly<GridState>): Observable<GridState> {
    const changes: Update<GridNode>[] = ids
      .map(id => state.dataRowState.entities[id])
      .map(dataRow => {
        return {id: dataRow.id as any, changes: {dataRow}};
      });
    const nodeState = gridNodeStateAdapter.updateMany(changes, state.nodesState);
    return of({...state, nodesState: nodeState});
  }

  dragStart(state: Readonly<GridState>): Observable<GridState> {
    const selectedRows = getDraggedRows(state);
    const descendants = getDescendantIds(state.dataRowState, selectedRows);
    const nodeToRemove: any[] = selectedRows.map(row => row.id).concat(descendants);
    let nodeState = gridNodeStateAdapter.removeMany(nodeToRemove, state.nodesState);
    nodeState = this.reorderNodes(nodeState.ids, nodeState);
    return of({...state, nodesState: nodeState});
  }

  dragOver(dndTarget: DragAndDropTarget, state: Readonly<GridState>): Observable<GridState> {
    let nodesState = {...state.nodesState};
    const draggedRows = getDraggedRows(state);
    const draggedRowIds = draggedRows.map(row => row.id) as any[];
    nodesState = gridNodeStateAdapter.removeMany([dndPlaceholderDataRowId, ...draggedRowIds], nodesState);
    if (dndTarget.zone === 'into') {
      nodesState = this.reorderNodes(nodesState.ids, nodesState);
    } else {
      const nextOrder = [...nodesState.ids].filter(id => id !== dndPlaceholderDataRowId);
      let index = nextOrder.indexOf(dndTarget.id as any);
      if (dndTarget.zone === 'bellow') {
        index++;
      }
      // console.log(`next order before splice: ${JSON.stringify(nextOrder)} with index:${index}`);
      const targetNode = nodesState.entities[dndTarget.id];
      nextOrder.splice(index, 0, dndPlaceholderDataRowId);
      // console.log(`next order after splice : ${JSON.stringify(nextOrder)}`);
      const placeHolder = this.createDndPlaceholderNode(draggedRows[0], targetNode.depth);
      nodesState = gridNodeStateAdapter.upsertOne(placeHolder, nodesState);
      nodesState = this.reorderNodes(nextOrder, nodesState);
    }
    return of({
      ...state,
      nodesState,
      uiState: {...state.uiState, dragState: {...state.uiState.dragState, currentDndTarget: dndTarget, dragging: true}}
    });
  }

  // Create a placeholder node at index 0 when no nodes are in state.
  protected createPlaceholderNode() {
    const emptyGridDataRow = new PlaceholderDataRow();
    return {
      id: emptyGridDataRow.id,
      dataRow: emptyGridDataRow,
      index: 0
    };
  }

  protected createDndPlaceholderNode(dataRow: DataRow, depth: number): GridNode {
    const dndDataRow = new DragAndDropPlaceHolderDataRow(dataRow);
    return {
      id: dndPlaceholderDataRowId,
      dataRow: dndDataRow,
      index: 0,
      depth,
      showAsDndTarget: false,
      isLast: false
    };
  }

  private reorderNodes(newOrder: Identifier[], nodes: Readonly<GridNodesState>): GridNodesState {
    const updates: Update<GridNode>[] = [];
    for (let i = 0; i < newOrder.length; i++) {
      const remainingTestStep = newOrder[i];
      updates.push({id: remainingTestStep as any, changes: {index: i}});
    }
    return gridNodeStateAdapter.updateMany(updates, nodes);
  }

  abstract computeNodeTree(state: Readonly<GridState>): Observable<GridState>;
}


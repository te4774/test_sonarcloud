import {GridState} from '../../model/state/grid.state';
import {OpenContextualMenuAction} from '../../model/actions/open-contextual-menu.action';

export class ContextualMenuService implements ContextualMenuService {

  constructor() {
  }

  openContextualMenu(action: OpenContextualMenuAction, state: GridState) {
    const uiState = {...state.uiState};
    uiState.showContextualMenu = true;
    return {...state, uiState};
  }

  closeContextualMenu(state: GridState) {
    const uiState = {...state.uiState};
    uiState.showContextualMenu = false;
    return {...state, uiState};
  }

}

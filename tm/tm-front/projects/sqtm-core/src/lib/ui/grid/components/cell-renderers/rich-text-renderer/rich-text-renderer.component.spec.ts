import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {RichTextRendererComponent} from './rich-text-renderer.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridDefinition} from '../../../model/grid-definition.model';
import {GridService} from '../../../services/grid.service';
import {gridServiceFactory} from '../../../grid.service.provider';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';
import {RestService} from '../../../../../core/services/rest.service';
import {grid} from '../../../../../model/grids/grid-builders';

describe('RichTextRendererComponent', () => {
  let component: RichTextRendererComponent;
  let fixture: ComponentFixture<RichTextRendererComponent>;

  const gridConfig = grid('grid-test').build();
  const restService = {};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RichTextRendererComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RichTextRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import type {ColumnDefinitionOption, Sort, WidthCalculationStrategy} from './column-definition.model';
import type {Type} from '@angular/core';
import type {LevelEnum} from '../../../model/level-enums/level-enum';
import type {GridFilter} from './state/filter.state';
import type {Identifier} from '../../../model/entity.model';
import type {POSITION} from './column-definition.builder';
import type {GridViewportName} from './state/column.state';
import type {DataRow} from './data-row.model';
import {ValueRenderer} from '../../grid-export/value-renderer/value-renderer';

export interface ColumnDisplay {
  id: Identifier;
  i18nKey?: string;
  label?: string;
  toolTipText?: string;
  widthCalculationStrategy: WidthCalculationStrategy;
  draggable?: boolean;
  sortable?: boolean;
  associateToFilter?: Identifier;
  resizable?: boolean;
  forceRenderOnNoValue?: boolean;
  sort?: Sort;
  show: boolean;
  cellRenderer?: Type<any>;
  exportValueRenderer?: Type<ValueRenderer>;
  levelEnum?: LevelEnum<any>;
  // levelEnumEditable?: boolean;
  headerRenderer?: Type<any>;
  iconName?: string;
  iconTheme?: string;
  cufId?: number;
  titleI18nKey?: string;
  viewportName: GridViewportName;
  headerPosition: POSITION;
  contentPosition: POSITION;
  editable?: boolean | ((columnDisplay: ColumnDisplay, row: DataRow) => boolean);
  showHeader: boolean;
  options?: ColumnDefinitionOption;
}

export type ColumnWithFilter = ColumnDisplay & { filter?: GridFilter };

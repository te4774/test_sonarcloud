import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import {GridNode} from '../../model/grid-node.model';
import {fromEvent, Subject} from 'rxjs';
import {RowRenderer} from '../../components/row-renderers/RowRenderer';
import {GridDisplay, ViewportDisplay} from '../../model/grid-display.model';
import {GridService} from '../../services/grid.service';
import {GridType} from '../../model/grid-definition.model';
import {switchMap, take, takeUntil} from 'rxjs/operators';
import {GridDndData} from '../../model/drag-and-drop/grid-dnd-data';
import {GridState} from '../../model/state/grid.state';
import {DraggableListItemDirective, DraggableListItemType} from '../../../drag-and-drop/draggable-list-item.directive';
import {DynamicComponentDirective} from '../../../workspace-common/directives/dynamic-component.directive';
import {GridViewportColumns, GridViewportService, RenderedGridViewport} from '../../services/grid-viewport.service';

// export const GRID_ROW_DROP_ZONE_HEIGHT = 4;

@Component({
  selector: 'sqtm-core-grid-row-wrapper',
  templateUrl: './grid-row-wrapper.component.html',
  styleUrls: ['./grid-row-wrapper.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridRowWrapperComponent implements OnInit, AfterViewInit, OnDestroy {

  private _gridDisplay: GridDisplay;

  get gridDisplay(): GridDisplay {
    return this._gridDisplay;
  }

  @Input()
  set gridDisplay(gridDisplay: GridDisplay) {
    this._gridDisplay = gridDisplay;
    this.updateDynamicComponent();
  }

  private _gridNode: GridNode;

  @Input()
  set gridNode(gridNode: GridNode) {
    if (Boolean(this.rowComponentRef)) {
      const previousType = this._gridNode.dataRow.type;
      this._gridNode = gridNode;
      if (previousType !== gridNode.dataRow.type) {
        this.createRow();
        this.cdRef.detectChanges();
      }
    } else {
      this._gridNode = gridNode;
      this.createRow();
    }
    this.updateDynamicComponent();
  }

  get gridNode(): GridNode {
    return this._gridNode;
  }

  private _viewport: ViewportDisplay;

  get viewport(): ViewportDisplay {
    return this._viewport;
  }

  @Input()
  set viewport(viewportDisplay: ViewportDisplay) {
    this._viewport = viewportDisplay;
    this.updateDynamicComponent();
  }

  @ViewChild('rowElement', {read: ElementRef, static: true})
  rowElement: ElementRef;

  @ViewChild(DynamicComponentDirective, {static: true})
  dynamicContainer: DynamicComponentDirective<RowRenderer>;

  @ViewChild(DraggableListItemDirective, {static: true})
  draggableListItem: DraggableListItemDirective;

  private rowComponentRef: ComponentRef<RowRenderer>;

  private unsub$ = new Subject<void>();

  private draggedRows = new Subject<GridDndData>();

  constructor(public grid: GridService,
              private zone: NgZone,
              private renderer: Renderer2,
              private cdRef: ChangeDetectorRef,
              private gridViewportService: GridViewportService) {
  }

  ngOnInit() {
    if (this.draggableListItem) {
      this.draggableListItem.connectDataSource(this.draggedRows.asObservable().pipe());
    }
    this.zone.runOutsideAngular(() => {
      this.gridViewportService.renderedGridViewport$
        .pipe(takeUntil(this.unsub$))
        .subscribe(renderedGridViewport => this.resizeRow(renderedGridViewport));
    });
  }

  ngAfterViewInit(): void {
    this.initializeMouseDown();
    this.initializeRowHighlight();
  }

  private initializeMouseDown() {
    // fixing dnd data when user mouse down in case of it could be a dnd.
    // we cannot just wait the dnd start, as data must be fixed at dnd init
    fromEvent(this.rowElement.nativeElement, 'mousedown').pipe(
      takeUntil(this.unsub$),
      switchMap(() => this.grid.gridState$.pipe(take(1)))
    ).subscribe((gridState: GridState) => {
      const selectedRowIds = gridState.dataRowState.selectedRows;
      let draggedRowIds = selectedRowIds;
      if (!selectedRowIds.includes(this.gridNode.id)) {
        draggedRowIds = [this.gridNode.id];
      }
      const draggedRows = draggedRowIds
        .map(id => gridState.dataRowState.entities[id])
        .filter(row => row?.allowMoves);
      this.draggedRows.next(new GridDndData(draggedRows));
    });
  }

  handleContextualMenu(event: MouseEvent) {
    // console.log(`Right Click On ${this.gridNode.id}`);
    // event.preventDefault();
    // this.grid.openContextualMenu({id: this.gridNode.id, x: event.clientX, y: event.clientY});
    return false;
  }

  ngOnDestroy(): void {
    this.draggedRows.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initializeRowHighlight() {
    if (this.shouldHighlightRows()) {
      this.initializeEnterRow();
      this.initializeLeaveRow();
    }
  }

  private resizeRow(renderedGridViewport: RenderedGridViewport) {
    const gridViewportColumns: GridViewportColumns = renderedGridViewport[this.viewport.name];
    // setting width
    let width: string;
    if (gridViewportColumns.totalWidth === 0) {
      width = `100%`;
    } else {
      width = `${gridViewportColumns.totalWidth}px`;
    }
    this.renderer.setStyle(this.rowElement.nativeElement, 'width', width);
    // setting height
    if (this.gridDisplay.gridType === GridType.TABLE) {
      this.renderer.setStyle(this.rowElement.nativeElement, 'display', 'flex');
    } else {
      const height = `${this.gridDisplay.rowHeight}px`;
      this.renderer.setStyle(this.rowElement.nativeElement, 'height', height);
    }
  }

  private shouldHighlightRows() {
    return this.gridDisplay.style.highlightHoveredRows;
  }

  private initializeEnterRow() {
    this.zone.runOutsideAngular(() => {
      fromEvent(this.rowElement.nativeElement, 'mouseenter').pipe(
        takeUntil(this.unsub$)
      ).subscribe(() => {
        this.grid.notifyEnterRow(this._gridNode.id);
      });
    });
  }

  private initializeLeaveRow() {
    this.zone.runOutsideAngular(() => {
      fromEvent(this.rowElement.nativeElement, 'mouseleave').pipe(
        takeUntil(this.unsub$)
      ).subscribe(() => {
        this.grid.notifyLeaveRow(this._gridNode.id);
      });
    });
  }

  private createRow() {
    this.dynamicContainer.type = this.gridNode.dataRow.component;
    this.rowComponentRef = this.dynamicContainer.instantiateComponent();
  }

  private rowIsRendered() {
    return Boolean(this.rowComponentRef);
  }

  private allAttributesAreSet() {
    return this.gridNode && this.gridDisplay && this.viewport;
  }

  private updateDynamicComponent() {
    if (this.rowIsRendered() && this.allAttributesAreSet()) {
      const instance = this.rowComponentRef.instance;
      instance.gridDisplay = this.gridDisplay;
      instance.gridNode = this.gridNode;
      instance.viewport = this.viewport;
      instance.cdRef.detectChanges();
    }
  }

  isDraggable(gridDisplay: GridDisplay, gridNode: GridNode): boolean {
    return gridDisplay.enableDrag
      && gridNode.dataRow?.allowMoves;
  }

  getDndType(gridNode: GridNode) {
    const allowMoves = gridNode.dataRow.allowMoves;
    const allowChildren = gridNode.dataRow.allowedChildren.length > 0;
    let dndType: DraggableListItemType = 'leaf';
    if (!allowMoves && allowChildren) {
      dndType = 'container';
    } else if (allowMoves && allowChildren) {
      dndType = 'node';
    }
    return dndType;
  }
}

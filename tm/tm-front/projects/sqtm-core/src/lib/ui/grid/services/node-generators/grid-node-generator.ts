import {GridState} from '../../model/state/grid.state';
import {Observable} from 'rxjs';
import {Identifier} from '../../../../model/entity.model';
import {DragAndDropTarget} from '../../../drag-and-drop/events';

/**
 * Generators are responsible of creating GridNodes (that are rendered by GridViewportComponent) from DataRows (The raw data). The main goal
 * of this transformation is to flat the tree of DataRows into a list of nodes with correct structure for display.
 */
export interface GridNodeGenerator {

  /**
   * Compute the whole tree into GridNode.
   * @param state of grid
   */
  computeNodeTree(state: Readonly<GridState>): Observable<GridState>;

  /**
   * Update only the data of given nodes, by synchronize nodes with their dataRow. This method do not recompute the tree, only update data.
   * @param ids of nodes/dataRows concerned.
   * @param state of grid
   */
  updateNodesData(ids: Identifier[], state: Readonly<GridState>): Observable<GridState>;

  /**
   * Start a drag operation by removing all selected rows and children from rendered nodes.
   * Will preserve the original node order to restore it if a cancel drag occurs.
   * @param state of grid
   */
  dragStart(state: Readonly<GridState>): Observable<GridState>;

  dragOver(dndTarget: DragAndDropTarget, state: Readonly<GridState>): Observable<GridState>;
}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewContainerRef
} from '@angular/core';
import {Identifier} from '../../../../../model/entity.model';
import {DataRow} from '../../../model/data-row.model';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {GridService} from '../../../services/grid.service';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';
import {RestService} from '../../../../../core/services/rest.service';
import {DialogService} from '../../../../dialog/services/dialog.service';
import {GridPersistenceService} from '../../../../../core/services/grid-persistence/grid-persistence.service';
import {Router} from '@angular/router';
import {TreeWithStatePersistence} from '../../../../workspace-common/components/tree/tree-with-state-persistence';
import {
  HIGH_LEVEL_REQUIREMENT_CHILDREN_TREE_PICKER_ID,
  REQUIREMENT_TREE_PICKER_CONFIG
} from '../../../tree-pickers.constant';
import {gridServiceFactory} from '../../../grid.service.provider';
import {GridDefinition} from '../../../model/grid-definition.model';
import {treePicker} from '../../../../../model/grids/grid-builders';
import {Extendable} from '../../../model/column-definition.model';
import {
  TreeNodeCellRendererComponent
} from '../../../../cell-renderer-common/tree-node-cell-renderer/tree-node-cell-renderer.component';
import {column} from '../../../model/common-column-definition.builders';


export function highLevelRequirementChildrenTreePickerDefinition(): GridDefinition {
  return treePicker(HIGH_LEVEL_REQUIREMENT_CHILDREN_TREE_PICKER_ID)
      .server()
      .withServerUrl(['requirement-tree'])
      .withColumns([
        column('NAME')
            .changeWidthCalculationStrategy(new Extendable(300))
            .withRenderer(TreeNodeCellRendererComponent)
      ])
      .enableDrag()
      .build();
}

@Component({
  selector: 'sqtm-core-high-level-requirement-children-picker',
  template:  `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./high-level-requirement-children-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: REQUIREMENT_TREE_PICKER_CONFIG,
      useFactory: highLevelRequirementChildrenTreePickerDefinition
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, REQUIREMENT_TREE_PICKER_CONFIG, ReferentialDataService]
    }
  ]
})
export class HighLevelRequirementChildrenPickerComponent extends TreeWithStatePersistence implements OnInit, OnDestroy {

  @Input()
  set pickerPersistenceKey(persistenceKey) {
    if (!Boolean(this.persistenceKey)) {
      this.persistenceKey = persistenceKey;
    } else {
      throw Error('Cannot change the persistence key dynamically ' + this.persistenceKey);
    }
  }

  @Input()
  initialSelectedNodes: Identifier[] = [];

  @Output()
  selectedRows = new EventEmitter<DataRow[]>();

  unsub$ = new Subject<void>();

  constructor(public tree: GridService, protected referentialDataService: ReferentialDataService, protected restService: RestService,
              private cdRef: ChangeDetectorRef, protected dialogService: DialogService, protected vcr: ViewContainerRef,
              protected gridPersistenceService: GridPersistenceService, protected router: Router) {
    super(tree, referentialDataService, gridPersistenceService, restService, router, dialogService, vcr, null, 'requirement-tree');
  }

  ngOnInit() {
    this.tree.selectedRows$.pipe(
        takeUntil(this.unsub$)
    ).subscribe((rows) => {
      this.selectedRows.next(rows);
    });

    this.initData({selectedNodes: this.initialSelectedNodes});
    if (Boolean(this.persistenceKey)) {
      this.registerStatePersistence();
    }
  }

  ngOnDestroy(): void {
    if (this.persistenceKey) {
      this.unregisterStatePersistence();
    }
    this.tree.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

}

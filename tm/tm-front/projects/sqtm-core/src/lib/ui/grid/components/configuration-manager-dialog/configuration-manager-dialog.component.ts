import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {GridService} from '../../services/grid.service';

@Component({
  selector: 'sqtm-core-configuration-manager-dialog',
  template: `
      <sqtm-core-customisable-dialog>
          <sqtm-core-dialog-header>
              <h2>CONF MANAGER</h2>
          </sqtm-core-dialog-header>
          <sqtm-core-dialog-body>
              <div class="flex-column full-height">
                  <div class="flex-fixed-size">
                      <div class="flex-row">
                          <nz-tag class="configuration-tag">
                              System
                          </nz-tag>
                          <nz-tag class="configuration-tag">
                              Project
                          </nz-tag>
                          <button class="flex-fixed-size" nz-button nzType="default" style="margin-left: auto">
                              <i nz-icon nzType="plus" nzTheme="outline"></i>
                          </button>
                      </div>
                      <div class="flex-row m-t-10">
                          <nz-tag class="configuration-tag" [nzMode]="'closeable'">
                              USER-CONF 1
                          </nz-tag>
                          <nz-tag class="configuration-tag" [nzMode]="'closeable'">
                              USER-CONF 2
                          </nz-tag>
                          <nz-tag class="configuration-tag" [nzMode]="'closeable'">
                              USER-CONF 3
                          </nz-tag>
                      </div>
                  </div>
                  <div style="overflow-y: auto; flex: auto">
                      <sqtm-core-column-manager></sqtm-core-column-manager>
                  </div>
              </div>
          </sqtm-core-dialog-body>
          <sqtm-core-dialog-footer>
              <div class="flex-row">
                  <button class="flex-fixed-size" nz-button nzType="primary" sqtmCoreDialogClose>SAVE</button>
                  <button class="flex-fixed-size m-l-10" nz-button nzType="default" sqtmCoreDialogClose>CANCEL</button>
              </div>
          </sqtm-core-dialog-footer>
      </sqtm-core-customisable-dialog>
  `,
  styleUrls: ['./configuration-manager-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfigurationManagerDialogComponent implements OnInit {

  constructor(private grid: GridService) {
  }

  ngOnInit() {
  }

  handleClose($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
  }
}

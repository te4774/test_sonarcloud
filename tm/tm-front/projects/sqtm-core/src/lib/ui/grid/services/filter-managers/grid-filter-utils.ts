import {GridFilter} from '../../model/state/filter.state';
import {
  FilterValue,
  FilterValueModel,
  isDateValue,
  isDiscreteValue,
  isMultiBooleanValue,
  isMultiDateValue,
  isMultiNumericValue,
  isMultiStringValue,
  isStringValue
} from '../../../filters/state/filter.state';

export class GridFilterUtils {

  public static mustIncludeFilter(oneFilter: GridFilter) {
    const value = oneFilter.value;
    let include = oneFilter.active && Boolean(value);
    if (isDiscreteValue(value)) {
      include = include && value.value != null && value.value.length > 0;
    }
    if (isMultiStringValue(value)) {
      include = include && value.value != null && value.value.length > 0;
    }
    if (isStringValue(value)) {
      include = include && value.value != null && value.value.length > 0;
    }
    if (isDateValue(value)) {
      include = include && value.value != null && value.value.length > 0 ;
    }
    if (isMultiDateValue(value)) {
      include = include && value.value != null && value.value.length > 0 && Boolean(value.value.find(v => v && v.length > 0));
    }
    if (isMultiNumericValue(value)) {
      include = include && value.value != null && value.value.length > 0;
    }
    if (isMultiBooleanValue(value)) {
      include = include && value.value != null && value.value.length > 0;
    }
    return include;
  }

  public static createRequestFilters(gridFilters: GridFilter[]): FilterValueModel[] {
    // Do not inline this lambda or you'll get compilation errors !
    // see https://github.com/ng-packagr/ng-packagr/issues/696
    const result = gridFilters
      .filter((oneFilter: GridFilter) => GridFilterUtils.mustIncludeFilter(oneFilter))
      .map(oneFilter => ({
        id: oneFilter.id.toString(),
        values: convertFilterValue(oneFilter.value),
        operation: oneFilter.operation.toString(),
        columnPrototype: oneFilter.columnPrototype,
        cufId: oneFilter.cufId,
      }));

    return result;
  }
}

function convertFilterValue(filterValue: FilterValue): string[] {
  if (isStringValue(filterValue)) {
    return [filterValue.value];
  } else if (isMultiStringValue(filterValue)) {
    return filterValue.value;
  } else if (isDiscreteValue(filterValue)) {
    return filterValue.value.map(v => v.id.toString());
  } else if (isDateValue(filterValue)) {
    return [filterValue.value];
  } else if (isMultiDateValue(filterValue)) {
    return filterValue.value;
  } else if (isMultiNumericValue(filterValue)) {
    return filterValue.value.map(v => v.toString());
  }
  throw Error('Unknown value kind ' + filterValue.kind);
}

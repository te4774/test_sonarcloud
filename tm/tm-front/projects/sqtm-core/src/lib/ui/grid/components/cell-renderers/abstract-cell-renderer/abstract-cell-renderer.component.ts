import {ChangeDetectorRef, Directive, Input} from '@angular/core';
import {CellRenderer} from '../../../model/cell-renderer';
import {GridService} from '../../../services/grid.service';
import {ColumnWithFilter} from '../../../model/column-display.model';
import {DataRow} from '../../../model/data-row.model';
import {GridDisplay} from '../../../model/grid-display.model';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractCellRendererComponent implements CellRenderer {

  @Input()
  columnDisplay: ColumnWithFilter;

  @Input()
  row: DataRow;

  @Input()
  depth: number;

  @Input()
  index: number;

  @Input()
  gridDisplay: GridDisplay;

  @Input()
  selected: boolean;

  @Input()
  showAsFilteredParent: boolean;

  @Input()
  isLast: boolean;

  @Input()
  depthMap: boolean[];

  protected constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
  }

}

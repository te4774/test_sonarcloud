import {GridNodeGenerator} from './services/node-generators/grid-node-generator';
import {ClientGridNodeGenerator} from './services/node-generators/client-grid-node-generator';
import {DataRowLoader} from './services/data-row-loaders/data-row-loader';
import {ClientDataRowLoader} from './services/data-row-loaders/client-data-row-loader';
import {ServerDataRowLoader} from './services/data-row-loaders/server-data-row-loader';
import {ServerGridNodeGenerator} from './services/node-generators/server-grid-node-generator';
import {RestService} from '../../core/services/rest.service';
import {StaticColumnDefinitionManager} from './services/column-definition-managers/static-column-definition.manager';
import {FilterManager} from './services/filter-managers/filter-manager';
import {ClientFilterManager} from './services/filter-managers/client-filter-manager';
import {ServerFilterManager} from './services/filter-managers/server-filter-manager';
import {GridDataProvider, GridDefinition} from './model/grid-definition.model';
import {ContextualMenuService} from './services/contextual-menu/contextual-menu.service';
import {GridServiceImpl} from './services/grid-service-impl';
import {ReferentialDataService} from '../../core/referential/services/referential-data.service';
import {
  DynamicColumnDefinitionManagerService
} from './services/column-definition-managers/dynamic-column-definition-manager.service';
import {
  AbstractServerOperationHandler
} from './services/row-server-operation-handlers/abstract-server-operation-handler';
import apply = Reflect.apply;

// FOR ANGULAR 7/ ANGULAR-CLI 7 DO NOT INLINE VARIABLE 'factory' !!! It will make ngc/ngx-packager crash
export function gridNodeGeneratorFactory(config: GridDefinition): () => GridNodeGenerator {
  const factory = () => {
    switch (config.dataProvider) {
      case GridDataProvider.CLIENT:
        return new ClientGridNodeGenerator();
      case GridDataProvider.SERVER:
        return new ServerGridNodeGenerator();
      default:
        throw new Error(`Unable to configure GRID_ROW_DISPLAY_PROVIDER for config : ${JSON.stringify(config)}`);
    }
  };
  return factory;
}

// FOR ANGULAR 7/ ANGULAR-CLI 7 DO NOT INLINE VARIABLE 'factory' !!! It will make ngc/ngx-packager crash
export function dataRowLoaderFactory(config: GridDefinition): (restService: RestService) => DataRowLoader {
  const factory = (restService: RestService) => {
    switch (config.dataProvider) {
      case GridDataProvider.CLIENT:
        return new ClientDataRowLoader(restService);
      case GridDataProvider.SERVER:
        return new ServerDataRowLoader(restService);
      default:
        throw new Error(`Unable to configure DATA_ROW_LOADER for config : ${JSON.stringify(config)}`);
    }
  };
  return factory;
}

// FOR ANGULAR 7/ ANGULAR-CLI 7 DO NOT INLINE VARIABLE 'factory' !!! It will make ngc/ngx-packager crash
export function filterManagerFactory(config: GridDefinition): () => FilterManager {
  const factory = () => {
    switch (config.dataProvider) {
      case GridDataProvider.CLIENT:
        return new ClientFilterManager();
      case GridDataProvider.SERVER:
        return new ServerFilterManager();
      default:
        throw new Error(`Unable to configure GRID_FILTER_MANAGER for config : ${JSON.stringify(config)}`);
    }
  };
  return factory;
}

export function gridServiceFactory(restService: RestService,
                                   gridDefinition: GridDefinition,
                                   referentialDataService: ReferentialDataService,
                                   serverOperationHandler?: AbstractServerOperationHandler,
                                   gridNodeGenerator?: GridNodeGenerator,
                                   dataRowLoader?: DataRowLoader): GridServiceImpl {


  if (!gridNodeGenerator) {
    gridNodeGenerator = apply(gridNodeGeneratorFactory(gridDefinition), this, []);
  }

  if (!dataRowLoader) {
    dataRowLoader = apply(dataRowLoaderFactory(gridDefinition), this, [restService]);
  }

  let columnManager;
  if (gridDefinition.cufColumnsByGlobalFilter) {
    columnManager = new DynamicColumnDefinitionManagerService(referentialDataService);
  } else {
    columnManager = new StaticColumnDefinitionManager();
  }

  const filterManager = apply(filterManagerFactory(gridDefinition), this, []);

  const contextualMenuService = new ContextualMenuService();

  const gridServiceImpl = new GridServiceImpl(
    gridDefinition,
    referentialDataService,
    gridNodeGenerator,
    dataRowLoader,
    columnManager,
    filterManager,
    contextualMenuService
  );

  if (serverOperationHandler) {
    serverOperationHandler.grid = gridServiceImpl;
    gridServiceImpl.serverOperationHandler = serverOperationHandler;
  }

  return gridServiceImpl;
}


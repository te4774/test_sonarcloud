import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {GridDisplay} from '../../../model/grid-display.model';

@Component({
  selector: 'sqtm-core-dummy-cell',
  templateUrl: './dummy-cell.component.html',
  styleUrls: ['./dummy-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DummyCellComponent implements OnInit {

  @Input()
  gridDisplay: GridDisplay;

  constructor() { }

  ngOnInit(): void {
  }

}

import {
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  Injector,
  OnChanges,
  OnInit,
  SimpleChanges,
  Type,
  ViewContainerRef
} from '@angular/core';
import {CellRenderer, GridElementRenderer} from '../model/cell-renderer';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractDynamicGridElement implements OnInit, OnChanges {

  protected componentRef: ComponentRef<CellRenderer>;
  protected changes: SimpleChanges;

  constructor(protected _viewContainerRef: ViewContainerRef, protected componentFactoryResolver: ComponentFactoryResolver,
              protected injector: Injector) {
  }

  ngOnInit(): void {
    this._viewContainerRef.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.getComponentType());
    this.componentRef = this._viewContainerRef.createComponent(componentFactory, this._viewContainerRef.length, this.injector);
    if (this.changes) {
      this.updateComp(this.changes);
      this.changes = null;
    }
  }

  abstract getComponentType(): Type<any>;
  abstract getPrefix(): string;

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(`Changes in cell ${JSON.stringify(changes)}. Compref ${this.componentRef}`);
    this.updateComp(changes);
  }

  private updateComp(changes: SimpleChanges) {
    if (this.componentRef && this.componentRef.instance) {
      Object.entries(changes).forEach(
        ([key, value]) => {
          key = this.getKeyFromInputName(key);
          if (key) {
            // console.log(`Should change pty ${key} with value ${JSON.stringify(value.currentValue)}`);
            // console.log(`update comp`);
            this.componentRef.instance[key] = value.currentValue;
          }
        }
      );
      (this.componentRef.instance as GridElementRenderer).cdRef.detectChanges();
    } else {
      this.changes = changes;
    }

  }

  private getKeyFromInputName(key) {
    key = key.replace(this.getPrefix(), '');
    let char = key.charAt(0);
    char = char.toLowerCase();
    key = key.substring(1);
    key = char + key;
    return key;
  }
}


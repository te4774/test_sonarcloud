import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {GridService} from '../../services/grid.service';

@Component({
  selector: 'sqtm-core-configuration-manager',
  templateUrl: './configuration-manager.component.html',
  styleUrls: ['./configuration-manager.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfigurationManagerComponent implements OnInit {

  constructor(public grid: GridService) {
  }

  ngOnInit() {
  }

}

import {DataRow, DataRowOpenState} from '../../model/data-row.model';
import {dataRowLoaderFactory} from '../../grid.service.provider';
import {ClientDataRowLoader} from './client-data-row-loader';
import {Identifier} from '../../../../model/entity.model';
import {
  getDataRowDataset,
  getDataRowDatasetWithContainersOpened,
  getSubtreesForInsert,
  getTestCaseForInsert,
} from './data-row-dataset.spec';
import {GridState, initialGridState} from '../../model/state/grid.state';
import {switchMap, tap} from 'rxjs/operators';
import {extractRootIds} from './data-row-utils';
import {createEntityAdapter} from '@ngrx/entity';
import {grid} from '../../../../model/grids/grid-builders';
import {convertSqtmLiterals} from '../../../../model/grids/data-row.type';
import {of} from 'rxjs';
import {flatTree} from '../node-generators/client-grid-node-generator';
import {gridNodeStateAdapter} from '../../model/state/grid-nodes.state';
import apply = Reflect.apply;


function getStateWithRowsLoaded() {
  const gridState: GridState = initialGridState();
  gridState.dataRowState = createEntityAdapter<DataRow>().setAll(getDataRowDataset(), gridState.dataRowState);
  gridState.dataRowState.rootRowIds = extractRootIds(getDataRowDataset());
  return gridState;
}

function getStateWithRowsLoadedAndOpened() {
  const gridState: GridState = initialGridState();
  gridState.dataRowState = createEntityAdapter<DataRow>().setAll(getDataRowDatasetWithContainersOpened(), gridState.dataRowState);
  gridState.dataRowState.rootRowIds = extractRootIds(getDataRowDatasetWithContainersOpened());
  return gridState;
}

describe('Client Data Row Loader', () => {

  const gridConfig = grid('grid-test').build();
  const restService = {};
  const dataRowLoader: ClientDataRowLoader = apply(dataRowLoaderFactory(gridConfig), this, [restService]);

  describe('Load Initial Data', () => {

    interface DataType {
      dataRows: DataRow[];
      count: number;
      expectedKeys: string[] | number[];
      expectedValues: DataRow[];
      expectedSize: number;
    }

    const dataRows = getDataRowDataset();
    const keys = getDataRowDataset().map(row => row.id) as any[];

    const dataSet: DataType[] =
      [
        {dataRows, expectedKeys: keys, count: dataRows.length, expectedValues: dataRows, expectedSize: dataRows.length},
        {dataRows, expectedKeys: keys, count: 255, expectedValues: dataRows, expectedSize: dataRows.length},
        {dataRows: [], expectedKeys: [], count: 25, expectedValues: [], expectedSize: 0},
      ];

    dataSet.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`DataSet ${index} - Should load ${data.dataRows.length} rows as pagination subset of ${data.count} total rows as initial data.`,
        function (done) {
          const gridState: GridState = initialGridState();
          gridState.definitionState.literalDataRowConverter = convertSqtmLiterals;
          dataRowLoader.loadInitialData(data.dataRows, data.count, [], gridState, []).pipe(
            tap(nextState => {
              expect(nextState.dataRowState.ids.length).toEqual(data.expectedSize);
              expect(nextState.dataRowState.count).toEqual(data.count);
              expect(nextState.dataRowState.ids).toEqual(data.expectedKeys);
              expect(Object.values(nextState.dataRowState.entities)).toEqual(data.expectedValues);
            })
          ).subscribe(() => done());
        });
    }
  });

  describe('Open row', () => {
    interface DataType {
      id: Identifier;
      stateBefore: DataRowOpenState;
      expectedState: DataRowOpenState;
    }

    const dataSets: DataType[] = [
      {id: 'TestCaseLibrary-1', stateBefore: DataRowOpenState.closed, expectedState: DataRowOpenState.open},
      {id: 'TestCaseLibrary-1', stateBefore: DataRowOpenState.open, expectedState: DataRowOpenState.open},
      {id: 'TestCaseFolder-1', stateBefore: DataRowOpenState.closed, expectedState: DataRowOpenState.open},
      {id: 'TestCaseFolder-1', stateBefore: DataRowOpenState.open, expectedState: DataRowOpenState.open},
      {id: 'TestCase-12', stateBefore: DataRowOpenState.leaf, expectedState: DataRowOpenState.leaf},
    ];

    dataSets.forEach((dataSet, index) => runTest(dataSet, index));

    function runTest(data: DataType, index: number) {
      it(`DataSet ${index} - State should pass from ${data.stateBefore} to ${data.expectedState} for row ${data.id}`, function (done) {
        const gridState: GridState = getStateWithRowsLoaded();
        const dataRow: DataRow = gridState.dataRowState.entities[data.id];
        dataRow.state = data.stateBefore;

        dataRowLoader.openRows([data.id], of(gridState), []).pipe(
          tap(nextState => {
            expect(nextState.dataRowState.entities[data.id].state).toEqual(data.expectedState);
          })
        ).subscribe(() => done());
      });
    }
  });

  describe('Close row', () => {
    interface DataType {
      id: Identifier;
      stateBefore: DataRowOpenState;
      expectedState: DataRowOpenState;
    }

    const dataSets: DataType[] = [
      {id: 'TestCaseLibrary-1', stateBefore: DataRowOpenState.open, expectedState: DataRowOpenState.closed},
      {id: 'TestCaseLibrary-1', stateBefore: DataRowOpenState.closed, expectedState: DataRowOpenState.closed},
      {id: 'TestCaseFolder-1', stateBefore: DataRowOpenState.open, expectedState: DataRowOpenState.closed},
      {id: 'TestCaseFolder-1', stateBefore: DataRowOpenState.closed, expectedState: DataRowOpenState.closed},
      {id: 'TestCase-12', stateBefore: DataRowOpenState.leaf, expectedState: DataRowOpenState.leaf},
    ];

    dataSets.forEach((dataSet, index) => runTest(dataSet, index));

    function runTest(data: DataType, index: number) {
      it(`DataSet ${index} - State should pass from ${data.stateBefore} to ${data.expectedState} for row ${data.id}`, function (done) {
        const gridState: GridState = getStateWithRowsLoaded();
        const dataRow: DataRow = gridState.dataRowState.entities[data.id];
        dataRow.state = data.stateBefore;

        dataRowLoader.closeRows([data.id], gridState).pipe(
          tap(nextState => {
            expect(nextState.dataRowState.entities[data.id].state).toEqual(data.expectedState);
          })
        ).subscribe(() => done());
      });
    }
  });

  describe('Select rows', () => {
    interface DataType {
      id: Identifier;
      selectedRowsBefore: Identifier[];
      selectedRowsAfter: Identifier[];
    }

    const dataSet: DataType[] = [
      {id: 'TestCaseLibrary-1', selectedRowsBefore: [], selectedRowsAfter: ['TestCaseLibrary-1']},
      {
        id: 'TestCase-4',
        selectedRowsBefore: ['TestCaseLibrary-2'],
        selectedRowsAfter: ['TestCaseLibrary-2', 'TestCase-4']
      },
      {id: 'TestCase-4', selectedRowsBefore: ['TestCase-12'], selectedRowsAfter: ['TestCase-12', 'TestCase-4']},
      {id: 'TestCase-4', selectedRowsBefore: ['TestCaseLibrary-1'], selectedRowsAfter: ['TestCaseLibrary-1']},
      {
        id: 'TestCaseLibrary-1',
        selectedRowsBefore: ['TestCase-4', 'TestCaseLibrary-2'],
        selectedRowsAfter: ['TestCaseLibrary-2', 'TestCaseLibrary-1']
      },
      {
        id: 'TestCaseFolder-1',
        selectedRowsBefore: ['TestCase-4', 'TestCaseLibrary-2', 'TestCase-12'],
        selectedRowsAfter: ['TestCaseLibrary-2', 'TestCase-12', 'TestCaseFolder-1']
      },
    ];

    dataSet.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`DataSet ${index} - When selecting ${data.id} with previous selection: ${JSON.stringify(data.selectedRowsBefore)},
       final selection should be ${JSON.stringify(data.selectedRowsAfter)}`,
        function (done) {

          const gridState: GridState = getStateWithRowsLoaded();
          gridState.dataRowState.selectedRows = data.selectedRowsBefore;

          dataRowLoader.toggleRowSelection(data.id, gridState).pipe(
            tap(nextState => {
              expect(nextState.dataRowState.selectedRows).toEqual(data.selectedRowsAfter);
            }),
            switchMap(nextState => dataRowLoader.toggleRowSelection(data.id, nextState)),
            tap(nextState => {
              expect(nextState.dataRowState.selectedRows.includes(data.id)).toBeFalsy();
            }),
          ).subscribe(() => done());
        });
    }
  });

  describe('Add to row selection', () => {
    interface DataType {
      id: Identifier;
      selectedRowsBefore: Identifier[];
      selectedRowsAfter: Identifier[];
      lastToggledRowId: Identifier;
    }

    const dataSet: DataType[] = [
      {
        id: 'TestCaseLibrary-1',
        selectedRowsBefore: [],
        selectedRowsAfter: ['TestCaseLibrary-1'],
        lastToggledRowId: null
      },
      {
        id: 'TestCaseLibrary-1',
        selectedRowsBefore: ['TestCaseLibrary-1'],
        selectedRowsAfter: [],
        lastToggledRowId: null
      },
      {
        id: 'TestCaseLibrary-2',
        selectedRowsBefore: ['TestCaseLibrary-1'],
        selectedRowsAfter: ['TestCaseLibrary-1', 'TestCaseLibrary-2'],
        lastToggledRowId: null
      },
      {
        id: 'TestCaseLibrary-2',
        selectedRowsBefore: ['TestCaseLibrary-1', 'TestCaseLibrary-2'],
        selectedRowsAfter: ['TestCaseLibrary-1'],
        lastToggledRowId: null
      },
      {
        id: 'TestCaseFolder-6',
        selectedRowsBefore: ['TestCaseFolder-3'],
        selectedRowsAfter: ['TestCaseFolder-3', 'TestCase-4', 'TestCase-5', 'TestCaseFolder-6'],
        lastToggledRowId: null
      },
      {
        id: 'TestCaseFolder-3',
        selectedRowsBefore: ['TestCase-5'],
        selectedRowsAfter: ['TestCase-5', 'TestCaseFolder-3', 'TestCase-4'],
        lastToggledRowId: null
      },
      {
        id: 'TestCaseFolder-3',
        selectedRowsBefore: ['TestCaseFolder-3', 'TestCase-4', 'TestCase-5', 'TestCaseFolder-6'],
        selectedRowsAfter: ['TestCaseFolder-6'],
        lastToggledRowId: 'TestCase-5'
      },
      {
        id: 'TestCase-4',
        selectedRowsBefore: ['TestCaseFolder-3', 'TestCase-4', 'TestCase-5', 'TestCaseFolder-6'],
        selectedRowsAfter: ['TestCaseFolder-3', 'TestCaseFolder-6'],
        lastToggledRowId: 'TestCase-5'
      },
      {
        id: 'TestCaseFolder-6',
        selectedRowsBefore: ['TestCaseFolder-3', 'TestCase-5', 'TestCaseFolder-6'],
        selectedRowsAfter: ['TestCaseFolder-3'],
        lastToggledRowId: 'TestCase-4'
      },
      {
        id: 'TestCase-9',
        selectedRowsBefore: ['TestCaseFolder-3', 'TestCase-4', 'TestCase-5', 'TestCaseFolder-6'],
        selectedRowsAfter: ['TestCase-9'],
        lastToggledRowId: 'TestCase-5'
      },
      {
        id: 'TestCaseLibrary-1',
        selectedRowsBefore: ['TestCase-12'],
        selectedRowsAfter: ['TestCaseLibrary-1'],
        lastToggledRowId: null
      },
      {
        id: 'TestCaseLibrary-1',
        selectedRowsBefore: ['TestCase-12', 'TestCaseLibrary-2'],
        selectedRowsAfter: ['TestCaseLibrary-2', 'TestCaseLibrary-1'],
        lastToggledRowId: null
      },
    ];

    dataSet.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`DataSet ${index} - When adding ${data.id} to previous selection: ${JSON.stringify(data.selectedRowsBefore)},
       final selection should be ${JSON.stringify(data.selectedRowsAfter)}`,
        function (done) {

          const gridState: GridState = getStateWithRowsLoadedAndOpened();
          gridState.dataRowState.selectedRows = data.selectedRowsBefore;
          const tree = flatTree(gridState.dataRowState, gridState.columnState, gridState.filterState, gridState.uiState);
          gridState.nodesState = gridNodeStateAdapter.setAll(tree, gridState.nodesState);
          gridState.dataRowState.lastToggledRowId = data.lastToggledRowId;

          dataRowLoader.addToSelection(data.id, gridState).pipe(
            tap(nextState => {
              expect(nextState.dataRowState.selectedRows).toEqual(data.selectedRowsAfter);
              expect(nextState.dataRowState.lastToggledRowId).toEqual(data.id);
            })
          ).subscribe(() => done());
        });
    }
  });

  describe('Delete leaf row', function () {
    interface DataType {
      ids: Identifier[];
      expectedParents: { id: Identifier, children: Identifier[] }[];
    }

    const dataSet: DataType[] = [
      {ids: ['TestCase-8'], expectedParents: [{id: 'TestCaseFolder-3', children: ['TestCaseFolder-7']}]},
      {
        ids: ['TestCase-12'],
        expectedParents: [{id: 'TestCaseLibrary-1', children: ['TestCaseFolder-1', 'TestCaseFolder-2']}]
      },
      {
        ids: ['TestCase-4', 'TestCase-5'],
        expectedParents: [
          {id: 'TestCaseFolder-1', children: ['TestCaseFolder-3', 'TestCaseFolder-6']}
        ]
      },
      {
        ids: ['TestCase-12', 'TestCase-4', 'TestCase-5'],
        expectedParents: [
          {id: 'TestCaseFolder-1', children: ['TestCaseFolder-3', 'TestCaseFolder-6']},
          {id: 'TestCaseLibrary-1', children: ['TestCaseFolder-1', 'TestCaseFolder-2']}
        ]
      },
    ];

    dataSet.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`DataSet ${index} - Should delete ${JSON.stringify(data.ids)}`, function (done) {
        const gridState: GridState = getStateWithRowsLoaded();
        dataRowLoader.removeRows(data.ids, gridState).pipe(
          tap(removeResult => {
            const entities = removeResult.state.dataRowState.entities;
            data.ids.forEach(id => expect(entities[id]).toBeFalsy());
            data.expectedParents.forEach(
              expectedParent => expect(entities[expectedParent.id].children).toEqual(expectedParent.children));
            const expectedParentKeys: Identifier[] = data.expectedParents.map(expectedParent => expectedParent.id);
            const ids = getStateWithRowsLoaded().dataRowState.ids as any;
            const notTouchedKeys = ids
              .filter(key => !data.ids.includes(key))
              .filter(key => !expectedParentKeys.includes(key));
            notTouchedKeys.forEach(key => expect({...entities[key]}).toEqual({...getDataRowDataset().find(row => row.id === key)}));
          }),
        ).subscribe(() => done());
      });
    }
  });

  describe('Delete parent rows and their children', function () {
    interface DataType {
      ids: Identifier[];
      expectedDeletedRows: Identifier[];
      expectedParents: {
        id: Identifier;
        children
          : Identifier[];
      }[];
    }

    const dataSet: DataType[] = [
      {
        ids: ['TestCaseFolder-7'],
        expectedDeletedRows: ['TestCaseFolder-7', 'TestCase-9'],
        expectedParents: [{
          id: 'TestCaseFolder-3',
          children: ['TestCase-8']
        }]
      },
      {
        ids: ['TestCaseFolder-3', 'TestCaseFolder-6'],
        expectedDeletedRows: ['TestCaseFolder-3', 'TestCaseFolder-6', 'TestCaseFolder-7', 'TestCase-8', 'TestCase-9'],
        expectedParents: [{
          id: 'TestCaseFolder-1',
          children: ['TestCase-4', 'TestCase-5']
        }]
      },
      {
        ids: ['TestCaseFolder-1', 'TestCase-10'],
        expectedDeletedRows: ['TestCaseFolder-1', 'TestCase-10', 'TestCaseFolder-3', 'TestCaseFolder-7', 'TestCase-9', 'TestCase-8',
          'TestCase-4', 'TestCase-5', 'TestCaseFolder-6'
        ],
        expectedParents: [
          {
            id: 'TestCaseLibrary-1',
            children: ['TestCase-12', 'TestCaseFolder-2']
          },
          {
            id: 'TestCaseLibrary-2',
            children: ['TestCaseFolder-11']
          }
        ]
      },
    ];

    dataSet.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`DataSet ${index} - Should delete ${JSON.stringify(data.expectedDeletedRows)} when deleting ${JSON.stringify(data.ids)}`,
        function (done) {
          const gridState = getStateWithRowsLoaded();
          dataRowLoader.removeRows(data.ids, gridState).pipe(
            tap(removeResult => {
              const entities = removeResult.state.dataRowState.entities;
              data.ids.forEach(id => expect(entities[id]).toBeFalsy());
              data.expectedParents.forEach(
                expectedParent => expect(entities[expectedParent.id].children).toEqual(expectedParent.children));
              const expectedParentKeys: Identifier[] = data.expectedParents.map(expectedParent => expectedParent.id);
              const ids = gridState.dataRowState.ids as any[];
              const notTouchedKeys = ids
                .filter(key => !data.expectedDeletedRows.includes(key))
                .filter(key => !expectedParentKeys.includes(key));
              notTouchedKeys.forEach(key => {
                expect({...entities[key]}).toEqual({...getDataRowDataset().find(row => row.id === key)});
              });
              const expectedSubTree = Object.values(entities).filter(row => data.expectedDeletedRows.includes(row.id));
              const originalRowsMap = getStateWithRowsLoaded().dataRowState.entities;
              data.ids.forEach(id => originalRowsMap[id].parentRowId = undefined);
              expectedSubTree.forEach(row => expect(originalRowsMap[row.id]).toEqual(row));
            }),
          ).subscribe(() => done());
        });
    }
  });

  describe('Insert child rows at various indexes', function () {

    interface DataType {
      id: Identifier;
      insertedRows: DataRow[];
      index: number;
      expectedChildren: Identifier[];
    }

    const dataSet: DataType[] = [
      {
        id: 'TestCaseLibrary-1',
        insertedRows: getTestCaseForInsert(),
        index: 1,
        expectedChildren: ['TestCase-12', 'TestCase-13', 'TestCase-14', 'TestCaseFolder-1', 'TestCaseFolder-2']
      },
      {
        id: 'TestCaseLibrary-1',
        insertedRows: getTestCaseForInsert(),
        index: 0,
        expectedChildren: ['TestCase-13', 'TestCase-14', 'TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2']
      },
      {
        id: 'TestCaseLibrary-1',
        insertedRows: getTestCaseForInsert(),
        index: 3,
        expectedChildren: ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2', 'TestCase-13', 'TestCase-14']
      },
      {
        id: 'TestCaseLibrary-1',
        insertedRows: getTestCaseForInsert(),
        index: undefined,
        expectedChildren: ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2', 'TestCase-13', 'TestCase-14']
      },
      {
        id: 'TestCaseFolder-1',
        insertedRows: getTestCaseForInsert(),
        index: 1,
        expectedChildren: ['TestCaseFolder-3', 'TestCase-13', 'TestCase-14', 'TestCase-4', 'TestCase-5', 'TestCaseFolder-6']
      }
    ];

    dataSet.forEach((data, index) => runTest(data, index));

    function runTest(data, index: number) {
      it(`DataSet ${index} - Should insert ${data.insertedRows.length} rows into ${data.id} at index ${data.index}.
      Children should be :${JSON.stringify(data.expectedChildren)}`, function (done) {
        const gridState = getStateWithRowsLoaded();

        dataRowLoader.insertRows(data.insertedRows, data.id, gridState, data.index).pipe(
          tap(nextState => {
            const entities = nextState.dataRowState.entities;
            expect(nextState.dataRowState.ids.length).toEqual(getStateWithRowsLoaded().dataRowState.ids.length + data.insertedRows.length);
            data.insertedRows.forEach(row => expect(entities[row.id]).toBeTruthy());
            expect(entities[data.id].children).toEqual(data.expectedChildren);
            expect(nextState.dataRowState.rootRowIds).toEqual(getStateWithRowsLoaded().dataRowState.rootRowIds);
          })
        ).subscribe(() => done());
      });
    }
  });

  /**
   * The method insert rows is not responsible for checking the legal aspect of insertion
   * So in test, we can do funny thing like inserting root test cases...
   */
  describe('Insert root rows at various indexes', function () {

    interface DataType {
      id: Identifier;
      insertedRows: DataRow[];
      index: number;
      expectedRootRowIds: Identifier[];
    }

    const dataSet: DataType[] = [
      {
        id: undefined,
        insertedRows: getTestCaseForInsert(),
        index: 1,
        expectedRootRowIds: ['TestCaseLibrary-1', 'TestCase-13', 'TestCase-14', 'TestCaseLibrary-2']
      },
      {
        id: undefined,
        insertedRows: getTestCaseForInsert(),
        index: 0,
        expectedRootRowIds: ['TestCase-13', 'TestCase-14', 'TestCaseLibrary-1', 'TestCaseLibrary-2']
      },
      {
        id: undefined,
        insertedRows: getTestCaseForInsert(),
        index: 2,
        expectedRootRowIds: ['TestCaseLibrary-1', 'TestCaseLibrary-2', 'TestCase-13', 'TestCase-14']
      },
      {
        id: undefined,
        insertedRows: getTestCaseForInsert(),
        index: 3,
        expectedRootRowIds: ['TestCaseLibrary-1', 'TestCaseLibrary-2', 'TestCase-13', 'TestCase-14']
      },
      {
        id: undefined,
        insertedRows: getTestCaseForInsert(),
        index: undefined,
        expectedRootRowIds: ['TestCaseLibrary-1', 'TestCaseLibrary-2', 'TestCase-13', 'TestCase-14']
      },

    ];

    dataSet.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`DataSet ${index} - Should insert ${data.insertedRows.length} rows in root at index ${data.index}.
      Root rows should be :${JSON.stringify(data.expectedRootRowIds)}`, function (done) {
        const gridState = getStateWithRowsLoaded();
        dataRowLoader.insertRows(data.insertedRows, data.id, gridState, data.index).pipe(
          tap(nextState => {
            const entities = nextState.dataRowState.entities;
            expect(nextState.dataRowState.ids.length).toEqual(getDataRowDataset().length + data.insertedRows.length);
            data.insertedRows.forEach(row => expect(entities[row.id]).toBeTruthy());
            expect(nextState.dataRowState.rootRowIds).toEqual(data.expectedRootRowIds);
          })
        ).subscribe(() => done());
      });
    }

  });

  describe('Insert Sub Trees', function () {
    interface DataType {
      id: Identifier;
      insertedRows: DataRow[];
      index: number;
      expectedChildren: Identifier[];
    }

    const dataSet: DataType[] = [
      {
        id: 'TestCaseLibrary-1',
        insertedRows: getSubtreesForInsert(),
        index: 1,
        expectedChildren: ['TestCase-12', 'TestCaseFolder-20', 'TestCaseFolder-40', 'TestCaseFolder-1', 'TestCaseFolder-2']
      }, {
        id: 'TestCaseLibrary-1',
        insertedRows: getSubtreesForInsert(),
        index: 0,
        expectedChildren: ['TestCaseFolder-20', 'TestCaseFolder-40', 'TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2']
      }, {
        id: 'TestCaseLibrary-1',
        insertedRows: getSubtreesForInsert(),
        index: 3,
        expectedChildren: ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2', 'TestCaseFolder-20', 'TestCaseFolder-40']
      }, {
        id: 'TestCaseLibrary-1',
        insertedRows: getSubtreesForInsert(),
        index: undefined,
        expectedChildren: ['TestCase-12', 'TestCaseFolder-1', 'TestCaseFolder-2', 'TestCaseFolder-20', 'TestCaseFolder-40']
      }, {
        id: 'TestCaseFolder-1',
        insertedRows: getSubtreesForInsert(),
        index: 1,
        expectedChildren: ['TestCaseFolder-3', 'TestCaseFolder-20', 'TestCaseFolder-40', 'TestCase-4', 'TestCase-5', 'TestCaseFolder-6']
      }];

    dataSet.forEach((data, index) => runTests(data, index));

    function runTests(data: DataType, index: number) {
      it(`DataSet ${index} - Should insert subtrees in ${data.id} at index ${data.index}`, function (done) {
        const gridState = getStateWithRowsLoaded();
        dataRowLoader.insertRows(data.insertedRows, data.id, gridState, data.index).pipe(
          tap(nextState => {
            const entities = nextState.dataRowState.entities;
            expect(nextState.dataRowState.ids.length).toEqual(getDataRowDataset().length + data.insertedRows.length);
            // checking that all root rows have been modified at insertion, just for their parent
            data.insertedRows.filter(row => !Boolean(row.parentRowId)).forEach(row => {
              const dataRow = entities.get[row.id];
              expect(dataRow.children).toEqual(row.children);
              expect(dataRow.data).toEqual(row.data);
              expect(dataRow.state).toEqual(row.state);
              expect(dataRow.parentRowId).toEqual(data.id);
            });
            // checking that all non root rows have not been modified at insertion
            data.insertedRows.filter(row => Boolean(row.parentRowId)).forEach(row => {
              expect(entities[row.id]).toEqual(row);
            });
            expect(entities[data.id].children).toEqual(data.expectedChildren);
            expect(nextState.dataRowState.rootRowIds).toEqual(getStateWithRowsLoaded().dataRowState.rootRowIds);
          })
        ).subscribe(() => done());
      });
    }

  });
});


import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent} from '../abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../../services/grid.service';

@Component({
  selector: 'sqtm-core-rich-text-renderer',
  template: `
    <ng-container *ngIf="columnDisplay">
      <div class="flex">
        <span style="margin: auto 0;"
              class="txt-ellipsis"
              nz-tooltip
              [sqtmCoreLabelTooltip]="convertHtmlToPlainText(row.data[columnDisplay.id])">
          {{convertHtmlToPlainText(row.data[columnDisplay.id])}}
        </span>
      </div>
    </ng-container>`,
  styleUrls: ['./rich-text-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RichTextRendererComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  ngOnInit() {
  }

  convertHtmlToPlainText(html: string) {
    const tempDivElement = document.createElement('div');
    tempDivElement.innerHTML = html;
    return tempDivElement.textContent || tempDivElement.innerText || '';
  }
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {SelectFiltersComponent} from './select-filters.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {GridService} from '../../services/grid.service';

describe('SelectFiltersComponent', () => {
  let component: SelectFiltersComponent;
  let fixture: ComponentFixture<SelectFiltersComponent>;

  const gridServiceMock = jasmine.createSpyObj(['loadInitialData']);
  gridServiceMock.gridState$ = EMPTY;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SelectFiltersComponent],
      providers: [{
        provide: GridService,
        useValue: gridServiceMock
      }],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

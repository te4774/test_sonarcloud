import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {GridService} from '../../services/grid.service';
import {PaginationDisplay} from '../../model/pagination-display.model';

@Component({
  selector: 'sqtm-core-grid-footer',
  templateUrl: './grid-footer.component.html',
  styleUrls: ['./grid-footer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridFooterComponent implements OnInit {

  @Input()
  disabled = false;

  constructor(public grid: GridService) {
  }

  ngOnInit() {

  }

  handlePrevious() {
    this.grid.paginatePreviousPage();
  }

  handleNext() {
    this.grid.paginateNextPage();
  }

  handlePaginationSizeChange(size: number) {
    this.grid.changePaginationSize(size);
  }

  handleGoFirstPage() {
    this.grid.goToFirstPage();
  }

  handleGoLastPage() {
    this.grid.goToLastPage();
  }

  getInferiorBoundary(pagination: PaginationDisplay) {
    return pagination.page * pagination.size + 1;
  }

  getSuperiorBoundary(pagination: PaginationDisplay) {
    return Math.min(pagination.size * (pagination.page + 1), pagination.count);
  }
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {GridService} from '../../../services/grid.service';
import {AbstractCellRendererComponent} from '../abstract-cell-renderer/abstract-cell-renderer.component';

@Component({
  selector: 'sqtm-core-check-box-cell-renderer',
  template: `
    <ng-container *ngIf="columnDisplay">
      <div class="sqtm-grid-cell-txt-renderer full-height flex-column">
        <ng-container *ngIf="row">
          <label nz-checkbox style="margin: auto" [nzChecked]="selected" (nzCheckedChange)="toggle()"></label>
        </ng-container>
      </div>
    </ng-container>
  `,
  styleUrls: ['./check-box-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckBoxCellRendererComponent extends AbstractCellRendererComponent implements OnInit, OnChanges {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  ngOnInit() {
  }

  /**
   * The tricks here is to prevent the dom to naturally toggle the box we return false in the handler
   * Witch is equivalent to prevent default. So the state of the box will be governed by the service as required by our
   * complex ancestor/child selection behavior
   */
  toggle() {
    setTimeout(() => {
      // by doing the action, we allow the native event handler to complete, and the state update is not lost.
      this.grid.toggleRowSelection(this.row.id);
    });
    // Returning false, so the checkbox state in the DOM not modified.
    // It will be modified by the grid service
    return false;
  }

  ngOnChanges(changes: SimpleChanges): void {
  }
}

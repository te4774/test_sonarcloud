import {Observable} from 'rxjs';
import {GridService} from '../grid.service';
import {Identifier} from '../../../../model/entity.model';
import {GridState} from '../../model/state/grid.state';

/**
 * This class represent a strategy for handling special server side operation in grid. GridService delegate some operation to this strategy.
 * When doing operation in grids, some operation can be handled from external components (adding rows...)
 * However some operations are best handled directly in grid (internal drag and drop...)
 */
export abstract class AbstractServerOperationHandler {
  canCopy$: Observable<boolean>;
  canPaste$: Observable<boolean>;
  canDelete$: Observable<boolean>;
  canCreate$: Observable<boolean>;
  canExport$: Observable<boolean>;
  canDrag$: Observable<boolean>;

  abstract grid: GridService;

  abstract copy(): void;

  abstract paste(): void;

  abstract delete(): void;

  abstract notifyInternalDrop(): void;

  abstract allowDropSibling(id: Identifier, state: GridState): boolean;

  abstract allowDropInto(id: Identifier, state: GridState): boolean;
}

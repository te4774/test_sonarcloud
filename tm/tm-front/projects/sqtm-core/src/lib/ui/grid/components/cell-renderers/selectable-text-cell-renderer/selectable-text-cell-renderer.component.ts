import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {GridService} from '../../../services/grid.service';
import {AbstractCellRendererComponent} from '../abstract-cell-renderer/abstract-cell-renderer.component';
import {ColumnDefinitionBuilder} from '../../../model/column-definition.builder';


@Component({
  selector: 'sqtm-core-selectable-text-cell-renderer',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column" [sqtmCoreSelectable]="row.id">
        <sqtm-core-text-cell-renderer
          [columnDisplay]="columnDisplay"
          [row]="row"
          [depth]="depth"
          [index]="index"
          [gridDisplay]="gridDisplay"
          [selected]="selected"
          [showAsFilteredParent]="showAsFilteredParent">
        </sqtm-core-text-cell-renderer>
      </div>
    </ng-container>`,
  styleUrls: ['./selectable-text-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectableTextCellRendererComponent  extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }
}

export function selectableTextColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(SelectableTextCellRendererComponent);
}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import {GridService} from '../../../services/grid.service';
import {AbstractCellRendererComponent} from '../abstract-cell-renderer/abstract-cell-renderer.component';
import {Subject, timer} from 'rxjs';
import {filter, takeUntil, tap} from 'rxjs/operators';
import {TableValueChange} from '../../../model/actions/table-value-change';
import {LevelEnumItem} from '../../../../../model/level-enums/level-enum';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-core-level-enum-cell-renderer',
  template: `
    <div *ngIf="columnDisplay" class="{{initHover()}} sqtm-grid-cell-txt-renderer full-height flex-column">
      <ng-container *ngIf="row && !edit">
        <span (click)="activateEditMode($event)">{{getTranslatedRowValue(row.data[columnDisplay.id])}}</span>
      </ng-container>
      <ng-container *ngIf="row && edit">
        <select #input *ngIf="row.data[columnDisplay.id] as enumId" (change)="changeValue($event)" (blur)="disableEditMode()">
          <option [value]="value.id" *ngFor="let value of getValues()" [selected]="enumId === value.id">
            {{value.i18nKey | translate}}
          </option>
        </select>
      </ng-container>
    </div>
  `,
  styleUrls: ['./level-enum-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LevelEnumCellRendererComponent extends AbstractCellRendererComponent implements OnInit {

  edit = false;

  @Input()
  excludedKeys: any[] = [];

  @ViewChild('input')
  input: ElementRef;

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef, private translate: TranslateService) {
    super(grid, cdRef);
  }

  activateEditMode($event: MouseEvent) {
    if (!this.edit && this.columnDisplay.editable) {
      this.edit = true;
      // kind of pulling for waiting that input is displayed and active.
      const unsub = new Subject<void>();
      timer(1, 10).pipe(
        takeUntil(unsub),
        filter(() => Boolean(this.input)),
        tap(() => this.input.nativeElement.focus()),
        tap(() => {
          const input = this.input.nativeElement as HTMLInputElement;
          input.value = this.row.data[this.columnDisplay.id];
        }),
        // auto unsub, so we don't have wild timers that run into app after an edition
        tap(() => {
          unsub.next();
          unsub.complete();
        })
      ).subscribe();
    }
    return false;
  }

  disableEditMode() {
    if (this.edit) {
      this.edit = false;
    }
  }

  ngOnInit() {
  }

  changeValue($event: Event) {
    const inputElement = this.input.nativeElement as HTMLInputElement;
    const changedValue: TableValueChange = {columnId: this.columnDisplay.id, value: inputElement.value};
    this.disableEditMode();
    this.grid.editRows([this.row.id], [changedValue]);
    return false;
  }

  getValues(): LevelEnumItem<any>[] {
    return Object.values(this.columnDisplay.levelEnum).filter(value => !this.excludedKeys.includes(value));
  }

  initHover() {
    return this.columnDisplay.editable ? '__hover_pointer' : '';
  }

  getTranslatedRowValue(rowValue: string) {
    const levelEnum = this.columnDisplay.levelEnum;
    for (const levelEnumKey in levelEnum) {
      if (rowValue === levelEnumKey) {
        return this.translate.instant(levelEnum[levelEnumKey].i18nKey);
      }
    }
    return rowValue;
  }
}

import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import {grid} from '../../../../../model/grids/grid-builders';
import {Fixed, Limited} from '../../../model/column-definition.model';
import {
  ToggleSelectionHeaderRendererComponent
} from '../../header-renderers/toggle-selection-header-renderer/toggle-selection-header-renderer.component';
import {GridDefinition} from '../../../model/grid-definition.model';
import {GridService} from '../../../services/grid.service';
import {gridServiceFactory} from '../../../grid.service.provider';
import {Project} from '../../../../../model/project/project.model';
import {GridFilter} from '../../../model/state/filter.state';
import {combineLatest, Subject} from 'rxjs';
import {map, take, tap} from 'rxjs/operators';
import {PaginationConfigBuilder} from '../../../model/grid-definition.builder';
import {ReferentialDataService} from '../../../../../core/referential/services/referential-data.service';
import {RestService} from '../../../../../core/services/rest.service';
import {Identifier} from '../../../../../model/entity.model';
import {selectRowColumn, textColumn} from '../../../model/common-column-definition.builders';
import {Overlay, OverlayConfig} from '@angular/cdk/overlay';
import {ConnectedPosition} from '@angular/cdk/overlay/position/flexible-connected-position-strategy';
import {ProjectPickerLayout} from './project-picker-layout';

export function projectPickerGridConfigFactory() {
  return grid('project-picker')
    .withColumns([
      selectRowColumn()
        .changeWidthCalculationStrategy(new Fixed(40))
        .withHeaderRenderer(ToggleSelectionHeaderRendererComponent),
      textColumn('name').withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Limited(260)),
      textColumn('label').withI18nKey('sqtm-core.entity.project.label.tag')
        .changeWidthCalculationStrategy(new Limited(240)),
    ])
    .disableRightToolBar()
    .withPagination(
      new PaginationConfigBuilder()
        .fixPaginationSize(-1)
    ).enableMultipleColumnsFiltering(['name', 'label'])
    .build();
}


@Component({
  selector: 'sqtm-core-project-picker',
  templateUrl: './project-picker.component.html',
  styleUrls: ['./project-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridDefinition,
      useFactory: projectPickerGridConfigFactory,
      deps: []
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, GridDefinition, ReferentialDataService]
    }
  ]
})
export class ProjectPickerComponent implements OnInit, OnDestroy {

  @Input()
  projects: Project[] = [];

  @Input()
  initiallySelectedProjects: number[] = [];

  /**
   * If true :
   * - The grid will be loaded with all projects available in referential data
   * - The selected rows will initially be equal to the project selected in the global filter
   */
  @Input()
  provideWithReferentialData = false;

  @Output()
  selectedProjects = new EventEmitter<Project[]>();

  @Output()
  cancelRequired = new EventEmitter<void>();

  private unsub$ = new Subject<void>();

  constructor(private gridService: GridService, private referentialDataService: ReferentialDataService) {
  }

  ngOnInit(): void {
    this.initializeGrid();
  }

  private initializeGrid() {
    if (this.provideWithReferentialData) {
      combineLatest([this.referentialDataService.projects$, this.referentialDataService.filteredProjectIds$]).pipe(
        take(1),
        map(([projects, ids]) => this.sortProjects(projects, ids)),
        tap(({projects, ids}) => this.gridService.loadInitialData(projects, projects.length, 'id', ids))
      ).subscribe(() => {
        this.gridService.addFilters(this.buildFilters());
      });
    } else {
      this.sortProjects(this.projects, this.initiallySelectedProjects);
      this.gridService.loadInitialData(this.projects, this.projects.length, 'id', this.initiallySelectedProjects);
      this.gridService.addFilters(this.buildFilters());
    }

  }

  private buildFilters(): GridFilter[] {
    return [{
      id: 'name', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false,
    }, {
      id: 'label', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false,
    }];
  }

  handleKeyboardInput($event: string) {
    this.gridService.applyMultiColumnsFilter($event);
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  confirm() {
    this.gridService.selectedRows$.pipe(
      take(1)
    ).subscribe(rows => this.selectedProjects.emit(rows.map(r => r.data as Project)));
  }

  cancel() {
    this.cancelRequired.emit();
  }

  static getOverlayConfig(layout: ProjectPickerLayout, scope: ElementRef, overlay: Overlay): OverlayConfig {
    const positionStrategy = overlay.position().flexibleConnectedTo(scope)
      .withPositions(this.getOverlayPositions(layout));
    return {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      width: 600,
      height: 800
    };
  }

  private static getOverlayPositions(layout: ProjectPickerLayout): ConnectedPosition[] {
    switch (layout) {
      case ProjectPickerLayout.CHART :
        return [
          {originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'top', offsetX: 20},
          {originX: 'start', overlayX: 'start', originY: 'center', overlayY: 'top'},
        ];
      case ProjectPickerLayout.KEYWORDSTEP :
        return [
          {originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'center', offsetX: 10, offsetY: 20}
        ];
    }
  }

  private sortProjects(projects: Project[], ids: Identifier[]) {
    projects.sort((firstRow, secondRow) => {
      if (ids.includes(firstRow.id)) {
        if (ids.includes(secondRow.id)) {
          return firstRow.name.localeCompare(secondRow.name);
        } else {
          return -1;
        }
      } else {
        if (ids.includes(secondRow.id)) {
          return 1;
        } else {
          return firstRow.name.localeCompare(secondRow.name);
        }
      }
    });
    return {projects, ids};
  }
}

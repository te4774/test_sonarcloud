import {ColumnDefinitionBuilder} from './column-definition.builder';
import {GridDataProvider, GridDefinition, GridType, PaginationConfig} from './grid-definition.model';
import {
  CustomScopeKind,
  GridScopeDefinition,
  GridStyleDefinition,
  LiteralDataRowConverter
} from './state/definition.state';
import {DataRow, GenericDataRow} from './data-row.model';
import {Type} from '@angular/core';
import {BindableEntity} from '../../../model/bindable-entity.model';
import {SortedColumn} from './column-definition.model';
import {DraggedContentRenderer} from '../../drag-and-drop/dragged-content-renderer';
import {
  DefaultTreeDraggedContentComponent
} from '../components/dragged-content/default-tree-dragged-content/default-tree-dragged-content.component';

export function pagination() {
  return new PaginationConfigBuilder();
}

export class GridDefinitionBuilder {
  private rowHeight = 25;
  private dataProvider: GridDataProvider = GridDataProvider.CLIENT;
  private serverRootUrl ?: string [];
  private columnDefinitionBuilders: ColumnDefinitionBuilder[] = [];
  private paginationBuilder: PaginationConfigBuilder;
  private allowShowAll = false;
  private enableHeaders = true;
  private enableRightToolBar = true;
  private enableMultiRowSelection = true;
  private enableOpenRowOnDoubleClick = false;
  private _enableDrag = false;
  private _enableInternalDrop = false;
  private _draggedContentRenderer: Type<DraggedContentRenderer> = DefaultTreeDraggedContentComponent;
  private literalDataRowConverter: LiteralDataRowConverter = defaultLiteralConverter;
  private contextualMenuComponent?: Type<any> = undefined;
  private dynamicHeight = false;
  private cufColumnsByGlobalFilter = false;
  private _showExtendedScopeOptionInFilterPanel = false;
  private appendCufFrom: BindableEntity;
  private addFilterCufFrom: BindableEntity;
  private initialSortedColumns: SortedColumn[] = [];
  private styleConfigurationBuilder: StyleDefinitionBuilder = new StyleDefinitionBuilder();
  private scopeDefinitionBuilder: ScopeDefinitionBuilder = new ScopeDefinitionBuilder();
  private serverModificationUrl?: string[];
  private multipleColumnsFilter?: string[] = [];
  private unselectRowOnSimpleClick = true;
  private shouldResetSorts = true;

  constructor(private id: string, private gridType: GridType) {
  }

  withRowHeight(rowHeight: number): GridDefinitionBuilder {
    this.rowHeight = rowHeight;
    return this;
  }

  withColumns(columnDefinitionBuilders: ColumnDefinitionBuilder[]): GridDefinitionBuilder {
    this.columnDefinitionBuilders.push(...columnDefinitionBuilders);
    return this;
  }

  server(): GridDefinitionBuilder {
    this.dataProvider = GridDataProvider.SERVER;
    return this;
  }

  withServerUrl(serverRootUrl: string[]): GridDefinitionBuilder {
    this.serverRootUrl = serverRootUrl;
    return this;
  }

  defaultPagination(): GridDefinitionBuilder {
    this.paginationBuilder = new PaginationConfigBuilder();
    return this;
  }

  withPagination(paginationBuilder: PaginationConfigBuilder): GridDefinitionBuilder {
    this.paginationBuilder = paginationBuilder;
    return this;
  }

  withStyle(styleDefinitionBuilder: StyleDefinitionBuilder): GridDefinitionBuilder {
    this.styleConfigurationBuilder = styleDefinitionBuilder;
    return this;
  }

  withScopeDefinition(scopeDefinitionBuilder: ScopeDefinitionBuilder): GridDefinitionBuilder {
    this.scopeDefinitionBuilder = scopeDefinitionBuilder;
    return this;
  }

  withRowConverter(rowConverter: LiteralDataRowConverter): GridDefinitionBuilder {
    this.literalDataRowConverter = rowConverter;
    return this;
  }

  withContextualMenu(componentType: Type<any>): GridDefinitionBuilder {
    this.contextualMenuComponent = componentType;
    return this;
  }

  disableHeaders(): GridDefinitionBuilder {
    this.enableHeaders = false;
    return this;
  }

  disableRightToolBar(): GridDefinitionBuilder {
    this.enableRightToolBar = false;
    return this;
  }

  disableMultiSelection(): GridDefinitionBuilder {
    this.enableMultiRowSelection = false;
    return this;
  }

  enableShowAll(): GridDefinitionBuilder {
    this.allowShowAll = true;
    return this;
  }

  enableDynamicHeight(): GridDefinitionBuilder {
    this.dynamicHeight = true;
    return this;
  }

  enableDrag(): GridDefinitionBuilder {
    this._enableDrag = true;
    return this;
  }

  enableInternalDrop(): GridDefinitionBuilder {
    this._enableInternalDrop = true;
    return this;
  }

  withDraggedContentRenderer(draggedContentRenderer: Type<DraggedContentRenderer>): GridDefinitionBuilder {
    this._draggedContentRenderer = draggedContentRenderer;
    return this;
  }

  synchronizeCufColumnsWithGlobalFilter(): GridDefinitionBuilder {
    this.cufColumnsByGlobalFilter = true;
    return this;
  }

  withCufFrom(bindableEntity: BindableEntity): GridDefinitionBuilder {
    this.appendCufFrom = bindableEntity;
    return this;
  }

  withFilterCufFrom(bindableEntity: BindableEntity): GridDefinitionBuilder {
    this.addFilterCufFrom = bindableEntity;
    return this;
  }

  withInitialSortedColumns(initialSortedColumns: SortedColumn[]): GridDefinitionBuilder {
    this.initialSortedColumns = initialSortedColumns;
    return this;
  }

  withModificationUrl(url: string[]) {
    this.serverModificationUrl = url;
    return this;
  }

  showExtendedScopeOptionInFilterPanel(): GridDefinitionBuilder {
    this._showExtendedScopeOptionInFilterPanel = true;
    return this;
  }

  disableUnselectRowOnSimpleClick(): GridDefinitionBuilder {
    this.unselectRowOnSimpleClick = false;
    return this;
  }

  enableMultipleColumnsFiltering(ids: string[]) {
    this.multipleColumnsFilter = ids;
    return this;
  }

  build(): GridDefinition {
    const columnDefinitions = this.columnDefinitionBuilders.map(builder => builder.build());
    return {
      id: this.id,
      rowHeight: this.rowHeight,
      dataProvider: this.dataProvider,
      serverRootUrl: this.serverRootUrl,
      gridType: this.gridType,
      allowShowAll: this.allowShowAll,
      enableHeaders: this.enableHeaders,
      enableRightToolBar: this.enableRightToolBar,
      enableMultiRowSelection: this.enableMultiRowSelection,
      unselectRowOnSimpleClick: this.unselectRowOnSimpleClick,
      dynamicHeight: this.dynamicHeight,
      columnDefinitions,
      pagination: this.paginationBuilder.build(),
      literalDataRowConverter: this.literalDataRowConverter,
      contextualMenuComponent: this.contextualMenuComponent,
      cufColumnsByGlobalFilter: this.cufColumnsByGlobalFilter,
      appendCufFrom: this.appendCufFrom,
      addFilterCufFrom: this.addFilterCufFrom,
      initialSortedColumns: this.initialSortedColumns,
      enableDrag: this._enableDrag,
      enableInternalDrop: this._enableInternalDrop,
      draggedContentRenderer: this._draggedContentRenderer,
      showExtendedScopeOptionInFilterPanel: this._showExtendedScopeOptionInFilterPanel,
      style: this.styleConfigurationBuilder.build(),
      scopeDefinition: this.scopeDefinitionBuilder.build(),
      serverModificationUrl: this.serverModificationUrl,
      multipleColumnsFilter: this.multipleColumnsFilter,
      shouldResetSorts: this.shouldResetSorts
    };
  }

}

export class PaginationConfigBuilder {
  private active = true;
  private size = 25;
  private showAll = false;
  private allowedPageSizes: number[] = [10, 25, 50, 100];
  private fixedPaginationSize = false;

  inactive(): PaginationConfigBuilder {
    this.active = false;
    return this;
  }

  initialSize(size: number): PaginationConfigBuilder {
    this.size = size;
    return this;
  }

  allowPageSizes(...sizes: number []): PaginationConfigBuilder {
    this.allowedPageSizes = sizes;
    return this;
  }

  fixPaginationSize(size: number): PaginationConfigBuilder {
    this.allowedPageSizes = [size];
    this.size = size;
    this.fixedPaginationSize = true;
    return this;
  }

  build(): PaginationConfig {
    return {...this as object as PaginationConfig};
  }
}

export class StyleDefinitionBuilder {
  private _showLines = false;
  private _highlightSelectedRows = true;
  private _highlightHoveredRows = true;
  private _showOverlayBeforeLoad = false;

  switchOffSelectedRows() {
    this._highlightSelectedRows = false;
    return this;
  }

  switchOffHoveredRows() {
    this._highlightHoveredRows = false;
    return this;
  }

  showLines() {
    this._showLines = true;
    return this;
  }

  enableInitialLoadAnimation() {
    this._showOverlayBeforeLoad = true;
    return this;
  }

  build(): GridStyleDefinition {
    return {
      showLines: this._showLines,
      showOverlayBeforeLoad: this._showOverlayBeforeLoad,
      highlightSelectedRows: this._highlightSelectedRows,
      highlightHoveredRows: this._highlightHoveredRows
    };
  }
}

export class ScopeDefinitionBuilder {
  private allowCustomScope = false;
  private customScopeKind: CustomScopeKind;

  withCustomScope(customScopeKind: CustomScopeKind) {
    this.allowCustomScope = true;
    this.customScopeKind = customScopeKind;
    return this;
  }

  build(): GridScopeDefinition {
    return {
      allowCustomScope: this.allowCustomScope,
      customScopeKind: this.customScopeKind
    };
  }
}

/**
 * The default Literal converter. Will transform any kind of row literal to GenericRow.
 * @param literals the partial rows literals to convert
 */
export function defaultLiteralConverter(literals: Partial<DataRow>[]): DataRow[] {
  return literals.reduce((datarows, literal) => {
    const dataRow = new GenericDataRow();
    Object.assign(dataRow, literal);
    datarows.push(dataRow);
    return datarows;
  }, []);
}

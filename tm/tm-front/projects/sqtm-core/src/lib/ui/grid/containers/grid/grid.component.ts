import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation
} from '@angular/core';
import {combineLatest, fromEvent, Observable, Subject} from 'rxjs';
import {GridViewportDirective} from '../../directives/grid-viewport.directive';
import {filter, takeUntil, withLatestFrom} from 'rxjs/operators';
import {GridDisplay} from '../../model/grid-display.model';
import {GridService} from '../../services/grid.service';
import {DOCUMENT} from '@angular/common';
import {UiState} from '../../model/state/ui.state';
import {OpenContextualMenuAction} from '../../model/actions/open-contextual-menu.action';
import {ComponentPortal} from '@angular/cdk/portal';
import {Overlay, OverlayRef} from '@angular/cdk/overlay';
import {GridNode} from '../../model/grid-node.model';
import {PaginationDisplay} from '../../model/pagination-display.model';
import {ScrollBarMeasurerService} from '../../../../core/services/scroll-bar-measurer.service';
import {
  ConfigurationManagerDialogComponent
} from '../../components/configuration-manager-dialog/configuration-manager-dialog.component';
import {DialogService} from '../../../dialog/services/dialog.service';
import {GridViewportService} from '../../services/grid-viewport.service';
import {GRID_SERVICE_TOKEN} from '../../token';

/** @dynamic */
@Component({
  selector: 'sqtm-core-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: GridViewportService
    },
    {
      provide: GRID_SERVICE_TOKEN,
      useExisting: GridService
    }
  ]
})
export class GridComponent implements OnInit, AfterViewInit, OnDestroy {

  // observables from store
  gridDisplay$: Observable<GridDisplay>;
  uiState$: Observable<UiState>;
  globalAsyncOperationRunning$: Observable<boolean>;

  // overlay menu reference
  overlayRef: OverlayRef;

  // unsub$
  unsub$ = new Subject<void>();

  @ViewChild(GridViewportDirective, {read: ElementRef})
  viewport: ElementRef;

  @ViewChild('gridElement', {read: ElementRef})
  gridElement: ElementRef;

  constructor(public grid: GridService, @Inject(DOCUMENT) private document: Document, private overlay: Overlay,
              private viewContainerRef: ViewContainerRef, private renderer: Renderer2, private dialogService: DialogService,
              private scrollBarMeasurer: ScrollBarMeasurerService) {
  }

  ngOnInit() {
    this.gridDisplay$ = this.grid.gridDisplay$;
    this.uiState$ = this.grid.uiState$;
    this.globalAsyncOperationRunning$ = this.grid.globalAsyncOperationRunning$;
    this.grid.dragging$.pipe(
      takeUntil(this.unsub$),
      filter(dragging => !dragging)
    ).subscribe(() => {
      this.document.body.style.cursor = 'default';
    });

    this.grid.openContextualMenu$.pipe(
      takeUntil(this.unsub$),
    ).subscribe((action: OpenContextualMenuAction) => {
      if (Boolean(this.overlayRef)) {
        // recycle previously opened menu
        this.moveContextualMenu(action);
      } else {
        this.doOpenContextualMenu(action);
      }
    });

    this.grid.openDisplayConfigurationDialog$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(() => {
      this.dialogService.openDialog({
        id: 'grid-display-configuration-manager',
        component: ConfigurationManagerDialogComponent,
        viewContainerReference: this.viewContainerRef,
        height: 800,
        width: 600
      });
    });

    this.grid.closeContextualMenu$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(() => this.doCloseContextualMenu());
  }

  ngAfterViewInit(): void {
    fromEvent(this.gridElement.nativeElement, 'click').pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.grid.uiState$),
      filter(([, uiState]) => uiState.showContextualMenu),
    ).subscribe(
      () => {
        this.grid.closeContextualMenu();
      }
    );

    combineLatest([this.grid.gridDisplay$, this.grid.gridNodes$, this.grid.paginationDisplay$]).pipe(
      takeUntil(this.unsub$),
      filter(([gridDisplay]) => gridDisplay.dynamicHeight)
    ).subscribe(([gridDisplay, gridNodes, pagination]) => {
      this.renderer.setStyle(this.gridElement.nativeElement, 'height', this.calculateGridHeight(gridDisplay, gridNodes, pagination));
    });

  }

  private doOpenContextualMenu(action: OpenContextualMenuAction) {
    const positionStrategy = this.createPositionStrategy(action);

    this.overlayRef = this.overlay.create({
      hasBackdrop: false,
      positionStrategy,
      disposeOnNavigation: true
    });
    this.overlayRef.attach(new ComponentPortal(action.component, this.viewContainerRef));
  }

  private createPositionStrategy(action: OpenContextualMenuAction) {
    const positionStrategy = this.overlay.position()
      .flexibleConnectedTo({x: action.x, y: action.y})
      .withPositions([
        {
          originX: 'center',
          originY: 'center',
          overlayX: 'start',
          overlayY: 'top',
        },
        {
          originX: 'center',
          originY: 'center',
          overlayX: 'start',
          overlayY: 'bottom',
        },
      ]);
    return positionStrategy;
  }

  private doCloseContextualMenu() {
    if (Boolean(this.overlayRef)) {
      this.overlayRef.dispose();
      this.overlayRef = null;
    }
  }

  private moveContextualMenu(action: OpenContextualMenuAction) {
    this.overlayRef.updatePositionStrategy(this.createPositionStrategy(action));
    this.overlayRef.updatePosition();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  // calculate the whole grid size. For small grids that must have a size depending on content and not on container
  calculateGridHeight(gridDisplay: GridDisplay, gridNodes: GridNode[], paginationDisplay: PaginationDisplay): string {
    let height = 0;
    if (gridDisplay.enableHeaders) {
      height = height + gridDisplay.rowHeight;
      // 2 pixels for underline of row height
      height = height + 2;
    }

    if (paginationDisplay.maxPage === 0) {
      height = height + gridNodes.length * gridDisplay.rowHeight;
    } else {
      height = height + paginationDisplay.size * gridDisplay.rowHeight;
    }

    height = height + this.scrollBarMeasurer.scrollBarWidth;

    if (paginationDisplay.showPaginationFooter) {
      height = height + 36;
    }

    // Height of the grid must be one more pixel than content to prevent scroll bar pop;
    height++;

    return `${height}px`;
  }
}




import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {GridService} from '../../../services/grid.service';
import {AbstractCellRendererComponent} from '../abstract-cell-renderer/abstract-cell-renderer.component';
import {ColumnDisplay} from '../../../model/column-display.model';
import {GridDisplay} from '../../../model/grid-display.model';
import {DataRow} from '../../../model/data-row.model';
import {GridType} from '../../../model/grid-definition.model';
import {RestService} from '../../../../../core/services/rest.service';
import {TableValueChange} from '../../../model/actions/table-value-change';
import {DialogService} from '../../../../dialog/services/dialog.service';
// tslint:disable-next-line:max-line-length
import {
  EditableTextFieldComponent
} from '../../../../workspace-common/components/editables/editable-text-field/editable-text-field.component';
import {SquashActionError, SquashFieldError} from '../../../../../model/error/error.model';

@Component({
  selector: 'sqtm-core-text-cell-renderer',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
        <ng-container *ngIf="canEdit(); else notEditable">
          <sqtm-core-editable-text-field #editableTextField style="margin: auto 5px;"
                                         class="sqtm-grid-cell-txt-renderer"
                                         [showPlaceHolder]="false"
                                         [displayInGrid]="true"
                                         [value]="row.data[columnDisplay.id]?.toString()" [layout]="'no-buttons'"
                                         [size]="'small'"
                                         (confirmEvent)="changeValue($event)"
          ></sqtm-core-editable-text-field>
        </ng-container>
        <ng-template #notEditable>
          <span style="margin: auto 0;" class="sqtm-grid-cell-txt-renderer"
                [ngClass]="textClass"
                [class.disabled-row]="row.disabled"
                [class.show-as-filtered-parent]="showAsFilteredParent"
                [sqtmCoreLabelTooltip]="getToolTipText(columnDisplay, gridDisplay, row)"
                nz-tooltip [nzTooltipTitle]="" [nzTooltipPlacement]="'topLeft'">
          {{getNotEditableText()}}
        </span>
        </ng-template>

      </div>
    </ng-container>`,
  styleUrls: ['./text-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextCellRendererComponent extends AbstractCellRendererComponent implements OnInit {

  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;


  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              public restService: RestService,
              public dialogService: DialogService) {
    super(grid, cdRef);
  }

  ngOnInit() {
  }

  canEdit() {
    if (typeof this.columnDisplay.editable === 'function') {
      return this.columnDisplay.editable(this.columnDisplay, this.row);
    } else {
      return this.columnDisplay.editable && this.row.simplePermissions.canWrite;
    }
  }

  getToolTipText(columnDisplay: ColumnDisplay, gridDisplay: GridDisplay, row: DataRow) {
    if (gridDisplay.gridType !== GridType.TREE) {
      return row.data[columnDisplay.id];
    } else {
      return '';
    }
  }

  getNotEditableText() {
    const value = this.row.data[this.columnDisplay.id];

    if (value == null || value === '') {
      return '-';
    } else {
      return value;
    }
  }

  get textClass(): string {
    return 'align-' + this.columnDisplay.contentPosition;
  }

  changeValue(value: string) {
    this.grid.updateCellValue(this.row, this.columnDisplay, value).subscribe(() => {
      const tableValueChange: TableValueChange = {columnId: this.columnDisplay.id, value};
      this.grid.editRows([this.row.id], [tableValueChange]);
    }, error => {

      if (error.status === 412) {
        const squashError = error.error.squashTMError;
        this.showError(squashError);
      }
    });
  }

  showError(error: SquashFieldError | SquashActionError) {
    const errors = error.kind === 'FIELD_VALIDATION_ERROR' ? error.fieldValidationErrors[0] : error.actionValidationError;
    this.dialogService.openAlert({
      id: 'error-dialog',
      titleKey: 'sqtm-core.generic.label.error',
      messageKey: errors.i18nKey,
      level: 'DANGER'
    });
    this.editableTextField.endAsync();
    this.cdRef.markForCheck();
  }
}

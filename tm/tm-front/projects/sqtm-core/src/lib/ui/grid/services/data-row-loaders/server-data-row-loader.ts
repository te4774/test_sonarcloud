import {Observable, of} from 'rxjs';
import {concatMap, map, take, withLatestFrom} from 'rxjs/operators';
import {AbstractDataRowLoader} from './abstract-data-row-loader';
import {RestService} from '../../../../core/services/rest.service';
import {DefinitionState} from '../../model/state/definition.state';
import {PaginationState} from '../../model/state/pagination.state';
import {DataRow, DataRowOpenState, GridRequest, GridRequestSort, GridResponse} from '../../model/data-row.model';
import {Identifier} from '../../../../model/entity.model';
import {
  decrementAsyncOperationCounter,
  extractRootIds,
  getDescendantIds,
  incrementAsyncOperationCounter
} from './data-row-utils';
import {GridState} from '../../model/state/grid.state';
import {ColumnState} from '../../model/state/column.state';
import {FilterState} from '../../model/state/filter.state';
import {Sort} from '../../model/column-definition.model';
import {gridLogger} from '../../grid.logger';
import {ProjectDataMap} from '../../../../model/project/project-data.model';
import {EntityState, Update} from '@ngrx/entity';
import {GridFilterUtils} from '../filter-managers/grid-filter-utils';

const logger = gridLogger.compose('ServerDataRowLoader');

export class ServerDataRowLoader extends AbstractDataRowLoader {

  constructor(restService: RestService) {
    super(restService);
  }

  public refreshData(initialState: GridState, projectData: ProjectDataMap, keepSelectedRows: boolean): Observable<GridState> {

    if (initialState.paginationState.active) {
      return this.fetchDataFromServer(
        initialState.paginationState, initialState.definitionState, initialState.columnState, initialState.filterState)
        .pipe(
          withLatestFrom(this.store.state$),
          map(([gridResponse, state]: [GridResponse, GridState]) => {
            const entities = state.definitionState.literalDataRowConverter(gridResponse.dataRows, projectData);
            let dataRowState = {
              ...state.dataRowState,
              count: gridResponse.count,
              rootRowIds: extractRootIds(gridResponse.dataRows),
              selectedRows: keepSelectedRows ? initialState.dataRowState.selectedRows : []
            };
            dataRowState = this.adapter.setAll(entities, dataRowState);
            return {...state, dataRowState};
          })
        );
    } else {
      return of(initialState);
    }
  }

  public openRows(ids: Identifier[], state$: Observable<GridState>, projectData: ProjectDataMap): Observable<GridState> {
    return state$.pipe(
      take(1),
      concatMap((state) => this.fetchChildren(state.definitionState.serverRootUrl, ids)),
      withLatestFrom(state$),
      // removing all rows just in case of something has been removed
      concatMap(([response, state]: [GridResponse, GridState]) => {
        const openedRows = ids
          .map(id => state.dataRowState.entities[id])
          .filter(row => Boolean(row))
          .filter(row => row.state === DataRowOpenState.open);
        if (openedRows.length > 0) {
          const children = openedRows.map(row => row.children).flat();
          return this.removeRows(children, state).pipe(
            map(removeRowResult => ([response, removeRowResult.state]))
          );
        } else {
          return of([response, state]);
        }
      }),
      map(([response, state]: [GridResponse, GridState]) => {
        let dataRowState = {...state.dataRowState};
        // parentRowId of the refreshed node is null in response so we must get the the parentRowId attribute from previous state
        // to avoid that upsertMany corrupt tree data by setting a parentRowId at null value...
        const rowLiterals = response.dataRows.map(dataRow => {
          if (ids.includes(dataRow.id) && dataRow.parentRowId == null) {
            dataRow.parentRowId = state.dataRowState.entities[dataRow.id].parentRowId;
          }
          return dataRow;
        });
        dataRowState = this.adapter.upsertMany(state.definitionState.literalDataRowConverter(rowLiterals, projectData), dataRowState);
        return {...state, dataRowState};
      })
    );
  }

  public refreshSubTrees(ids: Identifier[],
                         state$: Observable<GridState>,
                         projectData: ProjectDataMap,
                         forceOpen?: boolean): Observable<GridState> {
    return state$.pipe(
      take(1),
      map(state => {
        if (forceOpen) {
          const updates: Update<DataRow>[] = ids.map(id => ({id: id as any, changes: {state: DataRowOpenState.open}}));
          const dataRowState = this.adapter.updateMany(updates, state.dataRowState);
          return {...state, dataRowState};
        } else {
          return state;
        }
      }),
      concatMap((state) => this.fetchSubTrees(state.definitionState.serverRootUrl, ids, state.dataRowState)),
      withLatestFrom(state$),
      concatMap(([response, state]: [GridResponse, GridState]) => {
        const openedRows = ids
          .map(id => state.dataRowState.entities[id])
          .filter(row => Boolean(row))
          .filter(row => row.state === DataRowOpenState.open);
        if (openedRows.length > 0) {
          const children = openedRows.map(row => row.children).flat();
          return this.removeRows(children, state).pipe(
            map(removeRowResult => ([response, removeRowResult.state]))
          );
        } else {
          return of([response, state]);
        }
      }),
      map(([response, state]: [GridResponse, GridState]) => {
        let dataRowState = {...state.dataRowState};
        // parentRowId of the refreshed node is null in response so we must get the the parentRowId attribute from previous state
        // to avoid that upsertMany corrupt tree data by setting a parentRowId at null value...
        const rowLiterals = response.dataRows.map(dataRow => {
          if (ids.includes(dataRow.id) && dataRow.parentRowId == null) {
            dataRow.parentRowId = state.dataRowState.entities[dataRow.id].parentRowId;
          }
          return dataRow;
        });
        dataRowState = this.adapter.upsertMany(state.definitionState.literalDataRowConverter(rowLiterals, projectData), dataRowState);
        return {...state, dataRowState};
      })
    );
  }

  // when server side mode, the rows are already filtered server side and thus
  // we just need to reselect all available rows
  selectAllRows(state: GridState): Observable<GridState> {
    const dataRowState = {...state.dataRowState};
    dataRowState.selectedRows = [...dataRowState.ids];
    dataRowState.lastToggledRowId = null;
    return of({...state, dataRowState});
  }

  updateCell(url: string[], data: any): Observable<any> {
    return this.restService.post(url, data);
  }

  private fetchChildren(url: string [], ids: Identifier[]): Observable<GridResponse> {
    const pathParam = ids.map(id => id.toString()).join(',');
    return this.restService.get([...url, pathParam, 'content']);
  }

  private fetchSubTrees(url: string [], ids: Identifier[], dataRowState: EntityState<DataRow>): Observable<GridResponse> {
    const rows = ids.map(id => dataRowState.entities[id]);
    const descendantIds = getDescendantIds(dataRowState, rows);
    logger.debug('refresh : all descendants', [[...ids, ...descendantIds]]);
    const openedNodes = [...ids, ...descendantIds]
      .map(id => dataRowState.entities[id])
      .filter(row => row.state === DataRowOpenState.open)
      .map(row => row.id);
    logger.debug('refresh : opened nodes :', [openedNodes]);
    return this.restService.post([...url, 'refresh'], {
      nodeList: {references: openedNodes},
      nodeIds: ids
    });
  }

  private fetchDataFromServer(pagination: PaginationState,
                              definitionState: DefinitionState,
                              columnState: ColumnState,
                              filterState: FilterState): Observable<GridResponse> {
    const gridRequest = this.createRequest(columnState, filterState, pagination);
    logger.debug('Prepared grid request', [gridRequest]);
    return this.sendRequest(definitionState, gridRequest);
  }

  private sendRequest(config: DefinitionState, gridRequest: GridRequest): Observable<GridResponse> {

    const searchOnMultiColumns = config.multipleColumnsFilter.length > 0;

    return this.restService.post<GridResponse>(config.serverRootUrl, {
      gridRequest: 1, ...gridRequest,
      searchOnMultiColumns
    }).pipe(
      map(gridResponse => {
        const idAttribute = gridResponse.idAttribute;
        if (idAttribute) {
          gridResponse.dataRows = gridResponse.dataRows.map(datarow => {
            datarow.id = datarow.data[idAttribute];
            return datarow;
          });
        }
        return gridResponse;
      })
    );
  }

  private createRequest(columnState: ColumnState, filterState: FilterState, pagination: PaginationState) {
    const sorting = this.createRequestSorting(columnState);
    const filterModels = this.createRequestFilters(filterState);
    const scope = this.createRequestScope(filterState);
    const extendedHighLvlReqScope = filterState.extendedHighLvlReqScope;
    const gridRequest: GridRequest = {
      ...pagination, sort: sorting, filterValues: filterModels, scope, extendedHighLvlReqScope: extendedHighLvlReqScope
    };
    return gridRequest;
  }

  private createRequestScope(filterState: FilterState) {
    return filterState.scope.value.map(v => v.id);
  }

  private createRequestFilters(filterState: FilterState) {
    return GridFilterUtils.createRequestFilters(Object.values(filterState.filters.entities));
  }

  private createRequestSorting(columnState: ColumnState) {
    const sortedColumns = columnState.sortedColumns;
    const sorting: GridRequestSort[] = sortedColumns
      .filter(col => col.sort !== Sort.NO_SORT)
      .map(col => {
        const columnDefinition = columnState.entities[col.id];
        return {property: col.id.toString(), direction: col.sort, columnPrototype: columnDefinition.columnPrototype};
      });
    return sorting;
  }

  beginAsync(state: Readonly<GridState>): GridState {
    return incrementAsyncOperationCounter(state);
  }

  endAsync(state: Readonly<GridState>): GridState {
    return decrementAsyncOperationCounter(state);
  }
}

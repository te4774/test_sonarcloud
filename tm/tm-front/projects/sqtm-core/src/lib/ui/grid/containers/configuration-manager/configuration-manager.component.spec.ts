import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ConfigurationManagerComponent} from './configuration-manager.component';
import {GridTestingModule} from '../../grid-testing/grid-testing.module';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';

describe('ConfigurationManagerComponent', () => {
  let component: ConfigurationManagerComponent;
  let fixture: ComponentFixture<ConfigurationManagerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, TestingUtilsModule],
      declarations: [ConfigurationManagerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

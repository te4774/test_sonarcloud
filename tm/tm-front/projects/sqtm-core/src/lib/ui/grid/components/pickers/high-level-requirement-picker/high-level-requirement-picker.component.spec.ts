import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HighLevelRequirementPickerComponent} from './high-level-requirement-picker.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {RouterTestingModule} from '@angular/router/testing';

describe('HighLevelRequirementPickerComponent', () => {
  let component: HighLevelRequirementPickerComponent;
  let fixture: ComponentFixture<HighLevelRequirementPickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HighLevelRequirementPickerComponent ],
      imports: [HttpClientTestingModule, TestingUtilsModule, OverlayModule, RouterTestingModule],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HighLevelRequirementPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

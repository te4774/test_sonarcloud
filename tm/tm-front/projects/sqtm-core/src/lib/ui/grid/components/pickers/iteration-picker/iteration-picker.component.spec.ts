import {ComponentFixture, TestBed} from '@angular/core/testing';

import {IterationPickerComponent} from './iteration-picker.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../../testing-utils/testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {RouterTestingModule} from '@angular/router/testing';

describe('IterationPickerComponent', () => {
  let component: IterationPickerComponent;
  let fixture: ComponentFixture<IterationPickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule, OverlayModule, RouterTestingModule],
      declarations: [ IterationPickerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IterationPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-core-svg-icon-definition',
  templateUrl: './svg-icon-definition.component.html',
  styleUrls: ['./svg-icon-definition.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SvgIconDefinitionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SvgIconDefinitionComponent} from './components/svg-icon-definition/svg-icon-definition.component';
import {SvgIconComponent} from './components/svg-icon/svg-icon.component';

@NgModule({
    declarations: [SvgIconDefinitionComponent, SvgIconComponent],
    imports: [
        CommonModule
    ],
    exports: [SvgIconDefinitionComponent, SvgIconComponent]
})
export class SvgModule { }

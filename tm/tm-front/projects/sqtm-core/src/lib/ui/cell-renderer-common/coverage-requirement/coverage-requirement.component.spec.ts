import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CoverageRequirementComponent} from './coverage-requirement.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridTestingModule} from '../../grid/grid-testing/grid-testing.module';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';

describe('CoverageRequirementComponent', () => {
  let component: CoverageRequirementComponent;
  let fixture: ComponentFixture<CoverageRequirementComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, TestingUtilsModule],
      declarations: [CoverageRequirementComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoverageRequirementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {
  AbstractCellRendererComponent
} from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../grid/services/grid.service';
import {DataRow} from '../../grid/model/data-row.model';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';

@Component({
  selector: 'sqtm-core-issue-execution',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <a class="sqtm-grid-cell-txt-renderer"
           style="margin-top: auto; margin-bottom: auto"
           [sqtmCoreLabelTooltip]="getLabel(row)"
           nz-tooltip [nzTooltipPlacement]="'topLeft'"
           [routerLink]="getExecutionPath(row)">{{getLabel(row)}}</a>
      </div>
    </ng-container>`,
  styleUrls: ['./issue-execution.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueExecutionComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public girdService: GridService,
              public cdRef: ChangeDetectorRef,
              private translateService: TranslateService) {
    super(girdService, cdRef);
  }

  ngOnInit() {
  }

  getLabel(row: DataRow) {
    let executionOrder: number = row.data['executionOrder'];
    executionOrder = executionOrder + 1;

    const iteration = row.data['executionName'];
    const suiteNames = row.data['suiteNames'];
    let label = iteration;
    if (suiteNames !== '') {
      label = `${label} (${this.translateService.instant('sqtm-core.entity.test-suite.label.singular').toLowerCase()}
       '${suiteNames}', exe. #${executionOrder})`;
    } else {
      label = `${label} (exe. #${executionOrder})`;
    }
    return label;
  }

  getExecutionPath(row: DataRow) {
    return ['/', 'execution', row.data['executionId']];
  }
}

export function issueExecutionColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(IssueExecutionComponent);
}

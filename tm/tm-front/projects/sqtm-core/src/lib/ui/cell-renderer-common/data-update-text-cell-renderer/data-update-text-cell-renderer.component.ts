import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {
  AbstractCellRendererComponent
} from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../grid/services/grid.service';
import {RestService} from '../../../core/services/rest.service';
import {DialogService} from '../../dialog/services/dialog.service';
import {
  EditableTextFieldComponent
} from '../../workspace-common/components/editables/editable-text-field/editable-text-field.component';
import {TableValueChange} from '../../grid/model/actions/table-value-change';
import {SquashFieldError} from '../../../model/error/error.model';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';
import {concatMap, map, take} from 'rxjs/operators';


@Component({
  selector: 'sqtm-core-data-update-text-cell-renderer',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
        <sqtm-core-editable-text-field #editableTextField style="margin: auto 5px;"
                                       class="sqtm-grid-cell-txt-renderer"
                                       [showPlaceHolder]="false"
                                       [value]="row.data[columnDisplay.id]" [layout]="'no-buttons'"
                                       [size]="'small'"
                                       (confirmEvent)="updateValue($event)"
        ></sqtm-core-editable-text-field>
      </div>
    </ng-container>`,
  styleUrls: ['./data-update-text-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DataUpdateTextCellRendererComponent extends AbstractCellRendererComponent implements OnInit {


  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              public restService: RestService,
              public dialogService: DialogService) {
    super(grid, cdRef);
  }

  ngOnInit(): void {
  }

  updateValue(value: string) {
    this.grid.gridState$.pipe(
      take(1),
      map(state => ([`${state.definitionState.serverModificationUrl}/${this.row.id}/${this.columnDisplay.id}`])),
      concatMap(url => this.restService.post(url, {[this.columnDisplay.id]: value}))
    ).subscribe(() => {
      const tableValueChange: TableValueChange = {columnId: this.columnDisplay.id, value};
      this.grid.editRows([this.row.id], [tableValueChange]);
    }, httpError => {

      if (httpError.status === 412) {
        if (httpError.error.squashTMError != null) {
          const squashError = httpError.error.squashTMError;
          this.showError(squashError);
        } else {
          this.showError(httpError.error.error);
        }
      }
    });
  }

  showError(error: SquashFieldError) {
    const fieldErrors = error.fieldValidationErrors;
    this.dialogService.openAlert({
      id: 'error-dialog',
      titleKey: 'sqtm-core.generic.label.error',
      messageKey: fieldErrors[0].i18nKey,
      level: 'DANGER'
    });
    this.editableTextField.endAsync();
    this.cdRef.markForCheck();
  }
}

export function dataUpdateTextColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DataUpdateTextCellRendererComponent);
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';
import {GridService} from '../../grid/services/grid.service';
import {TranslateService} from '@ngx-translate/core';
import {
  AbstractCellRendererComponent
} from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import {DialogService} from '../../dialog/services/dialog.service';
import {
  CoverageVerifiedByDialogComponent
} from '../../dialog/components/coverage-verified-by-dialog/coverage-verified-by-dialog.component';
import {CoverageTestStepInfo} from '../../../model/test-case/requirement-version-coverage-model';

@Component({
  selector: 'sqtm-core-coverage-verified-by',
  templateUrl: './coverage-verified-by.component.html',
  styleUrls: ['./coverage-verified-by.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoverageVerifiedByComponent extends AbstractCellRendererComponent {

  get verifiedByStepOrDirectly(): string {
    const stepInfos = this.row.data['coverageStepInfos'];
    return stepInfos.length > 0 ? this.getVerifiedByTestStepContent(stepInfos) :
      this.row.data['directlyVerified'] ? this.getVerifiedByDirectlyContent() : '';
  }

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef,
              private translate: TranslateService, private dialogService: DialogService) {
    super(grid, cdRef);
  }

  isDirectlyVerified() {
    return this.verifiedByStepOrDirectly !== '';
  }

  getVerifiedByTestStepContent(stepIds: number[]) {
    let verifiedBy = this.translate.instant('sqtm-core.entity.requirement-version-coverage.verified-by.test-step');
    stepIds.forEach((stepIndex, index) => {
      verifiedBy = index !== stepIds.length - 1 ? `${verifiedBy} #${index + 1},` : `${verifiedBy} #${index + 1}`;
    });
    return verifiedBy;
  }

  getVerifiedByDirectlyContent() {
    return this.translate.instant('sqtm-core.entity.requirement-version-coverage.verified-by.test-case');
  }

  isMultiCovered() {
    const directly = this.row.data['directlyVerified'];
    const unDirectly = this.row.data['unDirectlyVerified'];
    const calledTestCaseIds = this.row.data['verifyingCalledTestCaseIds'];
    const stepInfos = this.row.data['coverageStepInfos'];
    if (stepInfos.length > 1 || calledTestCaseIds.length > 1) {
      return true;
    } else {
      return !!(directly && unDirectly);
    }
  }

  openInformationDialog() {
    this.dialogService.openDialog({
      id: 'verified-by',
      component: CoverageVerifiedByDialogComponent,
      data: {
        testCaseId: this.row.data['verifyingTestCaseId'],
        coverageStepInfoDto: this.row.data['coverageStepInfos'],
        calledTestCaseIds: this.row.data['verifyingCalledTestCaseIds']
      }
    });
  }

  getStepUrl(stepInfo: CoverageTestStepInfo) {
    const testCaseId = this.row.data['verifyingTestCaseId'];
    return `/detailed-test-step/${testCaseId}/step/${stepInfo.index}`;
  }

  getStepIndex(stepInfo: CoverageTestStepInfo) {
    return `#${stepInfo.index + 1}`;
  }

  getTooltipForCoverageStepInfo(stepInfo: CoverageTestStepInfo) {
    const testStepLabel = this.translate.instant('sqtm-core.entity.requirement-version-coverage.verified-by.test-step');
    const testStepIndex = this.getStepIndex(stepInfo);
    return `${testStepLabel} ${testStepIndex}`;
  }
}

export function coverageVerifiedByColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(CoverageVerifiedByComponent);
}

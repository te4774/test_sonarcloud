import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {
  AbstractCellRendererComponent
} from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../grid/services/grid.service';
import {TestCaseExecutionMode, TestCaseExecutionModeKeys} from '../../../model/level-enums/level-enum';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';

@Component({
  selector: 'sqtm-core-execution-mode-cell',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <sqtm-core-execution-mode
          style="margin: auto"
          [executionMode]="getExecutionMode(row.data[columnDisplay.id])">
        </sqtm-core-execution-mode>
      </div>
    </ng-container>`,
  styleUrls: ['./execution-mode-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionModeCellComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  getExecutionMode(key: TestCaseExecutionModeKeys) {
    return TestCaseExecutionMode[key];
  }
}

export function executionModeColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ExecutionModeCellComponent)
    .withHeaderPosition('center');
}

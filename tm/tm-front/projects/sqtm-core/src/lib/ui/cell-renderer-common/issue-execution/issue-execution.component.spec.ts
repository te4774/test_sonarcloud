import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {IssueExecutionComponent} from './issue-execution.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';
import {GridTestingModule} from '../../grid/grid-testing/grid-testing.module';
import {RouterTestingModule} from '@angular/router/testing';

describe('IssueExecutionComponent', () => {
  let component: IssueExecutionComponent;
  let fixture: ComponentFixture<IssueExecutionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [IssueExecutionComponent],
      imports: [GridTestingModule, TestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueExecutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

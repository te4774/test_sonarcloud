import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';
import {GridService} from '../../grid/services/grid.service';
import {
  AbstractCellRendererComponent
} from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';

@Component({
  selector: 'sqtm-core-coverage-requirement',
  template: `
    <div *ngIf="columnDisplay && row" class="sqtm-grid-cell-txt-renderer flex-column full-height">
      <a [class.disabled-row]="row.disabled" class="sqtm-grid-cell-txt-renderer" nz-tooltip [nzTooltipTitle]=""
         [sqtmCoreLabelTooltip]="row.data[columnDisplay.id]"
         [routerLink]="['/requirement-workspace/requirement-version/detail', row.data['requirementVersionId']]">{{row.data[columnDisplay.id]}}</a>
    </div>
  `,
  styleUrls: ['./coverage-requirement.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoverageRequirementComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  ngOnInit() {
  }

}

export function coverageRequirementColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(CoverageRequirementComponent);
}

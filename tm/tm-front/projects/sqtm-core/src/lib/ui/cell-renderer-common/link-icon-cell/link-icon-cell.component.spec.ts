import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {LinkIconCellComponent} from './link-icon-cell.component';
import {GridTestingModule} from '../../grid/grid-testing/grid-testing.module';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';

describe('LinkIconCellComponent', () => {
  let component: LinkIconCellComponent;
  let fixture: ComponentFixture<LinkIconCellComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, TestingUtilsModule],
      declarations: [ LinkIconCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkIconCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

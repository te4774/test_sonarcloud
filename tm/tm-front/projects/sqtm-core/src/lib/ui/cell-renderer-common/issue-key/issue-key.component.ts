import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {
  AbstractCellRendererComponent
} from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../grid/services/grid.service';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';
import {IssueKeyExportValueRenderer} from '../../grid-export/value-renderer/value-renderer';

@Component({
  selector: 'sqtm-core-issue-key',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <a class="sqtm-grid-cell-txt-renderer" style="margin-top: auto;margin-bottom: auto" [href]="row.data['url']"
           target="_blank"
           [sqtmCoreLabelTooltip]="remoteKey"
           nz-tooltip [nzTooltipTitle]="" [nzTooltipPlacement]="'topLeft'">
          {{remoteKey}}
        </a>
      </div>
    </ng-container>`,
  styleUrls: ['./issue-key.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueKeyComponent extends AbstractCellRendererComponent {
  constructor(public gridService: GridService, public cdRef: ChangeDetectorRef) {
    super(gridService, cdRef);
  }

  // The displayed key may not be the "logical" ID used for sorting etc..
  get remoteKey(): string {
    return this.row.data['remoteKey'] ?? this.row.data[this.columnDisplay.id];
  }
}

export function issueKeyColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(IssueKeyComponent).withExportValueRenderer(IssueKeyExportValueRenderer);
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {MilestoneLabelCellComponent} from './milestone-label-cell.component';
import {TranslateModule} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {GridTestingModule} from '../../grid/grid-testing/grid-testing.module';
import {DatePipe} from '@angular/common';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';

describe('MilestoneDateCellComponent', () => {
  let component: MilestoneLabelCellComponent;
  let fixture: ComponentFixture<MilestoneLabelCellComponent>;
  const datePipeMock = jasmine.createSpyObj('datePipe', ['transform']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), GridTestingModule, TestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [MilestoneLabelCellComponent],
      providers: [{provide: DatePipe, useValue: datePipeMock}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilestoneLabelCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';

import {Overlay} from '@angular/cdk/overlay';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {
  AbstractListCellRendererComponent
} from '../../grid/components/cell-renderers/abstract-list-cell-renderer/abstract-list-cell-renderer';
import {AutomatedTestTechnology, buildTechnologyLabel} from '../../../model/automation/automated-test-technology.model';
import {GridService} from '../../grid/services/grid.service';
import {ReferentialDataService} from '../../../core/referential/services/referential-data.service';
import {RestService} from '../../../core/services/rest.service';
import {ActionErrorDisplayService} from '../../../core/services/errors-handling/action-error-display.service';
import {ListPanelItem} from '../../workspace-common/components/forms/list-panel/list-panel.component';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';
import {catchError, finalize, tap, withLatestFrom} from 'rxjs/operators';

@Component({
  selector: 'sqtm-core-automated-test-technology',
  templateUrl: './automated-test-technology.component.html',
  styleUrls: ['./automated-test-technology.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomatedTestTechnologyComponent extends AbstractListCellRendererComponent {

  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  @ViewChild('infoListItemRef', {read: ElementRef})
  infoListItemRef: ElementRef;

  asyncOperationRunning = false;

  automatedTestTechnologies$: Observable<AutomatedTestTechnology[]>;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              public overlay: Overlay,
              public vcr: ViewContainerRef,
              private referentialDataService: ReferentialDataService,
              public translateService: TranslateService,
              public restService: RestService,
              public actionErrorDisplayService: ActionErrorDisplayService) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
    this.automatedTestTechnologies$ = this.referentialDataService.automatedTestTechnologies$;
  }

  showTemplate() {
    if (this.isEditable()) {
      this.showList(this.infoListItemRef, this.templatePortalContent);
    }
  }

  getListPanelItems(technologies: AutomatedTestTechnology[]): ListPanelItem[] {
    const listPanelItems: ListPanelItem[] = technologies.map(item => ({
        id: item.id,
        label: buildTechnologyLabel(item),
    }));

    listPanelItems.sort((first, second) => first.label.localeCompare(second.label));

    return [
      { id: null, label: this.translateService.instant('sqtm-core.generic.label.none.feminine') },
      ...listPanelItems
    ];
  }

  change(technologyId: number) {
    this.beginAsyncOperation();
    this.grid.updateCellValue(this.row, this.columnDisplay, technologyId).pipe(
      withLatestFrom(this.automatedTestTechnologies$),
      tap(([, technologies]: [void, AutomatedTestTechnology[]]) => {
        const technologyIdChange = {columnId: 'atTechnologyId', value: technologyId};
        const technologyName = technologies.find(t => t.id === technologyId)?.name;
        const nameChange = {columnId: this.columnDisplay.id, value: this.getLabel(technologyName)};
        this.grid.editRows([this.row.id], [technologyIdChange, nameChange]);
      }),
      catchError(err => this.errorDisplayService.handleActionError(err)),
      finalize(() => {
        this.close();
        this.endAsyncOperation();
      })
    ).subscribe();
  }

  getLabel(technologyName: string | null) {
    return technologyName ?? this.translateService.instant('sqtm-core.generic.label.none.feminine');
  }
}

export function automatedTestTechnologyColumn(technologyNameColumnId: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(technologyNameColumnId)
    .withRenderer(AutomatedTestTechnologyComponent);
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ExecutionStatusCellComponent} from './execution-status-cell.component';
import {GridTestingModule} from '../../grid/grid-testing/grid-testing.module';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ExecutionStatusCellComponent', () => {
  let component: ExecutionStatusCellComponent;
  let fixture: ComponentFixture<ExecutionStatusCellComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutionStatusCellComponent ],
      imports: [GridTestingModule, TestingUtilsModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionStatusCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

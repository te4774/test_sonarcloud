import {ChangeDetectorRef, Directive, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';
import {
  AbstractCellRendererComponent
} from '../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../grid/services/grid.service';
import {DialogService} from '../dialog/services/dialog.service';
import {
  ConfirmDeleteLevel
} from '../dialog/components/dialog-templates/confirm-delete-dialog/confirm-delete-configuration';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractDeleteCellRenderer extends AbstractCellRendererComponent implements OnInit, OnDestroy {

  unsub$ = new Subject<void>();

  protected constructor(public grid: GridService,
                        cdr: ChangeDetectorRef,
                        protected dialogService: DialogService) {
    super(grid, cdr);
  }

  ngOnInit(): void {
  }

  showDeleteConfirm() {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: this.getTitleKey(),
      messageKey: this.getMessageKey(),
      level: this.getLevel(),
    });

    dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      filter(result => result === true),
    ).subscribe(() => {
      this.doDelete();
    });
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'WARNING';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.dialog.delete.element.title';
  }

  protected getTitleKey(): string {
    return 'sqtm-core.dialog.delete.definitive';
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  protected abstract doDelete();
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {compareLevels, ExecutionStatus, ExecutionStatusKeys} from '../../../model/level-enums/level-enum';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';
import {GridService} from '../../grid/services/grid.service';
import {
  AbstractCellRendererComponent
} from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';

@Component({
  selector: 'sqtm-core-execution-status-cell',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <ng-container *ngIf="row.data[columnDisplay.id]">
          <div class="status-badge"
               [style.backgroundColor]="color"
               nz-tooltip [nzTooltipTitle]="i18nKey | translate | capitalize">
          </div>
        </ng-container>
        <ng-container *ngIf="!row.data[columnDisplay.id]">
          <div class="no-execution" nz-tooltip [nzTooltipTitle]="'sqtm-core.generic.label.no-execution' | translate">-</div>
        </ng-container>
      </div>
    </ng-container>`,
  styleUrls: ['./execution-status-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionStatusCellComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  get statusKey(): ExecutionStatusKeys {
    return this.row.data[this.columnDisplay.id];
  }

  get color(): string {
    return ExecutionStatus[this.statusKey].color;
  }

  get i18nKey(): string {
    return ExecutionStatus[this.statusKey].i18nKey;
  }
}

export function executionStatusColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ExecutionStatusCellComponent)
    .withHeaderPosition('center')
    .withSortFunction(sortExecutionStatus());
}

export function sortExecutionStatus() {
  return (keyA: any, keyB: any) => doSortExecutionStatus(keyA, keyB);
}

export function doSortExecutionStatus(keyA: any, keyB: any) {
  const noExecutionLevel = 0;
  const itemA = ExecutionStatus[keyA] ? ExecutionStatus[keyA].level : noExecutionLevel;
  const itemB = ExecutionStatus[keyB] ? ExecutionStatus[keyB].level : noExecutionLevel;
  return compareLevels(itemA, itemB);
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewContainerRef} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {
  AbstractCellRendererComponent
} from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../grid/services/grid.service';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';
import {DialogService} from '../../dialog/services/dialog.service';
import {NamedReference} from '../../../model/named-reference';
import {
  getIssueVerifiedRequirementVersionsDialogConf
} from '../../issues/components/issue-verified-requirement-versions-dialog/issue-verified-requirement-versions-dialog.component';
import {IssueRequirementsValueRenderer} from '../../grid-export/value-renderer/value-renderer';

@Component({
  selector: 'sqtm-core-issue-requirements',
  template: `
    <ng-container *ngIf="row && verifiedRequirementVersions">
      <div class="full-width full-height flex-column">
        <span *ngIf="isMultipleMode; else requirementVersionLink"
              class="sqtm-grid-cell-txt-renderer __hover_pointer"
              style="margin-top: auto; margin-bottom: auto"
              [sqtmCoreLabelTooltip]="'sqtm-core.entity.issue.multiple-requirements'|translate"
              nz-tooltip [nzTooltipPlacement]="'topLeft'"
              (click)="handleClick()">
          {{'sqtm-core.entity.issue.multiple-requirements'|translate}}
        </span>
      </div>
    </ng-container>

    <ng-template #requirementVersionLink>
      <a class="sqtm-grid-cell-txt-renderer"
         style="margin-top: auto; margin-bottom: auto"
         [sqtmCoreLabelTooltip]="getSingleExecutionLabel()"
         nz-tooltip [nzTooltipPlacement]="'topLeft'"
         [routerLink]="getPathToRequirementVersionView()">
        {{getSingleExecutionLabel()}}
      </a>
    </ng-template>`,
  styleUrls: ['./issue-requirements.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueRequirementsComponent extends AbstractCellRendererComponent {

  constructor(public gridService: GridService,
              public cdRef: ChangeDetectorRef,
              private translateService: TranslateService,
              private dialogService: DialogService,
              private viewContainerRef: ViewContainerRef) {
    super(gridService, cdRef);
  }

  get verifiedRequirementVersions(): NamedReference[] {
    return this.row.data.verifiedRequirementVersions;
  }

  get isMultipleMode(): boolean {
    return this.verifiedRequirementVersions.length > 1;
  }

  getSingleExecutionLabel(): string {
    const firstRequirementVersion = this.verifiedRequirementVersions[0];
    return firstRequirementVersion?.name ?? '-';
  }

  getPathToRequirementVersionView(): any[] {
    return ['/requirement-workspace/requirement-version/detail', this.verifiedRequirementVersions[0].id];
  }

  handleClick(): void {
    if (this.isMultipleMode) {
      this.dialogService.openDialog(getIssueVerifiedRequirementVersionsDialogConf(
        this.verifiedRequirementVersions,
        this.viewContainerRef));
    }
  }
}

export function issueRequirementsColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder('verifiedRequirementVersions')
    .withRenderer(IssueRequirementsComponent).withExportValueRenderer(IssueRequirementsValueRenderer);
}

import {ComponentFixture, TestBed} from '@angular/core/testing';

import {IssueExecutionsComponent} from './issue-executions.component';
import {GridTestingModule} from '../../grid/grid-testing/grid-testing.module';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {OverlayModule} from '@angular/cdk/overlay';

describe('IssueExecutionsComponent', () => {
  let component: IssueExecutionsComponent;
  let fixture: ComponentFixture<IssueExecutionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IssueExecutionsComponent ],
      imports: [GridTestingModule, TestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule, OverlayModule],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueExecutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewContainerRef} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {
  AbstractCellRendererComponent
} from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../grid/services/grid.service';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';
import {DialogService} from '../../dialog/services/dialog.service';
import {getIssueReportSiteAsString} from '../../../model/issue/issue-report-site.model';
import {
  getIssueReportSitesDialogConf
} from '../../issues/components/issue-report-sites-dialog/issue-report-sites-dialog.component';
import {IssueExecutionsExportValueRenderer} from '../../grid-export/value-renderer/value-renderer';

@Component({
  selector: 'sqtm-core-issue-executions',
  template: `
      <ng-container *ngIf="row && row.data.reportSites">
          <div class="full-width full-height flex-column">
        <span *ngIf="isMultipleExecutionsMode; else executionLink"
              class="sqtm-grid-cell-txt-renderer __hover_pointer"
              style="margin-top: auto; margin-bottom: auto; text-decoration: underline;"
              [sqtmCoreLabelTooltip]="'sqtm-core.entity.issue.multiple-executions'|translate"
              nz-tooltip [nzTooltipPlacement]="'topLeft'"
              (click)="handleClick()">
          {{'sqtm-core.entity.issue.multiple-executions'|translate}}
        </span>
          </div>
      </ng-container>

      <ng-template #executionLink>
          <a class="sqtm-grid-cell-txt-renderer"
             style="margin-top: auto; margin-bottom: auto"
             [sqtmCoreLabelTooltip]="getSingleExecutionLabel()"
             nz-tooltip [nzTooltipPlacement]="'topLeft'"
             [routerLink]="getSingleExecutionPath()">
              {{getSingleExecutionLabel()}}
          </a>
      </ng-template>`,
  styleUrls: ['./issue-executions.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueExecutionsComponent extends AbstractCellRendererComponent {
  constructor(public gridService: GridService,
              public cdRef: ChangeDetectorRef,
              private translateService: TranslateService,
              private dialogService: DialogService,
              private viewContainerRef: ViewContainerRef) {
    super(gridService, cdRef);
  }

  get isMultipleExecutionsMode(): boolean {
    return this.row.data.reportSites.length > 1;
  }

  getSingleExecutionLabel(): string {
    const firstReportSite = this.row.data.reportSites[0];
    return getIssueReportSiteAsString(firstReportSite, this.translateService);
  }

  getSingleExecutionPath(): any[] {
    return ['/', 'execution', this.row.data.reportSites[0].executionId];
  }

  handleClick(): void {
    if (this.isMultipleExecutionsMode) {
      this.dialogService.openDialog(getIssueReportSitesDialogConf(
              this.row.data.reportSites,
              this.viewContainerRef));
    }
  }
}

export function issueExecutionsColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder('reportSites').withRenderer(IssueExecutionsComponent)
          .withExportValueRenderer(IssueExecutionsExportValueRenderer);
}

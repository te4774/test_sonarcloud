import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {TreeNodeCellRendererComponent} from './tree-node-cell-renderer.component';
import {TranslateModule} from '@ngx-translate/core';
import {GridTestingModule} from '../../grid/grid-testing/grid-testing.module';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';

describe('TreeNodeCellRendererComponent', () => {
  let component: TreeNodeCellRendererComponent;
  let fixture: ComponentFixture<TreeNodeCellRendererComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        GridTestingModule,
        TestingUtilsModule,
        TranslateModule.forRoot(),
      ],
      declarations: [ TreeNodeCellRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeNodeCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

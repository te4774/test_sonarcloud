import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {
  AbstractCellRendererComponent
} from '../../grid/components/cell-renderers/abstract-cell-renderer/abstract-cell-renderer.component';
import {GridService} from '../../grid/services/grid.service';
import {isLinkOptions, LinkOptions} from '../../grid/model/column-definition.model';
import {ColumnDefinitionBuilder} from '../../grid/model/column-definition.builder';

@Component({
  selector: 'sqtm-core-external-link-cell-renderer',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <a [href]="getUrl()" [target]="'_blank'" class="text-overflow link-cell" nz-tooltip
           [sqtmCoreLabelTooltip]="row.data[columnDisplay.id]"
           [nzTooltipTitle]=""
           [nzTooltipPlacement]="'topLeft'">{{row.data[columnDisplay.id]}}</a>
      </div>
    </ng-container>
  `,
  styleUrls: ['./external-link-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExternalLinkCellRendererComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(grid: GridService, cdr: ChangeDetectorRef) {
    super(grid, cdr);
  }

  ngOnInit(): void {
  }

  getUrl(): string | string[] {
    const linkOptions = this.columnDisplay.options;
    if (!linkOptions || !isLinkOptions(linkOptions)) {
      throw Error('This cell renderer must be provided with LinkOptions.');
    }

    if (linkOptions.createUrlFunction) {
      return linkOptions.createUrlFunction(this.row, this.columnDisplay);
    }

    if (linkOptions.columnParamId) {
      return this.row.data[linkOptions.columnParamId];
    } else {
      return this.row.data[this.columnDisplay.id];
    }

  }
}

export function externalLinkColumn(id: string, options: LinkOptions): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ExternalLinkCellRendererComponent)
    .withOptions(options);
}

export function basicExternalLinkColumn(id: string): ColumnDefinitionBuilder {
  const options: LinkOptions = {kind: 'link'};
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ExternalLinkCellRendererComponent)
    .withOptions(options);
}

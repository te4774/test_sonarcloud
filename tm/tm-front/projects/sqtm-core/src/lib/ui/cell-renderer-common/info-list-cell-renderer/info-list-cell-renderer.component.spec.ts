import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {InfoListCellRendererComponent} from './info-list-cell-renderer.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {TranslateModule} from '@ngx-translate/core';
import {GridTestingModule} from '../../grid/grid-testing/grid-testing.module';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';

describe('InfoListCellRendererComponent', () => {
  let component: InfoListCellRendererComponent;
  let fixture: ComponentFixture<InfoListCellRendererComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [GridTestingModule, TestingUtilsModule, OverlayModule, TranslateModule.forRoot()],
      declarations: [InfoListCellRendererComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoListCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

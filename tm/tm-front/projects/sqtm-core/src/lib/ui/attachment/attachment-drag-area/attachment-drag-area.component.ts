import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
  ViewChild
} from '@angular/core';
import {take} from 'rxjs/operators';
import {ReferentialDataService} from '../../../core/referential/services/referential-data.service';

@Component({
  selector: 'sqtm-core-attachment-drag-area',
  templateUrl: './attachment-drag-area.component.html',
  styleUrls: ['./attachment-drag-area.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AttachmentDragAreaComponent {

  drawerMessage: string;

  isHovered = false;

  @Input()
  set canAttach(canAttach: boolean) {
    this._canAttach = canAttach;
    this.refreshMessage();
  }

  get canAttach(): boolean {
    return this._canAttach;
  }

  _canAttach: boolean;

  @ViewChild('file') file: ElementRef<HTMLInputElement>;

  @Output()
  uploadFilesEvent: EventEmitter<File[]> = new EventEmitter();

  @HostListener('click')
  onClick(): void {
    if (this.canAttach) {
      this.file.nativeElement.click();
    }
  }

  // preventDefault() needed to avoid the file to be opened in the browser
  @HostListener('dragover', ['$event'])
  onDragOver(event: any): void {
    this.isHovered = true;
    event.preventDefault();
  }

  @HostListener('dragleave')
  onAreaDragLeave() {
    this.isHovered = false;
  }

  onFileChange(event: Event): void {
    const input = event.target as HTMLInputElement;
    this.uploadFiles(Array.from(input.files));
    input.value = '';
  }

  uploadFiles(files: File[]): void {
    this.uploadFilesEvent.emit(files);
  }

  constructor(public readonly referentialDataService: ReferentialDataService) {
  }

  private refreshMessage(): void {
    this.referentialDataService.globalConfiguration$.pipe(
      take(1),
    ).subscribe((globalConf) => {
      this.drawerMessage = this.getAttachmentDrawerMessage(globalConf.milestoneFeatureEnabled);
    });
  }

  private getAttachmentDrawerMessage(milestoneFeatureEnabled: boolean) {
    if (this.canAttach) {
      return 'sqtm-core.attachment-drawer.message';
    } else {
      if (milestoneFeatureEnabled) {
        return 'sqtm-core.attachment-drawer.milestone-mode-message';
      } else {
        return 'sqtm-core.attachment-drawer.no-perm-message';
      }
    }
  }
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {AttachmentComponent} from './attachment.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CORE_MODULE_CONFIGURATION} from '../../../core/sqtm-core.tokens';
import {defaultSqtmConfiguration} from '../../../core/sqtm-core.module';
import {EntityViewService} from '../../../core/services/entity-view/entity-view.service';
import {SqtmEntityState} from '../../../core/services/entity-view/entity-view.state';
import {RestService} from '../../../core/services/rest.service';
import {ReferentialDataService} from '../../../core/referential/services/referential-data.service';
import {AttachmentService} from '../../../core/services/attachment.service';
import {TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';
import {TestCasePermissions} from '../../../model/permissions/simple-permissions';
import {ProjectData} from '../../../model/project/project-data.model';
import {CustomFieldValueService} from '../../../core/services/custom-field-value.service';
import {GenericEntityUiState} from '../../../core/services/genric-entity-view/generic-entity-view-state';
import {
  EntityViewAttachmentHelperService
} from '../../../core/services/entity-view/entity-view-attachment-helper.service';
import {
  EntityViewCustomFieldHelperService
} from '../../../core/services/entity-view/entity-view-custom-field-helper.service';
import {GenericEntityViewService} from '../../../core/services/genric-entity-view/generic-entity-view.service';

describe('AttachmentComponent', () => {
  let component: AttachmentComponent<DummyEntity, 'dummy', TestCasePermissions>;
  let fixture: ComponentFixture<AttachmentComponent<DummyEntity, 'dummy', TestCasePermissions>>;

  interface DummyEntity extends SqtmEntityState {
    name: string;
  }

  class DummyEntityViewService extends EntityViewService<DummyEntity, 'dummy', TestCasePermissions> {
    constructor(protected restService: RestService,
                protected referentialDataService: ReferentialDataService,
                protected attachmentService: AttachmentService,
                protected translateService: TranslateService,
                protected customFieldValueService: CustomFieldValueService,
                protected attachmentHelper: EntityViewAttachmentHelperService,
                protected customFieldHelper: EntityViewCustomFieldHelperService,
    ) {
      super(
        restService,
        referentialDataService,
        attachmentService,
        translateService,
        customFieldValueService,
        attachmentHelper,
        customFieldHelper,
      );
    }

    addSimplePermissions(projectData: ProjectData): TestCasePermissions {
      return new TestCasePermissions(projectData);
    }

    getInitialState(): { [K in 'dummy']: DummyEntity } & { type: 'dummy' } & { uiState: GenericEntityUiState<DummyEntity> } {
      return {
        type: 'dummy',
        uiState: {fieldState: {ids: [], entities: {}}},
        dummy: null
      };
    }
  }

  const componentData$ = of({
    type: 'dummy',
    dummy: {
      attachmentList: {
        id: 1,
        attachments:
          {
            ids: [],
            entities: {}
          }
      }
    }
  });
  const mockService = {componentData$};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AttachmentComponent],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: CORE_MODULE_CONFIGURATION,
          useValue: defaultSqtmConfiguration
        },
        {
          provide: GenericEntityViewService,
          useValue: mockService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent<AttachmentComponent<DummyEntity, 'dummy', TestCasePermissions>>(AttachmentComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));

});

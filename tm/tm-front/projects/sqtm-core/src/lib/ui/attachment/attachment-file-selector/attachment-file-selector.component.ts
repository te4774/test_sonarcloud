import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';

@Component({
    selector: 'sqtm-core-attachment-file-selector',
    templateUrl: './attachment-file-selector.component.html',
    styleUrls: ['./attachment-file-selector.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AttachmentFileSelectorComponent implements OnInit {

    @Input()
    restrictionFiles: string;

    _multiple = true;

    @Input()
    set multiple(value: boolean) {
        this._multiple = value;
    }

    @ViewChild('file') file: ElementRef;

    @Output()
    uploadFilesEvent: EventEmitter<File[]> = new EventEmitter();

    @HostListener('click')
    onClick(): void {
        this.openFileBrowser();
    }

    constructor() {
    }

    ngOnInit(): void {
    }

    openFileBrowser(): void {
        (this.file.nativeElement as HTMLInputElement).click();
    }

    onFileChange(event: Event): void {
        const input = event.target as HTMLInputElement;
        this.uploadFiles(Array.from(input.files));
        input.value = '';
    }

    uploadFiles(files: File[]): void {
        this.uploadFilesEvent.emit(files);
    }

}

import {AttachmentService} from '../../../core/services/attachment.service';
import {Directive, EventEmitter, Input, Output} from '@angular/core';
import {Attachment, PersistedAttachment, RejectedAttachment} from '../../../model/attachment/attachment.model';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractAttachmentList {

  @Input()
  attachments: Attachment[];

  @Input()
  attachmentListId: number;

  @Input()
  canAttach = true;

  @Input()
  showDeleteButton = true;

  @Output()
  deleteAttachmentEvent = new EventEmitter<PersistedAttachment>();

  @Output()
  confirmDeleteAttachmentEvent = new EventEmitter<PersistedAttachment>();

  @Output()
  cancelDeleteAttachmentEvent = new EventEmitter<PersistedAttachment>();

  @Output()
  removeRejectedAttachmentEvent = new EventEmitter<RejectedAttachment>();

  delete(attachment: PersistedAttachment): void {
    this.deleteAttachmentEvent.emit(attachment);
  }

  confirm(attachment: PersistedAttachment): void {
    this.confirmDeleteAttachmentEvent.emit(attachment);
  }

  cancel(attachment: PersistedAttachment): void {
    this.cancelDeleteAttachmentEvent.emit(attachment);
  }

  removeRejectedAttachment(attachment: RejectedAttachment) {
    this.removeRejectedAttachmentEvent.emit(attachment);
  }

  protected constructor(protected attachmentService: AttachmentService) {
  }

  getAttachmentDownloadURL(attachment: PersistedAttachment) {
    return this.attachmentService.getAttachmentDownloadURL(this.attachmentListId, attachment);
  }


}

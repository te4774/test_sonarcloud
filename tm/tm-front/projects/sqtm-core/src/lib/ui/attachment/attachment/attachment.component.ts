import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Optional,
  Renderer2,
  ViewChild
} from '@angular/core';
import {debounceTime, distinctUntilChanged, map, take, takeUntil} from 'rxjs/operators';
import {Attachment, PersistedAttachment, RejectedAttachment} from '../../../model/attachment/attachment.model';
import {BehaviorSubject, fromEvent, Observable, Subject} from 'rxjs';
import {NzDrawerComponent} from 'ng-zorro-antd/drawer';
import {DOCUMENT} from '@angular/common';
import {SqtmEntityState} from '../../../core/services/entity-view/entity-view.state';
import {SimplePermissions} from '../../../model/permissions/simple-permissions';
import {GenericEntityViewService} from '../../../core/services/genric-entity-view/generic-entity-view.service';

/** @dynamic */
@Component({
  selector: 'sqtm-core-attachment',
  template: `

    <div #attachmentPanel (drop)="onFileDrop($event)" class="full-width full-height p-15 no-drag border">
      <ng-container *ngIf="attachment$|async as attachments">
        <sqtm-core-attachment-drag-area (uploadFilesEvent)="uploadFiles($event)"
                                        [canAttach]="canAttach"></sqtm-core-attachment-drag-area>
        <sqtm-core-attachment-list [attachments]="attachments.attachments"
                                   [attachmentListId]="attachments.attachmentListId"
                                   [canAttach]="canAttach"
                                   (deleteAttachmentEvent)="markToDeleteAttachment($event)"
                                   (cancelDeleteAttachmentEvent)="cancelDeleteAttachment($event)"
                                   (confirmDeleteAttachmentEvent)="confirmDeleteAttachment($event, attachments.attachmentListId)"
                                   (removeRejectedAttachmentEvent)="removeRejectedAttachment($event)"
        >
        </sqtm-core-attachment-list>
      </ng-container>
    </div>

  `,
  styleUrls: ['./attachment.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AttachmentComponent<E extends SqtmEntityState, T extends string, P extends SimplePermissions> implements OnInit, OnDestroy {

  @ViewChild('attachmentPanel', {read: ElementRef, static: true})
  attachmentPanel: ElementRef;

  unsub$ = new Subject<void>();

  attachment$: Observable<AttachmentData>;
  private dragOverSubject = new BehaviorSubject<boolean>(false);
  private dragOver$ = this.dragOverSubject.asObservable().pipe(
    distinctUntilChanged()
  );

  @Input()
  canAttach: boolean;

  @Input()
  attachmentDrawerMessage: string;

  constructor(private entityViewService: GenericEntityViewService<E, T>,
              @Inject(DOCUMENT) private document: Document,
              private ngZone: NgZone,
              private renderer: Renderer2,
              @Optional() private nzDrawer: NzDrawerComponent,
  ) {
    this.attachment$ = this.entityViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData) => {
        const attachmentList = componentData[componentData.type].attachmentList;
        const attachments: Attachment[] = Object.values(attachmentList.attachments.entities);
        return {attachmentListId: attachmentList.id, attachments};
      }),
      distinctUntilChanged()
    );
  }

  ngOnInit() {
    // We need to prevent default component in dragover event to prevent some browser to open the attachment...
    // Maybe some browser will use another event and we will have to add other handlers here
    this.ngZone.runOutsideAngular(() => {
      fromEvent(this.document.body, 'dragover').pipe(
        takeUntil(this.unsub$)
      ).subscribe(e => {
        e.preventDefault();
      });

      fromEvent(this.attachmentPanel.nativeElement, 'dragover').pipe(
        takeUntil(this.unsub$)
      ).subscribe(() => {
        this.dragOverSubject.next(true);
      });

      fromEvent(this.attachmentPanel.nativeElement, 'dragleave').pipe(
        takeUntil(this.unsub$)
      ).subscribe(event => {
        this.dragOverSubject.next(false);
      });
    });

    // prevent the drop on backdrop in nzDrawer.
    // sometime it lag and the drop is done but the behavior is like to have dropped in drop zone so.... it should be ok.
    if (this.nzDrawer) {
      const hostElement = this.nzDrawer.overlayRef.hostElement;
      fromEvent(hostElement, 'drop').pipe(
        takeUntil(this.unsub$)
      ).subscribe(event => {
        event.preventDefault();
        event.stopPropagation();
        this.dragOverSubject.next(false);
      });

    }

    // add the class to add border on all zone when dragOver
    this.dragOver$.pipe(
      debounceTime(100)
    ).subscribe((dragOver: boolean) => {
      if (this.canAttach) {
        if (dragOver) {
          this.addBorder();
        } else {
          this.removeBorder();
        }
      }
    });
  }

  onFileDrop(event: DragEvent): void {
    if (this.canAttach) {
      const files: File[] = Array.prototype.slice.call(event.dataTransfer.files);
      this.dragOverSubject.next(false);
      if (files.length) {
        this.uploadFiles(files);
      }
    }

    event.preventDefault();
    event.stopPropagation();
  }

  uploadFiles(files: File[]): void {
    this.attachment$.pipe(
      take(1)
    ).subscribe(attachmentListView => {
      this.entityViewService.addAttachments(files, attachmentListView.attachmentListId);
    });
  }

  markToDeleteAttachment(attachment: PersistedAttachment): void {
    this.entityViewService.markAttachmentsToDelete([attachment.id]);
  }

  confirmDeleteAttachment(attachment: PersistedAttachment, attachmentListId: number): void {
    this.entityViewService.deleteAttachments([attachment.id], attachmentListId);
  }

  cancelDeleteAttachment(attachment: PersistedAttachment): void {
    this.entityViewService.cancelDeleteAttachments([attachment.id]);
  }

  removeRejectedAttachment(attachment: RejectedAttachment): void {
    this.entityViewService.removeRejectedAttachments([attachment.id]);
  }

  addBorder() {
    this.renderer.addClass(this.attachmentPanel.nativeElement, 'drag-over');
    this.renderer.removeClass(this.attachmentPanel.nativeElement, 'no-drag');
  }

  removeBorder() {
    this.renderer.addClass(this.attachmentPanel.nativeElement, 'no-drag');
    this.renderer.removeClass(this.attachmentPanel.nativeElement, 'drag-over');
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.dragOverSubject.complete();
  }

}

interface AttachmentData {
  attachmentListId: number;
  attachments: Attachment[];
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {AttachmentDragAreaComponent} from './attachment-drag-area.component';
import {Pipe, PipeTransform} from '@angular/core';
import {TranslatePipe} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../testing-utils/testing-utils.module';

@Pipe({ name: 'translate'})
class TranslatePipeMock implements PipeTransform {
  transform(): any {
    return 'mock-translate-pipe';
  }
}

describe('AttachmentDragAreaComponent', () => {
  let component: AttachmentDragAreaComponent;
  let fixture: ComponentFixture<AttachmentDragAreaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule],
      declarations: [ AttachmentDragAreaComponent, TranslatePipeMock ],
      providers: [{ provide: TranslatePipe, useClass: TranslatePipeMock }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentDragAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AttachmentListComponent} from './attachment-list/attachment-list.component';
import {AttachmentDrawerComponent} from './attachment-drawer/attachment-drawer.component';
import {WorkspaceCommonModule} from '../workspace-common/workspace-common.module';
import {TranslateModule} from '@ngx-translate/core';
import {NzBadgeModule} from 'ng-zorro-antd/badge';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzDrawerModule} from 'ng-zorro-antd/drawer';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzProgressModule} from 'ng-zorro-antd/progress';
import {AttachmentDragAreaComponent} from './attachment-drag-area/attachment-drag-area.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {AttachmentComponent} from './attachment/attachment.component';
import {AttachmentListCompactComponent} from './attachment-list-compact/attachment-list-compact.component';
import {DropZoneDirective} from './directives/drop-zone.directive';
import {AttachmentFileSelectorComponent} from './attachment-file-selector/attachment-file-selector.component';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';

@NgModule({
    imports: [
        CommonModule,
        WorkspaceCommonModule,
        TranslateModule.forChild(),
        OverlayModule,
        NzBadgeModule,
        NzDrawerModule,
        NzIconModule,
        NzButtonModule,
        NzProgressModule,
        NzToolTipModule
    ],
  declarations: [
    AttachmentListComponent,
    AttachmentDrawerComponent,
    AttachmentDragAreaComponent,
    AttachmentComponent,
    AttachmentListCompactComponent,
    DropZoneDirective,
    AttachmentFileSelectorComponent
  ],
  exports: [
    AttachmentDrawerComponent,
    AttachmentListComponent,
    AttachmentListCompactComponent,
    DropZoneDirective,
    AttachmentFileSelectorComponent
  ]
})
export class AttachmentModule {
}

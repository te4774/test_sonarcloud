import {
  Directive,
  ElementRef,
  EventEmitter,
  Host,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  Type,
  ViewContainerRef
} from '@angular/core';
import {BehaviorSubject, fromEvent, Subject, timer} from 'rxjs';
import {DragAndDropService} from './drag-and-drop.service';
import {distinctUntilChanged, filter, map, switchMap, takeUntil, withLatestFrom} from 'rxjs/operators';
import {
  DndPosition,
  DragAndDropTarget,
  SqtmDragCancelEvent,
  SqtmDragLeaveEvent,
  SqtmDragOverEvent,
  SqtmDragStartEvent,
  SqtmDropEvent
} from './events';
import {DragAndDropData} from './drag-and-drop-data.model';
import {DraggedContentRenderer} from './dragged-content-renderer';
import {DefaultDraggedContentComponent} from './default-dragged-content/default-dragged-content.component';
import {DraggableDropTarget} from './draggable-drop-target';
import {dndLogger} from './sqtm-drag-and-drop.logger';

const logger = dndLogger.compose('DraggableListDirective');

@Directive({
  selector: '[sqtmCoreDraggableList]'
})
export class DraggableListDirective implements OnInit, OnDestroy, DraggableDropTarget {

  private unsub$ = new Subject<void>();
  private _scroll = new BehaviorSubject(0);
  private _stopScroll = new Subject<void>();
  private _dragOver = new Subject<DragAndDropTarget>();

  @Output()
  dragOver = new EventEmitter<SqtmDragOverEvent>();

  @Output()
  dragStart = new EventEmitter<SqtmDragStartEvent>();

  @Output()
  dragCancel = new EventEmitter<SqtmDragCancelEvent>();

  @Output()
  drop = new EventEmitter<SqtmDropEvent>();

  @Output()
  dragLeave = new EventEmitter<SqtmDragLeaveEvent>();

  @Input('sqtmCoreDraggableList')
  listId: string;

  @Input()
  dragEnabled = true;

  @Input()
  dropEnabled = true;

  @Input()
  restrictToInternalDrop = false;

  @Input()
  draggedContentRenderer: Type<DraggedContentRenderer> = DefaultDraggedContentComponent;

  constructor(@Host() private host: ElementRef, public viewContainerRef: ViewContainerRef, private dndService: DragAndDropService,
              private ngZone: NgZone) {
  }

  private readonly scrollZoneHeight = 20;
  private readonly scrollSpeedDivider = 3;

  ngOnInit(): void {
    fromEvent(this.host.nativeElement, 'mouseup').pipe(
      takeUntil(this.unsub$),
      filter(() => this.dropEnabled),
      withLatestFrom(this.dndService.dragData$),
      filter(([$event, dragData]) => this.shouldHandleDrop(dragData)),
    ).subscribe(([$event, dragData]: [MouseEvent, DragAndDropData]) => {
      $event.stopPropagation();
      this.dndService.notifyDrop(this.listId);
      this.drop.emit(new SqtmDropEvent(this.listId, dragData));
      this._stopScroll.next();
    });

    this.ngZone.runOutsideAngular(() => {
      fromEvent(document.body, 'mousemove').pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.dndService.dragData$),
        filter(([$event, dragData]) => Boolean(dragData)),
        filter(([$event, dragData]: [MouseEvent, DragAndDropData]) => {
          const boundingClientRect = (this.host.nativeElement as HTMLElement).getBoundingClientRect();
          logger.trace(JSON.stringify(boundingClientRect.toJSON()));
          logger.trace('event', [$event]);
          const outsideWidth = $event.clientX < boundingClientRect.left
            || $event.clientX > boundingClientRect.x + boundingClientRect.width;
          const outsideHeight = $event.clientY < boundingClientRect.top
            || $event.clientY > boundingClientRect.y + boundingClientRect.height;
          logger.trace(`outside width ${outsideWidth} or outside height ${outsideHeight}`);
          return outsideWidth || outsideHeight;
        })
      ).subscribe(([$event, dragData]: [MouseEvent, DragAndDropData]) => {
        this.ngZone.run(() => {
          logger.debug(`Leaving dnd list ${this.listId}`);
          this.stopScroll();
          this.dragLeave.emit(new SqtmDragLeaveEvent(this.listId, dragData));
        });
      });

      // internal usage for scroll
      fromEvent(this.host.nativeElement, 'mousemove').pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.dndService.dragAndDrop$),
        filter(([$event, dnd]) => dnd)
      ).subscribe(([$event, dnd]: [MouseEvent, boolean]) => {
        const nativeElement = this.host.nativeElement as HTMLElement;
        const rect = nativeElement.getBoundingClientRect();
        const distanceFromTop = $event.pageY - rect.top;
        const distanceFromBottom = rect.bottom - $event.pageY;
        // console.log(`DraggableListDirective ${this.listId} : Drag item over the list ${distanceFromTop} - ${distanceFromBottom}`);
        let speed: number;
        if (distanceFromTop < this.scrollZoneHeight) {
          speed = (this.scrollZoneHeight - distanceFromTop) * -1;
          this._scroll.next(speed);
        } else if (distanceFromBottom < this.scrollZoneHeight) {
          speed = this.scrollZoneHeight - distanceFromBottom;
          this._scroll.next(speed);
        } else {
          this._stopScroll.next();
        }
      });

      this._scroll.pipe(
        takeUntil(this.unsub$),
        withLatestFrom(this.dndService.dragAndDrop$),
        filter(([scrollSpeed, dnd]) => dnd),
        switchMap(([scrollSpeed, dnd]) => {
          return timer(0, 10).pipe(
            takeUntil(this._stopScroll),
            map(() => scrollSpeed / this.scrollSpeedDivider));
        })
      ).subscribe((scrollSpeed) => {
        // console.log(`scroll at speed ${scrollSpeed}`);
        const nativeElement = this.host.nativeElement as HTMLElement;
        nativeElement.scrollTo({top: nativeElement.scrollTop + scrollSpeed});
      });
    });

    this._dragOver.pipe(
      takeUntil(this.unsub$),
      filter(() => this.dropEnabled),
      distinctUntilChanged((x: DragAndDropTarget, y: DragAndDropTarget) => {
        return x.id === y.id && x.zone === y.zone;
      }),
      withLatestFrom(this.dndService.dragData$)
    ).subscribe(([dragAndDropTarget, dragData]) => {
      // If there is change in dnd target we emit inside Angular Zone so the listener shouldn't have to call Zone.run or
      // cdRef.detectChange()
      this.ngZone.run(() => {
        logger.debug(
          `DraggableListDirective : ${this.listId}. Drag item over the list ${dragAndDropTarget.id} - ${dragAndDropTarget.zone}`);
        const event = new SqtmDragOverEvent(dragAndDropTarget, dragData);
        this.dragOver.next(event);
      });
    });
    this.dndService.registerList(this);
  }

  ngOnDestroy(): void {
    this.dndService.notifyListRemoved(this.listId);
    this.unsub$.next();
    this.unsub$.complete();
    this._stopScroll.complete();
    this._scroll.complete();
  }

  notifyStartDrag(position: DndPosition, rawData?: Exclude<Object, DragAndDropData>) {
    if (this.dragEnabled) {
      const data = new DragAndDropData(this.listId, rawData);
      this.dndService.notifyDragStart(data, position);
      this.dragStart.next(new SqtmDragStartEvent(data, position));
      this.stopScroll();
    }
  }

  notifyDragOverItem(dragAndDropTarget: DragAndDropTarget) {
    this.ngZone.runOutsideAngular(() => {
      this._dragOver.next(dragAndDropTarget);
    });
  }

  notifyCancelDrag(dragData: DragAndDropData) {
    this.dragCancel.emit(new SqtmDragCancelEvent(dragData));
  }

  private stopScroll() {
    this._stopScroll.next();
  }

  private shouldHandleDrop(dragData: DragAndDropData | undefined): boolean {
    if (! Boolean(dragData)) {
      return false;
    }

    if (this.restrictToInternalDrop) {
      return this.dropEnabled && dragData.origin === this.listId;
    }

    return this.dropEnabled;
  }
}

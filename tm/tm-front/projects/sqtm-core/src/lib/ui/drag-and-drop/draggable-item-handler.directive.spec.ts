import {DraggableItemHandlerDirective} from './draggable-item-handler.directive';
import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {DraggableListDirective} from './draggable-list.directive';
import {DraggableListItemDirective} from './draggable-list-item.directive';
import {OverlayModule} from '@angular/cdk/overlay';

@Component({
  selector: 'sqtm-core-drag-and-drop-handler-testing',
  template: `
      <div sqtmCoreDraggableItemHandler></div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DragAndDropItemHandlerTestingComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}

describe('DraggableItemHandlerDirective', () => {

  let component: DragAndDropItemHandlerTestingComponent;
  let fixture: ComponentFixture<DragAndDropItemHandlerTestingComponent>;

  const dndItem = jasmine.createSpyObj(['registerHandler']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [OverlayModule],
      declarations: [DragAndDropItemHandlerTestingComponent, DraggableItemHandlerDirective],
      providers: [
        {provide: DraggableListDirective, useValue: {}},
        {provide: DraggableListItemDirective, useValue: dndItem},
      ]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(DragAndDropItemHandlerTestingComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  }));

  it('should create the dnd handler with directive and register into item', () => {
    expect(dndItem.registerHandler).toHaveBeenCalledTimes(1);
    expect(component).toBeTruthy();
  });
});

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DraggableListItemDirective} from './draggable-list-item.directive';
import {DraggableListDirective} from './draggable-list.directive';
import {DragAndDropDisableSelectionDirective} from './drag-and-drop-disable-selection.directive';
import {DraggableItemHandlerDirective} from './draggable-item-handler.directive';
import {DefaultDraggedContentComponent} from './default-dragged-content/default-dragged-content.component';
import {DraggableDropZoneDirective} from './draggable-drop-zone.directive';


@NgModule({
    declarations: [
        DraggableListItemDirective,
        DraggableListDirective,
        DragAndDropDisableSelectionDirective,
        DraggableItemHandlerDirective,
        DefaultDraggedContentComponent,
        DraggableDropZoneDirective
    ],
    exports: [
        DraggableListItemDirective,
        DraggableListDirective,
        DragAndDropDisableSelectionDirective,
        DraggableItemHandlerDirective,
        DraggableDropZoneDirective
    ],
    imports: [
        CommonModule
    ]
})
export class SqtmDragAndDropModule {
}

import {ElementRef} from '@angular/core';
import {fromEvent, Observable} from 'rxjs';
import {filter, map, scan, take, takeUntil} from 'rxjs/operators';
import {DndPosition} from './events';

export function createStartDragObservable(elementRef: ElementRef, position: DndPosition): Observable<DndPosition> {

  const mouseUpStream = fromEvent(document.body, 'mouseup').pipe(
    take(1),
  );

  return fromEvent(document.body, 'mousemove').pipe(
    takeUntil(mouseUpStream),
    scan((dndMoveTrack: DndMoveTrack, event: MouseEvent) => {
      if (Boolean(dndMoveTrack)) {
        dndMoveTrack.updatePosition(event);
        return dndMoveTrack;
      }
      return new DndMoveTrack(event.screenX, event.screenY);
    }, null),
    filter(dndMoveTrack => dndMoveTrack.shouldTriggerDnd()),
    take(1),
    map(() => position)
  );

}

class DndMoveTrack {
  private xCurrent?: number;
  private yCurrent?: number;

  private readonly threshold = 15;

  constructor(private readonly xStart, private readonly yStart) {
  }

  updatePosition($event: MouseEvent) {
    this.xCurrent = $event.screenX;
    this.yCurrent = $event.screenY;
  }

  shouldTriggerDnd(): boolean {
    let trigger = false;
    if (Boolean(this.xCurrent) && (Boolean(this.yCurrent))) {
      const xDistance = Math.abs(this.xStart - this.xCurrent);
      const yDistance = Math.abs(this.yStart - this.yCurrent);
      const distance = Math.max(xDistance, yDistance);
      trigger = distance > this.threshold;
    }
    return trigger;
  }
}

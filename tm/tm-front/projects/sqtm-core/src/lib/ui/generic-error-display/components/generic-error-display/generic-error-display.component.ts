import {ChangeDetectionStrategy, Component, OnDestroy, TemplateRef, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {
  ErrorMessageEvent,
  GenericErrorDisplayService
} from '../../../../core/services/errors-handling/generic-error-display.service';
import {takeUntil} from 'rxjs/operators';
import {NzNotificationService} from 'ng-zorro-antd/notification';
import {DialogService} from '../../../dialog/services/dialog.service';

@Component({
  selector: 'sqtm-core-generic-error-display',
  template: `
    <ng-template #errorMessage let-data="data">
      <div class="flex-row overflow-hidden"
           [attr.data-test-component-id]="'generic-error-message'">
        <i nz-icon
           nzType="exclamation-circle"
           nzTheme="twotone"
           [nzTwotoneColor]="'#FF0000'"
           class="icon">
        </i>
        <div>
          {{'sqtm-core.error.generic.label'|translate}} (<span
            class="link"
            [attr.data-test-component-id]="'generic-error-message-details-link'"
            (click)="showErrorDialog(data)">{{'sqtm-core.generic.label.detail.plural'|translate}}</span>)
        </div>
      </div>
    </ng-template>
  `,
  styleUrls: ['./generic-error-display.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GenericErrorDisplayComponent implements OnDestroy {

  @ViewChild('errorMessage')
  templateRef: TemplateRef<any>;

  private readonly unsub$ = new Subject<void>();

  constructor(private readonly defaultErrorService: GenericErrorDisplayService,
              private readonly dialogService: DialogService,
              private readonly nzNotification: NzNotificationService) {
    this.defaultErrorService.errorMessage$
      .pipe(takeUntil(this.unsub$))
      .subscribe((error) => this.showErrorNotification(error));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  showErrorNotification(error: ErrorMessageEvent): void {
    this.nzNotification.template(this.templateRef, {
      nzDuration: 10000,
      nzData: error,
    });
  }

  showErrorDialog(error: ErrorMessageEvent): void {
    if (error.stackTraceEnabled) {
      GenericErrorDisplayComponent.openStackTracePopup(error);
    } else {
      this.openGenericAlert();
    }
  }

  private static openStackTracePopup(error: ErrorMessageEvent): void {
    const popup = window.open(
      'about:blank',
      'error_details',
      'resizable=yes, scrollbars=yes, status=no, menubar=no, toolbar=no, dialog=yes, location=no');
    popup.document.write(error.error.trace);
  }

  private openGenericAlert(): void {
    this.dialogService.openAlert({
      id: 'generic-error-message',
      level: 'DANGER',
      messageKey: 'sqtm-core.generic.label.exception.message',
    });
  }
}

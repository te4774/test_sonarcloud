import {ComponentFixture, TestBed} from '@angular/core/testing';

import {GenericErrorDisplayComponent} from './generic-error-display.component';
import {NzNotificationModule} from 'ng-zorro-antd/notification';
import {DialogService} from '../../../dialog/services/dialog.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('GenericErrorDisplayComponent', () => {
  let component: GenericErrorDisplayComponent;
  let fixture: ComponentFixture<GenericErrorDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NzNotificationModule],
      declarations: [GenericErrorDisplayComponent],
      providers: [
        {provide: DialogService, useValue: jasmine.createSpyObj<DialogService>(['openAlert'])}
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericErrorDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

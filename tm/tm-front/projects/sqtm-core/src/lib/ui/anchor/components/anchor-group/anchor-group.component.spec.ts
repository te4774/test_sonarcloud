import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {AnchorGroupComponent} from './anchor-group.component';
import {ViewIdDirective} from '../../directives/view-id.directive';
import {AnchorService} from '../../service/anchor.service';
import {Observable, of} from 'rxjs';


describe('AnchorGroupComponent', () => {
  let component: AnchorGroupComponent;
  let fixture: ComponentFixture<AnchorGroupComponent>;

  class DummyAnchorService {
    state$ = new Observable();

    getActiveGroup(): Observable<string> {
      return of('group-id');
    }
  }

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AnchorGroupComponent],
      providers: [AnchorService,
        {provide: ViewIdDirective, useValue: {}},
        {provide: AnchorService, useClass: DummyAnchorService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnchorGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

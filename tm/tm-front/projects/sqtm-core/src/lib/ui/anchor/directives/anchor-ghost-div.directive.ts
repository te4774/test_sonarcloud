import {AfterViewInit, Directive, ElementRef, Host} from '@angular/core';
import {AnchorResizeDirective} from './anchor-resize.directive';

@Directive({
  selector: '[sqtmCoreAnchorGhostDiv]'
})
export class AnchorGhostDivDirective implements AfterViewInit {

  constructor(@Host() private elementRef: ElementRef, private anchorResizeDirective: AnchorResizeDirective) {
  }


  ngAfterViewInit(): void {
    this.anchorResizeDirective.registerGhostDiv(this.elementRef);
  }
}

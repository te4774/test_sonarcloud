import {ChangeDetectionStrategy, Component, ViewContainerRef} from '@angular/core';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {TranslateService} from '@ngx-translate/core';
import {DialogConfiguration} from '../../../dialog/services/dialog-feature.state';
import {NamedReference} from '../../../../model/named-reference';

const DIALOG_ID = 'issue-verified-requirement-versions-dialog';

@Component({
  selector: 'sqtm-core-issue-verified-requirement-versions-dialog',
  templateUrl: './issue-verified-requirement-versions-dialog.component.html',
  styleUrls: ['./issue-verified-requirement-versions-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueVerifiedRequirementVersionsDialogComponent {

  readonly dialogId = DIALOG_ID;
  readonly data: DialogData;

  get requirementVersions(): NamedReference[] {
    return this.data.requirementVersions;
  }

  constructor(public readonly dialogRef: DialogReference<DialogData>,
              public readonly translateService: TranslateService) {
    this.data = dialogRef.data;
  }

  getLabel(requirementVersion: NamedReference): string {
    return requirementVersion.name;
  }

  getPath(requirementVersion: NamedReference): any[] {
    return ['/requirement-workspace/requirement-version/detail', requirementVersion.id];
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}

interface DialogData {
  requirementVersions: NamedReference[];
}

export function getIssueVerifiedRequirementVersionsDialogConf(requirementVersions: NamedReference[],
                                                              viewContainerRef: ViewContainerRef): DialogConfiguration<DialogData> {
  return {
    id: DIALOG_ID,
    data: {requirementVersions},
    component: IssueVerifiedRequirementVersionsDialogComponent,
    viewContainerReference: viewContainerRef,
  };
}

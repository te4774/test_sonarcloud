import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewContainerRef
} from '@angular/core';
import {BugtrackerConnectDialogComponent} from '../bugtracker-connect-dialog/bugtracker-connect-dialog.component';
import {Subject} from 'rxjs';
import {BugTracker} from '../../../../model/bugtracker/bug-tracker.model';
import {DialogService} from '../../../dialog/services/dialog.service';
import {AuthenticationProtocol} from '../../../../model/third-party-server/authentication.model';
import {IssuesService} from '../../service/issues.service';
import {InterWindowCommunicationService} from '../../../../core/services/inter-window-communication.service';
import {filter, take, takeUntil} from 'rxjs/operators';
import {BugtrackerConnectConfiguration} from '../bugtracker-connect-dialog/bugtracker-connect-configuration';

@Component({
  selector: 'sqtm-core-issues-connect',
  template: `
    <label>{{'sqtm-core.issue-panel.need-credentials' | translate}}
      <button nz-button nzType="primary" [attr.data-test-button-id]="'login'"
              (click)="openDialog()">{{'sqtm-core.generic.label.to-log-in' | translate}}</button>
    </label>
  `,
  styleUrls: ['./issues-connect.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssuesConnectComponent implements OnDestroy {

  @Input()
  bugTracker: BugTracker;

  @Output()
  connectSuccess = new EventEmitter<void>();

  unsub$ = new Subject<void>();

  constructor(private readonly dialogService: DialogService,
              private readonly issuesService: IssuesService,
              private readonly vcr: ViewContainerRef,
              private readonly interWindowCommunicationService: InterWindowCommunicationService) {
  }

  openDialog() {
    if (this.bugTracker.authProtocol === AuthenticationProtocol.OAUTH_1A) {
      this.openOAuthDialog();
    } else {
      this.openStandardAuthDialog();
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private openStandardAuthDialog(): void {
    const dialogRef = this.dialogService.openDialog<BugtrackerConnectConfiguration, boolean>({
      id: BugtrackerConnectDialogComponent.DIALOG_ID,
      viewContainerReference: this.vcr,
      component: BugtrackerConnectDialogComponent,
      data: {bugTracker: this.bugTracker}
    });

    dialogRef.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      filter((result) => Boolean(result))
    ).subscribe(() => {
      this.connectSuccess.emit();
    });
  }

  private openOAuthDialog(): void {
    this.issuesService.openOAuthDialog(this.bugTracker.id);
    this.interWindowCommunicationService.interWindowMessages$.pipe(
      takeUntil(this.unsub$),
      filter(message => message.isTypeOf('OAUTH-CONNECT-SUCCESS')),
      take(1),
    ).subscribe(() => {
      this.connectSuccess.emit();
    });
  }
}

import {ChangeDetectionStrategy, Component, ViewContainerRef} from '@angular/core';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {DialogConfiguration} from '../../../dialog/services/dialog-feature.state';
import {TranslateService} from '@ngx-translate/core';

const DIALOG_ID = 'issue-report-source-steps-dialog';

@Component({
  selector: 'sqtm-core-issue-report-source-steps-dialog',
  templateUrl: './issue-report-source-steps-dialog.component.html',
  styleUrls: ['./issue-report-source-steps-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueReportSourceStepsDialogComponent {

  readonly dialogId = DIALOG_ID;
  readonly data: DialogData;

  constructor(public readonly dialogRef: DialogReference<DialogData>,
              public readonly translateService: TranslateService) {
    this.data = dialogRef.data;
  }

  getLabel(stepOrder: number): string {
    if (stepOrder >= 0) {
      const oneBasedStepOrder = stepOrder + 1;
      const stepLabel = this.translateService.instant('sqtm-core.campaign-workspace.execution-page.step.label');
      return `${stepLabel} ${oneBasedStepOrder}`;
    } else {
      return this.translateService.instant('sqtm-core.campaign-workspace.execution-page.this-execution.label');
    }
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}

interface DialogData {
  executionSteps: number[];
}

export function getIssueReportSourceStepsDialogConf(executionSteps: number[],
                                                    viewContainerRef: ViewContainerRef): DialogConfiguration<DialogData> {
  return {
    id: DIALOG_ID,
    data: { executionSteps },
    component: IssueReportSourceStepsDialogComponent,
    viewContainerReference: viewContainerRef,
    width: 400,
    minWidth: 400,
  };
}

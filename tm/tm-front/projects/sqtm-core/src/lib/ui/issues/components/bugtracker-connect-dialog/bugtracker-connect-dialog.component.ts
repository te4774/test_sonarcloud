import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {BugTrackerAuthentication, IssuesService} from '../../service/issues.service';
import {BugtrackerConnectConfiguration} from './bugtracker-connect-configuration';
import {Subject} from 'rxjs';
import {take} from 'rxjs/operators';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {AuthenticationProtocol} from '../../../../model/third-party-server/authentication.model';

@Component({
  selector: 'sqtm-core-bugtracker-connect-dialog',
  templateUrl: './bugtracker-connect-dialog.component.html',
  styleUrls: ['./bugtracker-connect-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BugtrackerConnectDialogComponent implements OnDestroy {

  public static DIALOG_ID = 'bugtracker-connection';
  public readonly dialogId = BugtrackerConnectDialogComponent.DIALOG_ID;

  private unsub$ = new Subject<void>();

  formGroup: FormGroup;
  configuration: BugtrackerConnectConfiguration;
  connectionError: boolean;

  get authProtocol(): AuthenticationProtocol {
    return this.configuration.bugTracker.authProtocol;
  }

  get usesUsernameField(): boolean {
    return [
      AuthenticationProtocol.BASIC_AUTH
    ].includes(this.authProtocol);
  }

  get passwordFieldLabel(): string {
    return this.authProtocol === AuthenticationProtocol.BASIC_AUTH ? 'sqtm-core.generic.label.password'
      : 'sqtm-core.administration-workspace.bugtrackers.authentication-policy.credentials.token';
  }

  constructor(private dialogReference: DialogReference<BugtrackerConnectConfiguration, boolean>,
              private issuesService: IssuesService,
              private cdRef: ChangeDetectorRef) {
    this.formGroup = new FormGroup({
      username: new FormControl(''),
      password: new FormControl('')
    });

    this.configuration = this.dialogReference.data;
  }

  connect() {
    const authentication: BugTrackerAuthentication = {
      ...this.getPayload(),
      type: this.configuration.bugTracker.authProtocol
    };

    this.issuesService.connect(this.configuration.bugTracker.id, authentication)
      .pipe(take(1))
      .subscribe(() => this.handleConnectionSuccess(),
        (error) => this.handleConnectionError(error));
  }

  private getPayload(): any {
    switch (this.authProtocol) {
      case AuthenticationProtocol.TOKEN_AUTH:
        return { token: this.formGroup.controls['password'].value };
      case AuthenticationProtocol.BASIC_AUTH:
        return { ...this.formGroup.value };
      default:
        throw new Error('Auth protocol "' + this.authProtocol + '" is not handled by BugtrackerConnectDialogComponent.');
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private handleConnectionSuccess(): void {
    this.dialogReference.result = true;
    this.dialogReference.close();
  }

  private handleConnectionError(error: any): void {
    console.error(error);
    this.connectionError = true;
    this.cdRef.detectChanges();
  }
}

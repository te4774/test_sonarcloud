import {ComponentFixture, TestBed} from '@angular/core/testing';

import {IssueVerifiedRequirementVersionsDialogComponent} from './issue-verified-requirement-versions-dialog.component';
import {TestingUtilsModule} from '../../../testing-utils/testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {DialogReference} from '../../../dialog/model/dialog-reference';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('IssueVerifiedRequirementVersionsDialogComponent', () => {
  let component: IssueVerifiedRequirementVersionsDialogComponent;
  let fixture: ComponentFixture<IssueVerifiedRequirementVersionsDialogComponent>;

  beforeEach(async () => {
    const dialogRef: any = jasmine.createSpyObj<DialogReference>(['close']);
    dialogRef.data = { requirementVersions: [] };

    await TestBed.configureTestingModule({
      imports: [TestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      declarations: [ IssueVerifiedRequirementVersionsDialogComponent ],
      providers: [
        { provide: DialogReference, useValue: dialogRef },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueVerifiedRequirementVersionsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

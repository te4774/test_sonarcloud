import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GridExportMenuComponent} from './components/grid-export-menu/grid-export-menu.component';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [
    GridExportMenuComponent
  ],
  exports: [
    GridExportMenuComponent
  ],
  imports: [
    CommonModule,
    NzIconModule,
    NzDropDownModule,
    TranslateModule
  ]
})
export class GridExportModule {
}

import {Injectable, Provider} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {getIssueReportSiteAsString} from '../../../model/issue/issue-report-site.model';
import {UsersGroupHelpers} from '../../../model/user/users-group';
import {DatePipe} from '@angular/common';
import {getSupportedBrowserLang} from '../../../core/utils/browser-langage.utils';

export interface ValueRenderer {
  render(value: any, data?: { [K: string]: any }): string;
}

@Injectable()
export class PassThroughValueRenderer implements ValueRenderer {
  render(value: any): string {
    return value;
  }
}

@Injectable()
export class BooleanValueRenderer implements ValueRenderer {
  constructor(private translateService: TranslateService) {
  }

  render(value: any): string {
    const i18nKey = Boolean(value) ? 'sqtm-core.generic.label.yes' : 'sqtm-core.generic.label.no';
    return this.translateService.instant(i18nKey);
  }
}

@Injectable()
export class I18nValueRenderer implements ValueRenderer {
  constructor(private translateService: TranslateService) {
  }

  render(value: any): string {
    return value;
  }
}

@Injectable()
export class IssueExecutionsExportValueRenderer implements ValueRenderer {
  constructor(private translateService: TranslateService) {
  }

  render(value: any): string {
    if (value.length > 1) {
      const result: string[] = [];
      value.forEach(execution => result.push(getIssueReportSiteAsString(execution, this.translateService)));
      return result.join(', ');
    }
    return getIssueReportSiteAsString(value[0], this.translateService);
  }
}

@Injectable()
export class IssueReportedInValueRenderer implements ValueRenderer {
  constructor(private translateService: TranslateService) {
  }

  render(value: any, data: { [K: string]: any }): string {
    const executionSteps = data.executionSteps;
    if (executionSteps.length > 1) {
      return this.translateService.instant('sqtm-core.campaign-workspace.execution-page.multiple-sources');
    } else {
      const stepOrder: number = executionSteps[0];
      if (stepOrder >= 0) {
        const oneBasedStepOrder = stepOrder + 1;
        const stepLabel = this.translateService.instant('sqtm-core.campaign-workspace.execution-page.step.label');
        return `${stepLabel} ${oneBasedStepOrder}`;
      } else {
        return this.translateService.instant('sqtm-core.campaign-workspace.execution-page.this-execution.label');
      }
    }
  }
}

@Injectable()
export class IssueRequirementsValueRenderer implements ValueRenderer {
  render(value: any, data: { [K: string]: any }): string {
    const verifiedRequirementVersions = data.verifiedRequirementVersions;
    if (verifiedRequirementVersions.length > 1) {
      const result: string[] = [];
      verifiedRequirementVersions.forEach(requirementVersion => result.push(requirementVersion?.name ?? '-'));
      return result.join(', ');
    }
    const firstRequirementVersion = verifiedRequirementVersions[0];
    return firstRequirementVersion?.name ?? '-';
  }
}

@Injectable()
export class IssueKeyExportValueRenderer implements ValueRenderer {
  constructor() {
  }

  render(value: any, data: { [K: string]: any }): string {
    return data['remoteKey'];
  }
}

@Injectable()
export class UserGroupExportValueRenderer implements ValueRenderer {
  constructor(private translateService: TranslateService) {
  }

  render(value: any): string {
    return this.translateService.instant(UsersGroupHelpers.getI18nKeyFromQualifiedName(value));
  }
}

@Injectable()
export class DateExportValueRenderer extends DatePipe implements ValueRenderer {
  private format = 'shortDate';

  constructor(private translateService: TranslateService) {
    super(getSupportedBrowserLang(translateService));
  }

  render(value: any): string {
    if (value == null) {
      return '-';
    } else {
      return super.transform(value, this.format, '', getSupportedBrowserLang(this.translateService));
    }
  }
}

@Injectable()
export class DateTimeExportValueRenderer extends DatePipe implements ValueRenderer {
  private format = 'short';

  constructor(private translateService: TranslateService) {
    super(getSupportedBrowserLang(translateService));
  }

  render(value: any): string {
    if (value == null) {
      return '-';
    } else {
      return super.transform(value, this.format, '', getSupportedBrowserLang(this.translateService))
        .replace(',', '');
    }
  }
}

export const gridExportValueRenderers: Provider[] = [
  PassThroughValueRenderer,
  I18nValueRenderer,
  BooleanValueRenderer,
  IssueExecutionsExportValueRenderer,
  IssueReportedInValueRenderer,
  IssueKeyExportValueRenderer,
  IssueRequirementsValueRenderer,
  UserGroupExportValueRenderer,
  DateExportValueRenderer,
  DateTimeExportValueRenderer
];



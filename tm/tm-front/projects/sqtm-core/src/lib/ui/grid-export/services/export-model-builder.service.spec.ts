import {TestBed} from '@angular/core/testing';

import {ExportColumnDefinition, ExportModelBuilderService} from './export-model-builder.service';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {GridService} from '../../grid/services/grid.service';
import {concatMap, filter, take} from 'rxjs/operators';
import {grid} from '../../../model/grids/grid-builders';
// tslint:disable-next-line:import-blacklist
import {CORE_MODULE_CONFIGURATION} from '../../../core/sqtm-core.tokens';
// tslint:disable-next-line:import-blacklist
import {defaultSqtmConfiguration} from '../../../core/sqtm-core.module';
import {APP_BASE_HREF} from '@angular/common';
import {GridDefinition} from '../../grid/model/grid-definition.model';
import {gridServiceFactory} from '../../grid/grid.service.provider';
import {RestService} from '../../../core/services/rest.service';
import {ReferentialDataService} from '../../../core/referential/services/referential-data.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DataRow} from '../../grid/model/data-row.model';
import {booleanColumn, textColumn} from '../../grid/model/common-column-definition.builders';
import {gridExportValueRenderers} from '../value-renderer/value-renderer';

/**
 * JTH 31/03/2022 : Not using mocks here, I used the true services.
 * It was a sort of experiment and in fact it is probably a better way to test than using mocks...
 */
describe('ExportModelBuilderService', () => {
  let service: ExportModelBuilderService;
  let gridService: GridService;
  let translateService: TranslateService;

  function defaultGridConfFactory() {
    return grid('grid-test').withColumns([
      textColumn('name').withTitleI18nKey('test.column.name.title'),
      booleanColumn('isTemplate'),
    ]).build();
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        HttpClientTestingModule
      ],
      providers: [
        {provide: CORE_MODULE_CONFIGURATION, useValue: defaultSqtmConfiguration},
        {provide: APP_BASE_HREF, useValue: ''},
        {
          provide: GridDefinition,
          useFactory: defaultGridConfFactory
        },
        {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        },
        ExportModelBuilderService,
        gridExportValueRenderers
      ]
    });
    service = TestBed.inject(ExportModelBuilderService);
    gridService = TestBed.inject(GridService);
    translateService = TestBed.inject(TranslateService);
    const browserLang = translateService.getBrowserLang();
    translateService.addLangs([browserLang]);
    translateService.setTranslation(browserLang,
      {
        'test.column.name.title': 'Nom du Projet',
        'sqtm-core.generic.label.yes': 'Vrai'
      }
    );
    translateService.use(browserLang);
  });

  it('should build export model', (done) => {
    gridService.loaded$.pipe(
      filter(loaded => loaded),
      concatMap(() => service.buildModelAllRows()),
      take(1)
    ).subscribe(exportModel => {
      expect(exportModel).toBeTruthy();
      expect(exportModel.exportedRows.length).toEqual(2);
      expect(exportModel.exportedColumns.length).toEqual(2);
      const nameColumn: ExportColumnDefinition = exportModel.exportedColumns[0];
      expect(nameColumn.label).toEqual('Nom du Projet');
      expect(nameColumn.id).toEqual('name');
      const firstRow: DataRow = exportModel.exportedRows[0];
      expect(firstRow.data['name']).toEqual('Project-1');
      expect(firstRow.data['isTemplate']).toEqual('Vrai');
      done();
    });

    const rows: Partial<DataRow>[] = [
      {
        id: '1',
        data: {'name': 'Project-1', 'isTemplate': true}
      }, {
        id: '2',
        data: {'name': 'Project-2', 'isTemplate': false}
      }
    ];
    gridService.loadInitialDataRows(rows, 1);
  });
});

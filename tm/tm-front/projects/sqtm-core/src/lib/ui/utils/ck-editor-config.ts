import {CKEditor4} from 'ckeditor4-angular';


export function createCkEditorConfig(browserLang: string): CKEditor4.Config {
  return {
    language: browserLang,

    toolbar: 'Squash',
    skin: 'moono-lisa',
    toolbarCanCollapse: true,
    toolbar_Squash:
      [
        ['Bold', 'Italic', 'Underline', 'Strike', 'NumberedList', 'BulletedList'],
        ['Link'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['TextColor'], ['Font'], ['FontSize'],
        ['Scayt'],
        ['Table', 'Image'],
        ['Maximize']
      ],
    height: 200,
    resize_minHeight: 175,
    resize_minWidth: 2001,
    removePlugins: 'elementspath',
    extraPlugins: 'onchange'
  };
}

// Static flag to avoid adding multiple listeners.
let dialogDefinitionListenerAdded = false;

export function overrideCKEditorDefaultLinkTarget() {
  if (dialogDefinitionListenerAdded) {
    return;
  }
  const ckeSingleton = window['CKEDITOR'];

  if (ckeSingleton) {
    ckeSingleton.on('dialogDefinition', handleDialogDefinition);
    dialogDefinitionListenerAdded = true;
  } else {
    console.error('Could not access CKEDITOR singleton.');
  }
}

function handleDialogDefinition(ev: any) {
  try {
    const dialogName = ev.data.name;
    const dialogDefinition = ev.data.definition;
    if (dialogName === 'link') {
      const informationTab = dialogDefinition.getContents('target');
      const targetField = informationTab.get('linkTargetType');
      targetField['default'] = '_blank';
    }
  } catch (exception) {
    console.error('Error while preparing CKEditor : ' + ev.message);
  }
}

import {TestBed} from '@angular/core/testing';

import {ReportDefinitionService} from './report-definition.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../ui/testing-utils/testing-utils.module';

describe('ReportDefinitionService', () => {
  let service: ReportDefinitionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule]
    });
    service = TestBed.inject(ReportDefinitionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import {AttachmentListModel} from '../../../model/attachment/attachment-list.model';
import {ProjectData} from '../../../model/project/project-data.model';
import {GlobalConfiguration} from '../../referential/state/global-configuration.state';
import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {Attachment} from '../../../model/attachment/attachment.model';
import {SimplePermissions} from '../../../model/permissions/simple-permissions';
import {CustomFieldValue, CustomFieldValueModel} from '../../../model/customfield/custom-field-value.model';
import {CustomFieldData} from '../../../model/customfield/customfield.model';
import {BindableEntity} from '../../../model/bindable-entity.model';
import {SqtmGenericEntityState} from '../genric-entity-view/generic-entity-view-state';
import {Milestone} from '../../../model/milestone/milestone.model';

export type PartialEntityViewState<S extends SqtmEntityState, T extends string> = { [K in T]: S }
  & { type: T };

export type EntityViewState<S extends SqtmEntityState, T extends string> =
  PartialEntityViewState<S, T>
  & { uiState: UiState<S> };

// The entity MODEL represent the shape of the data server side.
// Ex for a TestCase you will have all TestCase Attributes, the Steps, the Coverages...
export interface EntityModel {
  id: number;
  projectId: number;
  attachmentList: AttachmentListModel;
  customFieldValues: CustomFieldValueModel[];
}

// The entity STATE represent the shape of data optimized for State storage client side :
// - Collections use the NgRx EntityState pattern to preserve performance and item order...
// - Some part of the MODEL can be held by other service (GirdService...), so parts of Entity MODEL can be removed from Entity STATE.
// Ex: For a TestCase :
// - Attachment array must be converted to an optimized EntityState<Attachment>
// - TestStep, Coverages... are managed into their respective grids. A good pattern in State management is to avoid State duplication, so
// Steps and Coverage will not be copied into TestCase STATE.
export interface SqtmEntityState extends SqtmGenericEntityState {
  id: number;
  projectId: number;
  attachmentList: AttachmentListState;
  customFieldValues: CustomFieldValueState;
  milestones?: Milestone[];
  // Only used for requirement versions
  statusAllowModification?: boolean;
}

export interface CustomFieldValueState extends EntityState<CustomFieldValue> {
}

export interface UiState<S extends SqtmEntityState> {
  fieldState: FieldsState<S>;
}

export interface FieldsState<S extends SqtmEntityState> extends EntityState<EditableFieldState<S>> {

}

export interface EditableFieldState<S extends SqtmEntityState> {
  status: EditableFieldStatus;
  errorMsgKey: string[];
  errorMsg: string[];
  id: keyof S;
}

export interface AttachmentListState {
  id: number;
  attachments: AttachmentState;
}

export interface AttachmentState extends EntityState<Attachment> {
}

export type EditableFieldStatus = 'VALID' | 'SERVER_ERROR';

// Have to define the partial hard way.
// Because with Generics, TypeScript mapped types (aka Partial, Exclude...) can't track the { [K in T]: E } part of the object
// and thus lead to compilation errors.
export type PartialEntityViewComponentData<S extends SqtmEntityState, T extends string, P extends SimplePermissions> =
  EntityViewState<S, T>
  & { type: T }
  & { projectData: ProjectData }
  & { permissions: P }
  & { milestonesAllowModification?: boolean }
  & { statusAllowModification?: boolean };

// Full component data payload (aka EntityState, projectData and globalConfigurationData)
export type EntityViewComponentData<S extends SqtmEntityState, T extends string, P extends SimplePermissions> =
  PartialEntityViewComponentData<S, T, P>
  & { globalConfiguration: GlobalConfiguration };

// return an initial state like {execution:ExecutionState}
export function provideInitialViewState<S extends SqtmEntityState, T extends string>(type: T): EntityViewState<S, T> {
  const base = {type};
  base[type as string] = null;
  return {
    ...base as PartialEntityViewState<S, T>,
    uiState: {fieldState: {ids: [], entities: {}}},
    statusAllowModification: true
  };
}

export const uiStateSelector = createFeatureSelector<UiState<any>>('uiState');
export const fieldStateSelector = createSelector(uiStateSelector, (uiState: UiState<any>) => uiState.fieldState);
export const createFieldSelector = (name: string) => {
  return createSelector(fieldStateSelector, (fieldState: FieldsState<any>) => fieldState.entities[name]);
};

export const permissionsSelector = createFeatureSelector<SimplePermissions>('permissions');
export const projectDataSelector = createFeatureSelector<ProjectData>('projectData');

export const entitySelector = (componentData: EntityViewState<any, any>) => componentData[componentData.type];

export const customFieldValueSelector = createSelector<EntityViewComponentData<any, any, any>, any, CustomFieldValueState>(
  entitySelector, (entity: SqtmEntityState) => entity.customFieldValues);

export const createCustomFieldValueDataSelector = (bindableEntity: BindableEntity) => {
  return createSelector<any, any, CustomFieldData[]>(customFieldValueSelector, projectDataSelector,
    (customFieldValueState: CustomFieldValueState, projectData: ProjectData) => {
      const cfvMap: Map<number, CustomFieldValue> = Object.values(customFieldValueState.entities).reduce((map, cfv) => {
        map.set(cfv.cufId, cfv);
        return map;
      }, new Map());

      return projectData.customFieldBinding[bindableEntity].map(binding => {
        const customFieldValue = cfvMap.get(binding.customField.id);
        return {
          ...binding.customField,
          cfvId: customFieldValue ? customFieldValue.id : undefined,
          value: customFieldValue ? customFieldValue.value : '',
          position: binding.position,
        };
      }).sort((cuf1, cuf2) => cuf1.position - cuf2.position);
    });
};

export const createCustomFieldValueSelector = (cfvId: number) => {
  return createSelector<any, any, CustomFieldValue>(customFieldValueSelector, (customFieldValueState: CustomFieldValueState) => {
    return customFieldValueState.entities[cfvId];
  });
};

export const attachmentEntityAdapter = createEntityAdapter<Attachment>();
export const customFieldValuesEntityAdapter = createEntityAdapter<CustomFieldValue>();

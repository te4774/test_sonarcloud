import {TestBed} from '@angular/core/testing';

import {GridPersistenceService} from './grid-persistence.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../ui/testing-utils/testing-utils.module';

describe('GridStatePersistenceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, TestingUtilsModule]
  }));

  it('should be created', () => {
    const service: GridPersistenceService = TestBed.inject(GridPersistenceService);
    expect(service).toBeTruthy();
  });
});

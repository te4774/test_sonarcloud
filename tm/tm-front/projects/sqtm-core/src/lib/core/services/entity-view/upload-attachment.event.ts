import {PersistedAttachment, RejectedAttachment, UploadingAttachment} from '../../../model/attachment/attachment.model';

export interface UploadAttachmentEvent {
  readonly kind: 'start' | 'progress' | 'complete';
}

export class StartUpload implements UploadAttachmentEvent {
  readonly kind: 'start' = 'start';

  constructor(public attachments: (UploadingAttachment | RejectedAttachment) []) {
  }
}

export class UploadProgress implements UploadAttachmentEvent {
  readonly kind: 'progress' = 'progress';

  constructor(public attachments: (Pick<UploadingAttachment, 'id' | 'uploadProgress'>) []) {

  }
}

export class UploadComplete implements UploadAttachmentEvent {
  readonly kind: 'complete' = 'complete';

  constructor(public attachments: PersistedAttachment [], public uploadingAttachmentsToDelete: string[]) {
  }
}

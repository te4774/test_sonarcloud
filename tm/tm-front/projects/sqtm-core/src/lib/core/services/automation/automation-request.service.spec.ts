import {TestBed} from '@angular/core/testing';

import {AutomationRequestService} from './automation-request.service';
import {TestingUtilsModule} from '../../../ui/testing-utils/testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('AutomationRequestService', () => {
  let service: AutomationRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule]
    });
    service = TestBed.inject(AutomationRequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import {Injectable} from '@angular/core';
import {map, pluck} from 'rxjs/operators';
import {RestService} from '../rest.service';
import {Observable} from 'rxjs';
import {EntityFormModel} from '../entity-creation.service';
import {Parameter} from '../../../model/test-case/parameter.model';
import {Dataset} from '../../../model/test-case/dataset.model';
import {DatasetParamValue} from '../../../model/test-case/dataset-param-value';
import {ChangeCoverageOperationReport} from '../../../model/change-coverage-operation-report';
import {RequirementVersionCoverage} from '../../../model/test-case/requirement-version-coverage-model';

export interface TestCaseFormModel extends EntityFormModel {
  scriptLanguage: string;
}

@Injectable({
  providedIn: 'root'
})
export class TestCaseService {

  constructor(private restService: RestService) {
  }

  getCoverages(testCaseId: number): Observable<RequirementVersionCoverage[]> {
    return this.restService.get<any>([buildTestCaseUrl(testCaseId), 'verified-requirements'])
      .pipe(pluck('coverages'));
  }

  persistCoverages(testCaseId: number, requirementIds: number[]): Observable<ChangeCoverageOperationReport> {
    return this.restService.post([buildTestCaseUrl(testCaseId), 'verified-requirements'], {requirementIds});
  }

  eraseCoverages(testCaseId: number, requirementVersionIds: number[]): Observable<ChangeCoverageOperationReport> {
    const pathVariable = requirementVersionIds.map(id => id.toString()).join(',');
    return this.restService.delete([buildTestCaseUrl(testCaseId), 'verified-requirement-versions', pathVariable]);
  }

  persistParameter(testCaseId: number, param): Observable<TestCaseParameterOperationReport> {
    return this.restService.post<TestCaseParameterOperationReport>([`test-cases/${testCaseId}/parameters/new`], param);
  }

  persistDataset(testCaseId: number, dataset): Observable<TestCaseParameterOperationReport> {
    return this.restService.post<TestCaseParameterOperationReport>([`test-cases/${testCaseId}/datasets/new`], dataset);
  }

  updateScript(testCaseId: number, script: string): Observable<void> {
    return this.restService.post(['test-case', testCaseId.toString(), 'scripted'], {script: script});
  }

  validateScript(testCaseId: number, script: string): Observable<void> {
    return this.restService.post(['test-case', testCaseId.toString(), 'scripted', 'validate'], {script: script});

  }

  getMatchingActionWord(projectId: number, searchInput: string, selectedProjectsIds: number[]): Observable<string[]> {
    return this.restService.post<{ actionList: string[] }>(
      ['keyword-test-cases', 'autocomplete'],
      { projectId, searchInput, selectedProjectsIds })
      .pipe(
        map(response => response.actionList));
  }

  getDuplicateActionWords(projectId: number, searchInput: string): Observable<any> {
    return this.restService.post(
      ['keyword-test-cases', 'duplicated-action'],
      { projectId, searchInput });
  }

  getScriptPreview(testCaseId: number): Observable<string> {
    return this.restService.get<{ script: string }>(
      ['keyword-test-cases', testCaseId.toString(), 'generated-script'])
      .pipe(
        map(response => response.script));
  }
}

function buildTestCaseUrl(testCaseId) {
  return `test-cases/${testCaseId}`;
}


export interface TestCaseParameterOperationReport {
  parameters: Parameter[];
  dataSets: Dataset[];
  paramValues: DatasetParamValue[];
}

export interface TestStepActionWordOperationReport {
  action: string;
  styledAction: string;
  actionWordId: number;
  paramOperationReport: TestCaseParameterOperationReport;
}

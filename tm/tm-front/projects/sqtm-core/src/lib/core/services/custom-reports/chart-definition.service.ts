import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {Observable, Subject} from 'rxjs';
import {ChartDefinitionModel, ChartWorkbenchData} from '../../../model/custom-report/chart-definition.model';

@Injectable({
  providedIn: 'root'
})
export class ChartDefinitionService {

  private _downloadChart: Subject<void> = new Subject<void>();
  downloadChart$: Observable<void>;

  constructor(private restService: RestService) {
    this.downloadChart$ = this._downloadChart.asObservable();
  }

  getChartDefinition(customReportLibraryNodeId: number): Observable<ChartDefinitionModel> {
    return this.restService.getWithoutErrorHandling<ChartDefinitionModel>(
      ['chart-definition-view', customReportLibraryNodeId.toString()]);
  }

  getWorkbenchData(customReportLibraryNodeId: number): Observable<ChartWorkbenchData> {
    return this.restService.get<ChartWorkbenchData>(['chart-workbench', customReportLibraryNodeId.toString()]);
  }

  getChartPreview(chartDefinition: Partial<ChartDefinitionModel>): Observable<ChartDefinitionModel> {
      return this.restService.post<ChartDefinitionModel>(
        ['chart-workbench', 'preview', chartDefinition.projectId.toString()],
        {...chartDefinition});
  }

  persistNewChartDefinition(chartDefinition: Partial<ChartDefinitionModel>, customReportLibraryNodeId: number)
    : Observable<{ id: number }> {
    return this.restService.post<{ id: number }>(
      ['chart-workbench', 'new', customReportLibraryNodeId.toString()],
      {...chartDefinition});
  }

  updateChartDefinition(chartDefinition: Partial<ChartDefinitionModel>, customReportLibraryNodeId: number): Observable<void> {
    return this.restService.post(
      ['chart-workbench', 'update', customReportLibraryNodeId.toString()],
      {...chartDefinition});
  }


  downloadChart() {
    this._downloadChart.next();
  }
}

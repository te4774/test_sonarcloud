import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

/**
 * Service that hold the copied things that can pasted in different screens.
 * Aka the test steps that you can copy from a test case to another.
 */
@Injectable({
  providedIn: 'root'
})
export class CopierService {

  _copiedTestStepIds = new BehaviorSubject<number[]>([]);
  _copiedTestStepTcKind = new BehaviorSubject<string>('');
  copiedTestStep$ = this._copiedTestStepIds.asObservable();
  copiedTestStepTcKind$ = this._copiedTestStepTcKind.asObservable();

  constructor() {
  }

  notifyCopySteps(stepIds: number[], testCaseKind: string) {
    this._copiedTestStepIds.next(stepIds);
    this._copiedTestStepTcKind.next(testCaseKind);
  }
}

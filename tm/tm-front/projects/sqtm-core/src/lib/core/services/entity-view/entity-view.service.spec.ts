import {TestBed, waitForAsync} from '@angular/core/testing';

import {EntityViewService} from './entity-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CORE_MODULE_CONFIGURATION} from '../../sqtm-core.tokens';
import {defaultSqtmConfiguration} from '../../sqtm-core.module';
import {SqtmEntityState} from './entity-view.state';
import {TranslateService} from '@ngx-translate/core';
import {RestService} from '../rest.service';
import {ReferentialDataService} from '../../referential/services/referential-data.service';
import {AttachmentService} from '../attachment.service';
import {ProjectData} from '../../../model/project/project-data.model';
import {of} from 'rxjs';
import {PersistedAttachment, RejectedAttachment, UploadingAttachment} from '../../../model/attachment/attachment.model';
import {TestCasePermissions} from '../../../model/permissions/simple-permissions';
import {NO_PERMISSIONS} from '../../../model/permissions/permissions.model';
import {CustomFieldValueService} from '../custom-field-value.service';
import {EntityViewAttachmentHelperService} from './entity-view-attachment-helper.service';
import {EntityViewCustomFieldHelperService} from './entity-view-custom-field-helper.service';
import {GenericEntityUiState} from '../genric-entity-view/generic-entity-view-state';
import SpyObj = jasmine.SpyObj;

interface DummyEntity extends SqtmEntityState {
  name: string;
}

class DummyEntityViewService extends EntityViewService<DummyEntity, 'dummy', TestCasePermissions> {
  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(data: ProjectData): TestCasePermissions {
    return new TestCasePermissions(data);
  }

  getInitialState(): { [K in 'dummy']: DummyEntity } & { type: 'dummy' } & { uiState: GenericEntityUiState<DummyEntity> } {
    return {
      type: 'dummy',
      uiState: {fieldState: {ids: [], entities: {}}},
      dummy: null
    };
  }
}

const projectData: ProjectData = {
  id: 1,
  allowAutomationWorkflow: false,
  customFieldBinding: null,
  label: 'project1',
  name: 'project1',
  requirementCategory: null,
  testCaseNature: null,
  testCaseType: null,
  uri: '',
  permissions: NO_PERMISSIONS,
  bugTracker: null,
  milestones: [],
  automationWorkflowType: 'NATIVE',
  taServer: null,
  disabledExecutionStatus: [],
  keywords: [],
  bddScriptLanguage: 'ENGLISH',
  allowTcModifDuringExec: false,
  activatedPlugins: null
};

let restServiceMock: any;
let attachmentServiceMock: any;
let translateServiceMock: any;
let referentialDataServiceMock: any;
let attachmentHelperMock: any;
let customFieldHelperMock: SpyObj<EntityViewCustomFieldHelperService>;


function getSimpleEntityState(): DummyEntity {
  return {
    id: 12,
    name: 'hello',
    projectId: 1,
    customFieldValues: {
      ids: [],
      entities: {}
    },
    attachmentList: {
      id: 1,
      attachments:
        {
          ids: [],
          entities: {}
        }
    }
  };
}

function getEntityStateWithAttachments() {
  const simpleEntityState = getSimpleEntityState();
  simpleEntityState.attachmentList.attachments.ids = [
    '1',
    '2',
    'def429ab-3db2-4a4a-9742-db878344fd87',
    '3a2003a4-95ed-4d21-9063-4e3eef5122a6'
  ];
  const attachment1: PersistedAttachment = {
    id: '1',
    kind: 'persisted-attachment',
    addedOn: new Date(),
    name: 'attachment1',
    size: 500,
    pendingDelete: true
  };
  const attachment2: PersistedAttachment = {
    id: '2',
    kind: 'persisted-attachment',
    addedOn: new Date(),
    name: 'attachment2',
    size: 500,
    pendingDelete: false
  };
  const rejectedAttachment: RejectedAttachment = {
    id: 'def429ab-3db2-4a4a-9742-db878344fd87',
    kind: 'rejected-attachment',
    addedOn: new Date(),
    name: 'log_jira_sync.txt',
    size: 50000000000,
    errors: ['Too Heavy']
  };
  const uploadingAttachment: UploadingAttachment = {
    id: '3a2003a4-95ed-4d21-9063-4e3eef5122a6',
    kind: 'uploading-attachment',
    addedOn: new Date(),
    name: 'hello.txt',
    size: 1,
    uploadProgress: 50
  };


  simpleEntityState.attachmentList.attachments.entities = {
    '1': attachment1,
    '2': attachment2,
    'def429ab-3db2-4a4a-9742-db878344fd87': rejectedAttachment,
    '3a2003a4-95ed-4d21-9063-4e3eef5122a6': uploadingAttachment,
  };
  return simpleEntityState;
}

export function getTestingFile(name: string, type: string, size = 500) {
  return {
    name,
    type,
    slice(start?: number, end?: number, contentType?: string): Blob {
      return null;
    },
    size,
    lastModified: new Date().getDate(),
  } as File;
}

describe('EntityViewService', () => {
  beforeEach(() => {
    restServiceMock = jasmine.createSpyObj('restService', ['get', 'post']);
    attachmentServiceMock = jasmine.createSpyObj('attachmentService', ['uploadAttachments', 'deleteAttachments']);
    translateServiceMock = jasmine.createSpyObj('translateService', ['instant']);
    attachmentHelperMock = jasmine.createSpyObj('attachmentHelper', ['initializeAttachmentState', 'addAttachments', 'eraseAttachments']);
    customFieldHelperMock = jasmine.createSpyObj('customFieldHelper', [
      'initializeCustomFieldValueState',
      'writeCustomFieldValue',
      'updateCustomFieldValue'
    ]);
    referentialDataServiceMock = jasmine.createSpyObj('refDataService', ['connectToProjectData']);
    referentialDataServiceMock.globalConfiguration$ = of({
      milestoneFeatureEnabled: false,
      uploadFileExtensionWhitelist: ['txt'],
      uploadFileSizeLimit: 1000
    });
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: CORE_MODULE_CONFIGURATION,
          useValue: defaultSqtmConfiguration
        },
        {
          provide: RestService,
          useValue: restServiceMock
        },
        {
          provide: TranslateService,
          useValue: translateServiceMock
        },
        {
          provide: ReferentialDataService,
          useValue: referentialDataServiceMock
        },
        {
          provide: AttachmentService,
          useValue: attachmentServiceMock
        },
        {
          provide: EntityViewAttachmentHelperService,
          useValue: attachmentHelperMock
        },
        {
          provide: EntityViewCustomFieldHelperService,
          useValue: customFieldHelperMock
        },
        {
          provide: EntityViewService,
          useClass: DummyEntityViewService,
          deps: [
            RestService,
            ReferentialDataService,
            AttachmentService,
            TranslateService,
            CustomFieldValueService,
            EntityViewAttachmentHelperService,
            EntityViewCustomFieldHelperService,
          ]
        }
      ]
    });
  });

  it('should be created', () => {
    const service: EntityViewService<any, 'dummy', TestCasePermissions>
      = TestBed.inject<EntityViewService<any, 'dummy', TestCasePermissions>>(EntityViewService);
    expect(service).toBeTruthy();
  });

  it('should load entity', (done) => {
    const service: EntityViewService<any, 'dummy', TestCasePermissions>
      = TestBed.inject<EntityViewService<any, 'dummy', TestCasePermissions>>(EntityViewService);
    referentialDataServiceMock.connectToProjectData.and.returnValue(of(projectData));

    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });
    service.initializeEntityState(getSimpleEntityState());
    expect(componentData['dummy'].id).toEqual(12);
    expect(componentData['dummy'].name).toEqual('hello');
    done();
  });

  it('should update entity', (done) => {
    const service: EntityViewService<any, 'dummy', TestCasePermissions>
      = TestBed.inject<EntityViewService<any, 'dummy', TestCasePermissions>>(EntityViewService);
    referentialDataServiceMock.connectToProjectData.and.returnValue(of(projectData));
    restServiceMock.post.and.returnValue(of(null));

    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });

    service.initializeEntityState(getSimpleEntityState());
    service.update('name', 'updated');
    expect(componentData['dummy'].id).toEqual(12);
    expect(componentData['dummy'].name).toEqual('updated');
    expect(restServiceMock.post).toHaveBeenCalledTimes(1);
    done();
  });

  it('should reject invalid update entity', (done) => {
    const service: EntityViewService<any, 'dummy', TestCasePermissions>
      = TestBed.inject<EntityViewService<any, 'dummy', TestCasePermissions>>(EntityViewService);
    referentialDataServiceMock.connectToProjectData.and.returnValue(of(projectData));

    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });
    service.initializeEntityState(getSimpleEntityState());
    const spyCommit = spyOn(service, 'commit').and.callThrough();
    service.update('bob' as any, 'updated');
    expect(componentData['dummy'].id).toEqual(12);
    expect(componentData['dummy'].name).toEqual('hello');
    expect(restServiceMock.post).toHaveBeenCalledTimes(0);
    expect(spyCommit).toHaveBeenCalledTimes(0);
    done();
  });

  it('should mark attachment to delete', (done) => {
    const service: EntityViewService<any, 'dummy', TestCasePermissions>
      = TestBed.inject<EntityViewService<any, 'dummy', TestCasePermissions>>(EntityViewService);
    referentialDataServiceMock.connectToProjectData.and.returnValue(of(projectData));

    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });
    const entity = getEntityStateWithAttachments();
    const initialAttachment = entity.attachmentList.attachments.entities['1'];
    const spyCommit = spyOn(service, 'commit').and.callThrough();
    service.initializeEntityState(entity);
    service.markAttachmentsToDelete(['1']);
    expect(componentData['dummy']).not.toBe(entity);
    expect(componentData['dummy'].attachmentList).not.toBe(entity.attachmentList);
    expect(componentData['dummy'].attachmentList.attachments.ids.length).toEqual(4);
    // attachment reference must have change
    expect(componentData['dummy'].attachmentList.attachments.entities['1']).not.toBe(initialAttachment);
    expect(componentData['dummy'].attachmentList.attachments.entities['1'].pendingDelete).toBeTruthy();
    expect(spyCommit).toHaveBeenCalledTimes(2);
    done();
  });

  it('should cancel attachment to delete', (done) => {
    const service: EntityViewService<any, 'dummy', TestCasePermissions>
      = TestBed.inject<EntityViewService<any, 'dummy', TestCasePermissions>>(EntityViewService);
    referentialDataServiceMock.connectToProjectData.and.returnValue(of(projectData));

    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });
    const entity = getEntityStateWithAttachments();
    const initialAttachment = entity.attachmentList.attachments.entities['1'];
    const spyCommit = spyOn(service, 'commit').and.callThrough();
    service.initializeEntityState(entity);
    service.cancelDeleteAttachments(['1']);
    expect(componentData['dummy'].attachmentList.attachments.ids.length).toEqual(4);
    // attachment reference must have change
    expect(componentData['dummy'].attachmentList.attachments.entities[1]).not.toBe(initialAttachment);
    expect(componentData['dummy'].attachmentList.attachments.entities[1].pendingDelete).toBeFalsy();
    expect(spyCommit).toHaveBeenCalledTimes(2);
    done();
  });

  it('should remove attachment', (done) => {
    const service: EntityViewService<any, 'dummy', TestCasePermissions>
      = TestBed.inject<EntityViewService<any, 'dummy', TestCasePermissions>>(EntityViewService);
    referentialDataServiceMock.connectToProjectData.and.returnValue(of(projectData));
    attachmentHelperMock.eraseAttachments.and.returnValue(of(null));

    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });
    const simpleEntityState = getEntityStateWithAttachments();
    const spyCommit = spyOn(service, 'commit').and.callThrough();
    service.initializeEntityState(simpleEntityState);
    service.deleteAttachments(['1'], 1);
    expect(componentData['dummy'].attachmentList.attachments.ids.length).toEqual(3);
    expect(componentData['dummy'].attachmentList.attachments.entities['1']).toBeFalsy();
    expect(spyCommit).toHaveBeenCalledTimes(2);
    done();
  });

  it('should update custom field value in entity', waitForAsync(function () {
    const service: EntityViewService<any, 'dummy', TestCasePermissions>
      = TestBed.inject<EntityViewService<any, 'dummy', TestCasePermissions>>(EntityViewService);
    referentialDataServiceMock.connectToProjectData.and.returnValue(of(projectData));
    customFieldHelperMock.writeCustomFieldValue.and.returnValue(of('endeavour'));
    customFieldHelperMock.updateCustomFieldValue.and.returnValue({
      ids: [1, 2], entities: {
        1: {id: 1, cufId: 1, value: 'endeavour', fieldType: 'CF'},
        2: {id: 2, cufId: 2, value: '2019-12-12', fieldType: 'CF'},
      }
    });
    const spyCommit = spyOn(service, 'commit').and.callThrough();

    let componentData;
    service.componentData$.subscribe(data => {
      componentData = data;
    });

    const simpleEntityState = getSimpleEntityState();
    simpleEntityState.customFieldValues = {
      ids: [1, 2], entities: {
        1: {id: 1, cufId: 1, value: 'atlantis', fieldType: 'CF'},
        2: {id: 2, cufId: 2, value: '2019-12-12', fieldType: 'CF'},
      }
    };
    service.initializeEntityState(simpleEntityState);
    service.updateCustomFieldValue(1, 'endeavour');
    // 1 time for init, 1 time for modification
    expect(spyCommit).toHaveBeenCalledTimes(2);
    expect(componentData['dummy'].customFieldValues.entities[1].value).toEqual('endeavour');
  }));

  it('should update attachment state', waitForAsync(function () {
    const service: EntityViewService<any, 'dummy', TestCasePermissions> =
      TestBed.inject<EntityViewService<any, 'dummy', TestCasePermissions>>(EntityViewService);
    referentialDataServiceMock.connectToProjectData.and.returnValue(of(projectData));
    const spyCommit = spyOn(service, 'commit').and.callThrough();

    const simpleEntityState = getSimpleEntityState();
    const attachmentList = simpleEntityState.attachmentList;
    Object.freeze(attachmentList);
    const initialState = service.getInitialState();
    initialState['dummy'] = simpleEntityState;
    const updatedState = service.updateAttachmentState(initialState, {...attachmentList.attachments});
    expect(updatedState).not.toBe(initialState);
    const updatedEntity = updatedState['dummy'] as DummyEntity;
    expect(updatedEntity).not.toBe(simpleEntityState);
    expect(updatedEntity.attachmentList).not.toBe(attachmentList);
  }));
});

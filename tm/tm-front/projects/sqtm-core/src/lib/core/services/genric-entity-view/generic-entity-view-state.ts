import {EntityState} from '@ngrx/entity';
import {AttachmentListState, EditableFieldStatus,} from '../entity-view/entity-view.state';
import {AttachmentListModel} from '../../../model/attachment/attachment-list.model';
import {HttpErrorResponse} from '@angular/common/http';


export type PartialGenericEntityViewState<S extends SqtmGenericEntityState, T extends string> = { [K in T]: S }
  & { type: T };

export type GenericEntityViewState<S extends SqtmGenericEntityState, T extends string> =
  PartialGenericEntityViewState<S, T>
  & { uiState: GenericEntityUiState<S> };

export interface AdminEntityModel {
  id: number;
  attachmentList: AttachmentListModel;

}

/**
 * A generic Sqtm Entity is just an object with an id and attachments as STATE.
 */
export interface SqtmGenericEntityState {
  id: number;
  attachmentList: AttachmentListState;
}

export interface GenericEntityUiState<S extends SqtmGenericEntityState> {
  fieldState: GenericEntityFieldsState<S>;
  entityNotFound?: boolean;
  loadError?: HttpErrorResponse;
}

export interface GenericEntityFieldsState<S extends SqtmGenericEntityState> extends EntityState<EditableGenericEntityFieldState<S>> {

}

export interface EditableGenericEntityFieldState<S extends SqtmGenericEntityState> {
  status: EditableFieldStatus;
  errorMsgKey: string[];
  errorMsg: string[];
  id: keyof S;
}

// Have to define the partial hard way.
// Because with Generics, TypeScript mapped types (aka Partial, Exclude...) can't track the { [K in T]: E } part of the object
// and thus lead to compilation errors.
export type GenericEntityViewComponentData<S extends SqtmGenericEntityState, T extends string> =
  GenericEntityViewState<S, T>
  & { type: T };

// return an initial state like {execution:ExecutionState}
export function provideInitialGenericViewState<S extends SqtmGenericEntityState, T extends string>(type: T): GenericEntityViewState<S, T> {
  const base = {type};
  base[type as string] = null;
  return {
    ...base as PartialGenericEntityViewState<S, T>,
    uiState: {fieldState: {ids: [], entities: {}}}
  };
}



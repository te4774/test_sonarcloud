import {TestBed} from '@angular/core/testing';

import {ExecutionService} from './execution.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../ui/testing-utils/testing-utils.module';
import {RestService} from '../rest.service';
import {of} from 'rxjs';
import SpyObj = jasmine.SpyObj;

describe('ExecutionService', () => {

  let restService: SpyObj<RestService>;

  beforeEach(() => {
    restService = jasmine.createSpyObj<RestService>(['get', 'getWithoutErrorHandling']);
    restService.get.and.returnValue(of({}));

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TestingUtilsModule],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        }
      ]
    });
  });

  it('should fetch execution model', () => {
    const service: ExecutionService = TestBed.inject(ExecutionService);
    service.fetchExecutionData(10);
    expect(restService.getWithoutErrorHandling).toHaveBeenCalledTimes(1);
    expect(restService.getWithoutErrorHandling).toHaveBeenCalledWith(['execution', '10']);
  });
});

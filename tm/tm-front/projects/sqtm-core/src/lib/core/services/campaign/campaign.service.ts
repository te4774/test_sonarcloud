import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {Observable} from 'rxjs';
import {TestCaseModel} from '../../../model/test-case/test-case.model';
import {RawValueMap} from '../../../model/customfield/customfield.model';
import {CampaignStatisticsBundle, IterationPlanning} from '../../../model/campaign/campaign-model';
import {Identifier} from '../../../model/entity.model';
import {map} from 'rxjs/operators';
import {DateFormatUtils} from '../../utils/date-format.utils';


export type NewCampaignFormModel = Pick<TestCaseModel, 'name' | 'reference' | 'description'> &
  {
    parentEntityReference: string;
    customFields: RawValueMap
  };

@Injectable({
  providedIn: 'root'
})
export class CampaignService {

  constructor(private restService: RestService) {
  }

  persistCampaign(campaignModel: NewCampaignFormModel): Observable<any> {
    return this.restService.post([`campaign-tree/new-campaign`], campaignModel);
  }

  updateActualStartDate(campaignId: number, actualStartDate: Date): Observable<any> {
    return this.restService.post(['campaign', campaignId.toString(), 'actual-start-date'], {actualStartDate: actualStartDate});
  }

  updateActualEndDate(campaignId: number, actualEndDate: Date): Observable<any> {
    return this.restService.post(['campaign', campaignId.toString(), 'actual-end-date'], {actualEndDate: actualEndDate});
  }

  updateScheduledStartDate(campaignId: number, scheduledStartDate: Date): Observable<any> {
    return this.restService.post(['campaign', campaignId.toString(), 'scheduled-start-date'], {scheduledStartDate: scheduledStartDate});
  }

  updateScheduledEndDate(campaignId: number, scheduledEndDate: Date): Observable<any> {
    return this.restService.post(['campaign', campaignId.toString(), 'scheduled-end-date'], {scheduledEndDate: scheduledEndDate});
  }

  updateActualStartAuto(campaignId: number, actualStartAuto: boolean): Observable<any> {
    return this.restService.post(['campaign', campaignId.toString(), 'actual-start-auto'], {actualStartAuto: actualStartAuto});
  }

  updateActualEndAuto(campaignId: number, actualEndAuto: boolean): Observable<any> {
    return this.restService.post(['campaign', campaignId.toString(), 'actual-end-auto'], {actualEndAuto: actualEndAuto});
  }

  findIterationsPlanning(campaignId: number): Observable<IterationPlanning[]> {
    return this.restService.get<RawIterationPlanning[]>(['campaign', campaignId.toString(), 'iterations']).pipe(
      map((rawIterationPlannings: RawIterationPlanning[]) => this.convertRawPlannings(rawIterationPlannings))
    );
  }

  private convertRawPlannings(rawIterationPlannings: RawIterationPlanning[]) {
    return rawIterationPlannings.map(raw => this.convertRawPlanning(raw));
  }

  // SQUASH-3208 Date from server cannot directly be real Date objects... aka object with Date.prototype...
  // There are indeed strings and must be parsed into read Date js objects
  private convertRawPlanning(raw: RawIterationPlanning) {
    return {
      id: raw.id,
      name: raw.name,
      scheduledStartDate: DateFormatUtils.createDateFromIsoString(raw.scheduledStartDate),
      scheduledEndDate: DateFormatUtils.createDateFromIsoString(raw.scheduledEndDate),
    };
  }

  setIterationsPlanning(campaignId: number, plannings: IterationPlanning[]): Observable<IterationPlanning[]> {
    return this.restService.post(['campaign', campaignId.toString(), 'iterations/planning'], {iterationPlannings: plannings});
  }

  changeItemsPosition(campaignId: number, itemsToMove: Identifier[], position: number): Observable<void> {
    return this.restService.post(['campaign', campaignId.toString(), 'test-plan',
      itemsToMove.join(','), 'position', position.toString()]);
  }

  addTestCase(testCaseIds: number [], campaignId: number): Observable<AddTestCaseResponse> {
    const url = `campaign/${campaignId}/test-plan-items`;
    return this.restService.post<AddTestCaseResponse>([url], {testCaseIds});
  }

  getCampaignStatistics(campaignId: number): Observable<CampaignStatisticsBundle> {
    return this.restService.get(['campaign-view', campaignId.toString(), 'statistics']);
  }

}

export interface RawIterationPlanning {
  id: number;
  name: string;
  scheduledStartDate: string;
  scheduledEndDate: string;
}

export interface AddTestCaseResponse {
  hasDataSet: boolean;
  totalNewTestPlanItemsNumber: number;
}

import {TestBed} from '@angular/core/testing';

import {MilestoneLocalPersistenceService} from './milestone-local-persistence.service';

describe('MilestoneLocalPersistenceService', () => {
  let service: MilestoneLocalPersistenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MilestoneLocalPersistenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

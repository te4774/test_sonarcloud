import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {Observable} from 'rxjs';
import {ActionStepFormModel} from './forms/action-step-form-model';
import {TestCaseParameterOperationReport, TestStepActionWordOperationReport} from '../test-case/test-case.service';
import {PasteTestStepOperationReport} from '../../../model/test-case/operation-reports.model';
import {ChangeCoverageOperationReport} from '../../../model/change-coverage-operation-report';
import {KeywordStepFormModel} from './forms/keyword-step-form.model';

@Injectable({
  providedIn: 'root'
})
export class TestStepService {

  readonly url = 'test-steps';

  constructor(private restService: RestService) {
  }

  postAction(id: number, action: string): Observable<any> {
    return this.restService.post([this.url, id.toString()], {action});
  }

  postExpectedResult(id: number, expectedResult: string): Observable<any> {
    return this.restService.post([this.url, id.toString()], {expectedResult});
  }

  postKeyword(id: number, keyword: string): Observable<any> {
    return this.restService.post([this.url, id.toString(), 'keyword'], {keyword});
  }

  postActionWord(id: number, action: string): Observable<TestStepActionWordOperationReport> {
    return this.restService.post([this.url, id.toString(), 'action-word'], {action});
  }

  postActionWordWithId(id: number, action: string, actionWordId: number): Observable<TestStepActionWordOperationReport> {
    return this.restService.post([this.url, id.toString(), 'action-word'], {action, actionWordId});
  }

  postDatatable(id: number, datatable: string): Observable<void> {
    return this.restService.post([this.url, id.toString(), 'datatable'], {datatable});
  }

  postDocstring(id: number, docstring: string): Observable<void> {
    return this.restService.post([this.url, id.toString(), 'docstring'], {docstring});
  }

  postComment(id: number, comment: string): Observable<void> {
    return this.restService.post([this.url, id.toString(), 'comment'], {comment});
  }

  addActionStep(testCaseId: number, actionStepForm: ActionStepFormModel) {
    return this.restService.post([buildTestCaseStepUrl(testCaseId), 'add'], actionStepForm);
  }

  addKeywordStep(testCaseId: number, keywordStepForm: KeywordStepFormModel) {
    return this.restService.post([buildTestCaseStepUrl(testCaseId), 'add-keyword-test-step'], keywordStepForm);
  }

  deleteSteps(stepIds: number[], testCaseId: number): Observable<TestCaseParameterOperationReport> {
    return this.restService.delete([buildTestCaseStepUrl(testCaseId), stepIds.join(',')]);
  }

  pasteSteps(copiedStepIds: number[], targetTestStepId: number, testCaseId: number, isAssignedToTargetProject?: boolean):
    Observable<PasteTestStepOperationReport> {
    return this.restService.post([buildTestCaseStepUrl(testCaseId), 'paste'],
      {copiedStepIds, targetTestStepId, isAssignedToTargetProject});
  }

  compareKeywordProjectsIds(testCaseId: number, copiedStepIds: number[]): Observable<boolean> {
    return this.restService.post([buildTestCaseStepUrl(testCaseId), 'compare-keywords-projects'], {
      copiedStepIds
    });
  }

  moveSteps(movedStepIds: number[], newIndex: number, testCaseId: number) {
    return this.restService.post([buildTestCaseStepUrl(testCaseId), 'move'], {newIndex, movedStepIds});
  }

  callTestCases(calledTestCaseIds: number[], index: number, callingTestCaseId: number) {
    return this.restService.post([buildTestCaseStepUrl(callingTestCaseId), 'call-test-case'], {
      index,
      calledTestCaseIds
    });
  }

  persistStepCoverage(requirementIds: number[], stepId: number, testCaseId: number): Observable<ChangeCoverageOperationReport> {
    return this.restService.post([buildTestCaseStepUrl(testCaseId), stepId.toString(), 'verified-requirements'], {requirementIds});
  }

  // "backend/test-cases/{testCaseId}/steps/{testStepId}/verified-requirement-versions/{requirementVersionsIds}"
  eraseStepCoverages(testCaseId: number, stepId: number, requirementVersionIds: number[]): Observable<ChangeCoverageOperationReport> {
    const pathVariable = requirementVersionIds.map(id => id.toString()).join(',');
    return this.restService.delete([buildTestCaseStepUrl(testCaseId), stepId.toString(), 'verified-requirement-versions', pathVariable]);
  }

  // /test-steps/1314/verified-requirement-versions/13395
  linkRequirementVersionToStep(stepId: number, requirementVersionId: number): Observable<ChangeCoverageOperationReport> {
    return this.restService.post(['test-steps', stepId.toString(), 'verified-requirement-versions', requirementVersionId.toString()]);
  }

  updateParameterAssignationMode(testCaseId: number,
                                 stepId: number,
                                 datasetId: number,
                                 mode: any) {
    return this.restService.post<TestCaseParameterOperationReport>(
      [`test-cases/${testCaseId}/steps/${stepId}/parameter-assignation-mode`], {
        datasetId,
        mode
      });
  }
}

function buildTestCaseStepUrl(testCaseId) {
  return `test-cases/${testCaseId}/steps`;
}

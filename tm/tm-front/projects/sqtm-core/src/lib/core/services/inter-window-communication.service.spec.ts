import {TestBed} from '@angular/core/testing';

import {InterWindowCommunicationService} from './inter-window-communication.service';

describe('InterWindowCommunicationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InterWindowCommunicationService = TestBed.inject(InterWindowCommunicationService);
    expect(service).toBeTruthy();
  });
});

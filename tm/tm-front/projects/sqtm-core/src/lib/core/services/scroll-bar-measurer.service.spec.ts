import {TestBed} from '@angular/core/testing';

import {ScrollBarMeasurerService} from './scroll-bar-measurer.service';

describe('ScrollBarMeasurerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScrollBarMeasurerService = TestBed.inject(ScrollBarMeasurerService);
    expect(service).toBeTruthy();
  });
});

import {TestBed} from '@angular/core/testing';

import {GenericErrorDisplayService} from './generic-error-display.service';

describe('GenericErrorDisplayService', () => {
  let service: GenericErrorDisplayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenericErrorDisplayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import {Injectable} from '@angular/core';
import {RequirementVersionStatsBundle} from '../../../model/requirement/requirement-version-stats-bundle.model';
import {RestService} from '../rest.service';
import {
  LinkedHighLevelRequirement,
  RequirementVersionModel
} from '../../../model/requirement/requirement-version.model';
import {Observable, Subject} from 'rxjs';
import {Milestone} from '../../../model/milestone/milestone.model';
import {
  BindRequirementToHighLevelRequirementOperationReport,
  ChangeLinkedRequirementOperationReport,
  ChangeVerifyingTestCaseOperationReport
} from '../../../model/change-coverage-operation-report';
import {RequirementVersionLinkPatch} from '../../../model/requirement/requirement-version-link-patch.model';
import {RequirementVersionLink} from '../../../model/requirement/requirement-version.link';
import {NewReqVersionParams} from '../../../model/requirement/new-req-version-params.model';

@Injectable({
  providedIn: 'root'
})
export class RequirementVersionService {

  private _refreshView: Subject<void> = new Subject<void>();

  refreshView$: Observable<void>;

  constructor(private restService: RestService) {
    this.refreshView$ = this._refreshView.asObservable();
  }

  loadCurrentVersion(requirementId: number): Observable<RequirementVersionModel> {
    const parts = ['requirement-view', 'current-version', requirementId.toString()];
    return this.restService.getWithoutErrorHandling<RequirementVersionModel>(parts);
  }

  loadHighLevelRequirementCurrentVersion(requirementId: number): Observable<RequirementVersionModel> {
    const parts = ['requirement-view', 'high-level', 'current-version', requirementId.toString()];
    return this.restService.getWithoutErrorHandling<RequirementVersionModel>(parts);
  }

  loadVersion(requirementVersionId: number): Observable<RequirementVersionModel> {
    const parts = ['requirement-view', requirementVersionId.toString()];
    return this.restService.getWithoutErrorHandling<RequirementVersionModel>(parts);
  }

  refreshView() {
    this._refreshView.next();
  }

  findAssociableMilestones(requirementVersionId: number): Observable<number[]> {
    return this.restService.get<number[]>(['requirement-version', requirementVersionId.toString(), 'associable-milestones']);
  }

  persistVerifyingTestCases(requirementVersionId: number, testCaseIds: number[]): Observable<ChangeVerifyingTestCaseOperationReport> {
    return this.restService.post(['requirement-version', requirementVersionId.toString(),
      'verifying-test-cases'], {testCaseIds});
  }

  removeVerifyingTestCases(requirementVersionId: number,
                           verifyingTestCaseIds: number[]): Observable<ChangeVerifyingTestCaseOperationReport> {
    return this.restService.delete(['requirement-version', requirementVersionId.toString(),
      'verifying-test-cases', verifyingTestCaseIds.toString()]);
  }

  removeRequirementLinks(requirementVersionId: number, requirementLinkIds: number[]): Observable<ChangeLinkedRequirementOperationReport> {
    return this.restService.delete(['requirement-version', requirementVersionId.toString(),
      'linked-requirement-versions', requirementLinkIds.toString()]);
  }

  removeLinkedLowLevelRequirements(requirementId: number, lowLevelRequirementIds: number[])
    : Observable<BindRequirementToHighLevelRequirementOperationReport> {
    return this.restService.delete(['high-level-requirement', requirementId.toString(),
      'unbind-requirement-from-high-level-requirement', lowLevelRequirementIds.toString()]);
  }

  bindMilestonesToRequirementVersion(requirementVersionId: number, milestoneIds: number[]): Observable<Milestone[]> {
    return this.restService.post<Milestone[]>(['requirement-version', requirementVersionId.toString(),
      'milestones', milestoneIds.toString()]);
  }

  updateStatus(requirementVersionId: number, status: string): Observable<any> {
    return this.restService.post(['requirement-version', requirementVersionId.toString(), 'status'], {status: status});
  }

  unbindMilestoneToRequirementVersion(requirementVersionId: number, milestoneId: number): Observable<Milestone[]> {
    return this.restService.delete<Milestone[]>(['requirement-version', requirementVersionId.toString(),
      'milestones', milestoneId.toString()]);
  }

  persistRequirementVersionLinks(requirementVersionId: number, requirementVersionLinkPatch: RequirementVersionLinkPatch):
    Observable<ChangeLinkedRequirementOperationReport> {
    return this.restService.post(['requirement-version', requirementVersionId.toString(),
      'linked-requirement-versions'], {...requirementVersionLinkPatch});
  }

  persistRequirementBindingToHighLevelRequirement(higLevelRequirementId: number, requirementIds: number[])
    : Observable<BindRequirementToHighLevelRequirementOperationReport> {
    return this.restService.post(['high-level-requirement', higLevelRequirementId.toString(),
      'bind-requirement-to-high-level-requirement', requirementIds.join(',')]);
  }

  updateRequirementVersionLinkType(requirementVersionId: number, requirementVersionLinkPatch: RequirementVersionLinkPatch):
    Observable<RequirementVersionLink[]> {
    return this.restService.post(['requirement-version', requirementVersionId.toString(),
      'linked-requirement-versions', 'update'], {...requirementVersionLinkPatch});
  }

  findRequirementVersionLinkNames(requirementVersionId: number, relatedIds: number[]): Observable<any> {
    return this.restService.get(['requirement-version', requirementVersionId.toString(),
      'linked-requirement-versions', relatedIds.toString()]);
  }

  createNewRequirementVersion(requirementId: number, params: NewReqVersionParams): Observable<RequirementVersionModel> {
    return this.restService.post(['requirement-view', requirementId.toString(), 'new'], params);
  }

  linkToHighLevelRequirement(requirementId: number, highLevelRequirementId: number): Observable<LinkedHighLevelRequirement> {
    return this.restService.post(['high-level-requirement', requirementId.toString(), 'link', highLevelRequirementId.toString()]);
  }

  unlinkFromHighLevelRequirement(requirementId: number): Observable<LinkedHighLevelRequirement> {
    return this.restService.delete(['high-level-requirement', requirementId.toString(), 'unlink']);
  }

  convertToHighLevelRequirement(requirementId: number): Observable<any> {
    return this.restService.post(['high-level-requirement', 'convert-into-high-level-requirement', requirementId.toString()]);
  }

  convertToStandardRequirement(requirementId: number): Observable<any> {
    return this.restService.post(['high-level-requirement', 'convert-into-standard-requirement', requirementId.toString()]);
  }

  getRequirementVersionIssuesCount(requirementVersionId: number): Observable<number> {
    return this.restService.get(['requirement-view', requirementVersionId.toString(), 'issues-count']);
  }

  getRequirementVersionStatistics(requirementVersionId: number): Observable<RequirementVersionStatsBundle> {
    return this.restService.get(['requirement-view', requirementVersionId.toString(), 'statistics']);
  }
}

import {TestBed} from '@angular/core/testing';

import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CampaignService} from './campaign.service';
import {TestingUtilsModule} from '../../../ui/testing-utils/testing-utils.module';

describe('CampaignService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [TestingUtilsModule, HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: CampaignService = TestBed.inject(CampaignService);
    expect(service).toBeTruthy();
  });
});

import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {Identifier} from '../../../model/entity.model';
import {Observable} from 'rxjs';
import {RequirementStatistics} from '../../../model/requirement/requirement-statistics.model';

@Injectable({
  providedIn: 'root'
})
export class RequirementStatisticsService {

  constructor(private restService: RestService) {
  }

  fetchStatistics(references: Identifier[], extendedHighLvlReqScope: boolean): Observable<RequirementStatistics> {
    return this.restService.post<RequirementStatistics>(['requirement', 'statistics'], {references, extendedHighLvlReqScope});
  }
}

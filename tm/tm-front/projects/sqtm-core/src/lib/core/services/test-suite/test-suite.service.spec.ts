import {TestBed} from '@angular/core/testing';

import {TestSuiteService} from './test-suite.service';
import {TestingUtilsModule} from '../../../ui/testing-utils/testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('TestSuiteService', () => {
  let service: TestSuiteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule]
    });
  });

  it('should be created', () => {
    service = TestBed.inject(TestSuiteService);
    expect(service).toBeTruthy();
  });
});

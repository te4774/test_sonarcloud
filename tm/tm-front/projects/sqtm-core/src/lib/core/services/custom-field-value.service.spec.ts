import {TestBed} from '@angular/core/testing';

import {CustomFieldValueService} from './custom-field-value.service';
import {RestService} from './rest.service';

describe('CustomFieldValueService', () => {
  const restService = jasmine.createSpyObj('RestService', ['instanr']);
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{
      provide: RestService,
      useValue: restService
    }]
  }));

  it('should be created', () => {
    const service: CustomFieldValueService = TestBed.inject(CustomFieldValueService);
    expect(service).toBeTruthy();
  });
});

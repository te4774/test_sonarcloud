import {TestBed} from '@angular/core/testing';

import {RequirementVersionService} from './requirement-version.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestingUtilsModule} from '../../../ui/testing-utils/testing-utils.module';

describe('RequirementVersionService', () => {
  let service: RequirementVersionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestingUtilsModule, HttpClientTestingModule]
    });
    service = TestBed.inject(RequirementVersionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

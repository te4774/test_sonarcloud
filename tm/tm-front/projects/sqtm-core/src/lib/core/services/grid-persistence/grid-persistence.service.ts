import {Injectable} from '@angular/core';
import {
  getEmptyState,
  GridFilterSnapshot,
  GridPersistenceLevel,
  GridPersistenceState,
  GridStateSnapshot,
  gridStateSnapshotAdapter,
  selectGridSnapshot
} from './grid-state-snapshot';
import {createStore, Store} from '../../store/store';
import {GridService} from '../../../ui/grid/services/grid.service';
import {sqtmCoreLogger} from '../../../lib.logger';
import {BehaviorSubject, combineLatest, Observable, Subject, Subscription} from 'rxjs';
import {LocalPersistenceService} from '../local-persistence.service';
import {distinctUntilChanged, map, skip, take, tap, withLatestFrom} from 'rxjs/operators';
import {select} from '@ngrx/store';
import {Identifier} from '../../../model/entity.model';
import {SortedColumn} from '../../../ui/grid/model/column-definition.model';
import {GridFilter} from '../../../ui/grid/model/state/filter.state';
import {PaginationDisplay} from '../../../ui/grid/model/pagination-display.model';
import {ColumnWithFilter} from '../../../ui/grid/model/column-display.model';
import {Update} from '@ngrx/entity';

const logger = sqtmCoreLogger.compose('GridPersistenceService');

@Injectable({
  providedIn: 'root'
})
export class GridPersistenceService {

  private store: Store<GridPersistenceState>;

  private _readySubject = new BehaviorSubject<boolean>(false);

  public ready$: Observable<boolean> = this._readySubject.asObservable();

  private readonly grids = new Map<string, Subscription[]>();

  private _refreshTreeWithSelectedNode: Subject<Identifier> = new Subject<Identifier>();
  refreshTreeWithSelectedNode$: Observable<Identifier>;

  constructor(private localPersistenceService: LocalPersistenceService) {
    const time = new Date().getTime();
    logger.trace(`init GridPersistenceService at ` + time);
    this.localPersistenceService.get('grid-persistence-service').pipe(
      take(1)
    ).subscribe(persistedState => {
      logger.trace(`fetched GridPersistenceService at ${new Date().getTime() - time}`);
      this.createStore(persistedState);
      this._readySubject.next(true);
      this.initializeSaveOnStateChange();
    });
    this.refreshTreeWithSelectedNode$ = this._refreshTreeWithSelectedNode.asObservable();
  }

  private initializeSaveOnStateChange() {
    this.store.state$.pipe(
      skip(1),
      distinctUntilChanged()
    ).subscribe(state => {
      logger.debug(`Should update local store `, [state]);
      const stateToSave = this.keepEligibleStateToSave(state);
      logger.debug(`Should update only with eligible grids `, [stateToSave]);
      this.localPersistenceService.set('grid-persistence-service', stateToSave).subscribe();
    });
  }

  private createStore(persistedState) {
    const initialState = persistedState as GridPersistenceState || getEmptyState();
    this.store = createStore<GridPersistenceState>(initialState, {
      id: 'grid-persistence-service',
      logDiff: 'simple'
    });
  }

  private keepEligibleStateToSave(state: GridPersistenceState) {
    const gridsToSave: GridStateSnapshot[] = Object.values(state.grids.entities).filter(g => g.persistenceLevel === 'localStorage');
    const gridsStateToSave = gridStateSnapshotAdapter.setAll(gridsToSave, gridStateSnapshotAdapter.getInitialState());
    const stateToSave: GridPersistenceState = {...state, grids: gridsStateToSave};
    return stateToSave;
  }

  register(persistenceKey: string, grid: GridService, persistenceLevel: GridPersistenceLevel = 'localStorage') {
    logger.debug(`Try to register grid ${persistenceKey}`);
    if (this.grids.has(persistenceKey)) {
      throw Error(`Already registered grid events for grid ${persistenceKey}`);
    }
    const subscriptionForSelectedRows = this.getPersistedSources(grid).pipe(
      map(([selectedRowIds, openedRowIds, sortedColumns, pagination, filters, columns]) =>
        this.buildSnapshot(persistenceKey, selectedRowIds, openedRowIds, sortedColumns, pagination, filters, columns, persistenceLevel)),
      withLatestFrom(this.store.state$)
    ).subscribe(([snapshot, state]) => {
      logger.debug(`new snapshot emitted by grid ${persistenceKey}`, [snapshot]);
      const grids = gridStateSnapshotAdapter.upsertOne(snapshot, state.grids);
      this.commit({...state, grids});
    });
    this.grids.set(persistenceKey, [subscriptionForSelectedRows]);
  }

  private getPersistedSources(grid: GridService) {
    return combineLatest([
      grid.selectedRowIds$,
      grid.openedRowIds$,
      grid.sortedColumns$,
      grid.paginationDisplay$,
      grid.filters$,
      grid.columns$]);
  }

  /**
   * Extract and save one snapshot. One time...
   * @param persistenceKey the persistence key for this grid
   * @param grid the grid
   * @param persistenceLevel the desired persistence level of the snapshot. Default is currentJsContext witch will NOT persist
   * your snapshot in a long form of storage but just un current js context (cleared at the first hard navigation,
   * is not Angular navigation but real navigation involving backend document response, like plugin navigation)
   */
  saveSnapshot(persistenceKey: string, grid: GridService, persistenceLevel: GridPersistenceLevel = 'currentJsContext'): Observable<any> {
    return this.getPersistedSources(grid).pipe(
      take(1),
      withLatestFrom(this.store.state$),
      map(([[selectedRowIds, openedRowIds, sortedColumns, pagination, filters, columns], state]) => {
        const gridSnapshot =
          this.buildSnapshot(persistenceKey, selectedRowIds, openedRowIds, sortedColumns, pagination, filters, columns, persistenceLevel);
        const grids = gridStateSnapshotAdapter.upsertOne(gridSnapshot, state.grids);
        return {...state, grids};
      }),
      tap(state => this.commit(state))
    );
  }

  private buildSnapshot(persistenceKey: string,
                        selectedRowIds: Identifier[],
                        openedRowIds: Identifier[],
                        sortedColumns: SortedColumn[],
                        pagination: PaginationDisplay,
                        filters: GridFilter[],
                        columns: ColumnWithFilter[],
                        persistenceLevel: GridPersistenceLevel) {
    const filterSnapshots: GridFilterSnapshot[] = filters.map(f => ({id: f.id, value: f.value}));
    const gridSnapshot: GridStateSnapshot = {
      id: persistenceKey,
      selectedRowIds: selectedRowIds,
      openedRows: openedRowIds,
      sortedColumns: sortedColumns,
      pagination: {
        page: pagination.page,
        size: pagination.size
      },
      filters: filterSnapshots,
      columns: columns,
      persistenceLevel
    };
    return gridSnapshot;
  }

  selectGridSnapshot(persistenceKey: string): Observable<GridStateSnapshot> {
    return this.store.state$.pipe(
      select(selectGridSnapshot(persistenceKey)),
      tap(snapshot => {
        if (logger.isDebugEnabled()) {
          logger.debug(`Select snapshot for grid ${persistenceKey}. `, [snapshot]);
        }
      })
    );
  }

  popGridSnapshot(persistenceKey: string): Observable<GridStateSnapshot> {
    return this.store.state$.pipe(
      take(1),
      select(selectGridSnapshot(persistenceKey)),
      withLatestFrom(this.store.state$),
      tap(([snapshot, state]) => {
        if (logger.isDebugEnabled()) {
          logger.debug(`Pop snapshot for grid ${persistenceKey}. `, [snapshot]);
        }
        if (!snapshot.noSnapshotExisting) {
          const grids = gridStateSnapshotAdapter.removeOne(persistenceKey as any, state.grids);
          this.commit({...state, grids});
        }
      }),
      map(([snapshot]) => snapshot)
    );
  }

  updateGridSnapshot(persistenceKey: string, changes: Partial<GridStateSnapshot>): Observable<void> {
    return this.store.state$.pipe(
      take(1),
      map((state) => {
          const update: Update<GridStateSnapshot> = {id: persistenceKey, changes: changes};
          const grids = gridStateSnapshotAdapter.updateOne(update, state.grids);
          this.commit({...state, grids});
      })
    );
  }

  unregister(persistenceKey: string) {
    logger.debug(`Try to unregister grid ${persistenceKey}`);
    const subscriptions = this.grids.get(persistenceKey);
    if (subscriptions) {
      logger.debug(`Try to unsub grid ${persistenceKey}`);
      subscriptions.forEach(sub => sub.unsubscribe());
      this.grids.delete(persistenceKey);
      logger.debug(`delete grid ${persistenceKey} : ${this.grids.has(persistenceKey)}`);
    }

  }

  private commit(state: GridPersistenceState) {
    if (logger.isDebugEnabled()) {
      logger.debug(`New grid persistence transient state. `, [state]);
    }
    this.store.commit(state);
  }

  refreshTreeWithSelectedNode(selectedNodeId: Identifier) {
    this._refreshTreeWithSelectedNode.next(selectedNodeId);
  }
}



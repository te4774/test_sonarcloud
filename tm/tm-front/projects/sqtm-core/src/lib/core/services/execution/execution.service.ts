import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {Observable} from 'rxjs';
import {ExecutionModel} from '../../../model/execution/execution.model';

@Injectable({
  providedIn: 'root'
})
export class ExecutionService {

  constructor(private restService: RestService) {
  }

  fetchExecutionData(executionId: number): Observable<ExecutionModel> {
    return this.restService.getWithoutErrorHandling<ExecutionModel>(['execution', executionId.toString()]);
  }

  createNewAfterModificationDuringExecution(executionId: number): Observable<{ id: number }> {
    return this.restService.post<{ id: number }>(['execution', executionId.toString(), 'update-from-tc']);
  }
}

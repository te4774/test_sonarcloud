import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {Observable} from 'rxjs';
import {
  ChartBinding,
  CustomDashboardBinding,
  CustomDashboardModel,
  DroppedNewItemInDashboard,
  isChartBinding,
  isReportBinding,
  ReportBinding
} from '../../../model/custom-report/custom-dashboard.model';
import {WorkspaceKeys} from '../../../model/level-enums/level-enum';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomDashboardService {

  constructor(private restService: RestService) {
  }

  getDashboard(customReportLibraryNodeId: number): Observable<CustomDashboardModel> {
    return this.restService.getWithoutErrorHandling<CustomDashboardModel>(['dashboard-view', customReportLibraryNodeId.toString()]);
  }

  getDashboardWithDynamicScope(customReportLibraryNodeId: number, dynamicScope: DashboardDynamicScope): Observable<CustomDashboardModel> {
    return this.restService.post<CustomDashboardModel>(['dashboard-view', customReportLibraryNodeId.toString()], {...dynamicScope});
  }

  addNewChartInDashboard(dashboardId: number, newBinding: DroppedNewItemInDashboard): Observable<ChartBinding> {
    return this.restService.post<ChartBinding>(['dashboard', dashboardId.toString(), 'chart-binding'], newBinding);
  }

  addNewReportInDashboard(dashboardId: number, newBinding: DroppedNewItemInDashboard): Observable<ReportBinding> {
    return this.restService.post<ReportBinding>(['dashboard', dashboardId.toString(), 'report-binding'], newBinding);
  }

  removeChartFromDashboard(bindingId: number): Observable<void> {
    return this.restService.delete<void>(['dashboard', 'chart-binding', bindingId.toString()]);
  }

  removeReportFromDashboard(bindingId: number): Observable<void> {
    return this.restService.delete<void>(['dashboard', 'report-binding', bindingId.toString()]);
  }

  swapChart(bindingId: number, chartNodeId: number): Observable<ChartBinding> {
    return this.restService.post<ChartBinding>(['dashboard', 'chart-binding', bindingId.toString(), 'swap', chartNodeId.toString()]);
  }

  swapReport(bindingId: number, reportNodeId: number): Observable<ReportBinding> {
    return this.restService.post<ReportBinding>(['dashboard', 'report-binding', bindingId.toString(), 'swap', reportNodeId.toString()]);
  }

  changeItemsPosition(bindings: CustomDashboardBinding[]): Observable<ReportBinding> {
    const charts = bindings.filter(b => isChartBinding(b));
    const reports = bindings.filter(b => isReportBinding(b));
    return this.restService.post<ReportBinding>(['dashboard', 'bindings'], {charts, reports});
  }

  bindToFavoriteDashboard(customReportLibraryNodeId: number, workspaceId: string): Observable<WorkspaceKeys[]> {
    return this.restService.post<{ workspaces: WorkspaceKeys[] }>(['dashboard', 'favorite',
      workspaceId, customReportLibraryNodeId.toString()])
      .pipe(
        map(response => response.workspaces)
      );
  }

  removeDashboardFromFavorites(customReportLibraryNodeId: number): Observable<void> {
    return this.restService.delete<void>(['dashboard', 'favorite', customReportLibraryNodeId.toString()]);
  }

  removeWorkspaceFromFavoriteDashboard(customReportLibraryNodeId: number, workspaceId: string): Observable<void> {
    return this.restService.delete<void>(['dashboard', 'favorite',
      workspaceId, customReportLibraryNodeId.toString()]);
  }
}

export interface DashboardDynamicScope {
  requirementLibraryIds?: number[];
  requirementFolderIds?: number[];
  requirementIds?: number[];
  extendedHighLvlReqScope?: boolean;
  testCaseLibraryIds?: number[];
  testCaseFolderIds?: number[];
  testCaseIds?: number[];
  campaignFolderIds?: number[];
  campaignIds?: number[];
  iterationIds?: number[];
  milestoneDashboard: boolean;
  workspaceName: WorkspaceKeys;
}

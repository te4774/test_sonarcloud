import {EntityState} from '@ngrx/entity';
import {BugTrackerReferentialDto} from '../../../model/bugtracker/bug-tracker.model';

export interface BugTrackerState extends EntityState<BugTrackerReferentialDto> {

}

export type BugTrackerStateReadOnly = Readonly<BugTrackerState>;

export function initialBugTrackerState(): BugTrackerStateReadOnly {
  return {
    ids: [],
    entities: {}
  };
}

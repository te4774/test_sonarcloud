import {Project} from '../../../model/project/project.model';
import {InfoList} from '../../../model/infolist/infolist.model';
import {Milestone} from '../../../model/milestone/milestone.model';
import {TestAutomationServer} from '../../../model/test-automation/test-automation-server.model';
import {RequirementVersionLinkType} from '../../../model/requirement/requirement-version-link-type.model';
import {AutomatedTestTechnology} from '../../../model/automation/automated-test-technology.model';
import {WorkspaceWizard} from '../../../model/workspace-wizard/workspace-wizard.model';
import {ScmServer} from '../../../model/scm-server/scm-server.model';
import {AdminReferentialDataModel} from './admin-referential-data.model';

/**
 * Class representing the ReferentialData fetched from server.
 */
export class ReferentialData extends AdminReferentialDataModel {
  projects: Project[];
  infoLists: InfoList[];
  filteredProjectIds: number[];
  projectFilterStatus: boolean;
  milestones: Milestone[];
  automationServers: TestAutomationServer[];
  requirementVersionLinkTypes: RequirementVersionLinkType[];
  workspacePlugins: WorkspacePlugin[];
  workspaceWizards: WorkspaceWizard[];
  automatedTestTechnologies: AutomatedTestTechnology[];
  scmServers: ScmServer[];
  premiumPluginInstalled: boolean;
}

export interface WorkspacePlugin {
  id: string;
  workspaceId: string;
  name: string;
  iconName: string;
  tooltip: string;
  theme: string;
  url: string;
}

import {createFeatureSelector, createSelector, Selector} from '@ngrx/store';
import {initialProjectState, ProjectState} from './project.state';
import {Identifier} from '../../../model/entity.model';
import {InfoListAttribute, Project} from '../../../model/project/project.model';
import {
  ActivatedPluginsByWorkspace,
  createEmptyActivatedPluginsByWorkspace,
  createEmptyBindingByEntity,
  CustomFieldBindingData,
  CustomFieldBindingDataByEntity,
  ProjectData,
  WorkspaceTypeForPlugins
} from '../../../model/project/project-data.model';
import {initialUserState, UserState} from './user.state';
import {CustomFieldState, initialCustomFieldState} from './custom-field-state';
import {BindableEntity} from '../../../model/bindable-entity.model';
import {CustomFieldBinding} from '../../../model/customfield/custom-field-binding.model';
import {provideGlobalConfigurationInitialState} from './global-configuration.state';
import {InfoListState, initialInfoListState} from './info-list.state';
import {ADMIN_PERMISSIONS, NO_PERMISSIONS} from '../../../model/permissions/permissions.model';
import {BugTrackerState, initialBugTrackerState} from './bug-tracker-state';
import {initialMilestoneState, MilestoneState, selectAllMilestone} from './milestone.state';
import {Milestone, MilestoneView} from '../../../model/milestone/milestone.model';
import {initialTestAutomationServerState, TestAutomationServerState} from './test-automation-server.state';
import {initialMilestoneFilterState, MilestoneFilterState} from './milestone-filter.state';
import {
  initialRequirementVersionLinkTypeState,
  RequirementVersionLinkTypeState
} from './requirement-version-link-type.state';
import {WorkspacePlugin} from './referential-data.model';
import {AdminReferentialDataState, getGlobalConfiguration} from './admin-referential-data.state';
import {provideLicenseInformationInitialState} from './license-information.state';
import {AutomatedTestTechnologyState, initialAutomatedTestTechnologyState} from './automated-test-technology.state';
import {WorkspaceWizard} from '../../../model/workspace-wizard/workspace-wizard.model';
import {initialScmServerState, ScmServerState} from './scm-server.state';
import {MilestoneStatus} from '../../../model/level-enums/level-enum';

export interface ReferentialDataState extends AdminReferentialDataState {
  stateVersion: string;
  projectState: ProjectState;
  infoListState: InfoListState;
  bugTrackerState: BugTrackerState;
  milestoneState: MilestoneState;
  taServerState: TestAutomationServerState;
  milestoneFilterState: MilestoneFilterState;
  requirementVersionLinkTypeState: RequirementVersionLinkTypeState;
  workspacePlugins: WorkspacePlugin[];
  workspaceWizards: WorkspaceWizard[];
  automatedTestTechnologiesState: AutomatedTestTechnologyState;
  scmServerState: ScmServerState;
  premiumPluginInstalled: boolean;
}

export type ReferentialDataStateReadOnly = Readonly<ReferentialDataState>;

export function initialReferentialDataState(): ReferentialDataStateReadOnly {
  return {
    stateVersion: '',
    loaded: false,
    projectState: initialProjectState(),
    infoListState: initialInfoListState(),
    userState: initialUserState(),
    customFieldState: initialCustomFieldState(),
    globalConfigurationState: provideGlobalConfigurationInitialState(),
    bugTrackerState: initialBugTrackerState(),
    milestoneState: initialMilestoneState(),
    taServerState: initialTestAutomationServerState(),
    milestoneFilterState: initialMilestoneFilterState(),
    requirementVersionLinkTypeState: initialRequirementVersionLinkTypeState(),
    workspacePlugins: [],
    workspaceWizards: [],
    licenseInformation: provideLicenseInformationInitialState(),
    automatedTestTechnologiesState: initialAutomatedTestTechnologyState(),
    availableTestAutomationServerKinds: [],
    scmServerState: initialScmServerState(),
    canManageLocalPassword: false,
    templateConfigurablePlugins: [],
    bugTrackers: initialBugTrackerState(),
    premiumPluginInstalled: false,
    documentationLinks: [],
    callbackUrl: null,
  };
}

export const getLoadedState: Selector<ReferentialDataState, boolean> =
  createFeatureSelector<ReferentialDataState, boolean>('loaded');

export const getProjectState: Selector<ReferentialDataState, ProjectState> =
  createFeatureSelector<ReferentialDataState, ProjectState>('projectState');

export const getInfoListState: Selector<ReferentialDataState, InfoListState> =
  createFeatureSelector<ReferentialDataState, InfoListState>('infoListState');

export const getAutomatedTestTechnologiesState: Selector<ReferentialDataState, AutomatedTestTechnologyState> =
  createFeatureSelector<ReferentialDataState, AutomatedTestTechnologyState>('automatedTestTechnologiesState');

export const getScmServersState: Selector<ReferentialDataState, ScmServerState> =
  createFeatureSelector<ReferentialDataState, ScmServerState>('scmServerState');

export const getUserState: Selector<ReferentialDataState, UserState> =
  createFeatureSelector<ReferentialDataState, UserState>('userState');

export const getCustomFieldState: Selector<ReferentialDataState, CustomFieldState> =
  createFeatureSelector<ReferentialDataState, CustomFieldState>('customFieldState');

export const getBugTrackerState: Selector<ReferentialDataState, BugTrackerState> =
  createFeatureSelector<ReferentialDataState, BugTrackerState>('bugTrackerState');

export const getMilestoneState: Selector<ReferentialDataState, MilestoneState> =
  createFeatureSelector<ReferentialDataState, MilestoneState>('milestoneState');

export const selectAllVisibleMilestones = createSelector(getMilestoneState, selectAllMilestone);

export const getTaServerState: Selector<ReferentialDataState, TestAutomationServerState> =
  createFeatureSelector<ReferentialDataState, TestAutomationServerState>('taServerState');

export const getMilestoneFilterState: Selector<ReferentialDataState, MilestoneFilterState> =
  createFeatureSelector<ReferentialDataState, MilestoneFilterState>('milestoneFilterState');

export const getRequirementVersionLinkTypeState: Selector<ReferentialDataState, RequirementVersionLinkTypeState> =
  createFeatureSelector<ReferentialDataState, RequirementVersionLinkTypeState>('requirementVersionLinkTypeState');

export function projectDataSelectorFactory(projectId: Identifier) {
  return createSelector(getProjectState, getInfoListState, getCustomFieldState, getUserState, getBugTrackerState,
    getMilestoneState, getTaServerState, (projectState: ProjectState, infoListState: InfoListState,
                                          customFieldState: CustomFieldState, userState: UserState, bugTrackerState: BugTrackerState,
                                          milestoneState: MilestoneState, taServerState: TestAutomationServerState) => {
      return generateProjectData(projectState, projectId, customFieldState, infoListState, userState, bugTrackerState,
        milestoneState, taServerState);
    });
}

export function customFieldSelectorFactory(customFieldId: Identifier) {
  return createSelector(getCustomFieldState, (customFieldState: CustomFieldState) => {
    return customFieldState.entities[customFieldId];
  });
}

export function selectInfoListItem(itemId: number) {
  return createSelector(getInfoListState, infoListState => {
    return Object.values(infoListState.entities)
      .map(list => list.items)
      .flat()
      .find(item => item.id === itemId);
  });
}

/**
 * Allow selection of all infolist at a given spot (nature, type...) for a given sets of projects.
 * @param projectIds The project ids list
 * @param attribute The slot (tcNature, tcType or reqCategory)
 */
export function selectInfolistByProjectsFactory(
  projectIds: number[] = [],
  attribute: InfoListAttribute) {
  return createSelector(getProjectState, getInfoListState, (projectState: ProjectState, infoListState: InfoListState) => {
    const ids = (projectState.ids as number[]).filter(id => projectIds.includes(id));
    const infoListIds: number[] = ids
      .map(id => projectState.entities[id])
      .map(project => project[attribute]);
    const uniqueInfoListIds = new Set(infoListIds);
    return Array.from(uniqueInfoListIds).map(id => infoListState.entities[id]);
  });
}

export function selectTestCaseNatureByProjectsFactory(projectIds: number[] = []) {
  return createSelector(getProjectState, getInfoListState, (projectState: ProjectState, infoListState: InfoListState) => {
    const ids = (projectState.ids as number[]).filter(id => projectIds.includes(id));
    const infoListIds: number[] = ids
      .map(id => projectState.entities[id])
      .map(project => project.testCaseNatureId);
    const uniqueInfoListIds = new Set(infoListIds);
    return Array.from(uniqueInfoListIds).map(id => infoListState.entities[id]);
  });
}

export function selectTestCaseTypeByProjectsFactory(projectIds: number[] = []) {
  return createSelector(getProjectState, getInfoListState, (projectState: ProjectState, infoListState: InfoListState) => {
    const ids = (projectState.ids as number[]).filter(id => projectIds.includes(id));
    const infoListIds: number[] = ids
      .map(id => projectState.entities[id])
      .map(project => project.testCaseTypeId);
    const uniqueInfoListIds = new Set(infoListIds);
    return Array.from(uniqueInfoListIds).map(id => infoListState.entities[id]);
  });
}

export function selectMilestonesByProjectsFactory(projectIds: number[] = []) {
  return createSelector(getProjectState, getMilestoneState, (projectState: ProjectState, milestoneState: MilestoneState) => {
    const ids = (projectState.ids as number[]).filter(id => projectIds.includes(id));
    const milestoneIds = ids
      .map(id => projectState.entities[id])
      .map(project => project.milestoneBindings.map(bindings => bindings.milestoneId));

    const newMilestoneIds: number[] = [].concat(...milestoneIds);
    const unique = new Set(newMilestoneIds);
    return Array.from(unique).map(id => milestoneState.entities[id]);
  });
}

function getProjectPermissions(userState: UserState, project: Project) {
  let permissions;
  if (userState.admin) {
    permissions = ADMIN_PERMISSIONS;
  } else {
    permissions = {...NO_PERMISSIONS, ...project.permissions};
  }
  return permissions;
}

function getBugTracker(bugTrackerState: BugTrackerState, project: Project) {
  const bugTrackerBinding = project.bugTrackerBinding;
  let bugTracker = null;
  if (bugTrackerBinding) {
    bugTracker = bugTrackerState.entities[bugTrackerBinding.bugTrackerId];
  }

  return bugTracker;
}

function getMilestonesView(milestoneState: MilestoneState, project: Project) {
  const milestoneBindings = project.milestoneBindings;
  const milestoneView: MilestoneView[] = [];
  milestoneBindings.forEach(bindings => {
    const milestone = milestoneState.entities[bindings.milestoneId];
    let bindableToObject = false;
    if (milestone.status === 'IN_PROGRESS' || milestone.status === 'FINISHED') {
      bindableToObject = true;
    }

    milestoneView.push({...milestone, boundToObject: bindableToObject});
  });
  return milestoneView;
}

function getTaServer(taServerState: TestAutomationServerState, project: Project) {
  const serverId = project.taServerId;
  let taServer = null;
  if (serverId) {
    taServer = taServerState.entities[serverId];
  }
  return taServer;
}

export function generateProjectData(
  projectState: ProjectState, projectId, customFieldState: CustomFieldState, infoListState: InfoListState, userState: UserState,
  bugTrackerState: BugTrackerState, milestoneState: MilestoneState, taServerState: TestAutomationServerState): ProjectData {
  const project: Project = projectState.entities[projectId];
  const customFieldBindingList: CustomFieldBindingDataByEntity = createEmptyBindingByEntity();

  for (const bindableEntity of Object.keys(BindableEntity)) {
    const customFieldBindings: CustomFieldBinding[] = project.customFieldBindings[bindableEntity];

    for (const customFieldBinding of customFieldBindings) {
      const customFieldData: CustomFieldBindingData = {
        ...customFieldBinding,
        customField: customFieldState.entities[customFieldBinding.customFieldId]
      };
      customFieldBindingList[bindableEntity].push(customFieldData);
    }
  }

  const activatedPlugins: ActivatedPluginsByWorkspace = createEmptyActivatedPluginsByWorkspace();

  for (const workspaceType of Object.keys(WorkspaceTypeForPlugins)) {
    const activatedPluginsList: string [] = project.activatedPlugins[workspaceType];

    for (const activatedPlugin of activatedPluginsList) {
      activatedPlugins[workspaceType].push(activatedPlugin);
    }
  }

  const permissions = getProjectPermissions(userState, project);
  const bugTracker = getBugTracker(bugTrackerState, project);
  const milestoneView = getMilestonesView(milestoneState, project);
  const taServer = getTaServer(taServerState, project);
  const projectData: ProjectData = {
    ...project,
    permissions, // overriding project state permissions for admin and assuring a value for each perm;
    requirementCategory: infoListState.entities[project.requirementCategoryId],
    testCaseNature: infoListState.entities[project.testCaseNatureId],
    testCaseType: infoListState.entities[project.testCaseTypeId],
    customFieldBinding: customFieldBindingList,
    bugTracker,
    milestones: milestoneView,
    taServer,
    activatedPlugins: activatedPlugins
  };
  return projectData;
}

export function isAdmin(userState: UserState): boolean {
  return userState.admin;
}

export function isAdminOrProjectManager(userState: UserState): boolean {
  return userState.admin || userState.projectManager;
}

export function selectMilestonesFactory(milestoneIds: number[] = []) {
  return createSelector(getMilestoneState, (milestoneState: MilestoneState) => {
    const ids = (milestoneState.ids as number[]).filter(id => milestoneIds.includes(id));
    const milestones: Milestone[] = ids
      .map(id => milestoneState.entities[id]);
    return milestones;
  });
}

export const selectMilestoneModeData =
  createSelector(getGlobalConfiguration, getMilestoneState, getMilestoneFilterState, getProjectState,
    (globalConf, milestoneState, milestoneFilterState, projectState) => {
      let milestones = Object.values(milestoneState.entities);

      if (projectState.filterActivated) {
        const projectIds = projectState.filteredProjectSet;
        const milestoneIds = projectIds
          .map(id => projectState.entities[id])
          .filter(projectOrNull => projectOrNull != null)
          .map(project => project.milestoneBindings.map(bindings => bindings.milestoneId));

        const newMilestoneIds: number[] = [].concat(...milestoneIds);
        const unique = new Set(newMilestoneIds);
        milestones = Array.from(unique)
          .map(milestoneId => milestoneState.entities[milestoneId]);
      }

      milestones = milestones.filter(m => m.status !== MilestoneStatus.PLANNED.id);

      const selectedMilestone = milestoneState.entities[milestoneFilterState.selectedMilestoneId];
      return {
        milestoneFeatureEnabled: globalConf.milestoneFeatureEnabled,
        milestones,
        milestoneModeEnabled: milestoneFilterState.milestoneModeEnabled,
        selectedMilestone
      };
    });

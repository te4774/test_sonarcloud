export interface ApiDocumentationLink {
  label: string;
  url: string;
}

import {Inject, isDevMode, LOCALE_ID, ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {de_DE, en_GB, es_ES, fr_FR, NZ_DATE_LOCALE, NzI18nService} from 'ng-zorro-antd/i18n';
import {NzIconModule, NzIconService} from 'ng-zorro-antd/icon';
import {NzMessageModule} from 'ng-zorro-antd/message';
import {NzNotificationModule} from 'ng-zorro-antd/notification';
import {BACKEND_CONTEXT_PATH, CORE_MODULE_CONFIGURATION, CORE_MODULE_USER_CONFIGURATION} from './sqtm-core.tokens';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MultiTranslateHttpLoader} from 'ngx-translate-multi-http-loader';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {APP_BASE_HREF, Location, LocationStrategy, PathLocationStrategy, registerLocaleData} from '@angular/common';
import fr from '@angular/common/locales/fr';
import en from '@angular/common/locales/en';
import es from '@angular/common/locales/es';
import de from '@angular/common/locales/de';
import {OverlayModule} from '@angular/cdk/overlay';
import {
  ArrowDownOutline,
  ArrowUpOutline,
  MinusOutline,
  PlusCircleOutline,
  PlusOutline,
  PlusSquareOutline,
  ReloadOutline,
  TableOutline,
} from '@ant-design/icons-angular/icons';
import {SquashPlatformNavigationService} from './services/navigation/squash-platform-navigation.service';
import {CookieService} from 'ngx-cookie-service';
import {sqtmIconLib} from './generated-icons/icons';

import DateFns_de from 'date-fns/locale/de';
import DateFns_enGB from 'date-fns/locale/en-GB';
import DateFns_es from 'date-fns/locale/es';
import DateFns_fr from 'date-fns/locale/fr';
import {RestService} from './services/rest.service';

const defaultCoreTranslateFile = './assets/sqtm-core/i18n/translations_';
const defaultCustomTranslateFile = './custom_translations_';

export function sqtmConfigurationFactory(userConf: Partial<SqtmCoreModuleConfiguration>) {
  const conf = userConf ? {...defaultSqtmConfiguration, ...userConf} : {...defaultSqtmConfiguration};
  conf.ngxTranslateFiles = [defaultCoreTranslateFile, ...conf.ngxTranslateFiles,  defaultCustomTranslateFile];
  if (!Boolean(conf.pluginIdentifier)) {
    throw new Error('PluginIdentifier is mandatory when instantiating core module. Provide one unique identifier for your plugin');
  }
  return conf;

}

export function HttpLoaderFactory(http: HttpClient, conf: SqtmCoreModuleConfiguration) {
  const resources = conf.ngxTranslateFiles.map(prefix => {
    return {prefix, suffix: '.json'};
  });
  return new MultiTranslateHttpLoader(http, resources);
}

export function localeIdFactory(translateService: TranslateService): string {
  if (isDevMode()) {
    console.log('Initialize Locale Id');
  }

  const handledAngularLocales = ['en', 'fr', 'de', 'es'];
  const browserLang = translateService.getBrowserLang();
  if (!handledAngularLocales.includes(browserLang)) {
    console.warn('Unable to load locale for Angular pipes. Will use en_GB by default');
    return 'en';
  } else {
    return browserLang;
  }
}


@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    OverlayModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient, CORE_MODULE_CONFIGURATION]
      }
    }),
    NzIconModule,
    NzMessageModule,
    NzNotificationModule
  ],
  providers: [
    CookieService,

    // Set the value of NZ_DATE_LOCALE in the application root module to activate date-fns mode
    // This is needed to tell NgZorro that it should use date-fns for date parsing instead of built-in
    // Angular features. The default value gets overridden in setLocale().
    // See: https://ng.ant.design/docs/i18n/en#how-to-format-a-date-using-date-fns
    { provide: NZ_DATE_LOCALE, useValue: DateFns_enGB },
  ],
  exports: []
})
export class SqtmCoreModule {

  static forRoot(userConf: Partial<SqtmCoreModuleConfiguration>): ModuleWithProviders<SqtmCoreModule> {
    return {
      ngModule: SqtmCoreModule,
      providers: [
        Location,
        {provide: LocationStrategy, useClass: PathLocationStrategy},
        {provide: CORE_MODULE_USER_CONFIGURATION, useValue: userConf},
        {
          provide: CORE_MODULE_CONFIGURATION,
          useFactory: sqtmConfigurationFactory,
          deps: [CORE_MODULE_USER_CONFIGURATION]
        },
        {provide: LOCALE_ID, useFactory: localeIdFactory, deps: [TranslateService]}
      ]
    };
  }

  constructor(@Optional() @SkipSelf() private sqtmCoreModule: SqtmCoreModule,
              @Inject(CORE_MODULE_CONFIGURATION) private conf: SqtmCoreModuleConfiguration,
              private iconService: NzIconService,
              private ngxTranslate: TranslateService,
              private nzI18nService: NzI18nService,
              private platformNavigationService: SquashPlatformNavigationService,
              private restService: RestService,
              @Inject(APP_BASE_HREF) private baseUrl: string,
              @Inject(BACKEND_CONTEXT_PATH) private serverServletContext: string) {
    this.assertCoreModuleIsUnique(sqtmCoreModule);
    this.configureRestService(conf);
    this.configurePlatformNavigationService(conf);
    this.configureIcons(conf);
    this.setLocale();
  }

  private assertCoreModuleIsUnique(sqtmCoreModule: any) {
    if (sqtmCoreModule) {
      const message = `MULTIPLE SQTM CORE MODULE INSTANTIATED.
      IT WILL LEAD TO SEVERAL INSTANCES OF SERVICES, MULTIPLE FETCH OF I18N FILES, AND OTHER HORRORS.
      CORE MODULE SHOULD BE IMPORTED ONLY ONE TIME, IN APP MODULE.`;
      throw Error(message);
    }
  }
  private configureRestService(conf: SqtmCoreModuleConfiguration) {
    this.restService.backendContextPath = this.serverServletContext;
    this.restService.backendRootUrl = conf.backendRootUrl;
  }

  private configurePlatformNavigationService(conf: SqtmCoreModuleConfiguration) {
    this.platformNavigationService.pluginIdentifier = conf.pluginIdentifier;
    this.platformNavigationService.backendContextPath = this.serverServletContext;
  }

  private configureIcons(conf: SqtmCoreModuleConfiguration) {
    this.changeNgZorroAssetRoot(conf);
    this.addNativeNgZorroIcons();
    this.addCustomIcons();
  }

  private addCustomIcons() {
    sqtmIconLib.forEach(({id, content}) => {
      this.iconService.addIconLiteral(id, content);
    });
  }

  private addNativeNgZorroIcons() {
    this.iconService.addIcon(
      ArrowDownOutline,
      ArrowUpOutline,
      MinusOutline,
      PlusCircleOutline,
      PlusOutline,
      PlusSquareOutline,
      ReloadOutline,
      TableOutline,
    );
  }

  private changeNgZorroAssetRoot(conf: SqtmCoreModuleConfiguration) {
    this.iconService.changeAssetsSource(this.baseUrl + conf.ngZorroIconAssetsSource);
  }

  private setLocale() {
    const browserLang = this.ngxTranslate.getBrowserLang();
    switch (browserLang) {
      case 'en':
        this.nzI18nService.setLocale(en_GB);
        this.nzI18nService.setDateLocale(DateFns_enGB);
        registerLocaleData(en);
        this.ngxTranslate.use(browserLang);
        break;
      case 'fr':
        this.nzI18nService.setLocale(fr_FR);
        this.nzI18nService.setDateLocale(DateFns_fr);
        registerLocaleData(fr);
        this.ngxTranslate.use(browserLang);
        break;
      case 'de':
        this.nzI18nService.setLocale(de_DE);
        this.nzI18nService.setDateLocale(DateFns_de);
        registerLocaleData(de);
        this.ngxTranslate.use(browserLang);
        break;
      case 'es':
        this.nzI18nService.setLocale(es_ES);
        this.nzI18nService.setDateLocale(DateFns_es);
        registerLocaleData(es);
        this.ngxTranslate.use(browserLang);
        break;
      default:
        console.warn('Unable to load locale for ngZorro. Will use en_GB by default');
        this.nzI18nService.setLocale(en_GB);
        this.nzI18nService.setDateLocale(DateFns_enGB);
        this.ngxTranslate.use('en');
        registerLocaleData(en);
    }
  }


}

export interface SqtmCoreModuleConfiguration {
  pluginIdentifier: string;
  backendRootUrl: string;
  ngxTranslateFiles: string[];
  ngZorroIconAssetsSource: string;
}

export const defaultSqtmConfiguration: Readonly<SqtmCoreModuleConfiguration> = {
  pluginIdentifier: null,
  backendRootUrl: 'backend',
  ngxTranslateFiles: [],
  ngZorroIconAssetsSource: 'assets/sqtm-core/icons',
};



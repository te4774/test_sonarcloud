/**
 * Ease localization of dates with NgZorro components.
 *
 * Why this helper ?
 * NgZorro's DatePicker component has a known bug caused by Angular's way of handling dates.
 * (see https://github.com/NG-ZORRO/ng-zorro-antd/issues/3976 and https://ng.ant.design/docs/i18n/en#how-to-format-a-date-using-date-fns)
 * To avoid this, we tell NgZorro to use date-fns instead of Angular built-in features, which in turn prevent us from using
 * the Angular locale agnostic format strings such as "shortDate" for [nzFormat] inputs. As a workaround, we explicitly specify the expected
 * date format based on the current locale. We try to mimic Angular behaviour to minimize inconsistencies across the app.
 */

export class DateFormatUtils {
  static shortDate(locale: string): string {
    switch (locale) {
      case 'de':
        return 'dd.MM.yy';
      case 'fr':
        return 'dd/MM/yyyy';
      case 'es':
        return 'd/M/yy';
      case 'en':
      default:
        return 'M/d/yy';
    }
  }

  static short(locale: string): string {
    switch (locale) {
      case 'de':
        return 'dd.MM.yy, hh:mm';
      case 'es':
        return 'd/M/yy hh:mm';
      case 'fr':
        return 'dd/MM/yyyy HH:mm';
      case 'en':
      default:
        return 'M/d/yy, h:mm a';
    }
  }

  // simple function to avoid 01/01/1970 date in place of null values
  static createDateFromIsoString(candidate: string): Date {
    return candidate ? new Date(candidate) : null;
  }
}

import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {FRONT_END_ERROR_IS_HANDLED_PARAM, STACK_TRACE_PARAM} from '../services/rest.service';
import {GenericErrorDisplayService} from '../services/errors-handling/generic-error-display.service';

export class ServerInternalErrorResponseInterceptor implements HttpInterceptor {

  constructor(private readonly errorDisplayService: GenericErrorDisplayService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError(err => {
        const reqParams = req.params;
        const isErrorHandled = reqParams.keys().includes(FRONT_END_ERROR_IS_HANDLED_PARAM);
        if (!isErrorHandled && err instanceof HttpErrorResponse && err.status === 500) {
          const stackTraceEnabled = err.headers.get(STACK_TRACE_PARAM) === 'enable';
          this.errorDisplayService.showError(err.error, stackTraceEnabled);
          return throwError(err);
        }
        return throwError(err);
      })
    );
  }
}

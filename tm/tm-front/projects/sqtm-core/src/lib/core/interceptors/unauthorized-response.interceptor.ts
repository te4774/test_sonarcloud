import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import {Observable, Subject, throwError} from 'rxjs';
import {catchError, filter, skipUntil, take, tap} from 'rxjs/operators';
import {SquashPlatformNavigationService} from '../services/navigation/squash-platform-navigation.service';
import {CORE_MODULE_CONFIGURATION, LOGIN_PAGE_REDIRECT_AFTER_AUTH} from '../sqtm-core.tokens';
import {NavigationEnd, Router} from '@angular/router';
import {coreLogger} from '../core.logger';
import {Inject} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
// tslint:disable-next-line:import-blacklist
import {SqtmCoreModuleConfiguration} from '../sqtm-core.module';

const logger = coreLogger.compose('UnauthorizedResponseInterceptor');

export class UnauthorizedResponseInterceptor implements HttpInterceptor {

  private currentUrl: string;

  private oneRequestEmitted = new Subject<void>();

  constructor(private squashPlatformNavigationService: SquashPlatformNavigationService,
              private router: Router,
              @Inject(APP_BASE_HREF) private appBaseRef,
              @Inject(CORE_MODULE_CONFIGURATION) private sqtmCoreModuleConfiguration: SqtmCoreModuleConfiguration) {
    logger.debug('Init UnauthorizedResponseInterceptor');
    // [SQUASH-3891] We must take only one url to allow proper redirection when accessing to page by direct url.
    // Specifically we want to avoid nasty sequences like :
    // hard navigation to /test-case-workspace/test-case-library/292
    // init ng application
    // loading workspace
    // /test-case-workspace/ navigation
    // 401 for referential data fetch
    // redirect to login page with bad redirect url (/test-case-workspace/, it should be /test-case-workspace/test-case-library/292)
    this.initBeforeAuthUrlCaching();

    // once auth is realized one time we can follow real navigation, it should work
    this.initAfterAuthUrlCaching();

  }

  private initAfterAuthUrlCaching() {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      skipUntil(this.oneRequestEmitted)
    ).subscribe((event: NavigationEnd) => {
      this.cacheCurrentUrl(event);
    });
  }

  private initBeforeAuthUrlCaching() {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      take(1)
    ).subscribe((event: NavigationEnd) => {
      this.cacheCurrentUrl(event);
    });
  }

  private cacheCurrentUrl(event: NavigationEnd): void {
    const fullUrl = UnauthorizedResponseInterceptor.buildFullUrl(this.router.url, this.appBaseRef);
    logger.debug(`Update current url to ${fullUrl}`);
    this.currentUrl = fullUrl;
  }

  /**
   * Builds the current URL from (but not including) the servlet context.
   * Because plugins' routers build their URL from the base app href, we need to manually append the "plugin/xx/" part.
   * @param localeUrl the current URL as seen by the Angular router
   * @param baseUrl the app's base URL (aka servlet context when in main app or "[servlet_context]/plugin/xxx/" for a plugin).
   * @private
   */
  private static buildFullUrl(localeUrl: string, baseUrl: string): string {
    let fullUrl = localeUrl;
    const indexOfPluginInBaseUrl = baseUrl.split('/').indexOf('plugin');

    if (indexOfPluginInBaseUrl > 0) {
      const pluginPrefix = baseUrl.split('/')
        .slice(indexOfPluginInBaseUrl)
        .join('/');

      if (fullUrl.startsWith('/')) {
        fullUrl = fullUrl.slice(1);
      }

      fullUrl = pluginPrefix + fullUrl;
    }

    return fullUrl;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap((resp) => {
        if (resp instanceof HttpResponse && resp.status === 200 && resp.url.includes(this.sqtmCoreModuleConfiguration.backendRootUrl)) {
          // [SQUASH-3891] We consider that the first successful request to backend ensure auth is correct
          // i'm aware that login/logout are also in backend and doesn't require auth but they should comme after referential failure
          // or you are specifically navigating to /login and thus we don't care about redirect after auth ^^
          logger.debug(`200 from backend controllers, now tracking each front-end navigation`, [resp]);
          this.oneRequestEmitted.next();
        }
      }),
      catchError(err => {
        if (err instanceof HttpErrorResponse && err.status === 401) {
          logger.debug(`401 from server, redirect to login page. Previous known url: ${this.currentUrl}`);
          const queryParams = this.currentUrl ? {
            [LOGIN_PAGE_REDIRECT_AFTER_AUTH]: this.currentUrl
          } : {};
          this.squashPlatformNavigationService.navigateToRoot(queryParams);
          return throwError(err);
        }
        return throwError(err);
      }),
    );
  }

}

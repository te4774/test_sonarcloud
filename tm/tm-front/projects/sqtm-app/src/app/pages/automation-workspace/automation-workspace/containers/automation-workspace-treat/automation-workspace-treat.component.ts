import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AutomationWorkspaceState} from '../../states/automation-workspace.state';
import {
  AutomationRequestStatus,
  buildFilters,
  DataRow,
  dateColumn,
  DateFilterComponent,
  DateFilterValueRendererComponent,
  Extendable,
  FilterBuilder,
  FilterOperation,
  Fixed,
  grid,
  GridDefinition,
  GridFilter,
  GridFilterUtils,
  GridService,
  gridServiceFactory,
  i18nEnumResearchFilter,
  indexColumn,
  Limited,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  selectableTextColumn,
  serverBackedGridEqualFilter,
  serverBackedGridTextFilter,
  Sort,
  StyleDefinitionBuilder,
  TestCaseKind,
  textColumn,
  userHistoryResearchFilter,
  UserHistorySearchProvider
} from 'sqtm-core';
import {AutomationWorkspaceService} from '../../services/automation-workspace.service';
import {ATW_TREAT_TABLE, ATW_TREAT_TABLE_CONFIG} from '../../automation-workspace.constant';
import {automationRequestLiteralConverter} from '../../../../../components/automation/automation-utils';
import {concatMap, filter, finalize, map, take, takeUntil, tap} from 'rxjs/operators';
import {AutomationTreatmentUserProvider} from './automation-treatment-user-provider';
import {
  uuidColumn
} from '../../../../../components/automation/components/cell-renderers/uuid-cell-renderer/uuid-cell-renderer.component';
import {canEditRows, filterEditableAutomReq} from '../../automation-workspace.utils';

export function atwTreatTableDefinition(): GridDefinition {
  return grid('automation-programmer-treat')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      textColumn('projectName')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Limited(350))
        .withAssociatedFilter(),
      textColumn('tclnId')
        .withI18nKey('sqtm-core.entity.generic.id.capitalize')
        .changeWidthCalculationStrategy(new Fixed(60))
        .withAssociatedFilter(),
      textColumn('reference')
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.reference')
        .withTitleI18nKey('sqtm-core.grid.header.reference')
        .changeWidthCalculationStrategy(new Limited(150))
        .withAssociatedFilter(),
      selectableTextColumn('name')
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .changeWidthCalculationStrategy(new Limited(350))
        .withAssociatedFilter(),
      uuidColumn('uuid')
        .disableSort()
        .withI18nKey('sqtm-core.entity.test-case.uuid.label')
        .changeWidthCalculationStrategy(new Fixed(40))
        .withAssociatedFilter(),
      textColumn('kind')
        .withEnumRenderer(TestCaseKind, false, true)
        .withI18nKey('sqtm-core.entity.test-case.kind.label')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2)),
      textColumn('login')
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.tester')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter('login'),
      textColumn('automationPriority')
        .withI18nKey('sqtm-core.grid.header.priority.short')
        .withTitleI18nKey('sqtm-core.grid.header.priority.long')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter(),
      textColumn('requestStatus')
        .withEnumRenderer(AutomationRequestStatus, false, true)
        .isEditable(false)
        .withI18nKey('sqtm-core.entity.automation-request.status.label.short')
        .withTitleI18nKey('sqtm-core.entity.automation-request.status.label.full')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2))
        .withAssociatedFilter(),
      dateColumn('transmittedOn')
        .withI18nKey('sqtm-core.automation-workspace.grid.headers.transmitted-on')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1))
        .withAssociatedFilter()
    ]).server().withServerUrl(['automation-workspace/treatment-autom-req'])
    .disableRightToolBar()
    .withInitialSortedColumns([{id: 'automationPriority', sort: Sort.DESC}, {id: 'requestStatus', sort: Sort.ASC}])
    .withRowConverter(automationRequestLiteralConverter)
    .withRowHeight(35)
    .withModificationUrl(['automation-requests'])
    .withStyle(new StyleDefinitionBuilder()
      .enableInitialLoadAnimation()
      .showLines())
    .build();
}


@Component({
  selector: 'sqtm-app-automation-workspace-treat',
  templateUrl: './automation-workspace-treat.component.html',
  styleUrls: ['./automation-workspace-treat.component.less'],
  providers: [
    {
      provide: ATW_TREAT_TABLE_CONFIG,
      useFactory: atwTreatTableDefinition
    },
    {
      provide: ATW_TREAT_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ATW_TREAT_TABLE_CONFIG, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: ATW_TREAT_TABLE
    },
    {provide: UserHistorySearchProvider, useClass: AutomationTreatmentUserProvider},
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomationWorkspaceTreatComponent implements OnInit, AfterViewInit, OnDestroy {
  canEditRows$: Observable<boolean>;
  componentData$: Observable<AutomationWorkspaceState>;
  unsub$ = new Subject<void>();
  activeFilters$: Observable<GridFilter[]>;

  constructor(public referentialDataService: ReferentialDataService,
              public gridService: GridService,
              private automationWorkspaceService: AutomationWorkspaceService) {
    this.componentData$ = this.automationWorkspaceService.componentData$;
    this.canEditRows$ = this.gridService.selectedRows$.pipe(
      map(rows => canEditRows(rows))
    );
    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) => gridFilters.filter(gridFilter => GridFilterUtils.mustIncludeFilter(gridFilter))),
    );
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.componentData$.pipe(
      filter(component => component.loaded),
      take(1),
      tap(() => this.gridService.addFilters(this.buildGridFilters()))
    ).subscribe(() => {
      this.gridService.refreshData();
    });
  }

  private buildGridFilters(): GridFilter[] {
    return buildFilters(
      [
        serverBackedGridTextFilter('projectName'),
        serverBackedGridEqualFilter('tclnId'),
        serverBackedGridTextFilter('reference'),
        serverBackedGridTextFilter('name'),
        serverBackedGridEqualFilter('automationPriority'),
        i18nEnumResearchFilter('requestStatus', ResearchColumnPrototype.TO_BE_TREAT_AUTOMATION_REQUEST_STATUS)
          .alwaysActive(),
        userHistoryResearchFilter('login', ResearchColumnPrototype.TEST_CASE_MODIFIED_BY)
          .alwaysActive(),
        this.buildTransmittedDateFilter()
      ]
    );
  }

  private buildTransmittedDateFilter(): FilterBuilder {
    return new FilterBuilder('transmittedOn')
      .alwaysActive()
      .withColumnPrototype(ResearchColumnPrototype.ITEM_TEST_PLAN_LASTEXECON)
      .withAvailableOperations([FilterOperation.BETWEEN])
      .withOperations(FilterOperation.BETWEEN)
      .withInitialValue({kind: 'multiple-date-value', value: []})
      .withWidget(DateFilterComponent)
      .withValueRenderer(DateFilterValueRendererComponent);
  }

  assignAutomationRequest() {
    this.gridService.selectedRows$
      .pipe(
        take(1),
        filter(rows => filterEditableAutomReq(rows).length > 0),
        tap(() => this.gridService.beginAsyncOperation()),
        map((rows: DataRow[]) => rows.map(row => Number(row.id))),
        concatMap((tcIds: number[]) => this.automationWorkspaceService.assignAutomationRequest(tcIds)),
        finalize(() => this.gridService.completeAsyncOperation())
      ).subscribe(() => {
      this.gridService.refreshData();
    });
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters.length > 0;
  }

  resetFilters() {
    this.gridService.resetFilters();
  }
}

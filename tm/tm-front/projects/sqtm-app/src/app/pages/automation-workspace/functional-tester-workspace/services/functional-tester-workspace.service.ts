import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {
  AutomationTesterWorkspaceDataModel,
  FunctionalTesterWorkspaceState
} from '../state/functional-tester-workspace.state';
import {AutomationRequestService, AutomationRequestStatus, createStore, RestService, Store} from 'sqtm-core';
import {filter, map, switchMap, tap} from 'rxjs/operators';

@Injectable()
export class FunctionalTesterWorkspaceService {
  public componentData$: Observable<FunctionalTesterWorkspaceState>;

  private readonly store: Store<FunctionalTesterWorkspaceState>;

  constructor(private restService: RestService,
              private automationRequestService: AutomationRequestService) {
    this.store = createStore<FunctionalTesterWorkspaceState>(this.getInitialState());
    this.componentData$ = this.store.state$.pipe(
      filter((state) => Boolean(state))
    );
  }

  load(): Observable<any> {
    return this.restService.get<AutomationTesterWorkspaceDataModel>(['automation-tester-workspace', 'data'])
      .pipe(
        map((model: AutomationTesterWorkspaceDataModel) => ({...model, loaded: true})),
        tap((state: FunctionalTesterWorkspaceState) => this.commit(state))
      );
  }

  commit(state: FunctionalTesterWorkspaceState) {
    this.store.commit(state);
  }

  changeStatusToTransmitted(testCaseIds: number[]) {
    return this.automationRequestService.updateAutomationRequestStatus(testCaseIds, AutomationRequestStatus.TRANSMITTED.id).pipe(
      switchMap(() => this.load())
    );
  }

  changeStatusToReady(testCaseIds: number[]) {
    return this.automationRequestService.updateAutomationRequestStatus(testCaseIds, AutomationRequestStatus.READY_TO_TRANSMIT.id).pipe(
      switchMap(() => this.load())
    );
  }

  changeStatusToSuspended(testCaseIds: number[]) {
    return this.automationRequestService.updateAutomationRequestStatus(testCaseIds, AutomationRequestStatus.SUSPENDED.id).pipe(
      switchMap(() => this.load())
    );
  }

  changeStatusToWorkInProgress(testCaseIds: number[]) {
    return this.automationRequestService.updateAutomationRequestStatus(testCaseIds, AutomationRequestStatus.WORK_IN_PROGRESS.id).pipe(
      switchMap(() => this.load())
    );
  }

  private getInitialState(): FunctionalTesterWorkspaceState {
   return {
     nbGlobalAutomReq: 0,
     nbReadyForTransmissionAutomReq: 0,
     nbToBeValidatedAutomReq: 0,
     usersWhoModifiedTestCasesGlobalView: [],
     usersWhoModifiedTestCasesReadyView: [],
     usersWhoModifiedTestCasesValidateView: [],
     loaded: false
   };
  }
}

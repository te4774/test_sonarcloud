import {User} from 'sqtm-core';

export type FunctionalTesterWorkspaceState = AutomationTesterWorkspaceDataModel;

export class AutomationTesterWorkspaceDataModel {
  nbGlobalAutomReq: number;
  nbReadyForTransmissionAutomReq: number;
  nbToBeValidatedAutomReq: number;
  usersWhoModifiedTestCasesGlobalView: User[];
  usersWhoModifiedTestCasesReadyView: User[];
  usersWhoModifiedTestCasesValidateView: User[];
  loaded: boolean;
}

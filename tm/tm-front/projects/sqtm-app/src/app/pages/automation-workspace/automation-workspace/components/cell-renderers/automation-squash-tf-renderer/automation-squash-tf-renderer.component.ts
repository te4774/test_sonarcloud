import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewContainerRef} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, DialogService, GridService,} from 'sqtm-core';
import {SquashTfDialogConfiguration} from '../../dialogs/squash-tf-dialog/squash-tf-dialog.configuration';
import {SquashTfDialogComponent} from '../../dialogs/squash-tf-dialog/squash-tf-dialog.component';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-automation-squash-tf-renderer',
  templateUrl: './automation-squash-tf-renderer.component.html',
  styleUrls: ['./automation-squash-tf-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomationSquashTfRendererComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public gridService: GridService,
              cdr: ChangeDetectorRef,
              private dialogService: DialogService,
              private vcr: ViewContainerRef) {
    super(gridService, cdr);
  }

  ngOnInit(): void {
  }

  isEditable(): boolean {
    return this.row.data['taServerId'] !== null;
  }

  openScriptDialog() {
    const dialog = this.dialogService.openDialog<SquashTfDialogConfiguration, any>({
      id: 'squash-tf-dialog',
      viewContainerReference: this.vcr,
      component: SquashTfDialogComponent,
      width: 600,

      data: {
        tcKind: this.row.data['kind'],
        testCaseId: this.row.data['tclnId'],
        scriptAuto: this.row.data[this.columnDisplay.id],
        uuid: this.row.data['uuid'],
        conflictAssociation: this.row.data['conflictAssociation'],
        canWrite: this.row.simplePermissions.canWrite,
        isManual: this.row.data['isManual']
      }
    });

    dialog.dialogResultChanged$.pipe(
      takeUntil(dialog.dialogClosed$),
    ).subscribe(result => {
      this.row.data['isManual'] = true;
      return this.gridService.editRows([this.row.id], [{columnId: this.columnDisplay.id, value: result}]);
    });
  }
}

export function squashTFColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(AutomationSquashTfRendererComponent);
}

import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FunctionalTesterWorkspaceState} from '../../state/functional-tester-workspace.state';
import {Observable} from 'rxjs';
import {WorkspaceWithGridComponent} from 'sqtm-core';
import {FunctionalTesterWorkspaceService} from '../../services/functional-tester-workspace.service';

@Component({
  selector: 'sqtm-app-functional-tester-workspace-anchors',
  templateUrl: './functional-tester-workspace-anchors.component.html',
  styleUrls: ['./functional-tester-workspace-anchors.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FunctionalTesterWorkspaceAnchorsComponent implements OnInit {

  componentData$: Observable<FunctionalTesterWorkspaceState>;

  constructor(private workspaceWithGrid: WorkspaceWithGridComponent,
              private automationTesterWorkspaceService: FunctionalTesterWorkspaceService) {
    this.componentData$ = this.automationTesterWorkspaceService.componentData$;
  }

  ngOnInit(): void {
  }

  hideContextualContent() {
    this.workspaceWithGrid.switchToNoRowLayout();
  }

}

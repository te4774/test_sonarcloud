import {ResearchColumnPrototype, UserHistorySearchProvider, UserListElement} from 'sqtm-core';
import {Injectable} from '@angular/core';
import {FunctionalTesterWorkspaceService} from '../../services/functional-tester-workspace.service';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {AutomationTesterWorkspaceDataModel} from '../../state/functional-tester-workspace.state';

@Injectable()
export class GlobalFunctionalTesterUserProvider extends UserHistorySearchProvider {

  constructor(private automationTesterWorkspaceService: FunctionalTesterWorkspaceService) {
    super();
  }

  provideUserList(columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.automationTesterWorkspaceService.componentData$.pipe(
      take(1),
      map((data: AutomationTesterWorkspaceDataModel) => data.usersWhoModifiedTestCasesGlobalView
        .map(u => ({...u, selected: false})))
    );
  }
}

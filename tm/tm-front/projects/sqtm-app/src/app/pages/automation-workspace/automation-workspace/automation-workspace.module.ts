import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AutomationWorkspaceComponent} from './containers/automation-workspace/automation-workspace.component';
import {RouterModule, Routes} from '@angular/router';
import {
  AnchorModule,
  CellRendererCommonModule,
  CustomFieldModule,
  DialogModule,
  GridModule,
  MilestoneModule,
  NavBarModule,
  PlatformNavigationModule,
  SvgModule,
  TaTestModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule
} from 'sqtm-core';
import {
  AutomationWorkspaceAssigneeComponent
} from './containers/automation-workspace-assignee/automation-workspace-assignee.component';
import {TranslateModule} from '@ngx-translate/core';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {NzDividerModule} from 'ng-zorro-antd/divider';
import {CKEditorModule} from 'ckeditor4-angular';
import {NzBadgeModule} from 'ng-zorro-antd/badge';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCollapseModule} from 'ng-zorro-antd/collapse';
import {NzSwitchModule} from 'ng-zorro-antd/switch';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzSpinModule} from 'ng-zorro-antd/spin';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzTabsModule} from 'ng-zorro-antd/tabs';
import {
  AutomationWorkspaceAnchorsComponent
} from './components/automation-workspace-anchors/automation-workspace-anchors.component';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {
  AutomationWorkspaceTreatComponent
} from './containers/automation-workspace-treat/automation-workspace-treat.component';
import {AutomationModule} from '../../../components/automation/automation.module';
import {NzPopoverModule} from 'ng-zorro-antd/popover';
import {NzTypographyModule} from 'ng-zorro-antd/typography';
import {NzModalModule} from 'ng-zorro-antd/modal';
import {NzTagModule} from 'ng-zorro-antd/tag';
import {OverlayModule} from '@angular/cdk/overlay';
import {NzRadioModule} from 'ng-zorro-antd/radio';
import {
  ScmAutocompleteCellRendererComponent
} from './components/cell-renderers/scm-autocomplete-cell-renderer/scm-autocomplete-cell-renderer.component';
import {NzAutocompleteModule} from 'ng-zorro-antd/auto-complete';
import {
  AutomationWorkspaceGlobalComponent
} from './containers/automation-workspace-global/automation-workspace-global.component';
import {
  AutomationSquashTfRendererComponent
} from './components/cell-renderers/automation-squash-tf-renderer/automation-squash-tf-renderer.component';
import {SquashTfDialogComponent} from './components/dialogs/squash-tf-dialog/squash-tf-dialog.component';
import {
  ConflictAssociationDialogComponent
} from './components/dialogs/conflict-association-dialog/conflict-association-dialog.component';
import {
  AutomationWorkspaceTestCaseViewComponent
} from './components/automation-workspace-test-case-view/automation-workspace-test-case-view.component';
import {
  TestCaseViewContentComponent
} from '../../test-case-workspace/test-case-view/components/test-case-view-content/test-case-view-content.component';
import {TestStepsComponent} from '../../test-case-workspace/test-case-view/components/test-steps/test-steps.component';
import {IssuesComponent} from '../../test-case-workspace/test-case-view/components/issues/issues.component';
import {ExecutionsComponent} from '../../test-case-workspace/test-case-view/components/executions/executions.component';
import {ScriptComponent} from '../../test-case-workspace/test-case-view/components/script/script.component';
import {
  KeywordTestStepsComponent
} from '../../test-case-workspace/test-case-view/components/keyword-test-steps/keyword-test-steps.component';
import {
  TestCaseViewDetailComponent
} from '../../test-case-workspace/test-case-view/containers/test-case-view-detail/test-case-view-detail.component';

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'assignee',
  },
  {
    path: '',
    component: AutomationWorkspaceComponent,
    children: [
      {
        path: 'assignee', component: AutomationWorkspaceAssigneeComponent, children: [
          {
            path: ':testCaseId', component: AutomationWorkspaceTestCaseViewComponent, children: [
              {
                path: '', component: TestCaseViewDetailComponent, children: [
                  {path: '', redirectTo: 'content'},
                  {path: 'content', component: TestCaseViewContentComponent},
                  {path: 'steps', component: TestStepsComponent},
                  {path: 'issues', component: IssuesComponent},
                  {path: 'executions', component: ExecutionsComponent},
                  {path: 'script', component: ScriptComponent},
                  {path: 'keywordSteps', component: KeywordTestStepsComponent},
                ]
              }

            ]
          }
        ]
      },
      {
        path: 'treat', component: AutomationWorkspaceTreatComponent, children: [
          {
            path: ':testCaseId', component: AutomationWorkspaceTestCaseViewComponent, children: [
              {
                path: '', component: TestCaseViewDetailComponent, children: [
                  {path: '', redirectTo: 'content'},
                  {path: 'content', component: TestCaseViewContentComponent},
                  {path: 'steps', component: TestStepsComponent},
                  {path: 'issues', component: IssuesComponent},
                  {path: 'executions', component: ExecutionsComponent},
                  {path: 'script', component: ScriptComponent},
                  {path: 'keywordSteps', component: KeywordTestStepsComponent},
                ]
              }

            ]
          }
        ]
      },
      {
        path: 'global', component: AutomationWorkspaceGlobalComponent, children: [
          {
            path: ':testCaseId', component: AutomationWorkspaceTestCaseViewComponent, children: [
              {
                path: '', component: TestCaseViewDetailComponent, children: [
                  {path: '', redirectTo: 'content'},
                  {path: 'content', component: TestCaseViewContentComponent},
                  {path: 'steps', component: TestStepsComponent},
                  {path: 'issues', component: IssuesComponent},
                  {path: 'executions', component: ExecutionsComponent},
                  {path: 'script', component: ScriptComponent},
                  {path: 'keywordSteps', component: KeywordTestStepsComponent},
                ]
              }

            ]
          }
        ]
      }
    ]
  }
];

@NgModule({
  declarations: [
    AutomationWorkspaceComponent,
    AutomationWorkspaceAssigneeComponent,
    AutomationWorkspaceAnchorsComponent,
    AutomationWorkspaceTreatComponent,
    ScmAutocompleteCellRendererComponent,
    AutomationWorkspaceGlobalComponent,
    AutomationSquashTfRendererComponent,
    SquashTfDialogComponent,
    ConflictAssociationDialogComponent,
    AutomationWorkspaceTestCaseViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NavBarModule,
    SvgModule,
    WorkspaceCommonModule,
    UiManagerModule,
    WorkspaceLayoutModule,
    GridModule,
    CellRendererCommonModule,
    TranslateModule.forChild(),
    NzDropDownModule,
    ReactiveFormsModule,
    FormsModule,
    CKEditorModule,
    CustomFieldModule,
    DialogModule,
    NzIconModule,
    NzTypographyModule,
    NzInputModule,
    NzPopoverModule,
    NzToolTipModule,
    NzRadioModule,
    AnchorModule,
    NzCollapseModule,
    MilestoneModule,
    OverlayModule,
    NzDividerModule,
    NzBadgeModule,
    NzCheckboxModule,
    NzButtonModule,
    NzSwitchModule,
    NzSpinModule,
    NzSelectModule,
    NzTabsModule,
    NzMenuModule,
    AutomationModule,
    PlatformNavigationModule,
    NzDividerModule,
    NzModalModule,
    NzTagModule,
    NzCheckboxModule,
    NzAutocompleteModule,
    TaTestModule,
  ]
})
export class AutomationWorkspaceModule {
}

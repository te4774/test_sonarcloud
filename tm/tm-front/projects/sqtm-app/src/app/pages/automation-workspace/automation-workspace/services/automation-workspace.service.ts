import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AutomationWorkspaceDataModel, AutomationWorkspaceState} from '../states/automation-workspace.state';

import {AutomationRequestService, createStore, DialogService, RestService, Store} from 'sqtm-core';
import {filter, map, switchMap, tap} from 'rxjs/operators';

@Injectable()
export class AutomationWorkspaceService {

  public componentData$: Observable<AutomationWorkspaceState>;

  private readonly store: Store<AutomationWorkspaceState>;

  constructor(private restService: RestService,
              private automationRequestService: AutomationRequestService,
              private dialogService: DialogService) {

    this.store = createStore<AutomationWorkspaceState>(this.getInitialState());
    this.componentData$ = this.store.state$.pipe(
      filter((state) => Boolean(state))
    );
  }

  load(): Observable<any> {
    return this.restService.get<AutomationWorkspaceDataModel>(['automation-workspace', 'data'])
      .pipe(
        map((model: AutomationWorkspaceDataModel) => ({...model, loaded: true})),
        tap((state: AutomationWorkspaceState) => this.commit(state)));
  }

  commit(state: AutomationWorkspaceState) {
    this.store.commit(state);
  }

  assignAutomationRequest(tcIds: number[]) {
    return this.automationRequestService.assignToAutomationRequest(tcIds)
      .pipe(
        switchMap(() => this.load())
      );
  }

  unAssignAutomationRequest(tcIds: number[]) {
    return this.automationRequestService.unAssignToAutomationRequest(tcIds)
      .pipe(
        switchMap(() => this.load())
      );
  }

  changeStatusToAutomated(testCaseIds: number[]) {
    return this.automationRequestService.updateAutomationRequestStatus(testCaseIds, 'AUTOMATED').pipe(
      tap(data => {
        if (Object.keys(data).length !== 0) {
          this.showNoScriptDialog();
        }
      }),
      switchMap(() => this.load())
    );
  }

  associatedTAScript(testCaseIds: number[]) {
    return this.automationRequestService.resolveTaScript(testCaseIds);
  }

  changeStatusToRejected(testCaseIds: number[]) {
    return this.automationRequestService.updateAutomationRequestStatus(testCaseIds, 'REJECTED').pipe(
      switchMap(() => this.load())
    );
  }

  changeStatusToAutomationInProgress(testCaseIds: number[]) {
    return this.automationRequestService.updateAutomationRequestStatus(testCaseIds, 'AUTOMATION_IN_PROGRESS').pipe(
      switchMap(() => this.load())
    );
  }

  showIllegalStatusDialog() {
    this.dialogService.openAlert({
      messageKey: 'sqtm-core.automation-workspace.dialog.message.tc-illegal-status',
      level: 'WARNING',
      titleKey: 'sqtm-core.generic.label.information.singular'
    });
  }

  showNoScriptDialog() {
    this.dialogService.openAlert({
      titleKey: 'sqtm-core.generic.label.warning',
      messageKey: 'sqtm-core.automation-workspace.dialog.message.test-case-no-script',
      level: 'WARNING'
    });
  }

  private getInitialState(): AutomationWorkspaceState {
    return {
      nbAssignedAutomReq: 0,
      usersWhoModifiedTestCasesAssignView: [],
      nbTotal: 0,
      nbAutomReqToTreat: 0,
      loaded: false,
      usersWhoModifiedTestCasesGlobalView: [],
      usersWhoModifiedTestCasesTreatmentView: [],
      usersAssignedTo: []
    };
  }
}

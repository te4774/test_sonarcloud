import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-app-automation-workspace-test-case-view',
  templateUrl: './automation-workspace-test-case-view.component.html',
  styleUrls: ['./automation-workspace-test-case-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomationWorkspaceTestCaseViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

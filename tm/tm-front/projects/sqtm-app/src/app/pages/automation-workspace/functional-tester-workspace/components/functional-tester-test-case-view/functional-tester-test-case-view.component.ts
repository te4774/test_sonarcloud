import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-app-functional-tester-test-case-view',
  templateUrl: './functional-tester-test-case-view.component.html',
  styleUrls: ['./functional-tester-test-case-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FunctionalTesterTestCaseViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

import {TestBed} from '@angular/core/testing';

import {AutomationWorkspaceService} from './automation-workspace.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {RestService} from 'sqtm-core';

describe('AutomationWorkspaceService', () => {
  let service: AutomationWorkspaceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [{
        provide: AutomationWorkspaceService,
        useClass: AutomationWorkspaceService,
        deps: [RestService]
      }]
    });
    service = TestBed.inject(AutomationWorkspaceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

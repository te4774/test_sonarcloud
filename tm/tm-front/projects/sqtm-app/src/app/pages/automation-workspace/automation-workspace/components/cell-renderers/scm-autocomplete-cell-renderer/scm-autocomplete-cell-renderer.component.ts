import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  ActionErrorDisplayService,
  buildScmRepositoryUrl,
  ColumnDefinitionBuilder,
  GridService,
  ListPanelItem,
  ReferentialDataService,
  RestService,
  ScmServer,
  TableValueChange
} from 'sqtm-core';
import {Observable} from 'rxjs';
import {Overlay} from '@angular/cdk/overlay';
import {TranslateService} from '@ngx-translate/core';
import {catchError, finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-scm-autocomplete-cell-renderer',
  templateUrl: './scm-autocomplete-cell-renderer.component.html',
  styleUrls: ['./scm-autocomplete-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScmAutocompleteCellRendererComponent extends AbstractListCellRendererComponent implements OnInit {

  protected edit = false;

  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  @ViewChild('infoListItemRef', {read: ElementRef})
  infoListItemRef: ElementRef;

  asyncOperationRunning = false;

  scmServers$: Observable<ScmServer[]>;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              public overlay: Overlay,
              public vcr: ViewContainerRef,
              private referentialDataService: ReferentialDataService,
              public translateService: TranslateService,
              public restService: RestService,
              public actionErrorDisplayService: ActionErrorDisplayService) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);
    this.scmServers$ = this.referentialDataService.scmServers$;
  }

  ngOnInit() {

  }

  showTemplate() {
    if (this.isEditable()) {
      this.showList(this.infoListItemRef, this.templatePortalContent);
    }
  }

  getValue(scmServers: ScmServer[], itemId: number) {
    const listPanel = this.getListPanelItems(scmServers);
    return this.getOptionLabel(listPanel.find(item => item.id === itemId));
  }

  getOptionLabel(item: ListPanelItem): string {
    if (item) {
      return item.label;
    } else {
      return this.translateService.instant('sqtm-core.generic.label.none.masculine');
    }
  }

  getListPanelItems(scmServers: ScmServer[]): ListPanelItem[] {
    const listPanelItems: ListPanelItem[] = [];
    listPanelItems.push({id: 0, label: this.translateService.instant('sqtm-core.generic.label.none.masculine')});


    scmServers.forEach(server => {

      const repositories: ListPanelItem[] = server.repositories.map(repo => {
        return {
          id: repo.scmRepositoryId,
          label: buildScmRepositoryUrl(server, repo),
        };
      });
      listPanelItems.push(...repositories);
    });
    return listPanelItems;
  }

  change(repositoryId: number) {
    const testCaseId = this.row.data['tclnId'];
    this.beginAsyncOperation();
    this.restService.post(['test-case', testCaseId, 'scm-repository-id'], {scmRepositoryId: repositoryId}).pipe(
      catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
      finalize(() => this.endAsyncOperation())
    ).subscribe(() => {
      const changes: TableValueChange[] = [
        {columnId: this.columnDisplay.id, value: repositoryId == null ? 0 : repositoryId}
      ];
      this.grid.editRows([this.row.id], changes);
      this.close();
    });
  }

}

export function scmRepositoriesColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ScmAutocompleteCellRendererComponent);
}

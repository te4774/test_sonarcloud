import {CustomDashboardModel, EntityScope, TestCaseStatistics} from 'sqtm-core';

export interface TestCaseMultiViewState {
  statistics: TestCaseStatistics;
  scope: EntityScope[];
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  dashboardLoaded: boolean;
}

export function provideInitialTestCaseMultiView(): Readonly<TestCaseMultiViewState> {
  return {
    statistics: null,
    scope: [],
    dashboard: null,
    canShowFavoriteDashboard: false,
    favoriteDashboardId: null,
    generatedDashboardOn: null,
    shouldShowFavoriteDashboard: false,
    dashboardLoaded: false
  };
}

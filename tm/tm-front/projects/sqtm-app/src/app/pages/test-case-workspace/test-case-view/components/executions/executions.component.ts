import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {
  executionModeColumn,
  executionStatusColumn,
  Extendable,
  Fixed,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  numericColumn,
  ReferentialDataService,
  RestService,
  Sort,
  textColumn
} from 'sqtm-core';
import {TCW_EXECUTION_TABLE, TCW_EXECUTION_TABLE_CONF} from '../../test-case-view.constant';
import {executionOrderColumn} from '../cell-renderers/execution-order/execution-order.component';
import {
  executionLastExecutedOn
} from '../cell-renderers/execution-last-executed-on/execution-last-executed-on.component';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {take} from 'rxjs/operators';
import {Subject} from 'rxjs';

export function tcwExecutionsTableDefinition(): GridDefinition {
  return grid('test-case-view-execution')
    .withColumns([
      textColumn('projectName')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      textColumn('campaignName')
        .withI18nKey('sqtm-core.entity.campaign.label.singular')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      textColumn('iterationName')
        .withI18nKey('sqtm-core.entity.iteration.label.singular')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      textColumn('testSuites')
        .withI18nKey('sqtm-core.entity.test-suite.label.singular')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      executionOrderColumn('executionOrder')
        .withI18nKey('sqtm-core.entity.execution.label.short-dot')
        .changeWidthCalculationStrategy(new Fixed(50))
        .withTitleI18nKey('sqtm-core.entity.execution.label.singular')
        .withHeaderPosition('center'),
      executionModeColumn('executionMode')
        .withI18nKey('sqtm-core.entity.execution.mode.label')
        .changeWidthCalculationStrategy(new Fixed(80)),
      textColumn('datasetLabel').withI18nKey('sqtm-core.entity.dataset.label.short')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .withVisibility(false),
      executionStatusColumn('executionStatus')
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .changeWidthCalculationStrategy(new Fixed(50))
        .withHeaderPosition('center'),
      textColumn('lastExecutedBy')
        .withI18nKey('sqtm-core.entity.execution.last-executed-by.label'),
      executionLastExecutedOn('lastExecutedOn')
        .withI18nKey('sqtm-core.entity.execution.last-executed-on.label'),
      numericColumn('nbIssues')
        .withI18nKey('sqtm-core.entity.issue.label.short')
        .withTitleI18nKey('sqtm-core.entity.issue.label.count')
        .changeWidthCalculationStrategy(new Fixed(50))
        .withHeaderPosition('center')
    ]).server()
    .withInitialSortedColumns([{
      id: 'lastExecutedOn',
      sort: Sort.DESC
    }])
    .disableRightToolBar()
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-executions',
  template: `
    <div class="flex-column full-height full-width p-r-15" style="overflow-y: hidden;">
      <div class="execution-title">
        {{'sqtm-core.test-case-workspace.title.executions' | translate}}
      </div>
      <div class="execution-container full-width full-height p-15">
        <sqtm-core-grid></sqtm-core-grid>
      </div>

    </div>
  `,
  styleUrls: ['./executions.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {provide: TCW_EXECUTION_TABLE_CONF, useFactory: tcwExecutionsTableDefinition},
    {
      provide: TCW_EXECUTION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_EXECUTION_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: TCW_EXECUTION_TABLE
    }
  ]
})
export class ExecutionsComponent implements OnInit, AfterViewInit, OnDestroy {

  private unsub$ = new Subject<void>();

  constructor(private testCaseViewService: TestCaseViewService, private gridService: GridService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.testCaseViewService.componentData$.pipe(
      take(1),
    ).subscribe(componentData => {
      if (componentData.testCase.datasets.ids.length > 0) {
        this.gridService.setColumnVisibility('datasetLabel', true);
      }
      this.gridService.setServerUrl([`test-case/${componentData.testCase.id}/executions`]);
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.gridService.complete();
  }


}

import {Injectable} from '@angular/core';
import {
  AttachmentService,
  CustomDashboardService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  FavoriteDashboardValue,
  PartyPreferencesService,
  ProjectData,
  ReferentialDataService,
  RestService,
  TestCaseFolderModel,
  TestCasePermissions,
  TestCaseStatisticsService
} from 'sqtm-core';
import {TestCaseFolderState} from '../state/test-case-folder.state';
import {provideInitialTestCaseFolderView, TestCaseFolderViewState} from '../state/test-case-folder-view.state';
import {TranslateService} from '@ngx-translate/core';
import {concatMap, map, take, tap, withLatestFrom} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable()
export class TestCaseFolderViewService extends EntityViewService<TestCaseFolderState, 'testCaseFolder', TestCasePermissions> {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              private statisticService: TestCaseStatisticsService,
              private customDashboardService: CustomDashboardService,
              private partyPreferencesService: PartyPreferencesService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(projectData: ProjectData): TestCasePermissions {
    return new TestCasePermissions(projectData);
  }

  getInitialState(): TestCaseFolderViewState {
    return provideInitialTestCaseFolderView();
  }

  load(id: number) {
    this.restService.getWithoutErrorHandling<TestCaseFolderModel>(['test-case-folder-view', id.toString()])
      .subscribe((testCaseFolderModel: TestCaseFolderModel) => {
        const testCaseFolder = this.initializeTestCaseFolderState(testCaseFolderModel);
        this.initializeEntityState(testCaseFolder);
      }, err => this.notifyEntityNotFound(err));
  }

  private initializeTestCaseFolderState(testCaseFolderModel: TestCaseFolderModel): TestCaseFolderState {
    const attachmentEntityState = this.initializeAttachmentState(testCaseFolderModel.attachmentList.attachments);
    const customFieldValueState = this.initializeCustomFieldValueState(testCaseFolderModel.customFieldValues);
    return {
      ...testCaseFolderModel,
      attachmentList: {
        id: testCaseFolderModel.attachmentList.id,
        attachments: attachmentEntityState
      },
      customFieldValues: customFieldValueState,
      generatedDashboardOn: new Date()
    };
  }

  refreshStatistics() {
    this.state$.pipe(
      take(1),
      concatMap(initialState => this.statisticService.fetchStatistics([`TestCaseFolder-${initialState.testCaseFolder.id}`]).pipe(
        withLatestFrom(this.state$),
        map(([statistics, state]) => ({
          ...state,
          testCaseFolder: {
            ...state.testCaseFolder, statistics: {...statistics},
            dashboard: null,
            generatedDashboardOn: new Date(),
            shouldShowFavoriteDashboard: false
          }
        }))
      ))
    ).subscribe(state => this.commit(state));
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue) {
    this.partyPreferencesService.changeTestCaseWorkspaceFavoriteDashboard(preferenceValue).pipe(
      tap(() => {
        if (preferenceValue === 'default') {
          this.refreshStatistics();
        } else {
          this.refreshFolderDashboard();
        }
      })
    ).subscribe();
  }

  refreshFolderDashboard() {
    this.state$.pipe(
      take(1),
      concatMap((initialState: TestCaseFolderViewState) => {
        if (initialState.testCaseFolder.canShowFavoriteDashboard) {
          return this.customDashboardService.getDashboardWithDynamicScope(initialState.testCaseFolder.favoriteDashboardId, {
            milestoneDashboard: false,
            workspaceName: 'TEST_CASE',
            testCaseFolderIds: [initialState.testCaseFolder.id],
            extendedHighLvlReqScope: false
          }).pipe(
            withLatestFrom(this.state$),
            map(([dashboard, state]) => ({
              ...state,
              testCaseFolder: {
                ...state.testCaseFolder, statistics: null,
                dashboard: {...dashboard},
                generatedDashboardOn: new Date(),
                shouldShowFavoriteDashboard: true
              }
            }))
          );
        } else {
          return of({
            ...initialState,
            testCaseFolder: {...initialState.testCaseFolder, shouldShowFavoriteDashboard: true}
          });
        }
      })
    ).subscribe(state => this.commit(state));
  }
}

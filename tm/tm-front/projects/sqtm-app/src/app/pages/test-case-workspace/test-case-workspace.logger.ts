import {Logger} from 'sqtm-core';
import {sqtmAppLogger} from '../../app-logger';

export const testCaseWorkspaceLogger: Logger = sqtmAppLogger.compose('test-case-workspace');

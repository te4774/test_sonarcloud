import {TestBed} from '@angular/core/testing';

import {TestStepViewService} from './test-step-view.service';
import {
  ActionStepModel,
  CallStepModel,
  CopierService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  PasteTestStepOperationReport,
  PersistedAttachment,
  ReferentialDataService,
  StartUpload,
  TestStepService
} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {provideInitialTestCaseView} from '../state/test-case-view.state';
import {
  ActionStepState,
  CallStepPlaceholderState,
  CallStepState,
  CreateActionStepFormState,
  stepsEntityAdapter
} from '../state/test-step.state';
import {TestCaseState} from '../state/test-case.state';
import {callStepDndPlaceholderId, createActionStepFormId} from '../test-case-view.constant';
import {of} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import SpyObj = jasmine.SpyObj;

function getInitialActionStep(): ActionStepState {
  return {
    id: 1,
    projectId: 1,
    extended: false,
    stepOrder: 0,
    action: '',
    expectedResult: '',
    kind: 'action-step',
    attachmentList: {id: 1, attachments: {ids: [], entities: {}}},
    customFieldValues: {ids: [], entities: {}}
  };
}

function getInitialActionStepModel(): ActionStepModel {
  return {
    id: 1,
    projectId: 1,
    stepOrder: 0,
    action: '',
    expectedResult: '',
    kind: 'action-step',
    attachmentList: {id: 1, attachments: []},
    customFieldValues: []
  };
}

function getInitialCallStepModel(): CallStepModel {
  return {
    id: 1,
    projectId: 1,
    stepOrder: 0,
    kind: 'call-step',
    calledTcId: 1,
    calledTcName: 'tc1',
    calledDatasetId: null,
    calledTestCaseSteps: [],
    delegateParam: false,
    calledDatasetName: null,
  };
}

function provideInitialState() {
  const initialState = {...provideInitialTestCaseView()};
  initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
  const actionStep1: ActionStepState = {
    id: 1,
    projectId: 1,
    extended: false,
    stepOrder: 0,
    action: '',
    expectedResult: '',
    kind: 'action-step',
    attachmentList: {id: 1, attachments: {ids: [], entities: {}}},
    customFieldValues: {
      ids: [1, 2], entities: {
        1: {id: 1, cufId: 1, value: 'atlantis', fieldType: 'CF'},
        2: {id: 2, cufId: 2, value: '2019-12-12', fieldType: 'CF'},
      }
    }
  };

  const actionStep2: ActionStepState = {
    id: 2,
    projectId: 1,
    extended: false,
    stepOrder: 1,
    action: '',
    expectedResult: '',
    kind: 'action-step',
    attachmentList: {id: 1, attachments: {ids: [], entities: {}}},
    customFieldValues: {
      ids: [3, 4], entities: {
        3: {id: 3, cufId: 1, value: 'endeavour', fieldType: 'CF'},
        4: {id: 4, cufId: 2, value: '2018-11-30', fieldType: 'CF'},
      }
    }
  };
  initialState.testCase.testSteps = stepsEntityAdapter.setAll([actionStep1, actionStep2], initialState.testCase.testSteps);
  return initialState;
}

describe('TestStepViewService', () => {

  const testStepServiceMock: SpyObj<TestStepService> =
    jasmine.createSpyObj('testStepService', ['changeAction', 'changeExpectedResult', 'moveSteps', 'callTestCases']);
  const attachmentHelperMock = jasmine.createSpyObj('attachmentHelper', ['initializeAttachmentState', 'mapUploadEventToState']);
  const customFieldHelper = jasmine.createSpyObj('customFieldHelper', ['initializeCustomFieldValueState', 'updateCustomFieldValue']);
  const referentialDataService = jasmine.createSpyObj('referentialDataService', ['refresh']);
  const copierServiceMock: SpyObj<CopierService> = jasmine.createSpyObj('CopierService', ['notifyCopySteps']);

  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
    providers: [
      {
        provide: TestStepService,
        useValue: testStepServiceMock
      },
      {
        provide: EntityViewAttachmentHelperService,
        useValue: attachmentHelperMock
      },
      {
        provide: EntityViewCustomFieldHelperService,
        useValue: customFieldHelper
      },
      {
        provide: ReferentialDataService,
        useValue: referentialDataService
      },
      {
        provide: CopierService,
        useValue: copierServiceMock
      },
      {
        provide: TestStepViewService,
        useClass: TestStepViewService,
        deps: [
          TestStepService,
          EntityViewAttachmentHelperService,
          EntityViewCustomFieldHelperService,
          ReferentialDataService,
          CopierService
        ]
      }]
  }));

  it('should be created', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    expect(service).toBeTruthy();
  });

  it('should initialize empty step state', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const testStepsState = service.initializeStepState([], 'STANDARD', {});
    expect(testStepsState.ids).toEqual([createActionStepFormId]);
  });

  it('should initialize step state from step models', () => {
    attachmentHelperMock.initializeAttachmentState.and.returnValue({ids: [], entities: {}});
    customFieldHelper.initializeCustomFieldValueState.and.returnValue({ids: [], entities: {}});
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const testStepModels = [
      {...getInitialActionStepModel(), action: 'action', expectedResult: 'result'},
      {...getInitialActionStepModel(), id: 2, stepOrder: 2},
      {...getInitialCallStepModel(), id: 3, stepOrder: 1},
    ];
    const testStepsState = service.initializeStepState(testStepModels, 'STANDARD', {});
    expect(testStepsState.ids).toEqual([1, 3, 2]);
    const actionStep = testStepsState.entities[1] as ActionStepState;
    expect(actionStep.id).toEqual(1);
    expect(actionStep.action).toEqual('action');
    expect(actionStep.expectedResult).toEqual('result');
    expect(actionStep.extended).toEqual(true);
    expect(actionStep.attachmentList.id).toEqual(1);
    expect(testStepsState.entities[2].id).toEqual(2);
    const callStep = testStepsState.entities[3] as CallStepState;
    expect(callStep.id).toEqual(3);
    expect(callStep.calledTcId).toEqual(1);
    expect(callStep.extended).toEqual(false);
  });

  it('should update action in state', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), action: 'initial'};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 1};
    initialState.testCase.testSteps = stepsEntityAdapter.setAll([actionStep1, actionStep2], initialState.testCase.testSteps);
    const nextState = service.updateAction(1, 'newAction', initialState);
    expect(nextState).not.toBe(initialState);
    const step = nextState.testCase.testSteps.entities[1] as ActionStepState;
    const initialStep = initialState.testCase.testSteps.entities[1] as ActionStepState;
    expect(step).not.toBe(initialStep);
    expect(step.action).toEqual('newAction');
    expect(nextState.testCase.testSteps.entities[2]).toBe(initialState.testCase.testSteps.entities[2]);
  });

  it('should update expected result in state', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), expectedResult: 'initial'};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 1};
    initialState.testCase.testSteps = stepsEntityAdapter.setAll([actionStep1, actionStep2], initialState.testCase.testSteps);
    const nextState = service.updateExpectedResult(1, 'newResult', initialState);
    expect(nextState).not.toBe(initialState);
    const step = nextState.testCase.testSteps.entities[1] as ActionStepState;
    const initialStep = initialState.testCase.testSteps.entities[1] as ActionStepState;
    expect(step).not.toBe(initialStep);
    expect(step.expectedResult).toEqual('newResult');
    expect(nextState.testCase.testSteps.entities[2]).toBe(initialState.testCase.testSteps.entities[2]);
  });

  it('it should update custom field value in step', () => {
    customFieldHelper.updateCustomFieldValue.and.returnValue({
      ids: [3, 4], entities: {
        3: {id: 3, cufId: 1, value: 'enterprise', fieldType: 'CF'},
        4: {id: 4, cufId: 2, value: '2018-11-30', fieldType: 'CF'},
      }
    });
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = provideInitialState();
    const nextState = service.updateStepCustomFieldValue(2, 3, 'enterprise', initialState);
    expect(nextState).not.toBe(initialState);
    const step = nextState.testCase.testSteps.entities[2] as ActionStepState;
    const initialStep = initialState.testCase.testSteps.entities[2] as ActionStepState;
    expect(step).not.toBe(initialStep);
    expect(step.customFieldValues.entities[3].value).toEqual('enterprise');
    expect(nextState.testCase.testSteps.entities[1]).toBe(initialState.testCase.testSteps.entities[1]);
  });

  it('it should mark step attachment to delete', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {
      ...getInitialActionStep(),
      attachmentList: {
        id: 1,
        attachments: {
          ids: ['1'],
          entities: {'1': {id: '1', name: 'saturnV', kind: 'persisted-attachment', size: 1000, addedOn: new Date()}}
        }
      }
    };
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 1};
    initialState.testCase.testSteps = stepsEntityAdapter.setAll([actionStep1, actionStep2], initialState.testCase.testSteps);
    const nextState = service.markStepAttachmentsToDelete(['1'], 1, initialState);
    const entity = nextState.testCase.testSteps.entities[1] as ActionStepState;
    const attachment = entity.attachmentList.attachments.entities['1'] as PersistedAttachment;
    expect(attachment.pendingDelete).toBeTruthy();
  });

  it('it should cancel delete', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {
      ...getInitialActionStep(),
      attachmentList: {
        id: 1,
        attachments: {
          ids: ['1'],
          entities: {
            '1': {
              id: '1',
              name: 'saturnV',
              kind: 'persisted-attachment',
              size: 1000,
              addedOn: new Date(),
              pendingDelete: true
            }
          }
        }
      }
    };
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 1};
    initialState.testCase.testSteps = stepsEntityAdapter.setAll([actionStep1, actionStep2], initialState.testCase.testSteps);
    const nextState = service.cancelStepAttachmentsToDelete(['1'], 1, initialState);
    const entity = nextState.testCase.testSteps.entities[1] as ActionStepState;
    const attachment = entity.attachmentList.attachments.entities['1'] as PersistedAttachment;
    expect(attachment.pendingDelete).toBeFalsy();
  });

  it('it should remove rejected attachment', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {
      ...getInitialActionStep(),
      attachmentList: {
        id: 1,
        attachments: {
          ids: ['1', 'rejected'],
          entities: {
            '1': {
              id: '1',
              name: 'saturnV',
              kind: 'persisted-attachment',
              size: 1000,
              addedOn: new Date(),
              pendingDelete: true
            },
            'rejected': {
              id: 'rejected',
              name: 'bourrane',
              kind: 'rejected-attachment',
              size: 1000,
              addedOn: new Date(),
              errors: []
            }
          }
        }
      }
    };
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 1};
    initialState.testCase.testSteps = stepsEntityAdapter.setAll([actionStep1, actionStep2], initialState.testCase.testSteps);
    const nextState = service.removeStepRejectedAttachments(['rejected'], 1, initialState);
    const entity = nextState.testCase.testSteps.entities[1] as ActionStepState;
    const ids = entity.attachmentList.attachments.ids;
    expect(ids).toEqual(['1']);
  });

  it('it should remove attachment', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {
      ...getInitialActionStep(),
      attachmentList: {
        id: 1,
        attachments: {
          ids: ['1', '2'],
          entities: {
            '1': {
              id: '1',
              name: 'saturnV',
              kind: 'persisted-attachment',
              size: 1000,
              addedOn: new Date(),
              pendingDelete: true
            },
            '2': {
              id: '2',
              name: 'ariane5',
              kind: 'persisted-attachment',
              size: 1000,
              addedOn: new Date(),
              pendingDelete: false
            }
          }
        }
      }
    };
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 1};
    initialState.testCase.testSteps = stepsEntityAdapter.setAll([actionStep1, actionStep2], initialState.testCase.testSteps);
    const nextState = service.removeAttachments(['1'], 1, initialState);
    const entity = nextState.testCase.testSteps.entities[1] as ActionStepState;
    const ids = entity.attachmentList.attachments.ids;
    expect(ids).toEqual(['2']);
  });

  it('it should map start upload event to correct step', () => {
    attachmentHelperMock.mapUploadEventToState.and.returnValue(
      {
        ids: ['1', '2', 'sts'],
        entities: {
          '1': {
            id: '1',
            name: 'saturnV',
            kind: 'persisted-attachment',
            size: 1000,
            addedOn: new Date(),
            pendingDelete: true
          },
          '2': {
            id: '2',
            name: 'ariane5',
            kind: 'persisted-attachment',
            size: 1000,
            addedOn: new Date(),
            pendingDelete: false
          },
          'sts': {
            id: 'sts',
            name: 'atlantis',
            kind: 'uploading-attachment',
            size: 1000,
            addedOn: new Date(),
            uploadProgress: 0
          }
        }
      }
    );
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {
      ...getInitialActionStep(),
      attachmentList: {
        id: 1,
        attachments: {
          ids: ['1', '2'],
          entities: {
            '1': {
              id: '1',
              name: 'saturnV',
              kind: 'persisted-attachment',
              size: 1000,
              addedOn: new Date(),
              pendingDelete: true
            },
            '2': {
              id: '2',
              name: 'ariane5',
              kind: 'persisted-attachment',
              size: 1000,
              addedOn: new Date(),
              pendingDelete: false
            }
          }
        }
      }
    };
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 1};
    initialState.testCase.testSteps = stepsEntityAdapter.setAll([actionStep1, actionStep2], initialState.testCase.testSteps);
    const startEvent = new StartUpload([{
      id: 'sts',
      name: 'atlantis',
      kind: 'uploading-attachment',
      size: 1000,
      addedOn: new Date(),
      uploadProgress: 0
    }]);
    const nextState = service.mapUploadEventToState(1, initialState, startEvent);
    const entity = nextState.testCase.testSteps.entities[1] as ActionStepState;
    const ids = entity.attachmentList.attachments.ids;
    const uploading = entity.attachmentList.attachments.entities['sts'];
    expect(ids).toEqual(['1', '2', 'sts']);
    expect(uploading.name).toEqual('atlantis');
  });

  it('it should insert create action step form', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {
      ...getInitialActionStep(),
    };
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 1};
    initialState.testCase.testSteps = stepsEntityAdapter.setAll([actionStep1, actionStep2], initialState.testCase.testSteps);
    const nextState = service.addActionStepForm(1, initialState);
    const ids = nextState.testCase.testSteps.ids;
    expect(ids).toEqual([1, createActionStepFormId, 2]);
  });

  it('it should cancel create action step', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {
      ...getInitialActionStep(),
    };
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const createActionStepForm: CreateActionStepFormState = {
      kind: 'create-action-step-form',
      id: createActionStepFormId,
      stepOrder: 1,
      projectId: 1,
      extended: true
    };
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll([actionStep1, createActionStepForm, actionStep2], initialState.testCase.testSteps);
    const nextState = service.cancelAddTestStep(initialState);
    const ids = nextState.testCase.testSteps.ids;
    expect(ids).toEqual([1, 2]);
  });

  it('it should add a new action step', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {
      ...getInitialActionStep(),
    };
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const createActionStepForm: CreateActionStepFormState = {
      kind: 'create-action-step-form',
      id: createActionStepFormId,
      stepOrder: 1,
      projectId: 1,
      extended: true
    };
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll([actionStep1, createActionStepForm, actionStep2], initialState.testCase.testSteps);
    const nextState = service.addNewActionStep({...getInitialActionStep(), id: 3, stepOrder: 1}, 1, initialState);
    const ids = nextState.testCase.testSteps.ids;
    expect(ids).toEqual([1, 3, 2]);
  });

  it('it should remove step', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {
      ...getInitialActionStep(),
    };
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2], initialState.testCase.testSteps);
    const nextState = service.removeSteps([3], initialState);
    const ids = nextState.testCase.testSteps.ids;
    expect(ids).toEqual([1, 2]);
    // checking that the stepOrder structure is correct (aka no hole)
    const steps = Object.values(nextState.testCase.testSteps.entities);
    const stepOrders = steps.map(step => step.stepOrder).sort();
    expect(stepOrders).toEqual([0, 1]);
  });

  it('it should show step creation form after deleting last step', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {
      ...getInitialActionStep(),
    };
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2], initialState.testCase.testSteps);
    const nextState = service.removeSteps([1, 2, 3], initialState);
    const ids = nextState.testCase.testSteps.ids;
    expect(ids).toEqual([createActionStepFormId]);
  });

  describe('Should copy steps', () => {
    interface DataType {
      selectedSteps: number[];
      expectedCopiedSteps: number[];
    }

    const dataSets: DataType[] = [
      {
        selectedSteps: [],
        expectedCopiedSteps: [3]
      },
      {
        selectedSteps: [1, 2],
        expectedCopiedSteps: [1, 2]
      },
    ];
    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - it should copy step with selection ${data.selectedSteps}`, () => {
        const spy = copierServiceMock.notifyCopySteps.and.callThrough();
        const service: TestStepViewService = TestBed.inject(TestStepViewService);
        const initialState = {...provideInitialTestCaseView()};
        initialState.testCase = {
          testSteps: {
            ids: [],
            entities: {},
            selectedStepIds: data.selectedSteps
          },
          kind: 'KEYWORD'
        } as TestCaseState;
        const actionStep1: ActionStepState = {
          ...getInitialActionStep(),
        };
        const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
        const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
        initialState.testCase.testSteps =
          stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2], initialState.testCase.testSteps);
        service.copySteps(3, initialState);
        expect(spy).toHaveBeenCalledWith(data.expectedCopiedSteps, initialState.testCase.kind);
      });
    }
  });

  it('it should add pasted steps', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {
      ...getInitialActionStep(),
    };
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2], initialState.testCase.testSteps);
    const copiedStep4: ActionStepModel = {
      id: 4,
      kind: 'action-step',
      projectId: 1,
      stepOrder: 2,
      action: 'action',
      expectedResult: 'expected',
      attachmentList: {id: 1, attachments: []},
      customFieldValues: []
    };
    const copiedStep5: ActionStepModel = {
      id: 5,
      kind: 'action-step',
      projectId: 1,
      stepOrder: 3,
      action: 'action',
      expectedResult: 'expected',
      attachmentList: {id: 1, attachments: []},
      customFieldValues: []
    };
    const pasteOperationReport: PasteTestStepOperationReport = {
      testSteps: [copiedStep4, copiedStep5],
      testCaseImportance: 'VERY_HIGH',
      operationReport: {
        dataSets: [],
        parameters: [],
        paramValues: []
      }
    };
    const nextState = service.addPastedStep(3, pasteOperationReport, initialState, {});
    const ids = nextState.testCase.testSteps.ids;
    expect(ids).toEqual([1, 3, 4, 5, 2]);
    // checking that the stepOrder structure is correct (aka no hole)
    const steps = Object.values(nextState.testCase.testSteps.entities);
    const stepOrders = steps.map(step => step.stepOrder).sort();
    expect(stepOrders).toEqual([0, 1, 2, 3, 4]);
    expect(nextState.testCase.importance).toEqual('VERY_HIGH');
  });

  it('it should select one step', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}, selectedStepIds: [1]}} as TestCaseState;
    const actionStep1: ActionStepState = {
      ...getInitialActionStep(),
    };
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2], initialState.testCase.testSteps);
    const nextState = service.selectStep(3, initialState);
    expect(nextState.testCase.testSteps.selectedStepIds).toEqual([3]);
  });

  describe('Toggle step selection', () => {
    interface DataType {
      toggledStep: number;
      previouslySelectedSteps: number[];
      expectedSelectedSteps: number[];
    }

    const dataSets: DataType[] = [
      {
        toggledStep: 3,
        previouslySelectedSteps: [],
        expectedSelectedSteps: [3]
      },
      {
        toggledStep: 3,
        previouslySelectedSteps: [1, 3, 2],
        expectedSelectedSteps: [1, 2]
      },
      {
        toggledStep: 3,
        previouslySelectedSteps: [1],
        expectedSelectedSteps: [1, 3]
      },
      // should not push at end but respect the step order as server is coded like that for copy/paste operations
      {
        toggledStep: 3,
        previouslySelectedSteps: [1, 2],
        expectedSelectedSteps: [1, 3, 2]
      },
    ];
    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - it should toggle step ${data.toggledStep} with selection ${JSON.stringify(data.previouslySelectedSteps)}`,
        () => {
          const service: TestStepViewService = TestBed.inject(TestStepViewService);
          const initialState = {...provideInitialTestCaseView()};
          initialState.testCase = {
            testSteps: {
              ids: [],
              entities: {},
              selectedStepIds: data.previouslySelectedSteps
            }
          } as TestCaseState;
          const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
          const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
          const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
          const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
          const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
          initialState.testCase.testSteps =
            stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2, actionStep4, actionStep5], initialState.testCase.testSteps);
          const nextState = service.toggleStepSelection(data.toggledStep, initialState);
          expect(nextState.testCase.testSteps.selectedStepIds).toEqual(data.expectedSelectedSteps);
        });
    }
  });

  describe('Extend step selection', () => {
    interface DataType {
      expendToStep: number;
      previouslySelectedSteps: number[];
      expectedSelectedSteps: number[];
    }

    const dataSets: DataType[] = [
      {
        expendToStep: 3,
        previouslySelectedSteps: [],
        expectedSelectedSteps: [3]
      },
      {
        expendToStep: 4,
        previouslySelectedSteps: [1],
        expectedSelectedSteps: [1, 3, 2, 4]
      },
      {
        expendToStep: 5,
        previouslySelectedSteps: [1],
        expectedSelectedSteps: [1, 3, 2, 4, 5]
      },
      {
        expendToStep: 1,
        previouslySelectedSteps: [4],
        expectedSelectedSteps: [1, 3, 2, 4]
      },
      // NOOP if already selected
      {
        expendToStep: 5,
        previouslySelectedSteps: [3, 5],
        expectedSelectedSteps: [3, 5]
      },
      // if you select inside a range it take all the range. Would be harder and probably confusing to make guess of witch side to extends
      {
        expendToStep: 2,
        previouslySelectedSteps: [3, 5],
        expectedSelectedSteps: [3, 2, 4, 5]
      },
    ];
    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - it should expend to step ${data.expendToStep} with selection ${JSON.stringify(data.previouslySelectedSteps)}`,
        () => {
          const service: TestStepViewService = TestBed.inject(TestStepViewService);
          const initialState = {...provideInitialTestCaseView()};
          initialState.testCase = {
            testSteps: {
              ids: [],
              entities: {},
              selectedStepIds: data.previouslySelectedSteps
            }
          } as TestCaseState;
          const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
          const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
          const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
          const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
          const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
          initialState.testCase.testSteps =
            stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2, actionStep4, actionStep5], initialState.testCase.testSteps);
          const nextState = service.extendStepSelection(data.expendToStep, initialState);
          expect(nextState.testCase.testSteps.selectedStepIds).toEqual(data.expectedSelectedSteps);
        });
    }
  });

  describe('Start Dragging Steps', () => {
    interface DataType {
      draggedSteps: number[];
      expectedIds: number[];
    }

    const dataSets: DataType[] = [
      {
        draggedSteps: [3],
        // Dragging only one step will not change order, just show the dragged step as placeholder.
        expectedIds: [1, 3, 2, 4, 5]
      },
      {
        // testing first step
        draggedSteps: [1],
        expectedIds: [1, 3, 2, 4, 5]
      },
      {
        // testing last step
        draggedSteps: [5],
        expectedIds: [1, 3, 2, 4, 5]
      },
      {
        draggedSteps: [3, 5],
        expectedIds: [1, 3, 5, 2, 4]
      },
      {
        // all steps
        draggedSteps: [1, 3, 2, 4, 5],
        expectedIds: [1, 3, 2, 4, 5]
      },
    ];
    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - it should start dragging steps ${data.draggedSteps}`,
        () => {
          const service: TestStepViewService = TestBed.inject(TestStepViewService);
          const initialState = {...provideInitialTestCaseView()};
          initialState.testCase = {testSteps: {ids: [], entities: {}, selectedStepIds: []}} as TestCaseState;
          const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
          const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
          const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
          const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
          const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
          initialState.testCase.testSteps =
            stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2, actionStep4, actionStep5], initialState.testCase.testSteps);
          const nextState = service.startDraggingStep(data.draggedSteps, initialState);
          const ids = nextState.testCase.testSteps.ids;
          expect(ids).toEqual(data.expectedIds);
          expect(nextState.testCase.testSteps.showPlaceHolder).toBeTruthy();
          expect(nextState.testCase.testSteps.draggingSteps).toBeTruthy();
          expect(nextState.testCase.testSteps.draggingTestCase).toBeFalsy();
          expect(nextState.testCase.testSteps.selectedStepIds).toEqual(data.draggedSteps);
        });
    }
  });

  describe('Drag Over Steps', () => {
    interface DataType {
      draggedSteps: number[];
      targetStepId: number;
      expectedIds: number[];
    }

    // initial order : [1, 3, 2, 4, 5] :)
    const dataSets: DataType[] = [
      {
        // go first position
        draggedSteps: [3],
        targetStepId: 1,
        expectedIds: [3, 1, 2, 4, 5]
      },
      {
        // go over last step -> inserted before last step
        draggedSteps: [3],
        targetStepId: 5,
        expectedIds: [1, 2, 4, 3, 5]
      },
      {
        // go to the end of list
        draggedSteps: [3],
        targetStepId: null,
        expectedIds: [1, 2, 4, 5, 3]
      },
      {
        // several steps
        draggedSteps: [3, 5],
        targetStepId: 2,
        expectedIds: [1, 3, 5, 2, 4]
      },
    ];
    dataSets.forEach((data, index) => runTest(data, index));

    function runTest(data: DataType, index: number) {
      it(`Dataset ${index} - it should drag steps ${data.draggedSteps} over ${data.targetStepId}`,
        () => {
          const service: TestStepViewService = TestBed.inject(TestStepViewService);
          const initialState = {...provideInitialTestCaseView()};
          initialState.testCase = {
            testSteps: {
              ids: [],
              entities: {},
              selectedStepIds: data.draggedSteps
            }
          } as TestCaseState;
          const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
          const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
          const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
          const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
          const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
          initialState.testCase.testSteps =
            stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2, actionStep4, actionStep5], initialState.testCase.testSteps);
          const nextState = service.dragOverStep(data.targetStepId, initialState);
          const ids = nextState.testCase.testSteps.ids;
          expect(ids).toEqual(data.expectedIds);
          expect(nextState.testCase.testSteps.showPlaceHolder).toBeTruthy();
          expect(nextState.testCase.testSteps.selectedStepIds).toEqual(data.draggedSteps);
        });
    }
  });

  it('it should write new step position on server', () => {
    const spy = testStepServiceMock.moveSteps.and.returnValue(of(true));
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {
      id: 1,
      testSteps: {ids: [], entities: {}, selectedStepIds: [1], currentDndTargetId: 2}
    } as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
    const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2, actionStep4, actionStep5], initialState.testCase.testSteps);
    service.writeNewStepPosition(initialState).subscribe();
    expect(spy).toHaveBeenCalledWith(initialState.testCase.testSteps.selectedStepIds, 1, initialState.testCase.id);
  });

  it('it should drop steps', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {
      id: 1,
      testSteps: {ids: [], entities: {}, selectedStepIds: [1], currentDndTargetId: 3}
    } as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
    const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2, actionStep4, actionStep5], initialState.testCase.testSteps);
    const nextState = service.dropStep(initialState);
    expect(nextState.testCase.testSteps.currentDndTargetId).toBeFalsy();
    expect(nextState.testCase.testSteps.draggingSteps).toBeFalsy();
    expect(nextState.testCase.testSteps.showPlaceHolder).toBeFalsy();
    expect(nextState.testCase.testSteps.selectedStepIds).toEqual([]);
  });

  it('it should write new call step on server', () => {
    const spy = testStepServiceMock.callTestCases.and.returnValue(of(true));
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {
      id: 1,
      testSteps: {ids: [], entities: {}, selectedStepIds: [1], currentDndTargetId: 2}
    } as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
    const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2, actionStep4, actionStep5], initialState.testCase.testSteps);
    service.writeCallTestCase([12], initialState).subscribe();
    expect(spy).toHaveBeenCalledWith([12], 2, 1);
  });

  it('it should drop test case and update state', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {
      id: 1,
      testSteps: {ids: [], entities: {}, selectedStepIds: [1], currentDndTargetId: 2}
    } as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
    const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll([actionStep1, actionStep3, actionStep2, actionStep4, actionStep5], initialState.testCase.testSteps);
    const callStep: CallStepModel = {
      id: 6,
      stepOrder: 2,
      projectId: 1,
      kind: 'call-step',
      calledTcId: 154,
      calledTcName: 'tc1',
      delegateParam: false,
      calledTestCaseSteps: [],
      calledDatasetId: 1,
      calledDatasetName: '',
    };
    const operationReport: PasteTestStepOperationReport = {
      testSteps: [callStep],
      testCaseImportance: 'VERY_HIGH',
      operationReport: {
        dataSets: [],
        parameters: [],
        paramValues: []
      }
    };
    const nextState = service.callTestCase(operationReport, initialState, {});
    const ids = nextState.testCase.testSteps.ids;
    expect(ids).toEqual([1, 3, 6, 2, 4, 5]);
  });

  it('it should cancel drag and drop', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {
      id: 1,
      testSteps: {ids: [], entities: {}, selectedStepIds: [1], currentDndTargetId: 2, initialStepOrder: [1, 3, 2, 4, 5]}
    } as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
    const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
    const placeholder: CallStepPlaceholderState = {
      id: callStepDndPlaceholderId,
      stepOrder: 5,
      extended: true,
      projectId: 1,
      kind: 'dnd-call-step-placeholder'
    };
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll(
        [actionStep1, actionStep3, actionStep2, actionStep4, actionStep5, placeholder],
        initialState.testCase.testSteps
      );
    const nextState = service.cancelDrag(initialState);
    const testSteps = nextState.testCase.testSteps;
    const ids = testSteps.ids;
    expect(ids).toEqual([1, 3, 2, 4, 5]);
    expect(testSteps.draggingSteps).toBeFalsy();
    expect(testSteps.draggingTestCase).toBeFalsy();
    expect(testSteps.showPlaceHolder).toBeFalsy();
    expect(testSteps.initialStepOrder).toEqual([]);
    expect(testSteps.currentDndTargetId).toBeFalsy();
  });

  it('it should drag over steps for calls', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {id: 1, testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
    const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll(
        [actionStep1, actionStep3, actionStep2, actionStep4, actionStep5],
        initialState.testCase.testSteps
      );
    const nextState = service.dragOverStepForCall(initialState, 2);
    const testSteps = nextState.testCase.testSteps;
    const ids = testSteps.ids;
    expect(ids).toEqual([1, 3, callStepDndPlaceholderId, 2, 4, 5]);
    expect(testSteps.draggingSteps).toBeFalsy();
    expect(testSteps.currentDndTargetId).toEqual(2);

  });

  it('it should drag over container for calls', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {id: 1, testSteps: {ids: [], entities: {}}} as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
    const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll(
        [actionStep1, actionStep3, actionStep2, actionStep4, actionStep5],
        initialState.testCase.testSteps
      );
    const nextState = service.dragOverStepForCall(initialState);
    const testSteps = nextState.testCase.testSteps;
    const ids = testSteps.ids;
    expect(ids).toEqual([1, 3, 2, 4, 5, callStepDndPlaceholderId]);
    expect(testSteps.draggingSteps).toBeFalsy();
    expect(testSteps.currentDndTargetId).toBeFalsy();

  });

  it('it should suspend drag and drop', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    const initialStepOrder = [1, 3, 2, 4, 5];
    initialState.testCase = {
      id: 1,
      testSteps: {ids: [], entities: {}, selectedStepIds: [1], currentDndTargetId: 2, initialStepOrder}
    } as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
    const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
    const placeholder: CallStepPlaceholderState = {
      id: callStepDndPlaceholderId,
      stepOrder: 5,
      extended: true,
      projectId: 1,
      kind: 'dnd-call-step-placeholder'
    };
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll(
        [actionStep1, actionStep3, actionStep2, actionStep4, actionStep5, placeholder],
        initialState.testCase.testSteps
      );
    const nextState = service.suspendTestCaseDrag(initialState);
    const testSteps = nextState.testCase.testSteps;
    const ids = testSteps.ids;
    expect(ids).toEqual(initialStepOrder);
    expect(testSteps.draggingSteps).toBeFalsy();
    expect(testSteps.draggingTestCase).toBeTruthy();
    expect(testSteps.showPlaceHolder).toBeFalsy();
    expect(testSteps.initialStepOrder).toEqual(initialStepOrder);
    expect(testSteps.currentDndTargetId).toBeFalsy();
  });

  it('it should collapse all steps', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    const initialStepOrder = [1, 3, 2, 4, 5];
    initialState.testCase = {
      id: 1,
      testSteps: {ids: [], entities: {}}
    } as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
    const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll(
        [actionStep1, actionStep3, actionStep2, actionStep4, actionStep5],
        initialState.testCase.testSteps
      );
    const nextState = service.collapseAllSteps(initialState);
    const testSteps = nextState.testCase.testSteps;
    const steps = Object.values(testSteps.entities);
    steps.forEach(step => expect(step.extended).toBeFalsy());
    expect(nextState.testCase.testSteps.extendedPrerequisite).toBeFalsy();
  });

  it('it should expend all steps', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    const initialStepOrder = [1, 3, 2, 4, 5];
    initialState.testCase = {
      id: 1,
      testSteps: {ids: [], entities: {}}
    } as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
    const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll(
        [actionStep1, actionStep3, actionStep2, actionStep4, actionStep5],
        initialState.testCase.testSteps
      );
    const nextState = service.expendAllSteps(initialState);
    const testSteps = nextState.testCase.testSteps;
    const steps = Object.values(testSteps.entities);
    steps.forEach(step => expect(step.extended).toBeTruthy());
    expect(nextState.testCase.testSteps.extendedPrerequisite).toBeTruthy();
  });

  it('it should toggle one step', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    const initialStepOrder = [1, 3, 2, 4, 5];
    initialState.testCase = {
      id: 1,
      testSteps: {ids: [], entities: {}}
    } as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
    const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll(
        [actionStep1, actionStep3, actionStep2, actionStep4, actionStep5],
        initialState.testCase.testSteps
      );
    const nextState = service.toggleStep(1, initialState);
    const testSteps = nextState.testCase.testSteps;
    expect(testSteps.entities[1].extended).toBeTruthy();
  });

  it('it should move one step', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    const initialStepOrder = [1, 3, 2, 4, 5];
    initialState.testCase = {
      id: 1,
      testSteps: {ids: [], entities: {}}
    } as TestCaseState;
    const actionStep1: ActionStepState = {...getInitialActionStep(), id: 1, stepOrder: 0};
    const actionStep3: ActionStepState = {...getInitialActionStep(), id: 3, stepOrder: 1};
    const actionStep2: ActionStepState = {...getInitialActionStep(), id: 2, stepOrder: 2};
    const actionStep4: ActionStepState = {...getInitialActionStep(), id: 4, stepOrder: 3};
    const actionStep5: ActionStepState = {...getInitialActionStep(), id: 5, stepOrder: 4};
    initialState.testCase.testSteps =
      stepsEntityAdapter.setAll(
        [actionStep1, actionStep3, actionStep2, actionStep4, actionStep5],
        initialState.testCase.testSteps
      );
    const nextState = service.moveStep(1, 1, initialState);
    const testSteps = nextState.testCase.testSteps;
    expect(testSteps.ids).toEqual([3, 1, 2, 4, 5]);
    expect(testSteps.entities[3].stepOrder).toEqual(0);
    expect(testSteps.entities[1].stepOrder).toEqual(1);
    expect(testSteps.entities[2].stepOrder).toEqual(2);
    expect(testSteps.entities[4].stepOrder).toEqual(3);
    expect(testSteps.entities[5].stepOrder).toEqual(4);
  });

  it('it should toggle prerequisite in step view', () => {
    const service: TestStepViewService = TestBed.inject(TestStepViewService);
    const initialState = {...provideInitialTestCaseView()};
    initialState.testCase = {testSteps: {ids: [], entities: {}, extendedPrerequisite: true}} as TestCaseState;
    const nextState = service.togglePrerequisite(initialState);
    expect(nextState.testCase.testSteps.extendedPrerequisite).toBeFalsy();
  });

});

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit,} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-parameter-source-test-case',
  template: `
    <ng-container *ngIf="columnDisplay && row && row.data['isDelegateParam']">
      <div class="full-width full-height flex-column">
        <a [routerLink]="['/test-case-workspace/test-case/', row.data['sourceTestCaseId']]">{{getSourceTestCaseName()}}</a>
      </div>
    </ng-container>
  `,
  styleUrls: ['./parameter-source-test-case.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParameterSourceTestCaseComponent extends AbstractCellRendererComponent implements OnInit, OnDestroy {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {

  }

  getSourceTestCaseName() {
    let sourceTestCaseName = `${this.row.data['sourceTestCaseName']} (${this.row.data['sourceTestCaseProjectName']})`;
    if (this.row.data['sourceTestCaseReference']) {
      sourceTestCaseName = `${this.row.data['sourceTestCaseReference']}-${sourceTestCaseName}`;
    }
    return sourceTestCaseName;
  }

}

export function parameterSourceTestCaseColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ParameterSourceTestCaseComponent);
}

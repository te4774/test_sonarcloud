import {Injectable} from '@angular/core';
import {
  AttachmentService,
  CustomDashboardService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  FavoriteDashboardValue,
  PartyPreferencesService,
  ProjectData,
  ReferentialDataService,
  RestService,
  TestCaseLibraryModel,
  TestCasePermissions,
  TestCaseStatisticsService
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {provideInitialTestCaseLibraryView, TestCaseLibraryViewState} from '../state/test-case-library-view.state';
import {TestCaseLibraryState} from '../state/test-case-library.state';
import {concatMap, map, take, tap, withLatestFrom} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable()
export class TestCaseLibraryViewService extends EntityViewService<TestCaseLibraryState, 'testCaseLibrary', TestCasePermissions> {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              private statisticService: TestCaseStatisticsService,
              private customDashboardService: CustomDashboardService,
              private partyPreferencesService: PartyPreferencesService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(projectData: ProjectData): TestCasePermissions {
    return new TestCasePermissions(projectData);
  }

  getInitialState(): TestCaseLibraryViewState {
    return provideInitialTestCaseLibraryView();
  }

  load(id: number) {
    this.restService.getWithoutErrorHandling<TestCaseLibraryModel>(['test-case-library-view', id.toString()])
      .subscribe((testCaseLibraryModel: TestCaseLibraryModel) => {
        const testCaseLibrary = this.initializeTestCaseLibraryState(testCaseLibraryModel);
        this.initializeEntityState(testCaseLibrary);
      }, err => this.notifyEntityNotFound(err));
  }

  private initializeTestCaseLibraryState(testCaseLibraryModel: TestCaseLibraryModel): TestCaseLibraryState {
    const attachmentEntityState = this.initializeAttachmentState(testCaseLibraryModel.attachmentList.attachments);
    const customFieldValueState = this.initializeCustomFieldValueState(testCaseLibraryModel.customFieldValues);
    return {
      ...testCaseLibraryModel,
      attachmentList: {id: testCaseLibraryModel.attachmentList.id, attachments: attachmentEntityState},
      customFieldValues: customFieldValueState,
      generatedDashboardOn: new Date()
    };
  }

  refreshStatistics() {
    this.state$.pipe(
      take(1),
      concatMap(initialState => this.statisticService.fetchStatistics([`TestCaseLibrary-${initialState.testCaseLibrary.id}`]).pipe(
        withLatestFrom(this.state$),
        map(([statistics, state]) => ({
          ...state,
          testCaseLibrary: {
            ...state.testCaseLibrary, statistics: {...statistics},
            dashboard: null,
            generatedDashboardOn: new Date(),
            shouldShowFavoriteDashboard: false
          }
        }))
      ))
    ).subscribe(state => this.commit(state));
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue) {
    this.partyPreferencesService.changeTestCaseWorkspaceFavoriteDashboard(preferenceValue).pipe(
      tap(() => {
        if (preferenceValue === 'default') {
          this.refreshStatistics();
        } else {
          this.refreshDashboard();
        }
      })
    ).subscribe();
  }

  refreshDashboard() {
    this.state$.pipe(
      take(1),
      concatMap((initialState: TestCaseLibraryViewState) => {
        if (initialState.testCaseLibrary.canShowFavoriteDashboard) {
          return this.customDashboardService.getDashboardWithDynamicScope(initialState.testCaseLibrary.favoriteDashboardId, {
            milestoneDashboard: false,
            workspaceName: 'TEST_CASE',
            testCaseLibraryIds: [initialState.testCaseLibrary.id],
            extendedHighLvlReqScope: false
          }).pipe(
            withLatestFrom(this.state$),
            map(([dashboard, state]) => ({
              ...state,
              testCaseLibrary: {
                ...state.testCaseLibrary,
                statistics: null,
                dashboard: {...dashboard},
                generatedDashboardOn: new Date(),
                shouldShowFavoriteDashboard: true
              }
            }))
          );
        } else {
          return of({
            ...initialState,
            testCaseLibrary: {...initialState.testCaseLibrary, shouldShowFavoriteDashboard: true}
          });
        }
      })
    ).subscribe(state => this.commit(state));
  }
}

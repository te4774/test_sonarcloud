import {ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {
  AbstractCreateEntityDialog,
  AbstractCreateEntityForm,
  ActionErrorDisplayService,
  DataRow,
  DialogReference,
  EntityCreationDialogData,
  EntityCreationService,
  SessionPingService
} from 'sqtm-core';
import {CreateTestCaseFormComponent} from '../create-test-case-form/create-test-case-form.component';

@Component({
  selector: 'sqtm-app-create-test-case-dialog',
  templateUrl: './create-test-case-dialog.component.html',
  styleUrls: ['./create-test-case-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateTestCaseDialogComponent extends AbstractCreateEntityDialog implements OnInit {

  @ViewChild(CreateTestCaseFormComponent)
  private form: CreateTestCaseFormComponent;

  constructor(dialogReference: DialogReference<EntityCreationDialogData, DataRow>,
              entityCreationService: EntityCreationService,
              actionErrorDisplayService: ActionErrorDisplayService,
              sessionPingService: SessionPingService) {
    super(dialogReference, entityCreationService, actionErrorDisplayService, sessionPingService);
  }

  get createEntityForm(): AbstractCreateEntityForm {
    return this.form;
  }

}

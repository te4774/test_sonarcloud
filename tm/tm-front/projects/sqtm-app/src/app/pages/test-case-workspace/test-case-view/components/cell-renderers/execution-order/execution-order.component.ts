import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-execution',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <a style="margin: auto;" class="text-overflow" [routerLink]="getExecutionUrl()">{{getExecutionOrder()}}</a>
      </div>
    </ng-container>`,
  styleUrls: ['./execution-order.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionOrderComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }
  ngOnInit() {
  }

  getExecutionOrder(): string {
    const order = this.row.data[this.columnDisplay.id];
    return `#${order + 1}`;
  }

  getExecutionUrl(): string {
    return `/execution/${this.row.data['executionId']}`;
  }

}

export function executionOrderColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ExecutionOrderComponent);
}

import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild} from '@angular/core';
import {TestCaseViewComponentData} from '../../containers/test-case-view/test-case-view.component';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {
  ActionErrorDisplayService,
  AutomatedTestTechnology,
  AutomationRequestStatusKeys,
  buildScmRepositoryUrl,
  buildTechnologyLabel,
  EditableSelectFieldComponent,
  EditableSelectLevelEnumFieldComponent,
  EditableTextFieldComponent,
  getSupportedBrowserLang,
  LevelEnumItem,
  Option,
  ReferentialDataService,
  RemoteAutomationRequestExtender,
  TestAutomationServerKind,
  TestCaseAutomatable
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';
import {catchError, finalize, take} from 'rxjs/operators';
import {TestCaseState} from '../../state/test-case.state';

@Component({
  selector: 'sqtm-app-test-case-view-automation-panel',
  templateUrl: './test-case-view-automation-panel.component.html',
  styleUrls: ['./test-case-view-automation-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseViewAutomationPanelComponent implements OnInit {

  @Input()
  componentData: TestCaseViewComponentData;

  @ViewChild('automationRequestPriorityField')
  automationRequestPriorityField: EditableTextFieldComponent;

  @ViewChild('technologyField')
  technologyField: EditableSelectFieldComponent;

  @ViewChild('autoRequestStatusField')
  autoRequestStatusField: EditableSelectLevelEnumFieldComponent;

  automatableOptions: Option[] = [];
  technologyOptions: Option[] = [];
  scmRepoUrlOptions: Option[] = [];

  readonly keys: { [key: string]: keyof TestCaseState } = {
    scmRepositoryId: 'scmRepositoryId',
    automatedTestReference: 'automatedTestReference',
  };

  get editable() {
    return this.componentData.permissions.canWrite && this.componentData.milestonesAllowModification;
  }

  get syncErrorMessage() {
    return `sqtm-core.test-case-workspace.automation.synchronization.message.${this.componentData.testCase.automationRequest.extender?.synchronizableIssueStatus}`;
  }

  get syncErrorTooltip() {
    return `sqtm-core.test-case-workspace.automation.synchronization.help.${this.componentData.testCase.automationRequest.extender?.synchronizableIssueStatus}`;
  }

  get isTaServerJenkinsKind() {
    return this.componentData.projectData.taServer != null
        && this.componentData.projectData.taServer.kind === TestAutomationServerKind.jenkins;
  }

  constructor(
    public testCaseViewService: TestCaseViewService,
    public translateService: TranslateService,
    private datePipe: DatePipe,
    private referentialDataService: ReferentialDataService,
    private actionDisplayError: ActionErrorDisplayService
  ) {
  }

  ngOnInit() {
    this.prepareAutomatableOptions();
    this.prepareTechnologyOptions();
    this.prepareScmRepoUrlOptions();
  }

  private prepareScmRepoUrlOptions(): void {
    this.referentialDataService.scmServers$
      .pipe(take(1))
      .subscribe((scmServers => {
        this.scmRepoUrlOptions = scmServers
          .flatMap(server => server.repositories.map(repo => ({
            value: repo.scmRepositoryId.toString(),
            label: buildScmRepositoryUrl(server, repo),
          })));
      }));
  }

  private prepareTechnologyOptions(): void {
    this.referentialDataService.automatedTestTechnologies$
      .pipe(take(1))
      .subscribe(((technologies: AutomatedTestTechnology[]) => {
        technologies.sort((first, second) => first.name.localeCompare(second.name));

        this.technologyOptions = technologies.map(techno => ({
            value: techno.id.toString(10),
            label: buildTechnologyLabel(techno),
          }));
      }));
  }

  private prepareAutomatableOptions(): void {
    this.automatableOptions = Object.values(TestCaseAutomatable).map(testCaseAutomatableItem => ({
      label: this.translateService.instant(testCaseAutomatableItem.i18nKey),
      value: testCaseAutomatableItem.id,
    }));
  }

  changeAutomatable(testCaseId: number, value) {
    this.testCaseViewService.updateAutomatable(testCaseId, value);
  }

  changeAutomationRequestPriority(testCaseId: number, value: string) {
    const parsedValue = parseInt(value, 10);

    if (!isNaN(parsedValue) || value === '') {
      this.testCaseViewService.updateAutomationRequestPriority(testCaseId, value);
      this.automationRequestPriorityField.value = value;
    } else {
      this.automationRequestPriorityField.formControl.setErrors({invalidNumber: true});
      setTimeout(() => this.automationRequestPriorityField.value = this.componentData.testCase.automationRequest.priority?.toString(), 500);
    }
  }

  changeAutomationRequestStatus(testCaseId: number, value: LevelEnumItem<AutomationRequestStatusKeys>) {
    if (value != null) {
      this.testCaseViewService.updateAutomationRequestStatus(testCaseId, value.id).pipe(
        catchError(err => this.actionDisplayError.handleActionError(err)),
        finalize(() => this.endStatusFieldAsync()),
      ).subscribe();
    } else {
      this.endStatusFieldAsync();
    }
  }

  getTableClass(componentData: TestCaseViewComponentData): string {
    return componentData.projectData.automationWorkflowType === 'REMOTE_WORKFLOW' ?
      'automation-panel-grid-half' : 'automation-panel-grid';
  }

  showSecondColumn(componentData: TestCaseViewComponentData): boolean {
    return componentData.testCase.automatable === 'Y'
      && componentData.projectData.automationWorkflowType === 'REMOTE_WORKFLOW';
  }

  isTableVisible(componentData: TestCaseViewComponentData): boolean {
    return componentData.testCase.automatable === 'Y'
      && !!componentData.testCase.automationRequest;
  }

  isAutomated(componentData: TestCaseViewComponentData): string {
    if (componentData.testCase.automationRequest.extender == null) {
      return '-';
    }

    const {configuredRemoteFinalStatus, automationRequest} = componentData.testCase;
    const isAutomated = configuredRemoteFinalStatus === automationRequest.extender.remoteStatus;
    const key = isAutomated ? 'sqtm-core.generic.label.yes' : 'sqtm-core.generic.label.no';
    return this.translateService.instant(key);
  }

  get transmittedOn(): string {
    if (this.componentData.testCase.automationRequest.transmittedOn !== null) {
      return this.formatDate(this.componentData.testCase.automationRequest.transmittedOn);
    } else {
      return this.translateService.instant('sqtm-core.generic.label.never');
    }
  }

  private formatDate(date: Date): string {
    return this.datePipe.transform(date, 'short', getSupportedBrowserLang(this.translateService));
  }

  changeAutomatedTestTechnology(option?: Option): void {
    const newValue = option?.value;
    this.technologyField.value = newValue;

    if (newValue == null || newValue === '') {
      this.testCaseViewService.changeAutomatedTestTechnology(null);
    } else {
      const technologyId = parseInt(newValue, 10);

      if (!isNaN(technologyId)) {
        this.testCaseViewService.changeAutomatedTestTechnology(technologyId);
      }
    }
  }

  shouldShowSyncErrorMessage(extender: RemoteAutomationRequestExtender): boolean {
    return extender?.synchronizableIssueStatus === 'NON_COMPLIANT' || extender?.synchronizableIssueStatus === 'DELETED';
  }

  toAutomDevopsExternalDocumentation(): void {
    const documentationUrl =  this.translateService.getBrowserLang() === 'fr' ?
      'https://autom-devops-fr.doc.squashtest.com/latest/autom/techno/index.html' :
      'https://autom-devops-en.doc.squashtest.com/latest/autom/techno/index.html';

    window.open(documentationUrl);
  }

  endStatusFieldAsync() {
    if (this.autoRequestStatusField) {
      this.autoRequestStatusField.child.edit = false;
      this.autoRequestStatusField.endAsync();
    }
  }
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {TestCaseFolderViewContentComponent} from './test-case-folder-view-content.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY} from 'rxjs';
import {TestCaseFolderViewService} from '../../services/test-case-folder-view.service';
import {TestCaseFolderState} from '../../state/test-case-folder.state';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import SpyObj = jasmine.SpyObj;

describe('TestCaseFolderViewContentComponent', () => {
  let component: TestCaseFolderViewContentComponent;
  let fixture: ComponentFixture<TestCaseFolderViewContentComponent>;

  const testCaseFolderViewService: SpyObj<TestCaseFolderViewService> = jasmine.createSpyObj('testCaseFolderViewService', ['load']);
  testCaseFolderViewService.componentData$ = EMPTY;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      declarations: [TestCaseFolderViewContentComponent],
      providers: [
        {
          provide: TestCaseFolderViewService,
          useValue: testCaseFolderViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCaseFolderViewContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should generate correct scope for serialization', () => {
    const testCaseFolderState: TestCaseFolderState = {
      id: 1,
      name: 'Folder 1',
      projectId: 1
    } as TestCaseFolderState;
    const scope = component.getStatisticScope(testCaseFolderState);
    const expectedScope = [
      {
        label: 'Folder 1',
        projectId: 1,
        id: 'TestCaseFolder-1'
      }];
    expect(scope).toEqual(expectedScope);
  });
});

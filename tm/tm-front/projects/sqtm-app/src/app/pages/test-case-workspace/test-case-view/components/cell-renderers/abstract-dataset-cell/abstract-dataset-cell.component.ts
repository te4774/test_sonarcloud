import {ChangeDetectorRef, Directive, ViewChild, ViewContainerRef} from '@angular/core';
import {
  AbstractCellRendererComponent,
  AlertDialogComponent,
  buildErrorI18nKeyProvider,
  DialogService,
  EditableTextFieldComponent,
  ErrorI18nKeyProvider,
  GridService,
} from 'sqtm-core';
import {ValidationErrors} from '@angular/forms';
import {timer} from 'rxjs';
import {take, tap} from 'rxjs/operators';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractDatasetCellComponent extends AbstractCellRendererComponent {

  @ViewChild(EditableTextFieldComponent)
  editableTextField: EditableTextFieldComponent;

  get editable() {
    return this.gridDisplay.allowModifications;
  }

  protected constructor(public grid: GridService,
              public cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              protected vcr: ViewContainerRef) {
    super(grid, cdr);
  }

  formErrors(errors: ValidationErrors) {
    const clientSideErrors = this.generateClientSideErrors(errors);
    const i18nKey = clientSideErrors[0].provideI18nKey();

    this.editableTextField.cancel();
    timer(1, 10).pipe(
      take(1),
      tap(() => {
        this.dialogService.openDialog({
          id: 'alert',
          component: AlertDialogComponent,
          viewContainerReference: this.vcr,
          data: {
            id: 'param-error',
            level: 'DANGER',
            titleKey: 'sqtm-core.generic.label.error',
            messageKey: i18nKey
          }
        });
      })
    ).subscribe();
  }

  private generateClientSideErrors(errors: ValidationErrors): ErrorI18nKeyProvider[] {
    return Object
      .entries(errors)
      .map(([fieldName, errorValue]: [string, any]) => buildErrorI18nKeyProvider(fieldName, errorValue));
  }

}

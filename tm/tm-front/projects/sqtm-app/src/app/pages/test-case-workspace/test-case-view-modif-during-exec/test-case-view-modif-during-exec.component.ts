import {ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {
  DialogService,
  ExecutionService,
  InterWindowCommunicationService,
  InterWindowMessages,
  ReferentialDataService
} from 'sqtm-core';
import {
  TestCaseViewDetailComponent
} from '../test-case-view/containers/test-case-view-detail/test-case-view-detail.component';
import {concatMap, filter, map, take, tap, withLatestFrom} from 'rxjs/operators';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {
  BackToExecutionMessagePayload,
  ExecutionRunnerOpenerService
} from '../../execution/execution-runner/services/execution-runner-opener.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'sqtm-app-test-case-view-modif-during-exec',
  templateUrl: './test-case-view-modif-during-exec.component.html',
  styleUrls: ['./test-case-view-modif-during-exec.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseViewModifDuringExecComponent implements OnInit {

  @ViewChild(TestCaseViewDetailComponent)
  testCaseViewDetail: TestCaseViewDetailComponent;

  constructor(private referentialDataService: ReferentialDataService,
              private interWindowCommunicationService: InterWindowCommunicationService,
              private activatedRoute: ActivatedRoute,
              private executionService: ExecutionService,
              private executionRunnerOpenerService: ExecutionRunnerOpenerService,
              private dialogService: DialogService) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh().subscribe();
  }

  back() {
    this.activatedRoute.paramMap.pipe(
      take(1),
      withLatestFrom(this.testCaseViewDetail.testCaseViewService.stepCount$),
      tap(([, stepCount]) => this.showEmptyTestWarningIfNeeded(stepCount)),
      filter(([, stepCount]) => stepCount > 0),
      concatMap(([paramMap]) => {
        const executionId = this.extractExecutionId(paramMap);
        return this.executionService.createNewAfterModificationDuringExecution(executionId);
      })
    ).subscribe(({id}) => {
      if (this.interWindowCommunicationService.isAbleToSendMessagesToOpener()) {
        this.sendRestartExecutionMessage(id);
      } else {
        this.restartExecutionLocally(id);
      }
      window.close();
    });
  }

  public backUrl(): Observable<string> {
    return this.activatedRoute.paramMap.pipe(
      map(paramMap => `test-case-workspace/test-case/modification-during-exec/${this.extractTestCaseId(paramMap)}/execution/${this.extractExecutionId(paramMap)}`)
    );
  }

  private extractExecutionId(paramMap: ParamMap) {
    return Number.parseInt(paramMap.get('executionId'), 10);
  }

  private extractTestCaseId(paramMap: ParamMap) {
    return Number.parseInt(paramMap.get('testCaseId'), 10);
  }

  private restartExecutionLocally(id: number) {
    this.executionRunnerOpenerService.openExecutionPrologue(id);
  }

  private sendRestartExecutionMessage(id: number) {
    const payload: BackToExecutionMessagePayload = {executionId: id};
    this.interWindowCommunicationService.sendMessage(
      new InterWindowMessages('MODIFICATION-DURING-EXECUTION', payload));
  }

  private showEmptyTestWarningIfNeeded(stepCount: number) {
    if (stepCount === 0) {
      this.dialogService.openAlert({messageKey: 'sqtm-core.test-case-workspace.modif-during-exec.empty-test-case'});
    }
  }
}

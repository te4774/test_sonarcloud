import {ChangeDetectionStrategy, Component, OnDestroy} from '@angular/core';
import {
  coverageRequirementColumn,
  coverageVerifiedByColumn,
  DataRow,
  deleteColumn,
  Extendable,
  GenericDataRow,
  GridDefinition,
  GridService,
  indexColumn,
  levelEnumColumn,
  Limited,
  milestoneLabelColumn,
  ReferentialDataService,
  RequirementCriticality,
  RequirementStatus,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textCellWithToolTipColumn,
  textColumn
} from 'sqtm-core';
import {TCW_COVERAGE_TABLE} from '../../test-case-view.constant';
import {DeleteCoverageComponent} from '../cell-renderers/delete-coverage/delete-coverage.component';
import {pluck, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';


export function tcwCoverageTableDefinition(): GridDefinition {
  return smallGrid('test-case-view-coverages')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      textCellWithToolTipColumn('projectName', 'path')
        .changeWidthCalculationStrategy(new Limited(200))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn('reference')
        .changeWidthCalculationStrategy(new Limited(120))
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      coverageRequirementColumn('name')
        .changeWidthCalculationStrategy(new Limited(500))
        .withI18nKey('sqtm-core.entity.requirement.label.singular'),
      milestoneLabelColumn('milestoneLabels')
        .changeWidthCalculationStrategy(new Limited(130)),
      levelEnumColumn('criticality', RequirementCriticality)
        .withTitleI18nKey('sqtm-core.entity.generic.criticality.label')
        .withI18nKey('sqtm-core.entity.generic.criticality.short')
        .isEditable(false)
        .changeWidthCalculationStrategy(new Extendable(40, 0.1)),
      levelEnumColumn('status', RequirementStatus)
        .withTitleI18nKey('sqtm-core.entity.generic.status.label')
        .withI18nKey('sqtm-core.entity.generic.status.short')
        .isEditable(false)
        .changeWidthCalculationStrategy(new Extendable(40, 0.1)),
      coverageVerifiedByColumn('verifiedBy')
        .changeWidthCalculationStrategy(new Extendable(60, 0.6))
        .withI18nKey('sqtm-core.entity.generic.verified-by.label'),
      deleteColumn(DeleteCoverageComponent)
        .withViewport('rightViewport')
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withRowConverter(coverageLiteralConverter)
    .withInitialSortedColumns([
      { id: 'criticality', sort: Sort.ASC },
      { id: 'projectName', sort: Sort.ASC },
      { id: 'reference', sort: Sort.ASC },
      { id: 'name', sort: Sort.ASC }
    ])
    .build();
}

@Component({
  selector: 'sqtm-app-coverage-table',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./coverage-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: TCW_COVERAGE_TABLE
    }
  ]
})
export class CoverageTableComponent implements OnDestroy {
  unsub$ = new Subject<void>();

  constructor(public grid: GridService, public referentialDataService: ReferentialDataService) {
    this.referentialDataService.globalConfiguration$.pipe(
      takeUntil(this.unsub$),
      pluck('milestoneFeatureEnabled')
    ).subscribe((milestoneFeatureEnabled) => {
      this.grid.setColumnVisibility('milestoneLabels', milestoneFeatureEnabled);
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function coverageLiteralConverter(literals: Partial<DataRow>[]): DataRow[] {
  return literals.reduce((datarows, literal) => {
    const dataRow = new GenericDataRow();
    Object.assign(dataRow, literal);
    const directlyVerified = dataRow.data['directlyVerified'];
    const unDirectlyVerified = dataRow.data['unDirectlyVerified'];
    dataRow.disabled = !directlyVerified && unDirectlyVerified;
    datarows.push(dataRow);
    return datarows;
  }, []);
}

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewContainerRef
} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {concatMap, filter, map, take, takeUntil, tap} from 'rxjs/operators';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {
  AttachmentService,
  CopierService,
  CustomFieldValueService,
  DialogConfiguration,
  DialogService,
  DragAndDropService,
  EntityRowReference,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewService,
  gridServiceFactory,
  MilestoneModeData,
  REFERENCE_FIELD,
  ReferentialDataService,
  RestService,
  SquashTmDataRowType,
  TestCasePermissions,
  TestCaseService,
  TestStepService,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {TestCaseState} from '../../state/test-case.state';
import {
  TCW_CALLED_TC_TABLE,
  TCW_CALLED_TC_TABLE_CONF,
  TCW_COVERAGE_TABLE,
  TCW_COVERAGE_TABLE_CONF,
  TCW_DATASET_TABLE,
  TCW_DATASET_TABLE_CONF
} from '../../test-case-view.constant';
import {tcwCoverageTableDefinition} from '../../components/coverage-table/coverage-table.component';

import {tcwDatasetsTableDefinition} from '../../components/datasets-table/datasets-table.component';
import {TestStepViewService} from '../../service/test-step-view.service';
import {tcwCalledTCTableDefiniton} from '../../components/called-test-case/called-test-case.component';
import {AbstractTestCaseViewComponent} from '../abstract-test-case-view.component';
import {combineLatest} from 'rxjs';
import {
  NewTestCaseVersionDialogConfiguration,
  NewTestCaseVersionDialogResult
} from '../../components/dialog/new-version-dialog/new-test-case-version-dialog.configuration';
import {NewVersionDialogComponent} from '../../components/dialog/new-version-dialog/new-version-dialog.component';

@Component({
  selector: 'sqtm-app-test-case-view',
  templateUrl: './test-case-view.component.html',
  styleUrls: ['./test-case-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TCW_COVERAGE_TABLE_CONF,
      useFactory: tcwCoverageTableDefinition
    },
    {
      provide: TCW_COVERAGE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_COVERAGE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: TCW_DATASET_TABLE_CONF,
      useFactory: tcwDatasetsTableDefinition
    },
    {
      provide: TCW_DATASET_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_DATASET_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: TCW_CALLED_TC_TABLE_CONF,
      useFactory: tcwCalledTCTableDefiniton
    },
    {
      provide: TCW_CALLED_TC_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_CALLED_TC_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: TestStepViewService,
      useClass: TestStepViewService,
      deps: [
        TestStepService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        ReferentialDataService,
        CopierService
      ]
    },
    {
      provide: TestCaseViewService,
      useClass: TestCaseViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        TestCaseService,
        CustomFieldValueService,
        TestStepViewService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        TCW_COVERAGE_TABLE,
        TCW_DATASET_TABLE,
        TCW_CALLED_TC_TABLE
      ]
    },
    {
      provide: EntityViewService,
      useExisting: TestCaseViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: TestCaseViewService
    }]
})
export class TestCaseViewComponent extends AbstractTestCaseViewComponent implements OnInit, OnDestroy, AfterViewInit {

  constructor(route: ActivatedRoute,
              router: Router,
              testCaseViewService: TestCaseViewService,
              referentialDataService: ReferentialDataService,
              cdRef: ChangeDetectorRef,
              translateService: TranslateService,
              dndService: DragAndDropService,
              renderer: Renderer2,
              public vcr: ViewContainerRef,
              public dialogService: DialogService,
              public workspaceWithTree: WorkspaceWithTreeComponent) {
    super(route, router, testCaseViewService, referentialDataService, cdRef, translateService, dndService, renderer, vcr, dialogService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.initializeTreeSynchronisation();
  }

  private initializeTreeSynchronisation() {
    this.testCaseViewService.simpleAttributeRequiringRefresh = ['name', 'reference', 'importance', 'importanceAuto', 'status', 'nature'];
    this.testCaseViewService.externalRefreshRequired$
      .pipe(
        takeUntil(this.unsub$),
        map(({id}) => new EntityRowReference(id, SquashTmDataRowType.TestCase).asString())
      ).subscribe((identifier) => this.workspaceWithTree.requireNodeRefresh([identifier]));
  }

  createNewTestCaseVersion() {

    combineLatest([this.componentData$, this.referentialDataService.milestoneModeData$]).pipe(
      take(1),
      filter<[TestCaseViewComponentData, MilestoneModeData]>((data) => data[1].selectedMilestone.status !== 'LOCKED'),
      concatMap(([componentData, milestoneData]: [TestCaseViewComponentData, MilestoneModeData]) => {
        const configuration = this.buildNewVersionDialogConfiguration(componentData, milestoneData);
        return this.dialogService.openDialog<NewTestCaseVersionDialogConfiguration, NewTestCaseVersionDialogResult>(configuration)
          .dialogClosed$.pipe(
          filter((result => Boolean(result))),
          tap((result: NewTestCaseVersionDialogResult) => {
            const id = new EntityRowReference(result.newVersionId, SquashTmDataRowType.TestCase).asString();
            this.workspaceWithTree.requireCurrentContainerRefresh(id);
          }));
      })).subscribe();
  }

  private buildNewVersionDialogConfiguration(componentData: TestCaseViewComponentData, milestoneData: MilestoneModeData) {
    const name = `${componentData.testCase.name}-${milestoneData.selectedMilestone.label}`;
    const reference = componentData.testCase.reference;
    const description = componentData.testCase.description;
    const tcKind = componentData.testCase.kind;
    const configuration: DialogConfiguration<NewTestCaseVersionDialogConfiguration> = {
      viewContainerReference: this.vcr,
      component: NewVersionDialogComponent,
      id: 'new-test-case-version',
      width: 800,
      data: {
        titleKey: 'sqtm-core.generic.label.add-new-version',
        id: 'new-test-case-version',
        originalTestCaseId: componentData.testCase.id,
        projectId: componentData.projectData.id,
        optionalTextFields: [{...REFERENCE_FIELD, defaultValue: reference}],
        defaultValues: {
          name,
          description
        },
        tcKind
      }
    };
    return configuration;
  }
}

export interface TestCaseViewComponentData extends EntityViewComponentData<TestCaseState, 'testCase', TestCasePermissions> {
}

import {CustomDashboardModel, SqtmEntityState, TestCaseStatistics} from 'sqtm-core';

// tslint:disable-next-line:no-empty-interface
export interface TestCaseFolderState extends SqtmEntityState {
  name: string;
  description: string;
  statistics: TestCaseStatistics;
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}

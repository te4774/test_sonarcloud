import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {TestCaseState} from './test-case.state';

export interface TestCaseViewState extends EntityViewState<TestCaseState, 'testCase'> {
  testCase: TestCaseState;
}

export function provideInitialTestCaseView(): Readonly<TestCaseViewState> {
  return provideInitialViewState<TestCaseState, 'testCase'>('testCase');
}

import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {TestCaseViewService} from '../../service/test-case-view.service';

@Component({
  selector: 'sqtm-app-keyword-test-steps',
  template: `
    <ng-container *ngIf="testCaseViewService.componentData$ | async as componentData">
      <sqtm-app-keyword-step-list [steps]="testCaseViewService.testStep$ | async"
                                  [keywordTestCaseId]="componentData.testCase.id"
                                  [keywords]="componentData.projectData.keywords"
                                  [actionWordLibraryActive]="componentData.testCase.actionWordLibraryActive"
                                  [editable]="componentData.permissions.canWrite && componentData.milestonesAllowModification"
                                  [linkable]="componentData.permissions.canLink">
      </sqtm-app-keyword-step-list>
    </ng-container>`,
  styleUrls: ['./keyword-test-steps.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class KeywordTestStepsComponent implements OnInit {

  constructor(public testCaseViewService: TestCaseViewService) {
  }

  ngOnInit(): void {
  }

}

import {Injectable} from '@angular/core';
import {Update} from '@ngrx/entity';
import {Observable} from 'rxjs';
import {concatMap, filter, map, take} from 'rxjs/operators';
import {
  ActionStepFormModel,
  ActionStepModel,
  ActionWordPermissions,
  AddTestStepOperationReport,
  attachmentEntityAdapter,
  AttachmentState,
  CallStepModel,
  ChangeCoverageOperationReport,
  CopierService,
  DeleteTestStepOperationReport,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  KeywordStepFormModel,
  KeywordStepModel,
  PasteTestStepOperationReport,
  PersistedAttachment,
  ProjectData,
  ProjectDataMap,
  ReferentialDataService,
  TestCaseKindKeys,
  TestCaseParameterOperationReport,
  TestStepModel,
  TestStepService,
  updateEntityInViewState,
  UploadAttachmentEvent
} from 'sqtm-core';
import {coverageEntityAdapter} from '../state/requirement-version-coverage.state';
import {TestCaseViewState} from '../state/test-case-view.state';
import {TestCaseState, TestCaseStepHolder} from '../state/test-case.state';
import {
  ActionStepState,
  CallStepPlaceholderState,
  CallStepState,
  CreateActionStepFormState,
  KeywordStepState,
  KeywordStepView,
  stepsEntityAdapter,
  TestStepsState,
  TestStepState
} from '../state/test-step.state';
import {callStepDndPlaceholderId, createActionStepFormId} from '../test-case-view.constant';
import {testCaseViewLogger} from '../test-case-view.logger';

const logger = testCaseViewLogger.compose('TestStepViewService');

@Injectable()
export class TestStepViewService {

  constructor(private testStepService: TestStepService, private attachmentHelper: EntityViewAttachmentHelperService,
              private customFieldHelper: EntityViewCustomFieldHelperService, private referentialDataService: ReferentialDataService,
              private copierService: CopierService) {
  }

  public initializeStepState(testStepModels: TestStepModel[],
                             testCaseKind: TestCaseKindKeys,
                             projectDataMap: ProjectDataMap): TestStepsState {
    const steps = this.convertStepModels(testStepModels, projectDataMap);
    const initialState = stepsEntityAdapter.getInitialState({
      selectedStepIds: [],
      copiedStepIds: [],
      initialStepOrder: [],
      draggingSteps: false,
      draggingTestCase: false,
      draggingRequirement: false,
      showPlaceHolder: false,
      currentDndTargetId: null,
      extendedPrerequisite: true,
      scrollTop: 0,
    });
    if (steps.length === 0 && testCaseKind !== 'KEYWORD') {
      return this.insertCreateActionStepForm(initialState, 0);
    } else {
      return stepsEntityAdapter.setAll(steps, initialState);
    }
  }

  /**
   * Forward the request to the dedicated http service. It's async and thus must be called in switchMap/concatMap.
   * Take care of using the last available state when updating client state in case of success
   */
  public writeAction(stepId: number, action: string): Observable<TestCaseParameterOperationReport> {
    return this.testStepService.postAction(stepId, action);
  }

  /**
   * Update the action in entity state. Do not send http request, it should be called in case of request success.
   */
  public updateAction(stepId: number, action: string, state: TestCaseViewState): TestCaseViewState {
    const testCase = {...state.testCase};
    const update: Update<ActionStepState> = {id: stepId, changes: {action}};
    testCase.testSteps = stepsEntityAdapter.updateOne(update, testCase.testSteps);
    return this.updateTestCase(testCase, state);
  }

  /**
   * Forward the request to the dedicated http service. It's async and thus must be called in switchMap/concatMap.
   * Take care of using the last available state when updating client state in case of success
   */
  public writeExpectedResult(id: number, expectedResult: string): Observable<TestCaseParameterOperationReport> {
    return this.testStepService.postExpectedResult(id, expectedResult);
  }

  /**
   * Update the expected result in entity state. Do not send http request, it should be called in case of request success.
   */
  public updateExpectedResult(id: number, expectedResult: string, state: TestCaseViewState): TestCaseViewState {
    const testCase = {...state.testCase};
    const update: Update<ActionStepState> = {id, changes: {expectedResult}};
    testCase.testSteps = stepsEntityAdapter.updateOne(update, testCase.testSteps);
    return this.updateTestCase(testCase, state);
  }

  public changeKeyword(stepId: number, keyword: string): Observable<any> {
    return this.testStepService.postKeyword(stepId, keyword);
  }

  public updateKeyword(stepId: number, keyword: string, state: TestCaseViewState): TestCaseViewState {
    const testCase = {...state.testCase};
    const update: Update<KeywordStepState> = {id: stepId, changes: {keyword}};
    testCase.testSteps = stepsEntityAdapter.updateOne(update, testCase.testSteps);
    return this.updateTestCase(testCase, state);
  }

  public changeActionWord(stepId: number, newActionWord: string): Observable<any> {
    return this.testStepService.postActionWord(stepId, newActionWord);
  }

  public changeActionWordWithId(stepId: number, newActionWord: string, actionWordId: number): Observable<any> {
    return this.testStepService.postActionWordWithId(stepId, newActionWord, actionWordId);
  }

  updateActionWord(stepId: number,
                   action: string,
                   styledAction: string,
                   actionWordId: number,
                   state: TestCaseViewState): TestCaseViewState {
    const testCase = {...state.testCase};
    const update: Update<KeywordStepState> = {id: stepId, changes: {action, styledAction, actionWordId}};
    testCase.testSteps = stepsEntityAdapter.updateOne(update, testCase.testSteps);
    return this.updateTestCase(testCase, state);
  }

  changeDatatable(stepId: number, datatable: any): Observable<any> {
    return this.testStepService.postDatatable(stepId, datatable);
  }

  updateDatatable(stepId: number, datatable: string, state: TestCaseViewState): TestCaseViewState {
    const testCase = {...state.testCase};
    const update: Update<KeywordStepState> = {id: stepId, changes: {datatable}};
    testCase.testSteps = stepsEntityAdapter.updateOne(update, testCase.testSteps);
    return this.updateTestCase(testCase, state);
  }

  changeDocstring(stepId: number, docstring: string): Observable<any> {
    return this.testStepService.postDocstring(stepId, docstring);
  }

  updateDocstring(stepId: number, docstring: string, state: TestCaseViewState) {
    const testCase = {...state.testCase};
    const update: Update<KeywordStepState> = {id: stepId, changes: {docstring}};
    testCase.testSteps = stepsEntityAdapter.updateOne(update, testCase.testSteps);
    return this.updateTestCase(testCase, state);
  }

  changeComment(stepId: number, comment: string): Observable<any> {
    return this.testStepService.postComment(stepId, comment);
  }

  updateComment(stepId: number, comment: string, state: any) {
    const testCase = {...state.testCase};
    const update: Update<KeywordStepState> = {id: stepId, changes: {comment}};
    testCase.testSteps = stepsEntityAdapter.updateOne(update, testCase.testSteps);
    return this.updateTestCase(testCase, state);
  }

  /**
   * Forward the request to the dedicated http service. It's async and thus must be called in switchMap/concatMap.
   * Take care of using the last available state when updating client state in case of success
   */
  writeCustomFieldValue<T extends TestCaseStepHolder>(stepId: number,
                                                      cufValueId: number,
                                                      value: string | string[],
                                                      initialState: Readonly<T>): Observable<string | string[]> {
    const testStep = initialState.testCase.testSteps.entities[stepId] as ActionStepState;
    const customFieldValue = testStep.customFieldValues.entities[cufValueId];
    return this.customFieldHelper.writeCustomFieldValue(customFieldValue, value);
  }

  /**
   * Update the custom field value client side in TestStepState. Do not send http request, it should be called in case of request success.
   */
  updateStepCustomFieldValue(stepId: number, cufValueId: number, value: string | string[], initialState: Readonly<TestCaseViewState>):
    TestCaseViewState {
    const testStep = initialState.testCase.testSteps.entities[stepId] as ActionStepState;
    const customFieldValueState = this.customFieldHelper.updateCustomFieldValue(cufValueId, value, testStep.customFieldValues);
    const changes: Update<ActionStepState> = {id: testStep.id, changes: {customFieldValues: customFieldValueState}};
    const testStepsState = stepsEntityAdapter.updateOne(changes, initialState.testCase.testSteps);
    const testCase: TestCaseState = {...initialState.testCase, testSteps: testStepsState};
    return this.updateTestCase(testCase, initialState);

  }

  markStepAttachmentsToDelete<T extends TestCaseStepHolder>(ids: string[], stepId: number, state: T): T {
    return this.changeStepAttachmentToDelete(ids, true, stepId, state);
  }

  cancelStepAttachmentsToDelete<T extends TestCaseStepHolder>(ids: string[], stepId: number, state: T): T {
    return this.changeStepAttachmentToDelete(ids, false, stepId, state);
  }

  changeStepAttachmentToDelete<T extends TestCaseStepHolder>(ids: string[], pendingDelete: boolean, stepId: number, state: T): T {
    const testStep = {...state.testCase.testSteps.entities[stepId]} as ActionStepState;
    const attachmentState = this.changeAttachmentDeleteStatus(testStep.attachmentList.attachments, ids, pendingDelete);
    return this.updateTestStepAttachmentState(state, attachmentState, stepId);
  }

  removeStepRejectedAttachments<T extends TestCaseStepHolder>(attachmentIds: string[], stepId: number, state: T): T {
    const testStep = {...state.testCase.testSteps.entities[stepId]} as ActionStepState;
    const attachmentState = attachmentEntityAdapter.removeMany(attachmentIds, testStep.attachmentList.attachments);
    return this.updateTestStepAttachmentState(state, attachmentState, stepId);
  }

  removeAttachments<T extends TestCaseStepHolder>(attachmentIds: string[], stepId: number, state: Readonly<T>): T {
    if (logger.isDebugEnabled()) {
      logger.debug(`Removing attachments ${JSON.stringify(attachmentIds)} from ${stepId}. Initial state :`, [state]);
    }
    const testStep = {...state.testCase.testSteps.entities[stepId]} as ActionStepState;
    const attachmentState = attachmentEntityAdapter.removeMany(attachmentIds, testStep.attachmentList.attachments);
    const nextState = this.updateTestStepAttachmentState(state, attachmentState, stepId);
    if (logger.isDebugEnabled()) {
      logger.debug(`Removed attachments ${JSON.stringify(attachmentIds)} from ${stepId}. Next state :`, [nextState]);
    }
    return nextState;
  }

  mapUploadEventToState<T extends TestCaseStepHolder>(stepId: number, state: T, event: UploadAttachmentEvent): T {
    const actionStep = state.testCase.testSteps.entities[stepId] as ActionStepState;
    const attachmentState = this.attachmentHelper.mapUploadEventToState(actionStep.attachmentList.attachments, event);
    return this.updateTestStepAttachmentState(state, attachmentState, stepId);
  }

  addActionStepForm<T extends TestCaseStepHolder>(index: number, state: T): T {
    const testSteps = this.insertCreateActionStepForm(state.testCase.testSteps, index);
    return {...state, testCase: {...state.testCase, testSteps: testSteps}};
  }

  cancelAddTestStep<T extends TestCaseStepHolder>(state: Readonly<T>): T {
    let testSteps = {...state.testCase.testSteps};
    const ids = [...testSteps.ids].filter(id => id !== createActionStepFormId) as number[];
    testSteps = stepsEntityAdapter.removeOne(createActionStepFormId, testSteps);
    testSteps = this.reorderSteps(ids, testSteps);
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  /**
   * Forward the request to the dedicated http service. It's async and thus must be called in switchMap/concatMap.
   * Take care of using the last available state when updating client state in case of success
   */
  persistActionStep(actionStepForm: ActionStepFormModel, testCaseId: number): Observable<TestCaseStepStateOperationReport> {
    return this.testStepService.addActionStep(testCaseId, actionStepForm).pipe(
      map((actionStepModel: AddTestStepOperationReport) => {
        const actionStepState = this.convertActionStepModel(actionStepModel.testStep as ActionStepModel);
        return {
          testStepState: actionStepState,
          testCaseParameterOperationReport: actionStepModel.operationReport
        };
      }));
  }

  persistKeywordStep(keywordStepForm: KeywordStepFormModel, testCaseId: number): Observable<TestCaseStepStateOperationReport> {
    return this.testStepService.addKeywordStep(testCaseId, keywordStepForm).pipe(
      map((keywordStepModel: AddTestStepOperationReport) => {
        const keywordStepState = (keywordStepModel.testStep as KeywordStepState);
        return {
          testStepState: keywordStepState,
          testCaseParameterOperationReport: keywordStepModel.operationReport
        };
      })
    );
  }

  addNewActionStep<T extends TestCaseStepHolder>(actionState: ActionStepState, index: number, state: T): T {
    let testSteps = {...state.testCase.testSteps};
    // creation was a success server side. We must insert step at desired index, remove placeholder, and update other step order.
    const ids = [...testSteps.ids].filter(id => id !== createActionStepFormId) as number[];
    ids.splice(index, 0, actionState.id);
    testSteps = stepsEntityAdapter.addOne(actionState, testSteps);
    testSteps = stepsEntityAdapter.removeOne(createActionStepFormId, testSteps);
    testSteps = this.reorderSteps(ids, testSteps);
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  addNewKeywordStep<T extends TestCaseStepHolder>(keywordStep: KeywordStepView,
                                                  index: number, state: T,
                                                  projectDataMap: ProjectDataMap): T {
    let testSteps = {...state.testCase.testSteps};

    const projectData: ProjectData = projectDataMap[keywordStep.actionWordProjectId];
    const canReadActionWord = projectData ? new ActionWordPermissions(projectData).canRead : false;
    keywordStep.canReadActionWord = canReadActionWord;

    // creation was a success server side. We must insert step at desired index, and update other step order.
    const ids = [...testSteps.ids, keywordStep.id] as number[];
    ids.splice(index, 0, keywordStep.id);
    testSteps = stepsEntityAdapter.addOne(keywordStep, testSteps);
    testSteps = this.reorderSteps(ids, testSteps);
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  /**
   * Forward the request to the dedicated http service. It's async and thus must be called in switchMap/concatMap.
   * Take care of using the last available state when updating client state in case of success
   */
  eraseSteps<T extends TestCaseStepHolder>(stepId: number, state: T): Observable<DeleteTestStepOperationReport> {
    const selectedStepIds = state.testCase.testSteps.selectedStepIds;
    let stepsToDelete = [stepId];
    if (selectedStepIds.length > 0) {
      stepsToDelete = selectedStepIds;
    }
    return this.testStepService.deleteSteps(stepsToDelete, state.testCase.id).pipe(
      map((operation: TestCaseParameterOperationReport) => {
        return {operationReport: operation, testStepsToDelete: stepsToDelete};
      })
    );
  }

  removeSteps<T extends TestCaseStepHolder>(stepsToDelete: number[], state: T): T {
    let testSteps = {...state.testCase.testSteps};
    const stepIds = state.testCase.testSteps.ids as number[];
    const remainingTestSteps = stepIds.filter(id => !stepsToDelete.includes(id));
    testSteps = this.reorderSteps(remainingTestSteps, testSteps);
    testSteps = stepsEntityAdapter.removeMany(stepsToDelete, testSteps);
    testSteps = {...testSteps, selectedStepIds: []};

    if (testSteps.ids.length === 0 && state.testCase.kind !== 'KEYWORD') {
      testSteps = this.insertCreateActionStepForm(testSteps, 0);
    }
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  copySteps(stepId: number, state: Readonly<TestCaseViewState>): void {
    const selectedStepIds = state.testCase.testSteps.selectedStepIds;
    let copiedStepIds = [stepId];
    if (selectedStepIds.length > 0) {
      copiedStepIds = selectedStepIds;
    }
    this.copierService.notifyCopySteps(copiedStepIds, state.testCase.kind);
  }

  /**
   * Forward the request to the dedicated http service. It's async and thus must be called in switchMap/concatMap.
   * Take care of using the last available state when updating client state in case of success
   */
  persistPastedSteps(targetStepId: number, testCaseId: number, isAssignedToTargetProject?: boolean):
    Observable<PasteTestStepOperationReport> {
    return this.copierService.copiedTestStep$.pipe(
      take(1),
      filter((stepIds: number[]) => stepIds && stepIds.length > 0),
      concatMap((copiedStepIds) =>
        this.testStepService.pasteSteps(copiedStepIds, targetStepId, testCaseId, isAssignedToTargetProject))
    );
  }

  compareKeywordProjectsIds(testCaseId: number): Observable<boolean> {
    return this.copierService.copiedTestStep$.pipe(
    take(1),
      filter((stepIds: number[]) => stepIds && stepIds.length > 0),
      concatMap((copiedStepIds) => this.testStepService.compareKeywordProjectsIds(testCaseId, copiedStepIds))
    );
  }

  /**
   * Add steps client side
   */
  addPastedStep(targetStepId: number,
                report: PasteTestStepOperationReport,
                state: Readonly<TestCaseViewState>,
                projectDataMap: ProjectDataMap): TestCaseViewState {
    let testSteps = state.testCase.testSteps;
    const newStepOrder = [...testSteps.ids] as number[];
    const newStepIds = report.testSteps.map(value => value.id);

    const lastTestStepIdIndex = testSteps.ids.length - 1;
    const lastTestStepId = testSteps.ids[lastTestStepIdIndex] as number;
    const checkedTargetStepId = targetStepId ? targetStepId : lastTestStepId;

    const insertionIndex = newStepOrder.indexOf(checkedTargetStepId) + 1;
    newStepOrder.splice(insertionIndex, 0, ...newStepIds);
    const newSteps = this.convertStepModels(report.testSteps, projectDataMap);
    testSteps = stepsEntityAdapter.addMany(newSteps, testSteps);
    testSteps = stepsEntityAdapter.removeOne(createActionStepFormId, testSteps);
    testSteps = this.reorderSteps(newStepOrder, testSteps);
    const importance = report.testCaseImportance || state.testCase.importance;
    return {...state, testCase: {...state.testCase, importance, testSteps: {...testSteps}}};
  }

  selectStep(stepId: number, state: Readonly<TestCaseViewState>): TestCaseViewState {
    const testSteps = {...state.testCase.testSteps};
    if (testSteps.selectedStepIds.length === 1 && testSteps.selectedStepIds[0] === stepId) {
      testSteps.selectedStepIds = [];
    } else {
      testSteps.selectedStepIds = [stepId];
    }
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  toggleStepSelection(stepId: number, state: Readonly<TestCaseViewState>): TestCaseViewState {
    const testSteps = {...state.testCase.testSteps};
    let selectedStepIds: number[] = [...testSteps.selectedStepIds];
    if (selectedStepIds.includes(stepId)) {
      selectedStepIds.splice(selectedStepIds.indexOf(stepId), 1);
    } else {
      selectedStepIds.push(stepId);
      // reordering selection order according to step index
      // so when you dnd or move multiple step they keep their order in list and not in selection.
      // because the back end is done like that... so it rules !
      const ids = testSteps.ids as number[];
      selectedStepIds = ids.filter(id => selectedStepIds.includes(id));
    }
    return {...state, testCase: {...state.testCase, testSteps: {...testSteps, selectedStepIds}}};
  }

  extendStepSelection(stepId: number, state: Readonly<TestCaseViewState>): TestCaseViewState {
    const testSteps = {...state.testCase.testSteps};
    let selectedStepIds = this.doExtendStepSelection(testSteps, stepId);
    // reordering selection order according to step index
    const ids = testSteps.ids as number[];
    selectedStepIds = ids.filter(id => selectedStepIds.includes(id));
    return {...state, testCase: {...state.testCase, testSteps: {...testSteps, selectedStepIds}}};
  }

  startDraggingStep(draggedStepIds: number[], state: Readonly<TestCaseViewState>): TestCaseViewState {
    const initialStepOrder: number[] = [...state.testCase.testSteps.ids] as number[];
    const nextState: TestCaseViewState = {
      ...state,
      testCase: {
        ...state.testCase,
        testSteps: {
          ...state.testCase.testSteps,
          selectedStepIds: draggedStepIds,
          initialStepOrder,
          currentDndTargetId: null,
          showPlaceHolder: false,
          draggingSteps: true
        }
      }
    };
    const ids = [...state.testCase.testSteps.ids] as number[];
    const currentIndex = ids.indexOf(draggedStepIds[0]);
    const nextValidTarget = ids.slice(currentIndex).find(id => !draggedStepIds.includes(id));
    logger.debug(`Init dnd steps. Found next valid target : ${nextValidTarget}`);
    return this.dragOverStep(nextValidTarget, nextState);
  }

  dragOverStep(targetId: number, state: Readonly<TestCaseViewState>): TestCaseViewState {
    const movedStepIds = state.testCase.testSteps.selectedStepIds;
    const ids = state.testCase.testSteps.ids as number[];
    const nexStepOrder = ids.filter(id => !movedStepIds.includes(id));
    const dropIndex = targetId == null ? nexStepOrder.length : nexStepOrder.indexOf(targetId);
    nexStepOrder.splice(dropIndex, 0, ...movedStepIds);
    const testSteps = this.reorderSteps(nexStepOrder, state.testCase.testSteps);
    testSteps.showPlaceHolder = true;
    testSteps.currentDndTargetId = targetId;
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  /**
   * Forward the request to the dedicated http service. It's async and thus must be called in switchMap/concatMap.
   * Take care of using the last available state when updating client state in case of success
   */
  writeNewStepPosition(state: TestCaseViewState): Observable<any> {
    const movedStepIds = state.testCase.testSteps.selectedStepIds;
    const ids = state.testCase.testSteps.ids as number[];
    const nexStepOrder = ids.filter(id => !movedStepIds.includes(id));
    const currentDndTargetId = state.testCase.testSteps.currentDndTargetId;
    const dropIndex = this.findDropIndex(currentDndTargetId, nexStepOrder);
    return this.testStepService.moveSteps(movedStepIds, dropIndex, state.testCase.id);
  }

  dropStep(state: TestCaseViewState): TestCaseViewState {
    const testSteps = {...state.testCase.testSteps};
    testSteps.draggingSteps = false;
    testSteps.showPlaceHolder = false;
    testSteps.selectedStepIds = [];
    testSteps.currentDndTargetId = null;
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  /**
   * Forward the request to the dedicated http service. It's async and thus must be called in switchMap/concatMap.
   * Take care of using the last available state when updating client state in case of success
   */
  writeCallTestCase(calledTestCaseIds: number[], state: Readonly<TestCaseViewState>): Observable<any> {
    const ids = state.testCase.testSteps.ids as number[];
    const nexStepOrder = [...ids].filter((id) => id !== callStepDndPlaceholderId);
    const currentDndTargetId = state.testCase.testSteps.currentDndTargetId;
    const dropIndex = currentDndTargetId == null ? nexStepOrder.length : nexStepOrder.indexOf(currentDndTargetId);
    return this.testStepService.callTestCases(calledTestCaseIds, dropIndex, state.testCase.id);
  }

  callTestCase(operationReport: PasteTestStepOperationReport,
               state: Readonly<TestCaseViewState>,
               projectDataMap: ProjectDataMap): TestCaseViewState {
    const ids = state.testCase.testSteps.ids as number[];
    const nexStepOrder = [...ids].filter((id) => id !== callStepDndPlaceholderId);
    const currentDndTargetId = state.testCase.testSteps.currentDndTargetId;
    const dropIndex = currentDndTargetId == null ? nexStepOrder.length : nexStepOrder.indexOf(currentDndTargetId);
    const newSteps = this.convertStepModels(operationReport.testSteps, projectDataMap);
    const newStepIds = newSteps.map(step => step.id);
    nexStepOrder.splice(dropIndex, 0, ...newStepIds);
    let testSteps = stepsEntityAdapter.addMany(newSteps, state.testCase.testSteps);
    testSteps = stepsEntityAdapter.removeOne(callStepDndPlaceholderId, testSteps);
    testSteps = this.reorderSteps(nexStepOrder, testSteps);
    testSteps.draggingSteps = false;
    testSteps.draggingTestCase = false;
    testSteps.showPlaceHolder = false;
    testSteps.selectedStepIds = [];
    testSteps.currentDndTargetId = null;
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  cancelDrag(state: Readonly<TestCaseViewState>): TestCaseViewState {
    let testSteps = stepsEntityAdapter.removeOne(callStepDndPlaceholderId, {...state.testCase.testSteps});
    if (state.testCase.testSteps.draggingSteps) {
      testSteps = this.reorderSteps(testSteps.initialStepOrder, testSteps);
    } else if (state.testCase.testSteps.draggingTestCase) {
      testSteps = this.reorderSteps(testSteps.ids as number[], testSteps);
    }
    testSteps.initialStepOrder = [];
    testSteps.draggingSteps = false;
    testSteps.draggingTestCase = false;
    testSteps.draggingRequirement = false;
    testSteps.showPlaceHolder = false;
    testSteps.currentDndTargetId = null;
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  dragOverStepForCall(state: Readonly<TestCaseViewState>, targetId?: number): TestCaseViewState {
    let testSteps = state.testCase.testSteps;
    testSteps = stepsEntityAdapter.removeOne(callStepDndPlaceholderId, testSteps);
    const nextOrder = [...testSteps.ids as number[]];
    let dropIndex = nextOrder.length;
    if (targetId) {
      dropIndex = nextOrder.indexOf(targetId);
    }
    nextOrder.splice(dropIndex, 0, callStepDndPlaceholderId);
    const placeholder: CallStepPlaceholderState = {
      id: callStepDndPlaceholderId,
      kind: 'dnd-call-step-placeholder',
      projectId: null,
      extended: true,
      stepOrder: 0,
    };
    testSteps = stepsEntityAdapter.addOne(placeholder, testSteps);
    testSteps = this.reorderSteps(nextOrder, testSteps);
    testSteps.showPlaceHolder = true;
    testSteps.draggingTestCase = true;
    testSteps.currentDndTargetId = targetId;
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  suspendStepDrag(state: Readonly<TestCaseViewState>): TestCaseViewState {
    let testSteps = {...state.testCase.testSteps};
    testSteps = this.reorderSteps(state.testCase.testSteps.initialStepOrder, testSteps);
    testSteps.draggingSteps = true;
    testSteps.draggingTestCase = false;
    testSteps.showPlaceHolder = false;
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  suspendTestCaseDrag(state: Readonly<TestCaseViewState>): TestCaseViewState {
    let testSteps = {...state.testCase.testSteps};
    testSteps = stepsEntityAdapter.removeOne(callStepDndPlaceholderId, testSteps);
    testSteps = this.reorderSteps(testSteps.ids as number[], testSteps);
    testSteps.draggingSteps = false;
    testSteps.draggingTestCase = true;
    testSteps.showPlaceHolder = false;
    testSteps.currentDndTargetId = null;
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  collapseAllSteps(state: Readonly<TestCaseViewState>): TestCaseViewState {
    let testSteps = {...state.testCase.testSteps};
    const changes: Update<TestStepState>[] = (testSteps.ids as number[]).map((id) => {
      return {id: id, changes: {extended: false}};
    });
    testSteps = stepsEntityAdapter.updateMany(changes, testSteps);
    testSteps.extendedPrerequisite = false;
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  expendAllSteps(state: Readonly<TestCaseViewState>): TestCaseViewState {
    let testSteps = {...state.testCase.testSteps};
    const changes: Update<TestStepState>[] = (testSteps.ids as number[]).map((id) => {
      return {id: id, changes: {extended: true}};
    });
    testSteps = stepsEntityAdapter.updateMany(changes, testSteps);
    testSteps.extendedPrerequisite = true;
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  toggleStep(id: number, state: Readonly<TestCaseViewState>): TestCaseViewState {
    let testSteps = {...state.testCase.testSteps};
    const testStep: TestStepState = testSteps.entities[id];
    testSteps = stepsEntityAdapter.updateOne({id: id, changes: {extended: !testStep.extended}}, testSteps);
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  writeStepMovement(stepId: number, dropIndex: number, state: TestCaseViewState): Observable<any> {
    return this.testStepService.moveSteps([stepId], dropIndex, state.testCase.id);
  }

  moveStep(stepId: number, dropIndex: number, state: Readonly<TestCaseViewState>): TestCaseViewState {
    const ids = state.testCase.testSteps.ids as number[];
    const nexStepOrder = ids.filter(id => id !== stepId);
    nexStepOrder.splice(dropIndex, 0, stepId);
    const testSteps = this.reorderSteps(nexStepOrder, state.testCase.testSteps);
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  togglePrerequisite(state: Readonly<TestCaseViewState>): TestCaseViewState {
    const testSteps: TestStepsState = {...state.testCase.testSteps};
    testSteps.extendedPrerequisite = !testSteps.extendedPrerequisite;
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  dragRequirementOverStep(stepId: number, state: Readonly<TestCaseViewState>): TestCaseViewState {
    const testSteps = {...state.testCase.testSteps};
    testSteps.currentDndTargetId = stepId;
    testSteps.draggingRequirement = true;
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  persistStepCoverages(requirementIds: number[], stepId: number, testCaseId: number) {
    return this.testStepService.persistStepCoverage(requirementIds, stepId, testCaseId);
  }

  dropRequirementForStepCoverages<T extends TestCaseStepHolder>(operationReport: ChangeCoverageOperationReport, state: Readonly<T>): T {
    const coverages = coverageEntityAdapter.setAll(operationReport.coverages, state.testCase.coverages);
    const testSteps = {...state.testCase.testSteps};
    testSteps.currentDndTargetId = null;
    testSteps.draggingRequirement = false;
    return {...state, testCase: {...state.testCase, coverages, testSteps}};
  }

  deleteStepCoverage<T extends TestCaseStepHolder>(operationReport: ChangeCoverageOperationReport, state: Readonly<T>): T {
    const coverages = coverageEntityAdapter.setAll(operationReport.coverages, state.testCase.coverages);
    const testSteps = {...state.testCase.testSteps};
    return {...state, testCase: {...state.testCase, coverages, testSteps}};
  }

  updateTestStepState(testSteps: TestStepModel[], state: TestCaseViewState, projectDataMap: ProjectDataMap) {
    const testStepsState = this.convertStepModels(testSteps, projectDataMap);
    const newTestSteps = stepsEntityAdapter.setAll(testStepsState, state.testCase.testSteps);
    return {...state, testCase: {...state.testCase, testSteps: newTestSteps}};
  }

  private findDropIndex(currentDndTargetId, nexStepOrder) {
    let dropIndex = nexStepOrder.length;
    if (currentDndTargetId && currentDndTargetId !== -1) {
      dropIndex = nexStepOrder.indexOf(currentDndTargetId);
    }
    return dropIndex;
  }

  private doExtendStepSelection(testSteps: TestStepsState, selectedStepId: number): number[] {
    let selectedStepIds: number[] = [...testSteps.selectedStepIds];
    if (selectedStepIds.length === 0) {
      selectedStepIds = [selectedStepId];
    } else {
      this.calculateExtendedSelection(testSteps, selectedStepIds, selectedStepId);
    }
    return selectedStepIds;
  }

  private calculateExtendedSelection(testSteps, selectedStepIds: number[], selectedStepId: number) {
    const allStepIds = [...testSteps.ids] as number[];
    const candidatesToSelect = this.findSelectionRange(selectedStepIds, allStepIds, selectedStepId);
    for (let i = 0; i < candidatesToSelect.length; i++) {
      const candidateToSelect = candidatesToSelect[i];
      if (!selectedStepIds.includes(candidateToSelect)) {
        selectedStepIds.push(candidateToSelect);
      }
    }
  }

  private findSelectionRange(selectedStepIds: number[], allStepIds: number[], selectedStepId: number) {
    const lastSelectedStep = selectedStepIds[selectedStepIds.length - 1];
    const lastSelectionIndex = allStepIds.indexOf(lastSelectedStep);
    const selectedStepIndex = allStepIds.indexOf(selectedStepId);
    let candidatesToSelect: number[];
    if (selectedStepIndex > lastSelectionIndex) {
      candidatesToSelect = allStepIds.slice(lastSelectionIndex, selectedStepIndex);
      candidatesToSelect.push(selectedStepId);
    } else {
      candidatesToSelect = allStepIds.slice(selectedStepIndex, lastSelectionIndex);
    }
    return candidatesToSelect;
  }


  private insertCreateActionStepForm(initialState: TestStepsState, index: number): TestStepsState {
    const createStepForm: CreateActionStepFormState = {
      kind: 'create-action-step-form',
      id: createActionStepFormId,
      stepOrder: 0,
      extended: true,
      projectId: null
    };
    let testSteps = {...initialState};
    const ids = [...testSteps.ids] as number[];
    ids.splice(index, 0, createStepForm.id);
    testSteps = stepsEntityAdapter.addOne(createStepForm, testSteps);
    testSteps = this.reorderSteps(ids, testSteps);
    return testSteps;
  }


  private reorderSteps(newOrder: number[], testSteps: Readonly<TestStepsState>): TestStepsState {
    const updates: Update<TestStepState>[] = [];
    for (let i = 0; i < newOrder.length; i++) {
      const remainingTestStep = newOrder[i];
      updates.push({id: remainingTestStep, changes: {stepOrder: i}});
    }
    return stepsEntityAdapter.updateMany(updates, testSteps);
  }

  private changeAttachmentDeleteStatus(state: AttachmentState, ids: string[], pendingDelete: boolean): AttachmentState {
    const changes: Update<PersistedAttachment>[] = ids.map(id => {
      return {id, changes: {pendingDelete}};
    });
    return attachmentEntityAdapter.updateMany(changes, state);
  }

  private updateTestStepAttachmentState<T extends TestCaseStepHolder>(state: T, attachmentState: AttachmentState, stepId: number): T {
    const testStep = {...state.testCase.testSteps.entities[stepId]} as ActionStepState;
    const attachmentList = {...testStep.attachmentList};
    attachmentList.attachments = attachmentState;
    const changes: Update<ActionStepState> = {id: testStep.id, changes: {attachmentList}};
    const testStepsState = stepsEntityAdapter.updateOne(changes, state.testCase.testSteps);
    return {...state, testCase: {...state.testCase, testSteps: testStepsState}};
  }

  convertStepModels(testStepModels: TestStepModel[], projectDataMap: ProjectDataMap) {
    const steps: TestStepState[] = testStepModels.map(model => {
      if (model.kind === 'action-step') {
        return this.convertActionStepModel(model as ActionStepModel);
      } else if (model.kind === 'call-step') {
        const callStepModel: CallStepModel = model as CallStepModel;
        return {...callStepModel, extended: false};
      } else {
        const keywordStepModel: KeywordStepModel = model as KeywordStepModel;
        const projectData: ProjectData = projectDataMap[keywordStepModel.actionWordProjectId];
        const canReadActionWord = projectData ? new ActionWordPermissions(projectData).canRead : false;
        return {...keywordStepModel, extended: false, canReadActionWord};
      }
    });
    return steps;
  }

  private convertActionStepModel(model: ActionStepModel): ActionStepState {
    const attachmentEntityState = this.attachmentHelper.initializeAttachmentState(model.attachmentList.attachments);
    const customFieldValueState = this.customFieldHelper.initializeCustomFieldValueState(model.customFieldValues);
    return {
      ...model,
      extended: true,
      attachmentList: {id: model.attachmentList.id, attachments: attachmentEntityState},
      customFieldValues: customFieldValueState
    };
  }

  private updateTestCase(testCase: TestCaseState, state: TestCaseViewState): TestCaseViewState {
    return updateEntityInViewState<TestCaseState, 'testCase'>(testCase, state);
  }


  eraseStepCoverages(id: number, stepId: number, requirementVersionIds: number[]): Observable<ChangeCoverageOperationReport> {
    return this.testStepService.eraseStepCoverages(id, stepId, requirementVersionIds);
  }

  updateStepViewScroll(scrollTop: number, state: TestCaseViewState) {
    const testSteps = {...state.testCase.testSteps};
    testSteps.scrollTop = scrollTop;
    return {...state, testCase: {...state.testCase, testSteps}};
  }

  updateParameterAssignationMode(testCaseId: number,
                                 stepId: number,
                                 parameterAssignationUpdate: ParameterAssignationUpdate,
                                 mode: TestCaseParameterMode): Observable<TestCaseParameterOperationReport> {
    return this.testStepService.updateParameterAssignationMode(testCaseId, stepId, parameterAssignationUpdate.datasetId, mode);
  }

  updateCallStepParameter<T extends TestCaseStepHolder>(stepId: number, calledDatasetId: number, calledDatasetName: string,
                                                        delegateParam: boolean, state: T): T {
    const testCase = {...state.testCase};
    const update: Update<CallStepState> = {id: stepId, changes: {calledDatasetId, delegateParam, calledDatasetName}};
    testCase.testSteps = stepsEntityAdapter.updateOne(update, testCase.testSteps);
    return {...state, testCase};
  }

  linkRequirementVersionToStep(stepId: number, requirementVersionId: number): Observable<any> {
    return this.testStepService.linkRequirementVersionToStep(stepId, requirementVersionId);
  }

  extractParameterMode(parameterAssignationMode: ParameterAssignationUpdate) {
    let mode: TestCaseParameterMode = 'DELEGATE';
    if (!parameterAssignationMode.delegateParam) {
      if (parameterAssignationMode.datasetId != null) {
        mode = 'CALLED_DATASET';
      } else {
        mode = 'NOTHING';
      }
    }
    return mode;
  }
}

export class TestCaseStepStateOperationReport {
  testStepState: TestStepState;
  testCaseParameterOperationReport: TestCaseParameterOperationReport;
}

export interface ParameterAssignationUpdate {
  datasetId: number;
  datasetName: string;
  delegateParam: boolean;
}

export type TestCaseParameterMode = 'DELEGATE' | 'CALLED_DATASET' | 'NOTHING';

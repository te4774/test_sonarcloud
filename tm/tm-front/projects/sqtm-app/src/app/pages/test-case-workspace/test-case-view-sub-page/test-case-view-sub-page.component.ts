import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ReferentialDataService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-test-case-view-sub-page',
  templateUrl: './test-case-view-sub-page.component.html',
  styleUrls: ['./test-case-view-sub-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseViewSubPageComponent implements OnInit {

  constructor(private referentialDataService: ReferentialDataService) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh().subscribe();
  }
}

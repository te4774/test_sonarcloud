import {ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {
  DialogReference,
  DisplayOption,
  GridService,
  i18NEnumToOptions,
  ReferentialDataService,
  RestService,
  SelectFieldComponent,
  TestCaseKind
} from 'sqtm-core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  CreateTestCaseFromRequirementDialogConfiguration
} from './create-test-case-from-requirement-dialog-configuration';

@Component({
  selector: 'sqtm-app-create-test-case-from-requirement-dialog',
  templateUrl: './create-test-case-from-requirement-dialog.component.html',
  styleUrls: ['./create-test-case-from-requirement-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateTestCaseFromRequirementDialogComponent implements OnInit {

  formGroup: FormGroup;
  data: CreateTestCaseFromRequirementDialogConfiguration;
  tree: GridService;

  @ViewChild(SelectFieldComponent)
  selectField: SelectFieldComponent;

  constructor(public dialogReference: DialogReference<CreateTestCaseFromRequirementDialogConfiguration>,
              private restService: RestService,
              private referentialDataService: ReferentialDataService,
              private fb: FormBuilder) {
    this.data = this.dialogReference.data;
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
        format: this.fb.control('STANDARD', [Validators.required])
      }
    );
  }

  confirm() {
    const tcKind: string = this.formGroup.controls.format.value;
    this.dialogReference.result = tcKind;
    this.dialogReference.close();

    // this.restService.post(['test-case-tree', destinationId, 'content/paste-from-requirement', tcKind], {references: nodeList})
    //   .subscribe(() => this.dialogReference.close());

    // this.createTcFromRequirement(destinationId, tcKind, nodeList);
  }

  // createTcFromRequirement(destinationId: string, tcKind: string, nodeList: string[]) {
  //   this.tree.beginAsyncOperation();
  //   this.restService.post(['test-case-tree', destinationId, 'content/paste-from-requirement', tcKind], {references: nodeList}).pipe(
  //     concatMap(() => this.refreshSubTrees([destinationId], true)),
  //     catchError(err => this.actionErrorDisplayService.handleActionError(err)),
  //     finalize(() => this.tree.completeAsyncOperation())
  //   ).subscribe((state: GridState) => this.tree.commit(state));
  // }
  //
  // private refreshSubTrees(ids: Identifier[], forceOpen?: boolean): Observable<GridState> {
  //   return this.referentialDataService.projectDatas$.pipe(
  //     take(1),
  //     concatMap((projectData: ProjectDataMap) =>
  //       this.tree.dataRowLoader.refreshSubTrees(ids, this.tree.gridState$, projectData, forceOpen)),
  //     switchMap(state => this.tree.filterManager.applyFilters(state)),
  //     concatMap(state => this.tree.gridNodeGenerator.computeNodeTree(state))
  //   );
  // }

  get kindOptions(): DisplayOption[] {
    return i18NEnumToOptions(TestCaseKind);
  }
}

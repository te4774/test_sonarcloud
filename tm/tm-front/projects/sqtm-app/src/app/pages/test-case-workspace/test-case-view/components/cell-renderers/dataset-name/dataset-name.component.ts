import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewContainerRef} from '@angular/core';
import {ColumnDefinitionBuilder, DialogService, GridService, RestService, SquashFieldError} from 'sqtm-core';
import {Validators} from '@angular/forms';
import {DatasetPatch, TestCaseViewService} from '../../../service/test-case-view.service';
import {AbstractDatasetCellComponent} from '../abstract-dataset-cell/abstract-dataset-cell.component';


@Component({
  selector: 'sqtm-app-dataset-name',
  template: `
    <ng-container *ngIf="columnDisplay && row.data">
      <div class="full-width full-height flex-row">
        <sqtm-core-editable-text-field style="margin: auto 0 auto 0" [layout]="'no-buttons'"
                                       class="full-width"
                                       [size]="'small'"
                                       [editable]="editable"
                                       [validators]="getValidators()"
                                       [value]="getName()"
                                       (confirmEvent)="updateAndClose($event)"
                                       (validatorErrorEvent)="formErrors($event)"
                                       #editableTextField ngDefaultControl></sqtm-core-editable-text-field>
      </div>
    </ng-container>`,
  styleUrls: ['./dataset-name.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetNameComponent  extends AbstractDatasetCellComponent implements OnInit {

  constructor(public  grid: GridService,
              public cdr: ChangeDetectorRef,
              private restService: RestService,
              private testCaseViewService: TestCaseViewService,
              protected dialogService: DialogService,
              protected vcr: ViewContainerRef) {
    super(grid, cdr, dialogService, vcr);
  }

  ngOnInit(): void {
  }

  getName() {
    return this.row.data['name'];
  }

  getValidators() {
    return [Validators.maxLength(255), Validators.required];
  }

  updateAndClose(value) {
    const dataSet: DatasetPatch = {
      id: this.row.id,
      name: value
    };
    this.restService.post([`datasets/rename`], dataSet).subscribe(
      () => {
        this.testCaseViewService.renameDataset(dataSet);
      },
      httpError => {
        if (httpError.status === 412) {
          const squashError = httpError.error.squashTMError;
          this.showError(squashError);
        }
      }
    );
  }

  showError(error: SquashFieldError) {
    const fieldErrors = error.fieldValidationErrors;
    const dialogRef = this.dialogService.openAlert({
      id: 'error-dialog',
      titleKey: 'sqtm-core.generic.label.error',
      messageKey: fieldErrors[0].i18nKey,
      level: 'DANGER'
    });
    dialogRef.dialogClosed$.subscribe(
      () => {
        this.editableTextField.endAsync();
        this.cdr.markForCheck();
      }
    );
  }

}

export function dataSetNameColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DatasetNameComponent);
}

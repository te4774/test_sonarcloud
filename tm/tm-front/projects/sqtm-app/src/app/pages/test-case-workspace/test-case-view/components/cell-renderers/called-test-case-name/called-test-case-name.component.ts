import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-called-test-case-name',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <a [routerLink]="['/', 'test-case-workspace','test-case', 'detail', row.data['id']]"
           style="margin: auto 0;"
           class="sqtm-grid-cell-txt-renderer" nz-tooltip
           [nzTooltipTitle]="row.data[columnDisplay.id]" [nzTooltipPlacement]="'topLeft'">{{row.data[columnDisplay.id]}}</a>
      </div>
    </ng-container>`,
  styleUrls: ['./called-test-case-name.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalledTestCaseNameComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(private gridService: GridService, private cdr: ChangeDetectorRef) {
    super(gridService, cdr);
  }

  ngOnInit() {
  }


}

export function calledTestCaseNameColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(CalledTestCaseNameComponent);
}

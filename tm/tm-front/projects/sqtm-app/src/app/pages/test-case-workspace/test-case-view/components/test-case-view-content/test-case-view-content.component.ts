import {AfterViewInit, ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TestCaseViewComponentData} from '../../containers/test-case-view/test-case-view.component';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {TranslateService} from '@ngx-translate/core';
import {catchError, take, takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {
  ActionErrorDisplayService,
  AutomationRequestStatus,
  BindableEntity,
  createCustomFieldValueDataSelector,
  createTestCaseCoverageMessageDialogConfiguration,
  CustomFieldData,
  DialogService,
  GridDndData,
  GridService,
  Option,
  parseDataRowId,
  REQUIREMENT_TREE_PICKER_ID,
  shouldShowCoverageMessageDialog,
  SqtmDropEvent,
  TestCaseAutomatable
} from 'sqtm-core';
import {testCaseViewLogger} from '../../test-case-view.logger';

import {TCW_COVERAGE_TABLE, testCaseViewContent} from '../../test-case-view.constant';
import {select} from '@ngrx/store';
import {Params} from '@angular/router';
import {
  TestCaseViewAutomationPanelComponent
} from '../test-case-view-automation-panel/test-case-view-automation-panel.component';

const logger = testCaseViewLogger.compose('TestCaseViewContentComponent');

@Component({
  selector: 'sqtm-app-test-case-view-info',
  templateUrl: './test-case-view-content.component.html',
  styleUrls: ['./test-case-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseViewContentComponent implements OnInit, AfterViewInit, OnDestroy {

  dropZoneId = testCaseViewContent;

  componentData$: Observable<TestCaseViewComponentData>;

  automatableOptions: Option[] = [];

  customFieldData: CustomFieldData[];

  unsub$ = new Subject<void>();

  @ViewChild(TestCaseViewAutomationPanelComponent)
  testCaseViewAutomationPanelComponent: TestCaseViewAutomationPanelComponent;

  constructor(public testCaseViewService: TestCaseViewService,
              public translateService: TranslateService,
              private dialogService: DialogService,
              @Inject(TCW_COVERAGE_TABLE) public coverageTable: GridService,
              private actionErrorDisplayService: ActionErrorDisplayService,
             ) {

  }

  ngOnInit() {
    this.componentData$ = this.testCaseViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    );

    this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createCustomFieldValueDataSelector(BindableEntity.TEST_CASE))
    ).subscribe(customFieldData => {
      this.customFieldData = customFieldData;
    });
    for (const testCaseAutomatable of Object.keys(TestCaseAutomatable)) {
      const item = TestCaseAutomatable[testCaseAutomatable];
      this.automatableOptions.push({label: this.translateService.instant(item.i18nKey), value: item.id});
    }
  }

  ngAfterViewInit(): void {
  }

  transmit(testCaseId: number, event) {
    event.stopPropagation();
    this.testCaseViewService.transmit(testCaseId).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err)),
     ).subscribe(() => {
        this.testCaseViewAutomationPanelComponent.autoRequestStatusField.beginAsync();
        this.testCaseViewAutomationPanelComponent.autoRequestStatusField.child.value = AutomationRequestStatus.TRANSMITTED.id;
        this.testCaseViewAutomationPanelComponent.endStatusFieldAsync();
    });
  }

  toggleCoverageDrawer(event) {
    event.stopPropagation();
    this.testCaseViewService.openRequirementTreePicker();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  dropIntoTestCase($event: SqtmDropEvent) {
    if ($event.dragAndDropData.origin === REQUIREMENT_TREE_PICKER_ID) {
      const data = $event.dragAndDropData.data as GridDndData;
      if (logger.isDebugEnabled()) {
        logger.debug(`Dropping requirement version in test case view.`, [data]);
      }
      const requirementIds = data.dataRows.map(row => parseDataRowId(row));
      this.testCaseViewService.addCoverages(requirementIds).pipe(take(1)).subscribe(operationReport => {
        if (shouldShowCoverageMessageDialog(operationReport)) {
          this.dialogService.openDialog(createTestCaseCoverageMessageDialogConfiguration(operationReport));
        }
      });
    }
  }

  deleteCoverages() {
    this.coverageTable.selectedRows$.pipe(
      take(1)
    ).subscribe((rows) => {
        const disabledRows = rows.filter(row => row.disabled);
        if (disabledRows.length > 0) {
          const alertDialog = this.dialogService.openAlert({
            level: 'INFO',
            titleKey: 'sqtm-core.generic.label.information.singular',
            messageKey: 'sqtm-core.test-case-workspace.dialog.message.indirect-verified-requirements'
          });
          alertDialog.dialogClosed$.subscribe(() => this.openDeleteConfirmCoveragesDialog());
        } else {
          this.openDeleteConfirmCoveragesDialog();
        }
      }
    );
  }

  private openDeleteConfirmCoveragesDialog() {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.test-case-workspace.dialog.title.remove-coverage.plural',
      messageKey: 'sqtm-core.test-case-workspace.dialog.message.remove-coverage.plural',
      level: 'WARNING'
    });
    dialogReference.dialogClosed$.subscribe((confirm) => {
      if (confirm) {
        this.testCaseViewService.deleteCoverages();
      }
    });
  }

  stopPropagation($event: MouseEvent) {
    $event.stopPropagation();
  }

  createSearchRequirementQueryParams(componentData: TestCaseViewComponentData): Params {
    const params: Params = {};
    const backUrl = componentData.testCase.navigationState.backUrl;
    if (backUrl) {
      params.backUrl = backUrl;
    }
    return params;
  }
}

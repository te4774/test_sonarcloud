import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {TestCaseViewService} from '../../service/test-case-view.service';
import {
  ExportModelBuilderService,
  Extendable,
  Fixed,
  grid,
  GridDefinition,
  GridExportService,
  GridService,
  gridServiceFactory,
  indexColumn,
  issueExecutionsColumn,
  issueKeyColumn,
  Limited,
  PaginationConfigBuilder,
  ReferentialDataService,
  RestService,
  Sort,
  textColumn
} from 'sqtm-core';
import {TCW_ISSUE_TABLE, TCW_ISSUE_TABLE_CONF} from '../../test-case-view.constant';
import {take, tap} from 'rxjs/operators';

export function tcwIssuesTableDefinition(): GridDefinition {
  return grid('test-case-view-issue').withColumns([
    indexColumn()
      .changeWidthCalculationStrategy(new Fixed(70))
      .withViewport('leftViewport'),
    issueKeyColumn('remoteId').withI18nKey('sqtm-core.entity.issue.key.label')
      .changeWidthCalculationStrategy(new Limited(100)),
    textColumn('btProject').withI18nKey('sqtm-core.entity.issue.project.label')
      .changeWidthCalculationStrategy(new Limited(150)).disableSort(),
    textColumn('summary').withI18nKey('sqtm-core.entity.issue.summary.label')
      .changeWidthCalculationStrategy(new Limited(200)).disableSort(),
    textColumn('priority').withI18nKey('sqtm-core.entity.issue.priority.label')
      .changeWidthCalculationStrategy(new Extendable(100, 0.4)).disableSort(),
    textColumn('status').withI18nKey('sqtm-core.entity.issue.status.label')
      .changeWidthCalculationStrategy(new Extendable(100, 0.4)).disableSort(),
    textColumn('assignee').withI18nKey('sqtm-core.entity.issue.assignee.label')
      .changeWidthCalculationStrategy(new Extendable(100, 1)).disableSort(),
    issueExecutionsColumn().withI18nKey('sqtm-core.entity.issue.reported-in.label')
      .changeWidthCalculationStrategy(new Extendable(100, 1.5)).disableSort(),
  ]).disableRightToolBar().server().withInitialSortedColumns([{
    id: 'remoteId',
    sort: Sort.DESC
  }]).withPagination(new PaginationConfigBuilder().initialSize(25)).withRowHeight(35).build();
}

@Component({
  selector: 'sqtm-app-issues',
  template:
    `
      <ng-container *ngIf="testCaseViewService.componentData$ | async as componentData">
        <div class="flex-column full-height full-width p-r-15" style="overflow-y: hidden;">
          <div class="flex-row p-r-10">
            <div class="issues-title">
              {{'sqtm-core.test-case-workspace.title.issues' | translate}}
            </div>
            <div class="collapse-button flex-row">
              <sqtm-core-grid-export-menu
                *ngIf="componentData.permissions.canRead"
                [exportContent]="'sqtm-core.grid.export.issue' | translate"
                [workspaceName]="'sqtm-core.test-case-workspace.label.short' | translate"
                [fileFormat]="'csv'"
              ></sqtm-core-grid-export-menu>
            </div>
          </div>
          <div class="issues-container full-width full-height p-15">
            <sqtm-core-issues-panel [entityId]="componentData.testCase.id"
                                    [entityType]="'TEST_CASE_TYPE'"
                                    [bugTracker]="componentData.projectData.bugTracker"
                                    (loadIssues)="loadData()">
              <sqtm-core-grid></sqtm-core-grid>
            </sqtm-core-issues-panel>
          </div>
        </div>
      </ng-container>
    `,
  styleUrls: ['./issues.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TCW_ISSUE_TABLE_CONF,
      useFactory: tcwIssuesTableDefinition
    },
    {
      provide: TCW_ISSUE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TCW_ISSUE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: TCW_ISSUE_TABLE
    },
    ExportModelBuilderService,
    GridExportService
  ]
})
export class IssuesComponent implements OnInit, OnDestroy {

  constructor(public testCaseViewService: TestCaseViewService,
              @Inject(TCW_ISSUE_TABLE) private gridService: GridService) {
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }

  loadData() {
    this.testCaseViewService.componentData$.pipe(
      take(1),
      tap(componentData => this.gridService.setServerUrl([`issues/test-case/${componentData.testCase.id}/known-issues`]))
    ).subscribe();
  }

}

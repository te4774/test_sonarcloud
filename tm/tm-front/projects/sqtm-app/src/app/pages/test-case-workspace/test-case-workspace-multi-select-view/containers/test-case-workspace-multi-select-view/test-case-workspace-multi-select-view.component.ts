import {ChangeDetectionStrategy, Component, OnDestroy} from '@angular/core';
import {TestCaseMultiSelectionService} from '../../services/test-case-multi-selection.service';
import {PartyPreferencesService, RestService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-test-case-workspace-multi-select-view',
  templateUrl: './test-case-workspace-multi-select-view.component.html',
  styleUrls: ['./test-case-workspace-multi-select-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TestCaseMultiSelectionService,
      useClass: TestCaseMultiSelectionService,
      deps: [RestService, PartyPreferencesService]
    }
  ]
})
export class TestCaseWorkspaceMultiSelectViewComponent implements OnDestroy {

  constructor(public readonly viewService: TestCaseMultiSelectionService) {
  }

  ngOnDestroy(): void {
    this.viewService.complete();
  }

}

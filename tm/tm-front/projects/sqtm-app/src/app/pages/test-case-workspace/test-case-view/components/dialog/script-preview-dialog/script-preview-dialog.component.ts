import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {DialogReference} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-script-preview-dialog',
  templateUrl: './script-preview-dialog.component.html',
  styleUrls: ['./script-preview-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScriptPreviewDialogComponent implements OnInit {

  data: string;

  constructor(private dialogReference: DialogReference<{ content: string }>) { }

  ngOnInit(): void {
    this.data = this.dialogReference.data.content;
  }

}

import {TestBed} from '@angular/core/testing';

import {TestCaseFolderViewService} from './test-case-folder-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {
  Permissions,
  ReferentialDataService,
  RestService,
  TestCaseFolderModel,
  TestCaseStatistics,
  TestCaseStatisticsService,
} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {of} from 'rxjs';
import {filter, scan} from 'rxjs/operators';
import {TestCaseFolderViewComponentData} from '../container/test-case-folder-view/test-case-folder-view.component';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {mockRestService} from '../../../../utils/testing-utils/mocks.service';
import SpyObj = jasmine.SpyObj;

describe('TestCaseFolderViewService', () => {

  const restService: SpyObj<RestService> = mockRestService();
  const testCaseStatisticsService: SpyObj<TestCaseStatisticsService> = jasmine.createSpyObj<TestCaseStatisticsService>(['fetchStatistics']);

  const referentialDataService = {} as ReferentialDataService;

  referentialDataService.connectToProjectData = jasmine.createSpy().and.returnValue(of({
    permissions: {TEST_CASE: [Permissions.WRITE]}
  }));

  referentialDataService.globalConfiguration$ = of(null);

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
    providers: [
      {
        provide: ReferentialDataService,
        useValue: referentialDataService
      },
      {
        provide: TestCaseStatisticsService,
        useValue: testCaseStatisticsService
      },
      {
        provide: RestService,
        useValue: restService
      },
      {
        provide: TestCaseFolderViewService,
        useClass: TestCaseFolderViewService
      }
    ]
  }));

  it('should load test case folder', (done) => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
    const service: TestCaseFolderViewService = TestBed.inject(TestCaseFolderViewService);
    service.componentData$.subscribe(data => {
      expect(data.testCaseFolder.id).toEqual(1);
      expect(data.testCaseFolder.generatedDashboardOn).toBeTruthy();
      done();
    });
    expect(service).toBeTruthy();
    service.load(1);
  });

  it('should refresh test case folder statistics', (done) => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
    testCaseStatisticsService.fetchStatistics.and.returnValue(of(getRefreshedStats()));
    const service: TestCaseFolderViewService = TestBed.inject(TestCaseFolderViewService);
    service.componentData$.pipe(
      scan((a, c) => [...a, c], []),
      filter(a => a.length === 2)
    ).subscribe(data => {
      const initialData: TestCaseFolderViewComponentData = data[0];
      const refreshedData: TestCaseFolderViewComponentData = data[1];
      expect(initialData).not.toEqual(refreshedData);
      expect(refreshedData.testCaseFolder.statistics.statusesStatistics.approved).toEqual(2);
      expect(refreshedData.testCaseFolder.statistics.statusesStatistics.underReview).toEqual(0);
      expect(refreshedData.testCaseFolder.generatedDashboardOn).not.toBe(initialData.testCaseFolder.generatedDashboardOn);
      // check immutability for other parts of state
      expect(initialData.testCaseFolder.attachmentList).toBe(refreshedData.testCaseFolder.attachmentList);
      done();
    });
    expect(service).toBeTruthy();
    service.load(1);
    service.refreshStatistics();
  });
});

function getInitialModel(): TestCaseFolderModel {
  return {
    id: 1,
    name: 'folder-1',
    description: '',
    attachmentList: {id: 1, attachments: []},
    customFieldValues: [],
    projectId: 1,
    statistics: {
      selectedIds: [2, 3],
      sizeStatistics: {zeroSteps: 0, between0And10Steps: 2, between11And20Steps: 0, above20Steps: 0},
      importanceStatistics: {low: 1, medium: 0, high: 1, veryHigh: 0},
      statusesStatistics: {workInProgress: 1, underReview: 1, approved: 0, toBeUpdated: 0, obsolete: 0},
      boundRequirementsStatistics: {zeroRequirements: 0, oneRequirement: 2, manyRequirements: 0}
    },
    dashboard: null,
    canShowFavoriteDashboard: false,
    favoriteDashboardId: null,
    shouldShowFavoriteDashboard: false
  };
}

function getRefreshedStats(): TestCaseStatistics {
  return {
    selectedIds: [2, 3],
    sizeStatistics: {zeroSteps: 0, between0And10Steps: 2, between11And20Steps: 0, above20Steps: 0},
    importanceStatistics: {low: 1, medium: 0, high: 1, veryHigh: 0},
    statusesStatistics: {workInProgress: 0, underReview: 0, approved: 2, toBeUpdated: 0, obsolete: 0},
    boundRequirementsStatistics: {zeroRequirements: 0, oneRequirement: 2, manyRequirements: 0}
  };
}

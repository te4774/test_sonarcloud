import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {DialogReference, DisplayOption, RestService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {TestCaseExportDialogConfiguration} from './test-case-export-dialog-configuration';

@Component({
  selector: 'sqtm-app-test-case-export-dialog',
  templateUrl: './test-case-export-dialog.component.html',
  styleUrls: ['./test-case-export-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class TestCaseExportDialogComponent implements OnInit {

  data: TestCaseExportDialogConfiguration;

  formGroup: FormGroup;

  constructor(public dialogReference: DialogReference<TestCaseExportDialogConfiguration>,
              private translateService: TranslateService,
              private fb: FormBuilder,
              private datePipe: DatePipe, private restService: RestService) {
    this.data = this.dialogReference.data;
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group(
      {
        format: this.fb.control('xls', [Validators.required]),
        fileName: this.fb.control(this.initFileName(), [Validators.required]),
        addCalledTC: this.fb.control(false, []),
        editableRichText: this.fb.control(true, []),
      }
    );
  }

  getFormat(): DisplayOption[] {
    return [{id: 'xls', label: 'XLS'}, {id: 'csv', label: 'CSV'}];
  }

  doExport() {
    const params = {
      nodes: this.data.nodes.toString(),
      libraries: this.data.libraries.toString(),
      filename: this.formGroup.controls.fileName.value,
      calls: this.formGroup.controls.addCalledTC.value,
      'keep-rte-format': this.formGroup.controls.editableRichText.value
    };
    return this.restService.buildExportUrlWithParams(`test-case/export/content/${this.formGroup.controls.format.value}`, params);
  }

  initFileName() {
    const date = new Date();
    const newDate = this.datePipe.transform(date, 'yyyyMMdd_HHmmss');
    return `${this.translateService.instant('sqtm-core.test-case-workspace.dialog.export.file-name-value')}_${newDate}`;
  }

  getFileName() {

    let fileName = this.formGroup.controls.fileName.value;

    if (this.formGroup.controls.format.value === 'xls') {
      fileName = fileName + '.xls';
    } else {
      fileName = fileName + '.csv';
    }
    return fileName;
  }

  close() {
    this.dialogReference.close();
  }
}

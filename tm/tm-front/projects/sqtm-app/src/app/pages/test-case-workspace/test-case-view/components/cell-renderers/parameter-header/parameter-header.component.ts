import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  AbstractHeaderRendererComponent,
  AlertDialogComponent,
  ColumnDefinitionBuilder,
  DialogService,
  EditableTextFieldComponent,
  GridService,
  ParametersService,
  SquashFieldError
} from 'sqtm-core';
import {Validators} from '@angular/forms';
import {ParameterPatch, TestCaseViewService} from '../../../service/test-case-view.service';
import {filter, take, takeUntil, tap} from 'rxjs/operators';
import {Observable, Subject, timer} from 'rxjs';
import {TestCaseViewComponentData} from '../../../containers/test-case-view/test-case-view.component';
import {
  ParamInformationDialogComponent
} from '../../dialog/param-information-dialog/param-information-dialog.component';


@Component({
  selector: 'sqtm-app-parameter-header',
  template: `
    <ng-container *ngIf="gridDisplay">
      <ng-container *ngIf="componentData$ | async as componentData">
        <div class="full-height full-width flex-row p-l-5 p-r-2" style="align-items: center">
          <sqtm-core-editable-text-field style="font-size: 12px" [layout]="'no-buttons'"
                                         class="sqtm-grid-cell-txt-renderer"
                                         [size]="'small'"
                                         [editable]="editable(componentData)"
                                         [ngStyle]="{'font-style': isDelegate(componentData) ? 'italic': 'normal'}"
                                         [validators]="getValidators()"
                                         [value]="columnDisplay.label"
                                         (confirmEvent)="renameParam($event)"
                                         (validatorErrorEvent)="formErrors()"
                                         #editableTextField ngDefaultControl>
          </sqtm-core-editable-text-field>
          <i nz-icon nz-dropdown [nzDropdownMenu]="paramMenu"
             nzType="ellipsis"
             nzTheme="outline"
             class="current-workspace-button __hover_pointer m-l-10 table-icon-size"
             [attr.data-test-icon-id]="'param-action'"></i>
        </div>
        <nz-dropdown-menu #paramMenu="nzDropdownMenu">
          <ul nz-menu [attr.data-test-menu-id]="'param-menu'">
            <li nz-menu-item (click)="openParamInformationDialog(componentData)"
                [attr.data-test-menu-item-id]="'information-param'">
              {{ ('sqtm-core.generic.label.information.plural' | translate) }}
            </li>
            <li *ngIf="editable(componentData)" style="color: rgba(255, 0, 0, 0.72);" nz-menu-item
                (click)="removeParam()"
                [attr.data-test-menu-item-id]="'remove-param'">
              {{ ('sqtm-core.generic.label.delete' | translate) }}
            </li>
          </ul>
        </nz-dropdown-menu>
      </ng-container>

    </ng-container>
  `,
  styleUrls: ['./parameter-header.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParameterHeaderComponent extends AbstractHeaderRendererComponent implements OnInit, OnDestroy {

  componentData$: Observable<TestCaseViewComponentData>;

  @ViewChild('editableTextField')
  editableTextField: EditableTextFieldComponent;

  unsub$ = new Subject<void>();

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              private testCaseViewService: TestCaseViewService,
              private dialogService: DialogService,
              private parameterService: ParametersService,
              private vcr: ViewContainerRef) {
    super(grid, cdRef);
    this.componentData$ = this.testCaseViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngOnInit() {
  }


  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getValidators() {
    return [Validators.required, Validators.pattern('[A-Za-z0-9_-]{1,255}')];
  }

  editable(componentData: TestCaseViewComponentData) {
    return componentData.permissions.canWrite && componentData.milestonesAllowModification && !this.isDelegate(componentData);
  }

  formErrors() {
    this.editableTextField.cancel();
    timer(1, 10).pipe(
      take(1),
      tap(() => {
        this.dialogService.openDialog({
          id: 'alert',
          component: AlertDialogComponent,
          viewContainerReference: this.vcr,
          data: {
            id: 'param-error',
            level: 'DANGER',
            titleKey: 'sqtm-core.generic.label.error',
            messageKey: 'sqtm-core.validation.errors.paramName'
          }
        });
      })
    ).subscribe();

  }

  renameParam(currentValue) {
    const paramId = this.getParamId();
    const param: ParameterPatch = {id: paramId, name: currentValue};
    this.parameterService.renameParameter(paramId, currentValue)
      .subscribe((parameterRename) => {
          this.testCaseViewService.renameParameter(param, parameterRename.testStepList, parameterRename.prerequisite);
          this.grid.renameColumn(this.columnDisplay.id, currentValue);
        },
        error => {
          if (error.status === 412) {
            const squashError = error.error.squashTMError;
            this.cantRenameParam(squashError);
          }
        }
      );
  }

  removeParam() {

    const columnId = this.getParamId();
    this.testCaseViewService.paramIsUsed(columnId).pipe(
      take(1),
    ).subscribe(used => {
        if (!used) {
          this.openParamDeleteDialog(columnId);
        } else {
          this.openAlert();
        }
      }
    );
  }

  cantRenameParam(error: SquashFieldError) {
    const fieldErrors = error.fieldValidationErrors;
    const dialogRef = this.dialogService.openAlert({
      id: 'error-dialog',
      titleKey: 'sqtm-core.generic.label.error',
      messageKey: fieldErrors[0].i18nKey,
      level: 'DANGER'
    });
    dialogRef.dialogClosed$.subscribe(
      () => {
        this.editableTextField.endAsync();
        this.cdRef.markForCheck();
      }
    );
  }

  openParamInformationDialog(componentData: TestCaseViewComponentData) {

    const parameter = componentData.testCase.parameters.entities[this.getParamId()];
    const informationDialog = this.dialogService.openDialog({
      id: 'information-param',
      component: ParamInformationDialogComponent,
      data: {
        id: 'information-param',
        parameter: parameter,
        delegate: this.isDelegate(componentData),
        canWrite: componentData.permissions.canWrite && componentData.milestonesAllowModification
      }
    });

    informationDialog.dialogResultChanged$.pipe(takeUntil(informationDialog.dialogClosed$)).subscribe(result => {
      if (result['name'] != null) {
        this.renameParam(result['name']);
      }

      if (result['description'] != null) {
        this.testCaseViewService.changeParameterDescription(result as ParameterPatch);
      }
    });
  }

  openParamDeleteDialog(columnId) {
    const dialogRef = this.dialogService.openDeletionConfirm({
      level: 'DANGER',
      id: 'delete-param',
      titleKey: 'sqtm-core.test-case-workspace.dialog.title.remove-parameter',
      messageKey: 'sqtm-core.test-case-workspace.dialog.message.remove-parameter'
    });

    dialogRef.dialogClosed$.pipe(
      take(1),
      filter(confirmRemove => confirmRemove === true),
      tap(() => this.testCaseViewService.removeParameter(columnId)),
      tap(() => this.grid.deleteColumns([this.columnDisplay.id]))
    ).subscribe();


  }

  openAlert() {
    this.dialogService.openAlert({
      level: 'DANGER',
      id: 'delete-param',
      titleKey: 'sqtm-core.test-case-workspace.dialog.title.cannot-remove-parameter',
      messageKey: 'sqtm-core.test-case-workspace.dialog.message.cannot-remove-parameter'
    });
  }

  isDelegate(componentData: TestCaseViewComponentData) {
    let paramId = this.columnDisplay.id.toString();
    paramId = paramId.replace('PARAM_COLUMN_', '');
    const parameter = componentData.testCase.parameters.entities[paramId];
    return parameter.sourceTestCaseId !== componentData.testCase.id;
  }

  private getParamId() {
    return this.columnDisplay.id.toString().replace('PARAM_COLUMN_', '');
  }
}

export function parameterNameColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ParameterHeaderComponent);
}

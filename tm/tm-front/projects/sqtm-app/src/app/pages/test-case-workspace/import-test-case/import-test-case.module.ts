import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  AttachmentModule,
  DialogModule,
  NavBarModule,
  SvgModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule
} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {ImportTestCaseComponent} from './containers/import-test-case/import-test-case.component';
import {ImportReportComponent} from './component/import-report/import-report.component';
import {XlsReportComponent} from './component/xls-report/xls-report.component';
import {ImportConfigurationComponent} from './component/import-configuration/import-configuration.component';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {NzPopoverModule} from 'ng-zorro-antd/popover';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
    declarations: [
        ImportTestCaseComponent,
        ImportReportComponent,
        XlsReportComponent,
        ImportConfigurationComponent
    ],
    imports: [
        CommonModule,
        SvgModule,
        WorkspaceCommonModule,
        NavBarModule,
        NzMenuModule,
        NzIconModule,
        NzToolTipModule,
        NzPopoverModule,
        NzInputModule,
        DialogModule,
        WorkspaceLayoutModule,
        NzDropDownModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
        NzButtonModule,
        FormsModule,
        NzSelectModule,
        AttachmentModule
    ]
})
export class ImportTestCaseModule {
}

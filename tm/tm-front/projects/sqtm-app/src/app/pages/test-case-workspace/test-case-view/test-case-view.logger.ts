import {testCaseWorkspaceLogger} from '../test-case-workspace.logger';

export const testCaseViewLogger = testCaseWorkspaceLogger.compose('test-case-view');

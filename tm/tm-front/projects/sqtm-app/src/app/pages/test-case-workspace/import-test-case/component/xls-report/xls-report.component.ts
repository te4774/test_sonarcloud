import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ImportFormatFailure, ImportLog} from '../../state/import-test-case.state';
import {ImportTestCaseService} from '../../services/import-test-case.service';

@Component({
  selector: 'sqtm-app-xls-report',
  templateUrl: './xls-report.component.html',
  styleUrls: ['./xls-report.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class XlsReportComponent implements OnInit {

  @Input()
  templateOk: ImportLog;

  @Input()
  templateKo: ImportFormatFailure;

  constructor(private importTestCaseService: ImportTestCaseService) {
  }

  ngOnInit(): void {
  }

  getFileName(templateOk: ImportLog) {
    return this.importTestCaseService.getDownLoadReportUrl(templateOk.reportUrl);
  }

}

import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {TC_WS_TREE, TC_WS_TREE_CONFIG, testCaseWorkspaceTreeId} from '../../../test-case-workspace.constant';
import {
  column,
  Extendable,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
  tree,
  TreeNodeCellRendererComponent,
  TreeNodeRendererDisplayOptions,
  TreeNodeServerOperationHandler,
} from 'sqtm-core';
import {
  TestCaseWorkspaceTreeMenuComponent
} from '../../components/test-case-workspace-tree-menu/test-case-workspace-tree-menu.component';
import {testCaseWorkspaceLogger} from '../../../test-case-workspace.logger';

export const logger = testCaseWorkspaceLogger.compose('TestCaseWorkspaceComponent');


export function testCaseWorkspaceTreeConfigFactory(): GridDefinition {
  const options: TreeNodeRendererDisplayOptions = {ellipsisOnLeft: false, kind: 'treeNodeRendererDisplay'};
  return tree(testCaseWorkspaceTreeId)
    .server()
    .withServerUrl(['test-case-tree'])
    .withColumns([
      column('NAME')
        .enableDnd()
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent)
        .withOptions(options)
    ])
    .withContextualMenu(TestCaseWorkspaceTreeMenuComponent)
    .build();
}

@Component({
  selector: 'sqtm-app-test-case-workspace',
  templateUrl: './test-case-workspace.component.html',
  styleUrls: ['./test-case-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TC_WS_TREE_CONFIG,
      useFactory: testCaseWorkspaceTreeConfigFactory,
      deps: []
    },
    TreeNodeServerOperationHandler,
    {
      provide: TC_WS_TREE,
      useFactory: gridServiceFactory,
      deps: [RestService, TC_WS_TREE_CONFIG, ReferentialDataService, TreeNodeServerOperationHandler]
    },
    {
      provide: GridService,
      useExisting: TC_WS_TREE
    }]
})
export class TestCaseWorkspaceComponent implements OnInit, OnDestroy {

  constructor(public readonly referentialDataService: ReferentialDataService,
              private readonly testCaseWorkspaceTree: GridService) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh().subscribe();
  }

  ngOnDestroy(): void {
    this.testCaseWorkspaceTree.complete();
  }

}

import {
  ActionStepFormModel,
  ActionStepModel,
  AttachmentService,
  AutomationRequest,
  AutomationRequestStatus,
  CalledTestCase,
  CallStepModel,
  ChangeCoverageOperationReport,
  CustomFieldValueService,
  Dataset,
  DatasetParamValue,
  DeleteTestStepOperationReport,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  EntityViewState,
  Execution,
  GridService,
  Identifier,
  KeywordStepFormModel,
  Milestone,
  Parameter,
  PasteTestStepOperationReport,
  ProjectData,
  ProjectDataMap,
  ReferentialDataService,
  RequirementVersionCoverage,
  RestService,
  StoreOptions,
  TestAutomation,
  TestCaseModel,
  TestCaseParameterOperationReport,
  TestCasePermissions,
  TestCaseService,
  TestStepActionWordOperationReport,
  TestStepModel,
  UploadAttachmentEvent
} from 'sqtm-core';
import {Observable} from 'rxjs';
import {concatMap, filter, map, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {
  calledTestCasesSelector,
  coveragesCountSelect,
  coveragesSelector,
  datasetCountSelect,
  draggingStepSelector,
  draggingTestCaseSelector,
  stepCountSelector,
  stepCreationModeSelector,
  stepsViewSelector,
  TestCaseState,
} from '../state/test-case.state';
import {provideInitialTestCaseView, TestCaseViewState} from '../state/test-case-view.state';
// tslint:disable-next-line:max-line-length
import {TranslateService} from '@ngx-translate/core';
import {
  ActionStepState,
  KeywordStepView,
  stepsEntityAdapter,
  TestStepsState,
  TestStepState,
  TestStepView
} from '../state/test-step.state';
import {select} from '@ngrx/store';
import {callStepDndPlaceholderId, createActionStepFormId} from '../test-case-view.constant';
import {ParameterAssignationUpdate, TestStepViewService} from './test-step-view.service';
import {testCaseWorkspaceLogger} from '../../test-case-workspace.logger';
import {coverageEntityAdapter, CoverageState} from '../state/requirement-version-coverage.state';
import {executionEntityAdapter, ExecutionState} from '../state/execution.state';
import {calledTestCaseEntityAdapter, CalledTestCaseState} from '../state/called-test-case.state';
import {TestCaseViewComponentData} from '../containers/test-case-view/test-case-view.component';
import {parameterEntityAdapter, ParameterState} from '../state/parameter.state';
import {datasetEntityAdapter, DataSetState} from '../state/dataset.state';
import {datasetParamValueEntityAdapter, DatasetParamValueState} from '../state/dataset-param-value.state';

const logger = testCaseWorkspaceLogger.compose('TestCaseViewService');
const storeOptions: StoreOptions = {
  id: 'TestCaseViewStore',
  logDiff: 'detailed'
};

/**
 * Facade for the TestCaseView.
 */
@Injectable()
export class TestCaseViewService extends EntityViewService<TestCaseState, 'testCase', TestCasePermissions> {

  public readonly testStep$: Observable<TestStepView[]> = this.componentData$.pipe(
    select(stepsViewSelector)
  );

  public readonly stepDragging$: Observable<boolean> = this.componentData$.pipe(
    select(draggingStepSelector)
  );

  public readonly testCaseDragging$: Observable<boolean> = this.componentData$.pipe(
    select(draggingTestCaseSelector)
  );

  /**
   * Does a step is actually in creation ? If true, all other step drag and drop controls and buttons must be disabled.
   */
  public readonly stepCreationMode$: Observable<boolean> = this.componentData$.pipe(
    select(stepCreationModeSelector)
  );

  /**
   * Number of real steps aka no creation form
   */
  public readonly stepCount$: Observable<number> = this.componentData$.pipe(
    select(stepCountSelector)
  );

  public readonly coveragesCount$: Observable<number> = this.componentData$.pipe(
    select(coveragesCountSelect)
  );

  public readonly datasetCount$: Observable<number> = this.componentData$.pipe(
    select(datasetCountSelect)
  );

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected testCaseService: TestCaseService,
              protected customFieldValueService: CustomFieldValueService,
              protected testStepViewService: TestStepViewService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              private coverageGrid: GridService,
              private datasetsTable: GridService,
              private calledTestCaseGrid: GridService
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
      storeOptions
    );
  }

  load(id: number, backUrl: string): void {
    this.restService.getWithoutErrorHandling<TestCaseModel>(['test-case-view', id.toString()]).pipe(
      withLatestFrom(this.referentialDataService.projectDatas$)
    ).subscribe(([testCaseModel, projectDataMap]: [TestCaseModel, ProjectDataMap]) => {
      this.initializeTestCase(testCaseModel, backUrl, projectDataMap);
      this.initializeCoverageGrid();
      this.initializeCalledTestCaseGrid();
    }, err => this.notifyEntityNotFound(err));
  }


  complete() {
    super.complete();
    this.coverageGrid.complete();
    this.datasetsTable.complete();
    this.calledTestCaseGrid.complete();
  }

  private initializeCoverageGrid() {
    const coverage$ = this.componentData$.pipe(
      select(coveragesSelector)
    );
    this.coverageGrid.connectToDatasource(coverage$, 'requirementVersionId');
    this.componentData$.pipe(take(1)).subscribe(componentData => {

      const milestoneFeatureEnabled = componentData.globalConfiguration.milestoneFeatureEnabled;
      this.coverageGrid.setColumnVisibility('milestoneIds', milestoneFeatureEnabled);
      const canEdit = componentData.permissions.canLink;
      this.coverageGrid.setColumnVisibility('delete', canEdit);
    });

  }

  private initializeCalledTestCaseGrid() {
    const calledTestCase$ = this.componentData$.pipe(
      select(calledTestCasesSelector)
    );
    this.calledTestCaseGrid.connectToDatasource(calledTestCase$, 'id');
  }

  public initializeTestCase(testCaseModel: TestCaseModel, backUrl: string, projectDataMap: ProjectDataMap) {
    const attachmentEntityState = this.initializeAttachmentState(testCaseModel.attachmentList.attachments);
    // tslint:disable-next-line:max-line-length
    const testSteps: TestStepsState = this.testStepViewService.initializeStepState(testCaseModel.testSteps, testCaseModel.kind, projectDataMap);
    const customFieldValueState = this.initializeCustomFieldValueState(testCaseModel.customFieldValues);
    const coverageState: CoverageState = this.initializeCoverageState(testCaseModel.coverages);
    const executionState: ExecutionState = this.initializeExecutionState(testCaseModel.executions);
    const calledTestCaseState: CalledTestCaseState = this.initializeCalledTestCaseState(testCaseModel.calledTestCases);
    const parameterState: ParameterState = this.initializeParameterState(testCaseModel.parameters);
    const datasetState: DataSetState = this.initializeDatasetState(testCaseModel.datasets);
    const datasetParamValueState: DatasetParamValueState = this.initializeDatasetParamValueState(testCaseModel.datasetParamValues);
    const entityState: TestCaseState = {
      ...testCaseModel,
      testSteps,
      attachmentList: {id: testCaseModel.attachmentList.id, attachments: attachmentEntityState},
      customFieldValues: customFieldValueState,
      coverages: coverageState,
      executions: executionState,
      uiState: {openTestCaseTreePicker: false, openRequirementTreePicker: false},
      calledTestCases: calledTestCaseState,
      parameters: parameterState,
      datasets: datasetState,
      datasetParamValues: datasetParamValueState,
      navigationState: {backUrl}
    };
    this.initializeEntityState(entityState);
  }

  updateTestCaseImportanceAuto(testCaseId: number, auto: boolean) {
    const update = {};
    update['importanceAuto'] = auto;

    this.restService.post<any>([`test-case/${testCaseId}/importance-auto`], update).pipe(
      withLatestFrom(this.store.state$)
    ).subscribe(([result, state]: [any, TestCaseViewState]) => {
      const nextState = {...state, testCase: {...state.testCase, importanceAuto: auto, importance: result.importance}};
      this.requireExternalUpdate(state.testCase.id, 'importanceAuto', auto);
      this.store.commit(nextState);
    });
  }


  unbindMilestone(testCaseId: number, milestoneId: number) {
    this.restService.delete<void>([`test-case/${testCaseId}/milestones/${[milestoneId]}`]).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]: [void, TestCaseViewState]) => {
        return unbindMilestoneToTC(state, milestoneId);
      })
    ).subscribe(state => {
      this.store.commit(state);
      this.updateLastModification();
    });
  }

  bindMilestones(testCaseId: number, milestoneIds: number[]) {
    this.restService.post<void>([`test-case/${testCaseId}/milestones/${milestoneIds}`]).pipe(
      withLatestFrom(this.store.state$, this.componentData$),
      map(([, state, componentData]: [void, TestCaseViewState, TestCaseViewComponentData]) => {
        return bindMilestonesToTC(componentData.projectData.milestones, state, milestoneIds);
      })
    ).subscribe(state => {
      this.store.commit(state);
      this.updateLastModification();
    });
  }

  updateAutomatable(testCaseId: number, automatable) {
    const patch = {};
    patch['automatable'] = automatable;
    this.restService.post<AutomationRequest>([`test-case/${testCaseId}/automatable`], patch).pipe(
      withLatestFrom(this.store.state$),
      map(([automRequest, state]: [AutomationRequest, TestCaseViewState]) => {
        return {
          ...state,
          testCase: {...state.testCase, automatable: automatable, automationRequest: automRequest}
        };
      })
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  transmit(testCaseId: number): Observable<any> {
    return this.updateAutomationRequestStatus(testCaseId, AutomationRequestStatus.TRANSMITTED.id);
  }

  updateAutomationRequestStatus(testCaseId: number, value): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state) => {
        const urlParts = [`automation-requests/${testCaseId}/status`];
        return this.restService.post<TestCaseModel[]>(urlParts, value);
      }),
      withLatestFrom(this.store.state$),
      map(([response, state]: [TestCaseModel[], TestCaseViewState]) => {
        const updatedTestCase = response[0];
        return {
          ...state,
          testCase: {
            ...state.testCase, automationRequest: {
              ...state.testCase.automationRequest,
              ...updatedTestCase.automationRequest
            }
          }
        };
      }),
      map((state) => this.store.commit(state)),
    );
  }

  updateAutomationRequestPriority(testCaseId: number, priority) {
    // In case the provided input is not an integer
    priority = priority === '' ? null : Math.floor(priority);

    this.restService.post<void>([`test-case/${testCaseId}/automation-request/priority`], {priority}).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]: [void, TestCaseViewState]) => {
        return {
          ...state,
          testCase: {...state.testCase, automationRequest: {...state.testCase.automationRequest, priority: priority}}
        };
      })
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  getDatasetByTestCaseId(testCaseId: any): Observable<Dataset[]> {
    return this.restService.get<Dataset[]>(['test-cases', testCaseId, 'datasets']);
  }

  getInitialState(): TestCaseViewState {
    return provideInitialTestCaseView();
  }

  addSimplePermissions(projectData: ProjectData): TestCasePermissions {
    return new TestCasePermissions(projectData);
  }

  changeAction(id: number, action: string): Observable<any> {
    return this.testStepViewService.writeAction(id, action).pipe(
      withLatestFrom(this.store.state$),
      map(([response, state]: [TestCaseParameterOperationReport, TestCaseViewState]) => this.updateParameterState(response, state)),
      map(state => this.testStepViewService.updateAction(id, action, state)),
      tap(state => this.commit(state)),
    );
  }

  changeExpectedResult(id: number, expectedResult: string): Observable<any> {
    return this.testStepViewService.writeExpectedResult(id, expectedResult).pipe(
      withLatestFrom(this.store.state$),
      map(([response, state]: [TestCaseParameterOperationReport, TestCaseViewState]) => this.updateParameterState(response, state)),
      map((state) => this.testStepViewService.updateExpectedResult(id, expectedResult, state)),
      tap(state => this.commit(state)),
    );
  }

  changeKeyword(id: number, keyword: string): Observable<any> {
    return this.testStepViewService.changeKeyword(id, keyword).pipe(
      withLatestFrom(this.store.state$),
      map(([response, state]) => this.testStepViewService.updateKeyword(id, keyword, state)),
      tap(state => this.commit(state))
    );
  }

  changeActionWord(id: number, newActionWord: string): Observable<TestCaseViewState> {
    return this.testStepViewService.changeActionWord(id, newActionWord).pipe(
      withLatestFrom(this.store.state$),
      map(([response, state]: [TestStepActionWordOperationReport, TestCaseViewState]) =>
        [response, this.updateParameterState(response.paramOperationReport, state)]),
      map(([response, state]: [TestStepActionWordOperationReport, TestCaseViewState]) =>
        this.testStepViewService.updateActionWord(id, response.action, response.styledAction, response.actionWordId, state)),
      tap(state => this.commit(state))
    );
  }

  changeActionWordWithId(id: number, newActionWord: string, actionWordId: number): Observable<TestCaseViewState> {
    return this.testStepViewService.changeActionWordWithId(id, newActionWord, actionWordId).pipe(
      withLatestFrom(this.store.state$),
      map(([response, state]: [TestStepActionWordOperationReport, TestCaseViewState]) =>
        [response, this.updateParameterState(response.paramOperationReport, state)]),
      map(([response, state]: [TestStepActionWordOperationReport, TestCaseViewState]) =>
        this.testStepViewService.updateActionWord(id, response.action, response.styledAction, response.actionWordId, state)),
      tap(state => this.commit(state))
    );
  }

  changeDatatable(id: number, newDatatable: string): Observable<TestCaseViewState> {
    return this.testStepViewService.changeDatatable(id, newDatatable).pipe(
      withLatestFrom(this.store.state$),
      map(([response, state]) => this.testStepViewService.updateDatatable(id, newDatatable, state)),
      tap(state => this.commit(state)));
  }

  changeDocstring(id: number, newDocstring: string): Observable<TestCaseViewState> {
    return this.testStepViewService.changeDocstring(id, newDocstring).pipe(
      withLatestFrom(this.store.state$),
      map(([response, state]) => this.testStepViewService.updateDocstring(id, newDocstring, state)),
      tap(state => this.commit(state)));
  }

  changeComment(id: number, newComment: string): Observable<TestCaseViewState> {
    return this.testStepViewService.changeComment(id, newComment).pipe(
      withLatestFrom(this.store.state$),
      map(([response, state]) => this.testStepViewService.updateComment(id, newComment, state)),
      tap(state => this.commit(state)));
  }

  private convertStepModels(testStepModels: TestStepModel[]) {
    const steps: TestStepState[] = testStepModels.map(model => {
      if (model.kind === 'action-step') {
        return this.convertActionStepModel(model as ActionStepModel);
      } else {
        const callStepModel: CallStepModel = model as CallStepModel;
        return {...callStepModel, extended: false};
      }
    });
    return steps;
  }

  private convertActionStepModel(model: ActionStepModel): ActionStepState {
    const attachmentEntityState = this.initializeAttachmentState(model.attachmentList.attachments);
    const customFieldValueState = this.initializeCustomFieldValueState(model.customFieldValues);
    return {
      ...model,
      extended: true,
      attachmentList: {id: model.attachmentList.id, attachments: attachmentEntityState},
      customFieldValues: customFieldValueState
    };
  }

  private initializeCoverageState(coverages: RequirementVersionCoverage[]) {
    return coverageEntityAdapter.setAll(coverages, coverageEntityAdapter.getInitialState());
  }

  private initializeExecutionState(executions: Execution[]) {
    return executionEntityAdapter.setAll(executions, executionEntityAdapter.getInitialState());
  }

  private initializeCalledTestCaseState(calledTestCases: CalledTestCase[]) {
    return calledTestCaseEntityAdapter.setAll(calledTestCases, calledTestCaseEntityAdapter.getInitialState());
  }

  private initializeParameterState(parameters: Parameter[]) {
    return parameterEntityAdapter.setAll(parameters, parameterEntityAdapter.getInitialState());
  }

  private initializeDatasetState(datasets: Dataset[]) {
    return datasetEntityAdapter.setAll(datasets, datasetEntityAdapter.getInitialState());
  }

  private initializeDatasetParamValueState(paramValues: DatasetParamValue[]) {
    return datasetParamValueEntityAdapter.setAll(paramValues, datasetParamValueEntityAdapter.getInitialState());
  }

  updateStepCustomFieldValue(stepId: number, cufValueId: number, value: string | string[]) {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.testStepViewService.writeCustomFieldValue(stepId, cufValueId, value, state)),
      withLatestFrom(this.store.state$),
      map(([, initialState]) => this.testStepViewService.updateStepCustomFieldValue(stepId, cufValueId, value, initialState))
    ).subscribe(state => this.commit(state));
  }

  addAttachmentsToStep(files: File[], stepId: number, attachmentListId: number): void {
    if (logger.isDebugEnabled()) {
      logger.debug(`Adding files to step ${stepId} : `, [files]);
    }

    this.state$.pipe(
      take(1),
      switchMap((state: EntityViewState<TestCaseState, 'testCase'>) => {
        const entity = this.getEntity(state);
        return this.attachmentHelper.addAttachments(files, attachmentListId, entity.id, state.type);
      }),
      withLatestFrom(this.store.state$)
    ).subscribe(([event, state]: [UploadAttachmentEvent, TestCaseViewState]) => {
      this.commit(this.testStepViewService.mapUploadEventToState(stepId, state, event));
    });
  }

  markStepAttachmentsToDelete(ids: string[], stepId: number) {
    this.state$.pipe(
      take(1),
      map(state => this.testStepViewService.markStepAttachmentsToDelete(ids, stepId, state)))
      .subscribe(state => this.commit(state));
  }

  cancelStepAttachmentsToDelete(ids: string[], stepId: number) {
    this.state$.pipe(
      take(1),
      map(state => this.testStepViewService.cancelStepAttachmentsToDelete(ids, stepId, state)))
      .subscribe(state => this.commit(state));
  }

  removeStepRejectedAttachments(ids: string[], stepId: number) {
    this.state$.pipe(
      take(1),
      map((state) => this.testStepViewService.removeStepRejectedAttachments(ids, stepId, state)))
      .subscribe(state => this.commit(state));
  }

  deleteStepAttachments(attachmentIds: string[], stepId: number, attachmentListId: number) {
    const idsAsNumber = attachmentIds.map(id => parseInt(id, 10));

    this.state$.pipe(
      take(1),
      switchMap((state: EntityViewState<TestCaseState, 'testCase'>) => {
        const entity = this.getEntity(state);
        return this.attachmentHelper.eraseAttachments(idsAsNumber, attachmentListId, entity.id, state.type);
      }),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.testStepViewService.removeAttachments(attachmentIds, stepId, state))
    ).subscribe((state) => this.commit(state));
  }

  addActionStepForm(index: number) {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) => this.testStepViewService.addActionStepForm(index, state))
    ).subscribe(state => this.commit(state));
  }

  cancelAddTestStep() {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) => this.testStepViewService.cancelAddTestStep(state))
    ).subscribe((state) => this.commit(state));
  }

  confirmAddActionStep(actionStepForm: ActionStepFormModel, testCaseId: number): Observable<any> {
    if (logger.isDebugEnabled()) {
      logger.debug(`Adding step to test case ${testCaseId}`, [actionStepForm]);
    }
    return this.testStepViewService.persistActionStep(actionStepForm, testCaseId).pipe(
      withLatestFrom(this.state$),
      map(([newStep, state]) => {
        const newState = this.testStepViewService.addNewActionStep(newStep.testStepState as ActionStepState, actionStepForm.index, state);
        return this.updateParameterState(newStep.testCaseParameterOperationReport, newState);
      }),
      tap((state) => this.requireTreeRefreshAfterAddingStep(state.testCase)),
      tap((state) => this.commit(state)),
    );
  }

  confirmAddKeywordStep(keywordStepForm: KeywordStepFormModel, testCaseId: number): Observable<any> {
    if (logger.isDebugEnabled()) {
      logger.debug(`Adding keyword step to keyword test case ${testCaseId}`, [keywordStepForm]);
    }
    return this.testStepViewService.persistKeywordStep(keywordStepForm, testCaseId).pipe(
      withLatestFrom(this.state$, this.referentialDataService.projectDatas$),
      map(([newStep, state, projectDataMap]) => {
        const newState = this.testStepViewService.addNewKeywordStep(
          newStep.testStepState as KeywordStepView,
          newStep.testStepState.stepOrder,
          state,
          projectDataMap);
        return this.updateParameterState(newStep.testCaseParameterOperationReport, newState);
      }),
      tap((state) => this.requireTreeRefreshAfterAddingStep(state.testCase)),
      tap((state) => this.commit(state)),
    );
  }

  private requireTreeRefreshAfterAddingStep(testCase: TestCaseState) {
      this.requireExternalUpdate(testCase.id);
  }

  confirmAddAnotherActionStep(actionStepForm: ActionStepFormModel, testCaseId: number): Observable<any> {
    return this.testStepViewService.persistActionStep(actionStepForm, testCaseId).pipe(
      withLatestFrom(this.state$),
      map(([newStep, state]) => {
        const newState = this.testStepViewService.addNewActionStep(
          newStep.testStepState as ActionStepState,
          actionStepForm.index, state);
        return this.updateParameterState(newStep.testCaseParameterOperationReport, newState);
      }),
      tap((state) => this.requireTreeRefreshAfterAddingStep(state.testCase)),
      map((state: TestCaseViewState) => this.testStepViewService.addActionStepForm(actionStepForm.index + 1, state)),
      tap((state) => this.commit(state)),
    );
  }

  deleteStep(stepId: number) {
    this.state$.pipe(
      take(1),
      concatMap(state => this.testStepViewService.eraseSteps(stepId, state)),
      withLatestFrom(this.state$),
      map(([stepDeleteOperation, state]: [
        DeleteTestStepOperationReport, TestCaseViewState]) => {
        const newState = this.testStepViewService.removeSteps(stepDeleteOperation.testStepsToDelete, state);
        return this.updateParameterState(stepDeleteOperation.operationReport, newState);
      }),
      tap((state) => this.requireTreeRefreshAfterRemoveStep(state.testCase)),
      tap(() => this.refreshCoverages()),
    ).subscribe(state => this.commit(state));
  }

  private requireTreeRefreshAfterRemoveStep(testCase: TestCaseState) {
    const stepIds = testCase.testSteps.ids;
    const numberOfSteps = stepIds.length;
    if (testCase.kind === 'STANDARD' && numberOfSteps === 1 && stepIds[0] === createActionStepFormId
      || testCase.kind === 'KEYWORD' && numberOfSteps === 0) {
      this.requireExternalUpdate(testCase.id);
    }
  }

  copySteps(stepId: number) {
    this.state$.pipe(take(1)).subscribe(state => this.testStepViewService.copySteps(stepId, state));
  }

  pasteSteps(targetStepId: number, testCaseId: number, isAssignedToTargetProject?: boolean) {
    this.testStepViewService.persistPastedSteps(targetStepId, testCaseId, isAssignedToTargetProject).pipe(
      withLatestFrom(this.state$, this.referentialDataService.projectDatas$),
      map(([operationReport, state, projectDataMap]: [PasteTestStepOperationReport, TestCaseViewState, ProjectDataMap]) => {
        const updatedState = this.testStepViewService.addPastedStep(targetStepId, operationReport, state, projectDataMap);
        return this.updateParameterState(operationReport.operationReport, updatedState);
      }),
      tap((state) => this.requireTreeRefreshAfterAddingStep(state.testCase)),
    ).subscribe(state => this.commit(state));
  }

  compareKeywordProjectsIds(testCaseId: number): Observable<boolean> {
    return this.testStepViewService.compareKeywordProjectsIds(testCaseId);
  }

  selectStep(stepId: number) {
    this.state$.pipe(
      take(1),
      map(state => this.testStepViewService.selectStep(stepId, state))
    ).subscribe(state => this.commit(state));
  }

  toggleStepSelection(stepId: number) {
    this.state$.pipe(
      take(1),
      map(state => this.testStepViewService.toggleStepSelection(stepId, state))
    ).subscribe(state => this.commit(state));
  }

  extendStepSelection(selectedStepId: number) {
    this.state$.pipe(
      take(1),
      map(state => this.testStepViewService.extendStepSelection(selectedStepId, state))
    ).subscribe(state => this.commit(state));
  }

  startDraggingSteps(draggedStepIds: number[]) {
    this.state$.pipe(
      take(1),
      map((state: Readonly<TestCaseViewState>) => this.testStepViewService.startDraggingStep(draggedStepIds, state))
    ).subscribe(state => this.commit(state));
  }

  dragOverStep(targetId?: number) {
    this.state$.pipe(
      take(1),
      filter(state => state.testCase.testSteps.draggingSteps),
      filter(state => !state.testCase.testSteps.selectedStepIds.includes(targetId)),
      map(state => this.testStepViewService.dragOverStep(targetId, state))
    ).subscribe(state => this.commit(state));
  }

  dropSteps() {
    this.state$.pipe(
      take(1),
      filter(state => state.testCase.testSteps.draggingSteps),
      concatMap(state => this.testStepViewService.writeNewStepPosition(state)),
      withLatestFrom(this.state$),
      map(([, state]) => this.testStepViewService.dropStep(state))
    ).subscribe(state => this.commit(state));
  }

  dropTestCaseForCall(calledTestCaseIds: number[]): Observable<any> {
    return this.state$.pipe(
      take(1),
      filter(state => state.testCase.testSteps.draggingTestCase),
      // removing any creation step form, in case of test case is empty before drop
      map(state => this.testStepViewService.cancelAddTestStep(state)),
      tap(state => this.commit(state)),
      concatMap(state => this.testStepViewService.writeCallTestCase(calledTestCaseIds, state)),
      withLatestFrom(this.state$, this.referentialDataService.projectDatas$),
      map(([operationReport, state, projectDataMap]: [PasteTestStepOperationReport, TestCaseViewState, ProjectDataMap]) =>
        this.testStepViewService.callTestCase(operationReport, state, projectDataMap)),
      tap((state) => this.commit(state)),
      tap(() => this.refreshCoverages()),
      tap((state) => this.requireTreeRefreshAfterAddingStep(state.testCase)),
    );
  }

  cancelDrag() {
    this.state$.pipe(
      take(1),
      map(state => this.testStepViewService.cancelDrag(state)),
      map(state => {
        if (state.testCase.testSteps.ids.length === 0 && state.testCase.kind === 'STANDARD') {
          return this.testStepViewService.addActionStepForm(0, state);
        } else {
          return state;
        }
      })
    ).subscribe(state => this.commit(state));
  }

  dragOverStepForCall(targetId?: number) {
    this.state$.pipe(
      take(1),
      filter(() => targetId !== callStepDndPlaceholderId),
      map(state => this.testStepViewService.dragOverStepForCall(state, targetId))
    ).subscribe(state => this.commit(state));
  }

  suspendStepDrag() {
    this.state$.pipe(
      take(1),
      filter(state => state.testCase.testSteps.draggingSteps),
      map(state => this.testStepViewService.suspendStepDrag(state))
    ).subscribe(state => this.commit(state));
  }

  suspendTestCaseDrag() {
    this.state$.pipe(
      take(1),
      filter(state => state.testCase.testSteps.draggingTestCase),
      map(state => this.testStepViewService.suspendTestCaseDrag(state))
    ).subscribe(state => this.commit(state));
  }

  collapseAllSteps() {
    this.state$.pipe(
      take(1),
      map((state) => this.testStepViewService.collapseAllSteps(state)),
    ).subscribe((state: TestCaseViewState) => this.commit(state));
  }

  expendAllSteps() {
    this.state$.pipe(
      take(1),
      map((state) => this.testStepViewService.expendAllSteps(state)),
    ).subscribe(state => this.commit(state));
  }

  toggleStep(id: number) {
    this.state$.pipe(
      take(1),
      map((state) => this.testStepViewService.toggleStep(id, state)),
    ).subscribe(state => this.commit(state));
  }

  moveStepUp(stepId: number) {
    this.state$.pipe(
      take(1),
      filter((state) => {
        const ids = state.testCase.testSteps.ids as number[];
        const index = ids.indexOf(stepId);
        return index > 0;
      }),
      concatMap((state) => {
        const ids = state.testCase.testSteps.ids as number[];
        const dropIndex = ids.indexOf(stepId) - 1;
        return this.testStepViewService.writeStepMovement(stepId, dropIndex, state);
      }),
      withLatestFrom(this.state$),
      map(([, state]) => {
        const ids = state.testCase.testSteps.ids as number[];
        const dropIndex = ids.indexOf(stepId) - 1;
        return this.testStepViewService.moveStep(stepId, dropIndex, state);
      })
    ).subscribe(state => this.commit(state));
  }

  moveStepDown(stepId: number) {
    this.state$.pipe(
      take(1),
      filter((state) => {
        const ids = state.testCase.testSteps.ids as number[];
        const index = ids.indexOf(stepId);
        return index < ids.length - 1;
      }),
      concatMap((state) => {
        const ids = state.testCase.testSteps.ids as number[];
        const dropIndex = ids.indexOf(stepId) + 1;
        return this.testStepViewService.writeStepMovement(stepId, dropIndex, state);
      }),
      withLatestFrom(this.state$),
      map(([, state]) => {
        const ids = state.testCase.testSteps.ids as number[];
        const dropIndex = ids.indexOf(stepId) + 1;
        return this.testStepViewService.moveStep(stepId, dropIndex, state);
      })
    ).subscribe(state => this.commit(state));
  }

  togglePrerequisite() {
    this.state$.pipe(
      take(1),
      map((state) => this.testStepViewService.togglePrerequisite(state)),
    ).subscribe(state => this.commit(state));
  }

  addCoverages(requirementIds: number[]): Observable<ChangeCoverageOperationReport> {
    return this.state$.pipe(
      take(1),
      tap(() => this.coverageGrid.beginAsyncOperation()),
      concatMap((state: TestCaseViewState) => this.testCaseService.persistCoverages(state.testCase.id, requirementIds)),
      withLatestFrom(this.state$),
      tap(([operationReport, state]: [ChangeCoverageOperationReport, TestCaseViewState]) =>
        this.requireTreeRefreshAfterCoverageOperation(state.testCase)),
      map(([operationReport, state]: [ChangeCoverageOperationReport, TestCaseViewState]) => {
        const coverages = coverageEntityAdapter.setAll(operationReport.coverages, state.testCase.coverages);
        return [operationReport, {...state, testCase: {...state.testCase, coverages}}];
      }),
      tap(() => this.coverageGrid.completeAsyncOperation()),
      map(([operationReport, state]: [ChangeCoverageOperationReport, TestCaseViewState]) => {
        this.commit(state);
        return operationReport;
      })
    );
  }

  refreshCoverages(): void {
    this.state$.pipe(
      take(1),
      tap(() => this.coverageGrid.beginAsyncOperation()),
      concatMap((state: TestCaseViewState) => this.testCaseService.getCoverages(state.testCase.id)),
      withLatestFrom(this.state$),
      map(([updatedCoverages, state]: [RequirementVersionCoverage[], TestCaseViewState]) => {
        const coverages = coverageEntityAdapter.setAll(updatedCoverages, state.testCase.coverages);
        return {...state, testCase: {...state.testCase, coverages}};
      }),
      tap(() => this.coverageGrid.completeAsyncOperation()),
      tap((state: TestCaseViewState) => this.commit(state))
    ).subscribe();
  }

  deleteCoverages() {
    this.state$.pipe(
      take(1),
      withLatestFrom(this.coverageGrid.selectedRowIds$),
      tap(() => this.coverageGrid.beginAsyncOperation()),
      concatMap(([state, rowIds]: [TestCaseViewState, Identifier[]]) => {
        return this.testCaseService.eraseCoverages(state.testCase.id, rowIds as number[]);
      }),
      withLatestFrom(this.state$),
      map(([operationReport, state]: [ChangeCoverageOperationReport, TestCaseViewState]) => {
        const coverages = coverageEntityAdapter.setAll(operationReport.coverages, state.testCase.coverages);
        return {...state, testCase: {...state.testCase, coverages}};
      }),
      tap((state) => this.requireTreeRefreshAfterCoverageOperation(state.testCase)),
      tap(() => this.coverageGrid.completeAsyncOperation()),
    ).subscribe((state) => this.commit(state));
  }

  deleteCoverage(id: number) {
    this.state$.pipe(
      take(1),
      tap(() => this.coverageGrid.beginAsyncOperation()),
      concatMap((state: TestCaseViewState) => {
        return this.testCaseService.eraseCoverages(state.testCase.id, [id]);
      }),
      withLatestFrom(this.state$),
      map(([operationReport, state]: [ChangeCoverageOperationReport, TestCaseViewState]) => {
        const coverages = coverageEntityAdapter.setAll(operationReport.coverages, state.testCase.coverages);
        return {...state, testCase: {...state.testCase, coverages}};
      }),
      tap((state) => this.requireTreeRefreshAfterCoverageOperation(state.testCase)),
      tap(() => this.coverageGrid.completeAsyncOperation()),
    ).subscribe((state) => this.commit(state));
  }

  dragRequirementOverStep(stepId: number) {
    this.state$.pipe(
      take(1),
      map(state => this.testStepViewService.dragRequirementOverStep(stepId, state))
    ).subscribe(state => this.commit(state));
  }

  openRequirementTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) =>
        ({
          ...state,
          testCase: {...state.testCase, uiState: {...state.testCase.uiState, openRequirementTreePicker: true}}
        })
      )
    ).subscribe(state => this.commit(state));
  }

  closeRequirementTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) =>
        ({
          ...state,
          testCase: {...state.testCase, uiState: {...state.testCase.uiState, openRequirementTreePicker: false}}
        })
      )
    ).subscribe(state => this.commit(state));
  }

  dropRequirementForCoverageInStep(requirementIds: number[]): Observable<ChangeCoverageOperationReport> {
    return this.state$.pipe(
      take(1),
      filter(state => this.isValidCoverageTarget(state)),
      concatMap((state: TestCaseViewState) =>
        this.testStepViewService.persistStepCoverages(requirementIds, state.testCase.testSteps.currentDndTargetId, state.testCase.id)),
      withLatestFrom(this.state$),
      tap(([operationReport, state]: [ChangeCoverageOperationReport, TestCaseViewState]) =>
        this.requireTreeRefreshAfterCoverageOperation(state.testCase)),
      map(([operationReport, state]: [ChangeCoverageOperationReport, TestCaseViewState]) =>
        [operationReport, this.testStepViewService.dropRequirementForStepCoverages(operationReport, state)]),
      map(([operationReport, state]: [ChangeCoverageOperationReport, TestCaseViewState]) => {
        this.commit(state);
        return operationReport;
      })
    );
  }

  private isValidCoverageTarget(state: TestCaseViewState): boolean {
    const currentDndTargetId = state.testCase.testSteps.currentDndTargetId;
    if (Boolean(currentDndTargetId)) {
      const step = state.testCase.testSteps.entities[currentDndTargetId];
      return step.kind === 'action-step';
    }
    return false;
  }

  deleteStepCoverages(requirementVersionIds: number[], stepId: number) {
    this.state$.pipe(
      take(1),
      concatMap((state: TestCaseViewState) =>
        this.testStepViewService.eraseStepCoverages(state.testCase.id, stepId, requirementVersionIds)),
      withLatestFrom(this.state$),
      map(([operationReport, state]: [ChangeCoverageOperationReport, TestCaseViewState]) =>
        this.testStepViewService.deleteStepCoverage(operationReport, state)),
      tap((state) => this.requireTreeRefreshAfterCoverageOperation(state.testCase)),
    ).subscribe(state => this.commit(state));
  }


  openTestCaseTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) =>
        ({
          ...state,
          testCase: {...state.testCase, uiState: {...state.testCase.uiState, openTestCaseTreePicker: true}}
        })
      )
    ).subscribe(state => {
      return this.commit(state);
    });
  }

  closeTestCaseTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) =>
        ({
          ...state,
          testCase: {...state.testCase, uiState: {...state.testCase.uiState, openTestCaseTreePicker: false}}
        })
      )
    ).subscribe(state => this.commit(state));
  }

  updateStepViewScroll(scrollTop: number) {
    this.state$.pipe(
      take(1),
      map(state => this.testStepViewService.updateStepViewScroll(scrollTop, state))
    ).subscribe(state => this.commit(state));
  }

  addParameter(operationParamDataSet: TestCaseParameterOperationReport) {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) => this.updateParameterState(operationParamDataSet, state)),
      tap(state => this.store.commit(state))
    ).subscribe();
  }

  addDataset(operationParamDataSet: TestCaseParameterOperationReport) {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) => this.updateParameterState(operationParamDataSet, state))
    ).subscribe(state => this.store.commit(state));
  }

  removeDataSet(id) {
    this.state$.pipe(
      take(1),
      tap(() => this.datasetsTable.beginAsyncOperation()),
      concatMap((state: TestCaseViewState) => this.restService.delete([`datasets/${id}`])),
      withLatestFrom(this.state$),
      map(([, state]: [void, TestCaseViewState]) => {
        const datasets = datasetEntityAdapter.removeOne(id, state.testCase.datasets);
        return {...state, testCase: {...state.testCase, datasets}};
      }),
      tap(state => this.store.commit(state))
    ).subscribe(() => this.datasetsTable.completeAsyncOperation());
  }

  updateParameterState(operationParamDataSet: TestCaseParameterOperationReport, state: TestCaseViewState) {
    const datasets = datasetEntityAdapter.setAll(operationParamDataSet.dataSets, state.testCase.datasets);
    const parameters = parameterEntityAdapter.setAll(operationParamDataSet.parameters, state.testCase.parameters);
    const datasetParamValues = datasetParamValueEntityAdapter
      .setAll(operationParamDataSet.paramValues, state.testCase.datasetParamValues);
    return {...state, testCase: {...state.testCase, datasets, parameters, datasetParamValues}};
  }

  paramIsUsed(paramId: string): Observable<boolean> {
    return this.state$.pipe(
      take(1),
      switchMap(state => this.restService.get([`parameters/${paramId}/used`]).pipe(
        map(response => response['PARAMETER_USED'])
      ))
    );
  }

  removeParameter(paramId: string) {
    this.state$.pipe(
      take(1),
      concatMap((state: TestCaseViewState) => this.restService.delete([`parameters/${paramId}`])),
      withLatestFrom(this.state$),
      map(([, state]: [void, TestCaseViewState]) => {
        const parameters = parameterEntityAdapter.removeOne(paramId, state.testCase.parameters);
        return {...state, testCase: {...state.testCase, parameters}};
      })
    ).subscribe(state => this.store.commit(state));
  }

  renameDataset(patch: DatasetPatch) {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) => {
        const datasets = datasetEntityAdapter.updateOne({
          id: patch.id,
          changes: {name: patch.name}
        }, state.testCase.datasets);
        return {...state, testCase: {...state.testCase, datasets}};
      })
    ).subscribe(state => this.store.commit(state));
  }

  updateParamValueData(patch: DatasetParamValuePatch) {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) => {
        const datasetParamValues = datasetParamValueEntityAdapter.updateOne({
          id: patch.id,
          changes: {value: patch.value}
        }, state.testCase.datasetParamValues);
        return {...state, testCase: {...state.testCase, datasetParamValues}};
      })
    ).subscribe(state => this.store.commit(state));
  }

  renameParameter(patch: ParameterPatch, testSteps: TestStepModel[], prerequisite: string) {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) => {
        const parameters = parameterEntityAdapter.updateOne({
          id: patch.id, changes: {name: patch.name}
        }, state.testCase.parameters);
        return {...state, testCase: {...state.testCase, parameters}};
      }),
      withLatestFrom(this.referentialDataService.projectDatas$),
      map(([state, projectDataMap]) => this.updateTestCaseStep(testSteps, state, projectDataMap, prerequisite))
    ).subscribe(state => this.store.commit(state));
  }


  changeParameterDescription(patch: ParameterPatch) {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) => {
        const parameters = parameterEntityAdapter.updateOne({
          id: patch.id,
          changes: {description: patch.description}
        }, state.testCase.parameters);
        return {...state, testCase: {...state.testCase, parameters}};
      })
    ).subscribe(state => this.store.commit(state));
  }

  changePrerequisite(prerequisiteValue: string): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap(state => this.restService.post<TestCaseParameterOperationReport>([`test-case/${state.testCase.id}/prerequisite`],
        {prerequisite: prerequisiteValue})),
      withLatestFrom(this.state$),
      map(([operation, state]) => this.updateParameterState(operation, state)),
      map(state => ({...state, testCase: {...state.testCase, prerequisite: prerequisiteValue}})),
      tap(state => this.store.commit(state))
    );
  }

  updateParameterAssignationMode(stepId: number, parameterAssignationMode: ParameterAssignationUpdate) {
    this.state$.pipe(
      take(1),
      concatMap(state => {
          const mode = this.testStepViewService.extractParameterMode(parameterAssignationMode);
          return this.testStepViewService.updateParameterAssignationMode(state.testCase.id, stepId, parameterAssignationMode, mode)
            .pipe(
              map(report => {
                const newState = this.testStepViewService.updateCallStepParameter(stepId,
                  parameterAssignationMode.datasetId,
                  parameterAssignationMode.datasetName,
                  parameterAssignationMode.delegateParam,
                  state);
                return this.updateParameterState(report, newState);
              })
            );
        }
      )
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  toggleRequirementTreePickerMessageVisibility(visible: boolean) {
    this.state$.pipe(
      take(1),
      map((state: TestCaseViewState) =>
        ({
          ...state,
          testCase: {
            ...state.testCase,
            uiState: {...state.testCase.uiState, requirementTreePickerMessageVisible: visible}
          }
        })
      )
    ).subscribe(state => this.commit(state));
  }

  private requireTreeRefreshAfterCoverageOperation(testCase: TestCaseState) {
    if (testCase.coverages.ids.length === 0) {
      this.requireExternalUpdate(testCase.id);
    }
  }

  updateTaTest(taTest: string): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap(state => this.restService.post<TestAutomation>([`test-case/${state.testCase.id}/test-automation/tests`], {path: taTest})),
      withLatestFrom(this.state$),
      map(([testAutomation, state]) => {
        return {...state, testCase: {...state.testCase, automatedTest: testAutomation}};
      }),
      tap(state => this.store.commit(state)));
  }

  updateScript(script: string) {
    this.state$.pipe(
      take(1),
      concatMap(state => this.testCaseService.updateScript(state.testCase.id, script)),
      withLatestFrom(this.state$),
      map(([, state]: [void, TestCaseViewState]) => {
        return {...state, testCase: {...state.testCase, script: script}};
      })
    ).subscribe(state => this.store.commit(state));
  }

  validateScript(script: string): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap(state => this.testCaseService.validateScript(state.testCase.id, script))
    );
  }

  getMatchingActionWords(searchInput: string, selectedProjectsIds: number[]): Observable<string[]> {
    return this.state$.pipe(
      take(1),
      switchMap((state: TestCaseViewState) =>
        this.testCaseService.getMatchingActionWord(state.testCase.projectId, searchInput, selectedProjectsIds))
    );
  }

  getDuplicateActionWords(searchInput: string): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state: TestCaseViewState) =>
        this.testCaseService.getDuplicateActionWords(state.testCase.projectId, searchInput))
    );
  }

  getScriptPreview(): Observable<string> {
    return this.state$.pipe(
      take(1),
      switchMap((state: TestCaseViewState) =>
        this.testCaseService.getScriptPreview(state.testCase.id))
    );
  }

  changeAutomatedTestTechnology(technologyId: number | null): void {
    this.update('automatedTestTechnology', technologyId);
  }

  private updateTestCaseStep(testSteps: TestStepModel[], state: any, projectDataMap: any, prerequisite: string) {
    const testStepsState = this.testStepViewService.convertStepModels(testSteps, projectDataMap);
    const newTestSteps = stepsEntityAdapter.setAll(testStepsState, state.testCase.testSteps);
    return {...state, testCase: {...state.testCase, testSteps: newTestSteps, prerequisite: prerequisite}};
  }
}


export function bindMilestonesToTC(projectMilestones: Milestone[], state: TestCaseViewState, milestoneIds: number[]) {
  projectMilestones = projectMilestones.filter(milestone => milestoneIds.find(id => id === milestone.id));
  const testCaseMilestones = state.testCase.milestones;
  projectMilestones.forEach(milestoneView => {
    const milestone: Milestone = {...milestoneView};
    testCaseMilestones.push(milestone);
  });
  return {
    ...state, testCase: {
      ...state.testCase, milestones: testCaseMilestones
    }
  };
}

export function unbindMilestoneToTC(state: TestCaseViewState, milestoneId: number) {
  return {
    ...state,
    testCase: {
      ...state.testCase,
      milestones: state.testCase.milestones.filter(milestone => milestone.id !== milestoneId)
    }
  };
}

export interface DatasetPatch {
  id;
  name: string;
}

export interface DatasetParamValuePatch {
  id;
  value: string;
}

export interface ParameterPatch {
  id;
  name?: string;
  description?: string;
}

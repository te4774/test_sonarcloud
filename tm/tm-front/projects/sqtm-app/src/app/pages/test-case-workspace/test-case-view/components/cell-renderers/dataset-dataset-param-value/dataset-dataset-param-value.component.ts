import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewContainerRef,} from '@angular/core';
import {ColumnDefinitionBuilder, DialogService, GridService, RestService, TableValueChange} from 'sqtm-core';
import {Validators} from '@angular/forms';
import {TestCaseViewService} from '../../../service/test-case-view.service';
import {AbstractDatasetCellComponent} from '../abstract-dataset-cell/abstract-dataset-cell.component';

@Component({
  selector: 'sqtm-app-dataset-dataset-param-value',
  template: `

    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column p-l-5">
        <sqtm-core-editable-text-field style="margin: auto 0 auto 0" [layout]="'no-buttons'"
                                       [size]="'small'"
                                       [value]="getValue()"
                                       [validators]="getValidators()"
                                       [editable]="editable"
                                       [showPlaceHolder]="false"
                                       [displayInGrid]="true"
                                       (confirmEvent)="updateAndClose($event)"
                                       (validatorErrorEvent)="formErrors($event)"
                                       #editableTextField ngDefaultControl></sqtm-core-editable-text-field>
      </div>
    </ng-container>
  `,
  styleUrls: ['./dataset-dataset-param-value.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetDatasetParamValueComponent extends AbstractDatasetCellComponent implements OnInit {

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              protected dialogService: DialogService,
              protected vcr: ViewContainerRef,
              private restService: RestService,
              private testCaseViewService: TestCaseViewService
  ) {
    super(grid, cdRef, dialogService, vcr);
  }

  ngOnInit() {
  }

  updateAndClose(currentValue: string) {

    const datasetParamValuePatch = {
      id: this.row.data[this.columnDisplay.id].id,
      value: currentValue
    };

    this.restService.post(['dataset-param-values'], datasetParamValuePatch)
      .subscribe(() => {
        this.testCaseViewService.updateParamValueData(datasetParamValuePatch);
        const changedDatasetParamValue = {...this.row.data[this.columnDisplay.id], value: currentValue};
        const changedValue: TableValueChange = {columnId: this.columnDisplay.id, value: changedDatasetParamValue};
        this.grid.editRows([this.row.id], [changedValue]);
      });
  }

  getValidators() {
    return [Validators.maxLength(1024)];
  }

  getValue() {
    return this.row.data[this.columnDisplay.id].value;
  }
}

export function datasetDatasetParamValueColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DatasetDatasetParamValueComponent);
}

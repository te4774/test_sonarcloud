import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService,} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-called-test-case-dataset-name',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
          <span style="margin: auto 0;" class="sqtm-grid-cell-txt-renderer"
                [ngClass]="textClass"
                [class.disabled-row]="row.disabled"
                [class.show-as-filtered-parent]="showAsFilteredParent"
                [sqtmCoreLabelTooltip]="getToolTipText()"
                nz-tooltip [nzTooltipTitle]="" [nzTooltipPlacement]="'topLeft'">
          {{getText()}}
        </span>
      </div>
    </ng-container>`,
  styleUrls: ['./called-test-case-dataset-name.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalledTestCaseDatasetNameComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              private translateService: TranslateService) {
    super(grid, cdRef);
  }

  ngOnInit(): void {
  }

  get textClass(): string {
    return 'align-' + this.columnDisplay.contentPosition;
  }

  getToolTipText() {
    const hasDelegatedParameter = this.row.data['hasDelegatedParameter'];
    if (hasDelegatedParameter) {
      return this.translateService.instant('sqtm-core.entity.call-step.dataset.delegate-no-parenthesis');
    } else {
      return this.row.data[this.columnDisplay.id];
    }
  }

  getText() {
    const value = this.row.data[this.columnDisplay.id];
    const hasDelegatedParameter = this.row.data['hasDelegatedParameter'];

    if (hasDelegatedParameter) {
      return this.translateService.instant('sqtm-core.entity.call-step.dataset.delegate-no-parenthesis');
    } else if (value == null || value === '') {
      return '-';
    } else {
      return value;
    }
  }

}

export function calledTestCaseDatasetNameColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(CalledTestCaseDatasetNameComponent);
}

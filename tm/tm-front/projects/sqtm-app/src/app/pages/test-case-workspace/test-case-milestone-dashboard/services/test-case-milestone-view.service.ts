import {Injectable, OnDestroy} from '@angular/core';
import {
  createStore,
  CustomDashboardModel,
  EntityScope,
  FavoriteDashboardValue,
  PartyPreferencesService,
  ProjectData,
  ProjectReference,
  ReferentialDataService,
  RestService,
  TestCaseStatistics
} from 'sqtm-core';
import {initialTestCaseMilestoneViewState, TestCaseMilestoneViewState} from '../state/test-case-milestone-view-state';
import {concatMap, filter, map, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';

@Injectable()
export class TestCaseMilestoneViewService implements OnDestroy {

  private readonly store = createStore<TestCaseMilestoneViewState>(initialTestCaseMilestoneViewState());

  public componentData$: Observable<Readonly<TestCaseMilestoneViewState>> = this.store.state$.pipe(
    filter(state => Boolean(state.milestone))
  );

  private unsub$ = new Subject<void>();

  constructor(private restService: RestService,
              private referentialDataService: ReferentialDataService,
              private partyPreferencesService: PartyPreferencesService) {
    this.observeMilestoneModeData();
  }

  observeMilestoneModeData() {
    this.referentialDataService.selectedMilestoneId$.pipe(
        takeUntil(this.unsub$),
        tap(() => this.init())
    ).subscribe();
  }

  init() {
    this.restService.get<MilestoneTestCaseDashboard>(['test-case-milestone-dashboard']).pipe(
      withLatestFrom(this.referentialDataService.milestoneModeData$),
      map(([statistics, milestoneModeData]) => {
        const state: TestCaseMilestoneViewState = {
          ...statistics,
          milestone: {...milestoneModeData.selectedMilestone},
          generatedDashboardOn: new Date(),
          scope: []
        };
        return state;
      }),
      concatMap((state: TestCaseMilestoneViewState) => {
        return this.referentialDataService.findMilestonePerimeter(state.milestone.id).pipe(
          take(1),
          map(projects => ([state, projects]))
        );
      }),
      map(([state, projects]: [TestCaseMilestoneViewState, ProjectData[]]) => {
        const scope: EntityScope[] = projects.map(project => ({
          id: new ProjectReference(project.id).asString(),
          label: project.name,
          projectId: project.id
        }));
        return {...state, scope};
      })
    ).subscribe((state: TestCaseMilestoneViewState) => this.store.commit(state));
  }

  complete() {
    this.store.complete();
  }

  refreshStatistics() {
    this.restService.get<TestCaseStatistics>(['test-case-milestone-dashboard']).pipe(
      withLatestFrom(this.store.state$),
      map(([statistics, state]) => {
        const nextState: TestCaseMilestoneViewState = {
          ...state,
          ...statistics,
          generatedDashboardOn: new Date()
        };
        return nextState;
      })
    ).subscribe((state: TestCaseMilestoneViewState) => this.store.commit(state));
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue): Observable<void> {
    return this.partyPreferencesService.changeTestCaseWorkspaceFavoriteDashboard(preferenceValue);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export class MilestoneTestCaseDashboard {
  statistics: TestCaseStatistics;
  dashboard: CustomDashboardModel;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}

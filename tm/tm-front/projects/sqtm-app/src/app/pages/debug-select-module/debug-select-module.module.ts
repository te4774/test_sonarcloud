import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DebugPageComponent} from './debug-page/debug-page.component';
import {RouterModule, Routes} from '@angular/router';
import {UiManagerModule, WorkspaceCommonModule} from 'sqtm-core';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {FormsModule} from '@angular/forms';

export const routes: Routes = [
  {
    path: '',
    component: DebugPageComponent
  }];

@NgModule({
  declarations: [
    DebugPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    WorkspaceCommonModule,
    UiManagerModule,
    NzSelectModule,
    FormsModule
  ]
})
export class DebugSelectModuleModule { }

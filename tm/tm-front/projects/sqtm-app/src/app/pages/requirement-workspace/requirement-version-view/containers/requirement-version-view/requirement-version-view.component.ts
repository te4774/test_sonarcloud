import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  Input,
  Renderer2,
  ViewContainerRef
} from '@angular/core';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {
  DialogService,
  DragAndDropService,
  EntityViewComponentData,
  GridPersistenceService,
  ReferentialDataService,
  RequirementPermissions
} from 'sqtm-core';
import {RequirementVersionState} from '../../state/requirement-version-view.state';
import {TranslateService} from '@ngx-translate/core';
import {AbstractRequirementVersionView} from '../abstract-requirement-version-view';
import {Router} from '@angular/router';


@Component({
  selector: 'sqtm-app-requirement-version-view',
  templateUrl: './requirement-version-view.component.html',
  styleUrls: ['./requirement-version-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementVersionViewComponent extends AbstractRequirementVersionView {

  @ContentChild('entityProjection')
  entityProjection: ElementRef;

  @Input()
  showNewVersionCreationMenuItem = true;

  constructor(requirementViewService: RequirementVersionViewService,
              translateService: TranslateService,
              dndService: DragAndDropService,
              renderer: Renderer2,
              referentialDataService: ReferentialDataService,
              dialogService: DialogService,
              vcr: ViewContainerRef,
              router: Router,
              gridPersistenceService: GridPersistenceService) {
    super(requirementViewService, translateService, dndService, renderer, referentialDataService, dialogService, vcr, router, gridPersistenceService);
  }

  showCreateNewVersionMenuItem(): boolean {
    return this.showNewVersionCreationMenuItem;
  }
}

export interface RequirementVersionViewComponentData
  extends EntityViewComponentData<RequirementVersionState, 'requirementVersion', RequirementPermissions> {
}



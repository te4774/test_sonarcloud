import {CustomDashboardModel, RequirementStatistics, SqtmEntityState} from 'sqtm-core';

// tslint:disable-next-line:no-empty-interface
export interface RequirementLibraryState extends SqtmEntityState {
  name: string;
  description: string;
  statistics: RequirementStatistics;
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  extendHighLvlReqScope: boolean;
}

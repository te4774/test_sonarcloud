import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {
  DataRow,
  deleteColumn,
  Extendable,
  GenericDataRow,
  GridDefinition,
  GridService,
  indexColumn,
  levelEnumColumn,
  Limited,
  milestoneLabelColumn,
  ProjectDataMap,
  ReadOnlyPermissions,
  RequirementCriticality,
  RequirementPermissions,
  RequirementStatus,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textCellWithToolTipColumn,
  textColumn,
  withLinkColumn
} from 'sqtm-core';
import {
  RVW_LINKED_LOW_LEVEL_REQUIREMENT_TABLE
} from '../../../../requirement-version-view/requirement-version-view.constant';
import {
  DeleteLinkedLowLvlRequirementComponent
} from '../delete-linked-low-lvl-requirement/delete-linked-low-lvl-requirement.component';

@Component({
  selector: 'sqtm-app-linked-low-lvl-requirement-table',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./linked-low-lvl-requirement-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: RVW_LINKED_LOW_LEVEL_REQUIREMENT_TABLE
    }
  ]
})
export class LinkedLowLvlRequirementTableComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}

export function linkedLowLevelRequirementTableDefinition(): GridDefinition {
  return smallGrid('requirement-version-linked-low-level').withColumns([
    indexColumn()
      .withViewport('leftViewport'),
    textCellWithToolTipColumn('projectName', 'path')
      .changeWidthCalculationStrategy(new Limited(200))
      .withI18nKey('sqtm-core.entity.project.label.singular'),
    textColumn('reference')
      .changeWidthCalculationStrategy(new Limited(120))
      .withI18nKey('sqtm-core.entity.generic.reference.label'),
    withLinkColumn('name', {
      kind: 'link',
      // change once the page for high level requirements level 2 is done.
      baseUrl: '/requirement-workspace/requirement-version/detail',
      columnParamId: 'requirementVersionId'
    })
      .changeWidthCalculationStrategy(new Limited(500))
      .withI18nKey('sqtm-core.entity.requirement.label.singular'),
    milestoneLabelColumn('milestoneLabels')
      .changeWidthCalculationStrategy(new Limited(150)),
    levelEnumColumn('criticality', RequirementCriticality)
      .withTitleI18nKey('sqtm-core.entity.generic.criticality.label')
      .withI18nKey('sqtm-core.entity.generic.criticality.short')
      .isEditable(false)
      .changeWidthCalculationStrategy(new Extendable(40, 0.1)),
    levelEnumColumn('requirementStatus', RequirementStatus)
      .withTitleI18nKey('sqtm-core.entity.generic.status.label')
      .withI18nKey('sqtm-core.entity.generic.status.short')
      .isEditable(false)
      .changeWidthCalculationStrategy(new Extendable(40, 0.1)),
    deleteColumn(DeleteLinkedLowLvlRequirementComponent)
        .withViewport('rightViewport')
  ]).withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withInitialSortedColumns([
        {id: 'criticality', sort: Sort.ASC},
        {id: 'projectName', sort: Sort.ASC},
        {id: 'reference', sort: Sort.ASC},
        {id: 'name', sort: Sort.ASC}
     ])
    .withRowConverter(convertLinkedLowLevelRequirementLiterals)
    .build();
}

export function convertLinkedLowLevelRequirementLiteral(literal: Partial<DataRow>, projectsData: ProjectDataMap): DataRow {
  const dataRow: DataRow = new GenericDataRow();
  dataRow.projectId = literal.projectId;
  attachPermissionsToRow(dataRow, projectsData);
  Object.assign(dataRow, literal);
  const childOfRequirement: boolean = dataRow.data['childOfRequirement'];
  dataRow.disabled = childOfRequirement;
  dataRow.selectable = !childOfRequirement;
  return dataRow;
}

function attachPermissionsToRow(dataRow: DataRow, projectData: ProjectDataMap) {
  if (dataRow.projectId == null) {
    dataRow.simplePermissions = new ReadOnlyPermissions();
    return;
  }

  const project = projectData[dataRow.projectId];

  if (project == null) {
    dataRow.simplePermissions = new ReadOnlyPermissions();
    return;
  }

  dataRow.simplePermissions = new RequirementPermissions(project);
}

export function convertLinkedLowLevelRequirementLiterals(literals: Partial<DataRow> [], projectsData: ProjectDataMap): DataRow[] {
  return literals.map(literal => convertLinkedLowLevelRequirementLiteral(literal, projectsData));
}


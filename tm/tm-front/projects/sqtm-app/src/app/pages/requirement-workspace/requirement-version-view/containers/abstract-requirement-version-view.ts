import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {RequirementVersionViewService} from '../services/requirement-version-view.service';
import {filter, map, switchMap, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {
  AttachmentDrawerComponent,
  AttachmentState,
  CapsuleInformationData,
  DialogService,
  DragAndDropService,
  GridPersistenceService,
  GridStateSnapshot,
  MilestoneModeData,
  NewReqVersionParams,
  ReferentialDataService,
  ReferentialDataState,
  REQUIREMENT_TREE_PICKER_ID,
  RequirementCriticality,
  RequirementCriticalityKeys,
  RequirementStatus,
  RequirementStatusKeys,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  TEST_CASE_TREE_PICKER_ID,
  Workspaces
} from 'sqtm-core';
import {requirementVersionViewContent} from '../requirement-version-view.constant';
import {Observable, Subject} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {
  NewRequirementVersionDialogComponent
} from '../components/dialogs/new-requirement-version-dialog/new-requirement-version-dialog.component';
import {RequirementVersionViewComponentData} from './requirement-version-view/requirement-version-view.component';
import {PrintModeService} from '../../../../components/print-mode/services/print-mode.service';
import {Router} from '@angular/router';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractRequirementVersionView implements OnInit, AfterViewInit, OnDestroy {
  testCaseWorkspace = Workspaces['test-case-workspace'];

  requirementWorkspace = Workspaces['requirement-workspace'];

  treeIds = [REQUIREMENT_TREE_PICKER_ID, TEST_CASE_TREE_PICKER_ID, 'requirement-workspace-main-tree'];

  componentData$: Observable<RequirementVersionViewComponentData>;

  referentialData$: Observable<ReferentialDataState>;

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  @ViewChild('content', {read: ElementRef})
  content: ElementRef;

  unsub$ = new Subject<void>();

  @Output()
  confirmNewVersion = new EventEmitter<number>();

  protected constructor(public requirementViewService: RequirementVersionViewService,
                        protected translateService: TranslateService,
                        protected dndService: DragAndDropService,
                        protected renderer: Renderer2,
                        protected referentialDataService: ReferentialDataService,
                        protected dialogService: DialogService,
                        protected vcr: ViewContainerRef,
                        private router: Router,
                        private gridPersistenceService: GridPersistenceService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.requirementViewService.componentData$;
    this.referentialData$ = this.referentialDataService.referentialData$;
    this.initializeDndFromTreePicker();
  }

  ngAfterViewInit(): void {
    this.dndService.dragAndDrop$.pipe(
      takeUntil(this.unsub$),
      filter((dnd: boolean) => !dnd && Boolean(this.content))
    ).subscribe(() => this.unmarkAsDropZone());
  }

  canAttach(componentData: RequirementVersionViewComponentData): boolean {
    return componentData.permissions.canAttach
      && componentData.milestonesAllowModification
      && this.allowRequirementModification(componentData.requirementVersion.status);
  }

  private initializeDndFromTreePicker() {
    this.dndService.dragEnter$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.componentData$),
      filter(([dragEnterEvent, componentData]: [SqtmDragEnterEvent, RequirementVersionViewComponentData]) =>
        this.shouldShowDndEffect(dragEnterEvent, componentData)),
    ).subscribe(() => {
      this.markAsDropZone();
    });

    this.dndService.dragLeave$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.componentData$),
      filter(([dragEnterEvent, componentData]: [SqtmDragLeaveEvent, RequirementVersionViewComponentData]) =>
        this.shouldShowDndEffect(dragEnterEvent, componentData)),
    ).subscribe(() => {
      this.unmarkAsDropZone();
    });
  }

  private markAsDropZone() {
    this.renderer.addClass(this.content.nativeElement, 'drop-test-case');
  }

  private unmarkAsDropZone() {
    this.renderer.removeClass(this.content.nativeElement, 'drop-test-case');
  }

  private shouldShowDndEffect(dragEnterEvent: SqtmDragEnterEvent | SqtmDragLeaveEvent, componentData: RequirementVersionViewComponentData) {
    return dragEnterEvent
      && dragEnterEvent.dropTargetId === requirementVersionViewContent
      && dragEnterEvent.dragAndDropData
      && this.treeIds.includes(dragEnterEvent.dragAndDropData.origin)
      && componentData.permissions.canLink;
  }

  getAttachmentCount(attachmentState: AttachmentState): number {
    const attachments = Object.values(attachmentState.entities);
    return attachments.filter(attachment => attachment.kind === 'persisted-attachment').length;
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }

  createNewRequirementVersion(componentData: RequirementVersionViewComponentData) {
    if (this.canCreateNewVersion(componentData)) {
      this.openNewRequirementVersionDialog();
    }
  }

  private openNewRequirementVersionDialog() {
    const dialogRef = this.dialogService.openDialog({
      id: 'new-requirement-version',
      component: NewRequirementVersionDialogComponent,
      viewContainerReference: this.vcr,
      width: 600
    });

    dialogRef.dialogClosed$.subscribe(
      (result: NewReqVersionParams) => {
        if (result != null) {
          this.requirementViewService.createNewVersion(result).subscribe(model => {
            this.confirmNewVersion.emit(model.requirementId);
          });
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getRequirementStatusInformationData(statusKey: RequirementStatusKeys): CapsuleInformationData {
    const status = RequirementStatus[statusKey];
    return {
      id: statusKey,
      color: status.color,
      icon: status.icon,
      labelI18nKey: status.i18nKey,
      titleI18nKey: 'sqtm-core.entity.generic.status.label'
    };
  }

  getRequirementCriticalityInformationData(criticalityKey: RequirementCriticalityKeys): CapsuleInformationData {
    const criticality = RequirementCriticality[criticalityKey];
    return {
      id: criticalityKey,
      color: criticality.color,
      icon: criticality.icon,
      labelI18nKey: criticality.i18nKey,
      titleI18nKey: 'sqtm-core.entity.requirement.criticality.label'
    };
  }

  getRequirementVersionNumberInformationData(versionNumber: number): CapsuleInformationData {
    const label = this.translateService.instant('sqtm-core.entity.requirement.version.label.singular');
    return {id: 'versionNumber', label: `${label} ${versionNumber}`};
  }

  closeTestCasePicker() {
    this.requirementViewService.closeTestCaseTreePicker();
  }

  closeRequirementPicker() {
    this.requirementViewService.closeRequirementTreePicker();
  }

  closeHighLevelRequirementChildrenPicker() {
    this.requirementViewService.closeHighLevelRequirementChildrenTreePicker();
  }

  allowRequirementModification(statusKey: RequirementStatusKeys) {
    return RequirementStatus[statusKey].allowModifications;
  }

  allowNavigationToVersion(milestoneState: MilestoneModeData): boolean {
    return !(milestoneState.milestoneModeEnabled && milestoneState.milestoneFeatureEnabled);
  }

  isSynchronized(componentData: RequirementVersionViewComponentData): boolean {
    return componentData.requirementVersion.hasExtender;
  }

  isNameEditable(componentData: RequirementVersionViewComponentData): boolean {
    return !this.isSynchronized(componentData)
      && this.allowRequirementModification(componentData.requirementVersion.status);
  }

  canCreateNewVersion(componentData: RequirementVersionViewComponentData): boolean {
    return componentData.milestonesAllowModification
      && componentData.permissions.canCreate
      && ! this.isSynchronized(componentData);
  }

  isConvertToHighLevelVisible(componentData: RequirementVersionViewComponentData,
                              referentialData: ReferentialDataState): boolean {
    return !componentData.requirementVersion.highLevelRequirement
      && !componentData.requirementVersion.childOfRequirement
      && referentialData.premiumPluginInstalled;
  }

  isConvertToStandardReqVisible(componentData: RequirementVersionViewComponentData,
                                referentialData: ReferentialDataState): boolean {
    return componentData.requirementVersion.highLevelRequirement
      && referentialData.premiumPluginInstalled;
  }

  canConvertToHighLevelOrStandard(componentData: RequirementVersionViewComponentData): boolean {
    return componentData.milestonesAllowModification
      && componentData.permissions.canCreate
      && ! this.isSynchronized(componentData);
  }

  openInPrintMode(componentData: RequirementVersionViewComponentData): void {
    PrintModeService.openPrintRequirementVersionWindow(componentData.requirementVersion.requirementId, componentData.requirementVersion.id);
  }

  convertToHighLevelOrStandardRequirement(componentData: RequirementVersionViewComponentData, convertToHighLevel: boolean) {
    if (this.canConvertToHighLevelOrStandard(componentData)) {
      const requirementId = componentData.requirementVersion.requirementId;
      this.showConfirmTransformRequirementDialog(convertToHighLevel).pipe(
        take(1),
        filter(confirm => confirm),
        switchMap(() => {
         return convertToHighLevel ?
            this.requirementViewService.convertToHighLevelRequirement(requirementId) :
            this.requirementViewService.convertToStandardRequirement(requirementId);
        }),
        switchMap(() => this.gridPersistenceService.selectGridSnapshot('requirement-workspace-main-tree')),
        take(1),
        map((gridSnapshot: GridStateSnapshot) =>
          this.updateLocalStorageAfterConversion(gridSnapshot, requirementId, convertToHighLevel)
        ),
        switchMap((updatedGridSnapshot) => {
          return this.gridPersistenceService.updateGridSnapshot('requirement-workspace-main-tree', updatedGridSnapshot);
        }),
        tap(() => {
          const entityType = convertToHighLevel ? 'HighLevelRequirement' : 'Requirement';
          const nodeToNavigate = entityType + '-' + requirementId.toString();
          this.gridPersistenceService.refreshTreeWithSelectedNode(nodeToNavigate);
        })
      ).subscribe(() => {
        const middleUrlPart = convertToHighLevel ? 'high-level-requirement' : 'requirement';
        const urlParts = ['requirement-workspace', middleUrlPart, requirementId.toString()];
        this.router.navigate(urlParts);
      });
    }
  }

  private showConfirmTransformRequirementDialog(convertToHighLevel: boolean): Observable<any> {
    const titleKey = convertToHighLevel ? 'sqtm-core.requirement-workspace.dialog.transform-to-high-level-requirement.title':
      'sqtm-core.requirement-workspace.dialog.transform-to-standard-requirement.title';

    const messageKey = convertToHighLevel ? 'sqtm-core.requirement-workspace.dialog.transform-to-high-level-requirement.message' :
      'sqtm-core.requirement-workspace.dialog.transform-to-standard-requirement.message';

    const dialogReference = this.dialogService.openConfirm({
      id: 'cuf-disable-optional-dialog',
      titleKey,
      messageKey
    });

    return dialogReference.dialogClosed$;
  }

  private updateLocalStorageAfterConversion(gridSnapshot: GridStateSnapshot,
                                            requirementId: number,
                                            convertToHighLevel: boolean): GridStateSnapshot {
    const newRowPrefix = convertToHighLevel ? 'HighLevelRequirement' : 'Requirement';
    const oldRowPrefix = convertToHighLevel ? 'Requirement' : 'HighLevelRequirement';
    const updatedGridSnapshot = {...gridSnapshot};
    updatedGridSnapshot.selectedRowIds = [`${newRowPrefix}-${requirementId}`];

    const selectedRequirementWithChild = updatedGridSnapshot.openedRows.find(rowId => rowId === `${oldRowPrefix}-${requirementId}`);
    updatedGridSnapshot.openedRows = updatedGridSnapshot.openedRows.filter(rowId => rowId !== `${oldRowPrefix}-${requirementId}`);
    if (selectedRequirementWithChild != null) {
      updatedGridSnapshot.openedRows.push(`${newRowPrefix}-${requirementId}`);
    }
    return updatedGridSnapshot;
  }

  showCreateNewVersionMenuItem() {
    return true;
  }
}

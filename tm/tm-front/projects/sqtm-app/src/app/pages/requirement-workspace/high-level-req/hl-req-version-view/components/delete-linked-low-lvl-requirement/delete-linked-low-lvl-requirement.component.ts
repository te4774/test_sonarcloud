import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractDeleteCellRenderer, DialogService, GridService} from 'sqtm-core';
import {
  RequirementVersionViewService
} from '../../../../requirement-version-view/services/requirement-version-view.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-linked-low-lvl-requirement',
  template: `
    <ng-container *ngIf="canLink && !row.data['childOfRequirement']">
      <sqtm-core-delete-icon
          (delete)="showDeleteConfirm()"
          [iconName]="'sqtm-core-generic:unlink'"></sqtm-core-delete-icon>
    </ng-container>
  `,
  styleUrls: ['./delete-linked-low-lvl-requirement.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteLinkedLowLvlRequirementComponent extends AbstractDeleteCellRenderer implements OnInit {

  canLink;

  constructor(public grid: GridService,
              public cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private requirementVersionViewService: RequirementVersionViewService) {
    super(grid, cdr, dialogService);
  }

  ngOnInit(): void {
    this.requirementVersionViewService.componentData$.pipe(
        takeUntil(this.unsub$)
    ).subscribe(componentData => {
      this.canLink = componentData.permissions.canLink && componentData.requirementVersion.status !== 'OBSOLETE';
      this.cdr.detectChanges();
    });
  }

  protected doDelete(): any {
    this.grid.beginAsyncOperation();

    if (!this.row.data.childOfRequirement) {
      this.requirementVersionViewService.removeLinkedLowLevelRequirementById(this.row.data.requirementId)
          .subscribe(() => this.grid.completeAsyncOperation());
    }
  }

  protected getTitleKey(): string {
    return 'sqtm-core.requirement-workspace.dialog.unbind-low-level-requirement.title';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.requirement-workspace.dialog.unbind-low-level-requirement.message';
  }

}

import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {BindReqToHighLevelReqDialogConfiguration} from './bind-req-to-high-level-req-dialog.configuration';
import {DialogReference} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-bind-requirement-to-high-level-requirement-dialog',
  templateUrl: './bind-req-to-high-level-req-dialog.component.html',
  styleUrls: ['./bind-req-to-high-level-req-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BindReqToHighLevelReqDialogComponent implements OnInit {

  data: BindReqToHighLevelReqDialogConfiguration;
  displayRequirementWithNotLinkableStatusDetails = false;
  displayAlreadyLinkedRequirementDetails = false;
  displayHighLevelRequirementsInSelectionDetails = false;
  displayChildRequirementsInSelectionDetails = false;
  displayAlreadyLinkedToAnotherHighLevelRequirementDetails = false;

  constructor(private dialogReference: DialogReference<BindReqToHighLevelReqDialogConfiguration, void>) {
    this.data = dialogReference.data;
  }

  ngOnInit(): void {
  }

  toggleRequirementWithNotLinkableStatusDisplay() {
    this.displayRequirementWithNotLinkableStatusDetails = !this.displayRequirementWithNotLinkableStatusDetails;
  }

  toggleAlreadyLinkedRequirementDisplay() {
    this.displayAlreadyLinkedRequirementDetails = ! this.displayAlreadyLinkedRequirementDetails;
  }

  toggleHighLevelRequirementsInSelectionDisplay() {
    this.displayHighLevelRequirementsInSelectionDetails = !this.displayHighLevelRequirementsInSelectionDetails;
  }

  toggleAlreadyLinkedToAnotherHighLevelRequirementDisplay() {
    this.displayAlreadyLinkedToAnotherHighLevelRequirementDetails = !this.displayAlreadyLinkedToAnotherHighLevelRequirementDetails;
  }

  toggleChildRequirementsInSelectionDisplay() {
    this.displayChildRequirementsInSelectionDetails = !this.displayChildRequirementsInSelectionDetails;
  }

  getDetailDisplayMessage(shouldDisplay: boolean): string {
    return shouldDisplay ? 'sqtm-core.generic.label.hide-detail' : 'sqtm-core.generic.label.show-detail';
  }

}

import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewContainerRef
} from '@angular/core';
import {DataRow, DialogService, LinkedHighLevelRequirement} from 'sqtm-core';
import {
  HighLevelRequirementSelectorDialogComponent
} from '../../dialogs/high-level-requirement-selector-dialog/high-level-requirement-selector-dialog.component';
import {concatMap, filter, finalize, map, take, tap} from 'rxjs/operators';
import {RequirementVersionViewService} from '../../../services/requirement-version-view.service';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'sqtm-app-high-level-requirement-selector',
  templateUrl: './high-level-requirement-selector.component.html',
  styleUrls: ['./high-level-requirement-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HighLevelRequirementSelectorComponent implements OnInit, OnDestroy {

  @Input()
  linkedHighLevelRequirement: LinkedHighLevelRequirement;

  @Input()
  canLink: boolean;

  @Input()
  isChild: boolean;

  @Output()
  confirmEvent = new EventEmitter<void>();

  _asyncRequestRunning = new BehaviorSubject<boolean>(false);

  asyncRequestRunning$ = this._asyncRequestRunning.asObservable().pipe(
    map(running => running != null && running)
  );

  constructor(private dialogService: DialogService,
              private vcr: ViewContainerRef,
              private requirementVersionViewService: RequirementVersionViewService) {
  }

  ngOnInit(): void {
  }

  openHighLevelRequirementSelector() {
    const dialogRef = this.dialogService.openDialog<void, DataRow>({
      id: 'bind-high-level-requirement-dialog',
      component: HighLevelRequirementSelectorDialogComponent,
      viewContainerReference: this.vcr,
      width: 600,
      height: 768
    });

    dialogRef.dialogClosed$
      .pipe(
        take(1),
        filter(result => Boolean(result)),
        tap(() => this.showAsyncSpinner()),
        concatMap((result) => this.requirementVersionViewService.linkToHighLevelRequirement(result)),
        finalize(() => this.hideAsyncSpinner())
      ).subscribe(() => {
        this.confirmEvent.emit();
    });
  }

  unlink() {
    const dialogRef = this.dialogService.openDeletionConfirm();
    dialogRef.dialogClosed$
      .pipe(
        take(1),
        filter(result => Boolean(result)),
        tap(() => this.showAsyncSpinner()),
        concatMap(() => this.requirementVersionViewService.unlinkFromHighLevelRequirement()),
        finalize(() => this.hideAsyncSpinner())
      ).subscribe(() => {
        this.confirmEvent.emit();
    });
  }

  ngOnDestroy(): void {
    this._asyncRequestRunning.complete();
  }

  getLinkText(): string {
    const reference = this.linkedHighLevelRequirement.reference;
    const name = this.linkedHighLevelRequirement.name;
    return reference ? `${reference}-${name}` : name;
  }

  private showAsyncSpinner() {
    this._asyncRequestRunning.next(true);
  }

  private hideAsyncSpinner() {
    this._asyncRequestRunning.next(false);
  }

  getLinkHref() {
    return ['/', 'requirement-workspace', 'requirement-version', 'detail', this.linkedHighLevelRequirement.requirementVersionId.toString()];
  }
}

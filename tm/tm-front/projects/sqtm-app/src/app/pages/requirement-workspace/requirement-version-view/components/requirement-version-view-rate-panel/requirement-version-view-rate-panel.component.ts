import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Observable} from 'rxjs';
import {
  RequirementVersionViewComponentData
} from '../../containers/requirement-version-view/requirement-version-view.component';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {RequirementVersionStatsBundle} from 'sqtm-core';


@Component({
  selector: 'sqtm-app-requirement-version-view-rate-panel',
  templateUrl: './requirement-version-view-rate-panel.component.html',
  styleUrls: ['./requirement-version-view-rate-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementVersionViewRatePanelComponent {

  readonly COVERAGE_KEY = 'sqtm-core.requirement-workspace.rate.coverage';

  componentData$: Observable<RequirementVersionViewComponentData>;

  @Input()
  isHighLevelRequirementView: boolean;

  constructor(private readonly requirementViewService: RequirementVersionViewService) {
    this.componentData$ = requirementViewService.componentData$;
  }

  get childStatsLabelKey(): string {
    return this.isHighLevelRequirementView ?
      'sqtm-core.requirement-workspace.rate.standard-requirement' :
      'sqtm-core.requirement-workspace.rate.child';
  }

  get currentStatsLabelKey(): string {
    return this.isHighLevelRequirementView ?
      'sqtm-core.requirement-workspace.rate.high-level-requirement' :
      'sqtm-core.requirement-workspace.rate.requirement';
  }

  getCurrentCoverageMatch(stats: RequirementVersionStatsBundle): number {
    return stats.currentVersion.allTestCaseCount > 0 ? 1 : 0;
  }

  getDescendantsCoverageMatch(stats: RequirementVersionStatsBundle): number {
    return stats.coveredDescendantsCount;
  }

  getDescendantsCoverageTotal(stats: RequirementVersionStatsBundle): number {
    return stats.nonObsoleteDescendantsCount;
  }

  getTotalCoverageMatch(stats: RequirementVersionStatsBundle): number {
    return this.getDescendantsCoverageMatch(stats) + this.getCurrentCoverageMatch(stats);
  }

  getTotalCoverageTotal(stats: RequirementVersionStatsBundle): number {
    return this.getDescendantsCoverageTotal(stats) + 1;
  }
}

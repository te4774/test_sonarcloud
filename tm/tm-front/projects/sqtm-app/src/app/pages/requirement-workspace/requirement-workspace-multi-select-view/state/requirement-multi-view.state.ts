import {CustomDashboardModel, EntityScope, RequirementStatistics} from 'sqtm-core';


export interface RequirementMultiViewState {
  statistics: RequirementStatistics;
  scope: EntityScope[];
  dashboard: CustomDashboardModel;
  generatedDashboardOn: Date;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
  dashboardLoaded: boolean;
  extendHighLvlReqScope: boolean;
}

export function provideInitialRequirementMultiView(): Readonly<RequirementMultiViewState> {
  return {
    statistics: null,
    scope: [],
    dashboard: null,
    canShowFavoriteDashboard: false,
    favoriteDashboardId: null,
    generatedDashboardOn: null,
    shouldShowFavoriteDashboard: false,
    dashboardLoaded: false,
    extendHighLvlReqScope: true
  };
}

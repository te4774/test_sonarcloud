import {InjectionToken} from '@angular/core';
import {GridDefinition, GridService} from 'sqtm-core';

export const RVW_VERIFYING_TEST_CASE_TABLE_CONF =
  new InjectionToken<GridDefinition>('Grid config for the verifying test case table of requirement version view');
export const RVW_VERIFYING_TEST_CASE_TABLE =
  new InjectionToken<GridService>('Grid service instance for the verifying test case table of requirement version view');

export const RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF =
  new InjectionToken<GridDefinition>('Grid config for the requirement version link table of requirement version view');
export const RVW_REQUIREMENT_VERSION_LINK_TABLE =
  new InjectionToken<GridService>('Grid service instance for the requirement version link table of requirement version view');

export const RVW_MODIFICATION_HISTORY_TABLE_CONF =
  new InjectionToken<GridDefinition>('Grid config for the modification history table of requirement version view');
export const RVW_MODIFICATION_HISTORY_TABLE =
  new InjectionToken<GridService>('Grid service instance for the modification history table of requirement version view');

export const RVW_LINKED_LOW_LEVEL_REQUIREMENT_TABLE_CONF =
  new InjectionToken<GridDefinition>('Grid config for the linked low level requirement of requirement version view');
export const RVW_LINKED_LOW_LEVEL_REQUIREMENT_TABLE =
  new InjectionToken<GridService>('Grid service instance for the linked low level requirement of requirement version view');

export const requirementVersionViewContent = 'requirement-version-view-content';


export const RV_ISSUE_TABLE_CONF = new InjectionToken<GridDefinition>('Grid config for the issue table of requirement version view');
export const RV_ISSUE_TABLE = new InjectionToken<GridService>('Grid service instance for the issue table of requirement version view');

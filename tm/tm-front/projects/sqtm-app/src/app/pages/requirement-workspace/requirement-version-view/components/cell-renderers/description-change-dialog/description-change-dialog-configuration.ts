export class DescriptionChangeDialogConfiguration {
  id: string;
  titleKey: string;
  level: 'WARNING' | 'DANGER' | 'INFO' = 'INFO';
  messageKey?: string;
  oldValue: string;
  newValue: string;
}

import {ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {
  AbstractCreateEntityDialog,
  AbstractCreateEntityForm,
  ActionErrorDisplayService,
  DataRow,
  DialogReference,
  EntityCreationDialogData,
  EntityCreationService,
  SessionPingService
} from 'sqtm-core';
import {CreateRequirementFormComponent} from '../create-requirement-form/create-requirement-form.component';

@Component({
  selector: 'sqtm-app-create-requirement-dialog',
  templateUrl: './create-requirement-dialog.component.html',
  styleUrls: ['./create-requirement-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateRequirementDialogComponent extends AbstractCreateEntityDialog implements OnInit {

  @ViewChild(CreateRequirementFormComponent)
  private form: CreateRequirementFormComponent;

  constructor(dialogReference: DialogReference<EntityCreationDialogData, DataRow>,
              entityCreationService: EntityCreationService,
              actionErrorDisplayService: ActionErrorDisplayService,
              sessionPingService: SessionPingService) {
    super(dialogReference, entityCreationService, actionErrorDisplayService, sessionPingService);
  }

  get createEntityForm(): AbstractCreateEntityForm {
    return this.form;
  }

}

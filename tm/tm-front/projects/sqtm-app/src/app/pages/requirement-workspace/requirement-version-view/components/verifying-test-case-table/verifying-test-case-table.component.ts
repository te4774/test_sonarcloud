import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {
  DataRow,
  deleteColumn,
  executionStatusColumn,
  Extendable,
  GenericDataRow,
  GridDefinition,
  GridService,
  indexColumn,
  levelEnumColumn,
  Limited,
  milestoneLabelColumn,
  ProjectDataMap,
  ReadOnlyPermissions,
  RequirementPermissions,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  TestCaseStatus,
  TestCaseWeight,
  textCellWithToolTipColumn,
  textColumn,
  withLinkColumn
} from 'sqtm-core';
import {RVW_VERIFYING_TEST_CASE_TABLE} from '../../requirement-version-view.constant';
import {
  DeleteVerifyingTestCaseComponent
} from '../cell-renderers/delete-verifying-test-case/delete-verifying-test-case.component';

export function rvwVerifyingTCTableDefinition(): GridDefinition {
  return smallGrid('requirement-version-view-verifying-tc').withColumns([
      indexColumn()
        .withViewport('leftViewport'),
    textCellWithToolTipColumn('projectName', 'path')
        .changeWidthCalculationStrategy(new Limited(200))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn('reference')
        .changeWidthCalculationStrategy(new Limited(120))
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      withLinkColumn('name', {kind: 'link', baseUrl: '/test-case-workspace/test-case/detail', columnParamId: 'id'})
        .changeWidthCalculationStrategy(new Limited(350))
        .withI18nKey('sqtm-core.entity.test-case.label.singular'),
      milestoneLabelColumn('milestoneLabels')
        .changeWidthCalculationStrategy(new Limited(150)),
      levelEnumColumn('status', TestCaseStatus)
        .changeWidthCalculationStrategy(new Extendable(40, 0.5))
        .withI18nKey('sqtm-core.search.test-case.grid.header.status.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.status.title'),
      levelEnumColumn('importance', TestCaseWeight)
        .changeWidthCalculationStrategy(new Extendable(60, 0.5))
        .withI18nKey('sqtm-core.search.test-case.grid.header.weight.label')
        .withTitleI18nKey('sqtm-core.search.test-case.grid.header.weight.title'),
      executionStatusColumn('lastExecutionStatus')
        .changeWidthCalculationStrategy(new Extendable(60, 0.5))
        .withI18nKey('sqtm-core.entity.execution-plan.last-execution.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.test-case.last-execution.label'),
      deleteColumn(DeleteVerifyingTestCaseComponent)
        .withViewport('rightViewport')
    ]
  ).withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withInitialSortedColumns([
      {id: 'importance', sort: Sort.ASC},
      {id: 'projectName', sort: Sort.ASC},
      {id: 'reference', sort: Sort.ASC},
      {id: 'name', sort: Sort.ASC}
    ])
    .withRowConverter(convertVerifyingTestCaseLiterals)
    .build();
}

export function convertVerifyingTestCaseLiteral(literal: Partial<DataRow>, projectsData: ProjectDataMap): DataRow {
  const dataRow: DataRow = new GenericDataRow();
  dataRow.projectId = literal.projectId;
  attachPermissionsToRow(dataRow, projectsData);
  Object.assign(dataRow, literal);
  const directlyLinked: boolean = dataRow.data['directlyLinked'];
  dataRow.disabled = !directlyLinked;
  dataRow.selectable = directlyLinked;
  return dataRow;
}

function attachPermissionsToRow(dataRow: DataRow, projectData: ProjectDataMap) {
  if (dataRow.projectId == null) {
    dataRow.simplePermissions = new ReadOnlyPermissions();
    return;
  }

  const project = projectData[dataRow.projectId];

  if (project == null) {
    dataRow.simplePermissions = new ReadOnlyPermissions();
    return;
  }

  dataRow.simplePermissions = new RequirementPermissions(project);
}

export function convertVerifyingTestCaseLiterals(literals: Partial<DataRow> [], projectsData: ProjectDataMap): DataRow[] {
  return literals.map(literal => convertVerifyingTestCaseLiteral(literal, projectsData));
}

@Component({
  selector: 'sqtm-app-verifying-test-case-table',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./verifying-test-case-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: RVW_VERIFYING_TEST_CASE_TABLE
    }
  ]
})
export class VerifyingTestCaseTableComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }
}

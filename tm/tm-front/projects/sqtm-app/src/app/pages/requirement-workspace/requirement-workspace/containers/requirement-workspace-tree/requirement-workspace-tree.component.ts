import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewContainerRef
} from '@angular/core';
import {REQ_WS_TREE} from '../../requirement-workspace.constant';
import {
  BindableEntity,
  CreateEntityDialogComponent,
  CreationDialogConfiguration,
  DataRow,
  DialogConfiguration,
  DialogService,
  EntityCreationDialogData,
  GridNode,
  GridPersistenceService,
  GridService,
  InterWindowCommunicationService,
  InterWindowMessages,
  isMilestoneModeActivated,
  milestoneAllowModification,
  MilestoneModeData,
  REFERENCE_FIELD,
  ReferentialDataService,
  ReferentialDataState,
  RestService,
  SquashTmDataRowType,
  toEntityRowReference,
  TransWorkspacesNodesCopierService,
  TreeWithStatePersistence,
  WorkspaceTypeForPlugins,
  WorkspaceWizardMenuItem,
  WorkspaceWizardNavigationService,
} from 'sqtm-core';
import {Router} from '@angular/router';
import {combineLatest, Observable, of} from 'rxjs';
import {concatMap, filter, map, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {requirementWorkspaceLogger} from '../../requirement-workspace.logger';
import {
  CreateRequirementDialogComponent
} from '../../components/create-requirement-dialog/create-requirement-dialog.component';
import {
  ImportRequirementComponent
} from '../../../import-requirement/containers/import-requirement/import-requirement.component';
import {
  RequirementExportDialogComponent
} from '../../../../../components/export-dialog/requirement-export-dialog/components/requirement-export-dialog/requirement-export-dialog.component';
import {
  SynchronizeRequirementsDialogComponent
} from '../../components/synchronize-requirements-dialog/synchronize-requirements-dialog.component';
import {
  SynchronizeRequirementsDialogConfiguration
} from '../../components/synchronize-requirements-dialog/synchronize-requirements-dialog-configuration';

const logger = requirementWorkspaceLogger.compose('RequirementWorkspaceTreeComponent');

@Component({
  selector: 'sqtm-app-requirement-workspace-tree',
  templateUrl: './requirement-workspace-tree.component.html',
  styleUrls: ['./requirement-workspace-tree.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: REQ_WS_TREE
    },
    {
      provide: WorkspaceWizardNavigationService,
      useClass: WorkspaceWizardNavigationService
    }
  ]
})
export class RequirementWorkspaceTreeComponent extends TreeWithStatePersistence implements OnInit, OnDestroy {

  wizardMenuItems$: Observable<WorkspaceWizardMenuItem[]>;
  creationMenuPermissions$: Observable<RequirementTreeCreationPermission>;
  creationMenuButtonActivation$: Observable<boolean>;

  batchMenuPermissions$: Observable<RequirementTreeBatchPermission>;
  batchMenuButtonActivation$: Observable<boolean>;

  constructor(protected restService: RestService,
              public tree: GridService,
              public referentialDataService: ReferentialDataService,
              protected gridPersistenceService: GridPersistenceService,
              protected dialogService: DialogService,
              protected vcr: ViewContainerRef,
              protected router: Router,
              private cdRef: ChangeDetectorRef,
              private transWorkspacesNodesCopierService: TransWorkspacesNodesCopierService,
              private wizardNavigationService: WorkspaceWizardNavigationService,
              private interWindowCommunicationService: InterWindowCommunicationService) {
    super(tree, referentialDataService, gridPersistenceService, restService, router, dialogService, vcr,
      'requirement-workspace-main-tree', 'requirement-tree'
    );

  }

  ngOnInit() {
    this.initEntityIdFromInitialUrl();
    this.initData();
    this.registerStatePersistence();
    this.initTreeSortMenu();
    this.initCreationMenu();
    this.initBatchMenu();
    this.initSyncMenu();
    this.initTruncateNameMenu();
    this.initTreeRefreshObserver();
  }

  private initSyncMenu() {
    this.wizardMenuItems$ = this.wizardNavigationService.initializeForWorkspace(WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE);
  }

  private initTreeRefreshObserver() {
    this.gridPersistenceService.refreshTreeWithSelectedNode$.pipe(
      takeUntil(this.unsub$),
    ).subscribe((selectedNode) => {
      this.reloadTreeData(true, [selectedNode]);
    });
  }

  ngOnDestroy(): void {
    this.unregisterStatePersistence();
    this.unsub$.next();
    this.unsub$.complete();
  }

  navigateToMilestoneDashboard() {
    this.tree.unselectAllRows();
    this.referentialDataService.milestoneModeData$.pipe(
      take(1),
      filter(milestoneData => isMilestoneModeActivated(milestoneData)),
    ).subscribe(() => this.router.navigate(['requirement-workspace', 'milestone-dashboard']));
  }

  navigateToSearch() {
    this.router.navigate(['/search/requirement']);
  }

  copy() {
    this.tree.copy();
    this.tree.selectedRows$.pipe(
      take(1)
    ).subscribe(dataRows => {
      this.transWorkspacesNodesCopierService.addCopiedNodes(dataRows);
    });
  }

  paste() {
    this.tree.paste();
  }

  delete() {
    this.tree.deleteRows();
  }

  private initCreationMenu() {
    this.creationMenuPermissions$ = combineLatest([this.tree.selectedRows$, this.referentialDataService.milestoneModeData$]).pipe(
      takeUntil(this.unsub$),
      map(([selectedRows, milestoneFilter]: [DataRow[], MilestoneModeData]) => this.updateCreationMenu(selectedRows, milestoneFilter))
    );

    this.creationMenuButtonActivation$ = this.creationMenuPermissions$.pipe(
      map(perm => perm.canCreateFolder || perm.canCreateRequirement || perm.canCreateHighLevelRequirement)
    );
  }

  private initBatchMenu() {
    this.batchMenuPermissions$ = combineLatest([this.referentialDataService.isAdminOrProjectManager(), this.tree.selectedRows$]).pipe(
      takeUntil(this.unsub$),
      map(([isAdminOrProjectManager, selectedRows]: [boolean, DataRow[]]) => this.updateBatchMenu(isAdminOrProjectManager, selectedRows))
    );

    this.batchMenuButtonActivation$ = this.batchMenuPermissions$.pipe(
      map(perm => perm.canExport || perm.canImport)
    );
  }

  private updateCreationMenu(selectedRows: DataRow[], milestoneState: MilestoneModeData): RequirementTreeCreationPermission {
    let permissions = {...REQ_TREE_NO_CREATION};
    let creationAllowed = selectedRows.length === 1;
    if (creationAllowed && milestoneState.milestoneFeatureEnabled && milestoneState.milestoneModeEnabled) {
      creationAllowed = milestoneState.selectedMilestone && milestoneAllowModification(milestoneState.selectedMilestone);
    }
    if (creationAllowed) {
      const selectedRow = selectedRows[0];
      const canCreate = selectedRow.simplePermissions.canCreate;
      permissions = {
        canCreateFolder: selectedRow.allowedChildren.includes(SquashTmDataRowType.RequirementFolder) && canCreate,
        canCreateRequirement:
          (selectedRow.allowedChildren.includes(SquashTmDataRowType.Requirement) ||
            selectedRow.allowedChildren.includes(SquashTmDataRowType.HighLevelRequirement)) && canCreate,
        canCreateHighLevelRequirement: selectedRow.allowedChildren.includes(SquashTmDataRowType.HighLevelRequirement) && canCreate,
      };
    }
    return permissions;
  }

  openCreateFolder() {
    combineLatest([this.tree.selectedRows$, this.creationMenuPermissions$]).pipe(
      take(1),
      filter(([_, perm]: [DataRow[], RequirementTreeCreationPermission]) => perm.canCreateFolder),
    ).subscribe(([dataRows, _]: [DataRow[], RequirementTreeCreationPermission]) => {
      // The creation menu item is enabled if there's only 1 selected element, so we can safely take the first one
      const selectedDataRow = dataRows[0];
      logger.debug(`DataRow: ${JSON.stringify(selectedDataRow)}`);

      const configuration: CreationDialogConfiguration<EntityCreationDialogData> = {
        viewContainerReference: this.vcr,
        formComponent: CreateEntityDialogComponent,
        data: {
          titleKey: 'sqtm-core.test-case-workspace.dialog.title.new-folder',
          id: 'add-requirement-folder',
          projectId: selectedDataRow.projectId,
          bindableEntity: BindableEntity.REQUIREMENT_FOLDER,
          parentEntityReference: selectedDataRow.id.toString(),
          postUrl: 'requirement-tree/new-folder'
        }
      };
      this.dialogService.openEntityCreationDialog<EntityCreationDialogData, DataRow>(configuration, this.tree);
    });
  }

  openCreateRequirement() {
    combineLatest([this.tree.selectedRows$, this.creationMenuPermissions$]).pipe(
      take(1),
      filter(([_, perm]: [DataRow[], RequirementTreeCreationPermission]) => perm.canCreateRequirement),
    ).subscribe(([dataRows, _]: [DataRow[], RequirementTreeCreationPermission]) => {
      // The creation menu item is enabled if there's only 1 selected element, so we can safely take the first one
      const selectedDataRow = dataRows[0];
      logger.debug(`DataRow: ${JSON.stringify(selectedDataRow)}`);

      const configuration: CreationDialogConfiguration<EntityCreationDialogData> = {
        viewContainerReference: this.vcr,
        formComponent: CreateRequirementDialogComponent,
        data: {
          titleKey: 'sqtm-core.requirement-workspace.tree.button.new-requirement',
          addAnotherLabelKey: 'sqtm-core.generic.label.add-another.feminine',
          id: 'add-requirement',
          projectId: selectedDataRow.projectId,
          bindableEntity: BindableEntity.REQUIREMENT_VERSION,
          parentEntityReference: selectedDataRow.id.toString(),
          postUrl: 'requirement-tree/new-requirement',
          optionalTextFields: [REFERENCE_FIELD]
        }
      };

      this.dialogService.openEntityCreationDialog<EntityCreationDialogData, DataRow>(configuration, this.tree);
    });
  }

  openCreateHighLevelRequirement() {
    combineLatest([this.tree.selectedRows$, this.creationMenuPermissions$, this.referentialDataService.referentialData$]).pipe(
      take(1),
      filter(([_, perm, referentialData]: [DataRow[], RequirementTreeCreationPermission, ReferentialDataState]) => perm.canCreateHighLevelRequirement),
    ).subscribe(([dataRows, _, refData]: [DataRow[], RequirementTreeCreationPermission, ReferentialDataState]) => {
      // The creation menu item is enabled if there's only 1 selected element, so we can safely take the first one
      const selectedDataRow = dataRows[0];
      logger.debug(`DataRow: ${JSON.stringify(selectedDataRow)}`);

      const configuration: CreationDialogConfiguration<EntityCreationDialogData> = {
        viewContainerReference: this.vcr,
        formComponent: CreateRequirementDialogComponent, // same component for requirements and high level reqs
        data: {
          titleKey: 'sqtm-core.requirement-workspace.tree.button.new-high-level-requirement',
          addAnotherLabelKey: 'sqtm-core.generic.label.add-another.feminine',
          id: 'add-high-level-requirement',
          projectId: selectedDataRow.projectId,
          bindableEntity: BindableEntity.REQUIREMENT_VERSION,
          parentEntityReference: selectedDataRow.id.toString(),
          postUrl: 'requirement-tree/new-high-level-requirement',
          optionalTextFields: [REFERENCE_FIELD]
        }
      };
      if (refData.premiumPluginInstalled) {
        this.dialogService.openEntityCreationDialog<EntityCreationDialogData, DataRow>(configuration, this.tree);
      } else {
        this.dialogService.openAlert({
          titleKey: 'sqtm-core.requirement-workspace.dialog.title.high-level-requirement-plugin-missing',
          messageKey: 'sqtm-core.requirement-workspace.dialog.message.high-level-requirement-plugin-missing',
          level: 'INFO'
        });
      }
    });
  }

  import() {
    this.referentialDataService.isAdminOrProjectManager().pipe(
      take(1),
      filter((isAdminOrProjectManager: boolean) => isAdminOrProjectManager),
      tap(() => this.openImportDialog())
    ).subscribe();
  }

  private openImportDialog() {
    const importDialogConfiguration: DialogConfiguration = {
      id: 'import',
      component: ImportRequirementComponent,
      width: 600
    };

    const importDialog = this.dialogService.openDialog(importDialogConfiguration);

    importDialog.dialogResultChanged$
      .pipe(
        take(1),
        filter((result: boolean) => result),
        withLatestFrom(this.tree.gridNodes$)
      ).subscribe(([_, nodes]: [boolean, GridNode[]]) => {
      const libraryNodes = nodes.filter(node => node.id.toString().includes('RequirementLibrary'));
      this.tree.refreshSubTree(libraryNodes.map(node => node.id));
    });
  }

  export() {
    this.tree.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => this.canExport(rows)),
      concatMap((rows: DataRow[]) => this.extractNodesAndLibraries(rows))
    ).subscribe(
      (result: {
        nodes: number[],
        libraries: number[]
      }) => {
        this.openExportDialog(result.libraries, result.nodes);
      }
    );
  }

  private canExport(rows: DataRow[]) {
    return rows.length > 0 && Boolean(rows.find(r => r.simplePermissions.canExport));
  }

  private openExportDialog(libraries: number[], nodes: number[]) {
    const exportDialogConfiguration: DialogConfiguration = {
      id: 'export-requirement',
      component: RequirementExportDialogComponent,
      viewContainerReference: this.vcr,
      width: 600,
      data: {
        id: 'export-requirement',
        libraries,
        nodes
      }
    };
    this.dialogService.openDialog(exportDialogConfiguration);
  }

  private extractNodesAndLibraries(rows: DataRow[]): Observable<{ nodes: number[], libraries: number[] }> {
    const nodes = [];
    const libraries = [];
    rows.forEach(row => {
      const entityRef = toEntityRowReference(row.id);
      if (entityRef.entityType === SquashTmDataRowType.RequirementLibrary) {
        libraries.push(entityRef.id);
      } else {
        nodes.push(entityRef.id);
      }
    });
    return of({nodes, libraries});
  }

  openSyncDialog(menuItem: WorkspaceWizardMenuItem) {
    if (menuItem.enabled) {
      const url = menuItem.generatedUrl;
      this.tree.selectedRowIds$.pipe(
        take(1)
      ).subscribe(projectIdentifiers => {

        const dialogRef = this.dialogService.openDialog<SynchronizeRequirementsDialogConfiguration, any>({
          id: 'synchronize-requirements-dialog',
          component: SynchronizeRequirementsDialogComponent,
          data: {
            url
          },
          viewContainerReference: this.vcr,
          width: 300,
          minHeight: 300
        });

        this.interWindowCommunicationService.interWindowMessages$.pipe(
          takeUntil(this.unsub$),
          takeUntil(dialogRef.dialogClosed$),
          filter((messages: InterWindowMessages) => messages.isTypeOf('PLUGIN-CLOSE-DIALOG'))
        ).subscribe(messages => {
          this.tree.refreshSubTree(projectIdentifiers);
          dialogRef.close();
        });
      });
    }
  }

  private updateBatchMenu(isAdminOrProjectManager: boolean, selectedRows: DataRow[]): RequirementTreeBatchPermission {
    const perm: RequirementTreeBatchPermission = REQ_TREE_BATCH_NO_PERM;
    perm.canImport = isAdminOrProjectManager;
    perm.canExport = selectedRows.some(r => r.simplePermissions.canExport);
    return perm;
  }
}

export interface RequirementTreeCreationPermission {
  canCreateRequirement: boolean;
  canCreateHighLevelRequirement: boolean;
  canCreateFolder: boolean;
}

export interface RequirementTreeBatchPermission {
  canExport: boolean;
  canImport: boolean;
}

export const REQ_TREE_NO_CREATION: Readonly<RequirementTreeCreationPermission> = {
  canCreateFolder: false,
  canCreateRequirement: false,
  canCreateHighLevelRequirement: false
};

export const REQ_TREE_BATCH_NO_PERM: Readonly<RequirementTreeBatchPermission> = {
  canExport: false,
  canImport: false
};


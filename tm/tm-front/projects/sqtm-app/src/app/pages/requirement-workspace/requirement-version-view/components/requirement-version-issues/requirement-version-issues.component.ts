import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {
  ExportModelBuilderService,
  Extendable,
  Fixed,
  grid,
  GridDefinition,
  GridExportService,
  GridService,
  gridServiceFactory,
  indexColumn,
  issueExecutionsColumn,
  issueKeyColumn,
  issueRequirementsColumn,
  Limited,
  PaginationConfigBuilder,
  ReferentialDataService,
  RestService,
  Sort,
  textColumn
} from 'sqtm-core';
import {take, tap} from 'rxjs/operators';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {RV_ISSUE_TABLE, RV_ISSUE_TABLE_CONF} from '../../requirement-version-view.constant';

@Component({
  selector: 'sqtm-app-requirement-version-issues',
  template: `
    <ng-container *ngIf="requirementVersionViewService.componentData$ | async as componentData">
      <div class="flex-column full-height full-width p-r-15" style="overflow-y: hidden;">
        <div class="flex-row p-r-10">
          <div class="issues-title">
            {{'sqtm-core.test-case-workspace.title.issues' | translate}}
          </div>
          <div class="collapse-button flex-row">
            <sqtm-core-grid-export-menu
              *ngIf="componentData.permissions.canRead"
              [exportContent]="'sqtm-core.grid.export.issue' | translate"
              [workspaceName]="'sqtm-core.requirement-workspace.label.short' | translate"
              [fileFormat]="'csv'"
            ></sqtm-core-grid-export-menu>
          </div>
        </div>
        <div class="issues-container full-width full-height p-15">
          <sqtm-core-issues-panel [entityId]="componentData.requirementVersion.id"
                                  [entityType]="'REQUIREMENT_VERSION_TYPE'"
                                  [bugTracker]="componentData.projectData.bugTracker"
                                  (loadIssues)="loadData()">
            <sqtm-core-grid></sqtm-core-grid>
          </sqtm-core-issues-panel>
        </div>
      </div>
    </ng-container>
  `,
  styleUrls: ['./requirement-version-issues.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: RV_ISSUE_TABLE_CONF,
      useFactory: rvIssuesTableDefinition
    },
    {
      provide: RV_ISSUE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, RV_ISSUE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: RV_ISSUE_TABLE
    },
    ExportModelBuilderService,
    GridExportService
  ]
})
export class RequirementVersionIssuesComponent implements OnInit, OnDestroy {

  constructor(public requirementVersionViewService: RequirementVersionViewService,
              @Inject(RV_ISSUE_TABLE) private gridService: GridService) {
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }

  loadData() {
    this.requirementVersionViewService.componentData$.pipe(
      take(1),
      tap(componentData =>
        this.gridService.setServerUrl([`issues/requirement-version/${componentData.requirementVersion.id}/known-issues/all`]))
    ).subscribe();
  }

}

export function rvIssuesTableDefinition(): GridDefinition {
  return grid('requirement-version-view-issue').withColumns([
    indexColumn()
      .changeWidthCalculationStrategy(new Fixed(70))
      .withViewport('leftViewport'),
    issueKeyColumn('remoteId').withI18nKey('sqtm-core.entity.issue.key.label')
      .changeWidthCalculationStrategy(new Limited(100)),
    textColumn('btProject').withI18nKey('sqtm-core.entity.issue.project.label')
      .changeWidthCalculationStrategy(new Limited(150)).disableSort(),
    textColumn('summary').withI18nKey('sqtm-core.entity.issue.summary.label')
      .changeWidthCalculationStrategy(new Limited(200)).disableSort(),
    textColumn('priority').withI18nKey('sqtm-core.entity.issue.priority.label')
      .changeWidthCalculationStrategy(new Extendable(100, 0.4)).disableSort(),
    textColumn('status').withI18nKey('sqtm-core.entity.issue.status.label')
      .changeWidthCalculationStrategy(new Extendable(100, 0.4)).disableSort(),
    textColumn('assignee').withI18nKey('sqtm-core.entity.issue.assignee.label')
      .changeWidthCalculationStrategy(new Extendable(100, 1)).disableSort(),
    issueExecutionsColumn().withI18nKey('sqtm-core.entity.issue.reported-in.label')
      .changeWidthCalculationStrategy(new Extendable(100, 1.5)).disableSort(),
    issueRequirementsColumn().withI18nKey('sqtm-core.entity.issue.attached-to-requirement-version')
      .changeWidthCalculationStrategy(new Extendable(100, 1.5)).disableSort()
  ]).disableRightToolBar().server().withInitialSortedColumns([{
    id: 'remoteId',
    sort: Sort.DESC
  }]).withPagination(new PaginationConfigBuilder().initialSize(25)).withRowHeight(35).build();
}

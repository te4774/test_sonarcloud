import {Injectable} from '@angular/core';
import {
  createStore,
  CustomDashboardModel,
  EntityScope,
  FavoriteDashboardValue,
  PartyPreferencesService,
  ProjectData,
  ProjectReference,
  ReferentialDataService,
  RequirementStatistics,
  RestService,
} from 'sqtm-core';
import {concatMap, filter, map, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {
  initialRequirementMilestoneViewState,
  RequirementMilestoneViewState
} from '../state/requirement-milestone-view-state';

@Injectable()
export class RequirementMilestoneViewService {

  private readonly store = createStore<RequirementMilestoneViewState>(initialRequirementMilestoneViewState());

  public componentData$: Observable<Readonly<RequirementMilestoneViewState>> = this.store.state$.pipe(
    filter(state => Boolean(state.milestone))
  );

  private unsub$ = new Subject<void>();

  constructor(private restService: RestService,
              private referentialDataService: ReferentialDataService,
              private partyPreferencesService: PartyPreferencesService) {
    this.observeMilestoneModeData();
  }

  observeMilestoneModeData() {
    this.referentialDataService.selectedMilestoneId$.pipe(
        takeUntil(this.unsub$),
        tap(() => this.init())
    ).subscribe();
  }

  init() {
    this.restService.get<MilestoneRequirementDashboard>(['requirement-milestone-dashboard']).pipe(
      withLatestFrom(this.referentialDataService.milestoneModeData$),
      map(([statistics, milestoneModeData]) => {
        const state: RequirementMilestoneViewState = {
          ...statistics,
          milestone: {...milestoneModeData.selectedMilestone},
          generatedDashboardOn: new Date(),
          scope: []
        };
        return state;
      }),
      concatMap((state: RequirementMilestoneViewState) => {
        return this.referentialDataService.findMilestonePerimeter(state.milestone.id).pipe(
          take(1),
          map(projects => ([state, projects]))
        );
      }),
      map(([state, projects]: [RequirementMilestoneViewState, ProjectData[]]) => {
        const scope: EntityScope[] = projects.map(project => ({
          id: new ProjectReference(project.id).asString(),
          label: project.name,
          projectId: project.id
        }));
        return {...state, scope};
      }),
    ).subscribe((state: RequirementMilestoneViewState) => this.store.commit(state));
  }

  complete() {
    this.store.complete();
  }

  refreshStatistics() {
    this.restService.get<MilestoneRequirementDashboard>(['requirement-milestone-dashboard'], ).pipe(
      withLatestFrom(this.store.state$),
      map(([statistics, state]) => {
        const nextState: RequirementMilestoneViewState = {
          ...state,
          ...statistics,
          generatedDashboardOn: new Date()
        };
        return nextState;
      })
    ).subscribe((state: RequirementMilestoneViewState) => this.store.commit(state));
    this.init();
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue): Observable<void> {
    return this.partyPreferencesService.changeRequirementWorkspaceFavoriteDashboard(preferenceValue);
  }
}

export class MilestoneRequirementDashboard {
  statistics: RequirementStatistics;
  dashboard: CustomDashboardModel;
  shouldShowFavoriteDashboard: boolean;
  canShowFavoriteDashboard: boolean;
  favoriteDashboardId: number;
}

export interface RequirementMultiVersionViewState {
  requirementId: number;
  requirementVersionId: number;
}

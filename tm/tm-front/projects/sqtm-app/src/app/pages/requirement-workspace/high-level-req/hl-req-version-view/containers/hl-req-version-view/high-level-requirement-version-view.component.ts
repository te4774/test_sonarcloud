import {ChangeDetectionStrategy, Component, ContentChild, ElementRef, Renderer2, ViewContainerRef} from '@angular/core';
import {
  RequirementVersionViewService
} from '../../../../requirement-version-view/services/requirement-version-view.service';
import {TranslateService} from '@ngx-translate/core';
import {DialogService, DragAndDropService, GridPersistenceService, ReferentialDataService} from 'sqtm-core';
import {
  AbstractRequirementVersionView
} from '../../../../requirement-version-view/containers/abstract-requirement-version-view';
import {Router} from '@angular/router';


@Component({
  selector: 'sqtm-app-high-level-requirement-version-view',
  templateUrl: './high-level-requirement-version-view.component.html',
  styleUrls: ['./high-level-requirement-version-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HighLevelRequirementVersionViewComponent extends AbstractRequirementVersionView  {
  @ContentChild('entityProjection')
  entityProjection: ElementRef;

  constructor(requirementViewService: RequirementVersionViewService,
              translateService: TranslateService,
              dndService: DragAndDropService,
              renderer: Renderer2,
              referentialDataService: ReferentialDataService,
              dialogService: DialogService,
              vcr: ViewContainerRef,
              router: Router,
              gridPersistenceService: GridPersistenceService
              ) {
    super(requirementViewService, translateService, dndService, renderer, referentialDataService, dialogService, vcr, router, gridPersistenceService);
  }

}

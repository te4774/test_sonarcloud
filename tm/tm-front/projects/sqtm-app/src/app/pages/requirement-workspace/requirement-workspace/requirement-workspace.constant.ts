import {InjectionToken} from '@angular/core';
import {GridDefinition, GridService} from 'sqtm-core';

export const REQ_WS_TREE = new InjectionToken<GridService>('Grid service instance for the main workspace tree');

export const REQ_WS_TREE_CONFIG = new InjectionToken<GridDefinition>('Grid config instance for the main workspace tree');

import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {RequirementVersionLink} from 'sqtm-core';

export interface RequirementVersionLinkState extends EntityState<RequirementVersionLink> {

}

export const requirementVersionLinkEntityAdapter = createEntityAdapter<RequirementVersionLink>({
  selectId: (reqVersionLink) => reqVersionLink.id
});

export const requirementVersionLinkEntitySelectors = requirementVersionLinkEntityAdapter.getSelectors();

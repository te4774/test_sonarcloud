import {ChangeDetectionStrategy, Component, Renderer2, ViewContainerRef} from '@angular/core';
import {take} from 'rxjs/operators';
import {
  RequirementVersionLinksData,
  RequirementVersionViewService
} from '../../services/requirement-version-view.service';
// tslint:disable-next-line:max-line-length
import {
  BindRequirementToHighLevelRequirementOperationReport,
  createRequirementVersionLinksMessageDialogConfiguration,
  DialogService,
  RequirementVersionLinkDialogComponent,
  RequirementVersionLinkDialogConfiguration,
  RequirementVersionLinkDialogResult,
  shouldShowRequirementLinksMessageDialog
} from 'sqtm-core';
import {requirementVersionLogger} from '../../requirement-version-view.logger';
import {AbstractRequirementVersionViewContent} from './abstract-requirement-version-view-content';
import {
  BindReqToHighLevelReqDialogComponent
} from '../dialogs/bind-req-to-high-level-req-dialog/bind-req-to-high-level-req-dialog.component';
import {
  BindReqToHighLevelReqDialogConfiguration
} from '../dialogs/bind-req-to-high-level-req-dialog/bind-req-to-high-level-req-dialog.configuration';

const logger = requirementVersionLogger.compose('RequirementVersionViewContentComponent');

@Component({
  selector: 'sqtm-app-requirement-view-content',
  templateUrl: './requirement-version-view-content.component.html',
  styleUrls: ['./requirement-version-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementVersionViewContentComponent extends AbstractRequirementVersionViewContent {

  constructor(requirementViewService: RequirementVersionViewService,
              dialogService: DialogService,
              vcr: ViewContainerRef,
              renderer: Renderer2) {
    super(requirementViewService, dialogService, vcr, renderer);
  }

  addRequirementLinks(requirementIds: number[]) {
    this.requirementViewService.findRelatedRequirementVersionInformation(requirementIds).subscribe(requirementVersionLinksData => {
      const dialogRef = this.dialogService.openDialog<RequirementVersionLinkDialogConfiguration, RequirementVersionLinkDialogResult>({
        id: 'requirement-version-link',
        component: RequirementVersionLinkDialogComponent,
        viewContainerReference: this.vcr,
        data: this.createRequirementLinkConfiguration(requirementVersionLinksData),
        width: 600
      });
      dialogRef.dialogClosed$.subscribe(result => {
        if (result) {
          this.requirementViewService.addRequirementLinks(requirementIds, result.linkTypeId, result.linkDirection)
            .pipe(
              take(1)
            ).subscribe(operationReport => {
            if (shouldShowRequirementLinksMessageDialog(operationReport)) {
              this.dialogService.openDialog(createRequirementVersionLinksMessageDialogConfiguration(operationReport));
            }
          });
        }
      });
    });
  }

  private createRequirementLinkConfiguration(requirementVersionLinksData: RequirementVersionLinksData) {
    return {
      titleKey: 'sqtm-core.requirement-workspace.dialog.requirement-links.title',
      requirementVersionLinkTypes: requirementVersionLinksData.linkTypes,
      requirementVersionNodesName: requirementVersionLinksData.nodeNames,
      requirementVersionName: requirementVersionLinksData.reqVersionName
    };
  }

  protected addChildrenToHighLevelRequirement(requirementIds: number[]) {
    this.requirementViewService.bindRequirementToHighLevelRequirement(requirementIds).subscribe(operationReportData => {
      if (this.checkIfBindReqToHighLevelReqOperationHasException(operationReportData)) {
        this.dialogService.openDialog<BindReqToHighLevelReqDialogConfiguration, void>({
          id: 'bind-requirement-to-high-level-requirement',
          component: BindReqToHighLevelReqDialogComponent,
          viewContainerReference: this.vcr,
          data: this.createBindRequirementToHigLevelRequirementConfiguration(operationReportData),
          width: 650
        });
      }
    });
  }

  private checkIfBindReqToHighLevelReqOperationHasException(operationReportData: BindRequirementToHighLevelRequirementOperationReport) {
    return operationReportData.summary.alreadyLinked.length > 0 ||
           operationReportData.summary.alreadyLinkedToAnotherHighLevelRequirement.length > 0 ||
           operationReportData.summary.highLevelRequirementsInSelection.length > 0 ||
           operationReportData.summary.requirementWithNotLinkableStatus.length > 0 ||
           operationReportData.summary.childRequirementsInSelection.length > 0;
  }

  private createBindRequirementToHigLevelRequirementConfiguration(operationReport: BindRequirementToHighLevelRequirementOperationReport) {
    return {
      titleKey: 'sqtm-core.requirement-workspace.dialog.bind-to-high-level-requirement.title',
      requirementWithNotLinkableStatus: operationReport.summary.requirementWithNotLinkableStatus,
      alreadyLinked: operationReport.summary.alreadyLinked,
      alreadyLinkedToAnotherHighLevelRequirement: operationReport.summary.alreadyLinkedToAnotherHighLevelRequirement,
      highLevelRequirementsInSelection: operationReport.summary.highLevelRequirementsInSelection,
      childRequirementsInSelection: operationReport.summary.childRequirementsInSelection
    };
  }

  removeRequirementVersionLinks() {
    const dialogRef = this.dialogService.openDeletionConfirm({
      id: 'confirm-delete',
      level: 'WARNING',
      titleKey: 'sqtm-core.requirement-workspace.dialog.unlink-requirement-version-links.title',
      messageKey: 'sqtm-core.requirement-workspace.dialog.unlink-requirement-version-links.message'
    });
    dialogRef.dialogClosed$.pipe(
      take(1)
    ).subscribe(confirm => {
      if (confirm) {
        this.requirementViewService.removeRequirementVersionLinks();
      }
    });
  }

  removeLinkedLowLevelRequirements() {
    const dialogRef = this.dialogService.openDeletionConfirm({
      id: 'confirm-delete',
      level: 'WARNING',
      titleKey: 'sqtm-core.requirement-workspace.dialog.unbind-low-level-requirements.title',
      messageKey: 'sqtm-core.requirement-workspace.dialog.unbind-low-level-requirements.message'
    });
    dialogRef.dialogClosed$.pipe(
        take(1)
    ).subscribe(confirm => {
      if (confirm) {
        this.requirementViewService.removeLinkedLowLevelRequirements();
      }
    });
  }
}

import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {RequirementLibraryViewService} from '../../service/requirement-library-view.service';
import {RequirementLibraryViewComponentData} from '../requirement-library-view/requirement-library-view.component';
import {
  CustomDashboardBinding,
  CustomDashboardModel,
  EntityRowReference,
  EntityScope,
  ReferentialDataService,
  SquashTmDataRowType
} from 'sqtm-core';
import {RequirementLibraryState} from '../../state/requirement-library.state';

@Component({
  selector: 'sqtm-app-requirement-library-view-content',
  templateUrl: './requirement-library-view-content.component.html',
  styleUrls: ['./requirement-library-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementLibraryViewContentComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();
  componentData$: Observable<RequirementLibraryViewComponentData>;

  constructor(private requirementLibraryViewService: RequirementLibraryViewService,
              public referentialDataService: ReferentialDataService) {
    this.componentData$ = requirementLibraryViewService.componentData$;
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  refreshStats($event: MouseEvent, componentData: RequirementLibraryViewComponentData) {
    $event.stopPropagation();

    this.doRefreshDashboard(componentData.requirementLibrary);
  }

  getStatisticScope(requirementLibraryState: RequirementLibraryState): EntityScope[] {
    const ref = new EntityRowReference(requirementLibraryState.id, SquashTmDataRowType.RequirementLibrary).asString();
    return [
      {
        id: ref,
        label: requirementLibraryState.name,
        projectId: requirementLibraryState.projectId
      }
    ];
  }

  displayFavoriteDashboard($event: MouseEvent, extendHighLvlReqScope: boolean) {
    $event.stopPropagation();
    this.requirementLibraryViewService.changeDashboardToDisplay('dashboard', extendHighLvlReqScope);
  }

  displayDefaultDashboard($event: MouseEvent, extendHighLvlReqScope: boolean) {
    $event.stopPropagation();
    this.requirementLibraryViewService.changeDashboardToDisplay('default', extendHighLvlReqScope);
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }

  changeExtendedScope(extendedScope: boolean, requirementLibrary: RequirementLibraryState) {
    requirementLibrary.extendHighLvlReqScope = extendedScope;
    this.doRefreshDashboard(requirementLibrary);
  }

  private doRefreshDashboard(requirementLibrary: RequirementLibraryState) {
    if (requirementLibrary.statistics) {
      this.requirementLibraryViewService.refreshStatistics(requirementLibrary.extendHighLvlReqScope);
    }
    if (requirementLibrary.dashboard) {
      this.requirementLibraryViewService.refreshDashboard(requirementLibrary.extendHighLvlReqScope);
    }
  }

  preventClickPropagation($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
  }
}

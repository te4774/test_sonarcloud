import {TestBed} from '@angular/core/testing';

import {
  AttachmentService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  Milestone,
  Permissions,
  ReferentialDataService,
  RequirementVersionService,
  RestService,
} from 'sqtm-core';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {of} from 'rxjs';
import {RequirementVersionViewService} from './requirement-version-view.service';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {RVW_VERIFYING_TEST_CASE_TABLE} from '../requirement-version-view.constant';


describe('RequirementViewService', () => {

  const tableMock = jasmine.createSpyObj('tableMock', ['load']);

  const restServiceMock = {} as RestService;

  const referentialDataService = {} as ReferentialDataService;

  referentialDataService.connectToProjectData = jasmine.createSpy().and.returnValue(of({
    permissions: {REQUIREMENT: [Permissions.WRITE]}
  }));

  referentialDataService.globalConfiguration$ = of(null);

  const milestones: Milestone[] = [
    {
      id: 1,
      endDate: null,
      status: 'IN_PROGRESS',
      description: 'Description of milestone 1',
      label: 'Milestone1',
      ownerFistName: 'admin',
      ownerLastName: 'admin',
      ownerLogin: 'admin',
      range: 'GLOBAL',
      createdBy: '', createdOn: null, lastModifiedBy: null, lastModifiedOn: null,
    },
    {
      id: 2,
      endDate: null,
      status: 'FINISHED',
      description: 'Description of milestone 2',
      label: 'Milestone2',
      ownerFistName: 'admin',
      ownerLastName: 'admin',
      ownerLogin: 'admin',
      range: 'GLOBAL',
      createdBy: '', createdOn: null, lastModifiedBy: null, lastModifiedOn: null,
    }];

  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
    providers: [
      {provide: RestService, useValue: restServiceMock},
      {provide: RVW_VERIFYING_TEST_CASE_TABLE, useValue: tableMock},
      {
        provide: RequirementVersionViewService,
        useClass: RequirementVersionViewService,
        deps: [
          RestService,
          ReferentialDataService,
          AttachmentService,
          TranslateService,
          RequirementVersionService,
          CustomFieldValueService,
          EntityViewAttachmentHelperService,
          EntityViewCustomFieldHelperService,
          RVW_VERIFYING_TEST_CASE_TABLE
        ]
      },
      {
        provide: ReferentialDataService,
        useValue: referentialDataService
      },
    ]
  }));

  it('should be created', () => {
    const service: RequirementVersionViewService = TestBed.get(RequirementVersionViewService);
    expect(service).toBeTruthy();
  });

  // it('should bind one milestone', () => {
  //   let testCaseState = getInitialTestCaseState();
  //   expect(testCaseState.testCase.milestones.length).toBe(0);
  //
  //   testCaseState = bindMilestonesToTC(milestones, testCaseState, [1]);
  //   expect(testCaseState.testCase.milestones.length).toBe(1);
  //   expect(testCaseState.testCase.milestones[0].label).toBe('Milestone1');
  //
  //   testCaseState = bindMilestonesToTC(milestones, testCaseState, [2]);
  //   expect(testCaseState.testCase.milestones.length).toBe(2);
  //   expect(testCaseState.testCase.milestones[0].label).toBe('Milestone1');
  //   expect(testCaseState.testCase.milestones[1].label).toBe('Milestone2');
  //
  // });

  // it('should bind an array of milestones', () => {
  //   let testCaseState = getInitialTestCaseState();
  //   testCaseState = bindMilestonesToTC(milestones, testCaseState, [1, 2, 3]);
  //   expect(testCaseState.testCase.milestones.length).toBe(2);
  //   expect(testCaseState.testCase.milestones[0].label).toBe('Milestone1');
  //   expect(testCaseState.testCase.milestones[1].label).toBe('Milestone2');
  // });
});

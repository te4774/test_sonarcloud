import {Directive, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild, ViewContainerRef} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {
  RequirementVersionViewComponentData
} from '../../containers/requirement-version-view/requirement-version-view.component';
import {
  BindableEntity,
  createCustomFieldValueDataSelector,
  createRequirementVersionVerifyingTestCasesMessageDialogConfiguration,
  CustomFieldData,
  DialogService,
  GridDndData,
  HIGH_LEVEL_REQUIREMENT_CHILDREN_TREE_PICKER_ID,
  isDndDataFromRequirementMainTree,
  parseDataRowId,
  REQUIREMENT_TREE_PICKER_ID,
  shouldShowCoverageMessageDialog,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  SqtmDropEvent,
  TEST_CASE_TREE_PICKER_ID,
} from 'sqtm-core';
import {RequirementVersionViewService} from '../../services/requirement-version-view.service';
import {take, takeUntil} from 'rxjs/operators';
import {select} from '@ngrx/store';
import {requirementVersionLogger} from '../../requirement-version-view.logger';

const logger = requirementVersionLogger.compose('AbstractRequirementVersionViewContentComponent');

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractRequirementVersionViewContent implements OnInit, OnDestroy {
  componentData$: Observable<RequirementVersionViewComponentData>;

  unsub$ = new Subject<void>();

  customFieldData: CustomFieldData[] = [];

  @ViewChild('dropRequirementVersionViewZone', {read: ElementRef})
  private dropRequirementVersionViewZone: ElementRef;

  @ViewChild('dropRequirementLinksZone', {read: ElementRef})
  private dropRequirementLinksZone: ElementRef;

  @ViewChild('dropLowLevelToHighLevelRequirementZone', {read: ElementRef})
  private dropLowLevelToHighLevelRequirementZone: ElementRef;

  constructor(public requirementViewService: RequirementVersionViewService,
              protected dialogService: DialogService,
              protected vcr: ViewContainerRef,
              protected renderer: Renderer2) {
    this.componentData$ = this.requirementViewService.componentData$;
  }

  ngOnInit(): void {
    this.initializeCufData();
  }

  private initializeCufData() {
    this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createCustomFieldValueDataSelector(BindableEntity.REQUIREMENT_VERSION))
    ).subscribe(customFieldData => {
      this.customFieldData = customFieldData;
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  removeVerifyingTestCases() {
    const dialogRef = this.dialogService.openDeletionConfirm({
      id: 'confirm-delete',
      level: 'WARNING',
      titleKey: 'sqtm-core.requirement-workspace.dialog.unlink-test-cases.title',
      messageKey: 'sqtm-core.requirement-workspace.dialog.unlink-test-cases.message'
    });

    dialogRef.dialogClosed$.pipe(
      take(1),
    ).subscribe(confirm => {
      if (confirm) {
        this.requirementViewService.removeVerifyingTestCases();
      }
    });
  }

  toggleTestCaseDrawer(event) {
    event.stopPropagation();
    this.requirementViewService.openTestCaseTreePicker();
  }

  toggleRequirementDrawer(event) {
    event.stopPropagation();
    this.requirementViewService.openRequirementTreePicker();
  }

  toggleHighLevelRequirementChildrenDrawer(event) {
    event.stopPropagation();
    this.requirementViewService.openHighLevelRequirementChildrenTreePicker();
  }

  stopPropagation(mouseEvent: MouseEvent) {
    mouseEvent.stopPropagation();
  }

  dropIntoRequirementVersion($event: SqtmDropEvent, componentData: RequirementVersionViewComponentData) {
    if (componentData.permissions.canLink) {
      this.handleDropIntoRequirementVersion($event, !componentData.requirementVersion.highLevelRequirement);
    }
    this.removeBorderOnRequirementVersionView();
  }

  dropIntoHighLevelRequirement($event: SqtmDropEvent, componentData: RequirementVersionViewComponentData) {
    if (componentData.permissions.canLink && componentData.requirementVersion.highLevelRequirement && isDndDataFromRequirementMainTree($event)) {
      if ($event.dropTargetId === "linked-low-level-requirement-table") {
        const data = $event.dragAndDropData.data as GridDndData;
        const ids = data.dataRows.map(row => parseDataRowId(row));
        this.addChildrenToHighLevelRequirement(ids);
        this.removeBorderOnLowLevelRequirementsTable();
      } else if ($event.dropTargetId === "requirement-links-table") {
        const data = $event.dragAndDropData.data as GridDndData;
        const ids = data.dataRows.map(row => parseDataRowId(row));
        this.addRequirementLinks(ids);
        this.removeBorderOnRequirementLinksTable(componentData.requirementVersion.highLevelRequirement);
      }
    } else {
      this.dropIntoRequirementVersion($event, componentData);
    }
  }
  dragEnterLowLevelRequirementsTable($event: SqtmDragEnterEvent) {
    if (isDndDataFromRequirementMainTree($event)) {
      this.addBorderOnLowLevelRequirementsTable();
    }
  }
  dragEnterRequirementLinksTable($event: SqtmDragEnterEvent, isHighLevelRequirement: boolean) {
    if (isHighLevelRequirement && isDndDataFromRequirementMainTree($event)) {
      this.addBorderOnRequirementLinksTable();
      this.removeBorderOnRequirementVersionView();
    }
  }
  dragEnterRequirementVersionView($event: SqtmDragEnterEvent, isHighLevelRequirement: boolean) {
    if (!isHighLevelRequirement || (isHighLevelRequirement && !isDndDataFromRequirementMainTree($event))) {
      this.addBorderOnRequirementVersionView();
    }
  }

  dragLeaveLowLevelRequirementsTable($event: SqtmDragLeaveEvent) {
    if (isDndDataFromRequirementMainTree($event)) {
      this.removeBorderOnLowLevelRequirementsTable();
    }
  }
  dragLeaveRequirementLinksTable($event: SqtmDragLeaveEvent, isHighLevelRequirement: boolean) {
    if (isHighLevelRequirement && isDndDataFromRequirementMainTree($event)) {
      this.removeBorderOnRequirementLinksTable(isHighLevelRequirement);
    }
  }
  dragLeaveRequirementVersionView(isHighLevelRequirement: boolean) {
    if (!isHighLevelRequirement) {
      this.removeBorderOnRequirementVersionView();
    }
  }

  private addBorderOnLowLevelRequirementsTable() {
    this.renderer.addClass(this.dropLowLevelToHighLevelRequirementZone.nativeElement, 'drop-requirement');
  }
  private addBorderOnRequirementLinksTable() {
    this.renderer.addClass(this.dropRequirementLinksZone.nativeElement, 'drop-requirement');
  }
  private addBorderOnRequirementVersionView() {
    this.renderer.addClass(this.dropRequirementVersionViewZone.nativeElement, 'drop-requirement');
  }

  removeBorderOnLowLevelRequirementsTable() {
    this.renderer.removeClass(this.dropLowLevelToHighLevelRequirementZone.nativeElement, 'drop-requirement');
  }
  removeBorderOnRequirementLinksTable(isHighLevelRequirement: boolean) {
    if (isHighLevelRequirement) {
      this.renderer.removeClass(this.dropRequirementLinksZone.nativeElement, 'drop-requirement');
    }
  }
  removeBorderOnRequirementVersionView() {
    this.renderer.removeClass(this.dropRequirementVersionViewZone.nativeElement, 'drop-requirement');
  }

  private handleDropIntoRequirementVersion($event: SqtmDropEvent, isNotHighLevelRequirement: boolean) {
    const data = $event.dragAndDropData.data as GridDndData;
    if (logger.isDebugEnabled()) {
      logger.debug(`Dropping dataRows in requirement version view.`, [data]);
    }
    const entityIds = data.dataRows.map(row => parseDataRowId(row));
    if ($event.dragAndDropData.origin === TEST_CASE_TREE_PICKER_ID) {
      this.requirementViewService.addTestCases(entityIds).pipe(take(1)).subscribe(operationReport => {
        if (shouldShowCoverageMessageDialog(operationReport)) {
          this.dialogService.openDialog(createRequirementVersionVerifyingTestCasesMessageDialogConfiguration(operationReport));
        }
      });
    } else if ($event.dragAndDropData.origin === REQUIREMENT_TREE_PICKER_ID
        || (isNotHighLevelRequirement && isDndDataFromRequirementMainTree($event))) {
      this.addRequirementLinks(entityIds);
    } else if ($event.dragAndDropData.origin === HIGH_LEVEL_REQUIREMENT_CHILDREN_TREE_PICKER_ID) {
      const ids = data.dataRows.map(row => parseDataRowId(row));
      this.addChildrenToHighLevelRequirement(ids);
    }
  }



  protected abstract addRequirementLinks(requirementIds: number[]);

  protected abstract addChildrenToHighLevelRequirement(requirementIds: number[]);

  protected abstract removeRequirementVersionLinks();

}

import {Injectable} from '@angular/core';
import {
  AttachmentService,
  CustomDashboardService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  FavoriteDashboardValue,
  PartyPreferencesService,
  ProjectData,
  ReferentialDataService,
  RequirementFolderModel,
  RequirementPermissions,
  RequirementStatisticsService,
  RestService,
} from 'sqtm-core';
import {RequirementFolderState} from '../state/requirement-folder.state';
import {provideInitialRequirementFolderView, RequirementFolderViewState} from '../state/requirement-folder-view.state';
import {TranslateService} from '@ngx-translate/core';
import {concatMap, map, take, tap, withLatestFrom} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable()
export class RequirementFolderViewService extends EntityViewService<RequirementFolderState, 'requirementFolder', RequirementPermissions> {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              private  requirementStatisticsService: RequirementStatisticsService,
              private customDashboardService: CustomDashboardService,
              private partyPreferencesService: PartyPreferencesService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(projectData: ProjectData): RequirementPermissions {
    return new RequirementPermissions(projectData);
  }

  getInitialState(): RequirementFolderViewState {
    return provideInitialRequirementFolderView();
  }

  load(id: number) {
    this.restService.getWithoutErrorHandling<RequirementFolderModel>(['requirement-folder-view', id.toString()])
      .subscribe((requirementFolderModel: RequirementFolderModel) => {
        const requirementFolderState = this.initializeFolderFolderState(requirementFolderModel);
        this.initializeEntityState(requirementFolderState);
      }, err => this.notifyEntityNotFound(err));
  }

  private initializeFolderFolderState(requirementFolderModel: RequirementFolderModel): RequirementFolderState {
    const attachmentEntityState = this.initializeAttachmentState(requirementFolderModel.attachmentList.attachments);
    const customFieldValueState = this.initializeCustomFieldValueState(requirementFolderModel.customFieldValues);
    return {
      ...requirementFolderModel,
      attachmentList: {
        id: requirementFolderModel.attachmentList.id,
        attachments: attachmentEntityState
      },
      customFieldValues: customFieldValueState,
      generatedDashboardOn: new Date(),
      extendHighLvlReqScope: true
    };
  }

  refreshStatistics(extendHighLvlReqScope: boolean) {
    this.state$.pipe(
      take(1),
      concatMap(initialState =>
        this.requirementStatisticsService.fetchStatistics([`RequirementFolder-${initialState.requirementFolder.id}`], extendHighLvlReqScope).pipe(
          withLatestFrom(this.state$),
          map(([statistics, state]) => ({
            ...state,
            requirementFolder: {
              ...state.requirementFolder, statistics: {...statistics},
              dashboard: null,
              shouldShowFavoriteDashboard: false,
              generatedDashboardOn: new Date()
            }
          }))
        ))
    ).subscribe(state => {
      this.commit(state);
    });
  }

  changeDashboardToDisplay(preferenceValue: FavoriteDashboardValue, extendHighLvlReqScope: boolean) {
    this.partyPreferencesService.changeRequirementWorkspaceFavoriteDashboard(preferenceValue).pipe(
      tap(() => {
        if (preferenceValue === 'default') {
          this.refreshStatistics(extendHighLvlReqScope);
        } else {
          this.refreshFolderDashboard();
        }
      })
    ).subscribe();
  }

  refreshFolderDashboard() {
    this.state$.pipe(
      take(1),
      concatMap((initialState: RequirementFolderViewState) => this.refreshFavoriteDashboardIfAllowed(initialState))
    ).subscribe(state => {
      this.commit(state);
    });
  }

  private refreshFavoriteDashboardIfAllowed(initialState: RequirementFolderViewState) {
    if (initialState.requirementFolder.canShowFavoriteDashboard) {
      return this.customDashboardService.getDashboardWithDynamicScope(initialState.requirementFolder.favoriteDashboardId, {
        milestoneDashboard: false,
        workspaceName: 'REQUIREMENT',
        requirementFolderIds: [initialState.requirementFolder.id],
        extendedHighLvlReqScope: initialState.requirementFolder.extendHighLvlReqScope
      }).pipe(
        withLatestFrom(this.state$),
        map(([dashboard, state]) => ({
          ...state,
          requirementFolder: {
            ...state.requirementFolder, statistics: null,
            dashboard: {...dashboard},
            shouldShowFavoriteDashboard: true,
            generatedDashboardOn: new Date()
          }
        }))
      );
    } else {
      return of({
        ...initialState,
        requirementFolder: {
          ...initialState.requirementFolder,
          statistics: null,
          dashboard: null,
          shouldShowFavoriteDashboard: true
        }
      });
    }
  }
}

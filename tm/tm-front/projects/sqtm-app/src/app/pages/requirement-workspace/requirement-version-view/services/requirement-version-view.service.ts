import {
  AttachmentService,
  BindRequirementToHighLevelRequirementOperationReport,
  ChangeLinkedRequirementOperationReport,
  ChangeVerifyingTestCaseOperationReport,
  CustomFieldValueService,
  DataRow,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GridService,
  Identifier,
  LinkedHighLevelRequirement,
  LinkedLowLevelRequirement,
  Milestone,
  NewReqVersionParams,
  ProjectData,
  ReferentialDataService,
  RequirementPermissions,
  RequirementStatus,
  RequirementVersionLink,
  RequirementVersionLinkType,
  RequirementVersionModel,
  RequirementVersionService,
  RequirementVersionStatsBundle,
  RestService,
  StoreOptions,
  toEntityRowReference,
  VerifyingTestCase,
} from 'sqtm-core';
import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {
  linkedLowLevelRequirementCountSelector,
  linkedLowLevelRequirementsSelector,
  provideInitialRequirementViewState,
  requirementVersionLinkCountSelector,
  requirementVersionLinkSelector,
  RequirementVersionState,
  RequirementVersionViewState,
  verifyingTestCaseCountSelect,
  verifyingTestCaseSelector
} from '../state/requirement-version-view.state';
import {concatMap, map, take, tap, withLatestFrom} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {select} from '@ngrx/store';
import {verifyingTestCaseEntityAdapter, VerifyingTestCaseState} from '../state/verifying-test-case.state';
import {requirementVersionLinkEntityAdapter} from '../state/requirement-version-link.state';
import {linkedLowLevelRequirementEntityAdapter} from '../state/linked-low-level-requirement.state';

const storeOptions: StoreOptions = {
  id: 'RequirementViewStore',
  logDiff: 'none'
};

/**
 * Facade for the RequirementView.
 */
@Injectable()
export class RequirementVersionViewService
  extends EntityViewService<RequirementVersionState, 'requirementVersion', RequirementPermissions> {

  public readonly verifyingTestCaseCount$: Observable<number> = this.componentData$.pipe(
    select(verifyingTestCaseCountSelect)
  );

  public readonly requirementVersionLinkCount$: Observable<number> = this.componentData$.pipe(
    select(requirementVersionLinkCountSelector)
  );

  public readonly linkedLowLevelRequirementCount$: Observable<number> = this.componentData$.pipe(
    select(linkedLowLevelRequirementCountSelector)
  );

  public readonly verifyingTestCaseHasSelectedRows$: Observable<boolean>;


  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected requirementVersionService: RequirementVersionService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              public verifyingTestCaseGrid: GridService,
              public requirementVersionLinkGrid: GridService,
              public linkedLowLevelRequirementsGrid: GridService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
      storeOptions
    );
    this.verifyingTestCaseHasSelectedRows$ = this.verifyingTestCaseGrid.hasSelectedRows$;
  }

  load(requirementVersionModel: RequirementVersionModel): void {
    const attachmentEntityState = this.initializeAttachmentState(requirementVersionModel.attachmentList.attachments);
    const customFieldValueState = this.initializeCustomFieldValueState(requirementVersionModel.customFieldValues);
    const verifyingTestCaseState: VerifyingTestCaseState =
      this.initializeVerifyingTestCaseState(requirementVersionModel.verifyingTestCases);
    const requirementVersionLinkState = this.initializeRequirementVersionLinkState(requirementVersionModel.requirementVersionLinks);
    const linkedLowLevelRequirementState = this.initializeLowLevelRequirementState(requirementVersionModel.lowLevelRequirements);
    const entityState: RequirementVersionState = {
      ...requirementVersionModel,
      attachmentList: {id: requirementVersionModel.attachmentList.id, attachments: attachmentEntityState},
      customFieldValues: customFieldValueState,
      uiState: {openTestCaseTreePicker: false, openRequirementTreePicker: false, openHighLevelRequirementChildrenTreePicker: false},
      verifyingTestCases: verifyingTestCaseState,
      requirementVersionLinks: requirementVersionLinkState,
      statusAllowModification: RequirementStatus[requirementVersionModel.status].allowModifications,
      lowLevelRequirements: linkedLowLevelRequirementState
    };
    this.initializeEntityState(entityState);
    this.initializeVerifyingTestCaseGrid();
    this.initializeRequirementVersionLinkGrid();
    this.initializeLinkedLowLevelRequirementsGrid();
    this.initializeRates();
    this.initializeIssuesCount();
  }

  private initializeVerifyingTestCaseState(verifyingTestCases: VerifyingTestCase[]) {
    return verifyingTestCaseEntityAdapter.setAll(verifyingTestCases || [], verifyingTestCaseEntityAdapter.getInitialState());
  }

  private initializeRequirementVersionLinkState(requirementVersionLinks: RequirementVersionLink[]) {
    return requirementVersionLinkEntityAdapter.setAll(requirementVersionLinks || [], requirementVersionLinkEntityAdapter.getInitialState());
  }

  private initializeLowLevelRequirementState(lowLevelRequirements: LinkedLowLevelRequirement[]) {
    return linkedLowLevelRequirementEntityAdapter.setAll(
      lowLevelRequirements || [],
      linkedLowLevelRequirementEntityAdapter.getInitialState());
  }

  addSimplePermissions(data: ProjectData): RequirementPermissions {
    return new RequirementPermissions(data);
  }

  getInitialState(): RequirementVersionViewState {
    return provideInitialRequirementViewState();
  }

  unbindMilestone(requirementVersionId: number, milestoneId: number) {
    this.requirementVersionService.unbindMilestoneToRequirementVersion(requirementVersionId, milestoneId).pipe(
      withLatestFrom(this.store.state$),
      map(([milestones, state]: [Milestone[], RequirementVersionViewState]) => {
        const newState = unbindMilestoneToRV(state, milestoneId);
        return this.updateBindableMilestones(newState, milestones);
      })
    ).subscribe(state => {
      this.store.commit(state);
      this.requireExternalUpdate(state.requirementVersion.id, 'bindableMilestones');
      this.updateLastModification();
    });
  }

  bindMilestones(requirementVersionId: number, milestoneIds: number[]) {
    this.requirementVersionService.bindMilestonesToRequirementVersion(requirementVersionId, milestoneIds).pipe(
      withLatestFrom(this.store.state$),
      map(([milestones, state]: [Milestone[], RequirementVersionViewState]) => {
        const newState = bindMilestonesToRV(state, milestoneIds);
        return this.updateBindableMilestones(newState, milestones);
      })
    ).subscribe(state => {
      this.store.commit(state);
      this.requireExternalUpdate(state.requirementVersion.id, 'bindableMilestones');
      this.updateLastModification();
    });
  }

  updateBindableMilestones(state: RequirementVersionViewState, milestones: Milestone[]) {
    return {...state, requirementVersion: {...state.requirementVersion, bindableMilestones: milestones}};
  }

  addTestCases(testCaseIds: number[]): Observable<ChangeVerifyingTestCaseOperationReport> {
    return this.store.state$.pipe(
      take(1),
      tap(() => this.verifyingTestCaseGrid.beginAsyncOperation()),
      concatMap((state: RequirementVersionViewState) =>
        this.requirementVersionService.persistVerifyingTestCases(state.requirementVersion.id, testCaseIds)
      ),
      withLatestFrom(this.state$),
      tap(([operationReport, state]: [ChangeVerifyingTestCaseOperationReport, RequirementVersionViewState]) => {
        this.requireTreeOrGridRefreshAfterLinkingOperation(state.requirementVersion);
      }),
      map(([operationReport, state]: [ChangeVerifyingTestCaseOperationReport, RequirementVersionViewState]) => {
        const verifyingTestCases = verifyingTestCaseEntityAdapter.setAll(
          operationReport.verifyingTestCases,
          state.requirementVersion.verifyingTestCases);
        return [operationReport, {...state, requirementVersion:
            {...state.requirementVersion,
              verifyingTestCases,
              requirementStats: operationReport.requirementStats,
              nbIssues: operationReport.nbIssues}
        }];
      }),
      tap(() => this.verifyingTestCaseGrid.completeAsyncOperation()),
      map(([operationReport, state]: [ChangeVerifyingTestCaseOperationReport, RequirementVersionViewState]) => {
        this.commit(state);
        return operationReport;
      })
    );
  }

  addRequirementLinks(requirementIds: number[], requirementLinkTypeId: number,
                      linkDirection: boolean): Observable<ChangeLinkedRequirementOperationReport> {
    return this.store.state$.pipe(
      take(1),
      tap(() => this.requirementVersionLinkGrid.beginAsyncOperation()),
      concatMap((state: RequirementVersionViewState) =>
        this.requirementVersionService.persistRequirementVersionLinks(state.requirementVersion.id, {
          reqVersionLinkTypeId: requirementLinkTypeId,
          reqVersionLinkTypeDirection: linkDirection,
          requirementNodesIds: requirementIds
        })
      ),
      withLatestFrom(this.state$),
      tap(([operationReport, state]: [ChangeLinkedRequirementOperationReport, RequirementVersionViewState]) => {
        this.requireTreeOrGridRefreshAfterLinkingOperation(state.requirementVersion);
      }),
      map(([operationReport, state]: [ChangeLinkedRequirementOperationReport, RequirementVersionViewState]) => {
        const requirementLinks = requirementVersionLinkEntityAdapter.setAll(operationReport.requirementVersionLinks,
          state.requirementVersion.requirementVersionLinks);
        return [operationReport, {
          ...state,
          requirementVersion: {...state.requirementVersion, requirementVersionLinks: requirementLinks}
        }];
      }),
      tap(() => this.requirementVersionLinkGrid.completeAsyncOperation()),
      map(([operationReport, state]: [ChangeLinkedRequirementOperationReport, RequirementVersionViewState]) => {
        this.commit(state);
        return operationReport;
      })
    );
  }

  bindRequirementToHighLevelRequirement(requirementIds: number[]): Observable<BindRequirementToHighLevelRequirementOperationReport> {

    return this.store.state$.pipe(
      take(1),
      tap(() => this.linkedLowLevelRequirementsGrid.beginAsyncOperation()),
      concatMap((state: RequirementVersionViewState) =>
        this.bindRequirementToHighLevelRequirementServerSide(state.requirementVersion.requirementId, requirementIds)),
      withLatestFrom(this.state$),
      tap(([operationReport, state]: [BindRequirementToHighLevelRequirementOperationReport , RequirementVersionViewState]) => {
          this.requireTreeOrGridRefreshAfterLinkingOperation(state.requirementVersion);
        }),
      map(([operationReport, state]: [BindRequirementToHighLevelRequirementOperationReport, RequirementVersionViewState]) => {
        const linkedLowLevelRequirements = linkedLowLevelRequirementEntityAdapter.setAll(operationReport.linkedLowLevelRequirements,
          state.requirementVersion.lowLevelRequirements);

        const verifyingTestCases = verifyingTestCaseEntityAdapter.setAll(operationReport.verifyingTestCases,
          state.requirementVersion.verifyingTestCases);

        const updatedState: RequirementVersionViewState = {
          ...state,
          requirementVersion: {
            ...state.requirementVersion,
            lowLevelRequirements: linkedLowLevelRequirements,
            verifyingTestCases,
            requirementStats: operationReport.requirementStats,
            nbIssues: operationReport.nbIssues
          }
        };
        return [operationReport, updatedState];
      }),
      tap(() => this.linkedLowLevelRequirementsGrid.completeAsyncOperation()),
      map(([operationReport, state]: [BindRequirementToHighLevelRequirementOperationReport, RequirementVersionViewState]) => {
         this.commit(state);
         return operationReport;
      })
    );
  }

  private bindRequirementToHighLevelRequirementServerSide(highLevelRequirementId: number, requirementIds: number[])
    : Observable<BindRequirementToHighLevelRequirementOperationReport> {
    return this.requirementVersionService.persistRequirementBindingToHighLevelRequirement(highLevelRequirementId, requirementIds);
  }

  updateRequirementLinks(requirementIds: number[], requirementLinkTypeId: number,
                         linkDirection: boolean, isRelatedIdANodeId: boolean) {
    return this.store.state$.pipe(
      take(1),
      tap(() => this.requirementVersionLinkGrid.beginAsyncOperation()),
      concatMap((state: RequirementVersionViewState) =>
        this.requirementVersionService.updateRequirementVersionLinkType(state.requirementVersion.id, {
          reqVersionLinkTypeId: requirementLinkTypeId,
          reqVersionLinkTypeDirection: linkDirection,
          requirementNodesIds: requirementIds,
          isRelatedIdANodeId: isRelatedIdANodeId
        })
      ),
      withLatestFrom(this.state$),
      map(([reqVersionLinks, state]: [RequirementVersionLink[], RequirementVersionViewState]) => {
        const requirementLinks = requirementVersionLinkEntityAdapter.setAll(reqVersionLinks,
          state.requirementVersion.requirementVersionLinks);
        return {
          ...state,
          requirementVersion: {...state.requirementVersion, requirementVersionLinks: requirementLinks}
        };
      }),
      tap(() => this.requirementVersionLinkGrid.completeAsyncOperation())
    ).subscribe(state => this.commit(state));
  }

  findRelatedRequirementVersionInformation(relatedVersionIds: number[]):
    Observable<RequirementVersionLinksData> {
    return this.state$.pipe(
      take(1),
      concatMap((state: RequirementVersionViewState) => this.requirementVersionService
        .findRequirementVersionLinkNames(state.requirementVersion.id, relatedVersionIds).pipe(
          map((versionNames) => {
            return {nodeNames: versionNames.versionName, reqVersionName: state.requirementVersion.name};
          })
        )),

      withLatestFrom(this.referentialDataService.requirementVersionLinkTypes$),
      map(([versionNames, requirementVersionLinkTypes]: [{ reqVersionName: string, nodeNames: string }, RequirementVersionLinkType[]]) => {
        return {...versionNames, linkTypes: requirementVersionLinkTypes};
      })
    );
  }

  removeVerifyingTestCases() {
    this.store.state$.pipe(
      take(1),
      withLatestFrom(this.verifyingTestCaseGrid.selectedRowIds$),
      tap(() => this.verifyingTestCaseGrid.beginAsyncOperation()),
      concatMap(([state, rowIds]: [RequirementVersionViewState, Identifier[]]) =>
        this.requirementVersionService.removeVerifyingTestCases(state.requirementVersion.id, rowIds as number[])),
      withLatestFrom(this.store.state$),
      map(([operationReport, state]: [ChangeVerifyingTestCaseOperationReport, RequirementVersionViewState]) =>
        this.removeVerifyingTestCasesInState(operationReport, state)),
      tap((state) => this.requireTreeOrGridRefreshAfterLinkingOperation(state.requirementVersion)),
      tap(state => this.store.commit(state))
    ).subscribe(() => this.verifyingTestCaseGrid.completeAsyncOperation());
  }

  removeVerifyingTestCasesById(testCaseIds: number[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      concatMap((state: RequirementVersionViewState) =>
        this.requirementVersionService.removeVerifyingTestCases(state.requirementVersion.id, testCaseIds)),
      withLatestFrom(this.store.state$),
      map(([operationReport, state]: [ChangeVerifyingTestCaseOperationReport, RequirementVersionViewState]) =>
        this.removeVerifyingTestCasesInState(operationReport, state)),
      tap((state) => this.requireTreeOrGridRefreshAfterLinkingOperation(state.requirementVersion)),
      tap(state => this.store.commit(state))
    );
  }

  removeRequirementVersionLinks() {
    this.store.state$.pipe(
      take(1),
      withLatestFrom(this.requirementVersionLinkGrid.selectedRowIds$),
      tap(() => this.requirementVersionLinkGrid.beginAsyncOperation()),
      concatMap(([state, rowIds]: [RequirementVersionViewState, Identifier[]]) =>
        this.requirementVersionService.removeRequirementLinks(state.requirementVersion.id, rowIds as number[])),
      withLatestFrom(this.store.state$),
      map(([operationReport, state]: [ChangeLinkedRequirementOperationReport, RequirementVersionViewState]) =>
        this.removeRequirementLinksInState(operationReport, state)),
      tap((state) => this.requireTreeOrGridRefreshAfterLinkingOperation(state.requirementVersion)),
      tap(state => this.store.commit(state))
    ).subscribe(() => this.requirementVersionLinkGrid.completeAsyncOperation());
  }

  removeRequirementLinkById(requirementLinkIds: number[]): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: RequirementVersionViewState) =>
        this.requirementVersionService.removeRequirementLinks(state.requirementVersion.id, requirementLinkIds)),
      withLatestFrom(this.state$),
      map(([operationReport, state]: [ChangeLinkedRequirementOperationReport, RequirementVersionViewState]) =>
        this.removeRequirementLinksInState(operationReport, state)),
      tap((state) => this.requireTreeOrGridRefreshAfterLinkingOperation(state.requirementVersion)),
      tap(state => this.store.commit(state))
    );
  }

  removeLinkedLowLevelRequirements() {
    this.store.state$.pipe(
        take(1),
        withLatestFrom(this.linkedLowLevelRequirementsGrid.selectedRowIds$),
        tap(() => this.linkedLowLevelRequirementsGrid.beginAsyncOperation()),
        concatMap(([state, rowIds]: [RequirementVersionViewState, Identifier[]]) =>
            this.requirementVersionService.removeLinkedLowLevelRequirements(state.requirementVersion.requirementId, rowIds as number[])),
        withLatestFrom(this.store.state$),
        map(([operationReport, state]: [BindRequirementToHighLevelRequirementOperationReport, RequirementVersionViewState]) =>
            this.removeLinkedLowLevelRequirementsInState(operationReport, state)),
        tap((state) => this.requireTreeOrGridRefreshAfterLinkingOperation(state.requirementVersion)),
        tap(state => this.store.commit(state))
    ).subscribe(() => this.linkedLowLevelRequirementsGrid.completeAsyncOperation());
  }

  removeLinkedLowLevelRequirementById(linkedLowLevelRequirementId: number): Observable<any> {
    return this.state$.pipe(
        take(1),
        concatMap((state: RequirementVersionViewState) =>
            this.requirementVersionService.removeLinkedLowLevelRequirements(state.requirementVersion.requirementId,
              [linkedLowLevelRequirementId])),
        withLatestFrom(this.state$),
        map(([operationReport, state]: [BindRequirementToHighLevelRequirementOperationReport, RequirementVersionViewState]) =>
            this.removeLinkedLowLevelRequirementsInState(operationReport, state)),
        tap((state) => this.requireTreeOrGridRefreshAfterLinkingOperation(state.requirementVersion)),
        tap(state => this.store.commit(state))
    );
  }

  private requireTreeOrGridRefreshAfterLinkingOperation(requirementVersion: RequirementVersionState) {
    this.requireExternalUpdate(requirementVersion.id);
  }

  openTestCaseTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: RequirementVersionViewState) =>
        ({
          ...state,
          requirementVersion: {
            ...state.requirementVersion,
            uiState: {...state.requirementVersion.uiState, openTestCaseTreePicker: true}
          }
        })
      )
    ).subscribe(state => this.commit(state));
  }

  closeTestCaseTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: RequirementVersionViewState) =>
        ({
          ...state,
          requirementVersion: {
            ...state.requirementVersion,
            uiState: {...state.requirementVersion.uiState, openTestCaseTreePicker: false}
          }
        })
      )
    ).subscribe(state => this.commit(state));
  }

  openRequirementTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: RequirementVersionViewState) =>
        ({
          ...state,
          requirementVersion: {
            ...state.requirementVersion,
            uiState: {...state.requirementVersion.uiState, openRequirementTreePicker: true}
          }
        })
      )
    ).subscribe(state => this.commit(state));
  }

  openHighLevelRequirementChildrenTreePicker() {
    this.state$.pipe(
        take(1),
        map((state: RequirementVersionViewState) =>
            ({
              ...state,
              requirementVersion: {
                ...state.requirementVersion,
                uiState: {...state.requirementVersion.uiState, openHighLevelRequirementChildrenTreePicker: true}
              }
            })
        )
    ).subscribe(state => this.commit(state));
  }

  closeRequirementTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: RequirementVersionViewState) =>
        ({
          ...state,
          requirementVersion: {
            ...state.requirementVersion,
            uiState: {...state.requirementVersion.uiState, openRequirementTreePicker: false}
          }
        })
      )
    ).subscribe(state => this.commit(state));
  }

  closeHighLevelRequirementChildrenTreePicker() {
    this.state$.pipe(
        take(1),
        map((state: RequirementVersionViewState) =>
            ({
              ...state,
              requirementVersion: {
                ...state.requirementVersion,
                uiState: {...state.requirementVersion.uiState, openHighLevelRequirementChildrenTreePicker: false}
              }
            })
        )
    ).subscribe(state => this.commit(state));
  }

  updateRequirementStatus(status: string): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap(state => this.requirementVersionService.updateStatus(state.requirementVersion.id, status)),
      withLatestFrom(this.state$),
      map(([, state]: [any, RequirementVersionViewState]) => {
        const statusAllowModification = RequirementStatus[status].allowModifications;
        return {...state, requirementVersion: {...state.requirementVersion, status: status, statusAllowModification}};
      }),
      tap((state: RequirementVersionViewState) => this.store.commit(state)),
      tap(state => this.requireExternalUpdate(state.requirementVersion.id, 'status', state.requirementVersion.versionNumber)),
      tap(() => this.updateLastModification())
    );
  }

  createNewVersion(params: NewReqVersionParams): Observable<RequirementVersionModel> {
    return this.state$.pipe(
      take(1),
      concatMap(state => this.requirementVersionService.createNewRequirementVersion(state.requirementVersion.requirementId, params)),
      tap((newReqVersion: RequirementVersionModel) => this.load(newReqVersion)),
      tap(newReqVersion => this.requireExternalUpdate(newReqVersion.id))
    );
  }

  linkToHighLevelRequirement(row: DataRow): Observable<any> {
    const reference = toEntityRowReference(row);
    return this.state$.pipe(
      take(1),
      concatMap((state: RequirementVersionViewState) =>
        this.requirementVersionService.linkToHighLevelRequirement(state.requirementVersion.requirementId, reference.id).pipe(
          map((linkedHighLevelRequirement: LinkedHighLevelRequirement) => {
            return {...state, requirementVersion: {...state.requirementVersion, linkedHighLevelRequirement}};
          })
        )),
      tap(state => this.commit(state)),
      tap(() => this.updateLastModification()));
  }

  unlinkFromHighLevelRequirement(): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: RequirementVersionViewState) =>
        this.requirementVersionService.unlinkFromHighLevelRequirement(state.requirementVersion.requirementId).pipe(
          map(() => ({...state, requirementVersion: {...state.requirementVersion, linkedHighLevelRequirement: null}}))
        )),
      tap(state => this.commit(state)),
      tap(() => this.updateLastModification()));
  }

  private removeVerifyingTestCasesInState(operationReport: ChangeVerifyingTestCaseOperationReport,
                                          state: RequirementVersionViewState): RequirementVersionViewState {
    const verifyingTestCases = verifyingTestCaseEntityAdapter.setAll(operationReport.verifyingTestCases,
      state.requirementVersion.verifyingTestCases);
    return {...state, requirementVersion: {...state.requirementVersion,
        verifyingTestCases,
        requirementStats: operationReport.requirementStats,
        nbIssues: operationReport.nbIssues
      }
    };
  }

  private removeRequirementLinksInState(operationReport: ChangeLinkedRequirementOperationReport,
                                        state: RequirementVersionViewState): RequirementVersionViewState {
    const linkedRequirementVersions = requirementVersionLinkEntityAdapter.setAll(operationReport.requirementVersionLinks,
      state.requirementVersion.requirementVersionLinks);
    return {
      ...state, requirementVersion: {
        ...state.requirementVersion,
        requirementVersionLinks: linkedRequirementVersions
      }
    };
  }

  private removeLinkedLowLevelRequirementsInState(operationReport: BindRequirementToHighLevelRequirementOperationReport,
                                        state: RequirementVersionViewState): RequirementVersionViewState {
    const lowLevelRequirements = linkedLowLevelRequirementEntityAdapter.setAll(operationReport.linkedLowLevelRequirements,
        state.requirementVersion.lowLevelRequirements);

    const verifyingTestCases = verifyingTestCaseEntityAdapter.setAll(operationReport.verifyingTestCases,
      state.requirementVersion.verifyingTestCases);

    return {
      ...state, requirementVersion: {
        ...state.requirementVersion,
        lowLevelRequirements,
        verifyingTestCases,
        requirementStats: operationReport.requirementStats,
        nbIssues: operationReport.nbIssues
      }
    };
  }

  private initializeVerifyingTestCaseGrid() {
    const verifyingTestCases$ = this.componentData$.pipe(
      select(verifyingTestCaseSelector)
    );
    this.verifyingTestCaseGrid.connectToDatasource(verifyingTestCases$, 'id');
    this.componentData$.pipe(
      take(1),
      tap(componentData => {
        const canEdit = componentData.permissions.canLink && componentData.requirementVersion.status !== 'OBSOLETE';
        this.verifyingTestCaseGrid.setColumnVisibility('milestoneLabels',
          componentData.globalConfiguration.milestoneFeatureEnabled);
        this.verifyingTestCaseGrid.setColumnVisibility('delete', canEdit);
      })
    ).subscribe();
  }

  private initializeRequirementVersionLinkGrid() {
    const requirementVersionLink$ = this.componentData$.pipe(
      select(requirementVersionLinkSelector)
    );
    this.requirementVersionLinkGrid.connectToDatasource(requirementVersionLink$, 'id');
    this.componentData$.pipe(
      take(1),
      tap(componentData => {
        this.requirementVersionLinkGrid.setColumnVisibility('milestoneLabels',
          componentData.globalConfiguration.milestoneFeatureEnabled);
        const canEdit = componentData.permissions.canLink && componentData.requirementVersion.status !== 'OBSOLETE';
        this.requirementVersionLinkGrid.setColumnVisibility('delete', canEdit);
        this.requirementVersionLinkGrid.setColumnVisibility('edit', canEdit);
      })
    ).subscribe();
  }


  private initializeLinkedLowLevelRequirementsGrid() {
    // for high level requirements
    // removing this "if" would require an abstract and probably generic service.
    // for now it's too expensive considering the scope of high level requirements
    if (this.linkedLowLevelRequirementsGrid) {
      const linkedLowLevelRequirements$ = this.componentData$.pipe(
        select(linkedLowLevelRequirementsSelector)
      );
      this.linkedLowLevelRequirementsGrid.connectToDatasource(linkedLowLevelRequirements$, 'requirementId');
    }
  }

  convertToHighLevelRequirement(requirementId: number): Observable<any> {
    return this.requirementVersionService.convertToHighLevelRequirement(requirementId);
  }

  convertToStandardRequirement(requirementId: number): Observable<any> {
    return this.requirementVersionService.convertToStandardRequirement(requirementId);
  }

  private initializeRates() {
    return this.state$.pipe(
      take(1),
      concatMap((state: RequirementVersionViewState) => this.requirementVersionService.getRequirementVersionIssuesCount(state.requirementVersion.id).pipe(
        withLatestFrom(this.state$),
        map(([nbIssues, state]: [number, RequirementVersionViewState]) => ({...state, requirementVersion: {...state.requirementVersion, nbIssues}}))
      )),
      tap((state: RequirementVersionViewState) => this.commit(state))
    ).subscribe();
  }

  private initializeIssuesCount() {
    return this.state$.pipe(
      take(1),
      concatMap((state: RequirementVersionViewState) => this.requirementVersionService.getRequirementVersionStatistics(state.requirementVersion.id).pipe(
        withLatestFrom(this.state$),
        map(([requirementStats, state]: [RequirementVersionStatsBundle, RequirementVersionViewState]) =>
          ({...state, requirementVersion: {...state.requirementVersion, requirementStats}}))
      )),
      tap((state: RequirementVersionViewState) => this.commit(state))
    ).subscribe();
  }
}


export function bindMilestonesToRV(state: RequirementVersionViewState, milestoneIds: number[]) {
  const bindableMilestones = state.requirementVersion.bindableMilestones;
  const oldMilestones = state.requirementVersion.milestones;
  const newMilestones = bindableMilestones.filter(m => milestoneIds.includes(m.id));
  const allMilestones = [...oldMilestones, ...newMilestones];
  return {
    ...state, requirementVersion: {
      ...state.requirementVersion, milestones: allMilestones
    }
  };
}

export function unbindMilestoneToRV(state: RequirementVersionViewState, milestoneId: number) {
  return {
    ...state,
    requirementVersion: {
      ...state.requirementVersion,
      milestones: state.requirementVersion.milestones.filter(milestone => milestone.id !== milestoneId)
    }
  };
}

export interface RequirementVersionLinksData {
  reqVersionName: string;
  nodeNames: string;
  linkTypes: RequirementVersionLinkType[];
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractDeleteCellRenderer, DialogService, GridService} from 'sqtm-core';
import {RequirementVersionViewService} from '../../../services/requirement-version-view.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-requirement-link',
  template: `
    <ng-container *ngIf="canLink">
        <sqtm-core-delete-icon
            (delete)="showDeleteConfirm()"
            [iconName]="'sqtm-core-generic:unlink'"></sqtm-core-delete-icon>
    </ng-container>
  `,
  styleUrls: ['./delete-requirement-link.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteRequirementLinkComponent extends AbstractDeleteCellRenderer implements OnInit {

  canLink;

  constructor(public grid: GridService,
              public cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private requirementVersionViewService: RequirementVersionViewService) {
    super(grid, cdr, dialogService);
  }

  ngOnInit(): void {
    this.requirementVersionViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(componentData => {
      this.canLink = componentData.permissions.canLink && componentData.requirementVersion.status !== 'OBSOLETE';
      this.cdr.detectChanges();
    });
  }

  protected doDelete(): any {
    this.grid.beginAsyncOperation();
    const requirementLinkId = this.row.data.id;
    this.requirementVersionViewService.removeRequirementLinkById([requirementLinkId])
      .subscribe(() => this.grid.completeAsyncOperation());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.requirement-workspace.dialog.unlink-requirement-version-link.title';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.requirement-workspace.dialog.unlink-requirement-version-link.message';
  }
}

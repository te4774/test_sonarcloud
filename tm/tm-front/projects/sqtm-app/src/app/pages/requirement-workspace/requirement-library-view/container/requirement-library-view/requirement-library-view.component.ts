import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {
  AttachmentDrawerComponent,
  AttachmentState,
  EntityViewComponentData,
  EntityViewService,
  GenericEntityViewService,
  ReferentialDataService,
  RequirementPermissions
} from 'sqtm-core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {filter, map, take, takeUntil} from 'rxjs/operators';
import {RequirementLibraryState} from '../../state/requirement-library.state';
import {RequirementLibraryViewService} from '../../service/requirement-library-view.service';
import {requirementLibraryViewLogger} from '../../requirement-library-view.logger';

const logger = requirementLibraryViewLogger.compose('RequirementLibraryViewComponent');

@Component({
  selector: 'sqtm-app-requirement-library-view',
  templateUrl: './requirement-library-view.component.html',
  styleUrls: ['./requirement-library-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: RequirementLibraryViewService,
      useClass: RequirementLibraryViewService,
    },
    {
      provide: EntityViewService,
      useExisting: RequirementLibraryViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: RequirementLibraryViewService
    }]
})
export class RequirementLibraryViewComponent implements OnInit, OnDestroy {

  unsub$ = new Subject<void>();

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  @ViewChild('content', {read: ElementRef})
  content: ElementRef;

  constructor(public readonly requirementLibraryService: RequirementLibraryViewService,
              private referentialDataService: ReferentialDataService,
              private route: ActivatedRoute
  ) {
    logger.debug('Instantiate RequirementLibraryViewComponent');
  }

  ngOnInit() {
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      logger.debug(`Loading RequirementLibraryViewComponent Data by http request`);
      this.loadData();
    });
  }


  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('requirementLibraryId')),
      ).subscribe((id) => {
      this.requirementLibraryService.load(parseInt(id, 10));
    });
  }

  ngOnDestroy(): void {
    this.requirementLibraryService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  getAttachmentCount(attachmentState: AttachmentState): number {
    const attachments = Object.values(attachmentState.entities);
    return attachments.filter(attachment => attachment.kind === 'persisted-attachment').length;
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }
}

export interface RequirementLibraryViewComponentData
  extends EntityViewComponentData<RequirementLibraryState, 'requirementLibrary', RequirementPermissions> {
}

import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  DataRow,
  DialogReference,
  HighLevelRequirementPickerComponent,
  ReferentialDataService,
  ReferentialDataState,
  SquashTmDataRowType
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {filter, map, shareReplay, take, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-high-level-requirement-selector-dialog',
  templateUrl: './high-level-requirement-selector-dialog.component.html',
  styleUrls: ['./high-level-requirement-selector-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HighLevelRequirementSelectorDialogComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild(HighLevelRequirementPickerComponent)
  private highLevelRequirementPickerComponent: HighLevelRequirementPickerComponent;

  allowConfirm$: Observable<boolean>;

  private selectedRows: Observable<DataRow[]>;

  private unsub$ = new Subject<void>();

  referentialData$: Observable<ReferentialDataState>;

  constructor(private dialogRef: DialogReference<void, DataRow>,
              private referentialDataService: ReferentialDataService) {
    this.referentialData$ = this.referentialDataService.referentialData$;
  }

  ngOnInit(): void {
  }


  ngAfterViewInit(): void {
    this.selectedRows = this.highLevelRequirementPickerComponent.selectedRows.pipe(
      takeUntil(this.unsub$),
      shareReplay(1));

    this.allowConfirm$ = this.selectedRows.pipe(
      map((rows: DataRow[]) => this.isSelectionValid(rows)));
  }

  private isSelectionValid(rows: DataRow[]) {
    if (rows.length === 1 && rows[0].type === SquashTmDataRowType.HighLevelRequirement) {
      return true;
    }
    return false;
  }

  confirmLinkToHighLevelRequirement() {
    this.selectedRows.pipe(
      take(1),
      filter((rows: DataRow[]) => this.isSelectionValid(rows))
    ).subscribe(rows => {
      this.dialogRef.result = rows[0];
      this.dialogRef.close();
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {
  RVW_LINKED_LOW_LEVEL_REQUIREMENT_TABLE,
  RVW_LINKED_LOW_LEVEL_REQUIREMENT_TABLE_CONF,
  RVW_REQUIREMENT_VERSION_LINK_TABLE,
  RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF,
  RVW_VERIFYING_TEST_CASE_TABLE,
  RVW_VERIFYING_TEST_CASE_TABLE_CONF
} from '../../../../requirement-version-view/requirement-version-view.constant';
import {
  rvwVerifyingTCTableDefinition
} from '../../../../requirement-version-view/components/verifying-test-case-table/verifying-test-case-table.component';
import {
  AttachmentService,
  CustomFieldValueService,
  EntityRowReference,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewService,
  gridServiceFactory,
  Identifier,
  ReferentialDataService,
  RequirementVersionModel,
  RequirementVersionService,
  RestService,
  SquashTmDataRowType,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {
  rvwRequirementVersionLinkTableDefinition
} from '../../../../requirement-version-view/components/requirement-version-link-table/requirement-version-link-table.component';
import {TranslateService} from '@ngx-translate/core';
import {
  RequirementVersionViewService
} from '../../../../requirement-version-view/services/requirement-version-view.service';
import {Subject} from 'rxjs';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {concatMap, filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {
  RequirementVersionViewComponentData
} from '../../../../requirement-version-view/containers/requirement-version-view/requirement-version-view.component';
import {highLevelRequirementViewLogger} from '../../high-level-requirement-view.logger';
import {
  linkedLowLevelRequirementTableDefinition
} from '../../../hl-req-version-view/components/linked-low-lvl-requirement-table/linked-low-lvl-requirement-table.component';


const logger = highLevelRequirementViewLogger.compose('HighLevelRequirementViewComponent');

@Component({
  selector: 'sqtm-app-high-level-requirement-view',
  templateUrl: './high-level-requirement-view.component.html',
  styleUrls: ['./high-level-requirement-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: RVW_VERIFYING_TEST_CASE_TABLE_CONF,
      useFactory: rvwVerifyingTCTableDefinition
    },
    {
      provide: RVW_VERIFYING_TEST_CASE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, RVW_VERIFYING_TEST_CASE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF,
      useFactory: rvwRequirementVersionLinkTableDefinition,
      deps: [TranslateService]
    },
    {
      provide: RVW_REQUIREMENT_VERSION_LINK_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, RVW_REQUIREMENT_VERSION_LINK_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: RVW_LINKED_LOW_LEVEL_REQUIREMENT_TABLE_CONF,
      useFactory: linkedLowLevelRequirementTableDefinition,
    },
    {
      provide: RVW_LINKED_LOW_LEVEL_REQUIREMENT_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, RVW_LINKED_LOW_LEVEL_REQUIREMENT_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: RequirementVersionViewService,
      useClass: RequirementVersionViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        RequirementVersionService,
        CustomFieldValueService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        RVW_VERIFYING_TEST_CASE_TABLE,
        RVW_REQUIREMENT_VERSION_LINK_TABLE,
        RVW_LINKED_LOW_LEVEL_REQUIREMENT_TABLE
      ]
    },
    {
      provide: EntityViewService,
      useExisting: RequirementVersionViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: RequirementVersionViewService
    }
  ]
})
export class HighLevelRequirementViewComponent implements OnInit, OnDestroy {

  unsub$ = new Subject<void>();

  constructor(private requirementVersionService: RequirementVersionService,
              private referentialDataService: ReferentialDataService,
              private requirementViewService: RequirementVersionViewService,
              private route: ActivatedRoute,
              private workspaceWithTree: WorkspaceWithTreeComponent) {
  }

  ngOnInit(): void {
    logger.debug(`ngOnInit HighLevelRequirementView`);
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      logger.debug(`Loading HighLevelRequirementView Data by http request`);
      this.loadData();
      // this.initializeDndFromRequirementTreePicker();
      this.initializeTreeSynchronisation();
      this.initializeRefreshViewObserver();
    });
  }

  ngOnDestroy(): void {
    // this.requirementViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('requirementId')),
        concatMap((requirementId) =>
          this.requirementVersionService.loadHighLevelRequirementCurrentVersion(parseInt(requirementId, 10)))
      ).subscribe((requirementVersionModel: RequirementVersionModel) => {
        logger.debug(`Loading HighLevelRequirementView ${requirementVersionModel.id}`);
        this.requirementViewService.load(requirementVersionModel);
      },
      (err) => this.requirementViewService.notifyEntityNotFound(err)
    );
  }

  private initializeTreeSynchronisation() {
    this.requirementViewService.simpleAttributeRequiringRefresh = ['name', 'reference', 'criticality', 'category', 'status',
      'description', 'verifyingTestCases', 'versionNumber'];
    this.requirementViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.requirementViewService.componentData$),
      map(([{}, componentData]: [{}, RequirementVersionViewComponentData]) =>
        new EntityRowReference(componentData.requirementVersion.requirementId, SquashTmDataRowType.HighLevelRequirement).asString())
    ).subscribe((identifier: Identifier) => {
      this.workspaceWithTree.requireNodeRefresh([identifier]);
    });
  }

  private initializeRefreshViewObserver() {
    this.requirementVersionService.refreshView$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(() => this.loadData());
  }

  newVersion(requirementId: number) {
    const id = new EntityRowReference(requirementId, SquashTmDataRowType.HighLevelRequirement).asString();
    this.workspaceWithTree.requireCurrentContainerRefresh(id);
  }

}

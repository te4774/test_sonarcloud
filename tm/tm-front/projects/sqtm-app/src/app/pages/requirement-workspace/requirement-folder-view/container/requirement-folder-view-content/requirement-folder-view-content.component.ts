import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {RequirementFolderViewService} from '../../services/requirement-folder-view.service';
import {Observable, Subject} from 'rxjs';
import {RequirementFolderViewComponentData} from '../requirement-folder-view/requirement-folder-view.component';
import {
  BindableEntity,
  createCustomFieldValueDataSelector,
  CustomDashboardBinding,
  CustomDashboardModel,
  CustomFieldData,
  EntityRowReference,
  EntityScope,
  ReferentialDataService,
  SquashTmDataRowType
} from 'sqtm-core';
import {takeUntil} from 'rxjs/operators';
import {select} from '@ngrx/store';
import {RequirementFolderState} from '../../state/requirement-folder.state';

@Component({
  selector: 'sqtm-app-requirement-folder-view-content',
  templateUrl: './requirement-folder-view-content.component.html',
  styleUrls: ['./requirement-folder-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementFolderViewContentComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();
  componentData$: Observable<RequirementFolderViewComponentData>;
  customFieldData: CustomFieldData[];

  constructor(private requirementFolderViewService: RequirementFolderViewService, public referentialDataService: ReferentialDataService) {
    this.componentData$ = requirementFolderViewService.componentData$;
  }

  ngOnInit() {
    this.initCufs();
  }

  initCufs() {
    this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createCustomFieldValueDataSelector(BindableEntity.REQUIREMENT_FOLDER))
    ).subscribe(customFieldData => {
      this.customFieldData = customFieldData;
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  refreshStats($event: MouseEvent, componentData: RequirementFolderViewComponentData) {
    $event.stopPropagation();

    this.doRefreshDashboard(componentData.requirementFolder);
  }

  getStatisticScope(requirementFolder: RequirementFolderState): EntityScope[] {
    const ref = new EntityRowReference(requirementFolder.id, SquashTmDataRowType.RequirementFolder).asString();
    return [
      {
        id: ref,
        label: requirementFolder.name,
        projectId: requirementFolder.projectId
      }
    ];
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }

  displayFavoriteDashboard($event: MouseEvent, extendHighLvlReqScope: boolean) {
    $event.stopPropagation();
    this.requirementFolderViewService.changeDashboardToDisplay('dashboard', extendHighLvlReqScope);
  }

  displayDefaultDashboard($event: MouseEvent, extendHighLvlReqScope: boolean) {
    $event.stopPropagation();
    this.requirementFolderViewService.changeDashboardToDisplay('default', extendHighLvlReqScope);
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }

  changeExtendedScope(extendedScope: boolean, requirementFolder: RequirementFolderState) {
    requirementFolder.extendHighLvlReqScope = extendedScope;
    this.doRefreshDashboard(requirementFolder);
  }

  private doRefreshDashboard(requirementFolder: RequirementFolderState) {
    if (requirementFolder.statistics) {
      this.requirementFolderViewService.refreshStatistics(requirementFolder.extendHighLvlReqScope);
    }
    if (requirementFolder.dashboard) {
      this.requirementFolderViewService.refreshFolderDashboard();
    }
  }
}

import {ProjectData} from 'sqtm-core';

export class ImportLog {
  requirementVersionSuccesses: number;
  requirementVersionWarnings: number;
  requirementVersionFailures: number;
  coverageSuccesses: number;
  coverageWarnings: number;
  coverageFailures: number;
  linkedLowLevelReqSuccesses: number;
  linkedLowLevelReqWarnings: number;
  linkedLowLevelReqFailures: number;
  reqlinksSuccesses: number;
  reqlinksWarnings: number;
  reqlinksFailures: number;
  reportUrl: string;
}

export interface ImportFormatFailure {
  missingMandatoryColumns: string[];
  duplicateColumns: string[];
  actionValidationError: any;
}

export class XlsReport {
  templateOk: ImportLog;
  importFormatFailure: ImportFormatFailure;
}

export class ImportRequirementState {
  currentStep: ImportRequirementSteps;
  format: RequirementImportFileFormatKeys;
  importFailed: boolean;
  file: File;
  projects: ProjectData[];
  selectedProject: number;
  loadingData: boolean;
  simulationReport: XlsReport;
  xlsReport: XlsReport;
}


export function initialImportRequirementState(): Readonly<ImportRequirementState> {
  return {
    currentStep: 'CONFIGURATION',
    format: 'XLS',
    importFailed: false,
    file: null,
    projects: [],
    selectedProject: null,
    loadingData: false,
    simulationReport: null,
    xlsReport: null
  };
}

export type RequirementImportFileFormatKeys = 'XLS';

export interface FileFormatItem<K> {
  id: string;
  i18nKey: string;
  value: string;
}

export type FileFormat<K extends string> = {
  [id in K]: FileFormatItem<K>
};

export const RequirementFileFormat: FileFormat<RequirementImportFileFormatKeys> = {
  XLS: {id: 'XLS', i18nKey: 'sqtm-core.generic.label.file.format.xls', value: 'xls'}
};

export type ImportRequirementSteps = 'CONFIGURATION' | 'SIMULATION_REPORT' | 'CONFIRMATION' | 'CONFIRMATION_REPORT';

// tslint:disable-next-line:max-line-length
export const XLS_TYPE = 'application/vnd.ms-excel,application/vnd.ms-excel.sheet.macroEnabled.12, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

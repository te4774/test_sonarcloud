import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {take, withLatestFrom} from 'rxjs/operators';
import {ExecutionRunnerNavigationService} from '../../services/execution-runner-navigation.service';
import {TestSuiteTestPlanRunnerNavigationService} from '../../services/test-suite-test-plan-runner-navigation.service';

@Component({
  selector: 'sqtm-app-test-suite-test-plan-runner',
  templateUrl: './test-suite-test-plan-runner.component.html',
  styleUrls: ['./test-suite-test-plan-runner.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    TestSuiteTestPlanRunnerNavigationService,
    {
      provide: ExecutionRunnerNavigationService,
      useExisting: TestSuiteTestPlanRunnerNavigationService
    }
  ]
})
export class TestSuiteTestPlanRunnerComponent implements OnInit, OnDestroy {

  constructor(private activatedRouteSnapshot: ActivatedRoute, private runnerNavigationService: TestSuiteTestPlanRunnerNavigationService) {
  }

  ngOnInit(): void {
    this.activatedRouteSnapshot.paramMap.pipe(
      take(1),
      withLatestFrom(this.activatedRouteSnapshot.queryParamMap)
    ).subscribe(([paramMap, queryParamMap]: [ParamMap, ParamMap]) => {
      const iterationId = Number.parseInt(paramMap.get('testSuiteId'), 10);
      const testPlanItemId = Number.parseInt(paramMap.get('testPlanItemId'), 10);
      const rawParam = queryParamMap.get('hasNextTestCase');
      const hasNextTestCase = rawParam && rawParam.toLowerCase() === 'true';
      const partialTestPlanItemIds = JSON.parse(queryParamMap.get('partialTestPlanItemIds'));
      this.runnerNavigationService.changeTestPlanItem(iterationId, testPlanItemId, hasNextTestCase, partialTestPlanItemIds);
    });
  }

  ngOnDestroy(): void {
    this.runnerNavigationService.complete();
  }

}

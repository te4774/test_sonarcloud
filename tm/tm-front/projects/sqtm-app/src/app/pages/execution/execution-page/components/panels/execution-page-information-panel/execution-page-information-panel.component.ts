import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {CustomFieldData} from 'sqtm-core';
import {ExecutionPageComponentData} from '../../../containers/abstract-execution-page.component';

@Component({
  selector: 'sqtm-app-execution-page-information-panel',
  templateUrl: './execution-page-information-panel.component.html',
  styleUrls: ['./execution-page-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionPageInformationPanelComponent {

  readonly SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX = 'sqtm-core-infolist-item';

  @Input()
  executionPageComponentData: ExecutionPageComponentData;

  @Input()
  customFieldData: CustomFieldData[];

  constructor() { }

  // method used to handle the migration of keys between 1.X and 2.0
  getDenormalizedNatureLabel(key: string): string {
    if (key && key.includes('test-case.nature.', 0)) {
      return 'sqtm-core.entity.' + key;
    }
    return key;
  }

  getDenormalizedTypeLabel(key: string): string {
    if (key && key.includes('test-case.type.', 0)) {
      return 'sqtm-core.entity.' + key;
    }
    return key;
  }

  getInfoListIcon(iconName: string): any {
    if (!Boolean(iconName) || iconName === '' || iconName === 'noicon') {
      return '';
    }

    return `${this.SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX}:${iconName}`;
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }

}

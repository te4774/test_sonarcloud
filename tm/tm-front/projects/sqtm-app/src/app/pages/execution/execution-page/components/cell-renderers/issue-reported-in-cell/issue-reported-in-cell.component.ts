import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewContainerRef} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DialogService,
  getIssueReportSourceStepsDialogConf,
  GridService,
  IssueReportedInValueRenderer
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-issue-reported-in-cell',
  template: `
      <ng-container *ngIf="row">
      <span class="full-width full-height flex-column">
        <label class="sqtm-grid-cell-txt-renderer m-auto m-l-1"
               [ngStyle]="labelStyle"
               [sqtmCoreLabelTooltip]="row.data[columnDisplay.id]"
               nz-tooltip [nzTooltipTitle]="" [nzTooltipPlacement]="'topLeft'"
               (click)="handleClick()">
          {{labelText}}
        </label>
      </span>
      </ng-container>`,
  styleUrls: ['./issue-reported-in-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueReportedInCellComponent extends AbstractCellRendererComponent {
  constructor(public gridService: GridService,
              public cdRef: ChangeDetectorRef,
              private translateService: TranslateService,
              private dialogService: DialogService,
              private viewContainerRef: ViewContainerRef) {
    super(gridService, cdRef);
  }

  get executionSteps(): number[] {
    return this.row.data.executionSteps;
  }

  get multipleMode(): boolean {
    return this.executionSteps.length > 1;
  }

  get labelStyle() {
    return {
      cursor: this.multipleMode ? 'pointer' : 'default',
      marginLeft: 0,
    };
  }

  get labelText() {
    if (this.multipleMode) {
      return this.translateService.instant('sqtm-core.campaign-workspace.execution-page.multiple-sources');
    } else {
      const stepOrder: number = this.executionSteps[0];
      if (stepOrder >= 0) {
        const oneBasedStepOrder = stepOrder + 1;
        const stepLabel = this.translateService.instant('sqtm-core.campaign-workspace.execution-page.step.label');
        return `${stepLabel} ${oneBasedStepOrder}`;
      } else {
        return this.translateService.instant('sqtm-core.campaign-workspace.execution-page.this-execution.label');
      }
    }
  }

  handleClick(): void {
    if (this.multipleMode) {
      this.dialogService.openDialog(getIssueReportSourceStepsDialogConf(
              this.executionSteps,
              this.viewContainerRef
      ));
    }
  }
}

export function issueIssueReportedInColumn(): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder('reportSites').withRenderer(IssueReportedInCellComponent)
          .withExportValueRenderer(IssueReportedInValueRenderer);
}

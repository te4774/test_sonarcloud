import {
  DenormalizedCustomFieldValueModel,
  ExecutionKind,
  ExecutionStatusKeys,
  Milestone,
  RequirementVersionCoverage,
  SqtmEntityState,
  TestAutomationServerKind,
  TestCaseImportanceKeys,
  TestCaseStatusKeys
} from 'sqtm-core';
import {createEntityAdapter, EntityState} from '@ngrx/entity';

export interface ExecutionState extends SqtmEntityState {
  id: number;
  name: string;
  executionOrder: number;
  executionSteps: EntityState<ExecutionStepState>;
  currentStepIndex: number;
  prerequisite: string;
  testCaseId?: number;
  tcNatLabel: string;
  tcNatIconName: string;
  tcTypeLabel: string;
  tcTypeIconName: string;
  tcStatus: TestCaseStatusKeys;
  tcImportance: TestCaseImportanceKeys;
  tcDescription: string;
  comment: string;
  datasetLabel?: string;
  // no need to create a state for that, it's just a simple array for denormalized cfv display
  denormalizedCustomFieldValues: DenormalizedCustomFieldValueModel[];
  coverages: RequirementVersionCoverage[];
  executionMode: string;
  lastExecutedOn: Date;
  lastExecutedBy: string;
  executionStatus: ExecutionStatusKeys;
  testAutomationServerKind: TestAutomationServerKind;
  automatedExecutionResultUrl: string;
  automatedExecutionResultSummary: string;
  automatedJobUrl: string;
  nbIssues: number;
  iterationId: number;
  kind: ExecutionKind;
  milestones: Milestone[];
  extendedPrerequisite: boolean;
  scrollTop: number;
  testPlanItemId: number;
  executionsCount: number;

}

export interface ExecutionStepState extends SqtmEntityState {
  order: number;
  executionStatus: ExecutionStatusKeys;
  action: string;
  expectedResult: string;
  comment: string;
  denormalizedCustomFieldValues: DenormalizedCustomFieldValueModel[];
  lastExecutedOn: Date;
  lastExecutedBy: string;
  // Does this step is shown as full view or reduced view
  extended: boolean;
  testStepId?: number;
}

export const executionStepAdapter = createEntityAdapter<ExecutionStepState>({
  sortComparer: (a, b) => a.order - b.order
});

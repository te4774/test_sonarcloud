import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild} from '@angular/core';
import {
  AttachmentFileSelectorComponent,
  CampaignPermissions,
  CustomFieldData,
  PersistedAttachment,
  RejectedAttachment
} from 'sqtm-core';
import {ExecutionRunnerService} from '../../services/execution-runner.service';
import {ExecutionState} from '../../../states/execution-state';

@Component({
  selector: 'sqtm-app-execution-prologue-body',
  templateUrl: './execution-prologue-body.component.html',
  styleUrls: ['./execution-prologue-body.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionPrologueBodyComponent implements OnInit {

  readonly SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX = 'sqtm-core-infolist-item';

  @Input()
  execution: ExecutionState;

  @Input()
  customFieldData: CustomFieldData[];

  @Input()
  permissions: CampaignPermissions;

  @Input()
  milestonesAllowModification: boolean;

  @ViewChild(AttachmentFileSelectorComponent)
  fileSelector: AttachmentFileSelectorComponent;

  get attachments() {
    return Object.values(this.execution.attachmentList.attachments.entities);
  }

  constructor(private executionRunnerService: ExecutionRunnerService) {
  }

  ngOnInit() {
  }

  // method used to handle the migration of keys between 1.X and 2.0
  getDenormalizedNatureLabel(key: string): string {
    if (key && key.includes('test-case.nature.', 0)) {
      return 'sqtm-core.entity.' + key;
    }
    return key;
  }

  getDenormalizedTypeLabel(key: string): string {
    if (key && key.includes('test-case.type.', 0)) {
      return 'sqtm-core.entity.' + key;
    }
    return key;
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }

  deleteAttachmentEvent(persistedAttachment: PersistedAttachment) {
    this.executionRunnerService.markAttachmentsToDelete([persistedAttachment.id]);
  }

  cancelDeleteAttachment(persistedAttachment: PersistedAttachment) {
    this.executionRunnerService.cancelDeleteAttachments([persistedAttachment.id]);
  }

  confirmDeleteAttachment(persistedAttachment: PersistedAttachment) {
    this.executionRunnerService.deleteAttachments([persistedAttachment.id], this.execution.attachmentList.id);
  }

  removeRejectedAttachment(rejectedAttachment: RejectedAttachment) {
    this.executionRunnerService.removeRejectedAttachments([rejectedAttachment.id]);
  }

  browseForAttachment(): void {
    this.fileSelector.openFileBrowser();
  }

  handleAddAttachments(files: File[]): void {
    this.executionRunnerService.addAttachments(files, this.execution.attachmentList.id);
  }

  getInfoListIcon(iconName: string): any {
    if (!Boolean(iconName) || iconName === '' || iconName === 'noicon') {
      return '';
    }

    return `${this.SQTM_CORE_INFOLIST_ITEM_ICON_PREFIX}:${iconName}`;
  }
}

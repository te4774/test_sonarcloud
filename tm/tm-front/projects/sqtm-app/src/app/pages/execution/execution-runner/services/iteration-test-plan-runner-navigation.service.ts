import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {DialogService, IterationService, TestPlanResumeModel} from 'sqtm-core';
import {TestPlanRunnerNavigationService} from './test-plan-runner-navigation.service';

@Injectable()
export class IterationTestPlanRunnerNavigationService extends TestPlanRunnerNavigationService {

  constructor(router: Router, dialogService: DialogService, private iterationService: IterationService) {
    super(router, dialogService);
  }

  protected getUrlRoot(): string[] {
    return [
      'execution-runner',
      'iteration', this.state.testPlanOwnerId.toString(),
      'test-plan', this.state.testPlanItemId.toString()
    ];
  }

  protected getNextExecution() {
    if (this.state.partialTestPlanItemIds?.length > 0) {
      return this.iterationService.getNextExecutionOfPartial(this.state.testPlanOwnerId, this.state.testPlanItemId, this.state.partialTestPlanItemIds);
    } else {
      return this.iterationService.getNextExecution(this.state.testPlanOwnerId, this.state.testPlanItemId);
    }
  }

  protected extractTestPlanOwnerId(testPlanResumeModel: TestPlanResumeModel) {
    return testPlanResumeModel.iterationId;
  }

}

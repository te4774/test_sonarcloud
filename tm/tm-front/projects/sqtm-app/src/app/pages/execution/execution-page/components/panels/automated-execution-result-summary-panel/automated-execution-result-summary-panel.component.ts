import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-app-automated-execution-result-summary-panel',
  templateUrl: './automated-execution-result-summary-panel.component.html',
  styleUrls: ['./automated-execution-result-summary-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomatedExecutionResultSummaryPanelComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

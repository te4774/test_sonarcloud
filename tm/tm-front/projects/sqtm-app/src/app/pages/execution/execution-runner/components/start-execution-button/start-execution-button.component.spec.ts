import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {StartExecutionButtonComponent} from './start-execution-button.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {OnPushComponentTester} from '../../../../../utils/testing-utils/on-push-component-tester';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {ExecutionRunnerNavigationService} from '../../services/execution-runner-navigation.service';
import {SingleExecutionRunnerNavigationService} from '../../services/single-execution-runner-navigation.service';
import {OverlayModule} from '@angular/cdk/overlay';

class StartExecutionButtonTester extends OnPushComponentTester<StartExecutionButtonComponent> {

  getStartButton() {
    return this.element('div');
  }
}

describe('StartExecutionButtonComponent', () => {
  let tester: StartExecutionButtonTester;
  let fixture: ComponentFixture<StartExecutionButtonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, RouterTestingModule, OverlayModule],
      declarations: [StartExecutionButtonComponent],
      providers: [
        SingleExecutionRunnerNavigationService,
        {
          provide: ExecutionRunnerNavigationService,
          useClass: SingleExecutionRunnerNavigationService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartExecutionButtonComponent);
    tester = new StartExecutionButtonTester(fixture);
    tester.componentInstance.executionId = 1;
    fixture.detectChanges();
  });

  it('should show start execution button', () => {
    expect(tester.getStartButton()).toBeTruthy();
  });
});

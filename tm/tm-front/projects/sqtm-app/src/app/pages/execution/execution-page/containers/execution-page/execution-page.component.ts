import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit,} from '@angular/core';
import {
  CapsuleInformationData,
  DialogService,
  ExecutionStatus,
  InterWindowCommunicationService,
  RestService,
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {ExecutionPageService} from '../../services/execution-page.service';
import {APP_BASE_HREF, DatePipe} from '@angular/common';
import {AbstractExecutionPageComponent} from '../abstract-execution-page.component';
import {ExecutionRunnerOpenerService} from '../../../execution-runner/services/execution-runner-opener.service';

@Component({
  selector: 'sqtm-app-execution-page',
  templateUrl: './execution-page.component.html',
  styleUrls: ['./execution-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionPageComponent extends AbstractExecutionPageComponent implements OnInit, OnDestroy {

   constructor(executionPageService: ExecutionPageService,
              translateService: TranslateService,
              datePipe: DatePipe,
              interWindowCommunicationService: InterWindowCommunicationService,
              @Inject(APP_BASE_HREF)  baseUrl: string,
              cdRef: ChangeDetectorRef,
              dialogService: DialogService,
              restService: RestService,
              executionRunnerOpenerService: ExecutionRunnerOpenerService) {
      super(executionPageService, translateService, datePipe, interWindowCommunicationService,
        baseUrl, cdRef, dialogService, restService, executionRunnerOpenerService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  getExecutionStatus(statusKey: string): CapsuleInformationData {
    const executionStatus = ExecutionStatus[statusKey];
    return {
      id: executionStatus.id,
      color: executionStatus.color,
      icon: executionStatus.icon,
      labelI18nKey: executionStatus.i18nKey,
      titleI18nKey: 'sqtm-core.entity.execution.status.long-label'
    };
  }
}



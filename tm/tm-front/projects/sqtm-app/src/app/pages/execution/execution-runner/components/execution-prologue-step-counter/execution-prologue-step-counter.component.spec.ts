import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ExecutionPrologueStepCounterComponent} from './execution-prologue-step-counter.component';
import {ComponentTester} from 'ngx-speculoos';
import {NO_ERRORS_SCHEMA} from '@angular/core';

class ExecutionPrologueStepCounterTester extends ComponentTester<ExecutionPrologueStepCounterComponent> {

  getStepCount(): string {
    return this.element('span').textContent;
  }
}

describe('ExecutionPrologueStepCounterComponent', () => {
  let tester: ExecutionPrologueStepCounterTester;
  let fixture: ComponentFixture<ExecutionPrologueStepCounterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutionPrologueStepCounterComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPrologueStepCounterComponent);
    tester = new ExecutionPrologueStepCounterTester(fixture);
    tester.componentInstance.stepCount = 3;
    fixture.detectChanges();
  });

  it('should show step count', () => {
    expect(tester.getStepCount()).toEqual('0/3');
  });
});

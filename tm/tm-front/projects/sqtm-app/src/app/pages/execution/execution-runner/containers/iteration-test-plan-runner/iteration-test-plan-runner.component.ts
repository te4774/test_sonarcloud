import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {ExecutionRunnerNavigationService} from '../../services/execution-runner-navigation.service';
import {take, withLatestFrom} from 'rxjs/operators';
import {IterationTestPlanRunnerNavigationService} from '../../services/iteration-test-plan-runner-navigation.service';

@Component({
  selector: 'sqtm-app-iteration-test-plan-runner',
  templateUrl: './iteration-test-plan-runner.component.html',
  styleUrls: ['./iteration-test-plan-runner.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    IterationTestPlanRunnerNavigationService,
    {
      provide: ExecutionRunnerNavigationService,
      useExisting: IterationTestPlanRunnerNavigationService
    }
  ]
})
export class IterationTestPlanRunnerComponent implements OnInit, OnDestroy {

  constructor(private activatedRouteSnapshot: ActivatedRoute, private runnerNavigationService: IterationTestPlanRunnerNavigationService) {
  }

  ngOnInit(): void {
    this.activatedRouteSnapshot.paramMap.pipe(
      take(1),
      withLatestFrom(this.activatedRouteSnapshot.queryParamMap)
    ).subscribe(([paramMap, queryParamMap]: [ParamMap, ParamMap]) => {
      const iterationId = Number.parseInt(paramMap.get('iterationId'), 10);
      const testPlanItemId = Number.parseInt(paramMap.get('testPlanItemId'), 10);
      const rawParam = queryParamMap.get('hasNextTestCase');
      const hasNextTestCase = rawParam && rawParam.toLowerCase() === 'true';
      const partialTestPlanItemIds = JSON.parse(queryParamMap.get('partialTestPlanItemIds'));
      this.runnerNavigationService.changeTestPlanItem(iterationId, testPlanItemId, hasNextTestCase, partialTestPlanItemIds);
    });
  }

  ngOnDestroy(): void {
    this.runnerNavigationService.complete();
  }

}

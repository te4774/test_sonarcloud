import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
} from '@angular/core';
import {ExecutionState, ExecutionStepState} from '../../../states/execution-state';
import {
  Attachment,
  BugTracker,
  CampaignPermissions,
  CustomField,
  CustomFieldBindingData,
  CustomFieldValue,
  DataRow,
  DialogService,
  EditableRichTextComponent,
  ExecutionStatus,
  ExecutionStatusKeys,
  PersistedAttachment,
  RejectedAttachment,
} from 'sqtm-core';
import {ExecutionPageService} from '../../services/execution-page.service';
import {TranslateService} from '@ngx-translate/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {filter, map, switchMap, takeUntil} from 'rxjs/operators';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'sqtm-app-execution-step',
  templateUrl: './execution-step.component.html',
  styleUrls: ['./execution-step.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionStepComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input()
  step: ExecutionStepState;

  @Input()
  execution: ExecutionState;

  @Input()
  permissions: CampaignPermissions;

  @Input()
  milestonesAllowModification: boolean;

  @Input()
  bugtracker: BugTracker;

  @Input()
  authenticated: boolean;

  @Input()
  gridId: string;

  @Input()
  issues: DataRow[];

  private _executionStepCustomFieldBindingData: CustomFieldBindingData[];

  get executionStepCustomFields(): CustomField[] {
    return this._executionStepCustomFieldBindingData.map(cufBindingData => cufBindingData.customField);
  }

  @Input()
  set executionStepCustomFieldBindingData(value: CustomFieldBindingData[]) {
    if (!Boolean(this._executionStepCustomFieldBindingData)) {
      this._executionStepCustomFieldBindingData = value;
    }
  }

  @Input()
  initialFraction: number;

  @Input()
  disabledExecutionStatus: ExecutionStatusKeys[] = [];

  @ViewChild('grid', {read: ElementRef, static: true})
  grid: ElementRef;

  @ViewChild('header', {read: ElementRef, static: true})
  headerAction: ElementRef;

  @ViewChild('dropFileZone', {read: ElementRef})
  private dropFileZone: ElementRef;

  @ViewChild('commentField', {read: EditableRichTextComponent})
  commentField: EditableRichTextComponent;

  @Output()
  resizeActionStepColumn = new EventEmitter<number>();

  @Output()
  refreshIssueData = new EventEmitter<void>();

  executionStatus = ExecutionStatus;
  actionColumnWidth = 0;
  fraction = 1.3;

  private _forceCommentFieldDisplay: BehaviorSubject<boolean>;
  forceCommentFieldDisplay$: Observable<boolean>;
  unsub$ = new Subject<void>();

  constructor( private renderer: Renderer2,
               private sanitizer: DomSanitizer,
               private translateService: TranslateService,
               private executionPageService: ExecutionPageService,
               private dialogService: DialogService) {
    this._forceCommentFieldDisplay = new BehaviorSubject<boolean>(false);
    this.forceCommentFieldDisplay$ = this._forceCommentFieldDisplay.pipe(
      takeUntil(this.unsub$),
      map(editCommentField => editCommentField)
    );
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  ngAfterViewInit(): void {
    this.actionColumnWidth = (this.headerAction.nativeElement as HTMLDivElement).clientWidth;
    if (this.initialFraction) {
      this.fraction = this.initialFraction;
      this.setFaction(this.initialFraction);
    }
  }

  setFaction(nextFraction: number) {
    this.fraction = nextFraction;
    if (this.execution.kind === 'STANDARD') {
      this.renderer.setStyle(this.grid.nativeElement, 'grid-template-columns', `${this.fraction}fr 1fr 120px`);
    }
  }

  getExecutionStepGridClass() {
    return this.execution.kind === 'STANDARD' ? 'standard-execution-step-main-grid' : 'other-execution-step-main-grid';
  }

  handleResize($event: number) {
    const gridWidth = (this.grid.nativeElement as HTMLDivElement).clientWidth;
    const divideFactor = gridWidth / 4;
    const nextFraction: number = this.fraction * (1 + ($event / divideFactor));
    if (nextFraction > 0.25 && nextFraction < 4) {
      this.resizeActionStepColumn.emit(nextFraction);
    }
  }

  getExecutionStepLeftBorderColor(statusKey: string) {
    const executionStatus = ExecutionStatus[statusKey];
    return {'border-left': `5px solid ${executionStatus.color}`};
  }

  getExecutionStepBackgroundColor(statusKey: string) {
    const executionStatus = ExecutionStatus[statusKey];
    return {'background-color': `${executionStatus.backgroundColor}`};
  }

  get action() {
    return this.sanitizer.bypassSecurityTrustHtml(this.step.action);
  }

  get expectedResult() {
    return this.sanitizer.bypassSecurityTrustHtml(this.step.expectedResult);
  }

  getCfvValue(cufData: CustomField): string | string[] {
    const cfv = this.getCfv(cufData);
    if (Boolean(cfv)) {
      return cfv.value;
    }
    return '';
  }

  private getCfv(customField: CustomField): CustomFieldValue {
    const cufId: number = customField.id;
    const cfvs: CustomFieldValue[] = Object.values(this.step.customFieldValues.entities);
    return cfvs.find(candidate => candidate.cufId === cufId);
  }

  updateCustomFieldValue(customField: CustomField, value: string | string[]) {
    const cfv = this.getCfv(customField);
    this.executionPageService.updateStepCustomFieldValue(this.step.id, cfv.id, value).subscribe();
  }

  addComment() {
    this._forceCommentFieldDisplay.next(true);
    setTimeout(() => this.commentField.enableEditMode());
  }

  changeComment(comment: string) {
    this.executionPageService.updateExecutionStepComment(this.step.id, comment).subscribe( () => {
      this.removeCommentFieldForceDisplay();
    });
  }

  getAttachments(step: ExecutionStepState): Attachment[] {
    return Object.values(step.attachmentList.attachments.entities);
  }

  addAttachment(files: File[]) {
    if (this.milestonesAllowModification) {
      this.executionPageService.addAttachmentsToStep(files, this.step.id, this.step.attachmentList.id).subscribe();
    }
    this.unmarkAsFileDropZone();
  }

  deleteAttachment(attachment: PersistedAttachment, stepId: number) {
    this.executionPageService.markAttachmentStepToDelete( stepId, [attachment.id]);
  }

  confirmDeleteAttachmentEvent(attachment: PersistedAttachment, stepId: number, attachmentListId: number) {
    this.executionPageService.deleteStepAttachments([attachment.id], stepId, attachmentListId).subscribe();
  }

  cancelDeleteAttachmentEvent(attachment: PersistedAttachment, stepId: number) {
    this.executionPageService.cancelAttachmentStepToDelete(stepId, [attachment.id]);
  }

  removeRejectedAttachment(attachment: RejectedAttachment, stepId: number) {
    this.executionPageService.removeStepRejectedAttachments([attachment.id], stepId);
  }

  handleAttachmentEnter() {
    this.markAsFileDropZone();
  }

  handleAttachmentLeave() {
    this.unmarkAsFileDropZone();
  }

  private unmarkAsFileDropZone() {
    this.renderer.removeClass(this.dropFileZone.nativeElement, 'file-is-over');
  }

  private markAsFileDropZone() {
    this.renderer.addClass(this.dropFileZone.nativeElement, 'file-is-over');
  }

  refresh() {
    this.refreshIssueData.emit();
  }

  deleteIssue(issueIds: number[]) {
    this.showConfirmDeleteAssociatedIssueDialog().pipe(
      takeUntil(this.unsub$),
      filter(({confirmDelete}) => confirmDelete),
      switchMap(() => {
        return this.executionPageService.removeIssues(issueIds);
      }),
    ).subscribe(() => this.refreshIssueData.emit());
  }

  private showConfirmDeleteAssociatedIssueDialog(): Observable<{ confirmDelete: boolean}> {

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.execution-page.dialog.title.remove-issue.delete-one',
      messageKey: 'sqtm-core.campaign-workspace.execution-page.dialog.message.remove-issue.delete-one-from-step',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete}))
    );
  }

  removeCommentFieldForceDisplay() {
    this._forceCommentFieldDisplay.next(false);
  }
}

import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewContainerRef} from '@angular/core';
import {
  BugTracker,
  CampaignPermissions,
  DialogService,
  ExecutionStatus,
  ExecutionStatusKeys,
  getSupportedBrowserLang
} from 'sqtm-core';
import {ExecutionState, ExecutionStepState} from '../../../states/execution-state';
import {DatePipe} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {ExecutionPageService} from '../../services/execution-page.service';
import {
  getRemoteIssueDialogConfiguration,
  RemoteIssueDialogData
} from '../../../../../components/remote-issue/containers/remote-issue-dialog/remote-issue-dialog.component';

@Component({
  selector: 'sqtm-app-execution-step-header-toolbar',
  templateUrl: './execution-step-header-toolbar.component.html',
  styleUrls: ['./execution-step-header-toolbar.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionStepHeaderToolbarComponent {

  @Input()
  step: ExecutionStepState;

  @Input()
  execution: ExecutionState;

  @Input()
  permissions: CampaignPermissions;

  @Input()
  milestonesAllowModification: boolean;

  @Input()
  bugtracker: BugTracker;

  @Input()
  authenticated: boolean;

  @Input()
  disabledExecutionStatus: ExecutionStatusKeys[] = [];

  @Input()
  issuesNb: number;

  @Output()
  refreshData = new EventEmitter<void>();

  @Output()
  stepToggled = new EventEmitter<void>();

  @Output()
  editCommentField = new EventEmitter<void>();

  @Output()
  addAttachment = new EventEmitter<File[]>();

  executionStatus = ExecutionStatus;

  constructor(private datePipe: DatePipe,
              private translateService: TranslateService,
              private executionPageService: ExecutionPageService,
              private dialogService: DialogService,
              private viewContainerRef: ViewContainerRef) {
  }

  get isActionMenuVisible(): boolean {
    return this.canAddAttachment || this.canAddComment || this.canAddIssue;
  }

  get canAddComment(): boolean {
    return this.permissions.canExecute && this.milestonesAllowModification && !this.step.comment;
  }

  get canAddAttachment(): boolean {
    return this.permissions.canAttach && this.milestonesAllowModification;
  }

  get canAddIssue(): boolean {
    return this.bugtracker
      && this.authenticated
      && this.permissions.canExecute
      && this.milestonesAllowModification;
  }

  getStepStatusI18nKey() {
    return ExecutionStatus[this.step.executionStatus].i18nKey;
  }

  getStepStatusTextColor() {
    return ExecutionStatus[this.step.executionStatus].textColor;
  }

  getExecutionStatusColor(statusKey: string) {
    return ExecutionStatus[statusKey].color;
  }

  getLastExecution(lastExecutedOn: Date, lastExecutedBy: string) {
    const date = this.datePipe.transform(lastExecutedOn, 'short', '', getSupportedBrowserLang(this.translateService));
    const fullLastExecInfo = date + ' (' + lastExecutedBy + ')';

    return this.translateService.instant(fullLastExecInfo);
  }

  isVisible(statusKey: ExecutionStatusKeys) {
    return !this.disabledExecutionStatus.includes(statusKey);
  }

  changeExecutionStepStatus(status: ExecutionStatusKeys) {
    this.executionPageService.changeExecutionStepStatus(this.step.id, status, this.execution.id);
  }

  toggleStep() {
    this.executionPageService.toggleStep(this.step.id);
    this.stepToggled.emit();
  }

  addComment() {
    if (!this.step.extended) {
      this.toggleStep();
    }

    this.editCommentField.next();
  }

  addAttachmentToStep(files: File[]) {
    this.addAttachment.emit(files);
  }

  addIssue(attachMode: boolean): void {
    const dialogConf = getRemoteIssueDialogConfiguration({
      bugTrackerId: this.bugtracker.id,
      squashProjectId: this.execution.projectId,
      boundEntityId: this.step.id,
      bindableEntity: 'EXECUTION_STEP_TYPE',
      attachMode,
    }, this.viewContainerRef);

    const dialogRef = this.dialogService.openDialog<RemoteIssueDialogData, any>(dialogConf);

    dialogRef.dialogClosed$.subscribe(result => {
      if (Boolean(result)) {
        this.refreshData.emit();
        this.executionPageService.refreshIssueCount();
      }
    });
  }
}

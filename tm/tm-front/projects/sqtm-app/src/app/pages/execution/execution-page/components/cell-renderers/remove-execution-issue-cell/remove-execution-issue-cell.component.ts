import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AbstractDeleteCellRenderer, DialogService, GridService} from 'sqtm-core';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {finalize} from 'rxjs/operators';
import {ExecutionPageComponentData} from '../../../containers/abstract-execution-page.component';

@Component({
  selector: 'sqtm-app-remove-execution-issue-cell',
  template: `
    <ng-container *ngIf="componentData$ | async as componentData;">
      <sqtm-core-delete-icon
        *ngIf="componentData.permissions.canExecute"
        [iconName]="getIcon()"
        (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
    </ng-container>
  `,
  styleUrls: ['./remove-execution-issue-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoveExecutionIssueCellComponent extends AbstractDeleteCellRenderer {

  unsub$ = new Subject<void>();
  componentData$: Observable<ExecutionPageComponentData>;

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              dialogService: DialogService,
              private executionPageService: ExecutionPageService) {
    super(grid, cdr, dialogService);
    this.componentData$ = this.executionPageService.componentData$;
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  get executionStepOrders() {
    return this.row.data.executionSteps;
  }

  get isMultiple(): boolean {
    return this.executionStepOrders.length > 1;
  }

  get isExecutionStep(): boolean {
    return this.executionStepOrders[0] >= 0;
  }

  protected doDelete(): void {
    this.grid.beginAsyncOperation();
    const issueIds = this.row.data.issueIds;
    this.executionPageService.removeIssues(issueIds).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.grid.refreshData());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.campaign-workspace.execution-page.dialog.title.remove-issue.delete-one';
  }

  protected getMessageKey(): string {
    if (this.isMultiple) {
      return 'sqtm-core.campaign-workspace.execution-page.dialog.message.remove-issue.delete-one-from-multiple';
    } else if (this.isExecutionStep) {
      return 'sqtm-core.campaign-workspace.execution-page.dialog.message.remove-issue.delete-one-from-step';
    } else {
      return 'sqtm-core.campaign-workspace.execution-page.dialog.message.remove-issue.delete-one';
    }
  }
}

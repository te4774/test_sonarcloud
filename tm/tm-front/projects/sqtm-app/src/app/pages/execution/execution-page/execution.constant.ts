import {InjectionToken} from '@angular/core';
import {GridDefinition, GridService} from 'sqtm-core';

export const EXECUTION_HISTORY_TABLE_CONF =
  new InjectionToken<GridDefinition>('Grid config for the execution history grid');

export const EXECUTION_HISTORY_TABLE =
  new InjectionToken<GridService>('Grid service instance for the execution history table of execution view');

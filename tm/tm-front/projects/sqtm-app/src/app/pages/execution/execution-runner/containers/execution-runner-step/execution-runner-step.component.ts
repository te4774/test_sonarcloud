import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {ExecutionRunnerService} from '../../services/execution-runner.service';
import {ExecutionRunnerComponentData} from '../execution-runner/execution-runner.component';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {catchError, filter, map, switchMap, take, takeUntil} from 'rxjs/operators';
import {ChangeStatusEvent} from '../../components/execution-runner-toolbar/execution-runner-toolbar.component';
import {executionRunnerLogger} from '../../execution-runner.logger';
import {TranslateService} from '@ngx-translate/core';
import {DOCUMENT} from '@angular/common';
import {
  ActionErrorDisplayService,
  DialogService,
  InterWindowCommunicationService,
  InterWindowMessages,
  ModifDuringExecService
} from 'sqtm-core';
import {ExecutionState, ExecutionStepState} from '../../../states/execution-state';
import {ExecutionRunnerNavigationService} from '../../services/execution-runner-navigation.service';
import {ExecutionRunnerOpenerService} from '../../services/execution-runner-opener.service';

const logger = executionRunnerLogger.compose('ExecutionRunnerStepComponent');

@Component({
  selector: 'sqtm-app-execution-runner-step',
  templateUrl: './execution-runner-step.component.html',
  styleUrls: ['./execution-runner-step.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionRunnerStepComponent implements OnInit, OnDestroy {

  activateFastForwardButton$: Observable<boolean>;

  componentData$: Observable<ExecutionRunnerComponentData>;

  private unsub$ = new Subject<void>();

  constructor(private executionRunnerService: ExecutionRunnerService,
              private route: ActivatedRoute,
              private router: Router,
              private translateService: TranslateService,
              private dialogService: DialogService,
              @Inject(DOCUMENT) private document: Document,
              private executionRunnerNavigationService: ExecutionRunnerNavigationService,
              private interWindowCommunicationService: InterWindowCommunicationService,
              private executionRunnerOpenerService: ExecutionRunnerOpenerService,
              private modifDuringExecService: ModifDuringExecService,
              private actionErrorDisplayService: ActionErrorDisplayService) {
  }

  ngOnInit() {
    this.initializeComponentData();
    this.initializeFastForwardButton();
    this.navigateToInitialStep();
    this.componentData$.pipe(
      take(1)
    ).subscribe(data => {
      this.changeDocumentTitle(data.execution);
    });
  }

  private initializeFastForwardButton() {
    this.activateFastForwardButton$ = this.executionRunnerNavigationService.activateFastForwardButton$;
  }

  private navigateToInitialStep() {
    this.route.paramMap
      .pipe(
        take(1),
        map(param => param.get('stepIndex'))
      ).subscribe((stepIndex: string) => {
      const orderIndex = Number.parseInt(stepIndex, 10) - 1;
      this.executionRunnerService.navigateToStep(orderIndex);
    });
  }

  navigateToPrologue(executionId: number) {
    this.executionRunnerNavigationService.navigateToPrologue(executionId);
  }

  navigateToStep(event: { executionId: number, stepIndex: number }) {
    this.executionRunnerNavigationService.navigateToStep(event);
  }

  private initializeComponentData() {
    this.componentData$ = this.executionRunnerService.componentData$.pipe(
      takeUntil(this.unsub$),
    );
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  changeExecutionStepStatus($event: ChangeStatusEvent) {
    this.executionRunnerService
      .changeExecutionStepStatus($event.executionStepId, $event.executionStatus, $event.executionId)
      .subscribe(changeStatusEvent => {
        logger.debug('Checking if must navigate after status change ', [changeStatusEvent]);
        if (!changeStatusEvent.isLastStep) {
          this.navigateToStep({
            executionId: $event.executionId,
            stepIndex: changeStatusEvent.nextStepIndex
          });
        } else {
          this.executionRunnerNavigationService.handleNextOnLastStep();
        }
      });
  }

  getCurrentStep(execution: ExecutionState): ExecutionStepState {
    const currentStepIndex = execution.currentStepIndex;
    const id = execution.executionSteps.ids[currentStepIndex];
    return execution.executionSteps.entities[id];
  }

  private changeDocumentTitle(executionState: ExecutionState) {
    let title = `#${executionState.executionOrder + 1} - ${executionState.name}`;
    if (executionState.datasetLabel) {
      title = title.concat(` - ${executionState.datasetLabel}`);
    }
    title = title.concat(` (${executionState.currentStepIndex + 1}/${executionState.executionSteps.ids.length})`);
    this.document.title = title;
  }

  addAttachmentToStep(files: File[], currentStep: ExecutionStepState) {
    this.componentData$.pipe(
      take(1),
      filter(componentData => componentData.milestonesAllowModification),
      switchMap(() => this.executionRunnerService.addAttachmentsToStep(files, currentStep.id, currentStep.attachmentList.id))
    ).subscribe();
  }

  fastForwardToNextExecution() {
    this.executionRunnerNavigationService.handleNextOnLastStep();
  }

  navigateToModifStepDuringExecution($event: { executionId: number; executionStepId: number }) {
    this.modifDuringExecService.checkPermissions($event.executionId, $event.executionStepId).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err))
    ).subscribe(() => {
      if (this.interWindowCommunicationService.isAbleToSendMessagesToOpener()) {
        const interWindowMessage = new InterWindowMessages('REQUIRE-MODIFICATION-DURING-EXECUTION-STEP', $event);
        this.interWindowCommunicationService.sendMessage(interWindowMessage);
      } else {
        this.executionRunnerOpenerService.openModifyRunningExecutionFromStep($event.executionId, $event.executionStepId);
      }
      window.close();
    });
  }
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';
import {AbstractDeleteCellRenderer, DialogService, GridService, RestService} from 'sqtm-core';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-remove-execution-step-issue-cell',
  template: `
    <sqtm-core-delete-icon
      [iconName]="getIcon()"
      (delete)="showDeleteConfirm()">
    </sqtm-core-delete-icon>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoveExecutionStepIssueCellComponent extends AbstractDeleteCellRenderer implements OnDestroy {

  unsub$ = new Subject<void>();

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              dialogService: DialogService,
              public readonly restService: RestService) {
    super(grid, cdr, dialogService);
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  protected doDelete(): void {
    this.grid.beginAsyncOperation();
    const issueIds = this.row.data.issueIds;
    this.restService.delete(['issues', issueIds]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.grid.refreshData());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.campaign-workspace.execution-page.dialog.title.remove-issue.delete-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.campaign-workspace.execution-page.dialog.message.remove-issue.delete-one-from-step';
  }
}

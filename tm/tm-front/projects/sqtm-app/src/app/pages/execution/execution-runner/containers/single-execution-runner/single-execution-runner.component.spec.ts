import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SingleExecutionRunnerComponent} from './single-execution-runner.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('SingleExecutionRunnerComponent', () => {
  let component: SingleExecutionRunnerComponent;
  let fixture: ComponentFixture<SingleExecutionRunnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleExecutionRunnerComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleExecutionRunnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

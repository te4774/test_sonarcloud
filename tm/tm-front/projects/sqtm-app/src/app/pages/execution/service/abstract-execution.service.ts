import {
  attachmentEntityAdapter,
  AttachmentService,
  AttachmentState,
  CampaignPermissions,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ExecutionModel,
  ExecutionService,
  ExecutionStepModel,
  ExecutionStepService,
  GridService,
  InterWindowCommunicationService,
  InterWindowMessages,
  ProjectData,
  ReferentialDataService,
  RestService,
  StoreOptions,
  UploadAttachmentEvent
} from 'sqtm-core';

import {TranslateService} from '@ngx-translate/core';
import {concatMap, map, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';
import {ExecutionState, executionStepAdapter, ExecutionStepState} from '../states/execution-state';
import {Observable, Subject} from 'rxjs';
import {ExecutionRunnerState} from '../execution-runner/state/execution-runner-state';
import {Update} from '@ngrx/entity';
import {executionRunnerLogger} from '../execution-runner/execution-runner.logger';

const logger = executionRunnerLogger.compose('AbstractExecutionService');

export abstract class AbstractExecutionService extends EntityViewService<ExecutionState, 'execution', CampaignPermissions> {

  /*
   * This observable is required because we need to reload when execution windows communicate with us
   * and thus we need to manually detect change because Angular detect change seems to not occurs naturally when the windows loose focus...
   */
  loaded$: Observable<void>;
  private _loaded: Subject<void>;

  protected constructor(protected restService: RestService,
                        protected referentialDataService: ReferentialDataService,
                        protected attachmentService: AttachmentService,
                        protected translateService: TranslateService,
                        protected customFieldValueService: CustomFieldValueService,
                        protected attachmentHelper: EntityViewAttachmentHelperService,
                        protected customFieldHelper: EntityViewCustomFieldHelperService,
                        protected executionService: ExecutionService,
                        protected coverageTable: GridService,
                        protected storeOptions: StoreOptions,
                        protected interWindowCommunicationService: InterWindowCommunicationService,
                        protected executionStepService: ExecutionStepService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
      storeOptions
    );

    this._loaded = new Subject();
    this.loaded$ = this._loaded.asObservable();
  }

  load(executionId: number) {
    this.executionService.fetchExecutionData(executionId).pipe(
      withLatestFrom(this.state$),
      map(([executionModel, state]) => this.loadExecutionModel(executionModel, state)),
    ).subscribe(state => {
      this.commit(state);
      this._loaded.next();
    }, err => this.notifyEntityNotFound(err));
  }

  complete() {
    super.complete();
    this.coverageTable.complete();
  }

  private loadExecutionModel(executionModel: ExecutionModel, state) {
    const executionSteps: ExecutionStepState[] = executionModel.executionStepViews.map((executionStepModel: ExecutionStepModel) => {
      return {
        ...executionStepModel,
        attachmentList: this.initializeAttachmentListState(executionStepModel.attachmentList),
        customFieldValues: this.initializeCustomFieldValueState(executionStepModel.customFieldValues),
        extended: false
      };
    });

    const executionStepsState = executionStepAdapter.setAll(executionSteps, executionStepAdapter.getInitialState());
    const customFieldValueState = this.initializeCustomFieldValueState(executionModel.customFieldValues);

    const executionState: ExecutionState = {
      ...state.execution,
      ...executionModel,
      attachmentList: this.initializeAttachmentListState(executionModel.attachmentList),
      executionSteps: executionStepsState,
      customFieldValues: customFieldValueState,
      extendedPrerequisite: false,
      scrollTop: 0,
    };
    this.coverageTable.loadInitialData(executionState.coverages, executionState.coverages.length, 'requirementVersionId');
    return this.updateEntity(executionState, state);
  }

  addSimplePermissions(projectData: ProjectData): CampaignPermissions {
    return new CampaignPermissions(projectData);
  }

  updateStepCustomFieldValue(executionStepId: number, cfvId: number, value: string | string[]) {
    return this.store.state$.pipe(
      take(1),
      concatMap(state => this.writeStepCustomFieldValue(executionStepId, cfvId, value, state)),
      withLatestFrom(this.store.state$),
      map(([, initialState]) => this.updateStateStepCustomFieldValue(executionStepId, cfvId, value, initialState)),
      tap(state => this.commit(state)),
      tap(() => {
        this.interWindowCommunicationService.sendMessage(new InterWindowMessages('EXECUTION-STEP-CHANGED'));
      }),
    );
  }

  writeStepCustomFieldValue(stepId: number, cufValueId: number, value: string | string[], initialState: Readonly<ExecutionRunnerState>):
    Observable<string | string[]> {
    const executionStep = initialState.execution.executionSteps.entities[stepId] as ExecutionStepState;
    const customFieldValue = executionStep.customFieldValues.entities[cufValueId];
    return this.customFieldHelper.writeCustomFieldValue(customFieldValue, value);
  }

  private updateStateStepCustomFieldValue(
    stepId: number,
    cufValueId: number,
    value: string | string[],
    initialState: Readonly<ExecutionRunnerState>): ExecutionRunnerState {
    const executionStep = initialState.execution.executionSteps.entities[stepId] as ExecutionStepState;
    const customFieldValueState = this.customFieldHelper.updateCustomFieldValue(cufValueId, value, executionStep.customFieldValues);
    const changes: Update<ExecutionStepState> = {
      id: executionStep.id,
      changes: {customFieldValues: customFieldValueState}
    };
    const executionSteps = executionStepAdapter.updateOne(changes, initialState.execution.executionSteps);
    const execution: ExecutionState = {...initialState.execution, executionSteps};
    return {...initialState, execution};
  }

  updateExecutionStepComment(executionStepId: number, comment: string) {
    return this.executionStepService.changeComment(executionStepId, comment).pipe(
      withLatestFrom(this.state$),
      map(([, state]: [void, ExecutionRunnerState]) => {
        const executionSteps = executionStepAdapter.updateOne({
          id: executionStepId,
          changes: {comment}
        }, state.execution.executionSteps);
        return {...state, execution: {...state.execution, executionSteps}};
      }),
      tap(state => this.commit(state)),
      tap(() => {
        this.interWindowCommunicationService.sendMessage(new InterWindowMessages('EXECUTION-STEP-CHANGED'));
      }),
    );
  }

  addAttachmentsToStep(files: File[], executionStepId: number, attachmentListId: number) {
    if (logger.isDebugEnabled()) {
      logger.debug(`Adding files to step ${executionStepId} : `, [files]);
    }

    return this.state$.pipe(
      take(1),
      switchMap((state: ExecutionRunnerState) => {
        const entity = this.getEntity(state);
        return this.attachmentHelper.addAttachments(files, attachmentListId, entity.id, state.type);
      }),
      withLatestFrom(this.store.state$),
      map(([event, state]: [UploadAttachmentEvent, ExecutionRunnerState]) => {
        this.commit(this.mapUploadEventToState(executionStepId, state, event));
      }),
      tap(() => {
        this.interWindowCommunicationService.sendMessage(new InterWindowMessages('EXECUTION-STEP-CHANGED'));
      })
    );
  }

  mapUploadEventToState(stepId: number, state: ExecutionRunnerState, event: UploadAttachmentEvent): ExecutionRunnerState {
    const actionStep = state.execution.executionSteps.entities[stepId] as ExecutionStepState;
    const attachmentState = this.attachmentHelper.mapUploadEventToState(actionStep.attachmentList.attachments, event);
    return this.updateTestStepAttachmentState(state, attachmentState, stepId);
  }

  deleteStepAttachments(attachmentIds: string[], executionStepId: number, attachmentListId: number) {
    const idsAsNumber = attachmentIds.map(id => parseInt(id, 10));
    return this.state$.pipe(
      take(1),
      switchMap((state: ExecutionRunnerState) => {
        const entity = this.getEntity(state);
        return this.attachmentHelper.eraseAttachments(idsAsNumber, attachmentListId, entity.id, state.type);
      }),
      withLatestFrom(this.store.state$),
      map(([, state]) => {
        if (logger.isDebugEnabled()) {
          logger.debug(`Removing attachments ${JSON.stringify(attachmentIds)} from ${executionStepId}. Initial state :`, [state]);
        }
        const executionStep = {...state.execution.executionSteps.entities[executionStepId]} as ExecutionStepState;
        const attachmentState = attachmentEntityAdapter.removeMany(attachmentIds, executionStep.attachmentList.attachments);
        const nextState = this.updateTestStepAttachmentState(state, attachmentState, executionStepId);
        if (logger.isDebugEnabled()) {
          logger.debug(`Removed attachments ${JSON.stringify(attachmentIds)} from ${executionStepId}. Next state :`, [nextState]);
        }
        return nextState;
      }),
      tap(state => this.commit(state)),
      tap(() => {
        this.interWindowCommunicationService.sendMessage(new InterWindowMessages('EXECUTION-STEP-CHANGED'));
      }),
    );
  }

  markAttachmentStepToDelete(stepId: number, attachmentIds: string[]) {
    this.changeAttachmentStepPendingDelete(stepId, attachmentIds, true);
  }

  cancelAttachmentStepToDelete(stepId: number, attachmentIds: string[]) {
    this.changeAttachmentStepPendingDelete(stepId, attachmentIds, false);
  }

  removeStepRejectedAttachments(attachmentIds: string[], stepId: number) {
    this.state$.pipe(
      take(1),
      map((state: ExecutionRunnerState) => {
        const executionStep = {...state.execution.executionSteps.entities[stepId]} as ExecutionStepState;
        const attachmentState = attachmentEntityAdapter.removeMany(attachmentIds, executionStep.attachmentList.attachments);
        return this.updateTestStepAttachmentState(state, attachmentState, stepId);
      })
    ).subscribe(state => this.commit(state));
  }

  private changeAttachmentStepPendingDelete(stepId: number, attachmentIds: string[], pendingDelete: boolean) {
    this.state$.pipe(
      take(1),
      map((state) => {
        const testStep = {...state.execution.executionSteps.entities[stepId]} as ExecutionStepState;
        const attachmentState = this.changeAttachmentDeleteStatus(testStep.attachmentList.attachments, attachmentIds, pendingDelete);
        return this.updateTestStepAttachmentState(state, attachmentState, stepId);
      }),
    ).subscribe(state => this.commit(state));
  }

  protected updateTestStepAttachmentState(state: ExecutionRunnerState, attachmentState: AttachmentState, stepId: number)
    : ExecutionRunnerState {
    const executionStep = {...state.execution.executionSteps.entities[stepId]} as ExecutionStepState;
    const attachmentList = {...executionStep.attachmentList};
    attachmentList.attachments = attachmentState;
    const changes: Update<ExecutionStepState> = {id: executionStep.id, changes: {attachmentList}};
    const executionSteps = executionStepAdapter.updateOne(changes, state.execution.executionSteps);
    return {...state, execution: {...state.execution, executionSteps}};
  }

  abstract getInitialState();
}

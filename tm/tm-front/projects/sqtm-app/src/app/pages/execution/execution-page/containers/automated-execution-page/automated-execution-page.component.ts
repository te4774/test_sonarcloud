import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {ExecutionPageService} from '../../services/execution-page.service';
import {TranslateService} from '@ngx-translate/core';
import {APP_BASE_HREF, DatePipe} from '@angular/common';
import {
  CapsuleInformationData,
  DialogService,
  ExecutionStatus,
  InterWindowCommunicationService,
  RestService
} from 'sqtm-core';
import {AbstractExecutionPageComponent} from '../abstract-execution-page.component';
import {ExecutionRunnerOpenerService} from '../../../execution-runner/services/execution-runner-opener.service';

@Component({
  selector: 'sqtm-app-automated-execution-page',
  templateUrl: './automated-execution-page.component.html',
  styleUrls: ['./automated-execution-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomatedExecutionPageComponent extends AbstractExecutionPageComponent implements OnInit {

  constructor(executionPageService: ExecutionPageService,
              translateService: TranslateService,
              datePipe: DatePipe,
              interWindowCommunicationService: InterWindowCommunicationService,
              @Inject(APP_BASE_HREF)  baseUrl: string,
              cdRef: ChangeDetectorRef,
              dialogService: DialogService,
              restService: RestService,
              executionRunnerOpenerService: ExecutionRunnerOpenerService) {
    super(executionPageService, translateService, datePipe, interWindowCommunicationService,
      baseUrl, cdRef, dialogService, restService, executionRunnerOpenerService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  getExecutionStatus(statusKey: string): CapsuleInformationData {
    const executionStatus = ExecutionStatus[statusKey];
    return {
      id: executionStatus.id,
      color: executionStatus.color,
      icon: 'sqtm-core-campaign:exec_status',
      labelI18nKey: executionStatus.i18nKey,
      titleI18nKey: 'sqtm-core.entity.execution.status.automated.long-label'
    };
  }

  getExecutionResultUrl(resultUrl: string) {

    return {
      id: 'executionResultUrl',
      color: 'var(--container-border-color)',
      labelI18nKey: resultUrl,
      titleI18nKey: 'sqtm-core.entity.execution.automated.result-url'
    };
  }

  getAutomatedJobUrl(automatedJobUrl: string) {
    return {
      id: 'automatedJobUrl',
      color: 'var(--container-border-color)',
      labelI18nKey: automatedJobUrl,
      titleI18nKey: 'sqtm-core.entity.execution.automated.job-url'
    };
  }

}

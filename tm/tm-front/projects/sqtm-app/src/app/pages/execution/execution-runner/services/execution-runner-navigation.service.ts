import {Router} from '@angular/router';
import {DialogService} from 'sqtm-core';
import {filter} from 'rxjs/operators';
import {Observable} from 'rxjs';

export abstract class ExecutionRunnerNavigationService {

  abstract activateFastForwardButton$: Observable<boolean>;

  protected constructor(protected router: Router, protected dialogService: DialogService) {
  }

  navigateToStep(event: { executionId: number, stepIndex: number }) {
    this.router.navigate([...this.getUrlRoot(), 'execution', event.executionId, 'step', event.stepIndex + 1],
      {queryParams: {hasNextTestCase: this.hasNextTestCase()}});
  }

  protected abstract getUrlRoot(): string[];

  abstract hasNextTestCase(): boolean;

  abstract getPartialTestPlanItemIds(): number[];

  protected abstract getEndOfExecutionKey(): string;

  abstract handleNextOnLastStep();

  navigateToPrologue(executionId: number) {
    this.router.navigate([...this.getUrlRoot(), 'execution', executionId, 'prologue'],
      {
        queryParams: {
          hasNextTestCase: this.hasNextTestCase(),
          partialTestPlanItemIds: JSON.stringify(this.getPartialTestPlanItemIds())
        }
      });
  }

  startExecution(executionId: number) {
    this.router.navigate([...this.getUrlRoot(), 'execution', executionId, 'step', 1],
      {queryParams: {hasNextTestCase: this.hasNextTestCase()}});
  }

  protected showEndOfExecutionDialog(): void {
    const dialogRef = this.dialogService.openConfirm({
      id: 'end-of-execution',
      titleKey: 'sqtm-core.entity.generic.information.label',
      messageKey: this.getEndOfExecutionKey(),
      level: 'INFO',
    });

    dialogRef.dialogClosed$.pipe(
      filter(result => Boolean(result))
    ).subscribe(() => this.closeRunner());
  }

  private closeRunner(): void {
    window.close();
  }

}

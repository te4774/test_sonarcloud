import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {
  centredTextColumn,
  convertSqtmLiterals,
  DataRow,
  dateTimeColumn,
  DialogService,
  executionModeColumn,
  executionStatusColumn,
  Extendable,
  Fixed,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  numericColumn,
  ProjectDataMap,
  ReferentialDataService,
  RestService,
  sortDate,
  SquashTmDataRowType,
  StyleDefinitionBuilder,
  testCaseImportanceColumn,
  textColumn,
  withLinkColumn
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {Router} from '@angular/router';
import {concatMap, filter, finalize, map, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {ExecutionPageComponentData} from '../../abstract-execution-page.component';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {
  SuccessRateComponent
} from '../../../../../campaign-workspace/iteration-view/components/cell-renderers/success-rate/success-rate.component';
import {DatePipe} from '@angular/common';
import {EXECUTION_HISTORY_TABLE, EXECUTION_HISTORY_TABLE_CONF} from '../../../execution.constant';
import {
  deleteExecutionPageHistoryColumn
} from '../../../components/cell-renderers/delete-execution-page-history/delete-execution-page-history.component';
import {ExecutionPageComponent} from '../../execution-page/execution-page.component';

@Component({
  selector: 'sqtm-app-execution-page-history',
  templateUrl: './execution-page-history.component.html',
  styleUrls: ['./execution-page-history.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: EXECUTION_HISTORY_TABLE_CONF,
      useFactory: executionHistoryTableDefinition
    },
    {
      provide: EXECUTION_HISTORY_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, EXECUTION_HISTORY_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: EXECUTION_HISTORY_TABLE
    }
  ]
})
export class ExecutionPageHistoryComponent implements OnInit, OnDestroy {

  componentData$: Observable<ExecutionPageComponentData>;
  unsub$ = new Subject<void>();
  canDelete$: Observable<boolean>;


  constructor(private gridService: GridService,
              private restService: RestService,
              private dialogService: DialogService,
              private router: Router,
              private executionPageService: ExecutionPageService,
              private executionPageComponent: ExecutionPageComponent,
  ) {
  }

  ngOnInit(): void {
    this.componentData$ = this.executionPageService.componentData$;
    this.componentData$.pipe(
      take(1)
    ).subscribe(componentData => {
      const iterationId = componentData.execution.iterationId;
      const testPlanItemId = componentData.execution.testPlanItemId;
      this.fetchExecutionHistory(iterationId, testPlanItemId);
    });
    this.canDelete$ = this.gridService.selectedRows$.pipe(
      takeUntil(this.unsub$),
      map(rows => this.canDelete(rows)));
  }

  private fetchExecutionHistory(iterationId, testPlanItemId) {
    this.gridService.setServerUrl([`iteration/${iterationId}/test-plan/${testPlanItemId}/executions`]);
  }

  private canDelete(rows: DataRow[]) {
    return rows.length > 0
      && !rows.some(row => row.data.boundToBlockingMilestone)
      && !rows.some(row => !row.simplePermissions.canDelete);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  showMassDeleteExecutionDialog() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => this.canDelete(rows)),
      concatMap((rows: DataRow[]) => this.openDeleteExecutionDialog(rows)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      withLatestFrom(this.executionPageService.componentData$),
      concatMap(([{rows}, componentData]) => this.removeExecutionsServerSide(rows, componentData)),
      finalize(() => this.gridService.completeAsyncOperation())
    ).subscribe(() => this.gridService.refreshData());
  }

  private openDeleteExecutionDialog(rows): Observable<{ confirmDelete: boolean, rows: string[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.mass-remove-execution',
      messageKey: 'sqtm-core.campaign-workspace.dialog.message.mass-remove-execution',
      level: 'DANGER'
    });
    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, rows}))
    );
  }

  private removeExecutionsServerSide(rows, componentData): Observable<any> {
    const rowIds = rows.map(row => row.data['executionId']);
    if (this.isCurrentDisplayedExecutionInSelection(rowIds, componentData.execution.id)) {
      return this.removeExecutionsServerSideWithRedirection(rowIds, componentData.execution.iterationId);
    } else {
      return this.removeExecutionsServerSideWithoutRedirection(rowIds, componentData.execution.iterationId);
    }
  }

  private isCurrentDisplayedExecutionInSelection(rowIds, executionId): boolean {
    return rowIds.includes(executionId);
  }

  private removeExecutionsServerSideWithRedirection(rowIds, iterationId): Observable<any> {
    return this.restService.delete<{ nbIssues: number }>(
      ['iteration', iterationId.toString(), 'test-plan/execution', rowIds.join(',')])
      .pipe(
        tap(() => this.executionPageComponent.executionDeleted.emit())
      );
  }

  private removeExecutionsServerSideWithoutRedirection(rowIds, iterationId): Observable<any> {
    return this.restService.delete<{ nbIssues: number }>(
      ['iteration', iterationId.toString(), 'test-plan/execution', rowIds.join(',')])
      .pipe(
        tap(() => this.executionPageComponent.refreshData())
      );
  }
}

function executionConverter(literals: Partial<DataRow>[], projectDataMap: ProjectDataMap): DataRow[] {
  const executions = literals.map(li => ({
    ...li,
    type: SquashTmDataRowType.Execution,
    data: {
      ...li.data,
      executionOrder: li.data.executionOrder + 1,
    }
  }));
  return convertSqtmLiterals(executions, projectDataMap);
}


export function executionHistoryTableDefinition(): GridDefinition {
  return grid('execution-page-history')
    .withColumns([
      indexColumn(),
      withLinkColumn('executionOrder', {
        kind: 'link',
        baseUrl: '/execution',
        columnParamId: 'executionId',
        saveGridStateBeforeNavigate: true
      })
        .withI18nKey('sqtm-core.entity.execution-plan.execution-number.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.execution-number.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      executionModeColumn('executionMode')
        .withI18nKey('sqtm-core.entity.execution.mode.label')
        .withTitleI18nKey('sqtm-core.entity.execution.mode.label')
        .changeWidthCalculationStrategy(new Fixed(60)),
      textColumn('executionName')
        .withI18nKey('sqtm-core.entity.name')
        .changeWidthCalculationStrategy(new Extendable(150, 0.2)),
      testCaseImportanceColumn('importance')
        .withI18nKey('sqtm-core.entity.test-case.importance.label-short-dot')
        .withTitleI18nKey('sqtm-core.entity.test-case.importance.label')
        .isEditable(false)
        .changeWidthCalculationStrategy(new Fixed(80)),
      textColumn('datasetName')
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Fixed(100)),
      centredTextColumn('successRate')
        .withRenderer(SuccessRateComponent)
        .withI18nKey('sqtm-core.entity.execution-plan.success-rate.label.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.success-rate.label.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      executionStatusColumn('executionStatus')
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .withTitleI18nKey('sqtm-core.entity.execution.status.long-label')
        .changeWidthCalculationStrategy(new Fixed(60)),
      numericColumn('issueCount')
        .withI18nKey('sqtm-core.entity.execution-plan.ano-number.short')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.ano-number.long')
        .changeWidthCalculationStrategy(new Fixed(60)),
      textColumn('user')
        .withI18nKey('sqtm-core.generic.label.user')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      dateTimeColumn('lastExecutedOn')
        .withSortFunction(sortDate)
        .withI18nKey('sqtm-core.entity.execution-plan.last-execution.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.last-execution.label.long')
        .changeWidthCalculationStrategy(new Extendable(130, 0.2)),
      deleteExecutionPageHistoryColumn('delete').withLabel('').disableSort().changeWidthCalculationStrategy(new Fixed(30))
    ]).server()
    .withRowConverter(executionConverter)
    .disableRightToolBar()
    .withStyle(new StyleDefinitionBuilder()
      .enableInitialLoadAnimation()
      .showLines())
    .withRowHeight(35)
    .build();
}

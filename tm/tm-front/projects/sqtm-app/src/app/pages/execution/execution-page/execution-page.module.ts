import {NgModule, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ExecutionPageComponent} from './containers/execution-page/execution-page.component';
import {
  AnchorModule,
  AttachmentModule,
  CellRendererCommonModule,
  CustomFieldModule,
  DialogModule,
  ExecutionUiModule,
  GridExportModule,
  GridModule,
  IssuesModule,
  NavBarModule,
  ReferentialDataService,
  SqtmDragAndDropModule,
  SvgModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule
} from 'sqtm-core';
import {NzAnchorModule} from 'ng-zorro-antd/anchor';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCollapseModule} from 'ng-zorro-antd/collapse';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzFormModule} from 'ng-zorro-antd/form';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CKEditorModule} from 'ckeditor4-angular';
import {TranslateModule} from '@ngx-translate/core';
import {ExecutionPageLevelTwoComponent} from './containers/execution-page-level-two/execution-page-level-two.component';
import {
  ExecutionPageContentComponent
} from './containers/panel-groups/execution-page-content/execution-page-content.component';
import {
  ExecutionPageInformationPanelComponent
} from './components/panels/execution-page-information-panel/execution-page-information-panel.component';
import {ExecutionModule} from '../../../components/execution/execution.module';
import {
  ExecutionPageCoveragePanelComponent
} from './components/panels/execution-page-coverage-panel/execution-page-coverage-panel.component';
import {
  AutomatedExecutionPageComponent
} from './containers/automated-execution-page/automated-execution-page.component';
import {
  ExecutionPageTypeSelectorComponent
} from './containers/execution-page-type-selector/execution-page-type-selector.component';
import {
  ExecutionPageCommentsPanelComponent
} from './components/panels/execution-page-comments-panel/execution-page-comments-panel.component';
import {
  ExecutionPageIssuesPanelComponent
} from './components/panels/execution-page-issues-panel/execution-page-issues-panel.component';
import {
  RemoveExecutionIssueCellComponent
} from './components/cell-renderers/remove-execution-issue-cell/remove-execution-issue-cell.component';
import {RemoteIssueModule} from '../../../components/remote-issue/remote-issue.module';
import {
  IssueReportedInCellComponent
} from './components/cell-renderers/issue-reported-in-cell/issue-reported-in-cell.component';
import {
  ExecutionPageScenarioComponent
} from './containers/panel-groups/execution-page-scenario/execution-page-scenario.component';
import {
  ExecutionPageScenarioPanelComponent
} from './components/panels/execution-page-scenario-panel/execution-page-scenario-panel.component';
import {ExecutionStepComponent} from './components/execution-step/execution-step.component';
import {
  ExecutionStepHeaderToolbarComponent
} from './components/execution-step-header-toolbar/execution-step-header-toolbar.component';
import {
  AutomatedExecutionResultSummaryPanelComponent
} from './components/panels/automated-execution-result-summary-panel/automated-execution-result-summary-panel.component';
import {
  ExecutionPageHistoryComponent
} from './containers/panel-groups/execution-page-history/execution-page-history.component';
import {
  DeleteExecutionPageHistoryComponent
} from './components/cell-renderers/delete-execution-page-history/delete-execution-page-history.component';

export const routes: Routes = [
  {
    path: ':executionId', component: ExecutionPageLevelTwoComponent, children: [
      {path: '', redirectTo: 'content'},
      {path: 'content', component: ExecutionPageContentComponent},
      {path: 'scenario', component: ExecutionPageScenarioComponent},
      {path: 'history', component: ExecutionPageHistoryComponent}
    ]
  }
];

@NgModule({
  declarations: [
    ExecutionPageComponent,
    ExecutionPageLevelTwoComponent,
    ExecutionPageContentComponent,
    ExecutionPageInformationPanelComponent,
    ExecutionPageCoveragePanelComponent,
    AutomatedExecutionPageComponent,
    ExecutionPageTypeSelectorComponent,
    ExecutionPageCommentsPanelComponent,
    ExecutionPageIssuesPanelComponent,
    RemoveExecutionIssueCellComponent,
    IssueReportedInCellComponent,
    ExecutionPageScenarioComponent,
    ExecutionPageHistoryComponent,
    ExecutionPageScenarioPanelComponent,
    ExecutionStepComponent,
    ExecutionStepHeaderToolbarComponent,
    AutomatedExecutionResultSummaryPanelComponent,
    DeleteExecutionPageHistoryComponent,
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SvgModule,
        WorkspaceCommonModule,
        UiManagerModule,
        NavBarModule,
        GridModule,
        CellRendererCommonModule,
        FormsModule,
        ReactiveFormsModule,
        DialogModule,
        WorkspaceLayoutModule,
        NzDropDownModule,
        CKEditorModule,
        TranslateModule.forChild(),
        AttachmentModule,
        NzButtonModule,
        NzCollapseModule,
        NzIconModule,
        NzToolTipModule,
        NzMenuModule,
        NzFormModule,
        NzAnchorModule,
        AnchorModule,
        AnchorModule,
        ExecutionModule,
        ExecutionUiModule,
        CustomFieldModule,
        IssuesModule,
        RemoteIssueModule,
        SqtmDragAndDropModule,
        GridExportModule,
    ]
})
export class ExecutionPageModule implements OnInit {

  constructor(private referentialDataService: ReferentialDataService) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh();
  }
}

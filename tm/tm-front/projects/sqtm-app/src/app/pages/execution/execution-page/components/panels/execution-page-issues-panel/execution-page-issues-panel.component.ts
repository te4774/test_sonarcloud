import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  InjectionToken,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';

import {
  DataRow,
  deleteColumn,
  DialogService,
  Extendable,
  Fixed,
  grid,
  GridDefinition,
  GridService,
  indexColumn,
  issueKeyColumn,
  Limited,
  PaginationConfigBuilder,
  Sort,
  textColumn
} from 'sqtm-core';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {concatMap, filter, finalize, map, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {
  RemoveExecutionIssueCellComponent
} from '../../cell-renderers/remove-execution-issue-cell/remove-execution-issue-cell.component';
import {Observable, Subject} from 'rxjs';
import {issueIssueReportedInColumn} from '../../cell-renderers/issue-reported-in-cell/issue-reported-in-cell.component';

export const EXECUTION_ISSUE_TABLE_CONF = new InjectionToken<GridDefinition>('EXECUTION_ISSUE_TABLE_CONF');
export const EXECUTION_ISSUE_TABLE = new InjectionToken<GridService>('EXECUTION_ISSUE_TABLE');

export function executionIssuesTableDefinition(): GridDefinition {
  return grid('execution-view-issues').withColumns([
    indexColumn()
      .changeWidthCalculationStrategy(new Fixed(70))
      .withViewport('leftViewport'),
    issueKeyColumn('remoteId').withI18nKey('sqtm-core.entity.issue.key.label')
      .changeWidthCalculationStrategy(new Limited(100)),
    textColumn('btProject').withI18nKey('sqtm-core.entity.issue.project.label')
      .changeWidthCalculationStrategy(new Limited(150)).disableSort(),
    textColumn('summary').withI18nKey('sqtm-core.entity.issue.summary.label')
      .changeWidthCalculationStrategy(new Limited(400)).disableSort(),
    textColumn('priority').withI18nKey('sqtm-core.entity.issue.priority.label')
      .changeWidthCalculationStrategy(new Extendable(90, 0.1)).disableSort(),
    textColumn('status').withI18nKey('sqtm-core.entity.issue.status.label')
      .changeWidthCalculationStrategy(new Extendable(90, 0.1)).disableSort(),
    textColumn('assignee').withI18nKey('sqtm-core.entity.issue.assignee.label')
      .changeWidthCalculationStrategy(new Extendable(100, 0.1)).disableSort(),
    issueIssueReportedInColumn().withI18nKey('sqtm-core.entity.issue.reported-in.short')
      .changeWidthCalculationStrategy(new Extendable(100, 1)).disableSort(),
    deleteColumn(RemoveExecutionIssueCellComponent)
      .withViewport('rightViewport'),
  ]).disableRightToolBar().server().withInitialSortedColumns([{
    id: 'remoteId',
    sort: Sort.DESC
  }]).withPagination(new PaginationConfigBuilder().initialSize(25)).withRowHeight(35).build();
}

@Component({
  selector: 'sqtm-app-execution-page-issues-panel',
  template: `
    <ng-container *ngIf="executionPageService.componentData$ | async as componentData">
      <sqtm-core-issues-panel [entityId]="componentData.execution.id"
                              [entityType]="'EXECUTION_TYPE'"
                              [bugTracker]="componentData.projectData.bugTracker"
                              [hasSmallGrid]="true"
                              [warnAboutMissingExecutionStepDetails]="true"
                              (loadIssues)="loadData()">
        <sqtm-core-grid></sqtm-core-grid>
      </sqtm-core-issues-panel>
    </ng-container>`,
  styleUrls: ['./execution-page-issues-panel.component.less'],
  providers: [
    {
      provide: GridService,
      useExisting: EXECUTION_ISSUE_TABLE
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionPageIssuesPanelComponent implements OnInit, OnDestroy {

  @Output() authenticated = new EventEmitter<boolean>();

  private unsub$ = new Subject<void>();

  constructor(public executionPageService: ExecutionPageService,
              private gridService: GridService,
              private dialogService: DialogService) {
  }

  ngOnInit(): void {
  }


  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  loadData() {
    this.executionPageService.componentData$.pipe(
      take(1),
      tap(componentData =>
        this.gridService.setServerUrl([`issues/execution/${componentData.execution.id}/known-issues`]))
    ).subscribe(() => this.authenticated.next(true));
  }

  removeIssueAssociations(): void {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.data.issueIds).flat(1)),
      concatMap((issueIds: number[]) => this.showConfirmDeleteAssociatedIssuesDialog(issueIds)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      switchMap(({issueIds}) => {
        return this.executionPageService.removeIssues(issueIds);
      }),
      finalize(() => this.gridService.completeAsyncOperation()),
    ).subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteAssociatedIssuesDialog(issueIds): Observable<{ confirmDelete: boolean, issueIds: number[] }> {

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.execution-page.dialog.title.remove-issue.delete-many',
      messageKey: 'sqtm-core.campaign-workspace.execution-page.dialog.message.remove-issue.delete-many',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, issueIds}))
    );
  }
}

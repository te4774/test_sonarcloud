import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  ColumnDefinitionBuilder,
  ConfirmDeleteLevel,
  DataRow,
  DialogService,
  GridService,
} from 'sqtm-core';

import {ExecutionPageComponent} from '../../../containers/execution-page/execution-page.component';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {Observable} from 'rxjs';
import {ExecutionPageComponentData} from '../../../containers/abstract-execution-page.component';
import {concatMap, finalize, take} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-execution-history',
  template: `
    <ng-container *ngIf="row && canDelete(row)">
      <sqtm-core-delete-icon
        [iconName]="getIcon()"
        (delete)="doDeleteWithCondition(row.id)"></sqtm-core-delete-icon>
    </ng-container>
  `,
  styleUrls: ['./delete-execution-page-history.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteExecutionPageHistoryComponent extends AbstractDeleteCellRenderer {

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              dialogService: DialogService,
              private executionPageComponent: ExecutionPageComponent,
              private executionPageService: ExecutionPageService) {
    super(grid, cdr, dialogService);
    this.componentData$ = this.executionPageService.componentData$;
  }

  componentData$: Observable<ExecutionPageComponentData>;

  protected doDelete() {
      throw new Error('Method not implemented.');
  }

  getIcon(): string {
    return 'sqtm-core-generic:delete';
  }

  canDelete(row: DataRow): boolean {
    return row.simplePermissions && row.simplePermissions.canDelete
      && !row.data.boundToBlockingMilestone;
  }

  protected doDeleteWithCondition(id): any {
    this.grid.beginAsyncOperation();
    this.componentData$.pipe(
      take(1),
      concatMap((componentData: ExecutionPageComponentData) => {
        if (componentData.execution.id.toString() === id.toString()) {
          this.executionPageComponent.deleteExecutionWithRedirect(id);
        } else {
          return this.executionPageComponent.deleteExecutionWithoutRedirect(id);
        }
      }),
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.grid.refreshData());
  }


  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.message.remove-execution';
  }

  protected getTitleKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.title.remove-execution';
  }
}

export function deleteExecutionPageHistoryColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DeleteExecutionPageHistoryComponent);
}

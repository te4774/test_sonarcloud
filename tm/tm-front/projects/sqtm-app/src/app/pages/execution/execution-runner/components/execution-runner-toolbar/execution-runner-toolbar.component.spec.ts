import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ExecutionRunnerToolbarComponent} from './execution-runner-toolbar.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {TranslateModule} from '@ngx-translate/core';
import {ComponentTester} from 'ngx-speculoos';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {ExecutionState, ExecutionStepState} from '../../../states/execution-state';
import {CampaignPermissions} from 'sqtm-core';

class ExecutionRunnerToolbarTester extends ComponentTester<ExecutionRunnerToolbarComponent> {
  countButtons() {
    return this.elements('sqtm-app-execution-runner-status-button').length;
  }
}

describe('ExecutionRunnerToolbarComponent', () => {
  let tester: ExecutionRunnerToolbarTester;
  let fixture: ComponentFixture<ExecutionRunnerToolbarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [NzButtonModule, NzIconModule, TranslateModule.forRoot(), AppTestingUtilsModule],
      declarations: [ExecutionRunnerToolbarComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionRunnerToolbarComponent);
    tester = new ExecutionRunnerToolbarTester(fixture);
    const dummyStep = {} as ExecutionStepState;
    tester.componentInstance.execution = {
      executionSteps: {ids: [1, 2, 3], entities: {1: dummyStep, 2: dummyStep, 3: dummyStep}},
      currentStepIndex: 1
    } as unknown as ExecutionState;
    tester.componentInstance.milestonesAllowModification = true;
    tester.componentInstance.permissions = {
      canExecute: true
    } as CampaignPermissions;
    fixture.detectChanges();
  });

  it('should have 3 change execution status buttons', () => {
    expect(tester.countButtons()).toEqual(3);
  });
});

import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Location} from '@angular/common';
import {
  AttachmentService,
  coverageRequirementColumn,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ExecutionService,
  ExecutionStepService,
  Extendable,
  Fixed,
  GenericEntityViewService,
  GridDefinition,
  gridServiceFactory,
  indexColumn,
  InterWindowCommunicationService,
  IssuesService,
  levelEnumColumn,
  Limited,
  ReferentialDataService,
  RequirementCriticality,
  RestService,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textColumn
} from 'sqtm-core';
import {ExecutionPageService} from '../../services/execution-page.service';
import {filter, map, switchMap, take, takeUntil} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {
  EXECUTION_COVERAGE_TABLE,
  EXECUTION_COVERAGE_TABLE_CONF
} from '../../../components/coverage-table/coverage-table.component';
import {Subject} from 'rxjs';
import {
  coverageLiteralConverter
} from '../../../../test-case-workspace/test-case-view/components/coverage-table/coverage-table.component';

function executionCoverageTableDefinition(): GridDefinition {
  return smallGrid('execution-view-coverages')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      textColumn('projectName')
        .changeWidthCalculationStrategy(new Limited(300))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn('reference')
        .changeWidthCalculationStrategy(new Limited(250))
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      coverageRequirementColumn('name')
        .changeWidthCalculationStrategy(new Extendable(200, 1))
        .withI18nKey('sqtm-core.entity.requirement.label.singular'),
      levelEnumColumn('criticality', RequirementCriticality)
        .withI18nKey('sqtm-core.entity.generic.criticality.label')
        .changeWidthCalculationStrategy(new Fixed(78))
        .isEditable(false)
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withRowConverter(coverageLiteralConverter)
    .withInitialSortedColumns([{id: 'criticality', sort: Sort.ASC}])
    .build();
}


@Component({
  selector: 'sqtm-app-execution-page-level-two',
  templateUrl: './execution-page-level-two.component.html',
  styleUrls: ['./execution-page-level-two.component.less'],
  providers: [
    {
      provide: EXECUTION_COVERAGE_TABLE_CONF,
      useFactory: executionCoverageTableDefinition
    },
    {
      provide: EXECUTION_COVERAGE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, EXECUTION_COVERAGE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: ExecutionPageService,
      useClass: ExecutionPageService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        CustomFieldValueService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        ExecutionService,
        EXECUTION_COVERAGE_TABLE,
        InterWindowCommunicationService,
        ExecutionStepService,
      ]
    },
    {
      provide: EntityViewService,
      useExisting: ExecutionPageService
    },
    {
      provide: GenericEntityViewService,
      useExisting: ExecutionPageService
    },
    IssuesService
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionPageLevelTwoComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  constructor(private route: ActivatedRoute,
              private referentialDataService: ReferentialDataService,
              private executionPageService: ExecutionPageService,
              private interWindowCommunicationService: InterWindowCommunicationService,
              private location: Location) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh().pipe(
      switchMap(() => this.route.paramMap),
      take(1),
      map((params: ParamMap) => params.get('executionId')),
    ).subscribe((executionIdParam: string) => {
      const executionId = parseInt(executionIdParam, 10);
      this.executionPageService.load(executionId);
    });

    // [SQUASH-3880]
    // update view here in reaction from inter-window communication service
    // we need to do this because the id of execution is changed when re-creating the execution
    this.interWindowCommunicationService.interWindowMessages$.pipe(
      takeUntil(this.unsub$),
      filter(m => m.isTypeOf('MODIFICATION-DURING-EXECUTION')),
      take(1)
    ).subscribe(message => {
      const executionId = message.payload.executionId;
      if (executionId) {
        // we need to do like that because a standard navigation will not be triggered properly with a windows in background
        // it's the case when coming back after a modif during exec and thus we need that trick to :
        // 1- update data
        this.executionPageService.load(executionId);
        // 2- update the url in navigator WITHOUT triggering real Router navigation
        this.location.replaceState(`execution/${executionId}`);
      }
    });
  }

  ngOnDestroy(): void {
    this.executionPageService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}

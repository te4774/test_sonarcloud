import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  InjectionToken,
  Input,
  OnDestroy,
  Output
} from '@angular/core';
import {ExecutionState, ExecutionStepState} from '../../../states/execution-state';
import {Observable, Subject} from 'rxjs';
import {
  BugTracker,
  DataRow,
  deleteColumn,
  DialogService,
  Extendable,
  grid,
  GridDefinition,
  GridService,
  indexColumn,
  issueKeyColumn,
  Limited,
  PaginationConfigBuilder,
  RestService,
  Sort,
  textColumn
} from 'sqtm-core';
import {concatMap, filter, finalize, map, pluck, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {ExecutionRunnerService} from '../../services/execution-runner.service';
import {
  RemoveExecutionStepIssueCellComponent
} from '../remove-execution-step-issue-cell/remove-execution-step-issue-cell.component';

export const EXEC_STEP_ISSUE_TABLE_CONF = new InjectionToken<GridDefinition>('Grid config for the issue table of an execution step');
export const EXEC_STEP_ISSUE_TABLE = new InjectionToken<GridService>('Grid service instance for the issue table of an execution step');

@Component({
  selector: 'sqtm-app-execution-step-issues',
  template: `
    <sqtm-core-issues-panel
        class="issues-panel"
        [entityId]="currentStep.id"
        [entityType]="'EXECUTION_STEP_TYPE'"
        [hasSmallGrid]="true"
        [bugTracker]="bugTracker$ | async"
        [useManualActivation]="true"
        (loadIssues)="loadData()">
      <sqtm-core-grid></sqtm-core-grid>
    </sqtm-core-issues-panel>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionStepIssuesComponent implements OnDestroy {

  bugTracker$: Observable<BugTracker>;

  @Input()
  execution: ExecutionState;

  @Output()
  authenticated = new EventEmitter<boolean>();

  private readonly unsub$ = new Subject<void>();

  constructor(private readonly executionRunnerService: ExecutionRunnerService,
              private readonly gridService: GridService,
              private readonly restService: RestService,
              private readonly dialogService: DialogService) {
    this.bugTracker$ = this.executionRunnerService.componentData$.pipe(pluck('projectData', 'bugTracker'));
  }

  get currentStep(): ExecutionStepState {
    const currentStepIndex = this.execution.currentStepIndex;
    const id = this.execution.executionSteps.ids[currentStepIndex];
    return this.execution.executionSteps.entities[id];
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  loadData() {
    this.gridService.setServerUrl([`issues/execution-step/${this.currentStep.id}/known-issues`]);
    this.authenticated.next(true);
  }

  refresh(): void {
    this.gridService.refreshData();
  }

  unlinkSelectedIssues(): void {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.data.issueIds).flat(1)),
      concatMap((issueIds: number[]) => this.showConfirmDeleteAssociatedIssuesDialog(issueIds)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      switchMap(({issueIds}) => this.restService.delete(['issues', issueIds.join(',')])),
      finalize(() => this.gridService.completeAsyncOperation()),
    ).subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteAssociatedIssuesDialog(issueIds): Observable<{ confirmDelete: boolean, issueIds: number[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.execution-page.dialog.title.remove-issue.delete-many',
      messageKey: 'sqtm-core.campaign-workspace.execution-page.dialog.message.remove-issue.delete-many-from-step',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, issueIds}))
    );
  }
}

export function executionStepIssuesTableDefinition(): GridDefinition {
  return grid('execution-step-issues').server().withColumns([
    indexColumn()
      .withViewport('leftViewport'),
    issueKeyColumn('remoteId')
      .withI18nKey('sqtm-core.entity.issue.key.label')
      .changeWidthCalculationStrategy(new Limited(100)),
    textColumn('btProject')
      .withI18nKey('sqtm-core.entity.issue.project.label')
      .changeWidthCalculationStrategy(new Limited(150)).disableSort(),
    textColumn('summary')
      .withI18nKey('sqtm-core.entity.issue.summary.label')
      .changeWidthCalculationStrategy(new Limited(200)).disableSort(),
    textColumn('priority')
      .withI18nKey('sqtm-core.entity.issue.priority.label')
      .changeWidthCalculationStrategy(new Extendable(80, 0.8)).disableSort(),
    textColumn('status')
      .withI18nKey('sqtm-core.entity.issue.status.label')
      .changeWidthCalculationStrategy(new Extendable(80, 0.8)).disableSort(),
    deleteColumn(RemoveExecutionStepIssueCellComponent),
  ]).withInitialSortedColumns([{
    id: 'remoteId',
    sort: Sort.DESC
  }]).withPagination(new PaginationConfigBuilder().initialSize(25))
    .disableRightToolBar()
    .withRowHeight(35)
    .build();
}

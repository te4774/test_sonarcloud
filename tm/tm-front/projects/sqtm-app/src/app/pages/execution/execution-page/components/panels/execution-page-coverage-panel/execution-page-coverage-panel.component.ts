import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {GridService} from 'sqtm-core';
import {EXECUTION_COVERAGE_TABLE} from '../../../../components/coverage-table/coverage-table.component';

@Component({
  selector: 'sqtm-app-execution-page-coverage-panel',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./execution-page-coverage-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: EXECUTION_COVERAGE_TABLE
    }
  ]
})
export class ExecutionPageCoveragePanelComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

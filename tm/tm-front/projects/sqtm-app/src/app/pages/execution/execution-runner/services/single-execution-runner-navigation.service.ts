import {Injectable} from '@angular/core';
import {ExecutionRunnerNavigationService} from './execution-runner-navigation.service';
import {Router} from '@angular/router';
import {DialogService} from 'sqtm-core';
import {Observable, of} from 'rxjs';

@Injectable()
export class SingleExecutionRunnerNavigationService extends ExecutionRunnerNavigationService {

  activateFastForwardButton$: Observable<boolean> = of(false);

  constructor(router: Router, dialogService: DialogService) {
    super(router, dialogService);
  }

  protected getUrlRoot(): string[] {
    return ['execution-runner'];
  }

  handleNextOnLastStep() {
    this.showEndOfExecutionDialog();
  }

  protected getEndOfExecutionKey(): string {
    return 'sqtm-core.campaign-workspace.execution-runner.dialog.message.end-of-test-execution';
  }

  protected showNextStepButton(isLastStepOfExecution: boolean): boolean {
    return !isLastStepOfExecution;
  }

  hasNextTestCase(): boolean {
    return false;
  }

  getPartialTestPlanItemIds(): number[] {
    return [];
  }
}

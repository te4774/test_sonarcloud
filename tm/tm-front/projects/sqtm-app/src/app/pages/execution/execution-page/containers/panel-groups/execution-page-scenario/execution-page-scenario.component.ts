import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ExecutionPageService} from '../../../services/execution-page.service';
import {ExecutionPageComponentData} from '../../abstract-execution-page.component';

@Component({
  selector: 'sqtm-app-execution-page-scenario',
  templateUrl: './execution-page-scenario.component.html',
  styleUrls: ['./execution-page-scenario.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecutionPageScenarioComponent implements OnInit, OnDestroy {

  componentData$: Observable<ExecutionPageComponentData>;
  unsub$ = new Subject<void>();

  constructor(public executionPageService: ExecutionPageService) { }

  ngOnInit(): void {
    this.componentData$ = this.executionPageService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}

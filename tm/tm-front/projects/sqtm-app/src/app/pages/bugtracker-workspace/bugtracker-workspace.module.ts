import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {BugtrackerWorkspaceComponent} from './containers/bugtracker-workspace/bugtracker-workspace.component';
import {WorkspaceLayoutModule} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';

export const routes: Routes = [
  {
    path: ':bugtrackerId',
    component: BugtrackerWorkspaceComponent
  }
];

@NgModule({
  declarations: [BugtrackerWorkspaceComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    WorkspaceLayoutModule,
    TranslateModule
  ]
})
export class BugtrackerWorkspaceModule { }

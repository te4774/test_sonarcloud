import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ReferentialDataService} from 'sqtm-core';
import {withLatestFrom} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'sqtm-app-bugtracker-workspace',
  templateUrl: './bugtracker-workspace.component.html',
  styleUrls: ['./bugtracker-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BugtrackerWorkspaceComponent implements OnInit {

  url: SafeResourceUrl;

  isLoading: boolean = true;

  constructor(public referentialDataService: ReferentialDataService, private activatedRoute: ActivatedRoute,
              private domSanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh().pipe(
      withLatestFrom(this.referentialDataService.bugTrackers$)
    ).subscribe(([, bugtrackers]) => {
      const bugtrackerId: number = parseInt(this.activatedRoute.snapshot.paramMap.get('bugtrackerId'));
      const bugtrackerUrl = bugtrackers.find(bugtracker => bugtracker.id === bugtrackerId)?.url;
      this.url = bugtrackerUrl == null ? null : this.domSanitizer.bypassSecurityTrustResourceUrl(bugtrackerUrl);
      this.isLoading = false;
    });
  }
}

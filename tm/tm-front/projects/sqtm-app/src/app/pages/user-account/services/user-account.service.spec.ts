import {TestBed, waitForAsync} from '@angular/core/testing';

import {UserAccountService} from './user-account.service';
import {AuthenticationProtocol, BasicAuthCredentials, BugTracker, RestService, UserAccount} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';
import {take} from 'rxjs/operators';
import {mockRestService} from '../../../utils/testing-utils/mocks.service';

describe('UserAccountService', () => {
  let service: UserAccountService;
  const restService = mockRestService();

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: UserAccountService,
          useClass: UserAccountService,
        }
      ]
    });
    service = TestBed.inject(UserAccountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load data', waitForAsync(() => {
    restService.get.and.returnValue(of(getInitialData()));
    service.load();
    service.componentData$.pipe(take(1)).subscribe(data => {
      expect(data.userAccount.login).toBe('admin');
    });
  }));

  it('should change password', waitForAsync(() => {
    restService.get.and.returnValue(of(getInitialData()));
    service.load();

    service.changePassword('admin', 'password', false);
    expect(restService.post).toHaveBeenCalledWith(
      ['user-account/password'],
      {oldPassword: 'admin', newPassword: 'password', initializing: false});
  }));

  it('should change bugtracker authentication mode', waitForAsync(() => {
    restService.get.and.returnValue(of(getInitialData()));
    service.load();

    service.changeBugTrackerMode(false).subscribe();

    service.componentData$.pipe(take(1)).subscribe(data => {
      expect(data.userAccount.bugTrackerMode).toBe('Manual');
    });

    service.changeBugTrackerMode(true).subscribe();

    service.componentData$.pipe(take(1)).subscribe(data => {
      expect(data.userAccount.bugTrackerMode).toBe('Automatic');
    });
  }));

  it('should change bugtracker credentials', waitForAsync(() => {
    restService.get.and.returnValue(of(getInitialData()));
    service.load();

    const credentials: BasicAuthCredentials = {
      type: AuthenticationProtocol.BASIC_AUTH,
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      password: 'password',
      username: 'username',
    };

    service.changeBugTrackerCredentials(1, credentials).subscribe();

    service.componentData$.pipe(take(1)).subscribe(data => {
      expect(data.userAccount.bugTrackerCredentials).toEqual([
        {
          bugTracker: {
            id: 1,
          } as BugTracker,
          credentials,
        },
        {
          bugTracker: {
            id: 2,
          } as BugTracker,
          credentials: null,
        }]);
    });
  }));
});


function getInitialData(): UserAccount {
  return {
    id: 1,
    firstName: 'admin',
    lastName: 'admin',
    bugTrackerMode: 'Automatic',
    userGroup: {id: 1, qualifiedName: 'squashtest.authz.group.core.Admin'},
    email: '',
    login: 'admin',
    bugTrackerCredentials: [
      {
        bugTracker: {
          id: 1,
        } as BugTracker,
        credentials: null,
      },
      {
        bugTracker: {
          id: 2,
        } as BugTracker,
        credentials: null,
      }
    ],
    projectPermissions: [],
    canManageLocalPassword: true,
    hasLocalPassword: true,
  };
}

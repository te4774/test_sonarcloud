import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChildren} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  DialogReference,
  FieldValidationError,
  passwordConfirmationValidator,
  passwordLengthValidator,
  TextFieldComponent
} from 'sqtm-core';
import {UserAccountService} from '../../../services/user-account.service';
import {HttpErrorResponse} from '@angular/common/http';

enum Fields {
  oldPassword = 'oldPassword',
  newPassword = 'newPassword',
  confirmPassword = 'confirmPassword',
}

@Component({
  selector: 'sqtm-app-change-user-password-dialog',
  templateUrl: './change-user-password-dialog.component.html',
  styleUrls: ['./change-user-password-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChangeUserPasswordDialogComponent {
  formGroup: FormGroup;

  @ViewChildren(TextFieldComponent)
  textFields: TextFieldComponent[];

  serverSideValidationErrors: FieldValidationError[] = [];

  data: ChangeUserPasswordDialogConfiguration;

  constructor(private readonly fb: FormBuilder,
              private readonly userAccountService: UserAccountService,
              private readonly dialogRef: DialogReference,
              private readonly changeDetectorRef: ChangeDetectorRef) {
    this.data = dialogRef.data;
    this.buildFormGroup();
  }

  buildFormGroup(): void {
    const controls: any = {
      newPassword: this.fb.control('', [Validators.required]),
      confirmPassword: this.fb.control('', [Validators.required]),
    };

    if (this.data.hasLocalPassword) {
      controls.oldPassword = this.fb.control('', [Validators.required]);
    }

    this.formGroup = this.fb.group(controls,
      {
        validators: [
          passwordLengthValidator(Fields.newPassword),
          passwordConfirmationValidator(Fields.newPassword, Fields.confirmPassword)
        ]
      });

  }

  handleDialogConfirm(): void {
    if (this.isFormValid()) {
      this.changePasswordServerSide();
    } else {
      this.showClientSideErrors();
    }
  }

  isFormValid(): boolean {
    const valid = this.formGroup.valid;

    if (!valid) {
      this.showClientSideErrors();
    }

    return valid;
  }

  private showClientSideErrors() {
    this.textFields.forEach(textField => textField.showClientSideError());
  }

  private changePasswordServerSide(): void {
    const oldPassword = this.formGroup.get(Fields.oldPassword)?.value ?? '';
    const newPassword = this.formGroup.get(Fields.newPassword).value;
    const initializing = !this.data.hasLocalPassword;

    this.userAccountService.changePassword(oldPassword, newPassword, initializing)
      .subscribe(() => this.handleChangePasswordSuccess(),
        (err) => this.handleChangePasswordError(err));
  }

  private handleChangePasswordSuccess(): void {
    this.dialogRef.result = true;
    this.dialogRef.close();
  }

  private handleChangePasswordError(error: HttpErrorResponse) {
    if (error.status === 412) {
      const squashError = error.error.squashTMError;
      if (squashError.kind === 'FIELD_VALIDATION_ERROR') {
        this.serverSideValidationErrors = squashError.fieldValidationErrors;
        this.changeDetectorRef.markForCheck();
      }
    }
  }
}

export interface ChangeUserPasswordDialogConfiguration {
  hasLocalPassword: boolean;
}

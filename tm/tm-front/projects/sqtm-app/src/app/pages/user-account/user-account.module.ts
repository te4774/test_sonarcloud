import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserAccountPageComponent} from './containers/user-account-page/user-account-page.component';
import {RouterModule, Routes} from '@angular/router';
import {
  AnchorModule,
  DialogModule,
  GridModule,
  NavBarModule,
  SvgModule,
  UiManagerModule,
  WorkspaceCommonModule
} from 'sqtm-core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCollapseModule} from 'ng-zorro-antd/collapse';
import {NzFormModule} from 'ng-zorro-antd/form';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzSwitchModule} from 'ng-zorro-antd/switch';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {UserAccountViewComponent} from './containers/user-account-view/user-account-view.component';
import {
  UserAccountInformationPanelComponent
} from './components/panels/user-account-information-panel/user-account-information-panel.component';
import {
  ChangeUserPasswordDialogComponent
} from './components/dialogs/change-user-password-dialog/change-user-password-dialog.component';
import {
  UserAccountPermissionsPanelComponent
} from './components/panels/user-account-permissions-panel/user-account-permissions-panel.component';
import {
  UserAccountBugtrackersConfigurationPanelComponent
} from './components/panels/user-account-bugtrackers-configuration-panel/user-account-bugtrackers-configuration-panel.component';

export const routes: Routes = [
  {
    path: '',
    component: UserAccountPageComponent,
    children: [
      {
        path: 'content',
        component: UserAccountViewComponent,
      }
    ]
  }
];

@NgModule({
  declarations: [
    UserAccountPageComponent,
    UserAccountViewComponent,
    UserAccountInformationPanelComponent,
    ChangeUserPasswordDialogComponent,
    UserAccountPermissionsPanelComponent,
    UserAccountBugtrackersConfigurationPanelComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NavBarModule,
    SvgModule,
    WorkspaceCommonModule,
    NzCollapseModule,
    NzFormModule,
    NzInputModule,
    FormsModule,
    ReactiveFormsModule,
    NzIconModule,
    NzButtonModule,
    UiManagerModule,
    TranslateModule,
    NzToolTipModule,
    AnchorModule,
    DialogModule,
    GridModule,
    NzSwitchModule,
    NzSelectModule,
  ]
})
export class UserAccountModule {
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {UserAccountInformationPanelComponent} from './user-account-information-panel.component';
import {UserAccountService} from '../../../services/user-account.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY, of} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {DialogService} from 'sqtm-core';
import {UserAccountViewState} from '../../../state/user-account.model';

describe('UserAccountInformationPanelComponent', () => {
  let component: UserAccountInformationPanelComponent;
  let fixture: ComponentFixture<UserAccountInformationPanelComponent>;
  const userAccountService = jasmine.createSpyObj(['load']);
  userAccountService.componentData$ = EMPTY;

  const dialogService = jasmine.createSpyObj(['openDialog']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [UserAccountInformationPanelComponent],
      providers: [
        {
          provide: UserAccountService,
          useValue: userAccountService,
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: DialogService,
          useValue: dialogService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAccountInformationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should format user name', () => {
    const username = component.getUsername({
      userAccount: {
        firstName: 'john',
        lastName: 'doe',
      }
    } as unknown as UserAccountViewState);
    expect(username).toBe('john doe');


    const usernameWithoutFirstName = component.getUsername({
      userAccount: {
        lastName: 'doe',
      }
    } as unknown as UserAccountViewState);
    expect(usernameWithoutFirstName).toBe('doe');
  });

  it('should format user group', () => {
    const groupKey = component.getUserGroup({
      userAccount: {
        userGroup: {
          id: 1,
          qualifiedName: 'squashtest.authz.group.core.Admin',
        }
      }
    } as unknown as UserAccountViewState);

    expect(groupKey).toBe('sqtm-core.entity.user.account.group-authority.admin');
  });

  it('should open change password dialog', () => {
    component.currentUser$ = of({
      userAccount: {
      }
    } as UserAccountViewState);
    component.openChangePasswordDialog();
    expect(dialogService.openDialog).toHaveBeenCalled();
  });
});

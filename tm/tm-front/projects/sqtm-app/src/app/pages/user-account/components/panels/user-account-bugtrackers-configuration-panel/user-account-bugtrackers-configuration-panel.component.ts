import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChildren} from '@angular/core';
import {UserAccountViewState} from '../../../state/user-account.model';
import {Observable, Subject} from 'rxjs';
import {UserAccountService} from '../../../services/user-account.service';
import {takeUntil} from 'rxjs/operators';
import {
  BugTracker,
  BugTrackerCredentials,
  Credentials,
  Identifier,
  ThirdPartyCredentialsFormComponent
} from 'sqtm-core';
import {NzSelectOptionInterface} from 'ng-zorro-antd/select';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-user-account-bugtrackers-configuration-panel',
  templateUrl: './user-account-bugtrackers-configuration-panel.component.html',
  styleUrls: ['./user-account-bugtrackers-configuration-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserAccountBugtrackersConfigurationPanelComponent implements OnInit, OnDestroy {

  @ViewChildren(ThirdPartyCredentialsFormComponent)
  credentialsForms: ThirdPartyCredentialsFormComponent[];

  componentData$: Observable<UserAccountViewState>;

  bugTrackerOptions: NzSelectOptionInterface[];

  credentialsFormsByBugTracker = new Map<Identifier, Credentials>();
  credentialsStatusMessage = '';
  statusIcon: 'DANGER' | 'INFO' = null;

  readonly unsub$ = new Subject<void>();

  set selectedBugTracker(bt: number) {
    this._selectedBugTracker = bt;
    this.resetCredentialsStatus();
    this.cdRef.markForCheck();
  }

  get selectedBugTracker(): number {
    return this._selectedBugTracker;
  }

  private _bugTrackerCredentials: BugTrackerCredentials[];
  private _selectedBugTracker: number;

  constructor(private readonly userAccountService: UserAccountService,
              private readonly cdRef: ChangeDetectorRef,
              private readonly translateService: TranslateService) {
    this.userAccountService.componentData$.pipe(
      takeUntil(this.unsub$),
    ).subscribe((componentData) => {
      this.bugTrackerOptions = componentData.userAccount.bugTrackerCredentials.map((btCredentials) => ({
        value: btCredentials.bugTracker.id,
        label: `${btCredentials.bugTracker.name} (${btCredentials.bugTracker.url})`,
      }));

      this._bugTrackerCredentials = componentData.userAccount.bugTrackerCredentials;

      if (this._selectedBugTracker == null && this.hasBugTrackerOptions) {
        this._selectedBugTracker = this.bugTrackerOptions[0].value;
      }
    });
  }

  get hasBugTrackerOptions(): boolean {
    return this.bugTrackerOptions?.length > 0;
  }

  get selectedBugTrackerCredentials(): BugTrackerCredentials {
    return this._bugTrackerCredentials?.find(btCreds => btCreds.bugTracker.id === this.selectedBugTracker);
  }

  ngOnInit(): void {
    this.componentData$ = this.userAccountService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  handleBugTrackerModelChange(isAutomatic: boolean): void {
    this.userAccountService.changeBugTrackerMode(isAutomatic)
      .subscribe();
  }

  submitCredentials($event: Credentials, bugTracker: BugTracker): void {
    this.userAccountService.changeBugTrackerCredentials(bugTracker.id, $event)
      .subscribe(() => this.handleCredentialsSaveSuccess(),
        (error) => this.handleCredentialsError(error));
  }

  deleteCredentials(bugTracker: BugTracker): void {
    this.userAccountService.deleteBugTrackerCredentials(bugTracker)
      .subscribe();
  }

  private resetCredentialsStatus(): void {
    this.credentialsStatusMessage = '';
    this.credentialsForms.forEach(form => form.formGroup.markAsPristine());
    this.statusIcon = null;
  }

  private handleCredentialsSaveSuccess(): void {
    this.credentialsStatusMessage = 'sqtm-core.administration-workspace.bugtrackers.authentication-policy.credentials.save-success';
    this.credentialsForms.forEach(form => form.endAsync());
    this.credentialsForms.forEach(form => form.formGroup.markAsPristine());
    this.statusIcon = 'INFO';
  }

  private handleCredentialsError(error: any): void {
    this.credentialsForms.forEach(form => form.endAsync());

    if (error.status === 412) {
      const squashError = error.error.squashTMError;
      if (squashError.kind === 'ACTION_ERROR') {
        this.credentialsStatusMessage = this.translateService.instant(squashError.actionValidationError.i18nKey);
        this.cdRef.markForCheck();
        this.statusIcon = 'DANGER';
      }
    }
  }
}

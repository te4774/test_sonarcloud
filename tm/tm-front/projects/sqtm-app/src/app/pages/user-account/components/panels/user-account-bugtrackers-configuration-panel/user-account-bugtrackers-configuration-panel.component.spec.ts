import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {
  UserAccountBugtrackersConfigurationPanelComponent
} from './user-account-bugtrackers-configuration-panel.component';
import {UserAccountService} from '../../../services/user-account.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {of, throwError} from 'rxjs';
import {TranslateModule} from '@ngx-translate/core';
import {AuthenticationProtocol, BasicAuthCredentials, BugTracker} from 'sqtm-core';
import {UserAccountEntityState} from '../../../state/user-account.model';
import {makeHttpActionError} from '../../../../../utils/testing-utils/test-error-response-generator';
import SpyObj = jasmine.SpyObj;
import createSpyObj = jasmine.createSpyObj;

describe('UserAccountBugtrackersConfigurationPanelComponent', () => {
  let component: UserAccountBugtrackersConfigurationPanelComponent;
  let fixture: ComponentFixture<UserAccountBugtrackersConfigurationPanelComponent>;

  const userAccountService: SpyObj<UserAccountService> = createSpyObj(['load', 'changeBugTrackerCredentials']);

  userAccountService.componentData$ = of({
    type: 'userAccount',
    uiState: null,
    userAccount: {
      id: 1,
      bugTrackerCredentials: [
        {
          bugTracker: {
            id: 1,
            name: 'jira.cloud',
            url: 'url1',
          },
          credentials: {
            implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
            type: AuthenticationProtocol.BASIC_AUTH,
            username: 'username',
            password: 'password',
          }
        },
        {
          bugTracker: {
            id: 2,
            name: 'mantis',
            url: 'url2',
          },
          credentials: null,
        },
      ]
    } as unknown as UserAccountEntityState,
  });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [UserAccountBugtrackersConfigurationPanelComponent],
      providers: [
        {
          provide: UserAccountService,
          useValue: userAccountService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(UserAccountBugtrackersConfigurationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    userAccountService.changeBugTrackerCredentials.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load bugtracker options', () => {
    expect(component.bugTrackerOptions.length).toBe(2);
  });

  it('should reset status message when changing selected bugtracker', () => {
    component.credentialsStatusMessage = 'message here';

    component.selectedBugTracker = 2;

    expect(component.credentialsStatusMessage).toBe('');
  });

  it('should successfully save credentials', () => {
    const credentials: BasicAuthCredentials = {
      type: AuthenticationProtocol.BASIC_AUTH,
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      username: 'username',
      password: 'password',
    };

    const bugTracker: BugTracker = {
      id: 1,
      name: 'bt'
    } as BugTracker;

    userAccountService.changeBugTrackerCredentials.and.returnValue(of({}));

    component.submitCredentials(credentials, bugTracker);

    expect(userAccountService.changeBugTrackerCredentials).toHaveBeenCalledWith(1, credentials);
    expect(component.statusIcon).toBe('INFO');
  });

  it('should handle save credentials success', () => {
    const credentials: BasicAuthCredentials = {
      type: AuthenticationProtocol.BASIC_AUTH,
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      username: 'username',
      password: 'password',
    };

    const bugTracker: BugTracker = {
      id: 1,
      name: 'bt'
    } as BugTracker;

    userAccountService.changeBugTrackerCredentials.and.returnValue(throwError(makeHttpActionError({
      exception: '',
      i18nParams: [],
      i18nKey: 'boom',
    })));

    component.submitCredentials(credentials, bugTracker);

    expect(userAccountService.changeBugTrackerCredentials).toHaveBeenCalledWith(1, credentials);
    expect(component.statusIcon).toBe('DANGER');
  });
});

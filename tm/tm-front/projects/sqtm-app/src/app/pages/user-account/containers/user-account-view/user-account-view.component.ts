import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {UserAccountService} from '../../services/user-account.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-user-account-view',
  templateUrl: './user-account-view.component.html',
  styleUrls: ['./user-account-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserAccountViewComponent implements OnInit {
  public readonly hasBugTrackerCredentials$: Observable<boolean>;

  constructor(public readonly userAccountService: UserAccountService) {
    this.hasBugTrackerCredentials$ = userAccountService.componentData$
      .pipe(map((componentData) => componentData.userAccount.bugTrackerCredentials.length > 0));
  }

  ngOnInit(): void {
  }

}

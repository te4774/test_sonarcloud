import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {DetailedTestStepViewComponent} from './containers/detailed-test-step-view/detailed-test-step-view.component';
import {
  AttachmentModule,
  CustomFieldModule,
  GridModule,
  NavBarModule,
  RequirementUiModule,
  SqtmDragAndDropModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule
} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {TestStepInformationsComponent} from './containers/test-step-informations/test-step-informations.component';
// tslint:disable-next-line:max-line-length
import {DetailedActionStepComponent} from './components/detailed-action-step/detailed-action-step.component';
import {DetailedCallStepComponent} from './components/detailed-call-step/detailed-call-step.component';
import {
  DetailedCreateStepFormComponent
} from './components/detailed-create-step-form/detailed-create-step-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CKEditorModule} from 'ckeditor4-angular';
import {
  DetailedStepViewsCommonModule
} from '../../../components/detailed-step-views-common/detailed-step-views-common.module';

export const routes: Routes = [
  {
    path: '',
    component: DetailedTestStepViewComponent,
    children: [
      {
        path: 'step/:stepIndex',
        component: TestStepInformationsComponent
      }
    ]
  }
];


@NgModule({
  declarations: [
    DetailedTestStepViewComponent,
    TestStepInformationsComponent,
    DetailedActionStepComponent,
    DetailedCallStepComponent,
    DetailedCreateStepFormComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        WorkspaceCommonModule,
        UiManagerModule,
        NavBarModule,
        TranslateModule.forChild(),
        NzIconModule,
        NzButtonModule,
        NzToolTipModule,
        GridModule,
        NzCheckboxModule,
        RequirementUiModule,
        SqtmDragAndDropModule,
        NzDropDownModule,
        CustomFieldModule,
        AttachmentModule,
        ReactiveFormsModule,
        CKEditorModule,
        DetailedStepViewsCommonModule,
        WorkspaceLayoutModule
    ]
})
export class DetailedTestStepViewModule {
}

import {ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {
  ActionErrorDisplayService,
  BindableEntity,
  createCustomFieldValueDataSelector,
  CustomFieldData,
  EditableRichTextComponent,
  PersistedAttachment,
  RejectedAttachment
} from 'sqtm-core';
import {catchError, finalize} from 'rxjs/operators';
import {ModifDuringExecStepViewService} from '../../services/modif-during-exec-step-view.service';
import {
  ModifDuringExecStepViewComponentData
} from '../../containers/modif-during-exec-step-view/modif-during-exec-step-view.component';
import {Observable} from 'rxjs';
import {select} from '@ngrx/store';
import {ModifDuringExecStepState} from '../../states/modif-during-exec-step.state';

@Component({
  selector: 'sqtm-app-exec-test-case-step-information',
  templateUrl: './exec-test-case-step-information.component.html',
  styleUrls: [
    './exec-test-case-step-information.component.less',
    '../../../detailed-test-step-view/containers/detailed-test-step-view/detailed-test-step-view.component.less',
    '../../../detailed-test-step-view/components/detailed-action-step/detailed-action-step.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExecTestCaseStepInformationComponent implements OnInit {

  componentData$: Observable<ModifDuringExecStepViewComponentData>;

  @ViewChild('action', {read: EditableRichTextComponent})
  actionField: EditableRichTextComponent;

  @ViewChild('expectedResult', {read: EditableRichTextComponent})
  expectedResultField: EditableRichTextComponent;
  customFieldData$: Observable<CustomFieldData[]>;

  constructor(private stepViewService: ModifDuringExecStepViewService,
              private actionErrorDisplayService: ActionErrorDisplayService) {
    this.componentData$ = stepViewService.componentData$;
    this.customFieldData$ = stepViewService.componentData$.pipe(
      select(createCustomFieldValueDataSelector(BindableEntity.TEST_STEP))
    );
  }

  ngOnInit(): void {
  }

  changeAction(action: string) {
    this.stepViewService.changeAction(action).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err)),
      finalize(() => this.actionField.endAsync()),
    ).subscribe(() => this.actionField.disableEditMode());
  }

  changeExpectedResult(expectedResult: string) {
    this.stepViewService.changeExpectedResult(expectedResult).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err)),
      finalize(() => this.expectedResultField.endAsync())
    ).subscribe(() => this.actionField.disableEditMode());
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }

  isEditable(componentData: ModifDuringExecStepViewComponentData) {
    return componentData.permissions.canWrite && componentData.milestonesAllowModification;
  }

  addAttachment(files: File[], actionStep: ModifDuringExecStepState) {
    this.stepViewService.addAttachments(files, actionStep.attachmentList.id);
  }

  getAttachments(actionStep: ModifDuringExecStepState) {
    return Object.values(actionStep.attachmentList.attachments.entities);
  }

  cancelDeleteAttachmentEvent(attachment: PersistedAttachment) {
    this.stepViewService.cancelDeleteAttachments([attachment.id]);
  }

  deleteAttachment(attachment: PersistedAttachment) {
    this.stepViewService.markAttachmentsToDelete([attachment.id]);
  }

  confirmDeleteAttachmentEvent(attachment: PersistedAttachment, actionStep: ModifDuringExecStepState) {
    this.stepViewService.deleteAttachments([attachment.id], actionStep.attachmentList.id);
  }

  removeRejectedAttachment(attachment: RejectedAttachment) {
    this.stepViewService.removeRejectedAttachments([attachment.id]);
  }
}

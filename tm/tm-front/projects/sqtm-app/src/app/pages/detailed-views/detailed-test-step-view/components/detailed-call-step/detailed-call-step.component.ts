import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {CallStepState} from '../../../../test-case-workspace/test-case-view/state/test-step.state';
import {Dataset, DialogService, RestService, TestStepModel} from 'sqtm-core';
import {DetailedTestCaseState} from '../../state/detailed-test-step-view.state';
import {TranslateService} from '@ngx-translate/core';
// tslint:disable-next-line:max-line-length
import {
  CallStepParamDialogComponent
} from '../../../../../components/test-steps/call-step/dialog/call-step-param-dialog/call-step-param-dialog.component';
import {DetailedTestStepViewService} from '../../services/detailed-test-step-view.service';
import {
  ParameterAssignationUpdate
} from '../../../../test-case-workspace/test-case-view/service/test-step-view.service';

@Component({
  selector: 'sqtm-app-detailed-call-step',
  templateUrl: './detailed-call-step.component.html',
  styleUrls: ['./detailed-call-step.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailedCallStepComponent implements OnInit {

  @Input()
  callTestStep: CallStepState;

  @Input()
  testCase: DetailedTestCaseState;

  @Input()
  editable = false;

  constructor(
    private detailedTestStepViewService: DetailedTestStepViewService,
    private translateService: TranslateService,
    private restService: RestService,
    private dialogService: DialogService) {
  }

  ngOnInit(): void {
  }

  getIndex(callTestStep: CallStepState, calledTestCaseStep: TestStepModel): string {
    return `${calledTestCaseStep.stepOrder + 1}`;
  }

  getCalledTestCaseLink(callTestStep: CallStepState) {
    return ['/test-case-workspace/test-case/', callTestStep.calledTcId.toString()];
  }

  chooseDataset(testCaseName: string) {
    this.restService.get<Dataset[]>([`test-cases/${this.callTestStep.calledTcId}/datasets`]).subscribe(result => {
      const dialogRef = this.dialogService.openDialog<any, ParameterAssignationUpdate>({
        id: 'call-step-param',
        component: CallStepParamDialogComponent,
        data: {id: 'call-step-param', dataSets: result, callStep: this.callTestStep, testCaseName}
      });

      dialogRef.dialogClosed$.subscribe(dialogResult => {
        if (dialogResult) {
          this.detailedTestStepViewService.updateParameterAssignationMode(this.callTestStep.id, dialogResult);
        }
      });
    });

  }

  getDataSetLabel() {
    if (this.callTestStep.delegateParam) {
      return this.translateService.instant('sqtm-core.entity.call-step.dataset.delegate');
    } else {
      return this.getCalledDataSetLabel();
    }
  }

  private getCalledDataSetLabel() {
    if (this.callTestStep.calledDatasetId != null) {
      return this.callTestStep.calledDatasetName;
    } else {
      if (this.editable) {
        return this.translateService.instant('sqtm-core.entity.call-step.dataset.pick-dataset.title');
      } else {
        return this.translateService.instant('sqtm-core.entity.call-step.dataset.no-dataset');
      }
    }
  }

}

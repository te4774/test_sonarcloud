import {Injectable} from '@angular/core';
import {
  ActionStepExecViewModel,
  AttachmentService,
  ChangeCoverageOperationReport,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewState,
  GridService,
  ModifDuringExecService,
  ProjectData,
  ReferentialDataService,
  RequirementVersionCoverage,
  RestService,
  TestCasePermissions,
  TestCaseService,
  TestStepService
} from 'sqtm-core';
import {
  linkedCoverageSelector,
  ModifDuringExecStepState,
  ModifDuringExecStepViewState,
  provideModifDuringExecStepViewState
} from '../states/modif-during-exec-step.state';
import {TranslateService} from '@ngx-translate/core';
import {concatMap, filter, finalize, map, take, tap, withLatestFrom} from 'rxjs/operators';
import {
  coverageEntityAdapter
} from '../../../test-case-workspace/test-case-view/state/requirement-version-coverage.state';
import {select} from '@ngrx/store';
import {
  StepViewRequirementLinkService
} from '../../../../components/detailed-step-views-common/step-view-requirement-link.service';
import {Observable, of} from 'rxjs';
import {Update} from '@ngrx/entity/src/models';
import {TestCaseViewUiState} from '../../../test-case-workspace/test-case-view/state/test-case.state';

@Injectable()
export class ModifDuringExecStepViewService
  extends EntityViewService<ModifDuringExecStepState, 'actionStep', TestCasePermissions>
  implements StepViewRequirementLinkService {

  stepCreationMode$: Observable<boolean> = of(false);

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              protected modifDuringExecService: ModifDuringExecService,
              private testStepService: TestStepService,
              private testCaseService: TestCaseService,
              private coverageGrid: GridService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(projectData: ProjectData): TestCasePermissions {
    return new TestCasePermissions(projectData);
  }

  getInitialState(): GenericEntityViewState<ModifDuringExecStepState, 'actionStep'> {
    return provideModifDuringExecStepViewState();
  }

  getAttachmentPermissionHolderEntityType(viewState: ModifDuringExecStepViewState) {
    return 'testCase';
  }

  getAttachmentPermissionHolderEntityId(state: ModifDuringExecStepState) {
    return state.actionStepTestCaseId;
  }

  load(executionId: number, actionStepId: number) {
    this.modifDuringExecService.getActionStepExecViewModel(executionId, actionStepId)
      .subscribe((model: ActionStepExecViewModel) => {
        const attachmentEntityState = this.initializeAttachmentState(model.attachmentList.attachments);
        const customFieldValueState = this.initializeCustomFieldValueState(model.customFieldValues);
        const coverageState = this.initializeCoverageState(model.coverages);
        const uiState: TestCaseViewUiState = {openTestCaseTreePicker: false, openRequirementTreePicker: false};
        const entityState = {
          ...model,
          attachmentList: {id: model.attachmentList.id, attachments: attachmentEntityState},
          customFieldValues: customFieldValueState,
          coverages: coverageState,
          uiState
        };
        this.initializeEntityState(entityState);
        this.initializeCoverageGrid();
      });
  }

  changeAction(action: string) {
    return this.store.state$.pipe(
      take(1),
      concatMap((state => this.testStepService.postAction(state.actionStep.id, action))),
      withLatestFrom(this.store.state$),
      map(([, state]) => ({...state, actionStep: {...state.actionStep, action}})),
      tap(state => this.store.commit(state))
    );
  }

  changeExpectedResult(expectedResult: string) {
    return this.store.state$.pipe(
      take(1),
      concatMap((state => this.testStepService.postExpectedResult(state.actionStep.id, expectedResult))),
      withLatestFrom(this.store.state$),
      map(([, state]) => ({...state, actionStep: {...state.actionStep, expectedResult}})),
      tap(state => this.store.commit(state))
    );
  }

  private initializeCoverageState(coverages: RequirementVersionCoverage[]) {
    return coverageEntityAdapter.setAll(coverages, coverageEntityAdapter.getInitialState());
  }

  private initializeCoverageGrid() {
    const coverage$ = this.componentData$.pipe(
      select(linkedCoverageSelector)
    );
    this.coverageGrid.connectToDatasource(coverage$, 'requirementVersionId');
  }

  closeRequirementTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: ModifDuringExecStepViewState) =>
        ({
          ...state,
          actionStep: {...state.actionStep, uiState: {...state.actionStep.uiState, openRequirementTreePicker: false}}
        })
      )
    ).subscribe(state => this.commit(state));
  }

  openRequirementTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: ModifDuringExecStepViewState) =>
        ({
          ...state,
          actionStep: {...state.actionStep, uiState: {...state.actionStep.uiState, openRequirementTreePicker: true}}
        })
      )
    ).subscribe(state => this.commit(state));
  }

  deleteCoverages(requirementVersionIds: number[]) {
    return this.store.state$.pipe(
      take(1),
      tap(() => this.coverageGrid.beginAsyncOperation()),
      concatMap((state) => {
        return this.testCaseService.eraseCoverages(state.actionStep.actionStepTestCaseId, requirementVersionIds);
      }),
      withLatestFrom(this.store.state$),
      map(([operationReport, state]: [ChangeCoverageOperationReport, ModifDuringExecStepViewState]) => {
        const coverages = coverageEntityAdapter.setAll(operationReport.coverages, state.actionStep.coverages);
        return {...state, actionStep: {...state.actionStep, coverages}};
      }),
      finalize(() => this.coverageGrid.completeAsyncOperation())
    ).subscribe(state => this.store.commit(state));
  }

  deleteStepCoverages(requirementVersionIds: number[]): Observable<any> {
    return this.state$.pipe(
      take(1),
      // async server call
      concatMap((state: ModifDuringExecStepViewState) =>
        this.testStepService.eraseStepCoverages(this.getTestCaseId(state), this.getStepId(state), requirementVersionIds)),
      withLatestFrom(this.store.state$),
      map(([, state]) => {
        const updates: Update<RequirementVersionCoverage>[] = requirementVersionIds.map(id => {
          const coverageStepInfos = this.getCoverageStepInfos(state, id).filter(i => i.id !== this.getStepId(state));
          return {id, changes: {coverageStepInfos}};
        });
        const coverages = coverageEntityAdapter.updateMany(updates, state.actionStep.coverages);
        return {...state, actionStep: {...state.actionStep, coverages}};
      }),
      tap(state => this.store.commit(state))
    );
  }

  linkRequirementToCurrentStep(requirementVersionId: number): Observable<any> {
    return this.state$.pipe(
      take(1),
      filter(state => !this.isAlreadyLinked(state, requirementVersionId)),
      map(state => this.getStepId(state)),
      // async server call
      concatMap((actionStepId) => this.testStepService.linkRequirementVersionToStep(actionStepId, requirementVersionId)),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.addNewLinkInState(state, requirementVersionId)),
      tap(state => this.store.commit(state))
    );
  }

  private getStepId(state: ModifDuringExecStepViewState): number {
    return state.actionStep.id;
  }

  private getTestCaseId(state: ModifDuringExecStepViewState): number {
    return state.actionStep.actionStepTestCaseId;
  }

  private addNewLinkInState(state, requirementVersionId: number) {
    const coverageStepInfos = this.getCoverageStepInfos(state, requirementVersionId);
    coverageStepInfos.push({id: state.actionStep.id});
    const coverages = coverageEntityAdapter.updateOne({
      id: requirementVersionId,
      changes: {
        coverageStepInfos
      }
    }, state.actionStep.coverages);
    return {...state, actionStep: {...state.actionStep, coverages}};
  }

  private isAlreadyLinked(state: ModifDuringExecStepViewState, requirementVersionId: number) {
    const coverageStepInfos = this.getCoverageStepInfos(state, requirementVersionId);
    const alreadyLinkedStepIds = coverageStepInfos.map(stepInfo => stepInfo.id);
    const stepId = state.actionStep.id;
    return alreadyLinkedStepIds.includes(stepId);
  }

  private getCoverageStepInfos(state: ModifDuringExecStepViewState, requirementVersionId: number) {
    return this.getCoverage(state, requirementVersionId).coverageStepInfos;
  }

  private getCoverage(state: ModifDuringExecStepViewState, requirementVersionId: number) {
    return state.actionStep.coverages.entities[requirementVersionId];
  }

  dropRequirement(requirementIds: number[]): Observable<ChangeCoverageOperationReport> {
    return this.store.state$.pipe(
      take(1),
      tap(() => this.coverageGrid.beginAsyncOperation()),
      concatMap((state: ModifDuringExecStepViewState) => {
        return this.testStepService.persistStepCoverage(requirementIds, state.actionStep.id, state.actionStep.actionStepTestCaseId);
      }),
      withLatestFrom(this.store.state$),
      map(([operationReport, state]: [ChangeCoverageOperationReport, ModifDuringExecStepViewState]) => {
        const nextState: ModifDuringExecStepViewState = {
          ...state, actionStep: {
            ...state.actionStep,
            coverages: coverageEntityAdapter.setAll(operationReport.coverages, state.actionStep.coverages)
          }
        };
        return {operationReport, nextState};
      }),
      tap(({operationReport, nextState}) => this.commit(nextState)),
      map(({operationReport, nextState}) => operationReport),
      finalize(() => this.coverageGrid.completeAsyncOperation())
    );
  }
}

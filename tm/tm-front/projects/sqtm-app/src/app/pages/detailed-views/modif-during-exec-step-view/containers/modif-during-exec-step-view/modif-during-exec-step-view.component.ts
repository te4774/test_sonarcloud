import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import {
  AttachmentService,
  createTestCaseCoverageMessageDialogConfiguration,
  CustomFieldValueService,
  DialogService,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  GenericEntityViewService,
  GridService,
  gridServiceFactory,
  isDndDataFromRequirementTreePicker,
  ModifDuringExecService,
  ReferentialDataService,
  RestService,
  shouldShowCoverageMessageDialog,
  SqtmDropEvent,
  TestCasePermissions,
  TestCaseService,
  TestStepService,
  Workspaces
} from 'sqtm-core';
import {ModifDuringExecViewService} from '../../services/modif-during-exec-view.service';
import {ActivatedRoute, Router} from '@angular/router';
import {combineLatest, Observable} from 'rxjs';
import {filter, take} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {ModifDuringExecStepViewService} from '../../services/modif-during-exec-step-view.service';
import {ModifDuringExecStepState} from '../../states/modif-during-exec-step.state';
import {select} from '@ngrx/store';
import {selectCurrentActionStepIndex, selectTotalStepCount} from '../../states/modif-during-exec.state';
import {
  DETAILED_COVERAGE_TABLE,
  DETAILED_STEP_COVERAGE_TABLE_CONF,
  STEP_VIEW_REQUIREMENT_LINK_SERVICE
} from '../../../../../components/detailed-step-views-common/detailed-step-view.constant.ts.constant';
import {
  detailedStepCoverageTableDefinition
} from '../../../../../components/detailed-step-views-common/components/detailed-step-coverage-table/detailed-step-coverage-table.component';
import {
  AbstractRequirementLinkableStepComponent
} from '../../../../../components/detailed-step-views-common/components/abstract-requirement-linkable-step.component';

@Component({
  selector: 'sqtm-app-modif-during-exec-step-view',
  templateUrl: './modif-during-exec-step-view.component.html',
  styleUrls: [
    './modif-during-exec-step-view.component.less',
    '../../../detailed-test-step-view/containers/detailed-test-step-view/detailed-test-step-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: DETAILED_STEP_COVERAGE_TABLE_CONF,
      useFactory: detailedStepCoverageTableDefinition
    },
    {
      provide: DETAILED_COVERAGE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, DETAILED_STEP_COVERAGE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: ModifDuringExecStepViewService,
      useClass: ModifDuringExecStepViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        TranslateService,
        CustomFieldValueService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        ModifDuringExecService,
        TestStepService,
        TestCaseService,
        DETAILED_COVERAGE_TABLE
      ]
    },
    {
      provide: EntityViewService,
      useExisting: ModifDuringExecStepViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: ModifDuringExecStepViewService
    },
    {
      provide: STEP_VIEW_REQUIREMENT_LINK_SERVICE,
      useExisting: ModifDuringExecStepViewService
    }
  ]
})
export class ModifDuringExecStepViewComponent extends AbstractRequirementLinkableStepComponent implements OnInit, OnDestroy {

  componentData$: Observable<ModifDuringExecStepViewComponentData>;
  currentStepIndex$: Observable<number>;
  totalStepCount$: Observable<number>;

  @ViewChild('dropFileZone', {read: ElementRef})
  private dropFileZone: ElementRef;

  requirementWorkspace = Workspaces['requirement-workspace'];

  constructor(private restService: RestService,
              private modifDuringExecViewService: ModifDuringExecViewService,
              private stepViewService: ModifDuringExecStepViewService,
              private route: ActivatedRoute,
              protected renderer: Renderer2,
              protected dialogService: DialogService,
              protected router: Router,
              @Inject(DETAILED_COVERAGE_TABLE) protected coverageGrid: GridService) {
    super(stepViewService, router, renderer, dialogService, coverageGrid);
    this.componentData$ = this.stepViewService.componentData$;
    this.currentStepIndex$ = this.modifDuringExecViewService.componentData$.pipe(
      select(selectCurrentActionStepIndex)
    );
    this.totalStepCount$ = this.modifDuringExecViewService.componentData$.pipe(
      select(selectTotalStepCount)
    );
  }

  ngOnInit(): void {
    combineLatest([this.route.parent.paramMap, this.route.paramMap]).pipe(
      take(1)
    ).subscribe(([parentParamMap, paramMap]) => {
      // 1 notify the parent service to set currentExecutionStep. It's a sync method, so we can safely call loadData after.
      const executionId = parseInt(parentParamMap.get('executionId'), 10);
      const executionStepId = parseInt(paramMap.get('stepId'), 10);
      this.modifDuringExecViewService.notifyCurrentExecutionStep(executionStepId);
      // 2 load data by using corresponding ACTION STEP id
      this.loadData(executionId);
    });
  }

  ngOnDestroy(): void {
    this.stepViewService.complete();
  }

  private loadData(executionId: number) {
    this.modifDuringExecViewService.currentActionStepId$.pipe(
      take(1),
    ).subscribe(actionStepId => {
      this.stepViewService.load(executionId, actionStepId);
    });
  }

  navigateBack() {
    this.modifDuringExecViewService.navigateBackToExecution();
  }

  openRequirementTreePicker() {
    this.stepViewService.openRequirementTreePicker();
  }

  closeRequirementPicker() {
    this.stepViewService.closeRequirementTreePicker();
  }

  drop($event: SqtmDropEvent) {
    if (isDndDataFromRequirementTreePicker($event)) {
      const requirementIds = this.extractRequirementIdsFromDragAndDropData($event);
      if (requirementIds.length > 0) {
        this.stepViewService.dropRequirement(requirementIds).subscribe(operationReport => {
          if (shouldShowCoverageMessageDialog(operationReport)) {
            this.dialogService.openDialog(createTestCaseCoverageMessageDialogConfiguration(operationReport));
          }
          this.removeBorderOnRequirementTable();
        });
      }
    }
  }

  dropFiles(files: File[]) {
    this.componentData$.pipe(
      take(1),
      filter(componentData => componentData.permissions.canAttach && componentData.milestonesAllowModification)
    ).subscribe(componentData => {
      this.unmarkAsFileDropZone();
      this.stepViewService.addAttachments(files, componentData.actionStep.attachmentList.id);
    });
  }

  dragFilesEnter() {
    this.componentData$.pipe(
      take(1),
      filter(componentData => componentData.permissions.canAttach && componentData.milestonesAllowModification)
    ).subscribe(() => this.markAsFileDropZone());
  }

  dragFileLeave() {
    this.unmarkAsFileDropZone();
  }

  getTestCaseName(actionStep: ModifDuringExecStepState): string {
    if (actionStep.executionTestCaseReference?.length > 0) {
      return `${actionStep.executionTestCaseReference} - ${actionStep.executionTestCaseName}`;
    } else {
      return actionStep.executionTestCaseName;
    }
  }

  getCalledTestCaseName(actionStep: ModifDuringExecStepState): string {
    if (actionStep.actionStepTestCaseReference?.length > 0) {
      return `${actionStep.actionStepTestCaseReference} - ${actionStep.actionStepTestCaseName}`;
    } else {
      return actionStep.actionStepTestCaseName;
    }
  }

  isCalledStep(actionStep: ModifDuringExecStepState) {
    return actionStep.actionStepTestCaseId !== actionStep.executionTestCaseId;
  }

  handleNavigateTo(stepIndex: number) {
    this.modifDuringExecViewService.navigateToStep(stepIndex);
  }

  private unmarkAsFileDropZone() {
    this.renderer.removeClass(this.dropFileZone.nativeElement, 'drop-requirement');
  }

  private markAsFileDropZone() {
    this.renderer.addClass(this.dropFileZone.nativeElement, 'drop-requirement');
  }
}

export interface ModifDuringExecStepViewComponentData
  extends EntityViewComponentData<ModifDuringExecStepState, 'actionStep', TestCasePermissions> {
}

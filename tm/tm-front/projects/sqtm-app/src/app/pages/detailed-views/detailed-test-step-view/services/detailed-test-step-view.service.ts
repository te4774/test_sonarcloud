import {Injectable} from '@angular/core';
import {
  ActionStepFormModel,
  AttachmentService,
  ChangeCoverageOperationReport,
  CoverageTestStepInfo,
  CustomFieldValueService,
  Dataset,
  DatasetParamValue,
  DeleteTestStepOperationReport,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  EntityViewState,
  GenericEntityViewState,
  GridService,
  Parameter,
  ProjectData,
  ReferentialDataService,
  RequirementVersionCoverage,
  RestService,
  StoreOptions,
  TestCaseModel,
  TestCaseParameterOperationReport,
  TestCasePermissions,
  TestCaseService,
  updateEntityInViewState,
  UploadAttachmentEvent
} from 'sqtm-core';
import {
  coveragesWithCurrentStepSelector,
  currentTestStepStateSelector,
  DetailedTestCaseState,
  DetailedTestStepViewState,
  provideInitialDetailedTestStepView
} from '../state/detailed-test-step-view.state';
import {TranslateService} from '@ngx-translate/core';
import {
  ParameterAssignationUpdate,
  TestStepViewService
} from '../../../test-case-workspace/test-case-view/service/test-step-view.service';
import {
  ActionStepState,
  stepsEntityAdapter,
  TestStepsState,
  TestStepState
} from '../../../test-case-workspace/test-case-view/state/test-step.state';
import {
  coverageEntityAdapter,
  CoverageState
} from '../../../test-case-workspace/test-case-view/state/requirement-version-coverage.state';
import {concatMap, finalize, map, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';
import {select} from '@ngrx/store';
import {Observable, Subject} from 'rxjs';
import {Update} from '@ngrx/entity';
import {detailedTestStepViewLogger} from '../detailed-test-step-view.logger';
import {stepCreationModeSelector} from '../../../test-case-workspace/test-case-view/state/test-case.state';
import {datasetEntityAdapter, DataSetState} from '../../../test-case-workspace/test-case-view/state/dataset.state';
import {
  parameterEntityAdapter,
  ParameterState
} from '../../../test-case-workspace/test-case-view/state/parameter.state';
import {
  datasetParamValueEntityAdapter,
  DatasetParamValueState
} from '../../../test-case-workspace/test-case-view/state/dataset-param-value.state';
import {
  StepViewRequirementLinkService
} from '../../../../components/detailed-step-views-common/step-view-requirement-link.service';

const logger = detailedTestStepViewLogger.compose('DetailedTestStepViewService');

const storeOptions: StoreOptions = {
  id: 'TestCaseViewStore',
  logDiff: 'detailed'
};

@Injectable()
export class DetailedTestStepViewService
  extends EntityViewService<DetailedTestCaseState, 'testCase', TestCasePermissions>
  implements StepViewRequirementLinkService {

  public readonly currentStepData$: Observable<TestStepState> = this.componentData$.pipe(
    select(currentTestStepStateSelector)
  );

  public readonly stepCreationMode$: Observable<boolean> = this.componentData$.pipe(
    select(stepCreationModeSelector)
  );

  private _notifyCreateStep = new Subject<void>();

  public notifyCreateStep$ = this._notifyCreateStep.asObservable();

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected testCaseService: TestCaseService,
              protected customFieldValueService: CustomFieldValueService,
              protected testStepViewService: TestStepViewService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              private coverageGrid: GridService,
  ) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper,
      storeOptions
    );
  }

  static validateIndex(index: number, state: DetailedTestStepViewState) {
    const ids = state.testCase.testSteps.ids;
    let validatedIndex = index;
    if (index < 0) {
      validatedIndex = 0;
    } else if (index >= ids.length) {
      validatedIndex = ids.length - 1;
    }
    return validatedIndex;
  }

  // By convenience and to speed up a lot development process we use the same shape as TestCaseModel
  load(id: number): void {
    this.restService
      .getWithoutErrorHandling<TestCaseModel>(['detailed-test-step-view', id.toString()])
      .subscribe((testCaseModel: TestCaseModel) => {
        this.initializeTestCase(testCaseModel);
        this.initializeCoverageGrid();
      }, err => this.notifyEntityNotFound(err));
  }

  public initializeTestCase(testCaseModel: TestCaseModel) {
    this.state$.pipe(
      take(1),
      withLatestFrom(this.referentialDataService.projectDatas$),
      map(([state, projectDataMap]) => {
        const attachmentEntityState = this.initializeAttachmentState(testCaseModel.attachmentList.attachments);
        const testSteps: TestStepsState =
          this.testStepViewService.initializeStepState(testCaseModel.testSteps, testCaseModel.kind, projectDataMap);
        const customFieldValueState = this.initializeCustomFieldValueState(testCaseModel.customFieldValues);
        const coverageState: CoverageState = this.initializeCoverageState(testCaseModel.coverages);
        const parameterState: ParameterState = this.initializeParameterState(testCaseModel.parameters);
        const datasetState: DataSetState = this.initializeDatasetState(testCaseModel.datasets);
        const datasetParamValueState: DatasetParamValueState = this.initializeDatasetParamValueState(testCaseModel.datasetParamValues);
        const entityState: Omit<DetailedTestCaseState, 'currentStepIndex'> = {
          ...testCaseModel,
          testSteps,
          attachmentList: {id: testCaseModel.attachmentList.id, attachments: attachmentEntityState},
          customFieldValues: customFieldValueState,
          coverages: coverageState,
          parameters: parameterState,
          datasets: datasetState,
          datasetParamValues: datasetParamValueState,
          uiState: {openTestCaseTreePicker: false, openRequirementTreePicker: false},
        };
        return {...state.testCase, ...entityState};
      })
    ).subscribe((entityState) => {
      this.initializeEntityState(entityState);
    });
  }

  private initializeCoverageState(coverages: RequirementVersionCoverage[]) {
    return coverageEntityAdapter.setAll(coverages, coverageEntityAdapter.getInitialState());
  }

  private initializeParameterState(parameters: Parameter[]) {
    return parameterEntityAdapter.setAll(parameters, parameterEntityAdapter.getInitialState());
  }

  private initializeDatasetState(datasets: Dataset[]) {
    return datasetEntityAdapter.setAll(datasets, datasetEntityAdapter.getInitialState());
  }

  private initializeDatasetParamValueState(paramValues: DatasetParamValue[]) {
    return datasetParamValueEntityAdapter.setAll(paramValues, datasetParamValueEntityAdapter.getInitialState());
  }

  addSimplePermissions(projectData: ProjectData): TestCasePermissions {
    return new TestCasePermissions(projectData);
  }

  getInitialState(): GenericEntityViewState<DetailedTestCaseState, 'testCase'> {
    return provideInitialDetailedTestStepView();
  }

  changeCurrentStepIndex(index: number) {
    this.state$.pipe(
      take(1),
      map((state: DetailedTestStepViewState) => {
        const validatedIndex = DetailedTestStepViewService.validateIndex(index, state);
        return {...state, testCase: {...state.testCase, currentStepIndex: validatedIndex}};
      })
    ).subscribe(state => this.commit(state));
  }

  private initializeCoverageGrid() {
    const coverage$ = this.componentData$.pipe(
      select(coveragesWithCurrentStepSelector)
    );
    this.coverageGrid.connectToDatasource(coverage$, 'requirementVersionId');
  }

  complete() {
    super.complete();
    this._notifyCreateStep.complete();
    this.coverageGrid.complete();
  }

  closeRequirementTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: DetailedTestStepViewState) =>
        ({
          ...state,
          testCase: {...state.testCase, uiState: {...state.testCase.uiState, openRequirementTreePicker: false}}
        })
      )
    ).subscribe(state => this.commit(state));
  }

  openRequirementTreePicker() {
    this.state$.pipe(
      take(1),
      map((state: DetailedTestStepViewState) =>
        ({
          ...state,
          testCase: {...state.testCase, uiState: {...state.testCase.uiState, openRequirementTreePicker: true}}
        })
      )
    ).subscribe(state => this.commit(state));
  }

  dropRequirement(requirementIds: number[]): Observable<ChangeCoverageOperationReport> {
    return this.state$.pipe(
      take(1),
      tap(() => this.coverageGrid.beginAsyncOperation()),
      concatMap((state: DetailedTestStepViewState) => {
        const currentStepId = this.getCurrentStepId(state);
        return this.testStepViewService.persistStepCoverages(requirementIds, currentStepId, state.testCase.id);
      }),
      withLatestFrom(this.state$),
      map(([operationReport, state]: [ChangeCoverageOperationReport, DetailedTestStepViewState]) => {
        const nextState = this.testStepViewService.dropRequirementForStepCoverages(operationReport, state);
        return {operationReport, nextState};
      }),
      tap(({operationReport, nextState}) => this.commit(nextState)),
      map(({operationReport, nextState}) => operationReport),
      finalize(() => this.coverageGrid.completeAsyncOperation())
    );
  }

  deleteCoverages(requirementVersionIds: number[]) {
    this.state$.pipe(
      take(1),
      tap(() => this.coverageGrid.beginAsyncOperation()),
      concatMap((state) => {
        return this.testCaseService.eraseCoverages(state.testCase.id, requirementVersionIds);
      }),
      withLatestFrom(this.state$),
      map(([operationReport, state]: [ChangeCoverageOperationReport, DetailedTestStepViewState]) => {
        const coverages = coverageEntityAdapter.setAll(operationReport.coverages, state.testCase.coverages);
        return {...state, testCase: {...state.testCase, coverages}};
      }),
      finalize(() => this.coverageGrid.completeAsyncOperation())
    ).subscribe(state => this.commit(state));
  }

  deleteStepCoverages(requirementVersionIds: number[]): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: DetailedTestStepViewState) =>
        this.testStepViewService.eraseStepCoverages(state.testCase.id, this.getCurrentStepId(state), requirementVersionIds)),
      withLatestFrom(this.state$),
      map(([operationReport, state]: [ChangeCoverageOperationReport, DetailedTestStepViewState]) =>
        this.testStepViewService.deleteStepCoverage(operationReport, state)),
      tap(state => this.commit(state))
    );
  }

  addActionStepForm(): Observable<DetailedTestStepViewState> {
    return this.state$.pipe(
      take(1),
      map((state: DetailedTestStepViewState) => this.testStepViewService.addActionStepForm(state.testCase.currentStepIndex + 1, state)),
      tap(state => this.commit(state))
    );
  }

  confirmAddActionStep(actionStepForm: ActionStepFormModel, testCaseId: number): Observable<any> {
    if (logger.isDebugEnabled()) {
      logger.debug(`Adding step to test case ${testCaseId}`, [actionStepForm]);
    }
    return this.testStepViewService.persistActionStep(actionStepForm, testCaseId).pipe(
      withLatestFrom(this.state$),
      map(([newStep, state]) =>
        this.testStepViewService.addNewActionStep(
          newStep.testStepState as ActionStepState,
          actionStepForm.index, state)),
      tap((state) => this.commit(state)),
    );
  }

  cancelAddTestStep(): Observable<any> {
    return this.state$.pipe(
      take(1),
      map((state: DetailedTestStepViewState) => this.testStepViewService.cancelAddTestStep(state)),
      map(state => {
        const currentStepIndex = Math.max(state.testCase.currentStepIndex - 1, 0);
        return {...state, testCase: {...state.testCase, currentStepIndex}};
      }),
      tap((state) => this.commit(state))
    );
  }

  changeAction(id: number, action: string): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      concatMap((state) => this.testStepViewService.writeAction(id, action).pipe(
        map(value => state)
      )),
      map(state => this.updateAction(id, action, state as any)),
      tap(state => this.commit(state))
    );
  }

  changeExpectedResult(id: number, expectedResult: string): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      concatMap((state) => this.testStepViewService.writeExpectedResult(id, expectedResult).pipe(
        map(value => state)
      )),
      map(state => this.updateExpectedResult(id, expectedResult, state as any)),
      tap(state => this.commit(state))
    );
  }

  updateStepCustomFieldValue(stepId: number, cufValueId: number, value: string | string[]) {
    this.store.state$.pipe(
      take(1),
      concatMap(state => this.testStepViewService.writeCustomFieldValue(stepId, cufValueId, value, state)),
      withLatestFrom(this.store.state$),
      map(([, initialState]) => this.updateStepCustomFieldValueInState(stepId, cufValueId, value, initialState))
    ).subscribe(state => this.commit(state));
  }


  addAttachmentsToStep(files: File[], stepId: number, attachmentListId: number): void {
    if (logger.isDebugEnabled()) {
      logger.debug(`Adding files to step ${stepId} : `, [files]);
    }

    this.state$.pipe(
      take(1),
      switchMap((state: EntityViewState<DetailedTestCaseState, 'testCase'>) => {
        const entity = this.getEntity(state);
        return this.attachmentHelper.addAttachments(files, attachmentListId, entity.id, state.type);
      }),
      withLatestFrom(this.store.state$)
    ).subscribe(([event, state]: [UploadAttachmentEvent, DetailedTestStepViewState]) => {
      this.commit(this.testStepViewService.mapUploadEventToState(stepId, state, event));
    });
  }

  markStepAttachmentsToDelete(ids: string[], stepId: number) {
    this.state$.pipe(
      take(1),
      map(state => this.testStepViewService.markStepAttachmentsToDelete(ids, stepId, state)))
      .subscribe(state => this.commit(state));
  }

  cancelStepAttachmentsToDelete(ids: string[], stepId: number) {
    this.state$.pipe(
      take(1),
      map(state => this.testStepViewService.cancelStepAttachmentsToDelete(ids, stepId, state)))
      .subscribe(state => this.commit(state));
  }

  removeStepRejectedAttachments(ids: string[], stepId: number) {
    this.state$.pipe(
      take(1),
      map((state) => this.testStepViewService.removeStepRejectedAttachments(ids, stepId, state)))
      .subscribe(state => this.commit(state));
  }

  deleteStepAttachments(attachmentIds: string[], stepId: number, attachmentListId: number) {
    const idsAsNumber = attachmentIds.map(id => parseInt(id, 10));

    this.state$.pipe(
      take(1),
      switchMap((state: EntityViewState<DetailedTestCaseState, 'testCase'>) => {
        const entity = this.getEntity(state);
        return this.attachmentHelper.eraseAttachments(idsAsNumber, attachmentListId, entity.id, state.type);
      }),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.testStepViewService.removeAttachments(attachmentIds, stepId, state))
    ).subscribe((state) => this.commit(state));
  }


  private getCurrentStepId(state: DetailedTestStepViewState): number {
    const stepStateIds = state.testCase.testSteps.ids;
    const currentStepIndex = state.testCase.currentStepIndex;
    return stepStateIds[currentStepIndex] as number;
  }

  private getCurrentStep(state: DetailedTestStepViewState) {
    const currentStepId = this.getCurrentStepId(state);
    return state.testCase.testSteps.entities[currentStepId];
  }

  linkRequirementToCurrentStep(requirementVersionId: number): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: DetailedTestStepViewState) =>
        this.testStepViewService.linkRequirementVersionToStep(this.getCurrentStepId(state), requirementVersionId)),
      withLatestFrom(this.state$),
      map(([, state]: [void, DetailedTestStepViewState]) =>
        this.addNewLinkToState(requirementVersionId, state)),
      tap(state => this.commit(state))
    );
  }

  private addNewLinkToState(requirementVersionId: number, state: DetailedTestStepViewState): DetailedTestStepViewState {
    const currentStepId = this.getCurrentStepId(state);
    const coverageStepInfos: CoverageTestStepInfo[] = [...state.testCase.coverages.entities[requirementVersionId].coverageStepInfos];
    const alreadyLinked = coverageStepInfos.filter(stepInfo => stepInfo.id === currentStepId).length > 0;

    if (!alreadyLinked) {
      coverageStepInfos.push({id: currentStepId, index: state.testCase.currentStepIndex});
    }
    const coverages = coverageEntityAdapter.updateOne({
      id: requirementVersionId,
      changes: {coverageStepInfos}
    }, state.testCase.coverages);
    return {...state, testCase: {...state.testCase, coverages}};
  }

  private updateAction(stepId: number, action: string, state: DetailedTestStepViewState): DetailedTestStepViewState {
    const testCase = {...state.testCase};
    const update: Update<ActionStepState> = {id: stepId, changes: {action}};
    testCase.testSteps = stepsEntityAdapter.updateOne(update, testCase.testSteps);
    return this.updateTestCase(testCase, state);
  }

  private updateExpectedResult(stepId: number, expectedResult: string, state: DetailedTestStepViewState): DetailedTestStepViewState {
    const testCase = {...state.testCase};
    const update: Update<ActionStepState> = {id: stepId, changes: {expectedResult: expectedResult}};
    testCase.testSteps = stepsEntityAdapter.updateOne(update, testCase.testSteps);
    return this.updateTestCase(testCase, state);
  }

  /**
   * Update the custom field value client side in TestStepState. Do not send http request, it should be called in case of request success.
   */
  updateStepCustomFieldValueInState(stepId: number,
                                    cufValueId: number,
                                    value: string | string[],
                                    initialState: Readonly<DetailedTestStepViewState>): DetailedTestStepViewState {
    const testStep = initialState.testCase.testSteps.entities[stepId] as ActionStepState;
    const customFieldValueState = this.customFieldHelper.updateCustomFieldValue(cufValueId, value, testStep.customFieldValues);
    const changes: Update<ActionStepState> = {id: testStep.id, changes: {customFieldValues: customFieldValueState}};
    const testStepsState = stepsEntityAdapter.updateOne(changes, initialState.testCase.testSteps);
    const testCase: DetailedTestCaseState = {...initialState.testCase, testSteps: testStepsState};
    return this.updateTestCase(testCase, initialState);

  }

  private updateTestCase(testCase: DetailedTestCaseState, state: DetailedTestStepViewState): DetailedTestStepViewState {
    return updateEntityInViewState<DetailedTestCaseState, 'testCase'>(testCase, state);
  }

  notifyCreateStep() {
    this._notifyCreateStep.next();
  }

  updateParameterAssignationMode(stepId: number, parameterAssignationMode: ParameterAssignationUpdate) {
    this.state$.pipe(
      take(1),
      concatMap(state => {
          const mode = this.testStepViewService.extractParameterMode(parameterAssignationMode);
          return this.testStepViewService.updateParameterAssignationMode(state.testCase.id, stepId, parameterAssignationMode, mode)
            .pipe(
              map(report => {
                const newState = this.testStepViewService.updateCallStepParameter(stepId,
                  parameterAssignationMode.datasetId,
                  parameterAssignationMode.datasetName,
                  parameterAssignationMode.delegateParam,
                  state);
                return this.updateParameterState(report, newState);
              })
            );
        }
      )
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  updateParameterState(operationParamDataSet: TestCaseParameterOperationReport, state: DetailedTestStepViewState) {
    const datasets = datasetEntityAdapter.setAll(operationParamDataSet.dataSets, state.testCase.datasets);
    const parameters = parameterEntityAdapter.setAll(operationParamDataSet.parameters, state.testCase.parameters);
    const datasetParamValues = datasetParamValueEntityAdapter
      .setAll(operationParamDataSet.paramValues, state.testCase.datasetParamValues);
    return {...state, testCase: {...state.testCase, datasets, parameters, datasetParamValues}};
  }

  deleteCurrentStep(): Observable<DetailedTestStepViewState> {
    return this.state$.pipe(
      take(1),
      concatMap(state => this.testStepViewService.eraseSteps(this.getCurrentStepId(state), state)),
      withLatestFrom(this.state$),
      map(([stepDeleteOperation, state]: [
        DeleteTestStepOperationReport, DetailedTestStepViewState]) => {
        const newState = this.testStepViewService.removeSteps(stepDeleteOperation.testStepsToDelete, state);
        return this.updateParameterState(stepDeleteOperation.operationReport, newState);
      }),
      map(state => {
        const currentStepIndex = Math.max(state.testCase.currentStepIndex - 1, 0);
        return {...state, testCase: {...state.testCase, currentStepIndex}};
      }),
      tap(state => this.commit(state))
    );
  }
}

import {Injectable} from '@angular/core';
import {
  AttachmentService,
  ChartBinding,
  CustomDashboardModel,
  CustomDashboardService,
  CustomFieldValueService,
  CustomReportPermissions,
  DroppedNewItemInDashboard,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ProjectData,
  ReferentialDataService,
  ReportBinding,
  RestService,
  WorkspaceKeys
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {
  chartBindingAdapter,
  DashboardState,
  DashboardViewState,
  provideInitialDashboardViewState,
  reportBindingAdapter
} from '../state/dashboard-view.state';
import {concatMap, map, take, withLatestFrom} from 'rxjs/operators';


/**
 * Service dedicated to handle dashboard view state. Note that ChartBindings and ReportBindings are handled here :
 *  - At load time
 *  - When first time dropping a chart/report
 *  - When the dashboard is fully cleared by suppressing everything
 *
 * When the dashboard is filled, AngularGridster internal mutable state will handle the internal state of dashboard.
 *
 *  This separation of concerns can appear strange but it will allow to have a better SRP.
 *  It allow custom messages when mounting dashboard in different screens, avoid unnecessary http request,
 *  ease permissions handling...
 */
@Injectable()
export class DashboardViewService extends EntityViewService<DashboardState, 'dashboard', CustomReportPermissions> {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService,
              private customDashboardService: CustomDashboardService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(projectData: ProjectData) {
    return new CustomReportPermissions(projectData);
  }

  getInitialState(): DashboardViewState {
    return provideInitialDashboardViewState();
  }

  load(customReportLibraryNodeId: number) {
    this.customDashboardService.getDashboard(customReportLibraryNodeId)
      .pipe(withLatestFrom(this.state$))
      .subscribe(([dashboardModel, state]: [CustomDashboardModel, DashboardViewState]) => {
        const chartBindings = chartBindingAdapter.setAll(dashboardModel.chartBindings, chartBindingAdapter.getInitialState());
        const reportBindings = reportBindingAdapter.setAll(dashboardModel.reportBindings, reportBindingAdapter.getInitialState());
        const chartDefinitionState = {
          ...state.dashboard, ...dashboardModel,
          chartBindings,
          reportBindings,
          generatedOn: new Date()
        };
        this.store.commit(this.updateEntity(chartDefinitionState, state));
      }, err => this.notifyEntityNotFound(err));
  }

  refresh() {
    this.state$.pipe(
      take(1)
    ).subscribe(state => this.load(state.dashboard.customReportLibraryNodeId));
  }

  dropNewItem(droppedNewItemInDashboard: DroppedNewItemInDashboard) {
    if (droppedNewItemInDashboard.chartDefinitionNodeId) {
      this.dropNewChart(droppedNewItemInDashboard);
    }

    if (droppedNewItemInDashboard.reportDefinitionNodeId) {
      this.dropNewReport(droppedNewItemInDashboard);
    }
  }

  private dropNewChart(droppedNewItemInDashboard: DroppedNewItemInDashboard) {
    this.store.state$.pipe(
      take(1),
      map((state: DashboardViewState) => state.dashboard.id),
      concatMap((dashboardId) => this.customDashboardService.addNewChartInDashboard(dashboardId, droppedNewItemInDashboard)),
      withLatestFrom(this.store.state$),
      map(([persistedBinding, state]: [ChartBinding, DashboardViewState]) => {
        const chartBindingEntityState = chartBindingAdapter.addOne(persistedBinding, state.dashboard.chartBindings);
        const dashboard: DashboardState = {...state.dashboard, chartBindings: chartBindingEntityState};
        return this.updateEntity(dashboard, state);
      })
    ).subscribe(state => this.store.commit(state));
  }

  private dropNewReport(droppedNewItemInDashboard: DroppedNewItemInDashboard) {
    this.store.state$.pipe(
      take(1),
      map((state: DashboardViewState) => state.dashboard.id),
      concatMap((dashboardId) => this.customDashboardService.addNewReportInDashboard(dashboardId, droppedNewItemInDashboard)),
      withLatestFrom(this.store.state$),
      map(([persistedBinding, state]: [ReportBinding, DashboardViewState]) => {
        const reportBindingEntityState = reportBindingAdapter.addOne(persistedBinding, state.dashboard.reportBindings);
        const dashboard: DashboardState = {...state.dashboard, reportBindings: reportBindingEntityState};
        return this.updateEntity(dashboard, state);
      })
    ).subscribe(state => this.store.commit(state));
  }

  clearDashboard() {
    this.store.state$.pipe(
      take(1),
      map((state: DashboardViewState) => ({
        ...state,
        dashboard: {
          ...state.dashboard,
          chartBindings: chartBindingAdapter.getInitialState(),
          reportBindings: reportBindingAdapter.getInitialState()
        }
      }))).subscribe(state => this.store.commit(state));
  }

  bindToFavoriteDashboard(workspaceId: WorkspaceKeys) {
    this.store.state$.pipe(
      take(1),
      map((state: DashboardViewState) => state.dashboard.customReportLibraryNodeId),
      concatMap((customReportLibraryNodeId) => this.customDashboardService.bindToFavoriteDashboard(customReportLibraryNodeId, workspaceId)),
      withLatestFrom(this.store.state$),
      map(([favoriteWorkspace, state]: [WorkspaceKeys[], DashboardViewState]) => {
        return {...state, dashboard: {...state.dashboard, favoriteWorkspaces: favoriteWorkspace}};
      })
    ).subscribe(state => this.store.commit(state));
  }

  removeWorkspaceFromFavorite(workspaceId: WorkspaceKeys) {
    this.store.state$.pipe(
      take(1),
      map((state: DashboardViewState) => state.dashboard.customReportLibraryNodeId),
      concatMap((customReportLibraryNodeId) => this.customDashboardService.removeWorkspaceFromFavoriteDashboard(customReportLibraryNodeId, workspaceId)),
      withLatestFrom(this.store.state$),
      map(([, state]: [void, DashboardViewState]) => {
        let favoriteWorkspace: WorkspaceKeys[] = (state.dashboard.favoriteWorkspaces).filter(workspace => workspace !== workspaceId);
        return {...state, dashboard: {...state.dashboard, favoriteWorkspaces: favoriteWorkspace}};
      })
    ).subscribe(state => this.store.commit(state));
  }

  removeDashboardFromFavorites() {
    this.store.state$.pipe(
      take(1),
      map((state: DashboardViewState) => state.dashboard.customReportLibraryNodeId),
      concatMap((customReportLibraryNodeId) => this.customDashboardService.removeDashboardFromFavorites(customReportLibraryNodeId)),
      withLatestFrom(this.store.state$),
      map(([, state]: [void, DashboardViewState]) => {
        return {...state, dashboard: {...state.dashboard, favoriteWorkspaces: []}};
      })
    ).subscribe(state => this.store.commit(state));
  }
}

export interface DashboardViewComponentData
  extends EntityViewComponentData<DashboardState, 'dashboard', CustomReportPermissions> {
}

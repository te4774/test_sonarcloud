import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {ReferentialDataService, ReportDefinitionService, ReportWorkbenchData} from 'sqtm-core';
import {ActivatedRoute, Params} from '@angular/router';
import {concatMap, map, withLatestFrom} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-modify-report-view',
  templateUrl: './modify-report-view.component.html',
  styleUrls: ['./modify-report-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModifyReportViewComponent implements OnInit {

  public workbenchData$ = new Subject<ReportWorkbenchData>();

  constructor(private referentialDataService: ReferentialDataService,
              private reportDefinitionService: ReportDefinitionService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh().pipe(
      withLatestFrom(this.activatedRoute.params),
      concatMap(([, params]: [boolean, Params]) => {
        const id = Number.parseInt(params['customReportLibraryNodeId'], 10);
        return this.reportDefinitionService.getWorkbenchData(id).pipe(
          map((workbenchData: ReportWorkbenchData) => ({...workbenchData, containerId: id})),
        );
      })
    ).subscribe((workbenchData: ReportWorkbenchData) => {
      this.workbenchData$.next(workbenchData);
    });
  }

}

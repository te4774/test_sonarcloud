import {TestBed} from '@angular/core/testing';

import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {CustomReportFolderViewService} from './custom-report-folder-view.service';

describe('CustomReportFolderViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
    providers: [
      {
        provide: CustomReportFolderViewService,
        useClass: CustomReportFolderViewService
      }
    ]
  }));

  it('should be created', () => {
    const service: CustomReportFolderViewService = TestBed.get(CustomReportFolderViewService);
    expect(service).toBeTruthy();
  });
});

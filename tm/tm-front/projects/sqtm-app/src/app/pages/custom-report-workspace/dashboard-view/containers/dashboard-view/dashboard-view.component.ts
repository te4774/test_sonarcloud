import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Optional,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  EntityRowReference,
  EntityViewService,
  GenericEntityViewService,
  Identifier,
  ReferentialDataService,
  SquashTmDataRowType,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {DashboardViewComponentData, DashboardViewService} from '../../services/dashboard-view.service';
import {Overlay, OverlayRef} from '@angular/cdk/overlay';

@Component({
  selector: 'sqtm-app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DashboardViewService,
    {
      provide: EntityViewService,
      useExisting: DashboardViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: DashboardViewService
    }
  ]
})
export class DashboardViewComponent implements OnInit, OnDestroy {

  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  @ViewChild('containerRef', {read: ElementRef})
  containerRef: ElementRef;

  overlayRef: OverlayRef;
  private unsub$ = new Subject<void>();
  componentData$: Observable<DashboardViewComponentData>;

  constructor(private dashboardViewService: DashboardViewService,
              private referentialDataService: ReferentialDataService,
              private route: ActivatedRoute,
              private overlay: Overlay,
              private vcr: ViewContainerRef,
              @Optional() private workspaceWithTree: WorkspaceWithTreeComponent) {
    this.componentData$ = this.dashboardViewService.componentData$;
  }

  ngOnInit(): void {
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      this.loadData();
      this.initializeTreeSynchronization();
    });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('customReportLibraryNodeId')),
      ).subscribe((id) => {
      this.dashboardViewService.load(parseInt(id, 10));
    });
  }

  private initializeTreeSynchronization() {
    this.dashboardViewService.simpleAttributeRequiringRefresh = ['name'];
    this.dashboardViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.dashboardViewService.componentData$),
      map(([{}, componentData]: [{}, DashboardViewComponentData]) =>
        new EntityRowReference(componentData.dashboard.customReportLibraryNodeId, SquashTmDataRowType.CustomReportDashboard).asString())
    ).subscribe((identifier: Identifier) => {
      if (this.workspaceWithTree) {
        this.workspaceWithTree.requireNodeRefresh([identifier]);
      }
    });
  }

  ngOnDestroy(): void {
    this.dashboardViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}

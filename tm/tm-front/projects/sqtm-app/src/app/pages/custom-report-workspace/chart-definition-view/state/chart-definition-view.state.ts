import {EntityViewState, provideInitialViewState} from 'sqtm-core';
import {ChartDefinitionState} from './chart-definition.state';

export interface ChartDefinitionViewState extends EntityViewState<ChartDefinitionState, 'chartDefinition'> {
  chartDefinition: ChartDefinitionState;
}

export function provideInitialChartDefinitionView(): Readonly<ChartDefinitionViewState> {
  return provideInitialViewState<ChartDefinitionState, 'chartDefinition'>('chartDefinition');
}

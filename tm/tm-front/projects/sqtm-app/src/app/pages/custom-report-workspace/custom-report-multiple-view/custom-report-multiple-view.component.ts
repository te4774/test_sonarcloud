import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'sqtm-app-custom-report-multiple-view',
  templateUrl: './custom-report-multiple-view.component.html',
  styleUrls: ['./custom-report-multiple-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomReportMultipleViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

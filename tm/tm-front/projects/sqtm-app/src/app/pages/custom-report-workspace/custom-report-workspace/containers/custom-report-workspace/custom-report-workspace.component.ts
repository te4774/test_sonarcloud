import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {
  column,
  CustomReportTreeNodeServerOperationHandler,
  Extendable,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
  tree,
  TreeNodeCellRendererComponent,
  TreeNodeRendererDisplayOptions
} from 'sqtm-core';
import {CR_WS_TREE, CR_WS_TREE_CONFIG, customReportWorkspaceTreeId} from '../../../custom-report-workspace.constant';

export function customReportWorkspaceTreeConfigFactory(): GridDefinition {
  const options: TreeNodeRendererDisplayOptions = {ellipsisOnLeft: false, kind: 'treeNodeRendererDisplay'};
  return tree(customReportWorkspaceTreeId)
    .server()
    .withServerUrl(['custom-report-tree'])
    .withColumns([
      column('NAME')
        .enableDnd()
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent)
        .withOptions(options)
    ])
    .build();
}

@Component({
  selector: 'sqtm-app-custom-report-workspace',
  templateUrl: './custom-report-workspace.component.html',
  styleUrls: ['./custom-report-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CR_WS_TREE_CONFIG,
      useFactory: customReportWorkspaceTreeConfigFactory,
      deps: []
    },
    CustomReportTreeNodeServerOperationHandler,
    {
      provide: CR_WS_TREE,
      useFactory: gridServiceFactory,
      deps: [RestService, CR_WS_TREE_CONFIG, ReferentialDataService, CustomReportTreeNodeServerOperationHandler]
    },
    {
      provide: GridService,
      useExisting: CR_WS_TREE
    }]
})
export class CustomReportWorkspaceComponent implements OnInit, OnDestroy {

  @Input() name: String = 'custom-report-workspace';

  constructor(public readonly referentialDataService: ReferentialDataService,
              private workspaceTree: GridService) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh().subscribe();
  }

  ngOnDestroy(): void {
    this.workspaceTree.complete();
  }

}

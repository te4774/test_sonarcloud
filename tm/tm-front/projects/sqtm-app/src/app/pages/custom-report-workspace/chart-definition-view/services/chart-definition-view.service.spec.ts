import {TestBed} from '@angular/core/testing';

import {ChartDefinitionViewService} from './chart-definition-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {ReferentialDataService, RestService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {mockRestService} from '../../../../utils/testing-utils/mocks.service';
import SpyObj = jasmine.SpyObj;

describe('ChartDefinitionViewService', () => {
  const restService = mockRestService();
  const referentialDataService: SpyObj<ReferentialDataService> = jasmine.createSpyObj<ReferentialDataService>(['refresh']);


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: ChartDefinitionViewService,
          useClass: ChartDefinitionViewService
        },
        {
          provide: ReferentialDataService,
          useValue: referentialDataService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  it('should be created', () => {
    const service: ChartDefinitionViewService = TestBed.inject(ChartDefinitionViewService);
    expect(service).toBeTruthy();
  });
});

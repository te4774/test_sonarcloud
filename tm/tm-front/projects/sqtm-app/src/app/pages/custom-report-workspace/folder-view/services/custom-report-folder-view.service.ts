import {Injectable} from '@angular/core';
import {
  AttachmentService,
  CustomFieldValueService,
  CustomReportFolderModel,
  CustomReportPermissions,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ProjectData,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {CustomReportFolderState} from '../states/custom-report-folder.state';
import {TranslateService} from '@ngx-translate/core';
import {
  CustomReportFolderViewState,
  provideInitialCustomReportFolderView
} from '../states/custom-report-folder-view.state';


@Injectable()
export class CustomReportFolderViewService
  extends EntityViewService<CustomReportFolderState, 'customReportFolder', CustomReportPermissions> {

  constructor(protected restService: RestService,
              protected referentialDataService: ReferentialDataService,
              protected attachmentService: AttachmentService,
              protected translateService: TranslateService,
              protected customFieldValueService: CustomFieldValueService,
              protected attachmentHelper: EntityViewAttachmentHelperService,
              protected customFieldHelper: EntityViewCustomFieldHelperService) {
    super(
      restService,
      referentialDataService,
      attachmentService,
      translateService,
      customFieldValueService,
      attachmentHelper,
      customFieldHelper
    );
  }

  addSimplePermissions(projectData: ProjectData): CustomReportPermissions {
    return new CustomReportPermissions(projectData);
  }

  getInitialState(): CustomReportFolderViewState {
    return provideInitialCustomReportFolderView();
  }
  load(id: number) {
    this.restService.getWithoutErrorHandling<CustomReportFolderModel>(['custom-report-folder-view', id.toString()])
      .subscribe((customReportFolderModel: CustomReportFolderModel) => {
        const customReportFolder = this.initializeCustomReportFolderState(customReportFolderModel);
        this.initializeEntityState(customReportFolder);
      }, err => this.notifyEntityNotFound(err));
  }

  private initializeCustomReportFolderState(customReportFolderModel: CustomReportFolderModel): CustomReportFolderState {
    const customFieldValueState = this.initializeCustomFieldValueState(customReportFolderModel.customFieldValues);
    return {
      ...customReportFolderModel,
      attachmentList: {id: null, attachments: null},
      customFieldValues: customFieldValueState
    };
  }
}

export interface CustomReportFolderViewComponentData
  extends EntityViewComponentData<CustomReportFolderState, 'customReportFolder', CustomReportPermissions> {
}


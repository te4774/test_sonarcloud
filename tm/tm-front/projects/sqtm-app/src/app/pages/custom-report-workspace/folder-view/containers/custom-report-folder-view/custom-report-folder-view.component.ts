import {ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, Optional, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {
  EntityRowReference,
  EntityViewService,
  GenericEntityViewService,
  Identifier,
  ReferentialDataService,
  SquashTmDataRowType,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {
  CustomReportFolderViewComponentData,
  CustomReportFolderViewService
} from '../../services/custom-report-folder-view.service';

@Component({
  selector: 'sqtm-app-custom-report-folder-view',
  templateUrl: './custom-report-folder-view.component.html',
  styleUrls: ['./custom-report-folder-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CustomReportFolderViewService,
      useClass: CustomReportFolderViewService,
    },
    {
      provide: EntityViewService,
      useExisting: CustomReportFolderViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: CustomReportFolderViewService
    }
  ]
})
export class CustomReportFolderViewComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  @ViewChild('content', {read: ElementRef})
  content: ElementRef;

  constructor(private referentialDataService: ReferentialDataService,
              public readonly customReportFolderViewService: CustomReportFolderViewService,
              private route: ActivatedRoute,
              @Optional() private workspaceWithTree: WorkspaceWithTreeComponent) {
  }

  ngOnInit(): void {
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      this.loadData();
    });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('customReportLibraryNodeId')),
      ).subscribe((id) => {
      this.customReportFolderViewService.load(parseInt(id, 10));
      this.initializeTreeSynchronization();
    });
  }

  private initializeTreeSynchronization() {
    this.customReportFolderViewService.simpleAttributeRequiringRefresh = ['name'];
    this.customReportFolderViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.customReportFolderViewService.componentData$),
      map(([{}, componentData]: [{}, CustomReportFolderViewComponentData]) =>
        new EntityRowReference(componentData.customReportFolder.id, SquashTmDataRowType.CustomReportFolder).asString())
    ).subscribe((identifier: Identifier) => {
      this.workspaceWithTree.requireNodeRefresh([identifier]);
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}

import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {DashboardViewComponentData, DashboardViewService} from '../../services/dashboard-view.service';
import {Observable} from 'rxjs';
import {select} from '@ngrx/store';
import {allBindingSelector} from '../../state/dashboard-view.state';
import {
  AbstractSqtmDragAndDropEvent,
  CustomDashboardBinding,
  CustomReportPermissions,
  DataRow,
  DroppedNewItemInDashboard,
  EntityRowReference,
  SqtmDragEnterEvent,
  SqtmDropEvent,
  SquashTmDataRowType,
  toEntityRowReference,
  WorkspaceKeys
} from 'sqtm-core';
import {customReportWorkspaceTreeId} from '../../../custom-report-workspace.constant';
import {ConnectedPosition} from '@angular/cdk/overlay/position/flexible-connected-position-strategy';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {TemplatePortal} from '@angular/cdk/portal';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-dashboard-view-content',
  templateUrl: './dashboard-view-content.component.html',
  styleUrls: ['./dashboard-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardViewContentComponent implements OnInit {

  componentData$: Observable<DashboardViewComponentData>;
  bindings$: Observable<CustomDashboardBinding[]>;

  @ViewChild('emptyDashboard', {read: ElementRef})
  emptyDashboard: ElementRef;

  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  @ViewChild('containerRef', {read: ElementRef})
  containerRef: ElementRef;

  overlayRef: OverlayRef;

  constructor(private dashboardViewService: DashboardViewService, private renderer: Renderer2,
              private vcr: ViewContainerRef, private overlay: Overlay,
              private translateService: TranslateService) {
    this.componentData$ = this.dashboardViewService.componentData$;
    this.bindings$ = this.dashboardViewService.componentData$.pipe(
      select(allBindingSelector)
    );
  }

  ngOnInit(): void {
  }

  refreshStats($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.dashboardViewService.refresh();
  }

  dropNewItemInDashboard(droppedNewItemInDashboard: DroppedNewItemInDashboard) {
    this.dashboardViewService.dropNewItem(droppedNewItemInDashboard);
  }

  dropInEmptyChart(dropEvent: SqtmDropEvent, permissions: CustomReportPermissions) {
    if (this.dragIsFromCustomReportTree(dropEvent)) {
      const dataRow: DataRow = this.extractFirstEligibleRow(dropEvent);
      if (dataRow && permissions.canWrite) {
        const entityRowReference: EntityRowReference = toEntityRowReference(dataRow.id);
        const droppedItem: DroppedNewItemInDashboard = {
          row: 1,
          col: 1,
          sizeX: 1,
          sizeY: 1,
          chartDefinitionNodeId: entityRowReference.entityType === SquashTmDataRowType.ChartDefinition ? entityRowReference.id : null,
          reportDefinitionNodeId: entityRowReference.entityType === SquashTmDataRowType.ReportDefinition ? entityRowReference.id : null,
        };
        this.dashboardViewService.dropNewItem(droppedItem);
      }
    }
  }

  dragEnter(dragEnterEvent: SqtmDragEnterEvent, permissions: CustomReportPermissions) {
    if (this.dragIsFromCustomReportTree(dragEnterEvent)) {
      const dataRow: DataRow = this.extractFirstEligibleRow(dragEnterEvent);
      if (dataRow && permissions.canWrite) {
        this.markAsDroppable();
      }
    }
  }

  dragLeave() {
    this.unmarkAsDroppable();
  }

  private markAsDroppable() {
    if (this.emptyDashboard) {
      this.renderer.addClass(this.emptyDashboard.nativeElement, '__hover_current_ws_border');
    }
  }

  private unmarkAsDroppable() {
    if (this.emptyDashboard) {
      this.renderer.removeClass(this.emptyDashboard.nativeElement, '__hover_current_ws_border');
    }
  }

  private dragIsFromCustomReportTree(dropEvent: AbstractSqtmDragAndDropEvent) {
    return dropEvent.dragAndDropData.origin === customReportWorkspaceTreeId;
  }

  private extractFirstEligibleRow(dropEvent: AbstractSqtmDragAndDropEvent): DataRow | undefined {
    const dataRows: DataRow[] = dropEvent.dragAndDropData.data.dataRows;
    return dataRows.find(
      r => r.type === SquashTmDataRowType.ChartDefinition
        || r.type === SquashTmDataRowType.ReportDefinition);
  }

  clearDashboard() {
    this.dashboardViewService.clearDashboard();
  }

  showList($event: MouseEvent) {

    $event.stopPropagation();

    const positions: ConnectedPosition[] = [
      {originX: 'center', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetY: 10},
      {originX: 'center', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetY: -10},
    ];

    const positionStrategy = this.overlay.position().flexibleConnectedTo(this.containerRef)
      .withPositions(positions);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      width: 200
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const templatePortal = new TemplatePortal(this.templatePortalContent, this.vcr);
    this.overlayRef.attach(templatePortal);
    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  private close() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
    }
  }

  dashboardIsFavoriteInWorkspace(componentData: DashboardViewComponentData, workspace: WorkspaceKeys) {
    return componentData.dashboard.favoriteWorkspaces.includes(workspace);
  }

  addOrWithdrawFavorite(componentData: DashboardViewComponentData, workspace: WorkspaceKeys) {
    let isFavorite = this.dashboardIsFavoriteInWorkspace(componentData, workspace);
    if (isFavorite) {
      this.withdrawWorkspaceFromFavorite(workspace);
    } else {
      this.addDashboardToFavorite(workspace);
    }
  }

  addDashboardToFavorite(workspaceId: WorkspaceKeys) {
    this.dashboardViewService.bindToFavoriteDashboard(workspaceId);
  }

  withdrawWorkspaceFromFavorite(workspaceId: WorkspaceKeys) {
    this.dashboardViewService.removeWorkspaceFromFavorite(workspaceId);
  }

  removeDashboardFromFavorites() {
    this.dashboardViewService.removeDashboardFromFavorites();
  }

  getAuditableText(date: string, userLogin: string) {
    if (date != null) {
      return `${date} (${userLogin})`;
    } else {
      return this.translateService.instant('sqtm-core.generic.label.never');
    }
  }
}

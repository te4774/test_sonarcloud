import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {CustomExportState} from '../../state/custom-export.state';
import {
  IdentifiedExportColumn
} from '../../../../../components/custom-export-workbench/state/custom-export-workbench.state';
import {CustomExportColumn, DisplayOption, ProjectData, ReferentialDataService} from 'sqtm-core';
import {take} from 'rxjs/operators';
import {DatePipe} from '@angular/common';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'sqtm-app-custom-export-information-panel',
  templateUrl: './custom-export-information-panel.component.html',
  styleUrls: ['./custom-export-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class CustomExportInformationPanelComponent implements OnInit {

  identifiedColumns: IdentifiedExportColumn[] = [];
  printExecutionOptions: DisplayOption[];
  formGroup: FormGroup;

  @Input()
  set customExport(customExport: CustomExportState) {
    this._customExport = customExport;
    this.refreshIdentifiedColumns();
  }

  get customExport(): CustomExportState {
    return this._customExport;
  }

  private _customExport: CustomExportState;

  constructor(public readonly referentialDataService: ReferentialDataService,
              public readonly cdr: ChangeDetectorRef,
              public readonly fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.printExecutionOptions = [
      {id: ExecutionPrintOptions.PRINT_LAST_EXECUTION, label: 'sqtm-core.custom-report-workspace.create-custom-export.workbench.label.options.last-execution'},
      {id: ExecutionPrintOptions.PRINT_ALL_EXECUTIONS , label: 'sqtm-core.custom-report-workspace.create-custom-export.workbench.label.options.all-executions'}
      ];
    this.formGroup = this.fb.group({
      printExecutionOption: this.fb.control(ExecutionPrintOptions.PRINT_LAST_EXECUTION, [Validators.required])
    });
  }

  private refreshIdentifiedColumns(): void {
    if (this._customExport) {
      this.referentialDataService.connectToProjectData(this._customExport.projectId).pipe(
        take(1),
      ).subscribe(projectData => {
        this.identifiedColumns = this.asIdentifiedColumns(this._customExport.columns, projectData);
        this.cdr.markForCheck();
      });
    }
  }

  private asIdentifiedColumns(columns: CustomExportColumn[], projectData: ProjectData): IdentifiedExportColumn[] {
    return columns.map(col => ({
      columnName: col.columnName,
      entityType: col.entityType,
      cufId: col.cufId,
      label: this.findLabel(col, projectData),
      id: undefined,
    }));
  }

  private findLabel(column: CustomExportColumn, projectData: ProjectData): string {
    if (Boolean(column.cufId)) {
      return this.findCufLabel(column, projectData);
    } else {
      return column.columnName;
    }
  }

  private findCufLabel(column: CustomExportColumn, projectData: ProjectData): string {
    const customField = Object.values(projectData.customFieldBinding).flat()
      .find(binding => binding.customField.id === column.cufId)?.customField;

    if (customField) {
      return customField.label;
    } else {
      return 'sqtm-core.generic.label.deleted';
    }
  }
}

export enum ExecutionPrintOptions {
  PRINT_ALL_EXECUTIONS = 'allExec',
  PRINT_LAST_EXECUTION = 'lastExec'
}


import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {concatMap, map, takeUntil, withLatestFrom} from 'rxjs/operators';
import {ActivatedRoute, ParamMap, Params} from '@angular/router';
import {
  ChartDefinitionService,
  ChartWorkbenchData,
  EntityViewService,
  GenericEntityViewService,
  ReferentialDataService
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {ChartDefinitionViewService} from '../chart-definition-view/services/chart-definition-view.service';
import {
  ChartDefinitionViewComponentData
} from '../chart-definition-view/containers/chart-definition-view/chart-definition-view.component';

@Component({
  selector: 'sqtm-app-modify-chart-view',
  templateUrl: './modify-chart-view.component.html',
  styleUrls: ['./modify-chart-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ChartDefinitionViewService,
      useClass: ChartDefinitionViewService,
    },
    {
      provide: EntityViewService,
      useExisting: ChartDefinitionViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: ChartDefinitionViewService
    }
  ]
})
export class ModifyChartViewComponent implements OnInit, OnDestroy {

  public componentData$: Observable<ChartDefinitionViewComponentData>;
  public workbenchData$ = new Subject<ChartWorkbenchData>();

  private unsub$ = new Subject<void>();

  constructor(private referentialDataService: ReferentialDataService,
              private chartDefinitionService: ChartDefinitionService,
              private chartDefinitionViewService: ChartDefinitionViewService,
              private activatedRoute: ActivatedRoute,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh().pipe(
      withLatestFrom(this.activatedRoute.params),
      concatMap(([, params]: [boolean, Params]) => {
        const id = Number.parseInt(params['customReportLibraryNodeId'], 10);
        return this.chartDefinitionService.getWorkbenchData(id);
      })
    ).subscribe((workbenchData: ChartWorkbenchData) => {
      this.workbenchData$.next(workbenchData);
      this.loadData();
      this.componentData$ = this.chartDefinitionViewService.componentData$;
    });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('customReportLibraryNodeId')),
      ).subscribe((id) => {
      this.chartDefinitionViewService.load(parseInt(id, 10));
    });
  }

  ngOnDestroy(): void {
    this.chartDefinitionViewService.complete();
    this.workbenchData$.complete();
  }
}

import {ChangeDetectionStrategy, Component, ViewChild} from '@angular/core';
import {CustomExportViewService} from '../../services/custom-export-view.service';
import {Router} from '@angular/router';
import {pluck, take} from 'rxjs/operators';
import {DialogService} from 'sqtm-core';
import {
  CustomExportInformationPanelComponent,
  ExecutionPrintOptions
} from '../../components/custom-export-information-panel/custom-export-information-panel.component';

@Component({
  selector: 'sqtm-app-custom-export-view-content',
  templateUrl: './custom-export-view-content.component.html',
  styleUrls: ['./custom-export-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomExportViewContentComponent {

  @ViewChild(CustomExportInformationPanelComponent, {static: false})
  informationPanel: CustomExportInformationPanelComponent;

  constructor(public readonly customExportViewService: CustomExportViewService,
              public readonly router: Router,
              private dialogService: DialogService) {
  }

  handleModify(): void {
    this.customExportViewService.componentData$
      .pipe(take(1), pluck('customExport'))
      .subscribe(customExport => this.router.navigate([
        'custom-report-workspace',
        'create-custom-export',
        customExport.customReportLibraryNodeId]));
  }

  openAlertForDeletedScope($event: MouseEvent) {
    $event.stopPropagation();
    this.dialogService.openAlert({
      id: 'deleted-scope-alert',
      messageKey: 'sqtm-core.custom-report-workspace.custom-export.scope.deleted',
      level: 'DANGER'
    });
  }

  getDownloadUrl() {
    if (this.informationPanel) {
      const printOnlyLastExec = this.informationPanel.formGroup.get('printExecutionOption').value === ExecutionPrintOptions.PRINT_LAST_EXECUTION;
      return this.customExportViewService.getDownloadUrl(printOnlyLastExec);
    }
    return this.customExportViewService.getDownloadUrl(false);
  }
}

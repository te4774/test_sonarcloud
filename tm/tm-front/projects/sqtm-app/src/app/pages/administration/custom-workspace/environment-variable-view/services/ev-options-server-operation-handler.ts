import {Injectable} from '@angular/core';
import {AbstractAdminServerOperationHandler, Identifier} from 'sqtm-core';
import {Observable} from 'rxjs';
import {EnvironmentVariableViewService} from './environment-variable-view.service';

@Injectable()
export class EvOptionsServerOperationHandler extends AbstractAdminServerOperationHandler {

  constructor(private readonly environmentVariableViewService: EnvironmentVariableViewService) {
    super();
  }

  doChangePosition(draggedRows: Identifier[], newPosition: number): Observable<any> {
    const optionIds = draggedRows.map(row => row.toString());
    return this.environmentVariableViewService.changeOptionPosition(optionIds, newPosition);
  }
}

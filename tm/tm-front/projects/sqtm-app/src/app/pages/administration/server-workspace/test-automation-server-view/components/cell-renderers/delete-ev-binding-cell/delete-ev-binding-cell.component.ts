import {Component, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {AbstractDeleteCellRenderer, DialogService, GridService} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';
import {finalize, takeUntil} from 'rxjs/operators';
import {AdminTestAutomationServerViewState} from '../../../states/admin-test-automation-server-view-state';

@Component({
  selector: 'sqtm-app-delete-ev-binding-cell',
  template: `
    <ng-container *ngIf="componentData$ | async as componentData">
      <sqtm-core-toggle-icon [attr.data-test-button-id]="'remove-options'"
                             class="full-height full-width icon-container current-workspace-main-color __hover_pointer">
        <i nz-icon nzType="sqtm-core-generic:unlink" nzTheme="outline"
           class="table-icon-size"
           (click)="showDeleteConfirm($event)"></i>
      </sqtm-core-toggle-icon>
    </ng-container>
  `,
  styleUrls: ['./delete-ev-binding-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteEvBindingCellComponent extends AbstractDeleteCellRenderer implements OnDestroy {

  componentData$: Observable<AdminTestAutomationServerViewState>;
  unsub$ = new Subject<void>();

  constructor(public grid: GridService,
              cdRef: ChangeDetectorRef,
              protected dialogService: DialogService,
              private testAutomationServerViewService: TestAutomationServerViewService) {
    super(grid, cdRef, dialogService);
    this.componentData$ = this.testAutomationServerViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  protected doDelete() {
    this.grid.beginAsyncOperation();
    this.testAutomationServerViewService.unbindEnvironmentVariables([this.row.data['id']]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
  }

 ngOnDestroy() {
   this.unsub$.next();
   this.unsub$.complete();
 }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.servers.test-automation-servers.environment-variables.dialog.title.unbind-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.servers.test-automation-servers.environment-variables.dialog.message.unbind-one';
  }

}

import {SqtmGenericEntityState, UsersGroup} from 'sqtm-core';
import {PermissionGroup} from '../../../project-workspace/project-view/state/admin-project-state';


export interface AdminUserState extends SqtmGenericEntityState {
  id: number;
  login: string;
  firstName: string;
  lastName: string;
  email: string;
  active: boolean;
  createdOn: string;
  createdBy: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  lastConnectedOn: string;
  usersGroupBinding: number;
  usersGroups: UsersGroup[];
  projectPermissions: ProjectPermission[];
  teams: AssociatedTeam[];
  canManageLocalPassword: boolean;
}
export interface ProjectPermission {
  projectId: number;
  projectName: string;
  permissionGroup: PermissionGroup;
}
export interface AssociatedTeam {
  partyId: number;
  name: string;
}




import {Component, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {AbstractDeleteCellRenderer, ConfirmDeleteLevel, DialogService, GridService, RestService} from 'sqtm-core';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-environment-variable',
  template: `
    <sqtm-core-delete-icon (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./delete-environment-variable.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteEnvironmentVariableComponent extends AbstractDeleteCellRenderer {

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private restService: RestService) {
    super(grid, cdr, dialogService);
  }


  protected doDelete(): any {
    this.grid.beginAsyncOperation();
    const evId = this.row.data.evId;
    this.restService.delete([`environment-variables/${evId}`]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.grid.refreshData());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.environment-variable.dialog.title.delete-one';
  }

  protected getMessageKey(): string {
    return this.row.data.bindingCount === 0 ? 'sqtm-core.administration-workspace.environment-variable.dialog.message.delete-one'
      : 'sqtm-core.administration-workspace.environment-variable.dialog.message.delete-bound-environment-variable';
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }

}

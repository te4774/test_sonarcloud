import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {AdminMilestoneViewComponentData} from '../../../containers/milestone-view/milestone-view.component';
import {
  ActionErrorDisplayService,
  AdminReferentialDataService,
  DialogService,
  DisplayOption,
  EditableSearchableSelectFieldComponent,
  EditableSelectLevelEnumFieldComponent,
  GridService,
  MilestoneRangeKeys,
  MilestoneStatusLevelEnumItem,
  UiManagerService,
  User,
  WorkspaceDirective
} from 'sqtm-core';
import {MilestoneViewService} from '../../../services/milestone-view.service';
import {catchError, filter, finalize, map, switchMap, take, takeUntil} from 'rxjs/operators';
import {Observable, of, Subject} from 'rxjs';

@Component({
  selector: 'sqtm-app-milestone-information-panel',
  templateUrl: './milestone-information-panel.component.html',
  styleUrls: ['./milestone-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MilestoneInformationPanelComponent implements OnInit, OnDestroy {

  @Input() componentData: AdminMilestoneViewComponentData;

  @ViewChild('rangeField') rangeField: EditableSelectLevelEnumFieldComponent;
  @ViewChild('ownerField') ownerField: EditableSearchableSelectFieldComponent;
  @ViewChild('statusField') statusField: EditableSelectLevelEnumFieldComponent;

  readonly loggedAsAdmin$: Observable<boolean>;
  readonly isMilestoneOwner$: Observable<boolean>;
  readonly canEditOwnerField$: Observable<boolean>;

  private unsub$ = new Subject<void>();

  ownerOptions: DisplayOption[];

  constructor(public readonly milestoneViewService: MilestoneViewService,
              public readonly cdRef: ChangeDetectorRef,
              public readonly adminReferentialService: AdminReferentialDataService,
              public readonly dialogService: DialogService,
              public readonly gridService: GridService,
              public readonly uiManager: UiManagerService,
              public readonly workspaceDirective: WorkspaceDirective,
              public readonly actionErrorDisplayService: ActionErrorDisplayService) {

    this.loggedAsAdmin$ = adminReferentialService.loggedAsAdmin$.pipe(takeUntil(this.unsub$));
    this.isMilestoneOwner$ = milestoneViewService.isMilestoneOwner$.pipe(takeUntil(this.unsub$));
    this.canEditOwnerField$ = milestoneViewService.canEditOwnerField$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit() {
    this.milestoneViewService.findPossibleOwners().pipe(
      map((users: User[]) => asDisplayOptions(users)),
    ).subscribe(items => {
      this.ownerOptions = items;
      this.cdRef.detectChanges();
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  handleOwnerChange(selectedUser: DisplayOption): void {
    const valueChanged = selectedUser.id !== this.componentData.milestone.ownerLogin;

    if (!valueChanged) {
      this.ownerField.value = this.componentData.milestone.ownerLogin;
      return;
    }

    this.openConfirmChangeOwnerDialogIfNeeded().subscribe((confirm) => {
      if (confirm) {
        this.ownerField.value = selectedUser.id.toString();
        this.milestoneViewService.setOwner(selectedUser.id.toString()).pipe(
          catchError((error) => this.handleChangeOwnerError(error)),
        ).subscribe();
      } else {
        this.ownerField.value = this.componentData.milestone.ownerLogin;
      }
    });
  }

  private handleChangeOwnerError(error): Observable<unknown> {
    if (error.status === 403) {
      this.redirectToMilestoneWorkspace();
    }
    return of(null);
  }

  private openConfirmChangeOwnerDialogIfNeeded(): Observable<any> {
    return this.loggedAsAdmin$.pipe(
      take(1),
      switchMap((loggedAsAdmin) => {
        if (loggedAsAdmin) {
          return of(true);
        } else {
          const dialogRef = this.dialogService.openConfirm({
            id: 'change-milestone-owner-warning',
            titleKey: 'sqtm-core.administration-workspace.milestones.dialog.title.change-owner-warning',
            messageKey: 'sqtm-core.administration-workspace.milestones.dialog.message.change-owner-warning',
          });

          return dialogRef.dialogClosed$;
        }
      }));
  }

  setRange(newRange: MilestoneRangeKeys): void {
    const valueChanged = newRange !== this.componentData.milestone.range;

    if (!valueChanged) {
      this.endRangeFieldAsync();
      return;
    }

    this.openConfirmChangeRangeDialogIfNeeded(newRange).subscribe((confirm) => {
      if (confirm) {
        this.doSetRange(newRange);
      } else {
        this.endRangeFieldAsync();
      }
    });
  }

  private doSetRange(newRange: MilestoneRangeKeys): void {
    this.milestoneViewService.setRange(newRange).pipe(
      finalize(() => this.endRangeFieldAsync())
    ).subscribe();
  }

  private openConfirmChangeRangeDialogIfNeeded(requestedRange: MilestoneRangeKeys): Observable<any> {
    if (requestedRange === 'GLOBAL') {
      return of(true);
    } else {
      return this.milestoneViewService.isMilestoneBoundToTemplate$.pipe(
        take(1),
        switchMap((boundToTemplate) => {
          if (boundToTemplate) {
            const dialogRef = this.dialogService.openConfirm({
              id: 'change-milestone-range-warning',
              titleKey: 'sqtm-core.administration-workspace.milestones.dialog.title.change-range-warning',
              messageKey: 'sqtm-core.administration-workspace.milestones.dialog.message.change-range-warning',
            });

            return dialogRef.dialogClosed$;
          } else {
            return of(true);
          }
        }));
    }
  }

  private endRangeFieldAsync() {
    if (this.rangeField) {
      this.rangeField.child.edit = false;
      this.rangeField.endAsync();
    }
  }

  private redirectToMilestoneWorkspace(): void {
    this.uiManager.closeContextualContent(this.workspaceDirective.workspaceName);
    this.gridService.refreshData();
  }

  handleStatusChange($event: MilestoneStatusLevelEnumItem<any>): void {
    const newStatusKey = $event.id;
    const valueChanged = newStatusKey !== this.componentData.milestone.status;

    if (!valueChanged) {
      this.statusField.child.value = newStatusKey;
      return;
    }

    if (newStatusKey === 'PLANNED') {
      const dialogRef = this.dialogService.openConfirm({
        titleKey: 'sqtm-core.administration-workspace.milestones.dialog.title.change-status-warning',
        messageKey: 'sqtm-core.administration-workspace.milestones.dialog.message.change-status-warning',
        id: 'confirm-change-milestone-status',
      });

      dialogRef.dialogClosed$.pipe(
        filter((confirmed) => {
          if (!confirmed) {
            this.statusField.child.value = this.componentData.milestone.status;
          }

          return confirmed;
        }),
        switchMap(() => this.milestoneViewService.setStatus(newStatusKey)),
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.endStatusFieldAsync()),
      ).subscribe();
    } else {
      this.milestoneViewService.setStatus(newStatusKey).pipe(
        catchError((err) => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.endStatusFieldAsync()),
      ).subscribe();
    }
  }

  private endStatusFieldAsync() {
      if (this.statusField) {
        this.statusField.child.edit = false;
        this.statusField.endAsync();
      }
    }

}

function asDisplayOptions(users: User[]): DisplayOption[] {
  return users.map((user) => {
    const label = getUserLabel(user);
    return {
      id: user.login,
      label
    };
  });
}

function getUserLabel(user: User) {
  if (Boolean(user.id)) {
    return Boolean(user.firstName) ? `${user.login} (${user.firstName} ${user.lastName})`
      : `${user.login} (${user.lastName})`;
  } else {
    return user.login;
  }
}

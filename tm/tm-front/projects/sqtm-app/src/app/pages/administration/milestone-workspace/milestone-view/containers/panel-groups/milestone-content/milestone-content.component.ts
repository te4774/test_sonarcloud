import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminMilestoneViewComponentData} from '../../milestone-view/milestone-view.component';
import {takeUntil} from 'rxjs/operators';
import {MilestoneViewService} from '../../../services/milestone-view.service';
import {GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {
  MILESTONE_PROJECTS_TABLE,
  MILESTONE_PROJECTS_TABLE_CONF,
  MilestoneProjectPanelComponent,
  milestoneProjectsTableDefinition
} from '../../../components/panels/milestone-project-panel/milestone-project-panel.component';

@Component({
  selector: 'sqtm-app-milestone-content',
  templateUrl: './milestone-content.component.html',
  styleUrls: ['./milestone-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: MILESTONE_PROJECTS_TABLE_CONF,
      useFactory: milestoneProjectsTableDefinition
    },
    {
      provide: MILESTONE_PROJECTS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, MILESTONE_PROJECTS_TABLE_CONF, ReferentialDataService]
    },
  ]
})
export class MilestoneContentComponent implements OnInit, OnDestroy {

  componentData$: Observable<AdminMilestoneViewComponentData>;
  unsub$ = new Subject<void>();

  @ViewChild(MilestoneProjectPanelComponent)
  private milestoneProjectPanelComponent;

  constructor(public milestoneViewService: MilestoneViewService,
              @Inject(MILESTONE_PROJECTS_TABLE) public projectsGridService: GridService) { }

  ngOnInit(): void {
    this.componentData$ = this.milestoneViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngOnDestroy(): void {
    this.projectsGridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  bindProjectsToMilestone($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.milestoneProjectPanelComponent.bindProjectsToMilestone();
  }

  unbindProjectsFromMilestone($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.milestoneProjectPanelComponent.unbindProjectsFromMilestone();
  }
}



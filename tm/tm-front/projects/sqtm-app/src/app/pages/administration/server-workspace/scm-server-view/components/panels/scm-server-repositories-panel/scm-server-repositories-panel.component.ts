import {ChangeDetectionStrategy, Component, InjectionToken, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminScmServerViewComponentData} from '../../../containers/scm-server-view/scm-server-view.component';
import {
  DataRow,
  deleteColumn,
  DialogService,
  Extendable,
  GridDefinition,
  GridService,
  indexColumn,
  RestService,
  smallGrid,
  Sort,
  StyleDefinitionBuilder,
  textColumn
} from 'sqtm-core';
import {getScmServerViewState, ScmServerViewService} from '../../../services/scm-server-view.service';
import {concatMap, filter, finalize, map, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {createSelector, select} from '@ngrx/store';
import {AdminScmServerState} from '../../../states/admin-scm-server-state';
import {
  DeleteScmRepositoryCellComponent
} from '../../cell-renderers/delete-scm-repository-cell/delete-scm-repository-cell.component';
import {
  editableWorkingBranchColumn
} from '../../cell-renderers/editable-working-branch-column/editable-working-branch-column.component';
import {
  AddScmRepositoryDialogComponent
} from '../../dialogs/add-scm-repository-dialog/add-scm-repository-dialog.component';

export const SCM_SERVER_REPOSITORIES_TABLE_CONF = new InjectionToken('SCM_SERVER_REPOSITORIES_TABLE_CONF');
export const SCM_SERVER_REPOSITORIES_TABLE = new InjectionToken('SCM_SERVER_REPOSITORIES_TABLE');
export function scmServerRepositoriesTableDefinition(): GridDefinition {
  return smallGrid('scm-server-repositories')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      textColumn('name')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      editableWorkingBranchColumn('workingBranch')
        .withI18nKey('sqtm-core.entity.scm-server.repositories.working-branch')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      textColumn('repositoryPath')
        .withI18nKey('sqtm-core.entity.scm-server.repositories.repository-path')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      textColumn('workingFolderPath')
        .withI18nKey('sqtm-core.entity.scm-server.repositories.working-directory')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      deleteColumn(DeleteScmRepositoryCellComponent)
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withInitialSortedColumns([{id: 'name', sort: Sort.ASC}])
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-scm-server-repositories-panel',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./scm-server-repositories-panel.component.less'],
  providers: [
    {
      provide: GridService,
      useExisting: SCM_SERVER_REPOSITORIES_TABLE
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScmServerRepositoriesPanelComponent implements OnInit, OnDestroy {

  private componentData$: Observable<AdminScmServerViewComponentData>;

  private unsub$ = new Subject<void>();

  constructor(private scmServerViewService: ScmServerViewService,
              public gridService: GridService,
              private dialogService: DialogService,
              private restService: RestService,
              private vcRef: ViewContainerRef) {
  }

  ngOnInit(): void {
    this.componentData$ = this.scmServerViewService.componentData$;
    this.initializeTable();
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  openAddRepositoryDialog(): void {
    const dialogRef = this.dialogService.openDialog({
      id: 'add-scm-repository-dialog',
      component: AddScmRepositoryDialogComponent,
      viewContainerReference: this.vcRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.servers.scm-servers.dialog.title.add-repository',
      },
      width: 720
    });

    dialogRef.dialogClosed$.subscribe((result) => {
      if (result) {
        this.refreshGrid();
      }
    });
  }

  refreshGrid(): void {
    this.gridService.refreshData();
  }

  deleteRepositories(): void {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.data.scmRepositoryId)),
      concatMap((scmRepositoryIds: number[]) => this.checkIfRepositoriesBoundToProject(scmRepositoryIds)),
      concatMap((result: BoundToProjectResult) => this.showConfirmDeleteRepositoryDialog(result.boundToProject, result.scmRepositoryIds)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      switchMap(({scmRepositoryIds}) => this.scmServerViewService.deleteRepositories(scmRepositoryIds)),
      finalize(() => this.gridService.completeAsyncOperation()),
    ).subscribe();
  }

  private checkIfRepositoriesBoundToProject(scmRepositoryIds): Observable<BoundToProjectResult> {
    return this.restService.get<boolean>(['scm-repositories', 'check-bound-to-project', scmRepositoryIds.join(',')]).pipe(
      take(1),
      map(boundToProject => ({boundToProject, scmRepositoryIds}))
    );
  }

  private showConfirmDeleteRepositoryDialog(boundToProject, scmRepositoryIds): Observable<ConfirmDeleteDialogResult> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.scm-servers.dialog.title.delete-many-repositories',
      messageKey: boundToProject ?
        'sqtm-core.administration-workspace.servers.scm-servers.dialog.message.delete-many-repositories-bound-to-project' :
        'sqtm-core.administration-workspace.servers.scm-servers.dialog.message.delete-many-repositories',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, scmRepositoryIds}))
    );
  }


  private initializeTable(): void {
    const repositoriesTable = this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createSelector(getScmServerViewState, (scmServerState: AdminScmServerState) => scmServerState.repositories)),
    );

    this.gridService.connectToDatasource(repositoriesTable, 'scmRepositoryId');
  }

}

interface BoundToProjectResult {
  boundToProject: boolean;
  scmRepositoryIds: number[];
}

interface ConfirmDeleteDialogResult {
  confirmDelete: boolean;
  scmRepositoryIds: number[];
}

import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {take} from 'rxjs/operators';
import {
  AlertUserHasSyncsConfiguration,
  AlertUserHasSyncsDialogComponent,
  DialogConfiguration,
  DialogService,
  RestService
} from 'sqtm-core';

@Injectable()
export class UserWorkspaceService {

  constructor(
    protected restService: RestService,
    private dialogService: DialogService,
  ) {
  }

  public getUserSyncsServerSide(partyId): Observable<any> {
    return this.restService.get([`users/${partyId}/synchronisations`]).pipe(
      take(1)
    );
  }

  public openHasSyncsAlert(synchronisations) {
    const configuration = this.buildUserHasSyncsAlertConfiguration(synchronisations);
    return this.dialogService.openDialog<AlertUserHasSyncsConfiguration, any>(configuration);
  }

  private buildUserHasSyncsAlertConfiguration(synchronisations): DialogConfiguration<AlertUserHasSyncsConfiguration> {
    return {
      id: 'alert',
      component: AlertUserHasSyncsDialogComponent,
      width: 600,
      data: {
        id: 'alert-user-has-synchro',
        titleKey: 'sqtm-core.generic.label.error',
        messageKey: ['sqtm-core.administration-workspace.users.dialog.message.has-syncs',
          'sqtm-core.administration-workspace.users.dialog.message.handle-syncs'],
        synchronisations: synchronisations,
      }
    };
  }
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, OnDestroy, Output} from '@angular/core';
import {
  ActionErrorDisplayService,
  AdminReferentialDataService,
  AuthenticatedUser,
  DialogService,
  GenericEntityViewComponentData,
  RestService,
  SquashPlatformNavigationService
} from 'sqtm-core';
import {Observable, Subject, switchMap} from 'rxjs';
import {catchError, concatMap, filter, map, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {UserViewService} from '../../services/user-view.service';
import {AdminUserState} from '../../states/admin-user-state';
import {UserWorkspaceService} from '../../../services/user-workspace.service';

@Component({
  selector: 'sqtm-app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserViewComponent implements OnDestroy {

  @Output()
  userDeleted = new EventEmitter<void>();

  public readonly componentData$: Observable<AdminUserViewComponentData>;
  public readonly canDeleteUser$: Observable<boolean>;
  public readonly isViewedUserSelf$: Observable<boolean>;
  private unsub$ = new Subject<void>();

  constructor(public userViewService: UserViewService,
              private cdRef: ChangeDetectorRef,
              private restService: RestService,
              private dialogService: DialogService,
              private adminReferentialDataService: AdminReferentialDataService,
              private actionErrorDisplayService: ActionErrorDisplayService,
              private platformNavigationService: SquashPlatformNavigationService,
              private userWorkspaceService: UserWorkspaceService
  ) {
    this.componentData$ = this.userViewService.componentData$;

    this.canDeleteUser$ = this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.userViewService.componentData$),
      map(([authenticatedUser, componentData]) => this.isAdminAndViewUserIsNotSelf(authenticatedUser, componentData.user))
    );

    this.isViewedUserSelf$ = this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.userViewService.componentData$),
      map(([authenticatedUser, componentData]) => this.isViewedUserSelf(authenticatedUser, componentData.user))
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  handleDelete() {
    this.componentData$.pipe(
      take(1),
      concatMap((componentData: AdminUserViewComponentData) => this.showConfirmDeleteUserDialog(componentData.user.id)),
      filter(({confirmDelete}) => confirmDelete),
      concatMap(({userId}) => {
        return this.userWorkspaceService.getUserSyncsServerSide(userId).pipe(
          map(synchronisations => [synchronisations, userId])
        );
      }),
      switchMap(([synchronisations, userId]) => {
          if (synchronisations.length > 0) {
            return this.showHasSyncsAlertDialog(synchronisations);
          } else {
            return this.deleteUserIfNoSynchro(userId);
          }
        }
      )
    ).subscribe();
  }

  private showHasSyncsAlertDialog(synchronisations): Observable<any> {
        const dialogReference = this.userWorkspaceService.openHasSyncsAlert(synchronisations);
        return dialogReference.dialogClosed$;
  }

  private deleteUserIfNoSynchro(userId): Observable<any> {
    return this.deleteUserServerSide(userId).pipe(
      catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      tap(() => this.userDeleted.emit())
    );
  }

  handleNameChanged(): void {
    this.platformNavigationService.navigate('/logout');
  }

  private showConfirmDeleteUserDialog(userId): Observable<{ confirmDelete: boolean, userId: string }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.users.dialog.title.delete-one',
      messageKey: 'sqtm-core.administration-workspace.users.dialog.message.delete-one',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, userId}))
    );
  }

  private deleteUserServerSide(userId): Observable<void> {
    return this.restService.delete(['users', userId]);
  }

  private isAdminAndViewUserIsNotSelf(user: AuthenticatedUser, viewedUser: AdminUserState): boolean {
    return user.admin && !this.isViewedUserSelf(user, viewedUser);
  }

  private isViewedUserSelf(user: AuthenticatedUser, viewedUser: AdminUserState): boolean {
    return user.userId === viewedUser.id;
  }
}

export interface AdminUserViewComponentData extends GenericEntityViewComponentData<AdminUserState, 'user'> {
}

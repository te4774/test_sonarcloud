import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef} from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  basicExternalLinkColumn,
  DataRow,
  DialogService,
  Extendable,
  FilterOperation,
  Fixed,
  grid,
  GridDefinition,
  GridService,
  indexColumn,
  RestService,
  selectableTextColumn,
  TestAutomationServerKindI18nEnum,
  textColumn,
  WorkspaceWithGridComponent
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {concatMap, filter, map, take, takeUntil, tap} from 'rxjs/operators';
import {
  deleteTestAutomationServerColumn
} from '../../components/cell-renderers/delete-test-automation-server/delete-test-automation-server.component';
import {
  TestAutomationServerCreationDialogComponent
} from '../../components/dialogs/test-automation-server-creation-dialog/test-automation-server-creation-dialog.component';
import {ActivatedRoute, Router} from "@angular/router";
import {AbstractAdministrationNavigation} from "../../../../components/abstract-administration-navigation";

export function adminTestAutomationServersTableDefinition(): GridDefinition {
  return grid('testAutomationServers')
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Fixed(60))
        .withViewport('leftViewport'),
      selectableTextColumn('name')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2)),
      textColumn('kind')
        .withEnumRenderer(TestAutomationServerKindI18nEnum, false, true)
        .withI18nKey('sqtm-core.entity.execution-server.kind.label')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2)),
      basicExternalLinkColumn('baseUrl')
        .withI18nKey('sqtm-core.entity.generic.url.label')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2)),
      deleteTestAutomationServerColumn('delete', '')
    ]).server().withServerUrl(['test-automation-servers'])
    .disableRightToolBar()
    .withRowHeight(35)
    .enableMultipleColumnsFiltering(['name'])
    .build();
}

@Component({
  selector: 'sqtm-app-test-automation-server-grid',
  templateUrl: './test-automation-server-grid.component.html',
  styleUrls: ['./test-automation-server-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestAutomationServerGridComponent extends AbstractAdministrationNavigation implements AfterViewInit, OnDestroy {

  authenticatedUser$: Observable<AuthenticatedUser>;

  unsub$ = new Subject<void>();

  protected readonly entityIdPositionInUrl = 3;

  constructor(public gridService: GridService,
              private restService: RestService,
              private dialogService: DialogService,
              private adminReferentialDataService: AdminReferentialDataService,
              private viewContainerRef: ViewContainerRef,
              private workspaceWithGrid: WorkspaceWithGridComponent,
              protected route: ActivatedRoute,
              protected router: Router) {
    super(route, router);
    this.workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngAfterViewInit() {
    this.addFilters();
    this.gridService.refreshData();
  }

  private addFilters() {
    this.gridService.addFilters([{
      id: 'name',
      active: false,
      initialValue: {kind: 'single-string-value', value: ''},
      tiedToPerimeter: false,
      operation: FilterOperation.LIKE
    }]);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openCreationDialog() {
    const dialogReference = this.dialogService.openDialog({
      component: TestAutomationServerCreationDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.title.new-test-automation-server',
      },
      id: 'test-automation-server-dialog',
      width: 600,
    });

    dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      filter(result => result != null),
      concatMap((result: any) => this.gridService.refreshDataAsync().pipe(map(() => result.id))),
      tap((id: string) => super.navigateToNewEntity(id))
    ).subscribe();
  }

  deleteTestAutomationServers() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      concatMap((rows: DataRow[]) => this.showConfirmDeleteTestAutomationServerDialog(rows)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({rows}) => this.deleteTestAutomationServersServerSide(rows)),
      tap(() => this.gridService.completeAsyncOperation())
    ).subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteTestAutomationServerDialog(rows): Observable<{ confirmDelete: boolean, rows: string[] }> {
    const serverHasAutomationProjectWithExecution = rows.filter(row => row.data['executionCount'] > 0).length > 0;

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.title.delete-many',
      messageKey: serverHasAutomationProjectWithExecution ?
        'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-many-with-bound-project'
        : 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-many-without-bound-project',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, rows}))
    );
  }

  private deleteTestAutomationServersServerSide(rows): Observable<void> {
    const pathVariable = rows.map(row => row.data['serverId']).join(',');
    return this.restService.delete([`test-automation-servers`, pathVariable]);
  }

  filterAutomatedTestServer($event: any) {
    this.gridService.applyMultiColumnsFilter($event);
  }
}

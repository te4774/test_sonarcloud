import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService, IconPickerFieldComponent} from 'sqtm-core';
import {getInfoListIcons, INFO_LIST_ICON_NAMESPACE} from '../../../../info-list-icons.constant';
import {InfoListOptionService} from '../../../services/info-list-option.service';

@Component({
  selector: 'sqtm-app-info-list-option-icon-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column" style="cursor: pointer">
        <sqtm-core-icon-picker-field
            class="m-auto"
            [icons]="infoListIcons"
            [icon]="iconName"
            (iconChanged)="handleIconChange($event)">
        </sqtm-core-icon-picker-field>
      </div>
    </ng-container>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoListOptionIconComponent extends AbstractCellRendererComponent {

  @ViewChild(IconPickerFieldComponent)
  iconField: IconPickerFieldComponent;

  infoListIcons: string[];

  get hasIcon(): boolean {
    const iconName = this.row.data[this.columnDisplay.id];
    return Boolean(iconName) && iconName.length > 0 && iconName !== 'noicon';
  }

  get iconName(): string {
    return this.hasIcon ? INFO_LIST_ICON_NAMESPACE + this.row.data[this.columnDisplay.id] : '';
  }

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              private infoListOptionService: InfoListOptionService) {
    super(grid, cdRef);
    this.infoListIcons = getInfoListIcons();
  }

  handleIconChange(newIcon: string): void {
    newIcon = newIcon.replace(INFO_LIST_ICON_NAMESPACE, '');
    this.infoListOptionService.changeItemIcon(this.row.data['label'], newIcon);
  }
}

export function infoListOptionIconColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(InfoListOptionIconComponent);
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  AuthenticatedUser,
  DialogService,
  GridService,
  ReferentialDataService
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {UserViewService} from '../../../services/user-view.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-remove-user-associated-team-cell',
  template: `
    <sqtm-core-delete-icon
      [iconName]="getIcon()"
      (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./remove-user-associated-team-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoveUserAssociatedTeamCellComponent extends AbstractDeleteCellRenderer implements OnDestroy {

  unsub$ = new Subject<void>();

  authenticatedUser$: Observable<AuthenticatedUser>;

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              dialogService: DialogService,
              private userViewService: UserViewService,
              private referentialDataService: ReferentialDataService) {
    super(grid, cdr, dialogService);
    this.authenticatedUser$ = referentialDataService.authenticatedUser$;
  }

  getIcon(): string {
    return 'sqtm-core-generic:unlink';
  }

  protected doDelete(): void {
    this.grid.beginAsyncOperation();
    const partyId = this.row.data.partyId;
    this.userViewService.removeTeamAssociation([partyId]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.users.dialog.title.remove-associated-team.remove-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.users.dialog.message.remove-associated-team.remove-one';
  }
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {DeleteUserCellRendererComponent} from './delete-user-cell-renderer.component';
import {
  DialogService,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
} from 'sqtm-core';

import {NO_ERRORS_SCHEMA} from '@angular/core';
import {of} from 'rxjs';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {UserWorkspaceService} from '../../../../services/user-workspace.service';

describe('DeleteUserCellRendererComponent', () => {
  let component: DeleteUserCellRendererComponent;
  let fixture: ComponentFixture<DeleteUserCellRendererComponent>;
  const gridConfig = grid('grid-test').build();
  const restService = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteUserCellRendererComponent],
      imports: [AppTestingUtilsModule, OverlayModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          userValue: {}
        }, {
          provide: UserWorkspaceService,
          userValue: {}
        }, {
          provide: ReferentialDataService,
          useValue: {
            authenticatedUser$: of({
              admin: true,
              name: 'admin',
              userId: 1
            })
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteUserCellRendererComponent);
    component = fixture.componentInstance;
    component.row = {
      allowMoves: false,
      allowedChildren: [],
      component: undefined,
      projectId: 0,
      simplePermissions: undefined,
      type: undefined,
      id: 0,
      data: {},
      selectable: true
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

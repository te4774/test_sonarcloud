import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  InjectionToken,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  BoundEnvironmentVariable,
  DialogService,
  Extendable,
  GridDefinition,
  GridService,
  ListItem,
  smallGrid,
  StyleDefinitionBuilder,
  Sort,
  indexColumn,
  deleteColumn,
  DataRow,
  ActionErrorDisplayService,
  EnvironmentVariable
} from 'sqtm-core';
import {concatMap, Observable, Subject} from 'rxjs';
import {
  AdminTestAutomationServerViewComponentData
} from '../../../containers/test-automation-server-view/test-automation-server-view.component';
import {catchError, filter, finalize, map, pluck, take, takeUntil, tap} from 'rxjs/operators';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';
import {Overlay} from '@angular/cdk/overlay';
import {
  DeleteEvBindingCellComponent
} from '../../cell-renderers/delete-ev-binding-cell/delete-ev-binding-cell.component';
import {environmentVariableValueColumn} from '../../cell-renderers/ev-field-value-cell/ev-field-value-cell.component';
import {clickableEnvironmentVariableNameColumn} from '../../../../../cell-renderer.builders';
import {
  EnvironmentVariableBindingDialogComponent, EnvironmentVariableBindingDialogConfig
} from '../../dialogs/environment-variable-binding-dialog/environment-variable-binding-dialog.component';

export const TEST_AUTOMATION_SERVER_EV_TABLE_CONF = new InjectionToken('TEST_AUTOMATION_SERVER_EV_TABLE_CONF');
export const TEST_AUTOMATION_SERVER_EV_TABLE = new InjectionToken('TEST_AUTOMATION_SERVER_EV_TABLE');


export function testAutomationServerEvTableDefinition(): GridDefinition {
  return smallGrid('test-automation-server-environment-variables')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      clickableEnvironmentVariableNameColumn('name')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5))
        .disableSort(),
      environmentVariableValueColumn('value')
        .withI18nKey('sqtm-core.generic.label.value')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5))
        .disableSort(),
      deleteColumn(DeleteEvBindingCellComponent)
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withInitialSortedColumns([{id: 'name', sort: Sort.ASC}])
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-tas-environment-variables-panel',
  templateUrl: './tas-environment-variables-panel.component.html',
  styleUrls: ['./tas-environment-variables-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: TEST_AUTOMATION_SERVER_EV_TABLE
    }
  ]
})
export class TestAutomationServerEnvironmentVariablesPanelComponent implements OnInit, OnDestroy {

  private componentData$: Observable<AdminTestAutomationServerViewComponentData>;
  private unsub$ = new Subject<void>();

  listItems: ListItem[] = [];

  @Output()
  valueChanged = new EventEmitter<ListItem[]>();

  @ViewChild('grid')
  grid: ElementRef;

  editMode: boolean;

  environmentVariablesDialogData: EnvironmentVariable[];

  constructor(private testAutomationServerViewService: TestAutomationServerViewService,
              private gridService: GridService,
              private dialogService: DialogService,
              private overlay: Overlay,
              private viewContainerRef: ViewContainerRef,
              public readonly cdRef: ChangeDetectorRef,
              private actionErrorDisplayService: ActionErrorDisplayService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.testAutomationServerViewService.componentData$.pipe(takeUntil(this.unsub$));
    this.initializeTable();
  }

  get selectedItems(): ListItem[] {
    return this.listItems.filter(item => item.selected);
  }

  set selectedItems(newSelection: ListItem[]) {
    this.listItems = this.listItems.map(item => ({
      ...item,
      selected: newSelection.map(i => i.id).includes(item.id),
    }));
    this.cdRef.detectChanges();
  }

  private initializeTable() {
    const environmentVariables$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      pluck('testAutomationServer', 'boundEnvironmentVariables')
    );

    this.gridService.connectToDatasource(environmentVariables$, 'name');
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openBindEnvironmentVariables(): void {
    this.testAutomationServerViewService.getAllEnvironmentVariables().pipe(
      take(1),
    ).subscribe((references) => {
      this.initializeListItem(references);
      this.displayOverlay(this.listItems);
    });
  }

  displayOverlay(listItems: ListItem[]): void {
    const dialogReference = this.dialogService.openDialog<EnvironmentVariableBindingDialogConfig, ListItem[]>(
      {
        component: EnvironmentVariableBindingDialogComponent,
        viewContainerReference: this.viewContainerRef,
        data: {
          titleKey: 'sqtm-core.administration-workspace.entities-customization.environment-variables.bind-many',
          environmentVariables: listItems,
          dialogData: this.environmentVariablesDialogData,
        },
        id: 'bind-environment-variable-dialog',
        width: 600,
        height: 500
      });
    dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      filter(result => result != null && result.length > 0)
    ).subscribe(items => this.valueChanged.emit(items));
  }

  private initializeListItem(references: EnvironmentVariable[]) {
    this.testAutomationServerViewService.componentData$.pipe(
      take(1),
      map(componentData => componentData.testAutomationServer.boundEnvironmentVariables),
      map((boundEnvironmentVariables: BoundEnvironmentVariable[]) =>
        boundEnvironmentVariables.map(variable => variable.id))
    ).subscribe(boundVariableIds => {
      this.environmentVariablesDialogData = references;
      this.listItems = references.filter(reference => !boundVariableIds.includes(reference.id))
        .map(reference => ({id: reference.id, label: reference.name, customTemplate: null, selected: false}));
    });
  }

  openUnbindEnvironmentVariableDialog(): void {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.data.id)),
      concatMap((ids: number[]) => this.showConfirmUnbindEnvironmentVariableDialog(ids)),
      filter(({confirmUnbind}) => confirmUnbind),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({environmentVariableIds}) => this.testAutomationServerViewService.unbindEnvironmentVariables(environmentVariableIds)),
      catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      finalize(() => this.gridService.completeAsyncOperation())
    ).subscribe();
  }

  private showConfirmUnbindEnvironmentVariableDialog(environmentVariableIds: number[]): Observable<{
    confirmUnbind: boolean,
    environmentVariableIds: number[]
  }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.test-automation-servers.environment-variables.dialog.title.unbind-many',
      messageKey: 'sqtm-core.administration-workspace.servers.test-automation-servers.environment-variables.dialog.message.unbind-many',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmUnbind => ({confirmUnbind, environmentVariableIds}))
    );
  }

}

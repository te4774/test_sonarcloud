import {SqtmGenericEntityState,} from 'sqtm-core';
import {ProjectPermission} from '../../user-view/states/admin-user-state';

export interface AdminTeamState extends SqtmGenericEntityState {
  id: number;
  name: string;
  description: string;
  createdOn: string;
  createdBy: string;
  lastModifiedBy: string;
  lastModifiedOn: string;
  projectPermissions: ProjectPermission[];
  members: Member[];
}

export interface Member {
  partyId: number;
  active: boolean;
  firstName: string;
  lastName: string;
  login: string;
  fullName: string;
}




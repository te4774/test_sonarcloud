import {ChangeDetectionStrategy, Component, InjectionToken, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {
  DataRow,
  deleteColumn,
  DialogService,
  Extendable,
  GridDefinition,
  GridFilter,
  GridService,
  indexColumn,
  smallGrid,
  Sort,
  StyleDefinitionBuilder
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {concatMap, filter, finalize, map, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {createSelector, select} from '@ngrx/store';
import {AdminUserState} from '../../../states/admin-user-state';
import {getUserViewState, UserViewService} from '../../../services/user-view.service';
import {AdminUserViewComponentData} from '../../../containers/user-view/user-view.component';
import {
  userAuthorisationProfileColumn
} from '../../cell-renderers/user-authorisation-profile-cell/user-authorisation-profile-cell.component';
import {
  AddUserAuthorisationDialogComponent
} from '../../dialogs/add-user-authorisation-dialog/add-user-authorisation-dialog.component';
import {
  RemoveUserAuthorisationCellComponent
} from '../../cell-renderers/remove-user-authorisation-cell/remove-user-authorisation-cell.component';
import {clickableProjectNameColumn} from '../../../../../cell-renderer.builders';

export const USER_AUTHORISATIONS_TABLE_CONF = new InjectionToken('USER_AUTHORISATIONS_TABLE_CONF');
export const USER_AUTHORISATIONS_TABLE = new InjectionToken('USER_AUTHORISATIONS_TABLE');

export function userAuthorisationsTableDefinition(): GridDefinition {
  return smallGrid('user-authorisations')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      clickableProjectNameColumn('projectName')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Extendable(200, 0.5)),
      userAuthorisationProfileColumn('permissionGroup')
        .changeWidthCalculationStrategy(new Extendable(100, 0.2)),
      deleteColumn(RemoveUserAuthorisationCellComponent),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .enableMultipleColumnsFiltering(['projectName'])
    .withInitialSortedColumns([{id: 'projectName', sort: Sort.ASC}])
    .build();
}

@Component({
  selector: 'sqtm-app-user-authorisations-panel',
  template: `
    <div class="m-t-10 m-l-10 m-b-15 flex-fixed-size" style="width: 200px">
      <sqtm-core-text-research-field (newResearchValue)="handleResearchInput($event)"></sqtm-core-text-research-field>
    </div>
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./user-authorisations-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: USER_AUTHORISATIONS_TABLE
    }
  ]
})
export class UserAuthorisationsPanelComponent implements OnInit, OnDestroy {

  private componentData$: Observable<AdminUserViewComponentData>;

  private unsub$ = new Subject<void>();

  constructor(private userViewService: UserViewService,
              public gridService: GridService,
              private dialogService: DialogService,
              private vcRef: ViewContainerRef) {
  }

  ngOnInit(): void {
    this.componentData$ = this.userViewService.componentData$;
    this.initializeTable();
    this.initializeFilters();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  refreshGrid(): void {
    this.gridService.refreshData();
  }

  openAddUserAuthorisationDialog(): void {
    const dialogRef = this.dialogService.openDialog({
      id: 'add-user-authorisation-dialog',
      component: AddUserAuthorisationDialogComponent,
      viewContainerReference: this.vcRef,
      width: 720,
      top: '100px'
    });

    dialogRef.dialogClosed$.subscribe((result) => {
      if (result) {
        this.refreshGrid();
      }
    });
  }

  removeAuthorisations(): void {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.data.projectId)),
      concatMap((projectIds: number[]) => this.showConfirmDeleteProjectAuthorisationDialog(projectIds)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      switchMap(({projectIds}) => this.userViewService.removeAuthorisation(projectIds)),
      finalize(() => this.gridService.completeAsyncOperation()),
    ).subscribe();
  }

  handleResearchInput($event: string): void {
    this.gridService.applyMultiColumnsFilter($event);
  }

  private showConfirmDeleteProjectAuthorisationDialog(projectIds): Observable<ConfirmDeleteDialogResult> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.users.dialog.title.remove-authorisation.remove-many',
      messageKey: 'sqtm-core.administration-workspace.users.dialog.message.remove-authorisation.remove-many',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, projectIds}))
    );
  }

  private initializeFilters() {
    const filters: GridFilter[] = [{
      id: 'projectName', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false
    }];
    this.gridService.addFilters(filters);
  }

  private initializeTable() {
    const authorizationTable = this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createSelector(getUserViewState, (userState: AdminUserState) => userState.projectPermissions)),
    );

    this.gridService.connectToDatasource(authorizationTable, 'projectId');
  }
}

interface ConfirmDeleteDialogResult {
  confirmDelete: boolean;
  projectIds: number[];
}

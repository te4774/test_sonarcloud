import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {GenericEntityViewService, GridService} from 'sqtm-core';
import {map, takeUntil} from 'rxjs/operators';
import {UserViewService} from '../../services/user-view.service';
import {Subject} from 'rxjs';
import {ActivatedRoute, ParamMap} from '@angular/router';

@Component({
  selector: 'sqtm-app-user-view-with-grid',
  templateUrl: './user-view-with-grid.component.html',
  styleUrls: ['./user-view-with-grid.component.less'],
  providers: [
    {
      provide: UserViewService,
      useClass: UserViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: UserViewService
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserViewWithGridComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  constructor(private readonly route: ActivatedRoute,
              public readonly gridService: GridService,
              private readonly userViewService: UserViewService) {
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('userId')),
      ).subscribe((id) => {
      this.userViewService.load(parseInt(id, 10));
    });

    this.prepareGridRefreshOnEntityChanges();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.userViewService.complete();
  }

  private prepareGridRefreshOnEntityChanges(): void {
    this.userViewService.simpleAttributeRequiringRefresh = [
      'login',
      'lastName',
      'firstName',
      'email',
      'projectPermissions',
      'teams',
      'usersGroupBinding'];

    this.userViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
    ).subscribe(() =>
      this.gridService.refreshDataAndKeepSelectedRows());
  }
}

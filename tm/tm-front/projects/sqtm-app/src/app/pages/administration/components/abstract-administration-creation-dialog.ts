import {HttpErrorResponse} from '@angular/common/http';
import {FormGroup} from '@angular/forms';
import {
  DatePickerComponent,
  DialogReference,
  extractSquashFirstFieldError,
  FieldValidationError,
  RestService,
  RichTextFieldComponent,
  SelectFieldComponent,
  TextFieldComponent
} from 'sqtm-core';
import {ChangeDetectorRef, Directive, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {switchMap, take} from 'rxjs/operators';
import {administrationLogger} from '../administration.logger';

const logger = administrationLogger.compose('AbstractAdministrationCreationDialogDirective');

@Directive()
export abstract class AbstractAdministrationCreationDialogDirective implements OnDestroy {

  abstract formGroup: FormGroup;

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  @ViewChildren(SelectFieldComponent)
  selectFields: QueryList<SelectFieldComponent>;

  @ViewChildren(DatePickerComponent)
  dateFields: QueryList<DatePickerComponent>;

  @ViewChildren(RichTextFieldComponent)
  richTextFields: QueryList<RichTextFieldComponent>;

  abstract serverSideValidationErrors: FieldValidationError[];

  // Internal use!
  readonly loading$ = new Subject<boolean>();

  protected constructor(protected readonly postUrl: string,
                        protected dialogReference: DialogReference,
                        protected restService: RestService,
                        protected cdr: ChangeDetectorRef) {
  }

  abstract get textFieldToFocus(): string;

  ngOnDestroy(): void {
    this.loading$.complete();
  }

  addEntity(addAnother?: boolean) {
    logger.debug('try to add entity...', [this.formGroup]);

    if (this.formIsValid()) {
      this.persist(addAnother);
    } else {
      logger.debug('client side errors when trying to add at ' + this.postUrl);
      this.showClientSideErrors();
    }
  }

  addAnother() {
    this.addEntity(true);
  }

  protected abstract getRequestPayload(): Observable<any>;

  private persist(addAnother?: boolean) {
    this.beginAsync();

    this.getRequestPayload().pipe(
      take(1),
      switchMap(payload => this.restService.post([this.postUrl], payload))
    ).subscribe(result => {
        this.handleCreationSuccess(result, addAnother);
      },
      error => {
        this.handleCreationFailure(error);
      }
    );
  }

  protected formIsValid() {
    return this.formGroup.valid;
  }

  protected handleCreationFailure(error: HttpErrorResponse) {
    this.endAsync();

    logger.debug('server side errors when trying to add at ' + this.postUrl, [error]);

    if (error.status === 412) {
      const squashError = extractSquashFirstFieldError(error);
      if (squashError.kind === 'FIELD_VALIDATION_ERROR') {
        this.serverSideValidationErrors = squashError.fieldValidationErrors;
        this.cdr.markForCheck();
      }
    }
  }

  protected showClientSideErrors() {
    this.textFields.filter(textField => this.formGroup.contains(textField.fieldName)).forEach(textField => textField.showClientSideError());
    this.selectFields.forEach(selectField => selectField.showClientSideError());
    this.dateFields.forEach(dateField => dateField.showClientSideError());
    this.richTextFields.forEach(dateField => dateField.showClientSideError());
  }

  protected handleCreationSuccess(result: any, addAnother?: boolean) {
    this.endAsync();
    this.dialogReference.result = result;

    if (addAnother) {
      this.resetForm();
    } else {
      this.dialogReference.close();
    }
  }

  private resetForm(): void {
    this.doResetForm();
    this.refreshTextFieldFocus();
    this.showClientSideErrors();
    this.serverSideValidationErrors = [];
    this.cdr.markForCheck();
  }

  protected refreshTextFieldFocus() {
    this.textFields.filter(textField => textField.fieldName === this.textFieldToFocus).forEach(textField => textField.grabFocus());
  }

  protected abstract doResetForm();

  protected getFormControl(fieldName: string) {
    return this.formGroup.controls[fieldName];
  }

  protected getFormControlValue(fieldName: string) {
    return this.getFormControl(fieldName).value;
  }

  protected resetFormControl(fieldName: string, value: any) {
    return this.getFormControl(fieldName).reset(value);
  }

  protected beginAsync(): void {
    this.loading$.next(true);
  }

  protected endAsync(): void {
    this.loading$.next(false);
  }
}

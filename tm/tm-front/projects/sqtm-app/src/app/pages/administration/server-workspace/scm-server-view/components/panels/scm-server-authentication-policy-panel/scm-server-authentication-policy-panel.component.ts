import {ChangeDetectionStrategy, Component, Input, ViewChild} from '@angular/core';
import {
  Credentials,
  isBasicAuthCredentials,
  isTokenAuthCredentials,
  ThirdPartyCredentialsFormComponent
} from 'sqtm-core';
import {ScmServerViewService} from '../../../services/scm-server-view.service';
import {AdminScmServerViewState} from '../../../states/admin-scm-server-view-state';

@Component({
  selector: 'sqtm-app-scm-server-authentication-policy-panel',
  templateUrl: './scm-server-authentication-policy-panel.component.html',
  styleUrls: ['./scm-server-authentication-policy-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScmServerAuthenticationPolicyPanelComponent {

  @Input()
  componentData: AdminScmServerViewState;

  @ViewChild(ThirdPartyCredentialsFormComponent)
  credentialsForm: ThirdPartyCredentialsFormComponent;
  credentialsStatusMessage = '';
  statusIcon: 'INFO' = null;

  constructor(public readonly scmServerViewService: ScmServerViewService) {
    this.observeAuthProtocolChange();
  }

  private observeAuthProtocolChange(): void {
    this.scmServerViewService.externalRefreshRequired$.subscribe(update => {
      if (update.key === 'authProtocol') {
        this.credentialsForm.authenticationProtocol = update.value;
        this.clearMessage();
      }
    });
  }

  sendCredentialsForm(credentials: Credentials) {
    this.clearMessage();
    if (isBasicAuthCredentials(credentials)) {
      this.scmServerViewService.setBasicAuthCredentials(credentials.username, credentials.password)
        .subscribe(() => this.handleCredentialsSaveSuccess());
    } else if (isTokenAuthCredentials(credentials)) {
      this.scmServerViewService.setTokenAuthCredentials(credentials.token)
        .subscribe(() => this.handleCredentialsSaveSuccess());
    } else {
      throw new Error('Only basic auth and token auth are handled for SCM servers.');
    }
  }

  private clearMessage(): void {
    this.credentialsStatusMessage = null;
    this.statusIcon = null;
  }

  private handleCredentialsSaveSuccess(): void {
    this.credentialsForm.formGroup.markAsPristine();
    this.credentialsForm.endAsync();

    this.credentialsStatusMessage = 'sqtm-core.entity.scm-server.credentials.save-success';
    this.statusIcon = 'INFO';
  }
}

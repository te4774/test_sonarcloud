import {TestBed} from '@angular/core/testing';

import {ProjectViewService, UpdatedAutomationJob} from './project-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {
  BddImplementationTechnology,
  BddScriptLanguage,
  BindableEntity,
  Bindings,
  CustomField,
  CustomFieldBinding,
  Milestone,
  MilestoneAdminProjectView,
  PartyProjectPermission,
  PermissionGroup,
  ProjectView,
  RestService,
  WorkspaceTypeForPlugins
} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {of} from 'rxjs';
import {take} from 'rxjs/operators';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {mockRestService} from '../../../../../utils/testing-utils/mocks.service';

describe('ProjectViewService', () => {

  const restService = mockRestService();

  let service: ProjectViewService;

  beforeEach(async (done) => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: ProjectViewService,
          useClass: ProjectViewService
        }
      ]
    });

    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
    service = TestBed.get(ProjectViewService);
    service.load(1);
    done();
  });

  describe('ProjectViewService - initialization', () => {
    it('should load project', async (done) => {
      service.componentData$.subscribe(data => {
        expect(data.project.id).toEqual(1);
        done();
      });
    });

    it('should initialize bound custom fields', async (done) => {
      service.boundCustomFields$
        .pipe(take(1))
        .subscribe((bindings) => {
          expect(bindings.length).toBe(2);
          done();
        });
    });
  });

  describe('ProjectViewService - template operations', () => {
    it('should coerce project into template', async (done) => {
      // when:
      service.coerceProjectIntoTemplate();

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.template).toBe(true);
          expect(componentData.project.linkedTemplate).toBe(null);
          expect(componentData.project.linkedTemplateId).toBe(null);
          done();
        });
    });

    it('should associate a project to a template', async (done) => {
      restService.post.and.returnValue(of({
        templateName: 'template'
      }));

      // when:
      service.associateWithTemplate(2, []);

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.linkedTemplate).toBe('template');
          expect(componentData.project.linkedTemplateId).toBe(2);
          done();
        });
    });

    it('should disassociate project from a template', async (done) => {
      restService.post.and.returnValue(of({templateName: 'template'}));
      service.associateWithTemplate(2, []);
      restService.post.and.returnValue(of({templateName: null}));

      // when:
      service.disassociateFromTemplate();

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.linkedTemplate).toBe(null);
          expect(componentData.project.linkedTemplateId).toBe(null);
          done();
        });
    });
  });

  describe('ProjectViewService - SCM repository settings', () => {
    it('should bind project to scm repository', async (done) => {
      // when:
      service.bindToScmRepository(1);

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.scmRepositoryId).toBe(1);
          done();
        });
    });

    it('should unbind project from scm repository', async (done) => {
      service.bindToScmRepository(1);

      // when:
      service.unbindFromScmRepository();

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.scmRepositoryId).toBe(null);
          done();
        });
    });
  });

  describe('Project View Service - BDD settings', () => {
    it('should change implementation technology', async (done) => {
      service.updateBddImplementationTechnology(1, BddImplementationTechnology.CUCUMBER_4.id).subscribe();

      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.bddImplementationTechnology).toBe(BddImplementationTechnology.CUCUMBER_4.id);
          done();
        });
    });

    it('should update implem technology with no scripts languages', async(done) => {
      service.updateBddImplementationTechnology(1, BddImplementationTechnology.ROBOT.id).subscribe();

      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.bddImplementationTechnology).toBe(BddImplementationTechnology.ROBOT.id);
          expect(componentData.project.bddScriptLanguage).toBe(BddScriptLanguage.ENGLISH.id);
          done();
        });
    });
  });

  describe('ProjectViewService - BugTracker settings', () => {
    it('should confirm bugtracker change', async (done) => {
      const bugTrackerResponse = {projectNames: ['job1', 'job2']};
      restService.get.and.returnValue(of(bugTrackerResponse));

      // when:
      service.confirmBugtracker(1);

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.bugtrackerProjectNames).toEqual(bugTrackerResponse.projectNames);
          expect(componentData.project.bugTrackerBinding).toBeTruthy();
          done();
        });
    });

    it('should remove bugtracker', async (done) => {
      restService.get.and.returnValue(of([]));

      // when:
      service.confirmBugtracker(null);

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.bugtrackerProjectNames).toBeFalsy();
          expect(componentData.project.bugTrackerBinding).toBeFalsy();
          done();
        });
    });
  });

  describe('ProjectViewService - Execution settings', () => {
    it('should change allow TC modif during execution', async (done) => {
      restService.get.and.returnValue(of([]));

      // when:
      service.changeAllowTcModifDuringExec(true);

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.allowTcModifDuringExec).toBeTruthy();
          done();
        });
    });

    it('should change execution status on project', async (done) => {
      restService.get.and.returnValue(of([]));

      // when:
      service.changeExecutionStatusOnProject(true, 'SETTLED');

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.allowedStatuses.SETTLED).toBeTruthy();
          done();
        });
    });

    it('should disable and replace execution status on project', async (done) => {
      restService.get.and.returnValue(of([]));

      // when:
      service.disableAndReplaceStatusWithinProject('UNTESTABLE', 'new');

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.allowedStatuses.SETTLED).toBeFalsy();
          done();
        });
    });
  });

  describe('ProjectViewService - Test automation server', () => {
    it('should bind to test automation server', async (done) => {
      restService.get.and.returnValue(of([]));

      // when:
      service.bindToTestAutomationServer(1);

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.taServerId).toEqual(1);
          expect(componentData.project.boundTestAutomationProjects).toEqual([]);
          done();
        });
    });

    it('should add test automation projects', async (done) => {
      const taProjects = [{
        canRunBdd: false,
        label: 'job1',
        remoteName: 'remote1'
      }, {
        canRunBdd: false,
        label: 'job2',
        remoteName: 'remote2'
      }];
      restService.post.and.returnValue(of({taProjects}));

      // when:
      service.addTestAutomationProjects([{
          canRunBdd: false,
          label: 'job2',
          remoteName: 'remote2'
        }]
      ).subscribe();

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.boundTestAutomationProjects
            .map(job => job.label)).toEqual(['job1', 'job2']);
          done();
        });
    });

    it('should update a test automation project', async (done) => {
      const taProjects = [{
        canRunBdd: false,
        label: 'job1',
        remoteName: 'remote1'
      }, {
        canRunBdd: false,
        label: 'job2',
        remoteName: 'remote2'
      }];
      restService.post.and.returnValue(of({taProjects}));

      // when:
      service.updateTestAutomationProject({
        taProjectId: 1,
      } as UpdatedAutomationJob).subscribe();
      // The updated data really is the data coming from server, no matter what request is sent...

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.boundTestAutomationProjects.length).toBe(2);
          done();
        });
    });

    it('should remove a test automation project', async (done) => {
      const taProjects = [{
        taProjectId: 1,
        canRunBdd: false,
        label: 'job1',
        remoteName: 'remote1'
      }, {
        taProjectId: 2,
        canRunBdd: false,
        label: 'job2',
        remoteName: 'remote2'
      }];
      restService.post.and.returnValue(of({taProjects}));
      service.addTestAutomationProjects([]).subscribe();

      restService.delete.and.returnValue(of(null));

      // when:
      service.removeTestAutomationProjects([1]).subscribe();

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.boundTestAutomationProjects.length).toBe(1);
          done();
        });
    });

    it('should change a job label', async (done) => {
      const taProjects = [{
        taProjectId: 1,
        canRunBdd: false,
        label: 'job1',
        remoteName: 'remote1'
      }, {
        taProjectId: 2,
        canRunBdd: false,
        label: 'job2',
        remoteName: 'remote2'
      }];
      restService.post.and.returnValue(of({taProjects}));
      service.addTestAutomationProjects([]).subscribe();

      restService.post.and.returnValue(of({
        taProjects: [{
          taServerId: 1,
          label: 'new',
        }]
      }));

      // when:
      service.setJobLabel(1, 'new').subscribe();

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.boundTestAutomationProjects[0].label).toBe('new');
          done();
        });
    });

    it('should set a job that can run BDD', async (done) => {
      const taProjects = [{
        taProjectId: 1,
        canRunBdd: false,
        label: 'job1',
        remoteName: 'remote1'
      }, {
        taProjectId: 2,
        canRunBdd: true,
        label: 'job2',
        remoteName: 'remote2'
      }];
      restService.post.and.returnValue(of({taProjects}));
      service.addTestAutomationProjects([]).subscribe();

      // when:
      service.setJobCanRunBdd(1, true);

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.boundTestAutomationProjects[1].canRunBdd).toBeTruthy();
          done();
        });
    });
  });

  describe('ProjectViewService - Custom field bindings', () => {
    it('should bind custom fields', async (done) => {
      const cufBinding: CustomFieldBinding = {
        id: 1,
        customFieldId: 777,
        bindableEntity: BindableEntity.EXECUTION_STEP,
        renderingLocations: [],
        boundProjectId: 1,
        position: 1,
      };

      const cuf: CustomField = {
        id: 777,
      } as CustomField;

      restService.post.and.returnValue(of({
        ...emptyCustomFieldBindings(),
        CAMPAIGN: [cufBinding]
      }));

      // when:
      service.bindCustomFields(BindableEntity.CAMPAIGN, [cuf]);

      // then:
      service.boundCustomFields$
        .pipe(take(1))
        .subscribe((boundCustomField) => {
          expect(boundCustomField.length).toBe(1);
          expect(boundCustomField[0].customFieldId).toBe(777);
          done();
        });
    });

    it('should unbind custom fields', async (done) => {
      // when:
      service.unbindCustomFields([1, 2]).subscribe(() => {

        // then:
        service.boundCustomFields$
          .pipe(take(1))
          .subscribe((boundCustomField) => {
            expect(boundCustomField.length).toBe(0);
            done();
          });
      });
    });
  });

  describe('ProjectViewService - Info list bindings', () => {
    it('should bind to info lists', async (done) => {
      // when:
      service.bindInfoListToProject(1, 'category');
      service.bindInfoListToProject(2, 'type');
      service.bindInfoListToProject(3, 'nature');

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.requirementCategoryId).toBe(1);
          expect(componentData.project.testCaseTypeId).toBe(2);
          expect(componentData.project.testCaseNatureId).toBe(3);
          done();
        });
    });
  });

  describe('ProjectViewService - Party project permissions', () => {
    it('should set party project permission groups', async (done) => {
      const group: PermissionGroup = {
        id: 1, qualifiedName: 'group', simpleName: 'group'
      };

      const partyProjectPermissions: PartyProjectPermission[] = [
        {partyId: 1, permissionGroup: group} as PartyProjectPermission,
        {partyId: 2, permissionGroup: group} as PartyProjectPermission,
      ];

      restService.post.and.returnValue(of({partyProjectPermissions}));

      // when:
      service.setPartyProjectPermissions([1, 2], group.simpleName).subscribe();

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.partyProjectPermissions).toEqual(partyProjectPermissions);
          done();
        });
    });

    it('should remove party project permission groups', async (done) => {
      const group: PermissionGroup = {
        id: 1, qualifiedName: 'group', simpleName: 'group'
      };

      const partyProjectPermissions: PartyProjectPermission[] = [
        {partyId: 1, permissionGroup: group} as PartyProjectPermission,
        {partyId: 2, permissionGroup: group} as PartyProjectPermission,
      ];

      restService.post.and.returnValue(of({partyProjectPermissions}));
      service.setPartyProjectPermissions([1, 2], group.simpleName).subscribe();

      // when:
      service.removePartyProjectPermissions([2]).subscribe();

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.partyProjectPermissions).toEqual([partyProjectPermissions[0]]);
          done();
        });
    });
  });


  describe('ProjectViewService - Milestones binding', () => {
    it('should bind to milestones', async (done) => {
      const milestones: Milestone[] = [
        {id: 1, description: 'milestone1'} as Milestone,
        {id: 2, description: 'milestone2'} as Milestone,
      ];

      // when:
      service.bindMilestonesToProject(milestones);

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.boundMilestonesInformation).toEqual([
            {milestone: milestones[0], milestoneBoundToOneObjectOfProject: false},
            {milestone: milestones[1], milestoneBoundToOneObjectOfProject: false},
          ]);
          done();
        });
    });

    it('should update state after creating new milestone', async (done) => {
      const milestone: MilestoneAdminProjectView = {
        milestoneBoundToOneObjectOfProject: false,
        milestone: {id: 1} as Milestone,
      };

      // when:
      service.updateBoundMilestoneInformation(milestone);

      // then:
      service.componentData$
        .pipe(take(1))
        .subscribe((componentData) => {
          expect(componentData.project.boundMilestonesInformation).toEqual([milestone]);
          done();
        });
    });

    it('should bind to milestones', async (done) => {
      const milestones: Milestone[] = [
        {id: 1, description: 'milestone1'} as Milestone,
        {id: 2, description: 'milestone2'} as Milestone,
      ];

      service.bindMilestonesToProject(milestones);

      // when:
      service.unbindMilestones([1]).subscribe(() => {
        // then:
        service.componentData$
          .pipe(take(1))
          .subscribe((componentData) => {
            expect(componentData.project.boundMilestonesInformation).toEqual([
              {milestone: milestones[1], milestoneBoundToOneObjectOfProject: false},
            ]);
            done();
          });
      });
    });
  });
});

function getInitialModel(): ProjectView {
  return {
    allowAutomationWorkflow: false,
    automationWorkflowType: '',
    availableBugtrackers: null,
    bugTrackerBinding: undefined,
    bugtrackerProjectNames: null,
    customFieldBindings: {
      ...emptyCustomFieldBindings(),
      CAMPAIGN: [
        {
          id: 1,
          customFieldId: 12,
          bindableEntity: BindableEntity.CAMPAIGN,
          renderingLocations: [],
          boundProjectId: 1,
          position: 0,
        }
      ],
      REQUIREMENT_VERSION: [
        {
          id: 2,
          customFieldId: 12,
          bindableEntity: BindableEntity.REQUIREMENT_VERSION,
          renderingLocations: [],
          boundProjectId: 1,
          position: 0,
        }
      ],
    },
    disabledExecutionStatus: [],
    label: '',
    milestoneBindings: [],
    permissions: undefined,
    requirementCategoryId: 0,
    taServerId: 0,
    testCaseNatureId: 0,
    testCaseTypeId: 0,
    id: 1,
    name: 'project-1',
    uri: '',
    attachmentList: {id: 1, attachments: []},
    hasData: false,
    description: '',
    lastModifiedOn: null,
    lastModifiedBy: null,
    createdOn: null,
    createdBy: null,
    linkedTemplate: null,
    linkedTemplateId: null,
    allowedStatuses: {
      SETTLED: false,
      UNTESTABLE: true,
    },
    statusesInUse: null,
    allowTcModifDuringExec: false,
    useTreeStructureInScmRepo: false,
    infoLists: null,
    template: true,
    templateLinkedToProjects: false,
    partyProjectPermissions: [],
    availableScmServers: [],
    availableTestAutomationServers: [],
    boundTestAutomationProjects: [],
    boundMilestonesInformation: [],
    scmRepositoryId: null,
    availablePlugins: [],
    keywords: [],
    bddScriptLanguage: 'ENGLISH',
    bddImplementationTechnology: 'CUCUMBER_4',
    automatedSuitesLifetime: null,
    activatedPlugins: {
      [WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE]: [],
      [WorkspaceTypeForPlugins.TEST_CASE_WORKSPACE]: [],
      [WorkspaceTypeForPlugins.REQUIREMENT_WORKSPACE]: [],
    },
    hasTemplateConfigurablePluginBinding: false
  };
}

function emptyCustomFieldBindings(): Bindings {
  const bindings: any = {};
  Object.keys(BindableEntity).forEach((key) => bindings[key] = []);
  return bindings as Bindings;
}

import {TestBed} from '@angular/core/testing';
import {DropdownListOptionService} from './dropdown-list-option.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {take} from 'rxjs/operators';

describe('DropdownListOptionService', () => {
  let service: DropdownListOptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
    });
    service = TestBed.inject(DropdownListOptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load initial state', async (done) => {
    service.initialize();
    service.dropdownListOptions$.pipe(take(1)).subscribe((options) => {
      expect(options).toEqual([]);
      done();
    });
  });

  it('should add options', async (done) => {
    service.initialize();
    service.addOption('option1', 'code1');
    service.addOption('option2', 'code2');
    service.dropdownListOptions$.pipe(take(1)).subscribe((options) => {
      expect(options.length).toEqual(2);
      done();
    });
  });

  it('should remove an option', async (done) => {
    service.initialize();
    service.addOption('option1', 'code1');
    service.addOption('option2', 'code2');
    service.removeOption('option1').subscribe();

    service.dropdownListOptions$.pipe(take(1)).subscribe((options) => {
      expect(options.length).toEqual(1);
      done();
    });
  });

  it('should set the default option', async (done) => {
    service.initialize();
    service.addOption('option1', 'code1');
    service.addOption('option2', 'code2');
    service.addOption('option3', 'code3');
    service.toggleDefault('option2');
    service.toggleDefault('option3');

    service.dropdownListOptions$.pipe(take(1)).subscribe((options) => {
      expect(options.find(opt => opt.optionName === 'option2').default).toBeFalsy();
      expect(options.find(opt => opt.optionName === 'option3').default).toBeTruthy();
      done();
    });
  });

  it('should change option color', async (done) => {
    service.initialize();
    service.addOption('option1', 'code1');

    const testColor = '#ff00ff';
    service.changeColor('option1', testColor);

    service.dropdownListOptions$.pipe(take(1)).subscribe((options) => {
      expect(options[0].color).toBe(testColor);
      done();
    });
  });
});

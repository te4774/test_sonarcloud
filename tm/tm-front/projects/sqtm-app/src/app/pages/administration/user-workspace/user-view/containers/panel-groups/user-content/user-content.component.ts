import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminUserViewComponentData} from '../../user-view/user-view.component';
import {UserViewService} from '../../../services/user-view.service';
import {takeUntil} from 'rxjs/operators';
import {
  USER_AUTHORISATIONS_TABLE,
  USER_AUTHORISATIONS_TABLE_CONF,
  UserAuthorisationsPanelComponent,
  userAuthorisationsTableDefinition
} from '../../../components/panels/user-authorisations-panel/user-authorisations-panel.component';
import {
  AdminReferentialDataService,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {
  USER_TEAMS_TABLE,
  USER_TEAMS_TABLE_CONF,
  UserTeamsPanelComponent,
  userTeamsTableDefinition
} from '../../../components/panels/user-teams-panel/user-teams-panel.component';
import {UserLicenseInformationDialogService} from '../../../../../services/user-license-information-dialog.service';

@Component({
  selector: 'sqtm-app-user-content',
  templateUrl: './user-content.component.html',
  styleUrls: ['./user-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: USER_AUTHORISATIONS_TABLE_CONF,
      useFactory: userAuthorisationsTableDefinition
    },
    {
      provide: USER_AUTHORISATIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, USER_AUTHORISATIONS_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: USER_TEAMS_TABLE_CONF,
      useFactory: userTeamsTableDefinition
    },
    {
      provide: USER_TEAMS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, USER_TEAMS_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: UserLicenseInformationDialogService,
      useClass: UserLicenseInformationDialogService,
    }
  ]
})
export class UserContentComponent implements OnInit, OnDestroy {

  componentData$: Observable<AdminUserViewComponentData>;

  unsub$ = new Subject<void>();

  @ViewChild(UserAuthorisationsPanelComponent)
  private userAuthorisationsPanelComponent;

  @ViewChild(UserTeamsPanelComponent)
  private userTeamsPanelComponent;

  constructor(public readonly userViewService: UserViewService,
              @Inject(USER_AUTHORISATIONS_TABLE) public readonly authorisationsGridService: GridService,
              @Inject(USER_TEAMS_TABLE) public readonly teamsGridService: GridService,
              public readonly adminReferentialDataService: AdminReferentialDataService,
              private readonly userLicenseInformationDialogService: UserLicenseInformationDialogService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.userViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
  }

  addUserAuthorisation($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    if (!this.userLicenseInformationDialogService.isUserCreationAllowed()) {
      this.userLicenseInformationDialogService.openLicenseDialog();
    } else {
      this.userAuthorisationsPanelComponent.openAddUserAuthorisationDialog();
    }
  }

  deletePermissions($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    this.userAuthorisationsPanelComponent.removeAuthorisations();
  }

  associateTeam($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.userTeamsPanelComponent.openAssociateTeamToUserDialog();
  }

  removeAssociatedTeams($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    this.userTeamsPanelComponent.removeTeamAssociations();
  }

  ngOnDestroy(): void {
    this.authorisationsGridService.complete();
    this.teamsGridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}

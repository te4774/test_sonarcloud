import {Injectable} from '@angular/core';
import {
  AttachmentService,
  AuthenticatedUser,
  CustomField,
  CustomFieldOption,
  EntityViewAttachmentHelperService,
  GenericEntityViewService,
  RestService
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {AdminCustomFieldState} from '../states/admin-custom-field-state';
import {AdminCustomFieldViewState, provideInitialAdminCustomFieldView} from '../states/admin-custom-field-view-state';
import {createFeatureSelector} from '@ngrx/store';
import {map, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable()
export class CustomFieldViewService extends GenericEntityViewService<AdminCustomFieldState, 'customField'> {
  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService
  ) {
    super(
      restService,
      attachmentService,
      translateService,
      attachmentHelper
    );
  }

  public getInitialState(): AdminCustomFieldViewState {
    return provideInitialAdminCustomFieldView();
  }

  protected getRootUrl(initialState?): string {
    return 'custom-fields';
  }

  load(cufId: number) {
    return this.restService.getWithoutErrorHandling<CustomField>(['custom-field-view', cufId.toString()])
      .subscribe(
        customField => this.initializeCustomField(customField),
        err => this.notifyEntityNotFound(err));
  }

  private initializeCustomField(customField: CustomField): void {
    const cufState: AdminCustomFieldState = {
      ...customField,
      attachmentList: {id: null, attachments: null},
    };
    this.initializeEntityState(cufState);
  }

  changeDefaultValue(defaultValue: string): Observable<void> {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminCustomFieldViewState) => this.changeDefaultValueServerSide(state, defaultValue)),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.updateStateWithNewDefaultValue(state, defaultValue)),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  private changeDefaultValueServerSide(state: AdminCustomFieldViewState, defaultValue: string) {
    const requestBody = {defaultValue: defaultValue};
    const urlParts = [
      'custom-fields',
      state.customField.id.toString(),
      'default-value'
    ];
    return this.restService.post(urlParts, requestBody);
  }

  private updateStateWithNewDefaultValue(state: AdminCustomFieldViewState,
                                         defaultValue: string): AdminCustomFieldViewState {
    return {
      ...state,
      customField: {
        ...state.customField,
        defaultValue: defaultValue
      }
    };
  }

  toggleOptional(isOptional: boolean) {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminCustomFieldViewState) => this.toggleOptionalServerSide(state, isOptional)),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.updateStateWithOptionalValue(state, isOptional)),
      tap((nextState) => this.store.commit(nextState)),
      tap((state) => this.requireExternalUpdate(state.customField.id, 'optional')),
    );
  }

  private toggleOptionalServerSide(state: AdminCustomFieldViewState, isOptional: boolean): Observable<CustomField> {
    const requestBody = {optional: isOptional};
    const urlParts = [
      'custom-fields',
      state.customField.id.toString(),
      'optional'
    ];

    return this.restService.post(urlParts, requestBody);
  }

  private updateStateWithOptionalValue(state: AdminCustomFieldViewState, isOptional: boolean): AdminCustomFieldViewState {
    return {
      ...state,
      customField: {
        ...state.customField,
        optional: isOptional
      }
    };
  }

  addOption(option: OptionToAdd): Observable<void> {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminCustomFieldViewState) => this.addOptionServerSide(state, option)),
      withLatestFrom(this.store.state$),
      map(([response, state]) => this.updateStateWithNewOption(state, response)),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  private addOptionServerSide(state: AdminCustomFieldViewState, option: OptionToAdd): Observable<CustomField> {

    const urlParts = [
      'custom-fields',
      state.customField.id.toString(),
      'options',
      'new'
    ];
    return this.restService.post(urlParts, option);
  }

  private updateStateWithNewOption(state: AdminCustomFieldViewState, response: CustomField): AdminCustomFieldViewState {
    return {
      ...state,
      customField: {
        ...state.customField,
        options: response.options
      }
    };
  }

  deleteOptions(optionLabels: string[]): Observable<void> {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminCustomFieldViewState) => this.deleteOptionsServerSide(state, optionLabels)),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.updateStateWithRemovedOptions(state, optionLabels)),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  private deleteOptionsServerSide(state: AdminCustomFieldViewState, optionLabels: string[]): Observable<void> {

    const urlParts = [
      'custom-fields',
      state.customField.id.toString(),
      'options',
      'remove'
    ];
    return this.restService.post(urlParts, {optionLabels: optionLabels});
  }

  private updateStateWithRemovedOptions(state: AdminCustomFieldViewState,
                                        optionLabels: string[]): AdminCustomFieldViewState {
    const updatedOptions = [...state.customField.options].filter((option: CustomFieldOption) => !optionLabels.includes(option.label));

    return {
      ...state,
      customField: {
        ...state.customField,
        options: updatedOptions
      }
    };
  }

  changeOptionLabel(currentLabel: string, newLabel: string): Observable<void> {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminCustomFieldViewState) => this.changeOptionLabelServerSide(state, currentLabel, newLabel)),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.updateStateWithNewOptionName(state, currentLabel, newLabel)),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  private changeOptionLabelServerSide(state: AdminCustomFieldViewState, currentLabel: string, newLabel: string): Observable<void> {
    const requestBody = {currentLabel: currentLabel, newLabel: newLabel};
    const urlParts = [
      'custom-fields',
      state.customField.id.toString(),
      'options',
      'label'
    ];
    return this.restService.post(urlParts, requestBody);
  }

  private updateStateWithNewOptionName(state: AdminCustomFieldViewState,
                                       currentLabel: string,
                                       newLabel: string): AdminCustomFieldViewState {
    const updatedOptions = [...state.customField.options];
    updatedOptions.map((option: CustomFieldOption) => {
      if (option.label === currentLabel) {
        return option.label = newLabel;
      } else {
        return {...option};
      }
    });

    return {
      ...state,
      customField: {
        ...state.customField,
        options: updatedOptions
      }
    };
  }

  changeOptionCode(optionLabel: string, newCode: string): Observable<void> {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminCustomFieldViewState) => this.changeOptionCodeServerSide(state, optionLabel, newCode)),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.updateStateWithNewOptionCode(state, optionLabel, newCode)),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  private changeOptionCodeServerSide(state: AdminCustomFieldViewState, optionLabel: string, newCode: string): Observable<void> {
    const requestBody = {currentLabel: optionLabel, code: newCode};
    const urlParts = [
      'custom-fields',
      state.customField.id.toString(),
      'options',
      'code'
    ];
    return this.restService.post(urlParts, requestBody);
  }

  private updateStateWithNewOptionCode(state: AdminCustomFieldViewState,
                                       currentLabel: string,
                                       newCode: string): AdminCustomFieldViewState {
    const updatedOptions = [...state.customField.options];
    updatedOptions.map((option: CustomFieldOption) => {
      if (option.label === currentLabel) {
        return option.code = newCode;
      } else {
        return {...option};
      }
    });

    return {
      ...state,
      customField: {
        ...state.customField,
        options: updatedOptions
      }
    };
  }

  changeOptionColor(optionLabel: string, newColor: string): void {
    this.state$.pipe(
      take(1),
      switchMap((state: AdminCustomFieldViewState) => this.changeOptionColorServerSide(state, optionLabel, newColor)),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.updateStateWithNewOptionColor(state, optionLabel, newColor)),
      map((nextState) => this.store.commit(nextState)),
    ).subscribe();
  }

  private changeOptionColorServerSide(state: AdminCustomFieldViewState, optionLabel: string, newColor: string) {
    const requestBody = {currentLabel: optionLabel, color: newColor};
    const urlParts = [
      'custom-fields',
      state.customField.id.toString(),
      'options',
      'color'
    ];
    return this.restService.post(urlParts, requestBody);
  }

  private updateStateWithNewOptionColor(state: AdminCustomFieldViewState,
                                        optionLabel: string,
                                        newColor: string): AdminCustomFieldViewState {
    const updatedOptions = [...state.customField.options];
    updatedOptions.map((option: CustomFieldOption) => {
      if (option.label === optionLabel) {
        return option.colour = newColor;
      } else {
        return {...option};
      }
    });

    return {
      ...state,
      customField: {
        ...state.customField,
        options: updatedOptions
      }
    };
  }

  public changeOptionsPosition(optionsToMove: string[], position: number): Observable<void> {
    return this.state$.pipe(
      take(1),
      switchMap(({customField}) => {
        const customFieldId = customField.id;
        const urlParts = [this.getRootUrl(), customFieldId.toString(), 'options', 'positions'];
        const body = {
          labels: optionsToMove,
          position: position,
        };
        return this.restService.post<CustomField>(urlParts, body);
      }),
      withLatestFrom(this.store.state$),
      map(([customField, state]) => ({
        ...state,
        customField: {
          ...state.customField,
          ...customField,
        }
      })),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  getCurrentUser(): Observable<AuthenticatedUser> {
    throw new Error('Not implemented.');
  }
}

interface OptionToAdd {
  label: string;
  code: string;
  colour: string;
}

export const getCustomFieldViewState = createFeatureSelector<AdminCustomFieldViewState, AdminCustomFieldState>('customField');

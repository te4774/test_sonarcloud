import {ChangeDetectionStrategy, Component, InjectionToken, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminProjectViewComponentData} from '../../project-view/project-view.component';
import {takeUntil} from 'rxjs/operators';
import {ProjectViewService} from '../../../services/project-view.service';
import {
  DataRow,
  Extendable,
  Fixed,
  GenericDataRow,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  ReferentialDataService,
  RestService,
  StyleDefinitionBuilder,
  textColumn
} from 'sqtm-core';
import {
  enablePluginColumn
} from '../../../components/cell-renderers/project-plugin-enabled-cell/project-plugin-enabled-cell.component';
import {
  pluginStatusColumn
} from '../../../components/cell-renderers/project-plugin-status-cell/project-plugin-status-cell.component';
import {
  configurePluginColumn
} from '../../../components/cell-renderers/plugin-configure-cell/plugin-configure-cell.component';

export const PROJECT_PLUGINS_TABLE_CONF = new InjectionToken('PROJECT_PLUGINS_TABLE_CONF');
export const PROJECT_PLUGINS_TABLE = new InjectionToken('PROJECT_PLUGINS_TABLE');

export function projectPluginsTableDefinition(): GridDefinition {
  return grid('project-plugins')
    .withColumns([
      indexColumn(),
      enablePluginColumn('enabled')
        .withHeaderPosition('center')
        .withI18nKey('sqtm-core.entity.plugin.enabled')
        .changeWidthCalculationStrategy(new Fixed(90)),
      textColumn('type')
        .withI18nKey('sqtm-core.entity.plugin.type')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      textColumn('name')
        .withI18nKey('sqtm-core.entity.plugin.name')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      pluginStatusColumn('status')
        .withI18nKey('sqtm-core.entity.plugin.status')
        .withHeaderPosition('center')
        .changeWidthCalculationStrategy(new Fixed(90)),
      configurePluginColumn('configUrl')
        .withI18nKey('sqtm-core.entity.plugin.configure')
        .withHeaderPosition('center')
        .changeWidthCalculationStrategy(new Fixed(100)),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .disableRightToolBar()
    .withRowHeight(35)
    .withRowConverter(pluginConverter)
    .build();
}

@Component({
  selector: 'sqtm-app-project-plugins',
  templateUrl: './project-plugins.component.html',
  styleUrls: ['./project-plugins.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: PROJECT_PLUGINS_TABLE_CONF,
      useFactory: projectPluginsTableDefinition
    },
    {
      provide: PROJECT_PLUGINS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, PROJECT_PLUGINS_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: PROJECT_PLUGINS_TABLE
    },
  ]
})
export class ProjectPluginsComponent implements OnInit, OnDestroy {

  componentData$: Observable<AdminProjectViewComponentData>;
  unsub$ = new Subject<void>();

  constructor(private projectViewService: ProjectViewService,
              private gridService: GridService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.projectViewService.componentData$.pipe(takeUntil(this.unsub$));

    this.gridService.connectToDatasource(this.projectViewService.availablePlugins$, 'index');

    this.projectViewService.availablePlugins$.pipe(
      takeUntil(this.unsub$),
    ).subscribe((plugins) => {
      const hasPluginWithConf = plugins.some(plugin => Boolean(plugin.configUrl));
      this.gridService.setColumnVisibility('configUrl', hasPluginWithConf);
    });
  }


  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }
}

function pluginConverter(literals: Partial<DataRow>[]): DataRow[] {
  return literals.reduce((datarows, literal) => {
    const dataRow = new GenericDataRow();
    Object.assign(dataRow, literal);
    dataRow.disabled = !literal.data['enabled'];
    datarows.push(dataRow);
    return datarows;
  }, []);
}

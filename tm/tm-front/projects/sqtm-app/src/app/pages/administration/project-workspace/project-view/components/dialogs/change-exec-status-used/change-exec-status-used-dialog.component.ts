import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {
  CapitalizePipe,
  DialogReference,
  DisplayOption,
  ExecutionStatus,
  FieldValidationError,
  RestService,
  SelectFieldComponent
} from 'sqtm-core';
import {ProjectViewService} from '../../../services/project-view.service';
import {switchMap, take} from 'rxjs/operators';
import {ChangeExecStatusUsedDialogConfiguration} from './change-exec-status-used-dialog.configuration';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-change-execution-status-already-in-use-dialog',
  templateUrl: './change-exec-status-used-dialog.component.html',
  styleUrls: ['./change-exec-status-used-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChangeExecStatusUsedDialogComponent implements OnInit, AfterViewInit {

  statusOptions: DisplayOption[];
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: ChangeExecStatusUsedDialogConfiguration;

  @ViewChild(SelectFieldComponent)
  statusSelectField: SelectFieldComponent;

  constructor(private dialogReference: DialogReference<ChangeExecStatusUsedDialogConfiguration>,
              private restService: RestService,
              private cdr: ChangeDetectorRef,
              private fb: FormBuilder,
              private projectViewService: ProjectViewService,
              private translateService: TranslateService,
              private capitalizePipe: CapitalizePipe) {
    this.data = this.dialogReference.data;
  }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  ngAfterViewInit(): void {
    this.projectViewService.componentData$.pipe(
      take(1),
      switchMap(componentData => {
        const projectId = componentData.project.id.toString();

        return this.restService.get<StatusesResponse>(
          ['generic-projects', projectId, 'get-enabled-execution-status', this.data.selectedStatus]);
      })
    ).subscribe((response) => {
      const statuses = this.retrieveStatusesAsDisplayOption(response.statuses);
      this.statusOptions = [...statuses];
      this.setStatusDefaultOption();
      this.cdr.detectChanges();
    });
  }

  private setStatusDefaultOption() {
    if (this.statusOptions.length > 0 && this.statusSelectField != null) {
      this.statusSelectField.disabled = false;
      this.formGroup.controls['status'].setValue(this.statusOptions[0].id);
    }
  }

  private retrieveStatusesAsDisplayOption(statuses) {
    return statuses.map(status => {
      const tokens = status.label.split('.');
      return {id: status.id, label: tokens[tokens.length - 1]};
    })
      .filter(status => status.label != null)
      .map((status) => {
        return {
          id: status.id,
          label: this.capitalizePipe.transform(this.translateService.instant(ExecutionStatus[status.label].i18nKey)),
        };
      });
  }

  confirm() {

    if (!this.formGroup.valid) {
      this.statusSelectField.showClientSideError();
      return;
    }
    const targetExecutionStatus = this.formGroup.controls['status'].value;
    this.projectViewService.disableAndReplaceStatusWithinProject(this.data.selectedStatus, targetExecutionStatus);
    this.dialogReference.result = true;
    this.dialogReference.close();
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      status: this.fb.control(null, [
        Validators.required,
      ]),
    });
  }
}

export interface StatusesResponse {
  statuses: { id: string, label: string }[];
}

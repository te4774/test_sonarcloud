import {ChangeDetectionStrategy, Component, InjectionToken, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {
  DialogReference,
  Extendable,
  Fixed,
  grid,
  GridDefinition,
  GridFilter,
  GridService,
  gridServiceFactory,
  PaginationConfigBuilder,
  ReferentialDataService,
  RestService,
  selectRowColumn,
  textColumn,
  ToggleSelectionHeaderRendererComponent
} from 'sqtm-core';
import {takeUntil} from 'rxjs/operators';
import {BindProjectsToMilestoneDialogConfiguration} from './bind-projects-to-milestone-dialog.configuration';
// tslint:disable-next-line:max-line-length
import {
  templateColumn
} from '../../../../../project-workspace/project-workspace/components/cell-renderers/project-template-cell-renderer/project-template-cell-renderer.component';

export const BIND_PROJECTS_TO_MILESTONE_TABLE_CONF = new InjectionToken('BIND_PROJECTS_TO_MILESTONE_TABLE_CONF');
export const BIND_PROJECTS_TO_MILESTONE_TABLE = new InjectionToken('BIND_PROJECTS_TO_MILESTONE_TABLE');

export function bindProjectsToMilestoneDefinition(): GridDefinition {
  return grid('bind-projects-to-milestone-grid')
    .withColumns(
      [
        selectRowColumn()
          .withHeaderRenderer(ToggleSelectionHeaderRendererComponent)
          .changeWidthCalculationStrategy(new Fixed(40)),
        templateColumn('template')
          .withHeaderPosition('center')
          .disableHeader()
          .changeWidthCalculationStrategy(new Fixed(50)),
        textColumn('name')
          .withI18nKey('sqtm-core.entity.generic.name.label')
          .changeWidthCalculationStrategy(new Extendable(100, 1)),
        textColumn('label')
          .withI18nKey('sqtm-core.entity.project.label.tag')
          .changeWidthCalculationStrategy(new Extendable(100, 0.8)),
      ]
    )
    .withPagination(
      new PaginationConfigBuilder().inactive()
    )
    .disableRightToolBar()
    .enableMultipleColumnsFiltering(['name', 'label'])
    .build();
}

@Component({
  selector: 'sqtm-app-bind-projects-to-milestone-dialog',
  templateUrl: './bind-projects-to-milestone-dialog.component.html',
  styleUrls: ['./bind-projects-to-milestone-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: BIND_PROJECTS_TO_MILESTONE_TABLE_CONF,
      useFactory: bindProjectsToMilestoneDefinition
    },
    {
      provide: BIND_PROJECTS_TO_MILESTONE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, BIND_PROJECTS_TO_MILESTONE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: BIND_PROJECTS_TO_MILESTONE_TABLE
    }
  ]
})
export class BindProjectsToMilestoneDialogComponent implements OnInit, OnDestroy {

  data: BindProjectsToMilestoneDialogConfiguration;
  private selectedProjectIds: number[];
  private unsub$ = new Subject<void>();

  constructor(private dialogReference: DialogReference<BindProjectsToMilestoneDialogConfiguration, number[]>,
              private restService: RestService,
              private gridService: GridService) {
    this.data = dialogReference.data;
  }

  ngOnInit(): void {
    this.initializeGrid();
    this.initializeFilters();
    this.initializeOutput();
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }


  confirm() {
    this.dialogReference.result = Array.from(this.selectedProjectIds);
    this.dialogReference.close();
  }

  private initializeGrid() {
    this.gridService.loadInitialData(this.data.bindableProjects, this.data.bindableProjects.length, 'id');
  }

  private initializeOutput() {
    this.gridService.selectedRowIds$.pipe(
      takeUntil(this.unsub$),
    ).subscribe((rowIds: number[])  => {
      this.selectedProjectIds = rowIds;
    });
  }

  private initializeFilters() {
    const filters: GridFilter[] = [{
      id: 'name', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false
    }, {
      id: 'label', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false
    }];
    this.gridService.addFilters(filters);
  }

  handleResearchInput($event: string) {
    this.gridService.applyMultiColumnsFilter($event);
  }
}

export interface BindableProject {
  id: number;
  label: string;
  name: string;
  template: boolean;
}

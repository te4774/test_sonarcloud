import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {ProjectWorkspaceComponent} from './project-workspace/containers/project-workspace/project-workspace.component';
import {ProjectGridComponent} from './project-workspace/containers/project-grid/project-grid.component';
import {TranslateModule} from '@ngx-translate/core';
import {NzBadgeModule} from 'ng-zorro-antd/badge';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzCollapseModule} from 'ng-zorro-antd/collapse';
import {NzDividerModule} from 'ng-zorro-antd/divider';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzFormModule} from 'ng-zorro-antd/form';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzSpinModule} from 'ng-zorro-antd/spin';
import {NzSwitchModule} from 'ng-zorro-antd/switch';
import {NzTabsModule} from 'ng-zorro-antd/tabs';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {
  AnchorModule,
  AttachmentModule,
  CapitalizePipe,
  CellRendererCommonModule,
  DialogModule,
  GridExportModule,
  GridModule,
  MilestoneModule,
  NavBarModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule
} from 'sqtm-core';
import {RouterModule, Routes} from '@angular/router';
import {ProjectViewComponent} from './project-view/containers/project-view/project-view.component';
import {
  ProjectContentComponent
} from './project-view/containers/panel-groups/project-content/project-content.component';
import {ProjectCustomComponent} from './project-view/containers/panel-groups/project-custom/project-custom.component';
import {
  TemplateFromProjectCreationDialogComponent
} from './project-workspace/components/dialogs/template-from-project-creation-dialog/template-from-project-creation-dialog.component';
import {
  TemplateCreationDialogComponent
} from './project-workspace/components/dialogs/template-creation-dialog/template-creation-dialog.component';
import {
  ProjectCreationDialogComponent
} from './project-workspace/components/dialogs/project-creation-dialog/project-creation-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CKEditorModule} from 'ckeditor4-angular';
import {
  ProjectTemplateCellRendererComponent
} from './project-workspace/components/cell-renderers/project-template-cell-renderer/project-template-cell-renderer.component';
import {
  ProjectMilestonesComponent
} from './project-view/containers/panel-groups/project-milestones/project-milestones.component';
import {
  ProjectPluginsComponent
} from './project-view/containers/panel-groups/project-plugins/project-plugins.component';
import {
  ProjectBugtrackerPanelComponent
} from './project-view/components/panels/project-bugtracker-panel/project-bugtracker-panel.component';
import {
  ProjectInformationPanelComponent
} from './project-view/components/panels/project-information-panel/project-information-panel.component';
import {
  ProjectPermissionsPanelComponent
} from './project-view/components/panels/project-permissions-panel/project-permissions-panel.component';
import {
  ProjectCustomFieldsPanelComponent
} from './project-view/components/panels/project-custom-fields-panel/project-custom-fields-panel.component';
import {
  AssociateTemplateDialogComponent
} from './project-view/components/dialogs/associate-template-dialog/associate-template-dialog.component';
import {
  ProjectPermissionsProfileCellComponent
} from './project-view/components/cell-renderers/project-permissions-profile-cell/project-permissions-profile-cell.component';
import {
  ProjectPermissionsTypeCellComponent
} from './project-view/components/cell-renderers/project-permissions-type-cell/project-permissions-type-cell.component';
import {
  DeleteProjectPermissionCellComponent
} from './project-view/components/cell-renderers/delete-project-permission-cell/delete-project-permission-cell.component';
import {
  ProjectExecutionPanelComponent
} from './project-view/components/panels/project-execution-panel/project-execution-panel.component';
import {
  ProjectAutomationPanelComponent
} from './project-view/components/panels/project-automation-panel/project-automation-panel.component';
import {
  DeleteProjectJobCellComponent
} from './project-view/components/cell-renderers/delete-project-job-cell/delete-project-job-cell.component';
import {
  ProjectJobCanRunBddCellComponent
} from './project-view/components/cell-renderers/project-job-can-run-bdd-cell/project-job-can-run-bdd-cell.component';
import {
  EditProjectJobCellComponent
} from './project-view/components/cell-renderers/edit-project-job-cell/edit-project-job-cell.component';
import {
  ChangeExecStatusUsedDialogComponent
} from './project-view/components/dialogs/change-exec-status-used/change-exec-status-used-dialog.component';
import {AddJobDialogComponent} from './project-view/components/dialogs/add-job-dialog/add-job-dialog.component';
import {
  AddJobLabelCellComponent
} from './project-view/components/cell-renderers/add-job-label-cell/add-job-label-cell.component';
import {
  AddJobBddCellComponent
} from './project-view/components/cell-renderers/add-job-bdd-cell/add-job-bdd-cell.component';
import {
  EditProjectAutomationJobDialogComponent
} from './project-view/components/dialogs/edit-project-automation-job-dialog/edit-project-automation-job-dialog.component';
import {
  AddPermissionDialogComponent
} from './project-view/components/dialogs/add-permission-dialog/add-permission-dialog.component';
import {
  ProjectInfoListsPanelComponent
} from './project-view/components/panels/project-info-lists-panel/project-info-lists-panel.component';
import {
  CustomFieldBoundEntityCellComponent
} from './project-view/components/cell-renderers/custom-field-bound-entity-cell/custom-field-bound-entity-cell.component';
import {
  CustomFieldBindingTypeCellComponent
} from './project-view/components/cell-renderers/custom-field-binding-type-cell/custom-field-binding-type-cell.component';
import {
  BindCustomFieldDialogComponent
} from './project-view/components/dialogs/bind-custom-field-dialog/bind-custom-field-dialog.component';
import {
  UnbindCustomFieldFromProjectCellComponent
} from './project-view/components/cell-renderers/unbind-custom-field-from-project-cell/unbind-custom-field-from-project-cell.component';
import {
  ProjectMilestonesPanelComponent
} from './project-view/components/panels/project-milestones-panel/project-milestones-panel.component';
import {
  ProjectJobLabelCellComponent
} from './project-view/components/cell-renderers/project-job-label-cell/project-job-label-cell.component';
import {
  UnbindMilestoneFromProjectCellComponent
} from './project-view/components/cell-renderers/unbind-milestone-from-project-cell/unbind-milestone-from-project-cell.component';
import {
  CreateBindMilestoneDialogComponent
} from './project-view/components/dialogs/create-bind-milestone-dialog/create-bind-milestone-dialog.component';
import {
  BindMilestoneDialogComponent
} from './project-view/components/dialogs/bind-milestone-dialog/bind-milestone-dialog.component';
import {
  ProjectPluginEnabledCellComponent
} from './project-view/components/cell-renderers/project-plugin-enabled-cell/project-plugin-enabled-cell.component';
import {
  ProjectPluginStatusCellComponent
} from './project-view/components/cell-renderers/project-plugin-status-cell/project-plugin-status-cell.component';
import {
  PluginConfigureCellComponent
} from './project-view/components/cell-renderers/plugin-configure-cell/plugin-configure-cell.component';
import {AdminViewHeaderModule} from '../components/admin-view-header/admin-view-header.module';
import {
  ProjectViewWithGridComponent
} from './project-view/containers/project-view-with-grid/project-view-with-grid.component';
import {ProjectViewDetailComponent} from './project-view/containers/project-view-detail/project-view-detail.component';
import {
  ConfigurePluginDialogComponent
} from './project-view/components/dialogs/configure-plugin-dialog/configure-plugin-dialog.component';
import {
  DeactivatePluginDialogComponent
} from './project-view/components/dialogs/deactivate-plugin-dialog/deactivate-plugin-dialog.component';
import {
  TAServerConnectDialogComponent
} from './project-view/components/dialogs/taserver-connect-dialog/taserver-connect-dialog.component';
import {
  AutomatedExecutionEnvironmentModule
} from '../../../components/automated-execution-environment/automated-execution-environment.module';
import {
  AutomatedExecutionEnvironmentVariableModule
} from '../../../components/automated-execution-environment-variable/automated-execution-environment-variable.module';

export const routes: Routes = [
  {
    path: '',
    component: ProjectWorkspaceComponent,
    children: [
      {
        path: ':projectId',
        component: ProjectViewWithGridComponent,
        children: [
          {
            path: '',
            redirectTo: 'content',
          },
          {
            path: 'content',
            component: ProjectContentComponent,
          },
          {
            path: 'custom',
            component: ProjectCustomComponent,
          },
          {
            path: 'milestones',
            component: ProjectMilestonesComponent,
          },
          {
            path: 'plugins',
            component: ProjectPluginsComponent,
          }
        ]
      },
    ],
  },
  {
    path: 'detail/:projectId',
    component: ProjectViewDetailComponent,
    children: [
      {
        path: '',
        redirectTo: 'content',
      },
      {
        path: 'content',
        component: ProjectContentComponent,
      },
      {
        path: 'custom',
        component: ProjectCustomComponent,
      },
      {
        path: 'milestones',
        component: ProjectMilestonesComponent,
      },
      {
        path: 'plugins',
        component: ProjectPluginsComponent,
      }
    ],
  },
];

@NgModule({
  declarations: [
    ProjectWorkspaceComponent,
    ProjectGridComponent,
    ProjectCreationDialogComponent,
    TemplateFromProjectCreationDialogComponent,
    TemplateCreationDialogComponent,
    ProjectTemplateCellRendererComponent,
    ProjectViewComponent,
    ProjectContentComponent,
    ProjectCustomComponent,
    ProjectBugtrackerPanelComponent,
    ProjectInformationPanelComponent,
    ProjectPermissionsPanelComponent,
    ProjectCustomFieldsPanelComponent,
    AssociateTemplateDialogComponent,
    ProjectPermissionsProfileCellComponent,
    ProjectPermissionsTypeCellComponent,
    DeleteProjectPermissionCellComponent,
    ProjectExecutionPanelComponent,
    ProjectAutomationPanelComponent,
    DeleteProjectJobCellComponent,
    ProjectJobCanRunBddCellComponent,
    EditProjectJobCellComponent,
    ChangeExecStatusUsedDialogComponent,
    AddJobDialogComponent,
    AddJobLabelCellComponent,
    AddJobBddCellComponent,
    EditProjectAutomationJobDialogComponent,
    AddPermissionDialogComponent,
    ProjectInfoListsPanelComponent,
    CustomFieldBoundEntityCellComponent,
    CustomFieldBindingTypeCellComponent,
    BindCustomFieldDialogComponent,
    UnbindCustomFieldFromProjectCellComponent,
    ProjectMilestonesComponent,
    ProjectMilestonesPanelComponent,
    ProjectJobLabelCellComponent,
    UnbindMilestoneFromProjectCellComponent,
    CreateBindMilestoneDialogComponent,
    BindMilestoneDialogComponent,
    ProjectPluginsComponent,
    ProjectPluginEnabledCellComponent,
    ProjectPluginStatusCellComponent,
    PluginConfigureCellComponent,
    ProjectViewWithGridComponent,
    ProjectViewDetailComponent,
    ConfigurePluginDialogComponent,
    DeactivatePluginDialogComponent,
    TAServerConnectDialogComponent,
  ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        TranslateModule.forChild(),
        NzDropDownModule,
        GridModule,
        WorkspaceLayoutModule,
        NzIconModule,
        NzCheckboxModule,
        NzFormModule,
        NzDividerModule,
        DialogModule,
        ReactiveFormsModule,
        WorkspaceCommonModule,
        CKEditorModule,
        UiManagerModule,
        NavBarModule,
        AnchorModule,
        NzToolTipModule,
        AttachmentModule,
        NzBadgeModule,
        NzButtonModule,
        NzCollapseModule,
        NzSwitchModule,
        FormsModule,
        NzInputModule,
        NzSpinModule,
        NzSelectModule,
        NzTabsModule,
        MilestoneModule,
        CellRendererCommonModule,
        AdminViewHeaderModule,
        GridExportModule,
        AutomatedExecutionEnvironmentModule,
        AutomatedExecutionEnvironmentVariableModule
    ], providers: [
    DatePipe,
    CapitalizePipe
  ],
})
export class ProjectWorkspaceModule {
}

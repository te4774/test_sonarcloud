import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {ADMIN_WS_CONNECTION_LOG_TABLE, ADMIN_WS_CONNECTION_LOG_TABLE_CONFIG} from '../../../user-workspace.constant';
import {adminConnectionLogTableDefinition} from '../connection-log-grid/connection-log-grid.component';

@Component({
  selector: 'sqtm-app-connection-log-workspace',
  templateUrl: './connection-log-workspace.component.html',
  styleUrls: ['./connection-log-workspace.component.less'],
  providers: [
    {
      provide: ADMIN_WS_CONNECTION_LOG_TABLE_CONFIG,
      useFactory: adminConnectionLogTableDefinition,
      deps: []
    },
    {
      provide: ADMIN_WS_CONNECTION_LOG_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_CONNECTION_LOG_TABLE_CONFIG, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_CONNECTION_LOG_TABLE
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConnectionLogWorkspaceComponent implements OnInit, OnDestroy  {

  authenticatedAdmin$: Observable<AuthenticatedUser>;

  constructor(public readonly adminReferentialDataService: AdminReferentialDataService,
              private gridService: GridService) {
  }

  ngOnInit(): void {
    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}

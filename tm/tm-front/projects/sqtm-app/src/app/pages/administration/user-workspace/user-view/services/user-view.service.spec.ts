import {TestBed} from '@angular/core/testing';

import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RestService, User} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {of} from 'rxjs';
import {UserViewService} from './user-view.service';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {take} from 'rxjs/operators';
import {mockRestService} from '../../../../../utils/testing-utils/mocks.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('UserViewService', () => {

  const restService = mockRestService();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: UserViewService,
          useClass: UserViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
  });

  it('should load a user', (done) => {
    const service = TestBed.inject(UserViewService);
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
    service.componentData$.subscribe(data => {
      expect(data.user.id).toEqual(1);
      done();
    });
    expect(service).toBeTruthy();
    service.load(1);
  });

  it('should activate user', (done) => {
    const service = TestBed.inject(UserViewService);
    const user = getInitialModel();
    user.active = false;
    restService.getWithoutErrorHandling.and.returnValue(of(user));
    service.load(1);
    service.activateUser();
    service.componentData$.pipe(
      take(1)
    ).subscribe(data => {
      expect(data.user.active).toEqual(true);
      done();
    });
  });

  it('should deactivate user', (done) => {
    const service = TestBed.inject(UserViewService);
    const user = getInitialModel();
    user.active = true;
    restService.getWithoutErrorHandling.and.returnValue(of(user));
    service.load(1);
    service.deactivateUser();
    service.componentData$.pipe(
      take(1)
    ).subscribe(data => {
      expect(data.user.active).toEqual(false);
      done();
    });
  });

  it('should change user group of user', (done) => {
    const service = TestBed.inject(UserViewService);
    const user = getInitialModel();
    user.usersGroupBinding = 1;
    restService.getWithoutErrorHandling.and.returnValue(of(user));
    service.load(1);
    service.confirmUserGroup(2);
    service.componentData$.pipe(
      take(1)
    ).subscribe(data => {
      expect(data.user.usersGroupBinding).toEqual(2);
      done();
    });
  });

  it('should give user authorisation on a project', (done) => {
    const service = TestBed.inject(UserViewService);
    const user = getInitialModel();
    restService.getWithoutErrorHandling.and.returnValue(of(user));
    service.load(1);
    restService.post.and.returnValue(of({
        projectPermissions:
          [
            {
              projectId: 1,
              projectName: 'Projet 1',
              permissionGroup: {
                id: 1,
                qualifiedName: 'automaticien',
                simpleName: 'automaticien'
              }
            }
          ]
      }
    ));
    service.setUserAuthorisation([1], 'automaticien').subscribe();
    service.componentData$.pipe(
      take(1)
    ).subscribe(data => {
      expect(data.user.projectPermissions).toEqual([
        {
          projectId: 1,
          projectName: 'Projet 1',
          permissionGroup: {
            id: 1,
            qualifiedName: 'automaticien',
            simpleName: 'automaticien'
          }
        }
      ]);
      done();
    });
  });

  it('should remove user authorisation on a project', (done) => {
    const service = TestBed.inject(UserViewService);
    const user = getInitialModel();
    user.projectPermissions = [
      {
        projectId: 1,
        projectName: 'Projet 1',
        permissionGroup: {
          id: 1,
          qualifiedName: 'automaticien',
          simpleName: 'automaticien'
        }
      }
    ];
    restService.getWithoutErrorHandling.and.returnValue(of(user));
    service.load(1);
    restService.delete.and.returnValue(of(null));
    service.removeAuthorisation([1]).subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(data => {
        expect(data.user.projectPermissions).toEqual([]);
        done();
      });
    });
  });

  it('should associate a user to a team', (done) => {
    const service = TestBed.inject(UserViewService);
    const user = getInitialModel();
    restService.getWithoutErrorHandling.and.returnValue(of(user));
    service.load(1);
    restService.post.and.returnValue(of({
        teams:
          [
            {
              partyId: 1,
              name: 'Team 1',
            }
          ]
      }
    ));
    service.associateTeams([1]).subscribe();
    service.componentData$.pipe(
      take(1)
    ).subscribe(data => {
      expect(data.user.teams).toEqual([
        {
          partyId: 1,
          name: 'Team 1',
        }
      ]);
      done();
    });
  });

  it('should remove user from team', (done) => {
    const service = TestBed.inject(UserViewService);
    const user = getInitialModel();
    user.teams = [
      {
        partyId: 1,
        name: 'Team 1',
      }
    ];
    restService.getWithoutErrorHandling.and.returnValue(of(user));
    service.load(1);
    restService.delete.and.returnValue(of(null));
    service.removeTeamAssociation([1]).subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(data => {
        expect(data.user.teams).toEqual([]);
        done();
      });
    });
  });

  function getInitialModel(): User {
    return {
      id: 1,
      login: 'jdoe',
      firstName: 'John',
      lastName: 'Doe',
      email: 'jdoe@test.fr',
      active: true,
      createdBy: null,
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
      lastConnectedOn: null,
      usersGroupBinding: 1,
      usersGroups: [],
      projectPermissions: [],
      teams: [],
      canManageLocalPassword: true,
    };
  }
});

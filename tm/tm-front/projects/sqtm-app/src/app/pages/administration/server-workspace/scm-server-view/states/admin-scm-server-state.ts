import {
  AuthenticationPolicy,
  AuthenticationProtocol,
  Credentials,
  ScmRepository,
  SqtmGenericEntityState
} from 'sqtm-core';

export interface AdminScmServerState extends SqtmGenericEntityState {
  serverId: number;
  name: string;
  url: string;
  kind: string;
  committerMail: string;
  repositories: ScmRepository[];
  authPolicy: AuthenticationPolicy;
  authProtocol: AuthenticationProtocol;
  supportedAuthenticationProtocols: AuthenticationProtocol[];
  credentials?: Credentials;
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ProjectMilestonesPanelComponent} from './project-milestones-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogService, GridService, Milestone, RestService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {PROJECT_MILESTONES_TABLE} from '../../../project-view.constant';
import {ProjectViewService} from '../../../services/project-view.service';
import {of} from 'rxjs';
import {mockDialogService, mockGridService} from '../../../../../../../utils/testing-utils/mocks.service';
import createSpyObj = jasmine.createSpyObj;

describe('ProjectMilestonesPanelComponent', () => {
  let component: ProjectMilestonesPanelComponent;
  let fixture: ComponentFixture<ProjectMilestonesPanelComponent>;
  const restService = {};

  const projectViewService = createSpyObj([
    'bindMilestonesToProject',
    'unbindMilestones',
    'updateBoundMilestoneInformation']);

  projectViewService.componentData$ = of({
    project: {
      id: 1,
      boundMilestonesInformation: [
        {
          milestone: {},
          milestoneBoundToOneObjectOfProject: true,
        }
      ]
    }
  });

  const dialogService = mockDialogService();
  const gridService = mockGridService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule, OverlayModule],
      declarations: [ProjectMilestonesPanelComponent],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: PROJECT_MILESTONES_TABLE,
          useValue: gridService,
        },
        {
          provide: ProjectViewService,
          useValue: projectViewService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectMilestonesPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    projectViewService.bindMilestonesToProject.calls.reset();
    projectViewService.updateBoundMilestoneInformation.calls.reset();
    projectViewService.unbindMilestones.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should bind milestones', () => {
    expect(component).toBeTruthy();

    const someMilestone: Milestone = {
      id: 1,
      label: 'milestone',
    } as Milestone;

    dialogService.openDialog.and.returnValue({
      dialogClosed$: of([{someMilestone}]),
    } as any);

    component.openBindMilestonesDialog();

    expect(projectViewService.bindMilestonesToProject).toHaveBeenCalled();
  });

  it('should create and bind milestone', () => {
    expect(component).toBeTruthy();

    const someMilestone: Milestone = {
      id: 1,
      label: 'milestone',
    } as Milestone;

    dialogService.openDialog.and.returnValue({
      dialogClosed$: of([{someMilestone}]),
    } as any);

    component.openCreateBindMilestonesDialog();

    expect(projectViewService.updateBoundMilestoneInformation).toHaveBeenCalled();
  });

  it('should unbind milestone', () => {
    gridService.selectedRowIds$ = of([1, 2, 3]);

    projectViewService.unbindMilestones.and.returnValue(of(null));

    dialogService.openDeletionConfirm.and.returnValue({
      dialogClosed$: of([1, 2, 3]),
    } as any);

    component.openUnbindMilestonesDialog();

    expect(projectViewService.unbindMilestones).toHaveBeenCalledWith([1, 2, 3]);
  });
});

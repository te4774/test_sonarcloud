import {ChangeDetectionStrategy, Component, OnDestroy} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {SystemViewState} from '../../../states/system-view.state';
import {SystemViewService} from '../../../services/system-view.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-system-view-cleaning',
  templateUrl: './system-view-cleaning.component.html',
  styleUrls: ['./system-view-cleaning.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemViewCleaningComponent implements OnDestroy {
  componentData$: Observable<SystemViewState>;

  unsub$ = new Subject<void>();

  constructor(public readonly systemViewService: SystemViewService) {
    this.componentData$ = this.systemViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}

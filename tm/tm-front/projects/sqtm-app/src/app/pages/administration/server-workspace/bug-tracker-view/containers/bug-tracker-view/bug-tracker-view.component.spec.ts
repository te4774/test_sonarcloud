import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {BugTrackerViewComponent} from './bug-tracker-view.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {TranslateService} from '@ngx-translate/core';
import {EMPTY, of, throwError} from 'rxjs';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {ActionErrorDisplayService, DialogService, GridService, RestService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {BugTrackerViewService} from '../../services/bug-tracker-view.service';
import {AdminBugTrackerViewState} from '../../states/admin-bug-tracker-view-state';
import {HttpErrorResponse} from '@angular/common/http';
import {mockDialogService, mockGridService, mockRestService} from '../../../../../../utils/testing-utils/mocks.service';

describe('BugTrackerViewComponent', () => {
  let component: BugTrackerViewComponent;
  let fixture: ComponentFixture<BugTrackerViewComponent>;

  const restService = mockRestService();
  const dialogService = mockDialogService();
  const viewService = jasmine.createSpyObj(['load']);

  const componentData: AdminBugTrackerViewState = {
    bugTracker: {
      id: 123,
    }
  } as AdminBugTrackerViewState;

  viewService['componentData$'] = of(componentData);

  const actionErrorDisplayService = jasmine.createSpyObj(['handleActionError']);

  const gridService = mockGridService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, AppTestingUtilsModule, NzDropDownModule],
      providers: [
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant']),
        },
        {
          provide: BugTrackerViewService,
          useValue: viewService,
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({id: 123}),
            paramMap: of(convertToParamMap({
              toto: 'tutu',
            })),
          }
        },
        {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: ActionErrorDisplayService,
          useValue: actionErrorDisplayService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [BugTrackerViewComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BugTrackerViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should delete a bugtracker', waitForAsync(() => {
    dialogService.openDeletionConfirm.and.returnValue({
      dialogClosed$: of(true),
    }  as any);
    component.componentData$ = of({bugTracker: {id: 123}} as unknown as AdminBugTrackerViewState);
    component.handleDelete();

    const params = ['bugtracker', 123] as any;
    expect(restService.delete).toHaveBeenCalledWith(params);
  }));

  it('should show delete errors', waitForAsync(() => {
    dialogService.openDeletionConfirm.and.returnValue({
      dialogClosed$: of(true),
    }  as any);

    const httpError = new HttpErrorResponse({
      status: 412,
      error: 'BOOM',
    });

    actionErrorDisplayService.handleActionError.and.returnValue(EMPTY);
    restService.delete.and.returnValue(throwError(httpError));

    component.componentData$ = of({bugTracker: {id: 123}} as unknown as AdminBugTrackerViewState);
    component.handleDelete();

    expect(actionErrorDisplayService.handleActionError).toHaveBeenCalledWith(httpError);
  }));
});

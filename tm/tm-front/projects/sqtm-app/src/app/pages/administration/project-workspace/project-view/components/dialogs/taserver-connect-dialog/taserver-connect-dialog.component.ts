import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs';
import {AuthenticationProtocol, Credentials, DialogReference, GenericEntityViewService} from 'sqtm-core';
import {TAServerConnectConfiguration} from './taserver-connect-configuration';
import {FormControl, FormGroup} from '@angular/forms';
import {
  TestAutomationServerViewService
} from '../../../../../server-workspace/test-automation-server-view/services/test-automation-server-view.service';
import {take} from 'rxjs/operators';
import {ProjectViewService} from '../../../services/project-view.service';

@Component({
  selector: 'sqtm-app-taserver-connect-dialog',
  templateUrl: './taserver-connect-dialog.component.html',
  styleUrls: ['./taserver-connect-dialog.component.less'],
  providers: [
    {
      provide: TestAutomationServerViewService,
      useClass: TestAutomationServerViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: TestAutomationServerViewService
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TAServerConnectDialogComponent implements OnDestroy {

  public static DIALOG_ID = 'taserver-connection';
  public readonly dialogId = TAServerConnectDialogComponent.DIALOG_ID;
  private unsub$ = new Subject<void>();

  formGroup: FormGroup;
  configuration: TAServerConnectConfiguration;
  connectionError: boolean;

  constructor(private dialogReference: DialogReference<TAServerConnectConfiguration, boolean>,
              private cdRef: ChangeDetectorRef,
              public readonly projectViewService: ProjectViewService) {
    this.configuration = this.dialogReference.data;
    this.formGroup = new FormGroup({
      username: new FormControl(''),
      password: new FormControl('')
    });
  }

  get authProtocol(): AuthenticationProtocol {
    return this.configuration.taServer.authProtocol;
  }

  get usesUsernameField(): boolean {
    return [
      AuthenticationProtocol.BASIC_AUTH
    ].includes(this.authProtocol);
  }

  connect() {
    const credentials: Credentials = {
      ...this.getPayload(),
      implementedProtocol: this.configuration.taServer.authProtocol,
      type: this.configuration.taServer.authProtocol
    };
    this.projectViewService.connectToTAServer(this.configuration.taServer.id, credentials)
      .pipe(take(1))
      .subscribe(
        () => this.handleConnectionSuccess(),
        (err) => this.handleConnectionError(err),
      );
  }

  private getPayload(): any {
    switch (this.authProtocol) {
      case AuthenticationProtocol.OAUTH_1A:
        return {
          token: this.formGroup.controls['password'].value,
          tokenSecret: this.formGroup.controls['password'].value
        };
      case AuthenticationProtocol.TOKEN_AUTH:
        return {token: this.formGroup.controls['password'].value};
      case AuthenticationProtocol.BASIC_AUTH:
        return {
          username: this.formGroup.controls['username'].value,
          password: this.formGroup.controls['password'].value
        };
      default:
        throw new Error('Auth protocol "' + this.authProtocol + '" is not handled by BugtrackerConnectDialogComponent.');
    }
  }

  private handleConnectionSuccess(): void {
    this.dialogReference.result = true;
    this.dialogReference.close();
  }

  private handleConnectionError(error: any): void {
    console.error(error);
    this.connectionError = true;
    this.cdRef.detectChanges();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

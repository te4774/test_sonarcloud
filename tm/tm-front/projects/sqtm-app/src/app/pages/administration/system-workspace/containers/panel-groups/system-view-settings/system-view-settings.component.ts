import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {SystemViewState} from '../../../states/system-view.state';
import {SystemViewService} from '../../../services/system-view.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-system-view-settings',
  templateUrl: './system-view-settings.component.html',
  styleUrls: ['./system-view-settings.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemViewSettingsComponent implements OnInit, OnDestroy {
  componentData$: Observable<SystemViewState>;

  unsub$ = new Subject<void>();

  constructor(public readonly systemViewService: SystemViewService) {
    this.componentData$ = this.systemViewService.componentData$.pipe(takeUntil(this.unsub$));
  }


  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}


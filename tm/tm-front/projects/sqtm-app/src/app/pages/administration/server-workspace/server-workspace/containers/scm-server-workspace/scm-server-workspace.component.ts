import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {ADMIN_WS_SCM_SERVERS_TABLE, ADMIN_WS_SCM_SERVERS_TABLE_CONFIG} from '../../../server-workspace.constant';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {adminScmServersTableDefinition} from '../scm-server-grid/scm-server-grid.component';
import {Observable} from 'rxjs';

@Component({
  selector: 'sqtm-app-scm-server-workspace',
  templateUrl: './scm-server-workspace.component.html',
  styleUrls: ['./scm-server-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: ADMIN_WS_SCM_SERVERS_TABLE_CONFIG,
      useFactory: adminScmServersTableDefinition,
      deps: []
    },
    {
      provide: ADMIN_WS_SCM_SERVERS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_SCM_SERVERS_TABLE_CONFIG, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_SCM_SERVERS_TABLE
    }
  ]
})
export class ScmServerWorkspaceComponent implements OnInit, OnDestroy {

  authenticatedAdmin$: Observable<AuthenticatedUser>;

  constructor(public readonly adminReferentialDataService: AdminReferentialDataService,
              private gridService: GridService) {
  }

  ngOnInit(): void {
    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }

}

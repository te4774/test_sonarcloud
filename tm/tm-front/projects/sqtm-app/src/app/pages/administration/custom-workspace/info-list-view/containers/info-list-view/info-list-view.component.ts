import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {
  DataRow,
  DialogService,
  GenericEntityViewComponentData,
  GenericEntityViewService,
  GridService,
  RestService
} from 'sqtm-core';
import {InfoListViewService} from '../../services/info-list-view.service';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {AdminInfoListViewState} from '../../states/admin-info-list-view-state';
import {concatMap, filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {AdminInfoListState} from '../../states/admin-info-list-state';

@Component({
  selector: 'sqtm-app-info-list-view',
  templateUrl: './info-list-view.component.html',
  styleUrls: ['./info-list-view.component.less'],
  providers: [
    {
      provide: InfoListViewService,
      useClass: InfoListViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: InfoListViewService
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoListViewComponent implements OnInit, OnDestroy  {

  componentData$: Observable<AdminInfoListViewState>;
  private unsub$ = new Subject<void>();

  constructor(private route: ActivatedRoute,
              public readonly infoListViewService: InfoListViewService,
              private cdRef: ChangeDetectorRef,
              private restService: RestService,
              private gridService: GridService,
              private dialogService: DialogService) {
    this.componentData$ = this.infoListViewService.componentData$;
    this.prepareGridRefreshOnEntityChanges();
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('infoListId')),
      ).subscribe((id) => {
      this.infoListViewService.load(parseInt(id, 10));
    });
  }

  ngOnDestroy(): void {
    this.infoListViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  private prepareGridRefreshOnEntityChanges(): void {
    this.infoListViewService.simpleAttributeRequiringRefresh = [
      'label',
      'code',
      'description',
    ];

    this.infoListViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
    ).subscribe(() =>
      this.gridService.refreshDataAndKeepSelectedRows());
  }

  handleDelete() {
    this.componentData$.pipe(
      take(1),
      withLatestFrom(this.gridService.selectedRows$),
      concatMap(([componentData, rows]: [AdminInfoListViewComponentData, DataRow[]]) =>
        this.showConfirmDeleteInfoListDialog(componentData.infoList.id, rows)),
      filter(({confirmDelete}) => confirmDelete),
      concatMap(({infoListId}) => this.deleteInfoListServerSide(infoListId)),
    ).subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteInfoListDialog(infoListId, rows: DataRow[]): Observable<{ confirmDelete: boolean, infoListId: string }> {

    const infoListHasProject = rows[0].data['projectCount'] > 0;
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.info-lists.dialog.title.delete-one',
      messageKey: infoListHasProject ?
        'sqtm-core.administration-workspace.info-lists.dialog.message.delete-one-with-project' :
        'sqtm-core.administration-workspace.info-lists.dialog.message.delete-one-without-project',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, infoListId}))
    );
  }

  private deleteInfoListServerSide(infoListId): Observable<void> {
    return this.restService.delete(['info-lists', infoListId]);
  }
}

export interface AdminInfoListViewComponentData extends GenericEntityViewComponentData<AdminInfoListState, 'infoList'> {
}

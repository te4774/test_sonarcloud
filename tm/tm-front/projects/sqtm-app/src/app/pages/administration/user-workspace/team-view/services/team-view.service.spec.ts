import {TestBed} from '@angular/core/testing';

import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RestService, Team} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {of} from 'rxjs';
import {TeamViewService} from './team-view.service';
import {take} from 'rxjs/operators';
import {mockRestService} from '../../../../../utils/testing-utils/mocks.service';


describe('TeamViewService', () => {

  const restService = mockRestService();

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: TeamViewService,
          useClass: TeamViewService
        }
      ]
    });
  });

  it('should load a team', (done) => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));
    const service: TeamViewService = TestBed.inject(TeamViewService);
    service.componentData$.subscribe(data => {
      expect(data.team.id).toEqual(1);
      done();
    });
    expect(service).toBeTruthy();
    service.load(1);
  });

  it('should give team authorisation on a project', (done) => {
    const service = TestBed.inject(TeamViewService);
    const team = getInitialModel();
    restService.getWithoutErrorHandling.and.returnValue(of(team));
    service.load(1);
    restService.post.and.returnValue(of({
        projectPermissions:
          [
            {
              projectId: 1,
              projectName: 'Projet 1',
              permissionGroup: {
                id: 1,
                qualifiedName: 'automaticien',
                simpleName: 'automaticien'
              }
            }
          ]
      }
    ));
    service.setTeamAuthorisation([1], 'automaticien').subscribe();
    service.componentData$.pipe(
      take(1)
    ).subscribe(data => {
      expect(data.team.projectPermissions).toEqual([
        {
          projectId: 1,
          projectName: 'Projet 1',
          permissionGroup: {
            id: 1,
            qualifiedName: 'automaticien',
            simpleName: 'automaticien'
          }
        }
      ]);
      done();
    });
  });

  it('should remove team authorisation on a project', (done) => {
    const service = TestBed.inject(TeamViewService);
    const team = getInitialModel();
    team.projectPermissions = [
      {
        projectId: 1,
        projectName: 'Projet 1',
        permissionGroup: {
          id: 1,
          qualifiedName: 'automaticien',
          simpleName: 'automaticien'
        }
      }
    ];
    restService.getWithoutErrorHandling.and.returnValue(of(team));
    service.load(1);
    restService.delete.and.returnValue(of(null));
    service.removeAuthorisation([1]).subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(data => {
        expect(data.team.projectPermissions).toEqual([]);
        done();
      });
    });
  });

  it('should add a team member', (done) => {
    const service = TestBed.inject(TeamViewService);
    const team = getInitialModel();
    restService.getWithoutErrorHandling.and.returnValue(of(team));
    service.load(1);
    restService.post.and.returnValue(of({
        members:
          [
            {
              partyId: 1,
              firstName: 'John',
              lastName: 'Doe',
              active: true,
              login: 'jdoe',
              fullName: 'John Doe (jdoe)'
            }
          ]
      }
    ));
    service.addTeamMembers(['jdoe']).subscribe();
    service.componentData$.pipe(
      take(1)
    ).subscribe(data => {
      expect(data.team.members).toEqual([
        {
          partyId: 1,
          firstName: 'John',
          lastName: 'Doe',
          active: true,
          login: 'jdoe',
          fullName: 'John Doe (jdoe)'
        }
      ]);
      done();
    });
  });

  it('should remove team member', (done) => {
    const service = TestBed.inject(TeamViewService);
    const team = getInitialModel();
    team.members = [
      {
        partyId: 1,
        firstName: 'John',
        lastName: 'Doe',
        active: true,
        login: 'jdoe',
        fullName: 'John Doe (jdoe)'
      }
    ];
    restService.getWithoutErrorHandling.and.returnValue(of(team));
    service.load(1);
    restService.delete.and.returnValue(of(null));
    service.removeTeamMember([1]).subscribe();
    service.componentData$.pipe(
      take(1)
    ).subscribe(data => {
      expect(data.team.members).toEqual([]);
      done();
    });
  });

  function getInitialModel(): Team {
    return {
      id: 1,
      name: 'Team A',
      description: '',
      createdOn: null,
      createdBy: 'admin',
      lastModifiedBy: 'admin',
      lastModifiedOn: null,
      projectPermissions: [],
      members: [],
    };
  }
});

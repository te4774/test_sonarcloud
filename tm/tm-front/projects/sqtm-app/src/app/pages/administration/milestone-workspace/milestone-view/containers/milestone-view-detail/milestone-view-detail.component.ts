import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GenericEntityViewService,
  GridService,
  isAdminOrProjectManager
} from 'sqtm-core';
import {Router} from '@angular/router';
import {filter, takeUntil} from 'rxjs/operators';
import {MilestoneViewService} from '../../services/milestone-view.service';

@Component({
  selector: 'sqtm-app-milestone-view-detail',
  templateUrl: './milestone-view-detail.component.html',
  styleUrls: ['./milestone-view-detail.component.less'],
  providers: [
    {
      provide: MilestoneViewService,
      useClass: MilestoneViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: MilestoneViewService,
    },
    {
      provide: GridService,
      useValue: {},
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MilestoneViewDetailComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  constructor(public readonly adminReferentialDataService: AdminReferentialDataService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.adminReferentialDataService.refresh().subscribe();

    this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => !isAdminOrProjectManager(authUser)),
    ).subscribe(() => this.router.navigate(['home-workspace']));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  back(): void {
    history.back();
  }
}

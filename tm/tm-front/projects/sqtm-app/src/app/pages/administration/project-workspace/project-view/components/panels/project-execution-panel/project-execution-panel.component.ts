import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {AdminProjectViewComponentData} from '../../../containers/project-view/project-view.component';
import {DialogService, ExecutionStatus} from 'sqtm-core';
import {ProjectViewService} from '../../../services/project-view.service';
import {
  ChangeExecStatusUsedDialogComponent
} from '../../dialogs/change-exec-status-used/change-exec-status-used-dialog.component';
import {filter, take, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';


@Component({
  selector: 'sqtm-app-project-execution-panel',
  templateUrl: './project-execution-panel.component.html',
  styleUrls: ['./project-execution-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectExecutionPanelComponent implements OnInit, OnDestroy {

  executionStatus = ExecutionStatus;

  private unsub$: Subject<void> = new Subject();

  @Input()
  adminProjectViewComponentData: AdminProjectViewComponentData;

  constructor(public adminProjectViewService: ProjectViewService,
              public dialogService: DialogService,
              private vcRef: ViewContainerRef) { }

  ngOnInit(): void {
    this.refreshStatusesInUse();
  }

  private refreshStatusesInUse(): void {
    this.adminProjectViewService.refreshStatusesInUse().subscribe();
  }

  changeAllowTcModifDuringExec() {

    if (this.adminProjectViewComponentData.project.linkedTemplateId != null) {
      this.openAlertForLockedParameter();
    } else {
      const newValue = !this.adminProjectViewComponentData.project.allowTcModifDuringExec;
      this.adminProjectViewService.changeAllowTcModifDuringExec(newValue);
    }
  }

  changeExecutionStatusOnProject(isEnabled: Event, status: string) {
    if (this.adminProjectViewComponentData.project.linkedTemplateId != null) {
      this.openAlertForLockedParameter();

    } else if (this.adminProjectViewComponentData.project.statusesInUse[status]) {
      const dialogRef = this.dialogService.openDialog({
        id: 'change-execution-status-already-in-use-dialog',
        component: ChangeExecStatusUsedDialogComponent,
        viewContainerReference: this.vcRef,
        width: 570,
        data: {
          selectedStatus: status
        }
      });

      dialogRef.dialogClosed$.pipe(
        take(1),
        takeUntil(this.unsub$),
        filter(result => Boolean(result))
      ).subscribe();

    } else {
      const newValue = !this.adminProjectViewComponentData.project.allowedStatuses[status];
      this.adminProjectViewService.changeExecutionStatusOnProject(newValue, status);
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private openAlertForLockedParameter() {
    this.dialogService.openAlert({
      titleKey: 'sqtm-core.administration-workspace.projects.dialog.title.parameter-locked-because-project-is-bound-to-a-template',
      messageKey: 'sqtm-core.administration-workspace.projects.dialog.message.parameter-locked-because-project-is-bound-to-a-template',
      level: 'WARNING',
    });
  }

}

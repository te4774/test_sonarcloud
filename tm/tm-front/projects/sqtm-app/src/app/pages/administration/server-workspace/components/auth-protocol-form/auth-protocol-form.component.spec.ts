import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {AuthProtocolFormComponent} from './auth-protocol-form.component';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthConfiguration, AuthenticationProtocol, SignatureMethod} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('AuthProtocolFormComponent', () => {
  let component: AuthProtocolFormComponent;
  let fixture: ComponentFixture<AuthProtocolFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), ReactiveFormsModule],
      declarations: [AuthProtocolFormComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthProtocolFormComponent);
    component = fixture.componentInstance;
    component.authProtocol = AuthenticationProtocol.BASIC_AUTH;
    component.supportedAuthenticationProtocols = [AuthenticationProtocol.BASIC_AUTH, AuthenticationProtocol.OAUTH_1A];
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should create with configuration', waitForAsync(() => {
    component.authConfiguration = makeAuthConfiguration();
    fixture.detectChanges();

    expect(component).toBeTruthy();
    expect(component.formGroup.controls['accessTokenUrl'].value).toBe('accessTokenUrl');
    expect(component.formGroup.controls['userAuthorizationUrl'].value).toBe('userAuthorizationUrl');
  }));

  it('should throw if configuration type is unknown', waitForAsync(() => {
    component.authConfiguration = {
      type: 'WHAT?',
      toto: 'titi',
    } as unknown as AuthConfiguration;

    expect(() => fixture.detectChanges()).toThrow();
  }));

  it('should not send form if required fields are empty', waitForAsync(() => {
    spyOn(component.configurationChanged, 'emit');

    component.authConfiguration = {
      ...makeAuthConfiguration(),
      requestTokenUrl: '',
    };

    fixture.detectChanges();
    component.sendOAuthForm();

    expect(component.configurationChanged.emit).not.toHaveBeenCalled();
  }));

  it('should change status message when form is dirty', waitForAsync(() => {
    component.authConfiguration = makeAuthConfiguration();
    fixture.detectChanges();
    component.formGroup.markAsDirty();

    expect(component.authConfigurationStatusMessage).toBe('sqtm-core.administration-workspace.servers.label.unsaved-changes');
  }));

  it('should send form', waitForAsync(() => {
    spyOn(component.configurationChanged, 'emit');

    component.authConfiguration = makeAuthConfiguration();
    component.authProtocol = AuthenticationProtocol.OAUTH_1A;
    fixture.detectChanges();
    const textField = jasmine.createSpyObj(['showClientSideError']);
    component.textFields.reset([textField]);

    component.sendOAuthForm();

    expect(component.configurationChanged.emit).toHaveBeenCalled();
  }));

  it('should not send form if there are validation errors', waitForAsync(() => {
    spyOn(component.configurationChanged, 'emit');

    const textField = jasmine.createSpyObj(['showClientSideError']);
    fixture.detectChanges();
    component.textFields.reset([textField]);
    component.textAreaFields.reset([textField]);
    component.formGroup.setErrors([{required: true}]);

    component.sendOAuthForm();

    expect(component.configurationChanged.emit).not.toHaveBeenCalled();
    expect(textField.showClientSideError).toHaveBeenCalledTimes(2);
  }));

  it('should not send form if not OAuth', waitForAsync(() => {
    spyOn(component.configurationChanged, 'emit');

    component.authProtocol = AuthenticationProtocol.BASIC_AUTH;
    fixture.detectChanges();
    component.sendOAuthForm();

    expect(component.configurationChanged.emit).not.toHaveBeenCalled();
  }));

  it('should handle save success', waitForAsync(() => {
    fixture.detectChanges();
    spyOn(component.formGroup, 'markAsPristine');

    component.handleServerSuccess();

    expect(component.formGroup.markAsPristine).toHaveBeenCalled();
    expect(component['hasSaved']).toBeTruthy();
  }));

  it('should handle save error', waitForAsync(() => {
    fixture.detectChanges();

    component.handleServerError();

    expect(component['hasSaved']).toBeFalsy();
    expect(component['hasServerError']).toBeTruthy();
  }));

  it('should emit event when auth protocol changed', waitForAsync(() => {
    fixture.detectChanges();
    spyOn(component.authProtocolChanged, 'emit');

    component.confirmAuthProtocol({ value: 'OAUTH_1A', label: 'OAuth 1a'});

    expect(component.authProtocolChanged.emit).toHaveBeenCalled();
  }));
});


function makeAuthConfiguration(): AuthConfiguration {
  return {
    type: 'OAUTH_1A',
    accessTokenUrl: 'accessTokenUrl',
    accessTokenHttpMethod: 'GET',
    requestTokenUrl: 'requestTokenUrl',
    requestTokenHttpMethod: 'GET',
    userAuthorizationUrl: 'userAuthorizationUrl',
    clientSecret: 'clientSecret',
    signatureMethod: SignatureMethod.RSA_SHA1,
    implementedProtocol: 'OAUTH_1A',
    consumerKey: 'consumerKey',
  };
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DataRow,
  DialogService,
  Fixed,
  GridService,
  RestService
} from 'sqtm-core';
import {Subject} from 'rxjs';
import {concatMap, filter, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-bugtracker-cell-renderer',
  template: `
    <ng-container *ngIf="row">
      <div *ngIf="canDelete()"
           class="full-height full-width flex-column icon-container current-workspace-main-color"
           (click)="removeItem(row)">
        <i nz-icon [nzType]="getIcon()" nzTheme="outline" class="table-icon-size"></i>
      </div>
    </ng-container>
  `,
  styleUrls: ['./delete-bugtracker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteBugtrackerComponent extends AbstractCellRendererComponent implements OnInit, OnDestroy {

  @Input()
  row: DataRow;

  unsub$ = new Subject<void>();

  constructor(public grid: GridService, cdr: ChangeDetectorRef,
              private dialogService: DialogService, private restService: RestService) {
    super(grid, cdr);
  }

  ngOnInit() {
  }

  getIcon(): string {
    return 'sqtm-core-generic:delete';
  }

  get isUsedBySynchronization() {
    return this.row.data['synchronisationCount'] > 0;
  }

  removeItem(row: DataRow) {
    if (this.isUsedBySynchronization) {
      this.openBugtrackerIsUsedBySynchronisationAlert();
    } else {
      const dialogReference = this.dialogService.openDeletionConfirm({
        titleKey: 'sqtm-core.administration-workspace.servers.bugtrackers.dialog.title.delete-one',
        messageKey: 'sqtm-core.administration-workspace.servers.bugtrackers.dialog.message.delete-one',
        level: 'DANGER',
      });

      dialogReference.dialogClosed$
        .pipe(
          takeUntil(this.unsub$),
          filter(result => result === true),
          concatMap(() => this.restService.delete([`bugtracker/${row.data['serverId']}`]))
        )
        .subscribe(() => {
          this.grid.refreshData();
        });
    }
  }

  private openBugtrackerIsUsedBySynchronisationAlert() {
    this.dialogService.openAlert({
      titleKey: 'sqtm-core.administration-workspace.servers.bugtrackers.dialog.title.delete-one-used-by-synchronization',
      messageKey: 'sqtm-core.administration-workspace.servers.bugtrackers.dialog.message.delete-one-used-by-synchronization',
      level: 'DANGER',
    });
  }

  canDelete(): boolean {
    return true;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function deleteBugTrackerColumn(id: string, label = ''): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DeleteBugtrackerComponent)
    .withLabel(label)
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(50));
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {
  DataRow,
  DialogService,
  GenericEntityViewComponentData,
  GenericEntityViewService,
  GridService,
  RestService
} from 'sqtm-core';
import {TestAutomationServerViewService} from '../../services/test-automation-server-view.service';
import {Observable, Subject} from 'rxjs';
import {AdminTestAutomationServerViewState} from '../../states/admin-test-automation-server-view-state';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {concatMap, filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {AdminTestAutomationServerState} from '../../states/admin-test-automation-server-state';

@Component({
  selector: 'sqtm-app-test-automation-server-view',
  templateUrl: './test-automation-server-view.component.html',
  styleUrls: ['./test-automation-server-view.component.less'],
  providers: [
    {
      provide: TestAutomationServerViewService,
      useClass: TestAutomationServerViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: TestAutomationServerViewService
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestAutomationServerViewComponent implements OnInit, OnDestroy {

  componentData$: Observable<AdminTestAutomationServerViewState>;

  private readonly unsub$ = new Subject<void>();

  constructor(private readonly route: ActivatedRoute,
              public readonly testAutomationServerViewService: TestAutomationServerViewService,
              private readonly gridService: GridService,
              private cdRef: ChangeDetectorRef,
              private restService: RestService,
              private dialogService: DialogService) {
    this.componentData$ = testAutomationServerViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('testAutomationServerId')),
      ).subscribe((id) => {
        this.testAutomationServerViewService.load(parseInt(id, 10));
    });

    this.prepareGridRefreshOnEntityChanges();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.testAutomationServerViewService.complete();
  }

  handleDelete(): void {
    this.componentData$.pipe(
      take(1),
      withLatestFrom(this.gridService.selectedRows$),
      concatMap(([componentData, rows]: [AdminTestAutomationServerViewState, DataRow[]]) =>
        this.showConfirmDeleteTestAutomationServerDialog(componentData.testAutomationServer.id, rows)),
      filter(({confirmDelete}) => confirmDelete),
      concatMap(({testAutomationServerId}) => this.deleteTestAutomationServersServerSide(testAutomationServerId)),
    ).subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteTestAutomationServerDialog(testAutomationServerId, rows: DataRow[])
    : Observable<{ confirmDelete: boolean, testAutomationServerId: string }> {
    const serverHasAutomationProjectWithExecution = rows.filter(row => row.data['executionCount'] > 0).length > 0;

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.title.delete-one',
      messageKey: serverHasAutomationProjectWithExecution ?
        'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-one-with-bound-project' :
        'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-one-without-bound-project',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, testAutomationServerId}))
    );
  }

  private deleteTestAutomationServersServerSide(testAutomationServerId): Observable<void> {
    return this.restService.delete([`test-automation-servers`, testAutomationServerId]);
  }

  private prepareGridRefreshOnEntityChanges(): void {
    this.testAutomationServerViewService.simpleAttributeRequiringRefresh = [
      'name',
      'baseUrl',
    ];

    this.testAutomationServerViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
    ).subscribe(() =>
      this.gridService.refreshDataAndKeepSelectedRows());
  }
}

// tslint:disable-next-line:max-line-length
export interface AdminTestAutomationServerViewComponentData extends GenericEntityViewComponentData<AdminTestAutomationServerState, 'testAutomationServer'> {
}

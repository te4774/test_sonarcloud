import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewContainerRef,
  ViewChild, TemplateRef, ElementRef, OnDestroy
} from '@angular/core';
import {
  AbstractListCellRendererComponent, ActionErrorDisplayService, AlertDialogComponent, buildErrorI18nKeyProvider,
  ColumnDefinitionBuilder, DialogService, EditableTextFieldComponent, EnvironmentVariableOption, ErrorI18nKeyProvider,
  GridService, ListPanelItem, RestService
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {Overlay} from '@angular/cdk/overlay';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';
import {ActivatedRoute} from '@angular/router';
import {finalize, take, tap} from 'rxjs/operators';
import {Subject, timer} from 'rxjs';
import {ValidationErrors, Validators} from '@angular/forms';

@Component({
  selector: 'sqtm-app-ev-field-value-cell',
  templateUrl: './ev-field-value-cell.component.html',
  styleUrls: ['./ev-field-value-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EvFieldValueCellComponent extends AbstractListCellRendererComponent implements OnInit, OnDestroy {

  readonly unsub$ = new Subject<void>();

  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  @ViewChild('optionName', {read: ElementRef})
  optionNameElementRef: ElementRef;

  @ViewChild('editableTextFieldComponent')
  editableTextFieldComponent: EditableTextFieldComponent;

  panelItems: ListPanelItem[] = [];

  options: EnvironmentVariableOption[] = [];

  constructor(private readonly route: ActivatedRoute,
              public grid: GridService,
              public cdr: ChangeDetectorRef,
              public readonly translateService: TranslateService,
              public readonly overlay: Overlay,
              public readonly vcr: ViewContainerRef,
              public readonly restService: RestService,
              public readonly actionErrorDisplayService: ActionErrorDisplayService,
              private testAutomationServerViewService: TestAutomationServerViewService,
              protected dialogService: DialogService, ) {
    super(grid, cdr, overlay, vcr, translateService, restService, actionErrorDisplayService);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  ngOnInit(): void {
  }

  getValue() {
    return this.row.data['value'];
  }

  getValidators() {
    return [Validators.maxLength(255)];
  }

  change(valueChange: any) {
    const environmentVariableId = this.row.data['id'];
    this.grid.beginAsyncOperation();
    this.testAutomationServerViewService.setEnvironmentVariableValue(environmentVariableId, valueChange).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
    this.close();
  }

  cellText(): string {
    if (this.row.data['value'] === '') {
      return '-';
    }
    return this.row.data['value'];
  }

  getPanelItems(): ListPanelItem[] {
    const panel = this.row.data['options'].map((option) => {
        return {
          label: option.label,
          id: option.label
        };
      }
    );
    panel.unshift({
      label: this.translateService.instant('sqtm-core.generic.label.none.feminine'),
      id: ''
    });
    return panel;
  }

  formErrors(errors: any) {
    const clientSideErrors = this.generateClientSideErrors(errors);
    const i18nKey = clientSideErrors[0].provideI18nKey();

    this.editableTextFieldComponent.cancel();
    timer(1, 10).pipe(
      take(1),
      tap(() => {
        this.dialogService.openDialog({
          id: 'alert',
          component: AlertDialogComponent,
          viewContainerReference: this.vcr,
          data: {
            id: 'param-error',
            level: 'DANGER',
            titleKey: 'sqtm-core.generic.label.error',
            messageKey: i18nKey
          }
        });
      })
    ).subscribe();
  }

  private generateClientSideErrors(errors: ValidationErrors): ErrorI18nKeyProvider[] {
    return Object
      .entries(errors)
      .map(([fieldName, errorValue]: [string, any]) => buildErrorI18nKeyProvider(fieldName, errorValue));
  }

}

export function environmentVariableValueColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(EvFieldValueCellComponent);
}

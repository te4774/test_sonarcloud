import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminTeamViewComponentData} from '../../team-view/team-view.component';
import {takeUntil} from 'rxjs/operators';
import {TeamViewService} from '../../../services/team-view.service';
import {GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {
  TEAM_AUTHORISATIONS_TABLE,
  TEAM_AUTHORISATIONS_TABLE_CONF,
  TeamAuthorisationsPanelComponent,
  teamAuthorisationsTableDefinition
} from '../../../components/panels/team-authorisations-panel/team-authorisations-panel.component';
import {
  TEAM_MEMBERS_TABLE,
  TEAM_MEMBERS_TABLE_CONF,
  TeamMembersPanelComponent,
  teamMembersTableDefinition
} from '../../../components/panels/team-member-panel/team-members-panel.component';
import {UserLicenseInformationDialogService} from '../../../../../services/user-license-information-dialog.service';


@Component({
  selector: 'sqtm-app-team-content',
  templateUrl: './team-content.component.html',
  styleUrls: ['./team-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TEAM_AUTHORISATIONS_TABLE_CONF,
      useFactory: teamAuthorisationsTableDefinition
    },
    {
      provide: TEAM_AUTHORISATIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TEAM_AUTHORISATIONS_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: TEAM_MEMBERS_TABLE_CONF,
      useFactory: teamMembersTableDefinition
    },
    {
      provide: TEAM_MEMBERS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TEAM_MEMBERS_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: UserLicenseInformationDialogService,
      useClass: UserLicenseInformationDialogService,
    }
  ]
})
export class TeamContentComponent implements OnInit, OnDestroy {

  componentData$: Observable<AdminTeamViewComponentData>;

  unsub$ = new Subject<void>();

  @ViewChild(TeamAuthorisationsPanelComponent)
  private teamAuthorisationsPanelComponent;

  @ViewChild(TeamMembersPanelComponent)
  private teamMembersPanelComponent;

  constructor(public teamViewService: TeamViewService,
              @Inject(TEAM_AUTHORISATIONS_TABLE) public authorisationsGridService: GridService,
              @Inject(TEAM_MEMBERS_TABLE) public membersGridService: GridService,
              private readonly userLicenseInformationDialogService: UserLicenseInformationDialogService) { }

  ngOnInit(): void {
    this.componentData$ = this.teamViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
  }

  addTeamAuthorisation($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    if (this.userLicenseInformationDialogService.isUserCreationAllowed()) {
      this.teamAuthorisationsPanelComponent.openAddTeamAuthorisationDialog();
    } else {
      this.userLicenseInformationDialogService.openLicenseDialog();
    }
  }

  deleteAuthorisations($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.teamAuthorisationsPanelComponent.removeAuthorisations();
  }

  addTeamMember($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.teamMembersPanelComponent.openAddTeamMemberDialog();
  }

  removeUsersFromTeam($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.teamMembersPanelComponent.removeTeamMembers();
  }

  ngOnDestroy(): void {
    this.authorisationsGridService.complete();
    this.membersGridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

}

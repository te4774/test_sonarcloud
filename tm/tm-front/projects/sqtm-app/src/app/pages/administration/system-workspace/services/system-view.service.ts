import {Injectable} from '@angular/core';
import {AutomationDeletionCount, createStore, RestService, Store, SystemViewModel} from 'sqtm-core';
import {Observable} from 'rxjs';
import {SystemViewState} from '../states/system-view.state';
import {filter, map, take, withLatestFrom} from 'rxjs/operators';
import decamelize from 'decamelize';

@Injectable()
export class SystemViewService {

  public componentData$: Observable<SystemViewState>;

  private readonly store: Store<SystemViewState>;

  public readonly rootUrl = 'system';

  constructor(protected restService: RestService) {
    this.store = createStore<SystemViewState>(SystemViewService.getInitialState());
    this.componentData$ = this.store.state$.pipe(
      filter((state) => Boolean(state) && Boolean(state.appVersion))
    );
  }

  load(): void {
    this.restService.get<SystemViewModel>(['system-view'])
      .subscribe((model) => this.commit(model));
  }

  retrieveCurrentActiveUsersCount(): Observable<void> {
    return this.restService.get(['system-view', 'current-active-users-count']).pipe(
      take(1),
      withLatestFrom(this.store.state$),
      map(([response, state]: [any, SystemViewState]) => this.updateCurrentActiveUsersCountState(response.currentActiveUsersCount, state)),
      map(state => this.store.commit(state)));
  }

  private updateCurrentActiveUsersCountState(currentActiveUsersCount: number, state: SystemViewState): SystemViewState {
    return {
      ...state,
      currentActiveUsersCount
    };
  }


  changeWhiteList(whiteList: string): Observable<SystemViewState> {
    return this.update('whiteList', whiteList, ['settings']);
  }

  changeUploadSizeLimit(size: string): Observable<SystemViewState> {
    return this.update('uploadSizeLimit', size, ['settings']);
  }

  changeImportSizeLimit(size: string): Observable<SystemViewState> {
    return this.update('importSizeLimit', size, ['settings']);
  }

  changeCallbackUrl(callbackUrl: string): Observable<any> {
    return this.update('callbackUrl', callbackUrl, ['settings']);
  }

  setLoginMessage(message: string): Observable<SystemViewState> {
    return this.update('loginMessage', message, ['messages']);
  }

  setWelcomeMessage(message: string): Observable<SystemViewState> {
    return this.update('welcomeMessage', message, ['messages']);
  }

  changeStackTraceFeatureEnabled(enabled: boolean): void {
    this.updateFeature('stackTraceFeatureIsEnabled', 'stack-trace', enabled);
  }

  changeCaseInsensitiveLoginEnabled(enabled: boolean): void {
    this.updateFeature('caseInsensitiveLogin', 'case-insensitive-login', enabled);
  }

  changeCaseInsensitiveActionsEnabled(enabled: boolean): void {
    this.updateFeature('caseInsensitiveActions', 'case-insensitive-actions', enabled);
  }

  changeAutoconnectOnConnection(enabled: boolean): void {
    this.updateFeature('autoconnectOnConnection', 'autoconnect-on-connection', enabled);
  }

  getOldAutomatedSuitesAndExecutionsCount(): Observable<AutomationDeletionCount> {
    return this.restService.get<AutomationDeletionCount>(['cleaning/count']);
  }

  cleanAutomatedSuitesAndExecutions(): Observable<void> {
    return this.restService.post(['cleaning']);
  }

  private static getInitialState(): SystemViewState {
    return {
      appVersion: null,
      statistics: null,
      plugins: null,
      whiteList: '',
      uploadSizeLimit: '',
      importSizeLimit: '',
      callbackUrl: '',
      stackTracePanelIsVisible: false,
      stackTraceFeatureIsEnabled: false,
      caseInsensitiveLogin: false,
      caseInsensitiveActions: false,
      duplicateLogins: [],
      duplicateActions: [],
      welcomeMessage: '',
      loginMessage: '',
      logFiles: [],
      autoconnectOnConnection: false,
      licenseInfo: null,
      currentActiveUsersCount: 0
    };
  }

  private commit(state: SystemViewState) {
    this.store.commit(state);
  }

  private update(fieldName: string, value: any, urlParts: string[] = []): Observable<SystemViewState> {
    const body = {[fieldName]: value};
    const snakeCased = decamelize(fieldName, '-');
    return this.restService.post([this.rootUrl, ...urlParts, snakeCased], body).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]) => {
        const updatedState: SystemViewState = {...state};
        updatedState[fieldName] = value;
        this.commit(updatedState);
        return updatedState;
      })
    );
  }

  private updateFeature(propertyName: keyof SystemViewState, urlPart: string, value: boolean) {
    const options = {params: {'enabled': value.toString()}};
    return this.restService.post(['features', urlPart], {}, options).pipe(
      take(1),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, SystemViewState]) => ({...state, [propertyName]: value}))
    ).subscribe(state => {
      this.store.commit(state);
    });
  }
}

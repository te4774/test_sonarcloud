import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {SystemViewState} from '../../../states/system-view.state';

@Component({
  selector: 'sqtm-app-system-plugins-panel',
  templateUrl: './system-plugins-panel.component.html',
  styleUrls: ['./system-plugins-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemPluginsPanelComponent implements OnInit {

  @Input() componentData: SystemViewState;

  get plugins(): string[] {
    const plugins = this.componentData.plugins;

    if (plugins == null) {
      return [];
    }

    plugins.sort();
    return plugins;
  }

  constructor() { }

  ngOnInit(): void {
  }

}

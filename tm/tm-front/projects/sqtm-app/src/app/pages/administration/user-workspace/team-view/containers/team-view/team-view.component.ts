import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminReferentialDataService, DialogService, GenericEntityViewComponentData, RestService} from 'sqtm-core';
import {AdminTeamState} from '../../states/admin-team-state';
import {TeamViewService} from '../../services/team-view.service';
import {concatMap, filter, map, take, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-team-view',
  templateUrl: './team-view.component.html',
  styleUrls: ['./team-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeamViewComponent implements OnInit, OnDestroy {

  public componentData$: Observable<AdminTeamViewComponentData>;
  private unsub$ = new Subject<void>();
  public loggedAsAdmin$: Observable<boolean>;

  constructor(public teamViewService: TeamViewService,
              private cdRef: ChangeDetectorRef,
              private restService: RestService,
              private dialogService: DialogService,
              private adminReferentialDataService: AdminReferentialDataService) {
  }

  @Output()
  teamDeleted = new EventEmitter<void>();

  ngOnInit(): void {
    this.componentData$ = this.teamViewService.componentData$;
    this.loggedAsAdmin$ = this.adminReferentialDataService.loggedAsAdmin$;
  }

  ngOnDestroy(): void {
    this.teamViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  handleDelete() {
    this.componentData$.pipe(
      take(1),
      concatMap((componentData: AdminTeamViewComponentData) => this.showConfirmDeleteTeamDialog(componentData.team.id)),
      filter(({confirmDelete}) => confirmDelete),
      concatMap(({teamId}) => this.deleteTeamServerSide(teamId)),
    ).subscribe(() => this.teamDeleted.emit());
  }

  private showConfirmDeleteTeamDialog(teamId): Observable<{ confirmDelete: boolean, teamId: string }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.teams.dialog.title.delete-one',
      messageKey: 'sqtm-core.administration-workspace.teams.dialog.message.delete-one',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, teamId}))
    );
  }

  private deleteTeamServerSide(teamId): Observable<void> {
    return this.restService.delete(['teams', teamId]);
  }
}

export interface AdminTeamViewComponentData extends GenericEntityViewComponentData<AdminTeamState, 'team'> {
}

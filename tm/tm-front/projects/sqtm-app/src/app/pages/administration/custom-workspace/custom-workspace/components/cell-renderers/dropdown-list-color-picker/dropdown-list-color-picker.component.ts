import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';
import {DropdownListOptionService} from '../../../services/dropdown-list-option.service';

@Component({
  selector: 'sqtm-app-dropdown-list-option-color-picker-cell',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column" style="justify-content: center;">
        <sqtm-core-color-picker-select-field
          [color]="color"
          (colorChanged)="changeColor($event)">
        </sqtm-core-color-picker-select-field>
      </div>
    </ng-container>`,
  styleUrls: ['./dropdown-list-color-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropdownListColorPickerComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              private dropdownListOptionService: DropdownListOptionService) {
    super(grid, cdRef);
  }

  get color(): string {
    return this.row.data[this.columnDisplay.id];
  }

  changeColor(newColor: string) {
    this.dropdownListOptionService.changeColor(this.row.data['optionName'], newColor);
  }
}

export function dropdownListOptionColorPickerColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DropdownListColorPickerComponent)
    .withHeaderPosition('center');
}

import {Component, OnInit, ChangeDetectionStrategy, OnDestroy} from '@angular/core';
import {Observable} from 'rxjs';
import {
  AdminReferentialDataService,
  AuthenticatedUser, GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {
  ADMIN_WS_ENVIRONMENT_VARIABLES_TABLE,
  ADMIN_WS_ENVIRONMENT_VARIABLES_TABLE_CONFIG
} from '../../../custom-workspace.constant';
import {
  adminEnvironmentVariableTableDefinition
} from '../environment-variable-grid/environment-variable-grid.component';

@Component({
  selector: 'sqtm-app-environment-variable-workspace',
  templateUrl: './environment-variable-workspace.component.html',
  styleUrls: ['./environment-variable-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide : ADMIN_WS_ENVIRONMENT_VARIABLES_TABLE_CONFIG,
      useFactory: adminEnvironmentVariableTableDefinition,
      deps: []
    },
    {
      provide : ADMIN_WS_ENVIRONMENT_VARIABLES_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_ENVIRONMENT_VARIABLES_TABLE_CONFIG, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_ENVIRONMENT_VARIABLES_TABLE
    }
  ]
})
export class EnvironmentVariableWorkspaceComponent implements OnInit, OnDestroy {
  authenticatedAdmin$: Observable<AuthenticatedUser>;

  constructor(public readonly adminReferentialDataService: AdminReferentialDataService,
              private gridService: GridService) { }

  ngOnInit(): void {
    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$;
  }

  ngOnDestroy(): void {
    this.gridService.complete();
  }
}

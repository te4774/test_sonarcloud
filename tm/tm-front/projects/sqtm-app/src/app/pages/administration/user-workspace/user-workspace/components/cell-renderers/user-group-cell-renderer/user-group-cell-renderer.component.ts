import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  GridService,
  UserGroupExportValueRenderer,
  UsersGroupHelpers
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-group-cell-renderer',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
            <span *ngIf="row.data[columnDisplay.id]" style="margin: auto 5px;" class="sqtm-grid-cell-txt-renderer"
                  [class.disabled-row]="row.disabled"
                  nz-tooltip [sqtmCoreLabelTooltip]="groupName | translate">
              {{groupName  | translate}}
            </span>
      </div>
    </ng-container>
  `,
  styleUrls: ['./user-group-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserGroupCellRendererComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  get groupName(): string {
    const groupClassName = this.row.data[this.columnDisplay.id];
    return UsersGroupHelpers.getI18nKeyFromQualifiedName(groupClassName);
  }

}

export function userGroupColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(UserGroupCellRendererComponent)
    .withExportValueRenderer(UserGroupExportValueRenderer);
}

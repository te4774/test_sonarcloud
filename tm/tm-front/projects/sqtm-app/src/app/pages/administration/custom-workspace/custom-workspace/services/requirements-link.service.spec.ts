import {TestBed} from '@angular/core/testing';

import {RequirementsLinkService} from './requirements-link.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {RouterTestingModule} from '@angular/router/testing';

describe('RequirementsLinkService', () => {
  let service: RequirementsLinkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, RouterTestingModule],
      providers: [
        {
          provide: RequirementsLinkService,
          useClass: RequirementsLinkService
        },
      ]
    });
    service = TestBed.inject(RequirementsLinkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

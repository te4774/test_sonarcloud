import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, ViewChild, ViewContainerRef} from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  DataRow,
  dateColumn,
  dateTimeColumn,
  deleteColumn,
  DialogService,
  Extendable,
  FilterOperation,
  Fixed,
  grid,
  GridDefinition,
  GridService,
  GridWithStatePersistence,
  indexColumn,
  RestService,
  selectableTextColumn,
  textColumn,
  TextResearchFieldComponent,
  WorkspaceWithGridComponent,
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {
  TeamCreationDialogComponent
} from '../../components/dialogs/team-creation-dialog/team-creation-dialog.component';
import {concatMap, filter, map, take, takeUntil, tap} from 'rxjs/operators';
import {
  DeleteTeamCellRendererComponent
} from '../../components/cell-renderers/delete-team-cell-renderer/delete-team-cell-renderer.component';
import {AbstractAdministrationNavigation} from "../../../../components/abstract-administration-navigation";
import {ActivatedRoute, Router} from "@angular/router";


export function adminTeamTableDefinition(): GridDefinition {
  return grid('teams')
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Fixed(60))
        .withViewport('leftViewport'),
      selectableTextColumn('name')
        .withI18nKey('sqtm-core.entity.team.name.label')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2)),
      textColumn('description')
        .disableSort()
        .withI18nKey('sqtm-core.entity.team.description.label')
        .changeWidthCalculationStrategy(new Extendable(60, 0.2)),
      textColumn('teamMembersCount')
        .withI18nKey('sqtm-core.entity.team.team-members-count.short')
        .withTitleI18nKey('sqtm-core.entity.team.team-members-count.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1))
        .withHeaderPosition('center')
        .withContentPosition('center'),
      dateTimeColumn('createdOn')
        .withI18nKey('sqtm-core.entity.generic.created-on.feminine')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      textColumn('createdBy')
        .withI18nKey('sqtm-core.entity.generic.created-by.feminine')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      dateColumn('lastModifiedOn')
        .withI18nKey('sqtm-core.entity.generic.last-modified-on.feminine')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      textColumn('lastModifiedBy')
        .withI18nKey('sqtm-core.entity.generic.last-modified-by.feminine')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      deleteColumn(DeleteTeamCellRendererComponent)
    ]).server().withServerUrl(['teams'])
    .disableRightToolBar()
    .withRowHeight(35)
    .enableMultipleColumnsFiltering(['name', 'lastModifiedBy', 'createdBy'])
    .build();
}

@Component({
  selector: 'sqtm-app-team-grid',
  templateUrl: './team-grid.component.html',
  styleUrls: ['./team-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeamGridComponent extends AbstractAdministrationNavigation implements AfterViewInit, OnDestroy {

  authenticatedUser$: Observable<AuthenticatedUser>;

  unsub$ = new Subject<void>();

  protected readonly entityIdPositionInUrl = 3;

  @ViewChild(TextResearchFieldComponent)
  searchField: TextResearchFieldComponent;

  constructor(public gridService: GridService,
              private restService: RestService,
              private dialogService: DialogService,
              private adminReferentialDataService: AdminReferentialDataService,
              private viewContainerRef: ViewContainerRef,
              private workspaceWithGrid: WorkspaceWithGridComponent,
              private gridWithStatePersistence: GridWithStatePersistence,
              protected route: ActivatedRoute,
              protected router: Router) {
    super(route, router);
    this.workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngAfterViewInit() {
    this.addFilters();
    this.gridWithStatePersistence.popGridState().subscribe((snapshot) => {
      GridWithStatePersistence.updateMultipleColumnSearchField(snapshot, this.searchField);
      this.gridService.refreshData();
    });
  }

  private addFilters() {
    this.gridService.addFilters([{
      id: 'name',
      active: false,
      initialValue: {kind: 'single-string-value', value: ''},
      tiedToPerimeter: false,
      operation: FilterOperation.LIKE
    }, {
      id: 'lastModifiedBy',
      active: false,
      initialValue: {kind: 'single-string-value', value: ''},
      tiedToPerimeter: false,
      operation: FilterOperation.LIKE
    }, {
      id: 'createdBy', active: false,
      initialValue: {kind: 'single-string-value', value: ''},
      tiedToPerimeter: false,
      operation: FilterOperation.LIKE
    }]);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openTeamDialog() {
    const dialogReference = this.dialogService.openDialog({
      component: TeamCreationDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.teams.dialog.title.new-team',
        addAnotherLabelKey: 'sqtm-core.generic.label.add-another.feminine'
      },
      id: 'team-dialog',
      width: 600
    });

    dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      filter(result => result != null),
      concatMap((result: any) => this.gridService.refreshDataAsync().pipe(map(() => result.id))),
      tap((id: string) => super.navigateToNewEntity(id))
    ).subscribe();
  }

  deleteTeams($event: MouseEvent) {
    $event.stopPropagation();

    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      concatMap((rows: DataRow[]) => this.showConfirmDeleteTeamDialog(rows)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({rows}) => this.deleteTeamsServerSide(rows)),
      tap(() => this.gridService.completeAsyncOperation())
    ).subscribe(() => this.gridService.refreshData());
  }

  private deleteTeamsServerSide(rows): Observable<void> {
    const pathVariable = rows.map(row => row.data['partyId']).join(',');
    return this.restService.delete([`teams`, pathVariable]);
  }

  private showConfirmDeleteTeamDialog(rows): Observable<{ confirmDelete: boolean, rows: string[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.teams.dialog.title.delete-many',
      messageKey: 'sqtm-core.administration-workspace.teams.dialog.message.delete-many',
      level: 'DANGER',
    });
    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, rows}))
    );
  }

  filterTeams($event: any) {
    this.gridService.applyMultiColumnsFilter($event);
  }
}

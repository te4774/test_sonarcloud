import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {DialogReference, GridService, GroupedMultiListFieldComponent, ListItem, RestService} from 'sqtm-core';
import {UserViewService} from '../../../services/user-view.service';
import {finalize, switchMap, take} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-associate-team-to-user-dialog',
  templateUrl: './associate-team-to-user-dialog.component.html',
  styleUrls: ['./associate-team-to-user-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AssociateTeamToUserDialogComponent implements AfterViewInit {

  listItems: ListItem[] = [];
  errorsOnMultiListField: string[] = [];

  @ViewChild(GroupedMultiListFieldComponent)
  teamsList: GroupedMultiListFieldComponent;

  constructor(private dialogReference: DialogReference,
              private restService: RestService,
              private cdr: ChangeDetectorRef,
              private userViewService: UserViewService,
              private grid: GridService) {
  }

  ngAfterViewInit(): void {
    this.prepareTeamsMultiListField();
  }

  selectedTeamsChanged($event: ListItem[]) {
    this.teamsList.selectedItems = $event;
  }

  confirm() {
    if (this.teamsList.selectedItems.length === 0) {
      const requiredKey = 'sqtm-core.validation.errors.required';
      this.errorsOnMultiListField.push(requiredKey);
    } else {
      this.doPost();
    }
  }

  private prepareTeamsMultiListField() {
    this.userViewService.componentData$.pipe(
      take(1),
      switchMap(componentData => {
        const userId = componentData.user.id.toString();

        return this.restService.get<TeamAssociation[]>(
          ['user-view', userId, 'unassociated-teams']);
      })
    ).subscribe((response) => {
      const teams = this.retrieveTeamsAsListItem(response);

      teams.sort((partyA, partyB) => partyA.label.localeCompare(partyB.label));

      this.listItems = [...teams];
      this.cdr.detectChanges();
    });
  }

  private retrieveTeamsAsListItem(response: TeamAssociation[]): ListItem[] {
    return response.map(team => {
      return {
        id: team.partyId,
        label: team.name,
        selected: false,
      };
    });
  }

  private getSelectedTeamIds(): number[] {
    return this.teamsList.selectedItems.map(team => Number(team.id));
  }

  private doPost() {
    this.grid.beginAsyncOperation();
    this.userViewService.associateTeams(this.getSelectedTeamIds()).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
    this.dialogReference.result = true;
    this.dialogReference.close();
  }
}

interface TeamAssociation {
  partyId: string;
  name: string;
}


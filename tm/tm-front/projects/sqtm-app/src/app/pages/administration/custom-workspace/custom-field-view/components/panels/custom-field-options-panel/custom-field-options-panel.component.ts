import {ChangeDetectionStrategy, Component, InjectionToken, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {
  ActionErrorDisplayService,
  DataRow,
  DefaultGridDraggedContentComponent,
  deleteColumn,
  DialogService,
  Extendable,
  Fixed,
  GenericDataRow,
  GridDefinition,
  GridService,
  indexColumn,
  InputType,
  smallGrid,
  StyleDefinitionBuilder,
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {AdminCustomFieldViewComponentData} from '../../../containers/custom-field-view/custom-field-view.component';
import {CustomFieldViewService, getCustomFieldViewState} from '../../../services/custom-field-view.service';
import {catchError, concatMap, filter, finalize, map, take, takeUntil, tap} from 'rxjs/operators';
import {createSelector, select} from '@ngrx/store';
import {AdminCustomFieldState} from '../../../states/admin-custom-field-state';
import {optionLabelColumn} from '../../cell-renderers/option-label-cell/option-label-cell.component';
import {optionCodeColumn} from '../../cell-renderers/option-code-cell/option-code-cell.component';
import {optionColorColumn} from '../../cell-renderers/option-color-cell/option-color-cell.component';
import {optionDefaultColumn} from '../../cell-renderers/option-default-cell/option-default-cell.component';
import {RemoveOptionCellComponent} from '../../cell-renderers/remove-option-cell/remove-option-cell.component';
import {
  AddCustomFieldOptionDialogComponent
} from '../../dialogs/add-custom-field-option-dialog/add-custom-field-option-dialog.component';
import {DummyPermissions} from '../../../../../services/dummy-permissions';

export const OPTIONS_TABLE_CONF = new InjectionToken('OPTIONS_TABLE_CONF');
export const OPTIONS_TABLE = new InjectionToken('OPTIONS_TABLE');

function optionDataRowFactory(literals: Partial<DataRow>[]): GenericDataRow[] {
  return literals.map(literal => {
    const row = new GenericDataRow();
    row.data = literal.data;
    row.id = literal.data.label;
    row.simplePermissions = new DummyPermissions();

    // This is a way to set the DefaultGridDraggedContent text.
    row.data['NAME'] = literal.data.label;
    return row;
  });
}

export function optionsTableDefinition(): GridDefinition {
  return smallGrid('options')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      optionLabelColumn('label')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5))
        .disableSort(),
      optionCodeColumn('code')
        .withI18nKey('sqtm-core.generic.label.code')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5))
        .disableSort(),
      optionColorColumn('colour')
        .withI18nKey('sqtm-core.entity.generic.color.label')
        .changeWidthCalculationStrategy(new Fixed(100))
        .withHeaderPosition('center')
        .disableSort(),
      optionDefaultColumn('default')
        .withI18nKey('sqtm-core.generic.label.default')
        .changeWidthCalculationStrategy(new Fixed(70))
        .withHeaderPosition('center')
        .disableSort(),
      deleteColumn(RemoveOptionCellComponent)
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .enableInternalDrop()
    .enableDrag()
    .withDraggedContentRenderer(DefaultGridDraggedContentComponent)
    .withRowConverter(optionDataRowFactory)
    .build();
}

@Component({
  selector: 'sqtm-app-custom-field-options-panel',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./custom-field-options-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: OPTIONS_TABLE
    }
  ]
})
export class CustomFieldOptionsPanelComponent implements OnInit, OnDestroy {

  private componentData$: Observable<AdminCustomFieldViewComponentData>;
  private unsub$ = new Subject<void>();

  constructor(private customFieldViewService: CustomFieldViewService,
              public gridService: GridService,
              private dialogService: DialogService,
              private vcRef: ViewContainerRef,
              private actionErrorDisplayService: ActionErrorDisplayService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.customFieldViewService.componentData$;
    this.initializeTable();
  }

  openAddCustomFieldOptionDialog(): void {
    this.dialogService.openDialog({
      id: 'add-custom-field-option-dialog',
      component: AddCustomFieldOptionDialogComponent,
      viewContainerReference: this.vcRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.custom-fields.dialog.title.add-option',
        addAnotherLabelKey: 'sqtm-core.generic.label.add-another.feminine'
      },
      width: 720
    });
  }

  deleteCustomFieldOptions() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.data.label)),
      concatMap((optionLabels: string[]) => this.showConfirmDeleteOptionsDialog(optionLabels)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({optionLabels}) => this.customFieldViewService.deleteOptions(optionLabels)),
      catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      finalize(() => this.gridService.completeAsyncOperation())
    ).subscribe();
  }

  private showConfirmDeleteOptionsDialog(optionLabels): Observable<{ confirmDelete: boolean, optionLabels: string[] }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.entities-customization.generic.dialog.option.title.delete-many',
      messageKey: 'sqtm-core.administration-workspace.custom-fields.dialog.message.delete-option.delete-many',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, optionLabels}))
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initializeTable() {
    const optionsTable = this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createSelector(getCustomFieldViewState, (cufState: AdminCustomFieldState) => {
        if (cufState.inputType === InputType.DROPDOWN_LIST) {
          return cufState.options;
        } else {
          return [];
        }
      })),
    );
    this.gridService.connectToDatasource(optionsTable, 'label');
  }
}

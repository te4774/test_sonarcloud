import {ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminProjectViewComponentData} from '../../../containers/project-view/project-view.component';
import {ProjectViewService} from '../../../services/project-view.service';
import {
  booleanColumn,
  dateColumn,
  deleteColumn,
  DialogService,
  Extendable,
  Fixed,
  formatMilestoneOwner,
  grid,
  GridDefinition,
  GridService,
  indexColumn,
  Milestone,
  MilestoneRange,
  RestService,
  Sort,
  StyleDefinitionBuilder,
  textColumn,
  withLinkColumn,
} from 'sqtm-core';
import {filter, finalize, map, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {
  milestoneRangeColumn
} from '../../../../../milestone-workspace/milestone-workspace/components/cell-renderers/milestone-range-cell-renderer/milestone-range-cell-renderer.component';
import {
  milestoneStatusColumn
} from '../../../../../milestone-workspace/milestone-workspace/components/cell-renderers/milestone-status-cell-renderer/milestone-status-cell-renderer.component';
import {
  CreateBindMilestoneDialogComponent
} from '../../dialogs/create-bind-milestone-dialog/create-bind-milestone-dialog.component';
import {
  milestoneOwnerColumn
} from '../../../../../milestone-workspace/milestone-workspace/components/cell-renderers/milestone-owner-cell-renderer/milestone-owner-cell-renderer.component';
import {BindMilestoneDialogComponent} from '../../dialogs/bind-milestone-dialog/bind-milestone-dialog.component';
import {
  UnbindMilestoneFromProjectCellComponent
} from '../../cell-renderers/unbind-milestone-from-project-cell/unbind-milestone-from-project-cell.component';
import {PROJECT_MILESTONES_TABLE} from '../../../project-view.constant';
import {TranslateService} from '@ngx-translate/core';

export function projectMilestonesTableDefinition(): GridDefinition {
  return grid('project-milestones')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      withLinkColumn('label', {
        kind: 'link',
        baseUrl: '/administration-workspace/milestones/detail',
        columnParamId: 'id',
        saveGridStateBeforeNavigate: true
      })
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      milestoneStatusColumn('status')
        .withI18nKey('sqtm-core.entity.milestone.status.label')
        .changeWidthCalculationStrategy(new Fixed(160)),
      dateColumn('endDate')
        .withI18nKey('sqtm-core.entity.milestone.end-date.label')
        .changeWidthCalculationStrategy(new Fixed(140)),
      milestoneRangeColumn('range')
        .withI18nKey('sqtm-core.entity.milestone.range.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      milestoneOwnerColumn('userSortColumn')
        .withI18nKey('sqtm-core.entity.milestone.owner.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      textColumn('description')
        .disableSort()
        .withI18nKey('sqtm-core.entity.milestone.description')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      booleanColumn('milestoneBoundToOneObjectOfProject')
        .withI18nKey('sqtm-core.entity.milestone.used')
        .changeWidthCalculationStrategy(new Fixed(180)),
      deleteColumn(UnbindMilestoneFromProjectCellComponent)
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .disableRightToolBar()
    .withRowHeight(35)
    .withInitialSortedColumns([{id: 'endDate', sort: Sort.DESC}])
    .build();
}

@Component({
  selector: 'sqtm-app-project-milestones-panel',
  template: `
    <sqtm-core-grid></sqtm-core-grid>`,
  styleUrls: ['./project-milestones-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: PROJECT_MILESTONES_TABLE
    }
  ]
})
export class ProjectMilestonesPanelComponent implements OnInit, OnDestroy {

  private componentData$: Observable<AdminProjectViewComponentData>;

  private unsub$ = new Subject<void>();

  constructor(public projectViewService: ProjectViewService,
              private gridService: GridService,
              private dialogService: DialogService,
              private restService: RestService,
              private vcRef: ViewContainerRef,
              private readonly translateService: TranslateService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.projectViewService.componentData$;
    this.initializeTable();
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  openBindMilestonesDialog() {
    this.componentData$.pipe(
      take(1),
      switchMap(componentData => this.doOpenBindMilestonesDialog(componentData)),
      filter(selectedMilestones => Boolean(selectedMilestones) && selectedMilestones.length > 0),
    ).subscribe((selectedMilestones: Milestone[]) => {
      this.projectViewService.bindMilestonesToProject(selectedMilestones);
    });
  }

  private doOpenBindMilestonesDialog(componentData: AdminProjectViewComponentData): Observable<Milestone[]> {
    const dialogRef = this.dialogService.openDialog<any, Milestone[]>({
      id: 'bind-milestones-dialog',
      component: BindMilestoneDialogComponent,
      viewContainerReference: this.vcRef,
      data: {
        titleKey: 'sqtm-core.dialog.milestone.picker.single',
        projectId: componentData.project.id
      },
      width: 720
    });

    return dialogRef.dialogClosed$;
  }

  openCreateBindMilestonesDialog() {
    this.componentData$.pipe(
      take(1),
      switchMap((componentData) => this.doOpenCreateBindMilestonesDialog(componentData)),
      filter((result) => Boolean(result))
    ).subscribe((result: any) => {
      this.projectViewService.updateBoundMilestoneInformation(result['milestone']);
    });
  }

  private doOpenCreateBindMilestonesDialog(componentData: AdminProjectViewComponentData): Observable<Milestone[]> {
    const dialogRef = this.dialogService.openDialog<any, Milestone[]>({
      id: 'create-bind-milestone-dialog',
      component: CreateBindMilestoneDialogComponent,
      viewContainerReference: this.vcRef,
      data: {
        projectId: componentData.project.id
      },
      width: 720
    });

    return dialogRef.dialogClosed$;
  }

  openUnbindMilestonesDialog() {
    this.gridService.selectedRowIds$.pipe(
      take(1),
    ).subscribe((milestoneBindingIds: number[]) => {
      const dialogReference = this.dialogService.openDeletionConfirm({
        titleKey: 'sqtm-core.administration-workspace.projects.dialog.title.unbind-milestone-from-project.unbind-many',
        messageKey: 'sqtm-core.administration-workspace.projects.dialog.message.unbind-milestone-from-project.unbind-many',
        level: 'WARNING',
      });

      dialogReference.dialogClosed$.pipe(
        take(1),
        filter(result => Boolean(result)),
        tap(() => this.gridService.beginAsyncOperation()),
        switchMap(() => this.projectViewService.unbindMilestones(milestoneBindingIds)),
        finalize(() => this.gridService.completeAsyncOperation())
      ).subscribe();
    });
  }

  private initializeTable() {
    const milestonesTable = this.componentData$.pipe(
      takeUntil(this.unsub$),
      map((componentData: AdminProjectViewComponentData) => {
        return componentData.project.boundMilestonesInformation.map(item => {
          return {...item.milestone, milestoneBoundToOneObjectOfProject: item.milestoneBoundToOneObjectOfProject};
        });
      }),
      map((rows: Milestone[]) => this.appendUserSortColumn(rows)),
    );

    this.gridService.connectToDatasource(milestonesTable, 'id');
  }


  private appendUserSortColumn(rows: Milestone[]): Milestone[] {
    return rows.map((milestone) => ({
        ...milestone,
        userSortColumn: this.getUserSortColumn(milestone),
      }
    ));
  }

  private getUserSortColumn(milestone: Milestone): string {
    const rangePrefix = (milestone.range === MilestoneRange.GLOBAL.id ? '0 ' : '1 ');
    return rangePrefix + this.translateService.instant(formatMilestoneOwner(milestone));
  }

}

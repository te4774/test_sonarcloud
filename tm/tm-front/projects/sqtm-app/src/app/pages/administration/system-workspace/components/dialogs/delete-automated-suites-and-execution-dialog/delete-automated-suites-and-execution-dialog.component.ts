import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {DeleteAutomatedSuitesAndExecutionConfiguration} from './delete-automated-suites-and-execution-configuration';
import {DialogReference} from 'sqtm-core';
import {SystemViewService} from '../../../services/system-view.service';

@Component({
  selector: 'sqtm-app-delete-automated-suites-and-execution-dialog',
  templateUrl: './delete-automated-suites-and-execution-dialog.component.html',
  styleUrls: ['./delete-automated-suites-and-execution-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteAutomatedSuitesAndExecutionDialogComponent implements OnInit {

  configuration: DeleteAutomatedSuitesAndExecutionConfiguration;

  loading = false;

  constructor(private dialogReference: DialogReference<DeleteAutomatedSuitesAndExecutionConfiguration, boolean>,
              private systemViewService: SystemViewService) {
    this.configuration = this.dialogReference.data;
  }

  ngOnInit(): void {
  }

  confirm() {
    this.loading = true;
    this.systemViewService.cleanAutomatedSuitesAndExecutions().pipe(
    ).subscribe(() =>  this.dialogReference.close(), err => this.handleError(err));
  }

  private handleError(err: any) {
    console.error(err);
  }
}

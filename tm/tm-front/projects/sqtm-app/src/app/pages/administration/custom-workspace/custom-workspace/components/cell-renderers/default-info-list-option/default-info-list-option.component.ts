import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';
import {InfoListOptionService} from '../../../services/info-list-option.service';

@Component({
  selector: 'sqtm-app-default-info-list-option-cell-renderer',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
        <label nz-radio style="margin: auto"
               [ngModel]="isChecked"
               (click)="handleClick()">
        </label>
      </div>
    </ng-container>`,
  styleUrls: ['./default-info-list-option.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefaultInfoListOptionComponent extends AbstractCellRendererComponent {


  get isChecked(): boolean {
    return Boolean(this.row.data[this.columnDisplay.id]);
  }

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              private infoListOptionService: InfoListOptionService) {
    super(grid, cdRef);
  }

  handleClick() {
    if (!this.isChecked) {
      this.infoListOptionService.toggleDefault(this.row.data['label']);
    }
  }
}


export function defaultInfoListOptionColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DefaultInfoListOptionComponent);
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  AdminReferentialDataService,
  AuthenticatedUser,
  ConfirmDeleteLevel,
  DialogService,
  GridService,
  RestService
} from 'sqtm-core';
import {finalize} from 'rxjs/operators';
import {MilestoneRowFields} from '../../../containers/milestone-workspace-grid/milestone-workspace-grid.constants';


@Component({
  selector: 'sqtm-app-delete-milestone-cell-renderer',
  template: `
    <ng-container *ngIf="adminReferentialDataService.authenticatedUser$ |async as authenticatedUser">
      <sqtm-core-delete-icon [show]="isAdminOrOwner(authenticatedUser)" (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
    </ng-container>
  `,
  styleUrls: ['./delete-milestone-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteMilestoneCellRendererComponent extends AbstractDeleteCellRenderer implements OnDestroy {

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private restService: RestService,
              public adminReferentialDataService: AdminReferentialDataService) {
    super(grid, cdr, dialogService);
  }

  isAdminOrOwner(user: AuthenticatedUser): boolean {
    return user.admin || this.row.data[MilestoneRowFields.ownerId] === user.userId;
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const milestoneId = this.row.data[MilestoneRowFields.milestoneId];
    this.restService.delete([`milestones/${milestoneId}`]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.grid.refreshData());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.milestones.dialog.title.delete-one';
  }

  protected getMessageKey(): string {
    const base = 'sqtm-core.administration-workspace.milestones.dialog.message.delete-one.';
    return base + (this.rowHasProject() ? 'with-bound-project' : 'without-bound-project');
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }

  private rowHasProject(): boolean {
    return this.row.data['projectCount'] > 0;
  }
}


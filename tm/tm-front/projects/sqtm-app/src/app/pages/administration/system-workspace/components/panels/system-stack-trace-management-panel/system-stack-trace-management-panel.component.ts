import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {SystemViewState} from '../../../states/system-view.state';
import {SystemViewService} from '../../../services/system-view.service';

@Component({
  selector: 'sqtm-app-system-stack-trace-management-panel',
  templateUrl: './system-stack-trace-management-panel.component.html',
  styleUrls: [
    './system-stack-trace-management-panel.component.less',
    '../../../styles/system-workspace.common.less'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemStackTraceManagementPanelComponent implements OnInit {


  @Input()
  componentData: SystemViewState;

  constructor(private systemViewService: SystemViewService) { }

  ngOnInit(): void {
  }

  changeStackTraceFeatureEnabled() {
    const isActive = this.componentData.stackTraceFeatureIsEnabled;
    const newStatus = !isActive;
    this.systemViewService.changeStackTraceFeatureEnabled(newStatus);
  }
}

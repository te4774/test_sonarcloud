import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminReferentialDataService, AuthenticatedUser, GridService} from 'sqtm-core';
import {Router} from '@angular/router';
import {filter, takeUntil} from 'rxjs/operators';
import {UserWorkspaceService} from '../../../services/user-workspace.service';

@Component({
  selector: 'sqtm-app-main-user-workspace',
  templateUrl: './main-user-workspace.component.html',
  styleUrls: ['./main-user-workspace.component.less'],
  providers: [
    {
      // When visiting UserViewDetail, we get an injection error if GridService is not provided
      // even though it is not used by any mounted component...
      provide: GridService,
      useValue: {},
    },
    {
      provide: UserWorkspaceService,
      useClass: UserWorkspaceService
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainUserWorkspaceComponent implements OnInit, OnDestroy {

  authenticatedAdmin$: Observable<AuthenticatedUser>;

  private unsub$ = new Subject<void>();

  constructor(private adminReferentialDataService: AdminReferentialDataService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.adminReferentialDataService.refresh().subscribe();

    this.authenticatedAdmin$ = this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => authUser.admin)
    );

    this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => !authUser.admin),
    ).subscribe(() => this.router.navigate(['home-workspace']));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

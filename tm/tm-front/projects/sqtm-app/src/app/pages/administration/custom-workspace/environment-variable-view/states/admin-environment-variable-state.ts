import {EvInputType, SqtmGenericEntityState, EnvironmentVariableOption} from 'sqtm-core';

export interface AdminEnvironmentVariableState extends SqtmGenericEntityState {
  id: number;
  name: string;
  code: string;
  inputType: EvInputType;
  boundToServer: boolean;
  options: EnvironmentVariableOption[];
}

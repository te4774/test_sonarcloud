import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminProjectViewComponentData} from '../../project-view/project-view.component';
import {ProjectViewService} from '../../../services/project-view.service';
import {TranslateService} from '@ngx-translate/core';
import {DialogService, GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {takeUntil} from 'rxjs/operators';
import {
  ProjectMilestonesPanelComponent,
  projectMilestonesTableDefinition
} from '../../../components/panels/project-milestones-panel/project-milestones-panel.component';
import {PROJECT_MILESTONES_TABLE, PROJECT_MILESTONES_TABLE_CONF} from '../../../project-view.constant';

@Component({
  selector: 'sqtm-app-project-milestones',
  templateUrl: './project-milestones.component.html',
  styleUrls: ['./project-milestones.component.less'],
  providers: [
    {
      provide: PROJECT_MILESTONES_TABLE_CONF,
      useFactory: projectMilestonesTableDefinition
    },
    {
      provide: PROJECT_MILESTONES_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, PROJECT_MILESTONES_TABLE_CONF, ReferentialDataService]
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectMilestonesComponent implements OnInit, OnDestroy {

  @ViewChild(ProjectMilestonesPanelComponent)
  private projectMilestonesPanelComponent: ProjectMilestonesPanelComponent;

  componentData$: Observable<AdminProjectViewComponentData>;

  unsub$ = new Subject<void>();

  constructor(public projectViewService: ProjectViewService,
              public translateService: TranslateService,
              public dialogService: DialogService,
              public vcr: ViewContainerRef,
              @Inject(PROJECT_MILESTONES_TABLE) public milestoneTable: GridService) {
    this.componentData$ = this.projectViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngOnInit(): void {
  }

  unbindMilestone() {
    this.projectMilestonesPanelComponent.openUnbindMilestonesDialog();
  }

  bindMilestone() {
    this.projectMilestonesPanelComponent.openBindMilestonesDialog();
  }

  createMilestone() {
    this.projectMilestonesPanelComponent.openCreateBindMilestonesDialog();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }


}

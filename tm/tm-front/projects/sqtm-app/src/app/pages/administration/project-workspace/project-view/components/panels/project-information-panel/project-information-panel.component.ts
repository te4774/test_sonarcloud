import {ChangeDetectionStrategy, Component, Input, OnDestroy} from '@angular/core';
import {AdminProjectViewComponentData} from '../../../containers/project-view/project-view.component';
import {ProjectViewService} from '../../../services/project-view.service';
import {map, takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';

@Component({
  selector: 'sqtm-app-project-information-panel',
  templateUrl: './project-information-panel.component.html',
  styleUrls: ['./project-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectInformationPanelComponent implements OnDestroy {

  @Input()
  adminProjectViewComponentData: AdminProjectViewComponentData;

  public isProjectTemplate$: Observable<boolean>;

  private unsub$ = new Subject<void>();

  constructor(
    public adminProjectViewService: ProjectViewService,
  ) {
    this.isProjectTemplate$ = this.adminProjectViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => componentData.project?.template));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  AclGroup,
  ActionErrorDisplayService,
  AdminReferentialDataService,
  AuthenticatedUser,
  ColumnDefinitionBuilder,
  getAclGroupI18nKey,
  GridService,
  LicenseInformationMessageProvider,
  ListPanelItem,
  PermissionGroup,
  RestService,
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {ConnectedPosition, Overlay, OverlayRef} from '@angular/cdk/overlay';
import {ProjectViewService} from '../../../services/project-view.service';
import {map, take, takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'sqtm-app-project-permissions-profile-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height container"
           [class.disabled-row]="row.disabled"
           [class.container-interactive]="canEdit()"
           [class.editable]="canEdit()"
           (click)="showProfileList()">
        <span #profileName
              class="text-ellipsis"
              style="margin: auto 0;"
              nz-tooltip
              [sqtmCoreLabelTooltip]="cellText">
          {{cellText}}
        </span>
        <ng-template #templatePortalContent>
          <sqtm-core-list-panel
              [selectedItem]="row.data[columnDisplay.id]"
              (itemSelectionChanged)="change($event)"
              [items]="panelItems">
          </sqtm-core-list-panel>
        </ng-template>
        <i *ngIf="canEdit()" class="caret-icon flex-fixed-size"
           nz-icon nzType="caret-down" nzTheme="outline"
           [class.container-interactive]="canEdit()"></i>
      </div>
    </ng-container>`,
  styleUrls: ['./project-permissions-profile-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectPermissionsProfileCellComponent extends AbstractListCellRendererComponent implements OnInit {

  constructor(public readonly grid: GridService,
              public readonly cdRef: ChangeDetectorRef,
              public readonly translateService: TranslateService,
              public readonly overlay: Overlay,
              public readonly vcr: ViewContainerRef,
              public readonly restService: RestService,
              public readonly projectViewService: ProjectViewService,
              public readonly actionErrorDisplayService: ActionErrorDisplayService,
              public readonly adminReferentialDataService: AdminReferentialDataService,
              private router: Router) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);

    this.cacheCurrentUser();
  }

  get cellText(): string {
    if (this.panelItems == null) {
      return '';
    }

    const permissionGroup = this.row.data['permissionGroup'] as PermissionGroup;
    const groupName = permissionGroup.qualifiedName;

    const items = this.panelItems.filter(item => item.id === groupName);

    if (items.length === 0) {
      return '';
    }

    return items[0].label;
  }

  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  @ViewChild('profileName', {read: ElementRef})
  profileName: ElementRef;

  overlayRef: OverlayRef;

  panelItems: ListPanelItem[] = [];

  _licenseAllowsUserCreation = false;

  private _currentUser: AuthenticatedUser;

  ngOnInit() {
    this.preparePanelItems();
    this.initialiseLicenseLock();
  }

  canEdit(): boolean {
    return this._licenseAllowsUserCreation;
  }

  showProfileList(): void {
    if (this.canEdit()) {
      this.showList(this.profileName, this.templatePortalContent, LIST_POSITIONS);
    }
  }

  private preparePanelItems(): void {
    this.panelItems = this.getPanelItems();
  }

  private getPanelItems(): ListPanelItem[] {
    const panelItems = Object.keys(AclGroup)
      .map(key => AclGroup[key])
      .map((groupName) => {
        return {
          label: this.translateService.instant(getAclGroupI18nKey(groupName)),
          id: groupName,
        };
      });

    // Sort options by locale label
    panelItems.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    return panelItems;
  }

  private initialiseLicenseLock(): void {
    this.adminReferentialDataService.licenseInformation$.pipe(
      takeUntil(this.unsub$),
      map(licenseInfo => new LicenseInformationMessageProvider(licenseInfo, this.translateService))
    ).subscribe(messageHelper => {
      this._licenseAllowsUserCreation = messageHelper.licenseInformation.allowCreateUsers;
      this.cdRef.detectChanges();
    });
  }

  private cacheCurrentUser(): void {
    this.adminReferentialDataService.authenticatedUser$.pipe(
      take(1),
    ).subscribe(user => this._currentUser = user);
  }

  change(newValue: any): void {
    const currentUserId = this.row.data['partyId'];
    this.projectViewService.setPartyProjectPermissions([currentUserId], newValue).subscribe(
      () => {},
      error => this.handleChangeError(newValue, error)
    );

    this.close();
  }

  private handleChangeError(newValue: any, error): void {
    if (this.checkIfProjectManagerRemovesHisOwnPermissions(newValue)) {
      this.projectViewService.forceProjectWorkspaceGridRefresh().subscribe(() => {
        this.router.navigate(['administration-workspace/projects']);
      });
    }
    console.error(error);
  }

  private checkIfProjectManagerRemovesHisOwnPermissions(newValue: string): boolean {
    const currentUserId = this.row.data['partyId'];
    return !this._currentUser.admin && this._currentUser.userId === currentUserId && newValue !== AclGroup.PROJECT_MANAGER;
  }
}

export function permissionsProfileColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(ProjectPermissionsProfileCellComponent)
    .withI18nKey('sqtm-core.administration-workspace.views.project.permissions.profile.label');
}

export function buildPermissionProfile(translateService: TranslateService) {
  return (keyA: any, keyB: any) => sortPermissionProfile(keyA, keyB, translateService);
}

function sortPermissionProfile(roleA: any, roleB: any, translateService: TranslateService) {
  const list = Object.keys(AclGroup)
    .map(key => AclGroup[key])
    .map((groupName) => {
      return {
        label: translateService.instant(getAclGroupI18nKey(groupName)),
        id: groupName,
      };
    });
  const translatedRoleA = list.find(item => item.id === roleA.qualifiedName);
  const translatedRoleB = list.find(item => item.id === roleB.qualifiedName);
  return translatedRoleA.label.localeCompare(translatedRoleB.label);
}

const LIST_POSITIONS: ConnectedPosition[] = [
  {originX: 'start', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetX: -10, offsetY: 6},
  {originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetX: -10, offsetY: -6},
];

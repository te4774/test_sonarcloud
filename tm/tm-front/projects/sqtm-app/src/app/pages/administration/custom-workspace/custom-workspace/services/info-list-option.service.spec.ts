import {TestBed} from '@angular/core/testing';

import {InfoListOptionService} from './info-list-option.service';
import {RestService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {mockRestService} from '../../../../../utils/testing-utils/mocks.service';

describe('InfoListOptionService', () => {

  const restService = mockRestService();
  let service: InfoListOptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: InfoListOptionService,
          useClass: InfoListOptionService
        }
      ]
    });
    service = TestBed.inject(InfoListOptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

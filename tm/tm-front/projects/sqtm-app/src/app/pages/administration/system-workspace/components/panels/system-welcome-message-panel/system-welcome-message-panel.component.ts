import {ChangeDetectionStrategy, Component} from '@angular/core';
import {SystemViewService} from '../../../services/system-view.service';

@Component({
  selector: 'sqtm-app-system-welcome-message-panel',
  template: `
    <sqtm-core-editable-rich-text
        [value]="(systemViewService.componentData$|async).welcomeMessage"
        [focus]="true"
        (confirmEvent)="changeMessage($event)"
        [attr.data-test-field-id]="'welcomeMessage'">
    </sqtm-core-editable-rich-text>
  `,
  styleUrls: ['./system-welcome-message-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemWelcomeMessagePanelComponent {

  constructor(public readonly systemViewService: SystemViewService) {
  }


  changeMessage($event: string): void {
    this.systemViewService.setWelcomeMessage($event).subscribe();
  }
}

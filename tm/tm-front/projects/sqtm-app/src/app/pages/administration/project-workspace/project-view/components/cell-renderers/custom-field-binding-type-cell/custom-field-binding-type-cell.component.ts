import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DataRowSortFunction,
  GridService,
  InputType
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-custom-field-binding-type-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
        <span style="margin: auto 0;" nz-tooltip [sqtmCoreLabelTooltip]="cellText | translate" [ellipsis]="true">
          {{cellText | translate}}
        </span>
      </div>
    </ng-container>`,
  styleUrls: ['./custom-field-binding-type-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomFieldBindingTypeCellComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  get cellText(): string {
    return 'sqtm-core.entity.custom-field.' + this.row.data[this.columnDisplay.id];
  }
}

// Map each input type to a numeric rank for sorting
const inputTypeSortMap = {
  [InputType.CHECKBOX]: 0,
  [InputType.DATE_PICKER]: 0,
  [InputType.DROPDOWN_LIST]: 0,
  [InputType.NUMERIC]: 0,
  [InputType.TAG]: 0,
  [InputType.RICH_TEXT]: 0,
  [InputType.PLAIN_TEXT]: 0,
};

const sortCustomFieldBindingTypes: DataRowSortFunction = (first, second) => {
  const firstIndex = inputTypeSortMap[first] ?? 0;
  const secondIndex = inputTypeSortMap[second] ?? 0;
  return firstIndex - secondIndex;
};

export function customFieldBindingTypeColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(CustomFieldBindingTypeCellComponent)
    .withI18nKey('sqtm-core.entity.custom-field.bound-entity.label')
    .withSortFunction(sortCustomFieldBindingTypes);
}

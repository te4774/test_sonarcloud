import {Injectable} from '@angular/core';
import {
  AdminReferentialDataService,
  AdminTestAutomationServer,
  AttachmentService,
  AuthenticatedUser,
  AuthenticationProtocol,
  Credentials,
  EntityViewAttachmentHelperService,
  GenericEntityViewService, Identifier,
  isBasicAuthCredentials,
  isOAuth1aCredentials,
  isTokenAuthCredentials,
  RestService,
  BoundEnvironmentVariable,
  EnvironmentVariable
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {
  AdminTestAutomationServerViewState,
  provideInitialAdminTestAutomationServerView
} from '../states/admin-test-automation-server-view-state';
import {AdminTestAutomationServerState} from '../states/admin-test-automation-server-state';
import {map, pluck, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable()
export class TestAutomationServerViewService extends GenericEntityViewService<AdminTestAutomationServerState, 'testAutomationServer'> {

  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    protected attachmentHelper: EntityViewAttachmentHelperService,
    private adminReferentialDataService: AdminReferentialDataService
  ) {
    super(
      restService,
      attachmentService,
      translateService,
      attachmentHelper
    );
  }

  public getInitialState(): AdminTestAutomationServerViewState {
    return provideInitialAdminTestAutomationServerView();
  }

  protected getRootUrl(): string {
    return 'test-automation-servers';
  }

  load(testAutomationServerId: number) {
    const parts = ['test-automation-server-view', testAutomationServerId.toString()];
    return this.restService.getWithoutErrorHandling<AdminTestAutomationServer>(parts)
      .subscribe(
        testAutomationServer => this.initializeTestAutomationServer(testAutomationServer),
        err => this.notifyEntityNotFound(err));
  }

  private initializeTestAutomationServer(testAutomationServer: AdminTestAutomationServer): void {
    const testAutomationServerState: AdminTestAutomationServerState = {
      ...testAutomationServer,
      attachmentList: {id: null, attachments: null},
    };
    this.initializeEntityState(testAutomationServerState);
  }

  changeManualSlaveSelection(selected: boolean): void {
    this.store.state$.pipe(
      take(1),
      switchMap((state: AdminTestAutomationServerViewState) => this.changeManualSlaveSelectionServerSide(state, selected)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminTestAutomationServerViewState]) => this.updateStateWithNewManualSlaveSelection(state, selected)),
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  private changeManualSlaveSelectionServerSide(state: AdminTestAutomationServerViewState, selected: boolean) {
    const urlParts = ['test-automation-servers', state.testAutomationServer.id.toString(), 'manual-selection'];
    return this.restService.post(urlParts, {manualSlaveSelection: selected});
  }

  private updateStateWithNewManualSlaveSelection(state: AdminTestAutomationServerViewState, selected: boolean) {
    return {
      ...state,
      testAutomationServer: {
        ...state.testAutomationServer,
        manualSlaveSelection: selected
      }
    };
  }

  setCredentials(credentials: Credentials): Observable<any> {
    if (isOAuth1aCredentials(credentials)) {
      return this.setOAuthCredentials(credentials.token, credentials.tokenSecret);
    } else if (isBasicAuthCredentials(credentials)) {
      return this.setBasicAuthCredentials(credentials.username, credentials.password);
    } else if (isTokenAuthCredentials(credentials)) {
      return this.setTokenAuthCredentials(credentials.token);
    }
  }

  setBasicAuthCredentials(username: string, password: string): Observable<any> {
    return this.doSetCredentials({
      implementedProtocol: AuthenticationProtocol.BASIC_AUTH,
      type: AuthenticationProtocol.BASIC_AUTH,
      username,
      password,
    });
  }

  reloadEnvironmentFromObserverUrl(url: string, serverId: any) {
    return this.componentData$.pipe(
      take(1),
      map((data: AdminTestAutomationServerViewState) => ({
        ...data,
        testAutomationServer: {
          ...data.testAutomationServer,
            id: serverId
        }
      })),
      tap((data: AdminTestAutomationServerViewState) => this.requireExternalUpdate(data.testAutomationServer.id, 'observerUrl', url)),
    );
  }

  setOAuthCredentials(token: string, tokenSecret: string): any {
    return this.doSetCredentials({
      implementedProtocol: AuthenticationProtocol.OAUTH_1A,
      type: AuthenticationProtocol.OAUTH_1A,
      token,
      tokenSecret,
    });
  }

  private setTokenAuthCredentials(token: string): Observable<any> {
    return this.doSetCredentials({
      implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
      type: AuthenticationProtocol.TOKEN_AUTH,
      token,
    });
  }

  private doSetCredentials(credentials: Credentials): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminTestAutomationServerViewState) => this.restService.post(
        [this.getRootUrl(), data.testAutomationServer.id.toString(), 'credentials'], credentials)),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminTestAutomationServerViewState]) => {
        credentials.registered = true;
        return {
          ...state,
          testAutomationServer: {
            ...state.testAutomationServer,
            credentials,
          }
        };
      }),
      tap((state) => this.requireExternalUpdate(state.testAutomationServer.id, 'credentials', credentials)),
      tap((nextState: AdminTestAutomationServerViewState) => this.store.commit(nextState))
    );
  }

  setAuthenticationProtocol(authProtocol: AuthenticationProtocol): Observable<any> {
    return this.componentData$.pipe(
      take(1),
      switchMap((data: AdminTestAutomationServerViewState) => this.restService.post(
        [this.getRootUrl(), data.testAutomationServer.id.toString(), 'auth-protocol'], {authProtocol})),
      withLatestFrom(this.componentData$),
      map(([, state]: [any, AdminTestAutomationServerViewState]) => ({
        ...state,
        testAutomationServer: {
          ...state.testAutomationServer,
          authProtocol: authProtocol
        }
      })),
      tap((nextState: AdminTestAutomationServerViewState) => this.store.commit(nextState)),
      tap((state) => this.requireExternalUpdate(state.testAutomationServer.id, 'authProtocol', authProtocol))
    );
  }

  getCurrentUser(): Observable<AuthenticatedUser> {
    return this.adminReferentialDataService.authenticatedUser$;
  }

  // This is called after tags were successfully changed in EnvironmentSelectionService so backend request has already been made
  handleDefaultTagsChanged(newTags: string[]): void {
    this.state$.pipe(
      take(1),
      map(state => ({
        ...state,
        testAutomationServer: {
          ...state.testAutomationServer,
          environmentTags: newTags,
        },
      })),
      tap(nextState => this.store.commit(nextState)),
    ).subscribe();
  }

  deleteCredentials(testAutomationServerId: number) {
    return this.restService.delete([this.getRootUrl(), testAutomationServerId.toString(), 'credentials']).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminTestAutomationServerViewState]) => this.updateTestAutomationServerCredentials(state, {
        implementedProtocol: AuthenticationProtocol.TOKEN_AUTH,
        type: AuthenticationProtocol.TOKEN_AUTH,
        registered: false,
        token: null
      })),
      tap((newState) => this.commit(newState)),
      tap((newState) => this.requireExternalUpdate(newState.testAutomationServer.id, 'credentials',
        newState.testAutomationServer.credentials)));
  }

  private updateTestAutomationServerCredentials(state: AdminTestAutomationServerViewState,
                                                credentials?: Credentials): AdminTestAutomationServerViewState {
    return {
      ...state,
      testAutomationServer: {
        ...state.testAutomationServer,
        credentials
      }
    };
  }

  getAllEnvironmentVariables(): Observable<EnvironmentVariable[]> {
    const urlParts = ['test-automation-server-view', 'environment-variables'];
    return this.restService.get<EnvironmentVariableResponse>(urlParts).pipe(
      take(1),
      map((response: EnvironmentVariableResponse) => response.environmentVariables)
    );
  }

  bindEnvironmentVariables(selectedItemIds: Identifier[]) {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminTestAutomationServerViewState) => this.bindEnvironmentVariablesServerSide(state, selectedItemIds)),
      withLatestFrom(this.store.state$),
      map(([result, state]) => this.updateStateWithNewBindings(result, state)),
      map((nextState) => {
        this.store.commit(nextState);
      }),
    );
  }

  private bindEnvironmentVariablesServerSide(state: AdminTestAutomationServerViewState,
                                             selectedItemIds: Identifier[]): Observable<BoundEnvironmentVariable[]> {
    const urlPart = [
      'test-automation-servers',
      state.testAutomationServer.id.toString(),
      'environment-variables',
      'bind',
      selectedItemIds.toString()
    ];
    return this.restService
      .post<{ boundEnvironmentVariables: BoundEnvironmentVariable[] }>(urlPart)
      .pipe(
        pluck('boundEnvironmentVariables')
      );
  }

  private updateStateWithNewBindings(result: BoundEnvironmentVariable[], state: AdminTestAutomationServerViewState) {
    return {
      ...state,
      testAutomationServer: {
        ...state.testAutomationServer,
        boundEnvironmentVariables: result
      }
    };
  }

  unbindEnvironmentVariables(environmentVariableIds: any) {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminTestAutomationServerViewState) =>
        this.unbindEnvironmentVariablesServerSide(state, environmentVariableIds)),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.updateStateWithUnbindEnvironmentVariables(state, environmentVariableIds)),
      map((nextState) => this.store.commit(nextState))
    );
  }

  private unbindEnvironmentVariablesServerSide(state: AdminTestAutomationServerViewState, environmentVariableIds: number[]) {
    const urlParts = [
      'test-automation-servers',
      state.testAutomationServer.id.toString(),
      'environment-variables',
      'unbind',
      environmentVariableIds.toString()
    ];
    return this.restService.post(urlParts);
  }

  private updateStateWithUnbindEnvironmentVariables(state: AdminTestAutomationServerViewState, environmentVariableIds: number[]) {
    const updatedEnvironmentVariableBinding = [...state.testAutomationServer.boundEnvironmentVariables]
      .filter((boundEnvironmentVariable: BoundEnvironmentVariable) => !environmentVariableIds.includes(boundEnvironmentVariable.id));
    return {
      ...state,
      testAutomationServer: {
        ...state.testAutomationServer,
        boundEnvironmentVariables: updatedEnvironmentVariableBinding
      }
    };
  }

  setEnvironmentVariableValue(environmentVariableId: number, valueChange: string): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminTestAutomationServerViewState) =>
        this.setEnvironmentVariableValueServerSide(environmentVariableId, valueChange, state)),
      withLatestFrom(this.store.state$),
      map(([, state]) => this.updateStateWithNewEnvironmentVariableValue(state, valueChange, environmentVariableId)),
      tap(state => this.store.commit(state))
    );
  }

  private setEnvironmentVariableValueServerSide(environmentVariableId: number,
                                                valueChange: string,
                                                state: AdminTestAutomationServerViewState) {
    const urlParts = [
      'test-automation-servers',
      state.testAutomationServer.id.toString(),
      'environment-variables',
      'value',
      environmentVariableId.toString()
    ];
    return this.restService.post(urlParts, {value: valueChange});
  }

  private updateStateWithNewEnvironmentVariableValue(state: AdminTestAutomationServerViewState, valueChange: string,
                                                     environmentVariableId: number) {

    const updateEnvironmentVariableValue: BoundEnvironmentVariable[] =
      [...state.testAutomationServer.boundEnvironmentVariables];

    const boundEnvironmentVariable: BoundEnvironmentVariable = updateEnvironmentVariableValue
      .filter(entity => entity.id === environmentVariableId)[0];

    boundEnvironmentVariable.value = valueChange;

    const index: number = updateEnvironmentVariableValue.findIndex(entity => entity.id === environmentVariableId);
    updateEnvironmentVariableValue[index] = boundEnvironmentVariable;

    return {
      ...state,
      testAutomationServer: {
        ...state.testAutomationServer,
        boundEnvironmentVariables: updateEnvironmentVariableValue
      }
    };
  }
}

interface EnvironmentVariableResponse {
  environmentVariables: EnvironmentVariable[];
}

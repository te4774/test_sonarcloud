import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractDeleteCellRenderer, DialogService, GridService} from 'sqtm-core';
import {InfoListOptionService} from '../../../services/info-list-option.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-info-list-option-cell-renderer',
  template: `
    <sqtm-core-delete-icon (delete)="doDelete()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./delete-info-list-option.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteInfoListOptionComponent extends AbstractDeleteCellRenderer {

  constructor(public grid: GridService,
              public cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private infoListOptionService: InfoListOptionService) {
    super(grid, cdr, dialogService);
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const optionLabel = this.row.data.label;
    this.infoListOptionService.removeOption(optionLabel).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.grid.refreshData());
  }
}

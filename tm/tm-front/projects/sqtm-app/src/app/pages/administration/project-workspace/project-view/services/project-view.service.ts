import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {
  AdminReferentialDataService,
  AdminViewAttachmentHelperService,
  AttachmentModel,
  AttachmentService,
  AttachmentState,
  AuthenticatedUser,
  AutomationWorkflowTypes,
  BddImplementationTechnology,
  BddImplementationTechnologyKeys,
  BddScriptLanguage,
  BindableEntity,
  Bindings,
  Credentials,
  CustomField,
  CustomFieldBinding,
  GenericEntityViewService,
  Milestone,
  MilestoneAdminProjectView,
  PluginId,
  ProjectPlugin,
  ProjectView,
  RestService,
  TestAutomationProject
} from 'sqtm-core';
import {AdminProjectState, StatusesInUseState} from '../state/admin-project-state';
import {AdminProjectViewState, provideInitialAdminProjectView} from '../state/admin-project-view-state';
import {concatMap, filter, map, pluck, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {createFeatureSelector} from '@ngrx/store';
import {AdminProjectViewComponentData} from '../containers/project-view/project-view.component';

@Injectable()
export class ProjectViewService extends GenericEntityViewService<AdminProjectState, 'project'> {

  public static readonly MAX_AUTOMATED_SUITES_LIFETIME = Math.pow(2, 31) - 1;

  public readonly boundCustomFields$: Observable<ExtendedCustomFieldBinding[]>;

  public readonly permissionCount$: Observable<number>;
  public readonly customFieldCount$: Observable<number>;
  public readonly milestoneCount$: Observable<number>;
  public readonly availablePlugins$: Observable<ProjectPlugin[]>;
  public readonly activePluginCount$: Observable<number>;

  constructor(
    protected restService: RestService,
    protected attachmentService: AttachmentService,
    protected translateService: TranslateService,
    private readonly adminReferentialDataService: AdminReferentialDataService,
    attachmentHelper: AdminViewAttachmentHelperService,
  ) {
    super(
      restService,
      attachmentService,
      translateService,
      attachmentHelper,
    );

    // Map the Bindings object to a flat array of CustomFieldBinding
    this.boundCustomFields$ = this.store.state$.pipe(
      map((adminProjectViewState: AdminProjectViewState) => adminProjectViewState.project?.customFieldBindings),
      filter(bindings => Boolean(bindings)),
      map(bindingsByEntity => Object.values(bindingsByEntity).flat()),
      withLatestFrom(adminReferentialDataService.adminReferentialData$),
      map(([bindings, referentialData]) => {
        const allCustomFields = Object.values(referentialData.customFieldState.entities);
        return this.extendCustomFieldBindings(bindings, allCustomFields);
      }),
    );

    this.permissionCount$ = this.componentData$.pipe(
      map(componentData => componentData.project.partyProjectPermissions.length)
    );

    this.customFieldCount$ = this.componentData$.pipe(
      map(componentData => this.countCustomFieldBindings(componentData.project.customFieldBindings))
    );

    this.milestoneCount$ = this.componentData$.pipe(
      map(componentData => componentData.project.boundMilestonesInformation.length)
    );

    this.availablePlugins$ = this.componentData$.pipe(
      pluck('project', 'availablePlugins')
    );

    this.activePluginCount$ = this.availablePlugins$.pipe(
      map((plugins) => plugins?.filter(plugin => plugin.enabled).length || 0)
    );
  }

  // Implements GenericEntityViewService
  public getInitialState(): AdminProjectViewState {
    return provideInitialAdminProjectView();
  }

  public load(id: number) {
    this.restService.getWithoutErrorHandling<ProjectView>(['project-view', id.toString()])
      .subscribe(
        (project: ProjectView) => this.initializeAdminProject(project),
        (err) => this.notifyEntityNotFound(err));
  }

  public coerceProjectIntoTemplate() {
    this.store.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => this.coerceProjectIntoTemplateServerSide(state)),
      concatMap(() => this.forceProjectWorkspaceGridRefresh()),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => this.updateStateForProjectBecomingTemplate(state))
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  private coerceProjectIntoTemplateServerSide(adminProjectViewState: AdminProjectViewState) {
    return this.restService.post(['projects', 'coerce-into-template'], {projectId: adminProjectViewState.project.id});
  }

  private updateStateForProjectBecomingTemplate(state: AdminProjectViewState): AdminProjectViewState {
    return {
      ...state,
      project: {
        ...state.project,
        template: true,
        linkedTemplate: null,
        linkedTemplateId: null,
      }
    };
  }

  associateWithTemplate(templateId: number, boundTemplatePluginIds: string[]) {
    this.store.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => this.associateProjectWithTemplateServerSide(state, templateId, boundTemplatePluginIds)),
      withLatestFrom(this.store.state$),
      map(([response, state]: [{ templateName: string }, AdminProjectViewState]) => {
        return this.updateStateWithNewAssociatedTemplate(state, response, templateId);
      })
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  private associateProjectWithTemplateServerSide(state: AdminProjectViewState,
                                                 templateId: number,
                                                 boundTemplatePlugins: string[]): Observable<{ templateName: string }> {
    const projectId = state.project.id.toString();
    return this.restService.post<{ templateName: string }>(
      [this.getRootUrl(), projectId, 'linked-template-id'],
      {
        linkedTemplateId: templateId,
        boundTemplatePlugins
      });
  }

  private updateStateWithNewAssociatedTemplate(state: AdminProjectViewState,
                                               response: { templateName: string },
                                               templateId: number): AdminProjectViewState {
    return {
      ...state,
      project: {
        ...state.project,
        linkedTemplate: response.templateName,
        linkedTemplateId: templateId,
      }
    };
  }

  disassociateFromTemplate() {
    this.associateWithTemplate(null, []);
  }

  unbindFromScmRepository() {
    this.bindToScmRepository(null);
  }

  bindToScmRepository(repositoryId: number) {
    this.update('scmRepositoryId', null);

    this.store.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => this.bindToScmRepositoryServerSide(state, repositoryId)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => this.updateStateWithNewScmRepositoryBinding(state, repositoryId))
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  private bindToScmRepositoryServerSide(state: AdminProjectViewState, repositoryId: number): Observable<{ scmRepositoryId: number }> {
    const projectId = state.project.id.toString();
    return this.restService.post<{ scmRepositoryId: number }>(
      [this.getRootUrl(), projectId, 'scm-repository-id'],
      {scmRepositoryId: repositoryId});
  }

  private updateStateWithNewScmRepositoryBinding(state: AdminProjectViewState, repositoryId: number): AdminProjectViewState {
    return {
      ...state,
      project: {
        ...state.project,
        scmRepositoryId: repositoryId,
      }
    };
  }

  confirmBugtracker(bugtrackerId: number) {
    this.store.state$.pipe(
      take(1),
      concatMap((state: AdminProjectViewState) => this.changeBugtrackerServerSideAndRetrieveProjectNames(state, bugtrackerId)),
      withLatestFrom(this.store.state$),
      map(([projectNames, state]: [string[], AdminProjectViewState]) => {
        return this.updateStateWithNewBugtrackerBinding(bugtrackerId, state, projectNames);
      })
    ).subscribe(state => {
      this.store.commit(state);
      this.requireExternalUpdate(state.project.id, 'bugTrackerBinding');
    });
  }

  private changeBugtrackerServerSideAndRetrieveProjectNames(state: AdminProjectViewState, bugtrackerId: number): Observable<string[]> {
    const projectId = state.project.id.toString();
    const urlParts = [this.getRootUrl(), projectId, 'bugtracker'];
    return this.restService.post(urlParts, {bugtrackerId: bugtrackerId}).pipe(
      switchMap(() => {
          return this.retrieveBugtrackerProjectNames(projectId);
        }
      )
    );
  }

  private retrieveBugtrackerProjectNames(projectId: string): Observable<string[]> {
    const parts = [this.getRootUrl(), projectId, 'bugtracker/projectNames'];
    return this.restService.get<{ projectNames: string[] }>(parts).pipe(
      pluck('projectNames')
    );
  }

  private updateStateWithNewBugtrackerBinding(bugtrackerId: number,
                                              state: AdminProjectViewState,
                                              projectNames: string[]): AdminProjectViewState {
    if (bugtrackerId === null) {
      return {
        ...state,
        project: {
          ...state.project,
          bugTrackerBinding: null,
          bugtrackerProjectNames: projectNames
        }
      };
    } else {
      return {
        ...state,
        project: {
          ...state.project,
          bugTrackerBinding: {
            ...state.project.bugTrackerBinding,
            bugTrackerId: Number(bugtrackerId),
          },
          bugtrackerProjectNames: projectNames
        }
      };
    }
  }

  changeAllowTcModifDuringExec(allowTcModifDuringExec: boolean) {
    this.store.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => this.changeAllowTcModifiDuringExecServerSide(state, allowTcModifDuringExec)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => this.updateStateForAllowTcModifDuringExec(state, allowTcModifDuringExec))
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  private changeAllowTcModifiDuringExecServerSide(state: AdminProjectViewState,
                                                  allowTcModifDuringExec: boolean): Observable<{ allowTcModifDuringExec: boolean }> {
    const projectId = state.project.id.toString();
    return this.restService.post<{ allowTcModifDuringExec: boolean }>(
      [this.getRootUrl(), projectId, 'allow-tc-modif-during-exec'],
      {allowTcModifDuringExec: allowTcModifDuringExec});
  }

  private updateStateForAllowTcModifDuringExec(state: AdminProjectViewState, allowTcModifDuringExec: boolean): AdminProjectViewState {
    return {
      ...state,
      project: {
        ...state.project,
        allowTcModifDuringExec: allowTcModifDuringExec,
      }
    };
  }

  changeExecutionStatusOnProject(isEnabled: boolean, status: string) {
    this.store.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => this.changeExecutionStatusOnProjectServerSide(state, status, isEnabled)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => this.updateStateWithAllowedStatuses(state, status, isEnabled))
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  private changeExecutionStatusOnProjectServerSide(state: AdminProjectViewState,
                                                   status: string,
                                                   isEnabled: boolean): Observable<{ allowedStatuses: Map<string, Boolean> }> {
    const projectId = state.project.id.toString();
    const updatedAllowedStatuses = {...state.project.allowedStatuses};
    updatedAllowedStatuses[status] = isEnabled;

    return this.restService.post<{ allowedStatuses: Map<string, Boolean> }>(
      [this.getRootUrl(), projectId, 'change-execution-status', status],
      updatedAllowedStatuses);
  }

  private updateStateWithAllowedStatuses(state: AdminProjectViewState, status: string, isEnabled: boolean): AdminProjectViewState {
    const updatedAllowedStatuses = {...state.project.allowedStatuses};
    updatedAllowedStatuses[status] = isEnabled;
    return {
      ...state,
      project: {
        ...state.project,
        allowedStatuses: updatedAllowedStatuses,
      }
    };
  }

  disableAndReplaceStatusWithinProject(sourceExecutionStatus: string, targetStatus: string) {
    this.store.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => this.disableAndReplaceStatusServerSide(state, targetStatus, sourceExecutionStatus)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => {
        return this.updateStateWithStatusesInUse(state, sourceExecutionStatus);
      })
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  private disableAndReplaceStatusServerSide(state: AdminProjectViewState,
                                            targetStatus: string,
                                            sourceExecutionStatus: string): Observable<{ executionStatus: Map<string, string> }> {
    const projectId = state.project.id.toString();
    const executionStatus = {'targetExecutionStatus': targetStatus};

    return this.restService.post<{ executionStatus: Map<string, string> }>(
      [this.getRootUrl(), projectId, 'disable-and-replace-execution-status-within-project', sourceExecutionStatus],
      executionStatus);
  }

  private updateStateWithStatusesInUse(state: AdminProjectViewState, sourceExecutionStatus: string): AdminProjectViewState {
    const updatedStatusesInUse = {...state.project.statusesInUse};
    updatedStatusesInUse[sourceExecutionStatus] = false;

    const updatedAllowedStatus = {...state.project.allowedStatuses};
    updatedAllowedStatus[sourceExecutionStatus] = false;

    return {
      ...state,
      project: {
        ...state.project,
        statusesInUse: updatedStatusesInUse,
        allowedStatuses: updatedAllowedStatus
      }
    };
  }

  bindToTestAutomationServer(taServerId: number) {
    this.store.state$.pipe(
      take(1),
      switchMap((sate: AdminProjectViewState) => this.bindTestAutomationServerServerSide(sate, taServerId)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => this.updateStateWithNewTestAutomationServerBinding(state, taServerId))
    ).subscribe(state => {
      this.store.commit(state);
      this.requireExternalUpdate(state.project.id, 'taServerId');
    });
  }

  private bindTestAutomationServerServerSide(adminProjectViewState: AdminProjectViewState,
                                             taServerId: number): Observable<{ scmRepositoryId: number }> {
    const projectId = adminProjectViewState.project.id.toString();
    return this.restService.post<{ scmRepositoryId: number }>(
      [this.getRootUrl(), projectId, 'ta-server-id'],
      {taServerId});
  }

  private updateStateWithNewTestAutomationServerBinding(state: AdminProjectViewState, taServerId: number): AdminProjectViewState {
    return {
      ...state,
      project: {
        ...state.project,
        taServerId,
        boundTestAutomationProjects: [],
      }
    };
  }

  addTestAutomationProjects(taProjects: TestAutomationProjectFormModel[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => this.addTestAutomationProjectsServerSide(taProjects, state)),
      withLatestFrom(this.store.state$),
      map(([response, state]) => this.updateStateWithNewTestAutomationProjects(state, response)),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  private addTestAutomationProjectsServerSide(taProjects: TestAutomationProjectFormModel[],
                                              state: AdminProjectViewState): Observable<{ taProjects: TestAutomationProject[] }> {
    const requestBody = {taProjects};
    const urlParts = [this.getRootUrl(), state.project.id.toString(), 'test-automation-projects/new'];
    return this.restService.post<{ taProjects: TestAutomationProject[] }>(urlParts, requestBody);
  }

  private updateStateWithNewTestAutomationProjects(state, response): AdminProjectViewState {
    return {
      ...state,
      project: {
        ...state.project,
        boundTestAutomationProjects: response.taProjects,
      }
    };
  }

  updateTestAutomationProject(updatedAutomationJob: UpdatedAutomationJob): Observable<any> {
    return this.updateTestAutomationProjectServerSide(updatedAutomationJob).pipe(
      withLatestFrom(this.state$),
      map(([response, state]) => this.updateStateWithUpdatedTestAutomationProject(state, response)),
      map((nextState) => this.store.commit(nextState)),
    );
  }

  private updateTestAutomationProjectServerSide(updatedJob: UpdatedAutomationJob): Observable<{ taProjects: TestAutomationProject[] }> {
    const urlParts = ['test-automation-projects', updatedJob.taProjectId.toString()];
    return this.restService.post<{ taProjects: TestAutomationProject[] }>(urlParts, updatedJob);
  }

  private updateStateWithUpdatedTestAutomationProject(state, response): AdminProjectViewState {
    return {
      ...state,
      project: {
        ...state.project,
        boundTestAutomationProjects: response.taProjects,
      }
    };
  }

  removeTestAutomationProjects(taProjectIds: number[]): Observable<any> {
    return this.removeTestAutomationProjectsServerSide(taProjectIds).pipe(
      withLatestFrom(this.state$),
      map(([, state]) => this.updateStateWithRemovedTestAutomationProjects(state, taProjectIds)),
      tap((nextState) => this.store.commit(nextState))
    );
  }

  private removeTestAutomationProjectsServerSide(taProjectIds: number[]): Observable<{ taProjects: TestAutomationProject[] }> {
    return this.restService.delete(['test-automation-projects', taProjectIds.join(',')]);
  }

  private updateStateWithRemovedTestAutomationProjects(state, taProjectIds: number[]): AdminProjectViewState {
    const updatedTestAutomationProjects = state.project.boundTestAutomationProjects
      .filter(taProject => !taProjectIds.includes(taProject.taProjectId));

    return {
      ...state,
      project: {
        ...state.project,
        boundTestAutomationProjects: updatedTestAutomationProjects,
      }
    };
  }

  setJobLabel(taProjectId: any, label: string): Observable<any> {
    return this.setJobLabelServerSide(taProjectId, label).pipe(
      withLatestFrom(this.state$),
      map(([response, state]) => this.updateStateWithNewJobLabel(state, response)),
      tap((nextState) => this.store.commit(nextState)),
    );
  }

  private setJobLabelServerSide(taProjectId: any, label: string): Observable<{ taProjects: TestAutomationProject[] }> {
    const urlParts = ['test-automation-projects', taProjectId.toString(), 'label'];
    return this.restService.post<{ taProjects: TestAutomationProject[] }>(urlParts, {label});
  }

  private updateStateWithNewJobLabel(state, response): AdminProjectViewState {
    return {
      ...state,
      project: {
        ...state.project,
        boundTestAutomationProjects: response.taProjects,
      }
    };
  }

  setJobCanRunBdd(taProjectId: number, canRunBdd: boolean): void {
    this.setJobCanRunBddServerSide(canRunBdd, taProjectId).pipe(
      withLatestFrom(this.state$),
      map(([response, state]) => this.updateStateWithNewSettingForJobCanRunBdd(state, response)),
    ).subscribe((nextState) => this.store.commit(nextState));
  }

  private setJobCanRunBddServerSide(canRunBdd: boolean, taProjectId: number): Observable<{ taProjects: TestAutomationProject[] }> {
    const requestBody = {canRunBdd};
    const urlParts = ['test-automation-projects', taProjectId.toString(), 'can-run-bdd'];
    return this.restService.post<{ taProjects: TestAutomationProject[] }>(urlParts, requestBody);
  }

  private updateStateWithNewSettingForJobCanRunBdd(state, response): AdminProjectViewState {
    return {
      ...state,
      project: {
        ...state.project,
        boundTestAutomationProjects: response.taProjects,
      }
    };
  }

  bindCustomFields(selectedEntity: BindableEntity, selectedCustomFields: CustomField[]) {
    this.state$.pipe(
      take(1),
      withLatestFrom(this.boundCustomFields$),
      switchMap(([state]) => this.bindCustomFieldsServerSide(state, selectedCustomFields, selectedEntity)),
      withLatestFrom(this.state$),
      map(([response, state]) => (this.updateStateWithNewCufBindings(state, response)))
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  private bindCustomFieldsServerSide(state, selectedCustomFields: CustomField[], selectedEntity: BindableEntity): Observable<Bindings> {
    const projectId = state.project.id;
    const bindingsToAdd = this.prepareCufBindingsToAddForRequestBody(selectedCustomFields, projectId, selectedEntity);
    const requestBody: { customFieldBindings: CustomFieldBindingModel[] } = {
      customFieldBindings: bindingsToAdd
    };

    const urlParts = ['custom-field-binding/project', projectId.toString(), 'bind-custom-fields'];
    return this.restService.post<Bindings>(urlParts, requestBody);
  }


  private prepareCufBindingsToAddForRequestBody(selectedCustomFields: CustomField[], projectId, selectedEntity: BindableEntity) {
    return selectedCustomFields
      .map(cuf => ({
        projectId,
        boundEntity: {enumName: selectedEntity.toString()},
        customField: {
          id: cuf.id,
          itype: cuf.inputType,
        }
      }));
  }

  private updateStateWithNewCufBindings(state, response): AdminProjectViewState {
    return {
      ...state,
      project: {
        ...state.project,
        customFieldBindings: response,
      }
    };
  }

  unbindCustomFields(bindingIds: number[]): Observable<any> {
    return this.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => this.unbindCustomFieldsServerSide(state, bindingIds)),
      withLatestFrom(this.state$),
      map(([, state]: [any, AdminProjectViewState]) => this.updateStateWithRemovedCustomFields(state, bindingIds)),
      tap((newState) => this.store.commit(newState)),
    );
  }

  private unbindCustomFieldsServerSide(state: AdminProjectViewState, bindingIds: number[]): Observable<void> {
    const urlParts = [
      'custom-field-binding/project',
      state.project.id.toString(),
      'unbind-custom-fields',
      bindingIds.join(',')
    ];
    return this.restService.delete(urlParts);
  }

  private updateStateWithRemovedCustomFields(state: AdminProjectViewState, bindingIds: number[]): AdminProjectViewState {
    const updatedBindings = state.project.customFieldBindings;

    Object.keys(updatedBindings).forEach((key) => {
      if (Array.isArray(updatedBindings[key])) {
        updatedBindings[key] = updatedBindings[key].filter(binding => !bindingIds.includes(binding.id));
      }
    });

    return {
      ...state,
      project: {
        ...state.project,
        customFieldBindings: updatedBindings,
      }
    };
  }

  bindInfoListToProject(infoListId: number, scope: 'category' | 'type' | 'nature') {
    this.store.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => {
        return this.bindInfoListToProjectServerSide(state, scope, infoListId);
      }),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => {
        return this.updateStateWithNewInfoListBinding(scope, state, infoListId);
      })
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  private bindInfoListToProjectServerSide(state: AdminProjectViewState,
                                          scope: 'category' | 'type' | 'nature',
                                          infoListId: number): Observable<{ infoListId: number }> {
    const projectId = state.project.id.toString();
    return this.restService.post<{ infoListId: number }>(
      ['info-list-binding/project', projectId, scope],
      {infoListId});
  }

  private updateStateWithNewInfoListBinding(scope: 'category' | 'type' | 'nature',
                                            state: AdminProjectViewState,
                                            infoListId: number): AdminProjectViewState {
    const infoListKeyToUpdate: string = this.getInfoListKey(scope);
    const updatedState: AdminProjectViewState = {...state};
    updatedState.project[infoListKeyToUpdate] = infoListId;
    return updatedState;
  }

  private getInfoListKey(scope: 'category' | 'type' | 'nature'): keyof AdminProjectState {
    switch (scope) {
      case 'category':
        return 'requirementCategoryId';
      case 'type':
        return 'testCaseTypeId';
      case 'nature':
        return 'testCaseNatureId';
      default :
        throw new Error('InfoList key does not exist');
    }
  }

  /**
   * Add or replace a permission group for a given party id.
   *
   * @param partyIds: ids of the users or teams to grant permission to
   * @param newGroup: an AclGroup qualified name
   */
  setPartyProjectPermissions(partyIds: number[], newGroup: string): Observable<void> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => this.setPartyProjectPermissionsServerSide(state, partyIds, newGroup)),
      withLatestFrom(this.store.state$),
      map(([response, state]: [any, AdminProjectViewState]) => this.updateStateWithNewPartyProjectPermissions(state, response)),
      map(state => this.store.commit(state)),
    );
  }

  private setPartyProjectPermissionsServerSide(state: AdminProjectViewState, partyIds: number[], newGroup: string): Observable<void> {
    const urlParts = [this.getRootUrl(), state.project.id.toString(), 'permissions', partyIds.join(','), 'group'];
    return this.restService.post(urlParts, {group: newGroup});
  }

  private updateStateWithNewPartyProjectPermissions(state: AdminProjectViewState, response): AdminProjectViewState {
    return {
      ...state,
      project: {
        ...state.project,
        partyProjectPermissions: response.partyProjectPermissions,
      }
    };
  }

  forceProjectWorkspaceGridRefresh(): Observable<void> {
    return this.store.state$.pipe(
      take(1),
      map(state => this.requireExternalUpdate(state.project.id))
    );
  }

  removePartyProjectPermissions(partyIds: number[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => this.removePartyProjectPermissionsServerSide(state, partyIds)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => this.updateStateWithRemovedPartyProjectPermissions(state, partyIds)),
      tap((state) => this.store.commit(state))
    );
  }

  private removePartyProjectPermissionsServerSide(adminProjectViewState: AdminProjectViewState, partyIds: number[]): Observable<void> {
    const urlParts = [
      this.getRootUrl(),
      adminProjectViewState.project.id.toString(),
      'permissions',
      partyIds.join(',')
    ];

    return this.restService.delete(urlParts);
  }

  private updateStateWithRemovedPartyProjectPermissions(state: AdminProjectViewState, partyIds: number[]): AdminProjectViewState {
    const filteredPartyProjectPerm = state.project.partyProjectPermissions
      .filter(value => !partyIds.includes(value.partyId));

    return {
      ...state,
      project: {
        ...state.project,
        partyProjectPermissions: filteredPartyProjectPerm,
      }
    };
  }

  bindMilestonesToProject(milestone: Milestone[]) {
    const milestoneIds: number[] = [];
    milestone.forEach(m => milestoneIds.push(m.id));
    this.store.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => this.bindMilestonesToProjectServerSide(state, milestoneIds)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => this.updateStateWithNewMilestonesBoundToProject(state, milestone))
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  private bindMilestonesToProjectServerSide(state: AdminProjectViewState, milestoneIds: number[]): Observable<void> {
    const urlParts = [
      'milestone-binding/project',
      state.project.id.toString(),
      'bind-milestones',
      milestoneIds.join(',')
    ];
    return this.restService.post(urlParts);
  }

  private updateStateWithNewMilestonesBoundToProject(state: AdminProjectViewState, milestone: Milestone[]): AdminProjectViewState {
    const updatedBoundMilestoneInformation = state.project.boundMilestonesInformation;
    milestone.forEach((newMilestone: Milestone) => {
      const element = document.createElement('element');
      element.innerHTML = newMilestone.description;
      newMilestone = {...newMilestone, description: element.innerText};
      const newMilestoneAdminProjectView: MilestoneAdminProjectView = {
        milestone: newMilestone,
        milestoneBoundToOneObjectOfProject: false
      };
      updatedBoundMilestoneInformation.push(newMilestoneAdminProjectView);
    });

    return {
      ...state,
      project: {
        ...state.project,
        boundMilestonesInformation: updatedBoundMilestoneInformation,
      }
    };
  }

  updateBoundMilestoneInformation(milestoneAdminProjectView: MilestoneAdminProjectView): void {
    this.store.state$.pipe(
      take(1),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => ({
        ...state,
        project: {
          ...state.project,
          boundMilestonesInformation: [
            ...state.project.boundMilestonesInformation,
            milestoneAdminProjectView,
          ],
        }
      })),
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  unbindMilestones(milestonesBindingIds: number[]): Observable<any> {
    return this.store.state$.pipe(
      take(1),
      switchMap((state: AdminProjectViewState) => this.unbindMilestonesServerSide(state, milestonesBindingIds)),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => this.updateStateWithUnboundMilestones(state, milestonesBindingIds)),
      tap(state => this.store.commit(state)),
    );
  }

  private unbindMilestonesServerSide(state: AdminProjectViewState, milestonesBindingIds: number[]): Observable<void> {
    const urlParts = [
      'milestone-binding/project',
      state.project.id.toString(),
      'unbind-milestones',
      milestonesBindingIds.join(',')
    ];
    return this.restService.delete(urlParts);
  }

  private updateStateWithUnboundMilestones(state: AdminProjectViewState, milestonesBindingIds: number[]): AdminProjectViewState {
    const updatedBoundMilestonesInfo = state.project.boundMilestonesInformation
      .filter(boundMilestone => !milestonesBindingIds.includes(boundMilestone.milestone.id));

    return {
      ...state,
      project: {
        ...state.project,
        boundMilestonesInformation: updatedBoundMilestonesInfo,
      }
    };
  }

  private initializeAdminProject(project: ProjectView) {
    const attachmentEntityState = this.initializeAttachmentState(project.attachmentList.attachments);
    const projectState: AdminProjectState = {
      ...project,
      attachmentList: {id: project.attachmentList.id, attachments: attachmentEntityState},
    };
    this.initializeEntityState(projectState);
  }

  private countCustomFieldBindings(customFieldBindings: Bindings): number {
    const lengthsByKey = Object.keys(customFieldBindings).map(key => {
      const bindings: CustomFieldBinding[] = customFieldBindings[key];
      return bindings.length;
    });

    return lengthsByKey.reduce((acc, curr) => acc + curr, 0);
  }

  // Implements GenericEntityViewService
  protected getRootUrl(initialState?): string {
    return 'generic-projects';
  }

  protected initializeAttachmentState(attachmentModels: AttachmentModel[]): AttachmentState {
    return this.attachmentHelper.initializeAttachmentState(attachmentModels);
  }

  enablePlugin(tmProjectId: number, pluginId: string): Observable<AdminProjectViewState> {
    return this.store.state$.pipe(
      take(1),
      switchMap(() => {
        const urlParts = ['generic-projects', tmProjectId.toString(), 'plugins', pluginId];
        return this.restService.post<{ hasValidConfiguration: boolean }>(urlParts);
      }),
      withLatestFrom(this.store.state$),
      map(([response, state]: [any, AdminProjectViewState]) => {
        const hasValidConfiguration = Boolean(response.hasValidConfiguration);
        return this.updateAvailablePlugins(state, pluginId, hasValidConfiguration, true);
      }),
      tap(state => this.store.commit(state))
    );
  }

  disablePlugin(tmProjectId: number, pluginId: string, saveConf: Boolean): Observable<AdminProjectViewState> {
    return this.store.state$.pipe(
      take(1),
      switchMap(() => {
        const urlParts = ['generic-projects', tmProjectId.toString(), 'plugins', pluginId];
        return this.restService.delete(urlParts, {params: {'saveConf': saveConf.toString()}});
      }),
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => this.updateAvailablePlugins(state, pluginId, false, false)),
      tap(state => this.store.commit(state))
    );
  }

  private updateAvailablePlugins(state: AdminProjectViewState,
                                 pluginId: string,
                                 hasValidConfiguration: boolean,
                                 enabled: boolean)
    : AdminProjectViewState {
    const updatedAvailablePlugins: ProjectPlugin[] = state.project.availablePlugins
      .map((plugin: ProjectPlugin) => {
        if (plugin.id === pluginId) {
          plugin.enabled = enabled;
          plugin.hasValidConfiguration = hasValidConfiguration;
        }
        return plugin;
      });

    return {
      ...state,
      project: {
        ...state.project,
        availablePlugins: updatedAvailablePlugins,
      }
    };
  }

  changeAutomationWorkflowType(tmProjectId: number, workflowType: AutomationWorkflowTypes): Observable<any> {
    const urlParts = ['generic-projects', tmProjectId.toString(), 'automation-workflow-type'];
    return this.restService.post(urlParts, {automationWorkflowType: workflowType});
  }

  changeAutomationWorkflowTypeAndDeactivatePlugin(tmProjectId: number, workflowType: any) {
    return this.changeAutomationWorkflowType(tmProjectId, workflowType).pipe(
      withLatestFrom(this.state$),
      map(([, state]: [any, AdminProjectViewState]) => {
        return {
          ...state,
          project: {
            ...state.project,
            automationWorkflowType: workflowType
          }
        };
      }),
      tap((state: AdminProjectViewState) => this.store.commit(state)));
  }

  disabledWorkflowAutomJiraPlugin(projectId: number, workflowType: AutomationWorkflowTypes, saveConf: boolean)
    : Observable<AdminProjectViewState> {
    return this.disablePlugin(projectId, PluginId.WORKFLOW_AUTOMATION_JIRA, saveConf).pipe(
      take(1),
      concatMap((state: AdminProjectViewState) =>
        this.changeAutomationWorkflowType(projectId, workflowType).pipe(
          map(() => {
            return {
              ...state,
              project: {
                ...state.project,
                automationWorkflowType: workflowType
              }
            };
          }))
      ),
      tap((state: AdminProjectViewState) => this.store.commit(state)));
  }

  enableWorkflowAutomJiraPlugin(projectId: number, workflowType: AutomationWorkflowTypes): Observable<AdminProjectViewState> {
    return this.enablePlugin(projectId, PluginId.WORKFLOW_AUTOMATION_JIRA).pipe(
      take(1),
      concatMap((state: AdminProjectViewState) =>
        this.changeAutomationWorkflowType(projectId, workflowType).pipe(
          map(() => {
            return {
              ...state,
              project: {
                ...state.project,
                automationWorkflowType: workflowType
              }
            };
          }))),
      tap((state) => this.store.commit(state)));
  }

  private extendCustomFieldBindings(bindings: CustomFieldBinding[], customFields: CustomField[]): ExtendedCustomFieldBinding[] {
    return bindings.map(binding => {
      const customField = customFields.find(cuf => cuf.id === binding.customFieldId);

      return {
        ...binding,
        name: customField?.name,
        inputType: customField?.inputType,
        optional: customField?.optional,
      };
    });
  }

  updateAutomatedSuitesLifetime(viewState: AdminProjectViewComponentData, newLifetime: any): Observable<AdminProjectViewState> {
    const urlParts = [
      this.getRootUrl(),
      viewState.project.id.toString(),
      'automated-suites-lifetime'
    ];
    const automatedSuitesLifetime = newLifetime === '' ? null : Number(newLifetime);

    return this.restService.post(urlParts, {automatedSuitesLifetime}).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => this.updateStateWithNewAutomatedSuitesLifetime(state, newLifetime)),
      map(state => {
        this.store.commit(state);
        return state;
      }),
    );
  }

  private updateStateWithNewAutomatedSuitesLifetime(state: AdminProjectViewState, newLifetime: string): AdminProjectViewState {
    const lifetime = newLifetime === '' ? null : Number(newLifetime);
    return {
      ...state,
      project: {
        ...state.project,
        automatedSuitesLifetime: lifetime
      }
    };
  }

  updateBddImplementationTechnology(projectId: number, technology: BddImplementationTechnologyKeys): Observable<any> {
    const urlParts = [
      this.getRootUrl(),
      projectId.toString(),
      'bdd-implementation-technology'
    ];

    return this.restService.post(urlParts, {bddImplementationTechnology: technology}).pipe(
      withLatestFrom(this.store.state$),
      map(([, state]: [any, AdminProjectViewState]) => {
        const updatedScriptLanguage =
          technology === BddImplementationTechnology.CUCUMBER_4.id ||
          technology === BddImplementationTechnology.CUCUMBER_5_PLUS.id ?
            state.project.bddScriptLanguage : BddScriptLanguage.ENGLISH.id;
        return {
          ...state,
          project: {
            ...state.project,
            bddImplementationTechnology: technology,
            bddScriptLanguage: updatedScriptLanguage
          }
        };
      }),
      map(state => {
        this.store.commit((state));
        return state;
      })
    );
  }

  getCurrentUser(): Observable<AuthenticatedUser> {
    return this.adminReferentialDataService.authenticatedUser$;
  }

  connectToTAServer(taServerId: number, credentials: Credentials): Observable<any> {
    return this.restService.post(
      ['test-automation-servers', taServerId.toString(), 'credentials']
      , credentials
    );
  }

  getProjectStatusesInUse(projectId: number): Observable<StatusesInUseState> {
    return this.restService.get<StatusesInUseState>(['project-view', projectId.toString(), 'statuses-in-use']);
  }

  refreshStatusesInUse(): Observable<any> {
    return this.state$.pipe(
      take(1),
      concatMap((state: AdminProjectViewState) => this.getProjectStatusesInUse(state.project.id).pipe(
        withLatestFrom(this.state$),
        map(([statusesInUse, state]: [StatusesInUseState, AdminProjectViewState]) => ({
          ...state,
          project: {...state.project, statusesInUse}
        }))
      )),
      tap((state: AdminProjectViewState) => this.commit(state))
    );
  }

}

/**
 * Subset of the TestAutomationProject interface used server-side for creating a new binding.
 */
interface TestAutomationProjectFormModel {
  remoteName: string;
  canRunBdd: boolean;
  label: string;
}

/**
 * Model used when creating new CUF bindings
 */
interface CustomFieldBindingModel {
  projectId: number;
  customField: {
    id: number;
    itype: string;
  };
  boundEntity: {
    enumName: string;
  };
}

/**
 * Model used when updating an existing test automation project (a.k.a job)
 */
export interface UpdatedAutomationJob {
  canRunBdd: boolean;
  taProjectId: number;
  remoteName: string;
  label: string;
  executionEnvironment: string;
}

export const getProjectViewState = createFeatureSelector<AdminProjectViewState, AdminProjectState>('project');

/** Denormalized custom field binding to show in ProjectCustomFieldsPanel's grid */
export class ExtendedCustomFieldBinding extends CustomFieldBinding {
  name: string;
  inputType: string;
  optional: boolean;
}

import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, InjectionToken} from '@angular/core';
import {
  AbstractAdministrationCreationDialogDirective
} from '../../../../../components/abstract-administration-creation-dialog';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {
  RestService,
  DialogReference,
  GridService,
  CreationDialogData,
  DisplayOption, FieldValidationError, isCUFCodePatternValid,
  deleteColumn,
  GridDefinition,
  smallGrid,
  textColumn,
  Extendable,
  StyleDefinitionBuilder,
  ReferentialDataService,
  gridServiceFactory,
  EvInputType
} from 'sqtm-core';
import {map, take} from 'rxjs/operators';
import {
  EnvironmentVariableOption,
  EnvironmentVariableOptionService
} from '../../../services/environment-variable-option.service';
import {
  DeleteEnvironmentVariableOptionComponent
} from '../../cell-renderers/delete-environment-variable-option/delete-environment-variable-option.component';

export enum EnvironmentVariableCreationDialogFields {
  name = 'name',
  code = 'code',
  evInputType = 'evInputType',
  dropdownListOptionLabel = 'dropdownListOptionLabel',
  dropdownListOptionCode = 'dropdownListOptionCode'
}

const Fields = EnvironmentVariableCreationDialogFields;

export const ENVIRONMENT_VARIABLE_OPTIONS_TABLE_CONF = new InjectionToken('ENVIRONMENT_VARIABLE_OPTIONS_TABLE_CONF');
export const ENVIRONMENT_VARIABLE_OPTIONS_TABLE = new InjectionToken('ENVIRONMENT_VARIABLE_OPTIONS_TABLE');

export function environmentVariableOptionsTableDefinition(): GridDefinition {
  return smallGrid('environment-variable-options')
    .withColumns([
      textColumn('label')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .disableSort(),
      textColumn('code')
        .withI18nKey('sqtm-core.generic.label.code')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .disableSort(),
      deleteColumn(DeleteEnvironmentVariableOptionComponent),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-environment-variable-creation-dialog',
  templateUrl: './environment-variable-creation-dialog.component.html',
  styleUrls: ['./environment-variable-creation-dialog.component.less'],
  providers: [
    {
      provide: ENVIRONMENT_VARIABLE_OPTIONS_TABLE_CONF,
      useFactory: environmentVariableOptionsTableDefinition
    },
    {
      provide: ENVIRONMENT_VARIABLE_OPTIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ENVIRONMENT_VARIABLE_OPTIONS_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: ENVIRONMENT_VARIABLE_OPTIONS_TABLE
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EnvironmentVariableCreationDialogComponent extends AbstractAdministrationCreationDialogDirective implements OnInit {
  formGroup: FormGroup;
  dropdownListFormGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;
  evInputTypeOptions: DisplayOption[];
  private environmentVariableOptions$: Observable<EnvironmentVariableOption[]>;
  private _hasDropdownListDefaultError = new BehaviorSubject<boolean>(false);
  public hasDropdownListDefaultError$ = this._hasDropdownListDefaultError.asObservable();

  protected doResetForm() {
    this.resetFormControl(Fields.name, '');
    this.resetFormControl(Fields.code, '');
    this.resetEnvironmentOptionListForm();
    this.environmentVariableOptionService.initialize();
  }

  protected getRequestPayload(): Observable<any> {
    const inputType = this.getFormControlValue(Fields.evInputType);

    return this.environmentVariableOptions$.pipe(
      take(1),
      map((opts: EnvironmentVariableOption[]) => {
        const options = EnvironmentVariableCreationDialogComponent.getEnvironmentVariableOptionsForRequestPayload(opts);
        return {
          name: this.getFormControlValue(Fields.name),
          code: this.getFormControlValue(Fields.code),
          inputType,
          options
        };
      })
    );
  }

  get textFieldToFocus(): string {
    return Fields.name;
  }

  constructor(private fb: FormBuilder,
              private translateService: TranslateService,
              private gridService: GridService,
              private environmentVariableOptionService: EnvironmentVariableOptionService,
              dialogReference: DialogReference,
              restService: RestService,
              cdr: ChangeDetectorRef) {
    super('environment-variables/new', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;

    this.initializeEvInputTypeField();
  }

  get isDropdownList(): boolean {
    return this.getFormControlValue(Fields.evInputType) === EvInputType.DROPDOWN_LIST;
  }

  ngOnInit(): void {
    this.initializeFormGroup();
    this.initializeDropdownListFormGroup();
    this.initializeEnvironmentVariableOptionsTable();
  }


  private initializeEvInputTypeField(): void {
    this.evInputTypeOptions = this.retrieveAllEvInputTypesAsDisplayOption();
  }



  private retrieveAllEvInputTypesAsDisplayOption(): DisplayOption[] {
    return [
      this.transformInputTypeInDisplayOption(EvInputType.PLAIN_TEXT),
      this.transformInputTypeInDisplayOption(EvInputType.DROPDOWN_LIST)
    ];
  }

  private transformInputTypeInDisplayOption(type: EvInputType): DisplayOption {
    return {
      id: type,
      label: this.translateService.instant('sqtm-core.entity.environment-variable.' + type),
    };
  }



  private initializeFormGroup(): void {
    this.formGroup = this.fb.group({
        name: this.fb.control('', [
          Validators.required,
          Validators.pattern('(.|\\s)*\\S(.|\\s)*'),
          Validators.maxLength(255)
        ]),
        code: this.fb.control('', [
          Validators.required,
          Validators.maxLength(30),
          this.codeValidator,
        ]),
        evInputType: this.fb.control(this.evInputTypeOptions[0].id)
      },
    );
  }

  private initializeEnvironmentVariableOptionsTable(): void {
    this.environmentVariableOptionService.initialize();
    this.environmentVariableOptions$ = this.environmentVariableOptionService.environmentVariableOptions$;
    this.gridService.connectToDatasource(this.environmentVariableOptions$, 'id');
  }

  private initializeDropdownListFormGroup(): void {
    this.dropdownListFormGroup = this.fb.group({
        dropdownListOptionLabel: this.fb.control('', [
          Validators.maxLength(255),
          Validators.required,
        ]),
        dropdownListOptionCode: this.fb.control('', [
          Validators.maxLength(30),
          Validators.required,
          this.codeValidator,
        ]),
      }
    );
  }

  private get codeValidator(): ValidatorFn {
    return function (formControl: AbstractControl): ValidationErrors {
      const isCodeValid = isCUFCodePatternValid(formControl.value);
      return isCodeValid ? null : {invalidCodePattern: true};
    };
  }

  addEnvironmentOption(): void {
    const optionName = this.getEnvironmentOptionListFormControlValue(Fields.dropdownListOptionLabel);
    const optionCode = this.getEnvironmentOptionListFormControlValue(Fields.dropdownListOptionCode);

    if (this.dropdownListFormIsValid()) {
      this.environmentVariableOptionService.addOption(optionName, optionCode).subscribe(() => {
          this.resetEnvironmentOptionListForm();
        }
      );
    } else {
      this.showDropdownListFormGroupClientSideError();
    }
  }

  private showDropdownListFormGroupClientSideError(): void {
    this.textFields.filter((textField) => {
      return textField.fieldName === Fields.dropdownListOptionLabel ||
        textField.fieldName === Fields.dropdownListOptionCode;
    }).forEach(filteredTextField => filteredTextField.showClientSideError());
  }

  private resetEnvironmentOptionListForm(): void {
    this.resetDropdownListFormControl(Fields.dropdownListOptionLabel, '');
    this.resetDropdownListFormControl(Fields.dropdownListOptionCode, '');
    this.removeDropdownListFormGroupClientSideError();
    this.textFields.filter(textField => textField.fieldName === Fields.dropdownListOptionLabel).forEach(textField => textField.grabFocus());
  }

  private removeDropdownListFormGroupClientSideError(): void {
    this.serverSideValidationErrors = [];
    this.cdr.markForCheck();
  }

  private resetDropdownListFormControl(fieldName: string, value: any): void {
    this.getDropdownListFormControl(fieldName).reset(value);
  }

  private getDropdownListFormControl(fieldName: string): AbstractControl {
    return this.dropdownListFormGroup.controls[fieldName];
  }

  private getEnvironmentOptionListFormControlValue(fieldName: string): any {
    return this.getDropdownListFormControl(fieldName).value;
  }

  private dropdownListFormIsValid(): boolean {
    this.initializeErrorsForDuplicateOptionLabelOrCode();
    return this.dropdownListFormGroup.status === 'VALID';
  }

  private initializeErrorsForDuplicateOptionLabelOrCode(): void {
    const optionName = this.getEnvironmentOptionListFormControlValue(Fields.dropdownListOptionLabel);
    const optionCode = this.getEnvironmentOptionListFormControlValue(Fields.dropdownListOptionCode);
    this.environmentVariableOptions$.pipe(
      take(1),
      map((options) => {
        if (this.checkOptionCodeAlreadyExist(optionCode, options)) {
          this.getDropdownListFormControl(Fields.dropdownListOptionCode).setErrors({optionCodeAlreadyExists: true});
        }

        if (this.checkOptionLabelAlreadyExist(optionName, options)) {
          this.getDropdownListFormControl(Fields.dropdownListOptionLabel).setErrors({optionNameAlreadyExists: true});
        }
      })
    ).subscribe();
  }

  private checkOptionCodeAlreadyExist(code: string, options: EnvironmentVariableOption[]): boolean {
    return options.find(option => option.code === code) != null;
  }

  private checkOptionLabelAlreadyExist(label: string, options: EnvironmentVariableOption[]): boolean {
    return options.find(option => option.label === label) != null;
  }

  private static getEnvironmentVariableOptionsForRequestPayload(opts: EnvironmentVariableOption[]) {
    return opts.map((option: EnvironmentVariableOption) => {
      return {label: option.label, code: option.code};
    });
  }

}

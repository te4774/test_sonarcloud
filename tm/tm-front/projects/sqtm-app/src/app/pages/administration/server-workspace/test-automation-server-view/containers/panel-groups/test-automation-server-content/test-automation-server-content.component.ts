import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
  Inject,
  ViewChild
} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {finalize, takeUntil} from 'rxjs/operators';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';
import {AdminTestAutomationServerViewState} from '../../../states/admin-test-automation-server-view-state';
import {GridService, gridServiceFactory, Identifier, ListItem, ReferentialDataService, RestService} from 'sqtm-core';
import {
  TEST_AUTOMATION_SERVER_EV_TABLE,
  TEST_AUTOMATION_SERVER_EV_TABLE_CONF,
  TestAutomationServerEnvironmentVariablesPanelComponent,
  testAutomationServerEvTableDefinition
} from '../../../components/panels/tas-environement-variables-panel/tas-environment-variables-panel.component';

@Component({
  selector: 'sqtm-app-test-automation-server-content',
  templateUrl: './test-automation-server-content.component.html',
  styleUrls: ['./test-automation-server-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TEST_AUTOMATION_SERVER_EV_TABLE_CONF,
      useFactory: testAutomationServerEvTableDefinition
    },
    {
      provide: TEST_AUTOMATION_SERVER_EV_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TEST_AUTOMATION_SERVER_EV_TABLE_CONF, ReferentialDataService]
    }
  ]
})
export class TestAutomationServerContentComponent implements OnInit, OnDestroy  {

  componentData$: Observable<AdminTestAutomationServerViewState>;
  unsub$ = new Subject<void>();

  @ViewChild(TestAutomationServerEnvironmentVariablesPanelComponent)
  private testAutomationServerEnvironmentVariablesPanelComponent;

  constructor(public testAutomationServerViewService: TestAutomationServerViewService,
              @Inject(TEST_AUTOMATION_SERVER_EV_TABLE) public evGridService: GridService) {
    this.componentData$ = testAutomationServerViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit(): void {
    this.componentData$ = this.testAutomationServerViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openEnvironmentVariableSelector($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.testAutomationServerEnvironmentVariablesPanelComponent.openBindEnvironmentVariables();
  }

  deleteEnvironmentVariableBindings($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.testAutomationServerEnvironmentVariablesPanelComponent.openUnbindEnvironmentVariableDialog();
  }

  bindEnvironmentVariable(items: ListItem[]) {
    this.evGridService.beginAsyncOperation();
    const selectedItemIds: Identifier[] = items.map(item => item.id);
    this.testAutomationServerViewService.bindEnvironmentVariables(selectedItemIds).pipe(
      finalize(() => this.evGridService.completeAsyncOperation())
    ).subscribe();
  }
}

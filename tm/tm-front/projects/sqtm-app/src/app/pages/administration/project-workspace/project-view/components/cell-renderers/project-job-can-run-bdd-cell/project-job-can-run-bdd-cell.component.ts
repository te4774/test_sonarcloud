import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, Fixed, GridService} from 'sqtm-core';
import {ProjectViewService} from '../../../services/project-view.service';

@Component({
  selector: 'sqtm-app-project-job-can-run-bdd-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column radio-button-container">
        <label nz-checkbox  style="margin: auto"
               [(ngModel)]="this.row.data[this.columnDisplay.id]"
               (nzCheckedChange)="handleClick($event)">
        </label>
      </div>
    </ng-container>`,
  styleUrls: ['./project-job-can-run-bdd-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectJobCanRunBddCellComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef, public projectViewService: ProjectViewService) {
    super(grid, cdRef);
  }

  ngOnInit(): void {
  }

  handleClick(canRunBdd: boolean) {
    this.projectViewService.setJobCanRunBdd(this.row.data.taProjectId, canRunBdd);
  }
}

export function projectJobCanRunBddColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withHeaderPosition('center')
    .withRenderer(ProjectJobCanRunBddCellComponent)
    .withI18nKey('sqtm-core.administration-workspace.views.project.automation.jobs.can-run-bdd')
    .changeWidthCalculationStrategy(new Fixed(120));
}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {
  Credentials,
  doesHttpErrorContainsSquashActionError,
  extractSquashActionError,
  TextFieldComponent,
  ThirdPartyCredentialsFormComponent
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {
  AdminTestAutomationServerViewComponentData
} from '../../../containers/test-automation-server-view/test-automation-server-view.component';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';
import {AdminTestAutomationServerState} from '../../../states/admin-test-automation-server-state';

const TRANSLATE_KEYS_BASE = 'sqtm-core.administration-workspace.bugtrackers.authentication-policy.';

@Component({
  selector: 'sqtm-app-taserver-auth-policy-panel',
  templateUrl: './taserver-auth-policy-panel.component.html',
  styleUrls: ['./taserver-auth-policy-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TAServerAuthPolicyPanelComponent {

  @Input()
  componentData: AdminTestAutomationServerViewComponentData;

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  @ViewChild(ThirdPartyCredentialsFormComponent)
  credentialsForm: ThirdPartyCredentialsFormComponent;
  credentialsStatusMessage = '';
  statusIcon: 'DANGER' | 'INFO' = null;

  translateKeys = {
    saveSuccessKey: TRANSLATE_KEYS_BASE + 'credentials.save-success',
  };

  get modificationWarning(): string {
    if (this.componentData.testAutomationServer.supportsAutomatedExecutionEnvironments) {
      if (this.componentData.testAutomationServer.environmentTags?.length > 0) {
        const key = 'sqtm-core.administration-workspace.servers.test-automation-servers.auth-policy.modification-warning';
        return this.translateService.instant(key);
      }
    }

    return null;
  }

  constructor(public readonly taServerViewService: TestAutomationServerViewService,
              private readonly cdRef: ChangeDetectorRef,
              private readonly translateService: TranslateService) {
    this.observeAuthProtocolChange();
  }

  private observeAuthProtocolChange(): void {
    this.taServerViewService.externalRefreshRequired$.subscribe(update => {
      if (update.key === 'authProtocol') {
        this.credentialsForm.authenticationProtocol = update.value;
        this.clearMessage();
      }
    });
  }

  sendCredentialsForm(credentials: Credentials) {
    this.clearMessage();

    this.taServerViewService.setCredentials(credentials).subscribe(
      () => this.handleCredentialsSaveSuccess(),
      (err) => this.handleCredentialsError(err),
    );
  }

  private clearMessage(): void {
    this.credentialsStatusMessage = null;
    this.statusIcon = null;
  }

  private handleCredentialsSaveSuccess(): void {
    this.credentialsStatusMessage = this.translateKeys.saveSuccessKey;
    this.statusIcon = 'INFO';
    this.credentialsForm.formGroup.markAsPristine();
    this.credentialsForm.endAsync();
  }

  private handleCredentialsError(error: any): void {
    this.credentialsForm.endAsync();

    if (doesHttpErrorContainsSquashActionError(error)) {
      const squashError = extractSquashActionError(error);
      this.credentialsStatusMessage = this.translateService.instant(squashError.actionValidationError.i18nKey);
      this.cdRef.markForCheck();
      this.statusIcon = 'DANGER';
    } else {
      // Default error handling
      console.error(error);
    }
  }

  deleteCredentials(testAutomationServer: AdminTestAutomationServerState) {
    this.taServerViewService.deleteCredentials(testAutomationServer.id).subscribe( ()=> this.clearMessage());
  }
}

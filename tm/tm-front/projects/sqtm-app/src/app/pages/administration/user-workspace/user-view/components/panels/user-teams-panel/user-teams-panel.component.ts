import {ChangeDetectionStrategy, Component, InjectionToken, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {
  DataRow,
  deleteColumn,
  DialogService,
  Extendable,
  GridDefinition,
  GridFilter,
  GridService,
  indexColumn,
  smallGrid,
  StyleDefinitionBuilder
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {AdminUserViewComponentData} from '../../../containers/user-view/user-view.component';
import {getUserViewState, UserViewService} from '../../../services/user-view.service';
import {concatMap, filter, finalize, map, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {createSelector, select} from '@ngrx/store';
import {AdminUserState} from '../../../states/admin-user-state';
import {
  AssociateTeamToUserDialogComponent
} from '../../dialogs/associate-team-to-user-dialog/associate-team-to-user-dialog.component';
import {
  RemoveUserAssociatedTeamCellComponent
} from '../../cell-renderers/remove-user-associated-team-cell/remove-user-associated-team-cell.component';
import {clickableTeamNameColumn} from '../../../../../cell-renderer.builders';

export const USER_TEAMS_TABLE_CONF = new InjectionToken('USER_AUTHORISATIONS_TABLE_CONF');
export const USER_TEAMS_TABLE = new InjectionToken('USER_AUTHORISATIONS_TABLE');

export function userTeamsTableDefinition(): GridDefinition {
  return smallGrid('user-teams')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      clickableTeamNameColumn('name')
        .withI18nKey('sqtm-core.entity.team.label')
        .changeWidthCalculationStrategy(new Extendable(200, 0.5)),
      deleteColumn(RemoveUserAssociatedTeamCellComponent),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .enableMultipleColumnsFiltering(['name'])
    .build();
}

@Component({
  selector: 'sqtm-app-user-teams-panel',
  template: `
    <div class="m-t-10 m-l-10 m-b-15 flex-fixed-size" style="width: 200px">
        <sqtm-core-text-research-field (newResearchValue)="handleResearchInput($event)"></sqtm-core-text-research-field>
    </div>
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./user-teams-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: USER_TEAMS_TABLE
    }
  ]
})
export class UserTeamsPanelComponent implements OnInit, OnDestroy  {

  private componentData$: Observable<AdminUserViewComponentData>;

  private unsub$ = new Subject<void>();

  constructor(private userViewService: UserViewService,
              public gridService: GridService,
              private dialogService: DialogService,
              private vcRef: ViewContainerRef) {
  }

  ngOnInit(): void {
    this.componentData$ = this.userViewService.componentData$;
    this.initializeTable();
    this.initializeFilters();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  refreshGrid() {
    this.gridService.refreshData();
  }

  openAssociateTeamToUserDialog() {
    const dialogRef = this.dialogService.openDialog({
      id: 'associate-team-to-user-dialog',
      component: AssociateTeamToUserDialogComponent,
      viewContainerReference: this.vcRef,
      width: 720,
      top: '100px'
    });

    dialogRef.dialogClosed$.subscribe((result) => {
      if (result) {
        this.refreshGrid();
      }
    });
  }

  removeTeamAssociations(): void {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.data.partyId)),
      concatMap((partyIds: number[]) => this.showConfirmDeleteAssociatedTeamDialog(partyIds)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      switchMap(({partyIds}) => this.userViewService.removeTeamAssociation(partyIds)),
      finalize(() => this.gridService.completeAsyncOperation()),
    ).subscribe();
  }

  private showConfirmDeleteAssociatedTeamDialog(partyIds): Observable<{ confirmDelete: boolean, partyIds: number[] }> {

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.users.dialog.title.remove-associated-team.remove-many',
      messageKey: 'sqtm-core.administration-workspace.users.dialog.message.remove-associated-team.remove-many',
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, partyIds}))
    );
  }


  private initializeFilters() {
    const filters: GridFilter[] = [{
      id: 'name', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false
    }];
    this.gridService.addFilters(filters);
  }

  handleResearchInput($event: string) {
    this.gridService.applyMultiColumnsFilter($event);
  }

  private initializeTable() {
    const teamsTable = this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createSelector(getUserViewState, (userState: AdminUserState) => userState.teams)),
    );

    this.gridService.connectToDatasource(teamsTable, 'partyId');
  }
}

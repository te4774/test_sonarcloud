import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, Fixed, GridService, Identifier} from 'sqtm-core';
import {Subject} from 'rxjs';
import {AddJobDialogService} from '../../../services/add-job-dialog.service';

@Component({
  selector: 'sqtm-app-add-job-bdd-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div *ngIf="isRowSelected(grid.selectedRowIds$ | async)"
           class="full-width full-height flex-column radio-button-container">
        <label nz-checkbox style="margin: auto"
               [nzChecked]="this.row.data[this.columnDisplay.id]"
               (nzCheckedChange)="handleClick($event)">
        </label>
      </div>
    </ng-container>`,
  styleUrls: ['./add-job-bdd-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class AddJobBddCellComponent extends AbstractCellRendererComponent implements OnDestroy {
  unsub$ = new Subject<void>();

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef, private readonly addJobDialogService: AddJobDialogService) {
    super(grid, cdRef);
  }

  isRowSelected(selectedRowIds: Identifier[]): boolean {
    return selectedRowIds.includes(this.row.id);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  handleClick(canRunBdd: boolean) {
    this.addJobDialogService.setCanRunBdd(this.row.id as string, canRunBdd);
  }
}

export function addJobBddColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withHeaderPosition('center')
    .withRenderer(AddJobBddCellComponent)
    .withI18nKey('sqtm-core.administration-workspace.views.project.automation.jobs.can-run-bdd')
    .changeWidthCalculationStrategy(new Fixed(120));
}

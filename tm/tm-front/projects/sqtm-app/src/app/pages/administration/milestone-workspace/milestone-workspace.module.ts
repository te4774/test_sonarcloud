import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MilestoneWorkspaceComponent
} from './milestone-workspace/containers/milestone-workspace/milestone-workspace.component';
import {
  MilestoneWorkspaceGridComponent
} from './milestone-workspace/containers/milestone-workspace-grid/milestone-workspace-grid.component';
import {TranslateModule} from '@ngx-translate/core';
import {
  AnchorModule,
  CellRendererCommonModule,
  DialogModule,
  GridModule,
  NavBarModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule
} from 'sqtm-core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzCollapseModule} from 'ng-zorro-antd/collapse';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzFormModule} from 'ng-zorro-antd/form';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzRadioModule} from 'ng-zorro-antd/radio';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {NzTypographyModule} from 'ng-zorro-antd/typography';
import {
  MilestoneCreationDialogComponent
} from './milestone-workspace/components/dialogs/milestone-creation-dialog/milestone-creation-dialog.component';
import {
  MilestoneRangeCellRendererComponent
} from './milestone-workspace/components/cell-renderers/milestone-range-cell-renderer/milestone-range-cell-renderer.component';
import {
  MilestoneStatusCellRendererComponent
} from './milestone-workspace/components/cell-renderers/milestone-status-cell-renderer/milestone-status-cell-renderer.component';
import {
  DeleteMilestoneCellRendererComponent
} from './milestone-workspace/components/cell-renderers/delete-milestone-cell-renderer/delete-milestone-cell-renderer.component';
import {RouterModule, Routes} from '@angular/router';
import {CKEditorModule} from 'ckeditor4-angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MilestoneOwnerCellRendererComponent
} from './milestone-workspace/components/cell-renderers/milestone-owner-cell-renderer/milestone-owner-cell-renderer.component';
import {MilestoneViewComponent} from './milestone-view/containers/milestone-view/milestone-view.component';
import {
  MilestoneContentComponent
} from './milestone-view/containers/panel-groups/milestone-content/milestone-content.component';
import {
  MilestoneDuplicationDialogComponent
} from './milestone-workspace/components/dialogs/milestone-duplication-dialog/milestone-duplication-dialog.component';
import {
  MilestoneSynchronizationDialogComponent
} from './milestone-workspace/components/dialogs/milestone-synchronization-dialog/milestone-synchronization-dialog.component';
import {
  MilestoneInformationPanelComponent
} from './milestone-view/components/panels/milestone-information-panel/milestone-information-panel.component';
import {
  MilestoneProjectPanelComponent
} from './milestone-view/components/panels/milestone-project-panel/milestone-project-panel.component';
import {
  UnbindProjectFromMilestoneCellComponent
} from './milestone-view/components/cell-renderers/unbind-project-from-milestone-cell/unbind-project-from-milestone-cell.component';
import {
  ProjectBoundToMilestoneCellComponent
} from './milestone-view/components/cell-renderers/project-bound-to-milestone-cell/project-bound-to-milestone-cell.component';
import {
  BindProjectsToMilestoneDialogComponent
} from './milestone-view/components/dialogs/bind-projects-to-milestone-dialog/bind-projects-to-milestone-dialog.component';
import {AdminViewHeaderModule} from '../components/admin-view-header/admin-view-header.module';
import {
  MilestoneViewDetailComponent
} from './milestone-view/containers/milestone-view-detail/milestone-view-detail.component';
import {
  MilestoneViewWithGridComponent
} from './milestone-view/containers/milestone-view-with-grid/milestone-view-with-grid.component';

const routes: Routes = [
  {
    path: '',
    component: MilestoneWorkspaceComponent,
    children: [
      {
        path: ':milestoneId',
        component: MilestoneViewWithGridComponent,
        children: [
          {
            path: '',
            redirectTo: 'content',
          },
          {
            path: 'content',
            component: MilestoneContentComponent,
          },
        ]
      },
    ],
  },
  {
    path: 'detail/:milestoneId',
    component: MilestoneViewDetailComponent,
    children: [
      {
        path: '',
        redirectTo: 'content',
      },
      {
        path: 'content',
        component: MilestoneContentComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [
    MilestoneWorkspaceComponent,
    MilestoneWorkspaceGridComponent,
    MilestoneCreationDialogComponent,
    MilestoneRangeCellRendererComponent,
    MilestoneStatusCellRendererComponent,
    DeleteMilestoneCellRendererComponent,
    MilestoneOwnerCellRendererComponent,
    MilestoneSynchronizationDialogComponent,
    MilestoneDuplicationDialogComponent,
    MilestoneViewComponent,
    MilestoneContentComponent,
    MilestoneInformationPanelComponent,
    MilestoneProjectPanelComponent,
    UnbindProjectFromMilestoneCellComponent,
    ProjectBoundToMilestoneCellComponent,
    BindProjectsToMilestoneDialogComponent,
    MilestoneViewDetailComponent,
    MilestoneViewWithGridComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    GridModule,
    NzIconModule,
    NzDropDownModule,
    CKEditorModule,
    WorkspaceCommonModule,
    DialogModule,
    ReactiveFormsModule,
    WorkspaceLayoutModule,
    CellRendererCommonModule,
    NzButtonModule,
    NzToolTipModule,
    NzCheckboxModule,
    NavBarModule,
    NzRadioModule,
    FormsModule,
    NzTypographyModule,
    AnchorModule,
    NzCollapseModule,
    AdminViewHeaderModule,
    NzFormModule,
    UiManagerModule,
  ]
})
export class MilestoneWorkspaceModule {
}

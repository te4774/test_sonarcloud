import {AdminBugTrackerState, GenericEntityViewState, provideInitialGenericViewState} from 'sqtm-core';

export interface AdminBugTrackerViewState extends GenericEntityViewState<AdminBugTrackerState, 'bugTracker'> {
  bugTracker: AdminBugTrackerState;
}

export function provideInitialAdminBugTrackerView(): Readonly<AdminBugTrackerViewState> {
  return provideInitialGenericViewState<AdminBugTrackerState, 'bugTracker'>('bugTracker');
}

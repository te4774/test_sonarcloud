import {Component, OnInit, ChangeDetectionStrategy, OnDestroy, Inject, ViewChild} from '@angular/core';
import {AdminEnvironmentVariableViewState} from '../../../states/admin-environment-variable-view-state';
import {Observable, Subject} from 'rxjs';
import {EnvironmentVariableViewService} from '../../../services/environment-variable-view.service';
import {takeUntil} from 'rxjs/operators';
import {
  EnvironmentVariableOptionsPanelComponent,
  EV_OPTIONS_TABLE,
  EV_OPTIONS_TABLE_CONF, evOptionsTableDefinition
} from '../../../components/panels/environment-variable-options-panel/environment-variable-options-panel.component';
import {GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {EvOptionsServerOperationHandler} from '../../../services/ev-options-server-operation-handler';

@Component({
  selector: 'sqtm-app-environment-variable-content',
  templateUrl: './environment-variable-content.component.html',
  styleUrls: ['./environment-variable-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: EV_OPTIONS_TABLE_CONF,
      useFactory: evOptionsTableDefinition
    },
    {
      provide: EvOptionsServerOperationHandler,
      useClass: EvOptionsServerOperationHandler,
      deps: [EnvironmentVariableViewService]
    },
    {
      provide: EV_OPTIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, EV_OPTIONS_TABLE_CONF, ReferentialDataService, EvOptionsServerOperationHandler]
    }
  ]
})
export class EnvironmentVariableContentComponent implements OnInit, OnDestroy {
  componentData$: Observable<AdminEnvironmentVariableViewState>;
  unsub$ = new Subject<void>();

  @ViewChild(EnvironmentVariableOptionsPanelComponent)
  private environmentVariableOptionsPanelComponent;

  constructor(private readonly environmentVariableViewService: EnvironmentVariableViewService,
              @Inject(EV_OPTIONS_TABLE) public optionsGridService: GridService) {

  }

  ngOnInit(): void {
    this.componentData$ = this.environmentVariableViewService.componentData$.pipe(
      takeUntil(this.unsub$)
    );
  }

  isDropdownList(componentData: AdminEnvironmentVariableViewState): boolean {
    return componentData.environmentVariable.inputType === 'DROPDOWN_LIST';
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  addEnvironmentVariableOptions($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.environmentVariableOptionsPanelComponent.openAddEnvironmentVariableOptionDialog();
  }

  deleteEnvironmentVariableOptions($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.environmentVariableOptionsPanelComponent.openDeleteEnvironmentVariableOptionsDialog();
  }
}

import {ChangeDetectionStrategy, Component} from '@angular/core';
import {GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-project-view-with-grid',
  templateUrl: './project-view-with-grid.component.html',
  styleUrls: ['./project-view-with-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectViewWithGridComponent {

  constructor(public readonly gridService: GridService) {
  }

  refreshGrid(keepSelectedRows: boolean): void {
    if (keepSelectedRows) {
      this.gridService.refreshDataAndKeepSelectedRows();
    } else {
      this.gridService.refreshData();
    }
  }
}

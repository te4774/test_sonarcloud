import {ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  bigGrid,
  booleanColumn,
  dateColumn,
  DialogConfiguration,
  DialogService,
  Extendable,
  FilterOperation,
  Fixed,
  GridDefinition,
  GridService,
  GridWithStatePersistence,
  indexColumn,
  selectableTextColumn,
  textColumn,
  TextResearchFieldComponent,
  WorkspaceWithGridComponent
} from 'sqtm-core';
import {
  templateColumn
} from '../../components/cell-renderers/project-template-cell-renderer/project-template-cell-renderer.component';
import {Observable, Subject} from 'rxjs';
import {concatMap, filter, map, take, takeUntil, tap} from 'rxjs/operators';
import {
  ProjectCreationDialogComponent
} from '../../components/dialogs/project-creation-dialog/project-creation-dialog.component';
import {
  TemplateCreationDialogComponent
} from '../../components/dialogs/template-creation-dialog/template-creation-dialog.component';
import {
  TemplateFromProjectCreationDialogComponent
} from '../../components/dialogs/template-from-project-creation-dialog/template-from-project-creation-dialog.component';
import {AbstractAdministrationNavigation} from '../../../../components/abstract-administration-navigation';
import {ActivatedRoute, Router} from '@angular/router';

export function adminProjectTableDefinition(): GridDefinition {
  return bigGrid('projects')
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Fixed(60))
        .withViewport('leftViewport'),
      selectableTextColumn('name')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(250, 0.5)),
      templateColumn('isTemplate')
        .withHeaderPosition('center')
        .withI18nKey('sqtm-core.entity.project.template.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.project.template.label.singular')
        .changeWidthCalculationStrategy(new Fixed(60)),
      textColumn('label')
        .withI18nKey('sqtm-core.entity.project.label.tag')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      dateColumn('createdOn')
        .withI18nKey('sqtm-core.entity.generic.created-on.masculine')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      textColumn('createdBy')
        .withI18nKey('sqtm-core.entity.generic.created-by.masculine')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      dateColumn('lastModifiedOn')
        .withI18nKey('sqtm-core.entity.generic.last-modified-on.masculine')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      textColumn('lastModifiedBy')
        .withI18nKey('sqtm-core.entity.generic.last-modified-by.masculine')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      booleanColumn('hasPermissions')
        .withI18nKey('sqtm-core.entity.project.authorizations.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.project.authorizations.label.plural')
        .changeWidthCalculationStrategy(new Fixed(80))
        .withHeaderPosition('center')
        .withContentPosition('center'),
      textColumn('bugtrackerName')
        .withI18nKey('sqtm-core.entity.bugtracker.label.short')
        .withTitleI18nKey('sqtm-core.entity.bugtracker.label.singular')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      textColumn('executionServer')
        .withI18nKey('sqtm-core.entity.execution-server.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.execution-server.label.singular')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
    ]).server().withServerUrl(['generic-projects'])
    .enableMultipleColumnsFiltering(['name', 'label', 'lastModifiedBy', 'createdBy'])
    .disableRightToolBar()
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-project-grid',
  templateUrl: './project-grid.component.html',
  styleUrls: ['./project-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectGridComponent extends AbstractAdministrationNavigation implements OnInit, OnDestroy {

  unsub$ = new Subject<void>();

  authenticatedUser$: Observable<AuthenticatedUser>;

  oneProjectIsSelected$: Observable<boolean>;

  protected readonly entityIdPositionInUrl = 2;

  @ViewChild(TextResearchFieldComponent)
  searchField: TextResearchFieldComponent;

  constructor(public gridService: GridService,
              private dialogService: DialogService,
              private adminReferentialDataService: AdminReferentialDataService,
              private viewContainerRef: ViewContainerRef,
              private workspaceWithGrid: WorkspaceWithGridComponent,
              private gridWithStatePersistence: GridWithStatePersistence,
              protected route: ActivatedRoute,
              protected router: Router) {
    super(route, router);
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
    workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;
  }

  ngOnInit(): void {
    this.initializeOneProjectSelectedObservable();
    this.addFilters();
    this.initializeGridSearchAndRefreshData();
  }

  private addFilters() {
    this.gridService.addFilters([{
      id: 'name',
      active: false,
      initialValue: {kind: 'single-string-value', value: ''},
      tiedToPerimeter: false,
      operation: FilterOperation.LIKE
    }, {
      id: 'label',
      active: false,
      initialValue: {kind: 'single-string-value', value: ''},
      tiedToPerimeter: false,
      operation: FilterOperation.LIKE
    }, {
      id: 'createdBy',
      active: false,
      initialValue: {kind: 'single-string-value', value: ''},
      tiedToPerimeter: false,
      operation: FilterOperation.LIKE
    }, {
      id: 'lastModifiedBy',
      active: false,
      initialValue: {kind: 'single-string-value', value: ''},
      tiedToPerimeter: false,
      operation: FilterOperation.LIKE
    }]);
  }

  private initializeOneProjectSelectedObservable() {
    this.oneProjectIsSelected$ = this.gridService.selectedRows$.pipe(
      takeUntil(this.unsub$),
      map(rows => rows.length === 1)
    );
  }

  private initializeGridSearchAndRefreshData(): void {
    this.gridWithStatePersistence.popGridState().subscribe((snapshot) => {
      GridWithStatePersistence.updateMultipleColumnSearchField(snapshot, this.searchField);
      this.gridService.refreshData();
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openProjectCreationDialog() {
    this.openDialogAndRefreshGridOnClose({
      component: ProjectCreationDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.projects.dialog.title.new-project',
      },
      id: 'project-dialog',
      width: 600
    });
  }

  openTemplateCreationDialog() {
    this.openDialogAndRefreshGridOnClose({
      component: TemplateCreationDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.projects.dialog.title.new-template',
      },
      id: 'template-dialog',
      width: 600
    });
  }

  openTemplateFromProjectCreationDialog() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter(selectedRows => selectedRows.length === 1),
      map(selectedRows => selectedRows[0]),
      concatMap(selectedRow => {
        const conf = {
          component: TemplateFromProjectCreationDialogComponent,
          viewContainerReference: this.viewContainerRef,
          data: {
            titleKey: 'sqtm-core.administration-workspace.projects.dialog.title.new-template-from-project',
            projectName: selectedRow.data.name,
            projectId: selectedRow.data.projectId,
          },
          id: 'template-from-project-dialog',
          width: 600
        };

        const dialogReference = this.dialogService.openDialog(conf);
        return dialogReference.dialogResultChanged$;
      }),
      filter(result => Boolean(result))
    ).subscribe(() => {
      this.gridService.refreshData();
    });
  }

  private openDialogAndRefreshGridOnClose(dialogConfiguration: DialogConfiguration) {
    const dialogReference = this.dialogService.openDialog(dialogConfiguration);

    dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      filter(result => result != null),
      concatMap((result: any) => this.gridService.refreshDataAsync().pipe(map(() => result.id))),
      tap((id: string) => super.navigateToNewEntity(id))
    ).subscribe();
  }

  filterProjects($event: any) {
    this.gridService.applyMultiColumnsFilter($event);
  }
}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import {DialogService, GenericEntityViewComponentData, GenericEntityViewService, RestService} from 'sqtm-core';
import {MilestoneViewService} from '../../services/milestone-view.service';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {concatMap, filter, map, take, takeUntil} from 'rxjs/operators';
import {AdminMilestoneState} from '../../states/admin-milestone-state';

@Component({
  selector: 'sqtm-app-milestone-view',
  templateUrl: './milestone-view.component.html',
  styleUrls: ['./milestone-view.component.less'],
  providers: [
    {
      provide: MilestoneViewService,
      useClass: MilestoneViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: MilestoneViewService
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MilestoneViewComponent implements OnInit, OnDestroy {

  @Output()
  gridRefreshRequired = new EventEmitter<void>();

  @Output()
  milestoneDeleted = new EventEmitter<void>();

  private unsub$ = new Subject<void>();

  constructor(private route: ActivatedRoute,
              public readonly milestoneViewService: MilestoneViewService,
              private cdRef: ChangeDetectorRef,
              private restService: RestService,
              private dialogService: DialogService) {
    this.prepareGridRefreshOnEntityChanges();
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('milestoneId')),
      ).subscribe((id) => this.milestoneViewService.load(parseInt(id, 10)));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.milestoneViewService.complete();
  }

  private prepareGridRefreshOnEntityChanges(): void {
    this.milestoneViewService.simpleAttributeRequiringRefresh = [
      'label',
      'status',
      'range',
      'ownerLogin',
      'endDate',
      'boundProjectsInformation',
      'description',
    ];

    this.milestoneViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
    ).subscribe(() => this.gridRefreshRequired.emit());
  }

  handleDelete() {
    this.milestoneViewService.componentData$.pipe(
      take(1),
      concatMap((componentData => this.showConfirmDeleteMilestoneDialog(componentData.milestone.id,
        componentData.milestone.boundProjectsInformation.length))),
      filter(({confirmDelete}) => confirmDelete),
      concatMap(({milestoneId}) => this.deleteMilestoneServerSide(milestoneId)),
    ).subscribe(() => this.milestoneDeleted.emit());
  }

  private showConfirmDeleteMilestoneDialog(milestoneId, projectCount: number): Observable<{ confirmDelete: boolean, milestoneId: string }> {
    const msgKeyBase = 'sqtm-core.administration-workspace.milestones.dialog.message.delete-one.';
    const milestoneHasProject = projectCount > 0;
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.milestones.dialog.title.delete-one',
      messageKey: msgKeyBase + (milestoneHasProject ? 'with-bound-project' : 'without-bound-project'),
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, milestoneId}))
    );
  }

  private deleteMilestoneServerSide(milestoneId): Observable<void> {
    return this.restService.delete(['milestones', milestoneId]);
  }
}

export interface AdminMilestoneViewComponentData extends GenericEntityViewComponentData<AdminMilestoneState, 'milestone'> {
}

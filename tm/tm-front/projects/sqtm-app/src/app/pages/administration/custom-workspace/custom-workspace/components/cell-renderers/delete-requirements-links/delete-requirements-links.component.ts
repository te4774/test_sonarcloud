import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewContainerRef} from '@angular/core';
import {AbstractDeleteCellRenderer, ConfirmDeleteLevel, DialogService, GridService, RestService} from 'sqtm-core';
import {finalize} from 'rxjs/operators';
import {RequirementsLinkService} from '../../../services/requirements-link.service';

@Component({
  selector: 'sqtm-app-delete-requirements-links-cell-renderer',
  template: `
    <sqtm-core-delete-icon (delete)="removeItem()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./delete-requirements-links.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteRequirementsLinksComponent extends AbstractDeleteCellRenderer {

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private restService: RestService,
              public vcr: ViewContainerRef,
              private requirementLinksService: RequirementsLinkService) {
    super(grid, cdr, dialogService);
  }

  public get rowIsDefault(): boolean {
    return this.row.data['default'];
  }

  public get typeIsUsedByRequirementsLinks(): boolean {
    return this.row.data['linkCount'] > 0;
  }

  removeItem() {
    if (this.rowIsDefault) {
      this.dialogService.openAlert({
        titleKey: 'sqtm-core.administration-workspace.requirements-link-types.dialog.title.delete-one',
        messageKey: 'sqtm-core.administration-workspace.requirements-link-types.dialog.message.delete-default-value',
        level: 'DANGER',
      });
    } else {
      this.showDeleteConfirm();
    }
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const typeId = this.row.data.id;
    this.restService.delete([`requirements-links/${typeId}`]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.requirementLinksService.refreshData());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.requirements-link-types.dialog.title.delete-one';
  }

  protected getMessageKey(): string {
    return this.typeIsUsedByRequirementsLinks ?
      'sqtm-core.administration-workspace.requirements-link-types.dialog.message.delete-one-which-is-used' :
      'sqtm-core.administration-workspace.requirements-link-types.dialog.message.delete-one';
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }

}



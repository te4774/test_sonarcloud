import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {
  CustomFieldCreationDialogComponent,
  CustomFieldCreationDialogFields
} from './custom-field-creation-dialog.component';
import {FormGroup, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {EMPTY, Observable, of} from 'rxjs';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CKEditorModule} from 'ckeditor4-angular';
import {DialogReference, InputType, RestService} from 'sqtm-core';
import {DropdownListOption, DropdownListOptionService} from '../../../services/dropdown-list-option.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {take} from 'rxjs/operators';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';
import {parse} from 'date-fns';
import createSpyObj = jasmine.createSpyObj;

describe('CustomFieldCreationDialogComponent', () => {
  let fixture: ComponentFixture<CustomFieldCreationDialogComponent>;
  let component: CustomFieldCreationDialogComponent;

  const dialogReference = createSpyObj(['close']);
  const dropdownListOptionService = createSpyObj(['initialize', 'addOption']);
  dropdownListOptionService.dropdownListOptions$ = EMPTY;
  dropdownListOptionService.state$ = EMPTY;
  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule, TranslateModule.forRoot(), HttpClientTestingModule, CKEditorModule],
      declarations: [CustomFieldCreationDialogComponent],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference
        },
        {
          provide: DropdownListOptionService,
          useValue: dropdownListOptionService,
        },
        {
          provide: RestService,
          useValue: restService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFieldCreationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component['dropdownListOptionService'] = dropdownListOptionService;
    component['dropdownListOptions$'] = of([]);

    dropdownListOptionService.addOption.calls.reset();
    restService.post.and.returnValue(of({}));
    restService.post.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();

    expect(component.textFieldToFocus).toBe('name');
  });

  describe('should return the appropriate request payload based on input type', () => {
    const dataSets: DataSet[] = [
      {
        name: 'Checkbox',
        values: {
          inputType: InputType.CHECKBOX,
          defaultChecked: 'true',
          optional: false,
        },
        expectations: {
          inputType: InputType.CHECKBOX,
          optional: true, // This should be overridden to true for checkboxes
          options: [],
          defaultValue: true,
        }
      },
      {
        name: 'Rich text',
        values: {
          inputType: InputType.RICH_TEXT,
          defaultRichText: 'hello, world',
          optional: false,
        },
        expectations: {
          inputType: InputType.RICH_TEXT,
          defaultValue: 'hello, world',
          optional: false,
          options: [],
        }
      },
      {
        name: 'Plain text',
        values: {
          inputType: InputType.PLAIN_TEXT,
          defaultText: 'dlrow ,olleh',
        },
        expectations: {
          inputType: InputType.PLAIN_TEXT,
          defaultValue: 'dlrow ,olleh',
          options: [],
        }
      },
      {
        name: 'Numeric',
        values: {
          inputType: InputType.NUMERIC,
          defaultNumeric: '1235',
        },
        expectations: {
          inputType: InputType.NUMERIC,
          defaultValue: '1235',
          options: [],
        }
      },
      {
        name: 'Tags',
        values: {
          inputType: InputType.TAG,
          defaultTags: ['a', 'b', 'c'],
        },
        expectations: {
          inputType: InputType.TAG,
          defaultValue: 'a|b|c',
          options: [],
        }
      },
      {
        name: 'Date',
        values: {
          inputType: InputType.DATE_PICKER,
          defaultDate: parse('01/04/2000', 'dd/MM/yyyy', new Date()),
        },
        expectations: {
          inputType: InputType.DATE_PICKER,
          defaultValue: '2000-04-01',
          options: [],
        }
      },
      {
        name: 'Dropdown list',
        values: {
          inputType: InputType.DROPDOWN_LIST,
        },
        dropdownListOptions: of([{
          id: 1,
          optionName: 'otherOption',
          optionCode: 'optionCode1',
          color: 'color1',
          default: false,
        }, {
          id: 2,
          optionName: 'defaultOption',
          optionCode: 'optionCode2',
          color: 'color2',
          default: true,
        }]),
        expectations: {
          inputType: InputType.DROPDOWN_LIST,
          defaultValue: 'defaultOption',
          options: [['otherOption', 'optionCode1', 'color1'], ['defaultOption', 'optionCode2', 'color2']],
        }
      },
    ];

    dataSets.forEach((dataSet, idx) => {
      it(`${idx + 1}. ${dataSet.name}`, () => {
        loadFormValues(component.formGroup, dataSet.values);

        component['dropdownListOptions$'] = dataSet.dropdownListOptions || of([]);

        component['getRequestPayload']().subscribe((payload) => {
          Object.entries(dataSet.expectations).forEach(([key, value]) => {
            expect(payload[key]).toEqual(value);
          });
        });
      });
    });
  });

  it('should make sure there\'s a default option for mandatory dropdown lists', () => {
    [
      { isMandatory: true, hasDefaultOption: true, expectError: false },
      { isMandatory: true, hasDefaultOption: false, expectError: true },
      { isMandatory: false, hasDefaultOption: false, expectError: false },
      { isMandatory: false, hasDefaultOption: false, expectError: false },
    ].forEach((dataSet) => {
      loadFormValues(component.formGroup, {
        inputType: InputType.DROPDOWN_LIST,
        optional: ! dataSet.isMandatory,
      });

      component['dropdownListOptions$'] = of([{
        id: 1, optionName: 'optionName', optionCode: 'optionCode', color: 'color',
        default: dataSet.hasDefaultOption,
      }]);

      component.addEntity(false);
      component['_hasDropdownListDefaultError']
        .pipe(take(1))
        .subscribe(result => expect(result).toBe(dataSet.expectError, JSON.stringify(dataSet)));
    });
  });

  it('should add a dropdown list option', () => {
    loadFormValues(component.dropdownListFormGroup, {
      dropdownListOptionCode: 'code',
      dropdownListOptionName: 'name',
    });

    component.addDropdownListOption();

    expect(dropdownListOptionService.addOption).toHaveBeenCalledWith('name', 'code');
  });

  it('should forbid to add a dropdown list option with duplicate name', () => {
    component['dropdownListOptions$'] = of([{
      id: 1, optionName: 'name', optionCode: 'optionCode', color: 'color', default: false,
    }]);

    loadFormValues(component.dropdownListFormGroup, {
      dropdownListOptionCode: 'code',
      dropdownListOptionName: 'name',
    });

    component.addDropdownListOption();

    expect(dropdownListOptionService.addOption).not.toHaveBeenCalled();
  });

  it('should forbid to add a dropdown list option with duplicate code', () => {
    component['dropdownListOptions$'] = of([{
      id: 1, optionName: 'optionName', optionCode: 'code', color: 'color', default: false,
    }]);

    loadFormValues(component.dropdownListFormGroup, {
      dropdownListOptionCode: 'code',
      dropdownListOptionName: 'name',
    });

    component.addDropdownListOption();

    expect(dropdownListOptionService.addOption).not.toHaveBeenCalled();
  });

  it('should add another entity and reset form', () => {
    loadFormValues(component.formGroup, {
      label: 'label', name: 'name', code: 'code', inputType: InputType.PLAIN_TEXT, defaultText: '', optional: true,
    });

    dropdownListOptionService.initialize.calls.reset();

    component.addEntity(true);

    expect(restService.post).toHaveBeenCalled();
    expect(dropdownListOptionService.initialize).toHaveBeenCalled();
    expect(component.formGroup.controls.name.value).toBe('');
    expect(component.formGroup.controls.label.value).toBe('');
    expect(component.formGroup.controls.code.value).toBe('');
  });

  it('should prevent adding mandatory CUF if default value is not preset', () => {
    // That's all types but CHECKBOX which is always optional
    const typesToTest = [
      InputType.PLAIN_TEXT,
      InputType.DROPDOWN_LIST,
      InputType.DATE_PICKER,
      InputType.NUMERIC,
      InputType.RICH_TEXT,
      InputType.TAG,
    ];

    typesToTest.forEach((inputType) => {
      restService.post.calls.reset();

      loadFormValues(component.formGroup, {
        label: 'label',
        name: 'name',
        code: 'code',
        inputType,
        defaultText: '',
        defaultDate: '',
        defaultTags: '',
        defaultRichText: '',
        defaultNumeric: null,
        optional: false,
      });

      restService.post.calls.reset();

      component.addEntity(true);

      expect(restService.post).not.toHaveBeenCalled();
    });
  });
});

type TestValues = {
  [key in CustomFieldCreationDialogFields]: any;
};

interface CreationPayload {
  name: string;
  label: string;
  code: string;
  inputType: InputType;
  optional: boolean;
  options: string[][];
  defaultValue: any;
}

interface DataSet {
  name: string;
  values: Partial<TestValues>;
  dropdownListOptions?: Observable<DropdownListOption[]>;
  expectations: Partial<CreationPayload>;
}

function loadFormValues(formGroup: FormGroup, values: Partial<TestValues>): void {
  Object.entries(values).forEach(([key, value]) => {
    formGroup.controls[key].setValue(value);
  });
}

import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  isDevMode,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {NzDropdownMenuComponent} from 'ng-zorro-antd/dropdown';
import {
  createFieldSelector,
  DialogService,
  EditableGenericEntityFieldState,
  EditableTextFieldComponent,
  GenericEntityViewService,
  SqtmGenericEntityState
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {filter, map, take, takeUntil} from 'rxjs/operators';
import {select} from '@ngrx/store';

@Component({
  selector: 'sqtm-app-admin-view-header',
  templateUrl: './admin-view-header.component.html',
  styleUrls: ['./admin-view-header.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminViewHeaderComponent<E extends SqtmGenericEntityState, T extends string> implements OnInit, OnDestroy {

  @Input() entityNameKey: keyof E;
  @Input() editableTitle = true;

  @Input() nameChangeConfirmationMessageKey: string;
  @Input() showsNameChangeConfirmDialog: boolean;
  @Output() nameChanged = new EventEmitter<void>();

  @Input() showMenu: boolean;
  @Input() dropdownMenu: NzDropdownMenuComponent;

  @Input() hasAttachments = false;
  @Input() attachmentCount: number;
  @Output() attachmentBadgeClickEvent = new EventEmitter<void>();

  @ViewChild('entityNameField') entityNameField: EditableTextFieldComponent;

  entityName$: Observable<E[keyof E]>;

  unsub$ = new Subject<void>();

  constructor(private readonly genericViewService: GenericEntityViewService<E, T>,
              private readonly dialogService: DialogService) {
  }

  ngOnInit(): void {
    this.entityName$ = this.genericViewService.componentData$.pipe(
      map(componentData => this.genericViewService.getEntity(componentData)[this.entityNameKey]),
    );

    this.observeErrors();
  }

  attachmentBadgeClick() {
    this.attachmentBadgeClickEvent.next();
  }

  handleNameChange($event: string): void {
    if (this.entityNameField.value === $event) {
      this.entityNameField.value = $event;
      return;
    }

    if (this.showsNameChangeConfirmDialog) {
      if (this.nameChangeConfirmationMessageKey == null) {
        if (isDevMode()) {
          throw new Error('You forgot to give your confirmation dialog a message !');
        } else {
          return;
        }
      }

      const dialogRef = this.dialogService.openConfirm({
        id: 'confirm-change-current-user-login',
        messageKey: this.nameChangeConfirmationMessageKey,
      });

      dialogRef.dialogClosed$.subscribe((result) => {
        if (result) {
          this.genericViewService.update(this.entityNameKey, $event as any);
          this.nameChanged.emit();
        } else {
          this.entityName$.pipe(take(1)).subscribe((currentName) => this.entityNameField.value = currentName as any);
        }
      });
    } else {
      this.genericViewService.update(this.entityNameKey, $event as any);
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private observeErrors() {
    this.genericViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createFieldSelector(this.entityNameKey as string)),
      filter((fieldState: EditableGenericEntityFieldState<E>) => Boolean(fieldState) && fieldState.status === 'SERVER_ERROR')
    ).subscribe((fieldState: EditableGenericEntityFieldState<E>) => this.entityNameField.showExternalErrorMessage(fieldState.errorMsg));
  }
}

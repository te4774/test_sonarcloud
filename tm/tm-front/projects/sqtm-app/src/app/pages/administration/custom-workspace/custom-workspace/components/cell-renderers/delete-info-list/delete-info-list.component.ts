import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {AbstractDeleteCellRenderer, ConfirmDeleteLevel, DialogService, GridService, RestService} from 'sqtm-core';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-info-list-cell-renderer',
  template: `
    <sqtm-core-delete-icon (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./delete-info-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteInfoListComponent extends AbstractDeleteCellRenderer implements OnDestroy {

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private restService: RestService) {
    super(grid, cdr, dialogService);
  }


  public get rowHasProjects(): boolean {
    return this.row.data['projectCount'] > 0;
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const infoListId = this.row.data.infoListId;
    this.restService.delete([`info-lists/${infoListId}`]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.grid.refreshData());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.info-lists.dialog.title.delete-one';
  }

  protected getMessageKey(): string {
    return this.rowHasProjects ?
      'sqtm-core.administration-workspace.info-lists.dialog.message.delete-one-with-project' :
      'sqtm-core.administration-workspace.info-lists.dialog.message.delete-one-without-project';
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }
}



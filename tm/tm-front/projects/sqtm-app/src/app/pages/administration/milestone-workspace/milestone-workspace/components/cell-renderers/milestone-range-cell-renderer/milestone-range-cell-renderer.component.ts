import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService, MilestoneRange} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-milestone-range-cell-renderer',
  template: `
        <ng-container *ngIf="columnDisplay && row">
          <div class="full-width full-height flex-column">
            <span *ngIf="row.data[columnDisplay.id]" style="margin: auto 5px;"  class="sqtm-grid-cell-txt-renderer">
              {{milestoneRange  | translate}}
            </span>
          </div>
        </ng-container>
   `,
  styleUrls: ['./milestone-range-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MilestoneRangeCellRendererComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  get milestoneRange(): string {
    // This component is used in the milestone workspace grid and in the project view's milestones panel.
    // In the first case, the keys are already translated but in the second, we have untranslated keys
    // coming... Hence this trick to determine if we do need to translate the keys or not.
    const status = this.row.data[this.columnDisplay.id];
    if (isRawRangeKey(status)) {
      return 'sqtm-core.entity.milestone.range.' + status;
    } else {
      return status;
    }
  }
}

function isRawRangeKey(key: string): boolean {
  return Object.keys(MilestoneRange).includes(key);
}

export function milestoneRangeColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(MilestoneRangeCellRendererComponent);
}

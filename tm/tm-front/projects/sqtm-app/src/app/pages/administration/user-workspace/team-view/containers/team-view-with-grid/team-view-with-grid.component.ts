import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {GenericEntityViewService, GridService} from 'sqtm-core';
import {map, takeUntil} from 'rxjs/operators';
import {TeamViewService} from '../../services/team-view.service';

@Component({
  selector: 'sqtm-app-team-view-with-grid',
  templateUrl: './team-view-with-grid.component.html',
  styleUrls: ['./team-view-with-grid.component.less'],
  providers: [
    {
      provide: TeamViewService,
      useClass: TeamViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: TeamViewService
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeamViewWithGridComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  constructor(private readonly route: ActivatedRoute,
              public readonly gridService: GridService,
              private readonly teamViewService: TeamViewService) {
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('teamId')),
      ).subscribe((id) => {
      this.teamViewService.load(parseInt(id, 10));
    });

    this.prepareGridRefreshOnEntityChanges();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.teamViewService.complete();
  }

  private prepareGridRefreshOnEntityChanges(): void {
    this.teamViewService.simpleAttributeRequiringRefresh = [
      'name',
      'description',
      'members',
      'lastModifiedBy',
      'lastModifiedOn'];

    this.teamViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
    ).subscribe(() =>
      this.gridService.refreshDataAndKeepSelectedRows());
  }
}

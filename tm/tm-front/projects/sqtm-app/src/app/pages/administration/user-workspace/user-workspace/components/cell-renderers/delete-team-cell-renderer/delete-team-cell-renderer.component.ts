import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {Subject} from 'rxjs';
import {AbstractDeleteCellRenderer, ConfirmDeleteLevel, DialogService, GridService, RestService} from 'sqtm-core';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-team-cell-renderer',
  template: `
    <sqtm-core-delete-icon (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./delete-team-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteTeamCellRendererComponent extends AbstractDeleteCellRenderer {

  unsub$ = new Subject<void>();

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private restService: RestService) {
    super(grid, cdr, dialogService);
  }

  protected doDelete() {
    this.grid.beginAsyncOperation();
    const partyId = this.row.data.partyId;
    this.restService.delete([`teams/${partyId}`]).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.grid.refreshData());
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.teams.dialog.title.delete-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.teams.dialog.message.delete-one';
  }

  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }
}

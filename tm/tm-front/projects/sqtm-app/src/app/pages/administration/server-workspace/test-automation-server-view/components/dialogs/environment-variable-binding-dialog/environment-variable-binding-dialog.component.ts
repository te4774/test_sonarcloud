import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  EmbeddedViewRef,
  ViewChild,
  ElementRef,
  TemplateRef,
  AfterViewInit
} from '@angular/core';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {
  DialogReference,
  ListItem,
  EnvironmentVariable
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-environment-variable-binding-dialog',
  templateUrl: './environment-variable-binding-dialog.component.html',
  styleUrls: ['./environment-variable-binding-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EnvironmentVariableBindingDialogComponent implements OnInit, AfterViewInit {

  private overlayRef: OverlayRef;

  private componentRef: EmbeddedViewRef<any>;

  @ViewChild('element')
  element: ElementRef;

  @ViewChild('environmentVariableItemTemplate')
  environmentVariableItemTemplate: TemplateRef<any>;

  editMode: boolean;

  listItems: ListItem[] = [];

  variablesAreAvailable: boolean = this.dialogRef.data.environmentVariables.length > 0;

  constructor(private overlay: Overlay,
              public readonly cdRef: ChangeDetectorRef,
              public readonly dialogRef: DialogReference<EnvironmentVariableBindingDialogConfig, ListItem[]>) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.listItems = this.dialogRef.data.environmentVariables.map(item => ({
      ...item,
      customTemplate: this.environmentVariableItemTemplate
    }));
    this.displayOverlay();
  }


  displayOverlay(): void {
    this.editMode = true;

    const positionStrategy = this.overlay.position()
      .flexibleConnectedTo(this.element)
      .withPositions([
        {originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: 70},
        {originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: 50},
      ]);

    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
    };

    this.overlayRef = this.overlay.create(overlayConfig);

    this.overlayRef.backdropClick().subscribe(() => this.close());
  }

  handleItemSelectionChange(selectedItem: ListItem) {
    this.listItems = this.listItems.map(item => {
      if (item.id === selectedItem.id) {
        item.selected = selectedItem.selected;
      }
      return item;
    });
    this.cdRef.detectChanges();
  }

  getEnvironmentVariableTypeLabelKey(id: number) {
    return 'sqtm-core.entity.environment-variable.' + this.getEnvironmentVariableType(id);
  }

  private getEnvironmentVariableType(evId: number) {
    return this.getEnvironmentVariable(evId)?.inputType;
  }

  private getEnvironmentVariable(evId: number): EnvironmentVariable {
    return this.dialogRef.data.dialogData.filter(variable => variable.id === evId)[0];
  }

  private close() {
    this.editMode = false;
    this.removeOverlay();
    this.cdRef.detectChanges();
  }

  private removeOverlay() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }
  }

  confirm() {
    this.dialogRef.result = this.listItems.filter(item => item.selected);
    this.dialogRef.close();
  }

  hasSelectedItems(): boolean {
    return !(this.listItems.filter(item => item.selected).length > 0);
  }


}

export interface EnvironmentVariableBindingDialogConfig {
  titleKey: string;
  environmentVariables: ListItem[];
  dialogData: EnvironmentVariable[];
}

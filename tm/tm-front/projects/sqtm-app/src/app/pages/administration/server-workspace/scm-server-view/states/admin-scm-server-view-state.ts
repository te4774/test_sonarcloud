import {GenericEntityViewState, provideInitialGenericViewState} from 'sqtm-core';
import {AdminScmServerState} from './admin-scm-server-state';


export interface AdminScmServerViewState extends GenericEntityViewState<AdminScmServerState, 'scmServer'> {
  scmServer: AdminScmServerState;
}

export function provideInitialAdminScmServerView(): Readonly<AdminScmServerViewState> {
  return provideInitialGenericViewState<AdminScmServerState, 'scmServer'>('scmServer');
}

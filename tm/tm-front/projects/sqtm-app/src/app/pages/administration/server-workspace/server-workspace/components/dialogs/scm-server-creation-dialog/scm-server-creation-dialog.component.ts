import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DialogReference, FieldValidationError, RestService, TextFieldComponent} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {ScmServerDialogConfiguration} from './scm.server.dialog.configuration';
import {
  AbstractAdministrationCreationDialogDirective
} from '../../../../../components/abstract-administration-creation-dialog';
import {of} from 'rxjs';

@Component({
  selector: 'sqtm-app-scm-server-creation-dialog',
  templateUrl: './scm-server-creation-dialog.component.html',
  styleUrls: ['./scm-server-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScmServerCreationDialogComponent extends AbstractAdministrationCreationDialogDirective implements OnInit {

  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: ScmServerDialogConfiguration;

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  constructor(private fb: FormBuilder,
              private translateService: TranslateService,
              dialogReference: DialogReference<ScmServerDialogConfiguration>,
              restService: RestService,
              cdr: ChangeDetectorRef) {
    super ('scm-servers/new', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  ngOnInit() {
    this.initializeFormGroup();
  }

  protected getRequestPayload() {
    return of({
      name: this.getFormControlValue('name'),
      kind: this.getFormControlValue('kind'),
      url: this.getFormControlValue('url'),
    });
  }

  protected doResetForm() {
    this.resetFormControl('name', '');
    this.resetFormControl('kind', '');
    this.resetFormControl('url', '');
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      kind: this.fb.control(this.data.scmServerKinds[0].id, [
        Validators.required,
      ]),
      url: this.fb.control('', [
        Validators.required,
        Validators.maxLength(255)
      ])
    });
  }
}

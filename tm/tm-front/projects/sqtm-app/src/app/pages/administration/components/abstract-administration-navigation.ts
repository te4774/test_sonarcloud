import {Directive} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";

@Directive()
export abstract class AbstractAdministrationNavigation {

  protected constructor(
    protected route: ActivatedRoute,
    protected router: Router
  ) {}

  protected navigateToNewEntity(id: string) {
    this.router.navigate([id, 'content'], {relativeTo: this.route});
  }

}


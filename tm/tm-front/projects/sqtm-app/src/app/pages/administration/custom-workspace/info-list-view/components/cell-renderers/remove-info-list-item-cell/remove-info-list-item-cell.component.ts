import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {AbstractDeleteCellRenderer, DialogService, GridService} from 'sqtm-core';
import {Observable} from 'rxjs';
import {AdminInfoListViewState} from '../../../states/admin-info-list-view-state';
import {InfoListViewService} from '../../../services/info-list-view.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-remove-info-list-item-cell',
  template: `
    <ng-container *ngIf="componentData$ | async as componentData">
      <sqtm-core-delete-icon
          *ngIf="! isDefaultValue()"
          (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
    </ng-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoveInfoListItemCellComponent extends AbstractDeleteCellRenderer implements OnDestroy {

  componentData$: Observable<AdminInfoListViewState>;

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private infoListViewService: InfoListViewService) {
    super(grid, cdr, dialogService);
    this.componentData$ = this.infoListViewService.componentData$;
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const optionId = Number(this.row.id);
    this.infoListViewService.deleteInfoListItems([optionId]).pipe(
       finalize(() => this.grid.completeAsyncOperation())
     ).subscribe();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.entities-customization.generic.dialog.option.title.delete-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.info-lists.dialog.message.option.delete-one';
  }

  isDefaultValue(): boolean {
    return this.row.data['isDefault'];
  }
}

import {TestBed} from '@angular/core/testing';

import {EnvironmentVariableOptionService} from './environment-variable-option.service';
import {mockRestService} from '../../../../../utils/testing-utils/mocks.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {RestService} from 'sqtm-core';
import {take} from 'rxjs/operators';

describe('EnvironmentVariableOptionService', () => {
  const restService = mockRestService();
  let service: EnvironmentVariableOptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: EnvironmentVariableOptionService,
          useClass: EnvironmentVariableOptionService
        }
      ]
    });
    service = TestBed.inject(EnvironmentVariableOptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load initial state', async (done) => {
    service.initialize();
    service.environmentVariableOptions$.pipe(
      take(1)
    ).subscribe(options => {
      expect(options).toEqual([]);
      done();
    });
  });

  it('should add options', (done) => {
    service.initialize();
    service.addOption('option1', 'code1').subscribe();
    service.addOption('option2', 'code2').subscribe();
    service.environmentVariableOptions$.pipe(
      take(1)
    ).subscribe(options => {
        expect(options.length).toEqual(2);
        done();
      }
    );
  });

  it('should remove an options', (done) => {
    service.initialize();
    service.addOption('option1', 'code1').subscribe();
    service.addOption('option2', 'code2').subscribe();
    service.removeOption('option1').subscribe();

    service.environmentVariableOptions$.pipe(
      take(1)
    ).subscribe(options => {
        expect(options.length).toEqual(1);
        done();
      }
    );
  });


});

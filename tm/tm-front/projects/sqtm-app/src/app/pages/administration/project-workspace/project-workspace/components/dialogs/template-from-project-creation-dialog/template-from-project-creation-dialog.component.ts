import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CreationDialogData, DialogReference, FieldValidationError, RestService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {
  AbstractAdministrationCreationDialogDirective
} from '../../../../../components/abstract-administration-creation-dialog';
import {of} from 'rxjs';

@Component({
  selector: 'sqtm-app-template-from-project-creation-dialog',
  templateUrl: './template-from-project-creation-dialog.component.html',
  styleUrls: ['./template-from-project-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplateFromProjectCreationDialogComponent extends AbstractAdministrationCreationDialogDirective implements OnInit {

  formGroup: FormGroup;

  serverSideValidationErrors: FieldValidationError[] = [];

  data: TemplateFromProjectCreationDialogData;

  constructor(private fb: FormBuilder,
              private translateService: TranslateService,
              dialogReference: DialogReference,
              restService: RestService,
              cdr: ChangeDetectorRef) {
    super('project-templates/new', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  ngOnInit() {
    // Prepare form group
    this.formGroup = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern('(.|\\s)*\\S(.|\\s)*'),
        Validators.maxLength(255)
      ]),
      description: this.fb.control(''),
      label: this.fb.control('', [Validators.maxLength(255)]),
      keepPermissions: this.fb.control(true),
      keepCustomFields: this.fb.control(true),
      keepInfoLists: this.fb.control(true),
      keepBugtracker: this.fb.control(true),
      keepAutomation: this.fb.control(true),
      keepMilestones: this.fb.control(true),
      keepAllowTcModificationsFromExecution: this.fb.control(true),
      keepOptionalExecutionStatuses: this.fb.control(true),
      keepPlugins: this.fb.control(true),
    });
  }

  protected getRequestPayload() {
    return of({
      name: this.formGroup.controls['name'].value,
      description: this.formGroup.controls['description'].value,
      label: this.formGroup.controls['label'].value,
      copyPermissions: this.formGroup.controls['keepPermissions'].value,
      copyCUF: this.formGroup.controls['keepCustomFields'].value,
      copyInfolists: this.formGroup.controls['keepInfoLists'].value,
      copyBugtrackerBinding: this.formGroup.controls['keepBugtracker'].value,
      copyAutomatedProjects: this.formGroup.controls['keepAutomation'].value,
      copyMilestone: this.formGroup.controls['keepMilestones'].value,
      copyAllowTcModifFromExec: this.formGroup.controls['keepAllowTcModificationsFromExecution'].value,
      copyOptionalExecStatuses: this.formGroup.controls['keepOptionalExecutionStatuses'].value,
      copyPluginsActivation: this.formGroup.controls['keepPlugins'].value,
      templateId: this.data.projectId,
    });
  }

  protected doResetForm(): void {
    this.formGroup.get('name').reset('');
    this.formGroup.get('description').reset('');
    this.formGroup.get('label').reset('');
    this.formGroup.get('keepPermissions').reset(true);
    this.formGroup.get('keepCustomFields').reset(true);
    this.formGroup.get('keepInfoLists').reset(true);
    this.formGroup.get('keepBugtracker').reset(true);
    this.formGroup.get('keepAutomation').reset(true);
    this.formGroup.get('keepMilestones').reset(true);
    this.formGroup.get('keepAllowTcModificationsFromExecution').reset(true);
    this.formGroup.get('keepOptionalExecutionStatuses').reset(true);
    this.formGroup.get('keepPlugins').reset(true);
  }
}

export interface TemplateFromProjectCreationDialogData extends CreationDialogData {
  id: string;
  titleKey: string;
  projectName: string;
  projectId: number;
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  AdminReferentialDataService,
  DialogReference,
  FieldValidationError,
  LicenseInformation,
  LicenseInformationMessageProvider,
  LicenseMessagePlacement,
  passwordConfirmationValidator,
  passwordLengthValidator,
  RestService,
  TextFieldComponent
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {UserDialogConfiguration} from './user.dialog.configuration';
import {
  AbstractAdministrationCreationDialogDirective
} from '../../../../../components/abstract-administration-creation-dialog';
import {of} from 'rxjs';
import {map, take, withLatestFrom} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-user-creation-dialog',
  templateUrl: './user-creation-dialog.component.html',
  styleUrls: ['./user-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserCreationDialogComponent extends AbstractAdministrationCreationDialogDirective implements OnInit {

  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: UserDialogConfiguration;
  licenseMessage: string;

  @ViewChildren(TextFieldComponent)
  textFields: QueryList<TextFieldComponent>;

  constructor(private fb: FormBuilder,
              private translateService: TranslateService,
              private adminReferentialDataService: AdminReferentialDataService,
              dialogReference: DialogReference<UserDialogConfiguration>,
              restService: RestService,
              cdr: ChangeDetectorRef) {
    super('users/new', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;

    this.adminReferentialDataService.licenseInformation$.pipe(
      take(1),
      map((licenseInfo: LicenseInformation) => new LicenseInformationMessageProvider(licenseInfo, translateService)),
      withLatestFrom(adminReferentialDataService.loggedAsAdmin$),
      map(([msgProvider, isAdmin]) => msgProvider.getLongMessage(LicenseMessagePlacement.USER_CREATION, isAdmin)),
    ).subscribe((longMessage) => this.licenseMessage = longMessage);
  }

  get canManageLocalPassword(): boolean {
    return this.data.canManageLocalPassword;
  }

  get textFieldToFocus(): string {
    return 'login';
  }

  ngOnInit() {
    this.initializeFormGroup();
  }

  protected getRequestPayload() {
    const payload: any = {
      user: {
        login: this.getFormControlValue('login').toString().trim(),
        firstName: this.getFormControlValue('firstName').toString().trim(),
        lastName: this.getFormControlValue('lastName').toString().trim(),
        email: this.getFormControlValue('email').toString().trim()
      },
      groupId: this.getFormControlValue('groupId'),
    };

    if (this.canManageLocalPassword) {
      payload.password = this.getFormControlValue('password');
    }

    return of(payload);
  }

  protected doResetForm() {
    this.resetFormControl('login', '');
    this.resetFormControl('firstName', '');
    this.resetFormControl('lastName', '');
    this.resetFormControl('email', '');
    this.resetFormControl('groupId', 2);

    if (this.canManageLocalPassword) {
      this.resetFormControl('password', '');
      this.resetFormControl('confirmPassword', '');
    }
  }


  private initializeFormGroup() {
    const controls: any = {
      login: this.fb.control('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      firstName: this.fb.control('', [
        Validators.maxLength(50)
      ]),
      lastName: this.fb.control('', [
        Validators.required,
        Validators.maxLength(50)
      ]),
      email: this.fb.control('', [
        Validators.maxLength(255)
      ]),
      groupId: this.fb.control(2, [
        Validators.required,
      ]),
    };

    let globalValidators = [];

    if (this.canManageLocalPassword) {
      controls.password = this.fb.control('', [
        Validators.required
      ]);
      controls.confirmPassword = this.fb.control('', [
        Validators.required
      ]);

      globalValidators = [
        passwordLengthValidator('password'),
        passwordConfirmationValidator('password', 'confirmPassword'),
      ];
    }

    this.formGroup = this.fb.group(controls, { validators: globalValidators });
  }
}

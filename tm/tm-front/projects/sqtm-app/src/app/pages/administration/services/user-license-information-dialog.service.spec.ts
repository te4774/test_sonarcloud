import {TestBed} from '@angular/core/testing';

import {UserLicenseInformationDialogService} from './user-license-information-dialog.service';
import {AdminReferentialDataService, DialogService} from 'sqtm-core';
import {
  mockAdminReferentialDataService,
  mockDialogService,
  mockPassThroughTranslateService
} from '../../../utils/testing-utils/mocks.service';
import {TranslateService} from '@ngx-translate/core';

describe('UserLicenseInformationDialogService', () => {
  let service: UserLicenseInformationDialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: UserLicenseInformationDialogService,
          useClass: UserLicenseInformationDialogService,
        },
        {
          provide: DialogService,
          useValue: mockDialogService(),
        },
        {
          provide: AdminReferentialDataService,
          useValue: mockAdminReferentialDataService(),
        },
        {
          provide: TranslateService,
          useValue: mockPassThroughTranslateService(),
        }
      ]
    });
    service = TestBed.inject(UserLicenseInformationDialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

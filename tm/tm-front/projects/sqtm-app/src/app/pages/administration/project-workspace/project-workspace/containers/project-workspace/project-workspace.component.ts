import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import {ADMIN_WS_PROJECT_TABLE, ADMIN_WS_PROJECT_TABLE_CONFIG} from '../../../project-workspace.constant';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  ExportModelBuilderService,
  GRID_PERSISTENCE_KEY,
  GridExportService,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  isAdminOrProjectManager,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {adminProjectTableDefinition} from '../project-grid/project-grid.component';
import {filter, takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'sqtm-app-project-workspace',
  templateUrl: './project-workspace.component.html',
  styleUrls: ['./project-workspace.component.less'],
  providers: [
    DatePipe,
    {
      provide: ADMIN_WS_PROJECT_TABLE_CONFIG,
      useFactory: adminProjectTableDefinition,
      deps: []
    },
    {
      provide: ADMIN_WS_PROJECT_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, ADMIN_WS_PROJECT_TABLE_CONFIG, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: ADMIN_WS_PROJECT_TABLE
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'project-workspace-main-grid'
    },
    GridWithStatePersistence,
    ExportModelBuilderService,
    GridExportService
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectWorkspaceComponent implements OnInit, OnDestroy {

  authenticatedUser$: Observable<AuthenticatedUser>;

  private unsub$ = new Subject<void>();

  constructor(public readonly adminReferentialDataService: AdminReferentialDataService,
              private router: Router,
              private gridService: GridService) {
  }

  ngOnInit(): void {
    this.adminReferentialDataService.refresh().subscribe();

    this.authenticatedUser$ = this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => isAdminOrProjectManager(authUser))
    );

    this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => !isAdminOrProjectManager(authUser)),
    ).subscribe(() => this.router.navigate(['home-workspace']));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
    this.gridService.complete();
  }

}

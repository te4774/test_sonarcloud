import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  GridService
} from 'sqtm-core';
import {CustomFieldViewService} from '../../../services/custom-field-view.service';
import {Observable} from 'rxjs';
import {AdminCustomFieldViewState} from '../../../states/admin-custom-field-view-state';
import {catchError, finalize} from 'rxjs/operators';
import {NzCheckboxComponent} from 'ng-zorro-antd/checkbox';

@Component({
  selector: 'sqtm-app-option-default-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row && componentData$ | async as componentData">
      <div class="full-width full-height flex-column">
        <label nz-checkbox style="margin: auto"
               [nzChecked]="componentData.customField.defaultValue === row.data['label']"
               (nzCheckedChange)="handleClick($event)">
        </label>
      </div>
    </ng-container>`,
  styleUrls: ['./option-default-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionDefaultCellComponent extends AbstractCellRendererComponent {

  componentData$: Observable<AdminCustomFieldViewState>;

  @ViewChild(NzCheckboxComponent)
  private defaultCheckBox: NzCheckboxComponent;

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              private customFieldViewService: CustomFieldViewService,
              private actionErrorDisplayService: ActionErrorDisplayService) {
    super(grid, cdRef);
    this.componentData$ = this.customFieldViewService.componentData$;
  }

  handleClick(isChecked: boolean) {
    this.grid.beginAsyncOperation();
    const optionLabel = this.row.data['label'];
    this.customFieldViewService.changeDefaultValue(isChecked ? optionLabel : '').pipe(
      catchError((error) => {
        this.defaultCheckBox.nzChecked = !isChecked;
        return this.actionErrorDisplayService.handleActionError(error);
      }),
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
  }
}

export function optionDefaultColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(OptionDefaultCellComponent);
}

import {TestBed} from '@angular/core/testing';

import {TestAutomationServerViewService} from './test-automation-server-view.service';
import {
  AdminReferentialDataService,
  AdminTestAutomationServer,
  AttachmentService,
  AuthenticationProtocol,
  EntityViewAttachmentHelperService, EvInputType,
  RestService,
  TestAutomationServerKind
} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';
import {take} from 'rxjs/operators';
import {mockAdminReferentialDataService, mockRestService} from '../../../../../utils/testing-utils/mocks.service';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('TestAutomationServerViewService', () => {
  let service: TestAutomationServerViewService;
  let restService: SpyObj<RestService>;

  beforeEach(() => {
    restService = mockRestService();

    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule],
      providers: [
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: TranslateService,
          useValue: createSpyObj(['instant']),
        },
        {
          provide: AdminReferentialDataService,
          useValue: mockAdminReferentialDataService(),
        },
        {
          provide: TestAutomationServerViewService,
          useClass: TestAutomationServerViewService,
          deps: [
            RestService,
            AttachmentService,
            TranslateService,
            EntityViewAttachmentHelperService,
            AdminReferentialDataService,
          ]
        },
      ]
    });
    service = TestBed.inject(TestAutomationServerViewService);
  });

  it('should load a test automation server', (done) => {
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialModel()));

    service.componentData$.subscribe(data => {
      expect(data.testAutomationServer.id).toEqual(1);
      done();
    });

    expect(service).toBeTruthy();

    service.load(1);
  });

  it('should change manual slave selection', async (done) => {

    const testAutomationServer = getInitialModel();
    restService.getWithoutErrorHandling.and.returnValue(of(testAutomationServer));
    service.load(1);

    const changeManualSlaveSelectionResponse = {
      ...testAutomationServer,
      manualSlaveSelection: true
    };
    restService.post.and.returnValue(of(changeManualSlaveSelectionResponse));

    service.changeManualSlaveSelection(true);
    service.componentData$.pipe(
      take(1)
    ).subscribe(componentData => {
      expect(componentData.testAutomationServer.manualSlaveSelection).toEqual(changeManualSlaveSelectionResponse.manualSlaveSelection);
      done();
    });
  });

  it('should bind environment variables', async (done) => {

    const testAutomationServer = getInitialModel();
    restService.getWithoutErrorHandling.and.returnValue(of(testAutomationServer));
    service.load(1);

    const boundEv = [
      {
        id: 2,
        name: 'environmentvariable',
        code: 'code',
        inputType: EvInputType.PLAIN_TEXT,
        boundToServer: false,
        options: [],
        value: undefined
      }
    ];

    const response = {
      ...testAutomationServer,
      boundEnvironmentVariables: boundEv
    };

    restService.post.and.returnValue(of(response));

    service.bindEnvironmentVariables([2]).subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(componentData => {
        expect(componentData.testAutomationServer.boundEnvironmentVariables).toEqual(boundEv);
        done();
      });
    });

  });

  it('should unbind environment variables', async (done) => {

    const testAutomationServer = getInitialModel();
    testAutomationServer.boundEnvironmentVariables = [
      {
        id: 1,
        name: 'environmentvariable',
        code: 'code',
        inputType: EvInputType.PLAIN_TEXT,
        boundToServer: true,
        options: [],
        value: undefined
      }
    ];

    restService.getWithoutErrorHandling.and.returnValue(of(testAutomationServer));
    service.load(1);

    const response = {
      ...testAutomationServer,
      boundEnvironmentVariables: []
    };

    restService.post.and.returnValue(of(response));

    service.unbindEnvironmentVariables([1]).subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(componentData => {
        expect(componentData.testAutomationServer.boundEnvironmentVariables.length).toEqual(0);
        done();
      });
    });
  });

  it('should set environment variable value', async (done) => {

    const testAutomationServer = getInitialModel();
    testAutomationServer.boundEnvironmentVariables = [
      {
        id: 1,
        name: 'environmentvariable',
        code: 'code',
        inputType: EvInputType.PLAIN_TEXT,
        boundToServer: true,
        options: [],
        value: undefined
      }
    ];
    restService.getWithoutErrorHandling.and.returnValue(of(testAutomationServer));
    service.load(1);

    const boundEv = [
      {
        id: 1,
        name: 'environmentvariable',
        code: 'code',
        inputType: EvInputType.PLAIN_TEXT,
        boundToServer: false,
        options: [],
        value: 'newValue'
      }
    ];

    const response = {
      ...testAutomationServer,
      boundEnvironmentVariables: boundEv
    };

    restService.post.and.returnValue(of(response));

    service.setEnvironmentVariableValue(1, 'newValue').subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(componentData => {
        expect(componentData.testAutomationServer.boundEnvironmentVariables[0].value).toEqual('newValue');
        done();
      });
    });

  });


  function getInitialModel(): AdminTestAutomationServer {
    return {
      id: 1,
      name: 'milestone 1',
      kind: TestAutomationServerKind.jenkins,
      baseUrl: 'http://127.0.0.01:8080',
      description: '',
      createdBy: '',
      createdOn: null,
      lastModifiedBy: null,
      lastModifiedOn: null,
      manualSlaveSelection: true,
      credentials: null,
      authProtocol: AuthenticationProtocol.BASIC_AUTH,
      supportedAuthenticationProtocols: [AuthenticationProtocol.BASIC_AUTH],
      supportsAutomatedExecutionEnvironments: false,
      availableEnvironments: [],
      environmentTags: [],
      availableEnvironmentTags: [],
      observerUrl: null,
      boundEnvironmentVariables: []
    };
  }
});

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {UserInformationPanelComponent} from './user-information-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AdminReferentialDataService, AuthenticatedUser, DialogService, GridService, RestService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {DatePipe, registerLocaleData} from '@angular/common';
import {UserViewService} from '../../../services/user-view.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {EMPTY, of} from 'rxjs';
import {
  mockClosableDialogService,
  mockGridService,
  mockRestService
} from '../../../../../../../utils/testing-utils/mocks.service';
import {mockEditableSelectField} from '../../../../../../../utils/testing-utils/test-component-generator';
import {AdminUserViewComponentData} from '../../../containers/user-view/user-view.component';
import fr from '@angular/common/locales/fr';

// We need to register the locale because it's used in AuditableFieldComponent (date parsing)
registerLocaleData(fr);

describe('UserInformationPanelComponent', () => {
  let component: UserInformationPanelComponent;
  let fixture: ComponentFixture<UserInformationPanelComponent>;

  const restService = mockRestService();
  const userViewService = jasmine.createSpyObj([
    'confirmUserGroup',
    'resetPassword',
    'activateUser',
    'deactivateUser']);
  userViewService.componentData$ = EMPTY;
  const gridService = mockGridService();
  const dialogMock = mockClosableDialogService();
  const dialogService = dialogMock.service;
  const adminReferentialDataService = jasmine.createSpyObj(['refresh']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AppTestingUtilsModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
      ],
      providers: [
        {
          provide: DatePipe,
          useValue: jasmine.createSpyObj(['transform']),
        },
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: UserViewService,
          useValue: userViewService
        },
        {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: AdminReferentialDataService,
          useValue: adminReferentialDataService,
        }
      ],
      declarations: [UserInformationPanelComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInformationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.userGroupSelectField = mockEditableSelectField();
    component.adminUserViewComponentData = {
      user: {
        id: 1,
      }
    } as AdminUserViewComponentData;

    userViewService.confirmUserGroup.calls.reset();
    userViewService.resetPassword.calls.reset();
    userViewService.activateUser.calls.reset();
    userViewService.deactivateUser.calls.reset();
    gridService.refreshDataAndKeepSelectedRows.calls.reset();
    dialogMock.resetSubjects();
    dialogMock.resetCalls();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change user group', () => {
    component.confirmUserGroup({
      value: '123', label: 'Hidden group',
    });

    expect(userViewService.confirmUserGroup).toHaveBeenCalledWith(123);
    expect(gridService.refreshDataAndKeepSelectedRows).toHaveBeenCalled();
  });

  it('should open dialog for password modification', () => {
    component.openResetPasswordDialog();
    dialogMock.closeDialogsWithResult(true);

    expect(userViewService.resetPassword).toHaveBeenCalled();
  });

  it('should convert users groups into select field options', () => {
    const groups = [
      {id: 1, qualifiedName: 'squashtest.authz.group.core.Admin'},
      {id: 12, qualifiedName: 'squashtest.authz.group.tm.User'},
      {id: 21, qualifiedName: 'squashtest.authz.group.tm.TestAutomationServer'},
    ];

    const options = component.getUsersGroups(groups);

    expect(options.length).toBe(3);
    expect(options[0].value).toBe('1');
    expect(options[1].value).toBe('12');
    expect(options[2].value).toBe('21');
  });

  it('should format last connection date', () => {
    component.adminUserViewComponentData.user.lastConnectedOn = null;
    expect(component.lastConnectedOn).toBe('sqtm-core.generic.label.never');

    component.adminUserViewComponentData.user.lastConnectedOn = '2021-06-05';
    expect(component.lastConnectedOn).not.toBe('sqtm-core.generic.label.never');
  });

  it('should activate user', () => {
    component.adminUserViewComponentData.user.id = 1;
    component.authenticatedUser$ = of({
      userId: 55,
    } as unknown as AuthenticatedUser);

    component.adminUserViewComponentData.user.active = false;
    component.changeUserStatus();

    expect(userViewService.activateUser).toHaveBeenCalled();
  });

  it('should deactivate user', () => {
    component.adminUserViewComponentData.user.id = 1;
    component.authenticatedUser$ = of({
      userId: 55,
    } as unknown as AuthenticatedUser);

    component.adminUserViewComponentData.user.active = true;
    component.changeUserStatus();

    expect(userViewService.deactivateUser).toHaveBeenCalled();
  });

  it('should show alert when trying to deactivate current user', () => {
    component.adminUserViewComponentData.user.id = 55;
    component.authenticatedUser$ = of({
      userId: 55,
    } as unknown as AuthenticatedUser);

    component.changeUserStatus();

    expect(dialogService.openAlert).toHaveBeenCalled();
    expect(userViewService.activateUser).not.toHaveBeenCalled();
    expect(userViewService.deactivateUser).not.toHaveBeenCalled();
  });

  it('should give correct CSS classes based on user activation', () => {
    component.adminUserViewComponentData.user.active = false;

    expect(component.inactiveColor).toBe('inactive');
    expect(component.activeColor).toBe('disabled');

    component.adminUserViewComponentData.user.active = true;

    expect(component.inactiveColor).toBe('disabled');
    expect(component.activeColor).toBe('active');
  });
});

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {
  MilestoneSynchronizationDialogComponent,
  MilestoneSynchronizationDialogData,
  MilestoneSynchronizationInfo
} from './milestone-synchronization-dialog.component';
import {DialogReference, RestService} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';

/**
 * A builder for the dialog configuration as there are many controls done on the input data.
 *
 * Note: This builder doesn't rely on destructuring to update its internal state. This means
 * you'd better only use ONE builder per generated dataset to avoid problems with shared references.
 * This make the code easier to read...
 */
class DataSetBuilder {

  constructor(name: string) {
    this.dataSet.name = name;
  }
  private dataSet = DataSetBuilder.getDefaultDataSet();

  private static getDefaultDataSet(): DataSet {
    return {
      name: '',
      dialogData: getDefaultTestData(),
      assertions: {
        isFirstTargetDisabled: false,
        isSecondTargetDisabled: false,
        isUnionDisabled: false,
        isExtendPerimeterDisabled: false,
      }
    };
  }

  forAdmin(): this {
    this.dataSet.dialogData.isProjectManager = false;
    return this;
  }

  forProjectManager(): this {
    this.dataSet.dialogData.isProjectManager = true;
    return this;
  }

  withFirstGlobal(): this {
    this.dataSet.dialogData.milestoneA.global = true;
    return this;
  }

  withFirstLocked(): this {
    this.dataSet.dialogData.milestoneA.locked = true;
    return this;
  }

  withFirstOwned(): this {
    this.dataSet.dialogData.milestoneA.owned = true;
    return this;
  }

  withSecondGlobal(): this {
    this.dataSet.dialogData.milestoneB.global = true;
    return this;
  }

  withSecondLocked(): this {
    this.dataSet.dialogData.milestoneB.locked = true;
    return this;
  }

  withSecondOwned(): this {
    this.dataSet.dialogData.milestoneB.owned = true;
    return this;
  }

  assertFirstTargetIsDisabled(): this {
    this.dataSet.assertions.isFirstTargetDisabled = true;
    return this;
  }

  assertSecondTargetIsDisabled(): this {
    this.dataSet.assertions.isSecondTargetDisabled = true;
    return this;
  }

  assertUnionIsDisabled(): this {
    this.dataSet.assertions.isUnionDisabled = true;
    return this;
  }

  assertExtendPerimeterIsDisabled(): this {
    this.dataSet.assertions.isExtendPerimeterDisabled = true;
    return this;
  }

  build(): DataSet {
    return {
      ...this.dataSet,
    };
  }
}

describe('MilestoneSynchronizationDialogComponent', () => {
  let component: MilestoneSynchronizationDialogComponent;
  let fixture: ComponentFixture<MilestoneSynchronizationDialogComponent>;
  const dialogReference = jasmine.createSpyObj(['close']);
  const restService = mockRestService();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      declarations: [MilestoneSynchronizationDialogComponent],
      providers: [
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
        {
          provide: RestService,
          useValue: restService
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  describe('Fields activation', () => {
    const testDataSets: DataSet[] = [
      new DataSetBuilder('Admin - General case')
        .forAdmin()
        .build(),

      new DataSetBuilder('Admin - Locked milestone')
        .forAdmin()
        .withFirstLocked()
        .assertFirstTargetIsDisabled()
        .assertUnionIsDisabled()
        .build(),

      new DataSetBuilder('PM - both owned, no locked')
        .forProjectManager()
        .withFirstOwned()
        .withSecondOwned()
        .build(),

      new DataSetBuilder('PM - both owned, one locked')
        .forProjectManager()
        .withFirstOwned()
        .withSecondOwned()
        .withFirstLocked()
        .assertFirstTargetIsDisabled()
        .assertUnionIsDisabled()
        .build(),

      new DataSetBuilder('PM - all restricted, none owned')
        .forProjectManager()
        .assertExtendPerimeterIsDisabled()
        .build(),

      new DataSetBuilder('PM - none owned, one global')
        .forProjectManager()
        .withFirstGlobal()
        .assertFirstTargetIsDisabled()
        .assertExtendPerimeterIsDisabled()
        .assertUnionIsDisabled()
        .build(),

      new DataSetBuilder('PM - one owned, one global')
        .forProjectManager()
        .withFirstGlobal()
        .withSecondOwned()
        .assertFirstTargetIsDisabled()
        .assertUnionIsDisabled()
        .build(),
    ];

    testDataSets.forEach((dataSet) => {
      it('Data set: ' + dataSet.name, async (done) => {
        dialogReference.data = dataSet.dialogData;
        fixture = TestBed.createComponent(MilestoneSynchronizationDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        const assertions = dataSet.assertions;
        expect(component).toBeTruthy();
        expect(component.isFirstTargetDisabled).toBe(assertions.isFirstTargetDisabled, 'isFirstTargetDisabled');
        expect(component.isSecondTargetDisabled).toBe(assertions.isSecondTargetDisabled, 'isSecondTargetDisabled');
        expect(component.isUnionDisabled).toBe(assertions.isUnionDisabled, 'isUnionDisabled');
        expect(component.isExtendPerimeterDisabled).toBe(assertions.isExtendPerimeterDisabled, 'isExtendPerimeterDisabled');
        done();
      });
    });
  });

  it('should send request on submission', async (done) => {
    dialogReference.data = getDefaultTestData();
    fixture = TestBed.createComponent(MilestoneSynchronizationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.synchronizationMode = 'union';


    component.confirm();
    const urlParts = ['milestones', 'A', 'synchronize', 'B'];
    const body = {union: true, extendPerimeter: false};
    expect(restService.post.calls.mostRecent().args).toEqual([urlParts, body]);
    done();
  });
});

interface DataSet {
  name: string;
  dialogData: MilestoneSynchronizationDialogData;
  assertions: Assertions;
}

interface Assertions {
  isFirstTargetDisabled: boolean;
  isSecondTargetDisabled: boolean;
  isUnionDisabled: boolean;
  isExtendPerimeterDisabled: boolean;
}

function getDefaultTestData(): MilestoneSynchronizationDialogData {
  return {
    isProjectManager: false,
    milestoneA: getDefaultMilestoneInfo('A'),
    milestoneB: getDefaultMilestoneInfo('B'),
    titleKey: '',
    dialogId: '',
  };
}

function getDefaultMilestoneInfo(label: string): MilestoneSynchronizationInfo {
  return {
    id: label,
    global: false,
    locked: false,
    owned: false,
    label,
  };
}

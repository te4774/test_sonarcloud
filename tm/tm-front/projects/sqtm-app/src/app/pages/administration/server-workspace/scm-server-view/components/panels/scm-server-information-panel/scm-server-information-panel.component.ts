import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {AdminScmServerViewState} from '../../../states/admin-scm-server-view-state';

@Component({
  selector: 'sqtm-app-scm-server-information-panel',
  templateUrl: './scm-server-information-panel.component.html',
  styleUrls: ['./scm-server-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScmServerInformationPanelComponent implements OnInit {

  @Input()
  componentData: AdminScmServerViewState;

  constructor() { }

  ngOnInit(): void {
  }
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DataRow,
  DialogService,
  Fixed,
  GridService,
  RestService
} from 'sqtm-core';
import {Subject} from 'rxjs';
import {concatMap, filter, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-test-automation-server-cell-renderer',
  template: `
    <ng-container *ngIf="row">
      <div
           class="full-height full-width flex-column icon-container current-workspace-main-color"
           (click)="removeItem(row)">
        <i nz-icon [nzType]="getIcon()" nzTheme="outline" class="table-icon-size"></i>
      </div>
    </ng-container>
  `,
  styleUrls: ['./delete-test-automation-server.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteTestAutomationServerComponent extends AbstractCellRendererComponent implements OnDestroy {

  unsub$ = new Subject<void>();

  constructor(public grid: GridService, cdr: ChangeDetectorRef,
              private dialogService: DialogService, private restService: RestService) {
    super(grid, cdr);
  }

  getIcon(): string {
    return 'sqtm-core-generic:delete';
  }

  public get rowHasAutomationProjectWithExecution(): boolean {
    return this.row.data['executionCount'] > 0;
  }

  removeItem(row: DataRow) {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.title.delete-one',
      messageKey: this.rowHasAutomationProjectWithExecution ?
        'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-one-with-bound-project' :
        'sqtm-core.administration-workspace.servers.test-automation-servers.dialog.message.delete-one-without-bound-project',
      level: 'DANGER'
    });

    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$),
        filter(result => result === true),
        concatMap(() => this.restService.delete([`test-automation-servers/${row.data['serverId']}`]))
      )
      .subscribe(() => {
        this.grid.refreshData();
      });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}


export function deleteTestAutomationServerColumn(id: string, label = ''): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DeleteTestAutomationServerComponent)
    .withLabel(label)
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(50));
}

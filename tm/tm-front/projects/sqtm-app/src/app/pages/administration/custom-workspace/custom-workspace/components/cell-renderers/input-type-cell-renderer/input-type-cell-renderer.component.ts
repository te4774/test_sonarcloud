import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, getInputTypeI18n, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-input-type-cell-renderer',
  template: `
        <ng-container *ngIf="columnDisplay && row">
          <div class="full-width full-height flex-column">
            <span *ngIf="row.data[columnDisplay.id]" style="margin: auto 0;"  class="sqtm-grid-cell-txt-renderer"
                  nz-tooltip [sqtmCoreLabelTooltip]="inputType | translate">
              {{inputType  | translate}}
            </span>
          </div>
        </ng-container>
   `,
  styleUrls: ['./input-type-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputTypeCellRendererComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  get inputType(): string {
    const inputType = this.row.data[this.columnDisplay.id];
    return getInputTypeI18n(inputType);
  }
}

export function inputTypeColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(InputTypeCellRendererComponent);
}

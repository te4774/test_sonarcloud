import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ProjectPermissionsPanelComponent} from './project-permissions-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogService, GridService, RestService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {OverlayModule} from '@angular/cdk/overlay';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {PROJECT_PERMISSIONS_TABLE} from '../../../project-view.constant';
import {ProjectViewService} from '../../../services/project-view.service';
import {of} from 'rxjs';
import {ReactiveFormsModule} from '@angular/forms';
import {mockAutoConfirmDialogService, mockGridService} from '../../../../../../../utils/testing-utils/mocks.service';
import createSpyObj = jasmine.createSpyObj;

describe('ProjectPermissionsPanelComponent', () => {
  let component: ProjectPermissionsPanelComponent;
  let fixture: ComponentFixture<ProjectPermissionsPanelComponent>;

  const gridService = mockGridService();
  const dialogService = mockAutoConfirmDialogService();

  const projectViewService = createSpyObj(['removePartyProjectPermissions', 'complete']);
  projectViewService.componentData$ = of({
    project: {
      partyProjectPermissions: [],
    }
  });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AppTestingUtilsModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        OverlayModule,
        ReactiveFormsModule,
      ],
      declarations: [ProjectPermissionsPanelComponent],
      providers: [
        {
          provide: RestService,
          useValue: {}
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: PROJECT_PERMISSIONS_TABLE,
          useValue: gridService,
        },
        {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: ProjectViewService,
          useValue: projectViewService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(waitForAsync(() => {
    fixture = TestBed.createComponent(ProjectPermissionsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    gridService.refreshData.calls.reset();
  }));

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should add user permissions', (() => {
    component.openAddUsersPermissionDialog();

    expect(gridService.refreshData).toHaveBeenCalledTimes(1);
  }));

  it('should add team permissions', waitForAsync(() => {
    component.openAddTeamsPermissionDialog();

    expect(gridService.refreshData).toHaveBeenCalledTimes(1);
  }));

  it('should delete permissions', waitForAsync(() => {
    (gridService as any).selectedRows$ = of([{data: {partyId: 3}}]);
    projectViewService.removePartyProjectPermissions.and.returnValue(of(null));

    component.openDeletePermissionsDialog();

    expect(projectViewService.removePartyProjectPermissions).toHaveBeenCalledWith([3]);
  }));
});

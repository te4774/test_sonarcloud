import {Component, OnInit, ChangeDetectionStrategy, OnDestroy} from '@angular/core';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  GenericEntityViewService, GridService, gridServiceFactory,
  isAdminOrProjectManager, ReferentialDataService, RestService
} from 'sqtm-core';
import {Router} from '@angular/router';
import {filter, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {EnvironmentVariableViewService} from '../../services/environment-variable-view.service';
import {
  EV_OPTIONS_TABLE,
  EV_OPTIONS_TABLE_CONF,
  evOptionsTableDefinition
} from '../../components/panels/environment-variable-options-panel/environment-variable-options-panel.component';

@Component({
  selector: 'sqtm-app-environment-variable-view-detail',
  templateUrl: './environment-variable-view-detail.component.html',
  styleUrls: ['./environment-variable-view-detail.component.less'],
  providers: [
    {
      provide: EnvironmentVariableViewService,
      useClass: EnvironmentVariableViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: EnvironmentVariableViewService
    },
    {
      provide: EV_OPTIONS_TABLE_CONF,
      useFactory: evOptionsTableDefinition
    },
    {
      provide: EV_OPTIONS_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, EV_OPTIONS_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: EV_OPTIONS_TABLE
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EnvironmentVariableViewDetailComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();

  constructor(public readonly adminReferentialDataService: AdminReferentialDataService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.adminReferentialDataService.refresh().subscribe();

    this.adminReferentialDataService.authenticatedUser$.pipe(
      takeUntil(this.unsub$),
      filter((authUser: AuthenticatedUser) => !isAdminOrProjectManager(authUser)),
    ).subscribe(() => this.router.navigate(['home-workspace']));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  back(): void {
    history.back();
  }

  handleEnvironmentVariableDeleted(): void {
    this.back();
  }

}

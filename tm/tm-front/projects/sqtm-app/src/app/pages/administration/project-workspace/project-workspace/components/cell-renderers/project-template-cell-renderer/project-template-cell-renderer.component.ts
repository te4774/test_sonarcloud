import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractCellRendererComponent, BooleanValueRenderer, ColumnDefinitionBuilder, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-template-cell-renderer',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column"
           nz-tooltip [nzTooltipTitle]="tooltipKey | translate">
        <span *ngIf="row.data[columnDisplay.id]" class="template-icon-container">
          <i nz-icon nzType="sqtm-core-administration:template" nzTheme="outline" class="table-icon-size"></i>
        </span>
      </div>
    </ng-container>`,
  styleUrls: ['./project-template-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectTemplateCellRendererComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  get tooltipKey(): string {
    const isTemplate = this.row.data[this.columnDisplay.id];
    return Boolean(isTemplate) ?
      'sqtm-core.entity.project.template.label.singular' : 'sqtm-core.entity.project.label.singular';
  }

}

export function templateColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ProjectTemplateCellRendererComponent).withExportValueRenderer(BooleanValueRenderer);
}

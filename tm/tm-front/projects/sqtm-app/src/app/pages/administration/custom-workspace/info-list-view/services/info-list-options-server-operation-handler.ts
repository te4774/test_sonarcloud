import {Injectable} from '@angular/core';
import {AbstractAdminServerOperationHandler, Identifier} from 'sqtm-core';
import {InfoListViewService} from './info-list-view.service';

@Injectable()
export class InfoListOptionsServerOperationHandler extends AbstractAdminServerOperationHandler {

  constructor(private readonly infoListViewService: InfoListViewService) {
    super();
  }

  doChangePosition(draggedRows: Identifier[], newPosition: number) {
    const optionIds = draggedRows.map(row => Number(row));
    return this.infoListViewService.changeOptionsPosition(optionIds, newPosition);
  }
}

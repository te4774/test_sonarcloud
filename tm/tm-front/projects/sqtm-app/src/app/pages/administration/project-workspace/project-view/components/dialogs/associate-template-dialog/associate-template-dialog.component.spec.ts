import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {AssociateTemplateDialogComponent} from './associate-template-dialog.component';
import {DialogReference, RestService, SelectFieldComponent} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {of} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {ProjectViewService} from '../../../services/project-view.service';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockRestService} from '../../../../../../../utils/testing-utils/mocks.service';
import {mockTextField} from '../../../../../../../utils/testing-utils/test-component-generator';
import SpyObj = jasmine.SpyObj;

describe('AssociateTemplateDialogComponent', () => {
  let component: AssociateTemplateDialogComponent;
  let fixture: ComponentFixture<AssociateTemplateDialogComponent>;
  const restService = mockRestService();
  const dialogReference = jasmine.createSpyObj(['close']);
  const projectViewService = jasmine.createSpyObj(['associateWithTemplate']);
  projectViewService.componentData$ = of({});

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: ProjectViewService,
          useValue: projectViewService,
        },
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
        {
          provide: TranslateService,
          useValue: jasmine.createSpyObj(['instant'])
        }
      ],
      declarations: [AssociateTemplateDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    restService.get.and.returnValue(of({templates: []}));
    fixture = TestBed.createComponent(AssociateTemplateDialogComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should enable template field if there are options', () => {
    const templateSelectField = mockTextField('template', component.formGroup) as unknown as SpyObj<SelectFieldComponent>;
    templateSelectField.disabled = true;
    component.templateSelectField = templateSelectField;
    restService.get.and.returnValue(of({templates: [{id: 1, name: 'number 1'}]}));
    fixture.detectChanges();

    expect(templateSelectField.disabled).toBe(false);
  });

  it('should show validation errors', () => {
    fixture.detectChanges();
    const templateSelectField = mockTextField('template', component.formGroup) as unknown as SpyObj<SelectFieldComponent>;
    component.templateSelectField = templateSelectField;
    component.formGroup.setErrors({error: 'kaboom'});

    component.confirm();

    expect(templateSelectField.showClientSideError).toHaveBeenCalled();
  });

  it('should confirm association', () => {
    fixture.detectChanges();
    component.formGroup.controls['template'].setValue('some value');
    component.confirm();

    expect(projectViewService.associateWithTemplate).toHaveBeenCalled();
    expect(dialogReference.close).toHaveBeenCalled();
  });
});

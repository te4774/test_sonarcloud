import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminScmServerViewState} from '../../../states/admin-scm-server-view-state';
import {takeUntil} from 'rxjs/operators';
import {ScmServerViewService} from '../../../services/scm-server-view.service';
import {GridService, gridServiceFactory, ReferentialDataService, RestService} from 'sqtm-core';
import {
  SCM_SERVER_REPOSITORIES_TABLE,
  SCM_SERVER_REPOSITORIES_TABLE_CONF,
  ScmServerRepositoriesPanelComponent,
  scmServerRepositoriesTableDefinition
} from '../../../components/panels/scm-server-repositories-panel/scm-server-repositories-panel.component';

@Component({
  selector: 'sqtm-app-scm-server-content',
  templateUrl: './scm-server-content.component.html',
  styleUrls: ['./scm-server-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: SCM_SERVER_REPOSITORIES_TABLE_CONF,
      useFactory: scmServerRepositoriesTableDefinition
    },
    {
      provide: SCM_SERVER_REPOSITORIES_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, SCM_SERVER_REPOSITORIES_TABLE_CONF, ReferentialDataService]
    }
  ]
})
export class ScmServerContentComponent implements OnInit, OnDestroy  {

  componentData$: Observable<AdminScmServerViewState>;

  unsub$ = new Subject<void>();

  @ViewChild(ScmServerRepositoriesPanelComponent)
  private scmServerRepositoriesPanelComponent;

  constructor(public readonly scmViewService: ScmServerViewService,
              @Inject(SCM_SERVER_REPOSITORIES_TABLE) public repositoriesGridService: GridService) {
    this.componentData$ = scmViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit(): void {
  }

  addRepository($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.scmServerRepositoriesPanelComponent.openAddRepositoryDialog();
  }

  deleteRepositories($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    this.scmServerRepositoriesPanelComponent.deleteRepositories();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

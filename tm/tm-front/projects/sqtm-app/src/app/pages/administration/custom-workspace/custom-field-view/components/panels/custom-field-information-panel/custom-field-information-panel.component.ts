import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActionErrorDisplayService, DialogService, getInputTypeI18n, InputType, Option} from 'sqtm-core';
import {CustomFieldViewService} from '../../../services/custom-field-view.service';
import {catchError, map, switchMap, take, takeUntil} from 'rxjs/operators';
import {NzCheckboxComponent} from 'ng-zorro-antd/checkbox';
import {Observable, of, Subject} from 'rxjs';
import {AdminCustomFieldViewComponentData} from '../../../containers/custom-field-view/custom-field-view.component';

@Component({
  selector: 'sqtm-app-custom-field-information-panel',
  templateUrl: './custom-field-information-panel.component.html',
  styleUrls: ['./custom-field-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomFieldInformationPanelComponent implements OnInit, OnDestroy {
  @Input()
  componentData: AdminCustomFieldViewComponentData;

  @ViewChild(NzCheckboxComponent)
  private optionalCheckBox: NzCheckboxComponent;

  defaultCheckedOptions: Option[];
  unsub$ = new Subject<void>();


  constructor(private translateService: TranslateService,
              private customFieldViewService: CustomFieldViewService,
              private dialogService: DialogService,
              private actionErrorDisplayService: ActionErrorDisplayService) {
  }

  get inputType(): InputType {
    return this.componentData.customField.inputType;
  }

  get translatedCustomFieldType(): string {
    const i18nKey = getInputTypeI18n(this.inputType);
    return this.translateService.instant(i18nKey);
  }

  get showOptionalField(): boolean {
    return this.inputType !== InputType.CHECKBOX;
  }

  get showDefaultValueField(): boolean {
    return this.inputType !== InputType.DROPDOWN_LIST;
  }

  ngOnInit(): void {
    this.initializeDefaultCheckedOptions();
  }

  addTagDefaultValue(tags: string[]) {
    this.customFieldViewService.changeDefaultValue(tags.join('|')).subscribe();
  }

  toggleOptional(isOptional: boolean) {
    if (this.checkIfDefaultValueExistsIfMandatory(isOptional)) {
      this.showConfirmChangeToMandatoryDefaultValueDialog().pipe(
        take(1),
        switchMap(({confirm}) => {
          if (!confirm) {
            this.optionalCheckBox.writeValue(true);
            return of({});
          } else {
           return this.customFieldViewService.toggleOptional(isOptional);
          }
        })
      ).subscribe();
    } else {
      this.customFieldViewService.toggleOptional(isOptional).pipe(
        catchError((error) => {
          this.optionalCheckBox.nzChecked = !isOptional;
          return this.actionErrorDisplayService.handleActionError(error);
        })
      ).subscribe();
    }
  }

  private checkIfDefaultValueExistsIfMandatory(isOptional: boolean): boolean {
    const defaultValueExists = this.componentData.customField.defaultValue !== '' && this.componentData.customField.defaultValue !== null;
    return !isOptional && defaultValueExists;
  }

  private showConfirmChangeToMandatoryDefaultValueDialog(): Observable<{confirm: boolean}> {
    const dialogReference = this.dialogService.openConfirm({
      id: 'cuf-disable-optional-dialog',
      titleKey: 'sqtm-core.administration-workspace.custom-fields.dialog.title.confirm-change-to-mandatory-default-value',
      messageKey: this.translateService.instant(
        'sqtm-core.administration-workspace.custom-fields.dialog.message.confirm-change-to-mandatory-default-value',
        {defaultValue: this.findActualDefaultValue()})
    });
    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirm => {
       return  ({confirm});
      })
    );
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initializeDefaultCheckedOptions(): void {
    this.defaultCheckedOptions = [
      {
        value: 'false',
        label: this.translateService.instant('sqtm-core.generic.label.false'),
      },
      {
        value: 'true',
        label: this.translateService.instant('sqtm-core.generic.label.true'),
      },
    ];
  }

  private findActualDefaultValue(): any {
    switch (this.componentData.customField.inputType) {
      case InputType.NUMERIC:
        return this.componentData.customField.numericDefaultValue;
      case InputType.RICH_TEXT:
        return this.componentData.customField.largeDefaultValue;
      default:
        return this.componentData.customField.defaultValue;
    }
  }
}

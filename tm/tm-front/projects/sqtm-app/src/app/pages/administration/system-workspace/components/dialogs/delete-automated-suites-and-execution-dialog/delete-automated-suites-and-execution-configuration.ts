import {AutomationDeletionCount, ConfirmDeleteLevel} from 'sqtm-core';

export class DeleteAutomatedSuitesAndExecutionConfiguration {
  id: string;
  titleKey: string;
  level: ConfirmDeleteLevel;
  messageKey: string;
  automationDeletionCount: AutomationDeletionCount;
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';
import {CustomFieldViewService} from '../../../services/custom-field-view.service';

@Component({
  selector: 'sqtm-app-option-color-cell',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column" style="justify-content: center;">
        <sqtm-core-color-picker-select-field
          [color]="color"
          [cpPosition]="'top-left'"
          (colorChanged)="changeColor($event)">
        </sqtm-core-color-picker-select-field>
      </div>
    </ng-container>`,
  styleUrls: ['./option-color-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionColorCellComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              private customFieldViewService: CustomFieldViewService) {
    super(grid, cdRef);
  }

  get color(): string {
    return this.row.data[this.columnDisplay.id];
  }

  changeColor(newColor: string) {
    this.customFieldViewService.changeOptionColor(this.row.data['label'], newColor);
  }
}

export function optionColorColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(OptionColorCellComponent);
}

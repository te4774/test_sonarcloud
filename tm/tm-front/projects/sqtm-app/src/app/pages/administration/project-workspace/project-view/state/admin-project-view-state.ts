import {GenericEntityViewState, provideInitialGenericViewState} from 'sqtm-core';
import {AdminProjectState} from './admin-project-state';

export interface AdminProjectViewState extends GenericEntityViewState<AdminProjectState, 'project'> {
  project: AdminProjectState;
}

export function provideInitialAdminProjectView(): Readonly<AdminProjectViewState> {
  return provideInitialGenericViewState<AdminProjectState, 'project'>('project');
}

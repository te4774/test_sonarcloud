import {AuthenticationProtocol} from 'sqtm-core';

export interface TAServerConnectInfo {
  id: number;
  authProtocol: AuthenticationProtocol;
}

export interface TAServerConnectConfiguration {
  taServer: TAServerConnectInfo;
}

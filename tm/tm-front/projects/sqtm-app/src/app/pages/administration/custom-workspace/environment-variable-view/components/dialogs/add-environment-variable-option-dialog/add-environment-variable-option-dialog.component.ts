import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {
  AbstractAdministrationCreationDialogDirective
} from '../../../../../components/abstract-administration-creation-dialog';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {
  DialogReference,
  FieldValidationError,
  GridService,
  isEVOptionCodePatternValid,
  RestService
} from 'sqtm-core';
import {Observable, of, throwError} from 'rxjs';
import {catchError, finalize, switchMap, take} from 'rxjs/operators';
import {EnvironmentVariableViewService} from '../../../services/environment-variable-view.service';

@Component({
  selector: 'sqtm-app-add-environment-variable-option-dialog',
  templateUrl: './add-environment-variable-option-dialog.component.html',
  styleUrls: ['./add-environment-variable-option-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddEnvironmentVariableOptionDialogComponent extends AbstractAdministrationCreationDialogDirective implements OnInit {

  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];

  constructor(protected dialogReference: DialogReference,
              protected restService: RestService,
              protected cdr: ChangeDetectorRef,
              private fb: FormBuilder,
              private environmentVariableViewService: EnvironmentVariableViewService,
              private grid: GridService) {
    super('', dialogReference, restService, cdr);
  }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  protected doResetForm(): void {
    this.resetFormControl('label', '');
    this.resetFormControl('code', '');
  }

  addEntity(addAnother?: boolean) {
    if (this.formIsValid()) {
      this.beginAsync();
      this.grid.beginAsyncOperation();
      this.getRequestPayload().pipe(
        take(1),
        switchMap(payload => this.environmentVariableViewService.addOption(payload)),
        catchError((error) => {
          this.handleCreationFailure(error);
          return throwError(error);
        }),
        finalize( () => this.grid.completeAsyncOperation())
      ).subscribe(result => this.handleCreationSuccess(result, addAnother));
    } else {
      this.showClientSideErrors();
    }
  }

  protected getRequestPayload(): Observable<any> {
    return of({
      label: this.getFormControlValue('label'),
      code: this.getFormControlValue('code')
    });
  }

  get textFieldToFocus(): string {
    return 'label';
  }

  private initializeFormGroup() {
    this.formGroup = this.fb.group({
      label: this.fb.control('', [
        Validators.maxLength(255),
        Validators.required,
      ]),
      code: this.fb.control('', [
        Validators.maxLength(30),
        Validators.required,
        this.codeValidator,
      ])
    });
  }

  get codeValidator(): ValidatorFn {
    return function (formControl: AbstractControl): ValidationErrors {
      const isCodeValid = isEVOptionCodePatternValid(formControl.value);
      return isCodeValid ? null : {invalidCodePattern: true};
    };
  }
}

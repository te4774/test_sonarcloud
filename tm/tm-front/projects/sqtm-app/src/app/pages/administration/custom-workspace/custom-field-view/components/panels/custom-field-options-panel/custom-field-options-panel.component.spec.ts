import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {
  CustomFieldOptionsPanelComponent,
  OPTIONS_TABLE,
  OPTIONS_TABLE_CONF,
  optionsTableDefinition
} from './custom-field-options-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {
  ActionErrorDisplayService,
  ActionValidationError,
  CustomField,
  DataRow,
  DialogService,
  GridService,
  InputType,
  RestService
} from 'sqtm-core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import {CustomFieldViewService} from '../../../services/custom-field-view.service';
import {of, throwError} from 'rxjs';
import {
  mockClosableDialogService,
  mockGridService,
  mockRestService
} from '../../../../../../../utils/testing-utils/mocks.service';
import {makeHttpActionError} from '../../../../../../../utils/testing-utils/test-error-response-generator';

describe('CustomFieldOptionsPanelComponent', () => {
  let component: CustomFieldOptionsPanelComponent;
  let fixture: ComponentFixture<CustomFieldOptionsPanelComponent>;
  const restService = mockRestService();
  const gridService = mockGridService();
  const dialogMock = mockClosableDialogService();
  const dialogService = dialogMock.service;
  const customFieldViewService = jasmine.createSpyObj(['deleteOptions']);
  customFieldViewService.componentData$ = of({
    customField: {
      inputType: InputType.DROPDOWN_LIST,
      options: [],
    } as CustomField
  });
  const actionErrorDisplayService = jasmine.createSpyObj(['handleActionError']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule, OverlayModule],
      declarations: [CustomFieldOptionsPanelComponent],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: OPTIONS_TABLE_CONF,
          useFactory: optionsTableDefinition,
        },
        {
          provide: OPTIONS_TABLE,
          useValue: gridService,
        },
        {
          provide: GridService,
          useValue: gridService,
        },
        {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: CustomFieldViewService,
          useValue: customFieldViewService,
        },
        {
          provide: ActionErrorDisplayService,
          useValue: actionErrorDisplayService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFieldOptionsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    dialogMock.resetCalls();
    dialogMock.resetSubjects();

    customFieldViewService.deleteOptions.calls.reset();
    customFieldViewService.deleteOptions.and.returnValue(of({}));
    actionErrorDisplayService.handleActionError.calls.reset();
    gridService.completeAsyncOperation.and.returnValue(of({}));
  });

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should open option creation dialog', waitForAsync(() => {
    component.openAddCustomFieldOptionDialog();
    expect(dialogService.openDialog).toHaveBeenCalled();
  }));

  it('should delete options', waitForAsync(() => {
    gridService.selectedRows$ = of([
      {data: {label: 'label1'}} as unknown as DataRow,
      {data: {label: 'label2'}} as unknown as DataRow,
    ]);

    component.deleteCustomFieldOptions();
    dialogMock.closeDialogsWithResult(true);

    expect(customFieldViewService.deleteOptions).toHaveBeenCalledWith(['label1', 'label2']);
  }));

  it('should handle action errors', waitForAsync(() => {
    actionErrorDisplayService.handleActionError.and.returnValue(of(null));
    gridService.selectedRows$ = of([
      {data: {label: 'label1'}} as unknown as DataRow,
    ]);
    const httpError = makeHttpActionError({} as ActionValidationError);
    customFieldViewService.deleteOptions.and.returnValue(throwError(httpError));

    component.deleteCustomFieldOptions();
    dialogMock.closeDialogsWithResult(true);

    expect(actionErrorDisplayService.handleActionError).toHaveBeenCalledWith(httpError);
  }));
});

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DataRow,
  DialogService,
  Fixed,
  GridService,
  RestService
} from 'sqtm-core';
import {Observable, Subject, switchMap} from 'rxjs';
import {concatMap, filter, pluck, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-scm-server-cell-renderer',
  template: `
    <ng-container *ngIf="row">
      <div *ngIf="canDelete()"
           class="full-height full-width flex-column icon-container current-workspace-main-color"
           (click)="removeItem(row)">
        <i nz-icon [nzType]="getIcon()" nzTheme="outline" class="table-icon-size"></i>
      </div>
    </ng-container>
  `,
  styleUrls: ['./delete-scm-server.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteScmServerComponent extends AbstractCellRendererComponent implements OnDestroy {

  @Input()
  row: DataRow;

  unsub$ = new Subject<void>();

  constructor(public grid: GridService, cdr: ChangeDetectorRef,
              private dialogService: DialogService, private restService: RestService) {
    super(grid, cdr);
  }

  getIcon(): string {
    return 'sqtm-core-generic:delete';
  }

  private checkServerUsage(): Observable<boolean> {
    const serverId = this.row.id;
    return this.restService.get<{ areUsed: boolean }>(['scm-server-view', serverId.toString(), 'are-used-by-test-cases']).pipe(
      pluck('areUsed')
    );
  }

  removeItem(row: DataRow) {
    this.checkServerUsage().pipe(
      switchMap(areReposUsedByTestCases => this.openConfirmDialog(areReposUsedByTestCases)),
      takeUntil(this.unsub$),
      filter(result => Boolean(result)),
      concatMap(() => this.restService.delete([`scm-servers/${row.data['serverId']}`]))
    ).subscribe(() => {
      this.grid.refreshData();
    });
  }

  private openConfirmDialog(areReposUsedByTestCases: boolean): Observable<boolean | undefined> {
    const messageKeyEnd = areReposUsedByTestCases ? 'with-test-cases' :
      (this.serverIsUsedByProject ? 'with-project' : 'without-project');
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.servers.scm-servers.dialog.title.delete-one',
      messageKey: `sqtm-core.administration-workspace.servers.scm-servers.dialog.message.delete-one-${messageKeyEnd}`,
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$;
  }

  get serverIsUsedByProject(): boolean {
    return this.row.data['projectCount'] > 0;
  }

  canDelete(): boolean {
    return true;
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function deleteScmServerColumn(id: string, label = ''): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(DeleteScmServerComponent)
    .withLabel(label)
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(50));
}

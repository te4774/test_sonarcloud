import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ProjectGridComponent} from './project-grid.component';
import {RouterTestingModule} from '@angular/router/testing';
import {
  AdminReferentialDataService,
  DataRow,
  DialogService,
  GRID_PERSISTENCE_KEY,
  GridService,
  GridTestingModule,
  GridWithStatePersistence,
  WorkspaceCommonModule,
  WorkspaceWithGridComponent
} from 'sqtm-core';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ADMIN_WS_PROJECT_TABLE} from '../../../project-workspace.constant';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {
  mockAdminReferentialDataService,
  mockClosableDialogService,
  mockGridService
} from '../../../../../../utils/testing-utils/mocks.service';
import {TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';

describe('ProjectGridComponent', () => {
  let component: ProjectGridComponent;
  let fixture: ComponentFixture<ProjectGridComponent>;
  const projectTable = mockGridService();

  const dialogServiceMock = mockClosableDialogService<any>();

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectGridComponent],
      imports: [
        RouterTestingModule,
        WorkspaceCommonModule,
        AppTestingUtilsModule,
        GridTestingModule,
        NzDropDownModule
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: AdminReferentialDataService,
          useValue: mockAdminReferentialDataService(),
        },
        {
          provide: DialogService,
          useValue: dialogServiceMock.service,
        },
        {
          provide: TranslateService,
          useValue: {},
        },
        {
          provide: ADMIN_WS_PROJECT_TABLE,
          useValue: projectTable,
        },
        {
          provide: GridService,
          useValue: projectTable,
        },
        {
          provide: WorkspaceWithGridComponent,
          useValue: {},
        },
        {
          provide: GRID_PERSISTENCE_KEY,
          useValue: 'project-workspace-main-grid'
        },
        GridWithStatePersistence
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    projectTable.refreshData.calls.reset();
    dialogServiceMock.resetCalls();
    dialogServiceMock.resetSubjects();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open project creation dialog', () => {
    component.openProjectCreationDialog();
    dialogServiceMock.closeDialogsWithResult({id: 1});
    expect(projectTable.refreshDataAsync).toHaveBeenCalled();
    expect(dialogServiceMock.service.openDialog).toHaveBeenCalled();
  });

  it('should open template creation dialog', () => {
    component.openTemplateCreationDialog();
    dialogServiceMock.closeDialogsWithResult({id: 1});
    expect(projectTable.refreshDataAsync).toHaveBeenCalled();
    expect(dialogServiceMock.service.openDialog).toHaveBeenCalled();
  });

  it('should open create project from template dialog', () => {
    const selection = [{
      data: {
        name: 'template',
        projectId: 1,
      }
    } as unknown as DataRow];

    projectTable.selectedRows$ = of(selection);
    component.openTemplateFromProjectCreationDialog();
    dialogServiceMock.closeDialogsWithResult(true);
    expect(dialogServiceMock.service.openDialog).toHaveBeenCalled();
  });

  it('should not open create project from template dialog if selection does not allow it', () => {
    const selections = [
      [],
      [1, 2],
    ];

    selections.forEach(selection => {
      projectTable.selectedRows$ = of(selection as unknown as DataRow[]);
      component.openTemplateFromProjectCreationDialog();
      dialogServiceMock.closeDialogsWithResult(true);
      expect(dialogServiceMock.service.openDialog).not.toHaveBeenCalled();
      expect(projectTable.refreshData).not.toHaveBeenCalled();
    });
  });
});

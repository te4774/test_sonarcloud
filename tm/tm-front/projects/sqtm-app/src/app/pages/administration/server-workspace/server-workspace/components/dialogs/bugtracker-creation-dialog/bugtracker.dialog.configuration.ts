import {DisplayOption} from 'sqtm-core';

export interface BugtrackerDialogConfiguration {
  titleKey: string;
  bugtrackerKinds: DisplayOption[];
}

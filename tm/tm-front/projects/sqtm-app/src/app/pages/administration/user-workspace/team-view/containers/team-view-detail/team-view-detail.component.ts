import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {GenericEntityViewService} from 'sqtm-core';
import {TeamViewService} from '../../services/team-view.service';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {map, takeUntil} from 'rxjs/operators';
import {AdminTeamViewComponentData} from '../team-view/team-view.component';

@Component({
  selector: 'sqtm-app-team-view-detail',
  templateUrl: './team-view-detail.component.html',
  styleUrls: ['./team-view-detail.component.less'],
  providers: [
    {
      provide: TeamViewService,
      useClass: TeamViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: TeamViewService
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeamViewDetailComponent implements OnInit, OnDestroy {

  public readonly componentData$: Observable<AdminTeamViewComponentData>;
  private unsub$ = new Subject<void>();

  constructor(private route: ActivatedRoute,
              private teamViewService: TeamViewService) {
    this.componentData$ = this.teamViewService.componentData$;
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('teamId')),
      ).subscribe((id) => {
      this.teamViewService.load(parseInt(id, 10));
    });
  }

  ngOnDestroy(): void {
    this.teamViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  back(): void {
    history.back();
  }
}

export enum MilestoneRowFields {
  ownerId = 'ownerId',
  projectCount = 'projectCount',
  milestoneId = 'milestoneId',
  range = 'range',
  status = 'status',
  label = 'label',
  ownerLogin = 'ownerLogin',
  createdBy = 'createdBy',
}

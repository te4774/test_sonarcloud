import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild} from '@angular/core';
import {SystemViewState} from '../../../states/system-view.state';
import {SystemViewService} from '../../../services/system-view.service';
import {EditableTextFieldComponent, extractSquashFirstFieldError} from 'sqtm-core';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-system-callback-url-panel',
  templateUrl: './system-callback-url-panel.component.html',
  styleUrls: [
    './system-callback-url-panel.component.less',
    '../../../styles/system-workspace.common.less'
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SystemCallbackUrlPanelComponent implements OnInit {

  @Input()
  componentData: SystemViewState;

  @ViewChild(EditableTextFieldComponent)
  textField: EditableTextFieldComponent;

  constructor(private systemViewService: SystemViewService,
              private translateService: TranslateService) { }

  ngOnInit(): void {
  }

  changeCallbackUrl(callbackUrl: string) {
    this.systemViewService.changeCallbackUrl(callbackUrl).pipe(
      catchError((error) => this.handleError(error, 'callbackUrl'))
    ).subscribe();
  }

  private handleError(error, fieldName: string) {
    const squashTMerror = extractSquashFirstFieldError(error);
    const i18nkey = this.translateService.instant(squashTMerror.fieldValidationErrors[0].i18nKey);
    if (squashTMerror.kind === 'FIELD_VALIDATION_ERROR' && squashTMerror.fieldValidationErrors[0].fieldName === fieldName) {
      this.textField.showExternalErrorMessage([i18nkey]);
    } else {
      console.error(error);
    }
    return of(error);
  }
}

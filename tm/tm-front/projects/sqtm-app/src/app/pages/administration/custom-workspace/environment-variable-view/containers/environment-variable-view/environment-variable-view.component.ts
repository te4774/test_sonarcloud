import {Component, OnInit, ChangeDetectionStrategy, OnDestroy, Output, EventEmitter} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AdminEnvironmentVariableViewState} from '../../states/admin-environment-variable-view-state';
import {EnvironmentVariableViewService} from '../../services/environment-variable-view.service';
import {concatMap, filter, map, take, takeUntil} from 'rxjs/operators';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {
  DialogService,
  GenericEntityViewComponentData,
  GenericEntityViewService, GridService, RestService
} from 'sqtm-core';
import {AdminEnvironmentVariableState} from '../../states/admin-environment-variable-state';

@Component({
  selector: 'sqtm-app-environment-variable-view',
  templateUrl: './environment-variable-view.component.html',
  styleUrls: ['./environment-variable-view.component.less'],
  providers: [
    {
      provide: EnvironmentVariableViewService,
      useClass: EnvironmentVariableViewService,
    },
    {
      provide: GenericEntityViewService,
      useExisting: EnvironmentVariableViewService
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EnvironmentVariableViewComponent implements OnInit, OnDestroy {

  componentData$: Observable<AdminEnvironmentVariableViewState>;
  private unsub$ = new Subject<void>();

  @Output()
  environmentVariableDeleted = new EventEmitter<void>();

  constructor(private route: ActivatedRoute,
              public readonly environmentVariableViewService: EnvironmentVariableViewService,
              private gridService: GridService,
              private dialogService: DialogService,
              private restService: RestService
  ) {
    this.componentData$ = this.environmentVariableViewService.componentData$;
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      takeUntil(this.unsub$),
      map((params: ParamMap) => params.get('environmentVariableId')),
    ).subscribe((id) => {
      this.environmentVariableViewService.load(parseInt(id, 10));
    });

    this.prepareGridRefreshOnEntityChanges();
  }

  ngOnDestroy(): void {
    this.environmentVariableViewService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  handleDelete(): void {
    this.componentData$.pipe(
      take(1),
      concatMap((componentData: AdminEnvironmentVariableViewComponentData) =>
        this.showConfirmDeleteDialog(componentData.environmentVariable.id,
          componentData.environmentVariable.boundToServer)
      ),
      filter(({confirmDelete}) => confirmDelete),
      concatMap(({evId}) => this.deleteEnvironmentVariableServerSide(evId)),
    ).subscribe(() => {
      this.gridService.refreshData();
      this.notifyEnvironmentVariableWasDeleted();
    });
  }

  private prepareGridRefreshOnEntityChanges(): void {
    this.environmentVariableViewService.simpleAttributeRequiringRefresh = [
      'name',
      'code'
    ];

    this.environmentVariableViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(() =>
      this.gridService.refreshDataAndKeepSelectedRows());
  }

  private showConfirmDeleteDialog(evId: any, boundToServer: boolean): Observable<{ confirmDelete: boolean, evId: string }> {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.environment-variable.dialog.title.delete-one',
      messageKey: boundToServer ? 'sqtm-core.administration-workspace.environment-variable.dialog.message.delete-bound-environment-variable' :
        'sqtm-core.administration-workspace.environment-variable.dialog.message.delete-one',
      level: 'DANGER',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, evId})),
    );
  }

  private deleteEnvironmentVariableServerSide(evId: any) {
    return this.restService.delete(['environment-variables', evId]);
  }

  private notifyEnvironmentVariableWasDeleted(): void {
    this.environmentVariableDeleted.emit();
  }
}

export interface AdminEnvironmentVariableViewComponentData extends GenericEntityViewComponentData<AdminEnvironmentVariableState, 'environmentVariable'> {
}

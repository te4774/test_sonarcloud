import {Component, OnInit, ChangeDetectionStrategy, InjectionToken, OnDestroy, ViewContainerRef} from '@angular/core';
import {
  ActionErrorDisplayService,
  DataRow, DefaultGridDraggedContentComponent, deleteColumn,
  DialogService,
  Extendable, GenericDataRow,
  GridDefinition, GridService,
  indexColumn,
  smallGrid,
  StyleDefinitionBuilder
} from 'sqtm-core';
import {concatMap, Observable, Subject} from 'rxjs';
import {
  AdminEnvironmentVariableViewComponentData
} from '../../../containers/environment-variable-view/environment-variable-view.component';
import {EnvironmentVariableViewService} from '../../../services/environment-variable-view.service';
import {catchError, filter, finalize, map, pluck, take, takeUntil, tap} from 'rxjs/operators';
import {evOptionLabelColumn} from '../../cell-renderers/ev-option-label-cell/ev-option-label-cell.component';
import {evOptionCodeColumn} from '../../cell-renderers/ev-option-code-cell/ev-option-code-cell.component';
import {
  AddEnvironmentVariableOptionDialogComponent
} from '../../dialogs/add-environment-variable-option-dialog/add-environment-variable-option-dialog.component';
import {EvOptionRemoveCellComponent} from '../../cell-renderers/ev-option-remove-cell/ev-option-remove-cell.component';
import {DummyPermissions} from '../../../../../services/dummy-permissions';

export const EV_OPTIONS_TABLE_CONF = new InjectionToken('EV_OPTIONS_TABLE_CONF');
export const EV_OPTIONS_TABLE = new InjectionToken('EV_OPTIONS_TABLE');

function environmentVariableOptionsDataRowFactory(literals: Partial<DataRow>[]): GenericDataRow[] {
  return literals.map(literal => {
    const row = new GenericDataRow();
    row.data = literal.data;
    row.id = literal.data.label;
    row.simplePermissions = new DummyPermissions();
    // This is a way to set the DefaultGridDraggedContent text.
    row.data['NAME'] = literal.data.label;
    return row;
  });
}

export function evOptionsTableDefinition(): GridDefinition {
  return smallGrid('options')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      evOptionLabelColumn('label')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5))
        .disableSort(),
      evOptionCodeColumn('code')
        .withI18nKey('sqtm-core.generic.label.code')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5))
        .disableSort(),
      deleteColumn(EvOptionRemoveCellComponent)
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .enableInternalDrop()
    .enableDrag()
    .withDraggedContentRenderer(DefaultGridDraggedContentComponent)
    .withRowConverter(environmentVariableOptionsDataRowFactory)
    .build();
}

@Component({
  selector: 'sqtm-app-environment-variable-options-panel',
  template: `
    <sqtm-core-grid></sqtm-core-grid>
  `,
  styleUrls: ['./environment-variable-options-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: EV_OPTIONS_TABLE
    }
  ]
})
export class EnvironmentVariableOptionsPanelComponent implements OnInit, OnDestroy {

  private componentData$: Observable<AdminEnvironmentVariableViewComponentData>;
  private unsub$ = new Subject<void>();

  constructor(private environmentVariableViewService: EnvironmentVariableViewService,
              private gridService: GridService,
              private dialogService: DialogService,
              private vcRef: ViewContainerRef,
              private actionErrorDisplayService: ActionErrorDisplayService) {
  }

  ngOnInit(): void {
    this.componentData$ = this.environmentVariableViewService.componentData$;
    this.initializeTable();
  }

  private initializeTable() {
    const optionsTable$ = this.componentData$.pipe(
      takeUntil(this.unsub$),
      pluck('environmentVariable', 'options')
    );
    this.gridService.connectToDatasource(optionsTable$, 'label');
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openAddEnvironmentVariableOptionDialog(): void {
    this.dialogService.openDialog({
      id: 'add-environment-variable-option-dialog',
      component: AddEnvironmentVariableOptionDialogComponent,
      viewContainerReference: this.vcRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.environment-variable.dialog.title.add-option',
        addAnotherLabelKey: 'sqtm-core.generic.label.add-another.feminine'
      },
      width: 720
    });
  }

  openDeleteEnvironmentVariableOptionsDialog(): void {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.data.label)),
      concatMap((optionLabels: string[]) => this.showConfirmDeleteOptionsDialog(optionLabels)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({optionLabels}) => this.environmentVariableViewService.deleteOptions(optionLabels)),
      catchError((error) => this.actionErrorDisplayService.handleActionError(error)),
      finalize(() => this.gridService.completeAsyncOperation())
    ).subscribe();
  }


  private showConfirmDeleteOptionsDialog(optionLabels: string[]): Observable<{confirmDelete: boolean, optionLabels: string[]}> {
   const dialogReference = this.dialogService.openDeletionConfirm({
     titleKey: 'sqtm-core.administration-workspace.entities-customization.generic.dialog.option.title.delete-many',
     messageKey: 'sqtm-core.administration-workspace.environment-variable.dialog.message.option.delete-many',
     level: 'WARNING',
   });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, optionLabels}))
    );
  }
}

import {Component, OnInit, ChangeDetectionStrategy, Input, OnDestroy} from '@angular/core';
import {
  AdminEnvironmentVariableViewComponentData
} from '../../../containers/environment-variable-view/environment-variable-view.component';
import {Subject} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {EnvironmentVariableViewService} from '../../../services/environment-variable-view.service';
import {EvInputType, getEvInputTypeI18n} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-environment-variable-information-panel',
  templateUrl: './environment-variable-information-panel.component.html',
  styleUrls: ['./environment-variable-information-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EnvironmentVariableInformationPanelComponent implements OnInit, OnDestroy {


  @Input()
  componentData: AdminEnvironmentVariableViewComponentData;

  unsub$ = new Subject<void>();
  constructor(private translateService: TranslateService,
              private environmentVariableViewService: EnvironmentVariableViewService) { }

  ngOnInit(): void {
  }

  get inputType(): EvInputType {
    return this.componentData.environmentVariable.inputType;
  }

  get translatedEnvironmentVariableType(): string {
    const i18nKey = getEvInputTypeI18n(this.inputType);
    return this.translateService.instant(i18nKey);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}

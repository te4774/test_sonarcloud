import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {AddJobDialogComponent} from './add-job-dialog.component';
import {DataRow, DialogReference, FieldValidationError, GridService, RestService} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {ProjectViewService} from '../../../services/project-view.service';
import {of, Subject, throwError} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AddJobDialogService} from '../../../services/add-job-dialog.service';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {
  mockGridService,
  mockPassThroughTranslateService,
  mockRestService
} from '../../../../../../../utils/testing-utils/mocks.service';
import {makeHttpFieldValidationError} from '../../../../../../../utils/testing-utils/test-error-response-generator';

const LABEL_KEY = 'sqtm-core.administration-workspace.views.project.automation.jobs.tm-label';

describe('BindTestAutomationProjectDialogComponent', () => {
  let component: AddJobDialogComponent;
  let fixture: ComponentFixture<AddJobDialogComponent>;

  const restService = mockRestService();
  const dialogReference = jasmine.createSpyObj(['close']);
  const dialogClosed$ = new Subject<void>();
  dialogReference.dialogClosed$ = dialogClosed$;
  const gridService = mockGridService();
  const projectViewService = jasmine.createSpyObj(['addTestAutomationProjects']);
  projectViewService.componentData$ = of({
    project: {id: 123}
  });

  const addJobDialogService = jasmine.createSpyObj(['load', 'unload']);
  addJobDialogService.dataState$ = of(null);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, ReactiveFormsModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: ProjectViewService,
          useValue: projectViewService,
        },
        {
          provide: DialogReference,
          useValue: jasmine.createSpyObj(['close'])
        },
        {
          provide: TranslateService,
          useValue:  mockPassThroughTranslateService(),
        },
        {
          provide: AddJobDialogService,
          useValue: addJobDialogService,
        },
        {
          provide: DialogReference,
          useValue: dialogReference,
        },
        {
          provide: GridService,
          useValue: gridService,
        }
      ],
      declarations: [AddJobDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddJobDialogComponent);
    component = fixture.componentInstance;
    component['gridService'] = gridService;
    fixture.detectChanges();

    dialogReference.close.calls.reset();
    projectViewService.addTestAutomationProjects.calls.reset();
    projectViewService.addTestAutomationProjects.and.returnValue(of({}));
    addJobDialogService.unload.calls.reset();
    spyOn(console, 'error');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add a job', () => {
    gridService.selectedRows$ = of([{
      data: {label: 'job'}
    } as unknown as DataRow]);

    component.confirm();

    expect(dialogReference.close).toHaveBeenCalled();
    expect(projectViewService.addTestAutomationProjects).toHaveBeenCalled();
  });

  it('should unload service when dialog is closed', () => {
    dialogClosed$.next();

    expect(addJobDialogService.unload).toHaveBeenCalled();
  });

  it('should show server errors when adding a job', () => {
    gridService.selectedRows$ = of([{
      data: {label: 'job'}
    } as unknown as DataRow]);

    const fve: FieldValidationError = {
      i18nKey: 'badaboom',
      fieldName: 'it goes'
    } as FieldValidationError;

    projectViewService.addTestAutomationProjects.and
      .returnValue(throwError(makeHttpFieldValidationError([fve])));

    component.confirm();

    expect(dialogReference.close).not.toHaveBeenCalled();
    expect(component.errors).toEqual(['it goes: badaboom']);
  });

  it('should prevent label duplicates', () => {
    gridService.selectedRows$ = of([
      {data: {label: 'job'}} as unknown as DataRow,
      {data: {label: 'job'}} as unknown as DataRow,
    ]);

    component.confirm();

    expect(dialogReference.close).not.toHaveBeenCalled();
    expect(component.errors).toEqual([`${LABEL_KEY}: sqtm-core.exception.duplicate.tmlabel`]);
    expect(projectViewService.addTestAutomationProjects).not.toHaveBeenCalled();
  });

  it('should prevent empty labels', () => {
    gridService.selectedRows$ = of([
      {data: {label: 'job'}} as unknown as DataRow,
      {data: {label: ''}} as unknown as DataRow,
    ]);

    component.confirm();

    expect(dialogReference.close).not.toHaveBeenCalled();
    expect(component.errors).toEqual([`${LABEL_KEY}: sqtm-core.validation.errors.required`]);
    expect(projectViewService.addTestAutomationProjects).not.toHaveBeenCalled();
  });

  it('should log unhandled errors to console', () => {
    gridService.selectedRows$ = of([{
      data: {label: 'job'}
    } as unknown as DataRow]);

    projectViewService.addTestAutomationProjects.and.returnValue(throwError({type: 'unhandled'}));

    component.confirm();

    expect(dialogReference.close).not.toHaveBeenCalled();
    expect(console.error).toHaveBeenCalled();
  });
});

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  AbstractListCellRendererComponent,
  AclGroup,
  ActionErrorDisplayService,
  ColumnDefinitionBuilder,
  getAclGroupI18nKey,
  GridService,
  ListPanelItem,
  PermissionGroup,
  RestService
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {ConnectedPosition, Overlay} from '@angular/cdk/overlay';
import {TeamViewService} from '../../../services/team-view.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-team-authorisation-profile-cell',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height __hover_pointer interactive-container"
           (click)="showProfileList()">
        <span #profileName
              class="text-ellipsis"
              style="margin: auto 0;"
              nz-tooltip
              [sqtmCoreLabelTooltip]="cellText">
          {{cellText}}
        </span>
        <ng-template #templatePortalContent>
          <sqtm-core-list-panel
              [selectedItem]="row.data[columnDisplay.id]"
              (itemSelectionChanged)="change($event)"
              [items]="panelItems">
          </sqtm-core-list-panel>
        </ng-template>
      </div>
    </ng-container>`,
  styleUrls: ['./team-authorisation-profile-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeamAuthorisationProfileCellComponent extends AbstractListCellRendererComponent {

  readonly panelItems: ListPanelItem[] = [];

  @ViewChild('templatePortalContent', {read: TemplateRef})
  templatePortalContent: TemplateRef<any>;

  @ViewChild('profileName', {read: ElementRef})
  profileName: ElementRef;

  constructor(public readonly grid: GridService,
              public readonly cdRef: ChangeDetectorRef,
              public readonly translateService: TranslateService,
              public readonly overlay: Overlay,
              public readonly vcr: ViewContainerRef,
              public readonly restService: RestService,
              public readonly teamViewService: TeamViewService,
              public readonly actionErrorDisplayService: ActionErrorDisplayService) {
    super(grid, cdRef, overlay, vcr, translateService, restService, actionErrorDisplayService);

    this.panelItems = this.getPanelItems();
  }

  get cellText(): string {
    if (this.panelItems == null) {
      return '';
    }

    const permissionGroup = this.row.data['permissionGroup'] as PermissionGroup;
    const groupName = permissionGroup.qualifiedName;

    const foundItem = this.panelItems.find(item => item.id === groupName);
    return foundItem?.label ?? '';
  }

  private getPanelItems(): ListPanelItem[] {
    const panelItems = Object.keys(AclGroup)
      .map(key => AclGroup[key])
      .map((groupName) => {
        return {
          label: this.translateService.instant(getAclGroupI18nKey(groupName)),
          id: groupName,
        };
      });

    // Sort options by locale label
    panelItems.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    return panelItems;
  }

  change(newValue: any) {
    this.grid.beginAsyncOperation();
    this.teamViewService.setTeamAuthorisation([this.row.data['projectId']], newValue).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe();
    this.close();
  }

  canEdit(): boolean {
    return true;
  }

  showProfileList() {
    if (this.canEdit()) {
      this.showList(this.profileName, this.templatePortalContent, LIST_POSITIONS);
    }
  }
}

export function teamAuthorisationProfileColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(TeamAuthorisationProfileCellComponent)
    .withI18nKey('sqtm-core.administration-workspace.views.project.permissions.profile.label');
}

const LIST_POSITIONS: ConnectedPosition[] = [
  {originX: 'start', overlayX: 'start', originY: 'bottom', overlayY: 'top', offsetX: -10, offsetY: 6},
  {originX: 'start', overlayX: 'start', originY: 'top', overlayY: 'bottom', offsetX: -10, offsetY: -6},
];

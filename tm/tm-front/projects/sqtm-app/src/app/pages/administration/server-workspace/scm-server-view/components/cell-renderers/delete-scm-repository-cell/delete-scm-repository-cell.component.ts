import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {AbstractCellRendererComponent, DialogService, GridService, RestService} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {ScmServerViewService} from '../../../services/scm-server-view.service';
import {concatMap, filter, finalize, map, switchMap, take, takeUntil, tap} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-scm-repository-cell',
  template: `
    <sqtm-core-delete-icon (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./delete-scm-repository-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteScmRepositoryCellComponent extends AbstractCellRendererComponent implements OnDestroy {

  unsub$ = new Subject<void>();

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private scmServerViewService: ScmServerViewService,
              private restService: RestService) {
    super(grid, cdr);
  }

  showDeleteConfirm() {
    this.checkIfRepositoryBoundToProject().pipe(
      take(1),
      concatMap(boundToProject => this.showDialog(boundToProject)),
      filter((confirmDelete) => confirmDelete === true),
      tap(() => this.grid.beginAsyncOperation()),
      switchMap(() => this.doDelete()),
      finalize(() => this.grid.completeAsyncOperation()),
    ).subscribe();
  }

  private showDialog(boundToProject: boolean) {
    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: this.getTitleKey(),
      messageKey: this.getCustomMessageKey(boundToProject),
      level: 'WARNING',
    });

    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => confirmDelete)
    );
  }

  doDelete() {
    const scmRepositoryId = this.row.data.scmRepositoryId;
    return this.scmServerViewService.deleteRepositories([scmRepositoryId]);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.servers.scm-servers.dialog.title.delete-one-repository';
  }

  protected getCustomMessageKey(boundToProject: boolean): string {
    return boundToProject ?
      'sqtm-core.administration-workspace.servers.scm-servers.dialog.message.delete-one-repository-bound-to-project' :
      'sqtm-core.administration-workspace.servers.scm-servers.dialog.message.delete-one-repository';
  }

  private checkIfRepositoryBoundToProject():  Observable<boolean> {
    const scmRepositoryId = this.row.data.scmRepositoryId;
    return this.restService.get<boolean>(['scm-repositories', 'check-bound-to-project', scmRepositoryId]);
  }
}



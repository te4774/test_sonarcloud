import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, ViewContainerRef} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DataRow,
  DialogService,
  Fixed,
  GridService,
  RestService
} from 'sqtm-core';
import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import {
  EditProjectAutomationJobDialogComponent
} from '../../dialogs/edit-project-automation-job-dialog/edit-project-automation-job-dialog.component';

@Component({
  selector: 'sqtm-app-edit-project-job-cell',
  template: `
    <ng-container *ngIf="row">
      <div class="full-height full-width flex-column icon-container current-workspace-main-color"
           (click)="editJob(row)">
        <i nz-icon [nzType]="getIcon()" nzTheme="outline" class="table-icon-size"></i>
      </div>
    </ng-container>
  `,
  styleUrls: ['./edit-project-job-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditProjectJobCellComponent extends AbstractCellRendererComponent implements OnDestroy {

  unsub$ = new Subject<void>();

  constructor(public grid: GridService, cdr: ChangeDetectorRef,
              private dialogService: DialogService,
              private restService: RestService,
              private viewContainerRef: ViewContainerRef) {
    super(grid, cdr);
  }

  getIcon(): string {
    return 'edit';
  }

  editJob(row: DataRow) {
    const dialogReference = this.dialogService.openDialog({
      component: EditProjectAutomationJobDialogComponent,
      data: {
        canRunBdd: row.data.canRunBdd,
        taProjectId: row.data.taProjectId,
        remoteName: row.data.remoteName,
        label: row.data.label,
        executionEnvironment:  row.data.executionEnvironment
      },
      id: 'project-automation-job-dialog',
      width: 600,
      viewContainerReference: this.viewContainerRef,
    });

    dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      filter(result => result != null)
    ).subscribe(() => {
      this.grid.refreshData();
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function editJobColumn(id: string, label = ''): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(EditProjectJobCellComponent)
    .withLabel(label)
    .disableSort()
    .changeWidthCalculationStrategy(new Fixed(50));
}

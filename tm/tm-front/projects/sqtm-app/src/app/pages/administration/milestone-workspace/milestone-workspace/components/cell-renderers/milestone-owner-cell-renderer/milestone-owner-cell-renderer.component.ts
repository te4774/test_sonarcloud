import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  formatMilestoneOwner,
  GridService,
  Milestone
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-milestone-owner-cell-renderer',
  template: `
    <ng-container *ngIf="columnDisplay && row">
      <div class="full-width full-height flex-column">
            <span *ngIf="row.data[columnDisplay.id]" style="margin: auto 5px;" class="sqtm-grid-cell-txt-renderer"
                  nz-tooltip [sqtmCoreLabelTooltip]="milestoneOwner | translate">
              {{milestoneOwner|translate}}
            </span>
      </div>
    </ng-container>
  `,
  styleUrls: ['./milestone-owner-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MilestoneOwnerCellRendererComponent extends AbstractCellRendererComponent {

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef) {
    super(grid, cdRef);
  }

  get milestoneOwner(): string {
    return formatMilestoneOwner(this.row.data as Milestone);
  }
}

export function milestoneOwnerColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(MilestoneOwnerCellRendererComponent);
}

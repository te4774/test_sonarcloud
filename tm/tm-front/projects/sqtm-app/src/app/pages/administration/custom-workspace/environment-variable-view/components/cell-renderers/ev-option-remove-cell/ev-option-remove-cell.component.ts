import {Component, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy} from '@angular/core';
import {AbstractDeleteCellRenderer, DialogService, GridService} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {AdminEnvironmentVariableViewState} from '../../../states/admin-environment-variable-view-state';
import {EnvironmentVariableViewService} from '../../../services/environment-variable-view.service';
import {finalize, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-ev-option-remove-cell',
  template: `
    <ng-container *ngIf="componentData$ | async as componentData">
    <sqtm-core-delete-icon
      (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
    </ng-container>
  `,
  styleUrls: ['./ev-option-remove-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EvOptionRemoveCellComponent extends AbstractDeleteCellRenderer implements  OnDestroy {

  componentData$: Observable<AdminEnvironmentVariableViewState>;
  unsub$ = new Subject<void>();

  constructor(public grid: GridService,
              cdRef: ChangeDetectorRef,
              protected dialogService: DialogService,
              private environmentVariableViewService:  EnvironmentVariableViewService) {
    super(grid, cdRef, dialogService);
    this.componentData$ = this.environmentVariableViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  protected doDelete() {
    this.grid.beginAsyncOperation();
   this.environmentVariableViewService.deleteOptions([this.row.data['label']]).pipe(
     finalize(() => this.grid.completeAsyncOperation())
   ).subscribe();
  }

  protected getTitleKey(): string {
    return 'sqtm-core.administration-workspace.entities-customization.generic.dialog.option.title.delete-one';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.administration-workspace.environment-variable.dialog.message.option.delete-one';
  }
}

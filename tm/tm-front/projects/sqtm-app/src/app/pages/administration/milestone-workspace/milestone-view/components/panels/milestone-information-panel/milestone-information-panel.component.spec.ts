import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {MilestoneInformationPanelComponent} from './milestone-information-panel.component';
import {MilestoneViewService} from '../../../services/milestone-view.service';
import {
  AdminReferentialDataService,
  DialogService,
  EditableSearchableSelectFieldComponent,
  EditableSelectFieldComponent,
  GridService,
  MilestoneRange,
  UiManagerService,
  User,
  WorkspaceDirective
} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {OverlayModule} from '@angular/cdk/overlay';
import {ChangeDetectorRef, NO_ERRORS_SCHEMA} from '@angular/core';
import {BehaviorSubject, of, throwError} from 'rxjs';
import {AdminMilestoneViewComponentData} from '../../../containers/milestone-view/milestone-view.component';
import {AdminMilestoneState} from '../../../states/admin-milestone-state';
import {HttpErrorResponse} from '@angular/common/http';
import {mockDialogService} from '../../../../../../../utils/testing-utils/mocks.service';

describe('MilestoneInformationPanelComponent', () => {
  let component: MilestoneInformationPanelComponent;
  let fixture: ComponentFixture<MilestoneInformationPanelComponent>;
  const milestoneViewService = jasmine.createSpyObj([
    'findPossibleOwners', 'setRange', 'setOwner'
  ]);

  (milestoneViewService as any).isMilestoneOwner$ = of({});
  (milestoneViewService as any).canEditOwnerField$ = of({});
  (milestoneViewService as any).isMilestoneBoundToTemplate$ = new BehaviorSubject<boolean>(false);

  const dialogService = mockDialogService();

  const adminReferentialDataService = {
    authenticatedUser$: of({}),
    loggedAsAdmin$: new BehaviorSubject<boolean>(true),
  };

  const uiManagerService = jasmine.createSpyObj(['closeContextualContent']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, OverlayModule],
      declarations: [MilestoneInformationPanelComponent],
      providers: [
        {
          provide: MilestoneViewService,
          useValue: milestoneViewService,
        },
        {
          provide: AdminReferentialDataService,
          useValue: adminReferentialDataService,
        },
        {
          provide: ChangeDetectorRef,
          useValue: jasmine.createSpyObj(['detectChanges']),
        },
        {
          provide: GridService,
          useValue: jasmine.createSpyObj(['refreshData']),
        },
        {
          provide: WorkspaceDirective,
          useValue: {},
        },
        {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: UiManagerService,
          useValue: uiManagerService,
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    milestoneViewService.findPossibleOwners.and.returnValue(of([]));
    milestoneViewService.setRange.and.returnValue(of({}));
    fixture = TestBed.createComponent(MilestoneInformationPanelComponent);
    component = fixture.componentInstance;

    component.componentData = {
      milestone: {
        ownerLogin: 'Old user',
      } as unknown as AdminMilestoneState
    } as unknown as AdminMilestoneViewComponentData;

    adminReferentialDataService.loggedAsAdmin$.next(true);

    const user: User = {
      id: 1,
      login: 'Old user',
    } as unknown as User;

    milestoneViewService.findPossibleOwners.and.returnValue(of([user]));

    fixture.detectChanges();

    component.ownerField = {
      value: 'Old user',
    } as unknown as EditableSearchableSelectFieldComponent;

    component.rangeField = jasmine.createSpyObj(['endAsync']);
    component.rangeField.child = {
      edit: false,
    } as unknown as EditableSelectFieldComponent;

    dialogService.openConfirm.calls.reset();

    milestoneViewService.setRange.and.returnValue(of({}));
    milestoneViewService.setRange.calls.reset();

    milestoneViewService.setOwner.and.returnValue(of({}));
    milestoneViewService.setOwner.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should ignore change owner if value is the same', () => {
    component.handleOwnerChange({
      id: 'Old user',
      label: 'Old user',
    });

    expect(milestoneViewService.setOwner).not.toHaveBeenCalled();
  });

  it('should change owner without confirmation dialog', () => {
    component.handleOwnerChange({
      id: '1',
      label: 'User',
    });

    expect(dialogService.openConfirm).not.toHaveBeenCalled();
    expect(milestoneViewService.setOwner).toHaveBeenCalled();
    expect(component.ownerField.value).toBe('1');
  });

  it('should not change owner if confirmation dialog gets canceled', () => {
    adminReferentialDataService.loggedAsAdmin$.next(false);

    dialogService.openConfirm.and.returnValue({
      dialogClosed$: of(false)
    } as any);

    component.handleOwnerChange({
      id: '1',
      label: 'User',
    });

    expect(milestoneViewService.setOwner).not.toHaveBeenCalled();
    expect(component.ownerField.value).toBe('Old user');
  });

  it('should redirect to workspace if read privilege is lost after owner is changed', () => {
    expect(component).toBeTruthy();

    milestoneViewService.setOwner.and.returnValue(throwError(new HttpErrorResponse({
      status: 403,
    })));

    component.handleOwnerChange({
      id: '1',
      label: 'User',
    });

    expect(uiManagerService.closeContextualContent).toHaveBeenCalledTimes(1);
  });

  it('should set range without confirmation', () => {
    component.setRange(MilestoneRange.GLOBAL.id);
    expect(dialogService.openConfirm).not.toHaveBeenCalled();
    expect(milestoneViewService.setRange).toHaveBeenCalled();
  });

  it('should show dialog confirmation range is set to global and milestone is bound to templates', () => {
    adminReferentialDataService.loggedAsAdmin$.next(false);
    (milestoneViewService as any).isMilestoneBoundToTemplate$.next(true);

    dialogService.openConfirm.and.returnValue({
      dialogClosed$: of(true)
    } as any);

    component.setRange(MilestoneRange.RESTRICTED.id);

    expect(dialogService.openConfirm).toHaveBeenCalled();
    expect(milestoneViewService.setRange).toHaveBeenCalled();
  });

  it('should not set range if confirmation gets canceled', () => {
    adminReferentialDataService.loggedAsAdmin$.next(false);
    (milestoneViewService as any).isMilestoneBoundToTemplate$.next(true);

    dialogService.openConfirm.and.returnValue({
      dialogClosed$: of(false)
    } as any);

    component.setRange(MilestoneRange.RESTRICTED.id);

    expect(dialogService.openConfirm).toHaveBeenCalled();
    expect(milestoneViewService.setRange).not.toHaveBeenCalled();
  });

  it('should not show dialog confirmation when range is set to global and milestone is not bound to templates', () => {
    adminReferentialDataService.loggedAsAdmin$.next(false);
    (milestoneViewService as any).isMilestoneBoundToTemplate$.next(false);

    component.setRange(MilestoneRange.RESTRICTED.id);

    expect(dialogService.openConfirm).not.toHaveBeenCalled();
    expect(milestoneViewService.setRange).toHaveBeenCalled();
  });
});

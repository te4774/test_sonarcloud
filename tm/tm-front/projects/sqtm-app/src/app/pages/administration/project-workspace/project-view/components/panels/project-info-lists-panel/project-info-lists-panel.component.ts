import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild} from '@angular/core';
import {DialogService, EditableSelectFieldComponent, InfoList, Option} from 'sqtm-core';
import {ProjectViewService} from '../../../services/project-view.service';
import {TranslateService} from '@ngx-translate/core';
import {AdminProjectViewComponentData} from '../../../containers/project-view/project-view.component';

@Component({
  selector: 'sqtm-app-project-info-lists-panel',
  templateUrl: './project-info-lists-panel.component.html',
  styleUrls: ['./project-info-lists-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectInfoListsPanelComponent implements OnInit {

  @ViewChild('categorySelectField')
  categorySelectField: EditableSelectFieldComponent;

  @ViewChild('natureSelectField')
  natureSelectField: EditableSelectFieldComponent;

  @ViewChild('typeSelectField')
  typeSelectField: EditableSelectFieldComponent;

  @Input()
  adminProjectViewComponentData: AdminProjectViewComponentData;

  SYSTEM_INFOLIST_CODES = ['DEF_REQ_CAT', 'DEF_TC_NAT', 'DEF_TC_TYP'];

  constructor(
    public adminProjectViewService: ProjectViewService,
    public translateService: TranslateService,
    public dialogService: DialogService,
  ) {
  }


  ngOnInit(): void {

  }

  public getAllInfoLists(infolists: InfoList[], scope: 'category' | 'type' | 'nature'): Option[] {

    let filteredInfoList: InfoList[] = [];
    switch (scope) {
      case 'category':
        filteredInfoList = infolists.filter((infoList) => infoList.code !== 'DEF_TC_NAT' && infoList.code !== 'DEF_TC_TYP');
        break;

      case 'nature':
        filteredInfoList = infolists.filter((infoList) => infoList.code !== 'DEF_REQ_CAT' && infoList.code !== 'DEF_TC_TYP');
        break;

      case 'type':
        filteredInfoList = infolists.filter((infoList) => infoList.code !== 'DEF_TC_NAT' && infoList.code !== 'DEF_REQ_CAT');
        break;

      default :
        throw new Error('Unknown scope');

    }

    filteredInfoList.sort((listA, listB) => listA.label.localeCompare(listB.label));

    const infoListOptions: Option[] = [];
    filteredInfoList.map(infoList => {
        const option = new Option();
        option.value = infoList.id.toString();
        if (this.SYSTEM_INFOLIST_CODES.includes(infoList.code)) {
          option.label = this.translateService.instant('sqtm-core.entity.info-list.default');
          infoListOptions.unshift(option);
        } else {
          option.label = infoList.label;
          infoListOptions.push(option);
      }
    });

    return infoListOptions;
  }

  checkIfProjectIsLinkedToTemplate(event: MouseEvent) {
    if (this.adminProjectViewComponentData.project.linkedTemplateId != null) {
      this.dialogService.openAlert({
        titleKey: 'sqtm-core.administration-workspace.projects.dialog.title.parameter-locked-because-project-is-bound-to-a-template',
        messageKey: 'sqtm-core.administration-workspace.projects.dialog.message.parameter-locked-because-project-is-bound-to-a-template',
        level: 'WARNING',
      });
    }

    event.stopImmediatePropagation();
    event.preventDefault();
  }

  openConfirmBindingAlert(option: Option, scope: 'category' | 'type' | 'nature') {
    const dialogRef = this.dialogService.openConfirm({
      id: 'confirm-info-list-binding-dialog',
      titleKey: 'sqtm-core.administration-workspace.projects.dialog.title.change-info-list-binding',
      messageKey: 'sqtm-core.administration-workspace.projects.dialog.message.change-info-list-binding',
    });

    dialogRef.dialogClosed$.subscribe((confirm) => {
      if (Boolean(confirm)) {
        this.adminProjectViewService.bindInfoListToProject(Number(option.value), scope);
      } else {
        switch (scope) {
          case 'category':
            this.categorySelectField.cancel();
            break;
          case 'nature':
            this.natureSelectField.cancel();
            break;
          case 'type':
            this.typeSelectField.cancel();
            break;
        }
      }
    });
  }

}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {
  AdminReferentialDataService,
  CreationDialogData,
  DialogReference,
  DisplayOption,
  FieldValidationError,
  JsonProjectFromTemplate,
  RestService,
  TemplateConfigurablePlugin
} from 'sqtm-core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {
  AbstractAdministrationCreationDialogDirective
} from '../../../../../components/abstract-administration-creation-dialog';
import {of} from 'rxjs';
import {pluck, take} from 'rxjs/operators';

const Controls = {
  name: 'name',
  description: 'description',
  label: 'label',
  keepTemplateBinding: 'keepTemplateBinding',
  keepPermissions: 'keepPermissions',
  keepCustomFields: 'keepCustomFields',
  keepInfoLists: 'keepInfoLists',
  keepBugtracker: 'keepBugtracker',
  keepAutomation: 'keepAutomation',
  keepMilestones: 'keepMilestones',
  keepAllowTcModificationsFromExecution: 'keepAllowTcModificationsFromExecution',
  keepOptionalExecutionStatuses: 'keepOptionalExecutionStatuses',
  keepPluginsActivation: 'keepPluginsActivation',
  template: 'template',
};

@Component({
  selector: 'sqtm-app-project-creation-dialog',
  templateUrl: './project-creation-dialog.component.html',
  styleUrls: ['./project-creation-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectCreationDialogComponent extends AbstractAdministrationCreationDialogDirective implements OnInit {

  Controls = Controls;
  formGroup: FormGroup;
  serverSideValidationErrors: FieldValidationError[] = [];
  data: CreationDialogData;
  templateOptions: DisplayOption[];
  templateConfigurablePlugins: TemplateConfigurablePlugin[];

  private readonly noTemplateOptionId = '';

  constructor(private fb: FormBuilder,
              private translateService: TranslateService,
              dialogReference: DialogReference,
              restService: RestService,
              cdr: ChangeDetectorRef,
              public readonly adminReferentialDataService: AdminReferentialDataService) {
    super('projects/new', dialogReference, restService, cdr);
    this.data = this.dialogReference.data;
  }

  get textFieldToFocus(): string {
    return 'name';
  }

  get hasTemplateSelected(): boolean {
    return this.formGroup.controls['template'].value !== this.noTemplateOptionId;
  }

  ngOnInit() {
    this.adminReferentialDataService.adminReferentialData$.pipe(
      take(1),
      pluck('templateConfigurablePlugins'),
    ).subscribe(templateConfigurablePlugins => {
      this.templateConfigurablePlugins = templateConfigurablePlugins;
      this.initializeFormGroup();
      this.initializeTemplateSelectField();
    });
  }

  protected getRequestPayload() {
    const boundTemplatePlugins = [];
    const copiedTemplatePlugins = [];

    this.templateConfigurablePlugins.forEach(plugin => {
      const keepBinding = this.getFormControlValue(this.getKeepPluginBindingControlId(plugin));
      const copyConf = this.getFormControlValue(this.getCopyTemplateConfControlId(plugin));

      if (keepBinding) {
        boundTemplatePlugins.push(plugin.id);
        copiedTemplatePlugins.push(plugin.id);
      } else if (copyConf) {
        copiedTemplatePlugins.push(plugin.id);
      }
    });

    const payload: JsonProjectFromTemplate = {
      name: this.getFormControlValue(Controls.name),
      description: this.getFormControlValue(Controls.description),
      label: this.getFormControlValue(Controls.label),
      keepTemplateBinding: this.getFormControlValue(Controls.keepTemplateBinding),
      copyPermissions: this.getFormControlValue(Controls.keepPermissions),
      copyCUF: this.getFormControlValue(Controls.keepCustomFields),
      copyInfolists: this.getFormControlValue(Controls.keepInfoLists),
      copyBugtrackerBinding: this.getFormControlValue(Controls.keepBugtracker),
      copyAutomatedProjects: this.getFormControlValue(Controls.keepAutomation),
      copyMilestone: this.getFormControlValue(Controls.keepMilestones),
      copyAllowTcModifFromExec: this.getFormControlValue(Controls.keepAllowTcModificationsFromExecution),
      copyOptionalExecStatuses: this.getFormControlValue(Controls.keepOptionalExecutionStatuses),
      copyPluginsActivation: this.getFormControlValue(Controls.keepPluginsActivation),
      fromTemplate: this.getFormControlValue(Controls.template) !== this.noTemplateOptionId,
      templateId: this.getFormControlValue(Controls.template),
      boundTemplatePlugins,
      copiedTemplatePlugins,
    };

    return of(payload);
  }

  protected doResetForm(): void {
    this.resetFormControl(Controls.name, '');
    this.resetFormControl(Controls.description, '');
    this.resetFormControl(Controls.label, '');
    this.resetFormControl(Controls.template, this.noTemplateOptionId);
    this.resetFormControl(Controls.keepTemplateBinding, true);
    this.resetFormControl(Controls.keepPermissions, true);
    this.resetFormControl(Controls.keepCustomFields, true);
    this.resetFormControl(Controls.keepInfoLists, true);
    this.resetFormControl(Controls.keepBugtracker, true);
    this.resetFormControl(Controls.keepAutomation, true);
    this.resetFormControl(Controls.keepMilestones, true);
    this.resetFormControl(Controls.keepAllowTcModificationsFromExecution, true);
    this.resetFormControl(Controls.keepOptionalExecutionStatuses, true);
    this.resetFormControl(Controls.keepPluginsActivation, true);

    this.templateConfigurablePlugins.forEach(plugin => {
      this.resetFormControl(this.getKeepPluginBindingControlId(plugin), true);
      this.resetFormControl(this.getCopyTemplateConfControlId(plugin), true);
    });

    this.refreshControlsActivation();
  }

  public handleKeepTemplateBindingChange(keepTemplateBinding: boolean) {
    const impactedFields = [
      Controls.keepCustomFields,
      Controls.keepInfoLists,
      Controls.keepAllowTcModificationsFromExecution,
      Controls.keepOptionalExecutionStatuses,
    ];

    if (keepTemplateBinding) {
      impactedFields.forEach(field => {
        this.formGroup.get(field).setValue(true);
        this.formGroup.get(field).disable();
      });
    } else {
      impactedFields.forEach(field => this.formGroup.get(field).enable());
    }

    this.templateConfigurablePlugins.forEach(plugin => {
      const keepBindingControl = this.getFormControl(this.getKeepPluginBindingControlId(plugin));
      const copyPluginConfControl = this.getFormControl(this.getCopyTemplateConfControlId(plugin));

      if (keepTemplateBinding) {
        keepBindingControl.setValue(true);
        keepBindingControl.enable();
        copyPluginConfControl.setValue(true);
        copyPluginConfControl.disable();
      } else {
        keepBindingControl.setValue(false);
        keepBindingControl.disable();
        copyPluginConfControl.setValue(true);
        copyPluginConfControl.enable();
      }
    });
  }

  private initializeFormGroup() {
    if (this.templateConfigurablePlugins == null) {
      throw new Error('Template configurable plugins should be set before initializing form group!');
    }

    const controlsConfig = {
      [Controls.name]: this.fb.control('', [
        Validators.required,
        Validators.pattern('(.|\\s)*\\S(.|\\s)*'),
        Validators.maxLength(255)
      ]),
      [Controls.description]: this.fb.control(''),
      [Controls.label]: this.fb.control('', [Validators.maxLength(255)]),
      [Controls.template]: this.fb.control(this.noTemplateOptionId),
      [Controls.keepTemplateBinding]: this.fb.control(true),
      [Controls.keepPermissions]: this.fb.control(true),
      [Controls.keepCustomFields]: this.fb.control(true),
      [Controls.keepInfoLists]: this.fb.control(true),
      [Controls.keepBugtracker]: this.fb.control(true),
      [Controls.keepAutomation]: this.fb.control(true),
      [Controls.keepMilestones]: this.fb.control(true),
      [Controls.keepAllowTcModificationsFromExecution]: this.fb.control(true),
      [Controls.keepOptionalExecutionStatuses]: this.fb.control(true),
      [Controls.keepPluginsActivation]: this.fb.control(true),
    };

    this.templateConfigurablePlugins.forEach(plugin => {
      controlsConfig[this.getKeepPluginBindingControlId(plugin)] = this.fb.control(true);
      controlsConfig[this.getCopyTemplateConfControlId(plugin)] = this.fb.control(true);
    });

    this.formGroup = this.fb.group(controlsConfig);

    this.refreshControlsActivation();
  }

  private refreshControlsActivation(): void {
    const keepTemplateBinding = this.getFormControlValue(Controls.keepTemplateBinding);
    this.handleKeepTemplateBindingChange(keepTemplateBinding);

    this.templateConfigurablePlugins.forEach(plugin => {
      const keepPluginConfBinding = this.getFormControlValue(this.getKeepPluginBindingControlId(plugin));
      this.handleKeepPluginConfBindingChange(keepPluginConfBinding, plugin);
    });
  }

  private initializeTemplateSelectField() {
    this.restService.get<{ templates: { id: string, name: string }[] }>(['generic-projects/templates']).subscribe((response) => {
      const options = retrieveTemplatesAsDisplayOptions(response.templates);
      const defaultOption: DisplayOption = this.retrieveDefaultOption();
      this.templateOptions = [defaultOption, ...options];

      if (options.length > 0) {
        this.getFormControl(Controls.template).enable();
      }

      this.cdr.detectChanges();
    });
  }

  private retrieveDefaultOption() {
    return {
      id: this.noTemplateOptionId,
      label: this.translateService.instant('sqtm-core.administration-workspace.projects.dialog.message.new-project.no-template'),
    };
  }

  public getKeepPluginBindingControlId(plugin: TemplateConfigurablePlugin): string {
    return 'keepPluginBinding_' + plugin.id;
  }

  public getCopyTemplateConfControlId(plugin: TemplateConfigurablePlugin): string {
    return 'copyPluginConf_' + plugin.id;
  }

  public handleKeepPluginConfBindingChange(keepPluginConfBinding: boolean, plugin: TemplateConfigurablePlugin) {
    const copyPluginConfControlId = this.getCopyTemplateConfControlId(plugin);
    const copyPluginConfFormControl = this.getFormControl(copyPluginConfControlId);

    if (keepPluginConfBinding) {
      copyPluginConfFormControl.setValue(true);
      copyPluginConfFormControl.disable();
    } else {
      copyPluginConfFormControl.enable();
    }
  }
}

function retrieveTemplatesAsDisplayOptions(templates: { id: string; name: string }[]) {
  return templates.map((ref) => ({id: ref.id, label: ref.name}));
}

import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {
  AdminTestAutomationServerViewComponentData
} from '../../../containers/test-automation-server-view/test-automation-server-view.component';
import {TestAutomationServerViewService} from '../../../services/test-automation-server-view.service';
import {getTestAutomationServerKindI18nKey, TestAutomationServerKind} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {
  EnvironmentSelectionService
} from '../../../../../../../components/automated-execution-environment/services/environment-selection.service';

const SQUASH_AUTOM_DOC_URL_EN = 'https://tm-en.doc.squashtest.com/latest/admin-guide/manage-servers/managing-autom-server.html#configure-squash-autom-test-automation-server';
const SQUASH_AUTOM_DOC_URL_FR = 'https://tm-fr.doc.squashtest.com/latest/admin-guide/gestion-serveurs/gerer-serveur-autom.html#configurer-un-serveur-dexecution-automatisee-squash-autom';

@Component({
  selector: 'sqtm-app-test-automation-server-information-panel',
  templateUrl: './test-auto-server-info-panel.component.html',
  styleUrls: ['./test-auto-server-info-panel.component.less'],
  providers: [
    {
      provide: EnvironmentSelectionService,
      useClass: EnvironmentSelectionService,
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestAutoServerInfoPanelComponent {

  @Input()
  componentData: AdminTestAutomationServerViewComponentData;

  get isManualSlaveSelectionVisible(): boolean {
    return this.componentData.testAutomationServer.kind === TestAutomationServerKind.jenkins;
  }

  get orchestratorConfigurationDocUrl(): string {
    return this.translateService.getBrowserLang() === 'fr' ?
      SQUASH_AUTOM_DOC_URL_FR : SQUASH_AUTOM_DOC_URL_EN;
  }

  constructor(private testAutomationServerViewService: TestAutomationServerViewService,
              private environmentSectionService: EnvironmentSelectionService,
              private translateService: TranslateService) {
  }

  changeManualSlaveSelection(selected: boolean) {
    this.testAutomationServerViewService.changeManualSlaveSelection(selected);
  }

  getServerKind(componentData: AdminTestAutomationServerViewComponentData): string {
    return getTestAutomationServerKindI18nKey(componentData.testAutomationServer.kind as TestAutomationServerKind);
  }

  isSquashAutomServer(componentData: AdminTestAutomationServerViewComponentData): boolean {
    return componentData.testAutomationServer.kind === TestAutomationServerKind.squashAutom;
  }

  deduceObserverUrl(componentData: AdminTestAutomationServerViewComponentData): string {
    const trimmedObserverUrl = (componentData.testAutomationServer.observerUrl ?? '').trim();
    return trimmedObserverUrl || componentData.testAutomationServer.baseUrl;
  }

  getUrlFieldLabelKey(componentData: AdminTestAutomationServerViewComponentData): string {
    return this.isSquashAutomServer(componentData) ?
      'sqtm-core.administration-workspace.servers.test-automation-servers.squashautom.receptionist-url.label'
      : 'sqtm-core.entity.generic.url.label';
  }

  reloadEnvironmentFromObserverUrl(url: string) {
    this.testAutomationServerViewService.reloadEnvironmentFromObserverUrl(url, this.componentData.testAutomationServer.id).subscribe();
  }
}

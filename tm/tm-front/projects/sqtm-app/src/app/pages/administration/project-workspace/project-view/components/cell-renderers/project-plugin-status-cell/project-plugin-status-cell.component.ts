import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-project-plugin-status-cell',
  template: `
    <ng-container *ngIf="row">
      <div class="status-wrapper"
           [ngStyle]="getDisabledIconOpacity(row.disabled)">
        <div class="status" [ngClass]="statusColor"></div>
      </div>
    </ng-container>
  `,
  styleUrls: ['./project-plugin-status-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectPluginStatusCellComponent extends AbstractCellRendererComponent {


  constructor(public grid: GridService,
              public cdr: ChangeDetectorRef) {
    super(grid, cdr);
  }

  get statusColor(): string[] {
    const classes = ['status-indicator'];
    const isEnabled = this.row.data['enabled'] as boolean;
    const hasValidConfiguration = this.row.data['hasValidConfiguration'] as boolean;

    if (! isEnabled) {
      classes.push('disabled');
    } else if (hasValidConfiguration) {
      classes.push('activated');
    } else {
      classes.push('unconfigured');
    }

    return classes;
  }

  getDisabledIconOpacity(disabled: boolean) {
    if (disabled) {
      return {'opacity': '65%'};
    }
  }
}

export function pluginStatusColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(ProjectPluginStatusCellComponent);
}

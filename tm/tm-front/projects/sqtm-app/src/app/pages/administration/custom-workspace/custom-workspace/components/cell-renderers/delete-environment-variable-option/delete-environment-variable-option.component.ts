import {Component, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {AbstractDeleteCellRenderer, DialogService, GridService} from 'sqtm-core';
import {EnvironmentVariableOptionService} from '../../../services/environment-variable-option.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-delete-environment-variable-option',
  template: `
    <sqtm-core-delete-icon (delete)="doDelete()"></sqtm-core-delete-icon>
  `,
  styleUrls: ['./delete-environment-variable-option.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteEnvironmentVariableOptionComponent extends AbstractDeleteCellRenderer {

  constructor(public grid: GridService,
              public cdr: ChangeDetectorRef,
              protected dialogService: DialogService,
              private environmentVariableOptionService: EnvironmentVariableOptionService) {
    super(grid, cdr, dialogService);
  }

  doDelete() {
    this.grid.beginAsyncOperation();
    const optionLabel = this.row.data.label;
    this.environmentVariableOptionService.removeOption(optionLabel).pipe(
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.grid.refreshData());
  }


}

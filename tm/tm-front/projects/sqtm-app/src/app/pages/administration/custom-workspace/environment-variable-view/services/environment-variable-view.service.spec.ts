import {TestBed} from '@angular/core/testing';

import {EnvironmentVariableViewService} from './environment-variable-view.service';
import {mockRestService} from '../../../../../utils/testing-utils/mocks.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {RestService, EvInputType, EnvironmentVariable} from 'sqtm-core';
import {AdminEnvironmentVariableState} from '../states/admin-environment-variable-state';
import {of} from 'rxjs';
import {take} from 'rxjs/operators';

describe('EnvironmentVariableViewService', () => {

  const restService = mockRestService();
  let service: EnvironmentVariableViewService;

  beforeEach((done) => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: RestService,
          useValue: restService
        },
        {
          provide: EnvironmentVariableViewService,
          useClass: EnvironmentVariableViewService
        }
      ]
    });
    restService.getWithoutErrorHandling.and.returnValue(of(getInitialState()));
    service = TestBed.inject(EnvironmentVariableViewService);
    service.load(1);
    done();
  });

  it('should load initial data', (done) => {
    service.componentData$.subscribe(data => {
      expect(data.environmentVariable.id).toEqual(1);
      done();
    });
  });

  it('sould add option', (done) => {
    service = TestBed.inject(EnvironmentVariableViewService);
    const environmentVariable = getInitialState();
    const option = {label: 'option3', code: 'code3'};
    const serverResponse: EnvironmentVariable = {
      id: 1,
      name: 'environment-variable',
      code: 'ev1',
      inputType: EvInputType.PLAIN_TEXT,
      boundToServer: false,
      options: [{evId: 1, label: 'option1', code: 'code1', position: 0},
        {evId: 1, label: 'option2', code: 'code2', position: 1},
        {evId: 1, label: 'option3', code: 'code3', position: 2}],
    };
    restService.getWithoutErrorHandling.and.returnValue(of(environmentVariable));
    service.load(1);
    restService.post.and.returnValue(of(serverResponse));
    service.addOption(option).subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(data => {
        expect(data.environmentVariable.options.length).toEqual(3);
        done();
      });
    });
  });

  it('should delete option', (done) => {
    service = TestBed.inject(EnvironmentVariableViewService);
    const environmentVariable = getInitialState();

    restService.getWithoutErrorHandling.and.returnValue(of(environmentVariable));
    service.load(1);
    service.deleteOptions(['option1']).subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(data => {
        expect(data.environmentVariable.options.length).toEqual(1);
        done();
      });
    });
  });

  it('should change option label', (done) => {
    service = TestBed.inject(EnvironmentVariableViewService);
    const environmentVariable = getInitialState();

    restService.getWithoutErrorHandling.and.returnValue(of(environmentVariable));
    service.load(1);
    service.changeOptionLabel('option1', 'optionUpdate').subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(data => {
        expect(data.environmentVariable.options[0].label).toEqual('optionUpdate');
        done();
      });
    });
  });

  it('should change option code', (done) => {
    service = TestBed.inject(EnvironmentVariableViewService);
    const environmentVariable = getInitialState();

    restService.getWithoutErrorHandling.and.returnValue(of(environmentVariable));
    service.load(1);
    service.changeOptionCode('option1', 'codeUpdate').subscribe(() => {
      service.componentData$.pipe(
        take(1)
      ).subscribe(data => {
        expect(data.environmentVariable.options[0].code).toEqual('codeUpdate');
        done();
      });
    });
  });

});


function getInitialState(): AdminEnvironmentVariableState {
  return {
    id: 1,
    name: 'environment-variable',
    code: 'ev1',
    inputType: EvInputType.PLAIN_TEXT,
    boundToServer: false,
    options: [
      {evId: 1, label: 'option1', code: 'code1', position: 0},
      {evId: 1, label: 'option2', code: 'code2', position: 1}
    ],
    attachmentList: {
      attachments: null,
      id: null,
    }
  };
}

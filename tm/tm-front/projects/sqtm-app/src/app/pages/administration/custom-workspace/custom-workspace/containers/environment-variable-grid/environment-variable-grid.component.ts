import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef} from '@angular/core';
import {AbstractAdministrationNavigation} from '../../../../components/abstract-administration-navigation';
import {
  AdminReferentialDataService,
  AuthenticatedUser,
  DataRow,
  deleteColumn,
  DialogService,
  Extendable,
  FilterOperation,
  grid,
  GridDefinition,
  GridService,
  Identifier,
  indexColumn,
  RestService,
  selectableTextColumn,
  textColumn,
  WorkspaceWithGridComponent
} from 'sqtm-core';
import {ActivatedRoute, Router} from '@angular/router';
import {concatMap, Observable, Subject} from 'rxjs';
import {
  inputTypeColumn
} from '../../components/cell-renderers/input-type-cell-renderer/input-type-cell-renderer.component';
import {
  DeleteEnvironmentVariableComponent
} from '../../components/cell-renderers/delete-environment-variable/delete-environment-variable.component';
import {
  EnvironmentVariableCreationDialogComponent
} from '../../components/dialogs/environment-variable-creation-dialog/environment-variable-creation-dialog.component';
import {filter, finalize, map, take, takeUntil, tap} from 'rxjs/operators';

export function adminEnvironmentVariableTableDefinition(): GridDefinition {
  return grid('environmentVariables')
    .withColumns([
      indexColumn(),
      selectableTextColumn('name')
        .withI18nKey('sqtm-core.entity.generic.name.label')
        .changeWidthCalculationStrategy(new Extendable(150, 0.1)),
      textColumn('code')
        .withI18nKey('sqtm-core.generic.label.code')
        .changeWidthCalculationStrategy(new Extendable(100, 0.1)),
      inputTypeColumn('inputType')
        .withI18nKey('sqtm-core.entity.custom-field.input-type.label')
        .changeWidthCalculationStrategy(new Extendable(80, 0.1)),
      deleteColumn(DeleteEnvironmentVariableComponent)
    ]).server().withServerUrl(['environment-variables'])
    .disableRightToolBar()
    .withRowHeight(35)
    .enableMultipleColumnsFiltering(['name'])
    .build();
}

@Component({
  selector: 'sqtm-app-environment-variable-grid',
  templateUrl: './environment-variable-grid.component.html',
  styleUrls: ['./environment-variable-grid.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EnvironmentVariableGridComponent extends AbstractAdministrationNavigation implements AfterViewInit, OnDestroy {

  authenticatedUser$: Observable<AuthenticatedUser>;

  unsub$ = new Subject<void>();

  protected readonly entityIdPositionInUrl = 3;

  constructor(public gridService: GridService,
              private restService: RestService,
              private dialogService: DialogService,
              private adminReferentialDataService: AdminReferentialDataService,
              private viewContainerRef: ViewContainerRef,
              private workspaceWithGrid: WorkspaceWithGridComponent,
              protected route: ActivatedRoute,
              protected router: Router) {
    super(route, router);
    this.workspaceWithGrid.entityIdPositionInUrl = this.entityIdPositionInUrl;
    this.authenticatedUser$ = adminReferentialDataService.authenticatedUser$;
  }

  ngAfterViewInit() {
    this.addFilters();
    this.gridService.refreshData();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private addFilters() {
    this.gridService.addFilters([{
      id: 'name',
      active: false,
      initialValue: {kind: 'single-string-value', value: ''},
      tiedToPerimeter: false,
      operation: FilterOperation.LIKE
    }]);
  }

  openCreateDialog() {
    const dialogReference = this.dialogService.openDialog({
      component: EnvironmentVariableCreationDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        titleKey: 'sqtm-core.administration-workspace.environment-variable.dialog.title.new-environment-variable'
      },
      id: 'create-environment-variable-dialog',
      width: 600
    });

    dialogReference.dialogResultChanged$.pipe(
      takeUntil(dialogReference.dialogClosed$),
      filter(result => result != null),
      concatMap((result: any) => this.gridService.refreshDataAsync().pipe(
        map(() => result.id))),
      tap((id: string) => super.navigateToNewEntity(id))
    ).subscribe();
  }

  filterEnvironmentVariables(event: any) {
    this.gridService.applyMultiColumnsFilter(event);
  }

  deleteEnvironmentVariables() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((dataRows: DataRow[]) => dataRows.length > 0),
      concatMap((dataRows: DataRow[]) => this.showConfirmDeleteEnvironmentVariableDialog(dataRows)),
      filter(({confirmDelete}) => confirmDelete),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({ids}) => this.deleteEnvironmentVariablesServerSide(ids)),
      finalize(() => this.gridService.completeAsyncOperation())
    ).subscribe(() => this.gridService.refreshData());
  }

  private showConfirmDeleteEnvironmentVariableDialog(dataRows: DataRow[]) {
    const ids: Identifier[] = dataRows.map(dataRow => dataRow.id);
    const bindingCounts: number[] = dataRows.map(dataRow => dataRow.data.bindingCount);
    const isBindingExist: boolean = bindingCounts.filter(count => count > 0).length > 0;

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.administration-workspace.environment-variable.dialog.title.delete-many',
      messageKey: isBindingExist ?
        'sqtm-core.administration-workspace.environment-variable.dialog.message.delete-many-bound-environment-variable' :
        'sqtm-core.administration-workspace.environment-variable.dialog.message.delete-many',
      level: 'DANGER',
    });
    return dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      map(confirmDelete => ({confirmDelete, ids}))
    );
  }

  private deleteEnvironmentVariablesServerSide(ids: Identifier[]): Observable<void> {
    const pathVariable = ids.join(',');
    return this.restService.delete([`environment-variables`, pathVariable]);
  }
}

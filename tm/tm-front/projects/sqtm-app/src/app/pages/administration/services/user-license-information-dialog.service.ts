import {Injectable} from '@angular/core';
import {map, take} from 'rxjs/operators';
import {
  AdminReferentialDataService,
  DialogService,
  LicenseInformationMessageProvider,
  LicenseMessagePlacement
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class UserLicenseInformationDialogService {

  private _licenseAllowsUserCreation: boolean;
  private _dialogMessage: string;

  constructor(private readonly dialogService: DialogService,
              private readonly adminReferentialDataService: AdminReferentialDataService,
              private readonly translateService: TranslateService) {
    this.initializeLicenseLock();
  }

  public isUserCreationAllowed(): boolean {
    return this._licenseAllowsUserCreation;
  }

  public openLicenseDialog(): void {
    this.dialogService.openAlert({
      messageKey: this._dialogMessage,
      titleKey: 'sqtm-core.license.title',
      id: 'user-license-information-detail',
      level: null,
    }, 650);
  }

  private initializeLicenseLock(): void {
    this.adminReferentialDataService.licenseInformation$.pipe(
      take(1),
      map(licenseInfo => new LicenseInformationMessageProvider(licenseInfo, this.translateService))
    ).subscribe(messageHelper => {
      this._licenseAllowsUserCreation = messageHelper.licenseInformation.allowCreateUsers;
      this._dialogMessage = messageHelper.getLongMessage(LicenseMessagePlacement.USER_CREATION, true);
    });
  }
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ProjectInfoListsPanelComponent} from './project-info-lists-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DialogService, InfoList} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {ProjectViewService} from '../../../services/project-view.service';
import {RouterTestingModule} from '@angular/router/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../../../utils/testing-utils/app-testing-utils.module';
import {mockAutoConfirmDialogService} from '../../../../../../../utils/testing-utils/mocks.service';
import {mockMouseEvent} from '../../../../../../../utils/testing-utils/test-component-generator';
import {AdminProjectViewComponentData} from '../../../containers/project-view/project-view.component';

describe('ProjectInfoListsPanelComponent', () => {
  let component: ProjectInfoListsPanelComponent;
  let fixture: ComponentFixture<ProjectInfoListsPanelComponent>;

  const dialogService = mockAutoConfirmDialogService();
  const adminProjectViewService = jasmine.createSpyObj(['bindInfoListToProject']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: ProjectViewService,
          useValue: {}
        },
        {
          provide: DialogService,
          useValue: dialogService,
        },
        {
          provide: ProjectViewService,
          useValue: adminProjectViewService,
        }
      ],
      declarations: [ProjectInfoListsPanelComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectInfoListsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    dialogService.openAlert.calls.reset();
    dialogService.openConfirm.calls.reset();
    adminProjectViewService.bindInfoListToProject.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return filtered info list options based on scope', () => {
    const all: InfoList[] = [
      {id: 1, code: 'DEF_TC_NAT', items: [], label: '', description: '', uri: ''},
      {id: 2, code: 'DEF_TC_TYP', items: [], label: '', description: '', uri: ''},
      {id: 3, code: 'DEF_REQ_CAT', items: [], label: '', description: '', uri: ''},
      {id: 4, code: 'custom', items: [], label: '', description: '', uri: ''},
    ];

    const catLists = component.getAllInfoLists(all, 'category');
    expect(catLists.map(list => list.value)).toEqual(['3', '4']);

    const typeLists = component.getAllInfoLists(all, 'type');
    expect(typeLists.map(list => list.value)).toEqual(['2', '4']);

    const natLists = component.getAllInfoLists(all, 'nature');
    expect(natLists.map(list => list.value)).toEqual(['1', '4']);
  });

  it('should show alert if project is linked to a template', () => {
    component.adminProjectViewComponentData = { project: { linkedTemplateId: 123 }} as AdminProjectViewComponentData;
    component.checkIfProjectIsLinkedToTemplate(mockMouseEvent());

    expect(dialogService.openAlert).toHaveBeenCalled();
  });

  it('should show confirmation and bind to info list', () => {
    component.openConfirmBindingAlert({value: '123', label: '123'}, 'category');

    expect(dialogService.openConfirm).toHaveBeenCalled();
    expect(adminProjectViewService.bindInfoListToProject).toHaveBeenCalledWith(123, 'category');
  });
});

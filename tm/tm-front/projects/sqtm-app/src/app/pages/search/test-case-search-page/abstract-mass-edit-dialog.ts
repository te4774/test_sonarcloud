import {TranslateService} from '@ngx-translate/core';
import {DisplayOption, InfoListItem, LevelEnum, ReferentialDataService, RestService} from 'sqtm-core';

export abstract class AbstractMassEditDialog {
  protected constructor(protected translateService: TranslateService,
                        protected referentialDataService: ReferentialDataService,
                        protected restService: RestService) {
  }

  protected convertInfoListItemsToDisplayOptions(infoListItems: InfoListItem[]) {
    const displayOptions: DisplayOption[] = infoListItems.map(item => {
      return {
        id: item.id,
        label: item.system ? this.translateService.instant(`sqtm-core.entity.${item.label}`) : item.label
      };
    });
    return displayOptions;
  }

  protected convertLevelEnumToDisplayOptions(levelEnum: LevelEnum<any>): DisplayOption[] {
    const levelEnumItems = Object.values(levelEnum);
    return levelEnumItems.map((item => {
      return {id: item.id, label: this.translateService.instant(item.i18nKey)};
    }));
  }
}

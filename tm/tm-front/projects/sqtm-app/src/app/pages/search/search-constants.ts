export const SCOPE_QUERY_PARAM_KEY = 'scope';
export const FILTER_QUERY_PARAM_KEY = 'filters';
export const MILESTONE_FILTER = 'milestoneLabel';
export const MILESTONE_STATUS = 'milestoneStatus';
export const MILESTONE_END_DATE = 'milestoneEndDate';
export const REQUIREMENT_KIND = 'requirementKind';
export const BACK_URL_PARAM = 'backUrl';

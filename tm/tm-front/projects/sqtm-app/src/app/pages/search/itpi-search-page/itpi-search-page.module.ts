import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ItpiSearchPageComponent} from './containers/itpi-search-page/itpi-search-page.component';
import {SearchViewModule} from '../search-view/search-view.module';
import {RouterModule, Routes} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzPopoverModule} from 'ng-zorro-antd/popover';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {CellRendererCommonModule, DialogModule, GridModule, SvgModule, WorkspaceCommonModule} from 'sqtm-core';
import {ReactiveFormsModule} from '@angular/forms';
import {OverlayModule} from '@angular/cdk/overlay';
import {
  StartExecutionSearchComponent
} from './components/cell-renderers/start-execution-search/start-execution-search.component';
import {
  ItpiMultiEditSearchDialogComponent
} from './components/dialog/itpi-multi-edit-dialog/itpi-multi-edit-search-dialog.component';
import {
  ItpiAddTestPlanDialogComponent
} from './components/dialog/itpi-add-test-plan-dialog/itpi-add-test-plan-dialog.component';

const routes: Routes = [
  {
    path: '',
    component: ItpiSearchPageComponent,
    pathMatch: 'full'
  }
];


@NgModule({
  declarations: [
    ItpiSearchPageComponent,
    StartExecutionSearchComponent,
    ItpiMultiEditSearchDialogComponent,
    ItpiAddTestPlanDialogComponent
  ],
  imports: [
    CommonModule,
    SearchViewModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
    NzToolTipModule,
    WorkspaceCommonModule,
    DialogModule,
    NzButtonModule,
    ReactiveFormsModule,
    NzCheckboxModule,
    OverlayModule,
    NzIconModule,
    NzPopoverModule,
    SvgModule,
    CellRendererCommonModule,
    GridModule
  ]
})
export class ItpiSearchPageModule {
}

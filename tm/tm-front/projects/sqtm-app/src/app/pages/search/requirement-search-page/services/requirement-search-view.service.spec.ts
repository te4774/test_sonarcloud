import {TestBed} from '@angular/core/testing';

import {RequirementSearchViewService} from './requirement-search-view.service';
import {RestService} from 'sqtm-core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';

describe('RequirementSearchViewService', () => {
  let service: RequirementSearchViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [
        {
          provide: RequirementSearchViewService,
          useClass: RequirementSearchViewService,
          deps: [RestService]
        }
      ]
    });
    service = TestBed.inject(RequirementSearchViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

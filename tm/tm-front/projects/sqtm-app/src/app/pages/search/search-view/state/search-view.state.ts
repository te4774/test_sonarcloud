import {createFeatureSelector} from '@ngrx/store';

export interface SearchViewState {
  uiState: {
    showFilterManager: boolean
  };
}

export function provideInitialSearchViewState(): Readonly<SearchViewState> {
  return {
    uiState: {
      showFilterManager: true
    }
  };
}

export const selectUiState = createFeatureSelector<{openFilterManager: boolean}>('uiState');

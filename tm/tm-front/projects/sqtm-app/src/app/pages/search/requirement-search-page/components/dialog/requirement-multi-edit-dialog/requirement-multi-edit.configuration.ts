export class RequirementMultiEditConfiguration {
  id: string;
  titleKey: string;
  editableRowsIds: number[];
  projectIds: number[];
  writingRightOnLine: boolean;
  canOnlyEditStatus: boolean;
}

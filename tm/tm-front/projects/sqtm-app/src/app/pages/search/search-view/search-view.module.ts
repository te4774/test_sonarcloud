import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SearchViewComponent} from './containers/search-view/search-view.component';
import {GridFilterManagerModule, GridModule, NavBarModule, UiManagerModule, WorkspaceCommonModule} from 'sqtm-core';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {TranslateModule} from '@ngx-translate/core';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';


@NgModule({
  declarations: [SearchViewComponent],
    imports: [
        CommonModule,
        UiManagerModule,
        NavBarModule,
        NzIconModule,
        TranslateModule.forChild(),
        WorkspaceCommonModule,
        GridModule,
        NzToolTipModule,
        GridFilterManagerModule
    ],
  exports: [SearchViewComponent]
})
export class SearchViewModule {
}

import {AfterViewInit, ChangeDetectionStrategy, Component, OnInit, ViewContainerRef} from '@angular/core';
import {
  DataRow,
  DialogService,
  GridService,
  gridServiceFactory,
  Identifier,
  Milestone,
  MilestoneView,
  ReferentialDataService,
  RestService,
  UserHistorySearchProvider
} from 'sqtm-core';
import {concatMap, filter, map, take} from 'rxjs/operators';
import {
  MilestoneDialogConfiguration
} from '../../../../../components/milestone/milestone-dialog/milestone.dialog.configuration';
import {
  MilestoneDialogComponent
} from '../../../../../components/milestone/milestone-dialog/milestone-dialog.component';
import {combineLatest, Observable} from 'rxjs';
// tslint:disable-next-line:max-line-length
import {
  SearchTestCaseExportDialogConfiguration
} from '../../components/search-test-case-export-dialog/search-test-case-export-dialog-configuration';
// tslint:disable-next-line:max-line-length
import {
  SearchTestCaseExportDialogComponent
} from '../../components/search-test-case-export-dialog/search-test-case-export-dialog.component';
// tslint:disable-next-line:max-line-length
import {
  TestCaseMultiEditDialogComponent
} from '../../components/test-case-multi-edit-dialog/test-case-multi-edit-dialog.component';
import {
  searchTestCaseGridConfigFactory,
  TEST_CASE_RESEARCH_GRID,
  TEST_CASE_RESEARCH_GRID_CONFIG
} from '../test-case-search-grid.builders';
import {TestCaseSearchViewService} from '../../services/test-case-search-view.service';
import {AbstractTestCaseSearchComponent} from '../abstract-test-case-search.component';
import {Dictionary} from '@ngrx/entity';
import {
  TestCaseMultiEditConfiguration
} from '../../components/test-case-multi-edit-dialog/test-case-multi-edit.configuration';

@Component({
  selector: 'sqtm-app-test-case-simple-search-page',
  templateUrl: './test-case-simple-search-page.component.html',
  styleUrls: ['./test-case-simple-search-page.component.less'],
  providers: [
    {
      provide: TEST_CASE_RESEARCH_GRID_CONFIG,
      useFactory: searchTestCaseGridConfigFactory
    },
    {
      provide: TEST_CASE_RESEARCH_GRID,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        TEST_CASE_RESEARCH_GRID_CONFIG,
        ReferentialDataService
      ]
    }, {
      provide: GridService,
      useExisting: TEST_CASE_RESEARCH_GRID
    },
    {
      provide: TestCaseSearchViewService,
      useClass: TestCaseSearchViewService,
      deps: [RestService]
    },
    {
      provide: UserHistorySearchProvider,
      useExisting: TestCaseSearchViewService,
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseSimpleSearchPageComponent extends AbstractTestCaseSearchComponent implements OnInit, AfterViewInit {

  constructor(referentialDataService: ReferentialDataService,
              testCaseSearchViewService: TestCaseSearchViewService,
              gridService: GridService,
              private dialogService: DialogService,
              private restService: RestService,
              private vcr: ViewContainerRef) {
    super(referentialDataService, testCaseSearchViewService, gridService);
  }

  massEdit() {
    this.gridService.selectedRows$.pipe(
      take(1)
    ).subscribe(dataRows => {
      const editableDataRows = this.editableTestCaseIds(dataRows);
      if (editableDataRows.length > 0) {
        const projectIds = new Set(editableDataRows.map(dataRow => dataRow.projectId));
        const writingRight = editableDataRows.length === dataRows.length;
        const testCaseIds = editableDataRows.map(dataRow => dataRow.data['id']);
        this.showMassEditDialog(testCaseIds, projectIds, writingRight);
      } else {
        this.showCantEditDialog('sqtm-core.search.generic.modify.no-line-with-writing-rights');
      }
    });
  }

  massEditMilestones() {
    this.gridService.selectedRows$.pipe(
      take(1),
      map((rows: DataRow[]) => this.editableDataRows(rows))
    ).subscribe(rows => {
      if (rows.length > 0) {
        this.displayMassBindingMilestonesDialog(rows);
      } else {
        this.showCantEditDialog('sqtm-core.search.generic.modify.no-line-with-writing-rights');
      }
    });
  }

  displayMassBindingMilestonesDialog(rows: DataRow[]) {
    const testCaseIds = rows.map(row => row.data['id']);
    return this.restService.get<MilestoneMassEdit>(['search/milestones/test-case', testCaseIds.toString()]).pipe(
      map((massEditMilestone: MilestoneMassEdit) => {
        massEditMilestone.testCaseIds = testCaseIds;
        return massEditMilestone;
      }),
      concatMap((massEditMilestone: MilestoneMassEdit) => {
        const milestoneIds = massEditMilestone.milestoneIds;
        return this.referentialDataService.findMilestones(milestoneIds).pipe(
          take(1),
          map((milestones: Milestone[]) => {
            const result: MilestoneMassEditView = {
              ...massEditMilestone,
              milestoneViews: this.convertToMilestoneViews(milestones, massEditMilestone)
            };
            return result;
          }));
      }),
    ).subscribe(milestoneView => {
      if (milestoneView.milestoneViews.length > 0) {
        this.showMilestoneDialog(milestoneView);
      } else if (milestoneView.milestoneViews.length === 0) {
        this.showCantEditDialog('sqtm-core.search.generic.modify.milestone.wrong-perimeter.test-case');
      } else {
        this.showCantEditDialog('sqtm-core.search.generic.modify.no-line-with-writing-rights');
      }
    });
  }

  showCantEditDialog(messageKey: string) {
    this.dialogService.openAlert({
      titleKey: 'sqtm-core.generic.label.information.singular',
      messageKey,
      level: 'INFO',
    });
  }

  showMilestoneDialog(massEditMilestone: MilestoneMassEditView) {
    const configuration: MilestoneDialogConfiguration = {
      id: 'search-milestone-dialog',
      titleKey: 'sqtm-core.search.generic.modify.milestone.title',
      checkedIds: massEditMilestone.checkedIds,
      milestoneViews: massEditMilestone.milestoneViews,
      samePerimeter: massEditMilestone.samePerimeter
    };

    const milestoneRef = this.dialogService.openDialog({
      id: 'research-milestone',
      component: MilestoneDialogComponent,
      data: {...configuration},
      height: 400,
      width: 800
    });

    milestoneRef.dialogClosed$.pipe(
      take(1),
      filter(result => result != null),
      concatMap(result => {
        const data = {};
        data['milestoneIds'] = result;
        data['bindableObjectIds'] = massEditMilestone.testCaseIds;
        return this.restService.post(['search/milestones/test-case'], data);
      })
    ).subscribe(() => {
      this.gridService.refreshData();
    });

  }

  showExportDialog() {
    combineLatest([this.gridService.gridNodes$, this.gridService.selectedRows$])
      .pipe(
        take(1),
        map(([rows, selectedRows]) => this.getExportedRowIds(selectedRows, rows))
      ).subscribe(ids => {
      this.dialogService.openDialog<SearchTestCaseExportDialogConfiguration, any>({
        id: 'search-test-case-export',
        component: SearchTestCaseExportDialogComponent,
        viewContainerReference: this.vcr,
        data: {id: 'search-test-case-export', nodes: ids},
        width: 600
      });
    });
  }

  private getExportedRowIds(selectedRows, rows) {
    if (this.hasSelection(selectedRows)) {
      return this.getSelectedRowIds(selectedRows);
    } else {
      return this.getAllRowIds(rows);
    }
  }

  private getAllRowIds(rows) {
    return rows.map(node => Number.parseInt(node.id.toString(), 10));
  }

  private getSelectedRowIds(selectedRows) {
    return selectedRows.map(dataRow => Number.parseInt(dataRow.id.toString(), 10));
  }

  private hasSelection(selectedRows) {
    return selectedRows.length > 0;
  }

  showMassEditDialog(testCaseIds: number[], projectIds: Set<number>, writingRight: boolean) {
    const dialogReference = this.dialogService.openDialog<TestCaseMultiEditConfiguration, boolean>({
      id: 'mass-edit',
      component: TestCaseMultiEditDialogComponent,
      data: {
        id: 'mass-edit',
        titleKey: 'sqtm-core.search.generic.modify.selection',
        projectIds: projectIds,
        testCaseIds: testCaseIds,
        writingRightOnLine: writingRight
      },
      width: TestCaseMultiEditDialogComponent.DIALOG_WIDTH,
    });
    dialogReference.dialogClosed$.pipe(take(1)).subscribe(result => {
      if (result) {
        this.gridService.refreshData();
      }
    });
  }

  editableTestCaseIds(dataRows: DataRow[]): DataRow[] {
    return dataRows
      .filter(row => row.simplePermissions.canWrite)
      .filter(row => {
        const tcLockedMilestones = row.data['tcMilestoneLocked'];
        return tcLockedMilestones === 0;
      });
  }

  editableDataRows(dataRows: DataRow[]): DataRow[] {
    return dataRows.filter(row => row.simplePermissions.canWrite);
  }

  convertToMilestoneViews(milestones: Milestone[], massEdit: MilestoneMassEdit): MilestoneView[] {
    const milestonesViews: MilestoneView[] = [];

    milestones.forEach(milestone => {
      const mv: MilestoneView = {...milestone, boundToObject: massEdit.checkedIds.includes(milestone.id)};
      milestonesViews.push(mv);
    });
    return milestonesViews;
  }

  canExport(): Observable<boolean> {
    return combineLatest([this.gridService.dataRows$, this.gridService.selectedRows$]).pipe(
      take(1),
      map(([rows, selectedRows]) => {
        return this.checkExportPermissions(rows, selectedRows);
      })
    );
  }

  private checkExportPermissions(rows: Dictionary<DataRow>, selectedRows: DataRow[]) {
    if (this.hasSelection(selectedRows)) {
      return selectedRows.some(r => r.simplePermissions.canExport);
    } else {
      const dataRows = Object.values(rows);
      return dataRows.some(r => r.simplePermissions.canExport);
    }
  }

}

export interface MilestoneMassEdit {
  milestoneIds: number[];
  checkedIds: number[];
  samePerimeter: boolean;
  testCaseIds: Identifier[];
}

export interface MilestoneMassEditView {
  checkedIds: number[];
  samePerimeter: boolean;
  milestoneViews: MilestoneView[];
  testCaseIds: Identifier[];
}

import {TestBed} from '@angular/core/testing';

import {ItpiSearchViewService} from './itpi-search-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {RestService} from 'sqtm-core';

describe('ItpiSearchViewService', () => {
  let service: ItpiSearchViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [{
        provide: ItpiSearchViewService,
        useClass: ItpiSearchViewService,
        deps: [RestService]
      }]
    });
    service = TestBed.inject(ItpiSearchViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {searchRequirementGridConfigFactory} from '../requirement-search-grid.builders';
import {
  ChangeCoverageOperationReport,
  createTestCaseCoverageMessageDialogConfiguration,
  DataRow,
  DialogService,
  GridNode,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
  shouldShowCoverageMessageDialog,
  TestCaseService,
  TestStepService,
  UserHistorySearchProvider
} from 'sqtm-core';
import {RequirementSearchViewService} from '../../services/requirement-search-view.service';
import {
  AbstractRequirementSearchComponent,
  REQUIREMENT_RESEARCH_GRID,
  REQUIREMENT_RESEARCH_GRID_CONFIG
} from '../abstract-requirement-search.component';
import {concatMap, map, take} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {BACK_URL_PARAM} from '../../../search-constants';

@Component({
  selector: 'sqtm-app-requirement-for-coverage-search-page',
  templateUrl: './requirement-for-coverage-search-page.component.html',
  styleUrls: ['./requirement-for-coverage-search-page.component.less'],
  providers: [
    {
      provide: REQUIREMENT_RESEARCH_GRID_CONFIG,
      useFactory: searchRequirementGridConfigFactory
    },
    {
      provide: REQUIREMENT_RESEARCH_GRID,
      useFactory: gridServiceFactory,
      deps: [
        RestService,
        REQUIREMENT_RESEARCH_GRID_CONFIG,
        ReferentialDataService
      ]
    }, {
      provide: GridService,
      useExisting: REQUIREMENT_RESEARCH_GRID
    },
    {
      provide: RequirementSearchViewService,
      useClass: RequirementSearchViewService,
      deps: [RestService]
    },
    {
      provide: UserHistorySearchProvider,
      useExisting: RequirementSearchViewService,
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementForCoverageSearchPageComponent
  extends AbstractRequirementSearchComponent implements OnInit, OnDestroy, AfterViewInit {

  constructor(referentialDataService: ReferentialDataService,
              requirementSearchViewService: RequirementSearchViewService,
              gridService: GridService,
              private testCaseService: TestCaseService,
              private testStepService: TestStepService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private dialogService: DialogService) {
    super(referentialDataService, requirementSearchViewService, gridService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  private testCaseId = this.activatedRoute.snapshot.paramMap.get('testCaseId');
  private stepId = this.activatedRoute.snapshot.paramMap.get('stepId');

  linkSelection() {
    this.linkFromObservable(this.gridService.selectedRows$);
  }

  linkAll() {
    this.linkFromObservable(this.gridService.gridNodes$.pipe(
      map(nodes => this.getDataRows(nodes))));
  }

  private getDataRows(nodes: GridNode[]) {
    return nodes.map(node => node.dataRow);
  }

  linkFromObservable(obs: Observable<DataRow[]>) {
    obs.pipe(
      take(1),
      map(rows => this.extractRequirementIds(rows)),
      concatMap(requirementIds => {
        if(this.stepId){
          return this.persistNewStepCoverages(requirementIds);
      }else {
          return this.persistNewCoverages(requirementIds);
        }
      })
    ).subscribe(operationReport => {
      if (shouldShowCoverageMessageDialog(operationReport)) {
        this.showCoverageReport(operationReport);
      } else {
        this.navigateBack();
      }
    });
  }

  navigateBack() {
    const backUrl =  this.activatedRoute.snapshot.queryParamMap.get(BACK_URL_PARAM) || 'test-case-workspace';
    this.router.navigate([backUrl]);
  }

  private showCoverageReport(operationReport: ChangeCoverageOperationReport) {
    this.dialogService.openDialog(createTestCaseCoverageMessageDialogConfiguration(operationReport));
  }

  private persistNewCoverages(requirementIds: number[]): Observable<ChangeCoverageOperationReport> {
    return this.testCaseService.persistCoverages(Number.parseInt(this.testCaseId, 10), requirementIds);
  }

  private persistNewStepCoverages(requirementIds: number[]): Observable<ChangeCoverageOperationReport> {
    return this.testStepService.persistStepCoverage(requirementIds, Number.parseInt(this.stepId, 10) ,Number.parseInt(this.testCaseId, 10));
  }

  private extractRequirementIds(rows: DataRow[]) {
    return rows.map(row => row.data.requirementId);
  }


}

import {ChangeDetectionStrategy, Component, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {
  AutomatedTestTechnology,
  buildScmRepositoryUrl,
  buildTechnologyLabel,
  DialogReference,
  DisplayOption,
  InfoList,
  InfoListItem,
  OptionalSelectFieldComponent,
  ProjectData,
  ProjectDataMap,
  ReferentialDataService,
  RestService,
  TestCaseAutomatable,
  TestCaseStatus,
  TestCaseWeight
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {TestCaseMultiEditConfiguration} from './test-case-multi-edit.configuration';
import {AbstractMassEditDialog} from '../../abstract-mass-edit-dialog';
import {take} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-test-case-multi-edit-dialog',
  templateUrl: './test-case-multi-edit-dialog.component.html',
  styleUrls: ['./test-case-multi-edit-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseMultiEditDialogComponent extends AbstractMassEditDialog implements OnInit {

  public static DIALOG_WIDTH = 850;
  public optionalLabelWidthRatio = 0.33;

  nature: InfoList;
  type: InfoList;
  projectDataMap$: Observable<ProjectDataMap>;
  automatedTestTechnologies$: Observable<AutomatedTestTechnology[]>;
  autoDisable = true;
  importanceAutoCheck = false;
  automatedTestTechnologyOptions: DisplayOption[];
  scmRepoUrlOptions: DisplayOption[];

  data: TestCaseMultiEditConfiguration;

  @ViewChildren(OptionalSelectFieldComponent)
  selectFields: QueryList<OptionalSelectFieldComponent>;

  @ViewChild('importance')
  importance: OptionalSelectFieldComponent;

  constructor(translateService: TranslateService,
              referentialDataService: ReferentialDataService,
              public dialogReference: DialogReference<TestCaseMultiEditConfiguration>,
              restService: RestService) {
    super(translateService, referentialDataService, restService);
  }

  ngOnInit() {
    this.data = this.dialogReference.data;
    this.projectDataMap$ = this.referentialDataService.projectDatas$;
    this.initializeAutomatedTestTechnologyOptions();
    this.initializeScmRepositoryOptions();
  }

  changeImportanceAuto($event) {
    this.importanceAutoCheck = $event;
  }

  getStatusValues() {
    return this.convertLevelEnumToDisplayOptions(TestCaseStatus);
  }

  getWeightValues() {
    return this.convertLevelEnumToDisplayOptions(TestCaseWeight);
  }

  getAutomatableValues() {
    return this.convertLevelEnumToDisplayOptions(TestCaseAutomatable);
  }

  getNatureValues(projectDataMap: ProjectDataMap): DisplayOption[] {
    const nature = this.getNature(projectDataMap);
    return this.convertInfoListItemsToDisplayOptions(nature.items);
  }

  getTypeValues(projectDataMap: ProjectDataMap): DisplayOption[] {
    const type = this.getType(projectDataMap);
    return this.convertInfoListItemsToDisplayOptions(type.items);
  }

  getDefaultType(projectDataMap: ProjectDataMap) {
    const type = this.getType(projectDataMap);
    return this.getDefaultItem(type.items);
  }

  getDefaultNature(projectDataMap: ProjectDataMap) {
    const nature = this.getNature(projectDataMap);
    return this.getDefaultItem(nature.items);
  }

  importanceAutoDisable($event) {
    return this.autoDisable = !$event;
  }

  private getType(projectDataMap: ProjectDataMap) {
    const projectsData = this.getProjectsData(projectDataMap, this.data.projectIds);
    return projectsData[0].testCaseType;
  }

  private getNature(projectDataMap: ProjectDataMap) {
    const projectsData = this.getProjectsData(projectDataMap, this.data.projectIds);
    return projectsData[0].testCaseNature;
  }

  private getDefaultItem(infoListItem: InfoListItem[]) {
    const systemItems = infoListItem.filter(item => item.system);

    if (systemItems.length > 0) {
      return infoListItem[0].id;
    } else {
      return infoListItem.find(item => item.default).id;
    }
  }

  cantEditAutomatable(projectDataMap: ProjectDataMap) {
    const projectsData = this.getProjectsData(projectDataMap, this.data.projectIds);
    return !projectsData.some(project => project.allowAutomationWorkflow);
  }

  cantEditNature(projectDataMap: ProjectDataMap): boolean {
    const projectsData = this.getProjectsData(projectDataMap, this.data.projectIds);
    const natureList = new Set(projectsData.map(projectData => projectData.testCaseNature.id));
    return natureList.size !== 1;
  }

  cantEditType(projectDataMap: ProjectDataMap): boolean {
    const projectsData = this.getProjectsData(projectDataMap, this.data.projectIds);
    const typeList = new Set(projectsData.map(projectData => projectData.testCaseType.id));
    return typeList.size !== 1;
  }

  cantEditAllFields(projectDataMap: ProjectDataMap): boolean {
    return this.cantEditNature(projectDataMap) && this.cantEditType(projectDataMap);
  }

  getProjectsData(projectDataMap: ProjectDataMap, ids: Set<number>): ProjectData[] {
    return [...ids].map(id => projectDataMap[id]);
  }

  confirmation() {
    const checkedFields = this.selectFields.filter(field => field.check);

    if (checkedFields.length === 0) {
      this.dialogReference.close();
      return;
    }

    const testCaseMassUpdatePatch: TestCaseMassUpdatePatch = {
      testCaseIds: this.data.testCaseIds,
      changeAutomatedTestTechnology: checkedFields.some(field => field.fieldName === 'automatedTestTechnology'),
      changeScmRepository: checkedFields.some(field => field.fieldName === 'scmRepository'),
    };

    if (checkedFields.some(field => field.fieldName === 'importance')) {
      testCaseMassUpdatePatch.importanceAuto = this.importanceAutoCheck;
    }

    checkedFields.forEach(checkField => {
      testCaseMassUpdatePatch[checkField.fieldName] = checkField.selectedValue;
    });

    this.restService.post(['search/test-case/mass-update'], testCaseMassUpdatePatch).subscribe(
      () => {
        this.dialogReference.result = true;
        this.dialogReference.close();
      }
    );
  }

  private initializeAutomatedTestTechnologyOptions(): void {
    this.referentialDataService.automatedTestTechnologies$
      .pipe(take(1))
      .subscribe((automatedTestTechnologies => {
        automatedTestTechnologies.sort((a,b) => {
            return a.name.localeCompare(b.name);
        });
        this.automatedTestTechnologyOptions = [
            {
              id: '',
              label: this.translateService.instant('sqtm-core.generic.label.none.feminine')
            },
            ...automatedTestTechnologies.map(tech => ({id: tech.id, label: buildTechnologyLabel(tech)})),
        ];
      }));
  }

  private initializeScmRepositoryOptions(): void {
    this.referentialDataService.scmServers$
      .pipe(take(1))
      .subscribe((scmServers => {
        this.scmRepoUrlOptions = [
          {
            id: '',
            label: this.translateService.instant('sqtm-core.generic.label.none.masculine')
          },
          ...scmServers.flatMap(server => server.repositories.map(repo => ({
            id: repo.scmRepositoryId.toString(),
            label: buildScmRepositoryUrl(server, repo),
          })))
        ];
      }));
  }
}

interface TestCaseMassUpdatePatch {
  testCaseIds: number[];
  nature?: number;
  type?: number;
  automatable?: string;
  importanceAuto?: boolean;
  importance?: string;
  status?: string;
  changeAutomatedTestTechnology: boolean;
  automatedTestTechnology?: number;
  changeScmRepository: boolean;
  scmRepository?: number;
}

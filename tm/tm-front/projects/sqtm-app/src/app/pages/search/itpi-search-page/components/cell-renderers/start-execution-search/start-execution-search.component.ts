import {APP_BASE_HREF} from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewContainerRef
} from '@angular/core';
import {Router} from '@angular/router';
import {filter, switchMap} from 'rxjs/operators';
import {
  AbstractCellRendererComponent,
  AutomatedSuitePreview,
  AutomatedSuiteService,
  CampaignPermissions,
  ColumnDefinitionBuilder,
  DialogService,
  EntityReference,
  EntityType,
  GridService,
  IterationService,
  ReadOnlyPermissions,
  RequirementPermissions,
  TestCasePermissions
} from 'sqtm-core';
import {
  AutomatedTestsExecutionSupervisionDialogComponent
} from '../../../../../campaign-workspace/iteration-view/components/automated-tests-execution-supervision-dialog/automated-tests-execution-supervision-dialog.component';
import {
  ExecutionRunnerOpenerService
} from '../../../../../execution/execution-runner/services/execution-runner-opener.service';
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'sqtm-app-start-execution-search',
  template: `
    <div *ngIf="row && canExecute(row.simplePermissions)"
         class="full-height full-width flex-column icon-container current-workspace-main-color"
         (click)="openExecutionMenu()"
         nz-popover
         [nzPopoverVisible]="menuVisible$|async"
         [nzPopoverContent]="contentTemplate"
         [nzPopoverTrigger]="'click'"
         [nzPopoverPlacement]="'left'"
    >
      <i nz-icon nzType="play-circle" nzTheme="outline" class="table-icon-size"></i>
    </div>
    <ng-template #contentTemplate>
      <a *ngIf="row.data.executionExecutionMode === 'AUTOMATED'"
         class="launch-execution-link m-b-10" (click)="startAutomatedExecution()">
        {{'sqtm-core.campaign-workspace.test-plan.launch-execution.automated'|translate}}</a>
      <a class="launch-execution-link" (click)="startExecutionInDialog()">
        {{'sqtm-core.campaign-workspace.test-plan.launch-execution.manual'|translate}}</a>
      <a class="launch-execution-link m-t-10" (click)="startExecutionInPage()">
        {{'sqtm-core.campaign-workspace.test-plan.launch-execution.page'|translate}}</a>
    </ng-template>

  `,
  styleUrls: ['./start-execution-search.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StartExecutionSearchComponent extends AbstractCellRendererComponent implements OnInit, OnDestroy {

  menuVisible$ = new BehaviorSubject<boolean>(false);

  constructor(grid: GridService,
              public cdRef: ChangeDetectorRef,
              private viewContainerRef: ViewContainerRef,
              private iterationService: IterationService,
              private router: Router,
              private dialogService: DialogService,
              private automatedSuiteService: AutomatedSuiteService,
              private executionRunnerOpenerService: ExecutionRunnerOpenerService,
              @Inject(APP_BASE_HREF) private baseUrl: string) {
    super(grid, cdRef);
  }

  ngOnInit() {
  }

  startExecutionInDialog() {
    const iterationId = this.row.data.iterationId;
    const testPlanItemId = this.row.data.id;
    if (iterationId && testPlanItemId) {
      this.persistExecutionAndOpenDialog(iterationId, testPlanItemId);
    } else {
      throw Error(`Unable to start execution in iteration ${iterationId} for itpi ${testPlanItemId}`);
    }
  }

  private persistExecutionAndOpenDialog(iterationId, testPlanItemId) {
    this.iterationService
      .persistManualExecution(iterationId, testPlanItemId)
      .subscribe(executionId => {
        this.menuVisible$.next(false);
        this.grid.refreshData();
        this.executionRunnerOpenerService.openExecutionPrologue(executionId);
      });
  }

  openExecutionMenu() {
    this.menuVisible$.next(true);
    this.cdRef.detectChanges();
  }

  canExecute(simplePermissions: ReadOnlyPermissions | TestCasePermissions | RequirementPermissions | CampaignPermissions) {
    return (simplePermissions as CampaignPermissions).canExecute;
  }

  startExecutionInPage() {
    const iterationId = this.row.data.iterationId;
    const testPlanItemId = this.row.data.id;
    if (iterationId && testPlanItemId) {
      this.persistExecutionAndGoToExecutionPage(iterationId, testPlanItemId);
    } else {
      throw Error(`Unable to start execution in iteration ${iterationId} for itpi ${testPlanItemId}`);
    }
  }

  startAutomatedExecution() {
    this.menuVisible$.next(false);
    const iterationId = this.row.data.iterationId;
    const itemIdAsArray: number[] = [this.row.data.id];
    this.automatedSuiteService.updateTaScriptsForItems(itemIdAsArray).pipe(
      switchMap(() => this.automatedSuiteService.generateAutomatedSuitePreview({
        context: new EntityReference(iterationId, EntityType.ITERATION),
        testPlanSubsetIds: itemIdAsArray
      }))
    ).subscribe((preview: AutomatedSuitePreview) => this.openAutomatedExecutionSupervisionDialog(preview));
  }

  private openAutomatedExecutionSupervisionDialog(data: AutomatedSuitePreview) {
    const automatedExecutionDialog = this.dialogService.openDialog({
      id: 'automated-tests-execution-supervision',
      viewContainerReference: this.viewContainerRef,
      component: AutomatedTestsExecutionSupervisionDialogComponent,
      data,
      height: 600,
      width: 800
    });
    automatedExecutionDialog.dialogClosed$.pipe(
      filter(result => Boolean(result))
    ).subscribe(() => this.grid.refreshData());
  }

  private persistExecutionAndGoToExecutionPage(iterationId: number, testPlanItemId: number) {
    this.iterationService
      .persistManualExecution(iterationId, testPlanItemId)
      .subscribe(executionId => {
        this.router.navigate(['execution', executionId]);
      });
  }

  ngOnDestroy(): void {
    this.menuVisible$.complete();
  }

}

export function startExecutionSearchColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(StartExecutionSearchComponent);
}

export class ItpiMultiEditSearchConfiguration {
  id: string;
  titleKey: string;
  disabledExecutionStatus: string[];
  itpiIds: number[];
  canEditAllRows: boolean;
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  TestCaseSimpleSearchPageComponent
} from './containers/test-case-simple-search-page/test-case-simple-search-page.component';
import {
  SearchTestCaseExportDialogComponent
} from './components/search-test-case-export-dialog/search-test-case-export-dialog.component';
import {
  TestCaseMultiEditDialogComponent
} from './components/test-case-multi-edit-dialog/test-case-multi-edit-dialog.component';
// tslint:disable-next-line:max-line-length
import {
  TestCaseForCoverageSearchPageComponent
} from './containers/test-case-for-coverage-search-page/test-case-for-coverage-search-page.component';
import {RouterModule, Routes} from '@angular/router';
import {CellRendererCommonModule, DialogModule, WorkspaceCommonModule} from 'sqtm-core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {SearchViewModule} from '../search-view/search-view.module';
// tslint:disable-next-line:max-line-length
import {
  TestCaseEditableTextRendererComponent
} from './components/cell-renderers/test-case-editable-text-renderer/test-case-editable-text-renderer.component';
// tslint:disable-next-line:max-line-length
import {OverlayModule} from '@angular/cdk/overlay';
import {
  TestCaseForIterationSearchPageComponent
} from './containers/for-iteration/test-case-for-iteration-search-page/test-case-for-iteration-search-page.component';
import {
  TestCaseBoundToItemRendererComponent
} from './components/cell-renderers/test-case-bound-to-item-checkbox-renderer/test-case-bound-to-item-renderer.component';
import {
  TestCaseByRequirementSearchPageComponent
} from './containers/for-iteration/test-case-by-requirement-search-page/test-case-by-requirement-search-page.component';
import {
  TestCaseForTestSuiteSearchPageComponent
} from './containers/for-test-suite/test-case-for-test-suite-search-page/test-case-for-test-suite-search-page.component';
import {
  TestCaseByReqForTsSearchPageComponent
} from './containers/for-test-suite/test-case-by-req-for-ts-search-page/test-case-by-req-for-ts-search-page.component';
import {
  TestCaseForCampaignSearchPageComponent
} from './containers/for-campaign/test-case-for-campaign-search-page/test-case-for-campaign-search-page.component';
import {
  TestCaseForCampaignWorkspaceEntitiesSearchPageComponent
} from './containers/generic/test-case-for-campaign-workspace-entities-search-page/test-case-for-campaign-workspace-entities-search-page.component';
import {
  TestCaseByReqForCampaignSearchPageComponent
} from './containers/for-campaign/test-case-by-req-for-campaign-search-page/test-case-by-req-for-campaign-search-page.component';
import {
  TcByReqForCampaignWorkspaceEntitiesSearchPageComponent
} from './containers/generic/tc-by-req-for-campaign-workspace-entities-search-page/tc-by-req-for-campaign-workspace-entities-search-page.component';

const routes: Routes = [
  {
    path: '',
    component: TestCaseSimpleSearchPageComponent,
    pathMatch: 'full'
  },
  {
    path: 'coverage/:requirementVersionId',
    component: TestCaseForCoverageSearchPageComponent,
  },
  {
    path: 'iteration/:iterationId',
    component: TestCaseForIterationSearchPageComponent,
  },
  {
    path: 'iteration/:iterationId/coverage',
    component: TestCaseByRequirementSearchPageComponent
  },
  {
    path: 'test-suite/:testSuiteId',
    component: TestCaseForTestSuiteSearchPageComponent
  },
  {
    path: 'test-suite/:testSuiteId/coverage',
    component: TestCaseByReqForTsSearchPageComponent
  },
  {
    path: 'campaign/:campaignId',
    component: TestCaseForCampaignSearchPageComponent
  },
  {
    path: 'campaign/:campaignId/coverage',
    component: TestCaseByReqForCampaignSearchPageComponent
  }
];

@NgModule({
  declarations: [
    TestCaseSimpleSearchPageComponent,
    SearchTestCaseExportDialogComponent,
    TestCaseMultiEditDialogComponent,
    TestCaseForCoverageSearchPageComponent,
    TestCaseEditableTextRendererComponent,
    TestCaseForIterationSearchPageComponent,
    TestCaseBoundToItemRendererComponent,
    TestCaseByRequirementSearchPageComponent,
    TestCaseForTestSuiteSearchPageComponent,
    TestCaseByReqForTsSearchPageComponent,
    TestCaseForCampaignSearchPageComponent,
    TestCaseForCampaignWorkspaceEntitiesSearchPageComponent,
    TestCaseByReqForCampaignSearchPageComponent,
    TcByReqForCampaignWorkspaceEntitiesSearchPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SearchViewModule,
    WorkspaceCommonModule,
    NzToolTipModule,
    NzIconModule,
    TranslateModule.forChild(),
    DialogModule,
    NzButtonModule,
    ReactiveFormsModule,
    NzCheckboxModule,
    OverlayModule,
    CellRendererCommonModule
  ]
})
export class TestCaseSearchPageModule {
}

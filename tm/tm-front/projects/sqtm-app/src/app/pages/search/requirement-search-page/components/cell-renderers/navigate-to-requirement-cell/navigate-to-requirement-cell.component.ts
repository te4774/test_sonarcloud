import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  Fixed,
  GridService,
  IconLinkOptions,
  isIconLinkOptions,
} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-navigate-to-requirement-cell',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <a class="current-workspace-main-color vertical-center" style="margin:auto" [routerLink]="getUrl()" target="_blank">
          <i nz-icon [nzType]="options.iconName"
             [nzTheme]="'fill'">
          </i>
        </a>
      </div>
    </ng-container>
  `,
  styleUrls: ['./navigate-to-requirement-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigateToRequirementCellComponent extends AbstractCellRendererComponent{

  get options(): IconLinkOptions {
    const options = this.columnDisplay.options;

    if (!options || !isIconLinkOptions(options)) {
      throw Error('You must provide IconLinkOptions');
    } else {
      return options;
    }
  }

  constructor(public grid: GridService, private cdr: ChangeDetectorRef) {
    super(grid, cdr);
  }

  getUrl() {
      return this.getBaseUrl() + '/' + this.row.data[this.options.columnParamId];
  }

  private getBaseUrl() {
    return this.row.data['isHighLevelRequirement'] ? '/requirement-workspace/high-level-requirement': '/requirement-workspace/requirement';
  }
}

export function navigateToRequirementColumn(id: string, options: IconLinkOptions): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(NavigateToRequirementCellComponent)
    .withOptions(options)
    .disableHeader()
    .changeWidthCalculationStrategy(new Fixed(35));
}

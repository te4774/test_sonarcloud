export class TestCaseMultiEditConfiguration {
  id: string;
  titleKey: string;
  projectIds: Set<number>;
  testCaseIds: number[];

  // 'WRITE' permission on row
  writingRightOnLine: boolean;
}

import {TestBed} from '@angular/core/testing';

import {TestCaseSearchViewService} from './test-case-search-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {RestService} from 'sqtm-core';

describe('TestCaseSearchViewService', () => {
  let service: TestCaseSearchViewService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [{
        provide: TestCaseSearchViewService,
        useClass: TestCaseSearchViewService,
        deps: [RestService]
      }]
    });
    service = TestBed.inject(TestCaseSearchViewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

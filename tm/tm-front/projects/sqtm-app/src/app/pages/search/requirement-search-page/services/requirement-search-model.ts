import {UserView} from 'sqtm-core';

export class RequirementSearchModel {
  usersWhoCreatedRequirements: UserView[];
  usersWhoModifiedRequirements: UserView[];
}

export type RequirementSearchState = RequirementSearchModel;

export function initialRequirementSearchState(): Readonly<RequirementSearchState> {
  return {usersWhoCreatedRequirements: [], usersWhoModifiedRequirements: []};
}

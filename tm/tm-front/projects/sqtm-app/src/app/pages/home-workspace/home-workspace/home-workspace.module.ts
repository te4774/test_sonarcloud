import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeWorkspacePageComponent} from './containers/home-workspace-page/home-workspace-page.component';
import {RouterModule, Routes} from '@angular/router';
import {NavBarModule, UiManagerModule, WorkspaceCommonModule} from 'sqtm-core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {TranslateModule} from '@ngx-translate/core';
import {CustomDashboardModule} from '../../../components/custom-dashboard/custom-dashboard.module';

export const routes: Routes = [
  {
    path: '',
    component: HomeWorkspacePageComponent
  }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        UiManagerModule,
        NavBarModule,
        WorkspaceCommonModule,
        TranslateModule.forChild(),
        NzButtonModule,
        CustomDashboardModule
    ],
  declarations: [HomeWorkspacePageComponent],
})
export class HomeWorkspaceModule {
}

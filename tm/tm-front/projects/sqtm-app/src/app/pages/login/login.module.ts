import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginPageComponent} from './containers/login-page/login-page.component';
import {LoginFormComponent} from './components/login-form/login-form.component';
import {RouterModule, Routes} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {NavBarModule, UiManagerModule} from 'sqtm-core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';

export const routes: Routes = [
  {
    path: '',
    component: LoginPageComponent
  }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        TranslateModule.forChild(),
        RouterModule.forChild(routes),
        UiManagerModule,
        NzIconModule,
        NzButtonModule,
        NzInputModule,
        NavBarModule
    ],
  declarations: [LoginPageComponent, LoginFormComponent]
})
export class LoginModule {

}

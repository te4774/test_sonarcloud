import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {KeyNames} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-login-form',
  template: `
    <div [formGroup]="parent" class="form-grid">
      <span class="label-color">
        {{'sqtm-core.generic.label.login.singular'| translate}}
      </span>
      <input nz-input type="text" formControlName="login" (keypress)="handlePasswordKeypress($event)">
      <span class="label-color">
        {{'sqtm-core.generic.label.password'| translate}}
      </span>
      <input nz-input type="password" formControlName="password" (keypress)="handlePasswordKeypress($event)">
    </div>
    <div [attr.data-test-component-id]="'login-fail-warning'" *ngIf="showLoginError" class="login-failed m-t-20">
      <i nz-icon nzType="warning" nzTheme="outline"></i>
      {{'sqtm-core.home-workspace.login.failed' | translate}}
    </div>
    <div class="submit-button m-t-20">
      <button nz-button id="submit-login-form" type="button" nzSize="large"
              [nzLoading]="pending"
              (click)="handleSubmit()">
        {{'sqtm-core.home-workspace.login.label' | translate}}
      </button>
    </div>
  `,
  styleUrls: ['./login-form.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginFormComponent implements OnInit {

  @Input()
  parent: FormGroup;

  @Input()
  showLoginError = false;

  @Input()
  pending: boolean;

  @Output()
  submit = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
  }

  handleSubmit() {
    this.submit.emit();
  }

  handlePasswordKeypress($event: KeyboardEvent) {
    if ($event.key === KeyNames.ENTER) {
      this.handleSubmit();
    }
  }
}

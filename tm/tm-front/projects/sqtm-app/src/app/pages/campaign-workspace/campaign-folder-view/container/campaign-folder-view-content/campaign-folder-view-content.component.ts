import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {CampaignFolderViewService} from '../../services/campaign-folder-view.service';
import {Observable, Subject} from 'rxjs';
import {CampaignFolderViewComponentData} from '../campaign-folder-view/campaign-folder-view.component';
import {
  BindableEntity,
  createCustomFieldValueDataSelector,
  CustomDashboardBinding,
  CustomDashboardModel,
  CustomFieldData,
  EntityRowReference,
  EntityScope,
  ExecutionStatusCount,
  SquashTmDataRowType
} from 'sqtm-core';
import {concatMap, take, takeUntil} from 'rxjs/operators';
import {select} from '@ngrx/store';
import {CampaignFolderState} from "../../state/campaign-folder.state";
import * as _ from "lodash";

@Component({
  selector: 'sqtm-app-campaign-folder-view-content',
  templateUrl: './campaign-folder-view-content.component.html',
  styleUrls: ['./campaign-folder-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignFolderViewContentComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();
  componentData$: Observable<CampaignFolderViewComponentData>;

  customFieldData: CustomFieldData[];

  constructor(private campaignFolderViewService: CampaignFolderViewService) {
    this.componentData$ = campaignFolderViewService.componentData$;
  }

  ngOnInit() {
    this.componentData$.pipe(
      take(1),
      concatMap(() => this.campaignFolderViewService.refreshStats())
    ).subscribe();
    this.initCufs();
  }

  initCufs() {
    this.componentData$.pipe(
      takeUntil(this.unsub$),
      select(createCustomFieldValueDataSelector(BindableEntity.CAMPAIGN_FOLDER))
    ).subscribe(customFieldData => {
      this.customFieldData = customFieldData;
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  campaignProgressionChartHasErrors(componentData: CampaignFolderViewComponentData) {
    const i18nErrors = componentData.campaignFolder.campaignFolderStatisticsBundle.campaignProgressionStatistics.errors;
    return i18nErrors && i18nErrors.length > 0;
  }

  getScope(campaignFolder: CampaignFolderState): EntityScope[] {
    const ref = new EntityRowReference(campaignFolder.id, SquashTmDataRowType.CampaignFolder).asString();
    return [
      {
        id: ref,
        label: campaignFolder.name,
        projectId: campaignFolder.projectId
      }
    ];
  }

  hasTestPlanItems(executionStatusCount: ExecutionStatusCount) {
    return Object.values(executionStatusCount).reduce(_.add) > 0;
  }

  trackCfd(cfd: CustomFieldData) {
    return cfd.id;
  }

  displayFavoriteDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.campaignFolderViewService.changeDashboardToDisplay('dashboard');
  }

  displayDefaultDashboard($event: MouseEvent) {
    $event.stopPropagation();
    this.campaignFolderViewService.changeDashboardToDisplay('default');
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings] as CustomDashboardBinding[];
  }
}

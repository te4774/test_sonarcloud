import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {DeleteTestSuiteTestPlanItemConfiguration} from './delete-test-suite-test-plan-item-configuration';
import {DialogReference, TestSuiteService} from 'sqtm-core';
import {concatMap, take} from 'rxjs/operators';
import {TestSuiteViewService} from '../../services/test-suite-view.service';

@Component({
  selector: 'sqtm-app-delete-test-suite-test-plan-item-dialog',
  templateUrl: './delete-test-suite-test-plan-item-dialog.component.html',
  styleUrls: ['./delete-test-suite-test-plan-item-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteTestSuiteTestPlanItemDialogComponent implements OnInit {

  configuration: DeleteTestSuiteTestPlanItemConfiguration;

  constructor(private dialogReference: DialogReference<DeleteTestSuiteTestPlanItemConfiguration, boolean>,
              private testSuiteService: TestSuiteService,
              private testSuiteViewService: TestSuiteViewService) {
    this.configuration = dialogReference.data;
  }

  ngOnInit(): void {
  }

  confirmDetachFromTestSuite(itemTestPlanIds: number[]) {
    this.testSuiteService.detachTestCaseFromTestSuite(this.configuration.testSuiteId,
                                          this.configuration.itemTestPlanIds)
      .pipe(
        take(1),
        concatMap((response) => this.testSuiteViewService.refreshStateAfterDeletingTestPlanItems(itemTestPlanIds, response['nbIssues']))
      ).subscribe();
    this.dialogReference.close();
  }

  confirmRemoveFromTestSuiteAndIteration(itemTestPlanIds: number[]) {
    this.testSuiteService.removeTestCaseFromTestSuiteAndIteration(this.configuration.testSuiteId,
                                                    this.configuration.itemTestPlanIds)
      .pipe(
      take(1),
      concatMap((response) => this.testSuiteViewService.refreshStateAfterDeletingTestPlanItems(itemTestPlanIds, response['nbIssues']))
    ).subscribe();
    this.dialogReference.close();
  }
}

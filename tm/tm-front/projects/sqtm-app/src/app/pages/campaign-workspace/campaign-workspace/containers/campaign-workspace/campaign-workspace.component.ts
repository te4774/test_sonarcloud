import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {
  CampaignTreeNodeServerOperationHandler,
  column,
  Extendable,
  GridDefinition,
  GridService,
  gridServiceFactory,
  ReferentialDataService,
  RestService,
  tree,
  TreeNodeCellRendererComponent,
  TreeNodeRendererDisplayOptions
} from 'sqtm-core';
import {CAMPAIGN_WS_TREE, CAMPAIGN_WS_TREE_CONFIG} from '../../../campaign-workspace.constant';


export function campaignTreeConfigFactory(): GridDefinition {
  const options: TreeNodeRendererDisplayOptions = {ellipsisOnLeft: false, kind: 'treeNodeRendererDisplay'};
  return tree('campaign-workspace-main-tree')
    .server()
    .withServerUrl(['campaign-tree'])
    .withColumns([
      column('NAME')
        .enableDnd()
        .changeWidthCalculationStrategy(new Extendable(300))
        .withRenderer(TreeNodeCellRendererComponent)
        .withOptions(options)
    ])
    .build();
}

@Component({
  selector: 'sqtm-app-campaign-workspace',
  templateUrl: './campaign-workspace.component.html',
  styleUrls: ['./campaign-workspace.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CAMPAIGN_WS_TREE_CONFIG,
      useFactory: campaignTreeConfigFactory,
      deps: []
    },
    CampaignTreeNodeServerOperationHandler,
    {
      provide: CAMPAIGN_WS_TREE,
      useFactory: gridServiceFactory,
      deps: [RestService, CAMPAIGN_WS_TREE_CONFIG, ReferentialDataService, CampaignTreeNodeServerOperationHandler]
    },
    {
      provide: GridService,
      useExisting: CAMPAIGN_WS_TREE
    }
  ]
})
export class CampaignWorkspaceComponent implements OnInit, OnDestroy {
  constructor(public readonly referentialDataService: ReferentialDataService,
              private campaignWorkspaceTree: GridService) {
  }

  ngOnInit(): void {
    this.referentialDataService.refresh().subscribe();
  }

  ngOnDestroy(): void {
    this.campaignWorkspaceTree.complete();
  }
}

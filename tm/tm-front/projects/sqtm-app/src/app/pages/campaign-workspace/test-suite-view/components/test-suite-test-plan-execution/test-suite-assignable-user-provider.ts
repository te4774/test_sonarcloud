import {Observable} from 'rxjs';
import {ResearchColumnPrototype, UserHistorySearchProvider, UserListElement} from 'sqtm-core';
import {TestSuiteViewService} from '../../services/test-suite-view.service';
import {map, take} from 'rxjs/operators';
import {Injectable} from '@angular/core';

@Injectable()
export class TestSuiteAssignableUserProvider extends UserHistorySearchProvider {

  constructor(private testSuiteViewService: TestSuiteViewService) {
    super();
  }

  provideUserList(columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.testSuiteViewService.componentData$.pipe(
      take(1),
      map(data => data.testSuite.users.map(u => ({...u, selected: false})))
    );
  }

}


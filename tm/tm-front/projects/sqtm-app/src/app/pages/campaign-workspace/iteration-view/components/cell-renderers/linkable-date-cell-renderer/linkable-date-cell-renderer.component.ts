import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {DatePipe} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, getSupportedBrowserLang, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-linkable-date-cell-renderer',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <ng-container *ngIf="hasExecutions; else noExecutions">
          <a style="margin: auto 0;"
             class="txt-ellipsis"
             nz-tooltip [nzTooltipTitle]="displayString" href="">{{displayString}}</a>
        </ng-container>
        <ng-template #noExecutions>
          <span
            style="margin: auto 0;"
            class="txt-ellipsis"
            nz-tooltip [nzTooltipTitle]="displayString">{{displayString}}</span>
        </ng-template>
      </div>
    </ng-container>`,
  styleUrls: ['./linkable-date-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class LinkableDateCellRendererComponent extends AbstractCellRendererComponent {

  protected format = 'shortDate';

  get hasExecutions(): boolean {
    return this.row.data['hasExecutions'];
  }

  constructor(public grid: GridService, public cdRef: ChangeDetectorRef,
              public translateService: TranslateService, public datePipe: DatePipe) {
    super(grid, cdRef);
  }

  get displayString(): string {
    return this.formatDate(this.row.data[this.columnDisplay.id]);
  }

  private formatDate(date: Date): string {
    return this.datePipe.transform(date, this.format, '', getSupportedBrowserLang(this.translateService));
  }
}

export function linkableDateColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(LinkableDateCellRendererComponent)
    .withHeaderPosition('center');
}


@Component({
  selector: 'sqtm-app-linkable-date-time-cell-renderer',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <ng-container *ngIf="hasExecutions; else noExecutions">
          <a style="margin: auto 0;"
             class="txt-ellipsis"
             nz-tooltip [nzTooltipTitle]="displayString" href="">{{displayString}}</a>
        </ng-container>
        <ng-template #noExecutions>
          <span
            style="margin: auto 0;"
            class="txt-ellipsis"
            nz-tooltip [nzTooltipTitle]="displayString">{{displayString}}</span>
        </ng-template>
      </div>
    </ng-container>`,
  styleUrls: ['./linkable-date-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LinkableDateTimeCellRendererComponent extends LinkableDateCellRendererComponent {
  protected format = 'short';
}

export function linkableDateTimeColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(LinkableDateTimeCellRendererComponent)
    .withHeaderPosition('center');
}

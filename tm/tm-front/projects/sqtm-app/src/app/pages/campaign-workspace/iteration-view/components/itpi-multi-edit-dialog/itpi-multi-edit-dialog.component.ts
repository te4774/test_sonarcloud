import {AfterViewInit, ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {ItpiMultiEditDialogConfiguration} from './itpi-multi-edit-dialog.configuration';
import {
  DialogReference,
  DisplayOption,
  ExecutionStatus,
  formatFullUserName,
  Identifier,
  ListItem,
  OptionalGroupedMultiListComponent,
  OptionalSelectFieldComponent,
  ProjectDataMap,
  ReferentialDataService,
  RestService,
  SimpleUser
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {AbstractMassEditDialog} from '../../../../search/test-case-search-page/abstract-mass-edit-dialog';
import {concatMap, take} from 'rxjs/operators';
import {IterationViewService} from '../../services/iteration-view.service';
import {IterationState} from '../../state/iteration.state';

const UNASSIGNED_ID = 'UNASSIGNED';
const UNASSIGNED_KEY = 'sqtm-core.campaign-workspace.test-plan.label.user.unassigned';

@Component({
  selector: 'sqtm-app-itpi-multi-edit-dialog',
  templateUrl: './itpi-multi-edit-dialog.component.html',
  styleUrls: ['./itpi-multi-edit-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItpiMultiEditDialogComponent extends AbstractMassEditDialog implements OnInit, AfterViewInit {

  data: ItpiMultiEditDialogConfiguration;

  statusOptions: DisplayOption[];
  userOptions: DisplayOption[];
  testSuiteItems: ListItem[] = [];

  @ViewChild('statusField')
  statusField: OptionalSelectFieldComponent;

  @ViewChild('assigneeField')
  assigneeField: OptionalSelectFieldComponent;

  @ViewChild('testSuitesField')
  testSuitesField: OptionalGroupedMultiListComponent;

  itpiIds: number[];
  projectIds: number[];

  constructor(translateService: TranslateService,
              referentialDataService: ReferentialDataService,
              public dialogReference: DialogReference<ItpiMultiEditDialogConfiguration>,
              restService: RestService,
              public iterationViewService: IterationViewService) {
    super(translateService, referentialDataService, restService);
    this.data = this.dialogReference.data;
  }

  ngOnInit() {
    this.itpiIds = this.data.rows.map(row => row.data.itemTestPlanId);
    this.projectIds = this.data.rows.map(row => row.data.projectId);
  }

  ngAfterViewInit(): void {
    this.referentialDataService.projectDatas$
      .pipe(take(1))
      .subscribe((projectDataMap) => this.buildStatusOptions(projectDataMap));

    this.iterationViewService.componentData$
      .pipe(take(1))
      .subscribe(({iteration}) => {
        this.buildAssignableUserOptions(iteration);
        this.buildTestSuiteItems(iteration);
      });
  }

  confirm(): void {
    if (!this.statusField.check
      && !this.assigneeField.check
      && !this.testSuitesField.check) {
      this.dialogReference.result = false;
      this.dialogReference.close();
      return;
    }

    const payload: ItpiMassEditPatch = {};

    if (this.statusField.check) {
      payload.executionStatus = this.statusField.selectedValue;
    }

    if (this.assigneeField.check) {
      payload.changeAssignee = true;
      const selectedAssignee = this.assigneeField.selectedValue;
      payload.assignee = selectedAssignee === UNASSIGNED_ID ? null : selectedAssignee;
    }

    if (this.testSuitesField.check) {
      payload.changeTestSuites = true;
      payload.testSuites = this.testSuiteItems
        .filter(testSuiteItem => testSuiteItem.selected)
        .map(testSuiteItem => testSuiteItem.id);
    }

    this.restService.post(['iteration/test-plan', this.itpiIds.join(','), 'mass-update'], payload)
      .pipe(
        concatMap(() => this.iterationViewService.refreshExecutionStatusMapAfterMassEdit(this.itpiIds, payload))
    )
      .subscribe(() => {
        this.dialogReference.result = true;
        this.dialogReference.close();
      });
  }

  private getFilteredExecutionStatusKeys(projectDataMap: ProjectDataMap): string[] {
    const filteredExecutionStatusKeys = ['READY', 'RUNNING', 'SUCCESS', 'FAILURE', 'BLOCKED'];
    if (!this.isStatusDisabledOnAnyProject('SETTLED', projectDataMap)) {
      filteredExecutionStatusKeys.push('SETTLED');
    }
    if (!this.isStatusDisabledOnAnyProject('UNTESTABLE', projectDataMap)) {
      filteredExecutionStatusKeys.push('UNTESTABLE');
    }
    return filteredExecutionStatusKeys;
  }

  private isStatusDisabledOnAnyProject(statusKey: string, projectDataMap: ProjectDataMap): boolean {
    return Object.values(projectDataMap)
      .filter((projectData) => this.projectIds.includes(projectData.id))
      .find((projectData) => projectData.disabledExecutionStatus.includes(statusKey)) != null;
  }

  private buildStatusOptions(projectDataMap: ProjectDataMap): void {
    this.statusOptions = this.getFilteredExecutionStatusKeys(projectDataMap)
      .map(statusKey => ({
        id: statusKey,
        label: this.translateService.instant(ExecutionStatus[statusKey].i18nKey),
      }));

    this.statusField.selectedValue = this.statusOptions[0]?.id;
  }

  private buildAssignableUserOptions(iteration: IterationState) {
    const options = iteration.users.map(user => ({
      id: user.id,
      label: this.getFullUsername(user),
    }));

    options.sort((a, b) => a.label.localeCompare(b.label));

    this.userOptions = [{
      id: UNASSIGNED_ID,
      label: this.translateService.instant(UNASSIGNED_KEY),
    }, ...options];

    this.assigneeField.selectedValue = this.userOptions[0]?.id;
  }

  private getFullUsername(user: SimpleUser): string {
    return formatFullUserName(user);
  }

  private buildTestSuiteItems(iteration: IterationState): void {
    this.testSuiteItems = iteration.testSuites.map(suite => ({
      id: suite.id,
      label: suite.name,
      selected: false,
    })).sort( (a, b) =>{
      return a.label.localeCompare(b.label);
    });
  }

  selectedSuitesChanged($event: ListItem[]): void {
    this.testSuiteItems.forEach(item => item.selected = $event.map(selected => selected.id).includes(item.id));
    this.testSuitesField.multiListField.selectedItems = $event;
  }
}

export interface ItpiMassEditPatch {
  changeAssignee?: boolean;
  changeTestSuites?: boolean;
  executionStatus?: Identifier;
  assignee?: Identifier;
  testSuites?: Identifier[];
}

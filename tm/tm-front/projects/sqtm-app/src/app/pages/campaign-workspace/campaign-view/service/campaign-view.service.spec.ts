import {TestBed} from '@angular/core/testing';

import {CampaignViewService} from './campaign-view.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';

describe('TestCaseLibraryService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [AppTestingUtilsModule, HttpClientTestingModule, TranslateModule.forRoot()],
    providers: [CampaignViewService]
  }));

  it('should be created', () => {
    const service: CampaignViewService = TestBed.get(CampaignViewService);
    expect(service).toBeTruthy();
  });
});

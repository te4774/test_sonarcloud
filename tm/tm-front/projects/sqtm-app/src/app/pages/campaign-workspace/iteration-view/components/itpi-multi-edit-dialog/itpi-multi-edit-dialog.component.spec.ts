import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {ItpiMultiEditDialogComponent} from './itpi-multi-edit-dialog.component';
import {TranslateModule} from '@ngx-translate/core';
import {DataRow, DialogReference, ProjectDataMap, ReferentialDataService, RestService} from 'sqtm-core';
import {mockReferentialDataService, mockRestService} from '../../../../../utils/testing-utils/mocks.service';
import {IterationViewService} from '../../services/iteration-view.service';
import {ItpiMultiEditDialogConfiguration} from './itpi-multi-edit-dialog.configuration';
import {Observable, of} from 'rxjs';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {IterationViewComponentData} from '../../container/iteration-view/iteration-view.component';
import SpyObj = jasmine.SpyObj;

describe('ItpiMultiEditDialogComponent', () => {
  let component: ItpiMultiEditDialogComponent;
  let fixture: ComponentFixture<ItpiMultiEditDialogComponent>;
  let referentialDataService: SpyObj<ReferentialDataService>;
  let dialogRef: SpyObj<DialogReference>;
  let restService: SpyObj<RestService>;
  let iterationViewService: SpyObj<IterationViewService>;

  beforeEach(waitForAsync(() => {
    referentialDataService = mockReferentialDataService();
    referentialDataService.projectDatas$ = of({
      1: {disabledExecutionStatus: []},
    } as unknown as ProjectDataMap);

    const data: ItpiMultiEditDialogConfiguration = {
      id: 'anything',
      rows: [
        {
          id: 50,
          data: {
            itemTestPlanId: 50,
            projectId: 1
          },
          projectId: 1
        } as unknown as DataRow,
        {
          id: 51,
          data: {
            itemTestPlanId: 51,
            projectId: 1
          },
          projectId: 1
        } as unknown as DataRow
      ],
      titleKey: 'title',
    };

    const executionStatusMap = {
      50: 'READY',
      51: 'READY'
    };

    dialogRef = jasmine.createSpyObj(['close']);
    (dialogRef as any).data = data;

    restService = mockRestService();

    iterationViewService = jasmine.createSpyObj<IterationViewService>(['refreshExecutionStatusMapAfterMassEdit']);
    iterationViewService.componentData$ = of({
        iteration: {
          users: [{id: 12, login: 'admin', lastName: 'admin'}],
          testSuites: [],
          executionStatusMap: new Map(Object.entries(executionStatusMap))
        }
    }) as unknown as Observable<IterationViewComponentData>;
    iterationViewService.refreshExecutionStatusMapAfterMassEdit.and.returnValue(of(null));

    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: ReferentialDataService,
          useValue: referentialDataService,
        },
        {
          provide: DialogReference,
          useValue: dialogRef,
        },
        {
          provide: RestService,
          useValue: restService,
        },
        {
          provide: IterationViewService,
          useValue: iterationViewService
        }
      ],
      declarations: [ItpiMultiEditDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  describe('with all statuses enabled', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(ItpiMultiEditDialogComponent);
      component = fixture.componentInstance;
      component.assigneeField = {} as any;
      component.statusField = {} as any;
      component.testSuitesField = {} as any;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();

      expect(component.assigneeField.selectedValue).toBe('UNASSIGNED');
      expect(component.statusOptions.length).toBe(7);
    });

    it('should confirm and close', () => {
      component.assigneeField.check = true;
      component.assigneeField.selectedValue = 'toto';
      component.statusField.check = true;
      component.statusField.selectedValue = 'READY';
      component.testSuitesField.check = true;
      component.testSuiteItems = [
        {id: 101, selected: true},
        {id: 102, selected: false},
        {id: 103, selected: true},
      ];

      const expectedPath = ['iteration/test-plan', '50,51', 'mass-update'];
      const expectedPayload = {
        assignee: 'toto',
        executionStatus: 'READY',
        testSuites: [101, 103],
        changeAssignee: true,
        changeTestSuites: true,
      };

      component.confirm();
      expect(restService.post).toHaveBeenCalledWith(expectedPath, expectedPayload);
      expect(dialogRef.close).toHaveBeenCalled();
    });

    it('should not send request and close when confirmed with no field checked', () => {
      component.assigneeField.check = false;
      component.statusField.check = false;
      component.testSuitesField.check = false;
      component.confirm();
      expect(restService.post).not.toHaveBeenCalled();
      expect(dialogRef.close).toHaveBeenCalled();
    });
  });

  it('should create with restricted status options', () => {
    referentialDataService.projectDatas$ = of({
      1: {id: 1, disabledExecutionStatus: ['UNTESTABLE', 'SETTLED']},
      2: {id: 2, disabledExecutionStatus: []},
    } as unknown as ProjectDataMap);

    fixture = TestBed.createComponent(ItpiMultiEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeTruthy();
    expect(component.statusOptions.length).toBe(5);
  });
});


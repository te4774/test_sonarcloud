import {ChangeDetectionStrategy, Component, OnInit, ViewContainerRef} from '@angular/core';
import {CampaignViewService} from '../../service/campaign-view.service';
import {Observable} from 'rxjs';
import {CampaignViewComponentData} from '../campaign-view/campaign-view.component';
import {
  CampaignService,
  CustomDashboardModel,
  DialogService,
  EntityRowReference,
  EntityScope,
  ExecutionStatusCount,
  IterationPlanning,
  SquashTmDataRowType
} from 'sqtm-core';
import {
  baseIterationPlanningDialogConfiguration
} from '../../components/dialog/iteration-planning-dialog/iteration-planning-dialog.configuration';
import {concatMap, take} from 'rxjs/operators';
import * as _ from 'lodash';

@Component({
  selector: 'sqtm-app-campaign-view-dashboard',
  templateUrl: './campaign-view-dashboard.component.html',
  styleUrls: ['./campaign-view-dashboard.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignViewDashboardComponent implements OnInit {

  componentData$: Observable<CampaignViewComponentData>;

  constructor(private campaignViewService: CampaignViewService,
              private campaignService: CampaignService,
              private dialogService: DialogService,
              private vcr: ViewContainerRef) {
    this.componentData$ = this.campaignViewService.componentData$;
  }

  ngOnInit(): void {
    this.componentData$.pipe(
      take(1),
      concatMap(() => this.campaignViewService.refreshStats())
    ).subscribe();
  }

  campaignProgressionChartHasErrors(componentData: CampaignViewComponentData) {
    const i18nErrors = componentData.campaign.campaignStatisticsBundle.campaignProgressionStatistics.errors;
    return i18nErrors && i18nErrors.length > 0;
  }

  openPlanningDialog(event, id: number) {
    event.stopPropagation();
    this.campaignService.findIterationsPlanning(id).subscribe(result => {
        const dialogRef = this.dialogService.openDialog({
          ...baseIterationPlanningDialogConfiguration,
          viewContainerReference: this.vcr,
          data: {iterationPlannings: result}
        });

        dialogRef.dialogClosed$.subscribe((plannings: IterationPlanning[]) => {
          if (plannings != null) {
            this.campaignService.setIterationsPlanning(id, plannings)
              .pipe(concatMap(() => this.campaignViewService.refreshStats()))
              .subscribe();
          }
        });
      }
    );
  }

  getScope(componentData: CampaignViewComponentData): EntityScope[] {
    const id = new EntityRowReference(componentData.campaign.id, SquashTmDataRowType.Campaign).asString();
    return [{id, label: componentData.campaign.name, projectId: componentData.projectData.id}];
  }

  hasTestPlanItems(executionStatusCount: ExecutionStatusCount) {
    return Object.values(executionStatusCount).reduce(_.add) > 0;
  }

  getChartBindings(dashboard: CustomDashboardModel) {
    return [...dashboard.chartBindings, ...dashboard.reportBindings];
  }

  displayFavoriteDashboard($event) {
    $event.stopPropagation();
    this.campaignViewService.changeDashboardToDisplay('dashboard');
  }

  displayDefaultDashboard($event) {
    $event.stopPropagation();
    this.campaignViewService.changeDashboardToDisplay('default');
  }
}

import {TestBed} from '@angular/core/testing';

import {TestSuiteViewService} from './test-suite-view.service';
import {
  AttachmentService,
  CustomFieldValueService,
  EntityViewAttachmentHelperService,
  EntityViewCustomFieldHelperService,
  ReferentialDataService,
  RestService
} from 'sqtm-core';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import SpyObj = jasmine.SpyObj;

describe('TestSuiteViewService', () => {
  const referentialDataService: SpyObj<ReferentialDataService> = jasmine.createSpyObj('referentialDataService', ['connectToProjectData']);


  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AppTestingUtilsModule,
      HttpClientTestingModule,
      TranslateModule.forRoot()
    ],
    providers: [
      {
        provide: ReferentialDataService,
        useValue: referentialDataService
      },
      {
        provide: TestSuiteViewService,
        useClass: TestSuiteViewService,
        deps: [
          RestService,
          ReferentialDataService,
          AttachmentService,
          CustomFieldValueService,
          TranslateService,
          EntityViewAttachmentHelperService,
          EntityViewCustomFieldHelperService,
        ]
      }
    ]
  }));

  it('should be created', () => {
    const service: TestSuiteViewService = TestBed.inject(TestSuiteViewService);
    expect(service).toBeTruthy();
  });
});

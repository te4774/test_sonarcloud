import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject} from '@angular/core';
import {
  AbstractDeleteCellRenderer,
  ColumnDefinitionBuilder,
  ConfirmDeleteLevel,
  DataRow,
  DialogService,
  EntityReference,
  EntityType,
  GridService,
  RestService
} from 'sqtm-core';
import {finalize, switchMap, take} from 'rxjs/operators';
import {GENERIC_TEST_PLAN_VIEW_SERVICE, GenericTestPlanViewService} from '../../../../generic-test-plan-view-service';

@Component({
  selector: 'sqtm-app-delete-execution-history',
  template: `
    <ng-container *ngIf="row && canDelete(row)">
      <sqtm-core-delete-icon
        [iconName]="getIcon()"
        (delete)="showDeleteConfirm()"></sqtm-core-delete-icon>
    </ng-container>
  `,
  styleUrls: ['./delete-execution-history.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteExecutionHistoryComponent extends AbstractDeleteCellRenderer {

  constructor(public grid: GridService,
              cdr: ChangeDetectorRef,
              dialogService: DialogService,
              private restService: RestService,
              @Inject(GENERIC_TEST_PLAN_VIEW_SERVICE) private genericTestPlanViewService: GenericTestPlanViewService) {
    super(grid, cdr, dialogService);
  }

  getIcon(): string {
    return 'sqtm-core-generic:delete';
  }

  canDelete(row: DataRow): boolean {
    return row.simplePermissions && row.simplePermissions.canDelete
      && !row.data.boundToBlockingMilestone;
  }

  protected doDelete(): any {
    this.grid.beginAsyncOperation();
    const iterationId = this.row.data['iterationId'];
    const executionId = this.row.data['executionId'];
    this.genericTestPlanViewService.getEntityReference().pipe(
      take(1),
      switchMap((entityRef: EntityReference) => {
        if (entityRef.type === EntityType.ITERATION) {
          return this.restService.delete<{nbIssues: number}>(['iteration', iterationId, 'test-plan/execution', executionId]);
        } else if (entityRef.type === EntityType.TEST_SUITE) {
          return this.restService.delete<{nbIssues: number}>(['test-suite', entityRef.id, 'test-plan/execution', executionId]);
        }
      }),
      switchMap(response => this.genericTestPlanViewService.updateStateAfterExecutionDeletedInTestPlanItem(response.nbIssues)),
      finalize(() => this.grid.completeAsyncOperation())
    ).subscribe(() => this.grid.refreshData());
  }


  protected getLevel(): ConfirmDeleteLevel {
    return 'DANGER';
  }

  protected getMessageKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.message.remove-execution';
  }

  protected getTitleKey(): string {
    return 'sqtm-core.campaign-workspace.dialog.title.remove-execution';
  }
}

export function deleteExecutionHistoryColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DeleteExecutionHistoryComponent);
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NzProgressModule} from 'ng-zorro-antd/progress';
import {
  AutomatedExecutionEnvironmentModule
} from '../../components/automated-execution-environment/automated-execution-environment.module';
import {
  CampaignWorkspaceComponent
} from './campaign-workspace/containers/campaign-workspace/campaign-workspace.component';
import {RouterModule, Routes} from '@angular/router';
import {
  AnchorModule,
  AttachmentModule,
  CellRendererCommonModule,
  CustomFieldModule,
  DialogModule,
  GridExportModule,
  GridModule,
  IssuesModule,
  NavBarModule,
  SqtmDragAndDropModule,
  SvgModule,
  UiManagerModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule
} from 'sqtm-core';
import {
  CampaignWorkspaceTreeComponent
} from './campaign-workspace/containers/campaign-workspace-tree/campaign-workspace-tree.component';
import {
  CampaignLibraryViewComponent
} from './campaign-library-view/container/campaign-library-view/campaign-library-view.component';
import {
  CampaignLibraryViewContentComponent
} from './campaign-library-view/container/campaign-library-view-content/campaign-library-view-content.component';
import {
  CampaignFolderViewContentComponent
} from './campaign-folder-view/container/campaign-folder-view-content/campaign-folder-view-content.component';
import {
  CampaignFolderViewComponent
} from './campaign-folder-view/container/campaign-folder-view/campaign-folder-view.component';
import {IterationViewComponent} from './iteration-view/container/iteration-view/iteration-view.component';
import {
  IterationViewContentComponent
} from './iteration-view/container/iteration-view-content/iteration-view-content.component';
import {
  IterationTestPlanExecutionComponent
} from './iteration-view/components/iteration-test-plan-execution/iteration-test-plan-execution.component';
import {CampaignViewComponent} from './campaign-view/container/campaign-view/campaign-view.component';
import {
  CampaignViewContentComponent
} from './campaign-view/container/campaign-view-content/campaign-view-content.component';
import {TranslateModule} from '@ngx-translate/core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCollapseModule} from 'ng-zorro-antd/collapse';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzPopoverModule} from 'ng-zorro-antd/popover';
import {NzRadioModule} from 'ng-zorro-antd/radio';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {NzTypographyModule} from 'ng-zorro-antd/typography';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CKEditorModule} from 'ckeditor4-angular';
import {
  CampaignInformationPanelComponent
} from './campaign-view/components/campaign-information-panel/campaign-information-panel.component';
import {MilestoneModule} from '../../components/milestone/milestone.module';
import {
  StartExecutionComponent
} from './iteration-view/components/cell-renderers/start-execution/start-execution.component';
import {
  DeleteTestPlanItemComponent
} from './iteration-view/components/cell-renderers/delete-test-plan-item/delete-test-plan-item.component';
import {SuccessRateComponent} from './iteration-view/components/cell-renderers/success-rate/success-rate.component';
import {CampaignStatisticsPanelComponent} from './campaign-statistics-panel/campaign-statistics-panel.component';
import {
  CampaignViewPlanningPanelComponent
} from './campaign-view/components/campaign-view-planning-panel/campaign-view-planning-panel.component';
import {
  IterationPlanningDialogComponent
} from './campaign-view/components/dialog/iteration-planning-dialog/iteration-planning-dialog.component';
import {
  CampaignViewDashboardComponent
} from './campaign-view/container/campaign-view-dashboard/campaign-view-dashboard.component';
import {
  IterationInformationPanelComponent
} from './iteration-view/components/iteration-information-panel/iteration-information-panel.component';
import {
  IterationViewPlanningPanelComponent
} from './iteration-view/components/iteration-view-planning-panel/iteration-view-planning-panel.component';
import {TestSuiteViewComponent} from './test-suite-view/containers/test-suite-view/test-suite-view.component';
import {
  TestSuiteViewContentComponent
} from './test-suite-view/containers/test-suite-view-content/test-suite-view-content.component';
import {
  TestSuiteInformationPanelComponent
} from './test-suite-view/components/test-suite-information-panel/test-suite-information-panel.component';
import {
  IterationViewDashboardComponent
} from './iteration-view/container/iteration-view-dashboard/iteration-view-dashboard.component';
import {CampaignIssuesComponent} from './campaign-view/components/campaign-issues/campaign-issues.component';
import {IterationIssuesComponent} from './iteration-view/components/iteration-issues/iteration-issues.component';
import {TestSuiteIssuesComponent} from './test-suite-view/components/test-suite-issues/test-suite-issues.component';
import {
  CampaignFolderIssuesComponent
} from './campaign-folder-view/components/campaign-folder-issues/campaign-folder-issues.component';
import {
  FilteredExecutionStatusCellComponent
} from './iteration-view/components/cell-renderers/filtered-execution-status-cell/filtered-execution-status-cell.component';
import {ExecutionHistoryComponent} from './iteration-view/components/execution-history/execution-history.component';
import {
  DeleteExecutionHistoryComponent
} from './iteration-view/components/cell-renderers/delete-execution-history/delete-execution-history.component';
import {
  AssignedUserCellComponent
} from './iteration-view/components/cell-renderers/assigned-user-cell/assigned-user-cell.component';
import {
  DatasetCellRendererComponent
} from './campaign-workspace/components/test-plan/dataset-cell-renderer/dataset-cell-renderer.component';
import {
  LinkableDateCellRendererComponent,
  LinkableDateTimeCellRendererComponent
} from './iteration-view/components/cell-renderers/linkable-date-cell-renderer/linkable-date-cell-renderer.component';
import {
  IterationTestPlanDraggedContentComponent
} from './iteration-view/components/iteration-test-plan-dragged-content/iteration-test-plan-dragged-content.component';
import {
  ItpiMultiEditDialogComponent
} from './iteration-view/components/itpi-multi-edit-dialog/itpi-multi-edit-dialog.component';
import {
  LastExecutionDateCellComponent
} from './iteration-view/components/cell-renderers/last-execution-date-cell/last-execution-date-cell.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {
  TestSuiteTestPlanExecutionComponent
} from './test-suite-view/components/test-suite-test-plan-execution/test-suite-test-plan-execution.component';
import {
  TestCaseLinkCellComponent
} from './campaign-workspace/components/test-plan/test-case-link-cell/test-case-link-cell.component';
import {
  TestSuiteAssignedUserCellComponent
} from './test-suite-view/components/cell-renderers/test-suite-assigned-user-cell/test-suite-assigned-user-cell.component';
import {
  CampaignTestPlanExecutionComponent
} from './campaign-view/components/campaign-test-plan-execution/campaign-test-plan-execution.component';
import {
  TestSuiteTestPlanDraggedContentComponent
} from './test-suite-view/components/test-suite-test-plan-dragged-content/test-suite-test-plan-dragged-content.component';
import {
  DeleteTestSuiteTestPlanItemDialogComponent
} from './test-suite-view/components/delete-test-suite-test-plan-item-dialog/delete-test-suite-test-plan-item-dialog.component';
import {
  DeleteTestSuiteTestPlanItemComponent
} from './test-suite-view/components/cell-renderers/delete-test-suite-test-plan-item/delete-test-suite-test-plan-item.component';
import {
  ProjectLinkCellComponent
} from './campaign-workspace/components/test-plan/project-link-cell/project-link-cell.component';
import {
  DeleteCampaignTestPlanItemComponent
} from './campaign-view/components/cell-renderers/delete-campaign-test-plan-item/delete-campaign-test-plan-item.component';
import {
  TestSuiteExecutionStatusCellComponent
} from './test-suite-view/components/cell-renderers/test-suite-execution-status-cell/test-suite-execution-status-cell.component';
import {
  TestSuiteTpiMultiEditDialogComponent
} from './test-suite-view/components/test-suite-tpi-multi-edit-dialog/test-suite-tpi-multi-edit-dialog.component';
import {
  CampaignAssignedUserCellComponent
} from './campaign-view/components/cell-renderers/campaign-assigned-user-cell/campaign-assigned-user-cell.component';
import {
  CampaignTestPlanDraggedContentComponent
} from './campaign-view/components/campaign-test-plan-dragged-content/campaign-test-plan-dragged-content.component';
import {CampaignStatisticsModule} from '../../components/statistics/campaign-statistics/campaign-statistics.module';
import {
  CtpiMultiEditDialogComponent
} from './campaign-view/components/dialog/ctpi-multi-edit-dialog/ctpi-multi-edit-dialog.component';
import {
  AutomatedSuiteExecutionsDialogComponent
} from './iteration-view/components/automated-suite-executions-dialog/automated-suite-executions-dialog.component';
import {
  IterationAutomatedSuiteComponent
} from './iteration-view/components/iteration-automated-suite/iteration-automated-suite.component';
import {
  AutomatedExecutionReportRendererComponent
} from './iteration-view/components/cell-renderers/automated-execution-report-renderer/automated-execution-report-renderer.component';
import {
  AutomatedExecutionDetailsRendererComponent
} from './iteration-view/components/cell-renderers/automated-execution-details-renderer/automated-execution-details-renderer.component';
import {
  CampaignMilestoneViewComponent
} from './campaign-milestone-dashboard/containers/campaign-milestone-view/campaign-milestone-view.component';
import {
  CampaignMilestoneViewContentComponent
} from './campaign-milestone-dashboard/containers/campaign-milestone-view-content/campaign-milestone-view-content.component';
import {CustomDashboardModule} from '../../components/custom-dashboard/custom-dashboard.module';
import {NzAnchorModule} from 'ng-zorro-antd/anchor';
import {
  AutomatedSuiteReportUrlDialogComponent
} from './iteration-view/components/automated-suite-report-url-dialog/automated-suite-report-url-dialog.component';
import {
  AutomatedSuiteExecDatasetComponent
} from './iteration-view/components/cell-renderers/automated-suite-exec-dataset/automated-suite-exec-dataset.component';
import {
  TestSuiteAutomatedSuiteComponent
} from './test-suite-view/components/test-suite-automated-suite/test-suite-automated-suite.component';
import {
  AutomatedTestsExecutionSupervisionDialogComponent
} from './iteration-view/components/automated-tests-execution-supervision-dialog/automated-tests-execution-supervision-dialog.component';
import {
  AutomationProjectMenuComponent
} from './iteration-view/components/automation-project-menu/automation-project-menu.component';
import {
  TfExecutionTableComponent
} from './iteration-view/components/automated-tests-execution-supervision-dialog/tf-execution-table/tf-execution-table.component';
import {
  SquashAutomExecutionTableComponent
} from './iteration-view/components/automated-tests-execution-supervision-dialog/squash-autom-execution-table/squash-autom-execution-table.component';
import {
  SquashAutomProjectPreviewComponent
} from './iteration-view/components/squash-autom-project-preview-component/squash-autom-project-preview.component';
import {
  SquashAutomTestCasesTableComponent
} from './iteration-view/components/automated-tests-execution-supervision-dialog/squash-autom-test-cases-table/squash-autom-test-cases-table.component';
import {
  AutomatedExecutionEnvironmentVariableModule
} from '../../components/automated-execution-environment-variable/automated-execution-environment-variable.module';

export const routes: Routes = [
  {
    path: '',
    component: CampaignWorkspaceComponent,
    children: [
      {
        path: 'campaign-library/:campaignLibraryId', component: CampaignLibraryViewComponent, children: [
          {path: 'content', component: CampaignLibraryViewContentComponent},
        ]
      },
      {
        path: 'campaign-folder/:campaignFolderId', component: CampaignFolderViewComponent, children: [
          {path: 'content', component: CampaignFolderViewContentComponent},
          {path: 'issues', component: CampaignFolderIssuesComponent}
        ]
      },
      {
        path: 'iteration/:iterationId', component: IterationViewComponent, children: [
          {path: 'content', component: IterationViewContentComponent},
          {path: 'test-plan', component: IterationTestPlanExecutionComponent},
          {path: 'dashboard', component: IterationViewDashboardComponent},
          {path: 'issues', component: IterationIssuesComponent},
          {path: 'automated-suite', component: IterationAutomatedSuiteComponent},

        ]
      },
      {
        path: 'test-suite/:testSuiteId', component: TestSuiteViewComponent, children: [
          {path: 'content', component: TestSuiteViewContentComponent},
          {path: 'issues', component: TestSuiteIssuesComponent},
          {path: 'test-plan', component: TestSuiteTestPlanExecutionComponent},
          {path: 'automated-suite', component: TestSuiteAutomatedSuiteComponent},
        ]
      },
      {
        path: 'campaign/:campaignId', component: CampaignViewComponent, children: [
          {path: 'dashboard', component: CampaignViewDashboardComponent},
          {path: 'content', component: CampaignViewContentComponent},
          {path: 'issues', component: CampaignIssuesComponent},
          {path: 'test-plan', component: CampaignTestPlanExecutionComponent}
        ]
      },
      {
        path: 'milestone-dashboard', component: CampaignMilestoneViewComponent, children: [
          {path: 'content', component: CampaignMilestoneViewContentComponent}
        ]
      }
    ]
  }
];

@NgModule({
    declarations: [
        CampaignWorkspaceComponent,
        CampaignWorkspaceTreeComponent,
        CampaignFolderViewComponent,
        CampaignFolderViewContentComponent,
        CampaignLibraryViewComponent,
        CampaignLibraryViewContentComponent,
        CampaignViewComponent,
        CampaignViewContentComponent,
        CampaignInformationPanelComponent,
        CampaignStatisticsPanelComponent,
        CampaignTestPlanExecutionComponent,
        IterationViewComponent,
        IterationTestPlanExecutionComponent,
        IterationViewContentComponent,
        StartExecutionComponent,
        DeleteTestPlanItemComponent,
        SuccessRateComponent,
        CampaignViewPlanningPanelComponent,
        IterationPlanningDialogComponent,
        CampaignViewDashboardComponent,
        IterationInformationPanelComponent,
        IterationViewPlanningPanelComponent,
        TestSuiteViewComponent,
        TestSuiteViewContentComponent,
        TestSuiteInformationPanelComponent,
        IterationViewDashboardComponent,
        CampaignIssuesComponent,
        IterationIssuesComponent,
        TestSuiteIssuesComponent,
        CampaignFolderIssuesComponent,
        FilteredExecutionStatusCellComponent,
        ExecutionHistoryComponent,
        DeleteExecutionHistoryComponent,
        AssignedUserCellComponent,
        DatasetCellRendererComponent,
        LinkableDateCellRendererComponent,
        LinkableDateTimeCellRendererComponent,
        IterationTestPlanDraggedContentComponent,
        LastExecutionDateCellComponent,
        ItpiMultiEditDialogComponent,
        TestSuiteTestPlanExecutionComponent,
        TestSuiteAssignedUserCellComponent,
        TestCaseLinkCellComponent,
        TestSuiteTestPlanDraggedContentComponent,
        DeleteTestSuiteTestPlanItemDialogComponent,
        DeleteTestSuiteTestPlanItemComponent,
        ProjectLinkCellComponent,
        DeleteCampaignTestPlanItemComponent,
        TestSuiteExecutionStatusCellComponent,
        TestSuiteTpiMultiEditDialogComponent,
        CampaignAssignedUserCellComponent,
        CampaignTestPlanDraggedContentComponent,
        CtpiMultiEditDialogComponent,
        IterationAutomatedSuiteComponent,
        AutomatedExecutionReportRendererComponent,
        AutomatedExecutionDetailsRendererComponent,
        AutomatedSuiteExecutionsDialogComponent,
        CampaignMilestoneViewComponent,
        CampaignMilestoneViewContentComponent,
        AutomatedSuiteExecutionsDialogComponent,
        AutomatedSuiteReportUrlDialogComponent,
        AutomatedSuiteExecDatasetComponent,
        TestSuiteAutomatedSuiteComponent,
        AutomatedTestsExecutionSupervisionDialogComponent,
        AutomationProjectMenuComponent,
        TfExecutionTableComponent,
        SquashAutomExecutionTableComponent,
        SquashAutomProjectPreviewComponent,
        SquashAutomTestCasesTableComponent
    ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NavBarModule,
    SvgModule,
    WorkspaceCommonModule,
    UiManagerModule,
    WorkspaceLayoutModule,
    GridModule,
    CellRendererCommonModule,
    TranslateModule.forChild(),
    NzDropDownModule,
    ReactiveFormsModule,
    FormsModule,
    CKEditorModule,
    CustomFieldModule,
    DialogModule,
    NzIconModule,
    NzTypographyModule,
    NzInputModule,
    NzPopoverModule,
    NzToolTipModule,
    NzRadioModule,
    AnchorModule,
    NzAnchorModule,
    AttachmentModule,
    SqtmDragAndDropModule,
    NzCollapseModule,
    IssuesModule,
    MilestoneModule,
    NzButtonModule,
    OverlayModule,
    CampaignStatisticsModule,
    CustomDashboardModule,
    NzProgressModule,
    GridExportModule,
    AutomatedExecutionEnvironmentModule,
    AutomatedExecutionEnvironmentVariableModule
  ]
})
export class CampaignWorkspaceModule {
}

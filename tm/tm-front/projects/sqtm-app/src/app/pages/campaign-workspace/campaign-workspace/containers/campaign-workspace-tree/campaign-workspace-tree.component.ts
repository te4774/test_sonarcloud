import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewContainerRef
} from '@angular/core';
import {filter, map, switchMap, take, takeUntil} from 'rxjs/operators';
import {
  BindableEntity,
  CreateEntityDialogComponent,
  CreationDialogConfiguration,
  DataRow,
  DialogService,
  EntityCreationDialogData,
  GridPersistenceService,
  GridService,
  isDataRowBoundToBlockingMilestone,
  isMilestoneModeActivated,
  milestoneAllowModification,
  OptionalTextField,
  REFERENCE_FIELD,
  ReferentialDataService,
  RestService,
  SquashPlatformNavigationService,
  SquashTmDataRowType,
  TreeWithStatePersistence,
  WorkspaceTypeForPlugins,
  WorkspaceWizardMenuItem,
  WorkspaceWizardNavigationService,
} from 'sqtm-core';
import {CAMPAIGN_WS_TREE} from '../../../campaign-workspace.constant';
import {Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {combineLatest, Observable} from 'rxjs';

@Component({
  selector: 'sqtm-app-campaign-workspace-tree',
  templateUrl: './campaign-workspace-tree.component.html',
  styleUrls: ['./campaign-workspace-tree.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: CAMPAIGN_WS_TREE
    },
    {
      provide: WorkspaceWizardNavigationService,
      useClass: WorkspaceWizardNavigationService
    }
  ]
})
export class CampaignWorkspaceTreeComponent extends TreeWithStatePersistence implements OnInit, OnDestroy {

  canCreateCampaignFolder$: Observable<boolean>;
  canCreateCampaign$: Observable<boolean>;
  canCreateIteration$: Observable<boolean>;
  canCreateSuite$: Observable<boolean>;
  canExportCampaign$: Observable<boolean>;

  creationButtonClasses$: Observable<string>;
  exportButtonClasses$: Observable<string>;
  wizardButtonClasses$: Observable<string>;

  wizardMenuItems$: Observable<WorkspaceWizardMenuItem[]>;

  constructor(protected restService: RestService,
              public tree: GridService,
              public referentialDataService: ReferentialDataService,
              protected cdRef: ChangeDetectorRef,
              protected dialogService: DialogService,
              protected vcr: ViewContainerRef,
              protected gridPersistenceService: GridPersistenceService,
              protected router: Router,
              private squashPlatformNavigationService: SquashPlatformNavigationService,
              private  workspaceWizardNavigationService: WorkspaceWizardNavigationService) {
    super(tree, referentialDataService, gridPersistenceService, restService, router, dialogService, vcr,
      'campaign-workspace-main-tree', 'campaign-tree');
  }

  ngOnInit() {
    this.initEntityIdFromInitialUrl();
    this.initData();
    this.registerStatePersistence();
    this.initTreeSortMenu();
    this.initCreationMenu();
    this.initExportMenu();
    this.initWizardMenu();
    this.initTruncateNameMenu();
  }

  ngOnDestroy(): void {
    this.unregisterStatePersistence();
    this.workspaceWizardNavigationService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  openCreateCampaign() {
    this.canCreateCampaign$.pipe(
      take(1),
      filter(canCreate => canCreate),
      switchMap(() => this.tree.selectedRows$),
      take(1),
    ).subscribe((dataRows: DataRow[]) => {
      const dataRow = dataRows[0];

      const configuration: CreationDialogConfiguration<EntityCreationDialogData> = {
        viewContainerReference: this.vcr,
        formComponent: CreateEntityDialogComponent,
        data: {
          titleKey: 'sqtm-core.campaign-workspace.dialog.title.new-campaign',
          id: 'add-campaign',
          projectId: dataRow.projectId,
          bindableEntity: BindableEntity.CAMPAIGN,
          parentEntityReference: dataRow.type === 'Campaign' ? dataRow.parentRowId.toString() : dataRow.id.toString(),
          postUrl: 'campaign-tree/new-campaign',
          optionalTextFields: [REFERENCE_FIELD],
          addAnotherLabelKey: 'sqtm-core.generic.label.add-another.feminine'
        }
      };

      this.dialogService.openEntityCreationDialog(configuration, this.tree);
    });
  }

  openCreateIteration() {
    this.canCreateIteration$.pipe(
      take(1),
      filter(canCreate => canCreate),
      switchMap(() => this.tree.selectedRows$),
      take(1),
    ).subscribe((dataRows: DataRow[]) => {
      const parentRow = dataRows[0];
      const initialRef = Number(parentRow.data['CHILD_COUNT']);
      const parentRowId = parentRow.id.toString();

      // High order function that keeps incrementing the default reference (number of children in the parent row + 1)
      function initialiseReferenceCounter(counter: number) {
        return () => {
          counter++;
          return counter.toString(10);
        };
      }

      const iterationTextField: OptionalTextField = {
        fieldName: 'reference',
        validators: [Validators.maxLength(50)],
        defaultValue: initialiseReferenceCounter(initialRef)
      };

      const configuration: CreationDialogConfiguration<EntityCreationDialogData> = {
        viewContainerReference: this.vcr,
        formComponent: CreateEntityDialogComponent,
        data: {
          titleKey: 'sqtm-core.campaign-workspace.dialog.title.new-iteration',
          id: 'add-iteration',
          projectId: parentRow.projectId,
          bindableEntity: BindableEntity.ITERATION,
          parentEntityReference: parentRowId,
          postUrl: `campaign-tree/campaign/${parentRow.data['CLN_ID']}/new-iteration`,
          optionalTextFields: [iterationTextField],
          addAnotherLabelKey: 'sqtm-core.generic.label.add-another.feminine',
          hasOptionalCopyTestPlanField: true
        }
      };

      this.dialogService.openEntityCreationDialog(configuration, this.tree);
    });
  }

  openCreateFolder() {
    this.canCreateCampaignFolder$.pipe(
      take(1),
      filter(canCreate => canCreate),
      switchMap(() => this.tree.selectedRows$),
      take(1),
    ).subscribe((dataRows: DataRow[]) => {
      // The creation menu item is enabled if there's only 1 selected element, so we can safely take the first one
      const selectedDataRow = dataRows[0];

      const configuration: CreationDialogConfiguration<EntityCreationDialogData> = {
        viewContainerReference: this.vcr,
        formComponent: CreateEntityDialogComponent,
        data: {
          titleKey: 'sqtm-core.test-case-workspace.dialog.title.new-folder',
          id: 'add-campaign-folder',
          projectId: selectedDataRow.projectId,
          bindableEntity: BindableEntity.CAMPAIGN_FOLDER,
          parentEntityReference: selectedDataRow.id.toString(),
          postUrl: 'campaign-tree/new-folder'
        }
      };
      this.dialogService.openEntityCreationDialog<EntityCreationDialogData, DataRow>(configuration, this.tree);
    });
  }

  openCreateSuite() {
    this.canCreateSuite$.pipe(
      take(1),
      filter(canCreate => canCreate),
      switchMap(() => this.tree.selectedRows$),
      take(1),
    ).subscribe((dataRows: DataRow[]) => {
      // The creation menu item is enabled if there's only 1 selected element, so we can safely take the first one
      const selectedDataRow = dataRows[0];
      const configuration: CreationDialogConfiguration<EntityCreationDialogData> = {
        viewContainerReference: this.vcr,
        formComponent: CreateEntityDialogComponent,
        data: {
          titleKey: 'sqtm-core.campaign-workspace.dialog.title.new-suite',
          id: 'add-suite',
          projectId: selectedDataRow.projectId,
          bindableEntity: BindableEntity.TEST_SUITE,
          parentEntityReference: selectedDataRow.id.toString(),
          postUrl: 'campaign-tree/new-test-suite',
          addAnotherLabelKey: 'sqtm-core.generic.label.add-another.feminine'
        }
      };
      this.dialogService.openEntityCreationDialog<EntityCreationDialogData, DataRow>(configuration, this.tree);
    });
  }

  navigateToMilestoneDashboard() {
    this.tree.unselectAllRows();
    this.referentialDataService.milestoneModeData$.pipe(
      take(1),
      filter(milestoneData => isMilestoneModeActivated(milestoneData)),
    ).subscribe(() => this.router.navigate(['campaign-workspace', 'milestone-dashboard']));
  }

  navigateToResearch() {
    this.router.navigate(['/search/campaign']);
  }

  copy() {
    this.tree.copy();
  }

  paste() {
    this.tree.paste();
  }

  delete() {
    this.tree.deleteRows();
  }

  exportCampaign(exportType: 'L' | 'S' | 'F'): Observable<string> {
    if (this.canExportCampaign$) {
      return this.canExportCampaign$.pipe(
        take(1),
        filter(canExport => canExport),
        switchMap(() => this.tree.selectedRows$),
        take(1),
        map((dataRows: DataRow[]) => {
          const selectedDataRowId = dataRows[0].data['CLN_ID'];
          const exportUrl = `campaign-tree/export-campaign/${selectedDataRowId.toString()}`;
          const params = {
            export: 'csv',
            exportType: exportType
          };

          return this.restService.buildExportUrlWithParams(exportUrl, params);
        })
      );
    }
  }

  navigateToPlugin(menuItem: WorkspaceWizardMenuItem): void {
    if (menuItem.enabled) {
      this.squashPlatformNavigationService.navigateFromMainApplication(menuItem.generatedUrl);
    }
  }

  private initCreationMenu() {
    const selectionAllowsCreation$ = this.tree.selectedRowIds$.pipe(map(ids => ids.length === 1));

    const milestoneModeAllowModification$ = this.referentialDataService.milestoneModeData$.pipe(
      map((milestoneState) =>
        (milestoneState.milestoneFeatureEnabled && milestoneState.milestoneModeEnabled) ?
          (milestoneState.selectedMilestone && milestoneAllowModification(milestoneState.selectedMilestone))
          : true));

    const targetRowCreatePermission$ = this.tree.selectedRows$
      .pipe(map(rows => rows[0]?.simplePermissions.canCreate));

    const targetRowHasNoLockedMilestone$ = this.tree.selectedRows$
      .pipe(map(rows => rows[0] && !isDataRowBoundToBlockingMilestone(rows[0])));

    const genericCheck$ = combineLatest([
      selectionAllowsCreation$,
      milestoneModeAllowModification$,
      targetRowCreatePermission$,
      targetRowHasNoLockedMilestone$,
    ]).pipe(
      takeUntil(this.unsub$),
      map((booleans) => booleans.every(b => b)));

    const selectedRow$ = this.tree.selectedRows$.pipe(
      map(rows => rows[0]),
      filter(row => Boolean(row)));

    this.canCreateCampaignFolder$ = combineLatest([genericCheck$, selectedRow$]).pipe(
      map(([canCreate, selectedRow]) => canCreate
        && isSuitableCampaignFolderCreationTarget(selectedRow.type)));

    this.canCreateCampaign$ = combineLatest([genericCheck$, selectedRow$]).pipe(
      map(([canCreate, selectedRow]) => canCreate
        && isSuitableCampaignCreationTarget(selectedRow.type)));

    this.canCreateIteration$ = combineLatest([genericCheck$, selectedRow$]).pipe(
      map(([canCreate, selectedRow]) => canCreate
        && isSuitableIterationCreationTarget(selectedRow.type)));

    this.canCreateSuite$ = combineLatest([genericCheck$, selectedRow$]).pipe(
      map(([canCreate, selectedRow]) => canCreate
        && isSuitableTestSuiteCreationTarget(selectedRow.type)));

    this.creationButtonClasses$ = combineLatest([
      this.canCreateCampaign$,
      this.canCreateCampaignFolder$,
      this.canCreateIteration$,
      this.canCreateSuite$
    ]).pipe(
      map((booleans) => booleans.some(b => b)),
      map(isActive => `action-icon-size ${isActive ? 'current-workspace-button' : 'label-color'}`),
    );
  }

  private initExportMenu() {
    const singleRowSelected$ = this.tree.selectedRowIds$.pipe(map(ids => ids.length === 1));

    const targetRowExportPermission$ = this.tree.selectedRows$
      .pipe(map(rows => rows[0]?.simplePermissions.canExport));

    const genericCheck$ = combineLatest([
      singleRowSelected$,
      targetRowExportPermission$
    ]).pipe(
      takeUntil(this.unsub$),
      map((booleans) => booleans.every(b => b)));

    const selectedRow$ = this.tree.selectedRows$.pipe(
      map(rows => rows[0]),
      filter(row => Boolean(row)));



    this.canExportCampaign$ = combineLatest([genericCheck$, selectedRow$]).pipe(
      map(([canExport, selectedRow]) => canExport
        && isSuitableCampaignExportTarget(selectedRow.type)));

    this.exportButtonClasses$ = this.canExportCampaign$.pipe(
      map((canExport) => `action-icon-size ${canExport ? 'current-workspace-button' : 'label-color'}`)
    );
  }

  getExportMenuItemStyle(canExport: boolean) {
    return canExport ? {'color': 'black', 'text-decoration': 'none'} : {'color': 'rgba(0, 0, 0, 0.25)', 'text-decoration': 'none'};
  }

  private initWizardMenu() {
    this.wizardMenuItems$ = this.workspaceWizardNavigationService.initializeForWorkspace(WorkspaceTypeForPlugins.CAMPAIGN_WORKSPACE);

    this.wizardButtonClasses$ = this.wizardMenuItems$.pipe(
      map(items => items.some(item => item.enabled)),
      map(isMenuActive => `action-icon-size ${isMenuActive ? 'current-workspace-button' : 'label-color'}`));
  }
}

function isSuitableCampaignFolderCreationTarget(parentType: SquashTmDataRowType): boolean {
  return [
    SquashTmDataRowType.CampaignLibrary, SquashTmDataRowType.CampaignFolder
  ].includes(parentType);
}

function isSuitableCampaignCreationTarget(parentType: SquashTmDataRowType): boolean {
  return [
    SquashTmDataRowType.CampaignLibrary, SquashTmDataRowType.CampaignFolder, SquashTmDataRowType.Campaign
  ].includes(parentType);
}

function isSuitableIterationCreationTarget(parentType: SquashTmDataRowType): boolean {
  return [SquashTmDataRowType.Campaign].includes(parentType);
}

function isSuitableTestSuiteCreationTarget(parentType: SquashTmDataRowType): boolean {
  return [SquashTmDataRowType.Iteration].includes(parentType);
}

function isSuitableCampaignExportTarget(rowType: SquashTmDataRowType): boolean {
  return [SquashTmDataRowType.Campaign].includes(rowType);
}

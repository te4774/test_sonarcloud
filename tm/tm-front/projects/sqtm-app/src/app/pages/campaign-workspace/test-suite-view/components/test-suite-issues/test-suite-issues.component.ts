import {ChangeDetectionStrategy, Component} from '@angular/core';
import {take, tap} from 'rxjs/operators';
import {
  ExportModelBuilderService,
  Extendable,
  Fixed,
  grid,
  GridDefinition,
  GridExportService,
  GridService,
  gridServiceFactory,
  indexColumn,
  issueExecutionsColumn,
  issueKeyColumn,
  Limited,
  PaginationConfigBuilder,
  ReferentialDataService,
  RestService,
  Sort,
  textColumn
} from 'sqtm-core';
import {TEST_SUITE_ISSUE_TABLE, TEST_SUITE_ISSUE_TABLE_CONF} from '../../test-suite-view.constant';
import {TestSuiteViewService} from '../../services/test-suite-view.service';

@Component({
  selector: 'sqtm-app-test-suite-issues',
  template: `
    <ng-container *ngIf="testSuiteViewService.componentData$ | async as componentData">
      <div class="flex-column full-height full-width p-r-15" style="overflow-y: hidden;">
        <div class="flex-row p-r-10">
          <div class="sqtm-large-grid-container-title">
            {{'sqtm-core.test-case-workspace.title.issues' | translate}}
          </div>
          <div class="collapse-button flex-row">
            <sqtm-core-grid-export-menu
              *ngIf="componentData.permissions.canRead"
              [exportContent]="'sqtm-core.grid.export.issue' | translate"
              [workspaceName]="'sqtm-core.campaign-workspace.title.automated-suite' | translate"
              [fileFormat]="'csv'"
            ></sqtm-core-grid-export-menu>
          </div>
        </div>
        <div class="sqtm-large-grid-container full-width full-height p-15">
          <sqtm-core-issues-panel [entityId]="componentData.testSuite.id"
                                  [entityType]="'TEST_SUITE_TYPE'"
                                  [bugTracker]="componentData.projectData.bugTracker"
                                  (loadIssues)="loadData()">
            <sqtm-core-grid></sqtm-core-grid>
          </sqtm-core-issues-panel>
        </div>
      </div>
    </ng-container>`,
  styleUrls: ['./test-suite-issues.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TEST_SUITE_ISSUE_TABLE_CONF,
      useFactory: testSuiteIssuesTableDefinition
    },
    {
      provide: TEST_SUITE_ISSUE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TEST_SUITE_ISSUE_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: TEST_SUITE_ISSUE_TABLE
    },
    ExportModelBuilderService,
    GridExportService
  ]
})
export class TestSuiteIssuesComponent {

  constructor(public testSuiteViewService: TestSuiteViewService, private gridService: GridService) {
  }

  loadData() {
    this.testSuiteViewService.componentData$.pipe(
      take(1),
      tap(componentData =>
        this.gridService.setServerUrl([`issues/test-suite/${componentData.testSuite.id}/known-issues`]))
    ).subscribe();
  }

}

export function testSuiteIssuesTableDefinition(): GridDefinition {
  return grid('test-suite-view-issue').withColumns([
    indexColumn()
      .changeWidthCalculationStrategy(new Fixed(70))
      .withViewport('leftViewport'),
    issueKeyColumn('remoteId').withI18nKey('sqtm-core.entity.issue.key.label')
      .changeWidthCalculationStrategy(new Limited(100)),
    textColumn('btProject').withI18nKey('sqtm-core.entity.issue.project.label')
      .changeWidthCalculationStrategy(new Limited(150)).disableSort(),
    textColumn('summary').withI18nKey('sqtm-core.entity.issue.summary.label')
      .changeWidthCalculationStrategy(new Limited(200)).disableSort(),
    textColumn('priority').withI18nKey('sqtm-core.entity.issue.priority.label')
      .changeWidthCalculationStrategy(new Extendable(100, 0.4)).disableSort(),
    textColumn('status').withI18nKey('sqtm-core.entity.issue.status.label')
      .changeWidthCalculationStrategy(new Extendable(100, 0.4)).disableSort(),
    textColumn('assignee').withI18nKey('sqtm-core.entity.issue.assignee.label')
      .changeWidthCalculationStrategy(new Extendable(100, 1)).disableSort(),
    issueExecutionsColumn().withI18nKey('sqtm-core.entity.issue.reported-in.label')
      .changeWidthCalculationStrategy(new Extendable(100, 1.5)).disableSort(),
  ]).disableRightToolBar().server().withInitialSortedColumns([{
    id: 'remoteId',
    sort: Sort.DESC
  }]).withPagination(new PaginationConfigBuilder().initialSize(25)).withRowHeight(35).build();
}

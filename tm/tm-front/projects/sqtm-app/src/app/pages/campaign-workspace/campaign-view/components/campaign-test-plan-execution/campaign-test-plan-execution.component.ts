import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {CampaignViewComponentData} from '../../container/campaign-view/campaign-view.component';
import {CampaignViewService} from '../../service/campaign-view.service';
import {concatMap, filter, map, switchMap, take, takeUntil, tap} from 'rxjs/operators';
import {
  assigneeFilter,
  buildFilters,
  CampaignPermissions,
  convertSqtmLiterals,
  DataRow,
  DialogService,
  DRAG_AND_DROP_DATA,
  EntityRowReference,
  EntityViewComponentData,
  Extendable,
  Fixed,
  grid,
  GRID_PERSISTENCE_KEY,
  GridDefinition,
  GridDndData,
  GridFilter,
  GridFilterUtils,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  i18nEnumResearchFilter,
  indexColumn,
  isDndDataFromTestCaseTreePicker,
  Limited,
  milestoneLabelColumn,
  parseDataRowId,
  ProjectDataMap,
  ReferentialDataService,
  ResearchColumnPrototype,
  RestService,
  serverBackedGridTextFilter,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  SqtmDropEvent,
  SquashTmDataRowType,
  StyleDefinitionBuilder,
  TEST_CASE_TREE_PICKER_ID,
  testCaseImportanceColumn,
  textColumn,
  UserHistorySearchProvider,
  Workspaces,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {CAMPAIGN_TEST_PLAN_DROP_ZONE_ID, CPV_CTPE_TABLE, CPV_CTPE_TABLE_CONF} from '../../campaign-view.constant';
import {CampaignTestPlanOperationHandler} from '../../service/campaign-test-plan-operation-handler';
import {DatePipe} from '@angular/common';
import {CampaignState} from '../../state/campaign.state';
import {
  deleteCampaignTestPlanItemColumn
} from '../cell-renderers/delete-campaign-test-plan-item/delete-campaign-test-plan-item.component';
import {
  withProjectLinkColumn
} from '../../../campaign-workspace/components/test-plan/project-link-cell/project-link-cell.component';
import {
  dataSetColumn
} from '../../../campaign-workspace/components/test-plan/dataset-cell-renderer/dataset-cell-renderer.component';
import {
  withTestCaseLinkColumn
} from '../../../campaign-workspace/components/test-plan/test-case-link-cell/test-case-link-cell.component';
import {
  campaignAssignedUserColumn
} from '../cell-renderers/campaign-assigned-user-cell/campaign-assigned-user-cell.component';
import {
  CampaignAssignableUsersProvider
} from '../cell-renderers/campaign-assigned-user-cell/campaign-assignable-user-provider';
import {
  CampaignTestPlanDraggedContentComponent
} from '../campaign-test-plan-dragged-content/campaign-test-plan-dragged-content.component';
import {campaignViewLogger} from '../../campaign-view.logger';
import {CtpiMultiEditDialogComponent} from '../dialog/ctpi-multi-edit-dialog/ctpi-multi-edit-dialog.component';
import {CtpiMultiEditDialogConfiguration} from '../dialog/ctpi-multi-edit-dialog/ctpi-multi-edit-dialog.configuration';
import {CampaignViewState} from '../../state/campaign-view.state';

export function cpvCtpeTableDefinition(): GridDefinition {
  return grid('campaign-test-plan')
    .withColumns([
      indexColumn()
        .enableDnd()
        .withViewport('leftViewport'),
      withProjectLinkColumn('projectName')
        .withI18nKey('sqtm-core.entity.project.label.singular')
        .changeWidthCalculationStrategy(new Limited(150))
        .withAssociatedFilter(),
      milestoneLabelColumn('milestoneLabels')
        .changeWidthCalculationStrategy(new Limited(100)),
      textColumn('testCaseReference')
        .withI18nKey('sqtm-core.entity.generic.reference.label')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(150)),
      withTestCaseLinkColumn('testCaseName')
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Limited(400)),
      testCaseImportanceColumn('importance')
        .withI18nKey('sqtm-core.entity.test-case.importance.label-short-dot')
        .withTitleI18nKey('sqtm-core.entity.test-case.importance.label')
        .isEditable(false)
        .withAssociatedFilter()
        .changeWidthCalculationStrategy(new Fixed(80)),
      dataSetColumn('datasetName', {kind: 'dataset', itemIdType: 'ctpiId'})
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withAssociatedFilter()
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .withHeaderPosition('left')
        .changeWidthCalculationStrategy(new Limited(100)),
      campaignAssignedUserColumn('user')
        .withI18nKey('sqtm-core.generic.label.user')
        .withAssociatedFilter('login')
        .withHeaderPosition('left')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      deleteCampaignTestPlanItemColumn('delete')
        .withLabel('')
        .disableSort()
        .changeWidthCalculationStrategy(new Fixed(50))
        .withViewport('rightViewport')
    ]).server()
    .withRowConverter(ctpiLiteralConverter)
    .disableRightToolBar()
    .withRowHeight(35)
    .withStyle(new StyleDefinitionBuilder()
      .enableInitialLoadAnimation()
      .showLines())
    .enableInternalDrop()
    .enableDrag()
    .withDraggedContentRenderer(CampaignTestPlanDraggedContentComponent)
    .build();

}

const logger = campaignViewLogger.compose('CampaignTestPlanExecutionComponent');

@Component({
  selector: 'sqtm-app-campaign-test-plan-execution',
  templateUrl: './campaign-test-plan-execution.component.html',
  styleUrls: ['./campaign-test-plan-execution.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: CPV_CTPE_TABLE_CONF,
      useFactory: cpvCtpeTableDefinition
    },
    CampaignTestPlanOperationHandler,
    {
      provide: CPV_CTPE_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, CPV_CTPE_TABLE_CONF, ReferentialDataService, CampaignTestPlanOperationHandler]
    },
    {provide: GridService, useExisting: CPV_CTPE_TABLE},
    {provide: UserHistorySearchProvider, useClass: CampaignAssignableUsersProvider},
    {
      provide: CampaignTestPlanDraggedContentComponent,
      useClass: CampaignTestPlanDraggedContentComponent,
      deps: [DRAG_AND_DROP_DATA, CPV_CTPE_TABLE],
    },
    {
      provide: GRID_PERSISTENCE_KEY,
      useValue: 'campaign-test-plan-grid'
    },
    GridWithStatePersistence
  ]
})
export class CampaignTestPlanExecutionComponent implements OnInit, OnDestroy, AfterViewInit {

  dropZoneId = CAMPAIGN_TEST_PLAN_DROP_ZONE_ID;

  componentData$: Observable<CampaignComponentData>;

  hasSelectedRows$: Observable<boolean>;
  activeFilters$: Observable<GridFilter[]>;

  unsub$ = new Subject<void>();

  @ViewChild('content', {read: ElementRef})
  content: ElementRef;

  testCaseWorkspace = Workspaces['test-case-workspace'];

  constructor(private campaignViewService: CampaignViewService,
              private gridService: GridService,
              private renderer: Renderer2,
              private dialogService: DialogService,
              private restService: RestService,
              private viewContainerRef: ViewContainerRef,
              private gridWithStatePersistence: GridWithStatePersistence,
              private workspaceWithTree: WorkspaceWithTreeComponent) {
  }

  ngOnInit(): void {
    this.componentData$ = this.campaignViewService.componentData$;
    this.gridService.addFilters(this.buildGridFilters());
    this.hasSelectedRows$ = this.gridService.hasSelectedRows$.pipe(
      takeUntil(this.unsub$)
    );
  }

  ngOnDestroy(): void {
    this.gridService.complete();
    this.unsub$.next();
    this.unsub$.complete();
  }

  ngAfterViewInit(): void {
    this.gridWithStatePersistence.popGridState().subscribe(() => {
      this.fetchTestPlan();
    });
    this.activeFilters$ = this.gridService.activeFilters$.pipe(
      takeUntil(this.unsub$),
      map((gridFilters: GridFilter[]) => gridFilters.filter(gridFilter => GridFilterUtils.mustIncludeFilter(gridFilter))),
    );
  }

  private fetchTestPlan() {
    this.componentData$.pipe(
      takeUntil(this.unsub$),
      filter((componentData: CampaignViewComponentData) => Boolean(componentData.campaign.id)),
      take(1),
      map(componentData => {
        const showMilestones = componentData.globalConfiguration.milestoneFeatureEnabled;
        this.gridService.setColumnVisibility('milestoneLabels', showMilestones);
        return componentData;
      })
    ).subscribe(componentData => {
      this.gridService.setServerUrl([`campaign/${componentData.campaign.id}/test-plan`]);
    });
  }

  private buildGridFilters() {
    return buildFilters([
      serverBackedGridTextFilter('projectName'),
      serverBackedGridTextFilter('testCaseReference'),
      serverBackedGridTextFilter('testCaseName'),
      i18nEnumResearchFilter('importance', ResearchColumnPrototype.TEST_CASE_IMPORTANCE)
        .alwaysActive(),
      serverBackedGridTextFilter('datasetName'),
      assigneeFilter('login', ResearchColumnPrototype.ITEM_TEST_PLAN_TESTER)
        .alwaysActive()
    ]);
  }

  openMassEditDialog() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => rows.map(row => row.data.ctpiId)),
      switchMap((ctpiIds: number[]) => this.doOpenMassEditDialog(ctpiIds)),
      tap(() => this.gridService.beginAsyncOperation()),
      tap(() => this.gridService.completeAsyncOperation())
    ).subscribe(() => {
      this.gridService.refreshData();
      this.refreshIterationNode();
    });
  }

  private refreshIterationNode(): void {
    this.campaignViewService.componentData$.pipe(
      take(1),
      map(({campaign}) => new EntityRowReference(campaign.id, SquashTmDataRowType.Campaign).asString())
    ).subscribe((identifier) => this.workspaceWithTree.requireNodeRefresh([identifier]));
  }

  toggleTestCasePickerDrawer() {
    this.campaignViewService.toggleTestCaseTreePicker();
  }

  showMassDeleteCtpiDialog() {
    this.gridService.selectedRows$.pipe(
      take(1),
      filter((rows: DataRow[]) => rows.length > 0),
      map((rows: DataRow[]) => this.filterDeletableRows(rows)),
      concatMap((rows: DataRow[]) => this.openMassDeleteDialog(rows)),
      tap(() => this.gridService.beginAsyncOperation()),
      concatMap(({
                   campaignId,
                   rowIds
                 }) => this.restService.delete([`campaign/${campaignId.toString()}/test-plan/${rowIds.toString()}`]).pipe(
        map(() => rowIds)
      )),
      concatMap((rowIds: number[]) => this.campaignViewService.refreshNbTestPlanItemsAfterDelete(rowIds)),
      tap(() => this.gridService.completeAsyncOperation()),
    ).subscribe(() => this.gridService.refreshData());
  }

  private openMassDeleteDialog(selectedRows: DataRow[]): Observable<{ campaignId: number, rowIds: number[] }> {
    const rowIds = selectedRows.map(row => parseInt(row.data.ctpiId, 10));
    const campaignId = selectedRows[0].data['campaignId'];

    const dialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.campaign-workspace.dialog.title.mass-remove-itpi',
      messageKey: 'sqtm-core.campaign-workspace.dialog.message.mass-remove-association.unbind',
      level: 'WARNING'
    });

    return dialogReference.dialogClosed$.pipe(
      filter(result => Boolean(result)),
      map(() => ({campaignId, rowIds})));
  }

  private filterDeletableRows(rows: DataRow[]): DataRow[] {
    if (rows.some(row => !row.simplePermissions.canExtendedDelete)) {
      return rows.filter(row => row.data.lastExecutedOn == null);
    }
    return rows;
  }

  dropIntoTestPlan($event: SqtmDropEvent) {
    if ($event.dragAndDropData.origin === TEST_CASE_TREE_PICKER_ID) {
      this.dropTestCasesIntoTestPlan($event);
    }
  }

  private dropTestCasesIntoTestPlan($event: SqtmDropEvent) {
    const data = $event.dragAndDropData.data as GridDndData;
    if (logger.isDebugEnabled()) {
      logger.debug(`Dropping test cases into campaign test plan.`, [data]);
    }
    const testCaseIds = data.dataRows.map(row => parseDataRowId(row));
    this.campaignViewService.addTestCaseIntoTestPlan(testCaseIds)
      .subscribe((res: CampaignViewState) => {
        this.gridService.setColumnVisibility('datasetName', res.campaign.hasDatasets);
        this.gridService.refreshData();
        this.unmarkAsDropZone();
      });
  }

  private unmarkAsDropZone() {
    this.renderer.removeClass(this.content.nativeElement, 'drop-test-case');
  }

  dragEnter($event: SqtmDragEnterEvent) {
    if (isDndDataFromTestCaseTreePicker($event)) {
      this.markAsDropZone();
    }
  }

  private markAsDropZone() {
    this.renderer.addClass(this.content.nativeElement, 'drop-test-case');
  }

  dragLeave($event: SqtmDragLeaveEvent) {
    if (isDndDataFromTestCaseTreePicker($event)) {
      this.unmarkAsDropZone();
    }
  }

  dragCancel() {
    this.unmarkAsDropZone();
  }

  private doOpenMassEditDialog(ctpiIds: number[]) {
    const dialogReference = this.dialogService.openDialog<CtpiMultiEditDialogConfiguration, void>({
      id: 'ctpi-multi-edit',
      component: CtpiMultiEditDialogComponent,
      viewContainerReference: this.viewContainerRef,
      data: {
        id: 'ctpi-multi-edit',
        titleKey: 'sqtm-core.search.generic.modify.selection',
        ctpiIds,
      }
    });

    return dialogReference.dialogClosed$;
  }

  shouldShowResetFilterLink(activeFilters: GridFilter[]): boolean {
    return activeFilters?.length > 0;
  }

  resetFilters() {
    this.gridService.resetFilters();
  }
}

export function ctpiLiteralConverter(literals: Partial<DataRow>[], projectDataMap: ProjectDataMap): DataRow[] {
  const ctpiLiterals = literals.map(li => ({...li, type: SquashTmDataRowType.CampaignTestPlanItem}));
  return convertSqtmLiterals(ctpiLiterals, projectDataMap);
}

type CampaignComponentData = EntityViewComponentData<CampaignState, 'campaign', CampaignPermissions>;

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TestSuiteViewService} from '../../services/test-suite-view.service';
import {
  AttachmentDrawerComponent,
  AttachmentService,
  AttachmentState,
  CampaignPermissions,
  CapsuleInformationData,
  CustomFieldValueService,
  EntityRowReference,
  EntityViewAttachmentHelperService,
  EntityViewComponentData,
  EntityViewCustomFieldHelperService,
  EntityViewService,
  ExecutionStatus,
  GenericEntityViewService,
  Identifier,
  ReferentialDataService,
  RestService,
  SquashTmDataRowType,
  TestPlanStatus,
  TestSuiteService,
  WorkspaceWithTreeComponent
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {Observable, Subject} from 'rxjs';
import {TestSuiteState} from '../../state/test-suite.state';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {GENERIC_TEST_PLAN_VIEW_SERVICE} from '../../../generic-test-plan-view-service';

@Component({
  selector: 'sqtm-app-test-suite-view',
  templateUrl: './test-suite-view.component.html',
  styleUrls: ['./test-suite-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TestSuiteViewService,
      useClass: TestSuiteViewService,
      deps: [
        RestService,
        ReferentialDataService,
        AttachmentService,
        CustomFieldValueService,
        TranslateService,
        EntityViewAttachmentHelperService,
        EntityViewCustomFieldHelperService,
        TestSuiteService
      ]
    },
    {
      provide: EntityViewService,
      useExisting: TestSuiteViewService
    },
    {
      provide: GenericEntityViewService,
      useExisting: TestSuiteViewService
    },
    {
      provide: GENERIC_TEST_PLAN_VIEW_SERVICE,
      useExisting: TestSuiteViewService
    }
  ]
})
export class TestSuiteViewComponent implements OnInit, OnDestroy {

  componentData$: Observable<TestSuiteViewComponentData>;

  @ViewChild(AttachmentDrawerComponent)
  attachmentDrawer: AttachmentDrawerComponent;

  unsub$ = new Subject<void>();

  constructor(private route: ActivatedRoute,
              private cdRef: ChangeDetectorRef,
              private referentialDataService: ReferentialDataService,
              private testSuiteViewService: TestSuiteViewService,
              private workspaceWithTree: WorkspaceWithTreeComponent) {
    this.componentData$ = this.testSuiteViewService.componentData$;
  }

  ngOnInit(): void {
    this.referentialDataService.loaded$.pipe(
      takeUntil(this.unsub$),
      filter(loaded => loaded),
      take(1)
    ).subscribe(() => {
      this.loadData();
    });
    this.initializeTreeSynchronization();
  }

  private initializeTreeSynchronization() {
    this.testSuiteViewService.simpleAttributeRequiringRefresh = ['name', 'executionStatus'];
    this.testSuiteViewService.externalRefreshRequired$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.testSuiteViewService.componentData$),
      map(([{}, componentData]: [{}, TestSuiteViewComponentData]) =>
        new EntityRowReference(componentData.testSuite.id, SquashTmDataRowType.TestSuite).asString())
    ).subscribe((identifier: Identifier) => {
        this.workspaceWithTree.requireNodeRefresh([identifier]);
      });
  }

  private loadData() {
    this.route.paramMap
      .pipe(
        takeUntil(this.unsub$),
        map((params: ParamMap) => params.get('testSuiteId')),
      ).subscribe((id) => {
      this.testSuiteViewService.load(parseInt(id, 10)).subscribe();
    });
  }

  ngOnDestroy(): void {
      this.testSuiteViewService.complete();
      this.unsub$.next();
      this.unsub$.complete();
  }

  createNewTestSuite() {

  }

  getAttachmentCount(attachmentState: AttachmentState): number {
    const attachments = Object.values(attachmentState.entities);
    return attachments.filter(attachment => attachment.kind === 'persisted-attachment').length;
  }

  toggleAttachmentPanel() {
    this.attachmentDrawer.open();
  }

  getExecutionStatusInformationData(statusKey: string): CapsuleInformationData {
    const executionStatus = ExecutionStatus[statusKey];
    return {
      id: executionStatus.id,
      color: executionStatus.color,
      icon: executionStatus.icon,
      labelI18nKey: executionStatus.i18nKey,
      titleI18nKey: 'sqtm-core.campaign-workspace.execution-status.label'
    };
  }

  getProgressStateInformationData(statusKey: string): CapsuleInformationData {
    const progressStatus = TestPlanStatus[statusKey];
    return {
      id: progressStatus.id,
      labelI18nKey: progressStatus.i18nKey,
      titleI18nKey: 'sqtm-core.campaign-workspace.progress-state.label'
    };
  }
}

export interface TestSuiteViewComponentData extends EntityViewComponentData<TestSuiteState, 'testSuite', CampaignPermissions> {

}


import {ChangeDetectionStrategy, Component, ElementRef, Host, Input, ViewChild} from '@angular/core';
import {DialogReference, DialogService, EditableDateFieldComponent} from 'sqtm-core';
import {IterationViewComponentData} from '../../container/iteration-view/iteration-view.component';
import {IterationViewService} from '../../services/iteration-view.service';

@Component({
  selector: 'sqtm-app-iteration-view-planning-panel',
  templateUrl: './iteration-view-planning-panel.component.html',
  styleUrls: ['./iteration-view-planning-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IterationViewPlanningPanelComponent {

  @ViewChild('actualStartDate')
  actualStartDate: EditableDateFieldComponent;

  @ViewChild('actualEndDate')
  actualEndDate: EditableDateFieldComponent;

  @ViewChild('scheduledStartDate')
  scheduledStartDate: EditableDateFieldComponent;

  @ViewChild('scheduledEndDate')
  scheduledEndDate: EditableDateFieldComponent;

  @Input()
  iterationViewComponentData: IterationViewComponentData;

  private readonly TWO_COLUMNS_LAYOUT_MIN_SIZE = 600;

  constructor(private iterationViewService: IterationViewService,
              private dialogService: DialogService,
              @Host() private hostElement: ElementRef<HTMLElement>) {}


  get isSmallLayout(): boolean {
    return this.hostElement.nativeElement.getBoundingClientRect().width < this.TWO_COLUMNS_LAYOUT_MIN_SIZE;
  }

  get canEdit(): boolean {
    return this.iterationViewComponentData.permissions.canWrite && this.iterationViewComponentData.milestonesAllowModification;
  }

  get isActualStartDateEditable(): boolean {
    return this.canEdit && !this.iterationViewComponentData.iteration.actualStartAuto;
  }

  get isActualEndDateEditable(): boolean {
    return this.canEdit && !this.iterationViewComponentData.iteration.actualEndAuto;
  }

  disabledActualStartDate = (actualStart: Date): boolean => {
    if (!actualStart || !this.actualEndDate.value) {
      return false;
    }
    const actualEndDate = new Date(this.actualEndDate.value);
    return this.compareDateOnly(actualStart, actualEndDate);
  }

  disabledActualEndDate = (actualEnd: Date): boolean => {
    if (!this.actualStartDate.value || !actualEnd) {
      return false;
    }
    const actualStartDate = new Date(this.actualStartDate.value);
    return this.compareDateOnly(actualStartDate, actualEnd);
  }

  disabledScheduledStartDate = (scheduledStart: Date): boolean => {
    if (!scheduledStart || !this.scheduledEndDate.value) {
      return false;
    }
    const scheduledEndDate = new Date(this.scheduledEndDate.value);
    return this.compareDateOnly(scheduledStart, scheduledEndDate) ;
  }

  disabledScheduledEndDate = (scheduledEnd: Date): boolean => {
    if (!this.scheduledStartDate.value || !scheduledEnd) {
      return false;
    }
    const scheduledStartDate = new Date(this.scheduledStartDate.value);
    return this.compareDateOnly(scheduledStartDate, scheduledEnd) ;
  }

  compareDateOnly(dateOne: Date, dateTwo: Date): boolean {
    const dateOneNoTime = this.removeTime(dateOne);
    const dateTwoNoTime = this.removeTime(dateTwo);
    return dateOneNoTime.getTime() > dateTwoNoTime.getTime();
  }

  removeTime(date: Date) {
    const dateOut = new Date(date);
    dateOut.setHours(0, 0, 0, 0);
    return dateOut;
  }

  updateScheduledStartDate(scheduledStartDate: Date, scheduledEndDate: Date) {
    if (this.canUpdateDate(scheduledStartDate, scheduledEndDate)) {
      this.iterationViewService.updateScheduledStartDate(scheduledStartDate).subscribe(
        () => this.scheduledStartDate.value = scheduledStartDate
      );
    } else {
      this.showAlertDateDialog();
      this.scheduledStartDate.endAsync();
      this.scheduledStartDate.cancel();

    }
  }

  updateActualStartDate(actualStartDate: Date, actualEndDate: Date) {
    if (this.canUpdateDate(actualStartDate, actualEndDate)) {
      this.iterationViewService.updateActualStartDate(actualStartDate).subscribe(
        () => this.actualStartDate.value = actualStartDate
      );
    } else {
      this.showAlertDateDialog();
      this.actualStartDate.endAsync();
      this.actualStartDate.cancel();
    }
  }

  updateActualStartAuto(startAuto: boolean) {
    this.iterationViewService.updateActualStartAuto(startAuto).subscribe();
  }

  updateScheduledEndDate(scheduledEndDate: Date, scheduledStartDate: Date) {
    if (this.canUpdateDate(scheduledStartDate, scheduledEndDate)) {
      this.iterationViewService.updateScheduledEndDate(scheduledEndDate).subscribe(
        () => this.scheduledEndDate.value = scheduledEndDate
      );
    } else {
      this.showAlertDateDialog();
      this.scheduledEndDate.endAsync();
      this.scheduledEndDate.cancel();
    }
  }

  updateActualEndDate(actualEndDate: Date, actualStartDate: Date) {
    if (this.canUpdateDate(actualStartDate, actualEndDate)) {
      this.iterationViewService.updateActualEndDate(actualEndDate).subscribe(
        () => this.actualEndDate.value = actualEndDate
      );
    } else {
      this.showAlertDateDialog();
      this.actualEndDate.endAsync();
      this.actualEndDate.cancel();
    }
  }

  updateActualEndAuto(endAuto: boolean) {
    this.iterationViewService.updateActualEndAuto(endAuto).subscribe();
  }

  showAlertDateDialog(): DialogReference {
    return this.dialogService.openAlert({
      level: 'DANGER',
      messageKey: 'sqtm-core.validation.errors.time-period-not-consistent'
    });
  }

  private canUpdateDate(startDate: Date, endDate: Date) {
    if (startDate != null && endDate != null) {
      const start = new Date(startDate);
      const end = new Date(endDate);
      return start <= end;
    } else {
      return true;
    }
  }
}

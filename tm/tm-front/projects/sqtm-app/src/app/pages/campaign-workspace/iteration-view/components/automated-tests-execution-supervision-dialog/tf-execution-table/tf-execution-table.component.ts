import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {
  DataRow,
  executionStatusColumn,
  Extendable,
  Fixed,
  grid,
  GridService,
  gridServiceFactory,
  pagination,
  ReferentialDataService,
  RestService,
  StyleDefinitionBuilder,
  textColumn
} from 'sqtm-core';
import {TF_EXECUTION_TABLE, TF_EXECUTION_TABLE_CONF} from '../../../iteration-view.constant';

function tfExecutionTableDefinition() {
  return grid('tf-automated-test-plan-execution')
    .withColumns([
      textColumn('automatedProject')
        .withI18nKey('sqtm-core.entity.automation-project')
        .changeWidthCalculationStrategy(new Extendable(150, 0.25)),
      textColumn('name')
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .changeWidthCalculationStrategy(new Extendable(150, 0.25)),
      executionStatusColumn('status')
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .withTitleI18nKey('sqtm-core.entity.execution.status.label')
        .changeWidthCalculationStrategy(new Fixed(60)),
      textColumn('node')
        .withI18nKey('sqtm-core.entity.execution-server.label.short-dot')
        .changeWidthCalculationStrategy(new Extendable(150, 0.25)),
    ])
    .withPagination(pagination().initialSize(10))
    .disableRightToolBar()
    .withStyle(new StyleDefinitionBuilder()
      .enableInitialLoadAnimation()
      .showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-tf-execution-table',
  templateUrl: './tf-execution-table.component.html',
  styleUrls: ['./tf-execution-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: TF_EXECUTION_TABLE_CONF,
      useFactory: tfExecutionTableDefinition
    },
    {
      provide: TF_EXECUTION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, TF_EXECUTION_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: TF_EXECUTION_TABLE
    }
  ]
})
export class TfExecutionTableComponent implements OnInit {

  @Input()
  set dataRows(value: any[]) {
    if (value != null) {
      this._dataRows = value;
      this.gridService.loadInitialDataRows(this._dataRows, this._dataRows.length);
    };
  }

  _dataRows: Partial<DataRow>[] = [];

  @Input()
  progress: number = 0;

  constructor(private gridService: GridService, private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewContainerRef} from '@angular/core';
import {
  AbstractCellRendererComponent,
  AttachmentListModel,
  ColumnDefinitionBuilder,
  DataRow,
  DialogService,
  GridService,
  RestService
} from 'sqtm-core';
import {
  AutomatedSuiteReportUrlDialogComponent
} from '../../automated-suite-report-url-dialog/automated-suite-report-url-dialog.component';

@Component({
  selector: 'sqtm-app-automated-execution-report-renderer',
  template: `
    <ng-container *ngIf="row">
      <div class="full-height full-width icon-container">
        <div *ngIf="canShowIcon(row); else noExecutionReport"
             class=" current-workspace-main-color __hover_pointer"
             (click)="showExecutionReport()">
          <i nz-icon nzType="sqtm-core-campaign:exec_report" nzTheme="outline" class="table-icon-size"></i>
        </div>
      </div>
      <ng-template #noExecutionReport>
        <span>/</span>
      </ng-template>
    </ng-container>
  `,
  styleUrls: ['./automated-execution-report-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomatedExecutionReportRendererComponent extends AbstractCellRendererComponent implements OnInit {


  constructor(gridService: GridService,
              cdRef: ChangeDetectorRef,
              private dialogService: DialogService,
              private restService: RestService,
              private vcr: ViewContainerRef) {
    super(gridService, cdRef);
  }

  canShowIcon(row: DataRow) {
    return row.data['hasResultUrl'] || row.data['automatedSuiteHasAttachment'] || row.data['executionExtenderHasAttachment'];
  }

  ngOnInit(): void {
  }

  showExecutionReport() {
    this.restService.get<{ reportUrls: string[], attachmentLists: AttachmentListModel[] }>(
      ['automated-suite', this.row.data.suiteId, 'report-urls']).subscribe(
      result => {
        this.dialogService.openDialog({
          id: 'automated-suite-execution-report',
          component: AutomatedSuiteReportUrlDialogComponent,
          viewContainerReference: this.vcr,
          data: {reportUrls: result.reportUrls, attachmentLists: result.attachmentLists},
          height: 600,
          width: 800
        });
      }
    );


  }

}

export function automatedExecutionReportColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id)
    .withRenderer(AutomatedExecutionReportRendererComponent)
    .withHeaderPosition('center');
}

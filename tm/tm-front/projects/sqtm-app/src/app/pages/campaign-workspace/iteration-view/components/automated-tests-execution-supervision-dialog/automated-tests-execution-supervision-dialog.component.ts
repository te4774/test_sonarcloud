import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {combineLatest, interval, Observable, of, Subject} from 'rxjs';
import {map, switchMap, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {
  AutomatedSuiteOverview,
  AutomatedSuitePreview,
  AutomatedSuiteService,
  BoundEnvironmentVariable,
  DataRow,
  DialogReference
} from 'sqtm-core';
import {
  SquashAutomProjectPreviewComponent
} from '../squash-autom-project-preview-component/squash-autom-project-preview.component';

@Component({
  selector: 'sqtm-app-automated-tests-execution-supervision-dialog',
  templateUrl: './automated-tests-execution-supervision-dialog.component.html',
  styleUrls: ['./automated-tests-execution-supervision-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomatedTestsExecutionSupervisionDialogComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChildren(SquashAutomProjectPreviewComponent)
  squashAutomProjectPreviews: QueryList<SquashAutomProjectPreviewComponent>;

  configuration: AutomatedSuitePreview;

  currentStep: ExecutionSupervisionDialogStep;

  tfExecutionDataRows: Partial<DataRow>[] = [];
  tfExecutionProgress: number = 0;
  squashAutomExecutionDataRows: Partial<DataRow>[] = [];

  jenkinsProjectsFormGroup: FormGroup;
  squashAutomProjectsFormGroup: FormGroup;

  private automatedSuiteId: string;

  areAllPreviewsLaunchable$: Observable<boolean> = of(true);

  private interval$: Observable<any> = interval(5000);

  private unsub$ = new Subject<void>();

  constructor(
    private dialogRef: DialogReference<AutomatedSuitePreview>,
    private fb: FormBuilder,
    private automatedSuiteService: AutomatedSuiteService,
    private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.jenkinsProjectsFormGroup = this.fb.group({});
    this.squashAutomProjectsFormGroup = this.fb.group({});
    this.configuration = this.dialogRef.data as unknown as AutomatedSuitePreview;
    if (this.configuration.manualServerSelection) {
      this.currentStep = 'select-execution-server';
    } else {
      this.loadExecutionPhase();
    }
  }

  ngAfterViewInit() {
    // if no squashAutomProjectPreviews exist, then Observable value stays 'true'
    if (this.squashAutomProjectPreviews != null && this.squashAutomProjectPreviews.length > 0) {
      this.areAllPreviewsLaunchable$ =
        combineLatest(this.squashAutomProjectPreviews.map(preview => preview.areAllTestsLaunchable())).pipe(
          takeUntil(this.unsub$),
          map((areSquashAutomPreviewsLaunchableArray) =>
            areSquashAutomPreviewsLaunchableArray.every(isSquashAutomPreviewLaunchable => isSquashAutomPreviewLaunchable)));
    }
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

  confirmSelectServerStep() {
    this.getNamespacesByTmProjectId()
      .pipe(
        take(1),
        withLatestFrom(this.getEnvironmentVariablesByProjectId())
      ).subscribe(([namespacesByProjectId, environmentVariablesByProject]: [any, Map<number, BoundEnvironmentVariable[]>]) => {
        this.configuration.specification.executionConfigurations =
          Object.keys(this.jenkinsProjectsFormGroup.value)
            .map((key: string) => ({
              projectId: +key,
              node: this.jenkinsProjectsFormGroup.value[key]
            }));
        this.configuration.specification.squashAutomExecutionConfigurations =
          Object.keys(this.squashAutomProjectsFormGroup.value)
            .map((key: string) => ({
              projectId: +key,
              namespaces: namespacesByProjectId.get(+key),
              environmentTags: this.squashAutomProjectsFormGroup.value[key],
              environmentVariables: this.convertEnvironmentVariables(environmentVariablesByProject.get(Number(key)))
            }));
        this.loadExecutionPhase();
      }
    );
  }

  private convertEnvironmentVariables(environmentVariables: BoundEnvironmentVariable[]): { [key: string]: string } {
    const environmentVariablesMap: { [key: string]: string } = {};
    environmentVariables.forEach(environmentVariable => environmentVariablesMap[environmentVariable.name] = environmentVariable.value);
    return environmentVariablesMap;
  }

  loadExecutionPhase() {
    this.automatedSuiteService.createAndExecuteAutomatedSuite(this.configuration.specification)
      .pipe(
        take(1),
        map((overview: AutomatedSuiteOverview) => ({
          ...overview,
          executions: overview.executions.map(execution => ({id: execution.id, data: execution})),
          items: overview.items.map(item => ({id: item.id, data: item}))
        }))).subscribe((overview: { suiteId: string, executions: Partial<DataRow>[], items: Partial<DataRow>[] }) => {
      this.dialogRef.result = true;
      this.automatedSuiteId = overview.suiteId;
      if (overview.executions.length > 0) {
        this.tfExecutionDataRows = overview.executions;
        this.startPullingExecutionStatus();
      }
      if (overview.items.length > 0) {
        this.squashAutomExecutionDataRows = overview.items;
      }
      this.cdr.detectChanges();
    });
    this.currentStep = 'execution-supervision';
  }

  startPullingExecutionStatus(): void {
    this.interval$.pipe(
      takeUntil(this.unsub$),
      switchMap(() => this.automatedSuiteService.updateExecutionsInformation(this.automatedSuiteId)),
      map((overview: AutomatedSuiteOverview) => ({
        ...overview,
        executions: overview.executions.map(execution => ({id: execution.id, data: execution}))
      })))
      .subscribe((overview: { executions: any[], percentage: number }) => {
        this.tfExecutionDataRows = overview.executions;
        this.tfExecutionProgress = overview.percentage;
        this.cdr.detectChanges();
        if (overview.percentage === 100) {
          this.unsub$.next();
        }
      });
  }

  closeExecutionPhase() {
    if (this.tfExecutionDataRows.length > 0 && this.tfExecutionProgress !== 100) {
      this.currentStep = 'close-warning';
    } else {
      this.dialogRef.close();
    }
  }

  resumeExecutionPhase() {
    this.currentStep = 'execution-supervision';
  }

  confirmCloseExecutionPhase() {
    this.dialogRef.close();
  }

  getNamespacesByTmProjectId(): Observable<Map<number, string[]>> {
    const namespacesByProjectId = new Map<number, string[]>();
    if (this.squashAutomProjectPreviews != null && this.squashAutomProjectPreviews.length > 0) {
      return combineLatest(this.squashAutomProjectPreviews.map(preview => preview.getNamespacesOfSelectedEnvironments())).pipe(
        tap(arrayOfProjectIdAndNamespaces =>
          arrayOfProjectIdAndNamespaces.forEach(projectIdAndNamespaces =>
            namespacesByProjectId.set(projectIdAndNamespaces.projectId, projectIdAndNamespaces.namespaces))),
        map(() => namespacesByProjectId));
    } else {
      // if no squashAutomProjectPreviews exist, then return the empty map
      return of(namespacesByProjectId);
    }
  }

  getEnvironmentVariablesByProjectId(): Observable<Map<number, BoundEnvironmentVariable[]>> {
    const environmentVariablesByProjectId = new Map<number, BoundEnvironmentVariable[]>();
    if (this.squashAutomProjectPreviews != null && this.squashAutomProjectPreviews.length > 0) {
      return combineLatest(this.squashAutomProjectPreviews.map(preview => this.getProjectIdAndBoundEnvironmentVariables(preview))).pipe(
        tap((arrayOfProjectIdAndEnvironmentVariables: { environmentVariables: BoundEnvironmentVariable[], projectId: number }[]) =>
          arrayOfProjectIdAndEnvironmentVariables.forEach(projectIdAndEnvironmentVariables =>
            environmentVariablesByProjectId.set(projectIdAndEnvironmentVariables.projectId,
              projectIdAndEnvironmentVariables.environmentVariables)
          )),
        map(() => environmentVariablesByProjectId));
    } else {
      return of(environmentVariablesByProjectId);
    }
  }

  private getProjectIdAndBoundEnvironmentVariables(preview: SquashAutomProjectPreviewComponent):
    Observable<{ environmentVariables: BoundEnvironmentVariable[], projectId: number }> {
    return preview.getBoundEnvironmentVariables().pipe(
      take(1),
      map((boundEnvironmentVariables) => (
        {environmentVariables: boundEnvironmentVariables, projectId: preview.squashAutomProject.projectId}))
    );
  }
}

export type ExecutionSupervisionDialogStep = 'select-execution-server' | 'execution-supervision' | 'close-warning';

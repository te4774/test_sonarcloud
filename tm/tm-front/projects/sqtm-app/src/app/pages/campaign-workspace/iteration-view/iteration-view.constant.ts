import {InjectionToken} from '@angular/core';
import {GridDefinition, GridService} from 'sqtm-core';

export const ITERATION_TEST_PLAN_DROP_ZONE_ID = 'iterationTestPlanDropZone';

export const ITV_ITPE_TABLE_CONF =
  new InjectionToken<GridDefinition>('Grid config for the iteration test plan execution table of iteration view');

export const ITV_ITPE_TABLE =
  new InjectionToken<GridService>('Grid service instance for the iteration test plan execution table of iteration view');

export const ITV_ITPE_HISTORY_TABLE_CONF =
  new InjectionToken<GridDefinition>('Grid config for the iteration test plan execution history table of iteration view');

export const ITV_ITPE_HISTORY_TABLE =
  new InjectionToken<GridService>('Grid service instance for the iteration test plan execution history table of iteration view');

export const TF_EXECUTION_TABLE_CONF =
  new InjectionToken<GridDefinition>('Grid config for TF automated test plan execution table');

export const TF_EXECUTION_TABLE =
  new InjectionToken<GridService>('Grid service instance for TF automated test plan execution table');

export const SQUASH_AUTOM_EXECUTION_TABLE_CONF =
  new InjectionToken<GridDefinition>('Grid config for Squash Autom automated test plan execution table');

export const SQUASH_AUTOM_EXECUTION_TABLE =
  new InjectionToken<GridService>('Grid service instance for Squash Autom automated test plan execution table');

export const SQUASH_AUTOM_TEST_CASES_TABLE_CONF =
  new InjectionToken<GridDefinition>('Grid config for Squash Autom test case list table');

export const SQUASH_AUTOM_TEST_CASES_TABLE =
  new InjectionToken<GridService>('Grid service instance for Squash Autom test case list table');

export const ITERATION_ISSUE_TABLE_CONF = new InjectionToken<GridDefinition>('Grid config for the issue table of iteration view');

export const ITERATION_ISSUE_TABLE = new InjectionToken<GridService>('Grid service instance for the issue table of iteration view');

export const ITERATION_AUTOMATED_SUITE_TABLE_CONF =
  new InjectionToken<GridDefinition>('Grid config for the iteration automated suite table of iteration view');

export const ITERATION_AUTOMATED_SUITE_TABLE =
  new InjectionToken<GridService>('Grid service instance for the iteration automated suite table of iteration view');

export const AUTOMATED_SUITE_EXECUTION_TABLE_CONF =
  new InjectionToken<GridDefinition>('Grid config for the automated suite execution table');

export const AUTOMATED_SUITE_EXECUTION_TABLE =
  new InjectionToken<GridService>('Grid service instance for the automated suite execution table');

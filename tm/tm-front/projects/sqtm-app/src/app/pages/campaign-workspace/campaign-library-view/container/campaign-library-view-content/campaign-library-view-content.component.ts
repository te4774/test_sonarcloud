import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {CampaignLibraryViewComponentData} from '../campaign-library-view/campaign-library-view.component';
import {CampaignLibraryViewService} from '../../service/campaign-library-view.service';

@Component({
  selector: 'sqtm-app-campaign-library-view-content',
  templateUrl: './campaign-library-view-content.component.html',
  styleUrls: ['./campaign-library-view-content.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignLibraryViewContentComponent implements OnInit, OnDestroy {

  private unsub$ = new Subject<void>();
  componentData$: Observable<CampaignLibraryViewComponentData>;

  constructor(private campaignLibraryViewService: CampaignLibraryViewService) {
    this.componentData$ = campaignLibraryViewService.componentData$;
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

}

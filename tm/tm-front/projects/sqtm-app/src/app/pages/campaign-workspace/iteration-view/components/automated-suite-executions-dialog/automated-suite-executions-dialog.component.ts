import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {
  dateTimeColumn,
  DialogConfiguration,
  DialogReference,
  executionStatusColumn,
  Extendable,
  Fixed,
  grid,
  GridDefinition,
  GridService,
  gridServiceFactory,
  indexColumn,
  ReferentialDataService,
  RestService,
  sortDate,
  StyleDefinitionBuilder,
  withLinkColumn
} from 'sqtm-core';
import {DatePipe} from '@angular/common';
import {AUTOMATED_SUITE_EXECUTION_TABLE, AUTOMATED_SUITE_EXECUTION_TABLE_CONF} from '../../iteration-view.constant';
import {filter, takeUntil} from 'rxjs/operators';
import {NavigationStart, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {
  automatedSuiteDatasetColumn
} from '../cell-renderers/automated-suite-exec-dataset/automated-suite-exec-dataset.component';


@Component({
  selector: 'sqtm-app-automated-suite-executions-dialog',
  templateUrl: './automated-suite-executions-dialog.component.html',
  styleUrls: ['./automated-suite-executions-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePipe,
    {
      provide: AUTOMATED_SUITE_EXECUTION_TABLE_CONF,
      useFactory: automatedSuiteExecutionTableDefinition
    },
    {
      provide: AUTOMATED_SUITE_EXECUTION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, AUTOMATED_SUITE_EXECUTION_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: AUTOMATED_SUITE_EXECUTION_TABLE
    }
  ]
})
export class AutomatedSuiteExecutionsDialogComponent implements OnInit, AfterViewInit, OnDestroy {

  conf: any;

  unsub$ = new Subject<void>();

  constructor(private dialogRef: DialogReference<DialogConfiguration>,
              private gridService: GridService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.conf = this.dialogRef.data;
    this.router.events
      .pipe(
        takeUntil(this.unsub$),
        filter(event => event instanceof NavigationStart)
      )
      .subscribe((event: NavigationStart) => {
        if (event.url.includes('/execution/')) {
          this.dialogRef.close();
        }
      });
  }

  ngAfterViewInit(): void {
    this.gridService.setServerUrl(['automated-suite', this.conf.suiteId, 'executions']);
  }

  ngOnDestroy(): void {
    this.unsub$.complete();
  }

}

export function automatedSuiteExecutionTableDefinition(): GridDefinition {
  return grid('automated-suite-execution')
    .withColumns([
      indexColumn()
        .withViewport('leftViewport'),
      executionStatusColumn('executionStatus')
        .withI18nKey('sqtm-core.entity.execution.status.label')
        .changeWidthCalculationStrategy(new Fixed(60)),
      withLinkColumn('name', {kind: 'link', baseUrl: '/execution', columnParamId: 'executionId'})
        .withI18nKey('sqtm-core.entity.name')
        .changeWidthCalculationStrategy(new Extendable(100, 1)),
      automatedSuiteDatasetColumn('datasetLabel')
        .withI18nKey('sqtm-core.entity.dataset.label.short')
        .withTitleI18nKey('sqtm-core.entity.dataset.label.singular')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5)),
      dateTimeColumn('lastExecutedOn')
        .withSortFunction(sortDate)
        .withI18nKey('sqtm-core.entity.execution-plan.last-execution.label.short-dot')
        .withTitleI18nKey('sqtm-core.entity.execution-plan.last-execution.label.long')
        .changeWidthCalculationStrategy(new Extendable(100, 0.5))
    ]).server()
    .disableRightToolBar()
    .withStyle(new StyleDefinitionBuilder()
      .enableInitialLoadAnimation()
      .showLines())
    .withRowHeight(35)
    .build();
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {StartExecutionComponent} from './start-execution.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {
  CampaignPermissions,
  convertSqtmLiteral,
  DataRow,
  DialogService,
  EntityViewService,
  grid,
  GRID_PERSISTENCE_KEY,
  GridDefinition,
  GridService,
  gridServiceFactory,
  GridWithStatePersistence,
  Permissions,
  ProjectDataMap,
  ReferentialDataService,
  RestService,
  SquashTmDataRowType
} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {OnPushComponentTester} from '../../../../../../utils/testing-utils/on-push-component-tester';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';
import {of} from 'rxjs';
import {IterationViewComponentData} from '../../../container/iteration-view/iteration-view.component';
import {mockGlobalConfiguration, mockIterationModel} from '../../../../../../utils/testing-utils/mocks.data';
import {IterationState} from '../../../state/iteration.state';
import {GENERIC_TEST_PLAN_VIEW_SERVICE} from '../../../../generic-test-plan-view-service';

class StartExecutionComponentTester extends OnPushComponentTester<StartExecutionComponent<any, any, any>> {
  getIcon() {
    return this.element('i');
  }
}

describe('StartExecutionComponent', () => {
  let fixture: ComponentFixture<StartExecutionComponent<any, any, any>>;
  let tester: StartExecutionComponentTester;

  const gridConfig = grid('grid-test').build();
  const restService = {};
  const dialogService = jasmine.createSpyObj('dialogService', ['create']);
  let iterationViewService = jasmine.createSpyObj('iterationViewService', ['load']);
  iterationViewService = {...iterationViewService, componentData$: of({milestonesAllowModification: true})};
  const genericTestPlanViewService = jasmine.createSpyObj('genericTestPlanViewService', ['getEntityReference']);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [StartExecutionComponent],
      imports: [AppTestingUtilsModule, TranslateModule.forRoot(), RouterTestingModule],
      providers: [
        {
          provide: GridDefinition,
          useValue: gridConfig
        }, {
          provide: RestService,
          useValue: restService
        }, {
          provide: GridService,
          useFactory: gridServiceFactory,
          deps: [RestService, GridDefinition, ReferentialDataService]
        }, {
          provide: DialogService,
          useValue: dialogService
        },
        {
          provide: EntityViewService,
          useValue: iterationViewService
        },
        {
          provide: GRID_PERSISTENCE_KEY,
          useValue: 'iteration-test-plan-grid'
        },
        {
          provide: GENERIC_TEST_PLAN_VIEW_SERVICE,
          useValue: genericTestPlanViewService,
        },
        GridWithStatePersistence
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartExecutionComponent);
    tester = new StartExecutionComponentTester(fixture);
    tester.componentInstance.row = convertSqtmLiteral({
      id: 1,
      data: {
        itemTestPlanId: 1,
        iterationId: 1
      },
      type: SquashTmDataRowType.IterationTestPlanItem,
      projectId: 1
    }, projectDataMap);
    fixture.detectChanges();
  });

  it('should show execute icon if user has execute permissions', () => {
    expect(tester.componentInstance).toBeTruthy();
    expect(tester.getIcon()).toBeTruthy();
    expect(tester.getIcon().attr('nztype')).toContain('play');
  });

  it('should allow execution', () => {
    tester.componentInstance.row = mockDataRow({});
    tester.detectChanges();

    expect(tester.componentInstance.canExecute(mockIterationViewComponentData())).toBeTruthy();
  });

  it('should apply various controls over execution rights based on row data', () => {
    // Deleted test case
    tester.componentInstance.row = mockDataRow({testCaseId: undefined});
    tester.detectChanges();
    expect(tester.componentInstance.canExecute(mockIterationViewComponentData())).toBeFalsy();

    // Bound to locked milestone
    tester.componentInstance.row = mockDataRow({});
    tester.detectChanges();
    expect(tester.componentInstance.canExecute(mockIterationViewComponentData({
      milestonesAllowModification: false
    }))).toBeFalsy();

    // Readonly project permissions
    tester.componentInstance.row = mockDataRow({}, 2);
    tester.detectChanges();
    expect(tester.componentInstance.canExecute(mockIterationViewComponentData({}, 2)))
      .toBeFalsy();
  });
});

function mockIterationViewComponentData(customData: any = {}, projectId = 1): IterationViewComponentData {
  return {
    globalConfiguration: mockGlobalConfiguration(),
    iteration: mockIterationModel() as unknown as IterationState,
    permissions: new CampaignPermissions(projectDataMap[projectId]),
    projectData: projectDataMap[projectId],
    type: 'iteration',
    uiState: undefined,
    milestonesAllowModification: true,
    ...customData
  };
}

function mockDataRow(customData: any, projectId = 1): DataRow {
  return convertSqtmLiteral({
    id: 1,
    data: {
      itemTestPlanId: 1,
      iterationId: 1,
      testCaseId: 1,
      ...customData
    },
    type: SquashTmDataRowType.IterationTestPlanItem,
    projectId,
  }, projectDataMap);
}

const defaultProjectData = {
  id: 1,
  name: 'Project 1',
  uri: '',
  label: '',
  testCaseNature: null,
  testCaseType: null,
  requirementCategory: null,
  allowAutomationWorkflow: false,
  customFieldBinding: null,
  permissions: {
    TEST_CASE_LIBRARY: [],
    CAMPAIGN_LIBRARY: [],
    REQUIREMENT_LIBRARY: [],
    PROJECT: [],
    PROJECT_TEMPLATE: [],
    CUSTOM_REPORT_LIBRARY: [],
    AUTOMATION_REQUEST_LIBRARY: [],
    ACTION_WORD_LIBRARY: []
  },
  bugTracker: null,
  milestones: [],
  taServer: null,
  automationWorkflowType: 'NATIVE',
  disabledExecutionStatus: [],
  keywords: [],
  bddScriptLanguage: 'ENGLISH',
  allowTcModifDuringExec: true,
  activatedPlugins: null
};

const projectDataMap: ProjectDataMap = {
  1: {
    ...defaultProjectData,
    permissions: {
      TEST_CASE_LIBRARY: [],
      CAMPAIGN_LIBRARY: [
        Permissions.READ,
        Permissions.WRITE,
        Permissions.CREATE,
        Permissions.ATTACH,
        Permissions.EXECUTE
      ],
      REQUIREMENT_LIBRARY: [],
      PROJECT: [],
      PROJECT_TEMPLATE: [],
      CUSTOM_REPORT_LIBRARY: [],
      AUTOMATION_REQUEST_LIBRARY: [],
      ACTION_WORD_LIBRARY: []
    }
  },
  // Project without permissions
  2: {
    ...defaultProjectData,
  }
};

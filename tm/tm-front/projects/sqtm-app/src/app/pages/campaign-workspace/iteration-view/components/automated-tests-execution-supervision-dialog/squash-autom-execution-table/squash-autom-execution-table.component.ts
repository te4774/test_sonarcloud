import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {
  DataRow,
  Extendable,
  grid,
  GridService,
  gridServiceFactory,
  pagination,
  ReferentialDataService,
  RestService,
  StyleDefinitionBuilder,
  textColumn
} from 'sqtm-core';
import {SQUASH_AUTOM_EXECUTION_TABLE, SQUASH_AUTOM_EXECUTION_TABLE_CONF} from '../../../iteration-view.constant';

function squashAutomExecutionTableDefinition() {
  return grid('squash-autom-automated-test-plan-execution')
    .withColumns([
      textColumn('name')
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .changeWidthCalculationStrategy(new Extendable(150, 0.5)),
      textColumn('automatedServerName')
        .withI18nKey('sqtm-core.entity.execution-server.label.short-dot')
        .changeWidthCalculationStrategy(new Extendable(150, 0.5))
    ])
    .withPagination(pagination().initialSize(10))
    .disableRightToolBar()
    .withStyle(new StyleDefinitionBuilder()
      .enableInitialLoadAnimation()
      .showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-squash-autom-execution-table',
  templateUrl: './squash-autom-execution-table.component.html',
  styleUrls: ['./squash-autom-execution-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: SQUASH_AUTOM_EXECUTION_TABLE_CONF,
      useFactory: squashAutomExecutionTableDefinition
    },
    {
      provide: SQUASH_AUTOM_EXECUTION_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, SQUASH_AUTOM_EXECUTION_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: SQUASH_AUTOM_EXECUTION_TABLE
    }
  ]
})
export class SquashAutomExecutionTableComponent implements OnInit {

  @Input()
  set dataRows(value: any[]) {
    if (value != null) {
      this._dataRows = value;
      this.gridService.loadInitialDataRows(this._dataRows, this._dataRows.length);
    };
  }

  _dataRows: Partial<DataRow>[] = [];

  constructor(private gridService: GridService) { }

  ngOnInit(): void {
  }

}

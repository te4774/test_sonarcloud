import {ComponentFixture, TestBed} from '@angular/core/testing';
import {AppTestingUtilsModule} from '../../../../../../utils/testing-utils/app-testing-utils.module';

import {SquashAutomTestCasesTableComponent} from './squash-autom-test-cases-table.component';

describe('SquashAutomTestCasesTableComponent', () => {
  let component: SquashAutomTestCasesTableComponent;
  let fixture: ComponentFixture<SquashAutomTestCasesTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SquashAutomTestCasesTableComponent ],
      imports: [ AppTestingUtilsModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SquashAutomTestCasesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

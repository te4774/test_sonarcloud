import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewContainerRef
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ColumnDefinitionBuilder,
  DataRow,
  DialogService,
  GridService,
  RestService
} from 'sqtm-core';
import {Observable, Subject} from 'rxjs';
import {TestSuiteViewComponentData} from '../../../containers/test-suite-view/test-suite-view.component';
import {TestSuiteViewService} from '../../../services/test-suite-view.service';
import {takeUntil} from 'rxjs/operators';
import {
  DeleteTestSuiteTestPlanItemConfiguration
} from '../../delete-test-suite-test-plan-item-dialog/delete-test-suite-test-plan-item-configuration';
import {
  DeleteTestSuiteTestPlanItemDialogComponent
} from '../../delete-test-suite-test-plan-item-dialog/delete-test-suite-test-plan-item-dialog.component';

@Component({
  selector: 'sqtm-app-delete-test-suite-test-plan-item',
  template: `
    <ng-container *ngIf="componentData$ | async as componentData">
      <ng-container *ngIf="row && componentData.milestonesAllowModification">
        <div *ngIf="canDelete(row)"
             class="full-height full-width flex-column icon-container current-workspace-main-color __hover_pointer"
             (click)="removeItem(row, componentData)">
          <i nz-icon [nzType]="getIcon(row)" nzTheme="outline" class="table-icon-size"></i>
        </div>
      </ng-container>
    </ng-container>
  `,
  styleUrls: ['./delete-test-suite-test-plan-item.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteTestSuiteTestPlanItemComponent extends AbstractCellRendererComponent implements OnInit, OnDestroy {

  unsub$ = new Subject<void>();

  componentData$: Observable<TestSuiteViewComponentData>;

  constructor(public grid: GridService, cdr: ChangeDetectorRef,
              private dialogService: DialogService,
              private restService: RestService,
              private  vcr: ViewContainerRef,
              private testSuiteViewService: TestSuiteViewService) {
    super(grid, cdr);
    this.componentData$ = this.testSuiteViewService.componentData$;
  }

  ngOnInit(): void {
  }

  getIcon(row: DataRow): string {
    return this.rowHasExecution(row) ? 'sqtm-core-generic:delete' : 'sqtm-core-generic:unlink';
  }

  removeItem(row: DataRow, componentData: TestSuiteViewComponentData) {
    const dialogReference = this.dialogService.openDialog<DeleteTestSuiteTestPlanItemConfiguration, boolean>({
      id: 'delete-test-suite-tpi',
      component: DeleteTestSuiteTestPlanItemDialogComponent,
      viewContainerReference: this.vcr,
      data: {
        id: 'delete-test-suite-tpi',
        titleKey: 'sqtm-core.campaign-workspace.test-suite.test-plan.title.singular',
        messageKey: this.rowHasExecution(row) ?
          'sqtm-core.campaign-workspace.test-suite.test-plan.message.has-executions.singular' :
          'sqtm-core.campaign-workspace.test-suite.test-plan.message.no-executions.singular',
        level: this.rowHasExecution(row) ? 'DANGER' : 'WARNING',
        testSuiteId: componentData.testSuite.id,
        itemTestPlanIds: [row.data['itemTestPlanId']]
      },
      width: 600
    });

    dialogReference.dialogClosed$
      .pipe(
        takeUntil(this.unsub$)
      )
      .subscribe(() => {
        this.grid.refreshData();
      });
  }

  private rowHasExecution(row: DataRow): boolean {
    return row.data['lastExecutedOn'] != null;
  }

  canDelete(row: DataRow): boolean {
    return row.simplePermissions && (this.rowHasExecution(row) ?
      row.simplePermissions.canExtendedDelete :
      row.simplePermissions.canDelete);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

export function deleteTestSuiteTestPlanItemColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(DeleteTestSuiteTestPlanItemComponent);
}

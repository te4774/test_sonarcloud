import {TestBed, waitForAsync} from '@angular/core/testing';
import {CampaignTestPlanOperationHandler} from './campaign-test-plan-operation-handler';
import {AppTestingUtilsModule} from '../../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {CampaignViewService} from './campaign-view.service';
import {mockGridService} from '../../../../utils/testing-utils/mocks.service';
import {of} from 'rxjs';
import {GridState} from 'sqtm-core';

describe('CampaignTestPlanOperationHandler', () => {
  const campaignViewService = jasmine.createSpyObj(['changeItemsPosition']);

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), AppTestingUtilsModule, HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: CampaignViewService, useValue: campaignViewService},
        CampaignTestPlanOperationHandler
      ]
    }).compileComponents();
  }));

  it('should be created', () => {
    const service: CampaignTestPlanOperationHandler = TestBed.inject(CampaignTestPlanOperationHandler);
    expect(service).toBeTruthy();
  });

  it('should handle internal reordering', () => {
    const service: CampaignTestPlanOperationHandler = TestBed.inject(CampaignTestPlanOperationHandler);
    service.grid = mockGridService();
    expect(service).toBeTruthy();
    service.grid.isSortedOrFiltered$ = of(false);

    campaignViewService.changeItemsPosition.and.returnValue(of({}));

    service.grid.gridState$ = of({
      dataRowState: {
        ids: [2, 5, 1, 3],
      },
      uiState: {
        dragState: {
          draggedRowIds: [3, 5],
          dragging: true,
          currentDndTarget: {
            id: 1,
            zone: 'bellow',
          }
        }
      }
    } as GridState);

    service.notifyInternalDrop();

    expect(campaignViewService.changeItemsPosition).toHaveBeenCalledWith([3, 5], 2);
  });
});

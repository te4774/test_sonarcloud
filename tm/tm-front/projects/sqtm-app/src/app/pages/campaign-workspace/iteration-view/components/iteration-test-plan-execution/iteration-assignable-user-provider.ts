import {ResearchColumnPrototype, UserHistorySearchProvider, UserListElement} from 'sqtm-core';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {IterationViewService} from '../../services/iteration-view.service';
import {Injectable} from '@angular/core';

@Injectable()
export class IterationAssignableUsersProvider extends UserHistorySearchProvider {

  constructor(private iterationViewService: IterationViewService) {
    super();
  }

  provideUserList(columnPrototype: ResearchColumnPrototype): Observable<UserListElement[]> {
    return this.iterationViewService.componentData$.pipe(
      take(1),
      map(data => data.iteration.users.map(u => ({...u, selected: false})))
    );
  }
}

import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {
  DataRow,
  Fixed,
  grid,
  GridService,
  gridServiceFactory,
  indexColumn,
  Limited,
  pagination,
  ReferentialDataService,
  RestService,
  StyleDefinitionBuilder,
  textColumn
} from 'sqtm-core';
import {SQUASH_AUTOM_TEST_CASES_TABLE, SQUASH_AUTOM_TEST_CASES_TABLE_CONF} from '../../../iteration-view.constant';

function squashAutomTestCaseTableDefinition() {
  return grid('squash-autom-test-case-list')
    .withColumns([
      indexColumn()
        .changeWidthCalculationStrategy(new Fixed(70)),
      textColumn('reference')
        .withI18nKey('sqtm-core.entity.generic.reference.label')
        .changeWidthCalculationStrategy(new Limited(150)),
      textColumn('name')
        .withI18nKey('sqtm-core.entity.test-case.label.singular')
        .changeWidthCalculationStrategy(new Limited(150))
    ])
    .withPagination(pagination().initialSize(10))
    .disableRightToolBar()
    .withStyle(
      new StyleDefinitionBuilder()
        .enableInitialLoadAnimation()
        .switchOffSelectedRows()
        .switchOffHoveredRows()
        .showLines())
    .withRowHeight(35)
    .build();
}

@Component({
  selector: 'sqtm-app-squash-autom-test-cases-table',
  templateUrl: './squash-autom-test-cases-table.component.html',
  styleUrls: ['./squash-autom-test-cases-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: SQUASH_AUTOM_TEST_CASES_TABLE_CONF,
      useFactory: squashAutomTestCaseTableDefinition
    },
    {
      provide: SQUASH_AUTOM_TEST_CASES_TABLE,
      useFactory: gridServiceFactory,
      deps: [RestService, SQUASH_AUTOM_TEST_CASES_TABLE_CONF, ReferentialDataService]
    },
    {
      provide: GridService,
      useExisting: SQUASH_AUTOM_TEST_CASES_TABLE
    }
  ]
})
export class SquashAutomTestCasesTableComponent implements OnInit {

  @Input()
  set dataRows(value: any[]) {
    if (value != null) {
      this._dataRows = value;
      this.gridService.loadInitialDataRows(this._dataRows, this._dataRows.length);
    }
  }
  _dataRows: Partial<DataRow>[] = [];

  constructor(private gridService: GridService) { }

  ngOnInit(): void {
  }

}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  ViewContainerRef
} from '@angular/core';
import {
  AbstractCellRendererComponent,
  ActionErrorDisplayService,
  AutomatedSuitePreview,
  AutomatedSuiteService,
  ColumnDefinitionBuilder,
  DialogConfiguration,
  DialogService,
  EntityReference,
  EntityViewService,
  GridService,
  GridWithStatePersistence,
  IterationService,
  SimplePermissions,
  SqtmEntityState
} from 'sqtm-core';
import {iterationViewLogger} from '../../../iteration-view.logger';
import {Router} from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';
import {BehaviorSubject, Subject} from 'rxjs';
import {
  AutomatedTestsExecutionSupervisionDialogComponent
} from '../../automated-tests-execution-supervision-dialog/automated-tests-execution-supervision-dialog.component';
import {ExecutionHistoryComponent} from '../../execution-history/execution-history.component';
import {catchError, filter, finalize, switchMap, takeUntil} from 'rxjs/operators';
import {IterationViewComponentData} from '../../../container/iteration-view/iteration-view.component';
import {
  ExecutionRunnerOpenerService
} from '../../../../../execution/execution-runner/services/execution-runner-opener.service';
import {GENERIC_TEST_PLAN_VIEW_SERVICE, GenericTestPlanViewService} from '../../../../generic-test-plan-view-service';

const logger = iterationViewLogger.compose('StartExecutionComponent');

@Component({
  selector: 'sqtm-app-start-execution',
  template: `
    <ng-container *ngIf="viewService.componentData$ | async as componentData">
      <div class="full-height full-width flex-column icon-container current-workspace-main-color"
           [attr.data-test-button-id]="'startExecution'"
           (click)="openExecutionMenu()"
           nz-popover
           [nzPopoverVisible]="menuVisible$|async"
           [nzPopoverContent]="contentTemplate"
           [nzPopoverTrigger]="'click'"
           [nzPopoverPlacement]="'left'">
        <i nz-icon nzType="sqtm-core-campaign:play" nzTheme="outline" class="table-icon-size"></i>
      </div>
      <ng-template #contentTemplate>
        <a *ngIf="row.data.executionMode === 'AUTOMATED'" class="launch-execution-link m-b-10"
           (click)="startAutomatedExecution(componentData)"
           [class.disabled]="! canExecute(componentData)">
          {{'sqtm-core.campaign-workspace.test-plan.launch-execution.automated'|translate}}
        </a>
        <a class="launch-execution-link"
           (click)="startExecutionInDialog(componentData)"
           [class.disabled]="! canExecute(componentData)">
          {{'sqtm-core.campaign-workspace.test-plan.launch-execution.manual'|translate}}</a>
        <a class="launch-execution-link m-t-10"
           [attr.data-test-link-id]="'manual-execution'"
           [class.disabled]="! canExecute(componentData)"
           (click)="startExecutionInPage(componentData)">
          {{'sqtm-core.campaign-workspace.test-plan.launch-execution.page'|translate}}</a>
        <a class="launch-execution-link m-t-10"
           [attr.data-test-link-id]="'show-execution-history'"
           (click)="displayExecutionHistory()">
          {{'sqtm-core.campaign-workspace.test-plan.launch-execution.history'|translate}}</a>
      </ng-template>
    </ng-container>
  `,
  styleUrls: ['./start-execution.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{provide: IterationService, useClass: IterationService}]
})
// tslint:disable-next-line:max-line-length
export class StartExecutionComponent<S extends SqtmEntityState, T extends string, P extends SimplePermissions> extends AbstractCellRendererComponent implements OnDestroy {

  menuVisible$ = new BehaviorSubject<boolean>(false);

  unsub$ = new Subject<void>();

  private _startingAutomatedExecution$ = new BehaviorSubject<boolean>(false);

  constructor(grid: GridService,
              protected dialogService: DialogService,
              public cdRef: ChangeDetectorRef,
              private viewContainerRef: ViewContainerRef,
              private iterationService: IterationService,
              private automatedSuiteService: AutomatedSuiteService,
              private router: Router,
              @Inject(APP_BASE_HREF) private baseUrl: string,
              public readonly viewService: EntityViewService<S, T, P>,
              private actionErrorDisplayService: ActionErrorDisplayService,
              private gridWithStatePersistence: GridWithStatePersistence,
              private executionRunnerOpenerService: ExecutionRunnerOpenerService,
              @Inject(GENERIC_TEST_PLAN_VIEW_SERVICE) private genericTestPlanViewService: GenericTestPlanViewService) {
    super(grid, cdRef);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  canExecute(componentData: IterationViewComponentData) {
    const hasTestCase = Boolean(this.row.data['testCaseId']);
    return componentData.permissions.canExecute
      && componentData.milestonesAllowModification
      && hasTestCase
      && ! this._startingAutomatedExecution$.getValue();
  }

  openExecutionMenu() {
    this.menuVisible$.next(true);
  }

  startExecutionInDialog(componentData: IterationViewComponentData) {
    if (!this.canExecute(componentData)) {
      return;
    }

    const iterationId = this.row.data.iterationId;
    const testPlanItemId = this.row.data.itemTestPlanId;
    logger.debug(`Try to start execution in iteration ${iterationId} for itpi ${testPlanItemId}`);
    if (iterationId && testPlanItemId) {
      this.persistExecutionAndOpenDialog(iterationId, testPlanItemId);
    } else {
      throw Error(`Unable to start execution in iteration ${iterationId} for itpi ${testPlanItemId}`);
    }
  }

  private persistExecutionAndOpenDialog(iterationId, testPlanItemId) {
    this.iterationService.persistManualExecution(iterationId, testPlanItemId).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err))
    ).subscribe(executionId => {
      logger.debug(`Successfully created execution ${executionId}`);
      this.menuVisible$.next(false);
      this.grid.refreshData();
      this.executionRunnerOpenerService.openExecutionPrologue(executionId);
    });
  }

  startAutomatedExecution(componentData: IterationViewComponentData) {
    if (!this.canExecute(componentData)) {
      return;
    }

    this._startingAutomatedExecution$.next(true);
    this.menuVisible$.next(false);

    const itemIdAsArray: number[] = [this.row.data.itemTestPlanId];
    this.automatedSuiteService.updateTaScriptsForItems(itemIdAsArray).pipe(
      switchMap(() => this.genericTestPlanViewService.getEntityReference()),
      switchMap((entityRef: EntityReference) => this.automatedSuiteService.generateAutomatedSuitePreview({
        context: entityRef,
        testPlanSubsetIds: itemIdAsArray
      })),
      finalize(() => this._startingAutomatedExecution$.next(false))
).subscribe((preview: AutomatedSuitePreview) => this.openAutomatedExecutionSupervisionDialog(preview));
  }

  private openAutomatedExecutionSupervisionDialog(data: AutomatedSuitePreview) {
    const automatedExecutionDialog = this.dialogService.openDialog({
      id: 'automated-tests-execution-supervision',
      viewContainerReference: this.viewContainerRef,
      component: AutomatedTestsExecutionSupervisionDialogComponent,
      data,
      height: 600,
      width: 800
    });
    automatedExecutionDialog.dialogClosed$.pipe(
      filter(result => Boolean(result)),
      switchMap(() => this.genericTestPlanViewService.incrementAutomatedSuiteCount())
    ).subscribe(() => this.grid.refreshData());
  }

  startExecutionInPage(componentData: IterationViewComponentData) {
    if (!this.canExecute(componentData)) {
      return;
    }

    const iterationId = this.row.data.iterationId;
    const testPlanItemId = this.row.data.itemTestPlanId;
    logger.debug(`Try to start execution in iteration ${iterationId} for itpi ${testPlanItemId}`);
    if (iterationId && testPlanItemId) {
      this.persistExecutionAndGoToExecutionPage(iterationId, testPlanItemId);
    } else {
      throw Error(`Unable to start execution in iteration ${iterationId} for itpi ${testPlanItemId}`);
    }
  }

  displayExecutionHistory() {
    const iterationId = this.row.data.iterationId;
    const testPlanItemId = this.row.data.itemTestPlanId;

    const displayTestPlanExecutionHistoryDialogConfiguration: DialogConfiguration<DisplayExecutionsDialogConf> = {
      id: 'import',
      viewContainerReference: this.viewContainerRef,
      component: ExecutionHistoryComponent,
      width: 1010,
      data: {
        iterationId,
        testPlanItemId
      }
    };

    this.menuVisible$.next(false);
    const dialogReference = this.dialogService.openDialog(displayTestPlanExecutionHistoryDialogConfiguration);

    dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(() => {
      this.grid.refreshData();
    });
  }

  private persistExecutionAndGoToExecutionPage(iterationId: number, testPlanItemId: number) {
    this.iterationService.persistManualExecution(iterationId, testPlanItemId).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err))
    ).subscribe(executionId => {
      logger.debug(`Successfully created execution ${executionId}`);
      this.gridWithStatePersistence.saveSnapshot();
      this.router.navigate(['execution', executionId]);
    });
  }
}

export function startExecutionColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(StartExecutionComponent);
}

export interface DisplayExecutionsDialogConf {
  iterationId: number;
  testPlanItemId: number;
}

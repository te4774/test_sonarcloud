import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {DialogReference, DialogService} from 'sqtm-core';
import {IterationPlanningDialogConfiguration} from './iteration-planning-dialog.configuration';
import {FormControl, FormGroup} from '@angular/forms';
import * as datefns from 'date-fns';

@Component({
  selector: 'sqtm-app-iteration-planning-dialog',
  templateUrl: './iteration-planning-dialog.component.html',
  styleUrls: ['./iteration-planning-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IterationPlanningDialogComponent implements OnInit {

  SCHEDULED_START_DATE_SUFFIX = '-scheduledStartDate';
  SCHEDULED_END_DATE_SUFFIX = '-scheduledEndDate';

  conf: IterationPlanningDialogConfiguration;
  formGroup: FormGroup;

  constructor(private dialogRef: DialogReference<IterationPlanningDialogConfiguration>,
              private dialogService: DialogService,
              private cdr: ChangeDetectorRef) {
    this.conf = this.dialogRef.data;
  }

  ngOnInit(): void {
    const formControls = this.conf.iterationPlannings.reduce((controls, iterationPlanning) => {
      const iterationName = iterationPlanning.name;
      const scheduledEndDate = iterationPlanning.scheduledEndDate;
      const scheduledStartDate = iterationPlanning.scheduledStartDate;
      const scheduledEndDateFormControl = new FormControl(scheduledEndDate);

      const scheduledStartDateFormControl = new FormControl(scheduledStartDate);
      controls[`${iterationName}${this.SCHEDULED_END_DATE_SUFFIX}`] = scheduledEndDateFormControl;
      controls[`${iterationName}${this.SCHEDULED_START_DATE_SUFFIX}`] = scheduledStartDateFormControl;
      return controls;
    }, {});

    this.formGroup = new FormGroup(formControls);
    const validators = this.conf.iterationPlannings.map(iterationPlanning => {
      return validateDate(`${iterationPlanning.name}${this.SCHEDULED_START_DATE_SUFFIX}`,
        `${iterationPlanning.name}${this.SCHEDULED_END_DATE_SUFFIX}`);
    });

    this.formGroup.setValidators(validators);

  }

  confirm() {
    const newList = [];
    if (this.formGroup.valid) {
      this.conf.iterationPlannings.forEach(iterPlanning => {
        const scheduledEndDate = this.ensureDateFormat(iterPlanning.name, this.SCHEDULED_END_DATE_SUFFIX);
        const scheduledStartDate = this.ensureDateFormat(iterPlanning.name, this.SCHEDULED_START_DATE_SUFFIX);
        newList.push({...iterPlanning, scheduledEndDate, scheduledStartDate});
      });
      this.dialogRef.result = newList;
      this.dialogRef.close();
    } else {
      this.dialogService.openAlert({
        level: 'DANGER',
        messageKey: 'sqtm-core.validation.errors.time-period-not-consistent'
      });
      this.cdr.markForCheck();
    }

  }

  private ensureDateFormat(iterPlanningName: string, suffix: string) {
    let date = this.formGroup.value[iterPlanningName + suffix] as Date;
    if (date) {
      date = datefns.setHours(date, 0);
      date = datefns.setMinutes(date, 0);
      date = datefns.setSeconds(date, 0);
      date = datefns.setMilliseconds(date, 0);
    }
    return date;
  }
}

function validateDate(startDateFieldName: string, fieldNameEndDate: string): any {
  return (formGroup: FormGroup) => {
    const startDateControl = formGroup.controls[startDateFieldName];

    const endDateControl = formGroup.controls[fieldNameEndDate];
    if (startDateControl.value != null && endDateControl.value != null) {
      const startDate = new Date(startDateControl.value);
      const endDate = new Date(endDateControl.value);
      if (startDate > endDate) {
        startDateControl.setErrors({rangeError: true});
      } else {
        startDateControl.setErrors(null);
      }
      if (endDate < startDate) {
        endDateControl.setErrors({rangeError: true});
      } else {
        endDateControl.setErrors(null);
      }
    }
    return null;
  };
}

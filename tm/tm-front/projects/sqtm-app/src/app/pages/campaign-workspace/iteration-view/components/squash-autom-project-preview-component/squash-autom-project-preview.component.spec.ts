import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormControl, FormGroup} from '@angular/forms';
import {TestingUtilsModule} from '../../../../../../../../sqtm-core/src/lib/ui/testing-utils/testing-utils.module';

import {SquashAutomProjectPreviewComponent} from './squash-autom-project-preview.component';

describe('SquashAutomProjectPreviewComponentComponent', () => {
  let component: SquashAutomProjectPreviewComponent;
  let fixture: ComponentFixture<SquashAutomProjectPreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TestingUtilsModule],
      declarations: [ SquashAutomProjectPreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SquashAutomProjectPreviewComponent);
    component = fixture.componentInstance;
    component.formGroup = new FormGroup({
      name: new FormControl('tags')
    });
    component.squashAutomProject = {
      projectId: 1,
      projectName: 'myProject',
      serverId: 1,
      serverName: 'local orchestrator',
      testCases: [{
        reference: 'TC1',
        name: 'Test Case 1'
      }]
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

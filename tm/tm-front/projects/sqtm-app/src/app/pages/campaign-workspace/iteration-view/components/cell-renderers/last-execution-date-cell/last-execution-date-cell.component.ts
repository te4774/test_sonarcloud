import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ColumnDefinitionBuilder, DateTimeCellRendererComponent, GridService, GridWithStatePersistence} from 'sqtm-core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'sqtm-app-last-execution-date-cell',
  template: `
    <ng-container *ngIf="row">
      <div class="full-width full-height flex-column">
        <a *ngIf="canClick; else cantClick" style="margin: auto 5px;"
           class="txt-ellipsis"
           [routerLink]="['/','execution', latestExecutionId]"
           (click)="saveTestPlanGridState()"
        >
          {{displayString}}
        </a>

        <ng-template #cantClick>
          <span style="margin: auto 5px;"
                class="txt-ellipsis">
            {{displayString}}
          </span>
        </ng-template>
      </div>
    </ng-container>`,
  styleUrls: ['./last-execution-date-cell.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LastExecutionDateCellComponent extends DateTimeCellRendererComponent implements OnInit {

  constructor(grid: GridService,
              public cdRef: ChangeDetectorRef,
              private router: Router,
              public translateService: TranslateService,
              public datePipe: DatePipe,
              private gridWithStatePersistence: GridWithStatePersistence) {
    super(grid, cdRef, translateService, datePipe);
  }

  ngOnInit(): void {
  }

  get latestExecutionId() {
    return this.row.data['latestExecutionId'];
  }

  get canClick(): boolean {
    const lastExecutedOn = this.row.data['lastExecutedOn'];
    return this.latestExecutionId && lastExecutedOn;
  }

  goToExecutionPage() {
    const latestExecutionId = this.row.data['latestExecutionId'];
    const lastExecutedOn = this.row.data['lastExecutedOn'];
    if (latestExecutionId && lastExecutedOn) {
      this.router.navigate(['execution', latestExecutionId]);
    }
  }

  saveTestPlanGridState() {
    this.gridWithStatePersistence.saveSnapshot();
  }
}

/*
 * Warning! This cell renderer require a GridWithStatePersistence in the dep tree.
 */
export function lastExecutionDateColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withRenderer(LastExecutionDateCellComponent);
}


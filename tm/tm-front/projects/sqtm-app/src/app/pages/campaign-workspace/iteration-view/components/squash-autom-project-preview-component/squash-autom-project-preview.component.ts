import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {
  AutomationEnvironmentTagHolder,
  BoundEnvironmentVariable,
  DataRow,
  SquashAutomProjectPreview
} from 'sqtm-core';
import {
  EnvironmentSelectionPanelComponent
} from '../../../../../components/automated-execution-environment/components/environment-selection-panel/environment-selection-panel.component';
import {
  AutomatedExecutionEnvironmentVariablePanelComponent
} from '../../../../../components/automated-execution-environment-variable/components/automated-execution-environment-variable-panel/automated-execution-environment-variable-panel.component';

@Component({
  selector: 'sqtm-app-squash-autom-project-preview',
  templateUrl: './squash-autom-project-preview.component.html',
  styleUrls: ['./squash-autom-project-preview.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SquashAutomProjectPreviewComponent implements OnInit {

  @Input()
  squashAutomProject: SquashAutomProjectPreview;

  @Input()
  formGroup: FormGroup;

  @ViewChild(EnvironmentSelectionPanelComponent)
  environmentPanel: EnvironmentSelectionPanelComponent;

  @ViewChild(AutomatedExecutionEnvironmentVariablePanelComponent)
  environmentVariablePanel: AutomatedExecutionEnvironmentVariablePanelComponent;

  formControl: FormControl;

  public readonly tagHolderType = AutomationEnvironmentTagHolder.EXECUTION_DIALOG;

  squashAutomTestCaseDataRows: Partial<DataRow>[];

  constructor() {
    this.formControl = new FormControl('');
  }

  ngOnInit(): void {
    this.formGroup.addControl(this.squashAutomProject.projectId.toString(), this.formControl);
    this.squashAutomTestCaseDataRows = this.squashAutomProject.testCases
      .map((testCase, index) => ({id: index, data: testCase}));
  }

  updateSelectedTagsValue(tags: string[]) {
    this.formControl.setValue(tags);
  }

  areAllTestsLaunchable(): Observable<boolean> {
    return this.environmentPanel.isOneEnvironmentAvailable();
  }

  getNamespacesOfSelectedEnvironments(): Observable<{ projectId: number, namespaces: string[] }> {
    return this.environmentPanel.getNamespacesOfSelectedEnvironments().pipe(
      map(namespaces => ({
        projectId: this.squashAutomProject.projectId,
        namespaces: namespaces
      })));
  }

  getBoundEnvironmentVariables(): Observable<BoundEnvironmentVariable[]> {
    return this.environmentVariablePanel.getBoundEnvironmentVariables();
  }
}

import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {
  AdminReferentialDataService,
  DialogService,
  LicenseInformationMessageProvider,
  LicenseMessagePlacement,
  LOGIN_PAGE_REDIRECT_AFTER_AUTH,
  SquashPlatformNavigationService
} from 'sqtm-core';
import {filter, map, take, withLatestFrom} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, ParamMap} from '@angular/router';

@Component({
  selector: 'sqtm-app-information-page',
  templateUrl: './information-page.component.html',
  styleUrls: ['./information-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InformationPageComponent implements OnInit {

  constructor(private readonly adminReferentialDataService: AdminReferentialDataService,
              private readonly activatedRoute: ActivatedRoute,
              private readonly platformNavigationService: SquashPlatformNavigationService,
              private readonly translateService: TranslateService,
              private readonly dialogService: DialogService) {
  }

  ngOnInit(): void {
    this.adminReferentialDataService.licenseInformation$.pipe(
      map(info => new LicenseInformationMessageProvider(info, this.translateService)),
      map(provider => provider.getLongMessage(LicenseMessagePlacement.ON_LOGGED_IN, true)),
      filter(msg => Boolean(msg)),
      take(1),
    ).subscribe((message) => this.showAlert(message));

    this.adminReferentialDataService.refresh().subscribe();
  }

  private showAlert(content: string): void {
    const dialogRef = this.dialogService.openAlert({
      messageKey: content,
      titleKey: 'sqtm-core.license.title',
      id: 'license-information-detail',
      level: null,
    }, 650);

    dialogRef.dialogClosed$.pipe(
      withLatestFrom(this.activatedRoute.queryParamMap)
    ).subscribe(([, params]) => {
      this.redirect(params);
    });
  }

  private redirect(params): void {
    const url = this.getRedirectUrl(params);
    this.platformNavigationService.navigateFromMainApplication(url);
  }

  private getRedirectUrl(paramMap: ParamMap) {
    return paramMap.has(LOGIN_PAGE_REDIRECT_AFTER_AUTH) ? paramMap.get(LOGIN_PAGE_REDIRECT_AFTER_AUTH) : '/home-workspace';
  }
}

import {RemoteIssueService} from '../../services/remote-issue.service';
import {RemoteIssue} from 'sqtm-core';
import {RemoteIssueDialogState} from '../../states/remote-issue-dialog.state';
import {ChangeDetectorRef, Directive} from '@angular/core';
import {finalize, switchMap} from 'rxjs/operators';
import {Observable, of, throwError} from 'rxjs';
import {FormBuilder, FormGroup} from '@angular/forms';
import {RemoteAttachmentFieldComponent} from '../widgets/remote-attachment-field/remote-attachment-field.component';

// This interface is used to monkey patch AbstractFormFields and RemoteIssueWidgetComponents.
export interface ServerSideErrorHandler {
  showClientSideError();
}

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractRemoteIssueForm {

  abstract get widgetsWithErrorHandling(): Iterable<ServerSideErrorHandler>;

  abstract get attachmentFields(): RemoteAttachmentFieldComponent[];

  abstract remoteIssueDialogState: Readonly<RemoteIssueDialogState>;
  abstract formGroup: FormGroup;

  asyncMode = false;

  protected constructor(public readonly remoteIssueService: RemoteIssueService,
                        public readonly cdRef: ChangeDetectorRef,
                        public readonly fb: FormBuilder) {
  }

  clearForm(): void {
    this.formGroup.reset();
  }

  submitForm(): Observable<any> {
    if (this.checkFormValidity()) {
      this.beginAsync();

      return this.getNewRemoteIssue().pipe(
        switchMap(remoteIssue => this.remoteIssueService.submitIssue(remoteIssue)),
        switchMap((submitResponse) => this.submitAttachments(submitResponse.issueId)),
        finalize(() => this.endAsync()));
    } else {
      // This error is only propagated so that the dialog doesn't get closed.
      return throwError('Invalid form.');
    }
  }

  protected checkFormValidity(): boolean {
    if (this.formGroup.valid) {
      return true;
    } else {
      this.showClientSideErrors();
      return false;
    }
  }

  protected showClientSideErrors() {
    for (const widget of this.widgetsWithErrorHandling) {
      widget.showClientSideError();
    }
  }

  protected abstract getNewRemoteIssue(): Observable<RemoteIssue>;

  protected beginAsync(): void {
    this.asyncMode = true;
    this.cdRef.detectChanges();
  }

  protected endAsync(): void {
    this.asyncMode = false;
    this.cdRef.detectChanges();
  }

  protected resetErrorMessage(): void {
    this.remoteIssueService.clearError();
  }

  private submitAttachments(issueId: string): Observable<any> {
    if (this.attachmentFields.length > 0) {
      const files = this.attachmentFields.reduce((acc, curr) => [...acc, ...curr.addedFiles], []);
      return this.remoteIssueService.submitAttachments(files, issueId);
    }

    return of(null);
  }
}


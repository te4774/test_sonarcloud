import {TestBed} from '@angular/core/testing';

import {RemoteIssueService} from './remote-issue.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';
import {AdvancedIssue, Field, IssueBindableEntity, RemoteIssue, RestService} from 'sqtm-core';
import {mockRestService} from '../../../utils/testing-utils/mocks.service';
import {Observable, of} from 'rxjs';
import {switchMap, withLatestFrom} from 'rxjs/operators';
import {mockAdvancedIssueCreateModel, mockBTIssueCreateModel} from './remote-issue.service.spec-data';
import SpyObj = jasmine.SpyObj;

describe('RemoteIssueService', () => {
  let service: RemoteIssueService;
  let restService: SpyObj<RestService>;

  beforeEach(() => {
    restService = mockRestService();

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [
        {provide: RemoteIssueService, useClass: RemoteIssueService},
        {provide: RestService, useValue: restService},
      ],
    });
    service = TestBed.inject(RemoteIssueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load initial state', () => {
    const initialIssue = mockBTIssueCreateModel();

    loadDefaultInitialState(initialIssue).pipe(
      withLatestFrom(service.reportForm$),
    ).subscribe(([, remoteIssue]) => expect(remoteIssue).toEqual(initialIssue));
  });

  it('should send a delegate command', () => {
    const initialIssue = mockBTIssueCreateModel();
    loadDefaultInitialState(initialIssue).subscribe();

    restService.post.and.returnValues(of('returnValue'));
    const arg = {toto: 'titi'};
    service.sendDelegateCommand('command', arg).subscribe((commandReturn) => {
      expect(restService.post).toHaveBeenCalledWith(['issues', 'mantis-pcktest2', 'command'], {
        command: 'command',
        argument: arg,
        context: {
          reportForm: initialIssue,
          remoteProject: 'P1',
        },
      });

      expect(commandReturn).toEqual('returnValue');
    });
  });

  it('should change current scheme', () => {
    const field: Field = {
      possibleValues: [],
      id: 'issuetype',
      label: 'label',
      rendering: null,
    };

    loadDefaultInitialState(mockAdvancedIssueCreateModel()).subscribe();

    service.changeSchemeAndCommit(field, 'Nouvelle fonctionnalité')
      .pipe(withLatestFrom(service.reportForm$))
      .subscribe(([, issue]) => {
        expect((issue as AdvancedIssue).currentScheme).toEqual('issuetype:Nouvelle fonctionnalité');
      });
  });

  describe('should submit form', () => {
    const standardIssue = mockBTIssueCreateModel();
    const advancedIssue = mockAdvancedIssueCreateModel();
    // TODO OSLC issue

    [
      {
        name: 'Exec step, standard issue',
        inputs: {type: 'EXECUTION_STEP_TYPE', issue: standardIssue},
        expectedUrl: ['issues', 'execution-step', '12', 'new-issue']
      },
      {
        name: 'Execution, advanced issue',
        inputs: {type: 'EXECUTION_TYPE', issue: advancedIssue},
        expectedUrl: ['issues', 'execution', '12', 'new-advanced-issue']
      },
    ].forEach(dataSet => {
      it(dataSet.name, () => {
        loadDefaultInitialState(dataSet.inputs.issue, dataSet.inputs.type).pipe(
          switchMap(() => service.submitIssue(dataSet.inputs.issue))
        ).subscribe(() => {
          expect(restService.post).toHaveBeenCalledWith(dataSet.expectedUrl, dataSet.inputs.issue);
        });
      });
    });
  });

  function loadDefaultInitialState(initialIssue: RemoteIssue, bindableEntityType = 'EXECUTION_STEP_TYPE'): Observable<any> {
    const projectNames = ['P1', 'P2'];
    restService.get.and.returnValues(of({projectNames}), of(initialIssue));

    return service.loadInitialState({
      bugTrackerId: 1,
      squashProjectId: 12,
      bindableEntity: bindableEntityType as IssueBindableEntity,
      boundEntityId: 12,
      attachMode: false,
    });
  }
});

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, ViewChild} from '@angular/core';
import {
  arraysAreSame,
  CascadingSelectOption,
  ChangeSet,
  DatePickerComponent,
  DialogService,
  DisplayOption,
  Field,
  FieldValue,
  isAppendPossibleValuesChange,
  isReplacePossibleValuesChange,
  isReplaceValueChange,
  RichTextFieldComponent,
  SelectFieldComponent,
  TextAreaFieldComponent,
  TextFieldComponent
} from 'sqtm-core';
import {FormControl, FormGroup} from '@angular/forms';
import {FieldToWidgetHelper} from '../../forms/field-to-widget.helper';
import {
  RemoteAutocompleteTextFieldComponent
} from '../remote-autocomplete-text-field/remote-autocomplete-text-field.component';
import {RemoteAttachmentFieldComponent} from '../remote-attachment-field/remote-attachment-field.component';
import {concatMap, distinctUntilChanged, filter, takeUntil} from 'rxjs/operators';
import {Subject, Subscription} from 'rxjs';
import {RemoteIssueService} from '../../../services/remote-issue.service';
import {RemoteCommandTagFieldComponent} from '../remote-command-tag-field/remote-command-tag-field.component';
import {RemoteComboBoxFieldComponent} from '../remote-combo-box-field/remote-combo-box-field.component';
import {
  FieldInputTypeName,
  isCompositeField,
  isDateField,
  isDateTimeField
} from '../../forms/new-advanced-issue-form/new-advanced-issue-form.model';
import {NzTreeNodeOptions} from 'ng-zorro-antd/core/tree/nz-tree-base-node';
import {CommonConfigurationKey} from '../../forms/new-advanced-issue-form/common-configuration-keys';

@Component({
  selector: 'sqtm-app-remote-issue-widget',
  templateUrl: './remote-issue-widget.component.html',
  styleUrls: ['./remote-issue-widget.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoteIssueWidgetComponent implements OnDestroy {

  // Local copy for template
  TypeName = FieldInputTypeName;

  @Input() set field(field: Field) {
    this._field = field;
    this.preComputeOptions();
  }

  get field(): Field {
    return this._field;
  }

  @Input() set formGroup(formGroup: FormGroup) {
    this._formGroup = formGroup;
    this.sendDelegateCommandOnValueChange();
    this.showConfirmDialogOnValueChanges();
  }

  get formGroup(): FormGroup {
    return this._formGroup;
  }

  @ViewChild(TextFieldComponent)
  textField?: TextFieldComponent;

  @ViewChild(RemoteAutocompleteTextFieldComponent)
  remoteAutocompleteTextField: RemoteAutocompleteTextFieldComponent;

  @ViewChild(SelectFieldComponent)
  selectField: SelectFieldComponent;

  @ViewChild(DatePickerComponent)
  dateField: DatePickerComponent;

  @ViewChild(RichTextFieldComponent)
  richTextField: RichTextFieldComponent;

  @ViewChild(TextAreaFieldComponent)
  textAreaField: TextAreaFieldComponent;

  @ViewChild(RemoteAttachmentFieldComponent)
  attachmentField: RemoteAttachmentFieldComponent;

  @ViewChild(RemoteCommandTagFieldComponent)
  remoteCommandTagField: RemoteCommandTagFieldComponent;

  @ViewChild(RemoteComboBoxFieldComponent)
  remoteComboBoxField: RemoteComboBoxFieldComponent;

  public displayOptions: DisplayOption[] = [];
  public checkboxOptions: DisplayOption[] = [];
  public cascadingSelectOptions: CascadingSelectOption[] = [];
  public nzNodes: NzTreeNodeOptions[] = [];

  private _field: Field;
  private _formGroup: FormGroup;
  private _onValueChangeFormControlSubscription: Subscription;
  private _showConfirmFormControlSubscription: Subscription;
  private unsub$ = new Subject<void>();
  private previousConfirmedValue: any = null;

  get onChangeCommand(): string {
    return this.field?.rendering.inputType.configuration?.onchange;
  }

  get confirmMessage(): string {
    return this.field?.rendering.inputType.configuration?.confirmMessage;
  }

  get filterPossibleValues(): boolean {
    return Boolean(this.field?.rendering.inputType.configuration?.hasOwnProperty(CommonConfigurationKey.FILTER_POSSIBLE_VALUES));
  }

  constructor(public readonly remoteIssueService: RemoteIssueService,
              public readonly dialogService: DialogService,
              public readonly cdRef: ChangeDetectorRef) {
    this.observeChangeSets();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  showClientSideError(): void {
    [
      this.textField,
      this.remoteAutocompleteTextField,
      this.selectField,
      this.dateField,
      this.richTextField,
      this.textAreaField,
      this.remoteCommandTagField,
      this.remoteComboBoxField,
    ].filter(Boolean).forEach(field => field.showClientSideError());
  }

  getFormControl(field: Field): FormControl {
    return this.formGroup.controls[field.id] as FormControl;
  }

  private preComputeOptions(): void {
    this.checkboxOptions = [];
    this.displayOptions = [];
    this.cascadingSelectOptions = [];
    this.nzNodes = [];

    if (this.field == null) {
      return;
    }

    switch (this.field.rendering.inputType.name) {
      case FieldInputTypeName.dropdown_list:
      case FieldInputTypeName.tag_list:
      case FieldInputTypeName.free_tag_list:
      case FieldInputTypeName.radio_button:
      case FieldInputTypeName.multi_select:
      case FieldInputTypeName.combo_box:
        this.displayOptions = FieldToWidgetHelper.getOptionsForField(this.field);
        break;
      case FieldInputTypeName.option_with_child:
      case FieldInputTypeName.cascading_select:
        this.cascadingSelectOptions = FieldToWidgetHelper.getCascadeOptionsForField(this.field);
        break;
      case FieldInputTypeName.checkbox_list:
        this.checkboxOptions = FieldToWidgetHelper.getCheckboxOptionsForField(this.field);
        break;
      case FieldInputTypeName.tree_select:
        this.buildNzNodesFromPossibleValues();
        break;
    }
  }

  private sendDelegateCommandOnValueChange(): void {
    if (!this.onChangeCommand) {
      return;
    }

    if (this.confirmMessage) {
      // We'll handle the onChange command only after the change is confirmed
      return;
    }

    if (this.formGroup == null) {
      return;
    }

    if (this._onValueChangeFormControlSubscription) {
      this._onValueChangeFormControlSubscription.unsubscribe();
    }

    const formControl = this.formGroup.controls[this.field.id];

    if (formControl == null) {
      throw new Error('The form group was not properly initialized. Cannot observe value changes.');
    }

    this._onValueChangeFormControlSubscription = formControl.valueChanges.pipe(
      takeUntil(this.unsub$),
      distinctUntilChanged(compareValues),
    ).subscribe(() => {
      const value = formControl.value;
      this.remoteIssueService.sendDelegateCommand<any>(this.onChangeCommand, value).subscribe();
    });
  }

  private showConfirmDialogOnValueChanges(): void {
    if (!this.confirmMessage) {
      return;
    }

    if (this._showConfirmFormControlSubscription) {
      this._showConfirmFormControlSubscription.unsubscribe();
    }

    const formControl = this.formGroup.controls[this.field.id];

    if (formControl == null) {
      throw new Error('The form group was not properly initialized. Cannot observe value changes.');
    }

    this.previousConfirmedValue = formControl.value;

    this._showConfirmFormControlSubscription = formControl.valueChanges.pipe(
      takeUntil(this.unsub$),
      distinctUntilChanged(compareValues),
      filter(newValue => newValue !== this.previousConfirmedValue),
      concatMap(() => {
        const dialogRef = this.dialogService.openConfirm({
          id: 'confirm-change-field-value',
          messageKey: this.confirmMessage,
        });

        return dialogRef.dialogClosed$;
      }),
    ).subscribe((confirm) => {
      if (confirm) {
        this.previousConfirmedValue = formControl.value;

        if (this.onChangeCommand) {
          this.remoteIssueService.sendDelegateCommand<any>(this.onChangeCommand, formControl.value).subscribe();
        }
      } else {
        formControl.setValue(this.previousConfirmedValue);
      }
    });
  }

  private observeChangeSets(): void {
    this.remoteIssueService.changeSet$.pipe(
      takeUntil(this.unsub$),
    ).subscribe((changeSet) => this.processChangeSet(changeSet));
  }

  private processChangeSet(changeSet: ChangeSet): void {
    const formControl = this.formGroup.controls[this.field.id];

    if (formControl == null) {
      throw new Error('The form group was not properly initialized. Cannot process value changes.');
    }

    changeSet.changes.forEach(change => {
      if (change.fieldId === this.field.id) {
        if (isReplaceValueChange(change) && change.newValue) {
          if (isCompositeField(this.field) && Array.isArray(change.newValue.composite)) {
            formControl.setValue(change.newValue.composite.map(v => v.scalar));
          } else if (isDateField(this.field)) {
            formControl.setValue(FieldToWidgetHelper.parseDate(change.newValue.scalar));
          } else if (isDateTimeField(this.field)) {
            formControl.setValue(FieldToWidgetHelper.parseDateTime(change.newValue.scalar));
          } else {
            formControl.setValue(change.newValue.scalar);
          }
        } else if (isReplacePossibleValuesChange(change) && change.newPossibleValues) {
          // TODO do not mutate input!
          this.field.possibleValues = change.newPossibleValues;
        } else if (isAppendPossibleValuesChange(change) && change.newPossibleValues) {
          this.field.possibleValues = [...this.field.possibleValues, ...change.newPossibleValues];
          this.cdRef.detectChanges();
        }
      }
    });
  }

  private buildNzNodesFromPossibleValues(): void {
    const rootNode = this._field?.possibleValues[0];

    if (rootNode == null) {
      this.nzNodes = [];
    } else {
      this.nzNodes = [this.asNzNodes(rootNode)];
    }
  }

  // Recursively transform a field value's composite into Nz tree nodes
  private asNzNodes(fieldValue: FieldValue): NzTreeNodeOptions {
    const composite = fieldValue.composite ?? [];
    return {
      title: fieldValue.scalar,
      key: fieldValue.id,
      children: composite.map(child => this.asNzNodes(child)),
      isLeaf: composite.length === 0
    };
  }
}

/**
 * Check if two values are the same. In the context of remote issue widgets, we consider values to be
 * the same if they are both arrays with identical content. If they are not arrays, we use triple equality
 * comparison as there shouldn't be complex data types (objects) among form values anyway.
 */
function compareValues(first: any, second: any) {
  if (Array.isArray(first)) {
    if (Array.isArray(second)) {
      return arraysAreSame(first, second);
    } else {
      return false;
    }
  } else {
    return first === second;
  }
}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {
  AbstractFormField,
  ChangeSet,
  DisplayOption,
  Field,
  FieldValidationError,
  SelectFieldComponent
} from 'sqtm-core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {RemoteIssueService} from '../../../services/remote-issue.service';

@Component({
  selector: 'sqtm-app-remote-command-tag-field',
  templateUrl: './remote-command-tag-field.component.html',
  styleUrls: ['./remote-command-tag-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoteCommandTagFieldComponent extends AbstractFormField implements OnInit, OnDestroy {
  @Input() formGroup: FormGroup;
  @Input() fieldName: string;
  @Input() options: DisplayOption[] = [];
  @Input() field: Field;

  @ViewChild(SelectFieldComponent)
  selectField: SelectFieldComponent;

  // Not displayed for now
  serverSideFieldValidationError: any[];

  private unsub$ = new Subject<void>();

  constructor(public readonly remoteIssueService: RemoteIssueService,
              cdRef: ChangeDetectorRef) {
    super(cdRef);
  }

  ngOnInit(): void {
    this.observeChangeSets();
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  showClientSideError(): void {
    this.selectField.showClientSideError();
  }

  protected showServerSideError(fieldsValidationErrors: FieldValidationError[]): void {
    this.selectField.serverSideFieldValidationError = fieldsValidationErrors;
  }

  private observeChangeSets(): void {
    this.remoteIssueService.changeSet$.pipe(
      takeUntil(this.unsub$),
    ).subscribe((changeSet) => this.processChangeSet(changeSet));
  }

  private processChangeSet(changeSet: ChangeSet): void {
    changeSet.changes.forEach(change => {
      if (change.fieldId === this.field.id) {
        if (change.newValue) {
          if (Array.isArray(change.newValue.composite)) {
            this.formGroup.controls[this.fieldName].setValue(change.newValue.composite.map(v => v.scalar));
          }
        }

        if (change.newPossibleValues) {
          this.options = change.newPossibleValues.map(value => ({id: value.id, label: value.scalar}));
        }
      }
    });
  }
}

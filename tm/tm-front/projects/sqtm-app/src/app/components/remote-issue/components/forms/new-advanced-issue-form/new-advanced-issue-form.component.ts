import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  NgZone,
  QueryList,
  ViewChildren
} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  ActionErrorDisplayService,
  AdvancedIssue,
  DialogReference,
  Field,
  isAdvancedIssue,
  ValueRenderingInputType
} from 'sqtm-core';
import {RemoteIssueDialogState} from '../../../states/remote-issue-dialog.state';
import {RemoteIssueService} from '../../../services/remote-issue.service';
import {AbstractRemoteIssueForm, ServerSideErrorHandler} from '../abstract-remote-issue-form';
import {Observable, Subscription} from 'rxjs';
import {remoteIssueLogger} from '../../../remote-issue.logger';
import {distinctUntilChanged, filter, map, switchMap, take} from 'rxjs/operators';
import {
  findFieldValueAmongPossibleValues,
  getPossibleValuesForField,
  isCompositeField,
  isDateField,
  isDateTimeField,
  isDropdownListField,
  isFileUpload,
  isIssueFieldTypeSupported,
  isTreeSelectField,
  SUPPORTED_INPUT_TYPES,
  SUPPORTED_UNKNOWN_INPUT_DATA_TYPES,
  toFieldValue
} from './new-advanced-issue-form.model';
import {TranslateService} from '@ngx-translate/core';
import * as _ from 'lodash';
import {FieldToWidgetHelper} from '../field-to-widget.helper';
import {RemoteIssueWidgetComponent} from '../../widgets/remote-issue-widget/remote-issue-widget.component';
import {RemoteAttachmentFieldComponent} from '../../widgets/remote-attachment-field/remote-attachment-field.component';
import {CommonConfigurationKey} from './common-configuration-keys';

const logger = remoteIssueLogger.compose('NewAdvancedIssueFormComponent');

/**
 * Remote issue reporting form for advanced issues (e.g. Jira issues).
 * The emphasis is made on display logic whereas the data handling is mostly delegated to pure functions
 * defined in new-advance-issue-form.model.ts.
 * Remote project selection and error handling is done in parent class.
 */
@Component({
  selector: 'sqtm-app-new-advanced-issue-form',
  templateUrl: './new-advanced-issue-form.component.html',
  styleUrls: [
    './new-advanced-issue-form.component.less',
    '../../../styles/remote-issue-dialog-commons.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewAdvancedIssueFormComponent extends AbstractRemoteIssueForm {

  formGroup: FormGroup;

  readonly currentScheme$: Observable<Field[]>;

  private _schemeSelectorSubscriptions: Subscription[] = [];

  @ViewChildren(RemoteIssueWidgetComponent)
  remoteIssueWidgets: QueryList<RemoteIssueWidgetComponent>;

  get widgetsWithErrorHandling(): Iterable<ServerSideErrorHandler> {
    return this.remoteIssueWidgets;
  }

  get attachmentFields(): RemoteAttachmentFieldComponent[] {
    return this.remoteIssueWidgets?.map(widget => widget.attachmentField)
      .filter(Boolean);
  }

  @Input()
  set remoteIssueDialogState(state: RemoteIssueDialogState) {
    this._remoteIssueDialogState = state;

    if (!isAdvancedIssue(this.remoteIssueDialogState.reportForm)) {
      throw new Error('Bad remote issue type. Expected AdvancedIssue. This component may not work as intended.');
    }

    this.buildFormGroup();
  }

  get remoteIssueDialogState(): RemoteIssueDialogState {
    return this._remoteIssueDialogState;
  }

  private _remoteIssueDialogState: RemoteIssueDialogState;

  get defaultRadioLabel(): string {
    const label = this.translateService.instant('sqtm-core.generic.label.none.masculine');
    return `(${label})`;
  }

  constructor(public readonly remoteIssueService: RemoteIssueService,
              public readonly fb: FormBuilder,
              public readonly cdRef: ChangeDetectorRef,
              public readonly actionErrorDisplayService: ActionErrorDisplayService,
              public readonly dialogReference: DialogReference,
              public readonly ngZone: NgZone,
              public readonly translateService: TranslateService) {
    super(remoteIssueService, cdRef, fb);

    this.currentScheme$ = remoteIssueService.state$.pipe(
      filter(state => state.reportForm != null),
      map(state => {
        const advancedIssue = state.reportForm as AdvancedIssue;
        return advancedIssue.project.schemes[advancedIssue.currentScheme];
      }),
      filter(scheme => Array.isArray(scheme)));
  }

  // This is NOT the transient state ! This is just the form data obtained on initial fetch.
  // It is still useful for things that won't change : remote project infos, schemes...
  get reportForm(): Readonly<AdvancedIssue> {
    // Type check is done once for all in remoteIssueDialogState setter
    return this._remoteIssueDialogState.reportForm as AdvancedIssue;
  }

  get isEditable(): boolean {
    return !this._remoteIssueDialogState.attachMode;
  }

  isFieldVisible(field: Field): boolean {
    const isSupported = this.isFieldTypeSupported(field.rendering.inputType);

    if (this.remoteIssueDialogState.attachMode) {
      return Object.keys(this.reportForm.fieldValues).includes(field.id);
    } else {
      return isSupported;
    }
  }

  isFieldTypeSupported(inputType: ValueRenderingInputType): boolean {
    const isSupported = isIssueFieldTypeSupported(inputType.name, inputType.dataType);

    if (!isSupported) {
      logger.debug('Unsupported field type received : ' + inputType);
    }

    return isSupported;
  }

  getLabelForField(field: Field): string {
    return field.rendering.required ? `${field.label}*` : field.label;
  }

  isHTMLField(field: Field): boolean {
    return Boolean(field.rendering.inputType.configuration[CommonConfigurationKey.RENDER_AS_HTML]);
  }

  getReadonlyValueForField(field: Field): any {
    // Returning the scalar value is enough because we only display plain text in read-only mode such as summary,
    // description,...
    return this.reportForm.fieldValues[field.id]?.scalar ?? '-';
  }

  protected getNewRemoteIssue(): Observable<AdvancedIssue> {
    return this.currentScheme$.pipe(
      take(1),
      map(scheme => this.makeIssueFromFormValues(scheme)));
  }

  private buildFormGroup(): void {
    this.currentScheme$.pipe(
      take(1),
    ).subscribe((currentScheme) => {
      this.resetSchemeSelectorObservers();
      this.formGroup = this.doBuildFormGroup(currentScheme);
      this.cdRef.detectChanges();
      this.observeSchemeSelectorsValueChanges();
      this.setSchemeSelectorsCurrentValue();
    });
  }

  private doBuildFormGroup(currentScheme: Field[]): FormGroup {
    const controls = {};

    currentScheme.forEach((field) => {
      let initialValue = this.determineCurrentFieldValue(field);
      const validator = field.rendering.required ? Validators.required : null;

      if (this.remoteIssueDialogState.attachMode) {
        initialValue = {value: initialValue, disabled: true};
      }

      controls[field.id] = this.fb.control(initialValue, validator);
    });

    return this.fb.group(controls);
  }

  private determineCurrentFieldValue(field: Field): any {
    const fieldId = field.id;
    const currentFormValue = this.formGroup?.controls[fieldId]?.value;
    const valueToUse = currentFormValue ?? this.findDefaultValue(field);
    return this.validateFormValue(valueToUse, field);
  }

  private findDefaultValue(field: Field): any {
    if (isDropdownListField(field) || isTreeSelectField(field)) {
      const currentFieldValueId = this.reportForm.fieldValues[field.id]?.id;
      return currentFieldValueId ?? FieldToWidgetHelper.getFirstAvailableOption(field)?.id;
    } else if (isCompositeField(field)) {
      return this.extractCompositesFromField(field);
    } else {
      return this.reportForm.fieldValues[field.id]?.scalar;
    }
  }

  private validateFormValue(value: any, field: Field): any {
    if (isDropdownListField(field) || isTreeSelectField(field)) {
      const isCurrentAmongPossiblesValues = getPossibleValuesForField(field)
        .map(fieldValue => fieldValue.id)
        .includes(value);

      if (!isCurrentAmongPossiblesValues) {
        // A dropdown cannot show a value that is not in its option list
        return FieldToWidgetHelper.getFirstAvailableOption(field)?.id;
      }
    }

    if (isCompositeField(field) && value != null && !Array.isArray(value)) {
      return [value];
    }

    if (isDateField(field)) {
      return FieldToWidgetHelper.parseDate(value);
    }

    if (isDateTimeField(field)) {
      return FieldToWidgetHelper.parseDateTime(value);
    }

    return value;
  }

  private makeIssueFromFormValues(scheme: Field[]): AdvancedIssue {
    const issue = {...this.reportForm, fieldValues: {}};
    const strippedIssue: Partial<AdvancedIssue> = stripUndesiredIssueProperties(issue);

    scheme.forEach((schemeField) => {
      const isNotSupportedInputTypeName = !SUPPORTED_INPUT_TYPES.includes(schemeField.rendering.inputType.name as any);
      const isNotSupportedUnknownDataType = schemeField.rendering.inputType.name === 'unknown' &&
        !SUPPORTED_UNKNOWN_INPUT_DATA_TYPES.includes(schemeField.rendering.inputType.dataType);

      if (isNotSupportedInputTypeName || isNotSupportedUnknownDataType) {
        logger.trace(`The type of field ${schemeField.id} is not supported`);
      } else if (isFileUpload(schemeField)) {
        strippedIssue.fieldValues = _.omit(strippedIssue.fieldValues, [schemeField.id]);
      } else {
        const formValue = this.formGroup?.controls[schemeField.id]?.value;
        strippedIssue.fieldValues[schemeField.id] = toFieldValue(schemeField, formValue);
      }
    });
    return strippedIssue as AdvancedIssue; // Yeah... WTF ? See stripUndesiredIssueProperties()
  }

  private observeSchemeSelectorsValueChanges(): void {
    this.currentScheme$
      .pipe(take(1))
      .subscribe((fields) => this.observeSchemeSelectors(fields));
  }

  private resetSchemeSelectorObservers(): void {
    this._schemeSelectorSubscriptions.forEach((sub) => sub.unsubscribe());
    this._schemeSelectorSubscriptions = [];
  }

  private observeSchemeSelectors(fields: Field[]): void {
    this._schemeSelectorSubscriptions = fields
      .filter((field) => isFieldSchemeSelector(field))
      .map(field => ({field, formControl: this.formGroup.controls[field.id]}))
      .filter(({formControl}) => formControl != null)
      .map(({field, formControl}) => this.subscribeToValueChange(field, formControl));
  }

  private subscribeToValueChange(field: Field, formControl: AbstractControl): Subscription {
    return formControl.valueChanges.pipe(
      distinctUntilChanged(),
      map(() => findFieldValueAmongPossibleValues(field, formControl.value)),
      filter(value => value != null),
      switchMap(value => this.remoteIssueService.changeSchemeAndCommit(field, value.scalar))
    ).subscribe(() => this.buildFormGroup());
  }

  public getHelpMessageForField(field: Field): string | undefined {
    return field.rendering?.inputType?.configuration?.['help-message'];
  }

  private extractCompositesFromField(field: Field): any {
    const composites = this.reportForm.fieldValues[field.id]?.composite;
    let result: any[] = [];
    if (composites != null) {
      result = composites.map(composite => {
        return composite.name;
      });
    }
    return result;
  }

  private setSchemeSelectorsCurrentValue(): void {
    // Find all scheme selectors and make sure their value corresponds to the current scheme's option
    this.remoteIssueService.state$.pipe(
      take(1)
    ).subscribe(state => {
      const advancedIssue = state.reportForm as AdvancedIssue;
      const currentSchemeName = advancedIssue.currentScheme;

      advancedIssue.project.schemes[currentSchemeName]
        .filter(isFieldSchemeSelector)
        .forEach(field => {
          const unPrefixedValue = currentSchemeName.replace(field.id + ':', '');
          const desiredOption = field.possibleValues.find(possibleValue => possibleValue.scalar === unPrefixedValue);

          if (desiredOption != null) {
            this.formGroup.get(field.id).setValue(desiredOption.id);
          }
        });
    });
  }
}

function isFieldSchemeSelector(field: Field): boolean {
  return field?.rendering.inputType.fieldSchemeSelector;
}

/**
 * Dirty hack inherited from legacy.
 * For some reason, when fetching the RemoteIssue, we receive an object with properties that
 * we need to remove before sending the object back to the server. If we don't remove them, the
 * bugtracker runs into troubles. This issue was detected with Redmine (SQUASH-3544).
 * I think the client shouldn't do such dirty things to please the server. Why is the server sending
 * these properties in the first place ?
 * Anyway, things would be easier and cleaner if the server wasn't using the same object for the
 * form configuration and the actual form values.
 *
 * Original comment :
 *  //this remove the properties that Jackson shouldn't bother with - thus preventing crashes
 *  //everything it needs is in the fieldValues
 */
function stripUndesiredIssueProperties(issue: AdvancedIssue): Partial<AdvancedIssue> {
  const propertiesToOmit = [
    'priority',
    'comment',
    'version',
    'status',
    'description',
    'category',
    'summary',
    'assignee',
  ];

  return _.omit(issue, propertiesToOmit);
}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  ViewChild
} from '@angular/core';
import {AbstractFormField, ChangeSet, Field, FieldValidationError, FieldValue} from 'sqtm-core';
import {FormGroup} from '@angular/forms';
import {RemoteIssueService} from '../../../services/remote-issue.service';
import {fromEvent, Subject} from 'rxjs';
import {auditTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'sqtm-app-remote-autocomplete-text-field',
  templateUrl: './remote-autocomplete-text-field.component.html',
  styleUrls: ['./remote-autocomplete-text-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoteAutocompleteTextFieldComponent extends AbstractFormField implements OnDestroy {
  @Input() field: Field;
  @Input() formGroup: FormGroup;
  @Input() size: 'small' | 'default' | 'large' = 'default';

  // Inherited but unused for now
  serverSideFieldValidationError: FieldValidationError[];

  @ViewChild('textInput') set textInput(elem: ElementRef) {
    this._textInput = elem;

    if (elem != null) {
      fromEvent(elem.nativeElement, 'input').pipe(
        auditTime(150),
        distinctUntilChanged()
      ).subscribe(() => this.onInput());
    }
  }

  get fieldName(): string {
    return this.field?.id;
  }

  get textInput(): ElementRef {
    return this._textInput;
  }

  private _textInput: ElementRef;
  private unsub$ = new Subject<void>();

  visibleOptionLabels: string[] = [];

  constructor(public readonly remoteIssueService: RemoteIssueService,
              cdr: ChangeDetectorRef) {
    super(cdr);

    this.observeChangeSets();
  }

  get command(): string {
    return this.field?.rendering.inputType.configuration?.onchange;
  }

  get showSearchIcon(): boolean {
    return this.command != null;
  }

  get showClearButton(): boolean {
    return Boolean(this.formControl?.value);
  }

  get isNumeric(): boolean {
    return this.field?.rendering.inputType.dataType === 'number';
  }

  get inputType(): string {
    return this.isNumeric ? 'number' : 'text';
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  // TODO refactor to use ChangeSet when possible
  onInput(): void {
    if (this.command != null) {
      const value = this.textInput?.nativeElement.value;
      this.remoteIssueService.sendDelegateCommand<FieldValue>(this.command, value).subscribe((result) => {
        if (result == null) {
          return;
        } else {
          // This is the legacy implementation for auto-completion : the connector responds with a Field whose field values
          // are the one used to auto-completion (e.g. Jira Server and Jira Cloud connectors).
          // When the server responds with a ChangeSet object, result is null.
          this.visibleOptionLabels = result.composite?.map(v => v.name) ?? [];
          this.cdr.detectChanges();
        }
      });
    }
  }

  clearInputAndShowList(): void {
    if (this.formControl) {
      this.formControl.setValue('');
    }

    if (this.textInput) {
      this.textInput.nativeElement.value = '';
      this.forceRefreshAutocompleteOptions();
    }
  }

  private forceRefreshAutocompleteOptions(): void {
    this.onInput();
    this.textInput?.nativeElement.focus();
  }

  private observeChangeSets(): void {
    this.remoteIssueService.changeSet$.pipe(
      takeUntil(this.unsub$),
    ).subscribe((changeSet) => this.processChangeSet(changeSet));
  }

  private processChangeSet(changeSet: ChangeSet): void {
    changeSet.changes.forEach(change => {
      if (change.fieldId === this.field.id) {
        if (change.newValue) {
          this.formGroup.controls[this.field.id].setValue(change.newValue.scalar);
        }

        if (change.newPossibleValues) {
          this.visibleOptionLabels = change.newPossibleValues.map(value => value.scalar);
        }
      }
    });
  }
}

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  ActionErrorDisplayService,
  DialogConfiguration,
  DialogReference,
  Identifier,
  isAdvancedIssue,
  isOslcIssue,
  IssueBindableEntity,
  RemoteIssueSearchForm,
  RemoteIssueSearchTerms
} from 'sqtm-core';
import {extractErrorMessageFromBugTracker, RemoteIssueService} from '../../services/remote-issue.service';
import {catchError, finalize, map, pluck} from 'rxjs/operators';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {
  NewAdvancedIssueFormComponent
} from '../../components/forms/new-advanced-issue-form/new-advanced-issue-form.component';
import {
  NewRemoteIssueFormComponent
} from '../../components/forms/new-remote-issue-form/new-remote-issue-form.component';
import {HttpErrorResponse} from '@angular/common/http';
import {RemoteIssueSearchComponent} from '../../components/forms/remote-issue-search/remote-issue-search.component';
import {NzSelectOptionInterface} from 'ng-zorro-antd/select';
import {RemoteIssueDialogState} from '../../states/remote-issue-dialog.state';

@Component({
  selector: 'sqtm-app-remote-issue-dialog',
  templateUrl: './remote-issue-dialog.component.html',
  styleUrls: [
    './remote-issue-dialog.component.less',
    '../../styles/remote-issue-dialog-commons.less',
  ],
  providers: [
    {
      provide: RemoteIssueService,
      useClass: RemoteIssueService,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoteIssueDialogComponent implements OnInit, OnDestroy {
  data: RemoteIssueDialogData;
  readonly isAdvancedIssue$: Observable<boolean>;
  readonly isOslcIssue$: Observable<boolean>;

  errorMessage$: Observable<string>;
  searchForm$: Observable<RemoteIssueSearchForm>;
  searchErrorMessage$: Observable<string>;
  canConfirm$: Observable<boolean>;

  @ViewChild('advancedForm')
  advancedForm: NewAdvancedIssueFormComponent;

  @ViewChild('btForm')
  btForm: NewRemoteIssueFormComponent;

  @ViewChild(RemoteIssueSearchComponent)
  issueSearchComponent: RemoteIssueSearchComponent;

  private _searchErrorMessage = new BehaviorSubject<string>(null);

  currentRemoteProject: Identifier = null;
  remoteProjectOptions: NzSelectOptionInterface[] = [];

  constructor(public dialogReference: DialogReference<RemoteIssueDialogData>,
              public readonly remoteIssueService: RemoteIssueService,
              public readonly cdRef: ChangeDetectorRef,
              public readonly actionErrorDisplayService: ActionErrorDisplayService) {
    this.data = this.dialogReference.data;

    this.isAdvancedIssue$ = this.remoteIssueService.reportForm$.pipe(map(issue => isAdvancedIssue(issue)));
    this.isOslcIssue$ = this.remoteIssueService.reportForm$.pipe(map(issue => isOslcIssue(issue)));
    this.errorMessage$ = this.remoteIssueService.errorMessage$;
    this.searchErrorMessage$ = this._searchErrorMessage.asObservable();
    this.searchForm$ = remoteIssueService.state$.pipe(pluck('searchForm'));
    this.canConfirm$ = this.data.attachMode ?
      this.remoteIssueService.reportForm$.pipe(map(reportForm => reportForm?.id != null))
      : this.remoteIssueService.reportForm$.pipe(map(Boolean));
  }

  ngOnInit(): void {
    this.remoteIssueService.loadInitialState(this.data)
      .subscribe((remoteIssueDialogState) => this.prepareRemoteProjectOptions(remoteIssueDialogState));
  }

  get titleKey(): string {
    return this.data.attachMode ?
      'sqtm-core.campaign-workspace.dialog.title.attach-issue' :
      'sqtm-core.campaign-workspace.dialog.title.report-new-issue';
  }

  ngOnDestroy(): void {
    this.remoteIssueService.complete();
  }

  @HostListener('window:dragover', ['$event'])
  preventDefaultFileDragOver($event: Event): void {
    $event.preventDefault();
  }

  @HostListener('window:drop', ['$event'])
  preventDefaultFileDrop($event: Event): void {
    $event.preventDefault();
  }

  handleConfirmation(): void {
    if (this.data.attachMode) {
      this.remoteIssueService.attachToExistingIssue()
        .subscribe(() => this.handleSubmitSuccess(), (err) => this.handleAttachError(err));
    } else {
      const formToUse = this.btForm || this.advancedForm;

      if (formToUse != null) {
        formToUse.submitForm().subscribe(() => this.handleSubmitSuccess());
      }
    }
  }

  searchForIssue(searchTerms: RemoteIssueSearchTerms): void {
    this._searchErrorMessage.next('');

    const formToUse = this.btForm || this.advancedForm;

    if (formToUse != null) {
      formToUse.clearForm();
    }

    this.remoteIssueService.searchForIssue(searchTerms).pipe(
      catchError((err) => this.handleSearchError(err)),
      finalize(() => this.issueSearchComponent.endAsync())
    ).subscribe();
  }

  private handleSubmitSuccess(): void {
    this.dialogReference.result = true;
    this.dialogReference.close();
  }

  private handleSearchError(httpErrorResponse: HttpErrorResponse): Observable<any> {
    const message = extractErrorMessageFromBugTracker(httpErrorResponse) ?? httpErrorResponse.error;
    this._searchErrorMessage.next(message);
    return of(null);
  }

  errorOverlayClicked(): void {
    this.remoteIssueService.clearError();
  }

  closeDialog(): void {
    this.dialogReference.close();
  }

  handleCloseOslc(result: boolean): void {
    if (result) {
      this.handleSubmitSuccess();
    } else {
      // Close without marking success so that grid doesn't get refreshed
      this.dialogReference.close();
    }
  }

  private prepareRemoteProjectOptions(remoteIssueDialogState: RemoteIssueDialogState): void {
    this.remoteProjectOptions = remoteIssueDialogState.bugTrackerInfo.projectNames.map(name => ({
      value: name,
      label: name
    }));
    this.currentRemoteProject = remoteIssueDialogState.remoteProjectName;
  }

  handleProjectSelectChange($event: Identifier): void {
    this.remoteIssueService.switchToRemoteProjectAndCommit($event as any)
      .subscribe(() => this.currentRemoteProject = $event);
  }

  private handleAttachError(err: any): void {
    this.actionErrorDisplayService.showActionError(err);
  }
}

export interface RemoteIssueDialogData {
  bugTrackerId: number;
  squashProjectId: Identifier;
  bindableEntity: IssueBindableEntity;
  boundEntityId: Identifier;
  attachMode: boolean;
}

export function getRemoteIssueDialogConfiguration(data: RemoteIssueDialogData,
                                                  vcRef: ViewContainerRef): DialogConfiguration {
  return {
    component: RemoteIssueDialogComponent,
    id: 'remote-issue-dialog',
    viewContainerReference: vcRef,
    minWidth: 650,
    maxWidth: 1000,
    minHeight: 620,
    maxHeight: '95%',
    data,
  };
}

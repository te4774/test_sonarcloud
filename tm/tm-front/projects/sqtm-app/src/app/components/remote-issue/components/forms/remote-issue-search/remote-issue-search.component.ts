import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  QueryList,
  ViewChildren
} from '@angular/core';
import {RemoteIssue, RemoteIssueSearchForm, RemoteIssueSearchTerms} from 'sqtm-core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {isCompositeField} from '../new-advanced-issue-form/new-advanced-issue-form.model';
import {RemoteIssueWidgetComponent} from '../../widgets/remote-issue-widget/remote-issue-widget.component';

@Component({
  selector: 'sqtm-app-remote-issue-search',
  templateUrl: './remote-issue-search.component.html',
  styleUrls: [
    './remote-issue-search.component.less',
    '../../../styles/remote-issue-dialog-commons.less',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoteIssueSearchComponent {

  searchText: string;

  loading: boolean;

  @Input()
  errorMessage: string;

  @Input()
  set searchForm(searchForm: RemoteIssueSearchForm) {
    this._searchForm = searchForm;
    this.initializeFormGroup();
  }

  get searchForm(): RemoteIssueSearchForm {
    return this._searchForm;
  }

  @Input() foundRemoteIssue: RemoteIssue | undefined;

  @Output()
  searchRequested = new EventEmitter<RemoteIssueSearchTerms>();

  @ViewChildren(RemoteIssueWidgetComponent)
  remoteIssueWidgets: QueryList<RemoteIssueWidgetComponent>;

  formGroup: FormGroup;

  private _searchForm: RemoteIssueSearchForm;

  constructor(private readonly cdr: ChangeDetectorRef,
              private readonly fb: FormBuilder) {
  }

  searchForIssue(): void {
    if (!this.formGroup.valid) {
      this.showValidationErrors();
    } else {
      this.loading = true;
      this.searchRequested.emit(this.getRemoteIssueSearchTerms());
      this.showValidationErrors();
    }
  }

  private getRemoteIssueSearchTerms(): RemoteIssueSearchTerms {
    return this.formGroup.value;
  }

  endAsync(): void {
    this.loading = false;
    this.cdr.detectChanges();
  }

  private initializeFormGroup(): void {
    if (this.searchForm == null) {
      this.formGroup = this.fb.group({});
    } else {

      const controls = {};

      this.searchForm.fields.forEach(field => {
        const initialValue = isCompositeField(field) ? [] : '';
        const required = field.rendering.required;
        controls[field.id] = this.fb.control(initialValue, required ? Validators.required : null);
      });

      this.formGroup = this.fb.group(controls);
    }
  }

  private showValidationErrors() {
    this.remoteIssueWidgets.forEach((widget) => widget.showClientSideError());
  }
}

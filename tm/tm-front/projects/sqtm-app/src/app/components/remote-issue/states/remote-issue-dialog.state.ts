import {Identifier, IssueBindableEntity, RemoteIssue, RemoteIssueSearchForm} from 'sqtm-core';

export interface BoundEntityInfo {
  bindableEntity: IssueBindableEntity;
  boundEntityId: Identifier;
}

// Model fetched from server
export interface BugTrackerInfo {
  projectNames: string[];
  kind: string;
}

export interface RemoteIssueDialogState {
  squashProjectId: Identifier;
  bugTrackerId: Identifier;
  bugTrackerInfo: BugTrackerInfo;
  attachMode: boolean;
  boundEntity: BoundEntityInfo;
  reportForm: RemoteIssue;
  remoteProjectName: string;
  searchForm: RemoteIssueSearchForm;
}

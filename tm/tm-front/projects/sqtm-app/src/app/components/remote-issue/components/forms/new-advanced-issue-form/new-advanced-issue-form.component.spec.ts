import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {NewAdvancedIssueFormComponent} from './new-advanced-issue-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ActionErrorDisplayService, AdvancedIssue, DialogReference, Field} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {RemoteIssueService} from '../../../services/remote-issue.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {EMPTY, of} from 'rxjs';
import {RemoteIssueDialogState} from '../../../states/remote-issue-dialog.state';
import {FieldInputTypeName} from './new-advanced-issue-form.model';
import {catchError} from 'rxjs/operators';
import {mockRemoteIssueService} from '../../../../../utils/testing-utils/mocks.service';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('NewAdvancedIssueFormComponent', () => {
  let component: NewAdvancedIssueFormComponent;
  let fixture: ComponentFixture<NewAdvancedIssueFormComponent>;
  let remoteIssueService: SpyObj<RemoteIssueService>;
  let actionErrorDisplayService: SpyObj<ActionErrorDisplayService>;

  beforeEach(waitForAsync(() => {
    remoteIssueService = mockRemoteIssueService();
    const dialogState = mockDialogState();

    (remoteIssueService as any).state$ = of(dialogState);

    actionErrorDisplayService = createSpyObj(['handleActionError']);
    actionErrorDisplayService.handleActionError.and.returnValue(EMPTY);

    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, TranslateModule.forRoot()],
      declarations: [NewAdvancedIssueFormComponent],
      providers: [
        {provide: RemoteIssueService, useValue: remoteIssueService},
        {provide: ActionErrorDisplayService, useValue: actionErrorDisplayService},
        {provide: DialogReference, useValue: jasmine.createSpyObj(['close'])},
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAdvancedIssueFormComponent);
    component = fixture.componentInstance;

    component.remoteIssueDialogState = mockDialogState();

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should throw if not given an advanced issue', () => {
    expect(() => component.remoteIssueDialogState = {reportForm: {truc: 'muche'}} as unknown as RemoteIssueDialogState)
      .toThrowError('Bad remote issue type. Expected AdvancedIssue. This component may not work as intended.');
  });

  it('should change scheme if scheme selector value changes', waitForAsync(() => {
    component.formGroup.controls['field1'].setValue('scheme1');
    expect(remoteIssueService.changeSchemeAndCommit).toHaveBeenCalled();
  }));

  it('should not submit form if not valid', waitForAsync(() => {
    component.submitForm()
      .pipe(catchError(() => of())) // If error is not handled, the test is considered as failing
      .subscribe();
    expect(remoteIssueService.submitIssue).not.toHaveBeenCalled();
  }));

  it('should submit form', waitForAsync(() => {
    remoteIssueService.submitIssue.and.returnValue(of({
      issueId: 'ISSUE01',
      url: 'www.url.com/bugtracker',
    }));
    component.formGroup.controls['field2'].setValue('any');

    component.submitForm().subscribe();
    expect(remoteIssueService.submitIssue).toHaveBeenCalled();
    expect(component.formGroup.status).toEqual('VALID');
    expect(component.formGroup.errors).toEqual(null);
  }));
});

function makeAdvancedIssue(): AdvancedIssue {
  return {
    currentScheme: 'field1:scheme1',
    assignee: undefined,
    btName: '',
    bugtracker: '',
    category: undefined,
    comment: '',
    description: '',
    fieldValues: {
      'field1': {
        id: 'field1',
        composite: [],
        name: 'field1',
        scalar: 'some-value',
        custom: null,
        typename: '',
      },
      'field2': null,
    },
    hasBlankId: false,
    id: '',
    priority: undefined,
    project: {
      schemes: {
        'field1:scheme1': SCHEME1,
        'field1:scheme2': [],
      },
      id: '1',
      name: 'prj',
    },
    state: undefined,
    summary: '',
    version: undefined,
    getNewKey(): string {
      return '';
    }
  };
}

const SCHEME1: Field[] = [
  {
    label: 'Field 1',
    id: 'field1',
    possibleValues: [
      {
        scalar: 'scheme1',
        id: 'scheme1',
        name: 'scheme1',
        typename: 'string',
        composite: [],
        custom: null,
      },
      {
        scalar: 'scheme2',
        id: 'scheme2',
        name: 'scheme2',
        typename: 'string',
        composite: [],
        custom: null,
      },
    ],
    rendering: {
      inputType: {
        name: FieldInputTypeName.text_field,
        dataType: 'string',
        configuration: null,
        fieldSchemeSelector: true,
        original: null,
      },
      operations: ['SET'],
      required: true,
    }
  },
  {
    label: 'Field 2',
    id: 'field2',
    possibleValues: [],
    rendering: {
      inputType: {
        name: FieldInputTypeName.text_field,
        dataType: 'string',
        configuration: null,
        fieldSchemeSelector: false,
        original: null,
      },
      operations: ['SET'],
      required: true,
    }
  },
];

function mockDialogState(): RemoteIssueDialogState {
  return {
    bugTrackerId: 1,
    attachMode: false,
    searchForm: {
      fields: [],
    },
    squashProjectId: 1,
    reportForm: makeAdvancedIssue(),
    bugTrackerInfo: {
      projectNames: ['P12', 'P13'],
      kind: 'whatev'
    },
    remoteProjectName: 'P12',
    boundEntity: {
      boundEntityId: 12,
      bindableEntity: 'EXECUTION_TYPE',
    }
  };
}

import {DragDropModule} from '@angular/cdk/drag-drop';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {CKEditorModule} from 'ckeditor4-angular';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzDividerModule} from 'ng-zorro-antd/divider';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzRadioModule} from 'ng-zorro-antd/radio';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {
  AttachmentModule,
  CustomFieldModule,
  DialogModule,
  PlatformNavigationModule,
  RequirementUiModule,
  SqtmDragAndDropModule,
  WorkspaceCommonModule
} from 'sqtm-core';
import {ActionStepComponent} from './action-step/action-step.component';
import {CallStepComponent} from './call-step/call-step.component';
import {CallStepParamDialogComponent} from './call-step/dialog/call-step-param-dialog/call-step-param-dialog.component';
import {CreateStepFormComponent} from './create-step-form/create-step-form.component';
import {DraggedKeywordStepsComponent} from './dragged-steps/dragged-keyword-steps/dragged-keyword-steps.component';
import {DraggedStepsComponent} from './dragged-steps/dragged-steps.component';
import {KeywordStepListComponent} from './keyword-step-list/keyword-step-list.component';
import {KeywordStepComponent} from './keyword-step/keyword-step.component';
import {TestStepListComponent} from './test-step-list/test-step-list.component';
import {TestStepComponent} from './test-step/test-step.component';
import {
  DuplicateActionWordsDialogComponent
} from './duplicate-action-words-dialog/duplicate-action-words-dialog.component';
import {ActionWordPasteDialogComponent} from './action-word-paste-dialog/action-word-paste-dialog.component';


@NgModule({
    declarations: [
        TestStepListComponent,
        ActionStepComponent,
        CallStepComponent,
        TestStepComponent,
        CreateStepFormComponent,
        DraggedStepsComponent,
        CallStepParamDialogComponent,
        KeywordStepComponent,
        KeywordStepListComponent,
        DraggedKeywordStepsComponent,
        DuplicateActionWordsDialogComponent,
        ActionWordPasteDialogComponent
    ],
    exports: [
        TestStepListComponent,
        KeywordStepComponent,
        KeywordStepListComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        DragDropModule,
        NzIconModule,
        TranslateModule.forChild(),
        WorkspaceCommonModule,
        CustomFieldModule,
        AttachmentModule,
        RequirementUiModule,
        AttachmentModule,
        SqtmDragAndDropModule,
        PlatformNavigationModule,
        ReactiveFormsModule,
        NzButtonModule,
        CKEditorModule,
        NzDropDownModule,
        NzToolTipModule,
        NzDividerModule,
        DialogModule,
        NzRadioModule,
        NzSelectModule,
        FormsModule
    ]
})
export class TestStepsModule {
}

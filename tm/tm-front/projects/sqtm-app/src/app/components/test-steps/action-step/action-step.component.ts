import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import {
  ActionStepState,
  ActionStepView,
  StepCoverageView,
  TestStepView
} from '../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import {
  ActionErrorDisplayService,
  Attachment,
  CopierService,
  CustomField,
  CustomFieldValue,
  DialogReference,
  DialogService,
  EditableRichTextComponent,
  PersistedAttachment,
  RejectedAttachment,
  RequirementCriticality,
  RequirementCriticalityKeys
} from 'sqtm-core';
import {TestCaseViewService} from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import {catchError, finalize, map, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {DomSanitizer} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {
  DETAILED_STEP_VIEW_URL
} from '../../../pages/detailed-views/detailed-test-step-view/detailed-test-step.constant';
import {
  TestCaseViewComponentData
} from '../../../pages/test-case-workspace/test-case-view/containers/test-case-view/test-case-view.component';

@Component({
  selector: 'sqtm-app-action-step',
  templateUrl: './action-step.component.html',
  styleUrls: ['./action-step.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionStepComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input()
  actionStep: ActionStepView;

  @Input()
  customFields: CustomField[];

  @Input()
  initialFraction: number;

  @Input()
  editable = false;

  @Input()
  linkable = false;

  hasCopiedSteps$: Observable<boolean>;

  get actionCollapsed() {
    return this.sanitizer.bypassSecurityTrustHtml(this.actionStep.action);
  }

  @ViewChild('grid', {read: ElementRef, static: true})
  grid: ElementRef;

  @ViewChild('header', {read: ElementRef, static: true})
  headerAction: ElementRef;

  @ViewChild('buttons', {read: ElementRef, static: true})
  buttons: ElementRef;

  @ViewChild('action', {read: EditableRichTextComponent})
  actionField: EditableRichTextComponent;

  @ViewChild('expectedResult', {read: EditableRichTextComponent})
  expectedResultField: EditableRichTextComponent;

  @Output()
  resizeActionStepColumn = new EventEmitter<number>();

  actionColumnWidth = 0;

  fraction = 1.3;

  private ERROR_LABEL = 'sqtm-core.generic.label.error';

  private unsub$ = new Subject<void>();

  constructor(
    private renderer: Renderer2,
    private testCaseViewService: TestCaseViewService,
    private dialogService: DialogService,
    private sanitizer: DomSanitizer,
    private router: Router,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private copierService: CopierService
  ) {
    this.initHasCopiedSteps();
  }

  private initHasCopiedSteps() {
    this.hasCopiedSteps$ = this.copierService.copiedTestStep$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.copierService.copiedTestStepTcKind$,
        this.testCaseViewService.componentData$),
      map(([ids, tcKind, componentData]: [number[], string, TestCaseViewComponentData]) =>
        ids?.length > 0 && componentData.testCase.kind === tcKind)
    );
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.actionColumnWidth = (this.headerAction.nativeElement as HTMLDivElement).clientWidth;
    if (this.initialFraction) {
      this.fraction = this.initialFraction;
      this.setFaction(this.initialFraction);
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getCfvValue(cufData: CustomField, actionStep: ActionStepState): string | string[] {
    const cfv = this.getCfv(cufData, actionStep);
    if (Boolean(cfv)) {
      return cfv.value;
    }
    return '';
  }

  private getCfv(customField: CustomField, actionStep: ActionStepState): CustomFieldValue {
    const cufId: number = customField.id;
    const cfvs: CustomFieldValue[] = Object.values(actionStep.customFieldValues.entities);
    return cfvs.find(candidate => candidate.cufId === cufId);
  }

  getAttachments(actionStep: ActionStepState): Attachment[] {
    return Object.values(actionStep.attachmentList.attachments.entities);
  }

  getCoverageCriticality(criticality: RequirementCriticalityKeys) {
    return RequirementCriticality[criticality];
  }

  removeStepCoverage(coverage: StepCoverageView, stepId: number) {
    this.testCaseViewService.deleteStepCoverages([coverage.requirementVersionId], stepId);
  }

  handleResize($event: number) {
    const gridWidth = (this.grid.nativeElement as HTMLDivElement).clientWidth;
    const divideFactor = gridWidth / 4;
    const nextFraction: number = this.fraction * (1 + ($event / divideFactor));
    if (nextFraction > 0.25 && nextFraction < 4) {
      this.resizeActionStepColumn.emit(nextFraction);
    }
  }

  setFaction(nextFraction: number) {
    this.fraction = nextFraction;
    this.renderer.setStyle(this.grid.nativeElement, 'grid-template-columns', `${this.fraction}fr 1fr 120px`);
  }

  changeAction(action: string) {
    this.testCaseViewService.changeAction(this.actionStep.id, action)
      .pipe(
        catchError(err => this.actionErrorDisplayService.handleActionError(err)),
        finalize(() => this.actionField.endAsync()),
        tap(() => this.actionField.disableEditMode())
      ).subscribe();
  }

  changeExpectedResult(expectedResult: string) {
    this.testCaseViewService.changeExpectedResult(this.actionStep.id, expectedResult).pipe(
      catchError(err => this.actionErrorDisplayService.handleActionError(err)),
      finalize(() => this.expectedResultField.endAsync()),
      tap(() => this.expectedResultField.disableEditMode())
    ).subscribe();
  }

  updateCustomFieldValue(customField: CustomField, value: string | string[]) {
    const cfv = this.getCfv(customField, this.actionStep);
    this.testCaseViewService.updateStepCustomFieldValue(this.actionStep.id, cfv.id, value);
  }

  deleteAttachment(attachment: PersistedAttachment, stepId: number) {
    this.testCaseViewService.markStepAttachmentsToDelete([attachment.id], stepId);
  }

  confirmDeleteAttachmentEvent(attachment: PersistedAttachment, stepId: number, attachmentListId: number) {
    this.testCaseViewService.deleteStepAttachments([attachment.id], stepId, attachmentListId);
  }

  cancelDeleteAttachmentEvent(attachment: PersistedAttachment, stepId: number) {
    this.testCaseViewService.cancelStepAttachmentsToDelete([attachment.id], stepId);
  }

  removeRejectedAttachment(attachment: RejectedAttachment, stepId: number) {
    this.testCaseViewService.removeStepRejectedAttachments([attachment.id], stepId);
  }

  addActionStepAfter(stepOrder: number) {
    this.testCaseViewService.addActionStepForm(stepOrder + 1);
  }

  deleteStep(actionStep: TestStepView) {
    const dialogReference: DialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.test-case-workspace.dialog.title.remove-test-step.plural',
      messageKey: 'sqtm-core.test-case-workspace.dialog.message.remove-test-step.plural'
    });
    dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      take(1)
    ).subscribe((confirm) => {
      if (confirm) {
        this.testCaseViewService.deleteStep(actionStep.id);
      }
    });
  }

  copyStep(actionStep: ActionStepView) {
    this.testCaseViewService.copySteps(actionStep.id);
  }

  pasteStep(actionStep: ActionStepView) {
    this.testCaseViewService.pasteSteps(actionStep.id, actionStep.testCaseId);
  }

  toggleStep(actionStep: ActionStepView) {
    this.testCaseViewService.toggleStep(actionStep.id);
  }

  openCollapsedAction(actionStep: ActionStepView) {
    this.testCaseViewService.toggleStep(actionStep.id);
    // setTimeout is required to allow ckEditor init after *ngIf is triggered by state change
    setTimeout(() => {
      this.actionField.enableEditMode();
    });
  }

  openCollapsedResult(actionStep: ActionStepView) {
    this.testCaseViewService.toggleStep(actionStep.id);
    // setTimeout is required to allow ckEditor init after *ngIf is triggered by state change
    setTimeout(() => {
      this.expectedResultField.enableEditMode();
    });
  }

  onMenuVisibilityChange(visible: boolean) {
    if (visible) {
      this.renderer.addClass(this.buttons.nativeElement, 'active-menu');
    } else {
      this.renderer.removeClass(this.buttons.nativeElement, 'active-menu');
    }
  }

  openRequirementTreePicker() {
    if (this.linkable) {
      this.testCaseViewService.openRequirementTreePicker();
    }
  }

  openTestCaseTreePicker() {
    if (this.editable) {
      this.testCaseViewService.openTestCaseTreePicker();
    }
  }

  navigateToDetailedStepView() {
    this.router.navigate([DETAILED_STEP_VIEW_URL, this.actionStep.testCaseId, 'step', this.actionStep.stepOrder]);
  }

  addAttachment(files: File[]) {
    this.testCaseViewService.addAttachmentsToStep(files, this.actionStep.id, this.actionStep.attachmentList.id);
  }
}

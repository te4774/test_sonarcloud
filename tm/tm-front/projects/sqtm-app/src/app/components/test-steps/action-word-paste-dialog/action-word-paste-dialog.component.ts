import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {DialogReference, Identifier} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-action-word-paste-dialog',
  templateUrl: './action-word-paste-dialog.component.html',
  styleUrls: ['./action-word-paste-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionWordPasteDialogComponent implements OnInit {

  selectedProjectId: Identifier;

  configuration: ActionWordPasteDialogConfiguration;

  public static readonly OTHER_PROJECTS_ID = 'otherProject';

  public static readonly CURRENT_PROJECT_ID = 'currentProject';

  public pasteDialogData: PasteDialogData[] = [];

  constructor(private dialogReference: DialogReference<ActionWordPasteDialogConfiguration>,
              translateService: TranslateService) {
    this.pasteDialogData = [
      {
        optionLabel: translateService.instant('sqtm-core.test-case-workspace.dialog.field.choose-target-project'),
        projectId: ActionWordPasteDialogComponent.CURRENT_PROJECT_ID
      },
      {
        optionLabel: translateService.instant('sqtm-core.test-case-workspace.dialog.field.choose-original-links'),
        projectId: ActionWordPasteDialogComponent.OTHER_PROJECTS_ID
      }
    ];
  }

  ngOnInit(): void {
    this.selectedProjectId = this.pasteDialogData[0].projectId;
  }

  handleConfirm() {
    this.dialogReference.result = this.selectedProjectId;
    this.dialogReference.close();
  }

}

export interface ActionWordPasteDialogConfiguration {
  pasteDialogData: PasteDialogData[];
}

export class PasteDialogData {
  optionLabel: string;
  projectId: Identifier;
}

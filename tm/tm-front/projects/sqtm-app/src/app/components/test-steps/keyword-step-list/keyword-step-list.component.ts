import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {catchError, concatMap, filter, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {
  AbstractActionWordFieldComponent,
  ActionErrorDisplayService,
  CopierService,
  DialogConfiguration,
  DialogReference,
  DialogService,
  Identifier,
  KeywordStepFormModel,
  LocalPersistenceService,
  Option,
  Project,
  ProjectPickerComponent,
  ProjectPickerLayout,
  ReferentialDataService,
  SqtmDragLeaveEvent,
  SqtmDragOverEvent,
  SqtmDragStartEvent,
  SqtmDropEvent
} from 'sqtm-core';
import {
  ScriptPreviewDialogComponent
} from '../../../pages/test-case-workspace/test-case-view/components/dialog/script-preview-dialog/script-preview-dialog.component';
import {
  TestCaseViewComponentData
} from '../../../pages/test-case-workspace/test-case-view/containers/test-case-view/test-case-view.component';
import {TestCaseViewService} from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import {KeywordStepView, TestStepState} from '../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import {DraggedKeywordStepsComponent} from '../dragged-steps/dragged-keyword-steps/dragged-keyword-steps.component';
import {DuplicateActionWord} from '../duplicate-action-words-dialog/duplicate-action-word.model';
import {
  DuplicateActionWordsDialogComponent
} from '../duplicate-action-words-dialog/duplicate-action-words-dialog.component';
import {KEYWORD_TEST_STEP_VIEW_ORIGIN, KeywordTestStepDndData} from '../keyword-step-dnd-data';
import {testStepsLogger} from '../test-steps.logger';
import {Observable, of, race, Subject} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {ComponentPortal} from '@angular/cdk/portal';
import {Overlay, OverlayRef} from '@angular/cdk/overlay';
import {createActionStepFormId} from '../../../pages/test-case-workspace/test-case-view/test-case-view.constant';
import {
  ActionWordPasteDialogComponent,
  ActionWordPasteDialogConfiguration
} from '../action-word-paste-dialog/action-word-paste-dialog.component';

const logger = testStepsLogger.compose('KeywordStepListComponent');

@Component({
  selector: 'sqtm-app-keyword-step-list',
  templateUrl: './keyword-step-list.component.html',
  styleUrls: ['./keyword-step-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class KeywordStepListComponent implements OnInit, OnDestroy {

  @Input()
  steps: KeywordStepView[];

  @Input()
  keywordTestCaseId: number;

  @Input()
  keywords: Option[];

  @Input()
  actionWordLibraryActive: boolean;

  @Input()
  editable = false;

  @Input()
  linkable = false;

  @ViewChild('scopeSelector')
  scopeSelector: ElementRef;

  @ViewChild('actionField')
  actionField: AbstractActionWordFieldComponent;

  selectedProjects: Project[] = [];

  private projectPickerComponentRef: ComponentRef<ProjectPickerComponent>;

  private overlayRef: OverlayRef;

  addKeywordStepFormGroup: FormGroup;

  private unsub$ = new Subject<void>();
  hasSelectedSteps$: Observable<boolean>;
  selectedStepIds$: Observable<number[]>;
  isAdminOrProjectManager$: Observable<boolean>;
  canPaste$: Observable<boolean>;
  hasCopiedKeywords$: Observable<boolean>;

  matchingActionWords: string[] = [];

  public readonly draggedStepComponent = DraggedKeywordStepsComponent;

  readonly origin = KEYWORD_TEST_STEP_VIEW_ORIGIN;

  readonly lastPositionDropZone = 'LAST_POSITION_DROP_ZONE';

  private readonly PROJECT_FILTER_PERSISTENCE: string = 'project-filter-persistence';

  private readonly CURRENT_PROJECT_AW_ID: Identifier = 'currentProject';

  constructor(private cdr: ChangeDetectorRef,
              private rds: ReferentialDataService,
              private vcr: ViewContainerRef,
              private overlay: Overlay,
              public testCaseViewService: TestCaseViewService,
              private formBuilder: FormBuilder,
              private errorService: ActionErrorDisplayService,
              private dialogService: DialogService,
              private translateService: TranslateService,
              private localPersistenceService: LocalPersistenceService,
              private copierService: CopierService) {
  }

  ngOnInit(): void {
    logger.debug('Initialize KeywordStepListComponent...');
    this.initAddKeywordStepFormGroup();
    this.initHasCopiedKeywords();

    this.selectedStepIds$ = this.testCaseViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => componentData.testCase.testSteps.selectedStepIds)
    );

    this.hasSelectedSteps$ = this.testCaseViewService.componentData$.pipe(
      takeUntil(this.unsub$),
      map(componentData => componentData.testCase.testSteps.selectedStepIds.length > 0)
    );
    this.getPersistedProjectsFilter();

    this.canPaste$ = this.selectedStepIds$.pipe(
      takeUntil(this.unsub$),
      map(stepIds => stepIds.length < 2),
      withLatestFrom(this.hasCopiedKeywords$),
      map(([hasOneRowSelected, hasCopiedSteps]: [boolean, boolean]) => hasOneRowSelected && hasCopiedSteps && this.editable)
    );
  }

  private getPersistedProjectsFilter(): void {
    this.localPersistenceService.get<Project[]>(this.PROJECT_FILTER_PERSISTENCE).subscribe(
      (localStorageSelectedProjects: Project[]) => {
        if (localStorageSelectedProjects) {
          this.selectedProjects = localStorageSelectedProjects;
        } else {
          this.initializeAllProjects();
        }
      });
  }

  getDisplayedKeyword() {
    return this.keywords.map(keyword => {
      return {id: keyword.value, label: keyword.label};
    });
  }

  initAddKeywordStepFormGroup() {
    this.addKeywordStepFormGroup = this.formBuilder
      .group({
        'keyword': this.formBuilder.control('GIVEN'),
        'action': this.formBuilder.control('', [Validators.required])
      });
  }

  filterKeywordSteps() {
    return this.steps.filter(step => step.kind === 'keyword-step');
  }

  addStep(event: KeyboardEvent) {
    event.stopPropagation();
    this.actionField.blurInput();
    if (this.addKeywordStepFormGroup.valid) {
      if (this.actionWordLibraryActive) {
        this.createStepCheckingActionWordDuplicate();
      } else {
        this.createStep();
      }
    } else {
      this.dialogService.openAlert({
        titleKey: 'sqtm-core.generic.label.error',
        messageKey: 'sqtm-core.error.action-word.input.value'
      });
    }
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  openProjectsFilter() {
    this.rds.projects$.pipe(
      take(1)
    ).subscribe(projects => {
      this.createOverlay();
      const projectPickerComponentPortal = new ComponentPortal(ProjectPickerComponent, this.vcr);
      this.projectPickerComponentRef = this.overlayRef.attach(projectPickerComponentPortal);
      this.projectPickerComponentRef.instance.projects = projects;
      this.projectPickerComponentRef.instance.initiallySelectedProjects = this.getInitiallySelectedProjects();
      this.projectPickerComponentRef.changeDetectorRef.detectChanges();
      this.projectPickerComponentRef.instance.selectedProjects.pipe(
        takeUntil(this.unsub$),
        take(1)
      ).subscribe(selectedProjects => {
        this.selectedProjects = selectedProjects;
        this.persistSelectedProjects(selectedProjects);
        this.cdr.detectChanges();
        this.closeProjectSelector();
      });
      race([this.overlayRef.backdropClick(), this.projectPickerComponentRef.instance.cancelRequired])
        .pipe(
          takeUntil(this.unsub$),
          take(1)
        ).subscribe(() => this.closeProjectSelector());
    });
  }

  copyTestSteps() {
    const copiedStepIds: number[] = this.steps.filter(step => step.selected).map(step => step.id);
    this.testCaseViewService.copySteps(copiedStepIds[0]);
  }

  pasteTestSteps() {
    const targetSteps = this.steps.filter(step => step.selected);
    const targetStepId = targetSteps.length === 0 ? null :
      targetSteps[0].id ===  createActionStepFormId ? null : targetSteps[0].id;
    this.testCaseViewService.compareKeywordProjectsIds(this.keywordTestCaseId).pipe(
      take(1),
      concatMap((shouldNotOpenDialog: boolean) => this.shouldAssignActionWordsToCurrentProject(shouldNotOpenDialog))
    ).subscribe((isAssignedToTargetProject: boolean) =>
      this.testCaseViewService.pasteSteps(targetStepId, this.keywordTestCaseId, isAssignedToTargetProject));
  }

  private shouldAssignActionWordsToCurrentProject(shouldNotOpenDialog: boolean): Observable<boolean> {
    if (!shouldNotOpenDialog) {
      return this.openPasteDialog();
    }
    return of(false);
  }

  private openPasteDialog(): Observable<boolean> {
    return this.dialogService.openDialog(this.getActionWordPasteDialogConfiguration())
      .dialogClosed$
      .pipe(
        filter((selectedProjectId: Identifier) => selectedProjectId != null),
        map((selectedProjectId: Identifier) => selectedProjectId !== ActionWordPasteDialogComponent.OTHER_PROJECTS_ID)
      );
  }

  private getActionWordPasteDialogConfiguration():
    DialogConfiguration<ActionWordPasteDialogConfiguration> {
    return {
      id: 'action-word-paste',
      component: ActionWordPasteDialogComponent,
      viewContainerReference: this.vcr,
      width: 600
    };
  }

  persistSelectedProjects(selectedProjects: Project[]): void {
    this.localPersistenceService.set<Project[]>(this.PROJECT_FILTER_PERSISTENCE, selectedProjects).subscribe();
  }

  getDisplayValue() {
    return this.selectedProjects.length > 0 ? this.selectedProjects.map(project => project.name)
      .sort()
      .join(', ') : this.translateService.instant('sqtm-core.project-filter.all-projects');
  }

  closeProjectSelector() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.projectPickerComponentRef = null;
    }
  }

  private getInitiallySelectedProjects() {
    return this.selectedProjects.map(project => project.id);
  }

  private createStepCheckingActionWordDuplicate() {
    this.testCaseViewService.getDuplicateActionWords(
      this.addKeywordStepFormGroup.get('action').value
    ).pipe(
      map(result => Object.keys(result)
        .map((key: string) => ({projectName: key, actionWordId: result[key]}))),
      catchError(error => this.errorService.handleActionError(error)),
    ).subscribe((duplicateActionWords: DuplicateActionWord[]) => {
      if (duplicateActionWords.length > 0) {
        const actualProject: DuplicateActionWord = {
          projectName: this.translateService.instant('sqtm-core.test-case-workspace.dialog.field.choose-this-project'),
          actionWordId: this.CURRENT_PROJECT_AW_ID
        };
          duplicateActionWords.unshift(actualProject);
        const duplicateActionDialog = this.dialogService.openDialog(
          this.getDuplicateActionDialogConfiguration(duplicateActionWords));
        duplicateActionDialog.dialogClosed$.pipe(
          take(1),
        ).subscribe((selectedActionWordId: Identifier) => {
          if (selectedActionWordId == null) {
            this.actionField.focusInput();
          } else if (selectedActionWordId === this.CURRENT_PROJECT_AW_ID) {
            this.createStep();
          } else if (typeof selectedActionWordId === 'number') {
            this.createStep(selectedActionWordId);
          } else {
            console.error(`Received invalid selected action word ID ${selectedActionWordId}.`);
          }
        });
      } else {
        this.createStep();
      }
    });
  }

  private getDuplicateActionDialogConfiguration(duplicateActionWords: DuplicateActionWord[]): DialogConfiguration {
    return {
      id: 'duplicate-action-word',
      component: DuplicateActionWordsDialogComponent,
      viewContainerReference: this.vcr,
      data: {duplicateActionWords},
      width: 600
    };
  }

  private createStep(actionWordId?: number) {
    const lastKeyword = this.addKeywordStepFormGroup.get('keyword').value;
    this.testCaseViewService.confirmAddKeywordStep(this.extractKeywordStepForm(actionWordId), this.keywordTestCaseId)
      .pipe(
        catchError(error => this.errorService.handleActionError(error))
      ).subscribe(
      () => {
        this.addKeywordStepFormGroup.get('keyword').reset(lastKeyword);
        this.addKeywordStepFormGroup.get('action').reset('');
        this.matchingActionWords = [];
        this.actionField.focusInput();
      });
  }


  private createOverlay() {
    this.overlayRef = this.overlay.create(ProjectPickerComponent.getOverlayConfig(ProjectPickerLayout.KEYWORDSTEP,
      this.scopeSelector, this.overlay));
  }

  private initializeAllProjects() {
    this.rds.projects$.pipe(
      take(1),
      withLatestFrom(this.rds.filteredProjects$),
      map(([projects, filteredProjects]) => {
        const ids = filteredProjects.map(project => project.id);
        this.selectedProjects = projects.filter(
          project => ids.includes(project.id)
        );
      })).subscribe();
  }

  extractKeywordStepForm(actionWordId?: number): KeywordStepFormModel {
    const keywordStepFormModel: KeywordStepFormModel = {
      keyword: this.addKeywordStepFormGroup.get('keyword').value,
      actionWord: this.addKeywordStepFormGroup.get('action').value
    };
    const lastSelectedIndex: number = this.getLastSelectedStepIndex();
    if (lastSelectedIndex !== undefined) {
      keywordStepFormModel.index = lastSelectedIndex + 1;
    }
    if (actionWordId !== undefined) {
      keywordStepFormModel.actionWordId = actionWordId;
    }
    return keywordStepFormModel;
  }

  private getLastSelectedStepIndex(): number {
    const selectedIndexes = this.steps.filter(step => step.selected).map(step => step.stepOrder);
    if (selectedIndexes.length > 0) {
      return Math.max(...selectedIndexes);
    }
  }

  trackStepFn(index: number, step: TestStepState) {
    return step.id;
  }

  getSelectedProjectsIds(): number[] {
    return this.selectedProjects.map(project => project.id);
  }

  getMatchingActionWords(searchInput: string) {
    this.testCaseViewService.getMatchingActionWords(searchInput, this.getSelectedProjectsIds())
      .pipe(take(1))
      .subscribe((matchingActionWords: string[]) => {
        this.matchingActionWords = matchingActionWords;
        this.cdr.detectChanges();
      });
  }

  /* Drag & Drop */

  dragStart($event: SqtmDragStartEvent) {
    const data = $event.dragAndDropData.data as KeywordTestStepDndData;
    const draggedStepIds = data.draggedSteps.map(step => step.id);
    this.testCaseViewService.startDraggingSteps(draggedStepIds);
  }

  dragOver($event: SqtmDragOverEvent) {
    // No dnd event handled if no write permission or if a step creation is in progress
    const eventOrigin = $event.dragAndDropData.origin;
    // console.log(`detecting over in step component ${$event.dndTarget.id}. origin : ${eventOrigin}`);
    if (eventOrigin === KEYWORD_TEST_STEP_VIEW_ORIGIN) {
      this.dragStepOver($event);
    }
  }

  private dragStepOver($event: SqtmDragOverEvent) {
    this.testCaseViewService.componentData$.pipe(
      take(1),
      filter(() => this.editable),
      filter((componentData: TestCaseViewComponentData) => $event.dndTarget.id !== componentData.testCase.testSteps.currentDndTargetId)
    ).subscribe(() => {
      if ($event.dndTarget.id === this.lastPositionDropZone) {
        this.testCaseViewService.dragOverStep(null);
      } else {
        this.testCaseViewService.dragOverStep($event.dndTarget.id as number);
      }
    });
  }

  private initHasCopiedKeywords() {
    this.hasCopiedKeywords$ = this.copierService.copiedTestStep$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.copierService.copiedTestStepTcKind$,
        this.testCaseViewService.componentData$),
      map(([ids, tcKind, componentData]: [number[], string, TestCaseViewComponentData]) =>
        ids?.length > 0 && componentData.testCase.kind === tcKind)
    );
  }

  drop($event: SqtmDropEvent) {
    if (this.editable && $event && $event.dragAndDropData) {
      const eventOrigin = $event.dragAndDropData.origin;
      if (eventOrigin === KEYWORD_TEST_STEP_VIEW_ORIGIN) {
        this.testCaseViewService.dropSteps();
      }
    }
  }

  dragLeave($event: SqtmDragLeaveEvent) {
    const eventOrigin = $event.dragAndDropData.origin;
    if (eventOrigin === this.origin) {
      this.testCaseViewService.suspendStepDrag();
    }
  }

  dragCancel() {
    logger.debug('Canceling dnd operation');
    this.testCaseViewService.cancelDrag();
  }

  showScriptPreview() {
    this.testCaseViewService.getScriptPreview()
      .pipe(take(1))
      .subscribe(script => {
        const dialogConfiguration: DialogConfiguration = {
          id: 'script-preview-dialog',
          component: ScriptPreviewDialogComponent,
          viewContainerReference: this.vcr,
          data: {content: script}
        };
        this.dialogService.openDialog(dialogConfiguration);
      });
  }

  deleteTestSteps() {
    const deletedStepIds: number[] = this.steps.filter(step => step.selected).map(step => step.id);

    const dialogReference: DialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.test-case-workspace.dialog.title.remove-test-step.plural',
      messageKey: 'sqtm-core.test-case-workspace.dialog.message.remove-test-step.plural'
    });
    dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      take(1)
    ).subscribe((confirm) => {
      if (confirm) {
        this.testCaseViewService.deleteStep(deletedStepIds[0]);
      }
    });
  }
}

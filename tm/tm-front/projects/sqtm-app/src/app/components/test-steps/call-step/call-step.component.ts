import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import {CallStepState, CallStepView} from '../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import {CopierService, Dataset, DialogService, RestService, TestStepModel} from 'sqtm-core';
import {TestCaseViewService} from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import {CallStepParamDialogComponent} from './dialog/call-step-param-dialog/call-step-param-dialog.component';
import {Observable, Subject} from 'rxjs';
import {
  TestCaseViewComponentData
} from '../../../pages/test-case-workspace/test-case-view/containers/test-case-view/test-case-view.component';
import {TranslateService} from '@ngx-translate/core';
import {map, takeUntil} from 'rxjs/operators';
import {
  ParameterAssignationUpdate
} from '../../../pages/test-case-workspace/test-case-view/service/test-step-view.service';
import {
  DETAILED_STEP_VIEW_URL
} from '../../../pages/detailed-views/detailed-test-step-view/detailed-test-step.constant';
import {Router} from '@angular/router';

@Component({
  selector: 'sqtm-app-call-step',
  templateUrl: './call-step.component.html',
  styleUrls: ['./call-step.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CallStepComponent implements OnInit, OnDestroy {

  testCaseViewComponentData$: Observable<TestCaseViewComponentData>;

  @ViewChild('buttons', {read: ElementRef})
  buttons: ElementRef;

  @Input()
  callTestStep: CallStepView;

  @Input()
  editable = false;

  hasCopiedSteps$: Observable<boolean>;

  unsub$ = new Subject<void>();

  constructor(private testCaseViewService: TestCaseViewService,
              private dialogService: DialogService,
              private restService: RestService,
              private copierService: CopierService,
              private translateService: TranslateService,
              private renderer: Renderer2,
              private router: Router) {
    this.testCaseViewComponentData$ = this.testCaseViewService.componentData$.pipe(takeUntil(this.unsub$));
  }

  ngOnInit() {
    this.hasCopiedSteps$ = this.copierService.copiedTestStep$.pipe(
      takeUntil(this.unsub$),
      map(ids => ids?.length > 0)
    );
  }

  getIndex(callTestStep: CallStepState, calledTestCaseStep: TestStepModel): string {
    return `${calledTestCaseStep.stepOrder + 1}`;
  }

  getCalledTestCaseLink(callTestStep: CallStepState) {
    return ['/test-case-workspace/test-case/detail', callTestStep.calledTcId.toString()];
  }

  addActionStepAfter(stepOrder: number) {
    this.testCaseViewService.addActionStepForm(stepOrder + 1);
  }

  deleteStep(callStepView: CallStepView) {
    this.testCaseViewService.deleteStep(callStepView.id);
  }

  copyStep(callStepView: CallStepView) {
    this.testCaseViewService.copySteps(callStepView.id);
  }

  pasteStep(callStepView: CallStepView) {
    this.testCaseViewService.pasteSteps(callStepView.id, callStepView.testCaseId);
  }

  toggleStep(callStepView: CallStepView) {
    this.testCaseViewService.toggleStep(callStepView.id);
  }

  getDataSetLabel() {
    if (this.callTestStep.delegateParam) {
      return this.translateService.instant('sqtm-core.entity.call-step.dataset.delegate');
    } else {
      return this.getCalledDataSetLabel();
    }
  }

  private getCalledDataSetLabel() {
    if (this.callTestStep.calledDatasetId != null) {
      return this.callTestStep.calledDatasetName;
    } else {
      if (this.editable) {
        return this.translateService.instant('sqtm-core.entity.call-step.dataset.pick-dataset.title');
      } else {
        return this.translateService.instant('sqtm-core.entity.call-step.dataset.no-dataset');
      }
    }
  }

  chooseDataset(testCaseName: string) {
    this.restService.get<Dataset[]>([`test-cases/${this.callTestStep.calledTcId}/datasets`]).subscribe(result => {
      const dialogRef = this.dialogService.openDialog<any, ParameterAssignationUpdate>({
        id: 'call-step-param',
        component: CallStepParamDialogComponent,
        data: {id: 'call-step-param', dataSets: result, callStep: this.callTestStep, testCaseName}
      });

      dialogRef.dialogClosed$.subscribe(dialogResult => {
        if (dialogResult) {
          this.testCaseViewService.updateParameterAssignationMode(this.callTestStep.id, dialogResult);
        }
      });
    });

  }

  onMenuVisibilityChange(visible: boolean) {
    if (visible) {
      this.renderer.addClass(this.buttons.nativeElement, 'active-menu');
    } else {
      this.renderer.removeClass(this.buttons.nativeElement, 'active-menu');
    }
  }

  navigateToDetailedStepView() {
    this.router.navigate([DETAILED_STEP_VIEW_URL, this.callTestStep.testCaseId, 'step', this.callTestStep.stepOrder]);
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {CallStepParamDialogComponent} from './call-step-param-dialog.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {DialogReference, RestService} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {CallStepView} from '../../../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import createSpyObj = jasmine.createSpyObj;

describe('CallStepParamDialogComponent', () => {
  let component: CallStepParamDialogComponent;
  let fixture: ComponentFixture<CallStepParamDialogComponent>;
  const dialogReference = createSpyObj(['close']);
  dialogReference.data = {id: '', callStep: {delegateParam: true} as CallStepView, testCaseName: '', dataSets: []};
  const restService = {};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [CallStepParamDialogComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {provide: DialogReference, useValue: dialogReference},
        {provide: RestService, useValue: restService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallStepParamDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Identifier} from 'sqtm-core';

export class DuplicateActionWord {
  projectName: string;
  actionWordId: Identifier;
}

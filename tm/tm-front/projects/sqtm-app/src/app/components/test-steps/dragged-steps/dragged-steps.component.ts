import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {DRAG_AND_DROP_DATA, DragAndDropData, DraggedContentRenderer} from 'sqtm-core';
import {TestStepDndData} from '../test-step-dnd-data';
import {TestStepState} from '../../../pages/test-case-workspace/test-case-view/state/test-step.state';

@Component({
  selector: 'sqtm-app-dragged-steps',
  templateUrl: './dragged-steps.component.html',
  styleUrls: ['./dragged-steps.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DraggedStepsComponent extends DraggedContentRenderer implements OnInit {

  public steps: TestStepState[];

  constructor(@Inject(DRAG_AND_DROP_DATA) public dragAnDropData: DragAndDropData) {
    super(dragAnDropData);
    this.steps = (dragAnDropData.data as TestStepDndData).draggedSteps;
  }

  ngOnInit() {
  }
}

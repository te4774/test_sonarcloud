import {DuplicateActionWord} from './duplicate-action-word.model';

export interface DuplicateActionWordDialogConfiguration {
  duplicateActionWords: DuplicateActionWord[];
}

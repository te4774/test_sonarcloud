import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {fromEvent, Observable, of, Subject} from 'rxjs';
import {
  catchError,
  concatMap,
  filter,
  finalize,
  map,
  switchMap,
  take,
  takeUntil,
  withLatestFrom,
} from 'rxjs/operators';
import {
  ActionErrorDisplayService,
  CopierService,
  DialogConfiguration,
  DialogReference,
  DialogService,
  DraggableItemHandlerDirective,
  DraggableListItemDirective,
  EditableActionTextFieldComponent,
  EditableSelectFieldComponent,
  EditableTextAreaFieldComponent,
  Identifier,
  Option
} from 'sqtm-core';
import {
  TestCaseViewComponentData
} from '../../../pages/test-case-workspace/test-case-view/containers/test-case-view/test-case-view.component';
import {TestCaseViewService} from '../../../pages/test-case-workspace/test-case-view/service/test-case-view.service';
import {KeywordStepView, TestStepView} from '../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import {DuplicateActionWord} from '../duplicate-action-words-dialog/duplicate-action-word.model';
import {
  DuplicateActionWordsDialogComponent
} from '../duplicate-action-words-dialog/duplicate-action-words-dialog.component';
import {KeywordTestStepDndData} from '../keyword-step-dnd-data';
import {
  ActionWordPasteDialogComponent,
  ActionWordPasteDialogConfiguration,
} from '../action-word-paste-dialog/action-word-paste-dialog.component';

@Component({
  selector: 'sqtm-app-keyword-step',
  templateUrl: './keyword-step.component.html',
  styleUrls: ['./keyword-step.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class KeywordStepComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor(
    private testCaseViewService: TestCaseViewService,
    private dialogService: DialogService,
    private readonly translationService: TranslateService,
    private cdr: ChangeDetectorRef,
    private actionErrorDisplayService: ActionErrorDisplayService,
    private vcr: ViewContainerRef,
    private copierService: CopierService) {
    this.initHasCopiedKeywords();
  }

  readonly DATATABLE = DATATABLE;
  readonly DOCSTRING = DOCSTRING;
  readonly COMMENT = COMMENT;

  @Input()
  keywordStep: KeywordStepView;

  @Input()
  keywords: Option[];

  @Input()
  editable;

  @Input()
  linkable = false;

  @Input()
  autocompleteActive = false;

  @ViewChild(EditableSelectFieldComponent)
  keywordSelectField: EditableSelectFieldComponent;

  @ViewChild(EditableActionTextFieldComponent)
  actionTextField: EditableActionTextFieldComponent;

  @ViewChild('datatableTextArea')
  datatableTextAreaField: EditableTextAreaFieldComponent;

  @ViewChild('docstringTextArea')
  docstringTextAreaField: EditableTextAreaFieldComponent;

  @ViewChild('commentTextArea')
  commentTextAreaField: EditableTextAreaFieldComponent;

  @ViewChild(DraggableListItemDirective, {static: true})
  draggableListItem: DraggableListItemDirective;

  @ViewChild(DraggableItemHandlerDirective, {static: true})
  dragHandler: DraggableItemHandlerDirective;

  private draggedSteps = new Subject<KeywordTestStepDndData>();

  addingDatatable = false;

  addingDocstring = false;

  addingComment = false;

  matchingActionWords: string[] = [];

  private unsub$ = new Subject<void>();

  hasCopiedKeywords$: Observable<boolean>;

  private readonly CURRENT_PROJECT_AW_ID: Identifier = 'currentProject';

  ngOnInit(): void {
    this.draggableListItem.connectDataSource(this.draggedSteps);
  }

  private initHasCopiedKeywords() {
    this.hasCopiedKeywords$ = this.copierService.copiedTestStep$.pipe(
      takeUntil(this.unsub$),
      withLatestFrom(this.copierService.copiedTestStepTcKind$,
        this.testCaseViewService.componentData$),
      map(([ids, tcKind, componentData]: [number[], string, TestCaseViewComponentData]) =>
        ids?.length > 0 && componentData.testCase.kind === tcKind)
    );
  }

  ngAfterViewInit(): void {
    // fixing dnd data when user mouse down on the drag handler in case of it could be a dnd.
    // we cannot just react to the dnd start, as data must be fixed at dnd init
    fromEvent(this.dragHandler.host.nativeElement, 'mousedown').pipe(
      takeUntil(this.unsub$),
      switchMap(() => this.testCaseViewService.componentData$.pipe(take(1)))
    ).subscribe((componentData: TestCaseViewComponentData) => {
      const testSteps = componentData.testCase.testSteps;
      const selectedStepIds = testSteps.selectedStepIds;
      let draggedStepIds = selectedStepIds;
      if (!selectedStepIds.includes(this.keywordStep.id)) {
        draggedStepIds = [this.keywordStep.id];
      }
      const draggedSteps = draggedStepIds.map(id => testSteps.entities[id]);
      this.draggedSteps.next(new KeywordTestStepDndData(draggedSteps, this.keywords));
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  selectStep($event: MouseEvent, id: number) {
    if ($event.ctrlKey) {
      this.testCaseViewService.toggleStepSelection(id);
    } else if ($event.shiftKey) {
      this.testCaseViewService.extendStepSelection(id);
    } else {
      this.testCaseViewService.selectStep(id);
    }
  }

  deleteStep() {
    const dialogReference: DialogReference = this.dialogService.openDeletionConfirm({
      titleKey: 'sqtm-core.test-case-workspace.dialog.title.remove-test-step.plural',
      messageKey: 'sqtm-core.test-case-workspace.dialog.message.remove-test-step.plural'
    });
    dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      take(1)
    ).subscribe((confirm) => {
      if (confirm) {
        this.testCaseViewService.deleteStep(this.keywordStep.id);
      }
    });
  }

  changeKeyword(newKeyword: Option) {
    this.testCaseViewService.changeKeyword(this.keywordStep.id, newKeyword.value).pipe(
      takeUntil(this.unsub$),
      take(1),
      catchError(err => this.actionErrorDisplayService.handleActionError(err)),
      finalize(() => this.keywordSelectField.disableEditMode())
    ).subscribe();
  }

  handleChangeAction(newAction: string) {
    if (this.autocompleteActive) {
      this.handleChangeActionWithAutocompleteActive(newAction);
    } else {
      this.changeAction(newAction);
    }
  }

  private handleChangeActionWithAutocompleteActive(newAction: string) {
    this.testCaseViewService.getDuplicateActionWords(newAction).pipe(
      map(result => Object.keys(result)
        .map((key: string) => ({ projectName: key, actionWordId: result[key] }))),
      catchError(error => {
        this.actionTextField.endAsync();
        return this.actionErrorDisplayService.handleActionError(error);
    }),
    ).subscribe((duplicateActionWords: DuplicateActionWord[]) => {
      if (duplicateActionWords.length > 0) {
        const currentProject: DuplicateActionWord = {
          projectName: this.translationService.instant('sqtm-core.test-case-workspace.dialog.field.choose-this-project'),
          actionWordId: this.CURRENT_PROJECT_AW_ID
        };
        duplicateActionWords.unshift(currentProject);
        const duplicateActionDialog = this.dialogService.openDialog(
          this.getDuplicateActionDialogConfiguration(duplicateActionWords));
        duplicateActionDialog.dialogClosed$.pipe(
          take(1),
        ).subscribe((selectedActionWordId: number) => {
          if (selectedActionWordId == null) {
            this.actionTextField.endAsync();
            this.actionTextField.focusInputField();
          } else if (selectedActionWordId === this.CURRENT_PROJECT_AW_ID) {
            this.changeAction(newAction);
          } else {
            this.changeActionWithActionWordId(newAction, selectedActionWordId);
          }
        });
      } else {
        this.changeAction(newAction);
      }
    });
  }

  private changeAction(newAction: string) {
    this.testCaseViewService.changeActionWord(this.keywordStep.id, newAction).pipe(
      takeUntil(this.unsub$),
      take(1),
      catchError(err => this.actionErrorDisplayService.handleActionError(err)),
      finalize(() => this.actionTextField.disableEditMode())
    ).subscribe();
  }

  private changeActionWithActionWordId(newAction: string, actionWordId: number) {
    this.testCaseViewService.changeActionWordWithId(this.keywordStep.id, newAction, actionWordId).pipe(
      takeUntil(this.unsub$),
      take(1),
      catchError(err => this.actionErrorDisplayService.handleActionError(err)),
      finalize(() => this.actionTextField.disableEditMode())
    ).subscribe();
  }

  private getDuplicateActionDialogConfiguration(duplicateActionWords: DuplicateActionWord[]): DialogConfiguration {
    return {
      id: 'duplicate-action-word',
      component: DuplicateActionWordsDialogComponent,
      viewContainerReference: this.vcr,
      data: { duplicateActionWords },
      width: 600
    };
  }

  getMatchingActionWord(value: string) {
    const selectedProjectsIds: number[] = []; // Empty array means all readable projects by the user in the back-end
    if (this.autocompleteActive) {
      this.testCaseViewService.getMatchingActionWords(value, selectedProjectsIds).pipe(
        takeUntil(this.unsub$),
        take(1))
        .subscribe(values => {
          this.matchingActionWords = values;
          this.cdr.detectChanges();
        });
    }
  }

  showAdvertisingDialog() {
    this.dialogService.openAlert({
      titleKey: 'sqtm-core.generic.label.information.short',
      messageKey: 'sqtm-core.action-word-workspace.messages.advertising',
      level: 'INFO'
    }, 700);
  }

  /* Move steps */

  moveStepUp(step: KeywordStepView) {
    this.testCaseViewService.moveStepUp(step.id);
  }

  moveStepDown(step: TestStepView) {
    this.testCaseViewService.moveStepDown(step.id);
  }

  showPlaceHolder(step: TestStepView) {
    return step.showPlaceHolder && step.selected;
  }

  deleteSection(sectionName: KeywordStepSectionName) {
    this.getChangeSectionValueObservable(sectionName, '')
      .subscribe();
  }

  changeSectionValue(sectionName: KeywordStepSectionName, newValue: string) {
    this.getChangeSectionValueObservable(sectionName, newValue)
      .subscribe(() => {
        this.getSectionTextAreaField(sectionName).disableEditMode();
        this.setAddingSectionState(sectionName, false);
      });
  }

  private getChangeSectionValueObservable(sectionName: KeywordStepSectionName, newValue: string) {
    switch (sectionName) {
      case DATATABLE:
        return this.testCaseViewService.changeDatatable(this.keywordStep.id, newValue);
      case DOCSTRING:
        return this.testCaseViewService.changeDocstring(this.keywordStep.id, newValue);
      case COMMENT:
        return this.testCaseViewService.changeComment(this.keywordStep.id, newValue);
      default:
        throw Error(`${sectionName} is not a KeywordStepSectionName.`);
    }
  }

  createSection(sectionName: KeywordStepSectionName) {
    if (!this.hasSectionAttribute(sectionName) && this.editable) {
      this.setAddingSectionState(sectionName, true);
      this.cdr.detectChanges();
      const textAreaField: EditableTextAreaFieldComponent = this.getSectionTextAreaField(sectionName);
      if (sectionName === DATATABLE) {
        textAreaField.editorData =
          this.translationService.instant('sqtm-core.entity.test-step.datatable.placeholder');
      }
      textAreaField.edit = true;
      textAreaField.focus();
    }
  }

  displaySection(sectionName: KeywordStepSectionName) {
    return this.hasSectionAttribute(sectionName) || this.getAddingSectionState(sectionName);
  }

  isEditingSectionField(sectionName: KeywordStepSectionName): boolean {
    return this.getSectionTextAreaField(sectionName)?.edit;
  }

  private getSectionTextAreaField(sectionName: KeywordStepSectionName) {
    switch (sectionName) {
      case DATATABLE:
        return this.datatableTextAreaField;
      case DOCSTRING:
        return this.docstringTextAreaField;
      case COMMENT:
        return this.commentTextAreaField;
      default:
        throw Error(`${sectionName} is not a KeywordStepSectionName.`);
    }
  }

  hasSectionAttribute(sectionName: KeywordStepSectionName): boolean {
    return this.getSectionAttribute(sectionName)?.trim().length > 0;
  }

  private getSectionAttribute(sectionName: KeywordStepSectionName) {
    switch (sectionName) {
      case DATATABLE:
        return this.keywordStep?.datatable;
      case DOCSTRING:
        return this.keywordStep?.docstring;
      case COMMENT:
        return this.keywordStep?.comment;
      default:
        throw Error(`${sectionName} is not a KeywordStepSectionName.`);
    }
  }

  handleCancelSection(sectionName: KeywordStepSectionName) {
    this.setAddingSectionState(sectionName, false);
  }

  private getAddingSectionState(sectionName: KeywordStepSectionName) {
    switch (sectionName) {
      case DATATABLE:
        return this.addingDatatable;
      case DOCSTRING:
        return this.addingDocstring;
      case COMMENT:
        return this.addingComment;
      default:
        throw Error(`${sectionName} is not a KeywordStepSectionName.`);
    }
  }

  private setAddingSectionState(sectionName: KeywordStepSectionName, value: boolean) {
    switch (sectionName) {
      case DATATABLE:
        this.addingDatatable = value;
        break;
      case DOCSTRING:
        this.addingDocstring = value;
        break;
      case COMMENT:
        this.addingComment = value;
        break;
      default:
        throw Error(`${sectionName} is not a KeywordStepSectionName.`);
    }
  }

  copyStep(keywordStepView: KeywordStepView) {
    this.testCaseViewService.copySteps(keywordStepView.id);
  }

  pasteStep(keywordStepView: KeywordStepView) {
    this.testCaseViewService.compareKeywordProjectsIds(keywordStepView.testCaseId).pipe(
      take(1),
      concatMap((shouldNotOpenDialog: boolean) => this.shouldAssignActionWordsToCurrentProject(shouldNotOpenDialog))
    ).subscribe((isAssignedToTargetProject: boolean) =>
        this.testCaseViewService.pasteSteps(keywordStepView.id, keywordStepView.testCaseId, isAssignedToTargetProject));
  }

  private shouldAssignActionWordsToCurrentProject(shouldNotOpenDialog: boolean): Observable<boolean> {
    if (!shouldNotOpenDialog) {
      return this.openPasteDialog();
    }
    return of(false);
  }

  private openPasteDialog(): Observable<boolean> {
    return this.dialogService.openDialog(this.getActionWordPasteDialogConfiguration())
      .dialogClosed$
      .pipe(
        filter((selectedProjectId: Identifier) => selectedProjectId != null),
        map((selectedProjectId: Identifier) => selectedProjectId !== ActionWordPasteDialogComponent.OTHER_PROJECTS_ID)
      );
  }

  private getActionWordPasteDialogConfiguration():
    DialogConfiguration<ActionWordPasteDialogConfiguration> {
    return {
      id: 'action-word-paste',
      component: ActionWordPasteDialogComponent,
      viewContainerReference: this.vcr,
      width: 600
    };
  }
}

export const DATATABLE = 'datatable';
export const DOCSTRING = 'docstring';
export const COMMENT = 'comment';
export type KeywordStepSectionName = 'datatable' | 'docstring' | 'comment';

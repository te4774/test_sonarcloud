import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChildren
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {
  CustomExportWorkbenchData,
  doesHttpErrorContainsSquashFieldError,
  extractSquashFirstFieldError,
  FieldValidationError,
  SqtmDropEvent,
  TextFieldComponent
} from 'sqtm-core';
import {CustomExportWorkbenchService} from '../../services/custom-export-workbench.service';
import {Router} from '@angular/router';
import {select} from '@ngrx/store';
import {
  availableColumnsSelector,
  CraftedExport,
  CustomExportScope,
  CustomExportWorkbenchState,
  exportColumnsSelector,
  IdentifiedExportColumn
} from '../../state/custom-export-workbench.state';
import {
  CreateCustomExportViewService
} from '../../../../pages/custom-report-workspace/create-custom-export-view/services/create-custom-export-view.service';
import {catchError, distinctUntilChanged, filter, map, pluck, switchMap, take} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';

@Component({
  selector: 'sqtm-app-custom-export-workbench',
  templateUrl: './custom-export-workbench.component.html',
  styleUrls: ['./custom-export-workbench.component.less'],
  providers: [
    {
      provide: CustomExportWorkbenchService,
      useClass: CustomExportWorkbenchService
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomExportWorkbenchComponent implements OnInit, OnDestroy {

  @ViewChildren(TextFieldComponent)
  textFields: TextFieldComponent[] = [];

  @Input()
  set workbenchData(workbenchData: CustomExportWorkbenchData) {
    this.workbenchService.load(workbenchData);

    if (workbenchData.customExport?.id) {
      this.confirmButtonKey = 'sqtm-core.generic.label.modify';
    } else {
      this.confirmButtonKey = 'sqtm-core.generic.label.add';
    }
  }

  formGroup: FormGroup;
  fieldValidationErrors: FieldValidationError[] = [];

  availableAttributes = this.workbenchService.state$.pipe(
    select(availableColumnsSelector)
  );

  selectedColumns$ = this.workbenchService.state$.pipe(
    select(exportColumnsSelector),
  );

  canAdd$: Observable<boolean>;

  confirmButtonKey: string;

  constructor(private readonly fb: FormBuilder,
              private readonly renderer: Renderer2,
              private readonly router: Router,
              private readonly cdRef: ChangeDetectorRef,
              public readonly workbenchService: CustomExportWorkbenchService,
              public readonly createCustomExportViewService: CreateCustomExportViewService) {
    this.canAdd$ = workbenchService.state$.pipe(
      map(state => isCraftedExportReadyToBeAdded(state.craftedExport)));
  }

  ngOnInit(): void {
    this.prepareNameField();
  }

  private prepareNameField(): void {
    this.formGroup = this.fb.group({
      'name': this.fb.control('', [Validators.required, Validators.maxLength(255)])
    });

    this.workbenchService.state$.pipe(
      pluck('craftedExport', 'name'),
      distinctUntilChanged(),
    ).subscribe(newName => this.formGroup.controls['name'].setValue(newName));
  }

  changeName(): void {
    const value = this.formGroup.get('name').value || '';
    const name = value.trim();
    this.workbenchService.changeName(name).subscribe(() => {
      this.formGroup.get('name').setValue(name);
      this.fieldValidationErrors = [];
      this.cdRef.detectChanges();
    });
  }

  handleAdd(): void {
    this.workbenchService.state$.pipe(
      take(1),
      filter(() => this.checkFormValidity()),
      switchMap(state => this.createOrUpdateExport(state)),
      catchError(err => this.handleSubmitError(err))
    ).subscribe((addedNodeId) => this.navigateToCustomExportView(addedNodeId));
  }

  private createOrUpdateExport(state: CustomExportWorkbenchState): Observable<number> {
    if (state.craftedExport.existingNodeId) {
      return this.createCustomExportViewService.updateCustomExport(state.craftedExport);
    } else {
      return this.createCustomExportViewService.createCustomExport(state.craftedExport, state.containerId);
    }
  }

  handleCancel() {
    const url = `/custom-report-workspace`;
    this.router.navigate([url]);
  }

  ngOnDestroy(): void {
    this.completeService();
  }

  private completeService() {
    this.workbenchService.complete();
  }

  handleDropAttribute($event: SqtmDropEvent): void {
    const ids = $event.dragAndDropData.data.dataRows.map(proto => proto.id);
    this.workbenchService.addSelectedColumns(ids);
  }

  handleRemoveAttribute(column: IdentifiedExportColumn): void {
    this.workbenchService.removeSelectedColumn(column.id);
  }

  handleScopeChange(newScope: CustomExportScope): void {
    this.workbenchService.changeScope(newScope).subscribe();
  }

  private handleSubmitError(err: any): Observable<any> {
    if (doesHttpErrorContainsSquashFieldError(err)) {
      const squashFieldError = extractSquashFirstFieldError(err);
      this.fieldValidationErrors = squashFieldError.fieldValidationErrors;
      this.cdRef.detectChanges();
    }
    return throwError(err);
  }

  private navigateToCustomExportView(customExportId: number): void {
    const url = `/custom-report-workspace/custom-report-custom-export/${customExportId}`;
    this.router.navigate([url]);
  }

  private checkFormValidity(): boolean {
    this.textFields.forEach(tf => tf.showClientSideError());
    return this.formGroup.valid;
  }
}

function isCraftedExportReadyToBeAdded(craftedExport: CraftedExport): boolean {
  return craftedExport
    && craftedExport.columns.length > 0
    && Boolean(craftedExport.name)
    && Boolean(craftedExport.scope?.id);
}

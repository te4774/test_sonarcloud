import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomExportWorkbenchComponent} from './containers/custom-export-workbench/custom-export-workbench.component';
import {TranslateModule} from '@ngx-translate/core';
import {GridModule, SqtmDragAndDropModule, UiManagerModule, WorkspaceCommonModule} from 'sqtm-core';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzCollapseModule} from 'ng-zorro-antd/collapse';
import {ReactiveFormsModule} from '@angular/forms';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {
  CustomExportScopeSelectorComponent
} from './components/custom-export-scope-selector/custom-export-scope-selector.component';
import {
  CustomExportScopeSelectorOverlayComponent
} from './components/custom-export-scope-selector-overlay/custom-export-scope-selector-overlay.component';
import {
  CustomExportColumnSelectorComponent
} from './components/custom-export-column-selector/custom-export-column-selector.component';
import {WorkbenchCommonModule} from '../workbench-common/workbench-common.module';
import {
  CustomExportColumnCellComponent
} from './components/custom-export-column-cell/custom-export-column-cell.component';
import {
  CustomExportColumnCapsuleComponent
} from './components/custom-export-column-capsule/custom-export-column-capsule.component';

@NgModule({
  declarations: [
    CustomExportWorkbenchComponent,
    CustomExportScopeSelectorComponent,
    CustomExportScopeSelectorOverlayComponent,
    CustomExportColumnSelectorComponent,
    CustomExportColumnCellComponent,
    CustomExportColumnCapsuleComponent,
  ],
  exports: [
    CustomExportWorkbenchComponent,
    CustomExportColumnCapsuleComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    WorkspaceCommonModule,
    NzIconModule,
    NzCollapseModule,
    ReactiveFormsModule,
    NzButtonModule,
    SqtmDragAndDropModule,
    NzToolTipModule,
    GridModule,
    WorkbenchCommonModule,
    UiManagerModule
  ]
})
export class CustomExportWorkbenchModule {
}

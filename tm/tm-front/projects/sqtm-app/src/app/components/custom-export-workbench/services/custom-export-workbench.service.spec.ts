import {TestBed, waitForAsync} from '@angular/core/testing';

import {CustomExportWorkbenchService} from './custom-export-workbench.service';
import {
  BindableEntity,
  CustomExportEntityType,
  CustomFieldBindingDataByEntity,
  getCustomExportColumnNamesByEntityType,
  GlobalConfigurationState,
  ProjectDataMap,
  ReferentialDataService
} from 'sqtm-core';
import {mockReferentialDataService} from '../../../utils/testing-utils/mocks.service';
import {switchMap, take, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {mockProjectData} from '../../../utils/testing-utils/mocks.data';

function mockProjectDataMap(): ProjectDataMap {
  return {
    1: mockProjectData({
      customFieldBinding: {
        CAMPAIGN: [{
          customField: {
            id: 1,
            label: 'my cuf',
          },
          bindableEntity: BindableEntity.CAMPAIGN,
          boundProjectId: 1,
          id: 1,
          position: 0,
          renderingLocations: [],
        }],
      } as CustomFieldBindingDataByEntity
    }),
  };
}

describe('CustomExportWorkbenchService', () => {
  let service: CustomExportWorkbenchService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: ReferentialDataService,
          useValue: mockReferentialDataService(),
        },
        {
          provide: CustomExportWorkbenchService,
          useClass: CustomExportWorkbenchService,
        },
      ]
    });
    service = TestBed.inject(CustomExportWorkbenchService);
    service['referentialDataService'].projectDatas$ = of(mockProjectDataMap());
    service['referentialDataService'].globalConfiguration$ = of({milestoneFeatureEnabled: true} as GlobalConfigurationState);
  });

  it('should load an empty workbench', waitForAsync(() => {
    service.load({
      customExport: null,
      containerId: 1,
      projectId: 1,
    });

    service.state$
      .pipe(take(1))
      .subscribe((state) => {
        expect(Object.values(state.availableColumns).flat().map(col => col.columnName))
          .toEqual(standardColumnsFlatArray);
      });
  }));

  it('should load a workbench with existing export def', waitForAsync(() => {
    service.load({
      containerId: 1,
      projectId: 1,
      customExport: {
        id: 1,
        customReportLibraryNodeId: 1,
        projectId: 1,
        name: 'my export',
        createdOn: (new Date()).toString(),
        createdBy: 'toto',
        lastModifiedOn: null,
        lastModifiedBy: null,
        scopeNodeId: 1,
        scopeNodeName: 'scopeNodeName',
        scopeProjectId: 1,
        columns: [
          {
            columnName: 'CAMPAIGN_CUF',
            entityType: CustomExportEntityType.CAMPAIGN,
            cufId: 1,
          },
          {
            columnName: 'CAMPAIGN_CUF',
            entityType: CustomExportEntityType.CAMPAIGN,
            cufId: 55,  // Deleted cuf
          }
        ],
      },
    });

    service.state$
      .pipe(take(1))
      .subscribe((state) => {
        expect(state.craftedExport.name).toEqual('my export');

        // Check that there was a column added for the CUF bound to scope's project
        const availableColumnLabels = Object.values(state.availableColumns).flat().map(col => col.label);
        expect(availableColumnLabels.length).toBe(standardColumnsFlatArray.length + 1);
        expect(availableColumnLabels).toContain('my cuf');

        // Check that CUFs are preserved if they still exist in project data
        expect(state.craftedExport.columns).toEqual([{
          id: 'CAMPAIGN-1',
          label: 'my cuf',
          columnName: null,
          entityType: CustomExportEntityType.CAMPAIGN,
          cufId: 1,
        }]);
      });
  }));

  it('should deselect a column', waitForAsync(() => {
    service.load({
      containerId: 1,
      projectId: 1,
      customExport: {
        id: 1,
        customReportLibraryNodeId: 1,
        projectId: 1,
        name: 'my export',
        createdOn: (new Date()).toString(),
        createdBy: 'toto',
        lastModifiedOn: null,
        lastModifiedBy: null,
        scopeNodeId: 1,
        scopeNodeName: 'scopeNodeName',
        scopeProjectId: 1,
        columns: [{
          columnName: 'CAMPAIGN_LABEL',
          entityType: CustomExportEntityType.CAMPAIGN,
          cufId: null,
        }],
      },
    });

    service.state$.pipe(
      take(1),
      tap(state => expect(state.craftedExport.columns.length).toBe(1)),
      tap(() => service.removeSelectedColumn('CAMPAIGN_LABEL')),
      switchMap(() => service.state$),
      take(1),
    ).subscribe((state) => expect(state.craftedExport.columns.length).toBe(0));
  }));
});

const standardColumnsFlatArray: string[] = Object.values(getCustomExportColumnNamesByEntityType(true)).flat() as string[];

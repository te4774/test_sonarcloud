import {ChangeDetectionStrategy, Component} from '@angular/core';
import {GridFilter, GridService} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-workbench-column-selector',
  templateUrl: './workbench-column-selector.component.html',
  styleUrls: ['./workbench-column-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkbenchColumnSelectorComponent {

  constructor(private gridService: GridService) {
    this.gridService.addFilters(this.buildFilters());
  }

  openAllEntityTypes() {
    this.gridService.openAllRows();
  }

  closeAllEntityTypes() {
    this.gridService.closeAllRows();
  }

  changeFilter(value: string) {
    this.gridService.applyMultiColumnsFilter(value);
  }

  private buildFilters(): GridFilter[] {
    return [{
      id: 'NAME', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false
    }];
  }
}

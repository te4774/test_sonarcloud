import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GridModule, UiManagerModule, WorkspaceCommonModule} from 'sqtm-core';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {
  WorkbenchColumnSelectorComponent
} from './column-prototype/components/workbench-column-selector/workbench-column-selector.component';

@NgModule({
  declarations: [
    WorkbenchColumnSelectorComponent,
  ],
  exports: [
    WorkbenchColumnSelectorComponent,
  ],
  imports: [
    CommonModule,
    WorkspaceCommonModule,
    GridModule,
    NzIconModule,
    UiManagerModule
  ]
})
export class WorkbenchCommonModule {
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomChartWorkbenchComponent} from './components/custom-chart-workbench/custom-chart-workbench.component';
import {AxisSelectorComponent} from './components/axis-selector/axis-selector.component';
import {TranslateModule} from '@ngx-translate/core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzRadioModule} from 'ng-zorro-antd/radio';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzTabsModule} from 'ng-zorro-antd/tabs';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {GridModule, SqtmDragAndDropModule, UiManagerModule, WorkspaceCommonModule} from 'sqtm-core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ChartsModule} from '../charts/charts.module';
import {
  OverlayColumnPrototypeSelectorComponent
} from './components/overlay-column-prototype-selector/overlay-column-prototype-selector.component';
import {ChartTypeSelectorComponent} from './components/chart-type-selector/chart-type-selector.component';
import {ScopeSelectorComponent} from './components/scope-selector/scope-selector.component';
import {CustomScopeSelectorComponent} from './components/custom-scope-selector/custom-scope-selector.component';
import {FilterPanelComponent} from './components/filter-panel/filter-panel.component';
import {FilterWidgetComponent} from './components/filter-widget/filter-widget.component';
import {
  ChartFilterValueRendererComponent
} from './components/chart-filter-value-renderer/chart-filter-value-renderer.component';
import {WorkbenchCommonModule} from '../workbench-common/workbench-common.module';
import {
  ColumnPrototypeSelectorComponent
} from './components/column-prototype-selector/column-prototype-selector.component';
import {
  FixedColumnPrototypeSelectorComponent
} from './components/fixed-column-prototype-selector/fixed-column-prototype-selector.component';
import {
  ColumnProtoCellRendererComponent
} from './components/column-proto-cell-renderer/column-proto-cell-renderer.component';


@NgModule({
  declarations: [
    CustomChartWorkbenchComponent,
    AxisSelectorComponent,
    OverlayColumnPrototypeSelectorComponent,
    ChartTypeSelectorComponent,
    ScopeSelectorComponent,
    CustomScopeSelectorComponent,
    FilterPanelComponent,
    FilterWidgetComponent,
    ChartFilterValueRendererComponent,
    ColumnPrototypeSelectorComponent,
    FixedColumnPrototypeSelectorComponent,
    ColumnProtoCellRendererComponent,
  ],
  exports: [
    CustomChartWorkbenchComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    NzIconModule,
    NzButtonModule,
    WorkspaceCommonModule,
    GridModule,
    NzToolTipModule,
    UiManagerModule,
    SqtmDragAndDropModule,
    NzInputModule,
    FormsModule,
    ReactiveFormsModule,
    NzSelectModule,
    ChartsModule,
    NzRadioModule,
    NzTabsModule,
    WorkbenchCommonModule,
  ]
})
export class ChartWorkbenchModule {
}

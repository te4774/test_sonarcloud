import {
  ChangeDetectionStrategy,
  Component,
  ComponentRef,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Type,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {ComponentPortal} from '@angular/cdk/portal';
import {
  OverlayColumnPrototypeSelectorComponent
} from '../overlay-column-prototype-selector/overlay-column-prototype-selector.component';
import {take, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {
  AbstractFilterWidget,
  ChangeFilteringAction,
  ChartColumnRole,
  ChartDataType,
  CheckboxCufFilterComponent,
  CustomChartEntityType,
  DateFilterComponent,
  EditFilterTextComponent,
  EntityScope,
  Filter,
  FilterAutomatedTestTechnologyComponent,
  filterAvailableChartOperations,
  FilterData,
  FilterEnumComponent,
  FilterEnumSingleSelectComponent,
  FilterExecutionStatusComponent,
  FilterInfolistComponent,
  FilteringChange,
  FilterOperation,
  MultiValueCufFilterComponent,
  NumericFilterComponent,
  ResearchColumnPrototype,
  Scope
} from 'sqtm-core';
import {CompleteScope, EnhancedColumnPrototype, RenderedCraftedChartFilter} from '../../state/chart-workbench.state';
import {
  ChartFilterValueRendererComponent,
  ChartWorkbenchFilter
} from '../chart-filter-value-renderer/chart-filter-value-renderer.component';
import {chartWorkbenchLogger} from '../../chart-workbench.logger';
import {ChartWorkbenchUtils} from '../chart-workbench.utils';

const logger = chartWorkbenchLogger.compose('FilterPanelComponent');

@Component({
  selector: 'sqtm-app-filter-panel',
  templateUrl: './filter-panel.component.html',
  styleUrls: ['./filter-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterPanelComponent implements OnInit {

  private overlayRef: OverlayRef;

  private componentRef: ComponentRef<OverlayColumnPrototypeSelectorComponent>;

  private unsub$ = new Subject<void>();

  @Input()
  columnPrototypes: { [K in CustomChartEntityType]: EnhancedColumnPrototype[] };

  private _filters: RenderedCraftedChartFilter[];

  filtersData: FilterData[];

  @Input()
  set filters(value: RenderedCraftedChartFilter[]) {
    this._filters = value;
    this.filtersData = this._filters.map(f => this.convertToFilterData(f));
  }

  @Input()
  completeScope: CompleteScope;

  @Output()
  filterAdded = new EventEmitter<string>();

  @Output()
  filterRemoved = new EventEmitter<string>();

  @Output()
  filterChanged = new EventEmitter<ChangeFilteringAction>();

  @ViewChild('filterZone')
  filterZone: ElementRef;

  private readonly filterWidgetByDataType: Partial<{ [K in ChartDataType]: Type<AbstractFilterWidget> }> = {
    [ChartDataType.STRING]: EditFilterTextComponent,
    [ChartDataType.NUMERIC]: NumericFilterComponent,
    [ChartDataType.INFO_LIST_ITEM]: FilterInfolistComponent,
    [ChartDataType.LEVEL_ENUM]: FilterEnumComponent,
    [ChartDataType.REQUIREMENT_STATUS]: FilterEnumComponent,
    [ChartDataType.EXECUTION_STATUS]: FilterExecutionStatusComponent,
    [ChartDataType.DATE]: DateFilterComponent,
    [ChartDataType.DATE_AS_STRING]: DateFilterComponent,
    [ChartDataType.LIST]: MultiValueCufFilterComponent,
    [ChartDataType.TAG]: MultiValueCufFilterComponent,
    [ChartDataType.BOOLEAN_AS_STRING]: CheckboxCufFilterComponent,
    [ChartDataType.BOOLEAN]: FilterEnumSingleSelectComponent,
    [ChartDataType.AUTOMATED_TEST_TECHNOLOGY]: FilterAutomatedTestTechnologyComponent
  };

  constructor(private overlay: Overlay, private vcr: ViewContainerRef) {
  }

  ngOnInit(): void {
  }


  showAddFilterPanel() {
    const positionStrategy = this.overlay.position().flexibleConnectedTo(this.filterZone)
      .withPositions([
        {originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'top', offsetX: 20},
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      width: 380,
      height: 800
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const componentPortal = new ComponentPortal(OverlayColumnPrototypeSelectorComponent, this.vcr);
    this.componentRef = this.overlayRef.attach(componentPortal);
    this.componentRef.instance.columnPrototypes = this.columnPrototypes;
    this.componentRef.changeDetectorRef.detectChanges();
    this.componentRef.instance.selectedPrototype$.pipe(
      take(1)
    ).subscribe(id => {
      this.filterAdded.emit(id);
      this.closeColumnSelector();
    });
    this.overlayRef.backdropClick()
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.closeColumnSelector());
  }

  closeColumnSelector() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }
  }

  convertToFilterData(chartFilter: RenderedCraftedChartFilter): FilterData {
    const projects: EntityScope[] = this.completeScope.projectScope.map((id: number) => ({
      id: id.toString(),
      projectId: id,
      label: ''
    }));
    const scope: Scope = {
      kind: 'project',
      initialKind: 'project',
      active: true,
      value: projects,
      initialValue: projects
    };
    const columnPrototype = chartFilter.column as EnhancedColumnPrototype;
    const operation = FilterOperation[chartFilter.operation];
    const availableOperations = filterAvailableChartOperations(ChartColumnRole.FILTER, columnPrototype.dataType)
      .map(o => FilterOperation[o]);

    const researchColumnPrototypeElement = ResearchColumnPrototype[columnPrototype.label];
    logger.debug('researchColumnPrototypeElement : ', [researchColumnPrototypeElement]);
    const filter: ChartWorkbenchFilter = {
      id: columnPrototype.generatedId,
      cufId: columnPrototype.cufId,
      widget: this.findFilterEditorWidget(columnPrototype),
      columnPrototype: researchColumnPrototypeElement,
      operation,
      availableOperations,
      value: chartFilter.value,
      valueRenderer: ChartFilterValueRendererComponent,
      entityType: columnPrototype.specializedType.entityType,
      i18nLabelKey: ChartWorkbenchUtils.getColumnNameI18nKey(columnPrototype, chartFilter.cufId),
      uiOptions: {
        workspace: ChartWorkbenchUtils.getWorkspace(columnPrototype.specializedType.entityType)
      },
      infoListSelectorOptions: {
        selectedAttribute: 'code'
      },
      preventOpening: chartFilter.preventOpening
    };

    return {
      scope, filter
    };
  }

  private findFilterEditorWidget(columnPrototype: EnhancedColumnPrototype) {
    const widgetType = this.filterWidgetByDataType[columnPrototype.dataType];
    if (!widgetType) {
      throw Error(`No filter widget for type ${columnPrototype.dataType}`);
    }
    return widgetType;
  }

  changeFiltering(filteringChange: FilteringChange, filterData: FilterData) {
    this.filterChanged.emit({...filteringChange, id: filterData.filter.id});
  }

  trackByFilterId(index: number, filterData: FilterData) {
    return filterData.filter.id;
  }

  removeFilter(filterData: FilterData) {
    this.filterRemoved.emit(filterData.filter.id as string);
  }

  extractPreventOpening(filter: Filter) {
    return (filter as ChartWorkbenchFilter).preventOpening;
  }
}

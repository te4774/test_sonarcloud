import {TestBed} from '@angular/core/testing';

import {ChartWorkbenchService} from './chart-workbench.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';
import {
  ChartWorkbenchState,
  emptyChartColumnProtoByEntityType,
  EnhancedColumnPrototype
} from '../state/chart-workbench.state';
import {
  ChartColumnPrototype,
  ChartColumnType,
  ChartDataType,
  ChartOperation,
  ChartType,
  createEmptyBindingByEntity,
  CustomChartEntityType,
  CustomField,
  CustomFieldBindingData,
  FilterOperation,
  InputType,
  MilestoneModeData,
  ProjectData,
  ProjectDataMap,
  ReferentialDataService
} from 'sqtm-core';
import {scan, skip} from 'rxjs/operators';
import {Observable, of} from 'rxjs';

describe('ChartWorkbenchService', () => {
  let service: ChartWorkbenchService;

  const customFieldBinding = createEmptyBindingByEntity();
  customFieldBinding.REQUIREMENT_VERSION = [{
    customField: {
      id: 1,
      inputType: InputType.PLAIN_TEXT,
      label: 'CUF-1'
    } as CustomField
  } as CustomFieldBindingData];
  const projectDatas$: Observable<ProjectDataMap> = of({
    1: {
      id: 1,
      customFieldBinding,
      allowTcModifDuringExec: false
    } as ProjectData
  });

  const milestoneModeData: MilestoneModeData = {
    milestoneFeatureEnabled: true,
    milestones: [],
    milestoneModeEnabled: false,
    selectedMilestone: null
  };

  const rdsMock = jasmine.createSpyObj(['load']);
  rdsMock.projectDatas$ = projectDatas$;
  rdsMock.milestoneModeData$ = of(milestoneModeData);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppTestingUtilsModule],
      providers: [
        {
          provide: ReferentialDataService,
          useValue: rdsMock
        },
        {
          provide: ChartWorkbenchService,
          useClass: ChartWorkbenchService
        }
      ]
    });
    service = TestBed.inject(ChartWorkbenchService);
  });

  it('should load building blocks', (done) => {
    service.state$.pipe(
      scan((all, current) => [...all, current], []),
      skip(1) // skip first emission, initial state
    ).subscribe((states) => {
      const initialState: ChartWorkbenchState = states[0];
      const nextState: ChartWorkbenchState = states[1];
      expect(initialState.rawColumnsPrototypesByEntity.REQUIREMENT_VERSION.length).toEqual(0);
      expect(nextState.rawColumnsPrototypesByEntity.REQUIREMENT_VERSION.length).toEqual(3);
      expect(initialState.chartColumnPrototypesByEntityType.REQUIREMENT_VERSION.length).toEqual(0);
      expect(nextState.chartColumnPrototypesByEntityType.REQUIREMENT_VERSION).toEqual(['7', '13', '95-1']);
      const generatedProto: EnhancedColumnPrototype = nextState.chartColumnPrototypes.entities['95-1'];
      expect(generatedProto).toBeTruthy();
      expect(generatedProto.cufLabel).toEqual('CUF-1');
      expect(generatedProto.cufId).toEqual(1);
      expect(generatedProto.generatedId).toEqual('95-1');
      done();
    });

    service.initializeBuildingBlocks({
      ...emptyChartColumnProtoByEntityType,
      [CustomChartEntityType.REQUIREMENT_VERSION]: reqVersionColumnPrototype
    }, 1);
  });

  it('should change axis', (done) => {
    service.state$.pipe(
      scan((all, current) => [...all, current], []),
      skip(4) // skip first emission, initial state and the data loading
    ).subscribe((states) => {
      const initialState: ChartWorkbenchState = states[1];
      const firstChangeState: ChartWorkbenchState = states[2];
      expect(initialState.craftedChart.axis).toBeFalsy();
      expect(firstChangeState.craftedChart.axis.column).toEqual('7');
      expect(firstChangeState.craftedChart.axis.operation).toEqual(ChartOperation.NONE);
      expect(firstChangeState.ui.touched).toBeTruthy();

      const secondChangeState: ChartWorkbenchState = states[3];
      expect(secondChangeState.craftedChart.axis.column).toEqual('13');
      expect(secondChangeState.craftedChart.axis.operation).toEqual(ChartOperation.BY_DAY);

      const thirdChangeState: ChartWorkbenchState = states[4];
      expect(thirdChangeState.craftedChart.axis.column).toEqual('95-1');
      expect(thirdChangeState.craftedChart.axis.operation).toEqual(ChartOperation.NONE);
      done();
    });

    service.initializeBuildingBlocks({
      ...emptyChartColumnProtoByEntityType,
      [CustomChartEntityType.REQUIREMENT_VERSION]: reqVersionColumnPrototype
    }, 1);

    service.changeAxis('7');
    service.changeAxis('13');
    service.changeAxis('95-1');
  });

  it('should change series', (done) => {
    service.state$.pipe(
      scan((all, current) => [...all, current], []),
      skip(4) // skip first emission, initial state and the data loading
    ).subscribe((states) => {
      const initialState: ChartWorkbenchState = states[1];
      const firstChangeState: ChartWorkbenchState = states[2];
      expect(initialState.craftedChart.series).toBeFalsy();
      expect(firstChangeState.craftedChart.series.column).toEqual('7');
      expect(firstChangeState.craftedChart.series.operation).toEqual(ChartOperation.NONE);
      expect(firstChangeState.ui.touched).toBeTruthy();

      const secondChangeState: ChartWorkbenchState = states[3];
      expect(secondChangeState.craftedChart.series.column).toEqual('13');
      expect(secondChangeState.craftedChart.series.operation).toEqual(ChartOperation.BY_DAY);

      const thirdChangeState: ChartWorkbenchState = states[4];
      expect(thirdChangeState.craftedChart.series.column).toEqual('95-1');
      expect(thirdChangeState.craftedChart.series.operation).toEqual(ChartOperation.NONE);
      done();
    });

    service.initializeBuildingBlocks({
      ...emptyChartColumnProtoByEntityType,
      [CustomChartEntityType.REQUIREMENT_VERSION]: reqVersionColumnPrototype
    }, 1);

    service.changeSeries('7');
    service.changeSeries('13');
    service.changeSeries('95-1');
  });

  it('should change measure', (done) => {
    service.state$.pipe(
      scan((all, current) => [...all, current], []),
      skip(4) // skip first emission, initial state and the data loading
    ).subscribe((states) => {
      const initialState: ChartWorkbenchState = states[1];
      const firstChangeState: ChartWorkbenchState = states[2];
      expect(initialState.craftedChart.measure).toBeFalsy();
      expect(firstChangeState.craftedChart.measure.column).toEqual('7');
      expect(firstChangeState.craftedChart.measure.operation).toEqual(ChartOperation.COUNT);
      expect(firstChangeState.ui.touched).toBeTruthy();

      const secondChangeState: ChartWorkbenchState = states[3];
      expect(secondChangeState.craftedChart.measure.column).toEqual('13');
      expect(secondChangeState.craftedChart.measure.operation).toEqual(ChartOperation.COUNT);

      const thirdChangeState: ChartWorkbenchState = states[4];
      expect(thirdChangeState.craftedChart.measure.column).toEqual('95-1');
      expect(thirdChangeState.craftedChart.measure.operation).toEqual(ChartOperation.COUNT);
      done();
    });

    service.initializeBuildingBlocks({
      ...emptyChartColumnProtoByEntityType,
      [CustomChartEntityType.REQUIREMENT_VERSION]: reqVersionColumnPrototype
    }, 1);

    service.changeMeasure('7');
    service.changeMeasure('13');
    service.changeMeasure('95-1');
  });

  it('should change name', (done) => {
    service.state$.pipe(
      scan((all, current) => [...all, current], []),
      skip(2) // skip first emission, initial state and the data loading
    ).subscribe((states) => {
      const initialState: ChartWorkbenchState = states[1];
      const firstChangeState: ChartWorkbenchState = states[2];
      expect(initialState.craftedChart.name).toEqual('New Chart');
      expect(firstChangeState.craftedChart.name).toEqual('new name');
      done();
    });

    service.initializeBuildingBlocks({
      ...emptyChartColumnProtoByEntityType,
      [CustomChartEntityType.REQUIREMENT_VERSION]: reqVersionColumnPrototype
    }, 1);

    service.changeName('new name');
  });

  it('should change axis operation', (done) => {
    service.state$.pipe(
      scan((all, current) => [...all, current], []),
      skip(3) // skip first emission, initial state and the data loading
    ).subscribe((states) => {
      const initialState: ChartWorkbenchState = states[1];
      const loadedAxisState: ChartWorkbenchState = states[2];
      const changedOperationState: ChartWorkbenchState = states[3];
      expect(initialState.craftedChart.axis).toBeFalsy();
      expect(loadedAxisState.craftedChart.axis.operation).toEqual(ChartOperation.NONE);
      expect(changedOperationState.craftedChart.axis.operation).toEqual(ChartOperation.AVG);
      done();
    });

    service.initializeBuildingBlocks({
      ...emptyChartColumnProtoByEntityType,
      [CustomChartEntityType.REQUIREMENT_VERSION]: reqVersionColumnPrototype
    }, 1);

    service.changeAxis('7');
    service.changeAxisOperation(ChartOperation.AVG);
  });

  it('should change measure operation', (done) => {
    service.state$.pipe(
      scan((all, current) => [...all, current], []),
      skip(3) // skip first emission, initial state and the data loading
    ).subscribe((states) => {
      const initialState: ChartWorkbenchState = states[1];
      const loadedAxisState: ChartWorkbenchState = states[2];
      const changedOperationState: ChartWorkbenchState = states[3];
      expect(initialState.craftedChart.measure).toBeFalsy();
      expect(loadedAxisState.craftedChart.measure.operation).toEqual(ChartOperation.COUNT);
      expect(changedOperationState.craftedChart.measure.operation).toEqual(ChartOperation.AVG);
      done();
    });

    service.initializeBuildingBlocks({
      ...emptyChartColumnProtoByEntityType,
      [CustomChartEntityType.REQUIREMENT_VERSION]: reqVersionColumnPrototype
    }, 1);

    service.changeMeasure('7');
    service.changeMeasureOperation(ChartOperation.AVG);
  });

  it('should change series operation', (done) => {
    service.state$.pipe(
      scan((all, current) => [...all, current], []),
      skip(3) // skip first emission, initial state and the data loading
    ).subscribe((states) => {
      const initialState: ChartWorkbenchState = states[1];
      const loadedAxisState: ChartWorkbenchState = states[2];
      const changedOperationState: ChartWorkbenchState = states[3];
      expect(initialState.craftedChart.series).toBeFalsy();
      expect(loadedAxisState.craftedChart.series.operation).toEqual(ChartOperation.NONE);
      expect(changedOperationState.craftedChart.series.operation).toEqual(ChartOperation.AVG);
      done();
    });

    service.initializeBuildingBlocks({
      ...emptyChartColumnProtoByEntityType,
      [CustomChartEntityType.REQUIREMENT_VERSION]: reqVersionColumnPrototype
    }, 1);

    service.changeSeries('7');
    service.changeSeriesOperation(ChartOperation.AVG);
  });

  it('should change chart type and keep axis', (done) => {
    service.state$.pipe(
      scan((all, current) => [...all, current], []),
      skip(5) // skip first emission, initial state and the data loading
    ).subscribe((states) => {
      const finalState: ChartWorkbenchState = states[5];
      expect(finalState.craftedChart.type).toEqual(ChartType.COMPARATIVE);
      expect(finalState.craftedChart.series).toBeFalsy();
      expect(finalState.craftedChart.measure.column).toEqual('7');
      expect(finalState.craftedChart.axis.column).toEqual('13');
      done();
    });

    service.initializeBuildingBlocks({
      ...emptyChartColumnProtoByEntityType,
      [CustomChartEntityType.REQUIREMENT_VERSION]: reqVersionColumnPrototype
    }, 1);

    service.changeType(ChartType.BAR);
    service.changeAxis('13');
    service.changeMeasure('7');
    service.changeType(ChartType.COMPARATIVE);
  });

  it('should change chart type and remove unused axis', (done) => {
    service.state$.pipe(
      scan((all, current) => [...all, current], []),
      skip(6)
    ).subscribe((states) => {
      const finalState: ChartWorkbenchState = states[6];
      expect(finalState.craftedChart.type).toEqual(ChartType.PIE);
      expect(finalState.craftedChart.series).toBeFalsy();
      expect(finalState.craftedChart.measure).toBeFalsy();
      expect(finalState.craftedChart.axis.column).toEqual('13');
      done();
    });

    service.initializeBuildingBlocks({
      ...emptyChartColumnProtoByEntityType,
      [CustomChartEntityType.REQUIREMENT_VERSION]: reqVersionColumnPrototype
    }, 1);

    service.changeType(ChartType.COMPARATIVE);
    service.changeAxis('13');
    service.changeMeasure('7');
    service.changeSeries('95-1');
    service.changeType(ChartType.PIE);
  });

  it('should add filters, modify , and remove', (done) => {
    service.state$.pipe(
      scan((all, current) => [...all, current], []),
      skip(5)
    ).subscribe((states) => {
      const twoFilterState: ChartWorkbenchState = states[3];
      const twoFilterModifiedState: ChartWorkbenchState = states[4];
      const oneFilterState: ChartWorkbenchState = states[5];
      expect(twoFilterState.craftedChart.filters.length).toEqual(2);
      expect(twoFilterState.craftedChart.filters.map(f => f.column)).toEqual(['7', '13']);
      expect(twoFilterState.ui.touched).toBeTruthy();
      expect(twoFilterModifiedState.craftedChart.filters.map(f => f.column)).toEqual(['7', '13']);
      expect(twoFilterModifiedState.craftedChart.filters.map(f => f.value.value)).toEqual(['', 'changedValue']);
      expect(oneFilterState.craftedChart.filters.map(f => f.column)).toEqual(['13']);
      done();
    });

    service.initializeBuildingBlocks({
      ...emptyChartColumnProtoByEntityType,
      [CustomChartEntityType.REQUIREMENT_VERSION]: reqVersionColumnPrototype
    }, 1);

    service.addFilter('7');
    service.addFilter('13');
    service.changeFilter({
      id: '13',
      operation: FilterOperation.NONE,
      filterValue: {kind: 'single-string-value', value: 'changedValue'}
    });
    service.removeFilter('7');
  });

  it('should add several filters', (done) => {
    service.state$.pipe(
      scan((all, current) => [...all, current], []),
      skip(3)
    ).subscribe((states) => {
      const oneFilterState: ChartWorkbenchState = states[2];
      const threeFilterModifiedState: ChartWorkbenchState = states[3];
      expect(oneFilterState.craftedChart.filters.length).toEqual(1);
      expect(oneFilterState.craftedChart.filters.map(f => f.column)).toEqual(['7']);
      expect(oneFilterState.craftedChart.filters.map(f => f.preventOpening)).toEqual([false]);
      expect(oneFilterState.ui.touched).toBeTruthy();
      expect(threeFilterModifiedState.craftedChart.filters.length).toEqual(3);
      expect(threeFilterModifiedState.craftedChart.filters.map(f => f.column)).toEqual(['7', '13', '95-1']);
      expect(threeFilterModifiedState.craftedChart.filters.map(f => f.preventOpening)).toEqual([false, true, true]);
      done();
    });

    service.initializeBuildingBlocks({
      ...emptyChartColumnProtoByEntityType,
      [CustomChartEntityType.REQUIREMENT_VERSION]: reqVersionColumnPrototype
    }, 1);

    service.addFilters(['7']);
    service.addFilters(['13', '95-1']);
  });
});


const reqVersionColumnPrototype = [
  {
    'id': 7,
    'columnType': 'ATTRIBUTE',
    'subQuery': null,
    'label': 'REQUIREMENT_VERSION_ID',
    'specializedType': {
      'entityType': 'REQUIREMENT_VERSION',
      'entityRole': null
    },
    'role': [
      'FILTER',
      'MEASURE',
      'AXIS'
    ],
    'dataType': 'NUMERIC',
    'attributeName': 'id',
    'business': true
  } as ChartColumnPrototype, {
    'id': 13,
    'columnType': 'ATTRIBUTE',
    'subQuery': null,
    'label': 'REQUIREMENT_VERSION_CREATED_ON',
    'specializedType': {
      'entityType': 'REQUIREMENT_VERSION',
      'entityRole': null
    },
    'role': [
      'FILTER',
      'AXIS'
    ],
    'dataType': 'DATE',
    'attributeName': 'audit.createdOn',
    'business': true
  } as ChartColumnPrototype,
  {
    'id': 95,
    'columnType': ChartColumnType.CUF,
    'subQuery': null,
    'label': 'REQUIREMENT_VERSION_CUF_TEXT',
    'specializedType': {
      'entityType': 'REQUIREMENT_VERSION',
      'entityRole': 'CUSTOM_FIELD'
    },
    'role': [
      'FILTER',
      'MEASURE',
      'AXIS'
    ],
    'dataType': ChartDataType.STRING,
    'attributeName': 'value',
    'business': true
  } as ChartColumnPrototype,
];

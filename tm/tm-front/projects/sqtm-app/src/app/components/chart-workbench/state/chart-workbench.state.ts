import {createEntityAdapter, EntityState} from '@ngrx/entity';
import {
  AxisColumn,
  ChartColumnPrototype,
  ChartColumnPrototypesByEntityType,
  ChartDataType,
  ChartDefinitionModel,
  ChartFilter,
  ChartOperation,
  ChartScopeType,
  ChartType,
  CustomChartEntityType,
  DiscreteFilterValue,
  EntityReference,
  EntityType,
  FilterValue,
  MeasureColumn
} from 'sqtm-core';
import {createFeatureSelector, createSelector} from '@ngrx/store';

// front end only type with additional attributes to handle cufs
export type EnhancedColumnPrototype = ChartColumnPrototype & {
  generatedId: string;
  cufId?: number;
  cufLabel?: string;
};

export type EnhancedChartColumnPrototypesByEntityType = { [K in CustomChartEntityType]: EnhancedColumnPrototype[] };

export interface CraftedChartAxis {
  cufId?: number;
  label: string;
  column: string;
  operation: ChartOperation;
}

export interface AxisColumns {
  measureRequired: boolean;
  measure?: MeasureColumn;
  axis: AxisColumn;
  seriesRequired: boolean;
  series?: AxisColumn;
}

interface BaseCraftedChartFilter {
  id: number;
  cufId?: number;
  value: FilterValue;
  operation: ChartOperation;
  // if true, the filter value selector will not be opened immediately
  // user will have to click to open it
  preventOpening?: boolean;
}

// for storage in state, column is normalized, aka designed by id
export interface CraftedChartFilter extends BaseCraftedChartFilter {
  column: string;
}

// for rendering, we need to denormalize columnPrototype
export interface RenderedCraftedChartFilter extends BaseCraftedChartFilter {
  column: EnhancedColumnPrototype;
}

export interface CraftedChart {
  name: string;
  id: number;
  scopeType: ChartScopeType;
  scope: EntityReference[];
  type: ChartType;
  measure: CraftedChartAxis;
  series: CraftedChartAxis;
  axis: CraftedChartAxis;
  filters: CraftedChartFilter[];
  projectId: number;
  // on the front end, projectScope is a number[]
  // on the server it's a string[]... don't ask me why !!!
  projectScope: number[];
}


export interface CraftedChartPreview {
  chartDefinition: Partial<ChartDefinitionModel>;
  isValid: boolean;
  serverErrorMessage?: string;
}

function selectFrontEndColumnPrototypeId(proto: EnhancedColumnPrototype) {
  return proto.generatedId;
}

export const chartColumnPrototypeAdapter = createEntityAdapter<EnhancedColumnPrototype>({
  selectId: selectFrontEndColumnPrototypeId
});

export interface ChartWorkbenchUiState {
  showAttributeSelector: boolean;
  allowAttributeSelector: boolean;
  touched: boolean;
}

export interface ChartWorkbenchState {
  chartColumnPrototypes: EntityState<EnhancedColumnPrototype>;
  chartColumnPrototypesByEntityType: { [K in CustomChartEntityType]: string[] };
  // this one contains the raw columns prototype as they are given by server at page initialization
  // we need to keep them as we can have to regenerate the cuf columns when scope change...
  rawColumnsPrototypesByEntity: ChartColumnPrototypesByEntityType;
  craftedChart: CraftedChart;
  ui: ChartWorkbenchUiState;
}

export const emptyChartColumnProtoByEntityType = {
  [CustomChartEntityType.REQUIREMENT]: [],
  [CustomChartEntityType.REQUIREMENT_VERSION]: [],
  [CustomChartEntityType.TEST_CASE]: [],
  [CustomChartEntityType.CAMPAIGN]: [],
  [CustomChartEntityType.ITERATION]: [],
  [CustomChartEntityType.ITEM_TEST_PLAN]: [],
  [CustomChartEntityType.EXECUTION]: [],
};
export const initialChartWorkbenchState: Readonly<ChartWorkbenchState> = {
  chartColumnPrototypes: chartColumnPrototypeAdapter.getInitialState(),
  chartColumnPrototypesByEntityType: {...emptyChartColumnProtoByEntityType},
  rawColumnsPrototypesByEntity: {...emptyChartColumnProtoByEntityType},
  craftedChart: {
    id: null,
    name: 'New Chart',
    type: ChartType.PIE,
    axis: null,
    filters: [],
    measure: null,
    series: null,
    scopeType: ChartScopeType.DEFAULT,
    scope: [],
    projectId: null,
    projectScope: []
  },
  ui: {
    showAttributeSelector: true,
    allowAttributeSelector: true,
    touched: false,
  }
};

const craftedChartSelector = createFeatureSelector<CraftedChart>('craftedChart');

const uiStateSelector = createFeatureSelector<ChartWorkbenchUiState>('ui');

const columnPrototypesSelector = createFeatureSelector<EntityState<EnhancedColumnPrototype>>('chartColumnPrototypes');

const columnPrototypesByEntitySelector =
  createFeatureSelector<{ [K in CustomChartEntityType]: string[] }>('chartColumnPrototypesByEntityType');




export const selectOrderedColumnPrototypes = createSelector(columnPrototypesSelector, columnPrototypesByEntitySelector,
  (columnProtoState, columnProtoByType) => {
    return Object.values<CustomChartEntityType>(CustomChartEntityType).reduce((acc: ChartColumnPrototypesByEntityType, entityType) => {
      acc[entityType] = getColumnPrototypeByEntityType(entityType, columnProtoByType, columnProtoState);
      return acc;
    }, {...emptyChartColumnProtoByEntityType});
  });

function findSourceEntityTypes(entityType: CustomChartEntityType): string[] {
  // We want attribute with entity type 'AUTOMATION_REQUEST' to be grouped with test case attributes
  if (entityType === CustomChartEntityType.TEST_CASE) {
    return [EntityType.TEST_CASE, EntityType.AUTOMATION_REQUEST];
  } else {
    return [entityType];
  }
}

function getColumnPrototypeByEntityType(entityType: CustomChartEntityType,
                                        columnProtoByType,
                                        columnProtoState: EntityState<EnhancedColumnPrototype>) {
  const sourceEntityTypes = findSourceEntityTypes(entityType);
  const columns: EnhancedColumnPrototype[] = [];
  sourceEntityTypes.map(sourceEntity => {
    columns.push(...columnProtoByType[sourceEntity].map(id => columnProtoState.entities[id]));
  });
  const noCufColumns = columns.filter(column => column.cufId == null);
  const cufColumns = columns.filter(column => column.cufId != null);
  return noCufColumns.concat(...cufColumns);
}

export const selectMeasure = createSelector(craftedChartSelector, columnPrototypesSelector, (craftedChart, columnProtoState) => {
  let measure: MeasureColumn = null;
  if (craftedChart.measure) {
    measure = {
      label: '',
      operation: craftedChart.measure.operation,
      column: columnProtoState.entities[craftedChart.measure.column]
    };
  }
  return measure;
});

export const selectChartScope = createSelector<ChartWorkbenchState, [CraftedChart], CompleteScope>(craftedChartSelector, (craftedChart) => {
  return craftedChart;
});

const selectCraftedChartFilters =
  createSelector<ChartWorkbenchState, [CraftedChart], CraftedChartFilter[]>(craftedChartSelector, (craftedChart) => {
    return craftedChart.filters;
  });

function convertOneAxis(craftedChartAxis: CraftedChartAxis,
                        columnProtoState: EntityState<EnhancedColumnPrototype>) {
  if (Boolean(craftedChartAxis)) {
    const columnPrototype = columnProtoState.entities[craftedChartAxis.column];
    return {
      label: '',
      operation: craftedChartAxis.operation,
      column: columnPrototype,
      cufId: columnPrototype.cufId
    };
  }
  return null;
}

function convertToColumnAxes(craftedChart: CraftedChart, columnProtoState: EntityState<EnhancedColumnPrototype>) {
  const axes: AxisColumn[] = [];
  if (Boolean(craftedChart.axis)) {
    axes.push(convertOneAxis(craftedChart.axis, columnProtoState));
  }
  if (Boolean(craftedChart.series)) {
    axes.push(convertOneAxis(craftedChart.series, columnProtoState));
  }
  return axes;
}

function doesMeasureIsRequired(craftedChart: CraftedChart) {
  return craftedChart.type !== ChartType.PIE;
}

function doesSeriesAreRequired(craftedChart: CraftedChart) {
  return craftedChart.type === ChartType.COMPARATIVE || craftedChart.type === ChartType.TREND;
}

export const selectAxis = createSelector<ChartWorkbenchState, [CraftedChart, EntityState<EnhancedColumnPrototype>], AxisColumns>
(craftedChartSelector, columnPrototypesSelector, (craftedChart, columnProtoState) => {
  const axes: AxisColumns = {
    measureRequired: doesMeasureIsRequired(craftedChart),
    seriesRequired: doesSeriesAreRequired(craftedChart),
    measure: convertOneAxis(craftedChart.measure, columnProtoState),
    axis: convertOneAxis(craftedChart.axis, columnProtoState),
    series: convertOneAxis(craftedChart.series, columnProtoState),
  };
  return axes;
});

export const selectFilters =
  createSelector<ChartWorkbenchState, [CraftedChartFilter[], EntityState<EnhancedColumnPrototype>], RenderedCraftedChartFilter[]>
  (selectCraftedChartFilters, columnPrototypesSelector, (craftedFilters, columnProtoState) => {
    return craftedFilters
      .map(craftedFilter => {
        const column = columnProtoState.entities[craftedFilter.column];
        return {...craftedFilter, column};
      });
  });

export const selectChartType = createSelector(craftedChartSelector, (craftedChart) => craftedChart.type);

function isChartValid(craftedChart: CraftedChart): boolean {
  if (craftedChart.scopeType !== ChartScopeType.DEFAULT && craftedChart.scope.length === 0) {
    return false;
  }
  if (!craftedChart.name || craftedChart.name.trim().length === 0) {
    return false;
  }
  switch (craftedChart.type) {
    case ChartType.PIE:
      return Boolean(craftedChart.axis);
    case ChartType.BAR:
    case ChartType.CUMULATIVE:
      return Boolean(craftedChart.measure) && Boolean(craftedChart.axis);
    case ChartType.TREND:
    case ChartType.COMPARATIVE:
      return Boolean(craftedChart.measure) && Boolean(craftedChart.axis) && Boolean(craftedChart.series);
    default:
      return Boolean(craftedChart.axis);
  }

}


function convertToColumnMeasures(craftedChart: CraftedChart,
                                 columnProtoState: EntityState<EnhancedColumnPrototype>,
                                 columnPrototypesByEntity: EnhancedChartColumnPrototypesByEntityType) {
  if (craftedChart.type === ChartType.PIE) {
    return generatePieChartMeasure(craftedChart, columnProtoState, columnPrototypesByEntity);
  } else {
    return [convertOneAxis(craftedChart.measure, columnProtoState)];
  }
}

function generatePieChartMeasure(craftedChart: CraftedChart,
                                 columnProtoState: EntityState<EnhancedColumnPrototype>,
                                 columnPrototypesByEntity: EnhancedChartColumnPrototypesByEntityType) {
  if (Boolean(craftedChart.axis)) {
    const columnPrototype = columnProtoState.entities[craftedChart.axis.column];
    const measurePrototypes: EnhancedColumnPrototype[] = Object.values<EnhancedColumnPrototype[]>(columnPrototypesByEntity)
      .find((columnPrototypes: EnhancedColumnPrototype[]) => columnPrototypes.map(p => p.id).includes(columnPrototype.id));
    const measureColumn: MeasureColumn = {
      column: measurePrototypes[0],
      operation: ChartOperation.COUNT,
      label: '',
    };
    return [measureColumn];
  } else {
    return [];
  }
}

function convertFilterValue(filterValue: FilterValue, dataType: ChartDataType) {
  let convertedValue = [];
  switch (filterValue.kind) {
    case 'single-date-value':
    case 'single-string-value':
    case 'single-numeric-value':
      const strValue = filterValue.value as string;
      if (strValue && strValue.length > 0) {
        convertedValue.push(strValue);
      }
      break;
    case 'multiple-string-value':
    case 'multiple-date-value':
      convertedValue.push(...filterValue.value as any[]);
      break;
    case 'multiple-discrete-value':
      const multipleValue = filterValue.value as DiscreteFilterValue[];
      convertedValue.push(...multipleValue.map(v => v.id.toString()));
      // can't believe that ****. For cuf checkbox filtering the server ignore "true", it require "TRUE"...
      // don't want to break persisted chart and don't want to make a database migration script just for that,
      // so we will have to handle that **** front end...
      if (dataType === ChartDataType.BOOLEAN_AS_STRING) {
        convertedValue = convertedValue.map(v => v.toUpperCase());
      }
      break;
  }
  return convertedValue;
}

function convertFilters(craftedChart: CraftedChart, columnProtoState: EntityState<EnhancedColumnPrototype>): ChartFilter[] {
  return craftedChart.filters
    .map(craftedFilter => {
      const column = columnProtoState.entities[craftedFilter.column];
      const values: string[] = convertFilterValue(craftedFilter.value, column.dataType);
      return {
        id: craftedFilter.id,
        cufId: craftedFilter.cufId,
        operation: craftedFilter.operation,
        column,
        values
      };
    })
    .filter(f => f.values.length > 0);
}

export const selectChartPreview =
  // tslint:disable-next-line:max-line-length
  createSelector<ChartWorkbenchState, [CraftedChart, EntityState<EnhancedColumnPrototype>, EnhancedChartColumnPrototypesByEntityType], CraftedChartPreview>
  (craftedChartSelector, columnPrototypesSelector, selectOrderedColumnPrototypes,
    (craftedChart, columnProtoState, columnPrototypesByEntity) => {
      const chartPreview: CraftedChartPreview = {
        chartDefinition: {
          type: craftedChart.type,
          name: craftedChart.name || '',
          axis: convertToColumnAxes(craftedChart, columnProtoState),
          measures: convertToColumnMeasures(craftedChart, columnProtoState, columnPrototypesByEntity),
          scope: craftedChart.scope,
          projectScope: craftedChart.projectScope.map(String),
          scopeType: craftedChart.scopeType,
          filters: convertFilters(craftedChart, columnProtoState),
          projectId: craftedChart.projectId
        },
        isValid: isChartValid(craftedChart)
      };
      return chartPreview;
    });

export const selectShowAttributeSelector = createSelector<ChartWorkbenchState, [ChartWorkbenchUiState], boolean>
(uiStateSelector, uiState => uiState.showAttributeSelector && uiState.allowAttributeSelector);

export const selectAllowAttributeSelector = createSelector<ChartWorkbenchState, [ChartWorkbenchUiState], boolean>
(uiStateSelector, uiState => uiState.allowAttributeSelector);

export const selectMessages = createSelector<ChartWorkbenchState, [ChartWorkbenchUiState], ChartWorkbenchHintMessages>
(uiStateSelector, uiState => {
  return {
    showHintMessage: !uiState.touched,
    showDndHintMessage: !uiState.touched && uiState.allowAttributeSelector
  };
});

export enum DetailedAxisRole {
  Measure = 'Measure',
  Axis = 'Axis',
  Series = 'Series'
}

export type CompleteScope = Pick<CraftedChart, 'scope' | 'scopeType' | 'projectScope'>;

export interface ChartWorkbenchHintMessages {
  showHintMessage: boolean;
  showDndHintMessage: boolean;
}

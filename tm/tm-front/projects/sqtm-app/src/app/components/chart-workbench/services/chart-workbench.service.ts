import {Injectable} from '@angular/core';
import {
  BindableEntity,
  ChangeFilteringAction,
  ChartColumnPrototype,
  ChartColumnPrototypesByEntityType,
  ChartColumnRole,
  ChartColumnType,
  ChartDataType,
  ChartDefinitionModel,
  ChartFilter,
  ChartOperation,
  ChartType,
  createStore,
  CustomChartEntityType,
  CustomFieldBindingData,
  DiscreteFilterValue,
  EntityReference,
  EntityType,
  filterAvailableChartOperations,
  FilterValue,
  InfoList,
  InputType,
  isSystemInfoList,
  MilestoneModeData,
  ProjectDataMap,
  ReferentialDataService,
  ResearchColumnPrototype,
  SearchColumnPrototypeUtils
} from 'sqtm-core';
import {
  chartColumnPrototypeAdapter,
  ChartWorkbenchState,
  CompleteScope,
  CraftedChartAxis,
  CraftedChartFilter,
  EnhancedColumnPrototype,
  initialChartWorkbenchState
} from '../state/chart-workbench.state';
import {filter, map, take, withLatestFrom} from 'rxjs/operators';
import * as _ from 'lodash';
import {chartWorkbenchLogger} from '../chart-workbench.logger';
import {EntityState} from '@ngrx/entity';
import {combineLatest} from 'rxjs';

const logger = chartWorkbenchLogger.compose('ChartWorkbenchService');

export type EnhancedColumnPrototypesByEntityType = { [K in CustomChartEntityType]: EnhancedColumnPrototype[] };
export type ColumnPrototypesByDataType = { [K in ChartDataType]: ChartColumnPrototype };

@Injectable()
export class ChartWorkbenchService {

  private readonly mileStoneRelatedColumnPrototypes = ['REQUIREMENT_VERSION_MILCOUNT', 'TEST_CASE_MILCOUNT'];

  private store = createStore<ChartWorkbenchState>(initialChartWorkbenchState, {
    id: 'ChartWorkbench',
    logDiff: 'simple'
  });

  public state$ = this.store.state$;

  private readonly chartInputTypeByCufType: Partial<{ [K in InputType]: ChartDataType }> = {
    [InputType.CHECKBOX]: ChartDataType.BOOLEAN_AS_STRING,
    [InputType.DATE_PICKER]: ChartDataType.DATE_AS_STRING,
    [InputType.DROPDOWN_LIST]: ChartDataType.LIST,
    [InputType.NUMERIC]: ChartDataType.NUMERIC,
    [InputType.PLAIN_TEXT]: ChartDataType.STRING,
    [InputType.TAG]: ChartDataType.TAG,
  };

  private readonly bindableEntityByChartEntity: Partial<{ [K in CustomChartEntityType]: BindableEntity }> = {
    [CustomChartEntityType.REQUIREMENT_VERSION]: BindableEntity.REQUIREMENT_VERSION,
    [CustomChartEntityType.TEST_CASE]: BindableEntity.TEST_CASE,
    [CustomChartEntityType.CAMPAIGN]: BindableEntity.CAMPAIGN,
    [CustomChartEntityType.ITERATION]: BindableEntity.ITERATION,
    [CustomChartEntityType.EXECUTION]: BindableEntity.EXECUTION,
  };

  constructor(private referentialDataService: ReferentialDataService) {
  }

  initializeBuildingBlocks(rawColumnsPrototypesByEntity: ChartColumnPrototypesByEntityType, projectId: number) {
    this.state$.pipe(
      take(1),
      withLatestFrom(this.referentialDataService.projectDatas$, this.referentialDataService.milestoneModeData$),
      map(([state, projectDataMap, milestoneModeData]: [ChartWorkbenchState, ProjectDataMap, MilestoneModeData]) => {
        const projectIds = [projectId];
        const transformedColumnPrototypes = this.enhanceColumnPrototypes(
          rawColumnsPrototypesByEntity,
          projectIds,
          projectDataMap,
          milestoneModeData);
        const chartColumnPrototypes =
          chartColumnPrototypeAdapter.setAll(Object.values(transformedColumnPrototypes).flat(), state.chartColumnPrototypes);
        const chartColumnPrototypesByEntityType = this.normalizeColumnProtoByEntityType(transformedColumnPrototypes);
        const scope = [new EntityReference(projectId, EntityType.PROJECT)];
        return {
          ...state,
          craftedChart: {...state.craftedChart, scope, projectId, projectScope: [projectId]},
          chartColumnPrototypes,
          chartColumnPrototypesByEntityType,
          rawColumnsPrototypesByEntity
        };
      })
    ).subscribe(state => this.store.commit(state));
  }

  changeScope(completeScope: CompleteScope) {
    this.state$.pipe(
      take(1),
      withLatestFrom(this.referentialDataService.projectDatas$, this.referentialDataService.milestoneModeData$),
      map(([state, projectDataMap, milestoneModeData]: [ChartWorkbenchState, ProjectDataMap, MilestoneModeData]) => {
        const transformedColumnPrototypes = this.enhanceColumnPrototypes(
          state.rawColumnsPrototypesByEntity,
          completeScope.projectScope,
          projectDataMap,
          milestoneModeData);
        const chartColumnPrototypes =
          chartColumnPrototypeAdapter.setAll(Object.values(transformedColumnPrototypes).flat(), state.chartColumnPrototypes);
        const chartColumnPrototypesByEntityType = this.normalizeColumnProtoByEntityType(transformedColumnPrototypes);
        const measure = this.filterAxisAfterScopeChanged(chartColumnPrototypes, state.craftedChart.measure);
        const axis = this.filterAxisAfterScopeChanged(chartColumnPrototypes, state.craftedChart.axis);
        const series = this.filterAxisAfterScopeChanged(chartColumnPrototypes, state.craftedChart.series);
        const filters = this.removeFiltersAfterScopeChanged(chartColumnPrototypes, state.craftedChart.filters);
        return {
          ...state,
          chartColumnPrototypes,
          chartColumnPrototypesByEntityType,
          craftedChart: {
            ...state.craftedChart,
            measure,
            axis,
            series,
            ...completeScope,
            filters
          }
        };
      })
    ).subscribe(state => this.store.commit(state));
  }

  private filterAxisAfterScopeChanged(chartColumnPrototypes: EntityState<EnhancedColumnPrototype>, originalAxis: CraftedChartAxis) {
    const chartColumnPrototypeEntities = chartColumnPrototypes.entities;
    const keepAxis = originalAxis && chartColumnPrototypeEntities[originalAxis.column];
    return keepAxis ? originalAxis : null;
  }

  changeAxis(id: string) {
    this.state$.pipe(
      take(1),
      map((state: ChartWorkbenchState) => this.doChangeAxis(state, id))
    ).subscribe(state => this.store.commit(state));
  }

  private doChangeAxis(state: ChartWorkbenchState, id: string) {
    const axis = this.createAxisFromColumnPrototypeId(state, id);
    return {...state, craftedChart: {...state.craftedChart, axis}, ui: {...state.ui, touched: true}};
  }

  private createAxisFromColumnPrototypeId(state: ChartWorkbenchState, id: string) {
    const columnPrototype = state.chartColumnPrototypes.entities[id];
    const axis: CraftedChartAxis = {
      column: id,
      label: '',
      operation: filterAvailableChartOperations(ChartColumnRole.AXIS, columnPrototype.dataType)[0]
    };
    return axis;
  }

  changeMeasure(id: string) {
    this.state$.pipe(
      take(1),
      map((state: ChartWorkbenchState) => this.doChangeMeasure(state, id))
    ).subscribe(state => this.store.commit(state));
  }

  private doChangeMeasure(state: ChartWorkbenchState, id: string) {
    const measure = this.createMeasureFromColumnId(state, id);
    return {...state, craftedChart: {...state.craftedChart, measure}, ui: {...state.ui, touched: true}};
  }

  private createMeasureFromColumnId(state: ChartWorkbenchState, id: string) {
    const columnPrototype = state.chartColumnPrototypes.entities[id];
    const measure: CraftedChartAxis = {
      column: id,
      label: '',
      operation: filterAvailableChartOperations(ChartColumnRole.MEASURE, columnPrototype.dataType)[0]
    };
    return measure;
  }

  changeSeries(id: string) {
    this.state$.pipe(
      take(1),
      map((state: ChartWorkbenchState) => {
        return this.doChangeSeries(state, id);
      })
    ).subscribe(state => this.store.commit(state));
  }

  private doChangeSeries(state: ChartWorkbenchState, id: string) {
    const columnPrototype = state.chartColumnPrototypes.entities[id];
    const series: CraftedChartAxis = {
      column: id,
      label: '',
      operation: filterAvailableChartOperations(ChartColumnRole.AXIS, columnPrototype.dataType)[0]
    };
    return {...state, craftedChart: {...state.craftedChart, series}, ui: {...state.ui, touched: true}};
  }

  changeName(name: string) {
    this.state$.pipe(
      take(1),
      map((state: ChartWorkbenchState) => {
        return {...state, craftedChart: {...state.craftedChart, name}};
      })
    ).subscribe(state => this.store.commit(state));
  }

  changeAxisOperation(operation: ChartOperation) {
    this.state$.pipe(
      take(1),
      map((state: ChartWorkbenchState) => {
        return {...state, craftedChart: {...state.craftedChart, axis: {...state.craftedChart.axis, operation}}};
      })
    ).subscribe(state => this.store.commit(state));
  }

  changeMeasureOperation(operation: ChartOperation) {
    this.state$.pipe(
      take(1),
      map((state: ChartWorkbenchState) => {
        return {...state, craftedChart: {...state.craftedChart, measure: {...state.craftedChart.measure, operation}}};
      })
    ).subscribe(state => this.store.commit(state));
  }

  changeSeriesOperation(operation: ChartOperation) {
    this.state$.pipe(
      take(1),
      map((state: ChartWorkbenchState) => {
        return {...state, craftedChart: {...state.craftedChart, series: {...state.craftedChart.series, operation}}};
      })
    ).subscribe(state => this.store.commit(state));
  }

  changeType(type: ChartType) {
    this.state$.pipe(
      take(1),
      map((state: ChartWorkbenchState) => {
        const shouldPreserveMeasure = type !== ChartType.PIE;
        const shouldPreserveSeries = type === ChartType.TREND || type === ChartType.COMPARATIVE;
        const measure = shouldPreserveMeasure ? state.craftedChart.measure : null;
        const series = shouldPreserveSeries ? state.craftedChart.series : null;
        return {
          ...state, craftedChart: {
            ...state.craftedChart,
            measure,
            series,
            type
          }
        };
      })
    ).subscribe(state => this.store.commit(state));
  }

  dropColumns(ids: string[]) {
    this.state$.pipe(
      take(1),
      map((state: ChartWorkbenchState) => {
        const chartType: ChartType = state.craftedChart.type;
        switch (chartType) {
          case ChartType.PIE:
            return this.handleDropInPieChart(state, ids);
          case ChartType.BAR:
          case ChartType.CUMULATIVE:
            return this.handleDropInTwoAxisChart(state, ids);
          case ChartType.COMPARATIVE:
          case ChartType.TREND:
            return this.handleDropInThreeAxisChart(state, ids);
          default:
            throw Error(`Unknown type ${chartType}`);
        }
      })
    ).subscribe(state => this.store.commit(state));
  }

  private handleDropInPieChart(state: ChartWorkbenchState, ids: string[]) {
    return this.doChangeAxis(state, ids[0]);
  }

  private handleDropInTwoAxisChart(state: ChartWorkbenchState, ids: string[]) {
    const measure = !state.craftedChart.measure ? this.createMeasureFromColumnId(state, ids.shift()) : state.craftedChart.measure;
    const shouldAddAxis = !state.craftedChart.axis && ids.length > 0;
    const axis = shouldAddAxis ? this.createAxisFromColumnPrototypeId(state, ids.shift()) : state.craftedChart.axis;
    return {...state, craftedChart: {...state.craftedChart, measure, axis}, ui: {...state.ui, touched: true}};
  }

  private handleDropInThreeAxisChart(state: ChartWorkbenchState, ids: string[]) {
    const measure = !state.craftedChart.measure ? this.createMeasureFromColumnId(state, ids.shift()) : state.craftedChart.measure;
    const shouldAddAxis = !state.craftedChart.axis && ids.length > 0;
    const axis = shouldAddAxis ? this.createAxisFromColumnPrototypeId(state, ids.shift()) : state.craftedChart.axis;
    const shouldAddSeries = !state.craftedChart.series
      && ids.length > 0 && [ChartType.TREND, ChartType.COMPARATIVE].includes(state.craftedChart.type);
    const series = shouldAddSeries ? this.createAxisFromColumnPrototypeId(state, ids.shift()) : state.craftedChart.series;
    return {...state, craftedChart: {...state.craftedChart, measure, axis, series}, ui: {...state.ui, touched: true}};
  }

  addFilter(id: string) {
    this.state$.pipe(
      take(1),
      filter(state => !state.craftedChart.filters.map(f => f.column).includes(id)),
      map((state: ChartWorkbenchState) => {
        return this.doAddFilter(state, id);
      })).subscribe(state => this.store.commit(state));
  }

  addFilters(ids: string[]) {
    this.state$.pipe(
      take(1),
      map((state: ChartWorkbenchState) => {
        const alreadyFilteredColumns = state.craftedChart.filters.map(f => f.column);
        const newIds = _.difference<string>(ids, alreadyFilteredColumns);
        return newIds.reduce((previousState, id) => {
          return this.doAddFilter(previousState, id, newIds.length > 1);
        }, state);
      })).subscribe(state => this.store.commit(state));
  }

  private doAddFilter(state: ChartWorkbenchState, id: string, preventOpening = false) {
    const columnPrototype: EnhancedColumnPrototype = state.chartColumnPrototypes.entities[id];
    const firstOperation = filterAvailableChartOperations(ChartColumnRole.FILTER, columnPrototype.dataType)[0];
    const newFilter: CraftedChartFilter = {
      id: null,
      column: id,
      operation: firstOperation,
      cufId: columnPrototype.cufId,
      value: this.generateInitialValue(columnPrototype, firstOperation),
      preventOpening
    };
    const filters = [...state.craftedChart.filters, newFilter];
    return {...state, craftedChart: {...state.craftedChart, filters}, ui: {...state.ui, touched: true}};
  }

  changeFilter(changeFilteringAction: ChangeFilteringAction) {
    logger.debug(`Changing filter ${changeFilteringAction.id}`, [changeFilteringAction]);
    const id = changeFilteringAction.id as string;
    this.state$.pipe(
      take(1),
      filter(state => state.craftedChart.filters.map(f => f.column).includes(id)),
      map((state: ChartWorkbenchState) => {
        const filters = [...state.craftedChart.filters];
        const index = filters.findIndex(f => f.column === id);
        const nextFilter = {...filters[index]};
        nextFilter.operation = ChartOperation[changeFilteringAction.operation] || nextFilter.operation;
        nextFilter.value = changeFilteringAction.filterValue || nextFilter.value;
        filters[index] = nextFilter;
        return {...state, craftedChart: {...state.craftedChart, filters}};
      })).subscribe(state => this.store.commit(state));
  }

  removeFilter(id: string) {
    this.state$.pipe(
      take(1),
      filter(state => state.craftedChart.filters.map(f => f.column).includes(id)),
      map((state: ChartWorkbenchState) => {
        const filters = [...state.craftedChart.filters];
        const index = filters.findIndex(f => f.column === id);
        filters.splice(index, 1);
        return {...state, craftedChart: {...state.craftedChart, filters}};
      })).subscribe(state => this.store.commit(state));
  }

  toggleAttributeSelector() {
    this.state$.pipe(
      take(1),
      map((state: ChartWorkbenchState) => {
        return {...state, ui: {...state.ui, showAttributeSelector: !state.ui.showAttributeSelector}};
      })).subscribe(state => this.store.commit(state));
  }

  allowAttributeSelector(allowAttributeSelector: boolean) {
    this.state$.pipe(
      take(1),
      map((state: ChartWorkbenchState) => {
        return {...state, ui: {...state.ui, allowAttributeSelector, showAttributeSelector: allowAttributeSelector}};
      })).subscribe(state => this.store.commit(state));
  }

  private generateInitialValue(chartColumnPrototype: EnhancedColumnPrototype, operation: ChartOperation) {
    const dataType = chartColumnPrototype.dataType;
    let filterValue: FilterValue;
    switch (dataType) {
      case ChartDataType.NUMERIC:
      case ChartDataType.STRING:
        filterValue = {
          kind: 'single-string-value',
          value: ''
        };
        break;
      case ChartDataType.DATE:
      case ChartDataType.DATE_AS_STRING:
        const isMultipleValue = operation === ChartOperation.BETWEEN;
        filterValue = {
          kind: isMultipleValue ? 'multiple-date-value' : 'single-date-value',
          value: isMultipleValue ? [] : ''
        };
        break;
      case ChartDataType.INFO_LIST_ITEM:
      case ChartDataType.LEVEL_ENUM:
      case ChartDataType.EXECUTION_STATUS:
      case ChartDataType.REQUIREMENT_STATUS:
      case ChartDataType.LIST:
      case ChartDataType.TAG:
      case ChartDataType.BOOLEAN_AS_STRING:
      case ChartDataType.BOOLEAN:
      case ChartDataType.AUTOMATED_TEST_TECHNOLOGY:
        filterValue = {
          kind: 'multiple-discrete-value',
          value: [],
        };
        break;
      default:
        throw Error(`No initial value for type ${dataType}`);
    }
    return filterValue;
  }

  private enhanceColumnPrototypes(rawColumnsPrototypesByEntity: ChartColumnPrototypesByEntityType,
                                  projectIds: number[],
                                  projectDataMap: ProjectDataMap,
                                  milestoneModeData: MilestoneModeData): EnhancedColumnPrototypesByEntityType {
    return _.transform<any, { [K in CustomChartEntityType]: EnhancedColumnPrototype[] }>(rawColumnsPrototypesByEntity,
      (result, protoForOneEntity: ChartColumnPrototype[], entityType: CustomChartEntityType) => {
        const bindableEntity = this.bindableEntityByChartEntity[entityType];
        const cufColumnProtoByDataType = this.getCufColumnProtoByDataType(protoForOneEntity);

        if (bindableEntity) {
          const generatedColumnPrototypes = this.generateCufColumnPrototypes(
            projectIds,
            projectDataMap,
            bindableEntity,
            cufColumnProtoByDataType);
          const nativePrototypes: EnhancedColumnPrototype[] = this.enhanceNativeColumnPrototypes(protoForOneEntity, milestoneModeData);
          result[entityType] = [...nativePrototypes, ...generatedColumnPrototypes];
        } else {
          const nativePrototypes: EnhancedColumnPrototype[] = this.enhanceNativeColumnPrototypes(protoForOneEntity, milestoneModeData);
          result[entityType] = [...nativePrototypes];
        }
      });
  }

  private enhanceNativeColumnPrototypes(protoForOneEntity: ChartColumnPrototype[],
                                        milestoneModeData: MilestoneModeData): EnhancedColumnPrototype[] {
    return protoForOneEntity
      .filter(p => this.filterColumnProtoForMilestoneActivated(milestoneModeData, p))
      .filter(p => p.columnType !== ChartColumnType.CUF)
      .map(p => ({...p, generatedId: `${p.id}`}));
  }


  private filterColumnProtoForMilestoneActivated(milestoneModeData: MilestoneModeData, p: ChartColumnPrototype) {
    if (milestoneModeData.milestoneFeatureEnabled) {
      return true;
    } else {
      // [SQUASH-2733] Hide milestone ColumnPrototypes if milestones are disabled.
      // As ColumnPrototypes do not include proper model to says if they are milestone related, we rely on ids here...
      return !this.mileStoneRelatedColumnPrototypes.includes(p.label);
    }
  }

  private generateCufColumnPrototypes(projectIds: number[],
                                      projectDataMap: ProjectDataMap,
                                      bindableEntity: BindableEntity,
                                      cufColumnProtoByDataType: ColumnPrototypesByDataType): EnhancedColumnPrototype[] {
    let generatedColumnPrototypes: EnhancedColumnPrototype[] = projectIds
      .map(id => projectDataMap[id])
      .filter(p => Boolean(p))
      .map(p => p.customFieldBinding[bindableEntity] as CustomFieldBindingData[])
      .flat()
      .map(binding => binding.customField)
      .filter(cuf => cuf.inputType !== InputType.RICH_TEXT)
      .map(cuf => {
        const chartDataType = this.chartInputTypeByCufType[cuf.inputType];
        const cufColumnProto: ChartColumnPrototype = cufColumnProtoByDataType[chartDataType];
        const generatedColumnProto: EnhancedColumnPrototype = {
          ...cufColumnProto,
          cufId: cuf.id,
          generatedId: `${cufColumnProto.id}-${cuf.id}`,
          cufLabel: cuf.label,
        };

        return generatedColumnProto;
      });

    generatedColumnPrototypes = _.uniqBy(generatedColumnPrototypes, p => p.generatedId);
    logger.debug(`Generated cuf prototypes for one entity type : `, [generatedColumnPrototypes]);
    return generatedColumnPrototypes;
  }

  private getCufColumnProtoByDataType(protoForOneEntity: ChartColumnPrototype[]): ColumnPrototypesByDataType {
    const cufPrototypesByDataTypes = _.groupBy(protoForOneEntity.filter(p => p.columnType === ChartColumnType.CUF), p => p.dataType);
    return _.transform(cufPrototypesByDataTypes, (result, value, key) => {
      result[key] = value[0];
    });
  }

  private normalizeColumnProtoByEntityType(rawColumnsPrototypesByEntity: EnhancedColumnPrototypesByEntityType) {
    return _.transform<any, { [K in CustomChartEntityType]: string[] }>(rawColumnsPrototypesByEntity,
      function (result, value, key) {
        result[key] = value.map(ccp => ccp.generatedId);
      });
  }

  loadExistingChart(chart: ChartDefinitionModel) {
    const completeScope = this.getCompleteScopeFromExistingChart(chart);
    combineLatest([
      this.state$,
      this.referentialDataService.projectDatas$,
      this.referentialDataService.infoLists$,
      this.referentialDataService.milestoneModeData$]).pipe(
      take(1),
      map(([state, projectDataMap, infoLists, milestoneModeData]: [ChartWorkbenchState, ProjectDataMap, InfoList[], MilestoneModeData]) => {

        const transformedColumnPrototypes = this.enhanceColumnPrototypes(
          state.rawColumnsPrototypesByEntity,
          completeScope,
          projectDataMap,
          milestoneModeData);
        const chartColumnPrototypes =
          chartColumnPrototypeAdapter.setAll(Object.values(transformedColumnPrototypes).flat(), state.chartColumnPrototypes);
        const chartColumnPrototypesByEntityType = this.normalizeColumnProtoByEntityType(transformedColumnPrototypes);
        return {
          ...state,
          craftedChart: {
            name: chart.name,
            id: chart.id,
            scopeType: chart.scopeType,
            scope: chart.scope,
            type: chart.type,
            measure: this.getMeasureFromExistingChart(chartColumnPrototypes, chart),
            series: this.getSeriesFromExistingChart(chartColumnPrototypes, chart),
            axis: this.getAxisFromExistingChart(chartColumnPrototypes, chart),
            filters: this.getFiltersFromExistingChart(chartColumnPrototypes, chart, infoLists),
            projectId: chart.projectId,
            projectScope: chart.projectScope.map(Number),
          },
          chartColumnPrototypes,
          chartColumnPrototypesByEntityType,
        };
      })
    ).subscribe(state => {
      this.store.commit(state);
    });
  }

  private getCompleteScopeFromExistingChart(chart: ChartDefinitionModel): number[] {
    const completeScope = chart.projectScope.map(Number);
    if (!completeScope.includes(chart.projectId)) {
      completeScope.push(chart.projectId);
    }
    return completeScope;
  }

  private getAxisFromExistingChart(chartColumnPrototypes: EntityState<EnhancedColumnPrototype>, chart: ChartDefinitionModel) {
    return {
      cufId: chart.axis[0].cufId,
      label: chart.axis[0].label,
      column: this.findColumnIdFromExistingChart(chartColumnPrototypes, chart.axis[0].column.label, chart.axis[0].cufId),
      operation: chart.axis[0].operation,
    };
  }

  private getMeasureFromExistingChart(chartColumnPrototypes: EntityState<EnhancedColumnPrototype>, chart: ChartDefinitionModel) {
    return this.doesMeasureIsRequired(chart.type) ? {
      cufId: chart.measures[0].cufId,
      label: chart.measures[0].label,
      column: this.findColumnIdFromExistingChart(chartColumnPrototypes, chart.measures[0].column.label, chart.measures[0].cufId),
      operation: chart.measures[0].operation,
    } : null;
  }

  private getSeriesFromExistingChart(chartColumnPrototypes: EntityState<EnhancedColumnPrototype>, chart: ChartDefinitionModel) {
    return this.doesSeriesAreRequired(chart.type) ? {
      cufId: chart.axis[1].cufId,
      label: chart.axis[1].label,
      column: this.findColumnIdFromExistingChart(chartColumnPrototypes, chart.axis[1].column.label, chart.axis[1].cufId),
      operation: chart.axis[1].operation,
    } : null;
  }


  private getFiltersFromExistingChart(chartColumnPrototypes: EntityState<EnhancedColumnPrototype>,
                                      chart: ChartDefinitionModel,
                                      infoLists: InfoList[]): CraftedChartFilter[] {
    return chart.filters.map(chartDefinitionFilter => {
      return {
        id: chartDefinitionFilter.id,
        column: this.findColumnIdFromExistingChart(chartColumnPrototypes, chartDefinitionFilter.column.label, chartDefinitionFilter.cufId),
        operation: chartDefinitionFilter.operation,
        cufId: chartDefinitionFilter.cufId,
        value: this.convertChartDefinitionFilterValues(chartDefinitionFilter, infoLists),
        preventOpening: true,
      };
    });
  }


  private findColumnIdFromExistingChart(chartColumnPrototypes: EntityState<EnhancedColumnPrototype>,
                                        columnLabel: string,
                                        cufId: number): string {
    const columnPrototypes = Object.values(chartColumnPrototypes.entities);
    return cufId ? `${columnPrototypes.find(column => column.label === columnLabel).id.toString()}-${cufId}` :
      columnPrototypes.find(column => column.label === columnLabel).id.toString();
  }

  private convertChartDefinitionFilterValues(chartDefinitionFilter: ChartFilter,
                                             infoLists: InfoList[]): FilterValue {
    let filterValue: FilterValue;
    const dataType = chartDefinitionFilter.column.dataType;
    const isMultipleValue = chartDefinitionFilter.operation === ChartOperation.BETWEEN;
    switch (dataType) {
      case ChartDataType.NUMERIC:
        filterValue = {
          kind: isMultipleValue ? 'multiple-string-value' : 'single-string-value',
          value: isMultipleValue ? [chartDefinitionFilter.values[0], chartDefinitionFilter.values[1]] : chartDefinitionFilter.values[0]
        };
        break;
      case ChartDataType.STRING:
        filterValue = {
          kind: 'single-string-value',
          value: chartDefinitionFilter.values[0]
        };
        break;
      case ChartDataType.DATE:
      case ChartDataType.DATE_AS_STRING:
        filterValue = {
          kind: isMultipleValue ? 'multiple-date-value' : 'single-date-value',
          value: isMultipleValue ? [chartDefinitionFilter.values[0], chartDefinitionFilter.values[1]] : chartDefinitionFilter.values[0]
        };
        break;
      case ChartDataType.BOOLEAN_AS_STRING:
      case ChartDataType.BOOLEAN:
        filterValue = {
          kind: 'multiple-discrete-value',
          value: this.findCraftedChartBooleanFilterValue(chartDefinitionFilter)
        };
        break;
      case ChartDataType.INFO_LIST_ITEM:
        filterValue = {
          kind: 'multiple-discrete-value',
          value: this.findCraftedChartInfoListFilterValue(chartDefinitionFilter, infoLists)
        };
        break;
      case ChartDataType.AUTOMATED_TEST_TECHNOLOGY:
        filterValue = {
          kind: 'multiple-discrete-value',
          value: this.getRawFilterValue(chartDefinitionFilter)
        };
        break;
      case ChartDataType.LEVEL_ENUM:
      case ChartDataType.EXECUTION_STATUS:
      case ChartDataType.REQUIREMENT_STATUS:
      case ChartDataType.LIST:
      case ChartDataType.TAG:
        filterValue = {
          kind: 'multiple-discrete-value',
          value: this.findCraftedChartFilterValue(chartDefinitionFilter)
        };
        break;
      default:
        throw Error(`No dataType for type ${dataType}`);
    }
    return filterValue;

  }

  private findCraftedChartFilterValue(chartDefinitionFilter: ChartFilter): DiscreteFilterValue[] {
    if (!chartDefinitionFilter.cufId) {
      const i18nEnum = SearchColumnPrototypeUtils.findEnum(ResearchColumnPrototype[chartDefinitionFilter.column.label]);
      return chartDefinitionFilter.values.map(value => {
        return {id: value, i18nLabelKey: i18nEnum[value].i18nKey};
      });
    } else {
      return this.getRawFilterValue(chartDefinitionFilter);
    }
  }

  private getRawFilterValue(chartDefinitionFilter: ChartFilter) {
    return chartDefinitionFilter.values.map(value => {
      return {id: value, label: value};
    });
  }

  private findCraftedChartBooleanFilterValue(chartDefinitionFilter: ChartFilter): DiscreteFilterValue[] {
    if (!chartDefinitionFilter.cufId) {
      const i18nEnum = SearchColumnPrototypeUtils.findEnum(ResearchColumnPrototype[chartDefinitionFilter.column.label]);
      return chartDefinitionFilter.values.map(value => {
        return {id: value, i18nLabelKey: i18nEnum[value].i18nKey};
      });
    } else {
      return chartDefinitionFilter.values.map(value => {
        return {id: value.toLowerCase(), i18nLabelKey: `sqtm-core.generic.label.${value.toLowerCase()}`};
      });
    }
  }

  private findCraftedChartInfoListFilterValue(chartDefinitionFilter: ChartFilter, infoLists: InfoList[]): DiscreteFilterValue[] {
    const filterValue = chartDefinitionFilter.values
      .map(value => {
        const infoList = infoLists.find(list => list.items.find(item => item.code === value));

        if (infoList) {
          const infoListItem = infoList.items.find(item => item.code === value);
          if (isSystemInfoList(infoList)) {
            const i18nKey = infoList.items.find(item => item.code === value).label;
            return {id: value, i18nLabelKey: `sqtm-core.entity.${i18nKey}`};
          } else if (infoListItem) {
            return {id: value, i18nLabelKey: infoList.items.find(item => item.code === value).label};
          } else {
            // We don't want to return the value if chart filters contain a custom info list item that has been deleted
            // in administration workspace after chart was created
            return undefined;
          }
        }
      }).filter(Boolean);
    return filterValue;
  }

  private doesMeasureIsRequired(chartType: ChartType) {
    return chartType !== ChartType.PIE;
  }

  private doesSeriesAreRequired(chartType: ChartType) {
    return chartType === ChartType.COMPARATIVE || chartType === ChartType.TREND;
  }

  complete() {
    this.store.complete();
  }

  private removeFiltersAfterScopeChanged(chartColumnPrototypes: EntityState<EnhancedColumnPrototype>,
                                         filters: CraftedChartFilter[]): CraftedChartFilter[] {
    return filters.filter(f => Boolean(chartColumnPrototypes.entities[f.column]));
  }
}

import {
  ChangeDetectionStrategy,
  Component,
  ComponentRef,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  AxisColumn,
  ChartColumnRole,
  ChartOperation,
  ChartType,
  CustomChartEntityType,
  filterAvailableChartOperations,
  Option
} from 'sqtm-core';
import {Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {ComponentPortal} from '@angular/cdk/portal';
import {Subject} from 'rxjs';
import {
  OverlayColumnPrototypeSelectorComponent
} from '../overlay-column-prototype-selector/overlay-column-prototype-selector.component';
import {take, takeUntil} from 'rxjs/operators';
import {DetailedAxisRole, EnhancedColumnPrototype} from '../../state/chart-workbench.state';
import {ChartWorkbenchUtils} from '../chart-workbench.utils';

@Component({
  selector: 'sqtm-app-axis-selector',
  templateUrl: './axis-selector.component.html',
  styleUrls: ['./axis-selector.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AxisSelectorComponent implements OnInit, OnDestroy {

  @Input()
  role: ChartColumnRole;

  @Input()
  detailedAxisRole: DetailedAxisRole;

  @Input()
  chartType: ChartType;

  private _axis: AxisColumn;


  get axis(): AxisColumn {
    return this._axis;
  }

  @Input()
  set axis(value: AxisColumn) {
    if (value) {
      this._axis = value;
      this.selectedOperation = this.axis.operation;
    } else {
      this._axis = null;
    }
  }

  @Input()
  axisName: string;

  @Input()
  columnPrototypes: { [K in CustomChartEntityType]: EnhancedColumnPrototype[] };

  @Output()
  operationChanged = new EventEmitter<ChartOperation>();

  @Output()
  columnChanged = new EventEmitter<string>();

  @ViewChild('columnField')
  private columnField: ElementRef;

  selectedOperation: ChartOperation;

  private overlayRef: OverlayRef;

  private componentRef: ComponentRef<OverlayColumnPrototypeSelectorComponent>;

  private unsub$ = new Subject<void>();

  private readonly yAxisIcon = 'sqtm-core-custom-report:up';
  private readonly xAxisIcon = 'sqtm-core-custom-report:right';

  get entityNameI18nKey(): string {
    const column = this._axis.column;
    return ChartWorkbenchUtils.getColumnEntityNameI18nKey(column);
  }

  get columnNameI18nKey(): string {
    return ChartWorkbenchUtils.getColumnNameI18nKey(this.axis.column, this.axis.cufId);
  }


  get columnPrototypeBorderColor() {
    return 'var(--current-workspace-main-color)';
  }

  get availableOperations(): Option[] {
    return filterAvailableChartOperations(this.role, this._axis.column.dataType).map(operation => (
      {
        value: operation,
        label: this.getOperationI18nKey(operation)
      }));
  }

  constructor(private overlay: Overlay, private vcr: ViewContainerRef) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  getWorkspace() {
    const entity = this._axis.column.specializedType.entityType;
    return ChartWorkbenchUtils.getWorkspace(entity);
  }

  getOperationI18nKey(operation: ChartOperation): string {
    return ChartWorkbenchUtils.getOperationI18nKey(operation);
  }

  changeOperation() {
    this.operationChanged.emit(this.selectedOperation);
  }

  openColumnSelector() {
    const positionStrategy = this.overlay.position().flexibleConnectedTo(this.columnField)
      .withPositions([
        {originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'top', offsetX: 20},
      ]);
    const overlayConfig: OverlayConfig = {
      positionStrategy,
      hasBackdrop: true,
      disposeOnNavigation: true,
      backdropClass: 'transparent-overlay-backdrop',
      width: 380,
      height: 800
    };
    this.overlayRef = this.overlay.create(overlayConfig);
    const componentPortal = new ComponentPortal(OverlayColumnPrototypeSelectorComponent, this.vcr);
    this.componentRef = this.overlayRef.attach(componentPortal);
    this.componentRef.instance.columnPrototypes = this.columnPrototypes;
    this.componentRef.changeDetectorRef.detectChanges();
    this.componentRef.instance.selectedPrototype$.pipe(
      take(1)
    ).subscribe(id => {
      this.columnChanged.emit(id);
      this.closeColumnSelector();
    });
    this.overlayRef.backdropClick()
      .pipe(takeUntil(this.unsub$))
      .subscribe(() => this.closeColumnSelector());
  }

  closeColumnSelector() {
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = null;
      this.componentRef = null;
    }
  }

  getAxisIcon() {
    switch (this.chartType) {
      case ChartType.PIE:
        return 'sqtm-core-custom-report:pie';
      case ChartType.BAR:
      case ChartType.CUMULATIVE:
      case ChartType.TREND:
        return this.getStandardChartIcons();
      case ChartType.COMPARATIVE:
        return this.getReversedChartIcons();

    }
  }



  private getStandardChartIcons() {
    switch (this.detailedAxisRole) {
      case DetailedAxisRole.Measure:
        return this.yAxisIcon;
      case DetailedAxisRole.Axis:
        return this.xAxisIcon;
      case DetailedAxisRole.Series:
        return 'unordered-list';

    }
  }

  private getReversedChartIcons() {
    switch (this.detailedAxisRole) {
      case DetailedAxisRole.Measure:
        return this.xAxisIcon;
      case DetailedAxisRole.Axis:
        return this.yAxisIcon;
      case DetailedAxisRole.Series:
        return 'unordered-list';
    }
  }

  isCuf() {
    return this._axis && this._axis.cufId;
  }
}

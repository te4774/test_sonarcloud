import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {AbstractCellRendererComponent, ConfirmDeleteConfiguration, DialogService, GridService} from 'sqtm-core';
import {filter, map, takeUntil} from 'rxjs/operators';
import {combineLatest, Observable, Subject} from 'rxjs';
import {STEP_VIEW_REQUIREMENT_LINK_SERVICE} from '../../../detailed-step-view.constant.ts.constant';
import {StepViewRequirementLinkService} from '../../../step-view-requirement-link.service';

@Component({
  selector: 'sqtm-app-delete-coverage-cell-renderer',
  templateUrl: './delete-coverage-cell-renderer.component.html',
  styleUrls: ['./delete-coverage-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteCoverageCellRendererComponent extends AbstractCellRendererComponent implements OnInit, OnDestroy {

  canWrite$: Observable<boolean>;

  unsub$ = new Subject<void>();

  constructor(public grid: GridService,
              public cdRef: ChangeDetectorRef,
              private dialogService: DialogService,
              @Inject(STEP_VIEW_REQUIREMENT_LINK_SERVICE) private viewService: StepViewRequirementLinkService) {
    super(grid, cdRef);
    this.initCanWrite();
  }

  ngOnInit(): void {

  }

  private initCanWrite() {
    this.canWrite$ = combineLatest([this.viewService.componentData$, this.viewService.stepCreationMode$]).pipe(
      takeUntil(this.unsub$),
      map(([data, stepCreationMode]) => data.permissions.canWrite && data.milestonesAllowModification && !stepCreationMode)
    );
  }

  openDeleteCoverageDialog($event: MouseEvent) {
    const configuration: Partial<ConfirmDeleteConfiguration> = {
      level: 'WARNING',
      titleKey: 'sqtm-core.detailed-test-step-view.dialogs.coverages.remove-one.title',
      messageKey: 'sqtm-core.detailed-test-step-view.dialogs.coverages.remove-one.message'
    };
    this.dialogService.openDeletionConfirm(configuration).dialogClosed$.pipe(
      filter(result => result)
    ).subscribe(() => this.viewService.deleteCoverages([this.row.id as number]));
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }
}

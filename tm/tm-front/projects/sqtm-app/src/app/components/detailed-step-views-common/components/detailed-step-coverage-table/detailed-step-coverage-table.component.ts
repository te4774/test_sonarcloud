import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {
  column,
  DataRow,
  Extendable,
  Fixed,
  GenericDataRow,
  GridDefinition,
  GridService,
  indexColumn,
  levelEnumColumn,
  RequirementCriticality,
  StyleDefinitionBuilder,
  table,
  textColumn
} from 'sqtm-core';
import {RequirementFoldableRowComponent} from '../requirement-foldable-row/requirement-foldable-row.component';
import {
  OpenCloseCellRendererComponent
} from '../cell-renderers/open-close-cell-renderer/open-close-cell-renderer.component';
import {
  LinkedToStepCellRendererComponent
} from '../cell-renderers/linked-to-step-cell-renderer/linked-to-step-cell-renderer.component';
import {
  DeleteCoverageCellRendererComponent
} from '../cell-renderers/delete-coverage-cell-renderer/delete-coverage-cell-renderer.component';
import {DETAILED_COVERAGE_TABLE} from '../../detailed-step-view.constant.ts.constant';

export function detailedStepCoverageTableDefinition(): GridDefinition {
  return table('detailed-test-step-view-coverages')
    .withColumns([
      indexColumn(), // do not put in leftViewport we have a special row renderer here
      column('open')
        .changeWidthCalculationStrategy(new Fixed(20))
        .withRenderer(OpenCloseCellRendererComponent)
        .disableHeader()
        .disableSort(),
      column('linkedToCurrentStep')
        .changeWidthCalculationStrategy(new Fixed(70))
        .withRenderer(LinkedToStepCellRendererComponent)
        .withI18nKey('sqtm-core.detailed-test-step-view.coverages.linked-step.short')
        .withTitleI18nKey('sqtm-core.detailed-test-step-view.coverages.linked-step.long')
        .withHeaderPosition('center')
        .disableSort(),
      textColumn('projectName')
        .changeWidthCalculationStrategy(new Extendable(100, 1))
        .withI18nKey('sqtm-core.entity.project.label.singular'),
      textColumn('reference')
        .withI18nKey('sqtm-core.entity.generic.reference.label'),
      textColumn('name')
        .changeWidthCalculationStrategy(new Extendable(100, 1.5))
        .withI18nKey('sqtm-core.entity.requirement.label.singular'),
      levelEnumColumn('criticality', RequirementCriticality)
        .withI18nKey('sqtm-core.entity.generic.criticality.label')
        .changeWidthCalculationStrategy(new Fixed(78))
        .isEditable(false),
      column('delete')
        .changeWidthCalculationStrategy(new Fixed(50))
        .withRenderer(DeleteCoverageCellRendererComponent)
        .disableHeader()
        .disableSort(),
    ])
    .withStyle(new StyleDefinitionBuilder().showLines())
    .withRowHeight(35)
    .withRowConverter(foldRequirementLiteralConverter)
    .build();
}

export function foldRequirementLiteralConverter(literals: Partial<DataRow>[]): DataRow[] {
  return literals.reduce((datarows, literal) => {
    const dataRow = new GenericDataRow();
    Object.assign(dataRow, literal);
    dataRow.component = RequirementFoldableRowComponent;
    datarows.push(dataRow);
    return datarows;
  }, []);
}

@Component({
  selector: 'sqtm-app-detailed-step-coverage-table',
  templateUrl: './detailed-step-coverage-table.component.html',
  styleUrls: ['./detailed-step-coverage-table.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridService,
      useExisting: DETAILED_COVERAGE_TABLE
    }
  ]
})
export class DetailedStepCoverageTableComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

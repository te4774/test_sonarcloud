import {Directive, ElementRef, Renderer2, ViewChild} from '@angular/core';
import {
  ConfirmDeleteConfiguration,
  DataRow,
  DialogService,
  EntityViewComponentData,
  GridDndData,
  GridService,
  Identifier,
  isDndDataFromRequirementTreePicker,
  parseDataRowId,
  SqtmDragEnterEvent,
  SqtmDragLeaveEvent,
  SqtmDropEvent,
  SquashTmDataRowType,
  TestCasePermissions
} from 'sqtm-core';
import {StepViewRequirementLinkService} from '../step-view-requirement-link.service';
import {combineLatest, Observable, Subject} from 'rxjs';
import {concatMap, filter, finalize, map, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {BACK_URL_PARAM} from '../../../pages/search/search-constants';
import {Router} from '@angular/router';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export class AbstractRequirementLinkableStepComponent {

  unsub$ = new Subject<void>();

  activateDeleteCoverageButton$: Observable<boolean>;
  activateDeleteCoverageFromStep$: Observable<boolean>;

  @ViewChild('dropRequirementZone', {read: ElementRef})
  private dropRequirementZone: ElementRef;

  constructor(public requirementLinkService: StepViewRequirementLinkService,
              protected router: Router,
              protected renderer: Renderer2,
              protected dialogService: DialogService,
              protected coverageGrid: GridService) {
    this.initializeCoverageButtons();
  }


  private initializeCoverageButtons() {
    this.activateDeleteCoverageButton$ =
      combineLatest([this.coverageGrid.selectedRowIds$, this.requirementLinkService.componentData$]).pipe(
        takeUntil(this.unsub$),
        map(([ids, componentData]: [Identifier[], EntityViewComponentData<any, any, TestCasePermissions>]) =>
          ids.length > 0 && componentData.permissions.canWrite && componentData.milestonesAllowModification),
      );

    this.activateDeleteCoverageFromStep$ =
      combineLatest([this.coverageGrid.selectedRows$, this.requirementLinkService.componentData$]).pipe(
        takeUntil(this.unsub$),
        map(([rows, componentData]: [DataRow[], EntityViewComponentData<any, any, TestCasePermissions>]) =>
          componentData.permissions.canWrite && rows.filter(row => row.data.linkedToStep).length > 0));
  }

  showDeleteCoverageFromTestCase() {
    const configuration: Partial<ConfirmDeleteConfiguration> = {
      level: 'WARNING',
      titleKey: 'sqtm-core.detailed-test-step-view.dialogs.coverages.remove-many.from-test-case.title',
      messageKey: 'sqtm-core.detailed-test-step-view.dialogs.coverages.remove-many.from-test-case.message'
    };
    this.dialogService.openDeletionConfirm(configuration).dialogClosed$.pipe(
      filter(result => result),
      withLatestFrom(this.coverageGrid.selectedRowIds$)
    ).subscribe(([, ids]) => this.requirementLinkService.deleteCoverages(ids as number[]));
  }

  showDeleteCoverageFromTestStep() {
    const configuration: Partial<ConfirmDeleteConfiguration> = {
      level: 'WARNING',
      titleKey: 'sqtm-core.detailed-test-step-view.dialogs.coverages.remove-many.from-test-step.title',
      messageKey: 'sqtm-core.detailed-test-step-view.dialogs.coverages.remove-many.from-test-step.message'
    };
    this.activateDeleteCoverageFromStep$.pipe(
      take(1),
      filter(active => active),
      concatMap(() => this.dialogService.openDeletionConfirm(configuration).dialogClosed$),
      filter(result => result),
      withLatestFrom(this.coverageGrid.selectedRowIds$),
      tap(() => this.coverageGrid.beginAsyncOperation()),
      concatMap(([, ids]) => this.requirementLinkService.deleteStepCoverages(ids as number[])),
      finalize(() => this.coverageGrid.completeAsyncOperation())
    ).subscribe();
  }

  dragEnter($event: SqtmDragEnterEvent) {
    if (isDndDataFromRequirementTreePicker($event)) {
      this.addBorderOnRequirementTable();
    }
  }

  dragLeave($event: SqtmDragLeaveEvent) {
    if (isDndDataFromRequirementTreePicker($event)) {
      this.removeBorderOnRequirementTable();
    }
  }

  private addBorderOnRequirementTable() {
    this.renderer.addClass(this.dropRequirementZone.nativeElement, 'drop-requirement');
  }

  removeBorderOnRequirementTable() {
    this.renderer.removeClass(this.dropRequirementZone.nativeElement, 'drop-requirement');
  }

  protected extractRequirementIdsFromDragAndDropData($event: SqtmDropEvent): number[] {
    const data = $event.dragAndDropData.data as GridDndData;
    const dataRows = data.dataRows;
    return dataRows
      .filter(row => row.type === SquashTmDataRowType.Requirement || row.type === SquashTmDataRowType.HighLevelRequirement || row.type === SquashTmDataRowType.RequirementFolder)
      .map(row => parseDataRowId(row));
  }

  navigateToSearchRequirementForTestCaseCoverage(id: number) {
    this.router.navigate(['/search/requirement/coverage', id], {queryParams: {[BACK_URL_PARAM]: this.router.url}});
  }

  navigateToSearchRequirementForStepCoverage(testCaseId: number, stepId: number) {
    this.router.navigate(['/search/requirement/coverage', testCaseId, stepId], {queryParams: {[BACK_URL_PARAM]: this.router.url}});
  }
}

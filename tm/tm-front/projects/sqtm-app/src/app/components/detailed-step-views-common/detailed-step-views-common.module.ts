import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  OpenCloseCellRendererComponent
} from './components/cell-renderers/open-close-cell-renderer/open-close-cell-renderer.component';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {
  LinkedToStepCellRendererComponent
} from './components/cell-renderers/linked-to-step-cell-renderer/linked-to-step-cell-renderer.component';
import {GridModule, RequirementUiModule, WorkspaceCommonModule} from 'sqtm-core';
import {
  DeleteCoverageCellRendererComponent
} from './components/cell-renderers/delete-coverage-cell-renderer/delete-coverage-cell-renderer.component';
import {
  RequirementFoldableRowComponent
} from './components/requirement-foldable-row/requirement-foldable-row.component';
import {
  RequirementVersionDetailsComponent
} from './components/requirement-version-details/requirement-version-details.component';
import {TranslateModule} from '@ngx-translate/core';
import {
  DetailedStepCoverageTableComponent
} from './components/detailed-step-coverage-table/detailed-step-coverage-table.component';


@NgModule({
  declarations: [
    OpenCloseCellRendererComponent,
    LinkedToStepCellRendererComponent,
    DeleteCoverageCellRendererComponent,
    RequirementFoldableRowComponent,
    RequirementVersionDetailsComponent,
    DetailedStepCoverageTableComponent
  ],
  exports: [
    OpenCloseCellRendererComponent,
    LinkedToStepCellRendererComponent,
    DeleteCoverageCellRendererComponent,
    RequirementFoldableRowComponent,
    RequirementVersionDetailsComponent,
    DetailedStepCoverageTableComponent
  ],
  imports: [
    CommonModule,
    NzIconModule,
    WorkspaceCommonModule,
    GridModule,
    TranslateModule.forChild(),
    RequirementUiModule
  ]
})
export class DetailedStepViewsCommonModule {
}

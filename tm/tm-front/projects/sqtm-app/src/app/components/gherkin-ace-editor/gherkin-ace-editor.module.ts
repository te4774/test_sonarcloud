import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EditableAceFieldComponent} from './components/editable-ace-field/editable-ace-field.component';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {TranslateModule} from '@ngx-translate/core';
import {WorkspaceCommonModule} from 'sqtm-core';


@NgModule({
  declarations: [EditableAceFieldComponent],
  imports: [
    CommonModule,
    NzToolTipModule,
    TranslateModule.forChild(),
    NzButtonModule,
    NzIconModule,
    WorkspaceCommonModule
  ],
  exports: [
    EditableAceFieldComponent
  ]
})
export class GherkinAceEditorModule {
}

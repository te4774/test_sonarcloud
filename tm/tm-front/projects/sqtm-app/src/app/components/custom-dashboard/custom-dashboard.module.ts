import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomDashboardComponent} from './containers/custom-dashboard/custom-dashboard.component';
import {GridsterModule} from 'angular-gridster2';
import {ChartsModule} from '../charts/charts.module';
import {SqtmDragAndDropModule, WorkspaceCommonModule} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {ReportWorkbenchModule} from '../report-workbench/report-workbench.module';


@NgModule({
  declarations: [CustomDashboardComponent],
  exports: [
    CustomDashboardComponent
  ],
  imports: [
    CommonModule,
    GridsterModule,
    ChartsModule,
    SqtmDragAndDropModule,
    TranslateModule.forChild(),
    NzIconModule,
    WorkspaceCommonModule,
    NzToolTipModule,
    ReportWorkbenchModule
  ]
})
export class CustomDashboardModule { }

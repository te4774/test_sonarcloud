import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CampaignAdvancementChartComponent} from './campaign-advancement-chart/campaign-advancement-chart.component';
import {
  CampaignStatisticsChartsPanelComponent
} from './campaign-statistics-charts-panel/campaign-statistics-charts-panel.component';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {WorkspaceCommonModule} from 'sqtm-core';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {CampaignInventoryComponent} from './campaign-inventory/campaign-inventory.component';
import {NzTableModule} from 'ng-zorro-antd/table';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {IterationInventoryComponent} from './iteration-inventory/iteration-inventory.component';
import {CampaignFolderInventoryComponent} from './campaign-folder-inventory/campaign-folder-inventory.component';
import {SimpleInventoryTableComponent} from './simple-inventory-table/simple-inventory-table.component';


@NgModule({
  declarations: [
    CampaignAdvancementChartComponent,
    CampaignStatisticsChartsPanelComponent,
    CampaignInventoryComponent,
    IterationInventoryComponent,
    CampaignFolderInventoryComponent,
    SimpleInventoryTableComponent
  ],
  exports: [
    CampaignAdvancementChartComponent,
    CampaignStatisticsChartsPanelComponent,
    CampaignFolderInventoryComponent,
    CampaignInventoryComponent,
    IterationInventoryComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forChild(),
    WorkspaceCommonModule,
    NzIconModule,
    NzTableModule,
    NzToolTipModule
  ]
})
export class CampaignStatisticsModule {
}

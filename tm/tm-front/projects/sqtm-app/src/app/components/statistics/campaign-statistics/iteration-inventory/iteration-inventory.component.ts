import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {
  ExecutionStatus,
  ExecutionStatusCount,
  ExecutionStatusKeys,
  TestCaseImportanceKeys,
  TestCaseWeight,
  TestSuiteTestInventoryStatistics
} from 'sqtm-core';
import * as _ from 'lodash';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-iteration-inventory',
  templateUrl: './iteration-inventory.component.html',
  styleUrls: ['./iteration-inventory.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IterationInventoryComponent implements OnInit {

  private _inventory: TestSuiteAggregate[];

  ExecutionStatus = ExecutionStatus;

  TestCaseImportance = TestCaseWeight;

  get extendedInventory(): TestSuiteAggregate[] {
    return this._inventory;
  }

  @Input()
  set inventory(inventory: TestSuiteTestInventoryStatistics[]) {
    const testSuiteAggregates: TestSuiteAggregate[] = this.getIterationAggregates(inventory);
    const resumeRow = this.getResumeRow(testSuiteAggregates);
    testSuiteAggregates.push(resumeRow);
    this._inventory = testSuiteAggregates;
  }

  @Input()
  disabledExecutionStatus: ExecutionStatusKeys[];

  get settledIsEnabled() {
    return !this.disabledExecutionStatus.includes(ExecutionStatus.SETTLED.id);
  }

  get untestableIsEnabled() {
    return !this.disabledExecutionStatus.includes(ExecutionStatus.UNTESTABLE.id);
  }

  get progressColSpan(): number {
    if (this.settledIsEnabled && this.untestableIsEnabled) {
      return 11;
    } else if (!this.settledIsEnabled && !this.untestableIsEnabled) {
      return 9;
    }
    return 10;

  }

  constructor(private readonly translateService: TranslateService) {
  }

  ngOnInit(): void {
  }

  private getIterationAggregates(inventory: TestSuiteTestInventoryStatistics[]): TestSuiteAggregate[] {
    return inventory.map(i => ({
      ...i,
      testSuiteName: i.testsuiteName,
      ...i.statusStatistics,
      ...i.importanceStatistics,
      isTotal: false,
    }));
  }

  private getResumeRow(iterationAggregates: TestSuiteAggregate[]): TestSuiteAggregate {
    const resume = iterationAggregates.reduce((reducedResume, current) => {
      const mappedNumbersEntries = Object.entries<any>(reducedResume)
        .filter(([key, value]) => typeof value === 'number')
        .map(([key, value]) => {
          return [key, value + (current[key] || 0)];
        });
      const sums: Partial<TestSuiteAggregate> = {..._.fromPairs(mappedNumbersEntries)};
      return {
        ...reducedResume,
        ...sums
      };
    }, this.getEmptyResume());

    resume.pcProgress = this.percentRoundedDown(resume.nbExecuted, resume.nbTotal);
    resume.pcSuccess = this.percentRoundedDown(resume.SETTLED + resume.SUCCESS, resume.nbExecuted);
    resume.pcFailure = this.percentRoundedUp(resume.FAILURE, resume.nbExecuted);
    // just an average of each suite rate...
    // yeah i know it's a value that mean nothing because the number of test in each suite is not even considered
    resume.pcPrevProgress = (Math.round(resume.pcPrevProgress / iterationAggregates.length)) || 0;

    return resume;
  }

  private getEmptyResume(): TestSuiteAggregate {
    return {
      testSuiteName: this.translateService.instant('sqtm-core.campaign-workspace.iteration-page.statistics.inventory.total') as any,
      FAILURE: 0,
      SUCCESS: 0,
      RUNNING: 0,
      READY: 0,
      BLOCKED: 0,
      UNTESTABLE: 0,
      SETTLED: 0,
      VERY_HIGH: 0,
      HIGH: 0,
      LOW: 0,
      MEDIUM: 0,
      nbTotal: 0,
      nbExecuted: 0,
      pcProgress: 0,
      nbToExecute: 0,
      pcSuccess: 0,
      pcFailure: 0,
      pcPrevProgress: 0,
      nbPrevToExecute: 0,
      isTotal: true as any,
    };
  }

  private percentRoundedDown(num: number, div: number) {
    return Math.floor(((num / div) * 100) || 0);
  }

  private percentRoundedUp(num: number, div: number) {
    return Math.floor(((num / div) * 100) || 0);
  }
}

interface TestSuiteAggregate extends Partial<ExecutionStatusCount>, Partial<{ [K in TestCaseImportanceKeys]: number }> {
  testSuiteName: any;
  isTotal: any;
  nbTotal: number;
  nbExecuted: number;
  nbToExecute: number;
  pcProgress: number;
  pcSuccess: number;
  pcFailure: number;
  pcPrevProgress: number;
  nbPrevToExecute: number;
}

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {CampaignProgressionStatistics, getSupportedBrowserLang, ScheduledIteration} from 'sqtm-core';
import * as Plotly from 'plotly.js';
import {Config, Layout, newPlot, Shape} from 'plotly.js';
import {ceil, last} from 'lodash';
import {TranslateService} from '@ngx-translate/core';
import {addDays, differenceInDays, isBefore, parseISO} from 'date-fns';
import {utcToZonedTime} from 'date-fns-tz';
import * as CssElementQuery from 'css-element-queries';
import {ResizeSensor} from 'css-element-queries';
import {Subject} from 'rxjs';
import {skip, take, takeUntil} from 'rxjs/operators';
import {ResizeEvent} from '../../abstract-statistics-panel.component';
import {registerPlotlyLocale} from '../../../charts/plotly.utils';

@Component({
  selector: 'sqtm-app-campaign-advancement-chart',
  templateUrl: './campaign-advancement-chart.component.html',
  styleUrls: ['./campaign-advancement-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignAdvancementChartComponent implements OnInit, OnDestroy, AfterViewInit {

  protected resizeSensor: ResizeSensor;
  protected _resizeEvent = new Subject<ResizeEvent>();
  protected resizeEvent$ = this._resizeEvent.asObservable().pipe();

  protected unsub$ = new Subject<void>();

  @Input()
  campaignProgressionStatistics: CampaignProgressionStatistics;

  @ViewChild('chart', {read: ElementRef})
  chart: ElementRef;

  private readonly locale: string;
  private readonly userTimeZone: string;

  constructor(private translateService: TranslateService) {
    this.locale = getSupportedBrowserLang(translateService);
    this.userTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    registerPlotlyLocale(this.locale);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.resizeSensor) {
      this.resizeSensor.detach();
    }
    this.unsub$.next();
    this.unsub$.complete();
  }

  ngAfterViewInit(): void {
    this.initResizeSensor();
    this.initChart();
    this.initChartResize();
  }

  private initChart() {
    this.resizeEvent$.pipe(
      takeUntil(this.unsub$),
      take(1) // only one, we want to plot only one time
    ).subscribe(resize => {
      this.generateChart(resize.height, resize.width);
    });
  }

  private initChartResize() {
    this.resizeEvent$.pipe(
      takeUntil(this.unsub$),
      skip(1)// skip the first one, it will be used to generate the chart
    ).subscribe(resize => {
      // only relayout, not full plot
      Plotly.relayout(this.chart.nativeElement, {height: resize.height, width: resize.width});
    });
  }

  private initResizeSensor() {
    const that = this;
    this.resizeSensor = new CssElementQuery.ResizeSensor(this.chart.nativeElement, function ($event: ResizeEvent) {
      that._resizeEvent.next($event);
    });
  }

  private generateChart(height: number, width: number) {
    const campaignTrace = this.getCampaignTrace();
    const iterationTrace = this.getIterationTrace();
    const iterationNameTrace = this.getIterationNameTrace();

    const shapes: Partial<Shape>[] = this.campaignProgressionStatistics.scheduledIterations
      .map(scheduledIteration => {
        return {
          type: 'rect',
          // x-reference is assigned to the x-values, aka the dates axis
          xref: 'x',
          // y-reference is assigned to the plot paper [0,1], so it will always span on the entire chart
          yref: 'paper',
          x0: this.parseUtcDateToLocaleDateTime(scheduledIteration.scheduledStart),
          y0: 0,
          x1: this.parseUtcDateToLocaleDateTime(scheduledIteration.scheduledEnd),
          y1: 1,
          fillcolor: '#d3d3d3',
          opacity: 0.5,
          layer: 'below',
          line: {
            width: 0
          }
        };
      });

    const layout: Partial<Layout> = {
      showlegend: false,
      yaxis: {
        fixedrange: true
      },
      xaxis: {
        type: 'date',
      },
      margin: {
        b: 40,
        l: 50,
        r: 20,
        t: 50
      },
      height,
      width,
      shapes
    };

    const config: Partial<Config> = {
      locale: this.locale,
      displaylogo: false,
      modeBarButtonsToRemove: ['lasso2d', 'select2d', 'resetScale2d', 'toggleSpikelines', 'hoverClosestCartesian', 'hoverCompareCartesian'],
    };

    newPlot(this.chart.nativeElement, [iterationNameTrace, campaignTrace, iterationTrace], layout, config);
  }

  private getCampaignTrace() {
    const xCampaign = this.campaignProgressionStatistics.cumulativeExecutionsPerDate
      .map(([dateAString]) => dateAString)
      .map(dateAsString => this.parseUtcDateToLocaleDateTime(dateAsString));
    const yCampaign = this.campaignProgressionStatistics.cumulativeExecutionsPerDate.map(([, count]) => count);

    if (this.campaignProgressionStatistics.cumulativeExecutionsPerDate.length > 0) {
      const lastIterationEndDateString = last<ScheduledIteration>(this.campaignProgressionStatistics.scheduledIterations).scheduledEnd;
      const lastCampaignExecutionDateString = last<[string, number]>(this.campaignProgressionStatistics.cumulativeExecutionsPerDate)[0];
      const lastIterationEndDate = this.parseUtcDateToLocaleDateTime(lastIterationEndDateString);
      const lastCampaignExecutionDate = this.parseUtcDateToLocaleDateTime(lastCampaignExecutionDateString);
      if (isBefore(lastCampaignExecutionDate, lastIterationEndDate)) {
        xCampaign.push(lastIterationEndDate);
        yCampaign.push(last(yCampaign));
      }
    }

    const i18nLabel = this.translateService
      .instant('sqtm-core.campaign-workspace.campaign-page.statistics.charts.cumulative.actualcount');

    const campaignTrace: any = {
      x: xCampaign,
      y: yCampaign,
      type: 'scatter',
      fill: 'tozeroy',
      hovertemplate: `${i18nLabel} : %{y}<br>%{x}<br><extra></extra>`,
      line: {color: '#751AA3', shape: 'hv'},
    };
    return campaignTrace;
  }

  private parseUtcDateToLocaleDateTime(dateAsString: string) {
    return utcToZonedTime(parseISO(dateAsString), this.userTimeZone);
  }

  private getIterationTrace() {
    const x = this.getIterationAbscissa();

    const y = this.getIterationOrdinate();

    const totalCount = this.getTotalCount();

    const percents = this.getIterationPercents(totalCount);

    const customData = percents.map(p => (`${p}%`));
    const i18nLabel = this.translateService
      .instant('sqtm-core.campaign-workspace.campaign-page.statistics.charts.cumulative.scheduledcount');

    return {
      x: x,
      y: y,
      customdata: customData,
      type: 'scatter',
      fill: 'tozeroy',
      line: {color: '#5479b1'},
      hovertemplate: `${i18nLabel} : %{y:.1f} (%{customdata})<br>%{x}<br><extra></extra>`
    };
  }

  private getIterationPercents(totalCount: number) {
    return this.campaignProgressionStatistics.scheduledIterations
      .map(i => i.cumulativeTestsByDate)
      .flat()
      .map(([, count]) => {
        const n = count / totalCount * 100;
        return ceil(n);
      });
  }

  private getTotalCount() {
    return this.campaignProgressionStatistics.scheduledIterations
      .map(iteration => iteration.testplanCount)
      .reduce((previousValue, currentValue) => previousValue + currentValue, 0);
  }

  private getIterationOrdinate() {
    return this.campaignProgressionStatistics.scheduledIterations
      .map(i => i.cumulativeTestsByDate)
      .flat()
      .map(([, count]) => count);
  }

  private getIterationAbscissa() {
    return this.campaignProgressionStatistics.scheduledIterations
      .map(i => i.cumulativeTestsByDate)
      .flat()
      .map(([dateAString]) => dateAString)
      .map(dateAString => this.parseUtcDateToLocaleDateTime(dateAString));
  }

  private getIterationNameTrace() {

    const x = this.campaignProgressionStatistics.scheduledIterations.map(iteration => {
      const scheduledStart = new Date(iteration.scheduledStart);
      const diffInDays = differenceInDays(new Date(iteration.scheduledEnd), scheduledStart) / 2;
      return addDays(scheduledStart, diffInDays).toISOString();
    });

    const xLabels = this.campaignProgressionStatistics.scheduledIterations
      .map((iteration) => iteration.name);

    const maxCount = this.campaignProgressionStatistics.scheduledIterations
      .map(iteration => iteration.testplanCount)
      .reduce((previousValue, currentValue) => previousValue + currentValue, 0);

    const y = this.campaignProgressionStatistics.scheduledIterations.map(() => maxCount * 1.05);

    return {
      x: x,
      y: y,
      type: 'scatter',
      mode: 'text',
      text: xLabels,
      hoverinfo: 'none',
      texttemplate: '<b>%{text}</b>'
    };
  }
}

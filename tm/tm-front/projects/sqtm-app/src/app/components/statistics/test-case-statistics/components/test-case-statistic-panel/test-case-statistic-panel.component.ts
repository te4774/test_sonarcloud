import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import {
  compareLevelEumItems,
  FilterOperation,
  getTestCaseImportanceChartColors,
  getTestCaseStatusChartColors,
  LevelEnumItem,
  Logger,
  SimpleFilter,
  TestCaseImportanceKeys,
  TestCaseImportanceLevelEnumItem,
  TestCaseStatistics,
  TestCaseStatus,
  TestCaseStatusKeys,
  TestCaseWeight
} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {testCaseStatisticsLogger} from '../../test-case-statistics.logger';
import * as camelcase from 'camelcase';
import {AbstractStatisticsPanelComponent} from '../../../abstract-statistics-panel.component';

const logger = testCaseStatisticsLogger.compose('TestCaseStatisticPanelComponent');

@Component({
  selector: 'sqtm-app-test-case-statistic-panel',
  templateUrl: './test-case-statistic-panel.component.html',
  styleUrls: ['./test-case-statistic-panel.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestCaseStatisticPanelComponent extends AbstractStatisticsPanelComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('coverageChart', {read: ElementRef})
  coverageChart: ElementRef;

  @ViewChild('statusChart', {read: ElementRef})
  statusChart: ElementRef;

  @ViewChild('importanceChart', {read: ElementRef})
  importanceChart: ElementRef;

  @ViewChild('stepChart', {read: ElementRef})
  stepChart: ElementRef;

  get statistics(): TestCaseStatistics {
    return this._statistics;
  }

  @Input()
  set statistics(value: TestCaseStatistics) {
    const hasPreviousSelection = this._statistics?.selectedIds?.length > 0;
    logger.debug('new value in charts :', [value]);
    this._statistics = value;
    this.updateAllCharts(hasPreviousSelection);
  }

  private _statistics: TestCaseStatistics;

  constructor(protected translateService: TranslateService,
              protected router: Router,
              protected renderer: Renderer2,
              protected zone: NgZone) {
    super(translateService, router, renderer, zone);
  }

  protected getAllCharts(): ElementRef[] {
    return [this.coverageChart, this.statusChart, this.importanceChart, this.stepChart];
  }

  protected buildAllCharts() {
    this.buildCoverageChart();
    this.buildStatusChart();
    this.buildImportanceChart();
    this.buildStepChart();
  }

  protected get logger(): Logger {
    return logger;
  }

  protected get searchBaseUrl(): string {
    return 'test-case';
  }

  protected get totalCount(): number {
    return this.statistics.selectedIds.length;
  }

  private buildCoverageChart() {
    const labels = ['sqtm-core.test-case-workspace.statistics.bound-reqs.legend.no-req',
      'sqtm-core.test-case-workspace.statistics.bound-reqs.legend.one-req',
      'sqtm-core.test-case-workspace.statistics.bound-reqs.legend.many-reqs'].map(key => this.translateService.instant(key));

    const values = [
      this._statistics.boundRequirementsStatistics.zeroRequirements,
      this._statistics.boundRequirementsStatistics.oneRequirement,
      this._statistics.boundRequirementsStatistics.manyRequirements,
    ];

    const noReqFilter = this.createCoveragesCountFilter('0');
    const oneReqFilter = this.createCoveragesCountFilter('1');
    const manyReqFilter = this.createCoveragesCountFilter('1', FilterOperation.GREATER);

    const filters = [noReqFilter, oneReqFilter, manyReqFilter];

    this.plotPieChart({
      titleKey: 'sqtm-core.test-case-workspace.statistics.bound-reqs.title',
      values,
      labels,
      elementRef: this.coverageChart,
      colors: ['#da4167', '#832161', '#3d2645'],
      customData: filters
    });
  }

  private buildStatusChart() {
    const orderedStatus: LevelEnumItem<TestCaseStatusKeys>[] = Object.values(TestCaseStatus)
      .sort(compareLevelEumItems);

    const labels = orderedStatus
      .map(status => this.translateService.instant(status.i18nKey));

    const values = orderedStatus
      .map(status => this.statistics.statusesStatistics[camelcase(status.id)]);

    const filters = this.buildStatusFilters(orderedStatus);

    this.plotPieChart({
      titleKey: 'sqtm-core.entity.generic.status.label',
      values,
      labels,
      elementRef: this.statusChart,
      colors: getTestCaseStatusChartColors(),
      customData: filters
    });
  }

  private buildStatusFilters(orderedStatus: LevelEnumItem<TestCaseStatusKeys>[]) {
    return orderedStatus.map((status: LevelEnumItem<TestCaseStatusKeys>) => ({
      id: 'status',
      operation: FilterOperation.IN,
      value: {
        kind: 'multiple-discrete-value',
        value: [{
          id: status.id,
          i18nLabelKey: status.i18nKey
        }]
      }
    }));
  }

  private buildImportanceChart() {
    const orderedWeight = this.orderWeightEnum();
    const filters = this.buildImportanceFilters(orderedWeight);
    const labels = orderedWeight
      .map(weight => this.translateService.instant(weight.i18nKey));
    const values = orderedWeight
      .map(weight => this.statistics.importanceStatistics[camelcase(weight.id)]);
    // const colors = orderedWeight
    //   .filter(weight => this.statistics.importanceStatistics[camelcase(weight.id)])
    //   .map((w) => w.chartColor);

    this.plotPieChart({
      titleKey: 'sqtm-core.entity.test-case.importance.label',
      values,
      labels,
      elementRef: this.importanceChart,
      colors: getTestCaseImportanceChartColors().reverse(),
      customData: filters
    });
  }

  private buildImportanceFilters(orderedWeight: LevelEnumItem<TestCaseImportanceKeys>[]) {
    return orderedWeight.map((weight: LevelEnumItem<TestCaseImportanceKeys>) => ({
      id: 'importance',
      operation: FilterOperation.IN,
      value: {
        kind: 'multiple-discrete-value',
        value: [{
          id: weight.id,
          i18nLabelKey: weight.i18nKey
        }]
      }
    }));
  }

  private orderWeightEnum(): TestCaseImportanceLevelEnumItem<TestCaseImportanceKeys>[] {
    return Object.values(TestCaseWeight)
      .sort(compareLevelEumItems)
      .reverse();
  }

  private buildStepChart() {
    const labels = [
      'sqtm-core.test-case-workspace.statistics.size.legend.zero-step',
      'sqtm-core.test-case-workspace.statistics.size.legend.b0and10',
      'sqtm-core.test-case-workspace.statistics.size.legend.b10and20',
      'sqtm-core.test-case-workspace.statistics.size.legend.above20']
      .map(key => this.translateService.instant(key));

    const values = [
      this._statistics.sizeStatistics.zeroSteps,
      this._statistics.sizeStatistics.between0And10Steps,
      this._statistics.sizeStatistics.between11And20Steps,
      this._statistics.sizeStatistics.above20Steps,
    ];

    const zeroStepFilter = this.createSingleSizeFilter('0');
    const b0and10StepFilter = this.createBetweenSizeFilter('1', '10');
    const b10and20StepFilter = this.createBetweenSizeFilter('11', '20');
    const above20StepFilter = this.createSingleSizeFilter('20', FilterOperation.GREATER);

    const filters = [zeroStepFilter, b0and10StepFilter, b10and20StepFilter, above20StepFilter];

    this.plotPieChart({
      titleKey: 'sqtm-core.entity.test-step.label.plural',
      values,
      labels,
      elementRef: this.stepChart,
      colors: ['#eab69f', '#e07a5f', '#8f5d5d', '#3d405b'],
      customData: filters
    });
  }

  private createCoveragesCountFilter(value, operation: FilterOperation = FilterOperation.EQUALS) {
    const filter: SimpleFilter = {
      id: 'coveragesCount',
      operation: operation,
      value: {
        kind: 'single-string-value',
        value: value
      }
    };
    return filter;
  }

  private createSingleSizeFilter(value, operation: FilterOperation = FilterOperation.EQUALS) {
    const filter: SimpleFilter = {
      id: 'stepCount',
      operation: operation,
      value: {
        kind: 'single-string-value',
        value: value
      }
    };
    return filter;
  }

  private createBetweenSizeFilter(min: string, max: string) {
    const filter: SimpleFilter = {
      id: 'stepCount',
      operation: FilterOperation.BETWEEN,
      value: {
        kind: 'multiple-string-value',
        value: [min, max]
      }
    };
    return filter;
  }

}

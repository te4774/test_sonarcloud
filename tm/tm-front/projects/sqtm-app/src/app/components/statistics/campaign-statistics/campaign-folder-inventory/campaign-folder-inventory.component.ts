import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {CampaignTestInventoryStatistics, ExecutionStatus, ExecutionStatusCount, ExecutionStatusKeys,} from 'sqtm-core';
import * as _ from 'lodash';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-campaign-folder-inventory',
  templateUrl: './campaign-folder-inventory.component.html',
  styleUrls: ['./campaign-folder-inventory.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CampaignFolderInventoryComponent implements OnInit {

  private _inventory: CampaignAggregate[];

  ExecutionStatus = ExecutionStatus;

  get extendedInventory() {
    return this._inventory;
  }

  @Input()
  set inventory(inventory: CampaignTestInventoryStatistics[]) {
    const campaignAggregates: CampaignAggregate[] = this.getCampaignAggregates(inventory);
    const resumeRow = this.getResumeRow(campaignAggregates);
    campaignAggregates.push(resumeRow);
    this._inventory = campaignAggregates;
  }

  @Input()
  disabledExecutionStatus: ExecutionStatusKeys[];

  constructor(private readonly translateService: TranslateService) { }

  ngOnInit(): void {
  }

  private getExecutionRate(executed: number, total: number) {
    return Math.floor((executed / total) * 100) || 0;
  }

  private getCampaignAggregates(inventory: CampaignTestInventoryStatistics[]) {
    return inventory.map(campaign => {
      const total: number = Object.values<number>(campaign.statistics).reduce(_.add);
      const executed: number = Object
        .entries<number>(campaign.statistics)
        .filter(e => ExecutionStatus[e[0]].terminal)
        .map(e => e[1])
        .reduce(_.add);
      return {
        isTotal: false,
        total,
        executed,
        executionRate: this.getExecutionRate(executed, total),
        campaignName: campaign.campaignName,
        ...campaign.statistics
      };
    });
  }

  private getResumeRow(campaignAggregates: CampaignAggregate[]) {
    const resumeRow = campaignAggregates.reduce((totalAggregate, campaignAggregate: CampaignAggregate) => {
      return {
        ...totalAggregate,
        total: totalAggregate.total + campaignAggregate.total,
        executed: totalAggregate.executed + campaignAggregate.executed,
        FAILURE: totalAggregate.FAILURE + campaignAggregate.FAILURE,
        SUCCESS: totalAggregate.SUCCESS + campaignAggregate.SUCCESS,
        RUNNING: totalAggregate.RUNNING + campaignAggregate.RUNNING,
        READY: totalAggregate.READY + campaignAggregate.READY,
        BLOCKED: totalAggregate.BLOCKED + campaignAggregate.BLOCKED,
        UNTESTABLE: totalAggregate.UNTESTABLE + campaignAggregate.UNTESTABLE,
        SETTLED: totalAggregate.SETTLED + campaignAggregate.SETTLED,
      };
    }, {
      total: 0,
      executed: 0,
      executionRate: 0,
      campaignName: this.translateService.instant('sqtm-core.generic.label.total'),
      FAILURE: 0,
      SUCCESS: 0,
      RUNNING: 0,
      READY: 0,
      BLOCKED: 0,
      UNTESTABLE: 0,
      SETTLED: 0,
      isTotal: true
    });

    resumeRow.executionRate = this.getExecutionRate(resumeRow.executed, resumeRow.total);
    return resumeRow;
  }

}

export interface CampaignAggregate extends ExecutionStatusCount {
  total: number;
  executed: number;
  executionRate?: number;
  campaignName: string;
  isTotal: boolean;
}

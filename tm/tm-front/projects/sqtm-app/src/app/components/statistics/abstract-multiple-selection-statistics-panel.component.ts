import {Directive, NgZone, Renderer2} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {AbstractStatisticsPanelComponent} from './abstract-statistics-panel.component';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class AbstractMultipleSelectionStatisticsPanelComponent extends AbstractStatisticsPanelComponent {

  protected constructor(protected translateService: TranslateService,
                        protected router: Router,
                        protected renderer: Renderer2,
                        protected zone: NgZone) {
    super(translateService, router, renderer, zone);
  }
}




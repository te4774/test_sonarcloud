import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MilestonesTagComponent} from './milestones-tag/milestones-tag.component';
import {MilestoneDialogComponent} from './milestone-dialog/milestone-dialog.component';
import {TranslateModule} from '@ngx-translate/core';
import {DialogModule, GridModule, WorkspaceCommonModule} from 'sqtm-core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzDividerModule} from 'ng-zorro-antd/divider';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzPopoverModule} from 'ng-zorro-antd/popover';
import {NzTagModule} from 'ng-zorro-antd/tag';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [MilestonesTagComponent, MilestoneDialogComponent],
    imports: [
        CommonModule,
        GridModule,
        WorkspaceCommonModule,
        TranslateModule,
        NzTagModule,
        NzPopoverModule,
        NzButtonModule,
        NzIconModule,
        NzDividerModule,
        NzCheckboxModule,
        FormsModule,
        DialogModule
    ],
    exports: [MilestonesTagComponent]
})
export class MilestoneModule {
}

import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {
  dateColumn,
  DialogReference,
  Extendable,
  Fixed,
  grid,
  GridDefinition,
  GridFilter,
  GridService,
  gridServiceFactory,
  MilestoneStatus,
  MilestoneView,
  PaginationConfigBuilder,
  ReferentialDataService,
  RestService,
  richTextColumn,
  selectRowColumn,
  sortDate,
  textColumn,
  ToggleSelectionHeaderRendererComponent
} from 'sqtm-core';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {MilestoneDialogConfiguration} from './milestone.dialog.configuration';
import {milestoneLogger} from '../milestone-logger';

export function entityMilestoneTableDefinition(): GridDefinition {
  return grid('milestones-grid')
    .withColumns(
      [
        selectRowColumn()
          .changeWidthCalculationStrategy(new Fixed(40))
          .withHeaderRenderer(ToggleSelectionHeaderRendererComponent),
        textColumn('label')
          .withI18nKey('sqtm-core.entity.generic.name.label')
          .changeWidthCalculationStrategy(new Extendable(100, 0.8)),
        textColumn('status')
          .withI18nKey('sqtm-core.entity.milestone.status.label')
          .withEnumRenderer(MilestoneStatus, false, true)
          .isEditable(false),
        dateColumn('endDate')
          .withI18nKey('sqtm-core.entity.milestone.term.label')
          .withSortFunction(sortDate),
        richTextColumn('description')
          .disableSort()
          .withI18nKey('sqtm-core.entity.milestone.description')
          .changeWidthCalculationStrategy(new Extendable(100, 1))
      ]
    )
    .enableShowAll()
    .withPagination(
      new PaginationConfigBuilder()
        .fixPaginationSize(-1)
    )
    .disableRightToolBar()
    .enableMultipleColumnsFiltering(['label'])
    .build();
}

const milestoneDialogLogger = milestoneLogger.compose('MilestoneDialogComponent');

@Component({
  selector: 'sqtm-app-milestone-dialog',
  templateUrl: './milestone-dialog.component.html',
  styleUrls: ['./milestone-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: GridDefinition,
      useFactory: entityMilestoneTableDefinition,
      deps: []
    },
    {
      provide: GridService,
      useFactory: gridServiceFactory,
      deps: [RestService, GridDefinition, ReferentialDataService]
    }
  ]

})
export class MilestoneDialogComponent implements OnInit, AfterViewInit, OnDestroy {

  private unsub$ = new Subject<void>();

  configuration: MilestoneDialogConfiguration;

  constructor(private gridService: GridService,
              private dialogReference: DialogReference<MilestoneDialogConfiguration, number[]>) {
    this.configuration = this.dialogReference.data;
  }

  ngOnInit() {
    this.initializeFilters();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      const dataRows = this.configuration.milestoneViews.map((row: MilestoneView) => {
        const data = {...row};
        return {id: data.id, data: data};
      });
      this.gridService.loadInitialDataRows(dataRows, dataRows.length, this.configuration.checkedIds);
    });
  }

  confirm() {
    this.gridService.selectedRows$.pipe(
      takeUntil(this.unsub$)
    ).subscribe(rows => {
      const selectedIds: number[] = rows.map(row => row.data.id);
      milestoneDialogLogger.debug(`Milestone ids to bind : ${selectedIds}`);
      this.dialogReference.result = selectedIds;
      this.dialogReference.close();
    });
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  private initializeFilters() {
    const filters: GridFilter[] = [{
      id: 'label', active: false, initialValue: {kind: 'single-string-value', value: ''}, tiedToPerimeter: false
    }];
    this.gridService.addFilters(filters);
  }

  handleResearchInput($event: string) {
    this.gridService.applyMultiColumnsFilter($event);
  }
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  EnvironmentSelectionPanelComponent
} from './components/environment-selection-panel/environment-selection-panel.component';
import {TranslateModule} from '@ngx-translate/core';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {GridModule, WorkspaceCommonModule} from 'sqtm-core';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';


@NgModule({
  declarations: [
    EnvironmentSelectionPanelComponent
  ],
  exports: [
    EnvironmentSelectionPanelComponent
  ],
    imports: [
        CommonModule,
        TranslateModule,
        NzIconModule,
        WorkspaceCommonModule,
        GridModule,
        NzToolTipModule
    ]
})
export class AutomatedExecutionEnvironmentModule {
}

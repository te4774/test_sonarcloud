import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WorkflowDialogComponent} from './dialogs/workflow-dialog/workflow-dialog.component';
import {DialogModule, SvgModule, UiManagerModule, WorkspaceCommonModule, WorkspaceLayoutModule} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {
  AutomationWorkspaceHeaderComponent
} from './components/automation-workspace-header/automation-workspace-header.component';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {UuidCellRendererComponent} from './components/cell-renderers/uuid-cell-renderer/uuid-cell-renderer.component';
import {NzTypographyModule} from 'ng-zorro-antd/typography';


@NgModule({
  declarations: [
    WorkflowDialogComponent,
    AutomationWorkspaceHeaderComponent,
    UuidCellRendererComponent
  ],
  imports: [
    CommonModule,
    SvgModule,
    WorkspaceCommonModule,
    UiManagerModule,
    TranslateModule.forChild(),
    WorkspaceLayoutModule,
    NzIconModule,
    NzToolTipModule,
    DialogModule,
    NzButtonModule,
    NzTypographyModule
  ],
  exports: [
    WorkflowDialogComponent,
    AutomationWorkspaceHeaderComponent
  ]
})
export class AutomationModule {
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AbstractCellRendererComponent, ColumnDefinitionBuilder, GridService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'sqtm-app-uuid-cell-renderer',
  template: `
  <ng-container *ngIf="columnDisplay && row">
    <div class="full-width full-height flex-column" style="align-items: center">
      <i nz-typography nzCopyable [nzCopyIcons]="['sqtm-core-generic:copy', 'sqtm-core-generic:confirm']"
         [nzCopyText]="getUuid()"
         class="action-icon-size"></i>
    </div>
  </ng-container>
  `,
  styleUrls: ['./uuid-cell-renderer.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UuidCellRendererComponent extends AbstractCellRendererComponent implements OnInit {

  constructor(gridService: GridService, cdr: ChangeDetectorRef, private translateService: TranslateService) {
    super(gridService, cdr);
  }

  ngOnInit(): void {
  }

  getUuid() {
    return this.row.data[this.columnDisplay.id];
  }
}

export function uuidColumn(id: string): ColumnDefinitionBuilder {
  return new ColumnDefinitionBuilder(id).withHeaderPosition('center').withRenderer(UuidCellRendererComponent);
}

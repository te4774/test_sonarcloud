import {ChangeDetectionStrategy, Component, Input, OnInit, ViewContainerRef} from '@angular/core';
import {DialogService} from 'sqtm-core';
import {WorkflowDialogComponent} from '../../dialogs/workflow-dialog/workflow-dialog.component';

@Component({
  selector: 'sqtm-app-automation-workspace-header',
  templateUrl: './automation-workspace-header.component.html',
  styleUrls: ['./automation-workspace-header.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomationWorkspaceHeaderComponent implements OnInit {
  @Input()
  mainTitleI18NKey: string;
  @Input()
  tableNameI18NKey: string;
  @Input()
  helpI18NKey: string;

  constructor(private dialogService: DialogService, private vcr: ViewContainerRef) {
  }

  ngOnInit(): void {
  }

  showWorkFlowDialog() {
    this.dialogService.openDialog({
      id: 'automation-workflow',
      viewContainerReference: this.vcr,
      component: WorkflowDialogComponent
    });
  }
}

import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {getSupportedBrowserLang} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-workflow-dialog',
  templateUrl: './workflow-dialog.component.html',
  styleUrls: ['./workflow-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkflowDialogComponent implements OnInit {

  constructor(private translateService: TranslateService) { }

  ngOnInit(): void {
  }

  getWorkflowImage() {
    const baseImageSrc = './assets/sqtm-core/img/automation/workflow_autom_';
    const browserLang = getSupportedBrowserLang(this.translateService);
    switch (browserLang) {
      case 'fr':
        return baseImageSrc + 'fr.png';
      case 'de':
        return baseImageSrc + 'de.png';
      default:
        return baseImageSrc + 'en.png';
    }
  }
}

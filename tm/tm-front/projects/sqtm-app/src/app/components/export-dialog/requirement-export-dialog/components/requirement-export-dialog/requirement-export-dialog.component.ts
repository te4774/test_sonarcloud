import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {RequirementExportDialogConfiguration} from './requirement-export-dialog.configuration';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DialogReference, RestService} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'sqtm-app-requirement-export-dialog',
  templateUrl: './requirement-export-dialog.component.html',
  styleUrls: ['./requirement-export-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class RequirementExportDialogComponent implements OnInit {

  data: RequirementExportDialogConfiguration;
  formGroup: FormGroup;

  constructor(public dialogReference: DialogReference<RequirementExportDialogConfiguration>,
              private translateService: TranslateService,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private restService: RestService) {
    this.data = this.dialogReference.data;
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group(
      {
        format: this.fb.control('xls', [Validators.required]),
        fileName: this.fb.control(this.initFileName(), [Validators.required]),
        addLinkedLowLevelReq: this.fb.control(true, []),
        editableRichText: this.fb.control(true, [])
      }
    );
  }

  private initFileName() {
    const date = new Date();
    const newDate = this.datePipe.transform(date, 'yyyyMMdd_HHmmss');
    return `${this.translateService.instant('sqtm-core.requirement-workspace.dialog.export.file-name-value')}_${newDate}`;
  }

  getFileName() {
    let fileName = this.formGroup.controls.fileName.value;

    if (this.formGroup.controls.format.value === 'xls') {
      fileName = fileName + '.xls';
    } else {
      fileName = fileName + '.csv';
    }
    return fileName;
  }

  close() {
    this.dialogReference.close();
  }

  getFormat() {
    return [{id: 'xls', label: 'XLS'}];
  }

  doExport() {
    const params = {
      nodes: this.data.nodes.toString(),
      libraries: this.data.libraries.toString(),
      filename: this.formGroup.controls.fileName.value,
      'add-linked-low-level-req': this.formGroup.controls.addLinkedLowLevelReq.value,
      'keep-rte-format': this.formGroup.controls.editableRichText.value
    };
    return this.restService.buildExportUrlWithParams(`requirement/export/content/${this.formGroup.controls.format.value}`, params);
  }
}

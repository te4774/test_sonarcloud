import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  AttachmentModule,
  DialogModule,
  NavBarModule,
  SvgModule,
  WorkspaceCommonModule,
  WorkspaceLayoutModule
} from 'sqtm-core';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {NzPopoverModule} from 'ng-zorro-antd/popover';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {
  RequirementExportDialogComponent
} from './components/requirement-export-dialog/requirement-export-dialog.component';

@NgModule({
  declarations: [RequirementExportDialogComponent],
  imports: [
    CommonModule,
    SvgModule,
    WorkspaceCommonModule,
    NavBarModule,
    NzMenuModule,
    NzIconModule,
    NzToolTipModule,
    NzPopoverModule,
    NzInputModule,
    DialogModule,
    WorkspaceLayoutModule,
    NzDropDownModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    NzButtonModule,
    FormsModule,
    NzSelectModule,
    AttachmentModule,
    NzCheckboxModule
  ]
})
export class RequirementExportDialogModule {
}

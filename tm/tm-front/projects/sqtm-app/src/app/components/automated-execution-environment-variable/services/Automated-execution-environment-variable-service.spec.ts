import {AutomatedExecutionEnvironmentVariableService} from './automated-execution-environment-variable.service';
import {TestBed} from '@angular/core/testing';
import {AutomationEnvironmentTagHolder, BoundEnvironmentVariable, EvInputType, RestService} from 'sqtm-core';
import {mockRestService} from '../../../utils/testing-utils/mocks.service';
import {of} from 'rxjs';
import {withLatestFrom} from 'rxjs/operators';
import SpyObj = jasmine.SpyObj;

describe('AutomatedExecutionEnvironmentVariableService', () => {
  let service: AutomatedExecutionEnvironmentVariableService;
  let restService: SpyObj<RestService>;

  beforeEach(() => {
    restService = mockRestService();

    TestBed.configureTestingModule({
      providers: [
        {provide: RestService, useValue: restService},
        {provide: AutomatedExecutionEnvironmentVariableService, useClass: AutomatedExecutionEnvironmentVariableService}
      ]
    });
    service = TestBed.inject(AutomatedExecutionEnvironmentVariableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load a environment variable panel', (done) => {
    restService.get.and.returnValue(of(getInitialModel()));

    service.load(1, AutomationEnvironmentTagHolder.PROJECT).pipe(
      withLatestFrom(service.state$)
    ).subscribe(([, state]) => {
      expect(state.boundEnvironmentVariables.length).toEqual(2);
      done();
    });

    expect(service).toBeTruthy();

  });

  it('should set environment variable value from project', (done) => {
    restService.get.and.returnValue(of(getInitialModel()));
    service.load(1, AutomationEnvironmentTagHolder.PROJECT).subscribe();


    service.setEnvironmentVariableValue('new value', 1, 1, AutomationEnvironmentTagHolder.PROJECT)
      .subscribe(([state, ]) => {
        const updatedEnvironmentVariable: BoundEnvironmentVariable =
          state.boundEnvironmentVariables.find(environmentVariable => environmentVariable.id === 1);
        expect(updatedEnvironmentVariable.value).toEqual('new value');
        expect(restService.post.calls.count()).toEqual(1);
        done();
      });
  });

  it('should set environment variable value from execution dialog', (done) => {
    restService.get.and.returnValue(of(getInitialModel()));
    service.load(1, AutomationEnvironmentTagHolder.EXECUTION_DIALOG).subscribe();

    service.setEnvironmentVariableValue('new value', 1, 1, AutomationEnvironmentTagHolder.EXECUTION_DIALOG)
      .subscribe(([state, ]) => {
        const updatedEnvironmentVariable: BoundEnvironmentVariable =
          state.boundEnvironmentVariables.find(environmentVariable => environmentVariable.id === 1);
        expect(updatedEnvironmentVariable.value).toEqual('new value');
        expect(restService.post.calls.count()).toEqual(0);
        done();
      });
  });

  it('should reset environment variable to default value', (done) => {
    restService.get.and.returnValue(of(getInitialModel()));
    service.load(1, AutomationEnvironmentTagHolder.PROJECT).subscribe();

    restService.post.and.returnValue(of({defaultValue: 'defaultValue'}));

    service.resetEnvironmentVariableDefaultValue(1, 1, AutomationEnvironmentTagHolder.PROJECT).pipe(
     withLatestFrom(service.state$)
    ).subscribe(([, state]) => {
      const updatedEnvironmentVariable: BoundEnvironmentVariable =
        state.boundEnvironmentVariables.find(environmentVariable => environmentVariable.id === 1);
        expect(updatedEnvironmentVariable.value).toEqual('defaultValue');
        done();
      });
  });

  function getInitialModel(): { boundEnvironmentVariables: BoundEnvironmentVariable[] } {
    return {
      boundEnvironmentVariables: [
        {
          id: 1,
          name: 'environment variable 1',
          code: 'EV1',
          inputType: EvInputType.PLAIN_TEXT,
          boundToServer: true,
          options: [],
          value: 'value'
        },
        {
          id: 2,
          name: 'environment variable 2',
          code: 'EV2',
          inputType: EvInputType.DROPDOWN_LIST,
          boundToServer: true,
          options: [
            {
              evId: 1,
              label: 'opt1',
              code: 'opt1',
              position: 0
            },
            {
              evId: 2,
              label: 'opt2',
              code: 'opt2',
              position: 1
            }
          ],
          value: 'opt1'
        },
      ]
    };
  }
});



import {AfterViewInit, ChangeDetectionStrategy, Component, Input, ViewChild} from '@angular/core';
import {
  BoundEnvironmentVariable,
  EvInputType,
  Option,
  EnvironmentVariableOption,
  EditableTextFieldComponent,
  EditableSelectFieldComponent,
  AutomationEnvironmentTagHolder
} from 'sqtm-core';
import {
  AutomatedExecutionEnvironmentVariableService, AutomatedExecutionEnvironmentVariableState
} from '../../services/automated-execution-environment-variable.service';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';
import {Validators} from '@angular/forms';

@Component({
  selector: 'sqtm-app-automated-execution-environment-variable-panel',
  templateUrl: './automated-execution-environment-variable-panel.component.html',
  styleUrls: ['./automated-execution-environment-variable-panel.component.less'],
  providers: [
    {
      provide: AutomatedExecutionEnvironmentVariableService,
      useClass: AutomatedExecutionEnvironmentVariableService
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomatedExecutionEnvironmentVariablePanelComponent implements AfterViewInit {

  state$: Observable<AutomatedExecutionEnvironmentVariableState>;

  @Input()
  entityId: number;

  @Input()
  entityType: AutomationEnvironmentTagHolder;

  @ViewChild('textFieldEVValue')
  textFieldEVValue: EditableTextFieldComponent;

  @ViewChild('selectFieldEVValue')
  selectFieldEVValue: EditableSelectFieldComponent;

  dropdownType: EvInputType = EvInputType.DROPDOWN_LIST;

  executionDialogType: AutomationEnvironmentTagHolder = AutomationEnvironmentTagHolder.EXECUTION_DIALOG;

  constructor(public automatedExecutionEnvironmentVariableService: AutomatedExecutionEnvironmentVariableService) {
    this.state$ = automatedExecutionEnvironmentVariableService.state$;
  }

  ngAfterViewInit(): void {
    this.automatedExecutionEnvironmentVariableService.load(this.entityId, this.entityType).subscribe();
  }

  getDropdownListOptions(options: EnvironmentVariableOption[]): Option[] {
    return options.map(option => ({
      value: option.label,
      label: option.label
    }));
  }

  textHandleProjectValueChange(value: string, boundVariable: BoundEnvironmentVariable) {
    this.textFieldEVValue.beginAsync();
    this.automatedExecutionEnvironmentVariableService.setEnvironmentVariableValue(value, boundVariable.id,
      this.entityId, this.entityType)
      .subscribe(() => {
        this.textFieldEVValue.markForCheck();
        this.textFieldEVValue.disableEditMode();
        this.textFieldEVValue.endAsync();
      });
  }

 getValidators() {
  return [Validators.maxLength(255)];
 }

  selectHandleProjectValueChange(selectedOption: Option, boundVariable: BoundEnvironmentVariable) {
    let updatedValue: string = selectedOption?.value;
    if (updatedValue == null) {
      updatedValue = '';
    }
    this.selectFieldEVValue.beginAsync();
    this.automatedExecutionEnvironmentVariableService.setEnvironmentVariableValue(updatedValue, boundVariable.id,
      this.entityId, this.entityType)
      .subscribe(() => {
        this.selectFieldEVValue.disableEditMode();
        this.selectFieldEVValue.markForCheck();
      });
  }

  resetDefaultValue(boundEnvironmentVariable: BoundEnvironmentVariable) {
    if (boundEnvironmentVariable.inputType === EvInputType.PLAIN_TEXT) {
      this.resetTextFieldDefaultValue(boundEnvironmentVariable);
    } else {
      this.resetSelectFieldDefaultValue(boundEnvironmentVariable);
    }
  }

  handleBoundServerChanged() {
    this.automatedExecutionEnvironmentVariableService.handleBoundServerChanged(this.entityId, this.entityType);
  }

  private resetTextFieldDefaultValue(boundEnvironmentVariable: BoundEnvironmentVariable) {
    this.textFieldEVValue.beginAsync();
    this.automatedExecutionEnvironmentVariableService
      .resetEnvironmentVariableDefaultValue(boundEnvironmentVariable.id, this.entityId, this.entityType)
      .subscribe(() => {
        this.textFieldEVValue.markForCheck();
        this.textFieldEVValue.endAsync();
      });
  }

  private resetSelectFieldDefaultValue(boundEnvironmentVariable: BoundEnvironmentVariable) {
    this.selectFieldEVValue.beginAsync();
    this.automatedExecutionEnvironmentVariableService
      .resetEnvironmentVariableDefaultValue(boundEnvironmentVariable.id, this.entityId, this.entityType)
      .subscribe(() => {
        this.selectFieldEVValue.disableEditMode();
        this.selectFieldEVValue.markForCheck();
      });
  }

  getBoundEnvironmentVariables(): Observable<BoundEnvironmentVariable[]> {
    return this.automatedExecutionEnvironmentVariableService.state$.pipe(
      take(1),
      map( state => state.boundEnvironmentVariables)
    );
  }
}

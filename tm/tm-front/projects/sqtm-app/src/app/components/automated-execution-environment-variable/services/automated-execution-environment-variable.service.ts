import {Injectable} from '@angular/core';
import {AutomationEnvironmentTagHolder, BoundEnvironmentVariable, createStore, RestService} from 'sqtm-core';
import {Observable} from 'rxjs';
import {catchError, map, pluck, take, tap, withLatestFrom} from 'rxjs/operators';

@Injectable()
export class AutomatedExecutionEnvironmentVariableService {

  constructor(public readonly restService: RestService) {
  }

  private readonly store =
    createStore<AutomatedExecutionEnvironmentVariableState>(getInitialAutomatedExecutionEnvironmentVariableState());

  public state$: Observable<AutomatedExecutionEnvironmentVariableState> = this.store.state$;

  public load(id: number, entityType: AutomationEnvironmentTagHolder): Observable<any> {
    return this.initializeAutomatedExecutionEnvironmentVariables(id, entityType);
  }

  protected getRootUrl(): string {
    return 'bound-environment-variables';
  }

  setEnvironmentVariableValue(value: string, environmentVariableId: number, entityId: number, entityType: AutomationEnvironmentTagHolder) {
    if (entityType === AutomationEnvironmentTagHolder.PROJECT) {
      return this.setEnvironmentVariableValueServerSide(value, environmentVariableId, entityId).pipe(
        withLatestFrom(this.store.state$),
        map(([, state]: [any, AutomatedExecutionEnvironmentVariableState]) =>
          this.updateStateWithNewBoundEVValue(state, value, environmentVariableId)),
        tap(([state, ]: [AutomatedExecutionEnvironmentVariableState, any]) => this.store.commit(state))
      );
    } else {
       return this.store.state$.pipe(
        take(1),
        map((state: AutomatedExecutionEnvironmentVariableState) =>
          this.updateStateWithNewBoundEVValue(state, value, environmentVariableId)),
         tap(([state, ]: [AutomatedExecutionEnvironmentVariableState, any]) => this.store.commit(state))
      );
    }
  }

  private setEnvironmentVariableValueServerSide(value: string, environmentVariableId: number,
                                                entityId: number): Observable<void> {
    const urlPart = [
      this.getRootUrl(),
      entityId.toString(),
      'project',
      environmentVariableId.toString(),
      'value'
    ];
    return this.restService.post(urlPart, {value: value});
  }

  private updateStateWithNewBoundEVValue(state: AutomatedExecutionEnvironmentVariableState, value: string,
                                         environmentVariableId: number): any {

    const boundEnvironmentVariables: BoundEnvironmentVariable[] = state.boundEnvironmentVariables;

    const boundEnvironmentVariable: BoundEnvironmentVariable = boundEnvironmentVariables
      .find(environmentVariable => environmentVariable.id === environmentVariableId);

    const index = boundEnvironmentVariables.indexOf(boundEnvironmentVariable);

    boundEnvironmentVariable.value = value;
    boundEnvironmentVariables[index] = boundEnvironmentVariable;

    const responseState: AutomatedExecutionEnvironmentVariableState = {
      ...state,
      boundEnvironmentVariables: boundEnvironmentVariables
    };

    return [responseState, value];
  }

  public handleBoundServerChanged(entityId: number, entityType: AutomationEnvironmentTagHolder): void {
    this.reloadBoundEnvironmentVariables(entityId, entityType);
  }

  resetEnvironmentVariableDefaultValue(boundEnvironmentVariableId: number, entityId: number, entityType: AutomationEnvironmentTagHolder) {
    return this.resetEvDefaultValueServerSide(boundEnvironmentVariableId, entityId, entityType).pipe(
      withLatestFrom(this.state$),
      map(([value, state]: [any, AutomatedExecutionEnvironmentVariableState]) =>
        this.updateStateWithNewBoundEVValue(state, value.defaultValue, boundEnvironmentVariableId)),
      map(([state, value]: [AutomatedExecutionEnvironmentVariableState, string]) => {
        this.store.commit(state);
        return value;
      }),
    );
  }

  private resetEvDefaultValueServerSide(environmentVariableId: number,
                                        entityId: number, entityType: AutomationEnvironmentTagHolder): Observable<any> {
    const urlPart = [
      this.getRootUrl(),
      entityId.toString(),
      'project',
      environmentVariableId.toString(),
      'reset'
    ];
    return this.restService.post<any>(urlPart, {entityType: entityType.toString()});
  }


  private initializeAutomatedExecutionEnvironmentVariables(id: number, entityType: AutomationEnvironmentTagHolder) {
    return this.getAutomatedExecutionEnvironmentVariables(id, entityType).pipe(
      withLatestFrom(this.store.state$),
      map(([response, state]: [BoundEnvironmentVariable[], AutomatedExecutionEnvironmentVariableState]) =>
        this.mergeResponseIntoState(response, state)),
      tap((state: AutomatedExecutionEnvironmentVariableState) => this.store.commit(state)),
      catchError(() => this.handleLoadError())
    );
  }

  private getAutomatedExecutionEnvironmentVariables(entityId: number, entityType: AutomationEnvironmentTagHolder)
    : Observable<BoundEnvironmentVariable[]> {

    const type: string = entityType === AutomationEnvironmentTagHolder.TEST_AUTOMATION_SERVER ? 'test-automation-server' : 'project';

    const urlParts = [
      this.getRootUrl(),
      entityId.toString(),
      type
    ];

    return this.restService.get<{ boundEnvironmentVariables: BoundEnvironmentVariable[] }>(urlParts).pipe(
      pluck('boundEnvironmentVariables')
    );
  }

  private mergeResponseIntoState(response: BoundEnvironmentVariable[], state: AutomatedExecutionEnvironmentVariableState) {
    return {
      ...state,
      boundEnvironmentVariables: response
    };
  }

  private handleLoadError() {
    return this.store.state$.pipe(
      take(1),
      tap((state: any) => this.store.commit({
        ...state,
        boundEnvironmentVariables: []
      }))
    );
  }

  private reloadBoundEnvironmentVariables(entityId: number, entityType: AutomationEnvironmentTagHolder) {
     this.load(entityId, entityType).subscribe();
  }
}

export interface AutomatedExecutionEnvironmentVariableState {
  boundEnvironmentVariables: BoundEnvironmentVariable[];
}

export function getInitialAutomatedExecutionEnvironmentVariableState(): AutomatedExecutionEnvironmentVariableState {
  return {
    boundEnvironmentVariables: []
  };
}

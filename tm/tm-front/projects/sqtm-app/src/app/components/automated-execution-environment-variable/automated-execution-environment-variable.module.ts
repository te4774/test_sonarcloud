import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WorkspaceCommonModule} from 'sqtm-core';
import {TranslateModule} from '@ngx-translate/core';
import {
  AutomatedExecutionEnvironmentVariablePanelComponent
} from './components/automated-execution-environment-variable-panel/automated-execution-environment-variable-panel.component';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {NzIconModule} from 'ng-zorro-antd/icon';


@NgModule({
  declarations: [
    AutomatedExecutionEnvironmentVariablePanelComponent
  ],
  exports: [
    AutomatedExecutionEnvironmentVariablePanelComponent
  ],
  imports: [
    CommonModule,
    WorkspaceCommonModule,
    TranslateModule,
    NzToolTipModule,
    NzIconModule
  ]
})
export class AutomatedExecutionEnvironmentVariableModule { }

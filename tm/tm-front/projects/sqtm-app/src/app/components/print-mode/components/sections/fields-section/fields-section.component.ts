import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {FieldsSection} from '../../../state/PrintData.state';

@Component({
  selector: 'sqtm-app-fields-section',
  templateUrl: './fields-section.component.html',
  styleUrls: ['../sections-common.less', './fields-section.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FieldsSectionComponent {
  @Input() section: FieldsSection;
}

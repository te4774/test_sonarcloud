export interface PrintDataState {
  name: string;
  entityNameKey: string;
  sections: Section[];
}

export enum SectionKind {
  information = 'information',
  fields = 'fields',
  table = 'table',
  testCaseScenario = 'testCaseScenario',
  requirementStats = 'requirementsStats',
}

export interface InformationSection {
  title: string;
  kind: SectionKind.information;
  fields: PrintField[];
  optionalFields?: PrintField[];
  customFields?: PrintField[];
  description: string;
}

export interface FieldsSection {
  title: string;
  kind: SectionKind.fields;
  fields: PrintField[];
}

export interface TableSection {
  title: string;
  kind: SectionKind.table;
  header: String[];
  rows: TableRow[];
}

export function emptyTableSection(title: string): TableSection {
  return {
    title,
    kind: SectionKind.table,
    header: [],
    rows: [],
  };
}

export interface TableRow {
  values: String[];
  displayInItalic?: boolean;
}

export interface RequirementStatsSection {
  title: string;
  kind: SectionKind.requirementStats;
  haveChildren: boolean;
  total: RequirementStat;
  currentVersion: RequirementStat;
  children: RequirementStat;
  childrenAmount: number;
  coveredChildrenAmount: number;
  highLevelRequirement: boolean;
}

export interface RequirementStat {
  plannedTestCase: number;
  allTestCaseCount: number;
  validatedTestCases: number;
  redactedTestCase: number;
  verifiedTestCase: number;
  executedTestCase: number;
}

export type Section = InformationSection
  | FieldsSection
  | TestCaseScenarioSection
  | TableSection
  | RequirementStatsSection;

export interface PrintField {
  name: string;
  value: string;
  renderAsHtml?: boolean;
}

/* Test Case */

export interface TestCaseScenarioSectionBase {
  title: string;
  kind: SectionKind.testCaseScenario;
}

export enum PrintTestStepKind {
  action = 'action',
  call = 'call',
  keyword = 'keyword',
}

interface ActionStepRow {
  kind: PrintTestStepKind.action;
  action: string;
  expectedResult: string;
  customFieldValues: PrintField[];
  linkedRequirementCount: number;
  attachmentCount: number;
}

interface CallTestCaseStepRow {
  kind: PrintTestStepKind.call;
  calledTestCaseName: string;
  datasetName: string;
  steps: StepRow[];
}

export type StepRow = ActionStepRow | CallTestCaseStepRow;

export interface StandardTestCaseScenarioSection extends TestCaseScenarioSectionBase {
  testCaseKind: 'STANDARD';
  prerequisite: string;
  steps: StepRow[];
}

export interface ScriptedTestCaseScenarioSection extends TestCaseScenarioSectionBase {
  testCaseKind: 'GHERKIN';
  script: string;
}

export interface KeywordTestCaseStep {
  testCaseKind: 'KEYWORD';
  keyword: string;
  action: string;
  datatable?: string;
  docstring?: string;
  comment?: string;
}

export interface KeywordTestCaseScenarioSection extends TestCaseScenarioSectionBase {
  testCaseKind: 'KEYWORD';
  steps: KeywordTestCaseStep[];
}

export type TestCaseScenarioSection = StandardTestCaseScenarioSection
  | KeywordTestCaseScenarioSection
  | ScriptedTestCaseScenarioSection;

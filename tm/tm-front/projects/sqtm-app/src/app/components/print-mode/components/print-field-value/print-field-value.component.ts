import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {PrintField} from '../../state/PrintData.state';

@Component({
  selector: 'sqtm-app-print-field-value',
  template: `
    <ng-container *ngIf="field && field.value; else noValue">
      <span
        *ngIf="field.renderAsHtml else textValue"
        [innerHTML]="field.value | safeRichContent">
      </span>
    </ng-container>

    <ng-template #textValue>
      <span>
        {{(field.value | translate)}}
      </span>
    </ng-template>

    <ng-template #noValue>
      <span>-</span>
    </ng-template>
  `,
  styleUrls: ['./print-field-value.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrintFieldValueComponent {
  @Input() field: PrintField;
}

import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {PrintDataState, SectionKind} from '../../state/PrintData.state';

@Component({
  selector: 'sqtm-app-print-mode-page',
  templateUrl: './print-mode-page.component.html',
  styleUrls: ['./print-mode-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrintModePageComponent implements OnInit {

  @Input()
  workspace: string;

  @Input()
  printData: PrintDataState;

  SectionKind = SectionKind;

  constructor() { }

  ngOnInit(): void {
  }

}

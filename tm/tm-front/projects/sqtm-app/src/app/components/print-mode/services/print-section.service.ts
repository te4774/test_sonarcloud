import {GridResponse,} from 'sqtm-core';
import {Injectable} from '@angular/core';
import {emptyTableSection, SectionKind, TableRow, TableSection} from '../state/PrintData.state';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class PrintSectionService {

  constructor(public translateService: TranslateService) {
  }

  public buildIssuesSection(issuesGrid: GridResponse, isRequirementVersionPrint: boolean): TableSection {
    if (issuesGrid.dataRows.length === 0) {
      return emptyTableSection('sqtm-core.test-case-workspace.title.issues');
    }

    const header = [
      'sqtm-core.entity.issue.key.label',
      'sqtm-core.entity.issue.project.label',
      'sqtm-core.entity.issue.summary.label',
      'sqtm-core.entity.issue.priority.label',
      'sqtm-core.entity.issue.status.label',
      'sqtm-core.entity.issue.assignee.label',
      'sqtm-core.entity.issue.reported-in.multiple',
    ];

    if (isRequirementVersionPrint) {
      header.push('sqtm-core.entity.issue.attached-to-requirement-versions');
    }

    const rows: TableRow[] = issuesGrid.dataRows.map((dataRow) => {
      const issueRow = dataRow.data;
      const reportSites = issueRow.reportSites.map(reportSite => {
        const executionOrder = reportSite.executionOrder + 1;
        const suiteNames = reportSite.suiteNames;
        const executionName = reportSite.executionName;
        return Boolean(suiteNames?.length > 0) ?
          `${executionName} (${this.translateService.instant('sqtm-core.entity.test-suite.label.singular').toLowerCase()} '${suiteNames}', exe. #${executionOrder})` :
          `${executionName} (exe. #${executionOrder})`;
      });
      const reportedIn = reportSites.join(', ');
      const values = [
        issueRow.remoteKey,
        issueRow.btProject,
        issueRow.summary,
        issueRow.priority,
        issueRow.status,
        issueRow.assignee,
        reportedIn,
      ];
      if (isRequirementVersionPrint) {
        const verifiedRequirementVersions = dataRow.data.verifiedRequirementVersions
          .map(rv => rv.name)
          .join(', ');
        values.push(verifiedRequirementVersions);
      }
      return {values};
    });

    return {
      kind: SectionKind.table,
      title: 'sqtm-core.test-case-workspace.title.issues',
      rows,
      header
    };
  }

}

import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {TableSection} from '../../../state/PrintData.state';

@Component({
  selector: 'sqtm-app-table-section',
  templateUrl: './table-section.component.html',
  styleUrls: ['../sections-common.less', './table-section.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableSectionComponent {

  @Input() section: TableSection;

  get hasData() {
    return this.section
      && this.section.rows.length > 0
      && this.section.header.length > 0;
  }

  constructor() { }

  getGridTemplateColumnsStyle() {
    return 'repeat(' + this.section.header.length + ', auto)';
  }

}

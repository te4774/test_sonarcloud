import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {RequirementStatsSection} from '../../../state/PrintData.state';

@Component({
  selector: 'sqtm-app-requirement-stats-section',
  templateUrl: './requirement-stats-section.component.html',
  styleUrls: ['../sections-common.less', './requirement-stats-section.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementStatsSectionComponent {
  @Input() section: RequirementStatsSection;
}

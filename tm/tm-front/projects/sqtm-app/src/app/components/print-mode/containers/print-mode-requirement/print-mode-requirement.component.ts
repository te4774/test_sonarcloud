import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {
  BindableEntity,
  ConvertFileSizePipe,
  ExecutionStatus,
  GridRequest,
  GridResponse,
  LinkedLowLevelRequirement,
  ProjectData,
  ReferentialDataService,
  ReferentialDataState,
  RequirementCriticality,
  RequirementStatus,
  RequirementVersionModel,
  RequirementVersionStatsBundle,
  RestService,
  TestCaseStatus,
  TestCaseWeight,
  VerifyingTestCase,
} from 'sqtm-core';
import {
  emptyTableSection,
  InformationSection,
  PrintDataState,
  PrintField,
  RequirementStatsSection,
  Section,
  SectionKind,
  TableSection
} from '../../state/PrintData.state';
import {TranslateService} from '@ngx-translate/core';
import {catchError, concatMap, map, take} from 'rxjs/operators';
import {PrintValueService} from '../../services/print-value.service';
import {DatePipe} from '@angular/common';
import {combineLatest, Observable, of} from 'rxjs';
import {PrintSectionService} from '../../services/print-section.service';
import {PrintModeService} from '../../services/print-mode.service';
import {
  verifyingTestCaseEntityAdapter
} from "../../../../pages/requirement-workspace/requirement-version-view/state/verifying-test-case.state";

const indexColumnHeader = '#';

@Component({
  selector: 'sqtm-app-print-mode-test-case',
  templateUrl: './print-mode-requirement.component.html',
  styleUrls: ['./print-mode-requirement.component.less'],
  providers: [ConvertFileSizePipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrintModeRequirementComponent implements OnInit {

  requirementId = 0;
  data: PrintDataState;

  constructor(public readonly activatedRoute: ActivatedRoute,
              public readonly restService: RestService,
              public readonly cdRef: ChangeDetectorRef,
              public readonly translateService: TranslateService,
              public readonly referentialDataService: ReferentialDataService,
              public readonly printValueService: PrintValueService,
              public readonly convertFileSize: ConvertFileSizePipe,
              public readonly datePipe: DatePipe,
              public readonly printSectionService: PrintSectionService) {
  }

  ngOnInit(): void {
    const requirementId = this.activatedRoute.snapshot.paramMap.get('requirementId');
    const requirementVersionId = this.activatedRoute.snapshot.paramMap.get('requirementVersionId');

    this.referentialDataService.refresh()
      .pipe(concatMap(() => combineLatest([
        this.referentialDataService.referentialData$,
        this.fetchPageData(requirementVersionId),
        this.fetchVersionsGrid(requirementId),
      ]))).subscribe(([referentialData, partialPageData, versionsGrid]) => {
      const {requirementVersionModel, statistics, historyGrid, issuesGrid} = partialPageData;
      this.data = this.buildPrintData(requirementVersionModel, referentialData, statistics, historyGrid, issuesGrid, versionsGrid);
      this.cdRef.detectChanges();
      PrintModeService.openPrintDialog();
    });
  }

  private fetchPageData(requirementVersionId: string): Observable<PartialPageData> {
    return this.restService.get<RequirementVersionModel>(['requirement-view', requirementVersionId]).pipe(
      concatMap((requirementVersionModel: RequirementVersionModel) => combineLatest([
        of(requirementVersionModel),
        this.fetchStatistics(requirementVersionModel),
        this.fetchHistory(requirementVersionModel),
        this.fetchIssueGridIfNeeded(requirementVersionModel),
      ])),
      map(([requirementVersionModel, statistics, historyGrid, issuesGrid]) => ({
        requirementVersionModel,
        statistics,
        historyGrid,
        issuesGrid,
      }))
    );
  }

  private fetchStatistics(requirementVersionModel: RequirementVersionModel): Observable<RequirementVersionStatsBundle> {
    const statisticsUrl = ['requirement-view', requirementVersionModel.id.toString(), 'statistics'];
    return this.restService.get<RequirementVersionStatsBundle>(statisticsUrl);
  }

  private fetchHistory(requirementVersionModel: RequirementVersionModel): Observable<GridResponse> {
    const nonPaginatedHistoryRequest: GridRequest = {page: 0, size: null};
    const historyUrl = ['requirement-version', requirementVersionModel.id.toString(), 'history'];
    return this.restService.post<GridResponse>(historyUrl, nonPaginatedHistoryRequest);
  }

  private fetchIssueGridIfNeeded(requirementVersionModel: RequirementVersionModel): Observable<GridResponse> {
    const nonPaginatedIssuesRequest: GridRequest = {page: 0, size: -1};

    return this.referentialDataService.connectToProjectData(requirementVersionModel.projectId).pipe(
      take(1),
      concatMap((projectData: ProjectData) => {
        if (Boolean(projectData.bugTracker)) {
          const urlParts = ['issues', 'requirement-version', requirementVersionModel.id.toString(), 'known-issues', 'all'];
          return this.restService.post<GridResponse>(urlParts, nonPaginatedIssuesRequest).pipe(
            catchError(() => {
              return of({ count: 0, idAttribute: null, dataRows: [] });
            })
          );
        } else {
          return of(null);
        }
      })
    );
  }

  private fetchVersionsGrid(requirementId: string): Observable<GridResponse> {
    const nonPaginatedVersionsRequest: GridRequest = {page: 0, size: null};
    const urlParts = ['requirement-view', 'versions', requirementId];
    return this.restService.post<GridResponse>(urlParts, nonPaginatedVersionsRequest);
  }

  private buildPrintData(model: RequirementVersionModel,
                         referentialData: ReferentialDataState,
                         statistics: RequirementVersionStatsBundle, historyGrid: GridResponse,
                         issuesGrid: GridResponse, versionsGrid: GridResponse): PrintDataState {
    const sections = this.getSections(model, referentialData, statistics, historyGrid, issuesGrid, versionsGrid);
    return {
      name: model.reference ? `${model.reference} - ${model.name}` : model.name,
      entityNameKey: 'sqtm-core.entity.requirement.label.singular',
      sections,
    };
  }

  private getSections(model: RequirementVersionModel, referentialData: ReferentialDataState, statistics: RequirementVersionStatsBundle,
                      historyGrid: GridResponse, issuesGrid: GridResponse, versionsGrid: GridResponse): Section[] {
    const sections: Section[] = [
      this.buildInformationSection(model, referentialData),
      this.buildStatisticsSection(model, statistics)];

    if (model.highLevelRequirement) {
      sections.push(this.buildLowLevelRequirementsSection(model));
    }
    sections.push(this.buildVerifyingTestCasesSection(model),
      this.buildRequirementVersionLinksSection(model),
      this.buildAttachmentSection(model),
      this.buildHistorySection(historyGrid),
      this.buildVersionsSection(versionsGrid, referentialData));

    if (issuesGrid != null) {
      sections.push(this.printSectionService.buildIssuesSection(issuesGrid, true));
    }

    return sections;
  }

  private buildInformationSection(model: RequirementVersionModel,
                                  referentialData: ReferentialDataState): InformationSection {
    const creation = this.printValueService.formatAuditableValue(model.createdOn, model.createdBy);
    const modification = model.lastModifiedOn ?
      this.printValueService.formatAuditableValue(model.lastModifiedOn, model.lastModifiedBy) :
      this.translateService.instant('sqtm-core.generic.label.never');
    const statusKey = RequirementStatus[model.status]?.i18nKey;
    const criticalityKey = RequirementCriticality[model.criticality]?.i18nKey;
    const natureKey = model.highLevelRequirement ? 'sqtm-core.entity.requirement.nature.high-level' : 'sqtm-core.entity.requirement.nature.standard';
    const categoryKey = this.printValueService.extractInfoListItemLabel(model.projectId, referentialData, model.category, 'requirementCategoryId');

    const customFields = this.buildCustomPrintFields(model, referentialData);

    const fields: PrintField[] = [
      {
        name: 'sqtm-core.entity.requirement.version-number.short',
        value: model.versionNumber.toString(),
      },
      {
        name: 'sqtm-core.entity.requirement.id.full',
        value: model.requirementId.toString(),
      },
      {
        name: 'sqtm-core.entity.generic.status.label',
        value: statusKey,
      },
      {
        name: 'sqtm-core.entity.requirement.version.id.full',
        value: model.id.toString(),
      },
      {
        name: 'sqtm-core.entity.requirement.criticality.label',
        value: criticalityKey,
      },
      {
        name: 'sqtm-core.entity.generic.creation.label',
        value: creation,
      },
      {
        name: 'sqtm-core.entity.requirement.category.label',
        value: categoryKey,
      },
      {
        name: 'sqtm-core.entity.generic.modification.label',
        value: modification,
      },
      {
        name: 'sqtm-core.entity.requirement.nature.label',
        value: natureKey,
      },
    ];
    const optionalFields: PrintField[] = [];
    if (!model.highLevelRequirement && Boolean(model.linkedHighLevelRequirement)) {
      const linkedHighLevelRequirementName = model.linkedHighLevelRequirement.reference === '' ?
        model.linkedHighLevelRequirement.name :
        model.linkedHighLevelRequirement.reference + ' - ' + model.linkedHighLevelRequirement.name;

      optionalFields.push({
        name: 'sqtm-core.entity.requirement.high-level.label',
        value: linkedHighLevelRequirementName,
      });
    }
    optionalFields.push({
      name: 'sqtm-core.entity.milestone.label.plural',
      value: model.milestones.map(milestone => milestone.label).join(', '),
    });
    return {
      title: 'sqtm-core.generic.label.information.plural',
      kind: SectionKind.information,
      description: model.description,
      fields: fields,
      optionalFields,
      customFields,
    };
  }

  private buildCustomPrintFields(model: RequirementVersionModel, referentialData: ReferentialDataState): PrintField[] {
    return this.printValueService.printCustomFieldsSorted(model.customFieldValues, referentialData,
      BindableEntity.REQUIREMENT_VERSION, model.projectId)
      .filter(Boolean);
  }

  private buildStatisticsSection(model: RequirementVersionModel, statistics: RequirementVersionStatsBundle): RequirementStatsSection {
    return {
      title: 'sqtm-core.requirement-workspace.title.rate',
      kind: SectionKind.requirementStats,
      haveChildren: statistics.haveChildren,
      total: statistics.total,
      currentVersion: statistics.currentVersion,
      children: statistics.children,
      childrenAmount: statistics.nonObsoleteDescendantsCount,
      coveredChildrenAmount: statistics.coveredDescendantsCount,
      highLevelRequirement: model.highLevelRequirement,
    };
  }

  private buildVerifyingTestCasesSection(model: RequirementVersionModel): TableSection {
    if (model.verifyingTestCases.length <= 0) {
      return emptyTableSection('sqtm-core.requirement-workspace.title.linked-test-case');
    } else {
      const rows = this.generateVerifyingTestCaseRows(model);
      const header = [
        indexColumnHeader,
        'sqtm-core.entity.project.label.singular',
        'sqtm-core.entity.generic.reference.label',
        'sqtm-core.entity.test-case.label.singular',
        'sqtm-core.entity.milestone.label.plural',
        'sqtm-core.search.test-case.grid.header.status.title',
        'sqtm-core.search.test-case.grid.header.weight.title',
        'sqtm-core.entity.test-case.last-execution.label'
      ];
      return {
        title: 'sqtm-core.requirement-workspace.title.linked-test-case',
        kind: SectionKind.table,
        header: header,
        rows: rows,
      };
    }
  }

  private generateVerifyingTestCaseRows(model: RequirementVersionModel) {
    return this.filterDuplicateVerifyingTestCase(model.verifyingTestCases).map((testCase, idx) => {
      return {
        values: [
          (idx + 1).toString(),
          testCase.projectName,
          testCase.reference ? testCase.reference : '-',
          testCase.name,
          testCase.milestoneLabels ? testCase.milestoneLabels : '-',
          TestCaseStatus[testCase.status]?.i18nKey,
          TestCaseWeight[testCase.importance]?.i18nKey,
          ExecutionStatus[testCase.lastExecutionStatus]?.i18nKey
        ],
        displayInItalic: !testCase.directlyLinked,
      };
    });
  }

  private filterDuplicateVerifyingTestCase(verifyingTestCases: VerifyingTestCase[]) {
    const verifyingTestCaseState = verifyingTestCaseEntityAdapter.setAll(
      verifyingTestCases, verifyingTestCaseEntityAdapter.getInitialState()).entities;

    return Object.keys(verifyingTestCaseState).map(index => {
      return verifyingTestCaseState[index]
    });
  }

  private buildRequirementVersionLinksSection(model: RequirementVersionModel): TableSection {
    if (model.requirementVersionLinks.length <= 0) {
      return emptyTableSection('sqtm-core.requirement-workspace.title.requirement-version-link');
    } else {
      const rows = this.generateRequirementVersionLinksRows(model);
      const header = [
        indexColumnHeader,
        'sqtm-core.entity.project.label.singular',
        'sqtm-core.entity.generic.reference.label',
        'sqtm-core.entity.requirement.label.singular',
        'sqtm-core.entity.milestone.label.plural',
        'sqtm-core.entity.requirement.version-number.short',
        'sqtm-core.entity.requirement.requirement-version.link.role.label',
      ];
      return {
        title: 'sqtm-core.requirement-workspace.title.requirement-version-link',
        kind: SectionKind.table,
        header: header,
        rows: rows,
      };
    }
  }

  private generateRequirementVersionLinksRows(model: RequirementVersionModel) {
    return model.requirementVersionLinks.map((requirement, idx) => {
      return {
        values: [
          (idx + 1).toString(),
          requirement.projectName,
          requirement.reference ? requirement.reference : '-',
          requirement.name,
          requirement.milestoneLabels ? requirement.milestoneLabels : '-',
          requirement.versionNumber.toString(),
          this.printValueService.formatReqLinksRole(requirement.role),
        ]
      };
    });
  }

  private buildAttachmentSection(model: RequirementVersionModel): TableSection {
    if (model.attachmentList.attachments.length <= 0) {
      return emptyTableSection('sqtm-core.entity.attachment.label.long.plural');
    } else {
      const rows = this.generateAttachmentsRows(model);
      const header = [
        indexColumnHeader,
        'sqtm-core.entity.generic.name.label',
        'sqtm-core.generic.file.size.label',
        'sqtm-core.entity.attachment.added-on',
      ];
      return {
        title: 'sqtm-core.entity.attachment.label.long.plural',
        kind: SectionKind.table,
        header: header,
        rows: rows
      };
    }
  }

  private generateAttachmentsRows(model: RequirementVersionModel) {
    return model.attachmentList.attachments.map((attachment, idx) => {
      return {
        values: [
          (idx + 1).toString(),
          attachment.name,
          this.convertFileSize.transform(attachment.size),
          this.datePipe.transform(attachment.addedOn),
        ]
      };
    });
  }

  private buildHistorySection(historyGrid: GridResponse): TableSection {
    if (historyGrid.dataRows.length <= 0) {
      return emptyTableSection('sqtm-core.requirement-workspace.title.modification-history');
    } else {
      const rows = historyGrid.dataRows.map(row => {
        const historyRow = row.data;
        return {
          values: [
            this.datePipe.transform(historyRow.date),
            historyRow.user,
            this.printValueService.formatHistoryEvent(row),
            historyRow.event !== 'description' ? this.printValueService.formatHistoryChangedValue(historyRow.oldValue, historyRow.event) : '',
            historyRow.event !== 'description' ? this.printValueService.formatHistoryChangedValue(historyRow.newValue, historyRow.event) : '',
          ]
        };
      });
      const header = [
        'sqtm-core.entity.requirement.requirement-version.modification-history.date',
        'sqtm-core.entity.user.label.singular',
        'sqtm-core.entity.requirement.requirement-version.modification-history.event',
        'sqtm-core.entity.requirement.requirement-version.modification-history.old-value',
        'sqtm-core.entity.requirement.requirement-version.modification-history.new-value',
      ];
      return {
        title: 'sqtm-core.requirement-workspace.title.modification-history',
        kind: SectionKind.table,
        header: header,
        rows: rows
      };
    }
  }

  private buildVersionsSection(versionsGrid: GridResponse, referentialData: ReferentialDataState): TableSection {
    if (versionsGrid.dataRows.length <= 0) {
      return emptyTableSection('sqtm-core.entity.requirement.version.label.plural');
    } else {
      const rows = versionsGrid.dataRows.map(row => {
        const versionRow = row.data;
        return {
          values: [
            versionRow.versionNumber,
            versionRow.reference,
            versionRow.name,
            RequirementStatus[versionRow.requirementStatus]?.i18nKey,
            RequirementCriticality[versionRow.criticality]?.i18nKey,
            this.printValueService.extractInfoListItemLabel(versionRow.projectId, referentialData, versionRow.category, 'requirementCategoryId'),
            versionRow.milestoneLabels,
            versionRow.links.toString(),
            versionRow.coverages.toString(),
          ]
        };
      });
      const header = [
        'sqtm-core.entity.requirement.version-number.short',
        'sqtm-core.entity.generic.reference.label',
        'sqtm-core.entity.generic.name.label',
        'sqtm-core.search.test-case.grid.header.status.title',
        'sqtm-core.entity.requirement.criticality.label',
        'sqtm-core.entity.requirement.category.label',
        'sqtm-core.entity.milestone.label.plural',
        'sqtm-core.requirement-workspace.multi-versions.number-of-links.full',
        'sqtm-core.requirement-workspace.multi-versions.number-of-coverages.full'
      ];
      return {
        title: 'sqtm-core.entity.requirement.version.label.plural',
        kind: SectionKind.table,
        header: header,
        rows: rows
      };
    }
  }

  private buildLowLevelRequirementsSection(model: RequirementVersionModel): TableSection {
    if (model.lowLevelRequirements.length <= 0) {
      return emptyTableSection('sqtm-core.requirement-workspace.title.low-level-requirement-version-link');
    } else {
      const rows = this.generateLowLevelRequirementsRows(model);
      const header = [
        indexColumnHeader,
        'sqtm-core.entity.project.label.singular',
        'sqtm-core.entity.generic.reference.label',
        'sqtm-core.entity.requirement.label.singular',
        'sqtm-core.entity.milestone.label.plural',
        'sqtm-core.entity.generic.criticality.label',
        'sqtm-core.entity.generic.status.label',
      ];
      return {
        title: 'sqtm-core.requirement-workspace.title.low-level-requirement-version-link',
        kind: SectionKind.table,
        header: header,
        rows: rows,
      };
    }
  }

  private generateLowLevelRequirementsRows(model: RequirementVersionModel) {
    return model.lowLevelRequirements.map((requirement: LinkedLowLevelRequirement, idx) => {
      return {
        values: [
          (idx + 1).toString(),
          requirement.projectName,
          requirement.reference ? requirement.reference : '-',
          requirement.name,
          requirement.milestoneLabels ? requirement.milestoneLabels : '-',
          RequirementCriticality[requirement.criticality]?.i18nKey,
          RequirementStatus[requirement.requirementStatus]?.i18nKey,
        ],
        displayInItalic: requirement.childOfRequirement,
      };
    });
  }
}

interface PartialPageData {
  requirementVersionModel: RequirementVersionModel;
  statistics: RequirementVersionStatsBundle;
  historyGrid: GridResponse;
  issuesGrid: GridResponse;
}

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {
  ActionStepModel,
  AutomationRequestStatus,
  AutomationWorkflowTypes,
  BindableEntity,
  buildScmRepositoryUrl,
  CallStepModel,
  ConvertFileSizePipe,
  DataRow,
  DatasetParamValue,
  Execution,
  ExecutionStatus,
  getSupportedBrowserLang,
  GridRequest,
  GridResponse,
  Project,
  ProjectData,
  ReferentialDataService,
  ReferentialDataState,
  RequirementCriticality,
  RequirementStatus,
  RequirementVersionCoverage,
  RestService,
  ScmServer,
  TestAutomationServerKind,
  TestCaseAutomatable,
  TestCaseExecutionMode,
  TestCaseKind,
  TestCaseModel,
  TestCaseStatus,
  TestCaseWeight,
  TestStepModel,
} from 'sqtm-core';
import {
  emptyTableSection,
  FieldsSection,
  InformationSection,
  KeywordTestCaseScenarioSection,
  KeywordTestCaseStep,
  PrintDataState,
  PrintField,
  PrintTestStepKind,
  ScriptedTestCaseScenarioSection,
  Section,
  SectionKind,
  StandardTestCaseScenarioSection,
  StepRow,
  TableRow,
  TableSection
} from '../../state/PrintData.state';
import {DatePipe} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {catchError, concatMap, map, take} from 'rxjs/operators';
import {PrintValueService} from '../../services/print-value.service';
import {KeywordStepState} from '../../../../pages/test-case-workspace/test-case-view/state/test-step.state';
import {combineLatest, Observable, of} from 'rxjs';
import {PrintModeService} from '../../services/print-mode.service';
import {PrintSectionService} from '../../services/print-section.service';

const indexColumnHeader = '#';

const titleKeys = {
  entityName: 'sqtm-core.entity.test-case.label.singular',
  information: 'sqtm-core.generic.label.information.plural',
  prerequisiteAndTestSteps: 'sqtm-core.test-case-workspace.title.prerequisite-and-test-steps',
  script: 'sqtm-core.entity.test-step.script',
  testSteps: 'sqtm-core.entity.test-step.label.plural',
  coverages: 'sqtm-core.test-case-workspace.title.coverages',
  parameters: 'sqtm-core.test-case-workspace.title.parameters',
  attachments: 'sqtm-core.entity.attachment.label.long.plural',
  executions: 'sqtm-core.entity.execution.label.plural',
  issues: 'sqtm-core.test-case-workspace.title.issues',
  calledSteps: 'sqtm-core.test-case-workspace.title.called-steps',
};

@Component({
  selector: 'sqtm-app-print-mode-test-case',
  templateUrl: './print-mode-test-case.component.html',
  styleUrls: ['./print-mode-test-case.component.less'],
  providers: [ConvertFileSizePipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrintModeTestCaseComponent implements OnInit {

  data: PrintDataState;

  constructor(public readonly activatedRoute: ActivatedRoute,
              public readonly restService: RestService,
              public readonly cdRef: ChangeDetectorRef,
              public readonly translateService: TranslateService,
              public readonly referentialDataService: ReferentialDataService,
              private readonly datePipe: DatePipe,
              public readonly printValueService: PrintValueService,
              public readonly fileSizePipe: ConvertFileSizePipe,
              public readonly printSectionService: PrintSectionService) {
  }

  ngOnInit(): void {
    const testCaseId = this.activatedRoute.snapshot.paramMap.get('testCaseId');

    this.referentialDataService.refresh()
      .pipe(concatMap(() => combineLatest([
        this.referentialDataService.referentialData$,
        this.referentialDataService.projectDatas$,
        this.fetchExecutions(testCaseId),
        this.restService.get<TestCaseModel>(['test-case-view', testCaseId])
          .pipe(concatMap((testCaseModel: TestCaseModel) => this.fetchOptionalIssueGrid(testCaseModel))),
      ])))
      .subscribe(([referentialData, projectDatas, executionGrid, {testCaseModel, issueGrid}]) => {
        this.data = this.buildPrintData(testCaseModel, referentialData, projectDatas[testCaseModel.projectId], executionGrid, issueGrid);
        this.cdRef.detectChanges();
        PrintModeService.openPrintDialog();
      });
  }

  private fetchOptionalIssueGrid(testCaseModel: TestCaseModel): Observable<{ testCaseModel: TestCaseModel, issueGrid: GridResponse }> {
    const nonPaginatedIssuesRequest: GridRequest = {page: 0, size: -1}; // negative size to ignore issue pagination
    return this.referentialDataService.connectToProjectData(testCaseModel.projectId).pipe(
      take(1),
      concatMap((projectData: ProjectData) => {
        if (Boolean((projectData.bugTracker))) {
          const urlParts = ['issues', 'test-case', testCaseModel.id.toString(), 'known-issues'];
          return this.restService.post<GridResponse>(urlParts, nonPaginatedIssuesRequest)
            .pipe(
              map(issueGrid => ({testCaseModel, issueGrid})),
              catchError(() => {
                return of({testCaseModel, issueGrid: { count: 0, idAttribute: null, dataRows: [] }});
              }));
        } else {
          return of({testCaseModel, issueGrid: null});
        }
      }),
    );
  }

  private fetchExecutions(testCaseId: string) {
    const nonPaginatedExecRequest: GridRequest = {page: 0, size: null}; // null size to ignore exec pagination
    return this.restService.post<GridResponse>(['test-case', testCaseId, 'executions'], nonPaginatedExecRequest);
  }

  private buildPrintData(model: TestCaseModel,
                         referentialData: ReferentialDataState,
                         projectData: ProjectData,
                         executionGrid: GridResponse,
                         issueGrid: GridResponse): PrintDataState {
    const sections = this.getSectionsBasedOnTestCaseKind(model, referentialData, projectData, executionGrid, issueGrid);

    return {
      name: model.reference ? `${model.reference} - ${model.name}` : model.name,
      entityNameKey: titleKeys.entityName,
      sections,
    };
  }

  private buildInformationSection(model: TestCaseModel,
                                  referentialData: ReferentialDataState): InformationSection {
    const projectId = model.projectId;
    const creation = this.printValueService.formatAuditableValue(model.createdOn, model.createdBy);
    const modification = model.lastModifiedOn ?
      this.printValueService.formatAuditableValue(model.lastModifiedOn, model.lastModifiedBy) :
      this.translateService.instant('sqtm-core.generic.label.never');
    const statusKey = this.printValueService.extractEnumKey(model, 'status', TestCaseStatus);
    const importanceKey = this.printValueService.extractEnumKey(model, 'importance', TestCaseWeight);
    const typeKey = this.printValueService.extractInfoListItemLabel(projectId, referentialData, model.type, 'testCaseTypeId');
    const natureKey = this.printValueService.extractInfoListItemLabel(projectId, referentialData, model.nature, 'testCaseNatureId');
    const kindKey = TestCaseKind[model.kind]?.i18nKey;

    const customFields = this.buildCustomPrintFields(model, referentialData);

    return {
      title: titleKeys.information,
      kind: SectionKind.information,
      description: model.description,
      fields: [
        {
          name: 'sqtm-core.entity.generic.status.label',
          value: statusKey,
        },
        {
          name: 'sqtm-core.entity.generic.id.label',
          value: model.id.toString(),
        },
        {
          name: 'sqtm-core.entity.test-case.importance.label',
          value: importanceKey,
        },
        {
          name: 'sqtm-core.entity.test-case.kind.label',
          value: kindKey,
        },
        {
          name: 'sqtm-core.entity.test-case.nature.label',
          value: natureKey,
        },
        {
          name: 'sqtm-core.entity.generic.creation.label',
          value: creation,
        },
        {
          name: 'sqtm-core.entity.test-case.type.label',
          value: typeKey,
        },
        {
          name: 'sqtm-core.entity.generic.modification.label',
          value: modification,
        },
        {
          name: 'sqtm-core.entity.milestone.label.plural',
          value: model.milestones.map(milestone => milestone.label).join(', '),
        },
      ],
      customFields,
    };
  }

  private buildAutomationSection(model: TestCaseModel,
                                 referentialData: ReferentialDataState,
                                 projectData: ProjectData): FieldsSection {
    const title = 'sqtm-core.test-case-workspace.title.automation';
    const project = referentialData.projectState.entities[model.projectId];
    const automatableKey = this.printValueService.extractEnumKey(model, 'automatable', TestCaseAutomatable);
    const requestStatus = this.getAutomationStatus(projectData, model);
    const showAllFields = model.automatable === TestCaseAutomatable.Y.id;

    if (project.allowAutomationWorkflow) {
      const automatableField = {
        name: 'sqtm-core.generic.label.automation.indicator',
        value: automatableKey,
      };

      const fields: PrintField[] = showAllFields ? [
        automatableField,
        {
          name: 'sqtm-core.generic.label.priority.numeric',
          value: model.automationRequest?.priority?.toString(),
        },
        {
          name: 'sqtm-core.generic.label.automation.status',
          value: requestStatus,
        },
        {
          name: 'sqtm-core.entity.test-case.uuid.label',
          value: model.uuid,
        },
      ] : [automatableField];

      if (project.automationWorkflowType !== AutomationWorkflowTypes.NATIVE) {
        const transmittedOn = model.automationRequest?.transmittedOn == null ?
          'sqtm-core.generic.label.never' :
          this.datePipe.transform(model.automationRequest?.transmittedOn, 'short', getSupportedBrowserLang(this.translateService));

        fields.push(
          ...getCommonAutomationFields(model, referentialData),
          {
            name: 'sqtm-core.entity.automation-request.transmitted-on',
            value: transmittedOn,
          }
        );
      }

      const showSecondColumn = model.automatable === TestCaseAutomatable.Y.id
        && project.automationWorkflowType === 'REMOTE_WORKFLOW';

      if (showSecondColumn) {
        const hasExtender = model.automationRequest?.extender != null;
        const hasFinalStatus = model.configuredRemoteFinalStatus === model.automationRequest?.extender?.remoteStatus;
        const isAutomated = hasExtender && hasFinalStatus;
        const isAutomatedKey = hasExtender ?
          (isAutomated ? 'sqtm-core.generic.label.yes' : 'sqtm-core.generic.label.no')
          : null;

        fields.push(
          {
            name: 'sqtm-core.entity.test-case.automated',
            value: isAutomatedKey,
          },
          {
            name: 'sqtm-core.entity.generic.url.label',
            value: model.automationRequest?.extender?.remoteRequestUrl,
          },
          {
            name: 'sqtm-core.entity.automation-request.assigned-to.label',
            value: model.automationRequest.extender?.remoteAssignedTo,
          }
        );
      }

      return {
        kind: SectionKind.fields,
        title,
        fields,
      };
    } else {
      return {
        kind: SectionKind.fields,
        title,
        fields: getCommonAutomationFields(model, referentialData),
      };
    }
  }

  private getAutomationStatus(projectData: ProjectData, model: TestCaseModel) {
    // Used same logic as in test-case-view-automation-component.html
    let requestStatus = '';
    if (projectData.automationWorkflowType === 'NATIVE') {
      requestStatus = AutomationRequestStatus[model.automationRequest?.requestStatus]?.i18nKey;
    } else if (projectData.automationWorkflowType === 'REMOTE_WORKFLOW' && model.automatable === 'Y' && model.automationRequest.extender) {
      requestStatus = model.automationRequest.extender.remoteStatus;
    }
    return requestStatus;
  }

  private buildCustomPrintFields(model: TestCaseModel, referentialData: ReferentialDataState): PrintField[] {
    // @ts-ignore
    return this.printValueService.printCustomFieldsSorted(model.customFieldValues, referentialData,
      BindableEntity.TEST_CASE, model.projectId)
      .filter(Boolean);
  }

  private getSectionsBasedOnTestCaseKind(model: TestCaseModel,
                                         referentialData: ReferentialDataState,
                                         projectData: ProjectData,
                                         executionGrid: GridResponse,
                                         issueGrid: GridResponse): Section[] {
    const sections: Section[] = [
      this.buildInformationSection(model, referentialData),
      this.buildAutomationSection(model, referentialData, projectData),
      this.buildCoveragesSection(model),
    ];

    if (model.kind === 'STANDARD') {
      sections.push(
        this.buildDatasetSection(model),
        this.buildStandardScenarioSection(model, referentialData),
        this.buildCalledByTestCasesSection(model),
      );
    } else if (model.kind === 'GHERKIN') {
      sections.push(
        this.buildScriptedScenarioSection(model),
      );
    } else if (model.kind === 'KEYWORD') {
      sections.push(
        this.buildDatasetSection(model),
        this.buildKeywordScenarioSection(model, referentialData),
      );
    }

    sections.push(
      this.buildAttachmentsSection(model),
      this.buildExecutionsSection(executionGrid),
    );

    const showIssuesSection = issueGrid != null;

    if (showIssuesSection) {
      sections.push(this.printSectionService.buildIssuesSection(issueGrid, false));
    }

    return sections;
  }

  private buildStandardScenarioSection(model: TestCaseModel,
                                       referentialData: ReferentialDataState): StandardTestCaseScenarioSection {
    const steps: StepRow[] = model.testSteps.map(step => this.asStandardPrintStepRow(model, step, referentialData, model.projectId));

    return {
      title: titleKeys.prerequisiteAndTestSteps,
      kind: SectionKind.testCaseScenario,
      testCaseKind: 'STANDARD',
      prerequisite: model.prerequisite,
      steps,
    };
  }

  private buildScriptedScenarioSection(model: TestCaseModel): ScriptedTestCaseScenarioSection {
    return {
      title: titleKeys.script,
      kind: SectionKind.testCaseScenario,
      testCaseKind: 'GHERKIN',
      script: model.script,
    };
  }

  private buildKeywordScenarioSection(model: TestCaseModel,
                                      referentialData: ReferentialDataState): KeywordTestCaseScenarioSection {
    const project = referentialData.projectState.entities[model.projectId] as Project;
    const getKeywordLabel = (value: string) => project.keywords.find(kw => kw.value === value)?.label;

    const steps: KeywordTestCaseStep[] = model.testSteps.map((step) => {
      const keywordStep = step as KeywordStepState;

      const testStep: KeywordTestCaseStep = {
        testCaseKind: 'KEYWORD',
        keyword: getKeywordLabel(keywordStep.keyword),
        action: keywordStep.styledAction
      };

      if (keywordStep.datatable) {
        testStep.datatable = keywordStep.datatable;
      }

      if (keywordStep.docstring) {
        testStep.docstring = keywordStep.docstring;
      }

      if (keywordStep.comment) {
        testStep.comment = keywordStep.comment;
      }

      return testStep;
    });

    return {
      title: titleKeys.testSteps,
      kind: SectionKind.testCaseScenario,
      testCaseKind: 'KEYWORD',
      steps,
    };
  }

  private asStandardPrintStepRow(testCase: TestCaseModel,
                                 step: TestStepModel,
                                 referentialData: ReferentialDataState, projectId: number): StepRow {
    switch (step.kind) {
      case 'action-step':
        const actionStep = step as ActionStepModel;
        const coverages = testCase.coverages
          .filter(cov => cov.coverageStepInfos.some(info => info.id === step.id));

        return {
          kind: PrintTestStepKind.action,
          action: actionStep.action,
          expectedResult: actionStep.expectedResult,
          attachmentCount: actionStep.attachmentList.attachments.length,
          linkedRequirementCount: coverages.length,
          // tslint:disable-next-line:max-line-length
          customFieldValues: actionStep.customFieldValues && this.printValueService.printCustomFieldsSorted(actionStep.customFieldValues, referentialData, BindableEntity.TEST_STEP, projectId),
        };
      case 'call-step':
        const calledStep = step as CallStepModel;
        return {
          kind: PrintTestStepKind.call,
          calledTestCaseName: calledStep.calledTcName,
          datasetName: this.getDatasetLabel(calledStep),
          steps: calledStep.calledTestCaseSteps?.map(s => this.asStandardPrintStepRow(testCase, s, referentialData, projectId)),
        };
    }
  }

  private getDatasetLabel(calledStep: CallStepModel): string {
    if (calledStep.delegateParam) {
      return this.translateService.instant('sqtm-core.entity.call-step.dataset.delegate');
    } else {
      return this.getCalledDataSetLabel(calledStep);
    }
  }

  private getCalledDataSetLabel(calledStep: CallStepModel) {
    if (calledStep.calledDatasetId != null) {
      return calledStep.calledDatasetName;
    } else {
      return this.translateService.instant('sqtm-core.entity.call-step.dataset.no-dataset');
    }
  }

  private buildCoveragesSection(model: TestCaseModel): TableSection {
    const showMilestones = model.coverages.some(coverage => Boolean(coverage.milestoneLabels));
    const project = 'sqtm-core.entity.project.label.singular';
    const reference = 'sqtm-core.entity.generic.reference.label';
    const requirement = 'sqtm-core.entity.requirement.label.singular';
    const milestones = 'sqtm-core.entity.milestone.label.plural';
    const criticality = 'sqtm-core.entity.requirement.criticality.label';
    const status = 'sqtm-core.entity.generic.status.label';
    const verifiedByHeader = 'sqtm-core.entity.generic.verified-by.label';

    const header = showMilestones ? [
      indexColumnHeader, project, reference, requirement, milestones, criticality, status, verifiedByHeader,
    ] : [
      indexColumnHeader, project, reference, requirement, criticality, status, verifiedByHeader,
    ];

    const rows: TableRow[] = model.coverages.map((cov, index) => {
      const cells = [
        (index + 1).toString(),
        cov.projectName,
        cov.reference,
        cov.name
      ];

      if (showMilestones) {
        if (cov.milestoneMinDate != null) {
          const startDate = this.printValueService.formatDate(cov.milestoneMinDate);
          const endDate = this.printValueService.formatDate(cov.milestoneMaxDate);
          let milestoneDates = '-';

          if (cov.milestoneLabels) {
            if (startDate === endDate) {
              milestoneDates = Boolean(startDate) ? startDate : '-';
            } else {
              milestoneDates = `${startDate} - ${endDate}`;
            }
          }

          cells.push(milestoneDates);
        } else {
          cells.push(null);
        }
      }

      const criticalityKey = this.printValueService.extractEnumKey(cov, 'criticality', RequirementCriticality);
      const statusKey = this.printValueService.extractEnumKey(cov, 'status', RequirementStatus);
      const verifiedBy = getVerifiedByCellContent(cov);

      cells.push(criticalityKey, statusKey, verifiedBy);

      return {
        values: cells,
      };
    });

    return {
      kind: SectionKind.table,
      title: titleKeys.coverages,
      header,
      rows,
    };
  }

  private buildDatasetSection(model: TestCaseModel): TableSection {
    if (model.datasetParamValues.length === 0) {
      return emptyTableSection(titleKeys.parameters);
    }

    const valuesByDataset: ValuesByDataSet = model.datasetParamValues.reduce((acc, dpv) => {
      acc[dpv.datasetId] = [...(acc[dpv.datasetId] ?? []), dpv];
      return acc;
    }, {} as ValuesByDataSet);

    const sortedParams = valuesByDataset[model.datasets[0]?.id];
    sortedParams.sort((first, second) => first.id - second.id);

    const names = sortedParams
      .map(paramValue => paramValue.parameterId)
      .map(paramId => model.parameters.find(p => p.id === paramId))
      .map(parameter => parameter.name);

    const header = [
      indexColumnHeader,
      'sqtm-core.entity.generic.name.label',
      ...names
    ] as string[];

    const rows: TableRow[] = model.datasets.map((dataset, index) => {
      const paramValues = [...valuesByDataset[dataset.id]];

      paramValues.sort((first, second) => first.id - second.id);

      const values = [
        (index + 1).toString(),
        dataset.name,
        ...paramValues.map(pv => pv.value),
      ];

      return {
        values,
      };
    });

    return {
      kind: SectionKind.table,
      title: titleKeys.parameters,
      header,
      rows,
    };
  }

  private buildAttachmentsSection(model: TestCaseModel): TableSection {
    if (model.attachmentList.attachments.length === 0) {
      return emptyTableSection(titleKeys.attachments);
    }

    const rows: TableRow[] = model.attachmentList.attachments.map((attachment, idx) => {
      return {
        values: [
          (idx + 1).toString(),
          attachment.name,
          this.fileSizePipe.transform(attachment.size),
          this.printValueService.formatDate(attachment.addedOn),
        ],
      };
    });

    const header = [
      indexColumnHeader,
      'sqtm-core.entity.generic.name.label',
      'sqtm-core.generic.file.size.label',
      'sqtm-core.entity.attachment.added-on',
    ];

    return {
      kind: SectionKind.table,
      rows,
      header,
      title: titleKeys.attachments,
    };
  }

  private buildExecutionsSection(executionGrid: GridResponse): TableSection {
    if (executionGrid.dataRows.length === 0) {
      return emptyTableSection(titleKeys.executions);
    }

    const showIssuesColumn = executionGrid.dataRows.some((dataRow: DataRow) => {
      const executionRow = dataRow.data as Execution;
      return executionRow.nbIssues > 0;
    });

    const header = [
      'sqtm-core.entity.project.label.singular',
      'sqtm-core.entity.campaign.label.singular',
      'sqtm-core.entity.iteration.label.singular',
      'sqtm-core.entity.test-suite.label.singular',
      'sqtm-core.entity.execution.label.singular',
      'sqtm-core.entity.execution.mode.label',
      'sqtm-core.entity.dataset.label.singular',
      'sqtm-core.entity.execution.status.label',
      'sqtm-core.entity.execution.last-executed-by.label',
      'sqtm-core.entity.execution.last-executed-on.label',
    ];

    if (showIssuesColumn) {
      header.push('sqtm-core.entity.issue.label.count');
    }

    const rows: TableRow[] = executionGrid.dataRows.map((dataRow: DataRow) => {
      const executionRow = dataRow.data;
      const executionModeKey = this.printValueService.extractEnumKey(executionRow, 'executionMode', TestCaseExecutionMode);
      const executionStatusKey = this.printValueService.extractEnumKey(executionRow, 'executionStatus', ExecutionStatus);
      const values = [
        executionRow.projectName,
        executionRow.campaignName,
        executionRow.iterationName,
        executionRow.testSuites,
        '#' + (executionRow.executionOrder + 1),
        executionModeKey,
        executionRow.datasetLabel,
        executionStatusKey,
        executionRow.lastExecutedBy,
        this.printValueService.formatDate(executionRow.lastExecutedOn),
      ];

      if (showIssuesColumn) {
        values.push(executionRow.nbIssues.toString());
      }

      return {values};
    });

    return {
      title: titleKeys.executions,
      kind: SectionKind.table,
      header,
      rows,
    };
  }

  private buildCalledByTestCasesSection(model: TestCaseModel): TableSection {
    if (model.calledTestCases.length === 0) {
      return emptyTableSection(titleKeys.calledSteps);
    }

    const header = [
      indexColumnHeader,
      'sqtm-core.entity.project.label.singular',
      'sqtm-core.entity.generic.reference.label',
      'sqtm-core.entity.test-case.label.singular',
      'sqtm-core.entity.dataset.label.singular',
      'sqtm-core.entity.test-case.test-step.order.label',
    ];

    const rows: TableRow[] = model.calledTestCases.map((ctc, index) => {
      const values = [
        (index + 1).toString(),
        ctc.projectName,
        ctc.reference,
        ctc.name,
        ctc.datasetName,
        (ctc.stepOrder + 1).toString(),
      ];

      return {values};
    });

    return {
      kind: SectionKind.table,
      title: titleKeys.calledSteps,
      rows,
      header
    };
  }
}

function getCommonAutomationFields(model: TestCaseModel,
                                   referentialData: ReferentialDataState): PrintField[] {
  const repoUrl = findRepositoryUrl(model, referentialData);
  const automatedTestTechnology = referentialData.automatedTestTechnologiesState.entities[model.automatedTestTechnology]?.name;

  let fields = [
    {
      name: 'sqtm-core.test-case-workspace.automation.automated-test-technology.label',
      value: automatedTestTechnology,
    },
    {
      name: 'sqtm-core.test-case-workspace.automation.source-code-repository-url.label',
      value: repoUrl,
    },
    {
      name: 'sqtm-core.test-case-workspace.automation.automated-test-reference.label',
      value: model.automatedTestReference,
    },
  ];

  const project = referentialData.projectState.entities[model.projectId];
  const isTaServerJenkinsKind = referentialData.taServerState.entities[project.taServerId]?.kind === TestAutomationServerKind.jenkins;

  if (isTaServerJenkinsKind) {
    const scriptField: PrintField = {
      name: 'sqtm-core.entity.test-case.script-auto',
      value: model.automatedTest?.fullLabel,
    };
    fields = [scriptField, ...fields];
  }

  return fields;
}

function findRepositoryUrl(model: TestCaseModel, referentialData: ReferentialDataState): string {
  for (const scmServer of Object.values(referentialData.scmServerState.entities)) {
    for (const repository of (scmServer as ScmServer).repositories) {
      if (repository.scmRepositoryId === model.scmRepositoryId) {
        return buildScmRepositoryUrl(scmServer, repository);
      }
    }
  }

  return null;
}


interface ValuesByDataSet {
  [datasetId: number]: DatasetParamValue[];
}

function getVerifiedByCellContent(cov: RequirementVersionCoverage): string {
  const isMultiCovered = cov.coverageStepInfos.length > 1
    || cov.verifyingCalledTestCaseIds.length > 1
    || (cov.directlyVerified && cov.unDirectlyVerified);

  if (isMultiCovered) {
    return 'sqtm-core.test-case-workspace.coverages.multiple-sources';
  }

  if (cov.directlyVerified) {
    if (cov.coverageStepInfos.length === 0) {
      return 'sqtm-core.entity.requirement-version-coverage.verified-by.test-case';
    } else {
      return 'sqtm-core.entity.requirement-version-coverage.verified-by.test-step';
    }
  }

  if (cov.unDirectlyVerified) {
    return 'sqtm-core.entity.requirement-version-coverage.verified-by.call-step';
  }

  return null;
}

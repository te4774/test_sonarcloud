import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PrintModeTestCaseComponent} from './containers/print-mode-test-case/print-mode-test-case.component';
import {RouterModule, Routes} from '@angular/router';
import {UiManagerModule, WorkspaceCommonModule} from 'sqtm-core';
import {PrintModePageComponent} from './containers/print-mode-page/print-mode-page.component';
import {InformationSectionComponent} from './components/sections/information-section/information-section.component';
import {TranslateModule} from '@ngx-translate/core';
import {PrintModeRequirementComponent} from './containers/print-mode-requirement/print-mode-requirement.component';
import {FieldsSectionComponent} from './components/sections/fields-section/fields-section.component';
import {PrintFieldValueComponent} from './components/print-field-value/print-field-value.component';
import {TableSectionComponent} from './components/sections/table-section/table-section.component';
import {
  TestScenarioSectionComponent
} from './components/sections/test-scenario-section/test-scenario-section.component';
import {ConvertFileSizePipe} from '../../../../../sqtm-core/src/lib/ui/workspace-common/pipes/convert-file-size.pipe';
import {
  RequirementStatsSectionComponent
} from './components/sections/requirement-stats-section/requirement-stats-section.component';


const routes: Routes = [
  {
    path: 'test-case/:testCaseId',
    component: PrintModeTestCaseComponent,
  },
  {
    path: 'requirement/:requirementId/version/:requirementVersionId',
    component: PrintModeRequirementComponent,
  },
];

@NgModule({
  declarations: [
    PrintModeTestCaseComponent,
    PrintModePageComponent,
    PrintModeRequirementComponent,
    InformationSectionComponent,
    FieldsSectionComponent,
    PrintFieldValueComponent,
    TableSectionComponent,
    TestScenarioSectionComponent,
    RequirementStatsSectionComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    UiManagerModule,
    WorkspaceCommonModule,
    TranslateModule,
  ],
  providers: [
    ConvertFileSizePipe,
  ]
})
export class PrintModeModule {
}

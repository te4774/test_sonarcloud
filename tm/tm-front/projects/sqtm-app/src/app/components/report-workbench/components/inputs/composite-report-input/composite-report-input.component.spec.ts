import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CompositeReportInputComponent} from './composite-report-input.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AppTestingUtilsModule} from '../../../../../utils/testing-utils/app-testing-utils.module';
import {TranslateModule} from '@ngx-translate/core';
import {
  BindableEntity,
  CustomField,
  InputType,
  NodeType,
  ReferentialDataService,
  ReportInputType,
  ReportOption
} from 'sqtm-core';
import {mockReferentialDataService} from '../../../../../utils/testing-utils/mocks.service';
import {mockProjectData} from '../../../../../utils/testing-utils/mocks.data';
import {of} from 'rxjs';
import SpyObj = jasmine.SpyObj;


const reportOption: ReportOption = {
  name: 'name',
  contentType: ReportInputType.TAG_PICKER,
  label: 'label',
  bindableEntity: BindableEntity.TEST_CASE,
  composite: true,
  defaultSelected: true,
  givesAccessTo: null,
  value: 'value',
  nodeType: NodeType.TEST_CASE,
  disabledBy: ''
};


describe('CompositeReportInputComponent', () => {
  let component: CompositeReportInputComponent;
  let fixture: ComponentFixture<CompositeReportInputComponent>;
  const referentialDataService: SpyObj<ReferentialDataService> = mockReferentialDataService();


  referentialDataService.filteredProjects$ = of([mockProjectData()]);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppTestingUtilsModule, TranslateModule.forRoot()],
      providers: [{provide: ReferentialDataService, useValue: referentialDataService}],
      declarations: [CompositeReportInputComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompositeReportInputComponent);
    component = fixture.componentInstance;
    component.reportInput = {
      name: 'report',
      options: [],
      type: ReportInputType.RADIO_BUTTONS_GROUP,
      required: false,
      label: 'report'
    };
    component.errors = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show tag picker', () => {
    referentialDataService.findCustomFieldByProjectIdAndDomain.and.returnValue(of([{
      inputType: InputType.TAG,
    } as CustomField]));
    component.isTagPickerVisible(reportOption).subscribe((isVisible) => {
        expect(isVisible).toBeTruthy();
      }
    );
  });

  it('should hide tag picker', () => {
    referentialDataService.findCustomFieldByProjectIdAndDomain.and.returnValue(of([{
      inputType: InputType.CHECKBOX,
    } as CustomField]));
    component.isTagPickerVisible(reportOption).subscribe((isVisible) => {
        expect(isVisible).toBeFalsy();
      }
    );
  });
});

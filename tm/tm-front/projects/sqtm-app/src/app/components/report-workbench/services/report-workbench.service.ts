import {Injectable} from '@angular/core';
import {createStore, Report} from 'sqtm-core';
import {initialReportWorkbenchState, reportEntityAdapter, ReportWorkbenchState} from '../state/report-workbench.state';
import {map, take} from 'rxjs/operators';

@Injectable()
export class ReportWorkbenchService {

  private store = createStore<ReportWorkbenchState>(initialReportWorkbenchState(), {id: 'report-workbench-store', logDiff: 'detailed'});

  public state$ = this.store.state$;

  constructor() {
  }

  initializeInitialData(reports: Report[]) {
    this.store.state$.pipe(
      take(1),
      map(state => {
        const availableReports = reportEntityAdapter.setAll(reports, state.availableReports);
        return {...state, availableReports};
      })
    ).subscribe(state => this.store.commit(state));
  }

  changeSelectedReport(selectedReport: string) {
    this.store.state$.pipe(
      take(1),
      map(state => ({...state, selectedReport}))
    ).subscribe(state => this.store.commit(state));
  }

  toggleSelectReportPanel() {
    this.store.state$.pipe(
      take(1),
      map(state => ({...state, showSelectReportPanel: !state.showSelectReportPanel}))
    ).subscribe(state => this.store.commit(state));
  }
}

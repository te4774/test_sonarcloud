import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReportWorkbenchComponent} from './containers/report-workbench/report-workbench.component';
import {TranslateModule} from '@ngx-translate/core';
import {DialogModule, GridModule, UiManagerModule, WorkspaceCommonModule} from 'sqtm-core';
import {ReportListComponent} from './components/report-list/report-list.component';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzCollapseModule} from 'ng-zorro-antd/collapse';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  CompositeReportInputComponent
} from './components/inputs/composite-report-input/composite-report-input.component';
import {NzRadioModule} from 'ng-zorro-antd/radio';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {FormProjectPickerComponent} from './components/inputs/form-project-picker/form-project-picker.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {SimpleReportInputComponent} from './components/inputs/simple-report-input/simple-report-input.component';
import {NzCheckboxModule} from 'ng-zorro-antd/checkbox';
import {FormMilestonePickerComponent} from './components/inputs/form-milestone-picker/form-milestone-picker.component';
import {
  FormRequirementTreePickerComponent
} from './components/inputs/form-requirement-tree-picker/form-requirement-tree-picker.component';
import {FormTagPickerComponent} from './components/inputs/form-tag-picker/form-tag-picker.component';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {JasperReportDialogComponent} from './containers/jasper-report-dialog/jasper-report-dialog.component';
import {NzTabsModule} from 'ng-zorro-antd/tabs';
import {JasperReportViewComponent} from './components/jasper-report-view/jasper-report-view.component';
import {
  FormTestCaseTreePickerComponent
} from './components/inputs/form-test-case-tree-picker/form-test-case-tree-picker.component';
import {NzGridModule} from 'ng-zorro-antd/grid';
import {InputsGroupReportComponent} from './components/inputs/inputs-group-report/inputs-group-report.component';
import {NzDatePickerModule} from 'ng-zorro-antd/date-picker';
import {
  FormCampaignTreePickerComponent
} from './components/inputs/form-campaign-tree-picker/form-campaign-tree-picker.component';
import {UninstalledReportListComponent} from './components/uninstalled-report-list/uninstalled-report-list.component';
import {CustomReportReportComponent} from './containers/custom-report-report/custom-report-report.component';


@NgModule({
  declarations: [
    ReportWorkbenchComponent,
    ReportListComponent,
    CompositeReportInputComponent,
    FormProjectPickerComponent,
    SimpleReportInputComponent,
    FormMilestonePickerComponent,
    FormRequirementTreePickerComponent,
    FormTagPickerComponent,
    JasperReportDialogComponent,
    JasperReportViewComponent,
    FormTestCaseTreePickerComponent,
    InputsGroupReportComponent,
    FormCampaignTreePickerComponent,
    UninstalledReportListComponent,
    CustomReportReportComponent
  ],
  exports: [
    ReportWorkbenchComponent,
    CustomReportReportComponent
  ],
    imports: [
        CommonModule,
        TranslateModule,
        OverlayModule,
        WorkspaceCommonModule,
        GridModule,
        NzIconModule,
        NzCollapseModule,
        ReactiveFormsModule,
        NzRadioModule,
        NzButtonModule,
        NzToolTipModule,
        NzCheckboxModule,
        NzSelectModule,
        DialogModule,
        NzTabsModule,
        FormsModule,
        UiManagerModule,
        NzGridModule,
        NzDatePickerModule
    ]
})
export class ReportWorkbenchModule {
}

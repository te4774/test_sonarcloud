import {TestBed} from '@angular/core/testing';

import {DirectDownloadableReportService} from './direct-downloadable-report.service';
import {AppTestingUtilsModule} from '../../../utils/testing-utils/app-testing-utils.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('DirectDownloadableReportService', () => {
  let service: DirectDownloadableReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppTestingUtilsModule,
        HttpClientTestingModule
      ],
        providers: [
          DirectDownloadableReportService
      ]
    });
    service = TestBed.inject(DirectDownloadableReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import {ChangeDetectionStrategy, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ReportView} from 'sqtm-core';
import {BehaviorSubject} from 'rxjs';
import {JasperReportService} from '../../services/jasper-report.service';

@Component({
  selector: 'sqtm-app-jasper-report-view',
  templateUrl: './jasper-report-view.component.html',
  styleUrls: ['./jasper-report-view.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JasperReportViewComponent implements OnInit, OnDestroy {

  @Input()
  view: ReportView;

  @Input()
  viewIndex: number;

  @Input()
  reportId: string;

  @Input()
  params: any;

  @ViewChild('viewContainer', {read: ElementRef})
  viewContainer: ElementRef;

  showSpinner$ = new BehaviorSubject<boolean>(true);

  constructor(private jasperReportService: JasperReportService) {
  }

  ngOnInit(): void {
    this.jasperReportService.getHtmlPreview(this.reportId, this.viewIndex, this.params).subscribe(x => {
      this.showSpinner$.next(false);
      this.viewContainer.nativeElement.innerHTML = x;
    });
  }

  ngOnDestroy(): void {
    this.showSpinner$.complete();
  }

}

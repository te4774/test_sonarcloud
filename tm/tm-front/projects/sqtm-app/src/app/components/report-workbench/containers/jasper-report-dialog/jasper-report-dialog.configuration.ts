import {Report} from 'sqtm-core';

export class JasperReportDialogConfiguration {
  report: Report;
  params: any;
}

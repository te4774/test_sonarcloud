import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {
  DialogService,
  doesHttpErrorContainsSquashFieldError,
  extractSquashFirstFieldError,
  isReportOptionGroup,
  MilestoneModeData,
  ProjectDataMap,
  ReferentialDataService,
  Report,
  ReportCheckboxInput,
  ReportDefinitionModel,
  ReportDefinitionService,
  ReportInput,
  ReportInputsGroup,
  ReportInputType,
  ReportOption,
  ReportOptionGroup,
  RestService,
  TextFieldComponent,
  UnInstallReport
} from 'sqtm-core';
import {ReportWorkbenchService} from '../../services/report-workbench.service';
import {select} from '@ngrx/store';
import {
  availableExecutionPhaseReportsSelector,
  availablePreparationPhaseReportsSelector,
  availableVariousReportsSelector,
  selectedReportSelector,
  showSelectReportPanelSelector
} from '../../state/report-workbench.state';
import {BehaviorSubject, combineLatest, Observable, Subject, throwError} from 'rxjs';
import {catchError, filter, map, shareReplay, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {reportWorkbenchLogger} from '../../report-workbench.logger';
import {Router} from '@angular/router';
import {DocxReportService} from '../../services/docx-report.service';
import {JasperReportDialogComponent} from '../jasper-report-dialog/jasper-report-dialog.component';
import {JasperReportDialogConfiguration} from '../jasper-report-dialog/jasper-report-dialog.configuration';
import {TranslateService} from '@ngx-translate/core';
import {DirectDownloadableReportService} from "../../services/direct-downloadable-report.service";


const logger = reportWorkbenchLogger.compose('ReportWorkbenchComponent');

@Component({
  selector: 'sqtm-app-report-workbench',
  templateUrl: './report-workbench.component.html',
  styleUrls: ['./report-workbench.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    ReportWorkbenchService,
    DocxReportService,
    DirectDownloadableReportService
  ]
})
export class ReportWorkbenchComponent implements OnInit, OnDestroy {

  @ViewChild('nameField', {read: TextFieldComponent})
  nameField: TextFieldComponent;

  @Input()
  availableReports: Report[];

  @Input()
  projectId: number;

  @Input()
  containerId: number;

  @Input()
  reportDefinition: ReportDefinitionModel;

  isEditMode: boolean;

  unInstalledReports$: Observable<UnInstallReport[]>;

  availableExecutionPhaseReports$: Observable<Report[]>;

  availablePreparationPhaseReports$: Observable<Report[]>;

  availableVariousReports$: Observable<Report[]>;

  selectedReport$: Observable<Report>;

  showSelectReportPanelIcon$: Observable<string>;

  showSelectReportPanel$: Observable<boolean>;

  serverSideValidationErrors = [];

  formGroup: FormGroup;

  showSpinner$: Observable<boolean>;
  private saving$ = new BehaviorSubject<boolean>(false);

  private unsub$ = new Subject<void>();

  constructor(private reportWorkbenchService: ReportWorkbenchService,
              private fb: FormBuilder,
              private changeDetectorRef: ChangeDetectorRef,
              private restService: RestService,
              private router: Router,
              private docxService: DocxReportService,
              private referentialDataService: ReferentialDataService,
              private reportDefinitionService: ReportDefinitionService,
              private dialogService: DialogService,
              private vcr: ViewContainerRef,
              private translateService: TranslateService,
              private directDownloadableReportService: DirectDownloadableReportService) {
  }

  ngOnDestroy(): void {
    this.unsub$.next();
    this.unsub$.complete();
  }

  ngOnInit(): void {
    this.availableExecutionPhaseReports$ = this.reportWorkbenchService.state$.pipe(
      select(availableExecutionPhaseReportsSelector)
    );

    this.availablePreparationPhaseReports$ = this.reportWorkbenchService.state$.pipe(
      select(availablePreparationPhaseReportsSelector)
    );

    this.availableVariousReports$ = this.reportWorkbenchService.state$.pipe(
      select(availableVariousReportsSelector)
    );

    this.unInstalledReports$ = this.reportWorkbenchService.state$.pipe(
      map(state => {
        const reports: UnInstallReport[] = [];
        const availableReport = Object.values(state.availableReports.entities);
        if (availableReport.filter(report => report.id === 'report.iteration.report.label').length !== 1) {
          reports.push({
            id: 'report.iteration.report.label',
            labelKey: 'sqtm-core.custom-report-workspace.reports.uninstalled.iteration.label',
            messageKey: 'sqtm-core.custom-report-workspace.reports.uninstalled.iteration.message'
          });
        }

        if (availableReport.filter(report => report.id === 'report.campaignassessment.report.label').length !== 1) {
          reports.push({
            id: 'report.campaignassessment.report.label',
            labelKey: 'sqtm-core.custom-report-workspace.reports.uninstalled.campaignassessment.label',
            messageKey: 'sqtm-core.custom-report-workspace.reports.uninstalled.campaignassessment.message'
          });
        }


        return reports;
      })
    );
    this.selectedReport$ = this.reportWorkbenchService.state$.pipe(
      takeUntil(this.unsub$),
      select(selectedReportSelector),
      filter(report => Boolean(report)),
      withLatestFrom(this.referentialDataService.milestoneModeData$, this.referentialDataService.projectDatas$),
      tap(([report, milestoneModeData, projectDataMap]: [Report, MilestoneModeData, ProjectDataMap]) =>
        this.isEditMode = this.reportDefinition && report.id === this.reportDefinition.pluginNamespace),
      // side effect... take care to multiple subscription on cold observable
      // we initialize all fields even hidden one to ensure no need values are sent to server
      tap(([report, milestoneModeData, projectDataMap]: [Report, MilestoneModeData, ProjectDataMap]) =>
        this.initializeCriteria(report, milestoneModeData)),
      // filter inputs and composite input options
      map(([report, milestoneModeData, projectDataMap]: [Report, MilestoneModeData, ProjectDataMap]) => {
        const filteredInputs = this.filterInputs(report, milestoneModeData, projectDataMap);
        return {...report, inputs: filteredInputs};
      }),
      // share replay to avoid reset of criteria each time a subscriber is registered.
      // Remember that cold observable replay the whole chain for each connected subscriber...
      shareReplay(1)
    );

    this.showSelectReportPanel$ = this.reportWorkbenchService.state$.pipe(
      select(showSelectReportPanelSelector),
    );

    this.showSelectReportPanelIcon$ = this.reportWorkbenchService.state$.pipe(
      select(showSelectReportPanelSelector),
      map(show => show ? 'double-left' : 'double-right')
    );

    this.reportWorkbenchService.initializeInitialData(this.availableReports);
    if (this.reportDefinition != null) {
      this.formGroup = this.fb.group({
        name: this.fb.control(this.reportDefinition.name, [
          Validators.required,
          Validators.pattern('(.|\\s)*\\S(.|\\s)*'),
          Validators.maxLength(255)
        ]),
        summary: this.fb.control(this.reportDefinition.summary),
        description: this.fb.control(this.reportDefinition.description),
        criteria: this.fb.array([])
      });
      this.reportWorkbenchService.changeSelectedReport(this.reportDefinition.pluginNamespace);
    } else {
      const placeHolder = this.translateService.instant('sqtm-core.custom-report-workspace.create-report.new-report');
      this.formGroup = this.fb.group({
        name: this.fb.control(placeHolder, [
          Validators.required,
          Validators.pattern('(.|\\s)*\\S(.|\\s)*'),
          Validators.maxLength(255)
        ]),
        summary: this.fb.control(''),
        description: this.fb.control(''),
        criteria: this.fb.array([])
      });
    }


    this.showSpinner$ = combineLatest([this.docxService.generatingReport$, this.saving$]).pipe(
      takeUntil(this.unsub$),
      map(([generating, saving]) => generating || saving)
    );
  }

  changeSelectedReport(reportId: string) {
    this.reportWorkbenchService.changeSelectedReport(reportId);
  }

  toggleSelectReportPanel() {
    this.reportWorkbenchService.toggleSelectReportPanel();
  }

  // clear ALL criteria fields. Take care to call it only when the selected report change
  private initializeCriteria(report, milestoneModeData) {
    const control = this.formGroup.get('criteria') as FormArray;
    control.clear();

    if (this.isEditMode) {
      const parsedParameters = JSON.parse(this.reportDefinition.parameters);
      report.inputs.forEach(input => {
        if (isReportOptionGroup(input)) {
          this.appendGroupFormControlWithInitialValues(input, control, parsedParameters);
        } else if (input.type === ReportInputType.INPUTS_GROUP) {
          this.appendInputsGroupFormControlWithInitialValues(input, control, parsedParameters);
        } else {
          const subGroup = new FormGroup({});
          const val = parsedParameters[input.name].val;
          subGroup.addControl(input.name, new FormControl(val));
          control.push(subGroup);
        }
      });
    } else {
      report.inputs.forEach(input => {
        if (isReportOptionGroup(input)) {
          this.appendGroupFormControl(input, control, milestoneModeData);
        } else if (input.type === ReportInputType.INPUTS_GROUP) {
          this.appendInputsGroupFormControl(input, control);
        } else {
          const subGroup = new FormGroup({});
          this.addControlInput(subGroup, input, milestoneModeData);
          control.push(subGroup);
        }
      });
    }


  }

  private appendGroupFormControlWithInitialValues(input, control: FormArray, parsedParameters) {
    const optionGroup = input as ReportOptionGroup;
    const compositeOptions = optionGroup.options.filter(o => o && o.composite);

    if (compositeOptions.length > 0) {

      let value = '';
      const defaultOption = parsedParameters[input.name];
      value = defaultOption.val;
      const subGroup = new FormGroup({});
      subGroup.addControl(input.name, new FormControl(value));

      if (optionGroup.type === ReportInputType.RADIO_BUTTONS_GROUP) {
        compositeOptions.forEach(option => {
          if (option.contentType === ReportInputType.PROJECT_PICKER) {
            const paramValue: string[] = parsedParameters[option.givesAccessTo].val;
            const values = paramValue.map(i => Number.parseInt(i, 10));
            subGroup.addControl(option.givesAccessTo, new FormControl(values));
          } else {
            let paramValue = [];
            if (parsedParameters[option.givesAccessTo] != null) {
              paramValue = parsedParameters[option.givesAccessTo].val;
            }

            subGroup.addControl(option.givesAccessTo, new FormControl(paramValue));
          }
        });
      }
      if (optionGroup.type === ReportInputType.CHECKBOXES_GROUP) {
        compositeOptions.forEach(option => {
          const checked = parsedParameters[input.name].val.includes(option.value);
          subGroup.addControl(option.givesAccessTo, new FormControl(checked));
        });
      }

      control.push(subGroup);
    } else {
      const defaultOption = parsedParameters[input.name];
      control.push(new FormControl(defaultOption.val));
    }
  }

  private addControlInput(subGroup: FormGroup, reportInput, milestoneModeData: MilestoneModeData) {
    let value: any;
    switch (reportInput.type) {
      case ReportInputType.CHECKBOX:
        const checkBoxInput = reportInput as ReportCheckboxInput;
        value = checkBoxInput.defaultSelected;
        break;
      case ReportInputType.PROJECT_PICKER:
        value = [this.projectId];
        break;
      case ReportInputType.MILESTONE_PICKER:
        value = this.getSelectedMilestoneValue(milestoneModeData);
        break;
      case ReportInputType.TREE_PICKER:
      case ReportInputType.TAG_PICKER:
        value = [];
        break;
    }
    subGroup.addControl(reportInput.name, new FormControl(value));
  }

  private appendInputsGroupFormControlWithInitialValues(input, control: FormArray, parsedParameters) {
    const optionGroup = input as ReportInputsGroup;
    const inputs = optionGroup.inputs;
    if (inputs.length > 0) {
      const subGroup = new FormGroup({});
      inputs.forEach(option => {
        if (option.type === ReportInputType.DATE) {
          const paramValue = parsedParameters[option.name].val;
          if (paramValue !== '--') {
            const dateValue: Date = new Date(paramValue);
            subGroup.addControl(option.name, new FormControl(dateValue));
          } else {
            subGroup.addControl(option.name, new FormControl(''));
          }
        } else {
          subGroup.addControl(option.name, new FormControl(parsedParameters[option.name].val));
        }

      });
      control.push(subGroup);
    } else {
      control.push(new FormControl(''));
    }
  }

  private appendInputsGroupFormControl(input, control: FormArray) {
    const optionGroup = input as ReportInputsGroup;
    const inputs = optionGroup.inputs;
    if (inputs.length > 0) {
      const subGroup = new FormGroup({});
      // subGroup.addControl(input.name, new FormControl(''));
      inputs.forEach(option => subGroup.addControl(option.name, new FormControl('')));
      control.push(subGroup);
    } else {
      control.push(new FormControl(''));
    }
  }

  private appendGroupFormControl(input, control: FormArray, milestoneModeData: MilestoneModeData) {
    const optionGroup = input as ReportOptionGroup;
    const compositeOptions = optionGroup.options.filter(o => o && o.composite);
    if (compositeOptions.length > 0) {

      let value = '';
      const defaultOption = optionGroup.options.find(o => o && o.defaultSelected);
      value = defaultOption.value;
      const subGroup = new FormGroup({});
      subGroup.addControl(input.name, new FormControl(value));
      compositeOptions.forEach(option => this.addControl(subGroup, option, milestoneModeData));
      control.push(subGroup);
    } else {
      const defaultOption = optionGroup.options.find(o => o && o.defaultSelected);
      control.push(new FormControl(defaultOption.value));
    }
  }

  private addControl(subGroup: FormGroup, option, milestoneModeData) {
    let value: any[];
    switch (option.contentType) {
      case ReportInputType.CHECKBOX:
        value = option.defaultSelected;
        break;
      case ReportInputType.PROJECT_PICKER:
        value = [this.projectId];
        break;
      case ReportInputType.MILESTONE_PICKER:
        value = this.getSelectedMilestoneValue(milestoneModeData);
        break;
      case ReportInputType.TREE_PICKER:
      case ReportInputType.TAG_PICKER:
        value = [];
        break;
    }
    subGroup.addControl(option.givesAccessTo, new FormControl(value));
  }

  private getSelectedMilestoneValue(milestoneModeData): number[] {
    if (milestoneModeData.milestoneFeatureEnabled && milestoneModeData.milestoneModeEnabled) {
      return [milestoneModeData.selectedMilestone.id];
    } else {
      return [];
    }
  }

// for use in templates
  isReportOptionGroup(input: ReportInput) {
    return isReportOptionGroup(input);
  }

  isReportInputsGroup(input: ReportInput) {
    return input.type === ReportInputType.INPUTS_GROUP;
  }

  getSubForm(i: number) {
    return (this.formGroup.get('criteria') as FormArray).controls[i] as FormGroup;
  }

  handleAdd() {
    this.selectedReport$.pipe(
      take(1)
    ).subscribe((report) => {
      const criteria = this.validateCriteriaForm(report);
      if (this.formGroup.get('name').errors) {
        this.nameField.showClientSideError();
      } else {
        this.nameField.errors = [];
        this.nameField.showClientSideError();
      }
      if (this.formGroup.valid) {
        this.saveReport(report, criteria);
      }
    });
  }

  private saveLabel() {
    if (this.reportDefinition != null) {
      return this.translateService.instant('sqtm-core.generic.label.save');
    } else {
      return this.translateService.instant('sqtm-core.generic.label.add');
    }
  }

  private saveReport(report: Report, criteria: FormArray) {
    const params = this.convertCriteriaFormToHttpRequestParam(report, criteria);
    const reportDefinition: ReportDefinitionModel = {
      id: null,
      name: this.formGroup.get('name').value,
      summary: this.formGroup.get('summary').value,
      description: this.formGroup.get('description').value,
      parameters: JSON.stringify(params),
      pluginNamespace: report.id
    };
    this.saving$.next(true);
    if (this.reportDefinition != null) {
      this.reportDefinitionService.updateReportDefinition(this.containerId, reportDefinition).pipe(
        catchError(err => {
          this.saving$.next(false);
          if (doesHttpErrorContainsSquashFieldError(err)) {
            const squashFieldError = extractSquashFirstFieldError(err);
            this.serverSideValidationErrors = squashFieldError.fieldValidationErrors;
            this.changeDetectorRef.detectChanges();
          }
          return throwError(err);
        })
      ).subscribe(({id}) => this.router.navigate(['custom-report-workspace', 'report-definition', id]));
    } else {
      this.reportDefinitionService.saveNewReportDefinition(this.containerId, reportDefinition).pipe(
        catchError(err => {
          this.saving$.next(false);
          if (doesHttpErrorContainsSquashFieldError(err)) {
            const squashFieldError = extractSquashFirstFieldError(err);
            this.serverSideValidationErrors = squashFieldError.fieldValidationErrors;
            this.changeDetectorRef.detectChanges();
          }
          return throwError(err);
        })
      ).subscribe(({id}) => this.router.navigate(['custom-report-workspace', 'report-definition', id]));
    }

  }

  handleDownload() {
    logger.debug('criteria form : ', [this.formGroup.getRawValue()]);
    this.selectedReport$.pipe(
      take(1)
    ).subscribe((report) => {
      const criteria = this.validateCriteriaForm(report);
      logger.debug(`form is valid : ${this.formGroup.valid}`);
      if (this.formGroup.get('criteria').valid) {
        this.downloadReport(report, criteria);
      } else {
        logger.debug('ERROR IN FORM');
        this.changeDetectorRef.detectChanges();
      }
    });
  }

  private downloadReport(report: Report, criteria: FormArray) {
    logger.debug('GO FOR DOWNLOAD');
    const params = this.convertCriteriaFormToHttpRequestParam(report, criteria);
    logger.debug('launching report download with params : ', [params]);

    if (report.isDirectDownloadableReport) {
      this.directDownloadableReportService.directDownloadReport(report, params);
    } else if (report.isDocxTemplate) {
      this.downloadDocXReport(report, params);
    } else {
      this.openJasperReportDialog(report, params);
    }
  }

  private downloadDocXReport(report: Report, params) {
    this.docxService.generateReport(report, {json: params});
  }

  private convertCriteriaFormToHttpRequestParam(report: Report, criteria: FormArray) {
    const json: any = {};
    report.inputs.forEach((input, index) => {
      if (input.type === ReportInputType.CHECKBOXES_GROUP) {
        const subFormGroup = criteria.get([index]) as FormGroup;
        const optionGroup = input as ReportOptionGroup;
        const value = [];
        optionGroup.options.forEach(o => {
          if (o.contentType === ReportInputType.CHECKBOX) {
            const checked: boolean = subFormGroup.get(o.givesAccessTo).value;
            if (checked) {
              value.push(o.value);
            }
          }
        });
        json[input.name] = {type: input.type, val: value};
      } else if (input.type === ReportInputType.RADIO_BUTTONS_GROUP) {
        this.appendRadioButtonValue(criteria, index, input, json);
      } else if (input.type === ReportInputType.INPUTS_GROUP) {
        this.appendInputsGroupValue(criteria, index, input, json);
      } else if (input.type === ReportInputType.DROPDOWN_LIST) {
        this.appendDropDownListValue(criteria, index, input, json);
      } else {
        const subFormGroup = criteria.get([index]) as FormGroup;
        const value: any = subFormGroup.get(input.name).value;
        json[input.name] = {type: input.type, val: value};
      }
    });

    return json;
  }

  private appendInputsGroupValue(criteria: FormArray, index: number, input: ReportInput, json: any) {
    const subFormGroup = criteria.get([index]) as FormGroup;
    const optionGroup = input as ReportInputsGroup;
    optionGroup.inputs
      .filter(o => o.type !== ReportInputType.CHECKBOX)
      .forEach((option, optionIndex) => {
        const identifier = option.name;
        const optionValue = this.extractInputValue(subFormGroup, option);
        json[identifier] = {type: option.type, val: optionValue};
      });
  }

  private appendDropDownListValue(criteria: FormArray, index: number, input: ReportInput, json: any) {
    const subFormGroup = criteria.get([index]) as FormGroup;
    const optionGroup = input as ReportOptionGroup;
    optionGroup.options
      .filter(o => o.contentType !== ReportInputType.CHECKBOX)
      .forEach((option, optionIndex) => {
        const identifier = option.givesAccessTo || option.name;
        const optionValue = this.extractOptionValue(subFormGroup, option);
        json[identifier] = {type: option.contentType || input.type, val: optionValue};
      });
    const value: any = subFormGroup.get(input.name).value;
    json[input.name] = {type: input.type, val: value.toString()};
  }

  private appendRadioButtonValue(criteria: FormArray, index: number, input: ReportInput, json: any) {
    const subFormGroup = criteria.get([index]) as FormGroup;
    const optionGroup = input as ReportOptionGroup;
    optionGroup.options
      .filter(o => o.contentType !== ReportInputType.CHECKBOX)
      .forEach((option, optionIndex) => {
        const identifier = option.givesAccessTo || option.name;
        const optionValue = this.extractOptionValue(subFormGroup, option);
        json[identifier] = {type: option.contentType || input.type, val: optionValue};
      });
    const value: any = subFormGroup.get(input.name).value;
    json[input.name] = {type: input.type, val: value.toString()};
  }

  private extractInputValue(subFormGroup: FormGroup, option: ReportInput) {
    return subFormGroup.get(option.name).value || '--';
  }

  private extractOptionValue(subFormGroup: FormGroup, option: ReportOption) {
    let optionValue: any[] = subFormGroup.get(option.givesAccessTo).value || [];
    if (option.contentType === ReportInputType.PROJECT_PICKER) {
      optionValue = optionValue.map(o => o.toString());
    }
    return optionValue;
  }

  private validateCriteriaForm(report: Report) {
    const criteria = this.formGroup.get('criteria') as FormArray;
    report.inputs.forEach((input, index) => {
      if (input.required && isReportOptionGroup(input)) {
        const optionGroup = input as ReportOptionGroup;
        const subFormGroup = criteria.get([index]) as FormGroup;
        const baseInput = subFormGroup.get(input.name);
        const baseInputValue = baseInput.value;
        const selectedInput = optionGroup.options
          .find(o => {
            if (o.contentType === ReportInputType.CHECKBOX) {
              return o.givesAccessTo === baseInputValue;
            } else {
              return o.contentType === baseInputValue;
            }
          });
        const selectedInputValue: any[] = subFormGroup.get(selectedInput.givesAccessTo).value;
        if (selectedInputValue.length === 0) {
          logger.debug(`Empty value for composite input : ${input.name}. Selected subInput is of type ${baseInputValue}, with id ${selectedInput.givesAccessTo} in form controls.`);
          subFormGroup.setErrors({required: true});
        }
      } else if (input.required && input.type === ReportInputType.TREE_PICKER) {
        const subFormGroup = criteria.get([index]) as FormGroup;
        const baseInput = subFormGroup.get(input.name);
        const baseInputValue = baseInput.value;
        if (baseInputValue.length !== 1) {
          logger.debug(`Empty value for composite input : ${input.name}. Selected subInput is of type ${baseInputValue}, with id ${input.name} in form controls.`);
          subFormGroup.setErrors({required: true});
        }
      }
    });
    return criteria;
  }

  private filterInputs(report: Report, milestoneModeData: MilestoneModeData, projectDataMap: ProjectDataMap) {
    const filteredInputs = report.inputs.filter(i => this.filterInput(i, milestoneModeData, projectDataMap));
    filteredInputs.filter(i => isReportOptionGroup(i)).forEach(input => this.filterOptions(input, milestoneModeData, projectDataMap));
    return filteredInputs;
  }

  private filterInput(input: ReportInput, milestoneModeData: MilestoneModeData, projectDataMap: ProjectDataMap) {
    switch (input.type) {
      case ReportInputType.MILESTONE_PICKER:
        return milestoneModeData.milestoneFeatureEnabled;
      default:
        return Boolean(input);
    }

  }

  private filterOptions(input: ReportInput, milestoneModeData: MilestoneModeData, projectDataMap: ProjectDataMap) {
    const optionGroup = input as ReportOptionGroup;
    optionGroup.options = optionGroup.options.filter(option => this.filterOption(option, milestoneModeData, projectDataMap));
  }

  private filterOption(option: ReportOption, milestoneModeData: MilestoneModeData, projectDataMap: ProjectDataMap) {
    switch (option.contentType) {
      case ReportInputType.MILESTONE_PICKER:
        return milestoneModeData.milestoneFeatureEnabled;
      case ReportInputType.TAG_PICKER:
        const bindableEntity = option.bindableEntity;
        return Boolean(Object.values(projectDataMap).find(p => p.customFieldBinding[bindableEntity]?.length > 0));
      default:
        return true;
    }
  }

  private openJasperReportDialog(report: Report, params: any) {
    const configuration: JasperReportDialogConfiguration = {report, params};

    this.dialogService.openDialog<JasperReportDialogConfiguration, void>({
      id: 'jasper-report-dialog',
      component: JasperReportDialogComponent,
      viewContainerReference: this.vcr,
      width: 1230,
      height: 768,
      data: configuration
    });
  }

  handleCancel() {
    this.router.navigate(['custom-report-workspace']);
  }
}

import {Inject, Injectable} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import {CORE_MODULE_CONFIGURATION, SqtmCoreModuleConfiguration} from 'sqtm-core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {saveAs} from 'file-saver';
import {format} from 'date-fns';
import {catchError} from 'rxjs/operators';

const REPORT_DATE_FORMAT = 'yyyyMMdd-hhmm';

@Injectable()
export class JasperReportService {

  private _generatingReportSubject = new BehaviorSubject<boolean>(false);
  public generatingReport$ = this._generatingReportSubject.asObservable();

  constructor(@Inject(APP_BASE_HREF) private baseHref: string,
              @Inject(CORE_MODULE_CONFIGURATION) private sqtmCoreModuleConfiguration: SqtmCoreModuleConfiguration,
              private http: HttpClient) {

  }

  public getHtmlPreview(reportId: string, viewIndex: number, params: any): Observable<string> {
    const viewUrl = this.getViewUrl(reportId, viewIndex);
    return this.http.get<string>(viewUrl, {
      responseType: 'text' as any,
      params: {json: JSON.stringify(params)}
    });
  }

  public downloadReport(reportId: string, viewIndex: number, params: any, fileFormat: string, reportFileName: string) {
    const viewUrl = this.getViewUrl(reportId, viewIndex, fileFormat);
    this.emitStartGeneratingReport();

    this.http.get<any>(viewUrl, {
      responseType: 'arraybuffer' as any,
      params: {json: JSON.stringify(params)}
    }).pipe(
      catchError(err => {
        this.emitStopGeneratingReport();
        return of(err);
      })
    ).subscribe(binaryData => {
      try {
        const formattedDate = format(new Date(), REPORT_DATE_FORMAT);
        const file = new File([binaryData], `${reportFileName}-${formattedDate}.${fileFormat}`, {type: fileFormat});
        saveAs(file);
        this.emitStopGeneratingReport();
      } catch (err) {
        this.emitStopGeneratingReport();
        throw err;
      }

    });
  }

  private getViewUrl(reportId: string, viewIndex: number, fileFormat = 'html') {
    // tslint:disable-next-line:max-line-length
    return `${this.baseHref}${this.sqtmCoreModuleConfiguration.backendRootUrl}/reports/${reportId}/views/${viewIndex}/formats/${fileFormat}`;
  }

  private emitStartGeneratingReport() {
    this._generatingReportSubject.next(true);
  }

  private emitStopGeneratingReport() {
    this._generatingReportSubject.next(false);
  }

  public complete() {
    this._generatingReportSubject.complete();
  }
}

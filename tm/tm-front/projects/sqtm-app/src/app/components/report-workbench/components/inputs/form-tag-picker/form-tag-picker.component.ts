import {ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {BindableEntity, CustomFieldBindingData, InputType, ReferentialDataService} from 'sqtm-core';
import {map, take} from 'rxjs/operators';
import {NzSelectOptionInterface} from 'ng-zorro-antd/select';
import {FormGroup} from '@angular/forms';
import * as _ from 'lodash';

@Component({
  selector: 'sqtm-app-form-tag-picker',
  templateUrl: './form-tag-picker.component.html',
  styleUrls: ['./form-tag-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormTagPickerComponent implements OnInit {

  // Do not rename this to formControlName or Angular will have trouble to make distinction between [formControl] directive
  // and this input.
  @Input()
  criteriaFormControlName: string;

  @Input()
  criteriaFormGroup: FormGroup;

  @Input()
  bindableEntity: BindableEntity;

  tagValues$: Observable<NzSelectOptionInterface[]>;

  get selectedIds(): any[] {
    return this._selectedIds;
  }

  set selectedIds(selectedIds: any[]) {
    this._selectedIds = selectedIds;
    this.updateSelectedNames();
  }

  protected unsub$ = new Subject<void>();

  @ViewChild('button', {read: ElementRef})
  buttonElement: ElementRef;

  private _selectedIds: number[] = [];

  concatenatedNames = '';

  touched = false;

  disabled = false;
  listOfOption: any;

  constructor(private referentialDataService: ReferentialDataService) {
  }

  ngOnInit(): void {
    this.tagValues$ = this.referentialDataService.projectDatas$.pipe(
      take(1),
      map(projectDataMap => Object.values(projectDataMap)),
      map(projects => projects.map(p => p.customFieldBinding[this.bindableEntity])),
      map(cufArray => cufArray.flat()),
      map((cufs: CustomFieldBindingData[]) => cufs.map(c => c.customField).filter(c => c.inputType === InputType.TAG)),
      map(tagsCufs => tagsCufs.map(cuf => cuf.options).flat().map(option => option.label)),
      map((tags: string[]) => _.uniq(tags)),
      map((tags: string[]) => tags.map(tag => ({value: tag, label: tag})))
      );
  }

  private updateSelectedNames() {

  }

  handleOpen() {

  }
}

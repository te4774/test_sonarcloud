import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpEventType} from "@angular/common/http";
import {Report} from "../../../../../../../cypress/integration/model/custom-report/report-definition.model";
import {APP_BASE_HREF} from "@angular/common";
import {CORE_MODULE_CONFIGURATION, SqtmCoreModuleConfiguration} from 'sqtm-core';

@Injectable()
export class DirectDownloadableReportService {

  constructor(@Inject(APP_BASE_HREF) private baseHref: string,
              @Inject(CORE_MODULE_CONFIGURATION) private sqtmCoreModuleConfiguration: SqtmCoreModuleConfiguration,
              private http: HttpClient) { }

  public directDownloadReport(report: Report, params) {
    const reportId = typeof report === 'string' ? report : report.id;
    const url = `${this.baseHref}${this.sqtmCoreModuleConfiguration.backendRootUrl}/reports/${reportId}/data/direct-downloadable-report`;
    return this.http.get(url, {params: {json: JSON.stringify(params)}, responseType: 'blob', observe: 'events'}).subscribe(resp => this.downloadFile(resp));
  }

  private downloadFile(resp: HttpEvent<Blob>) {
    if (resp.type === HttpEventType.Response) {
      const a = document.createElement('a');
      const blob = new Blob([resp.body]);
      const url = window.URL.createObjectURL(blob);
      const filename = resp.headers.get("Content-Disposition").split("filename=")[1];
      a.href = url;
      a.download = filename;
      a.click();
      a.remove();
      window.URL.revokeObjectURL(url);
    }
  }
}

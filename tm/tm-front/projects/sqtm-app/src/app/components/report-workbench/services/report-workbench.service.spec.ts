import {TestBed} from '@angular/core/testing';

import {ReportWorkbenchService} from './report-workbench.service';

describe('ReportWorkbenchService', () => {
  let service: ReportWorkbenchService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ReportWorkbenchService
      ]
    });
    service = TestBed.inject(ReportWorkbenchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Overlay} from '@angular/cdk/overlay';
import {buildTreePickerDialogDefinition, DataRow, DialogService, TreePickerDialogConfiguration} from 'sqtm-core';
import {TranslateService} from '@ngx-translate/core';
import {AbstractFormPicker, ReportNodeReference} from '../abstract-form-picker';
import {filter, map, take, takeUntil} from 'rxjs/operators';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'sqtm-app-form-test-case-tree-picker',
  templateUrl: './form-test-case-tree-picker.component.html',
  styleUrls: ['./form-test-case-tree-picker.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: FormTestCaseTreePickerComponent
    }
  ]
})
export class FormTestCaseTreePickerComponent extends AbstractFormPicker implements OnInit, ControlValueAccessor {

  constructor(overlay: Overlay,
              private dialogService: DialogService,
              private changeDetectorRef: ChangeDetectorRef,
              private translateService: TranslateService) {
    super(overlay);
  }

  ngOnInit(): void {
  }

  protected updateSelectedNames() {
    this.concatenatedNames = this.translateService.instant(
      'sqtm-core.custom-report-workspace.create-report.element-selected',
      {nb: this.selectedIds.length.toString()});
  }

  handleOpen() {
    const selectedReportNodeReferences = this.selectedIds as ReportNodeReference[];
    const selectedNodes = selectedReportNodeReferences.map(r => this.reportNodeReferenceToRowIdentifier(r)).map(r => r.asString());
    const dialogReference = this.dialogService.openDialog<TreePickerDialogConfiguration, DataRow[]>(
      buildTreePickerDialogDefinition('test-case-picker-dialog', selectedNodes));
    dialogReference.dialogClosed$.pipe(
      takeUntil(this.unsub$),
      take(1),
      filter(result => Boolean(result)),
      map(rows => this.convertRowsToReportNodeReference(rows))
    ).subscribe((reportNodeReferences) => {
        this.selectedIds = reportNodeReferences;
        this.onChange(reportNodeReferences);
        this.changeDetectorRef.detectChanges();
      }
    );
  }
}

import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {CustomField, CustomFieldOption, DenormalizedCustomFieldValueModel} from 'sqtm-core';

@Component({
  selector: 'sqtm-app-denormalized-custom-field',
  templateUrl: './denormalized-custom-field.component.html',
  styleUrls: ['./denormalized-custom-field.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DenormalizedCustomFieldComponent implements OnInit {

  @Input()
  denormalizedFieldValue: DenormalizedCustomFieldValueModel;

  get customField(): CustomField {
    const options: CustomFieldOption[] = [{
      label: this.denormalizedFieldValue.value
    } as CustomFieldOption];

    return {
      inputType: this.denormalizedFieldValue.inputType,
      label: this.denormalizedFieldValue.label,
      options
    } as CustomField;
  }

  get customFieldValue(): string | string[] {
    if (this.denormalizedFieldValue.inputType === 'TAG'
      && this.denormalizedFieldValue.value != null
    ) {
      if (this.denormalizedFieldValue.value.length === 0) {
       return null;
      } else {
       return this.denormalizedFieldValue.value.split('|');
      }
    }
    return this.denormalizedFieldValue.value;
  }

  constructor() {
  }

  ngOnInit() {
  }

}

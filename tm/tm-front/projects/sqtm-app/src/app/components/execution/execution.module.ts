import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DenormalizedStatusComponent} from './denormalized-status/denormalized-status.component';
import {TranslateModule} from '@ngx-translate/core';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {DenormalizedImportanceComponent} from './denormalized-importance/denormalized-importance.component';
import {CustomFieldModule, WorkspaceCommonModule} from 'sqtm-core';
import {DenormalizedCustomFieldComponent} from './denormalized-custom-field/denormalized-custom-field.component';


@NgModule({
  declarations: [DenormalizedStatusComponent, DenormalizedImportanceComponent, DenormalizedCustomFieldComponent],
    exports: [
        DenormalizedStatusComponent,
        DenormalizedImportanceComponent,
        DenormalizedCustomFieldComponent
    ],
    imports: [
        CommonModule,
        TranslateModule.forChild(),
        NzIconModule,
        WorkspaceCommonModule,
        CustomFieldModule,
        NzToolTipModule,
    ]
})
export class ExecutionModule {
}

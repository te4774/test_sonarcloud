import {AxisLabelsConverter} from './axis-labels-converters';
import {AxisColumn, ChartDataType, ChartOperation} from 'sqtm-core';
import {Injectable} from '@angular/core';
import {format, setISOWeek, setYear} from 'date-fns';
import {TranslateService} from '@ngx-translate/core';
import {chartsLogger} from '../charts.logger';

const logger = chartsLogger.compose('DateByWeekAxisConverter');

@Injectable()
export class DateByWeekToStringAxisConverter implements AxisLabelsConverter {


  constructor(private translateService: TranslateService) {
  }

  static parseDateByWeek(rawLabel) {
    logger.trace(`Parsing raw date by week ${rawLabel}`);
    const year = rawLabel.toString().slice(0, 4);
    const week = rawLabel.toString().slice(4, 6);
    let date = new Date();
    logger.trace(`Parsed raw date by week ${date}`);
    logger.trace(`setting week ${ Number.parseInt(week, 10)}`);
    date = setYear(date, Number.parseInt(year, 10));
    date = setISOWeek(date, Number.parseInt(week, 10));
    logger.trace(`Parsed raw date by week ${date}`);
    return date;
  }

  convertAxisLabels(axis: AxisColumn, rawLabels: any[]): string[] {
    logger.debug(`Converting labels for axis ${axis.label}. Raw labels are `, [rawLabels]);
    return rawLabels.map(rawLabel => {
      if (rawLabel) {
        const date = DateByWeekToStringAxisConverter.parseDateByWeek(rawLabel);
        logger.debug('Standard date', [date]);
        const formattedDate = format(date, 'ww - yyyy');
        logger.debug('Formatted date', [formattedDate]);
        return formattedDate;
      }
      return this.translateService.instant('sqtm-core.generic.label.never');
    });
  }

  handleAxis(axis: AxisColumn): boolean {
    // tslint:disable-next-line:max-line-length
    logger.debug(`Testing if DateByDayAxisConverter can handle ${axis.label}. Datatype : ${axis.column.dataType}. Operation : ${axis.operation}`, [axis]);
    return axis.column.dataType === ChartDataType.DATE
      && axis.operation === ChartOperation.BY_WEEK;
  }

}

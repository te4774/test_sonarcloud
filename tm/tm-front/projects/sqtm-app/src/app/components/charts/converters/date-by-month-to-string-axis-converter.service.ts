import {AxisLabelsConverter} from './axis-labels-converters';
import {AxisColumn, ChartDataType, ChartOperation, getSupportedBrowserLang} from 'sqtm-core';
import {Injectable} from '@angular/core';
import {format, setMonth} from 'date-fns';
import enGB from 'date-fns/locale/en-GB';
import fr from 'date-fns/locale/fr';
import es from 'date-fns/locale/es';
import de from 'date-fns/locale/de';
import {TranslateService} from '@ngx-translate/core';
import {chartsLogger} from '../charts.logger';
import {capitalize} from 'lodash';

const logger = chartsLogger.compose('DateByMonthToStringAxisConverter');


@Injectable()
export class DateByMonthToStringAxisConverter implements AxisLabelsConverter {


  constructor(private translateService: TranslateService) {
  }

  static parseDateByMonth(rawLabel): Date {
    const year = rawLabel.toString().slice(0, 4);
    const month = rawLabel.toString().slice(4, 6);
    let date = new Date(year);
    date = setMonth(date, Number.parseInt(month, 10) - 1);
    return date;
  }

  convertAxisLabels(axis: AxisColumn, rawLabels: any[]): string[] {
    logger.debug(`Converting labels for axis ${axis.label}. Raw labels are `, [rawLabels]);
    return rawLabels.map(rawLabel => {
      if (rawLabel) {
        const date = DateByMonthToStringAxisConverter.parseDateByMonth(rawLabel);
        // date = setDay(date, 1);
        logger.debug('Formatted date', [date]);
        return capitalize(format(date, 'MMM yyyy', {locale: findDateFnsLocale(getSupportedBrowserLang(this.translateService))}));
      }
      return this.translateService.instant('sqtm-core.generic.label.never');
    });
  }

  handleAxis(axis: AxisColumn): boolean {
    // tslint:disable-next-line:max-line-length
    logger.debug(`Testing if DateByDayAxisConverter can handle ${axis.label}. Datatype : ${axis.column.dataType}. Operation : ${axis.operation}`, [axis]);
    return axis.column.dataType === ChartDataType.DATE
      && axis.operation === ChartOperation.BY_MONTH;
  }

}

export function findDateFnsLocale(ngxTranslateLocal: string): Locale {
  switch (ngxTranslateLocal) {
    case 'en':
      return enGB;
    case 'fr':
      return fr;
    case 'es':
      return es;
    case 'de':
      return de;
    default:
      return enGB;
  }
}

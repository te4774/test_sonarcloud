import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  CustomReportBarChartComponent
} from './components/chart-rendering/custom-report-bar-chart/custom-report-bar-chart.component';
import {
  CustomReportPieChartComponent
} from './components/chart-rendering/custom-report-pie-chart/custom-report-pie-chart.component';
import {
  CustomReportCumulativeChartComponent
} from './components/chart-rendering/custom-report-cumulative-chart/custom-report-cumulative-chart.component';
import {
  CustomReportComparativeChartComponent
} from './components/chart-rendering/custom-report-comparative-chart/custom-report-comparative-chart.component';
import {
  CustomReportTrendChartComponent
} from './components/chart-rendering/custom-report-trend-chart/custom-report-trend-chart.component';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzInputModule} from 'ng-zorro-antd/input';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzToolTipModule} from 'ng-zorro-antd/tooltip';
import {TranslateModule} from '@ngx-translate/core';
import {GridModule, SqtmDragAndDropModule, UiManagerModule, WorkspaceCommonModule} from 'sqtm-core';
import {CustomChartComponent} from './components/chart-rendering/custom-chart/custom-chart.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    CustomReportBarChartComponent,
    CustomReportPieChartComponent,
    CustomReportCumulativeChartComponent,
    CustomReportComparativeChartComponent,
    CustomReportTrendChartComponent,
    CustomChartComponent,
  ],
  exports: [
    CustomReportBarChartComponent,
    CustomReportPieChartComponent,
    CustomReportCumulativeChartComponent,
    CustomReportComparativeChartComponent,
    CustomReportTrendChartComponent,
    CustomChartComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    NzIconModule,
    NzButtonModule,
    WorkspaceCommonModule,
    GridModule,
    NzToolTipModule,
    UiManagerModule,
    SqtmDragAndDropModule,
    NzInputModule,
    FormsModule,
    ReactiveFormsModule,
    NzSelectModule,
  ]
})
export class ChartsModule {
}

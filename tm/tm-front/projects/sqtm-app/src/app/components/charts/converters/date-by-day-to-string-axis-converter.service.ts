import {AxisLabelsConverter} from './axis-labels-converters';
import {
  AxisColumn,
  ChartDataType,
  ChartDefinitionModel,
  ChartOperation,
  ChartType,
  getSupportedBrowserLang
} from 'sqtm-core';
import {Injectable} from '@angular/core';
import {format, parse} from 'date-fns';
import {TranslateService} from '@ngx-translate/core';
import {chartsLogger} from '../charts.logger';
import {findDateFnsLocale} from './date-by-month-to-string-axis-converter.service';

const logger = chartsLogger.compose('DateByDayToStringAxisConverter');

@Injectable()
export class DateByDayToStringAxisConverter implements AxisLabelsConverter {


  constructor(private translateService: TranslateService) {
  }

  static parseDateByDay(rawLabel): Date {
    // toString() because our date are transmitted as... number... but this number is NOT a timestamp
    // it's a number like 20200823 that represent 23/08/2020
    return parse(rawLabel.toString(), 'yyyyMMdd', new Date());
  }

  convertAxisLabels(axis: AxisColumn, rawLabels: any[]): string[] {
    logger.debug(`Converting labels for axis ${axis.label}. Raw labels are `, [rawLabels]);
    return rawLabels.map(rawLabel => {
      if (rawLabel) {
        const parsed = DateByDayToStringAxisConverter.parseDateByDay(rawLabel);
        getSupportedBrowserLang(this.translateService);
        return format(parsed, 'dd MMM yyyy', {locale: findDateFnsLocale(getSupportedBrowserLang(this.translateService))});
      }
      return this.translateService.instant('sqtm-core.generic.label.never');
    });
  }

  handleAxis(axis: AxisColumn, chartDefinition: ChartDefinitionModel): boolean {
    // tslint:disable-next-line:max-line-length
    logger.debug(`Testing if DateByDayAxisConverter can handle ${axis.label}. Datatype : ${axis.column.dataType}. Operation : ${axis.operation}`, [axis]);
    return axis.column.dataType === ChartDataType.DATE
      && axis.operation === ChartOperation.BY_DAY;
  }

}

export function isTimeSensitiveChart(chartType: ChartType) {
  return [ChartType.CUMULATIVE, ChartType.TREND].includes(chartType);
}

export function isTimeInsensitiveChart(chartType: ChartType) {
  return !isTimeSensitiveChart(chartType);
}

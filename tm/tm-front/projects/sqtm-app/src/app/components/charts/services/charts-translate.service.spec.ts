import {TestBed} from '@angular/core/testing';

import {ChartsTranslateService} from './charts-translate.service';
import {AXIS_CONVERTERS} from '../charts.constants';
import {LevelEnumAxisConverter} from '../converters/level-enum-axis-converter.service';
import {DateByDayToStringAxisConverter} from '../converters/date-by-day-to-string-axis-converter.service';
import {TranslateModule} from '@ngx-translate/core';
import {PassTroughAxisConverter} from '../converters/pass-trough-axis-converter';

describe('ChartsTranslateService', () => {
  let service: ChartsTranslateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      providers: [
        {
          provide: AXIS_CONVERTERS,
          useClass: LevelEnumAxisConverter,
          multi: true
        },
        {
          provide: AXIS_CONVERTERS,
          useClass: DateByDayToStringAxisConverter,
          multi: true
        },
        {
          provide: ChartsTranslateService,
          useClass: ChartsTranslateService
        },
        {
          provide: PassTroughAxisConverter,
          useClass: PassTroughAxisConverter
        }
      ]
    });
    service = TestBed.inject(ChartsTranslateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

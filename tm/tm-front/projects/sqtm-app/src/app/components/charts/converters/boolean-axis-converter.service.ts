import {AxisLabelsConverter} from './axis-labels-converters';
import {AxisColumn, ChartDataType} from 'sqtm-core';
import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class BooleanAxisConverter implements AxisLabelsConverter {

  constructor(private translateService: TranslateService) {
  }

  convertAxisLabels(axis: AxisColumn, rawLabels: any[]): string[] {
    return rawLabels.map(rawLabel => this.translateService.instant(`sqtm-core.generic.label.${rawLabel}`));
  }

  handleAxis(axis: AxisColumn): boolean {
    return axis.column.dataType === ChartDataType.BOOLEAN;
  }

}

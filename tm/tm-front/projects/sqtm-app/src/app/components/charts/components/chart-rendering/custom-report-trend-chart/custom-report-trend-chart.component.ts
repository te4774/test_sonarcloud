import {ChangeDetectionStrategy, Component} from '@angular/core';
import {AbstractCustomReportChart} from '../abstract-custom-report-chart';
import {ChartsTranslateService} from '../../../services/charts-translate.service';
import {ChartDefinitionService, ChartType, ReferentialDataService} from 'sqtm-core';
import * as _ from 'lodash';
import {Dictionary} from 'lodash';
import {AxisType, Layout, newPlot} from 'plotly.js';
import {chartsLogger} from '../../../charts.logger';
import {TranslateService} from '@ngx-translate/core';

const logger = chartsLogger.compose('CustomReportTrendChartComponent');

@Component({
  selector: 'sqtm-app-custom-report-trend-chart',
  templateUrl: './custom-report-trend-chart.component.html',
  styleUrls: ['./custom-report-trend-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomReportTrendChartComponent extends AbstractCustomReportChart {

  constructor(protected chartsTranslateService: ChartsTranslateService,
              protected chartDefinitionService: ChartDefinitionService,
              protected referentialDataService: ReferentialDataService,
              protected translateService: TranslateService) {
    super(chartsTranslateService, chartDefinitionService, referentialDataService, translateService);
  }

  protected generateChart() {
    logger.debug(`Building chart ${this.chartDefinition.name} `);
    if (this.isDateChart()) {
      this.generateDateChart();
    } else {
      this.generateDefaultChart('category');
    }
  }

  private generateDefaultChart(plotlyType: AxisType) {
    const traces: Dictionary<TraceData[]> = this.extractTraces();
    const data = this.buildPlotlyData(traces);
    const layout: Partial<Layout> = {
      title: this.chartTitle,
      showlegend: true,
      legend: {
        font: this.legendFont
      },
      yaxis: {
        rangemode: 'tozero',
        title: {
          text: this.getMeasureText(),
          font: this.axisTitleFont
        },
        fixedrange: true,
        tickfont: this.legendFont,
      },
      xaxis: {
        type: plotlyType,
        title: {
          text: this.getAxisText(),
          font: this.axisTitleFont
        },
        fixedrange: true,
        tickfont: this.legendFont,
      }
    };

    if (this.isCompactLayout()) {
      layout.margin = {
        t: 25
      };
    }

    const config: Partial<Plotly.Config> = this.getDefaultConfig();

    newPlot(this.chartContainer.nativeElement, data, layout, config);
  }

  private buildPlotlyData(traces: Dictionary<TraceData[]>, xAxis = 'x', type = 'scatter', showlegend = true) {

    return Object
      .entries<TraceData[]>(traces)
      // .filter(([key, serie]) => serie.length > 0)
      .map(([key, serie], index) => {
        const xLabels = serie.map(s => s.xLabel);
        const yValues = serie.map(s => s.yValue);
        let color: string;
        if (this.hasCustomColors()) {
          color = this.chartDefinition.colours[index];
        } else if (this.isColorConstrained(this.chartDefinition.axis[1])) {
          const rawLabel = serie[0].traceRawLabel?.toString();
          color = this.getColors([rawLabel], this.chartDefinition.axis[1])[0];
        } else {
          color = this.defaultColours[index];
        }
        // const colors = this.getColors(traceRawLabels, this.chartDefinition.axis[1]);
        // const color = this.chartDefinition.colours?.length > 0 ? colors[index] : colors[0];

        let characterLength = 25;
        if (this.isCompactLayout()) {
          characterLength = 17;
        }

        const truncatedLegendLabel = key.length > characterLength ? [key.slice(0, characterLength), '...'].join('') : key;

        const serieData: any = {
          x: xLabels,
          y: yValues,
          type,
          name: truncatedLegendLabel,
          xaxis: xAxis,
          showlegend,
          marker: {
            color
          },
          line: {
            color
          },
        };
        return serieData;
      });
  }

  private extractTraces(): Dictionary<TraceData[]> {
    const traceData = this.extractTraceData();
    const traces = _.groupBy<TraceData>(traceData, 'traceName');
    // now we must extract all the xLabels to spot missing data
    const uniqXLabels = _.uniq(traceData.map(t => t.xLabel));
    // if the value for a label doesn't exist, we reuse the last value. Else we force put a 0, hopping plotly will trace the 0...
    const conformedTraces = Object.entries(traces)
      .reduce((modifiedTraces, [traceName, trace]) => {
        const modifiedTrace: TraceData[] = [...trace];
        logger.debug(`conforming ${traceName}`, [trace]);
        let currentValue = 0;
        uniqXLabels.forEach((uniqXLabel, index) => {
          const traceElement: TraceData = modifiedTrace[index];
          if (traceElement && traceElement.xLabel === uniqXLabel) {
            if (!traceElement.isNever) {
              currentValue = currentValue + traceElement.yValue;
              traceElement.yValue = currentValue;
            }
          } else {
            modifiedTrace.splice(index, 0, {
              traceName: traceName,
              traceRawLabel: traceElement?.traceRawLabel,
              xLabel: uniqXLabel,
              yValue: currentValue
            });
          }
        });
        logger.debug(`conformed ${traceName}`, [modifiedTrace]);
        modifiedTraces[traceName] = modifiedTrace;
        return modifiedTraces;
      }, {} as any);
    logger.debug('Series are regrouped and translated as :', [conformedTraces]);
    return conformedTraces;
  }

  private extractTraceData() {
    const xAxisDefinition = this.chartDefinition.axis[0];
    const xAxisConverter = this.chartsTranslateService.findConverter(xAxisDefinition, this.chartDefinition);
    const isDateChart = this.isDateChart();
    logger.debug(`x axis definition is `, [xAxisDefinition]);
    const traceAxisDefinition = this.chartDefinition.axis[1];
    const traceAxisConverter = this.chartsTranslateService.findConverter(traceAxisDefinition, this.chartDefinition);
    logger.debug(`trace name definition is `, [traceAxisDefinition]);
    const values = Object.values(this.chartDefinition.series)[0];
    logger.debug(`values are `, [values]);
    const traceData: TraceData[] = this.chartDefinition.abscissa
      .map((tuple, index) => {
        return {
          traceName: tuple[1],
          traceRawLabel: tuple[1],
          xLabel: tuple[0],
          yValue: values[index]
        };
      })
      .map((value: TraceData) => {
        const isNever = isDateChart && value.xLabel === null;
        const xLabel = isDateChart && !isNever ?
          this.convertDates(xAxisDefinition, [value.xLabel as number])[0]
          : xAxisConverter.convertAxisLabels(xAxisDefinition, [value.xLabel])[0];

        return {
          traceName: traceAxisConverter.convertAxisLabels(traceAxisDefinition, [value.traceName])[0],
          traceRawLabel: value.traceRawLabel,
          xLabel,
          yValue: value.yValue,
          isNever
        };
      });
    return traceData;
  }

  private getUniqXLabels(): string[] {
    const traceNames: string[] = this.chartDefinition.abscissa.map(tuple => tuple[0]);
    return _.uniq(traceNames);
  }

  protected getHandledType(): ChartType {
    return ChartType.TREND;
  }

  private generateDateChart() {
    const hasNeverValue = this.dateChartHasNeverValue();
    if (hasNeverValue) {
      this.generateDateChartWithNeverValue();
    } else {
      this.generateDefaultChart('date');
    }
  }

  private dateChartHasNeverValue() {
    const uniqXLabels = this.getUniqXLabels();
    return uniqXLabels[0] === null;
  }

  private generateDateChartWithNeverValue() {
    const traces: Dictionary<TraceData[]> = this.extractTraces();
    const neverData: Dictionary<TraceData[]> = {};
    Object.entries(traces).forEach(([key, traceData]) => {
      neverData[key] = [traceData.shift()];
    });
    const plotlyData = [
      ...this.buildPlotlyData(neverData, 'x', 'bar', false),
      ...this.buildPlotlyData(traces, 'x2')];

    logger.debug('Final plotlyData : ', [plotlyData]);
    const layout: Partial<Layout> = {
      title: this.chartTitle,
      showlegend: true,
      legend: {
        font: this.legendFont
      },
      yaxis: {
        title: {
          text: this.getMeasureText(),
          font: this.axisTitleFont
        },
        fixedrange: true,
        tickfont: this.legendFont,
      },
      xaxis: {
        domain: [0, 0.1],
        fixedrange: true,
        tickfont: this.legendFont,
      },
      xaxis2: {
        type: 'date',
        domain: [0.1, 1],
        title: {
          text: this.getAxisText(),
          font: this.axisTitleFont
        },
        fixedrange: true,
        tickfont: this.legendFont,
      }
    };

    const config: Partial<Plotly.Config> = this.getDefaultConfig();

    newPlot(this.chartContainer.nativeElement, plotlyData, layout, config);
  }
}

export interface TraceData {
  traceName: string;
  traceRawLabel: string;
  xLabel: string | number | Date;
  yValue: number;
  isNever?: boolean;
}

import {AxisLabelsConverter} from './axis-labels-converters';
import {AxisColumn, ChartDataType} from 'sqtm-core';
import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class InfoListItemConverter implements AxisLabelsConverter {

  constructor(private translateService: TranslateService) {
  }

  convertAxisLabels(axis: AxisColumn, rawLabels: any[]): string[] {
    return rawLabels.map(rawLabel => this.isSystemItem(rawLabel) ?
      this.translateService.instant(`sqtm-core.entity.${rawLabel}`) : rawLabel);
  }

  handleAxis(axis: AxisColumn): boolean {
    return axis.column.dataType === ChartDataType.INFO_LIST_ITEM;
  }

  isSystemItem(rawLabel: string) {
    return rawLabel.includes('requirement.category.') ||
      rawLabel.includes('test-case.nature.') ||
      rawLabel.includes('test-case.type.');
  }

}

import {EditableSelectFieldComponent, TextFieldComponent, ThirdPartyCredentialsFormComponent} from 'sqtm-core';
import {FormGroup} from '@angular/forms';
import {
  AuthProtocolFormComponent
} from '../../pages/administration/server-workspace/components/auth-protocol-form/auth-protocol-form.component';
import SpyObj = jasmine.SpyObj;

export function mockTextField(fieldName: string, formGroup: FormGroup): TextFieldComponent {
  const mock = jasmine.createSpyObj(['grabFocus', 'showClientSideError']);
  mock.formGroup = formGroup;
  mock.errors = [];
  mock.fieldName = fieldName;
  return mock as TextFieldComponent;
}

export function mockEditableSelectField(): SpyObj<EditableSelectFieldComponent> {
  return jasmine.createSpyObj<EditableSelectFieldComponent>([
    'enableEditMode',
    'disableEditMode',
    'cancel',
    'confirm',
    'beginAsync',
    'endAsync',
    'markForCheck',
  ]);
}

export function mockMouseEvent(): SpyObj<MouseEvent> {
  return jasmine.createSpyObj<MouseEvent>('MouseEvent', [
    'stopPropagation',
    'stopImmediatePropagation',
    'preventDefault',
  ]);
}

export function mockCredentialsForm(): SpyObj<ThirdPartyCredentialsFormComponent> {
  const mock = jasmine.createSpyObj<ThirdPartyCredentialsFormComponent>('ThirdPartyCredentialsFormComponent', [
    'handleSubmit',
    'endAsync',
  ]);

  mock.formGroup = jasmine.createSpyObj<FormGroup>('FormGroup', [
    'markAsPristine',
  ]);

  return mock;
}

export function mockAuthProtocolForm(): SpyObj<AuthProtocolFormComponent> {
  const mock = jasmine.createSpyObj<AuthProtocolFormComponent>('AuthProtocolFormComponent', [
    'confirmAuthProtocol',
    'handleServerError',
    'handleServerSuccess',
  ]);

  mock.formGroup = jasmine.createSpyObj<FormGroup>('FormGroup', [
    'markAsPristine',
  ]);

  return mock;
}

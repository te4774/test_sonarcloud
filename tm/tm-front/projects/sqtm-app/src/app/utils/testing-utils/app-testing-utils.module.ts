import {NgModule} from '@angular/core';
import {APP_BASE_HREF, CommonModule} from '@angular/common';
import {CapitalizeMockPipe, ConvertFileSizePipeMock, TranslateMockPipe} from './mocks.pipe';
import {IconDefinition} from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';
import {NZ_ICONS, NzIconModule} from 'ng-zorro-antd/icon';
import {
  BACKEND_CONTEXT_PATH,
  CORE_MODULE_CONFIGURATION,
  CORE_MODULE_USER_CONFIGURATION,
  RestService,
  SQTM_MAIN_APP_IDENTIFIER,
  sqtmConfigurationFactory
} from 'sqtm-core';
import {
  ExecutionRunnerOpenerService
} from '../../pages/execution/execution-runner/services/execution-runner-opener.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};

const navIcon: string[] = [
  'requirement-workspace',
  'test-case-workspace',
  'campaign-workspace',
  'custom-report-workspace',
  'automation-workspace',
  'bugtrackers',
  'project-filter',
  'help',
];

const adminIcon: string[] = [
  'automation',
  'bugtracker',
  'connection_history',
  'custom_field',
  'execution',
  'infolist',
  'information',
  'milestone',
  'plugin',
  'project',
  'requirements_link',
  'scm_server',
  'team',
  'template',
  'user',
  'external_link',
];

const genericIcon: string[] = [
  'delete',
  'copy',
  'plus',
  'paste',
  'more',
  'add',
  'unlink',
  'edit',
  'help',
  'read'
];

const customReportIcon: string[] = [
  'pie',
  'bar',
  'cumulative',
  'trend',
  'comparative'
];

const treeIcon: string[] = [
  'add',
];

const campaignIcon: string[] = [
  'play',
  'play-2',
];

const automationIcon: string[] = [
  'assign',
  'assign_to_me',
  'automated',
  'edit',
  'global_view',
  'help',
  'more',
  'ready_for_transmission',
  'to_be_validated',
  'transmit',
  'unassigned',
  'workflow',
];

function iconFactory() {
  const icons: IconDefinition[] = Object.keys(antDesignIcons).map(function (key) {
    const i = antDesignIcons[key];
    return i;
  });
  icons.push(...buildIconNamespace('sqtm-core-nav', navIcon));
  icons.push(...buildIconNamespace('sqtm-core-administration', adminIcon));
  icons.push(...buildIconNamespace('sqtm-core-generic', genericIcon));
  icons.push(...buildIconNamespace('sqtm-core-custom-report', customReportIcon));
  icons.push(...buildIconNamespace('sqtm-core-tree', treeIcon));
  icons.push(...buildIconNamespace('sqtm-core-campaign', campaignIcon));
  icons.push(...buildIconNamespace('sqtm-core-automation', automationIcon));
  return icons;
}

function buildIconNamespace(nameSpace: string, names: string[]): IconDefinition[] {
  return names.map(name => `${nameSpace}:${name}`).map(prefixedName => ({name: prefixedName, icon: '<svg></svg>'}));
}

// @dynamic
@NgModule({
  declarations: [CapitalizeMockPipe, TranslateMockPipe, ConvertFileSizePipeMock],
  imports: [
    CommonModule,
    HttpClientTestingModule
  ],
  providers: [
    {
      provide: APP_BASE_HREF,
      useValue: ''
    },
    {
      provide: BACKEND_CONTEXT_PATH,
      useValue: ''
    },
    {
      provide: CORE_MODULE_USER_CONFIGURATION,
      useValue: {pluginIdentifier: SQTM_MAIN_APP_IDENTIFIER}
    },
    {
      provide: CORE_MODULE_CONFIGURATION,
      useFactory: sqtmConfigurationFactory,
      deps: [CORE_MODULE_USER_CONFIGURATION]
    },
    {
      provide: NZ_ICONS,
      useFactory: iconFactory
    },
    ExecutionRunnerOpenerService
  ],
  exports: [TranslateMockPipe, CapitalizeMockPipe, ConvertFileSizePipeMock, NzIconModule]
})
export class AppTestingUtilsModule {
  constructor(private restService: RestService) {
    restService.backendRootUrl = 'backend';
    restService.backendContextPath = '';
  }
}

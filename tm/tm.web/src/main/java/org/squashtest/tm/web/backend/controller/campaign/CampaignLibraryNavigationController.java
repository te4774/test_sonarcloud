/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.campaign.export.CampaignExportCSVModel;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.exception.library.RightsUnsuficientsForOperationException;
import org.squashtest.tm.service.campaign.CampaignFinder;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.campaign.IterationModificationService;
import org.squashtest.tm.service.deletion.CampaignLibraryNodesToDelete;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.display.workspace.tree.MultipleHierarchyTreeBrowser;
import org.squashtest.tm.service.display.workspace.tree.TreeBrowser;
import org.squashtest.tm.service.display.workspace.tree.TreeNodeCollectorService;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;
import org.squashtest.tm.service.internal.display.grid.TreeRequest;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.web.backend.controller.RequestParams;
import org.squashtest.tm.web.backend.controller.form.model.CampaignFolderFormModel;
import org.squashtest.tm.web.backend.controller.form.model.CampaignFormModel;
import org.squashtest.tm.web.backend.controller.form.model.EntityFormModel;
import org.squashtest.tm.web.backend.controller.form.model.EntityFormModelValidator;
import org.squashtest.tm.web.backend.controller.form.model.IterationFormModel;
import org.squashtest.tm.web.backend.controller.form.model.NodeList;
import org.squashtest.tm.web.backend.controller.form.model.RefreshTreeNodeModel;
import org.squashtest.tm.web.backend.controller.form.model.TestSuiteFormModel;
import org.squashtest.tm.web.backend.controller.navigation.Messages;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping(path = "backend/campaign-tree")
public class CampaignLibraryNavigationController {

	public static final Logger LOGGER = LoggerFactory.getLogger(CampaignLibraryNavigationController.class);

	private static final String ADD_CAMPAIGN = "add-campaign";
	private static final String ADD_ITERATION = "add-iteration";
	private static final String ADD_CAMPAIGN_FOLDER = "add-campaign-folder";
	private static final String REMOVE_FROM_ITER = "remove_from_iter";

	private final TreeBrowser treeBrowser;
	private ActiveMilestoneHolder activeMilestoneHolder;
	private final CampaignLibraryNavigationService campaignLibraryNavigationService;
	private final TreeNodeCollectorService treeNodeCollectorService;
	private final IterationModificationService iterationModificationService;
	private final MessageSource messageSource;
	private final CampaignFinder campaignFinder;

	public CampaignLibraryNavigationController(MultipleHierarchyTreeBrowser treeBrowser,
											   ActiveMilestoneHolder activeMilestoneHolder,
											   CampaignLibraryNavigationService campaignLibraryNavigationService,
											   TreeNodeCollectorService treeNodeCollectorService,
											   IterationModificationService iterationModificationService,
											   MessageSource messageSource,
											   CampaignFinder campaignFinder) {
		this.treeBrowser = treeBrowser;
		this.activeMilestoneHolder = activeMilestoneHolder;
		this.campaignLibraryNavigationService = campaignLibraryNavigationService;
		this.treeNodeCollectorService = treeNodeCollectorService;
		this.iterationModificationService = iterationModificationService;
		this.messageSource = messageSource;
		this.campaignFinder =campaignFinder;
	}

	@RequestMapping(method = RequestMethod.POST)
	public TreeGridResponse getInitialRows(@RequestBody TreeRequest treeRequest) {
		return treeBrowser.getInitialTree(NodeWorkspace.CAMPAIGN,
			NodeReference.fromNodeIds(treeRequest.getOpenedNodes()),
			NodeReference.fromNodeIds(treeRequest.getSelectedNodes()));
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{ids}/content")
	public TreeGridResponse getChildren(@PathVariable List<String> ids) {
		Set<NodeReference> nodeReference = NodeReference.fromNodeIds(ids);
		return treeBrowser.findSubHierarchy(nodeReference, new HashSet<>(nodeReference));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/refresh")
	public TreeGridResponse refreshNodes(@RequestBody RefreshTreeNodeModel refreshTreeNodeModel) {
		return treeBrowser.findSubHierarchy(NodeReference.fromNodeIds(refreshTreeNodeModel.getNodeIds()),
			new HashSet<>(refreshTreeNodeModel.getNodeList().getNodeReferences()));
	}

	@ResponseBody
	@RequestMapping(value = "new-campaign", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public DataRow addNewCampaign(@RequestBody CampaignFormModel campaignFormModel) throws BindException {
		validateFormModel(campaignFormModel, ADD_CAMPAIGN);

		Campaign campaign = campaignFormModel.getCampaign();
		Map<Long, RawValue> customFieldValues = campaignFormModel.getCufs();

		String serializedParentEntityReference = campaignFormModel.getParentEntityReference();
		EntityReference parentEntityReference = EntityReference.fromNodeId(serializedParentEntityReference);
		switch (parentEntityReference.getType()) {
			case CAMPAIGN_LIBRARY:
				campaignLibraryNavigationService.addCampaignToCampaignLibrary(parentEntityReference.getId(), campaign, customFieldValues);
				break;
			case CAMPAIGN_FOLDER:
				campaignLibraryNavigationService.addCampaignToCampaignFolder(parentEntityReference.getId(), campaign, customFieldValues);
				break;
			default:
				throw new IllegalArgumentException("This entity type is not handled " + serializedParentEntityReference);
		}
		return treeNodeCollectorService.collectNode(NodeType.CAMPAIGN, campaign);
	}

	@ResponseBody
	@RequestMapping(value = "campaign/{campaignId}/new-iteration", method = RequestMethod.POST,
		consumes = "application/json", produces = "application/json")
	public DataRow addNewIterationToCampaign(@PathVariable long campaignId,
											 @RequestBody IterationFormModel iterationForm)
		throws BindException {
		validateFormModel(iterationForm, ADD_ITERATION);

		Iteration newIteration = iterationForm.getIteration();
		Map<Long, RawValue> customFieldValues = iterationForm.getCufs();
		boolean copyTestPlan = iterationForm.isCopyTestPlan();

		campaignLibraryNavigationService.addIterationToCampaign(newIteration, campaignId,
			copyTestPlan, customFieldValues);

		return treeNodeCollectorService.collectNode(NodeType.ITERATION, newIteration);
	}

	@ResponseBody
	@RequestMapping(value = "new-folder", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public DataRow addNewFolder(@RequestBody CampaignFolderFormModel folderModel) throws BindException {

		validateCampaignFolderFormModel(folderModel);

		CampaignFolder campaignFolder = folderModel.getCampaignFolder();

		Map<Long, RawValue> customFieldValues = folderModel.getCufs();

		String serializedParentEntityReference = folderModel.getParentEntityReference();
		EntityReference parentEntityReference = EntityReference.fromNodeId(serializedParentEntityReference);
		switch (parentEntityReference.getType()) {
			case CAMPAIGN_LIBRARY:
				campaignLibraryNavigationService.addFolderToLibrary(parentEntityReference.getId(), campaignFolder, customFieldValues);
				break;
			case CAMPAIGN_FOLDER:
				campaignLibraryNavigationService.addFolderToFolder(parentEntityReference.getId(), campaignFolder, customFieldValues);
				break;
			default:
				throw new IllegalArgumentException("This entity type is not handled " + serializedParentEntityReference);
		}
		return treeNodeCollectorService.collectNode(NodeType.CAMPAIGN_FOLDER, campaignFolder);
	}

	@ResponseBody
	@PostMapping(value = "new-test-suite")
	public DataRow addTestSuite(@RequestBody TestSuiteFormModel suiteFormModel) {

		TestSuite suite = suiteFormModel.getTestSuite();
		Map<Long, RawValue> customFieldValues = suiteFormModel.getCufs();
		String serializedParentEntityReference = suiteFormModel.getParentEntityReference();
		EntityReference parentEntityReference = EntityReference.fromNodeId(serializedParentEntityReference);
		campaignLibraryNavigationService.addTestSuiteToIteration(parentEntityReference.getId(), suite, customFieldValues);
		return treeNodeCollectorService.collectNode(NodeType.TEST_SUITE, suite);
	}

	@ResponseBody
	@RequestMapping(value = "/{destinationId}/content/paste", method = RequestMethod.POST)
	public void copyNodes(@RequestBody() NodeList nodeList, @PathVariable("destinationId") String destinationId) {
		NodeReference nodeReference = NodeReference.fromNodeId(destinationId);
		List<Long> copiedNodeIds = nodeList.getIds();

		try {
			switch (nodeReference.getNodeType()) {
				case CAMPAIGN_FOLDER:
					campaignLibraryNavigationService.copyNodesToFolder(nodeReference.getId(), copiedNodeIds.toArray(new Long[0]));
					break;
				case CAMPAIGN_LIBRARY:
					campaignLibraryNavigationService.copyNodesToLibrary(nodeReference.getId(), copiedNodeIds.toArray(new Long[0]));
					break;
				case CAMPAIGN:
					campaignLibraryNavigationService.copyIterationsToCampaign(nodeReference.getId(), copiedNodeIds.toArray(new Long[0]));
					break;
				case ITERATION:
					iterationModificationService.copyPasteTestSuitesToIteration(copiedNodeIds.toArray(new Long[0]), nodeReference.getId());
					break;
				default:
					throw new IllegalArgumentException("copy nodes : specified destination type doesn't exists : "
						+ nodeReference.getNodeType());
			}
		} catch (AccessDeniedException ade) {
			throw new RightsUnsuficientsForOperationException(ade);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/{destinationRef}/content/move", method = RequestMethod.POST)
	public void moveNodes(@RequestBody() NodeList nodeList, @PathVariable("destinationRef") String destinationRef) {
		NodeReference nodeReference = NodeReference.fromNodeId(destinationRef);
		Long destinationId = nodeReference.getId();
		NodeType destinationType = nodeReference.getNodeType();
		Long[] movedNodeIds = nodeList.getIds().toArray(new Long[0]);

		try {
			switch (destinationType) {
				case CAMPAIGN_LIBRARY:
					campaignLibraryNavigationService.moveNodesToLibrary(destinationId, movedNodeIds);
					break;
				case CAMPAIGN_FOLDER:
					campaignLibraryNavigationService.moveNodesToFolder(destinationId, movedNodeIds);
					break;
				default:
					throw new IllegalArgumentException("move nodes : specified destination type doesn't exists : "
						+ destinationType);
			}
		} catch (AccessDeniedException ade) {
			throw new RightsUnsuficientsForOperationException(ade);
		}

	}


	@ResponseBody
	@RequestMapping(value = "/{destinationRef}/content/move/{position}", method = RequestMethod.POST)
	public void moveNodesAtPosition(@RequestBody() NodeList nodeList, @PathVariable("destinationRef") String destinationRef, @PathVariable("position") int position) {
		NodeReference nodeReference = NodeReference.fromNodeId(destinationRef);
		Long destinationId = nodeReference.getId();
		NodeType destinationType = nodeReference.getNodeType();
		Long[] movedNodeIds = nodeList.getIds().toArray(new Long[0]);

		try {
			switch (destinationType) {
				case CAMPAIGN_LIBRARY:
					campaignLibraryNavigationService.moveNodesToLibrary(destinationId, movedNodeIds, position);
					break;
				case CAMPAIGN_FOLDER:
					campaignLibraryNavigationService.moveNodesToFolder(destinationId, movedNodeIds, position);
					break;
				case CAMPAIGN:
					campaignLibraryNavigationService.moveIterationsWithinCampaign(destinationId, movedNodeIds, position);
					break;
				case ITERATION:
					iterationModificationService.changeTestSuitePosition(destinationId, position, nodeList.getIds());
					break;
				default:
					throw new IllegalArgumentException("move nodes : specified destination type doesn't exists : "
						+ destinationType);
			}
		} catch (AccessDeniedException ade) {
			throw new RightsUnsuficientsForOperationException(ade);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/deletion-simulation/{nodeIds}", method = RequestMethod.GET)
	public Messages simulateNodesDeletion(@PathVariable(RequestParams.NODE_IDS) List<String> nodeIds,
										  Locale locale) {
		CampaignLibraryNodesToDelete deletionNodes = CampaignLibraryNodesToDelete.from(nodeIds);
		List<Long> folderAndCampaignIds = deletionNodes.getFolderAndCampaignNodeIds();

		Messages messages = new Messages();

		if(!folderAndCampaignIds.isEmpty()) {
			List<SuppressionPreviewReport> reportList = campaignLibraryNavigationService.simulateDeletion(folderAndCampaignIds);
			for (SuppressionPreviewReport report : reportList) {
				messages.addMessage(report.toString(messageSource, locale));
			}
		}

		List<SuppressionPreviewReport> reportIterationList = campaignLibraryNavigationService
			.simulateIterationDeletion(deletionNodes.getIterationIds());
		List<SuppressionPreviewReport> reportSuiteList = campaignLibraryNavigationService.simulateSuiteDeletion(deletionNodes.getSuiteIds());


		for (SuppressionPreviewReport report : reportIterationList) {
			messages.addMessage(report.toString(messageSource, locale));
		}
		for (SuppressionPreviewReport report : reportSuiteList) {
			messages.addMessage(report.toString(messageSource, locale));
		}
		return messages;
	}

	@ResponseBody
	@RequestMapping(value = "/{nodeIds}", params = {REMOVE_FROM_ITER}, method = RequestMethod.DELETE)
	public OperationReport confirmNodeDeletions(@PathVariable(RequestParams.NODE_IDS) List<String> nodeIds,
												@RequestParam(REMOVE_FROM_ITER) boolean removeFromIter) {

		CampaignLibraryNodesToDelete deletionNodes = CampaignLibraryNodesToDelete.from(nodeIds);
		return campaignLibraryNavigationService.deleteNodes(deletionNodes, removeFromIter);
	}

	@PostMapping(value = "campaign/{campaignId}/new-iteration-with-items")
	@ResponseBody
	public void addIterationToCampaignWithItems(@PathVariable long campaignId, @RequestBody IterationCreation patch) {

		Iteration iteration = patch.getIteration();
		Map<Long, RawValue> cufs = new HashMap<>();
		this.campaignLibraryNavigationService.addIterationToCampaignWithItems(iteration, campaignId, false, cufs, patch.getItemTestPlanIds());
	}

	@ResponseBody
	@RequestMapping(value = "/export-campaign/{campaignId}", method = RequestMethod.GET, params = "export=csv")
	public FileSystemResource exportCampaign(@PathVariable(RequestParams.CAMPAIGN_ID) long campaignId,
											 @RequestParam(value = "exportType", defaultValue = "S") String exportType, HttpServletResponse response) {

		Campaign campaign = campaignFinder.findById(campaignId);
		CampaignExportCSVModel model = campaignLibraryNavigationService.exportCampaignToCSV(campaignId, exportType);

		// prepare the response
		response.setContentType("application/octet-stream");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");

		response.setHeader("Content-Disposition", "attachment; filename=" + "EXPORT_CPG_" + exportType + "_"
			+ campaign.getName().replace(" ", "_") + "_" + sdf.format(new Date()) + ".csv");

		File exported = exportToFile(model);
		return new FileSystemResource(exported);
	}

	private void validateCampaignFolderFormModel(CampaignFolderFormModel folderModel) throws BindException {
		BindingResult validation = new BeanPropertyBindingResult(folderModel, ADD_CAMPAIGN_FOLDER);
		EntityFormModelValidator entityFormModelValidator = new EntityFormModelValidator();
		entityFormModelValidator.validate(folderModel, validation);

		if (validation.hasErrors()) {
			throw new BindException(validation);
		}
	}

	private void validateFormModel(EntityFormModel formModel, String objectName) throws BindException {
		BindingResult validation = new BeanPropertyBindingResult(formModel, objectName);
		EntityFormModelValidator validator = new EntityFormModelValidator();
		validator.validate(formModel, validation);

		if (validation.hasErrors()) {
			throw new BindException(validation);
		}
	}

	private File exportToFile(CampaignExportCSVModel model) {

		File file;
		PrintWriter writer = null;
		try {
			file = File.createTempFile("export-requirement", "tmp");
			file.deleteOnExit();

			writer = new PrintWriter(file);

			// print header
			CampaignExportCSVModel.Row header = model.getHeader();
			writer.write(header.toString() + "\n");

			// print the rest
			Iterator<CampaignExportCSVModel.Row> iterator = model.dataIterator();
			while (iterator.hasNext()) {
				CampaignExportCSVModel.Row datarow = iterator.next();
				String cleanRowValue = HTMLCleanupUtils.htmlToTrimmedText(datarow.toString()).replaceAll("\\n", "")
					.replaceAll("\\r", "");
				writer.write(cleanRowValue + "\n");
			}

			writer.close();

			return file;
		} catch (IOException e) {
			LOGGER.error("campaign export : I/O failure while creating the temporary file : " + e.getMessage());
			throw new RuntimeException(e);
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	static class IterationCreation {
		private String name;
		private String description;
		private List<Long> itemTestPlanIds = new ArrayList<>();

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public List<Long> getItemTestPlanIds() {
			return itemTestPlanIds;
		}

		public void setItemTestPlanIds(List<Long> itemTestPlanIds) {
			this.itemTestPlanIds = itemTestPlanIds;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public Iteration getIteration() {
			Iteration iteration = new Iteration();
			iteration.setName(getName());
			iteration.setDescription(getDescription());
			return iteration;
		}
	}
}

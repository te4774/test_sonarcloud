/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.environmentVariable;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableValueService;
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/backend/bound-environment-variables")
public class BoundEnvironmentVariableController {

	private final EnvironmentVariableBindingService environmentVariableBindingService;

	private final EnvironmentVariableValueService environmentVariableValueService;

	public BoundEnvironmentVariableController(EnvironmentVariableBindingService environmentVariableBindingService, EnvironmentVariableValueService environmentVariableValueService) {
		this.environmentVariableBindingService = environmentVariableBindingService;
		this.environmentVariableValueService = environmentVariableValueService;
	}

	@ResponseBody
	@GetMapping("{serverId}/test-automation-server")
	public Map<String, List<BoundEnvironmentVariableDto>> getBoundEnvironmentVariableFromServer(@PathVariable Long serverId) {
		return Collections.singletonMap("boundEnvironmentVariables",
			environmentVariableBindingService.getAllDefaultBoundEnvironmentVariableDto(serverId));
	}

	@ResponseBody
	@GetMapping("{projectId}/project")
	public Map<String, List<BoundEnvironmentVariableDto>> getBoundEnvironmentVariablesFromProject(@PathVariable Long projectId) {
		return Collections.singletonMap("boundEnvironmentVariables",
			environmentVariableBindingService.getAllBoundEnvironmentVariableDtoByProjectId(projectId));
	}

	@ResponseBody
	@PostMapping("{projectId}/project/{environmentVariableId}/value")
	public void editEnvironmentVariableValueFromProject(@PathVariable Long projectId, @PathVariable Long environmentVariableId,
														@RequestBody EnvironmentVariableProjectValuePatch patch) {
		environmentVariableValueService.editEnvironmentVariableValueFromProject(projectId,
			environmentVariableId, patch.getValue());
	}

	@ResponseBody
	@PostMapping(value = "/{projectId}/project/{environmentVariableId}/reset")
	public Map<String, String> resetEnvironmentVariableDefaultValue(@PathVariable Long projectId,
																	@PathVariable Long environmentVariableId,
																	@RequestBody EnvironmentVariableProjectValuePatch patch) {
		String defaultValue = environmentVariableValueService.resetDefaultValue(projectId, environmentVariableId, patch.getEntityType());
		return Collections.singletonMap("defaultValue", defaultValue);
	}

	public static class EnvironmentVariableProjectValuePatch {

		String value;

		String entityType;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getEntityType() {
			return entityType;
		}

		public void setEntityType(String entityType) {
			this.entityType = entityType;
		}
	}

}



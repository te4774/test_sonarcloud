/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.filter.xss;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;
import org.jsoup.safety.Whitelist;
import org.owasp.encoder.esapi.ESAPIEncoder;
import org.owasp.esapi.errors.IntrusionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;
import java.util.Objects;

public class XSSRequestWrapper extends HttpServletRequestWrapper {

	private final String HEADER_REFERER = "referer";

	/**
	 * Constructs a request object wrapping the given request.
	 *
	 * @param request the {@link HttpServletRequest} to be wrapped.
	 * @throws IllegalArgumentException if the request is null
	 */
	public XSSRequestWrapper(HttpServletRequest request) {
		super(request);
	}

	@Override
	public Map<String, String[]> getParameterMap() {
		Map<String, String[]> parameterMap = super.getParameterMap();
		if (Objects.isNull(parameterMap)) {
			return null;
		}
		parameterMap.forEach(this::stripXSS);
		return parameterMap;
	}

	@Override
	public String[] getParameterValues(String name) {
		String[] parameterValues = super.getParameterValues(name);
		if (Objects.isNull(parameterValues)) {
			return null;
		}
		stripXSS(name, parameterValues);
		return parameterValues;
	}

	@Override
	public String getParameter(String name) {
		String parameterValue = super.getParameter(name);
		stripXSS(name, parameterValue);
		return parameterValue;
	}

	@Override
	public Enumeration<String> getHeaders(String name) {
		Enumeration<String> headers = super.getHeaders(name);
		while (headers.hasMoreElements()) {
			String header = headers.nextElement();
			String[] tokens = header.split(",");
			for (String token : tokens) {
				stripXSS(name, token);
			}
		}
		return super.getHeaders(name);
	}

	@Override
	public String getHeader(String name) {
		String header = super.getHeader(name);
		stripXSS(name, header);
		return header;
	}

	private void stripXSS(String parameterName, String[] values) {
		Arrays.stream(values)
			.forEach(value -> stripXSS(parameterName, value));
	}

	private void stripXSS(String parameterName, String value) {
		// The 'Referer' header can trigger input validation errors (IntrusionException) because its value may contain
		// JSON with escaped characters that are then URL-encoded. This causes the ESAPIEncoder `canonicalize` to detect
		// a mixed encoding in strict mode. As a workaround, we allow mixed encoding for the 'Referer' parameter.
		if (parameterName.equals(HEADER_REFERER)) {
			doStripXSS(value, StripOptions.ALLOW_MIXED_ENCODING);
		} else {
			doStripXSS(value, StripOptions.STRICT);
		}
	}

	private void doStripXSS(String value, StripOptions options) {
		if (value != null) {
			// canonicalize mean that we decode the value if encoded, escaped or obfuscated by attacker to escape filters
			// but having the final client run the malicious code
			String escapedValue = ESAPIEncoder.getInstance()
				.canonicalize(value, options.restrictMultiple, options.restrictMixed)
				.replaceAll("\0", "")// replacing null characters by empty string
				.trim();
			Document.OutputSettings outputSettings = new Document.OutputSettings();
			outputSettings.escapeMode(Entities.EscapeMode.xhtml);
			boolean cleanedValue = Jsoup.isValid(escapedValue, Whitelist.none());// once full decode is done give it to jsoup for actual cleaning
			if (!cleanedValue) {
				String message = String.format("Xss protection activated. Escaped value %s. Raw value was %s", escapedValue, value);
				throw new IntrusionException("", message);
			}
		}
	}

	private enum StripOptions {
		STRICT(true, true),
		ALLOW_MIXED_ENCODING(true, false);

		final boolean restrictMultiple;
		final boolean restrictMixed;

		StripOptions(boolean restrictMultiple, boolean restrictMixed) {
			this.restrictMultiple = restrictMultiple;
			this.restrictMixed = restrictMixed;
		}
	}
}

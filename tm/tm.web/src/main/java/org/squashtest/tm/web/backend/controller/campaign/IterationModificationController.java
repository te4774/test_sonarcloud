/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationStatus;
import org.squashtest.tm.service.campaign.IterationModificationService;

import java.util.Date;

@RestController
@RequestMapping("backend/iteration/{iterationId}")
public class IterationModificationController {

	private final IterationModificationService iterationModificationService;

	public IterationModificationController(IterationModificationService iterationModificationService) {
		this.iterationModificationService = iterationModificationService;
	}

	@ResponseBody
	@PostMapping(value = "/name")
	public void rename(@PathVariable Long iterationId, @RequestBody IterationPatch patch) {
		iterationModificationService.rename(iterationId, patch.getName());
	}

	@ResponseBody
	@PostMapping(value = "/reference")
	public void changeReference(@PathVariable Long iterationId, @RequestBody IterationPatch patch) {
		iterationModificationService.changeReference(iterationId, patch.getReference());
	}

	@ResponseBody
	@PostMapping(value = "/description")
	public void changeDescription(@PathVariable Long iterationId, @RequestBody IterationPatch patch) {
		iterationModificationService.changeDescription(iterationId, patch.getDescription());
	}

	@ResponseBody
	@PostMapping(value = "/iteration-status")
	public void changeCampaignStatus(@PathVariable Long iterationId, @RequestBody IterationPatch patch) {
		iterationModificationService.changeStatus(iterationId, IterationStatus.valueOf(patch.getIterationStatus()));
	}

	@ResponseBody
	@PostMapping(value = "/scheduled-start-date")
	public void setScheduledStart(@PathVariable long iterationId,
								  @RequestBody IterationPatch patch) {
		iterationModificationService.changeScheduledStartDate(iterationId, patch.getScheduledStartDate());
	}

	@PostMapping(value = "/scheduled-end-date")
	@ResponseBody
	public void setScheduledEnd(@PathVariable long iterationId,
								@RequestBody IterationPatch patch) {
		iterationModificationService.changeScheduledEndDate(iterationId, patch.getScheduledEndDate());
	}

	/**
	 * the next functions may receive null arguments : empty string
	 **/

	@PostMapping(value = "/actual-start-date")
	@ResponseBody
	public void setActualStart(@PathVariable long iterationId,
							   @RequestBody IterationPatch patch) {
		iterationModificationService.changeActualStartDate(iterationId, patch.getActualStartDate());
	}

	@PostMapping(value = "/actual-end-date")
	@ResponseBody
	public void setActualEnd(@PathVariable long iterationId,
							 @RequestBody IterationPatch patch) {

		iterationModificationService.changeActualEndDate(iterationId, patch.getActualEndDate());
	}

	@PostMapping(value = "/actual-start-auto")
	@ResponseBody
	public Date setActualStartAuto(@PathVariable long iterationId,
								   @RequestBody IterationPatch patch) {
		iterationModificationService.changeActualStartAuto(iterationId, patch.getActualStartAuto());
		Iteration iteration = iterationModificationService.findById(iterationId);
		return iteration.getActualStartDate();
	}

	@PostMapping(value = "/actual-end-auto")
	@ResponseBody
	public Date setActualEndAuto(@PathVariable long iterationId,
								 @RequestBody IterationPatch patch) {
		iterationModificationService.changeActualEndAuto(iterationId, patch.getActualEndAuto());
		Iteration iteration = iterationModificationService.findById(iterationId);
		return iteration.getActualEndDate();
	}


	static class IterationPatch {
		private String name;
		private String reference;
		private String description;
		private String iterationStatus;
		private Date scheduledEndDate;
		private Date scheduledStartDate;
		private Date actualEndDate;
		private Date actualStartDate;
		private Boolean actualEndAuto;
		private Boolean actualStartAuto;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getReference() {
			return reference;
		}

		public void setReference(String reference) {
			this.reference = reference;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getIterationStatus() {
			return iterationStatus;
		}

		public void setIterationStatus(String iterationStatus) {
			this.iterationStatus = iterationStatus;
		}

		public Date getScheduledEndDate() {
			return scheduledEndDate;
		}

		public void setScheduledEndDate(Date scheduledEndDate) {
			this.scheduledEndDate = scheduledEndDate;
		}

		public Date getScheduledStartDate() {
			return scheduledStartDate;
		}

		public void setScheduledStartDate(Date scheduledStartDate) {
			this.scheduledStartDate = scheduledStartDate;
		}

		public Date getActualEndDate() {
			return actualEndDate;
		}

		public void setActualEndDate(Date actualEndDate) {
			this.actualEndDate = actualEndDate;
		}

		public Date getActualStartDate() {
			return actualStartDate;
		}

		public void setActualStartDate(Date actualStartDate) {
			this.actualStartDate = actualStartDate;
		}

		public Boolean getActualEndAuto() {
			return actualEndAuto;
		}

		public void setActualEndAuto(Boolean actualEndAuto) {
			this.actualEndAuto = actualEndAuto;
		}

		public Boolean getActualStartAuto() {
			return actualStartAuto;
		}

		public void setActualStartAuto(Boolean actualStartAuto) {
			this.actualStartAuto = actualStartAuto;
		}
	}

}

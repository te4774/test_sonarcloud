/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.export;

import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.domain.testcase.ExportTestCaseData;
import org.squashtest.tm.domain.testcase.ExportTestStepData;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.web.backend.controller.RequestParams;
import org.squashtest.tm.web.backend.report.service.JasperReportsService;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping(value = "backend/test-case/export")
public class TestCaseExportController {

	private static final String CALLS = "calls";
	private static final String JASPER_EXPORT_FILE = "WEB-INF/reports/test-case-export.jasper";

	private final MessageSource messageSource;

	private final JasperReportsService jrServices;

	private final TestCaseLibraryNavigationService testCaseLibraryNavigationService;

	public TestCaseExportController(MessageSource messageSource,
									JasperReportsService jrServices,
									TestCaseLibraryNavigationService testCaseLibraryNavigationService) {
		this.messageSource = messageSource;
		this.jrServices = jrServices;
		this.testCaseLibraryNavigationService = testCaseLibraryNavigationService;
	}

	@RequestMapping(value = "/content/csv", produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM, method = RequestMethod.GET, params = {
		RequestParams.FILENAME, RequestParams.LIBRARIES, RequestParams.NODES, CALLS, RequestParams.RTEFORMAT})
	@ResponseBody
	public void exportAsCsv(Locale locale, @RequestParam(RequestParams.FILENAME) String filename,
							@RequestParam(RequestParams.LIBRARIES) List<Long> libraryIds, @RequestParam(RequestParams.NODES) List<Long> nodeIds,
							@RequestParam(CALLS) Boolean includeCalledTests, @RequestParam(RequestParams.RTEFORMAT) Boolean keepRteFormat,
							HttpServletResponse response) {

		response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
		response.setHeader(RequestParams.CONTENT_DISPOSITION, RequestParams.ATTACHMENT_FILENAME + filename + ".xls");

		List<ExportTestCaseData> dataSource = testCaseLibraryNavigationService.findTestCasesToExport(libraryIds,
			nodeIds, includeCalledTests);

		if (!keepRteFormat) {
			escapePrerequisiteAndSteps(dataSource);
		}
		convertHtmlSpecialCharactersToUnicode(dataSource);

		printExport(dataSource, filename, JASPER_EXPORT_FILE, response, locale, "csv", keepRteFormat);
	}

	@RequestMapping(value = "/content/xls", produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM, method = RequestMethod.GET, params = {
		RequestParams.FILENAME, RequestParams.LIBRARIES, RequestParams.NODES, CALLS, RequestParams.RTEFORMAT})
	@ResponseBody
	public FileSystemResource exportAsExcel(@RequestParam(RequestParams.FILENAME) String filename,
											@RequestParam(RequestParams.LIBRARIES) List<Long> libraryIds, @RequestParam(RequestParams.NODES) List<Long> nodeIds,
											@RequestParam(CALLS) Boolean includeCalledTests, @RequestParam(RequestParams.RTEFORMAT) Boolean keepRteFormat,
											HttpServletResponse response) {

		response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
		response.setHeader(RequestParams.CONTENT_DISPOSITION, RequestParams.ATTACHMENT_FILENAME + filename + ".xls");

		File export = testCaseLibraryNavigationService.exportTestCaseAsExcel(libraryIds, nodeIds, includeCalledTests,
			keepRteFormat, messageSource);
		return new FileSystemResource(export);

	}

	@ResponseBody
	@RequestMapping(
		value = "/content/keyword-scripts",
		method = RequestMethod.GET,
		produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM,
		params = { RequestParams.FILENAME, RequestParams.LIBRARIES, RequestParams.NODES })
	public FileSystemResource exportKeywordScripts(
		@RequestParam(RequestParams.FILENAME) String filename,
		@RequestParam(RequestParams.LIBRARIES) List<Long> libraryIds,
		@RequestParam(RequestParams.NODES) List<Long> nodeIds,
		HttpServletResponse response) {
		response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
		response.setHeader(RequestParams.CONTENT_DISPOSITION, RequestParams.ATTACHMENT_FILENAME + filename + ".zip");
		File file = testCaseLibraryNavigationService.exportKeywordTestCaseAsScriptFiles(libraryIds, nodeIds, messageSource);
		return new FileSystemResource(file);
	}

	@ResponseBody
	@RequestMapping(
		value = "/content/features",
		method = RequestMethod.GET,
		produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM,
		params = { RequestParams.FILENAME, RequestParams.LIBRARIES, RequestParams.NODES })
	public FileSystemResource exportGherkinFeatures(
		@RequestParam(RequestParams.FILENAME) String filename,
		@RequestParam(RequestParams.LIBRARIES) List<Long> libraryIds,
		@RequestParam(RequestParams.NODES) List<Long> nodeIds,
		HttpServletResponse response) {
		response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
		response.setHeader(RequestParams.CONTENT_DISPOSITION, RequestParams.ATTACHMENT_FILENAME + filename + ".zip");
		File file = testCaseLibraryNavigationService.exportGherkinTestCaseAsFeatureFiles(libraryIds, nodeIds, messageSource);
		return new FileSystemResource(file);
	}

	@RequestMapping(value = "/searchExports", produces = RequestParams.APPLICATION_SLASH_OCTET_STREAM, method = RequestMethod.GET, params = {
		RequestParams.FILENAME, RequestParams.NODES, CALLS, RequestParams.RTEFORMAT})
	@ResponseBody
	public FileSystemResource searchExportAsExcel(@RequestParam(RequestParams.FILENAME) String filename,
												  @RequestParam(RequestParams.NODES) List<Long> nodeIds,
												  @RequestParam(CALLS) Boolean includeCalledTests,
												  @RequestParam(RequestParams.TYPE) String type,
												  @RequestParam(RequestParams.RTEFORMAT) Boolean keepRteFormat,
												  HttpServletResponse response) throws FileNotFoundException {

		response.setContentType(RequestParams.APPLICATION_SLASH_OCTET_STREAM);
		response.setHeader(RequestParams.CONTENT_DISPOSITION, RequestParams.ATTACHMENT_FILENAME + filename + ".xls");

		File export = testCaseLibraryNavigationService.searchExportTestCaseAsExcel(nodeIds, includeCalledTests,
			keepRteFormat, messageSource, type);
		return new FileSystemResource(export);

	}

	protected void printExport(List<ExportTestCaseData> dataSource, String filename2, String jasperExportFile,
							   HttpServletResponse response, Locale locale, String string, Boolean keepRteFormat) {
		ExportUtil.printExport(dataSource, filename2, jasperExportFile, response, locale, string, keepRteFormat,
			new HashMap<>(), messageSource, jrServices);

	}

	private void escapePrerequisiteAndSteps(List<ExportTestCaseData> dataSource) {
		for (ExportTestCaseData data : dataSource) {
			// escape prerequisite
			String htmlPrerequisite = data.getPrerequisite();
			String prerequisite = HTMLCleanupUtils.htmlToTrimmedText(htmlPrerequisite);
			data.setPrerequisite(prerequisite);

			// escape first step
			String htmlFirstAction = data.getFirstAction();
			String firstAction = HTMLCleanupUtils.htmlToTrimmedText(htmlFirstAction);
			data.setFirstAction(firstAction);

			String htmlFirstResult = data.getFirstExpectedResult();
			String firstResult = HTMLCleanupUtils.htmlToTrimmedText(htmlFirstResult);
			data.setFirstExpectedResult(firstResult);

			// escape other steps
			for (ExportTestStepData step : data.getSteps()) {
				String htmlAction = step.getAction();
				String action = HTMLCleanupUtils.htmlToTrimmedText(htmlAction);
				step.setAction(action);

				String htmlExpectedResult = step.getExpectedResult();
				String expectedResult = HTMLCleanupUtils.htmlToTrimmedText(htmlExpectedResult);
				step.setExpectedResult(expectedResult);
			}

		}
	}

	private void convertHtmlSpecialCharactersToUnicode(List<ExportTestCaseData> dataSource) {
		for (ExportTestCaseData data : dataSource) {
			data.setDescription(HtmlUtils.htmlUnescape(data.getDescription()));
			data.setPrerequisite(HtmlUtils.htmlUnescape(data.getPrerequisite()));
			data.setFirstAction(HtmlUtils.htmlUnescape(data.getFirstAction()));
			data.setFirstExpectedResult(HtmlUtils.htmlUnescape(data.getFirstExpectedResult()));
			for (ExportTestStepData step : data.getSteps()) {
				step.setAction(HtmlUtils.htmlUnescape(step.getAction()));
				step.setExpectedResult(HtmlUtils.htmlUnescape(step.getExpectedResult()));
			}
		}
	}
}

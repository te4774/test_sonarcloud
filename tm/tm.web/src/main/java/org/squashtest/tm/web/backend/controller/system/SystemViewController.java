/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.system;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.actionword.ActionWordLibraryNodeService;
import org.squashtest.tm.service.feature.FeatureManager;
import org.squashtest.tm.service.internal.display.dto.SystemViewDto;
import org.squashtest.tm.service.system.SystemAdministrationService;
import org.squashtest.tm.service.user.UserManagerService;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/backend/system-view")
public class SystemViewController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystemViewController.class);

    private final SystemAdministrationService systemAdministrationService;
    private final FeatureManager featureManager;
    private final UserManagerService userManagerService;
    private final ActionWordLibraryNodeService actionWordLibraryNodeService;
    private final Environment environment;

    @Value("${logging.dir}")
    private String loggingPath;

    @Value("${info.app.version}")
    private String appVersion;

    @Value("${squashtm.stack.trace.control.panel.visible:false}")
    private Boolean stackTracePanelIsVisible;

    SystemViewController(SystemAdministrationService administrationService,
                         FeatureManager featureManager,
                         UserManagerService userManagerService,
                         Environment environment,
						 ActionWordLibraryNodeService actionWordLibraryNodeService) {
        this.systemAdministrationService = administrationService;
        this.featureManager = featureManager;
        this.userManagerService = userManagerService;
        this.environment = environment;
        this.actionWordLibraryNodeService = actionWordLibraryNodeService;
    }

    @GetMapping()
    public SystemViewDto getSystemView() {
        SystemViewDto dto = new SystemViewDto();

        dto.setStatistics(systemAdministrationService.findAdministrationStatistics());
        dto.setAppVersion(appVersion);

        dto.setPlugins(systemAdministrationService.findAllPluginsFilesOnInstance());
        dto.setWhiteList(systemAdministrationService.findWhiteList());
        dto.setUploadSizeLimit(systemAdministrationService.findUploadSizeLimit());
        dto.setImportSizeLimit(systemAdministrationService.findImportSizeLimit());
        dto.setCallbackUrl(systemAdministrationService.findCallbackUrl());
        dto.setStackTracePanelIsVisible(stackTracePanelIsVisible);
        dto.setStackTraceFeatureIsEnabled(featureManager.isEnabled(FeatureManager.Feature.STACK_TRACE));
        dto.setAutoconnectOnConnection(featureManager.isEnabled(FeatureManager.Feature.AUTOCONNECT_ON_CONNECTION));
        dto.setCaseInsensitiveLogin(featureManager.isEnabled(FeatureManager.Feature.CASE_INSENSITIVE_LOGIN));
        dto.setDuplicateLogins(userManagerService.findAllDuplicateLogins());
        dto.setCaseInsensitiveActions(featureManager.isEnabled(FeatureManager.Feature.CASE_INSENSITIVE_ACTIONS));
        dto.setDuplicateActions(actionWordLibraryNodeService.findAllCaseInsensitiveDuplicateActions());
        dto.setWelcomeMessage(systemAdministrationService.findWelcomeMessage());
        dto.setLoginMessage(systemAdministrationService.findLoginMessage());
        dto.setLogFiles(getAllLogFileNames());
		dto.setLicenseInfo(systemAdministrationService.getBasicLicenseInfo());

        return dto;
    }

	@GetMapping("/current-active-users-count")
	@ResponseBody
	public Map<String, Integer> getCurrentActiveUsersCount() {
		Map<String, Integer> responseBody = new HashMap<>();
    	int currentActiveUsersCount = userManagerService.countAllActiveUsersAssignedToAtLeastOneProject();
    	responseBody.put("currentActiveUsersCount", currentActiveUsersCount);
    	return responseBody;
	}

    private List<String> getAllLogFileNames() {
        if (Arrays.asList(environment.getActiveProfiles()).contains("dev")) {
            return new ArrayList<>();
        } else {
            return findAllLogFiles().stream()
                    .map(File::getName)
                    .collect(Collectors.toList());
        }
    }

    private List<File> findAllLogFiles() {
        File logsFolder = new File(loggingPath);

        if (!logsFolder.exists() || !logsFolder.isDirectory()) {
            LOGGER.warn("Logs path '{}' is not a readable folder. There will be no log files", logsFolder.getAbsolutePath());
            return new ArrayList<>();
        }

        LOGGER.info("Enumerating log files in folder '{}'", logsFolder.getAbsolutePath());

        File[] logFiles = logsFolder.listFiles((File dir, String name) -> name.startsWith("squash-tm.log."));

        if (logFiles != null) {
            return Arrays.stream(logFiles).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }
}

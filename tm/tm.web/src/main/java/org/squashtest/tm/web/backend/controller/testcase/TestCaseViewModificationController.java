/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.dto.AutomationRequestDto;
import org.squashtest.tm.service.internal.display.testcase.parameter.TestCaseParameterOperationReport;
import org.squashtest.tm.service.testcase.TestCaseModificationService;
import org.squashtest.tm.service.tf.AutomationRequestModificationService;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "backend/test-case/{testCaseId}")
public class TestCaseViewModificationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestCaseViewModificationController.class);

	private static final String TEST_SPACE_CASE = "test case ";


	private TestCaseModificationService testCaseModificationService;


	private AutomationRequestModificationService automationRequestModificationService;

	private TestCaseDisplayService testCaseDisplayService;

	public TestCaseViewModificationController(TestCaseModificationService testCaseModificationService,
											  AutomationRequestModificationService automationRequestModificationService,
											  TestCaseDisplayService testCaseDisplayService) {
		this.testCaseModificationService = testCaseModificationService;
		this.automationRequestModificationService = automationRequestModificationService;
		this.testCaseDisplayService = testCaseDisplayService;
	}

	@ResponseBody
	@RequestMapping(value = "/importance", method = RequestMethod.POST)
	public void changeImportance(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
		testCaseModificationService.changeImportance(testCaseId, TestCaseImportance.valueOf(patch.getImportance()));
	}

	@ResponseBody
	@RequestMapping(value = "/name", method = RequestMethod.POST)
	public void changeName(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
		testCaseModificationService.rename(testCaseId, patch.getName());
	}

	@ResponseBody
	@RequestMapping(value = "/reference", method = RequestMethod.POST)
	public void changeReference(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
		testCaseModificationService.changeReference(testCaseId, patch.getReference());
	}

	@ResponseBody
	@RequestMapping(value = "/status", method = RequestMethod.POST)
	public void changeStatus(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
		testCaseModificationService.changeStatus(testCaseId, TestCaseStatus.valueOf(patch.getStatus()));
	}

	@ResponseBody
	@RequestMapping(value = "/importance-auto", method = RequestMethod.POST)
	public Map<String, String> changeImportanceAuto(@PathVariable long testCaseId,
													@RequestBody TestCasePatch patch) {
		testCaseModificationService.changeImportanceAuto(testCaseId, patch.isImportanceAuto());
		TestCase testCase = testCaseModificationService.findById(testCaseId);
		Map<String, String> testCaseImportance = new HashMap<>();
		testCaseImportance.put("importance", testCase.getImportance().name());
		return testCaseImportance;
	}

	@ResponseBody
	@RequestMapping(value = "/nature", method = RequestMethod.POST)
	public void changeNature(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
		testCaseModificationService.changeNature(testCaseId, patch.getNature());
	}

	@ResponseBody
	@RequestMapping(value = "/type", method = RequestMethod.POST)
	public void changeType(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
		testCaseModificationService.changeType(testCaseId, patch.getType());
	}


	@ResponseBody
	@RequestMapping(value = "/description", method = RequestMethod.POST)
	public void changeDescription(@RequestBody TestCasePatch patch, @PathVariable long testCaseId) {
		testCaseModificationService.changeDescription(testCaseId, patch.getDescription());
	}

	@RequestMapping(value = "/milestones/{milestoneIds}", method = RequestMethod.POST)
	@ResponseBody
	public void bindMilestones(@PathVariable long testCaseId, @PathVariable("milestoneIds") List<Long> milestoneIds) {
		testCaseModificationService.bindMilestones(testCaseId, milestoneIds);
	}


	@ResponseBody
	@RequestMapping(value = "/milestones/{milestoneIds}", method = RequestMethod.DELETE)
	public void unbindMilestones(@PathVariable long testCaseId, @PathVariable List<Long> milestoneIds) {
		testCaseModificationService.unbindMilestones(testCaseId, milestoneIds);
	}


	@ResponseBody
	@RequestMapping(value = "/automatable", method = RequestMethod.POST)
	public AutomationRequestDto changeAutomatable(@RequestBody TestCasePatch patch, @PathVariable long testCaseId) {
		return testCaseModificationService.changeAutomatable(TestCaseAutomatable.valueOf(patch.getAutomatable()), testCaseId);
	}


	@ResponseBody
	@RequestMapping(value = "/prerequisite", method = RequestMethod.POST)
	public TestCaseParameterOperationReport changePrerequisite(@PathVariable long testCaseId, @RequestBody TestCasePatch patch) {
		testCaseModificationService.changePrerequisite(testCaseId, patch.getPrerequisite());
		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace(TEST_SPACE_CASE + testCaseId + ": updated prerequisite to " + patch.getPrerequisite());
		}
		return testCaseDisplayService.findParametersData(testCaseId);
	}

	@ResponseBody
	@RequestMapping(value = "/automation-request/priority", method = RequestMethod.POST)
	public void changeAutomationRequestPriority(@RequestBody Map<String, Integer> request, @PathVariable long testCaseId) {
		automationRequestModificationService.changePriority(Collections.singletonList(testCaseId), request.get("priority"));
	}

	@ResponseBody
	@PostMapping(value = "/scm-repository-id")
	public void changeScmRepository(@RequestBody TestCasePatch testCasePatch, @PathVariable long testCaseId) {
		if (testCasePatch.getScmRepositoryId() == null || testCasePatch.getScmRepositoryId().equals(0L)) {
			testCaseModificationService.unbindSourceCodeRepository(testCaseId);
		} else {
			String newScmRepositoryUrl =
				testCaseModificationService.changeSourceCodeRepository(testCaseId, testCasePatch.getScmRepositoryId());
			if (LOGGER.isTraceEnabled()) {
				LOGGER.trace(TEST_SPACE_CASE + testCaseId + ": updated git repository to " + newScmRepositoryUrl);
			}
		}
	}

	@ResponseBody
	@PostMapping(value = "/automated-test-technology")
	public void changeAutomatedTestTechnology(@RequestBody TestCasePatch testCasePatch, @PathVariable long testCaseId) {
		if (testCasePatch.getAutomatedTestTechnology() != null) {
			testCaseModificationService.changeAutomatedTestTechnology(testCaseId, testCasePatch.getAutomatedTestTechnology());
		} else {
			testCaseModificationService.unbindAutomatedTestTechnology(testCaseId);
		}
	}

	@ResponseBody
	@PostMapping(value = "/automated-test-reference")
	public void changeAutomatedTestReference(@RequestBody TestCasePatch testCasePatch, @PathVariable long testCaseId) {
		testCaseModificationService.changeAutomatedTestReference(testCaseId, testCasePatch.getAutomatedTestReference());
	}

	static class TestCasePatch {
		private String name;
		private String reference;
		private String description;
		private String prerequisite;
		private String importance;
		private String status;
		private String automatable;
		private Long nature;
		private Long type;
		private boolean importanceAuto;
		private Long scmRepositoryId;
		private Long automatedTestTechnology;
		private String automatedTestReference;

		public TestCasePatch() {
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getReference() {
			return reference;
		}

		public void setReference(String reference) {
			this.reference = reference;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getImportance() {
			return importance;
		}

		public void setImportance(String importance) {
			this.importance = importance;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Long getNature() {
			return nature;
		}

		public void setNature(Long nature) {
			this.nature = nature;
		}

		public Long getType() {
			return type;
		}

		public void setType(Long type) {
			this.type = type;
		}

		public String getPrerequisite() {
			return prerequisite;
		}

		public void setPrerequisite(String prerequisite) {
			this.prerequisite = prerequisite;
		}

		public boolean isImportanceAuto() {
			return importanceAuto;
		}

		public void setImportanceAuto(boolean importanceAuto) {
			this.importanceAuto = importanceAuto;
		}

		public String getAutomatable() {
			return automatable;
		}

		public void setAutomatable(String automatable) {
			this.automatable = automatable;
		}

		public Long getScmRepositoryId() {
			return scmRepositoryId;
		}

		public void setScmRepositoryId(Long scmRepositoryId) {
			this.scmRepositoryId = scmRepositoryId;
		}

		public Long getAutomatedTestTechnology() {
			return automatedTestTechnology;
		}

		public void setAutomatedTestTechnology(Long automatedTestTechnology) {
			this.automatedTestTechnology = automatedTestTechnology;
		}

		public String getAutomatedTestReference() {
			return automatedTestReference;
		}

		public void setAutomatedTestReference(String automatedTestReference) {
			this.automatedTestReference = automatedTestReference;
		}
	}

}

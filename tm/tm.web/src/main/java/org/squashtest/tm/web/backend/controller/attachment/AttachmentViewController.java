/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.attachment;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.display.attachment.AttachmentDisplayService;
import org.squashtest.tm.service.internal.display.dto.AttachmentDto;
import org.squashtest.tm.service.internal.display.dto.AttachmentListDto;
import org.squashtest.tm.web.backend.fileupload.UploadContentFilterUtil;
import org.squashtest.tm.web.backend.fileupload.UploadSummary;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("backend/attach-list/{attachListId}")
public class AttachmentViewController {

	private static final String STR_UPLOAD_STATUS_OK = "dialog.attachment.summary.statusok.label";
	private static final String STR_UPLOAD_STATUS_WRONGFILETYPE = "dialog.attachment.summary.statuswrongtype.label";

	private static final Logger LOGGER = LoggerFactory.getLogger(AttachmentViewController.class);

	@Inject
	private AttachmentManagerService attachmentManagerService;

	@Inject
	private AttachmentDisplayService attachmentDisplayService;

	@Inject
	private MessageSource messageSource;

	@Inject
	private UploadContentFilterUtil filterUtil;

	@InitBinder
	public void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws ServletException {
		binder.registerCustomEditor(UploadedData.class, new UploadedDataPropertyEditorSupport());
	}

	@RequestMapping(method = RequestMethod.GET)
	public AttachmentListDto getAttachmentList(@PathVariable long attachListId) {
		return attachmentDisplayService.getAttachmentList(attachListId);
	}

	@RequestMapping(value = "/attachments/{attachmentIds}", method = RequestMethod.DELETE)
	public void removeAttachment(@PathVariable long attachListId,
								 @PathVariable("attachmentIds") List<Long> attachmentIds,
								 @RequestParam("entityId") long entityId,
								 @RequestParam("entityType") String entityType) throws IOException {

		final EntityReference entityRef = getEntityReference(entityType, entityId);
		attachmentManagerService.removeListOfAttachments(attachListId, attachmentIds, entityRef);
	}

	@RequestMapping(value = "/attachments/upload", method = RequestMethod.POST, produces = "application/json")
	public List<UploadSummary> uploadAttachmentAsJson(@RequestParam("attachment[]") List<UploadedData> attachments,
													  @PathVariable long attachListId,
													  @RequestParam("entityId") long entityId,
													  @RequestParam("entityType") String entityType,
													  Locale locale)
		throws IOException {

		List<UploadSummary> summary = new LinkedList<>();

		final EntityReference entityRef = getEntityReference(entityType, entityId);

		for (UploadedData upload : attachments) {

			LOGGER.trace("AttachmentController : adding attachment " + upload.getName());

			// file type checking
			boolean shouldProceed = filterUtil.isTypeAllowed(upload);

			if (!shouldProceed) {
				AttachmentDto refusedAttachmentDto = new AttachmentDto();
				refusedAttachmentDto.setName(HtmlUtils.htmlEscape(upload.getName()));

				summary.add(
					new UploadSummary(
						refusedAttachmentDto,
						getUploadSummary(STR_UPLOAD_STATUS_WRONGFILETYPE, locale),
						UploadSummary.INT_UPLOAD_STATUS_WRONGFILETYPE)
				);
			} else {
				AttachmentDto newAttachmentDto = attachmentManagerService.addAttachment(attachListId, upload, entityRef);

				summary.add(
					new UploadSummary(
						newAttachmentDto,
						getUploadSummary(STR_UPLOAD_STATUS_OK, locale),
						UploadSummary.INT_UPLOAD_STATUS_OK)
				);
			}
		}
		return summary;
	}

	@ResponseBody
	@RequestMapping(value = "/attachments/download/{attachemendId}", method = RequestMethod.GET)
	public void downloadAttachment(@PathVariable("attachemendId") long attachmentId, HttpServletResponse response) {

		try {
			Attachment attachment = attachmentManagerService.findAttachment(attachmentId);
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment; filename=" + attachment.getName().replace(" ", "_"));

			ServletOutputStream outStream = response.getOutputStream();

			attachmentManagerService.writeContent(attachmentId, outStream);
		} catch (IOException e) {
			LOGGER.warn("Error happened during attachment download : " + e.getMessage(), e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

		}
	}

	private String getUploadSummary(String key, Locale locale) {
		return messageSource.getMessage(key, null, locale);
	}

	private EntityReference getEntityReference(String entityType, long entityId) {
		String className = StringUtils.capitalize(entityType);
		EntityType type = EntityType.fromSimpleName(className);
		return new EntityReference(type, entityId);
	}

}

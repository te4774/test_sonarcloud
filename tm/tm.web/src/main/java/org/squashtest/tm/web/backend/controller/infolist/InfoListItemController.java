/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.infolist;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.service.display.infolist.InfoListDisplayService;
import org.squashtest.tm.service.infolist.InfoListItemManagerService;
import org.squashtest.tm.service.infolist.InfoListManagerService;
import org.squashtest.tm.service.internal.display.dto.InfoListAdminViewDto;

import javax.inject.Inject;
import java.util.List;

@Controller
@RequestMapping("/backend/info-list-items")
public class InfoListItemController {

	InfoListItemManagerService infoListItemManagerService;
	InfoListManagerService infoListManagerService;
	InfoListDisplayService infoListDisplayService;

	@Inject
    InfoListItemController(InfoListItemManagerService infoListItemManagerService,
						   InfoListManagerService infoListManagerService,
						   InfoListDisplayService infoListDisplayService) {
		this.infoListItemManagerService = infoListItemManagerService;
		this.infoListManagerService = infoListManagerService;
		this.infoListDisplayService = infoListDisplayService;
	}

	@ResponseBody
	@RequestMapping(value = "/{infoListItemId}/label", method = RequestMethod.POST)
	public void changeLabel(@PathVariable long infoListItemId, @RequestBody InfoListItemPatch patch) {
		infoListItemManagerService.changeLabel(infoListItemId, patch.getLabel());
	}

	@ResponseBody
	@RequestMapping(value = "/{infoListItemId}/code", method = RequestMethod.POST)
	public void changeCode(@PathVariable long infoListItemId, @RequestBody InfoListItemPatch patch) {
		infoListItemManagerService.changeCode(infoListItemId, patch.getCode());
	}

	@ResponseBody
	@RequestMapping(value = "/{infoListItemId}/colour", method = RequestMethod.POST)
	public void changeColour(@PathVariable long infoListItemId, @RequestBody InfoListItemPatch patch) {
		infoListItemManagerService.changeColour(infoListItemId, patch.getColour());
	}

	@ResponseBody
	@RequestMapping(value = "/{infoListItemId}/default", method = RequestMethod.POST)
	public void setDefault(@PathVariable long infoListItemId) {
		infoListItemManagerService.changeDefault(infoListItemId);
	}

	@ResponseBody
	@RequestMapping(value = "/{infoListItemId}/icon-name", method = RequestMethod.POST)
	public void changeIcon(@PathVariable long infoListItemId, @RequestBody InfoListItemPatch patch) {
		infoListItemManagerService.changeIcon(infoListItemId, patch.getIconName());
	}

	@RequestMapping(value = "/{infoListId}/items/{infoListItemIds}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteInfoListItem(@PathVariable long infoListId, @PathVariable List<Long> infoListItemIds) {
		infoListItemManagerService.removeInfoListItems(infoListItemIds, infoListId);
	}

	@RequestMapping(value = "/{infoListId}/items/positions", method = RequestMethod.POST)
	@ResponseBody
	public InfoListAdminViewDto changeOptionsPositions(@PathVariable long infoListId, @RequestBody ReorderOptionsRequestBody requestBody) {
		infoListManagerService.changeItemsPositions(infoListId, requestBody.getPosition(), requestBody.getInfoListItemIds());
		return infoListDisplayService.getInfoListView(infoListId);
	}

	public static class InfoListItemPatch {
		private String label;
		private String code;
		private String colour;
		private String iconName;

		public String getLabel() { return label; }

		public void setLabel(String label) { this.label = label; }

		public String getCode() { return code; }

		public void setCode(String code) { this.code = code; }

		public String getColour() {
			return colour;
		}

		public void setColour(String colour) {
			this.colour = colour;
		}

		public String getIconName() {
			return iconName;
		}

		public void setIconName(String iconName) {
			this.iconName = iconName;
		}
	}

	static class ReorderOptionsRequestBody {
		private List<Long> infoListItemIds;
		private Integer position;

		public Integer getPosition() {
			return position;
		}

		public void setPosition(Integer position) {
			this.position = position;
		}

		public List<Long> getInfoListItemIds() { return infoListItemIds; }

		public void setInfoListItemIds(List<Long> infoListItemIds) { this.infoListItemIds = infoListItemIds; }
	}
}

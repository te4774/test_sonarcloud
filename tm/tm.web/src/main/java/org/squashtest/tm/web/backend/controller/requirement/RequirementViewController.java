/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.requirement;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.service.display.requirements.RequirementDisplayService;
import org.squashtest.tm.service.internal.display.dto.requirement.AbstractRequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.HighLevelRequirementVersionDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionBundleStatsDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder;
import org.squashtest.tm.service.requirement.RequirementVersionManagerService;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping(path = "backend/requirement-view")
public class RequirementViewController {

	private final RequirementDisplayService requirementDisplayService;

	private final RequirementVersionManagerService versionManagerService;

	private final ActiveMilestoneHolder activeMilestoneHolder;

	public RequirementViewController(RequirementDisplayService requirementDisplayService,
									 RequirementVersionManagerService versionManagerService,
									 ActiveMilestoneHolder activeMilestoneHolder) {
		this.requirementDisplayService = requirementDisplayService;
		this.versionManagerService = versionManagerService;
		this.activeMilestoneHolder = activeMilestoneHolder;
	}

	@GetMapping(path = "/current-version/{requirementId}")
	public AbstractRequirementVersionDto findCurrentVersion(@PathVariable Long requirementId) {
		return requirementDisplayService.findCurrentVersionView(requirementId);
	}

	@GetMapping(path = "high-level/current-version/{requirementId}")
	public HighLevelRequirementVersionDto findHighLevelCurrentVersion(@PathVariable Long requirementId) {
		return requirementDisplayService.findHighLevelCurrentVersionView(requirementId);
	}

	@GetMapping(path = "/{requirementVersionId}")
	public AbstractRequirementVersionDto findVersion(@PathVariable Long requirementVersionId) {
		return requirementDisplayService.findVersionView(requirementVersionId);
	}

	@GetMapping(path = "/{requirementVersionId}/statistics")
	public RequirementVersionBundleStatsDto getRequirementVersionStatistics(@PathVariable Long requirementVersionId) {
		return requirementDisplayService.computeRequirementVersionStatistics(requirementVersionId);
	}

	@GetMapping(path = "/{requirementVersionId}/issues-count")
	public Integer getRequirementVersionIssuesCount(@PathVariable Long requirementVersionId) {
		return requirementDisplayService.countIssuesByRequirementVersionId(requirementVersionId);
	}

	@ResponseBody
	@PostMapping(path = "/versions/{requirementId}")
	public GridResponse findRequirementVersions(@PathVariable Long requirementId, @RequestBody GridRequest request) {
		return requirementDisplayService.findVersionsByRequirementId(requirementId, request);
	}

	@ResponseBody
	@PostMapping(path = "/{requirementId}/new")
	public AbstractRequirementVersionDto createNewVersion(@PathVariable Long requirementId, @RequestBody NewReqVersionParams params) {
		Optional<Milestone> activeMilestone = activeMilestoneHolder.getActiveMilestone();
		if (activeMilestone.isPresent()) {
			// milestone mode creation
			ArrayList<Long> milestoneIds = new ArrayList<>();
			milestoneIds.add(activeMilestone.get().getId());
			versionManagerService.createNewVersion(requirementId, milestoneIds, params.isInheritReqLinks(), params.isInheritTestCases());
		} else {
			// normal mode creation
			versionManagerService.createNewVersion(requirementId, params.isInheritReqLinks(), params.isInheritTestCases());
		}

		return requirementDisplayService.findCurrentVersionView(requirementId);
	}

	static class NewReqVersionParams {
		private boolean inheritReqLinks;
		private boolean inheritTestCases;

		public boolean isInheritReqLinks() {
			return inheritReqLinks;
		}

		public void setInheritReqLinks(boolean inheritReqLinks) {
			this.inheritReqLinks = inheritReqLinks;
		}

		public boolean isInheritTestCases() {
			return inheritTestCases;
		}

		public void setInheritTestCases(boolean inheritTestCases) {
			this.inheritTestCases = inheritTestCases;
		}
	}

}

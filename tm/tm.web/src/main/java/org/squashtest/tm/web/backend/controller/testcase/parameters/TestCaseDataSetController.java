/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.parameters;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.exception.DuplicateNameException;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.service.display.dataset.DatasetDisplayService;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.dto.testcase.DataSetDto;
import org.squashtest.tm.service.internal.display.testcase.parameter.NewDatasetData;
import org.squashtest.tm.service.internal.display.testcase.parameter.TestCaseParameterOperationReport;
import org.squashtest.tm.service.testcase.DatasetModificationService;
import org.squashtest.tm.service.testcase.ParameterFinder;

import java.util.List;

@Controller
@RequestMapping("backend/test-cases")
public class TestCaseDataSetController {

	private final DatasetModificationService datasetModificationService;

	private final ParameterFinder parameterFinder;

	private final TestCaseDisplayService testCaseDisplayService;

	private final DatasetDisplayService datasetDisplayService;

	public TestCaseDataSetController(DatasetModificationService datasetModificationService,
									 ParameterFinder parameterFinder,
									 TestCaseDisplayService testCaseDisplayService,
									 DatasetDisplayService datasetDisplayService) {
		this.datasetModificationService = datasetModificationService;
		this.parameterFinder = parameterFinder;
		this.testCaseDisplayService = testCaseDisplayService;
		this.datasetDisplayService = datasetDisplayService;
	}

	@GetMapping("/{testCaseId}/datasets")
	@ResponseBody
	public List<DataSetDto> getAvailableDatasets(@PathVariable Long testCaseId) {
		return datasetDisplayService.findAllByTestCaseId(testCaseId);
	}

	@PostMapping("/{testCaseId}/datasets/new")
	@ResponseBody
	public TestCaseParameterOperationReport newDataset(@PathVariable long testCaseId, @RequestBody NewDatasetData dataset) {
		try {
			datasetModificationService.persist(dataset.createDataSet(parameterFinder), testCaseId);
		} catch (DuplicateNameException ex) {
			throw new NameAlreadyInUseException("Dataset", dataset.getName());
		}

		return testCaseDisplayService.findParametersData(testCaseId);
	}

}

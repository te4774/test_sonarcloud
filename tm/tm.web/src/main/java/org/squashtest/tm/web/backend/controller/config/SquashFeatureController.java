/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.config;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.squashtest.tm.service.feature.FeatureManager;
import org.squashtest.tm.web.backend.listener.SquashConfigContextExposer;

import javax.inject.Inject;
import javax.servlet.ServletContext;

@Controller
@RequestMapping("/backend/features")
public class SquashFeatureController {

	private static final String ENABLED = "enabled";

    /**
     * This "monitor" should be synchronized when performing changes on the app
     * scope to emulate transactions. Consider using something higher level from
     * java.concurrent when this class gets more complex.
     *
     * Note that we do **not** want to synchronize ServletContext for a
     * potentially long time.
     */
    private final Object monitor = new Object();

    @Inject
    private FeatureManager featureManager;

    @Inject
    private ServletContext applicationScope;

    @PostMapping(value = "/milestones")
    @ResponseBody
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Secured("ROLE_ADMIN")
    public void setMilestonesFeature(@RequestBody() SetMilestoneFeatureRequestBody requestBody) {
        final Boolean enabled = requestBody.getEnabled();
        synchronized (monitor) {
            Object prevState = applicationScope.getAttribute(SquashConfigContextExposer.MILESTONE_FEATURE_ENABLED_CONTEXT_ATTR);
            // nobody should be able to use the feature while it is being turned
            // on/off
            applicationScope.setAttribute(SquashConfigContextExposer.MILESTONE_FEATURE_ENABLED_CONTEXT_ATTR, false);

            try {
                featureManager.setEnabled(FeatureManager.Feature.MILESTONE, enabled);
                applicationScope.setAttribute(SquashConfigContextExposer.MILESTONE_FEATURE_ENABLED_CONTEXT_ATTR, enabled);

            } catch (RuntimeException ex) {
                // exception occurred : we rollback the app state
                applicationScope.setAttribute(SquashConfigContextExposer.MILESTONE_FEATURE_ENABLED_CONTEXT_ATTR, prevState);
                throw ex;
            }
        }
    }

    public static class SetMilestoneFeatureRequestBody {
        Boolean enabled;

        public Boolean getEnabled() {
            return enabled;
        }

        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }
    }

	@RequestMapping(value = "/case-insensitive-login", method = RequestMethod.POST, params = ENABLED)
	@ResponseBody
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@Secured("ROLE_ADMIN")
	public void setCaseInsensitiveLoginFeature(@RequestParam(ENABLED) boolean enabled) {
		featureManager.setEnabled(FeatureManager.Feature.CASE_INSENSITIVE_LOGIN, enabled);
	}

	@RequestMapping(value = "/case-insensitive-actions", method = RequestMethod.POST, params = ENABLED)
	@ResponseBody
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@Secured("ROLE_ADMIN")
	public void setCaseInsensitiveActionsFeature(@RequestParam(ENABLED) boolean enabled) {
		featureManager.setEnabled(FeatureManager.Feature.CASE_INSENSITIVE_ACTIONS, enabled);
	}

	@RequestMapping(value = "/stack-trace", method = RequestMethod.POST, params = ENABLED)
	@ResponseBody
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@Secured("ROLE_ADMIN")
	public void setStackTraceFeature(@RequestParam(ENABLED) boolean enabled) {
		featureManager.setEnabled(FeatureManager.Feature.STACK_TRACE, enabled);
	}

    @RequestMapping(value = "/autoconnect-on-connection", method = RequestMethod.POST, params = ENABLED)
    @ResponseBody
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Secured("ROLE_ADMIN")
    public void setAutoconnectOnConnectionFeature(@RequestParam(ENABLED) boolean enabled) {
        featureManager.setEnabled(FeatureManager.Feature.AUTOCONNECT_ON_CONNECTION, enabled);
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.requirements.links;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.exception.WrongStringSizeException;
import org.squashtest.tm.service.requirement.RequirementVersionLinkTypeManagerService;

import javax.inject.Inject;

@Controller
@RequestMapping("/backend/requirement-link-type/{requirementLinkTypeId}")
public class RequirementsLinkModificationController {

	@Inject
	private RequirementVersionLinkTypeManagerService requirementVersionLinkTypeManagerService;


	@ResponseBody
	@RequestMapping(value = "/role", method = RequestMethod.POST)
	public void changeRole1(@PathVariable long requirementLinkTypeId, @RequestBody RequirementLinkTypePatch patch) {
		if (patch.getRole().length() > 50) {
			throw new WrongStringSizeException("role1", 0, 50);
		}

		requirementVersionLinkTypeManagerService.changeRole1(requirementLinkTypeId, patch.getRole());
	}

	@ResponseBody
	@RequestMapping(value = "/role1Code", method = RequestMethod.POST)
	public void changeRole1Code(@PathVariable long requirementLinkTypeId, @RequestBody RequirementLinkTypePatch patch) {
		if (patch.getRole1Code().length() > 30) {
			throw new WrongStringSizeException("role1Code", 0, 30);
		}

		requirementVersionLinkTypeManagerService.changeCode1(requirementLinkTypeId, patch.getRole1Code());
	}

	@ResponseBody
	@RequestMapping(value = "/role2", method = RequestMethod.POST)
	public void changeRole2(@PathVariable long requirementLinkTypeId, @RequestBody RequirementLinkTypePatch patch) {
		if (patch.getRole2().length() > 50) {
			throw new WrongStringSizeException("role2", 0, 50);
		}
		requirementVersionLinkTypeManagerService.changeRole2(requirementLinkTypeId, patch.getRole2());
	}

	@ResponseBody
	@RequestMapping(value = "/role2Code", method = RequestMethod.POST)
	public void changeRole2Code(@PathVariable long requirementLinkTypeId, @RequestBody RequirementLinkTypePatch patch) {
		if (patch.getRole2Code().length() > 30) {
			throw new WrongStringSizeException("role2Code", 0, 30);
		}

		requirementVersionLinkTypeManagerService.changeCode2(requirementLinkTypeId, patch.getRole2Code());
	}

	@ResponseBody
	@RequestMapping(value = "/default", method = RequestMethod.POST)
	public void changeDefault(@PathVariable long requirementLinkTypeId) {
		requirementVersionLinkTypeManagerService.changeDefault(requirementLinkTypeId);
	}


	static class RequirementLinkTypePatch {
		private String role;
		private String role1Code;
		private String role2;
		private String role2Code;

		public String getRole() { return role; }

		public void setRole(String role) {this.role = role; }

		public String getRole1Code() { return role1Code; }

		public void setRole1Code(String role1Code) { this.role1Code = role1Code; }

		public String getRole2() { return role2; }

		public void setRole2(String role2) { this.role2 = role2; }

		public String getRole2Code() { return role2Code; }

		public void setRole2Code(String role2Code) { this.role2Code = role2Code; }
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * This controller is responsible to serve initial index.html for angular main application.
 * His main utility compared to serve the angular app as a static resource is to set the servlet context in the index.html file.
 * So despite the fact that angular make a static build, when can dynamically set the context to the servlet context path, thanks to thymeleaf.
 * Note that because Angular build is mainly automated by angular cli, the index.html file is directly provided by tm-front module and not by tm.web module.
 */
@Controller
public class IndexController {
	private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

	@GetMapping("/index")
	public String index() {
		LOGGER.debug("Page request forwarded to SquashTM index controller. Single page app will be served with current context path.");
		return "index.html";
	}
}

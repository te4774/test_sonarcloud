/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Configuration;

/**
 * This class is dedicated to activate DataSourceAutoConfiguration only if squash.datasource.enable.proxy is falsy.
 * If squash.datasource.enable.proxy is enabled we switch to manual DataSource config to use a JDBC proxy able to log
 * all JDBC request (Native, Jooq, Hibernate...) with parameters.
 * See {@link org.squashtest.tm.service.RepositoryConfig} for the details of the manual configuration.
 */
@Configuration
@ConditionalOnProperty(value = "squash.datasource.enable.proxy", havingValue = "false", matchIfMissing = true)
public class SquashDataSourceAutoConfiguration extends DataSourceAutoConfiguration {
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.importer.testcase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.squashtest.tm.service.importer.ImportSummary;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;
import org.squashtest.tm.web.backend.controller.RequestParams;
import org.squashtest.tm.web.backend.controller.importer.ImportExcelResponse;
import org.squashtest.tm.web.backend.controller.importer.ImportHelper;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Gregory Fouquet
 *
 */
// XSS OK
@Controller
@RequestMapping("backend/test-cases/importer")
public class TestCaseImportController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestCaseImportController.class);

	@Inject
	private TestCaseLibraryNavigationService navigationService;

	@Inject
	private TestCaseImportHelper importHelper;

	/**
	 * Will import test cases given in the form of zipped archive. The zip must contain a folder hierarchy, with
	 * test-cases represented by xls files. One test-case is represented by one xls file where only the first tab of the
	 * file is read.
	 *
	 * @see TestCaseLibraryNavigationService#importZipTestCase(InputStream, long, String)
	 * @param archive
	 *            : the uploaded file
	 * @param projectId
	 *            : the id of the project where the hierarchy must be imported
	 * @param zipEncoding
	 *            : the encoding to use for file names,
	 * @return a view with the result of the import
	 * @throws IOException exception thrown
	 */
	@RequestMapping(value = "/zip", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ImportSummary importZippedTestCases(@RequestParam("archive") MultipartFile archive,
											   @RequestParam(RequestParams.PROJECT_ID) long projectId, @RequestParam("zipEncoding") String zipEncoding)
		throws IOException {

		InputStream stream = archive.getInputStream();
		ModelAndView mav = new ModelAndView("fragment/import/import-summary");

		ImportSummary summary = navigationService.importZipTestCase(stream, projectId, zipEncoding);
		mav.addObject(ImportHelper.SUMMARY, summary);
		mav.addObject("workspace", "test-case");

		return summary;
	}

	/**
	 * Will simulate import of test cases in a one xls file format.
	 *
	 * @see TestCaseLibraryNavigationService#simulateImportExcelTestCase(File)
	 * @param uploadedFile
	 *            : the xls file to import in a {@link MultipartFile} form
	 * @param request
	 *            : the {@link WebRequest}
	 * @return a {@link ModelAndView} containing the summary of the import and the link to a complete log for any
	 *         invalid information it contains
	 */
	@RequestMapping(value = "/xls", method = RequestMethod.POST, params = "dry-run")
	@ResponseBody
	public ImportExcelResponse dryRunExcelWorkbook(@RequestParam("archive") MultipartFile uploadedFile, WebRequest request) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("dryRunExcelWorkbook");
		}
		return importHelper.importWorkbook(ImportHelper.TEST_CASES, uploadedFile, xls -> navigationService.simulateImportExcelTestCase(xls));
	}

	/**
	 * Will import test cases in a one xls file format.
	 *
	 * @see TestCaseLibraryNavigationService#performImportExcelTestCase(File)
	 * @param uploadedFile
	 *            : the xls file to import in a {@link MultipartFile} form
	 * @return @return a {@link ModelAndView} containing the summary of the import and the link to a complete log for
	 *         any invalid information it contains
	 */
	@RequestMapping(value = "/xls", params = "!dry-run", method = RequestMethod.POST)
	@ResponseBody
	public ImportExcelResponse importExcelWorkbook(@RequestParam("archive") MultipartFile uploadedFile) {
		return importHelper.importWorkbook(ImportHelper.TEST_CASES, uploadedFile, xls -> navigationService.performImportExcelTestCase(xls));
	}

}

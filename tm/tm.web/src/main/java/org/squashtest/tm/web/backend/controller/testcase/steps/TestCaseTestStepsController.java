/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.steps;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.KeywordTestStep;
import org.squashtest.tm.domain.testcase.ParameterAssignationMode;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.dto.testcase.AddTestStepOperationReport;
import org.squashtest.tm.service.internal.display.dto.testcase.PasteTestStepOperationReport;
import org.squashtest.tm.service.internal.display.dto.testcase.TestStepDto;
import org.squashtest.tm.service.internal.display.testcase.parameter.TestCaseParameterOperationReport;
import org.squashtest.tm.service.testcase.CallStepManagerService;
import org.squashtest.tm.service.testcase.TestCaseModificationService;
import org.squashtest.tm.web.i18n.InternationalizationHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

// XSS OK
@Controller
@RequestMapping("/backend/test-cases/{testCaseId}/steps")
public class TestCaseTestStepsController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestCaseTestStepsController.class);
	private static final String TEST_CASE_ = "test case ";

	private CallStepManagerService callStepManager;

	private TestCaseModificationService testCaseModificationService;

	private TestCaseDisplayService testCaseDisplayService;

	private InternationalizationHelper internationalizationHelper;

	public TestCaseTestStepsController(CallStepManagerService callStepManager,
									   TestCaseModificationService testCaseModificationService,
									   TestCaseDisplayService testCaseDisplayService,
									   InternationalizationHelper internationalizationHelper) {
		this.callStepManager = callStepManager;
		this.testCaseModificationService = testCaseModificationService;
		this.testCaseDisplayService = testCaseDisplayService;
		this.internationalizationHelper = internationalizationHelper;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public AddTestStepOperationReport addActionTestStep(@RequestBody ActionStepFormModel stepModel,
														@PathVariable long testCaseId) throws BindException {

		ActionTestStep step = stepModel.getActionTestStep();

		Map<Long, RawValue> customFieldValues = stepModel.getCufs();
		int index = stepModel.getIndex();

		ActionTestStep actionTestStep;

		if (index != 0) {
			actionTestStep = testCaseModificationService.addActionTestStep(testCaseId, step, customFieldValues, index);
		} else {
			actionTestStep = testCaseModificationService.addActionTestStep(testCaseId, step, customFieldValues);
		}

		LOGGER.trace(TEST_CASE_ + testCaseId + ": step added, action : " + step.getAction() + ", expected result : "
			+ step.getExpectedResult());

		// fetching the created test step as a DTO conform to the 2.0+ front
		// could be better to transform the ActionStep into DTO but it will require more code
		// and the performance hit of direct fetching is very low as it's a Jooq direct request
		TestStepDto dto = this.testCaseModificationService.findTestStep(actionTestStep.getId());
		TestCaseParameterOperationReport testCaseParameterOperationReport = testCaseDisplayService.findParametersDataByTestStepId(actionTestStep.getId());
		return new AddTestStepOperationReport(dto, testCaseParameterOperationReport);
	}

	@ResponseBody
	@PostMapping(value = "/add-keyword-test-step")
	public AddTestStepOperationReport addKeywordTestStep(@RequestBody KeywordTestStepModel keywordTestStepDto, @PathVariable long testCaseId)
		throws BindException {
		validateDto(keywordTestStepDto);

		String keyword = keywordTestStepDto.getKeyword();
		String actionWord = keywordTestStepDto.getActionWord();
		int index = keywordTestStepDto.getIndex();

		Long newKeywordStepId;

		if (keywordTestStepDto.getActionWordId() != null) {
			newKeywordStepId = addKeywordTestStepWithActionWordId(keywordTestStepDto, testCaseId);
		} else if (index != 0) {
			newKeywordStepId =
				testCaseModificationService.addKeywordTestStep(testCaseId, keyword, actionWord, index)
			.getId();
		} else {
			newKeywordStepId =
				testCaseModificationService.addKeywordTestStep(testCaseId, keyword, actionWord)
			.getId();
		}
		TestStepDto dto = this.testCaseModificationService.findTestStep(newKeywordStepId);
		TestCaseParameterOperationReport testCaseParameterOperationReport =
			testCaseDisplayService.findParametersDataByTestStepId(newKeywordStepId);
		return new AddTestStepOperationReport(dto, testCaseParameterOperationReport);
	}

	public Long addKeywordTestStepWithActionWordId(KeywordTestStepModel keywordTestStepDto, long testCaseId) throws BindException {
		validateDto(keywordTestStepDto);

		String keyword = keywordTestStepDto.getKeyword();
		String actionWord = keywordTestStepDto.getActionWord();
		int index = keywordTestStepDto.getIndex();
		long actionWordId = keywordTestStepDto.getActionWordId();

		KeywordTestStep step = testCaseModificationService.addKeywordTestStep(testCaseId, keyword, actionWord, actionWordId, index);

		return step.getId();
	}

	private void validateDto(KeywordTestStepModel keywordTestStepDto) throws BindException {
		BindingResult validation = new BeanPropertyBindingResult(keywordTestStepDto, "add-keyword-test-step");
		KeywordTestStepModel.KeywordTestStepModelValidator validator =
			new KeywordTestStepModel.KeywordTestStepModelValidator(internationalizationHelper);
		validator.validate(keywordTestStepDto, validation);

		if (validation.hasErrors()) {
			throw new BindException(validation);
		}
	}

	@PostMapping(value = "/compare-keywords-projects", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Boolean> compareKeywordProjectsIds(@RequestBody CopyStepModel copyStepModel, @PathVariable long testCaseId){
		List<Long> copiedTestStepIds = copyStepModel.getCopiedStepIds();
		List<Long> copiedTestStepsProjectsIds = testCaseModificationService.fetchProjectsIdsByTestStepsIds(copiedTestStepIds);
		Long targetProjectId = testCaseModificationService.fetchTargetProjectId(testCaseId);
		return ResponseEntity.ok(testCaseModificationService.areCopiedTestStepsProjectsIdsEqualToTargetProjectId(copiedTestStepsProjectsIds, targetProjectId));
	}

	@RequestMapping(value = "/paste", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public PasteTestStepOperationReport pasteSteps(@RequestBody CopyStepModel copyStepModel, @PathVariable long testCaseId) {
		List<Long> copiedStepIds = copyStepModel.getCopiedStepIds();
		callStepManager.checkForCyclicStepCallBeforePaste(testCaseId, copiedStepIds);
		PasteTestStepOperationReport pasteTestStepOperationReport;
		if (copyStepModel.targetTestStepId != null) {
			pasteTestStepOperationReport = testCaseModificationService.pasteCopiedTestSteps(testCaseId, copyStepModel.getTargetTestStepId(), copiedStepIds,
				copyStepModel.isAssignedToTargetProject());
		} else {
			pasteTestStepOperationReport = testCaseModificationService.pasteCopiedTestStepToLastIndex(testCaseId, copiedStepIds, copyStepModel.isAssignedToTargetProject());
		}
		pasteTestStepOperationReport.setOperationReport(testCaseDisplayService.findParametersData(testCaseId));
		return pasteTestStepOperationReport;
	}

	@RequestMapping(value = "/move", method = RequestMethod.POST)
	@ResponseBody
	public void changeStepsIndex(@PathVariable long testCaseId, @RequestBody MoveStepModel moveStepModel) {
		testCaseModificationService.changeTestStepsPosition(testCaseId, moveStepModel.newIndex, moveStepModel.movedStepIds);
	}

	@RequestMapping(value = "/{stepIds}", method = RequestMethod.DELETE)
	@ResponseBody
	public TestCaseParameterOperationReport deleteSteps(@PathVariable("stepIds") List<Long> stepIds, @PathVariable long testCaseId) {
		testCaseModificationService.removeListOfSteps(testCaseId, stepIds);
		return testCaseDisplayService.findParametersData(testCaseId);
	}

	@RequestMapping(value = "/call-test-case", method = RequestMethod.POST)
	@ResponseBody
	public PasteTestStepOperationReport addCallTestStep(@PathVariable("testCaseId") long callingTestCaseId,
														@RequestBody() CallTestCaseModel callTestCaseModel) {
		return callStepManager.addCallTestSteps(callingTestCaseId, callTestCaseModel.calledTestCaseIds, callTestCaseModel.index);
	}


	@RequestMapping(value = "{stepId}/parameter-assignation-mode", method = RequestMethod.POST)
	@ResponseBody
	public TestCaseParameterOperationReport changeParameterAssignationMode(@PathVariable("testCaseId") Long testCaseId, @PathVariable("stepId") Long stepId,
																		   @RequestBody ParameterAssignationModel model) {
		callStepManager.setParameterAssignationMode(stepId, ParameterAssignationMode.valueOf(model.getMode()), model.getDatasetId());
		return testCaseDisplayService.findParametersData(testCaseId);
	}

	static class CallTestCaseModel {
		private List<Long> calledTestCaseIds = new ArrayList<>();
		private Integer index;

		public List<Long> getCalledTestCaseIds() {
			return calledTestCaseIds;
		}

		public void setCalledTestCaseIds(List<Long> calledTestCaseIds) {
			this.calledTestCaseIds = calledTestCaseIds;
		}

		public Integer getIndex() {
			return index;
		}

		public void setIndex(Integer index) {
			this.index = index;
		}
	}

	static class CopyStepModel {
		private List<Long> copiedStepIds = new ArrayList<>();
		private Long targetTestStepId;
		@JsonProperty
		private Boolean isAssignedToTargetProject;

		public List<Long> getCopiedStepIds() {
			return copiedStepIds;
		}

		public void setCopiedStepIds(List<Long> copiedStepIds) {
			this.copiedStepIds = copiedStepIds;
		}

		public Long getTargetTestStepId() {
			return targetTestStepId;
		}

		public void setTargetTestStepId(Long targetTestStepId) {
			this.targetTestStepId = targetTestStepId;
		}

		public boolean isAssignedToTargetProject() {
			return Optional.ofNullable(isAssignedToTargetProject).orElse(false);
		}
	}


	static class MoveStepModel {
		private List<Long> movedStepIds = new ArrayList<>();
		private Integer newIndex;

		public List<Long> getMovedStepIds() {
			return movedStepIds;
		}

		public void setMovedStepIds(List<Long> movedStepIds) {
			this.movedStepIds = movedStepIds;
		}

		public Integer getNewIndex() {
			return newIndex;
		}

		public void setNewIndex(Integer newIndex) {
			this.newIndex = newIndex;
		}
	}

	static class ParameterAssignationModel {
		private String mode;
		private Long datasetId;

		public String getMode() {
			return mode;
		}

		public void setMode(String mode) {
			this.mode = mode;
		}

		public Long getDatasetId() {
			return datasetId;
		}

		public void setDatasetId(Long datasetId) {
			this.datasetId = datasetId;
		}
	}
}

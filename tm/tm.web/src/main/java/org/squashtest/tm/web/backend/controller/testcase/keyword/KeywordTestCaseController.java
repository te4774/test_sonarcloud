/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.keyword;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.service.actionword.ActionWordService;
import org.squashtest.tm.service.testcase.bdd.KeywordTestCaseService;
import org.squashtest.tm.web.backend.controller.form.model.ActionList;
import org.squashtest.tm.web.backend.controller.form.model.ScriptPreviewModel;

import javax.inject.Inject;
import java.util.Map;

@Controller
@RequestMapping("backend/keyword-test-cases")
public class KeywordTestCaseController {

	@Autowired(required = false)
	private ActionWordService actionWordService;

	@Inject
	private KeywordTestCaseService keywordTestCaseService;

	@ResponseBody
	@PostMapping("/autocomplete")
	public ActionList findAllMatchingActionWords(@RequestBody ActionWordAutocompleteInput input) {
		return new ActionList(
			Lists.newArrayList(
				actionWordService.findAllMatchingActionWords(input.getProjectId(), input.getSearchInput(), input.getSelectedProjectsIds())));
	}

	@ResponseBody
	@PostMapping("/duplicated-action")
	public Map<String, Long> findAllDuplicatedActionWithProject(@RequestBody ActionWordAutocompleteInput input) {
		return actionWordService.findAllDuplicatedActionWithProject(input.getProjectId(), input.getSearchInput());
	}

	@ResponseBody
	@RequestMapping("/{testCaseId}/generated-script")
	public ScriptPreviewModel getGeneratedScript(@PathVariable long testCaseId) {
		return new ScriptPreviewModel(
			keywordTestCaseService.writeScriptFromTestCase(testCaseId, false));
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.tf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.core.foundation.exception.InvalidUrlException;
import org.squashtest.tm.core.foundation.lang.UrlUtils;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.exception.WrongUrlException;
import org.squashtest.tm.exception.tf.WrongPriorityFormatException;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.dto.testcase.TestCaseDto;
import org.squashtest.tm.service.testautomation.model.TestAutomationProjectContent;
import org.squashtest.tm.service.testcase.TestCaseModificationService;
import org.squashtest.tm.service.tf.AutomationRequestModificationService;
import org.squashtest.tm.web.backend.model.testautomation.TATestNode;
import org.squashtest.tm.web.backend.model.testautomation.TATestNodeListBuilder;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.squashtest.tm.web.backend.controller.RequestParams.ITERATION_ID;
import static org.squashtest.tm.web.backend.controller.RequestParams.TEST_PLAN_ITEMS_IDS;
import static org.squashtest.tm.web.backend.controller.RequestParams.TEST_SUITE_ID;

@Controller
@RequestMapping("/backend/automation-requests")
public class AutomationModificationViewController {

	public static final Logger LOGGER = LoggerFactory.getLogger(AutomationModificationViewController.class);
	private static final String TEST_CASE_ID = "testCaseId";
	private static final String PATH = "path";
	private static final String TEST_SPACE_CASE = "test case ";

	private TestCaseModificationService testCaseModificationService;

	private TestCaseDisplayService testCaseDisplayService;

	private AutomationRequestModificationService automationRequestModificationService;

	public AutomationModificationViewController(TestCaseModificationService testCaseModificationService,
												TestCaseDisplayService testCaseDisplayService,
												AutomationRequestModificationService automationRequestModificationService) {
		this.testCaseModificationService = testCaseModificationService;
		this.testCaseDisplayService = testCaseDisplayService;
		this.automationRequestModificationService = automationRequestModificationService;
	}

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "/{tcIds}/status")
	public List<TestCaseDto> changeStatus(@PathVariable List<Long> tcIds, @RequestBody String status) {
		automationRequestModificationService.changeStatus(tcIds, AutomationRequestStatus.valueOf(status));
		return tcIds.stream().map((id) -> testCaseDisplayService.getTestCaseView(id)).collect(Collectors.toList());
	}

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "/{tcIds}/request-status")
	public Map<Long, String> changeAutomationRequestStatus(@PathVariable List<Long> tcIds, @RequestBody TestCaseAutomationPatch patch) {
		return automationRequestModificationService.changeStatus(tcIds, AutomationRequestStatus.valueOf(patch.getRequestStatus()));
	}

	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "/{tcIds}/unassign")
	public void unassignedUser(@PathVariable("tcIds") List<Long> tcIds) {
		automationRequestModificationService.unassignRequests(tcIds);
	}

	@RequestMapping(value = "{testCaseId}/tests", method = RequestMethod.POST, params = {PATH})
	@ResponseBody
	public String bindAutomatedTest(@PathVariable(TEST_CASE_ID) long testCaseId, @RequestParam(PATH) String testPath) {
		testCaseModificationService.bindAutomatedTestByAutomationProgrammer(testCaseId, testPath);
		return testPath;
	}

	@RequestMapping(value = "{testCaseId}/tests", method = RequestMethod.GET)
	@ResponseBody
	public Collection<TATestNode> findAssignableAutomatedTests(@PathVariable(TEST_CASE_ID) Long testCaseId) {
		Collection<TestAutomationProjectContent> projectContents = testCaseModificationService.findAssignableAutomationTestsToAutomationProgramer(testCaseId);
		return new TATestNodeListBuilder().build(projectContents);

	}

	@RequestMapping(method = RequestMethod.POST, value = "{testCaseId}/automatedTestTechnology")
	@ResponseBody
	public void changeAutomatedTestTechnology(@RequestBody TestCaseAutomationPatch patch, @PathVariable long testCaseId) {
		if (patch.getAutomatedTestTechnology() != null) {
			testCaseModificationService.changeAutomatedTestTechnology(testCaseId, patch.getAutomatedTestTechnology());
		} else {
			testCaseModificationService.unbindAutomatedTestTechnology(testCaseId);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "{testCaseId}/automatedTestReference")
	@ResponseBody
	public void changeAutomatedTestReference(@RequestBody TestCaseAutomationPatch patch, @PathVariable long testCaseId) {
		testCaseModificationService.changeAutomatedTestReference(testCaseId, patch.getAutomatedTestReference());
	}

	@RequestMapping(method = RequestMethod.POST, value = "{testCaseId}/automationPriority")
	@ResponseBody
	public void changeAutomationPriority(@RequestBody TestCaseAutomationPatch patch, @PathVariable long testCaseId) {
		try {
			Integer newPriority;
			if(patch.automationPriority.isEmpty()) {
				newPriority = null;
			} else {
				newPriority = Integer.parseInt(patch.automationPriority);
			}
			testCaseModificationService.changeAutomationPriority(testCaseId, newPriority);
		} catch (NumberFormatException nfe) {
			throw new WrongPriorityFormatException(nfe);
		}
	}

	@RequestMapping(value = "/{tcIds}/assign", method = RequestMethod.POST)
	@ResponseBody
	public void assigneeToAutomationReq(@PathVariable("tcIds") List<Long> tcIds) {
		automationRequestModificationService.assignedToRequest(tcIds);
	}

	@RequestMapping(value = "/{tcIds}/associate-TA-script", method = RequestMethod.POST)
	@ResponseBody
	public Map<Long, String> resolveTAScriptAssociation(@PathVariable("tcIds") List<Long> tcIds) {
		return automationRequestModificationService.updateTAScript(tcIds);
	}

	/*TM-13: update automatic association */

	@ResponseBody
	@RequestMapping(value = "/associate-TA-script", method = RequestMethod.POST, params = {ITERATION_ID})
	public Map<Long, String> resolveTAScriptAssociationForIteration(@RequestParam(ITERATION_ID) long iterationId) {
		return automationRequestModificationService.updateTAScriptForIteration(iterationId);
	}

	@ResponseBody
	@RequestMapping(value = "/associate-TA-script", method = RequestMethod.POST, params = {TEST_SUITE_ID})
	public Map<Long, String> resolveTAScriptAssociationForTestSuite(@RequestParam(TEST_SUITE_ID) long testSuiteId) {
		return automationRequestModificationService.updateTAScriptForTestSuite(testSuiteId);
	}

	@ResponseBody
	@RequestMapping(value = "/associate-TA-script", method = RequestMethod.POST, params = {TEST_PLAN_ITEMS_IDS})
	public Map<Long, String> resolveTAScriptAssociationForIterationItems(@RequestParam(TEST_PLAN_ITEMS_IDS) List<Long> testPlanIds) {
		return automationRequestModificationService.updateTAScriptForItems(testPlanIds);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{tcId}/source-code-repository-url")
	@ResponseBody
	public void changeSourceCodeRepositoryUrl(@PathVariable Long tcId, @RequestBody TestCaseAutomationPatch patch) throws WrongUrlException {
		String testCaseSourceCodeRepositoryUrl = patch.getTestCaseSourceRepositoryUrl();
		testCaseSourceCodeRepositoryUrl = testCaseSourceCodeRepositoryUrl.substring(0, Math.min(testCaseSourceCodeRepositoryUrl.length(), 255));
		if (!testCaseSourceCodeRepositoryUrl.isEmpty()) {
			//Checking if string has URL format
			try {
				UrlUtils.toUrl(testCaseSourceCodeRepositoryUrl);
			} catch (InvalidUrlException iue) {
				throw new WrongUrlException("url", iue);
			}

		}
		//testCaseModificationService.changeSourceCodeRepositoryUrl(tcId, testCaseSourceCodeRepositoryUrl); TODO update after 1.22.3 merge
		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace(TEST_SPACE_CASE + tcId + ": updated git repository url to " + testCaseSourceCodeRepositoryUrl);
		}
	}

	static class TestCaseAutomationPatch {
		private Long automatedTestTechnology;
		private String automatedTestReference;
		private String automationPriority;
		private String requestStatus;
		private String testCaseSourceRepositoryUrl;

		public String getAutomationPriority() {
			return automationPriority;
		}

		public void setAutomationPriority(String automationPriority) {
			this.automationPriority = automationPriority;
		}

		public String getAutomatedTestReference() {
			return automatedTestReference;
		}

		public void setAutomatedTestReference(String automatedTestReference) {
			this.automatedTestReference = automatedTestReference;
		}

		public Long getAutomatedTestTechnology() {
			return automatedTestTechnology;
		}

		public void setAutomatedTestTechnology(Long automatedTestTechnology) {
			this.automatedTestTechnology = automatedTestTechnology;
		}

		public String getRequestStatus() {
			return requestStatus;
		}

		public void setRequestStatus(String requestStatus) {
			this.requestStatus = requestStatus;
		}

		public String getTestCaseSourceRepositoryUrl() {
			return testCaseSourceRepositoryUrl;
		}

		public void setTestCaseSourceRepositoryUrl(String testCaseSourceRepositoryUrl) {
			this.testCaseSourceRepositoryUrl = testCaseSourceRepositoryUrl;
		}
	}
}

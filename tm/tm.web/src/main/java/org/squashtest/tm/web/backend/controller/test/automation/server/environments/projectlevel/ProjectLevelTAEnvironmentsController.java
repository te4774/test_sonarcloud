/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.test.automation.server.environments.projectlevel;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.service.project.CustomProjectModificationService;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService;
import org.squashtest.tm.web.backend.controller.form.model.StringList;
import org.squashtest.tm.web.backend.controller.test.automation.server.environments.AbstractTAEnvironmentsController;

/**
 * Controller for operations on automated execution environment at project-level. Restricted to project managers and
 * administrators (see endpoint configuration in {@link org.squashtest.tm.web.config.WebSecurityConfig}).
 */
@Controller
@RequestMapping("/backend/generic-projects/")
public class ProjectLevelTAEnvironmentsController extends AbstractTAEnvironmentsController {

	private final CustomProjectModificationService customProjectModificationService;

	public ProjectLevelTAEnvironmentsController(AutomatedExecutionEnvironmentService automatedExecutionEnvironmentService, CustomProjectModificationService customProjectModificationService, StoredCredentialsManager storedCredentialsManager) {
		super(automatedExecutionEnvironmentService, storedCredentialsManager);
		this.customProjectModificationService = customProjectModificationService;
	}

	@ResponseBody
	@PostMapping(value = "{projectId}/environment-tags")
	public void updateProjectEnvironmentTags(@PathVariable Long projectId, @RequestBody StringList tagList) {
		customProjectModificationService.overrideEnvironmentTags(projectId, tagList.getList());
	}

	@ResponseBody
	@DeleteMapping(value = "{projectId}/environment-tags")
	public void clearProjectTagOverrides(@PathVariable Long projectId) {
		customProjectModificationService.clearEnvironmentTagOverrides(projectId);
	}

	@ResponseBody
	@PostMapping("{projectId}/automated-execution-environments/tokens/{serverId}")
	public void changeTAServerTokenOverride(@PathVariable Long projectId,
											@PathVariable Long serverId,
											@RequestBody TokenOverrideRequestBody body) {
		customProjectModificationService.setTestAutomationServerTokenOverride(projectId, serverId, body.token);
	}

	@ResponseBody
	@DeleteMapping("{projectId}/automated-execution-environments/tokens/{serverId}")
	public void clearTAServerTokenOverride(@PathVariable Long projectId, @PathVariable Long serverId) {
		customProjectModificationService.clearTestAutomationServerTokenOverride(projectId, serverId);
	}

	public static class TokenOverrideRequestBody {
		private String token;

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.report;

import org.squashtest.tm.api.report.form.DateInput;
import org.squashtest.tm.api.report.form.Input;
import org.squashtest.tm.api.report.form.InputType;
import org.squashtest.tm.api.report.form.InputsGroup;

import java.util.List;
import java.util.stream.Collectors;

public class InputsGroupJson implements InputJson {

	private final String label;
	private final String name;
	private final InputType type;
	private final boolean required;

	private List<InputJson> inputs;

	public InputsGroupJson(InputsGroup group) {
		this.label = group.getLabel();
		this.name = group.getName();
		this.type = group.getType();
		this.required = group.isRequired();
		this.inputs = group.getInputs().stream().map(InputsGroupJson::fromInput).collect(Collectors.toList());
	}

	private static InputJson fromInput(Input input) {
		if (InputType.DATE.equals(input.getType())) {
			DateInput dateInput = (DateInput) input;
			return new BasicInputJson(dateInput);
		}

		return null;
	}

	@Override
	public String getName() {
		return name;
	}

	public InputType getType() {
		return type;
	}

	public boolean isRequired() {
		return required;
	}

	public List<InputJson> getInputs() {
		return inputs;
	}

	public String getLabel() {
		return label;
	}
}

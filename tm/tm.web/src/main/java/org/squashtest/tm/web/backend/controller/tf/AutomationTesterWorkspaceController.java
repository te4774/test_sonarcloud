/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.tf;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.service.display.tf.AutomationTesterRequestDisplayService;
import org.squashtest.tm.service.display.tf.AutomationWorkspaceDataProviderService;
import org.squashtest.tm.service.internal.display.dto.tf.AutomationTesterWorkspaceDataDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

@Controller
@RequestMapping(value = "backend/automation-tester-workspace")
public class AutomationTesterWorkspaceController {

	private final AutomationTesterRequestDisplayService automationTesterRequestDisplayService;
	private final AutomationWorkspaceDataProviderService automationWorkspaceDataProviderService;

	public AutomationTesterWorkspaceController(AutomationTesterRequestDisplayService automationTesterRequestDisplayService, AutomationWorkspaceDataProviderService automationWorkspaceDataProviderService) {
		this.automationTesterRequestDisplayService = automationTesterRequestDisplayService;
		this.automationWorkspaceDataProviderService = automationWorkspaceDataProviderService;
	}

	@ResponseBody
	@GetMapping(value = "data")
	public AutomationTesterWorkspaceDataDto getWorkspaceRefData() {
		return automationWorkspaceDataProviderService.findFunctionalTesterData();
	}

	@ResponseBody
	@PostMapping(value = "global-view")
	public GridResponse getAutomationRequestForGlobalView(@RequestBody GridRequest request) {
		return automationTesterRequestDisplayService.findGlobalView(request);
	}

	@ResponseBody
	@PostMapping(value = "ready-for-transmission")
	public GridResponse getAutomationRequestForReadyView(@RequestBody GridRequest request) {
		return automationTesterRequestDisplayService.findReadyForTransmissionRequests(request);
	}

	@ResponseBody
	@PostMapping(value = "to-be-validated")
	public GridResponse getAutomationRequestForValidateView(@RequestBody GridRequest request) {
		return automationTesterRequestDisplayService.findToBeValidatedRequests(request);
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.domain.audit.AuditableMixin;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.report.ReportDefinition;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.report.ReportModificationService;
import org.squashtest.tm.web.backend.helper.JsonHelper;
import org.squashtest.tm.web.backend.helper.ReportHelper;
import org.squashtest.tm.web.backend.report.IdentifiedReportDecorator;
import org.squashtest.tm.web.backend.report.ReportsRegistry;
import org.squashtest.tm.web.backend.report.criteria.ConciseFormToCriteriaConverter;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("backend/report-definition-view")
public class ReportDefinitionViewController {

	private final CustomReportLibraryNodeService customReportLibraryNodeService;
	private final ReportModificationService reportModificationService;
	private final ReportsRegistry reportsRegistry;
	private final ReportHelper reportHelper;
	private final ProjectFinder projectFinder;

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportDefinitionViewController.class);

	public ReportDefinitionViewController(CustomReportLibraryNodeService customReportLibraryNodeService,
										  ReportModificationService reportModificationService,
										  ReportsRegistry reportsRegistry,
										  ReportHelper reportHelper,
										  ProjectFinder projectFinder) {
		this.customReportLibraryNodeService = customReportLibraryNodeService;
		this.reportModificationService = reportModificationService;
		this.reportsRegistry = reportsRegistry;
		this.reportHelper = reportHelper;
		this.projectFinder = projectFinder;
	}

	@RequestMapping(value = "/{customReportLibraryNodeId}", method = RequestMethod.GET)
	public ReportDefinitionViewModel getReportDefinitionViewModel(@PathVariable Long customReportLibraryNodeId) {
		ReportDefinition reportDefinition = customReportLibraryNodeService.findReportDefinitionByNodeId(customReportLibraryNodeId);
		return getViewModelFromDefinition(reportDefinition, customReportLibraryNodeId);
	}

	@RequestMapping(value = "/report-definition/{reportDefinitionId}")
	public ReportDefinitionViewModel getReportDefinitionViewModelByReportId(@PathVariable Long reportDefinitionId) {
		ReportDefinition reportDefinition = reportModificationService.findById(reportDefinitionId);
		return getViewModelFromDefinition(reportDefinition, null);
	}

	private ReportDefinitionViewModel getViewModelFromDefinition(ReportDefinition reportDefinition, Long customReportLibraryNodeId) {
		IdentifiedReportDecorator report = reportsRegistry.findReport(reportDefinition.getPluginNamespace());
		if (Objects.nonNull(report)) {
			Map<String, Object> form = null;
			try {
				form = JsonHelper.deserialize(reportDefinition.getParameters());
			} catch (IOException e) {
				LOGGER.error("the report : " + reportDefinition.getName() + " has corrupted parameters.", e);
			}

			List<Project> projects = projectFinder.findAllOrderedByName();
			Map<String, Criteria> crit = new ConciseFormToCriteriaConverter(report, projects).convert(form);
			return new ReportDefinitionViewModel(reportDefinition, customReportLibraryNodeId, reportHelper.getAttributesForReport(report, crit), report);
		} else {
			return new ReportDefinitionViewModel(reportDefinition, customReportLibraryNodeId);
		}
	}

	static class ReportDefinitionViewModel {
		public final Long id;
		public final Long customReportLibraryNodeId;
		public final Long projectId;
		public final String name;
		public final String description;
		public final String summary;
		public final String pluginNamespace;
		public final String parameters;
		public Date createdOn;
		public String createdBy;
		public Date lastModifiedOn;
		public String lastModifiedBy;
		public Map<String, List<String>> attributes;
		public String reportLabel;
		public ReportData report;
		public boolean missingPlugin = true;

		public ReportDefinitionViewModel(ReportDefinition reportDefinition, Long customReportLibraryNodeId) {
			this.id = reportDefinition.getId();
			this.customReportLibraryNodeId = customReportLibraryNodeId;
			this.projectId = reportDefinition.getProject().getId();
			this.name = reportDefinition.getName();
			this.description = reportDefinition.getDescription();
			this.summary = reportDefinition.getSummary();
			this.pluginNamespace = reportDefinition.getPluginNamespace();
			this.parameters = reportDefinition.getParameters();
			this.attributes = new HashMap<>();
			doAuditableAttributes(reportDefinition);
		}

		public ReportDefinitionViewModel(ReportDefinition reportDefinition,
										 Long customReportLibraryNodeId,
										 Map<String, List<String>> reportAttributes,
										 IdentifiedReportDecorator report) {
			this(reportDefinition, customReportLibraryNodeId);
			this.reportLabel = report.getLabel();
			this.missingPlugin = false;
			this.attributes = reportAttributes;
			this.report = new ReportData(report);
		}

		private void doAuditableAttributes(ReportDefinition def) {
			AuditableMixin audit = (AuditableMixin) def;
			this.createdBy = audit.getCreatedBy();
			this.lastModifiedBy = audit.getLastModifiedBy();
			this.createdOn = audit.getCreatedOn();
			this.lastModifiedOn = audit.getLastModifiedOn();
		}
	}
}

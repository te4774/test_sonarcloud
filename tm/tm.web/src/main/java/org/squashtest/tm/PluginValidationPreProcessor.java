/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.PriorityOrdered;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.configuration.ConfigurationService;

import javax.inject.Inject;

/**
 * This application listener fires before any plugin validation in order to reset the database license flags.
 * This allows to unlock a community plan for a database that used to be under license.
 */
@Component()
public class PluginValidationPreProcessor implements ApplicationListener<ApplicationReadyEvent>, PriorityOrdered {

	@Inject
	private ConfigurationService configurationService;

	private static final Logger LOGGER = LoggerFactory.getLogger(PluginValidationPreProcessor.class);

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		LOGGER.info("About to reset plugin license validation information.");
		resetPluginLicenseInfo();
	}

	@Override
	public int getOrder() {
		// Make sures the listener fires before the plugins validation
		return HIGHEST_PRECEDENCE;
	}

	private void resetPluginLicenseInfo() {
		configurationService.updateConfiguration(ConfigurationService.Properties.PLUGIN_LICENSE_EXPIRATION, "");
		configurationService.updateConfiguration(ConfigurationService.Properties.ACTIVATED_USER_EXCESS, "");
	}
}

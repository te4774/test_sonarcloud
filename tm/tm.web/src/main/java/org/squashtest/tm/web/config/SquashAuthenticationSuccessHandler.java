/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.user.UserAdministrationService;
import org.squashtest.tm.web.backend.controller.RootController;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.squashtest.tm.web.backend.controller.RootController.REDIRECT_AFTER_AUTH;

@Component
@Configuration
public class SquashAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
	private RequestCache requestCache = new HttpSessionRequestCache();

	@Inject
	private UserAdministrationService userAdministrationService;

	public SquashAuthenticationSuccessHandler() {
		super();
		setUseReferer(true);
		setAlwaysUseDefaultTargetUrl(false);
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
		final HttpSession session = request.getSession();
		final UserDetails authUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		session.setAttribute("username", authUser.getUsername());
		response.setStatus(HttpServletResponse.SC_OK);

		final SavedRequest savedRequest = requestCache.getRequest(request, response);
		final String targetUrl = savedRequest != null ?
			savedRequest.getRedirectUrl() : RootController.getDefaultAuthenticatedRedirectUrlForUserAuthority();

		if (! userAdministrationService.findPostLoginInformation().isEmpty())  {
			getRedirectStrategy().sendRedirect(request, response, String.format("/information?%s=%s", REDIRECT_AFTER_AUTH, targetUrl));
		} else {
			getRedirectStrategy().sendRedirect(request, response, targetUrl);
		}
	}

	@Override
	public void setRequestCache(RequestCache requestCache) {
		this.requestCache = requestCache;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.search;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.search.RequirementSearchGridDisplayService;
import org.squashtest.tm.service.display.search.RequirementSearchInputData;
import org.squashtest.tm.service.display.search.RequirementSearchInputDataProvider;
import org.squashtest.tm.service.display.search.RequirementSearchService;
import org.squashtest.tm.service.display.search.ResearchResult;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;

@RestController
@RequestMapping("backend/search/requirement")
public class RequirementSearchController {

	private final RequirementSearchInputDataProvider inputDataProvider;
	private final RequirementSearchService requirementSearchService;
	private final RequirementSearchGridDisplayService requirementSearchGridService;

	public RequirementSearchController(RequirementSearchInputDataProvider inputDataProvider, RequirementSearchService requirementSearchService, RequirementSearchGridDisplayService requirementSearchGridService) {
		this.inputDataProvider = inputDataProvider;
		this.requirementSearchService = requirementSearchService;
		this.requirementSearchGridService = requirementSearchGridService;
	}

	@GetMapping
	public RequirementSearchInputData getSearchInputData() {
		return inputDataProvider.provide();
	}

	@PostMapping
	public GridResponse search(@RequestBody GridRequest gridRequest) {
		ResearchResult result = requirementSearchService.search(gridRequest);
		return requirementSearchGridService.fetchResearchRows(result);
	}

}

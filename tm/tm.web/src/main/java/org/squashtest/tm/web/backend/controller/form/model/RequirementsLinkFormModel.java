/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.form.model;

import org.squashtest.tm.domain.requirement.RequirementVersionLinkType;


public class RequirementsLinkFormModel {

	private String role1;
	private String role1Code;
	private String role2;
	private String role2Code;

	public RequirementVersionLinkType getRequirementsLink() {
		RequirementVersionLinkType requirementVersionLinkType = new RequirementVersionLinkType();
		requirementVersionLinkType.setRole1(role1);
		requirementVersionLinkType.setRole1Code(role1Code);
		requirementVersionLinkType.setRole2(role2);
		requirementVersionLinkType.setRole2Code(role2Code);
		return requirementVersionLinkType;
	}


	public String getRole1() { return role1; }

	public void setRole1(String role1) { this.role1 = role1; }

	public String getRole1Code() {return role1Code; }

	public void setRole1Code(String role1Code) { this.role1Code = role1Code; }

	public String getRole2() { return role2; }

	public void setRole2(String role2) { this.role2 = role2; }

	public String getRole2Code() { return role2Code; }

	public void setRole2Code(String role2Code) { this.role2Code = role2Code; }


}

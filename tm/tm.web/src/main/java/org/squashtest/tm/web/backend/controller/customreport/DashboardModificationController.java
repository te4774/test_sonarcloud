/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.customreport;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportChartBinding;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.domain.customreport.CustomReportReportBinding;
import org.squashtest.tm.service.customreport.CustomReportDashboardService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.display.customreport.DashboardDisplayService;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportChartBinding;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportReportBinding;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportChartBindingBuilder;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportReportBindingBuilder;

import javax.inject.Provider;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/backend/dashboard")
public class DashboardModificationController {

	private final CustomReportLibraryNodeService customReportLibraryNodeService;
	private final CustomReportDashboardService dashboardService;
	private final Provider<JsonCustomReportChartBindingBuilder> chartBindingBuilderProvider;
	private final Provider<JsonCustomReportReportBindingBuilder> reportBindingBuilderProvider;
	private final DashboardDisplayService dashboardDisplayService;

	public DashboardModificationController(CustomReportLibraryNodeService customReportLibraryNodeService,
										   CustomReportDashboardService customReportDashboardService,
										   Provider<JsonCustomReportChartBindingBuilder> chartBindingBuilderProvider,
										   Provider<JsonCustomReportReportBindingBuilder> reportBindingBuilderProvider,
										   DashboardDisplayService dashboardDisplayService) {
		this.customReportLibraryNodeService = customReportLibraryNodeService;
		this.dashboardService = customReportDashboardService;
		this.chartBindingBuilderProvider = chartBindingBuilderProvider;
		this.reportBindingBuilderProvider = reportBindingBuilderProvider;
		this.dashboardDisplayService = dashboardDisplayService;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{dashboardEntityId}/name")
	public void rename(@PathVariable long dashboardEntityId, @RequestBody DashboardPatch patch) {
		customReportLibraryNodeService.renameNode(dashboardEntityId, patch.getName(), CustomReportDashboard.class);
	}

	@RequestMapping(value = "/{dashboardEntityId}/chart-binding", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public JsonCustomReportChartBinding createChartBinding(@PathVariable long dashboardEntityId, @RequestBody ChartBindingForm formChartBinding) {
		CustomReportChartBinding chartBinding = formChartBinding.convertToEntity();
		dashboardService.bindChart(chartBinding, formChartBinding.getChartDefinitionNodeId(), dashboardEntityId);
		return chartBindingBuilderProvider.get().build(chartBinding);
	}

	@RequestMapping(value = "/{dashboardEntityId}/report-binding", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public JsonCustomReportReportBinding createReportBinding(@PathVariable long dashboardEntityId, @RequestBody ReportBindingForm formReportBinding) {
		CustomReportReportBinding reportBinding = formReportBinding.convertToEntity();
		dashboardService.bindReport(reportBinding, formReportBinding.getReportDefinitionNodeId(), dashboardEntityId);
		return reportBindingBuilderProvider.get().build(reportBinding);
	}

	@ResponseBody
	@RequestMapping(value = "/chart-binding/{id}/swap/{chartNodeId}", method = RequestMethod.POST)
	public JsonCustomReportChartBinding swapChart(@PathVariable long id, @PathVariable long chartNodeId) {
		CustomReportChartBinding chartBinding = dashboardService.changeBindedChart(id, chartNodeId);
		return chartBindingBuilderProvider.get().build(chartBinding);
	}

	@ResponseBody
	@RequestMapping(value = "/report-binding/{id}/swap/{reportNodeId}", method = RequestMethod.POST)
	public JsonCustomReportReportBinding swapReport(@PathVariable long id, @PathVariable long reportNodeId) {
		CustomReportReportBinding reportBinding = dashboardService.changeBindedReport(id, reportNodeId);
		return reportBindingBuilderProvider.get().build(reportBinding);
	}

	@ResponseBody
	@RequestMapping(value = "/bindings", method = RequestMethod.POST)
	public void updateGrid(@RequestBody GridPositionUpdate gridElements) {
		List<CustomReportChartBinding> chartBindings = extractChartBindings(gridElements);
		List<CustomReportReportBinding> reportBindings = extractReportBindings(gridElements);
		dashboardService.updateGridPosition(chartBindings, reportBindings);
	}

	@ResponseBody
	@RequestMapping(value = "/chart-binding/{id}", method = RequestMethod.DELETE)
	public void unbindChart(@PathVariable long id) {
		dashboardService.unbindChart(id);
	}

	@ResponseBody
	@RequestMapping(value = "/report-binding/{id}", method = RequestMethod.DELETE)
	public void unbindReport(@PathVariable long id) {
		dashboardService.unbindReport(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/favorite/{workspace}/{dashboardId}")
	@ResponseBody
	public DashboardFavoriteWorkspaces changeFavoriteDashboard(@PathVariable long dashboardId, @PathVariable String workspace) {
		Workspace workspaceId = Workspace.valueOf(workspace);
		dashboardService.chooseFavoriteDashboardForCurrentUser(workspaceId, dashboardId);
		return new DashboardFavoriteWorkspaces(dashboardDisplayService.findFavoriteWorkspaceForDashboardAndCurrentUser(dashboardId));
	}

	@DeleteMapping(value = "/favorite/{dashboardId}")
	@ResponseBody
	public void removeDashboardFromFavorites(@PathVariable long dashboardId) {
		dashboardService.removeDashboardFromFavoritesForCurrentUser(dashboardId);
	}

	@DeleteMapping(value = "/favorite/{workspace}/{dashboardId}")
	@ResponseBody
	public void removeDashboardFromFavoriteWithPreferenceKey(@PathVariable long dashboardId, @PathVariable String workspace) {
		Workspace workspaceId = Workspace.valueOf(workspace);
		dashboardService.removeDashboardFromFavoriteWithPreferenceKey(dashboardId, workspaceId);
	}

	private List<CustomReportReportBinding> extractReportBindings(GridPositionUpdate gridElements) {
		return gridElements.getReports().stream().map(ReportBindingForm::convertToEntity).collect(Collectors.toList());
	}

	private List<CustomReportChartBinding> extractChartBindings(GridPositionUpdate gridElements) {
		return gridElements.getCharts().stream().map(ChartBindingForm::convertToEntity).collect(Collectors.toList());
	}

	static class DashboardPatch {
		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	static abstract class DashboardBindingForm {
		// id is set only when updating
		protected Long id;
		protected int row;
		protected int col;
		protected int sizeX;
		protected int sizeY;

		public int getRow() {
			return row;
		}

		public void setRow(int row) {
			this.row = row;
		}

		public int getCol() {
			return col;
		}

		public void setCol(int col) {
			this.col = col;
		}

		public int getSizeX() {
			return sizeX;
		}

		public void setSizeX(int sizeX) {
			this.sizeX = sizeX;
		}

		public int getSizeY() {
			return sizeY;
		}

		public void setSizeY(int sizeY) {
			this.sizeY = sizeY;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}
	}

	static class ChartBindingForm extends DashboardBindingForm {
		private Long chartDefinitionNodeId;

		public Long getChartDefinitionNodeId() {
			return chartDefinitionNodeId;
		}

		public void setChartDefinitionNodeId(Long chartDefinitionNodeId) {
			this.chartDefinitionNodeId = chartDefinitionNodeId;
		}

		public CustomReportChartBinding convertToEntity() {
			CustomReportChartBinding chartBinding = new CustomReportChartBinding();
			chartBinding.setId(id);
			chartBinding.setCol(col);
			chartBinding.setRow(row);
			chartBinding.setSizeX(sizeX);
			chartBinding.setSizeY(sizeY);

			return chartBinding;
		}

	}

	static class ReportBindingForm extends DashboardBindingForm {
		private Long reportDefinitionNodeId;

		public Long getReportDefinitionNodeId() {
			return reportDefinitionNodeId;
		}

		public void setReportDefinitionNodeId(Long reportDefinitionNodeId) {
			this.reportDefinitionNodeId = reportDefinitionNodeId;
		}


		public CustomReportReportBinding convertToEntity() {
			CustomReportReportBinding reportBinding = new CustomReportReportBinding();
			reportBinding.setId(id);
			reportBinding.setCol(col);
			reportBinding.setRow(row);
			reportBinding.setSizeX(sizeX);
			reportBinding.setSizeY(sizeY);

			return reportBinding;
		}
	}

	static class GridPositionUpdate {
		private List<ChartBindingForm> charts = new ArrayList<>();
		private List<ReportBindingForm> reports = new ArrayList<>();

		public List<ChartBindingForm> getCharts() {
			return charts;
		}

		public void setCharts(List<ChartBindingForm> charts) {
			this.charts = charts;
		}

		public List<ReportBindingForm> getReports() {
			return reports;
		}

		public void setReports(List<ReportBindingForm> reports) {
			this.reports = reports;
		}
	}

	static class DashboardFavoriteWorkspaces {
		private List<Workspace> workspaces;

		public DashboardFavoriteWorkspaces(List<Workspace> workspaces) {
			this.workspaces = workspaces;
		}

		public List<Workspace> getWorkspaces() {
			return workspaces;
		}
	}
}

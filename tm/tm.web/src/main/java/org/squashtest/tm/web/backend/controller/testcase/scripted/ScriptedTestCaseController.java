/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.scripted;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.service.testcase.scripted.ScriptedTestCaseService;


@RequestMapping("backend/test-case/{testCaseId}/scripted")
@Controller
public class ScriptedTestCaseController {

	private final ScriptedTestCaseService scriptedTestCaseService;

	public ScriptedTestCaseController(ScriptedTestCaseService scriptedTestCaseService) {
		this.scriptedTestCaseService = scriptedTestCaseService;
	}

	@PostMapping
	@ResponseBody
	public void updateTcScript(@PathVariable Long testCaseId, @RequestBody ScriptedTestCaseModel scriptedTestCaseModel) {
		scriptedTestCaseService.updateTcScript(testCaseId, scriptedTestCaseModel.getScript());
	}

	@PostMapping(path = "/validate")
	@ResponseBody
	public void validateTcScript(@PathVariable Long testCaseId, @RequestBody ScriptedTestCaseModel scriptedTestCaseModel) {
			scriptedTestCaseService.validateScript(scriptedTestCaseModel.getScript());
	}
}

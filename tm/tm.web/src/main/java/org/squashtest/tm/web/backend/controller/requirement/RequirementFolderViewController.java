/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.requirement;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.display.requirements.RequirementDisplayService;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementFolderDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;

import javax.inject.Named;
import javax.inject.Provider;
import java.util.Collections;
import java.util.Locale;


@RestController
@RequestMapping("/backend/requirement-folder-view")
public class RequirementFolderViewController {

	private final RequirementLibraryNavigationService requirementLibraryNavigationService;
	private final RequirementDisplayService requirementDisplayService;
	private final CustomReportLibraryNodeService customReportLibraryNodeService;
	@Named("customReport.dashboardBuilder")
	private final Provider<JsonCustomReportDashboardBuilder> builderProvider;

	public RequirementFolderViewController(RequirementDisplayService requirementDisplayService,
										   RequirementLibraryNavigationService requirementLibraryNavigationService,
										   CustomReportLibraryNodeService customReportLibraryNodeService,
										   Provider<JsonCustomReportDashboardBuilder> builderProvider) {
		this.requirementDisplayService = requirementDisplayService;
		this.requirementLibraryNavigationService = requirementLibraryNavigationService;
		this.customReportLibraryNodeService = customReportLibraryNodeService;
		this.builderProvider = builderProvider;
	}

	@RequestMapping(value = "/{requirementFolderId}", method = RequestMethod.GET)
	public RequirementFolderDto getRequirementFolderView(@PathVariable long requirementFolderId, Locale locale) {
		RequirementFolderDto requirementFolderView = requirementDisplayService.getRequirementFolderView(requirementFolderId);
		if (requirementFolderView.isShouldShowFavoriteDashboard()) {
			if (requirementFolderView.isCanShowFavoriteDashboard()) {
				EntityReference folder = new EntityReference(EntityType.REQUIREMENT_FOLDER, requirementFolderId);
				CustomReportDashboard dashboard = customReportLibraryNodeService.findCustomReportDashboardById(requirementFolderView.getFavoriteDashboardId());
				JsonCustomReportDashboard jsonDashboard = builderProvider.get()
					.build(requirementFolderView.getFavoriteDashboardId(), dashboard, locale, Collections.singletonList(folder), false, true, Workspace.REQUIREMENT);
				requirementFolderView.setDashboard(jsonDashboard);
			}
		} else {
			requirementFolderView.setStatistics(requirementLibraryNavigationService
				.getStatisticsForSelection(Collections.emptyList(), Collections.singleton(requirementFolderId), true));
		}

		return requirementFolderView;
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.export;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.squashtest.tm.web.backend.controller.export.grid.CsvGridExporter;
import org.squashtest.tm.web.backend.controller.export.grid.GridExportModel;
import org.squashtest.tm.web.backend.controller.export.grid.XlsGridExporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@Controller()
@RequestMapping("/backend/grid-export")
public class GridExportController {

	@PostMapping("/xlsx")
	public ResponseEntity<InputStreamResource> export(@RequestBody GridExportModel gridExportModel) {
		try {
			XlsGridExporter exporter = new XlsGridExporter(gridExportModel);
			File export = exporter.export();
			InputStreamResource resource = new InputStreamResource(new FileInputStream(export));

			return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + export.getName())
				.contentType(MediaType.APPLICATION_OCTET_STREAM)
				.contentLength(export.length())
				.body(resource);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	@PostMapping("/csv")
	public ResponseEntity<InputStreamResource> exportCsv(@RequestBody GridExportModel gridExportModel) {
		try {
			CsvGridExporter exporter = new CsvGridExporter(gridExportModel);
			File export = exporter.export();
			InputStreamResource resource = new InputStreamResource(new FileInputStream(export));

			return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + export.getName())
				.contentType(MediaType.APPLICATION_OCTET_STREAM)
				.contentLength(export.length())
				.body(resource);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
}

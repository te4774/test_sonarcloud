/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.scm.server;

import org.apache.commons.collections4.map.SingletonMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.scm.server.ScmServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.ScmServerAdminViewDto;
import org.squashtest.tm.service.scmserver.ScmServerManagerService;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/backend/scm-server-view")
public class ScmServerViewController {

	private final ScmServerDisplayService scmServerDisplayService;
	private final ScmServerManagerService scmServerManagerService;

	@Inject
	ScmServerViewController(ScmServerDisplayService scmServerDisplayService,
							ScmServerManagerService scmServerManagerService) {
		this.scmServerDisplayService = scmServerDisplayService;
		this.scmServerManagerService = scmServerManagerService;
	}

	@RequestMapping(value = "/{scmServerId}", method = RequestMethod.GET)
	@ResponseBody
	public ScmServerAdminViewDto getScmServerView(@PathVariable long scmServerId) {
		return scmServerDisplayService.getScmServerView(scmServerId);
	}

	@GetMapping("/{scmServerIds}/are-used-by-test-cases")
	@ResponseBody
	public SingletonMap<String, Boolean> areScmServersUsed(@PathVariable List<Long> scmServerIds) {
		final boolean areUsed = scmServerManagerService.areServerRepositoriesBoundToProjectOrTestCases(scmServerIds);
		return new SingletonMap<>("areUsed", areUsed);
	}
}

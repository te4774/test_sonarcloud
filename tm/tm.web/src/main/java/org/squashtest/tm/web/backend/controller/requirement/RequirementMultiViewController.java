/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.requirement;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.domain.Workspace;
import org.squashtest.tm.domain.customreport.CustomReportDashboard;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.display.requirements.RequirementDisplayService;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementMultiSelectionDto;
import org.squashtest.tm.service.internal.dto.json.JsonCustomReportDashboard;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.web.backend.controller.requirement.RequirementStatisticsController.RequirementNodeList;
import org.squashtest.tm.web.backend.model.builder.JsonCustomReportDashboardBuilder;

import javax.inject.Named;
import javax.inject.Provider;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/backend/requirement-workspace-multi-view")
public class RequirementMultiViewController {

	private final RequirementLibraryNavigationService requirementLibraryNavigationService;
	private final RequirementDisplayService requirementDisplayService;
	private final CustomReportLibraryNodeService customReportLibraryNodeService;
	@Named("customReport.dashboardBuilder")
	private final Provider<JsonCustomReportDashboardBuilder> builderProvider;

	public RequirementMultiViewController(RequirementDisplayService requirementDisplayService,
										  RequirementLibraryNavigationService requirementLibraryNavigationService,
										  CustomReportLibraryNodeService customReportLibraryNodeService,
										  Provider<JsonCustomReportDashboardBuilder> builderProvider) {
		this.requirementDisplayService = requirementDisplayService;
		this.requirementLibraryNavigationService = requirementLibraryNavigationService;
		this.customReportLibraryNodeService = customReportLibraryNodeService;
		this.builderProvider = builderProvider;
	}

	@ResponseBody
	@PostMapping
	public RequirementMultiSelectionDto getRequirementMultiView(@RequestBody RequirementNodeList statisticRequest, Locale locale) {
		NodeReferences nodeReferences = statisticRequest.asNodeReferences();

		RequirementMultiSelectionDto requirementMultiView = requirementDisplayService.getRequirementMultiView();

		if (requirementMultiView.isShouldShowFavoriteDashboard()) {
			if (requirementMultiView.isCanShowFavoriteDashboard()) {
				List<String> references = statisticRequest.getReferences();
				List<EntityReference> entityReferences = references.stream().map(EntityReference::fromNodeId).collect(Collectors.toList());
				CustomReportDashboard dashboard = customReportLibraryNodeService.findCustomReportDashboardById(requirementMultiView.getFavoriteDashboardId());
				JsonCustomReportDashboard jsonDashboard = builderProvider.get()
					.build(requirementMultiView.getFavoriteDashboardId(), dashboard, locale, entityReferences, false, statisticRequest.isExtendedHighLvlReqScope(), Workspace.REQUIREMENT);
				requirementMultiView.setDashboard(jsonDashboard);
			}
		} else {
			Set<Long> libraryIds = nodeReferences.extractLibraryIds();
			Set<Long> requirementNodeIds = nodeReferences.extractNonLibraryIds();
			requirementMultiView.setStatistics(requirementLibraryNavigationService
				.getStatisticsForSelection(libraryIds, requirementNodeIds, statisticRequest.isExtendedHighLvlReqScope()));
		}
		return requirementMultiView;
	}
}

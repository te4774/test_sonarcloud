/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.environmentVariable;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableOption;
import org.squashtest.tm.exception.DomainException;
import org.squashtest.tm.service.display.environmentvariable.EnvironmentVariableDisplayService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableManagerService;
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto;
import org.squashtest.tm.service.internal.display.dto.EnvironmentVariableDto;
import org.squashtest.tm.service.internal.display.dto.EnvironmentVariableOptionDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.web.backend.controller.form.model.EnvironmentVariableFormModel;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/backend/environment-variables")
public class EnvironmentVariableController {
	private final EnvironmentVariableManagerService environmentVariableManagerService;
	private final EnvironmentVariableDisplayService environmentVariableDisplayService;

	private final EnvironmentVariableBindingService environmentVariableBindingService;

	public EnvironmentVariableController(EnvironmentVariableManagerService environmentVariableManagerService,
										 EnvironmentVariableDisplayService environmentVariableDisplayService, EnvironmentVariableBindingService environmentVariableBindingService) {
		this.environmentVariableManagerService = environmentVariableManagerService;
		this.environmentVariableDisplayService = environmentVariableDisplayService;
		this.environmentVariableBindingService = environmentVariableBindingService;
	}

	@ResponseBody
	@PostMapping
	public GridResponse getAllEnvironmentVariables(@RequestBody GridRequest request) {
		return environmentVariableDisplayService.findAll(request);
	}


	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/new")
	@ResponseBody
	public Map<String, Object> createEnvironmentVariable(@RequestBody EnvironmentVariableFormModel model) {
		Map<String, Object> response = new HashMap<>();
		EnvironmentVariable environmentVariable = model.getEnvironmentVariable();
		environmentVariableManagerService.persist(environmentVariable);
		response.put("id", environmentVariable.getId());
		return response;
	}

	@ResponseBody
	@DeleteMapping(value = "/{environmentVariableIds}")
	public void deleteEnvironmentVariable(@PathVariable("environmentVariableIds")List<Long> environmentVariableIds) {
		environmentVariableManagerService.deleteEnvironmentVariable(environmentVariableIds);
	}

	@ResponseBody
	@PostMapping(value = "/{evId}/name")
	public void changeName(@PathVariable long evId, @RequestBody EnvironmentVariablePatch patch) {
		environmentVariableManagerService.changeName(evId, patch.getName());

	}

	@ResponseBody
	@PostMapping(value = "/{evId}/code")
	public void changeCode(@PathVariable long evId, @RequestBody EnvironmentVariablePatch patch) {
		environmentVariableManagerService.changeCode(evId, patch.getCode());
	}

	@ResponseBody
	@PostMapping(value = "/{evId}/options/code")
	public void changeOptionCode(@PathVariable long evId, @RequestBody EnvironmentVariableOptionPatch patch) {
		environmentVariableManagerService.changeOptionCode(evId, patch.getCurrentLabel(), patch.getCode());
	}

	@ResponseBody
	@PostMapping(value = "/{evId}/options/label")
	public void changeOptionLabel(@PathVariable long evId, @RequestBody EnvironmentVariableOptionPatch patch) {
		environmentVariableManagerService.changeOptionLabel(evId, patch.getCurrentLabel(), patch.getNewLabel());
	}

	@ResponseBody
	@PostMapping(value ="/{evId}/options/new")
	public Map<String, List<EnvironmentVariableOptionDto>> addNewOption(@PathVariable long evId, @Valid @RequestBody EnvironmentVariableOption option) {
		Map<String, List<EnvironmentVariableOptionDto>> response = new HashMap<>();
		try {
			environmentVariableManagerService.addOption(evId, option);
		} catch (DomainException e) {
			e.setObjectName("new-environment-variable-option");
			throw e;
		}
		response.put("options", this.environmentVariableDisplayService.getOptionsByEvId(evId));
		return response;
	}

	@ResponseBody
	@PostMapping(value = "/{evId}/options/remove")
	public void removeOptions(@PathVariable Long evId, @RequestBody Map<String, List<String>> request) {
		environmentVariableManagerService.removeOptions(evId, request.get("optionLabels"));
	}

	@ResponseBody
	@GetMapping("/{serverId}/bound-environment-variables")
	public List<BoundEnvironmentVariableDto> getBoundEnvironmentVariableFromServer(@PathVariable Long serverId) {
		return environmentVariableBindingService.getAllDefaultBoundEnvironmentVariableDto(serverId);
	}

	@ResponseBody
	@PostMapping("/{environmentVariableId}/options/positions")
	public EnvironmentVariableDto changeOptionsPositions(@PathVariable Long environmentVariableId, @RequestBody ReorderOptionsPatch patch) {
		environmentVariableManagerService.changeOptionsPosition(environmentVariableId, patch.getPosition(), patch.getLabels());
		return environmentVariableDisplayService.getEnvironmentVariableView(environmentVariableId);
	}

	static class EnvironmentVariablePatch {

		private String name;
		private String code;


		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	static class EnvironmentVariableOptionPatch {

		private String currentLabel;
		private String newLabel;
		private String code;

		public String getCurrentLabel() {
			return currentLabel;
		}

		public void setCurrentLabel(String currentLabel) {
			this.currentLabel = currentLabel;
		}

		public String getNewLabel() {
			return newLabel;
		}

		public void setNewLabel(String newLabel) {
			this.newLabel = newLabel;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	static class ReorderOptionsPatch {

		private List<String> labels;

		private Integer position;

		public List<String> getLabels() {
			return labels;
		}

		public void setLabels(List<String> labels) {
			this.labels = labels;
		}

		public Integer getPosition() {
			return position;
		}

		public void setPosition(Integer position) {
			this.position = position;
		}
	}
}


/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.export.grid;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class XlsGridExporter extends GridExporter {

	public XlsGridExporter(GridExportModel gridExportModel) {
		super(gridExportModel);
	}

	@Override
	public File export() throws IOException {
		printHeaders();
		printRows();
		formatColumns();
		File exportedFile = flushToTemporaryFile();
		clean();
		return exportedFile;
	}

	private void formatColumns() {
		for (int i = 0; i < gridExportModel.getExportedColumns().size(); i++) {
			sheet.autoSizeColumn(i);
		}
	}

	private void clean() {
		workbook.dispose();
	}

	private File flushToTemporaryFile() throws IOException {
		File temp = File.createTempFile("grid_export_", ".xls");
		temp.deleteOnExit();

		FileOutputStream fos = new FileOutputStream(temp);
		workbook.write(fos);
		fos.close();
		return temp;
	}
}

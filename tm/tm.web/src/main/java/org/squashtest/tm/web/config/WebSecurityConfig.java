/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.squashtest.tm.api.security.authentication.ApiSecurityExemptionEndPoint;
import org.squashtest.tm.api.security.authentication.SecurityExemptionEndPoint;
import org.squashtest.tm.web.security.authentication.SinglePageAppAuthenticationFailureHandler;
import org.squashtest.tm.web.security.authentication.SinglePageAppAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN_OR_PROJECT_MANAGER;

/**
 * This configures Spring Security
 * <p/>
 * #configure(AuthenticationManagerBuilder) should not be overriden ! When it is overriden, it supersedes any "global"
 * AuthenticationManager. This means any third party authentication provider will be ignored.
 *
 * @author Gregory Fouquet
 * @since 1.13.0
 */
@Configuration
public class WebSecurityConfig {

	private static final String ALTERNATE_AUTH_PATH = "/auth/**";
	private static final String LOGIN = "/login";
	private static final String ROOT_PATH = "/";
	private static final String CONTROLLERS_ROOT_URL = "/backend";
	private static final String[] ADMIN_OR_PROJECT_MANAGER_URLS = {
		CONTROLLERS_ROOT_URL + "/project-view/**",
		CONTROLLERS_ROOT_URL + "/generic-projects/**",
		CONTROLLERS_ROOT_URL + "/custom-field-binding/**",
		CONTROLLERS_ROOT_URL + "/milestones/**",
		CONTROLLERS_ROOT_URL + "/milestone-view/**",
		CONTROLLERS_ROOT_URL + "/milestone-binding/**",
		CONTROLLERS_ROOT_URL + "/scm-repositories/**",
		CONTROLLERS_ROOT_URL + "/info-list-binding/**",
		CONTROLLERS_ROOT_URL + "/referential/admin"
	};
	private static final String[] ADMIN_ONLY_URLS = {
		CONTROLLERS_ROOT_URL + "/project-templates/**",
		CONTROLLERS_ROOT_URL + "/projects/**",
		CONTROLLERS_ROOT_URL + "/bugtracker/**",
		CONTROLLERS_ROOT_URL + "/bugtrackers/**",
		CONTROLLERS_ROOT_URL + "/users/**",
		CONTROLLERS_ROOT_URL + "/custom-field-view/**",
		CONTROLLERS_ROOT_URL + "/environment-variables/**",
		CONTROLLERS_ROOT_URL + "/environment-variable-view/**",
		CONTROLLERS_ROOT_URL + "/info-lists/**",
		CONTROLLERS_ROOT_URL + "/info-list-items/**",
		CONTROLLERS_ROOT_URL + "/info-list-view/**",
		CONTROLLERS_ROOT_URL + "/requirements-links/**",
		CONTROLLERS_ROOT_URL + "/requirement-link-type/**",
		CONTROLLERS_ROOT_URL + "/scm-server-view/**",
		CONTROLLERS_ROOT_URL + "/scm-servers/**",
		CONTROLLERS_ROOT_URL + "/system/**",
		CONTROLLERS_ROOT_URL + "/system-view/**",
		CONTROLLERS_ROOT_URL + "/teams/**",
		CONTROLLERS_ROOT_URL + "/team-view/**",
		CONTROLLERS_ROOT_URL + "/test-automation-servers/**",
		CONTROLLERS_ROOT_URL + "/users/**",
		CONTROLLERS_ROOT_URL + "/user-view/**"};

	/* *********************************************************
	 *
	 *  Enpoint-specific security filter chains
	 *
	 * *********************************************************/

	@Configuration
	@Order(10)
	public static class SquashTAWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

		/**
		 * part of the fix for [Issue #6900]
		 */
		@Value("${squash.security.basic.token-charset}")
		private String basicAuthCharset = "ISO-8859-1";

		@Bean
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			// @formatter:off
			http
				//NO CSRF for these URL. They must be used by Jenkins to post execution results in Squash TM.
				.csrf().disable()
				.antMatcher("/automated-executions/**")
				.authorizeRequests()
				.anyRequest().access("hasRole('ROLE_TA_API_CLIENT')")
				.and()
				.httpBasic()
				.withObjectPostProcessor(new BasicAuthCharsetConfigurer(basicAuthCharset));
			// @formatter:on
		}
	}

	@Configuration
	@Order(20)
	public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

		/**
		 * part of the fix for [Issue #6900]
		 */
		@Value("${squash.security.basic.token-charset}")
		private String basicAuthCharset = "ISO-8859-1";

		@Autowired(required = false)
		private final Collection<ApiSecurityExemptionEndPoint> apiSecurityExemptionEndPoints = Collections.emptyList();

		@Override
		public void configure(WebSecurity webSecurity) {
			// This allows to completely bypass Spring's authentication: requests to URLs under permitAll() would still
			//  be subject to the authentication mechanism when there's an Basic Authorization header, which would cause
			//	them to be rejected if the provided credentials don't match real users
			webSecurity.ignoring().antMatchers(gatherIgnoringAuthUrlPatterns());
		}

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			// @formatter:off
			http
				.csrf().disable()
				.authorizeRequests().antMatchers(gatherIgnoringAuthUrlPatterns()).permitAll().and()
				.antMatcher("/api/**")
				.authorizeRequests()
				.anyRequest()
				.authenticated()
				.and()
				.httpBasic()
				.withObjectPostProcessor(new BasicAuthCharsetConfigurer(basicAuthCharset))
				.realmName("squash-api")
				.authenticationEntryPoint(new AuthenticationEntryPoint() {

					@Override
					public void commence(HttpServletRequest request,
										 HttpServletResponse response, AuthenticationException authException)
						throws IOException {
						// TODO Auto-generated method stub

						response.addHeader("WWW-Authenticate", "Basic realm=\"squah-api\"");
						response.addHeader("Content-Type", "application/json");
						response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
							authException.getMessage() +
								". You may authenticate using " +
								"1/ basic authentication or " +
								"2/ fetching a cookie JSESSIONID from /login");
					}
				})
				.and()
				.logout()
				.permitAll()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.invalidateHttpSession(true)
				.logoutSuccessUrl("/");
			// @formatter:on
		}

		private String[] gatherIgnoringAuthUrlPatterns() {
			List<String> result = new ArrayList<>();

			for (ApiSecurityExemptionEndPoint endPoint : apiSecurityExemptionEndPoints) {
				result.addAll(endPoint.getIgnoreAuthUrlPatterns());
			}

			return result.toArray(new String[0]);
		}
	}


	@Configuration
	@Order(30)
	public static class StandardWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

		private static final List<String> DEFAULT_IGNORE_AUTH_URLS =
			Arrays.asList(ROOT_PATH, LOGIN, ALTERNATE_AUTH_PATH, "/logout", "/logged-out");

		/**
		 * The Collection of collected SecurityExemptionEndPoint from the classpath.
		 */
		@Autowired(required = false)
		private Collection<SecurityExemptionEndPoint> securityExemptionEndPoints = Collections.emptyList();

		@Value("${squash.security.filter.debug.enabled:false}")
		private boolean debugSecurityFilter;

		@Value("${squash.security.preferred-auth-url:/login}")
		private String entryPointUrl = LOGIN;

		@Value("${squash.security.ignored:/scripts/**}")
		private String[] secIngored;

		@Bean
		public SessionRegistry sessionRegistry() {
			return new SessionRegistryImpl();
		}

		@Bean
		public HttpSessionEventPublisher httpSessionEventPublisher() {
			return new HttpSessionEventPublisher();
		}

		@Override
		public void configure(WebSecurity web) throws Exception {
			web.debug(debugSecurityFilter)
				.ignoring()
				.antMatchers(secIngored);
		}


		@Override
		protected void configure(HttpSecurity http) throws Exception {
			// @formatter:off
			http
				.headers()
				.frameOptions().sameOrigin()
				.and().csrf()
				.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
				.ignoringAntMatchers(gatherIgnoringCsrfUrlPatterns())
				.and().httpBasic()
				.and().authorizeRequests()
				// allow access to main/alternate authentication portals
				// note : on this domain the requests will always succeed, thus the user will not be redirected via the main entry point
				.antMatchers(gatherIgnoringAuthUrlPatterns()).permitAll()
				// public
				.antMatchers(CONTROLLERS_ROOT_URL + "/login", CONTROLLERS_ROOT_URL + "/logout", CONTROLLERS_ROOT_URL + "/login-page", CONTROLLERS_ROOT_URL + "/version", "/index.html", "/login", "/favicon.ico").permitAll()
				.antMatchers("/plugin/**", "/index").permitAll()
				.antMatchers(AngularAppPageUrls.getAllUrlsPatterns()).permitAll()
				.antMatchers("/*.js", "/**/*.js", "/**/*.json", "/*.js.map", "/**/*.js.map", "/resources/**", "/*.css", "/*.ts", "/*.ttf", "/assets/**").permitAll()
				// admin workspace
				.antMatchers(ADMIN_OR_PROJECT_MANAGER_URLS).access(HAS_ROLE_ADMIN_OR_PROJECT_MANAGER)
				.antMatchers(ADMIN_ONLY_URLS).access(HAS_ROLE_ADMIN)
				.anyRequest().authenticated()

				// main entry point for unauthenticated users
				.and()
				.exceptionHandling()
				.authenticationEntryPoint(mainEntryPoint())

				.and().formLogin()
				.loginProcessingUrl(CONTROLLERS_ROOT_URL + "/login")
				.successHandler(singlePageAppAuthenticationSuccessHandler())
				.failureHandler(singlePageAppAuthenticationFailureHandler())
				// http sessions management
				.and()
					.sessionManagement()
					.maximumSessions(-1)
					.sessionRegistry(sessionRegistry());
			// @formatter:on
		}

		@Bean
		public SinglePageAppAuthenticationSuccessHandler singlePageAppAuthenticationSuccessHandler() {
			return new SinglePageAppAuthenticationSuccessHandler();
		}

		@Bean
		public SinglePageAppAuthenticationFailureHandler singlePageAppAuthenticationFailureHandler() {
			return new SinglePageAppAuthenticationFailureHandler();
		}

		@Bean
		public AuthenticationEntryPoint mainEntryPoint() {
			return new MainEntryPoint(entryPointUrl);
		}

		private String[] gatherIgnoringCsrfUrlPatterns() {
			List<String> result = new ArrayList<>(Collections.singletonList(ALTERNATE_AUTH_PATH));

			for (SecurityExemptionEndPoint endPoint : securityExemptionEndPoints) {
				result.addAll(endPoint.getIgnoreCsrfUrlPatterns());
			}

			return result.toArray(new String[0]);
		}

		private String[] gatherIgnoringAuthUrlPatterns() {
			List<String> result = new ArrayList<>(DEFAULT_IGNORE_AUTH_URLS);

			for (SecurityExemptionEndPoint endPoint : securityExemptionEndPoints) {
				result.addAll(endPoint.getIgnoreAuthUrlPatterns());
			}

			return result.toArray(new String[0]);
		}
	}

	/**
	 * [Issue #6900]
	 * <p>
	 * The base64-encoded token for basic auth has a charset too. Spring Sec expects it to be UTF-8 but many people around expects it
	 * to be Latin-1 (iso-8859-1). This configurer allows to configure the desired encoding.
	 *
	 * @author bsiri
	 */
	private static final class BasicAuthCharsetConfigurer implements ObjectPostProcessor<BasicAuthenticationFilter> {

		private final String charset;

		public BasicAuthCharsetConfigurer(String charset) {
			super();
			this.charset = charset;
		}

		@Override
		public <O extends BasicAuthenticationFilter> O postProcess(O object) {
			object.setCredentialsCharset(charset);
			return object;
		}
	}
}


/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.customreport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.EntityReference;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeType;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.domain.customreport.CustomReportFolder;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.exception.library.RightsUnsuficientsForOperationException;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.deletion.OperationReport;
import org.squashtest.tm.service.deletion.SuppressionPreviewReport;
import org.squashtest.tm.service.display.workspace.tree.SingleHierarchyTreeBrowser;
import org.squashtest.tm.service.display.workspace.tree.TreeNodeCollectorService;
import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;
import org.squashtest.tm.service.internal.display.grid.TreeRequest;
import org.squashtest.tm.web.backend.controller.RequestParams;
import org.squashtest.tm.web.backend.controller.form.model.CustomReportFolderFormModel;
import org.squashtest.tm.web.backend.controller.form.model.DashboardFormModel;
import org.squashtest.tm.web.backend.controller.form.model.EntityFormModelValidator;
import org.squashtest.tm.web.backend.controller.form.model.NodeList;
import org.squashtest.tm.web.backend.controller.form.model.RefreshTreeNodeModel;
import org.squashtest.tm.web.backend.controller.navigation.Messages;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@RestController
@RequestMapping(path = "backend/custom-report-tree")
public class CustomReportLibraryNavigationController {

	public static final Logger LOGGER = LoggerFactory.getLogger(CustomReportLibraryNavigationController.class);

	private final SingleHierarchyTreeBrowser treeBrowser;
	private final TreeNodeCollectorService treeNodeCollectorService;
	private final CustomReportLibraryNodeService customReportLibraryNodeService;
	private final MessageSource messageSource;

	private static final String ADD_DASHBOARD = "add-dashboard";
	private static final String ADD_CUSTOM_REPORT_FOLDER = "add-custom-report-folder";

	public CustomReportLibraryNavigationController(SingleHierarchyTreeBrowser treeBrowser,
												   TreeNodeCollectorService treeNodeCollectorService,
												   CustomReportLibraryNodeService customReportLibraryNodeService,
												   MessageSource messageSource) {
		this.treeBrowser = treeBrowser;
		this.treeNodeCollectorService = treeNodeCollectorService;
		this.customReportLibraryNodeService = customReportLibraryNodeService;
		this.messageSource = messageSource;
	}

	@RequestMapping(method = RequestMethod.POST)
	public TreeGridResponse getInitialRows(@RequestBody TreeRequest treeRequest) {
		return treeBrowser.getInitialTree(NodeWorkspace.CUSTOM_REPORT,
			NodeReference.fromNodeIds(treeRequest.getOpenedNodes()),
			NodeReference.fromNodeIds(treeRequest.getSelectedNodes()));
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{ids}/content")
	public TreeGridResponse getChildren(@PathVariable List<String> ids) {
		Set<NodeReference> nodeReference = NodeReference.fromNodeIds(ids);
		return treeBrowser.findSubHierarchy(nodeReference, new HashSet<>(nodeReference));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/refresh")
	public TreeGridResponse refreshTree(@RequestBody RefreshTreeNodeModel refreshTreeNodeModel) {
		return treeBrowser.findSubHierarchy(NodeReference.fromNodeIds(refreshTreeNodeModel.getNodeIds()),
			new HashSet<>(refreshTreeNodeModel.getNodeList().getNodeReferences()));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{ids}/refresh")
	public TreeGridResponse refreshNodes(@PathVariable List<String> ids, @RequestBody NodeList openedNodes) {
		return treeBrowser.findSubHierarchy(NodeReference.fromNodeIds(ids), new HashSet<>(openedNodes.getNodeReferences()));
	}

	@ResponseBody
	@RequestMapping(value = "/deletion-simulation/{nodeIds}", method = RequestMethod.GET)
	public Messages simulateNodeDeletion(@PathVariable(RequestParams.NODE_IDS) List<Long> nodeIds,
										 Locale locale) {
		List<SuppressionPreviewReport> reportList = customReportLibraryNodeService.simulateDeletion(nodeIds);
		Messages messages = new Messages();
		for (SuppressionPreviewReport report : reportList) {
			messages.addMessage(report.toString(messageSource, locale));
		}
		return messages;
	}

	@ResponseBody
	@RequestMapping(value = "/{nodeIds}", method = RequestMethod.DELETE)
	public OperationReport confirmNodeDeletion(@PathVariable(RequestParams.NODE_IDS) List<Long> nodeIds) {
		return customReportLibraryNodeService.delete(nodeIds);
	}

	@ResponseBody
	@RequestMapping(value = "new-folder", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public DataRow addNewFolder(@RequestBody CustomReportFolderFormModel folderModel) throws BindException {

		validateCustomReportFolderFormModel(folderModel);
		CustomReportFolder customReportFolder = folderModel.getCustomReportFolder();
		CustomReportLibraryNode newNode;

		String serializedParentEntityReference = folderModel.getParentEntityReference();
		EntityReference parentEntityReference = EntityReference.fromNodeId(serializedParentEntityReference);
		switch (parentEntityReference.getType()) {
			case CUSTOM_REPORT_LIBRARY:
			case CUSTOM_REPORT_FOLDER:
				newNode = customReportLibraryNodeService.createNewNode(parentEntityReference.getId(), customReportFolder);
				break;
			default:
				throw new IllegalArgumentException("This entity type is not handled " + serializedParentEntityReference);
		}
		return treeNodeCollectorService.collectNode(NodeType.CUSTOM_REPORT_FOLDER, newNode);
	}

	@ResponseBody
	@RequestMapping(value = "/{destinationId}/content/paste", method = RequestMethod.POST)
	public void copyNodes(@RequestBody() NodeList nodeList, @PathVariable("destinationId") String destinationId) {
		NodeReference nodeReference = NodeReference.fromNodeId(destinationId);
		List<Long> copiedNodeIds = nodeList.getIds();

		try {
			switch (nodeReference.getNodeType()) {
				case CUSTOM_REPORT_FOLDER:
				case CUSTOM_REPORT_LIBRARY:
					this.customReportLibraryNodeService.copyNodes(copiedNodeIds, nodeReference.getId());
					break;
				default:
					throw new IllegalArgumentException("copy nodes : specified destination type doesn't exists : "
						+ nodeReference.getNodeType());
			}
		} catch (AccessDeniedException ade) {
			throw new RightsUnsuficientsForOperationException(ade);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/{destinationRef}/content/move", method = RequestMethod.POST)
	public void moveNodes(@RequestBody() NodeList nodeList, @PathVariable("destinationRef") String destinationRef) {
		NodeReference nodeReference = NodeReference.fromNodeId(destinationRef);
		Long destinationId = nodeReference.getId();
		NodeType destinationType = nodeReference.getNodeType();
		List<Long> movedNodeIds = nodeList.getIds();

		try {
			switch (destinationType) {
				case CUSTOM_REPORT_FOLDER:
				case CUSTOM_REPORT_LIBRARY:
					customReportLibraryNodeService.moveNodes(movedNodeIds,destinationId);
					break;
				default:
					throw new IllegalArgumentException("move nodes : specified destination type doesn't exists : "
						+ destinationType);
			}
		} catch (AccessDeniedException ade) {
			throw new RightsUnsuficientsForOperationException(ade);
		}

	}

	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("new-dashboard")
	public DataRow addNewDashBoard(@RequestBody DashboardFormModel dashboardFormModel) throws BindException {
		validateDashboardFormModel(dashboardFormModel);
		Long parentId = dashboardFormModel.getParentNodeReference().getId();
		CustomReportLibraryNode newNode = this.customReportLibraryNodeService.createNewNode(parentId, dashboardFormModel.getDashboard());
		return treeNodeCollectorService.collectNode(NodeType.CUSTOM_REPORT_DASHBOARD, newNode);
	}

	private void validateCustomReportFolderFormModel(CustomReportFolderFormModel folderModel) throws BindException {
		BindingResult validation = new BeanPropertyBindingResult(folderModel, ADD_CUSTOM_REPORT_FOLDER);
		EntityFormModelValidator entityFormModelValidator = new EntityFormModelValidator();
		entityFormModelValidator.validate(folderModel, validation);

		if (validation.hasErrors()) {
			throw new BindException(validation);
		}
	}


	private void validateDashboardFormModel(DashboardFormModel dashboardFormModel) throws BindException {
		BindingResult validation = new BeanPropertyBindingResult(dashboardFormModel, ADD_DASHBOARD);
		EntityFormModelValidator entityFormModelValidator = new EntityFormModelValidator();
		entityFormModelValidator.validate(dashboardFormModel, validation);

		if (validation.hasErrors()) {
			throw new BindException(validation);
		}
	}
}

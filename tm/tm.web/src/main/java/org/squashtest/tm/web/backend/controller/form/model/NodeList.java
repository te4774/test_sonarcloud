/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.form.model;

import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeReferences;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NodeList {
	private List<String> references = new ArrayList<>();

	public List<String> getReferences() {
		return references;
	}

	public List<Long> getIds() {
		return references.stream()
			.map(NodeReference::fromNodeId)
			.map(NodeReference::getId)
			.collect(Collectors.toList());
	}

	public List<NodeReference> getNodeReferences() {
		return references.stream()
			.map(NodeReference::fromNodeId)
			.collect(Collectors.toList());
	}

	public NodeReferences asNodeReferences() {
		List<NodeReference> nodeReferences = references.stream()
			.map(NodeReference::fromNodeId)
			.collect(Collectors.toList());
		return new NodeReferences(nodeReferences);
	}

	public void setReferences(List<String> references) {
		this.references = references;
	}
}

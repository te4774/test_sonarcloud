/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.execution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.tm.core.foundation.exception.ActionException;
import org.squashtest.tm.domain.campaign.TestPlanOwner;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.exception.execution.TestPlanItemNotExecutableException;
import org.squashtest.tm.exception.execution.TestPlanTerminatedOrNoStepsException;
import org.squashtest.tm.service.campaign.EntityFinder;
import org.squashtest.tm.service.campaign.TestPlanExecutionProcessingService;
import org.squashtest.tm.service.execution.ExecutionProcessingService;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;

import javax.inject.Inject;
import java.util.List;

/**
 * Abstract class for controller classes responsible of running an entire test plan
 * @param <E> class of the entity owning the test plan (for now, a {@link org.squashtest.tm.domain.campaign.TestSuite} or an {@link org.squashtest.tm.domain.campaign.Iteration}
 * @author aguilhem
 */
public abstract class AbstractTestPlanExecutionRunnerController<E extends TestPlanOwner> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTestPlanExecutionRunnerController.class);

	@Inject
	protected TestPlanExecutionProcessingService<E> testPlanExecutionRunner;

	@Inject
	protected ExecutionProcessingService executionRunner;

	@Inject
	EntityFinder<E> entityFinder;

	/**
	 * @param testPlanOwnerId id of the {@link TestPlanOwner}
	 */
	public Execution startResume(long testPlanOwnerId) {
		boolean hasDeletedTestCaseInTestPlan = hasDeletedTestCaseInTestPlan(testPlanOwnerId);
		if (!hasDeletedTestCaseInTestPlan) {
			try {
				return testPlanExecutionRunner.startResume(testPlanOwnerId);
			} catch (TestPlanItemNotExecutableException e) {
				throw new TestPlanTerminatedOrNoStepsException(e);
			}
		} else {
			throw getTestPlanHasDeletedTestCaseException();
		}
	}

	public Execution relaunch(long testPlanOwnerId) {
		boolean hasDeletedTestCaseInTestPlan = hasDeletedTestCaseInTestPlan(testPlanOwnerId);
		if (!hasDeletedTestCaseInTestPlan) {
			try {
				return testPlanExecutionRunner.relaunch(testPlanOwnerId);
			} catch (TestPlanItemNotExecutableException e) {
				throw new TestPlanTerminatedOrNoStepsException(e);
			}
		} else {
			throw getTestPlanHasDeletedTestCaseException();
		}
	}

	/**
	 * Getter for the {@link TestPlanOwner} specific "HasDeletedTestCaseException"
	 *
	 * @return {@link TestPlanOwner} specific "HasDeletedTestCaseException".
	 * For example, {@link org.squashtest.tm.exception.execution.IterationTestPlanHasDeletedTestCaseException}
	 */
	abstract ActionException getTestPlanHasDeletedTestCaseException();

	/**
	 * Issue 7366
	 * Method which tests if a {@link TestPlanOwner} has at least one deleted test case in its test plan
	 *
	 * @param testPlanOwnerId id of the {@link TestPlanOwner}
	 * @return true if test suite has at least one deleted test case in its test plan
	 */
	abstract boolean hasDeletedTestCaseInTestPlan(long testPlanOwnerId);

	protected static class FiltersContainer {
		private List<GridFilterValue> filterValues;

		public List<GridFilterValue> getFilterValues() {
			return filterValues;
		}

		public void setFilterValues(List<GridFilterValue> filterValues) {
			this.filterValues = filterValues;
		}
	}
}

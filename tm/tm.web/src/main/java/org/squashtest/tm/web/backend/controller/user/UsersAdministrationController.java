/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.synchronisation.RemoteSynchronisation;
import org.squashtest.tm.domain.users.UsersGroup;
import org.squashtest.tm.service.display.user.UserDisplayService;
import org.squashtest.tm.service.internal.display.dto.ProjectPermissionDto;
import org.squashtest.tm.service.internal.display.dto.UserAdminViewTeamDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.project.ProjectsPermissionManagementService;
import org.squashtest.tm.service.user.UserAdministrationService;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/backend/users")
public class UsersAdministrationController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UsersAdministrationController.class);
	private UserDisplayService userDisplayService;
	private UserAdministrationService adminService;
	private ProjectsPermissionManagementService permissionService;

	@Inject
	public UsersAdministrationController(UserDisplayService userDisplayService,
										 UserAdministrationService adminService,
										 ProjectsPermissionManagementService permissionService) {
		this.userDisplayService = userDisplayService;
		this.adminService = adminService;
		this.permissionService = permissionService;
	}

	@ResponseBody
	@PostMapping
	public GridResponse getAllUsers(@RequestBody GridRequest request) {
		return userDisplayService.findAll(request);
	}

	@ResponseBody
	@PostMapping(value = "/new")
	public Map<String, Long> addUser(@RequestBody @Valid UserForm userForm) {
		if (userForm.getPassword() == null) {
			adminService.createUserWithoutCredentials(userForm.getUser(), userForm.getGroupId());
		} else {
			adminService.addUser(userForm.getUser(), userForm.getGroupId(), userForm.getPassword());
		}

		return Collections.singletonMap("id", userForm.getUser().getId());
	}

	@ResponseBody
	@GetMapping(value = "/get-users-groups")
	public Map<String, List<UsersGroup>> getUsersGroups() {

		Map<String, List<UsersGroup>> response = new HashMap<>();

		response.put("usersGroups", adminService.findAllUsersGroupOrderedByQualifiedName());
		return response;
	}

	@RequestMapping(value = "/{userId}/login", method = RequestMethod.POST)
	@ResponseBody
	public void modifyLogin(@PathVariable long userId, @RequestBody UsersAdministrationController.UserPatch patch) {
		adminService.modifyUserLogin(userId, patch.getLogin());
	}

	@RequestMapping(value = "/{userId}/first-name", method = RequestMethod.POST)
	@ResponseBody
	public void modifyFirstName(@PathVariable long userId, @RequestBody UsersAdministrationController.UserPatch patch) {
		adminService.modifyUserFirstName(userId, patch.getFirstName());
	}

	@RequestMapping(value = "/{userId}/last-name", method = RequestMethod.POST)
	@ResponseBody
	public void modifyLastName(@PathVariable long userId, @RequestBody UsersAdministrationController.UserPatch patch) {
		adminService.modifyUserLastName(userId, patch.getLastName());
	}

	@RequestMapping(value = "/{userId}/email", method = RequestMethod.POST)
	@ResponseBody
	public void modifyEmail(@PathVariable long userId, @RequestBody UsersAdministrationController.UserPatch patch) {
		adminService.modifyUserEmail(userId, patch.getEmail());
	}

	@ResponseBody
	@RequestMapping(value = "/{userIds}", method = RequestMethod.DELETE)
	public void deleteUsers(@PathVariable("userIds") List<Long> userIds) {
		adminService.deleteUsers(userIds);
	}

	@GetMapping(value = "/{userId}/synchronisations")
	@ResponseBody
	public List<RemoteSynchronisation> getSynchronisationsByUser(@PathVariable("userId") Long userId) {
		return adminService.getSynchronisationsByUser(userId);
	}

	@ResponseBody
	@RequestMapping(value = "/{userIds}/deactivate", method = RequestMethod.POST)
	public void deactivateUsers(@PathVariable("userIds") List<Long> userIds) {
		adminService.deactivateUsers(userIds);
	}

	@ResponseBody
	@RequestMapping(value = "/{userIds}/activate", method = RequestMethod.POST)
	public void activateUsers(@PathVariable("userIds") List<Long> userIds) {
		adminService.activateUsers(userIds);
	}

	@ResponseBody
	@RequestMapping(value = "/{userId}/change-group/{groupId}", method = RequestMethod.POST)
	public void changeUserGroup(@PathVariable long userId, @PathVariable long groupId) {
		adminService.setUserGroupAuthority(userId, groupId);
	}

	@ResponseBody
	@RequestMapping(value = "/{userId}/reset-password", method = RequestMethod.POST)
	public void resetPassword(@PathVariable long userId, @RequestBody UserPatch patch) {
		LOGGER.trace("Reset password for user #" + userId);
		adminService.resetUserPassword(userId, patch.getPassword());
	}

	@ResponseBody
	@RequestMapping(value = "/{userId}/permissions/{projectIds}", method = RequestMethod.POST)
	public Map<String, List<ProjectPermissionDto>> addNewPermissions(@PathVariable long userId,
																	 @PathVariable List<Long> projectIds,
																	 @RequestParam String permission) {

		permissionService.addNewPermissionToProject(userId, projectIds, permission);
		return Collections.singletonMap("projectPermissions", userDisplayService.getProjectPermissions(userId));
	}

	@ResponseBody
	@RequestMapping(value = "/{userId}/permissions/{projectIds}", method = RequestMethod.DELETE)
	public void removePermissions(@PathVariable long userId,
								  @PathVariable List<Long> projectIds) {
		permissionService.removeProjectPermission(userId, projectIds);
	}

	@ResponseBody
	@RequestMapping(value = "/{userId}/teams/{partyIds}", method = RequestMethod.POST)
	public Map<String, List<UserAdminViewTeamDto>> associateTeams(@PathVariable long userId,
																  @PathVariable List<Long> partyIds) {
		adminService.associateToTeams(userId, partyIds);
		return Collections.singletonMap("teams", userDisplayService.getAssociatedTeams(userId));
	}

	@ResponseBody
	@RequestMapping(value = "/{userId}/teams/{partyIds}", method = RequestMethod.DELETE)
	public void disassociateTeams(@PathVariable long userId,
								  @PathVariable List<Long> partyIds) {
		adminService.deassociateTeams(userId, partyIds);
	}


	static class UserPatch {
		private String login;
		private String firstName;
		private String lastName;
		private String email;
		private String password;

		public String getLogin() {
			return login;
		}

		public void setLogin(String login) {
			this.login = login;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
	}
}

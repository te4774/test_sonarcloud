/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.test.automation.server.environments;

import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Base class for automated execution environments controllers. Each subclass has a different visibility based
 * on their respective endpoint prefix (see configuration in {@link org.squashtest.tm.web.config.WebSecurityConfig}).
 */
public abstract class AbstractTAEnvironmentsController {
	private final AutomatedExecutionEnvironmentService automatedExecutionEnvironmentService;
	private final StoredCredentialsManager storedCredentialsManager;

	protected AbstractTAEnvironmentsController(AutomatedExecutionEnvironmentService automatedExecutionEnvironmentService,
											   StoredCredentialsManager storedCredentialsManager) {
		this.automatedExecutionEnvironmentService = automatedExecutionEnvironmentService;
		this.storedCredentialsManager = storedCredentialsManager;
	}

	protected List<String> extractAllTags(List<AutomatedExecutionEnvironment> environments) {
		return environments.stream()
			.map(AutomatedExecutionEnvironment::getTags)
			.flatMap(Collection::stream)
			.distinct()
			.sorted()
			.collect(Collectors.toList());
	}

	protected List<AutomatedExecutionEnvironment> getAutomatedExecutionEnvironments(Long testAutomationServerId,
																				  	TokenAuthCredentials optionalProjectToken) {
		return optionalProjectToken != null ?
			automatedExecutionEnvironmentService.getAllAccessibleEnvironments(testAutomationServerId, optionalProjectToken)
			: automatedExecutionEnvironmentService.getAllAccessibleEnvironments(testAutomationServerId);
	}

	protected Optional<TokenAuthCredentials> findProjectToken(Long testAutomationServerId, Long projectId) {
		return Optional.ofNullable(storedCredentialsManager.findProjectCredentials(testAutomationServerId, projectId))
			.filter(credentials -> credentials instanceof TokenAuthCredentials)
			.map(credentials -> (TokenAuthCredentials) credentials);
	}
}

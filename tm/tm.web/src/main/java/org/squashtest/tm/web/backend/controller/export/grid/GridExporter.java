/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.export.grid;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.squashtest.tm.service.internal.display.grid.DataRow;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class GridExporter {

	protected final GridExportModel gridExportModel;
	protected final SXSSFWorkbook workbook;
	protected final Sheet sheet;
	protected final List<Row> rows = new ArrayList<>();
	protected Row row;

	protected GridExporter(GridExportModel gridExportModel) {
		this.gridExportModel = gridExportModel;
		workbook = new SXSSFWorkbook(100);
		sheet = workbook.createSheet();
	}

	protected void printRows() {
		for (int i = 0; i < gridExportModel.getExportedRows().size(); i++) {
			DataRow dataRow = gridExportModel.getExportedRows().get(i);
			printRow(i, dataRow);
		}
	}

	protected void printRow(int rowIndex, DataRow dataRow) {
		row = sheet.createRow(rowIndex + 1);
		for (int i = 0; i < gridExportModel.getExportedColumns().size(); i++) {
			GridExportModel.ExportColumnDefinition exportColumnDefinition = gridExportModel.getExportedColumns().get(i);
			Cell cell = row.createCell(i);
			Map<String, Object> data = dataRow.getData();
			Object value = Optional.ofNullable(data.get(exportColumnDefinition.getId())).orElse("");
			cell.setCellValue(value.toString());
		}
		rows.add(row);
	}

	protected void printHeaders() {
		row = sheet.createRow(0);
		for (int i = 0; i < gridExportModel.getExportedColumns().size(); i++) {
			GridExportModel.ExportColumnDefinition exportColumnDefinition = gridExportModel.getExportedColumns().get(i);
			Cell cell = row.createCell(i);
			cell.setCellValue(exportColumnDefinition.getLabel());
		}
		rows.add(row);
	}
	abstract File export() throws IOException;
}

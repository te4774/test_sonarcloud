/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.project;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.library.CannotDeleteProjectException;
import org.squashtest.tm.service.display.project.ProjectDisplayService;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.project.ProjectManagerService;
import org.squashtest.tm.web.backend.model.json.JsonProjectFromTemplate;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/backend/projects")
public class ProjectController {

	private final ProjectDisplayService projectDisplayService;
	private final ProjectManagerService projectManager;
	private final GenericProjectManagerService genericProjectManager;

	public ProjectController(ProjectManagerService projectManager,
							 GenericProjectManagerService genericProjectManager,
							 ProjectDisplayService projectDisplayService) {
		this.projectManager = projectManager;
		this.genericProjectManager = genericProjectManager;
		this.projectDisplayService = projectDisplayService;
	}

	@ResponseBody
	@ResponseStatus(value = HttpStatus.CREATED)
	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public Map<String, Object> createProjectFromTemplate(@Valid @RequestBody JsonProjectFromTemplate jsonProjectFromTemplate) {
		try {
			if (jsonProjectFromTemplate.isFromTemplate()) {
				projectManager.addProjectFromTemplate(jsonProjectFromTemplate.getProject(),
					jsonProjectFromTemplate.getTemplateId(), jsonProjectFromTemplate.getParams());
				return Collections.singletonMap("id", jsonProjectFromTemplate.getProject().getId());
			} else {
				Project project = jsonProjectFromTemplate.getProject();
				genericProjectManager.persist(project);
				return Collections.singletonMap("id", project.getId());
			}
		} catch (NameAlreadyInUseException ex) {
			ex.setObjectName("add-project-from-template");
			throw ex;
		}
	}

	@ResponseBody
	@ResponseStatus(value = HttpStatus.CREATED)
	@RequestMapping(value = "/new-template", method = RequestMethod.POST)
	public Map<String, Object> createNewTemplate(@RequestBody @Valid ProjectTemplate template) {
		try {
			genericProjectManager.persist(template);
			return Collections.singletonMap("id", template.getId());
		} catch (NameAlreadyInUseException ex) {
			ex.setObjectName("add-template");
			throw ex;
		}
	}

	@ResponseBody
	@DeleteMapping(value = "/{projectId}")
	public void deleteProject(@PathVariable long projectId) {
		if (projectDisplayService.hasProjectData(projectId)) {
			throw new CannotDeleteProjectException();
		}

		projectManager.deleteProject(projectId);
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(value = "coerce-into-template")
	public void coerceProjectIntoTemplate(@RequestBody Map<String, Long> payload) {
		final long projectId = payload.get("projectId");
		genericProjectManager.coerceProjectIntoTemplate(projectId);
	}
}

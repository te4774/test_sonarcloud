/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.campaign;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignStatus;
import org.squashtest.tm.service.campaign.CampaignModificationService;
import org.squashtest.tm.service.campaign.CustomCampaignModificationService;
import org.squashtest.tm.service.campaign.IterationModificationService;
import org.squashtest.tm.service.display.campaign.CampaignDisplayService;
import org.squashtest.tm.service.internal.display.dto.campaign.IterationPlanningDto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("backend/campaign/{campaignId}")
public class CampaignViewModificationController {
	private final CustomCampaignModificationService customCampaignModificationService;
	private final CampaignModificationService campaignModificationService;
	private final IterationModificationService iterationModificationService;
	private final CampaignDisplayService campaignDisplayService;

	public CampaignViewModificationController(CustomCampaignModificationService customCampaignModificationService,
											  CampaignModificationService campaignModificationService,
											  IterationModificationService iterationModificationService,
											  CampaignDisplayService campaignDisplayService) {
		this.customCampaignModificationService = customCampaignModificationService;
		this.campaignModificationService = campaignModificationService;
		this.iterationModificationService = iterationModificationService;
		this.campaignDisplayService = campaignDisplayService;
	}

	@ResponseBody
	@PostMapping(value = "/name")
	public void rename(@PathVariable Long campaignId, @RequestBody CampaignPatch patch) {
		customCampaignModificationService.rename(campaignId, patch.getName());
	}

	@ResponseBody
	@PostMapping(value = "/reference")
	public void changeReference(@PathVariable Long campaignId, @RequestBody CampaignPatch patch) {
		campaignModificationService.changeReference(campaignId, patch.getReference());
	}

	@ResponseBody
	@PostMapping(value = "/description")
	public void changeDescription(@PathVariable Long campaignId, @RequestBody CampaignPatch patch) {
		campaignModificationService.changeDescription(campaignId, patch.getDescription());
	}

	@ResponseBody
	@PostMapping(value = "/campaign-status")
	public void changeCampaignStatus(@PathVariable Long campaignId, @RequestBody CampaignPatch patch) {
		campaignModificationService.changeStatus(campaignId, CampaignStatus.valueOf(patch.getCampaignStatus()));
	}

	@ResponseBody
	@PostMapping(value = "/milestone/{milestoneId}")
	public void bindMilestone(@PathVariable Long campaignId, @PathVariable Long milestoneId) {
		campaignModificationService.bindMilestone(campaignId, milestoneId);
	}

	@ResponseBody
	@DeleteMapping(value = "/milestone")
	public void unbindMilestones(@PathVariable Long campaignId, @RequestBody List<Long> milestoneIds) {
		campaignModificationService.unbindMilestones(campaignId, milestoneIds);
	}

	@ResponseBody
	@DeleteMapping(value = "/milestone/{milestoneId}")
	public void unbindSingleMilestone(@PathVariable Long campaignId, @PathVariable Long milestoneId) {
		campaignModificationService.unbindSingleMilestone(campaignId, milestoneId);
	}

	@ResponseBody
	@PostMapping(value = "/scheduled-start-date")
	public void setScheduledStart(@PathVariable long campaignId,
								  @RequestBody CampaignPatch campaignPatch) {
		campaignModificationService.changeScheduledStartDate(campaignId, campaignPatch.getScheduledStartDate());
	}

	@PostMapping(value = "/scheduled-end-date")
	@ResponseBody
	public void setScheduledEnd(@PathVariable long campaignId,
								@RequestBody CampaignPatch campaignPatch) {
		campaignModificationService.changeScheduledEndDate(campaignId, campaignPatch.getScheduledEndDate());
	}

	/**
	 * the next functions may receive null arguments : empty string
	 **/

	@PostMapping(value = "/actual-start-date")
	@ResponseBody
	public void setActualStart(@PathVariable long campaignId,
							   @RequestBody CampaignPatch campaignPatch) {
		campaignModificationService.changeActualStartDate(campaignId, campaignPatch.getActualStartDate());
	}

	@PostMapping(value = "/actual-end-date")
	@ResponseBody
	public void setActualEnd(@PathVariable long campaignId,
							 @RequestBody CampaignPatch campaignPatch) {

		campaignModificationService.changeActualEndDate(campaignId, campaignPatch.getActualEndDate());

	}

	@PostMapping(value = "/actual-start-auto")
	@ResponseBody
	public Date setActualStartAuto(@PathVariable long campaignId,
								   @RequestBody CampaignPatch campaignPatch) {
		campaignModificationService.changeActualStartAuto(campaignId, campaignPatch.getActualStartAuto());
		Campaign campaign = campaignModificationService.findById(campaignId);
		return campaign.getActualStartDate();
	}

	@PostMapping(value = "/actual-end-auto")
	@ResponseBody
	public Date setActualEndAuto(@PathVariable long campaignId,
								 @RequestBody CampaignPatch campaignPatch) {
		campaignModificationService.changeActualEndAuto(campaignId, campaignPatch.getActualEndAuto());
		Campaign campaign = campaignModificationService.findById(campaignId);
		return campaign.getActualEndDate();

	}

	@GetMapping(value = "/iterations")
	@ResponseBody
	public List<IterationPlanningDto> getIterations(@PathVariable long campaignId) {
		return campaignDisplayService.findIterationsPlanningByCampaign(campaignId);
	}

	@PostMapping(value = "/iterations/planning")
	@ResponseBody
	public void setIterationsPlanning(@RequestBody IterationPlanningPatch patch) {
		for (IterationPlanningDto iter : patch.iterationPlannings) {
			iterationModificationService.changeScheduledStartDate(iter.getId(), iter.getScheduledStartDate());
			iterationModificationService.changeScheduledEndDate(iter.getId(), iter.getScheduledEndDate());
		}
	}

	static class IterationPlanningPatch {
		List<IterationPlanningDto> iterationPlannings = new ArrayList<>();

		public List<IterationPlanningDto> getIterationPlannings() {
			return iterationPlannings;
		}

		public void setIterationPlannings(List<IterationPlanningDto> iterationPlannings) {
			this.iterationPlannings = iterationPlannings;
		}
	}

	static class CampaignPatch {
		private String name;
		private String reference;
		private String description;
		private String campaignStatus;
		private Date scheduledEndDate;
		private Date scheduledStartDate;
		private Date actualEndDate;
		private Date actualStartDate;
		private Boolean actualEndAuto;
		private Boolean actualStartAuto;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getReference() {
			return reference;
		}

		public void setReference(String reference) {
			this.reference = reference;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public Date getScheduledEndDate() {
			return scheduledEndDate;
		}

		public void setScheduledEndDate(Date scheduledEndDate) {
			this.scheduledEndDate = scheduledEndDate;
		}

		public Date getScheduledStartDate() {
			return scheduledStartDate;
		}

		public void setScheduledStartDate(Date scheduledStartDate) {
			this.scheduledStartDate = scheduledStartDate;
		}

		public Date getActualEndDate() {
			return actualEndDate;
		}

		public void setActualEndDate(Date actualEndDate) {
			this.actualEndDate = actualEndDate;
		}

		public Date getActualStartDate() {
			return actualStartDate;
		}

		public void setActualStartDate(Date actualStartDate) {
			this.actualStartDate = actualStartDate;
		}

		public Boolean getActualEndAuto() {
			return actualEndAuto;
		}

		public void setActualEndAuto(Boolean actualEndAuto) {
			this.actualEndAuto = actualEndAuto;
		}

		public Boolean getActualStartAuto() {
			return actualStartAuto;
		}

		public void setActualStartAuto(Boolean actualStartAuto) {
			this.actualStartAuto = actualStartAuto;
		}

		public String getCampaignStatus() {
			return campaignStatus;
		}

		public void setCampaignStatus(String campaignStatus) {
			this.campaignStatus = campaignStatus;
		}
	}
}

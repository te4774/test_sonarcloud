/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.test.automation.server;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.squashtest.tm.core.foundation.exception.InvalidUrlException;
import org.squashtest.tm.core.foundation.lang.UrlUtils;
import org.squashtest.tm.domain.servers.AuthenticationProtocol;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.exception.WrongUrlException;
import org.squashtest.tm.service.display.test.automation.server.TestAutomationServerDisplayService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService;
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableValueService;
import org.squashtest.tm.service.internal.display.dto.BoundEnvironmentVariableDto;
import org.squashtest.tm.service.internal.display.dto.TestAutomationServerDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.servers.ManageableCredentials;
import org.squashtest.tm.service.servers.ServerAuthConfiguration;
import org.squashtest.tm.service.testautomation.TestAutomationServerManagerService;
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService;
import org.squashtest.tm.service.thirdpartyserver.ThirdPartyServerCredentialsService;
import org.squashtest.tm.web.backend.controller.form.model.TestAutomationServerFormModel;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/backend/test-automation-servers")
public class TestAutomationServersController {

	private static final String TEST_AUTOMATION_SERVER_URL = "/{serverIds}";

	private final TestAutomationServerDisplayService testAutomationServerDisplayService;

	private final TestAutomationServerManagerService testAutomationServerManagerService;

	private final ThirdPartyServerCredentialsService credentialsService;

	private final AutomatedExecutionEnvironmentService automatedExecutionEnvironmentService;

	private final EnvironmentVariableBindingService environmentVariableBindingService;

	private final EnvironmentVariableValueService environmentVariableValueService;


	@Inject
	public TestAutomationServersController(TestAutomationServerDisplayService testAutomationServerDisplayService,
										   TestAutomationServerManagerService testAutomationServerManagerService,
										   ThirdPartyServerCredentialsService credentialsService,
										   AutomatedExecutionEnvironmentService automatedExecutionEnvironmentService, EnvironmentVariableBindingService environmentVariableBindingService, EnvironmentVariableValueService environmentVariableValueService) {
		this.testAutomationServerDisplayService = testAutomationServerDisplayService;
		this.testAutomationServerManagerService = testAutomationServerManagerService;
		this.credentialsService = credentialsService;
		this. automatedExecutionEnvironmentService = automatedExecutionEnvironmentService;
		this.environmentVariableBindingService = environmentVariableBindingService;
		this.environmentVariableValueService = environmentVariableValueService;
	}

	@ResponseBody
	@GetMapping
	public Map<String, List<TestAutomationServerDto>> getAll() {
		return Collections.singletonMap("testAutomationServers", testAutomationServerDisplayService.findAll());
	}

	@ResponseBody
	@PostMapping
	public GridResponse getAllAutomationServers(@RequestBody GridRequest request) {
		return testAutomationServerDisplayService.getTestAutomationServerGrid(request);
	}

	@ResponseBody
	@DeleteMapping(TEST_AUTOMATION_SERVER_URL)
	public void deleteAutomationServers(@PathVariable("serverIds") List<Long> serverIds) {
		testAutomationServerManagerService.deleteServer(serverIds);
	}

	@PostMapping("/new")
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public Map<String, Object> createNewAutomationServer(@RequestBody TestAutomationServerFormModel formModel) {
		checkURL(formModel.getBaseUrl());
		TestAutomationServer server = formModel.getTestAutomationServer();
		testAutomationServerManagerService.persist(server);
		return Collections.singletonMap("id", server.getId());
	}

	@PostMapping("/{testAutomationServerId}/name")
	@ResponseBody
	public void changeName(@PathVariable long testAutomationServerId, @RequestBody TestAutomationServerPatch patch) {
		testAutomationServerManagerService.changeName(testAutomationServerId, patch.getName());
	}

	@PostMapping("/{testAutomationServerId}/base-url")
	@ResponseBody
	public void changeURL(@PathVariable long testAutomationServerId, @RequestBody TestAutomationServerPatch patch) {
		URL url = checkURL(patch.getBaseUrl());
		testAutomationServerManagerService.changeURL(testAutomationServerId, url);
	}

	@PostMapping("/{testAutomationServerId}/description")
	@ResponseBody
	public void changeDescription(@PathVariable long testAutomationServerId, @RequestBody TestAutomationServerPatch patch) {
		testAutomationServerManagerService.changeDescription(testAutomationServerId, patch.getDescription());
	}

	@PostMapping("/{testAutomationServerId}/manual-selection")
	@ResponseBody
	public void changeManualSelection(@PathVariable long testAutomationServerId, @RequestBody TestAutomationServerPatch patch) {
		testAutomationServerManagerService.changeManualSlaveSelection(testAutomationServerId, patch.isManualSlaveSelection());
	}

	@PostMapping("/{testAutomationServerId}/authentication-protocol")
	@ResponseBody
	public void changeAuthProtocol(@PathVariable long testAutomationServerId, @RequestBody TestAutomationServerPatch patch) {
		AuthenticationProtocol protocol = Enum.valueOf(AuthenticationProtocol.class, patch.getProtocol());
		credentialsService.changeAuthenticationProtocol(testAutomationServerId, protocol);
	}

	@PostMapping("/{testAutomationServerId}/authentication-protocol/configuration")
	@ResponseBody
	public void saveAuthConfiguration(@PathVariable long testAutomationServerId, @Valid @RequestBody ServerAuthConfiguration configuration) {
		credentialsService.storeAuthConfiguration(testAutomationServerId, configuration);
	}


	@PostMapping("/{testAutomationServerId}/credentials")
	@ResponseBody
	public void storeCredentials(@PathVariable long testAutomationServerId, @RequestBody ManageableCredentials credentials) {
		credentialsService.storeCredentials(testAutomationServerId, credentials);

		if (automatedExecutionEnvironmentService.doesServerSupportAutomatedExecutionEnvironments(testAutomationServerId)) {
			testAutomationServerManagerService.clearEnvironmentTags(testAutomationServerId);
		}
	}

	@DeleteMapping("/{testAutomationServerId}/credentials")
	@ResponseBody
	public void deleteCredentials(@PathVariable long testAutomationServerId) {
		credentialsService.deleteCredentials(testAutomationServerId);
	}

	@PostMapping("/{testAutomationServerId}/observer-url")
	@ResponseBody
	public void changeObserverURL(@PathVariable long testAutomationServerId, @RequestBody TestAutomationServerPatch patch) {
		final String observerUrl = patch.getObserverUrl();

		if (StringUtils.isBlank(observerUrl)) {
			testAutomationServerManagerService.clearObserverURL(testAutomationServerId);
		} else {
			URL url = checkURL(observerUrl);
			testAutomationServerManagerService.changeObserverURL(testAutomationServerId, url);
		}
	}

	@PostMapping("/{testAutomationServerId}/environment-variables/bind/{environmentVariableIds}")
	@ResponseBody
	public Map<String, List<BoundEnvironmentVariableDto>> bindEnvironmentVariables(@PathVariable Long testAutomationServerId, @PathVariable List<Long> environmentVariableIds) {
		environmentVariableBindingService.createNewBindings(testAutomationServerId, environmentVariableIds);
		Map<String, List<BoundEnvironmentVariableDto>> result = new HashMap<>();
		result.put("boundEnvironmentVariables",testAutomationServerDisplayService.getEnvironmentVariablesDtoByServerId(testAutomationServerId));
		return result;
	}

	@PostMapping("/{testAutomationServerId}/environment-variables/unbind/{environmentVariableIds}")
	@ResponseBody
	public void unbindEnvironmentVariables(@PathVariable Long testAutomationServerId, @PathVariable List<Long> environmentVariableIds) {
		environmentVariableBindingService.unbind(testAutomationServerId, environmentVariableIds);
	}

	@PostMapping("/{testAutomationServerId}/environment-variables/value/{environmentVariableId}")
	@ResponseBody
	public void changeEnvironmentVariableValue(@PathVariable Long testAutomationServerId,
											   @PathVariable Long environmentVariableId,
											   @RequestBody EnvironmentVariableValuePatch patch) {
		environmentVariableValueService.editEnvironmentVariableValueFromServer(testAutomationServerId,
			environmentVariableId, patch.getValue());
	}

	public static class TestAutomationServerPatch {
		String name;
		String baseUrl;
		String description;
		boolean manualSlaveSelection;
		String protocol;
		String observerUrl;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getBaseUrl() {
			return baseUrl;
		}

		public void setBaseUrl(String baseUrl) {
			this.baseUrl = baseUrl;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public boolean isManualSlaveSelection() {
			return manualSlaveSelection;
		}

		public void setManualSlaveSelection(boolean manualSlaveSelection) {
			this.manualSlaveSelection = manualSlaveSelection;
		}

		public String getProtocol() {
			return protocol;
		}

		public void setProtocol(String protocol) {
			this.protocol = protocol;
		}

		public String getObserverUrl() {
			return observerUrl;
		}

		public void setObserverUrl(String observerUrl) {
			this.observerUrl = observerUrl;
		}
	}

	private URL checkURL(String urlToTest) {
		try {
			return UrlUtils.toUrl(urlToTest);
		} catch (InvalidUrlException iue) {
			throw new WrongUrlException("baseUrl", iue);
		}
	}

	public static class EnvironmentVariableValuePatch {
		String value;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.requirement;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.display.requirements.RequirementDisplayService;
import org.squashtest.tm.service.display.requirements.RequirementPathFinderService;
import org.squashtest.tm.service.internal.display.dto.requirement.HighLevelRequirementVersionDto.LinkedLowLevelRequirementDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionBundleStatsDto;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionDto.LinkedHighLevelRequirementDto;
import org.squashtest.tm.service.internal.display.dto.requirement.VerifyingTestCaseDto;
import org.squashtest.tm.service.internal.dto.HighLevelRequirementExceptionSummary;
import org.squashtest.tm.service.requirement.HighLevelRequirementService;
import org.squashtest.tm.service.requirement.RequirementHelper;
import org.squashtest.tm.service.requirement.RequirementStatisticsService;

import java.util.List;

@RestController
@RequestMapping("backend/high-level-requirement")
public class HighLevelRequirementController {

    private final HighLevelRequirementService highLevelRequirementService;
    private final RequirementDisplayService requirementDisplayService;
    private final RequirementStatisticsService requirementStatisticsService;
    private final RequirementHelper requirementHelper;
	private final RequirementPathFinderService requirementsPathService;

    public HighLevelRequirementController(HighLevelRequirementService highLevelRequirementService,
										  RequirementDisplayService requirementDisplayService,
										  RequirementStatisticsService requirementStatisticsService,
										  RequirementHelper requirementHelper,
										  RequirementPathFinderService requirementsPathService) {
        this.highLevelRequirementService = highLevelRequirementService;
        this.requirementDisplayService = requirementDisplayService;
        this.requirementStatisticsService = requirementStatisticsService;
        this.requirementHelper = requirementHelper;
		this.requirementsPathService = requirementsPathService;
	}

    @PostMapping("/{lowLevelRequirementId}/link/{highLevelRequirementId}")
    public LinkedHighLevelRequirementDto linkToHighLevelRequirement(@PathVariable Long lowLevelRequirementId, @PathVariable Long highLevelRequirementId) {
        if (requirementHelper.checkIfRequirementIsNotChild(lowLevelRequirementId)) {
            highLevelRequirementService.linkToHighLevelRequirement(highLevelRequirementId, lowLevelRequirementId);
			LinkedHighLevelRequirementDto linkedHighLevelRequirementDto = requirementDisplayService.findLinkedHighLevelRequirement(lowLevelRequirementId);
            String path = requirementsPathService.buildRequirementLinkPath(linkedHighLevelRequirementDto.getRequirementId(), linkedHighLevelRequirementDto.getProjectName());
			linkedHighLevelRequirementDto.setPath(path);
			return linkedHighLevelRequirementDto;
        } else {
            // TODO throw proper error
            return new LinkedHighLevelRequirementDto();
        }
    }

    @DeleteMapping("/{lowLevelRequirementId}/unlink")
    public void unlinkToHighLevelRequirement(@PathVariable Long lowLevelRequirementId) {
        if (requirementHelper.checkIfRequirementIsNotChild(lowLevelRequirementId)) {
            highLevelRequirementService.unlinkToHighLevelRequirement(lowLevelRequirementId);
        }
    }

    @PostMapping("/{highLevelRequirementId}/bind-requirement-to-high-level-requirement/{lowLevelRequirementIds}")
    public BindRequirementToHighLevelRequirementOperationReport bindRequirementToHighLevelRequirement(@PathVariable Long highLevelRequirementId, @PathVariable List<Long> lowLevelRequirementIds) {
        HighLevelRequirementExceptionSummary summary = new HighLevelRequirementExceptionSummary();
        highLevelRequirementService.bindRequirementsToHighLevelRequirement(highLevelRequirementId, lowLevelRequirementIds, summary);

        BindRequirementToHighLevelRequirementOperationReport report =
                new BindRequirementToHighLevelRequirementOperationReport(
                        highLevelRequirementService.findLinkedLowLevelRequirements(highLevelRequirementId),
						requirementDisplayService.findVerifyingTestCasesByHighLvlReqIdForCurrentVersion(highLevelRequirementId),
                        requirementStatisticsService.findCoveragesStatsByHighLvlReqId(highLevelRequirementId),
                        requirementDisplayService.countIssuesByHighLvlReqId(highLevelRequirementId));
        report.setSummary(summary);

        return report;
    }

    @DeleteMapping("/{highLevelRequirementId}/unbind-requirement-from-high-level-requirement/{lowLevelRequirementIds}")
    public BindRequirementToHighLevelRequirementOperationReport unbindRequirementFromHighLevelRequirement(@PathVariable Long highLevelRequirementId, @PathVariable List<Long> lowLevelRequirementIds) {
        this.highLevelRequirementService.unbindRequirementFromHighLevelRequirement(highLevelRequirementId, lowLevelRequirementIds);
        return new BindRequirementToHighLevelRequirementOperationReport(
                highLevelRequirementService.findLinkedLowLevelRequirements(highLevelRequirementId),
				requirementDisplayService.findVerifyingTestCasesByHighLvlReqIdForCurrentVersion(highLevelRequirementId),
                requirementStatisticsService.findCoveragesStatsByHighLvlReqId(highLevelRequirementId),
                requirementDisplayService.countIssuesByHighLvlReqId(highLevelRequirementId));
    }

	@RequestMapping(value = "/convert-into-high-level-requirement/{requirementId}", method = RequestMethod.POST)
	@ResponseBody
	public void convertIntoHighLevelRequirement(@PathVariable long requirementId) {
		this.highLevelRequirementService.convertIntoHighLevelRequirement(requirementId);
	}

    @RequestMapping(value = "/convert-into-standard-requirement/{requirementId}", method = RequestMethod.POST)
    @ResponseBody
    public void convertIntoStandardRequirement(@PathVariable long requirementId) {
        this.highLevelRequirementService.convertIntoStandardRequirement(requirementId);
    }

    static class BindRequirementToHighLevelRequirementOperationReport {

        private List<LinkedLowLevelRequirementDto> linkedLowLevelRequirements;
        private List<VerifyingTestCaseDto> verifyingTestCases;
        private RequirementVersionBundleStatsDto requirementStats;
        private Integer nbIssues;
        private HighLevelRequirementExceptionSummary summary;

        public BindRequirementToHighLevelRequirementOperationReport(List<LinkedLowLevelRequirementDto> linkedLowLevelRequirements,
                                                                    List<VerifyingTestCaseDto> verifyingTestCases,
                                                                    RequirementVersionBundleStatsDto requirementStats,
                                                                    Integer nbIssues) {
            this.linkedLowLevelRequirements = linkedLowLevelRequirements;
            this.verifyingTestCases = verifyingTestCases;
            this.requirementStats = requirementStats;
            this.nbIssues = nbIssues;
        }

        public List<LinkedLowLevelRequirementDto> getLinkedLowLevelRequirements() {
            return linkedLowLevelRequirements;
        }

        public void setLinkedLowLevelRequirements(List<LinkedLowLevelRequirementDto> linkedLowLevelRequirements) {
            this.linkedLowLevelRequirements = linkedLowLevelRequirements;
        }

        public RequirementVersionBundleStatsDto getRequirementStats() {
            return requirementStats;
        }

        public void setRequirementStats(RequirementVersionBundleStatsDto requirementStats) {
            this.requirementStats = requirementStats;
        }

        public List<VerifyingTestCaseDto> getVerifyingTestCases() {
			return verifyingTestCases;
		}

		public void setVerifyingTestCases(List<VerifyingTestCaseDto> verifyingTestCases) {
			this.verifyingTestCases = verifyingTestCases;
		}

        public Integer getNbIssues() {
            return nbIssues;
        }

        public void setNbIssues(Integer nbIssues) {
            this.nbIssues = nbIssues;
        }

        public HighLevelRequirementExceptionSummary getSummary() {
            return summary;
        }

        public void setSummary(HighLevelRequirementExceptionSummary summary) {
            this.summary = summary;
        }
    }
}

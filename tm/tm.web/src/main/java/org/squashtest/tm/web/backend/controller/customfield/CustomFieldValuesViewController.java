/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.customfield;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.service.customfield.CustomFieldValueManagerService;
import org.squashtest.tm.service.internal.dto.CustomFieldJsonConverter;
import org.squashtest.tm.service.internal.dto.CustomFieldValueModel;
import org.squashtest.tm.web.backend.controller.AcceptHeaders;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("backend/custom-fields/values")
public class CustomFieldValuesViewController {
	private static final String BOUND_ENTITY_TYPE = "boundEntityType";

	private static final String BOUND_ENTITY_ID = "boundEntityId";

	@Inject
	private CustomFieldValueManagerService managerService;

	@Inject
	private CustomFieldJsonConverter converter;


	@RequestMapping(method = RequestMethod.GET, params = {BOUND_ENTITY_ID, BOUND_ENTITY_TYPE}, headers = AcceptHeaders.CONTENT_JSON)
	@ResponseBody
	public List<CustomFieldValueModel> getCustomFieldValuesForEntity(@RequestParam(BOUND_ENTITY_ID) long id,
																	 @RequestParam(BOUND_ENTITY_TYPE) BindableEntity entityType) {

		List<CustomFieldValue> values = managerService.findAllCustomFieldValues(id, entityType);

		return valuesToJson(values);

	}


	@RequestMapping(value = "/{id}/single-value", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public void updateSingleCustomValue(@PathVariable long id, @RequestBody SingleValueRawModel singleValueRawModel) {
		managerService.changeValue(id, singleValueRawModel.toRawValue());
	}

	@RequestMapping(value = "/{id}/multi-value", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public void updateMultiCustomValue(@PathVariable long id, @RequestBody MultiValueRawModel multiValueRawModel) {
		managerService.changeValue(id, multiValueRawModel.toRawValue());
	}


	private List<CustomFieldValueModel> valuesToJson(List<CustomFieldValue> values) {
		List<CustomFieldValueModel> models = new LinkedList<>();
		for (CustomFieldValue value : values) {
			models.add(converter.toJson(value));
		}

		return models;
	}
}

class SingleValueRawModel {

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	RawValue toRawValue() {
		RawValue rawValue = new RawValue();
		rawValue.setValue(this.getValue());
		return rawValue;
	}
}

class MultiValueRawModel {

	private List<String> values;

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	RawValue toRawValue() {
		RawValue rawValue = new RawValue();
		rawValue.setValues(this.getValues());
		return rawValue;
	}
}

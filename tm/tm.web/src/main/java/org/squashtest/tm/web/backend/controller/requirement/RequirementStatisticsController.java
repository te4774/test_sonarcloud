/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.requirement;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.NodeReferences;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.service.statistics.requirement.RequirementStatisticsBundle;
import org.squashtest.tm.web.backend.controller.form.model.NodeList;

import java.util.Set;


@RestController
@RequestMapping("/backend/requirement/statistics")
public class RequirementStatisticsController {

	private final RequirementLibraryNavigationService requirementLibraryNavigationService;

	public RequirementStatisticsController(RequirementLibraryNavigationService requirementLibraryNavigationService) {
		this.requirementLibraryNavigationService = requirementLibraryNavigationService;
	}

	@ResponseBody
	@PostMapping
	public RequirementStatisticsBundle getStatistics(@RequestBody RequirementNodeList nodeList) {
		NodeReferences nodeReferences = nodeList.asNodeReferences();
		Set<Long> libraryIds = nodeReferences.extractLibraryIds();
		Set<Long> nodeIds = nodeReferences.extractNonLibraryIds();
		return requirementLibraryNavigationService.getStatisticsForSelection(libraryIds, nodeIds, nodeList.isExtendedHighLvlReqScope());
	}

	public static class RequirementNodeList extends NodeList {
		private boolean isExtendedHighLvlReqScope;

		public boolean isExtendedHighLvlReqScope() {
			return isExtendedHighLvlReqScope;
		}

		public void setExtendedHighLvlReqScope(boolean extendedHighLvlReqScope) {
			isExtendedHighLvlReqScope = extendedHighLvlReqScope;
		}
	}

}

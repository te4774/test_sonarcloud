/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.execution;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.core.foundation.exception.ActionException;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.exception.execution.IterationTestPlanHasDeletedTestCaseException;
import org.squashtest.tm.service.campaign.TestPlanExecutionProcessingService;
import org.squashtest.tm.service.internal.display.dto.execution.TestPlanResume;
import org.squashtest.tm.service.internal.display.dto.execution.TestPlanResume.TestSuiteTestPlanResume;
import org.squashtest.tm.web.backend.controller.form.model.IdList;

import javax.inject.Inject;

import static java.util.Objects.nonNull;

/**
 * @author jthebault
 */
@RestController
@RequestMapping("/backend/test-suite/{testSuiteId}/test-plan")
public class TestSuiteExecutionRunnerController extends AbstractTestPlanExecutionRunnerController<TestSuite> {

	@Inject
	private TestPlanExecutionProcessingService<TestSuite> testSuiteExecutionProcessingService;

	public TestSuiteExecutionRunnerController() {
		super();
	}

	@RequestMapping(value = "/resume", method = RequestMethod.POST)
	public TestSuiteTestPlanResume resumeTestSuite(@PathVariable long testSuiteId) {
		Execution execution = super.startResume(testSuiteId);
		boolean hasNextTestCase = hasNextTestCase(testSuiteId, execution);
		return new TestSuiteTestPlanResume(testSuiteId, execution, hasNextTestCase);
	}

	@RequestMapping(value = "/resume-filtered-selection", method = RequestMethod.POST)
	public TestPlanResume resumeIterationWithFilter(@PathVariable long testSuiteId, @RequestBody FiltersContainer filtersContainer) {
		return testPlanExecutionRunner.resumeWithFilteredTestPlan(testSuiteId, filtersContainer.getFilterValues());
	}

	@RequestMapping(value = "/relaunch", method = RequestMethod.POST)
	public TestSuiteTestPlanResume relaunchTestSuite(@PathVariable long testSuiteId) {
		Execution execution = super.relaunch(testSuiteId);
		boolean hasNextTestCase = hasNextTestCase(testSuiteId, execution);
		return new TestSuiteTestPlanResume(testSuiteId, execution, hasNextTestCase);
	}

	@RequestMapping(value = "/relaunch-filtered-selection", method = RequestMethod.POST)
	public TestPlanResume relaunchIterationWithFilter(@PathVariable long testSuiteId, @RequestBody FiltersContainer filtersContainer) {
		return testPlanExecutionRunner.relaunchFilteredTestPlan(testSuiteId, filtersContainer.getFilterValues());
	}

	@Override
	ActionException getTestPlanHasDeletedTestCaseException() {
		return new IterationTestPlanHasDeletedTestCaseException();
	}

	@RequestMapping(value = "/{testPlanItemId}/next-execution")
	public TestSuiteTestPlanResume moveToNextTestCase(@PathVariable("testPlanItemId") long testPlanItemId,
													  @PathVariable("testSuiteId") long testSuiteId) {
		Execution execution = testPlanExecutionRunner.startResumeNextExecution(testSuiteId, testPlanItemId);
		boolean hasNextTestCase = hasNextTestCase(testSuiteId, execution);
		return new TestSuiteTestPlanResume(testSuiteId, execution, hasNextTestCase);
	}

	@RequestMapping(value = "/{testPlanItemId}/next-execution-filtered-selection")
	public TestPlanResume moveToNextTestCaseOfFilteredTestPlan(
		@PathVariable("testPlanItemId") long testPlanItemId,
		@PathVariable("testSuiteId") long testSuiteId,
		@RequestBody IdList partialTestPlanItemIds) {
		return testPlanExecutionRunner.resumeNextExecutionOfFilteredTestPlan(testSuiteId, testPlanItemId, partialTestPlanItemIds.getIds());
	}

	private boolean hasNextTestCase(long testSuiteId, Execution execution) {
		boolean hasNextTestCase = false;
		if (nonNull(execution)) {
			hasNextTestCase = testSuiteExecutionProcessingService.hasMoreExecutableItems(testSuiteId, execution.getTestPlan().getId());
		}
		return hasNextTestCase;
	}

	@Override
	boolean hasDeletedTestCaseInTestPlan(long testSuiteId) {
		return entityFinder.findById(testSuiteId).getTestPlan().stream().anyMatch(c -> c.getReferencedTestCase() == null);
	}
}

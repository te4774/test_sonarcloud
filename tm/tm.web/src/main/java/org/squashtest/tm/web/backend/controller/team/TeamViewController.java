/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.team;

import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.service.display.team.TeamDisplayService;
import org.squashtest.tm.service.internal.display.dto.TeamAdminViewDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TeamMemberDto;
import org.squashtest.tm.service.project.ProjectsPermissionManagementService;
import org.squashtest.tm.service.user.TeamModificationService;
import org.squashtest.tm.web.backend.controller.project.ProjectModel;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/backend/team-view")
public class TeamViewController {

	private TeamDisplayService teamDisplayService;
	private ProjectsPermissionManagementService permissionService;
	private TeamModificationService teamModificationService;

	@Inject
    TeamViewController(TeamDisplayService teamDisplayService,
					   ProjectsPermissionManagementService permissionService,
					   TeamModificationService teamModificationService) {
		this.teamDisplayService = teamDisplayService;
		this.permissionService = permissionService;
		this.teamModificationService = teamModificationService;
	}

	@RequestMapping(value = "/{teamId}", method = RequestMethod.GET)
	@ResponseBody
	public TeamAdminViewDto getTeamView(@PathVariable long teamId) {
		return teamDisplayService.getTeamView(teamId);
	}

	@GetMapping(value = "/{teamId}/projects-without-permission")
	public List<ProjectModel> getProjectWithoutPermission(@PathVariable long teamId){

		List<GenericProject> projectList = permissionService.findProjectWithoutPermissionByParty(teamId, Sort.by(Sort.Direction.ASC, "name"));

		List<ProjectModel> projectModelList = new ArrayList<>();
		if (projectList != null) {
			for (GenericProject project : projectList) {
				projectModelList.add(new ProjectModel(project));
			}
		}

		return projectModelList;
	}

	@RequestMapping(value = "/{teamId}/non-members", headers = "Accept=application/json")
	@ResponseBody
	public List<TeamMemberDto> getNonMembers(@PathVariable long teamId) {
		List<User> nonMembers = teamModificationService.findAllNonMemberUsers(teamId);
		List<TeamMemberDto> nonMembersListDto = new ArrayList<>();

		nonMembers.forEach(member -> {
			TeamMemberDto memberDto = new TeamMemberDto();
			memberDto.setPartyId(member.getId());
			memberDto.setActive(member.getActive());
			memberDto.setFirstName(member.getFirstName());
			memberDto.setLastName(member.getLastName());
			memberDto.setLogin(member.getLogin());
			nonMembersListDto.add(memberDto);
		});
		return nonMembersListDto;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.infolist;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.service.infolist.InfoListBindingManagerService;

import javax.inject.Inject;
import java.util.Map;

@Controller
@RequestMapping("/backend/info-list-binding")
public class InfoListBindingController {

	private InfoListBindingManagerService infoListBindingManagerService;

	@Inject
    InfoListBindingController(InfoListBindingManagerService infoListBindingManagerService) {
		this.infoListBindingManagerService = infoListBindingManagerService;
	}


	@ResponseBody
	@RequestMapping(value = "/project/{projectId}/category", method = RequestMethod.POST)
	public void bindCategoryToProject(@PathVariable Long projectId, @RequestBody Map<String, Long> request) {
		infoListBindingManagerService.bindListToProjectReqCategory(request.get("infoListId"), projectId);
	}

	@ResponseBody
	@RequestMapping(value = "/project/{projectId}/nature", method = RequestMethod.POST)
	public void bindNatureToProject(@PathVariable Long projectId, @RequestBody Map<String, Long> request) {
		infoListBindingManagerService.bindListToProjectTcNature(request.get("infoListId"), projectId);
	}

	@ResponseBody
	@RequestMapping(value = "/project/{projectId}/type", method = RequestMethod.POST)
	public void bindTypeToProject(@PathVariable Long projectId, @RequestBody Map<String, Long> request) {
		infoListBindingManagerService.bindListToProjectTcType(request.get("infoListId"), projectId);
	}
}

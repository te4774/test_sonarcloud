/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.model.testautomation;

import org.squashtest.tm.service.internal.display.grid.DataRow;
import org.squashtest.tm.service.internal.display.grid.TreeGridResponse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class TATestFlatTree {

	private static String NAME_FIELD ="NAME";
	private static String TYPE_FIELD ="TYPE";

	public TreeGridResponse flatTree(Collection<TATestNode> taTestNodes) {

		List<DataRow> dataRows = new ArrayList<>();
		for (TATestNode taTestNode : taTestNodes) {

			DataRow row = new DataRow();
			row.setId(taTestNode.getAttr().getName());
			HashMap<String, Object> data = new HashMap();
			data.put(NAME_FIELD, taTestNode.getAttr().getName());
			data.put(TYPE_FIELD, taTestNode.getAttr().getRestype());
			row.setData(data);
			if (!taTestNode.getChildren().isEmpty()) {
				row.setState(DataRow.State.closed);
				List<String> childIds = taTestNode.getChildren().stream().map(childNode -> generateChildId(childNode, row.getId())).collect(Collectors.toList());
				row.setChildren(childIds);
				appendChildren(taTestNode.getChildren(), dataRows, row.getId());
			}
			dataRows.add(row);
		}

		TreeGridResponse treeGridResponse = new TreeGridResponse();
		treeGridResponse.setDataRows(dataRows);
		return treeGridResponse;
	}

	private String generateChildId(TATestNode child, String parentId) {
		return parentId + '/' + child.getAttr().getName();
	}

	private void appendChildren(Collection<TATestNode> nodes, List<DataRow> rows, String parentId) {
		for (TATestNode node : nodes) {
			DataRow row = new DataRow();
			row.setId(generateChildId(node, parentId));
			HashMap<String, Object> data = new HashMap();
			data.put(NAME_FIELD, node.getAttr().getName());
			data.put(TYPE_FIELD, node.getAttr().getRestype());
			row.setData(data);
			row.setParentRowId(parentId);

			if (!node.getChildren().isEmpty()) {
				List<String> childIds = node.getChildren().stream().map(childNode -> generateChildId(childNode, row.getId())).collect(Collectors.toList());
				row.setChildren(childIds);
				row.setState(DataRow.State.closed);
				appendChildren(node.getChildren(), rows, row.getId());
			}
			rows.add(row);
		}
	}

}

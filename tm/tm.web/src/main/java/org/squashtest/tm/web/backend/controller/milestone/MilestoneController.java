/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.milestone;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.milestone.MilestoneRange;
import org.squashtest.tm.domain.milestone.MilestoneStatus;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.milestone.MilestoneLabelAlreadyExistsException;
import org.squashtest.tm.exception.milestone.MilestoneRangeNotFoundException;
import org.squashtest.tm.exception.milestone.MilestoneWritePermissionDenied;
import org.squashtest.tm.service.display.milestone.MilestoneDisplayService;
import org.squashtest.tm.service.internal.display.dto.MilestoneDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.milestone.MilestoneBindingManagerService;
import org.squashtest.tm.service.milestone.MilestoneManagerService;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.user.UserAccountService;
import org.squashtest.tm.service.user.UserAdministrationService;
import org.squashtest.tm.web.backend.controller.form.model.MilestoneFormModel;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.squashtest.tm.api.security.acls.Roles.ROLE_ADMIN;

@Controller
@RequestMapping("/backend/milestones")
public class MilestoneController {

	private static final String MILESTONES_URL = "/{milestoneIds}";

	private final MilestoneDisplayService milestoneDisplayService;
	private final MilestoneManagerService milestoneManager;
	private final PermissionEvaluationService permissionEvaluationService;
	private final ProjectFinder projectFinder;
	private final UserAdministrationService userAdministrationService;
	private final UserAccountService userService;
	private final MilestoneBindingManagerService milestoneBindingManager;

	@Inject
	MilestoneController(MilestoneDisplayService milestoneDisplayService,
						MilestoneManagerService milestoneManager,
						PermissionEvaluationService permissionEvaluationService,
						ProjectFinder projectFinder,
						UserAdministrationService userAdministrationService,
						UserAccountService userService,
						MilestoneBindingManagerService milestoneBindingManager) {
		this.milestoneDisplayService = milestoneDisplayService;
		this.milestoneManager = milestoneManager;
		this.permissionEvaluationService = permissionEvaluationService;
		this.projectFinder = projectFinder;
		this.userAdministrationService = userAdministrationService;
		this.userService = userService;
		this.milestoneBindingManager = milestoneBindingManager;
	}

	@ResponseBody
	@PostMapping
	public GridResponse getAllMilestones(@RequestBody GridRequest request) {
		return milestoneDisplayService.findAll(request);
	}

	@ResponseBody
	@RequestMapping(value = MILESTONES_URL, method = RequestMethod.DELETE)
	public void deleteMilestone(@PathVariable("milestoneIds") List<Long> milestoneIds) {
		milestoneManager.removeMilestones(milestoneIds);
	}

	@ResponseBody
	@PostMapping(value="/new")
	public Map<String,Object> addMilestone(@Valid @RequestBody MilestoneFormModel milestoneFormModel) {
		Map<String, Object> response = new HashMap<>();
		Milestone milestone = milestoneFormModel.getMilestone();
		setRange(milestone);
		setPerimeter(milestone);

		try {
			milestoneManager.addMilestone(milestone);
			response.put("id", milestone.getId());
		} catch (MilestoneLabelAlreadyExistsException ex) {
			throw new NameAlreadyInUseException("Milestone", milestoneFormModel.getLabel(), "label");
		}

		return response;
	}

	private void setRange(Milestone milestone){
		if (permissionEvaluationService.hasRole(ROLE_ADMIN)) {
			milestone.setRange(MilestoneRange.GLOBAL);
		} else {
			milestone.setRange(MilestoneRange.RESTRICTED);
		}
	}

	private void setPerimeter(Milestone milestone){
		if (!permissionEvaluationService.hasRole(ROLE_ADMIN)) {
			List<GenericProject> projects = projectFinder.findAllICanManage();
			milestone.addProjectsToPerimeter(projects);
		}
	}

	@RequestMapping(value = "/{milestoneId}/label", method = RequestMethod.POST)
	@ResponseBody
	public void changeLabel(@PathVariable long milestoneId, @RequestBody MilestonePatch patch) {
		verifyCanEditMilestone(milestoneId);
		try {
			milestoneManager.changeLabel(milestoneId, patch.getLabel());
		} catch (MilestoneLabelAlreadyExistsException ex) {
			throw new NameAlreadyInUseException("Milestone", patch.getLabel(), "label");
		}
	}

	@ResponseBody
	@PostMapping(value="/{milestoneId}/status")
	public MilestoneDto changeStatus(@PathVariable("milestoneId") Long milestoneId, @RequestBody MilestonePatch patch) {
		verifyCanEditMilestone(milestoneId);
		MilestoneStatus status = MilestoneStatus.valueOf(patch.getStatus());
		milestoneManager.changeStatus(milestoneId, status);

		// A single service call would be more adapted here...
		if (status.equals(MilestoneStatus.PLANNED)) {
			milestoneManager.unbindAllObjects(milestoneId);
		}

		return milestoneDisplayService.getMilestoneView(milestoneId);
	}

	@ResponseBody
	@PostMapping(value="/{milestoneId}/end-date")
	public MilestoneDto changeEndDate(@PathVariable("milestoneId") Long milestoneId, @RequestBody MilestonePatch patch) {
		verifyCanEditMilestone(milestoneId);
		milestoneManager.changeEndDate(milestoneId, patch.getEndDate());
		return milestoneDisplayService.getMilestoneView(milestoneId);
	}

	@ResponseBody
	@PostMapping(value="/{milestoneId}/range")
	public MilestoneDto changeRange(@PathVariable("milestoneId") Long milestoneId, @RequestBody MilestonePatch patch) {
		milestoneManager.verifyCanEditMilestoneRange();
		checkRangeParameter(patch.getRange());

		if (patch.getRange().equals("RESTRICTED") && milestoneManager.isBoundToATemplate(milestoneId)) {
			milestoneBindingManager.unbindTemplateFrom(milestoneId);
		}

		milestoneManager.changeOwner(milestoneId, userService.findCurrentUser());
		milestoneManager.changeRange(milestoneId, MilestoneRange.valueOf(patch.getRange()));
		return milestoneDisplayService.getMilestoneView(milestoneId);
	}

	private void checkRangeParameter(String range) {
		try {
			MilestoneRange.valueOf(range);
		} catch (IllegalArgumentException ex) {
			throw new MilestoneRangeNotFoundException();
		}
	}

	@ResponseBody
	@PostMapping(value="/{milestoneId}/description")
	public MilestoneDto changeDescription(@PathVariable("milestoneId") Long milestoneId, @RequestBody MilestonePatch patch) {
		verifyCanEditMilestone(milestoneId);
		milestoneManager.changeDescription(milestoneId, patch.getDescription());
		return milestoneDisplayService.getMilestoneView(milestoneId);
	}

	@ResponseBody
	@PostMapping(value="/{milestoneId}/owner")
	public MilestoneDto changeOwner(@PathVariable("milestoneId") Long milestoneId, @RequestBody MilestonePatch patch) {
		verifyCanEditMilestone(milestoneId);
		User newOwner = userAdministrationService.findByLogin(patch.getOwnerLogin());
		milestoneManager.changeOwner(milestoneId, newOwner);
		return milestoneDisplayService.getMilestoneView(milestoneId);
	}

	@ResponseBody
	@GetMapping(value = "possible-owners")
	public Map<String, List<User>> findPossibleOwners() {
		List<User> sortedUsers = userAdministrationService.findAllAdminOrManager()
				.stream().sorted((o1, o2) -> o1.getLogin().compareToIgnoreCase(o2.getLogin()))
				.collect(Collectors.toList());
		return Collections.singletonMap("users", sortedUsers);
	}

	@ResponseBody
	@RequestMapping(value = "/{motherId}/clone", method = RequestMethod.POST)
	public  Map<String,Long> cloneMilestone(@PathVariable("motherId") long motherId, @RequestBody MilestoneDuplicationPatch patch) {
		Milestone milestone = new Milestone();
		milestone.setLabel(patch.getLabel());
		milestone.setStatus(patch.getStatus());
		milestone.setEndDate(patch.getEndDate());
		milestone.setDescription(patch.getDescription());

		if (permissionEvaluationService.hasRole(ROLE_ADMIN)) {
			//keep range for admin user
			milestone.setRange(milestoneManager.findById(motherId).getRange());
		} else {
			//set to restricted if non admin
			milestone.setRange(MilestoneRange.RESTRICTED);
		}

		try {
			milestoneManager.cloneMilestone(motherId, milestone, patch.isBindToRequirements(), patch.isBindToTestCases());
			return Collections.singletonMap("id", milestone.getId());
		} catch (MilestoneLabelAlreadyExistsException ex) {
			throw new NameAlreadyInUseException("Milestone", patch.getLabel(), "label");
		}
	}

	@ResponseBody
	@RequestMapping(value = "/{sourceId}/synchronize/{targetId}", method = RequestMethod.POST)
	public void synchronizeMilestone(@PathVariable("sourceId") long sourceId,
									 @PathVariable("targetId") long targetId,
									 @RequestBody SynchronizationParameters parameters) {
		milestoneManager.synchronize(sourceId, targetId, parameters.isExtendPerimeter(), parameters.isUnion());
	}

	public static class MilestonePatch {
		private String label;
		private String range;
		private Date endDate;
		private String status;
		private String description;
		private String ownerLogin;

		public String getLabel() { return label; }

		public void setLabel(String label) { this.label = label; }

		public String getRange() { return range; }

		public void setRange(String range) { this.range = range; }

		public Date getEndDate() { return endDate; }

		public void setEndDate(Date endDate) { this.endDate = endDate; }

		public String getStatus() { return status; }

		public void setStatus(String status) { this.status = status; }

		public String getDescription() { return description; }

		public void setDescription(String description) { this.description = description; }

		public String getOwnerLogin() { return ownerLogin; }

		public void setOwnerLogin(String ownerLogin) { this.ownerLogin = ownerLogin; }
	}

	public static class MilestoneDuplicationPatch {
		private String label;
		private MilestoneStatus status;
		private Date endDate;
		private String description;
		private boolean bindToRequirements;
		private boolean bindToTestCases;

		public String getLabel() { return label; }

		public void setLabel(String label) { this.label = label; }

		public MilestoneStatus getStatus() { return status; }

		public void setStatus(MilestoneStatus status) { this.status = status; }

		public Date getEndDate() { return endDate; }

		public void setEndDate(Date endDate) { this.endDate = endDate; }

		public String getDescription() { return description; }

		public void setDescription(String description) { this.description = description; }

		public boolean isBindToRequirements() {	return bindToRequirements; }

		public void setBindToRequirements(boolean bindToRequirements) { this.bindToRequirements = bindToRequirements; }

		public boolean isBindToTestCases() { return bindToTestCases; }

		public void setBindToTestCases(boolean bindToTestCases) { this.bindToTestCases = bindToTestCases; }
	}

	private void verifyCanEditMilestone(Long milestoneId) {
		try {
			this.milestoneManager.verifyCanEditMilestone(milestoneId);
		} catch(IllegalAccessError ex) {
			throw new MilestoneWritePermissionDenied();
		}
	}

	public static class SynchronizationParameters {
		boolean extendPerimeter;
		boolean union;

		public boolean isExtendPerimeter() {
			return extendPerimeter;
		}

		public void setExtendPerimeter(boolean extendPerimeter) {
			this.extendPerimeter = extendPerimeter;
		}

		public boolean isUnion() {
			return union;
		}

		public void setUnion(boolean isUnion) {
			union = isUnion;
		}
	}
}

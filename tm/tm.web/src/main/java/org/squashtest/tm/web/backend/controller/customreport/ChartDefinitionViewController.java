/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.customreport;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.domain.chart.ChartDefinition;
import org.squashtest.tm.domain.chart.ChartInstance;
import org.squashtest.tm.service.chart.ChartModificationService;
import org.squashtest.tm.service.customreport.CustomReportLibraryNodeService;
import org.squashtest.tm.service.internal.dto.json.JsonChartInstance;


@RestController
@RequestMapping("/backend/chart-definition-view")
public class ChartDefinitionViewController {

	private final CustomReportLibraryNodeService customReportLibraryNodeService;
	private final ChartModificationService chartService;

	public ChartDefinitionViewController(CustomReportLibraryNodeService customReportLibraryNodeService,
										 ChartModificationService chartService) {
		this.customReportLibraryNodeService = customReportLibraryNodeService;
		this.chartService = chartService;
	}

	@RequestMapping(value = "/{customReportLibraryNodeId}", method = RequestMethod.GET)
	public JsonChartInstance getChartDetails(@PathVariable Long customReportLibraryNodeId){
		ChartDefinition chartDef = customReportLibraryNodeService.findChartDefinitionByNodeId(customReportLibraryNodeId);
		ChartInstance instance = chartService.generateChart(chartDef.getId(),null,null);
		JsonChartInstance jsonChartInstance = new JsonChartInstance(instance);
		jsonChartInstance.setCustomReportLibraryNodeId(customReportLibraryNodeId);
		return jsonChartInstance;
	}

}

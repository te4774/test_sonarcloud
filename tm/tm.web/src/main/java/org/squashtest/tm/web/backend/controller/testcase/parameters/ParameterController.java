/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.testcase.parameters;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.exception.DuplicateNameException;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.service.display.testcase.TestCaseDisplayService;
import org.squashtest.tm.service.internal.display.testcase.parameter.TestCaseParameterRenameOperationReport;
import org.squashtest.tm.service.testcase.ParameterFinder;
import org.squashtest.tm.service.testcase.ParameterModificationService;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "backend/parameters")
public class ParameterController {
	private static final String PARAMETER_ID_URL = "/{parameterId}";
	private static final String PARAMETER_USED = "PARAMETER_USED";

	private ParameterModificationService parameterService;

	private TestCaseDisplayService testCaseDisplayService;

	private ParameterFinder parameterFinder;

	public ParameterController(ParameterModificationService parameterService,
							   TestCaseDisplayService testCaseDisplayService,
							   ParameterFinder parameterFinder) {
		this.parameterService = parameterService;
		this.testCaseDisplayService = testCaseDisplayService;
		this.parameterFinder = parameterFinder;
	}

	@RequestMapping(value = PARAMETER_ID_URL + "/used", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Boolean> isUsedParameter(@PathVariable Long parameterId) {
		Map<String, Boolean> used = new HashMap<>();
		used.put(PARAMETER_USED, parameterService.isUsed(parameterId));
		return used;
	}

	@RequestMapping(value = PARAMETER_ID_URL, method = RequestMethod.DELETE)
	@ResponseBody
	public void removeParameter(@PathVariable Long parameterId) {
		parameterService.removeById(parameterId);
	}

	@RequestMapping(value = PARAMETER_ID_URL + "/rename", method = RequestMethod.POST)
	@ResponseBody
	public TestCaseParameterRenameOperationReport renameParameter(@PathVariable Long parameterId,
											 @RequestBody ParameterPatch patch) {
		try {
			parameterService.changeName(parameterId, patch.getName());
			Parameter parameter = parameterFinder.findById(parameterId);
			return new TestCaseParameterRenameOperationReport(
				testCaseDisplayService.findTestStepsByTestCaseId(parameter.getTestCase().getId()),
				parameter.getTestCase().getPrerequisite());
		} catch (DuplicateNameException ex) {
			throw new NameAlreadyInUseException("Parameter", patch.getName());
		}
	}

	@RequestMapping(value = PARAMETER_ID_URL + "/description", method = RequestMethod.POST)
	@ResponseBody
	public void changeDescription(@PathVariable Long parameterId, @RequestBody ParameterPatch patch) {
		parameterService.changeDescription(parameterId, patch.getDescription());
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.requirements.links;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.requirement.RequirementVersionLinkType;
import org.squashtest.tm.service.display.requirements.links.RequirementsLinksDisplayService;
import org.squashtest.tm.service.internal.display.dto.RequirementsLinksTypeDto;
import org.squashtest.tm.service.requirement.RequirementVersionLinkTypeManagerService;
import org.squashtest.tm.web.backend.controller.form.model.RequirementsLinkFormModel;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/backend/requirements-links")
public class RequirementsLinksController {

	private static final String REQUIREMENTS_LINKS_URL = "{requirementsLinksId}";

	@Inject
	RequirementsLinksDisplayService requirementsLinksDisplayService;

	@Inject
	RequirementVersionLinkTypeManagerService requirementVersionLinkTypeManagerService;

	@ResponseBody
	@PostMapping
	public RequirementLinkWrapper getAllRequirementsLinks() {
		return new RequirementLinkWrapper(requirementsLinksDisplayService.findAllRequirementsLinksType());
	}

	@ResponseBody
	@PostMapping(value="/new")
	public Map<String,Object> addRequirementsLink(@Valid @RequestBody RequirementsLinkFormModel requirementsLinkFormModel) {
		Map<String, Object> tempReturn = new HashMap<>();

			RequirementVersionLinkType requirementVersionLinkType = requirementsLinkFormModel.getRequirementsLink();
			requirementVersionLinkTypeManagerService.addLinkType(requirementVersionLinkType);
			tempReturn.put("id", requirementVersionLinkType.getId());

		return tempReturn;
	}

	@ResponseBody
	@RequestMapping(value = REQUIREMENTS_LINKS_URL, method = RequestMethod.DELETE)
	public void deleteRequirementsLink(@PathVariable("requirementsLinksId") List<Long> requirementsLinksId) {
		requirementVersionLinkTypeManagerService.deleteLinkTypes(requirementsLinksId);
	}

	static class RequirementLinkWrapper {
		List<RequirementsLinksTypeDto> requirementLinks ;

		public RequirementLinkWrapper(List<RequirementsLinksTypeDto> requirementLinks) {
			this.requirementLinks = requirementLinks;
		}

		public List<RequirementsLinksTypeDto> getRequirementLinks() {
			return requirementLinks;
		}

		public void setRequirementLinks(List<RequirementsLinksTypeDto> requirementLinks) {
			this.requirementLinks = requirementLinks;
		}
	}
}

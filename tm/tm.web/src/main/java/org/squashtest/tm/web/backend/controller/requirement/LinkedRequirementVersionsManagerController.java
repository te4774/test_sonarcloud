/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.requirement;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.exception.requirement.VerifiedRequirementException;
import org.squashtest.tm.exception.requirement.link.LinkedRequirementVersionException;
import org.squashtest.tm.service.display.requirements.RequirementDisplayService;
import org.squashtest.tm.service.internal.display.dto.requirement.RequirementVersionLinkDto;
import org.squashtest.tm.service.requirement.LinkedRequirementVersionManagerService;
import org.squashtest.tm.web.backend.helper.LinkedRequirementVersionActionSummaryBuilder;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * New Controller for the management of Requirement Versions linked to other Requirement Versions.
 *
 */
// XSS OK
@Controller
@RequestMapping("backend/requirement-version/{requirementVersionId}/linked-requirement-versions")
public class LinkedRequirementVersionsManagerController {

	private static final String IS_RELATED_ID_A_NODE_ID = "isRelatedIdANodeId";
	private static final String REQUIREMENT_VERSION_ID = "requirementVersionId";

	private final LinkedRequirementVersionManagerService linkedReqVersionManager;

	private final RequirementDisplayService requirementDisplayService;

	public LinkedRequirementVersionsManagerController(
		LinkedRequirementVersionManagerService linkedReqVersionManager,
		RequirementDisplayService requirementDisplayService
	) {
		this.linkedReqVersionManager = linkedReqVersionManager;
		this.requirementDisplayService = requirementDisplayService;
	}

	@ResponseBody
	@PostMapping
	public ChangeLinkedRequirementOperationReport addLinkWithVersionIdAndNodeId(
		@PathVariable(REQUIREMENT_VERSION_ID) long requirementVersionId, @RequestBody RequirementVersionLinkPatch requirementVersionLinkPatch
	) {
		Map<String, Object> summary = new HashMap<>();
		for (Long ids : requirementVersionLinkPatch.requirementNodesIds) {
			Collection<LinkedRequirementVersionException> rejections = linkedReqVersionManager.addLinkWithNodeIds(requirementVersionId, ids,
				requirementVersionLinkPatch.reqVersionLinkTypeId, requirementVersionLinkPatch.reqVersionLinkTypeDirection);
			summary.putAll(buildSummary(rejections));
		}
		List<RequirementVersionLinkDto> requirementVersionLinks = requirementDisplayService.findLinkedRequirementsByRequirementVersionId(requirementVersionId);
		ChangeLinkedRequirementOperationReport report = new ChangeLinkedRequirementOperationReport(requirementVersionLinks);
		report.setSummary(summary);

		return report;
	}

	@ResponseBody
	@GetMapping(value = "/{relatedIds}")
	public Map<String, String> getRequirementVersionInformation(@PathVariable List<Long> relatedIds) {
		return requirementDisplayService.findRequirementVersionNamesByRequirementIds(relatedIds);
	}

	@ResponseBody
	@DeleteMapping(value = "/{requirementLinkIds}")
	public ChangeLinkedRequirementOperationReport removeLinkedRequirementsFromRequirement(
		@PathVariable(REQUIREMENT_VERSION_ID) long requirementVersionId,
		@PathVariable("requirementLinkIds") List<Long> requirementLinkIds) {
		linkedReqVersionManager.removeLinkedRequirementVersionsFromRequirementVersion(requirementVersionId, requirementLinkIds);
		List<RequirementVersionLinkDto> requirementVersionLinkDtos = requirementDisplayService.findLinkedRequirementsByRequirementVersionId(requirementVersionId);
		return new ChangeLinkedRequirementOperationReport(requirementVersionLinkDtos);
	}

	@ResponseBody
	@PostMapping(value = "/update")
	public List<RequirementVersionLinkDto> updateLinkTypeAndDirection(
		@PathVariable(REQUIREMENT_VERSION_ID) long requirementVersionId,
		@RequestBody RequirementVersionLinkPatch requirementVersionLinkPatch) {
		for (Long paramRelatedId : requirementVersionLinkPatch.requirementNodesIds) {
			long relatedId = paramRelatedId;
			linkedReqVersionManager.updateLinkTypeAndDirection(
				requirementVersionId, relatedId, requirementVersionLinkPatch.isRelatedIdANodeId,
				requirementVersionLinkPatch.reqVersionLinkTypeId, requirementVersionLinkPatch.reqVersionLinkTypeDirection);
		}
		return requirementDisplayService.findLinkedRequirementsByRequirementVersionId(requirementVersionId);
	}

	private Map<String, Object> buildSummary(Collection<LinkedRequirementVersionException> rejections) {
		return LinkedRequirementVersionActionSummaryBuilder.buildAddActionSummary(rejections);
	}

	static class RequirementVersionLinkPatch {
		private List<Long> requirementNodesIds;
		private Long reqVersionLinkTypeId;
		private boolean reqVersionLinkTypeDirection;
		private boolean isRelatedIdANodeId;

		public List<Long> getRequirementNodesIds() {
			return requirementNodesIds;
		}

		public void setRequirementNodesIds(List<Long> requirementNodesIds) {
			this.requirementNodesIds = requirementNodesIds;
		}

		public Long getReqVersionLinkTypeId() {
			return reqVersionLinkTypeId;
		}

		public void setReqVersionLinkTypeId(Long reqVersionLinkTypeId) {
			this.reqVersionLinkTypeId = reqVersionLinkTypeId;
		}

		public boolean isReqVersionLinkTypeDirection() {
			return reqVersionLinkTypeDirection;
		}

		public void setReqVersionLinkTypeDirection(boolean reqVersionLinkTypeDirection) {
			this.reqVersionLinkTypeDirection = reqVersionLinkTypeDirection;
		}

		public boolean isRelatedIdANodeId() {
			return isRelatedIdANodeId;
		}

		public void setRelatedIdANodeId(boolean relatedIdANodeId) {
			isRelatedIdANodeId = relatedIdANodeId;
		}
	}

	static class ChangeLinkedRequirementOperationReport {

		private List<RequirementVersionLinkDto> requirementVersionLinks;
		private Map<String, Object> summary;
		private Map<Long, VerifiedRequirementException> exceptions;

		public ChangeLinkedRequirementOperationReport(List<RequirementVersionLinkDto> requirementVersionLinkDtos) {
			this.requirementVersionLinks = requirementVersionLinkDtos;
		}

		public List<RequirementVersionLinkDto> getRequirementVersionLinks() {
			return requirementVersionLinks;
		}

		public void setRequirementVersionLinks(List<RequirementVersionLinkDto> requirementVersionLinkDtos) {
			this.requirementVersionLinks = requirementVersionLinkDtos;
		}

		public Map<String, Object> getSummary() {
			return summary;
		}

		public void setSummary(Map<String, Object> summary) {
			this.summary = summary;
		}

		public Map<Long, VerifiedRequirementException> getExceptions() {
			return exceptions;
		}

		public void setExceptions(Map<Long, VerifiedRequirementException> exceptions) {
			this.exceptions = exceptions;
		}
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.connection.logs;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.service.display.connection.log.ConnectionLogDisplayService;
import org.squashtest.tm.service.internal.display.grid.GridFilterValue;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.internal.display.grid.GridSort;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/backend/users/connection-logs")
public class ConnectionLogsController {

	private final ConnectionLogDisplayService connectionLogDisplayService;

	ConnectionLogsController(ConnectionLogDisplayService connectionLogDisplayService) {
		this.connectionLogDisplayService = connectionLogDisplayService;
	}

	@PostMapping
	@ResponseBody
	public GridResponse getAllConnectionLogs(@RequestBody GridRequest request) {
		return connectionLogDisplayService.findAll(request);
	}

	@PostMapping("/export")
	@ResponseBody
	public Map<String, String> exportConnectionLog(@RequestBody ExportRequestBody requestBody) {
		final GridRequest gridRequest = prepareNonPaginatedGridRequest(requestBody.getFilterValues());
		final GridResponse gridResponse = connectionLogDisplayService.findAll(gridRequest);
		final String export = ExportFileBuilder.exportAsCSV(gridResponse);
		return Collections.singletonMap("export", export);
	}

	private GridRequest prepareNonPaginatedGridRequest(List<GridFilterValue> filterValues) {
		final GridRequest gridRequest = new GridRequest();

		// Apply filter values
		gridRequest.setFilterValues(filterValues);

		// Sort by connection date ascending
		final GridSort gridSort = new GridSort(ExportFileBuilder.CONNECTION_DATE_KEY, GridSort.SortDirection.ASC);
		gridRequest.setSort(Collections.singletonList(gridSort));

		return gridRequest.toNonPaginatedRequest();
	}

	private static class ExportRequestBody {
		private List<GridFilterValue> filterValues;

		public List<GridFilterValue> getFilterValues() {
			return filterValues;
		}

		public void setFilterValues(List<GridFilterValue> filterValues) {
			this.filterValues = filterValues;
		}
	}

	/**
	 * Helper class to build the CSV file content from a grid response.
	 * This used to be made in the service layer but the code was rewritten in a stateless fashion without any dependencies.
	 * The file processing (creating and filling a temporary file to be downloaded) is now done client-side.
	 * Please move this class in services if it gets more complex !
	 */
	private static class ExportFileBuilder {

		// Keys in GridResponse's data object
		public static final String ID_KEY = "attemptId";
		public static final String LOGIN_KEY = "login";
		public static final String CONNECTION_DATE_KEY = "connectionDate";
		public static final String SUCCESS_KEY = "success";

		// Column names to be printed
		public static final String ID_COLUMN = "Id";
		public static final String LOGIN_COLUMN = "Login";
		public static final String CONNECTION_DATE_COLUMN = "Connection Date";
		public static final String SUCCESS_COLUMN = "Success";

		private static final String NEW_LINE = "\n";

		/**
		 * This function has tight coupling with the ConnectionLogGrid fetching (it has to know the column names).
		 * If it needs to evolve further, please consider using strong typing by replacing the grid response with an interface.
		 *
		 * @param gridResponse the grid response to be translated into CSV
		 * @return the CSV formatted string
		 */
		static String exportAsCSV(GridResponse gridResponse) {
			final StringBuilder stringBuilder = new StringBuilder();

			stringBuilder.append(buildHeaders());
			gridResponse.getDataRows().forEach(dataRow ->
					stringBuilder.append(buildLine(
							dataRow.getData().getOrDefault(ID_KEY, "-").toString(),
							dataRow.getData().getOrDefault(LOGIN_KEY, "-").toString(),
							dataRow.getData().getOrDefault(CONNECTION_DATE_KEY, "-").toString(),
							dataRow.getData().getOrDefault(SUCCESS_KEY, "-").toString()
					))
			);

			return stringBuilder.toString();
		}

		private static String buildHeaders(){
			return buildLine(ID_COLUMN, LOGIN_COLUMN, CONNECTION_DATE_COLUMN, SUCCESS_COLUMN);
		}

		private static String buildLine(String... values) {
			return String.join(";", values) + NEW_LINE;
		}
	}
}

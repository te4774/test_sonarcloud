/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.test.automation.server.environments.unrestricted;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.environmenttag.AutomationEnvironmentTag;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.servers.TokenAuthCredentials;
import org.squashtest.tm.service.display.test.automation.server.TestAutomationServerDisplayService;
import org.squashtest.tm.service.internal.display.dto.automatedexecutionenvironments.EnvironmentSelectionPanelDto;
import org.squashtest.tm.service.project.GenericProjectFinder;
import org.squashtest.tm.service.servers.StoredCredentialsManager;
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionEnvironment;
import org.squashtest.tm.service.testautomation.spi.TestAutomationException;
import org.squashtest.tm.web.backend.controller.form.model.StringList;
import org.squashtest.tm.web.backend.controller.test.automation.server.environments.AbstractTAEnvironmentsController;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Controller for operations on automated execution environments that are accessible to all users.
 */
@Controller
@RequestMapping("/backend/test-automation/")
public class UnrestrictedTAEnvironmentsController extends AbstractTAEnvironmentsController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UnrestrictedTAEnvironmentsController.class);

	private final TestAutomationServerDisplayService testAutomationServerDisplayService;
	private final GenericProjectFinder genericProjectFinder;

	protected UnrestrictedTAEnvironmentsController(AutomatedExecutionEnvironmentService automatedExecutionEnvironmentService,
												   TestAutomationServerDisplayService testAutomationServerDisplayService,
												   GenericProjectFinder genericProjectFinder,
												   StoredCredentialsManager storedCredentialsManager) {
		super(automatedExecutionEnvironmentService, storedCredentialsManager);
		this.testAutomationServerDisplayService = testAutomationServerDisplayService;
		this.genericProjectFinder = genericProjectFinder;
	}

	@ResponseBody
	@GetMapping(value = "{projectId}/available-tags")
	public StringList getAvailableProjectEnvironmentTags(@PathVariable Long projectId) {
		final GenericProject project = Objects.requireNonNull(genericProjectFinder.findById(projectId),
			String.format("Could not find a project with ID %d.", projectId));

		Objects.requireNonNull(project.getTestAutomationServer(),
			String.format("Expect project with ID %d to have a test automation server bound.", projectId));

		final Long testAutomationServerId = project.getTestAutomationServer().getId();
		final Optional<TokenAuthCredentials> optionalProjectToken = findProjectToken(testAutomationServerId, projectId);
		final List<AutomatedExecutionEnvironment> environments = getAutomatedExecutionEnvironments(testAutomationServerId, optionalProjectToken.orElse(null));
		return new StringList(extractAllTags(environments));
	}

	@ResponseBody
	@GetMapping(value = "{projectId}/automated-execution-environments/all")
	public EnvironmentSelectionPanelDto getProjectEnvironmentsPanel(@PathVariable Long projectId) {
		final GenericProject project = Objects.requireNonNull(genericProjectFinder.findById(projectId),
			String.format("Could not find a project with ID %d.", projectId));

		Objects.requireNonNull(project.getTestAutomationServer(),
			String.format("Expect project with ID %d to have a test automation server bound.", projectId));

		final Long testAutomationServerId = project.getTestAutomationServer().getId();

		final boolean hasServerToken = testAutomationServerDisplayService.hasDefinedCredentials(testAutomationServerId);
		final Optional<TokenAuthCredentials> optionalProjectToken = findProjectToken(testAutomationServerId, projectId);
		final boolean hasProjectToken = optionalProjectToken.isPresent();

		if (hasServerToken || hasProjectToken) {
			return getProjectEnvironmentPanelResponse(projectId, project, testAutomationServerId, optionalProjectToken, hasServerToken, hasProjectToken);
		} else {
			return EnvironmentSelectionPanelDto.forProjectWithoutCredentials(testAutomationServerId, projectId);
		}
	}

	private EnvironmentSelectionPanelDto getProjectEnvironmentPanelResponse(Long projectId,
																			GenericProject project,
																			Long testAutomationServerId,
																			Optional<TokenAuthCredentials> optionalProjectToken,
																			boolean hasServerCredentials,
																			boolean hasProjectToken) {
		final List<String> defaultTags = testAutomationServerDisplayService.getDefaultEnvironmentTags(testAutomationServerId);

		final List<String> projectTags = project.getEnvironmentTags().stream()
			.map(AutomationEnvironmentTag::getValue)
			.collect(Collectors.toList());

		final boolean areProjectTagsInherited = project.isInheritsEnvironmentTags();

		List<AutomatedExecutionEnvironment> allAccessibleEnvironments = null;

		try {
			allAccessibleEnvironments = getAutomatedExecutionEnvironments(testAutomationServerId, optionalProjectToken.orElse(null));
		} catch(TestAutomationException ex) {
			if (LOGGER.isTraceEnabled()) {
				LOGGER.trace(String.format("Could not fetch available automated execution environments for project %d.", projectId), ex);
			}
		}

		return EnvironmentSelectionPanelDto.forProject(
			testAutomationServerId,
			defaultTags,
			hasServerCredentials,
			allAccessibleEnvironments,
			projectId,
			hasProjectToken,
			projectTags,
			areProjectTagsInherited);
	}
}

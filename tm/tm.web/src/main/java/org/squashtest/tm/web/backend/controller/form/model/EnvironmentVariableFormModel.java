/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.form.model;

import org.squashtest.tm.domain.environmentvariable.EVInputType;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable;
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableOption;
import org.squashtest.tm.domain.environmentvariable.SingleSelectEnvironmentVariable;

import java.util.List;

public class EnvironmentVariableFormModel {

	private String name;
	private String code;
	private EVInputType inputType;
	private List<EVOptionDto> options;

	public EnvironmentVariable getEnvironmentVariable() {
		EnvironmentVariable environmentVariable;

		if (inputType == EVInputType.DROPDOWN_LIST) {
			environmentVariable = createSingleSelectField();
		} else {
			environmentVariable = new EnvironmentVariable(inputType);
		}

		environmentVariable.setName(name);
		environmentVariable.setCode(code);

		return environmentVariable;
	}

	private EnvironmentVariable createSingleSelectField() {
		SingleSelectEnvironmentVariable ssev = new SingleSelectEnvironmentVariable();

		options.forEach(option -> ssev.addOption(new EnvironmentVariableOption(option.label, option.code)));

		return ssev;
	}

	public static class EVOptionDto {
		private String label;
		private String code;

		public EVOptionDto() {
			//Default constructor
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setInputType(EVInputType inputType) {
		this.inputType = inputType;
	}

	public void setOptions(List<EVOptionDto> options) {
		this.options = options;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.home;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.internal.utils.HTMLCleanupUtils;
import org.squashtest.tm.service.system.SystemAdministrationService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("backend/login-page")
public class LoginPageController {

	private final SystemAdministrationService systemAdministrationService;

	@Value("${info.app.version}")
	private String version;

	// Issue 7509, Spring is a myth, please don't replace @Autowired by @Inject,
	// otherwise, the ClassLoader can not find the class used in Trac plugin
	@Autowired
	private Environment environment;

	public LoginPageController(SystemAdministrationService systemAdministrationService) {
		this.systemAdministrationService = systemAdministrationService;
	}


	@RequestMapping(method = RequestMethod.GET)
	public WelcomePageData loadLoginPage() {
		String loginMessage = systemAdministrationService.findLoginMessage();
		boolean isH2 = isH2();
		return new WelcomePageData(loginMessage, version, isH2);
	}

	private boolean isH2() {
		List<String> activeProfiles = Arrays.asList(environment.getActiveProfiles());
		return activeProfiles.contains("h2");
	}

	static class WelcomePageData {
		private final String loginMessage;

		private final String squashVersion;

		private final boolean isH2;

		public WelcomePageData(String loginMessage, String squashVersion, boolean isH2) {
			this.loginMessage = HTMLCleanupUtils.cleanHtml(loginMessage);
			this.squashVersion = squashVersion;
			this.isH2 = isH2;
		}

		public String getLoginMessage() {
			return loginMessage;
		}


		public String getSquashVersion() {
			return squashVersion;
		}

		@JsonProperty("isH2")
		public boolean isH2() {
			return isH2;
		}
	}
}

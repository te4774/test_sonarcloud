/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.export.grid;

import org.squashtest.tm.service.internal.display.grid.DataRow;

import java.util.List;

public class GridExportModel {
	private List<DataRow> exportedRows;
	private List<ExportColumnDefinition> exportedColumns;

	public GridExportModel(List<DataRow> exportedRows, List<ExportColumnDefinition> exportedColumns) {
		this.exportedRows = exportedRows;
		this.exportedColumns = exportedColumns;
	}

	public GridExportModel() {
	}

	public List<DataRow> getExportedRows() {
		return exportedRows;
	}

	public void setExportedRows(List<DataRow> exportedRows) {
		this.exportedRows = exportedRows;
	}

	public List<ExportColumnDefinition> getExportedColumns() {
		return exportedColumns;
	}

	public void setExportedColumns(List<ExportColumnDefinition> exportedColumns) {
		this.exportedColumns = exportedColumns;
	}

	public static class ExportColumnDefinition {
		private String id;
		private String label;

		public ExportColumnDefinition() {
		}

		public ExportColumnDefinition(String id, String label) {
			this.id = id;
			this.label = label;
		}

		public String getId() {
			return id;
		}

		public String getLabel() {
			return label;
		}
	}
}

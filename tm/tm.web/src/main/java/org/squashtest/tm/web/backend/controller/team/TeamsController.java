/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.team;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.exception.NameAlreadyInUseException;
import org.squashtest.tm.exception.user.LoginAlreadyExistsException;
import org.squashtest.tm.service.display.team.TeamDisplayService;
import org.squashtest.tm.service.internal.display.dto.ProjectPermissionDto;
import org.squashtest.tm.service.internal.display.dto.testcase.TeamMemberDto;
import org.squashtest.tm.service.internal.display.grid.GridRequest;
import org.squashtest.tm.service.internal.display.grid.GridResponse;
import org.squashtest.tm.service.project.ProjectsPermissionManagementService;
import org.squashtest.tm.service.user.CustomTeamModificationService;
import org.squashtest.tm.service.user.TeamModificationService;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/backend/teams")
public class TeamsController {

	private TeamDisplayService teamDisplayService;
	private CustomTeamModificationService customTeamModificationService;
	private TeamModificationService teamModificationService;
	private ProjectsPermissionManagementService permissionService;

	@Inject
	public TeamsController(TeamDisplayService teamDisplayService,
						   CustomTeamModificationService customTeamModificationService,
						   TeamModificationService teamModificationService,
						   ProjectsPermissionManagementService permissionService) {
		this.teamDisplayService = teamDisplayService;
		this.customTeamModificationService = customTeamModificationService;
		this.teamModificationService = teamModificationService;
		this.permissionService = permissionService;
	}


	@ResponseBody
	@PostMapping
	public GridResponse getAllTeams(@RequestBody GridRequest request) {
		return teamDisplayService.findAll(request);
	}

	@ResponseBody
	@PostMapping(value="/new")
	public Map<String,Object> addTeam(@Valid @RequestBody Team team) {
		Map<String, Object> tempReturn = new HashMap<>();
		try {
			customTeamModificationService.persist(team);
			tempReturn.put("id", team.getId());

		} catch (NameAlreadyInUseException ex) {
			ex.setObjectName("add-team");
			throw ex;
		}
		return tempReturn;
	}

	@RequestMapping(value = "/{teamId}/name", method = RequestMethod.POST)
	@ResponseBody
	public void changeName(@PathVariable long teamId, @RequestBody TeamsController.TeamPatch patch) {
		try {
			teamModificationService.changeName(teamId, patch.getName());
		} catch (LoginAlreadyExistsException ex) {
			NameAlreadyInUseException nameAlreadyInUseEx = new NameAlreadyInUseException("team", "add-team", "name");
			throw nameAlreadyInUseEx;
		}
	}

	@RequestMapping(value = "/{teamId}/description", method = RequestMethod.POST)
	@ResponseBody
	public void changeDescription(@PathVariable Long teamId ,@RequestBody TeamsController.TeamPatch patch) {
		teamModificationService.changeDescription(teamId, patch.getDescription());
	}

	@ResponseBody
	@RequestMapping(value = "/{teamIds}", method = RequestMethod.DELETE)
	public void deleteTeams(@PathVariable("teamIds") List<Long> teamIds) {
		customTeamModificationService.deleteTeam(teamIds);
	}

	@ResponseBody
	@RequestMapping(value = "/{teamId}/permissions/{projectIds}", method = RequestMethod.POST)
	public Map<String, List<ProjectPermissionDto>> addNewPermissions(@PathVariable long teamId,
																	 @PathVariable List<Long> projectIds,
																	 @RequestParam String permission) {

		permissionService.addNewPermissionToProject(teamId, projectIds, permission);
		return Collections.singletonMap("projectPermissions", teamDisplayService.getProjectPermissions(teamId));
	}

	@ResponseBody
	@RequestMapping(value = "/{teamId}/permissions/{projectIds}", method = RequestMethod.DELETE)
	public void removePermissions(@PathVariable long teamId,
								  @PathVariable List<Long> projectIds) {
		permissionService.removeProjectPermission(teamId, projectIds);
	}

	@RequestMapping(value = "/{teamId}/members/{logins}", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, List<TeamMemberDto>> addMembers(@PathVariable long teamId,
													   @PathVariable List<String> logins) {
		teamModificationService.addMembers(teamId, logins);
		return Collections.singletonMap("members", teamDisplayService.getTeamMembers(teamId));
	}

	@RequestMapping(value = "/{teamId}/members/{memberIds}", method = RequestMethod.DELETE)
	@ResponseBody
	public void removeMember(@PathVariable long teamId,
							 @PathVariable("memberIds") List<Long> memberIds) {
		teamModificationService.removeMembers(teamId, memberIds);
	}

	static class TeamPatch {
		private String name;
		private String description;

		public String getName() { return name; }

		public void setName(String name) { this.name = name; }

		public String getDescription() { return description; }

		public void setDescription(String description) { this.description = description; }
	}


}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.system;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.squashtest.tm.service.system.SystemAdministrationService;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Arrays;

@RestController
@RequestMapping("/backend/system")
public class SystemAdministrationController {

	@Value("${logging.dir}")
	private String loggingPath;

	private static final String APPLICATION_SLASH_OCTET_STREAM = "application/octet-stream";
	private static final String ATTACHMENT_FILENAME = "attachment; filename=";
	private static final String CONTENT_DISPOSITION = "Content-Disposition";
	private static final String FILENAME = "filename";

	private final Environment environment;
    private final SystemAdministrationService systemAdministrationService;

    SystemAdministrationController(SystemAdministrationService systemAdministrationService,
								   Environment environment) {
        this.systemAdministrationService = systemAdministrationService;
        this.environment = environment;
    }

	@RequestMapping(value = "/settings/white-list", method = RequestMethod.POST)
	@ResponseBody
	public void changeWhiteList(@RequestBody SystemSettingsPatch patch) {
    	systemAdministrationService.changeWhiteList(patch.getWhiteList());
	}

	@RequestMapping(value = "/settings/upload-size-limit", method = RequestMethod.POST)
	@ResponseBody
	public void changeUploadSizeLimit(@RequestBody SystemSettingsPatch patch) {
		systemAdministrationService.changeUploadSizeLimit(patch.getUploadSizeLimit());
	}

	@RequestMapping(value = "/settings/import-size-limit", method = RequestMethod.POST)
	@ResponseBody
	public void changeImportSizeLimit(@RequestBody SystemSettingsPatch patch) {
		systemAdministrationService.changeImportSizeLimit(patch.getImportSizeLimit());
	}

	@RequestMapping(value = "/settings/callback-url", method = RequestMethod.POST)
	@ResponseBody
	public void changeCallbackUrl(@RequestBody SystemSettingsPatch patch) {
		systemAdministrationService.changeCallbackUrl(patch.getCallbackUrl());
	}

	public static class SystemSettingsPatch {
    	String whiteList;
    	String uploadSizeLimit;
    	String importSizeLimit;
    	String callbackUrl;

		public String getWhiteList() {
			return whiteList;
		}

		public void setWhiteList(String whiteList) {
			this.whiteList = whiteList;
		}

		public String getUploadSizeLimit() {
			return uploadSizeLimit;
		}

		public void setUploadSizeLimit(String uploadSizeLimit) {
			this.uploadSizeLimit = uploadSizeLimit;
		}

		public String getImportSizeLimit() {
			return importSizeLimit;
		}

		public void setImportSizeLimit(String importSizeLimit) {
			this.importSizeLimit = importSizeLimit;
		}

		public String getCallbackUrl() {
			return callbackUrl;
		}

		public void setCallbackUrl(String callbackUrl) {
			this.callbackUrl = callbackUrl;
		}
	}


	@PostMapping("/messages/welcome-message")
	void changeWelcomeMessage(@RequestBody SystemMessagesPatch patch) {
		systemAdministrationService.changeWelcomeMessage(patch.getWelcomeMessage());
	}

	@PostMapping("/messages/login-message")
	void changeLoginMessage(@RequestBody SystemMessagesPatch patch) {
		systemAdministrationService.changeLoginMessage(patch.getLoginMessage());
	}

	public static class SystemMessagesPatch {
		private String welcomeMessage;
		private String loginMessage;

		public String getWelcomeMessage() {
			return welcomeMessage;
		}

		public void setWelcomeMessage(String welcomeMessage) {
			this.welcomeMessage = welcomeMessage;
		}

		public String getLoginMessage() {
			return loginMessage;
		}

		public void setLoginMessage(String loginMessage) {
			this.loginMessage = loginMessage;
		}
	}

	@GetMapping("logs/latest")
	@ResponseBody
	public FileSystemResource downloadCurrentLogfile(HttpServletResponse response) {
    	final String fileName = "squash-tm.log";
		File logfile = getCurrentLogFile();
		response.setContentType(APPLICATION_SLASH_OCTET_STREAM);
		response.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + fileName);
		return new FileSystemResource(logfile);
	}

	@GetMapping(value = "logs", params = { FILENAME })
	@ResponseBody
	public FileSystemResource downloadLogfile(@RequestParam(FILENAME) String filename,
											  HttpServletResponse response) {
		File logfile = getLogFile(filename);
		response.setContentType(APPLICATION_SLASH_OCTET_STREAM);
		response.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + logfile.getName());
		return new FileSystemResource(logfile);
	}

	private File getCurrentLogFile() {
		if (Arrays.asList(environment.getActiveProfiles()).contains("dev")) {
			return new File(loggingPath + "/spring.log");
		} else {
			return new File(loggingPath + "/squash-tm.log");
		}
	}

	private File getLogFile(String fileName) {
		if (Arrays.asList(environment.getActiveProfiles()).contains("dev")) {
			throw new RuntimeException("You should not call getLogFile in a dev build.");
		} else {
			return new File(loggingPath + "/" + fileName);
		}
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.web.backend.controller.export.grid


import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.squashtest.tm.service.internal.display.grid.DataRow
import spock.lang.Specification

class XlsGridExporterTest extends Specification {

	def "Should export a simple grid"() {
		given:
		def exportModel = buildBasicModel()

		def rowA = new DataRow()
		rowA.addData("name","TC1")
		rowA.addData("crit","Très Haute")

		def rowB = new DataRow()
		rowB.addData("name","TC2")
		rowB.addData("crit","Haute")
		exportModel.exportedRows = List.of(rowA, rowB)

		when:
		def file = new XlsGridExporter(exportModel).export()

		then:
		file.isFile()
		XSSFWorkbook workbook = new XSSFWorkbook(file)
		def sheet = workbook.getSheetAt(0)
		def headerRow = sheet.getRow(0)
		def nameColumnHeader = headerRow.getCell(0)
		nameColumnHeader.getStringCellValue() == "Nom"
		def critColumnHeader = headerRow.getCell(1)
		critColumnHeader.getStringCellValue() == "Criticité"

		def renderedRowA = sheet.getRow(1)
		def nameCellA = renderedRowA.getCell(0)
		nameCellA.getStringCellValue() == "TC1"
		def critCellA = renderedRowA.getCell(1)
		critCellA.getStringCellValue() == "Très Haute"

		def renderedRowB = sheet.getRow(2)
		def nameCellB = renderedRowB.getCell(0)
		nameCellB.getStringCellValue() == "TC2"
		def critCellB = renderedRowB.getCell(1)
		critCellB.getStringCellValue() == "Haute"

		workbook.close()
	}

	def "Should not crash if missing data"(){
		given:
		def exportModel = buildBasicModel()

		// missing key : name
		// key present but value is null : crit
		def rowA = new DataRow()
		rowA.addData("crit",null)

		exportModel.exportedRows = List.of(rowA)

		when:
		def file = new XlsGridExporter(exportModel).export()

		then:
		file.isFile()
		XSSFWorkbook workbook = new XSSFWorkbook(file)
		def sheet = workbook.getSheetAt(0)

		def renderedRowA = sheet.getRow(1)
		def nameCellA = renderedRowA.getCell(0)
		nameCellA.getStringCellValue() == ""
		def critCellA = renderedRowA.getCell(1)
		critCellA.getStringCellValue() == ""

		workbook.close()
	}

	def buildBasicModel(){
		def exportModel = new GridExportModel()
		exportModel.exportedColumns =
			List.of(
				new GridExportModel.ExportColumnDefinition("name", "Nom"),
				new GridExportModel.ExportColumnDefinition("crit","Criticité")
			)
		exportModel
	}
}

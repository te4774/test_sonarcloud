/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.administration;

import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RequirementsLinksTest extends DatabaseTest {

	@Test
	public void shouldShowAllRequirementsLinks() {
		Cookie session = login(Users.ADMIN);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json")
			.when()
			.post("backend/requirements-links")
			.then()
			.statusCode(200)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		// There should be 2 requirements links
		assertEquals(2, dataRows.size());

		assertEquals("role1", jsonPath.getString("dataRows[0].data.role1"));
		assertEquals("code1", jsonPath.getString("dataRows[0].data.role1Code"));
		assertEquals("role2", jsonPath.getString("dataRows[0].data.role2"));
		assertEquals("code2", jsonPath.getString("dataRows[0].data.role2Code"));
		assertEquals(true, jsonPath.getBoolean("dataRows[0].data.isDefault"));

	}

	@Test
	public void shouldAddRequirementsLinkType() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("test1", "test1Code","test2", "test2Code");

		String response = addRequirementsLinkType(session, formModel, 200);

		JsonPath jsonPath = from(response);
		assertNotNull(jsonPath.getJsonObject("id"));
	}

	@Test
	public void shouldForbidRequirementsLinkTypeCreationWithEmptyRole1() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("", "test1Code","test2", "test2Code");

		String response = addRequirementsLinkType(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("role1", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldForbidRequirementsLinkTypeCreationWithEmptyRole1Code() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("test1", "","test2", "test2Code");

		String response = addRequirementsLinkType(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("role1Code", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldForbidRequirementsLinkTypeCreationWithEmptyRole2() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("test1", "test1Code","", "test2Code");

		String response = addRequirementsLinkType(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("role2", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldForbidRequirementsLinkTypeCreationWithEmptyRole2Code() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("test1", "test1Code","test2", "");

		String response = addRequirementsLinkType(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("role2Code", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldForbidRequirementsLinkTypeCreationIfRole1CodeAlreadyExists() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("test1", "code1","test2", "test2Code");

		String response = addRequirementsLinkType(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("squashTMError.fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("role1Code", jsonPath.getString("squashTMError.fieldValidationErrors[0].fieldName"));
		assertEquals("sqtm-core.exception.requirements-links.link-type-code-already-exists", jsonPath.getString("squashTMError.fieldValidationErrors[0].i18nKey"));
	}

	@Test
	public void shouldForbidRequirementsLinkTypeCreationIfRole2CodeAlreadyExists() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("test1", "test1Code","test2", "code2");

		String response = addRequirementsLinkType(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("squashTMError.fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("role2Code", jsonPath.getString("squashTMError.fieldValidationErrors[0].fieldName"));
		assertEquals("sqtm-core.exception.requirements-links.link-type-code-already-exists", jsonPath.getString("squashTMError.fieldValidationErrors[0].i18nKey"));
	}


	private Map<String, Object> createFormModel(String role1, String role1Code, String role2, String role2Code) {
		Map<String, Object> formModel = new HashMap<>();
		formModel.put("role1", role1);
		formModel.put("role1Code", role1Code);
		formModel.put("role2", role2);
		formModel.put("role2Code", role2Code);
		return formModel;
	}

	private String getRequirementsLinksGrid(Cookie session, int gridSize, int gridPage) {
		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(gridSize);
		gridRequest.setPage(gridPage);

		return given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json")
			.when()
			.post("backend/requirements-links")
			.then()
			.statusCode(200)
			.extract().response().asString();
	}

	private String addRequirementsLinkType(Cookie session, Map<String, Object> requirementsLinkTypeToAdd, int expectedStatusCode) {
		return given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(requirementsLinkTypeToAdd).
				when()
			.post("backend/requirements-links/new").
				then()
			.statusCode(expectedStatusCode)
			.extract().response().asString();
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"administration/requirements-links-data-test.xml"
		);
	}
}

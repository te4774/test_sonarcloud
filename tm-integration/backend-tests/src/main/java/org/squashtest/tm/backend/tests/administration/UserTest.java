/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.administration;

import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserTest extends DatabaseTest {

	@Test
	public void shouldCreateUser() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> userModel = new HashMap<>();
		userModel.put("login", "User 1");
		userModel.put("lastName", "Dupont");

		Map<String, Object> userToAdd = new HashMap<>();
		userToAdd.put("user", userModel);
		userToAdd.put("groupId", 2);
		userToAdd.put("password", "password");

		// Add users
		addUser(session, userToAdd, 200);

		// Re-fetch grid to check its size and content
		String response = fetchGrid(session);

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		assertEquals(3, dataRows.size());
	}

	private void addUser(Cookie session, Map<String, Object> user, int expectedStatusCode) {
		given()
			.cookie(session)
			.contentType("application/json")
			.body(user).
			when()
			.post("backend/users/new").
			then()
			.statusCode(expectedStatusCode)
			.extract().response().asString();
	}

	private String fetchGrid(Cookie session) {
		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		return given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json").
				when()
			.post("backend/users").
				then()
			.statusCode(200)
			.extract().response().asString();
	}

	@Test
	public void shouldForbidToCreateUser() {
		Cookie session = login(Users.VIEWER);

		Map<String, Object> userModel = new HashMap<>();
		userModel.put("login", "User 1");
		userModel.put("lastName", "Dupont");

		Map<String, Object> formModel = new HashMap<>();
		formModel.put("user", userModel);
		formModel.put("groupId", 2);
		formModel.put("password", "password");

		given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
			when()
			.post("backend/users/new").
			then()
			.statusCode(403);
	}

	@Test
	public void shouldForbidUserCreationIfLoginAlreadyExists() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> userModel = new HashMap<>();
		userModel.put("login", "viewer");
		userModel.put("lastName", "Dupont");

		Map<String, Object> formModel = new HashMap<>();
		formModel.put("user", userModel);
		formModel.put("groupId", 2);
		formModel.put("password", "password");

		String response = given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
				when()
			.post("backend/users/new").
				then()
			.statusCode(412)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("squashTMError.fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("sqtm-core.error.generic.name-already-in-use", jsonPath.getString("squashTMError.fieldValidationErrors[0].i18nKey"));
	}

	@Test
	public void shouldForbidUserCreationWithEmptyLogin() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> userModel = new HashMap<>();
		userModel.put("login", "");
		userModel.put("lastName", "Dupont");

		Map<String, Object> formModel = new HashMap<>();
		formModel.put("user", userModel);
		formModel.put("groupId", 2);
		formModel.put("password", "password");

		String response = given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
				when()
			.post("backend/users/new").
				then()
			.statusCode(412)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("user.login", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldForbidUserCreationWithEmptyLastName() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> userModel = new HashMap<>();
		userModel.put("login", "User 1");
		userModel.put("lastName", "");

		Map<String, Object> formModel = new HashMap<>();
		formModel.put("user", userModel);
		formModel.put("groupId", 2);
		formModel.put("password", "password");

		String response = given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
				when()
			.post("backend/users/new").
				then()
			.statusCode(412)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("user.lastName", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldForbidUserCreationWithInsuffisientPasswordLength() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> userModel = new HashMap();
		userModel.put("login", "User 1");
		userModel.put("lastName", "Dupont");

		Map<String, Object> formModel = new HashMap<>();
		formModel.put("user", userModel);

		formModel.put("groupId", 2);

		//Password must have 6 characters
		formModel.put("password", "12345");

		String response = given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
				when()
			.post("backend/users/new").
				then()
			.statusCode(412)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("password", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldShowAllUsers() {
		Cookie session = login(Users.ADMIN);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json")
				.when()
			.post("backend/users")
				.then()
			.statusCode(200)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		// There should be 2 users
		assertEquals(2, dataRows.size());
		assertTrue(jsonPath.getBoolean("dataRows[0].data.active"));
		assertEquals("admin", jsonPath.getString("dataRows[0].data.login"));
		assertEquals("squashtest.authz.group.core.Admin", jsonPath.getString("dataRows[0].data.userGroup"));
		assertEquals("adminFirstName", jsonPath.getString("dataRows[0].data.firstName"));
		assertEquals("adminLastName", jsonPath.getString("dataRows[0].data.lastName"));
		assertEquals("admin@henix.com", jsonPath.getString("dataRows[0].data.email"));
		assertEquals("2020-04-07T22:00:00.000+0000", jsonPath.getString("dataRows[0].data.createdOn"));
		assertEquals("superAdmin", jsonPath.getString("dataRows[0].data.createdBy"));
		assertEquals(3, jsonPath.getInt("dataRows[0].data.habilitationCount"));
		assertEquals(3, jsonPath.getInt("dataRows[0].data.teamCount"));

		//We check the second user's last connection because the admin (first user) is being connected at the beginning of the test
		assertEquals("2020-04-08T22:00:00.000+0000", jsonPath.getString("dataRows[1].data.lastConnectedOn"));
	}

	@Test
	public void shouldDeleteUsers() {
		Cookie session = login(Users.ADMIN);

		// Delete users
		deleteAllUsers(session, 200);

		// Check how many are left
		String response = fetchGrid(session);

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		//There should only be the admin left
		assertEquals(1, dataRows.size());
	}

	private void deleteAllUsers(Cookie session, int expectedStatusCode) {
		given()
			.cookie(session)
			.contentType("application/json").
			when()
			.delete("backend/users/-2").
			then()
			.statusCode(expectedStatusCode)
			.extract().response().asString();
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"administration/user-data-test.xml");
	}


}

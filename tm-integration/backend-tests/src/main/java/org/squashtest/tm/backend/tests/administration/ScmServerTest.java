/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.administration;

import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ScmServerTest extends DatabaseTest {

	@Test
	public void shouldShowAllScmServers() {
		Cookie session = login(Users.ADMIN);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json").
				when()
			.post("backend/scm-servers").
				then()
			.statusCode(200)
			.assertThat().body(matchesJsonSchemaInClasspath("json-schema/tests/grid/grid-response-schema.json"))
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		assertEquals(2, 						dataRows.size());

		assertEquals(-1L, 									jsonPath.getLong("dataRows[0].data.serverId"));
		assertEquals("Github", 							jsonPath.getString("dataRows[0].data.name"));
		assertEquals("git", 								jsonPath.getString("dataRows[0].data.kind"));
		assertEquals("https://github.com/RecetteSquash", 	jsonPath.getString("dataRows[0].data.url"));
		assertEquals(0, 									jsonPath.getInt("dataRows[0].data.projectCount"));

		assertEquals(-3L, 									jsonPath.getLong("dataRows[1].data.serverId"));
		assertEquals("Gitlab", 							jsonPath.getString("dataRows[1].data.name"));
		assertEquals("git", 								jsonPath.getString("dataRows[1].data.kind"));
		assertEquals("https://gitlab.com/RecetteSquash", 	jsonPath.getString("dataRows[1].data.url"));
		assertEquals(3, 									jsonPath.getInt("dataRows[1].data.projectCount"));
	}

	@Test
	public void shouldAddScmServer() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("scmServer1", "http://localhost:8080/scmServer1", "Github");

		String response = addScmServer(session, formModel, 200);

		JsonPath jsonPath = from(response);
		assertNotNull(jsonPath.getJsonObject("id"));
	}

	@Test
	public void shouldForbidScmServerCreationIfNameAlreadyExists() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("Github", "http://localhost:8080/scmServer1", "Github");

		String response = addScmServer(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("squashTMError.fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("sqtm-core.error.generic.name-already-in-use", jsonPath.getString("squashTMError.fieldValidationErrors[0].i18nKey"));
	}

	@Test
	public void shouldForbidScmServerCreationWithEmptyName() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("", "http://localhost:8080/scmServer1", "Github");

		String response = addScmServer(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("name", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldForbidScmServerCreationWithEmptyUrl() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("scmServer1", "", "Github");

		String response = addScmServer(session, formModel, 412);

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("squashTMError.fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("url", jsonPath.getString("squashTMError.fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldForbidScmServerCreationWithMalformedUrl() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = createFormModel("scmServer1", "42", "Github");

		String response = addScmServer(session, formModel, 412);

		JsonPath jsonPath = from(response);
		assertEquals("url", jsonPath.getString("squashTMError.fieldValidationErrors[0].fieldName"));
		assertEquals("sqtm-core.exception.wrong-url", jsonPath.getString("squashTMError.fieldValidationErrors[0].i18nKey"));
	}

	@Test
	public void shouldDeleteScmServer() {
		Cookie session = login(Users.ADMIN);

		// Do delete
		given()
			.cookie(session)
			.contentType("application/json")
			.when()
			.delete("backend/scm-servers/-1")
			.then()
			.statusCode(200)
			.extract().response().asString();

		// check that new count is 0
		String fetchResponse = getScmServerGrid(session, 10, 0);
		JsonPath jsonPath = from(fetchResponse);
		List<Object> dataRows = jsonPath.getList("dataRows");
		assertEquals(1, dataRows.size());
	}

	private Map<String, Object> createFormModel(String name, String url, String kind) {
		Map<String, Object> formModel = new HashMap<>();
		formModel.put("name", name);
		formModel.put("url", url);
		formModel.put("kind", kind);
		return formModel;
	}

	private String getScmServerGrid(Cookie session, int gridSize, int gridPage) {
		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(gridSize);
		gridRequest.setPage(gridPage);

		return given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json")
			.when()
			.post("backend/scm-servers")
			.then()
			.statusCode(200)
			.extract().response().asString();
	}

	private String addScmServer(Cookie session, Map<String, Object> serverToAdd, int expectedStatusCode) {
		return given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(serverToAdd).
				when()
			.post("backend/scm-servers/new").
				then()
			.statusCode(expectedStatusCode)
			.extract().response().asString();
	}

	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"referential/info-list-system.xml",
			"administration/scm-server-data-test.xml");
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.administration;


import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;
import org.squashtest.tm.backend.tests.basetest.DatabaseTest;
import org.squashtest.tm.backend.tests.basetest.Users;
import org.squashtest.tm.service.internal.display.grid.GridRequest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;
import static org.junit.Assert.assertEquals;

public class TeamTest extends DatabaseTest {

	@Test
	public void shouldCreateTeam() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> teamToAdd = new HashMap<>();
		teamToAdd.put("name", "Team 1");
		teamToAdd.put("description", "My team");

		// Add users
		addTeam(session, teamToAdd, 200);

		// Re-fetch grid to check its size and content
		String response = fetchGrid(session);

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		assertEquals(4, dataRows.size());
	}

	private void addTeam(Cookie session, Map<String, Object> team, int expectedStatusCode) {
		given()
			.cookie(session)
			.contentType("application/json")
			.body(team).
			when()
			.post("backend/teams/new").
			then()
			.statusCode(expectedStatusCode)
			.extract().response().asString();
	}

	private String fetchGrid(Cookie session) {
		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		return given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json").
				when()
			.post("backend/teams").
				then()
			.statusCode(200)
			.extract().response().asString();
	}


	@Test
	public void shouldForbidToCreateTeam() {
		Cookie session = login(Users.VIEWER);

		Map<String, Object> formModel = new HashMap<>();
		formModel.put("name", "Team 1");
		formModel.put("description", "My team");

		given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
			when()
			.post("backend/teams/new").
			then()
			.statusCode(403);
	}

	@Test
	public void shouldForbidTeamCreationIfNameAlreadyExists() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = new HashMap<>();
		formModel.put("name", "team1");
		formModel.put("description", "My team");

		String response = given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
				when()
			.post("backend/teams/new").
				then()
			.statusCode(412)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("squashTMError.fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("sqtm-core.error.generic.name-already-in-use", jsonPath.getString("squashTMError.fieldValidationErrors[0].i18nKey"));
	}

	@Test
	public void shouldForbidTeamCreationWithEmptyName() {
		Cookie session = login(Users.ADMIN);

		Map<String, Object> formModel = new HashMap<>();
		formModel.put("name", "");
		formModel.put("description", "");

		String response = given()
			.cookie(session)
			.contentType(ContentType.JSON)
			.body(formModel).
			when()
			.post("backend/teams/new").
			then()
			.statusCode(412)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> fieldValidationErrors = jsonPath.getList("fieldValidationErrors");
		assertEquals(fieldValidationErrors.size(), 1);
		assertEquals("name", jsonPath.getString("fieldValidationErrors[0].fieldName"));
	}

	@Test
	public void shouldShowAllTeams() {
		Cookie session = login(Users.ADMIN);

		GridRequest gridRequest = new GridRequest();
		gridRequest.setSize(10);
		gridRequest.setPage(0);

		String response = given()
			.cookie(session)
			.body(gridRequest)
			.contentType("application/json")
				.when()
			.post("backend/teams")
				.then()
			.statusCode(200)
			.extract().response().asString();

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		// There should be 3 teams
		assertEquals(3, dataRows.size());
		assertEquals("team1", jsonPath.getString("dataRows[0].data.name"));
		assertEquals("team2", jsonPath.getString("dataRows[1].data.name"));
		assertEquals("team3", jsonPath.getString("dataRows[2].data.name"));

		// Further tests on data are made in TeamDisplayServiceIT
	}

	@Test
	public void shouldDeleteTeams() {
		Cookie session = login(Users.ADMIN);

		// Delete users
		deleteAllTeams(session, 200);

		// Check how many are left
		String response = fetchGrid(session);

		JsonPath jsonPath = from(response);
		List<Object> dataRows = jsonPath.getList("dataRows");

		assertEquals(0, dataRows.size());
	}

	private void deleteAllTeams(Cookie session, int expectedStatusCode) {
		given()
			.cookie(session)
			.contentType("application/json").
			when()
			.delete("backend/teams/-3,-4,-5").
			then()
			.statusCode(expectedStatusCode)
			.extract().response().asString();
	}


	@Override
	protected List<String> getSetupDatasetPath() {
		return Arrays.asList(
			"core-config/core-config.xml",
			"administration/user-data-test.xml");
	}

}

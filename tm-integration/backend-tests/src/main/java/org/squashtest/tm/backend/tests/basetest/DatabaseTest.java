/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.backend.tests.basetest;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class DatabaseTest extends BackEndTest {

	private static final String POSTGRESQL_DRIVER_NAME = "org.postgresql.Driver";
	private static final String MYSQL_DRIVER_NAME = "com.mysql.jdbc.Driver";
	private Logger LOGGER = LoggerFactory.getLogger(DatabaseTest.class);

	// Allow to specify update datasets that should be fired before the DELETE_ALL operations.
	// Especially useful for cleaning self-referencing foreign keys.
	protected List<String> getPreTearDownUpdateDatasetPaths() {
		return new ArrayList<>();
	}

	protected abstract List<String> getSetupDatasetPath();

	protected String getTearDownDatasetPath() {
		return "target/classes/datasets/tests/clean-all.xml";
	}

	private IDataSet readDataSet(String filename) throws Exception {
		return new FlatXmlDataSetBuilder().build(new File(filename));
	}

	@BeforeEach
	private void insertData() throws Exception {
		List<String> datasetPaths = this.getSetupDatasetPath();
		List<IDatabaseTester> databaseTester = new ArrayList<>();
		clean();

		for (String datasetPath : datasetPaths) {
			IDataSet dataset = readDataSet("target/classes/datasets/tests/" + datasetPath);
			databaseTester.add(getDatabaseTester(dataset, DatabaseOperation.CLEAN_INSERT, DatabaseOperation.DELETE_ALL));
		}

		for (IDatabaseTester iDatabaseTester : databaseTester) {
			iDatabaseTester.onSetup();
		}
	}

	private void clean() throws Exception {
		// Pre delete updates
		List<String> cleanPaths = this.getPreTearDownUpdateDatasetPaths();
		List<IDatabaseTester> preDeletes = new ArrayList<>();

		for (String cleanPath : cleanPaths) {
			ReplacementDataSet cleanDataset = new ReplacementDataSet(readDataSet("target/classes/datasets/tests/" + cleanPath));
			// Easiest way to specify null values is to use replacement objects
			cleanDataset.addReplacementObject("[NULL]", null);
			preDeletes.add(getDatabaseTester(cleanDataset, DatabaseOperation.UPDATE, DatabaseOperation.UPDATE));
		}

		for (IDatabaseTester iDatabaseTester : preDeletes) {
			iDatabaseTester.onSetup();
		}

		// Delete all
		String datasetPath = this.getTearDownDatasetPath();
		IDataSet dataset = this.readDataSet(datasetPath);
		IDatabaseTester deleter = getDatabaseTester(dataset, DatabaseOperation.DELETE_ALL, DatabaseOperation.DELETE_ALL);
		deleter.onSetup();
	}

//	@AfterEach
//	private void deleteAllData() throws Exception {
//		String datasetPath = this.getTearDownDatasetPath();
//		if (datasetPath != null) {
//			this.databaseTester.get(0).onTearDown();
//		}
//	}

//	private void executeDataSet(String datasetPath, DatabaseOperation databaseOperation) throws Exception {
//
//		databaseTester.onSetup();
//	}

	private IDatabaseTester getDatabaseTester(IDataSet dataset, DatabaseOperation setup, DatabaseOperation teardown) throws ClassNotFoundException {
		IDatabaseTester databaseTester = new JdbcDatabaseTester(
			System.getProperty("database.driver"),
			System.getProperty("database.url"),
			System.getProperty("database.user"),
			System.getProperty("database.password")) {

			// dbunit is not able to handle properly config props so we need to override the method as connection seems
			// to be locked after
			@Override
			public IDatabaseConnection getConnection() throws Exception {
				IDatabaseConnection connection = super.getConnection();
				switch (System.getProperty("database.driver")) {
					case MYSQL_DRIVER_NAME:
						connection.getConfig().setProperty(
							DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
							new MySqlDataTypeFactory());
						break;
					case POSTGRESQL_DRIVER_NAME:
						connection.getConfig().setProperty(
							DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
							new PostgresqlDataTypeFactory());
						break;
					default:
						LOGGER.error("Unknown database driver name");
						throw new Exception("Unknown database driver name");
				}
				connection.getConfig().setProperty(DatabaseConfig.FEATURE_ALLOW_EMPTY_FIELDS, true);
				return connection;
			}
		};
		databaseTester.setSetUpOperation(setup);
		databaseTester.setTearDownOperation(teardown);
		databaseTester.setDataSet(dataset);
		return databaseTester;
	}
}

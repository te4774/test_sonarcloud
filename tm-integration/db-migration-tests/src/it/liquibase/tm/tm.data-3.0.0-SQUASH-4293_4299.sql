INSERT INTO ATTACHMENT_LIST (ATTACHMENT_LIST_ID) VALUES
(-800), (-801), (-802), (-803), (-804), (-805), (-806), (-807), (-808), (-809), (-810), (-811), (-812), (-813);

INSERT INTO INFO_LIST (INFO_LIST_ID, LABEL, CODE, CREATED_BY, CREATED_ON) VALUES
(4, 'infolist1', 'list1', 'liquibase', '2014-12-03');

INSERT INTO INFO_LIST_ITEM (ITEM_ID, ITEM_TYPE, LIST_ID, ITEM_INDEX, LABEL, CODE, IS_DEFAULT, ICON_NAME) VALUES
(-1, 'USR', 4, 0, 'Business', 'DEF_CAT_BUSINESS', true, 'def_cat_business'),
(-2, 'USR', 4, 1, 'Ergonomic', 'DEF_CAT_ERGONOMIC', false, 'def_cat_ergonomic'),
(-3, 'USR', 4, 2, 'Functional', 'DEF_CAT_FUNCTIONAL', false, 'def_cat_functional'),
(-4, 'USR', 4, 3, 'No icon', 'DEF_CAT_NOICON', false, 'def_cat_noicon'),
(-5, 'USR', 4, 4, 'Non functional', 'DEF_CAT_NON_FUNCTIONAL', false, 'def_cat_non-functional'),
(-6, 'USR', 4, 5, 'Performance', 'DEF_CAT_PERFORMANCE', false, 'def_cat_performance'),
(-7, 'USR', 4, 6, 'Security', 'DEF_CAT_SECURITY', false, 'def_cat_security'),
(-8, 'USR', 4, 7, 'Technical', 'DEF_CAT_TECHNICAL', false, 'def_cat_technical'),
(-9, 'USR', 4, 8, 'Test requirement', 'DEF_CAT_TEST_REQUIREMENT', false, 'def_cat_test-requirement'),
(-10, 'USR', 4, 9, 'Undefined', 'DEF_CAT_UNDEFINED', false, 'def_cat_undefined'),
(-11, 'USR', 4, 10, 'Use case', 'DEF_CAT_USE_CASE', false, 'def_cat_use-case'),
(-12, 'USR', 4, 11, 'User story', 'DEF_CAT_USER_STORY', false, 'def_cat_user-story');

INSERT INTO TEST_CASE_LIBRARY_NODE(TCLN_ID, DESCRIPTION, CREATED_BY, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_ON, ATTACHMENT_LIST_ID, PROJECT_ID, NAME) VALUES
(-10, '', 'liquibase', '2014-12-03', null, null, -800, 2, 'squash-4293-undefined-undefined'),
(-11, '', 'liquibase', '2014-12-03', null, null, -801, 2, 'squash-4293-functional-compliance'),
(-12, '', 'liquibase', '2014-12-03', null, null, -802, 2, 'squash-4293-business-correction'),
(-13, '', 'liquibase', '2014-12-03', null, null, -803, 2, 'squash-4293-user-evolution'),
(-14, '', 'liquibase', '2014-12-03', null, null, -804, 2, 'squash-4293-non_functional-regression'),
(-15, '', 'liquibase', '2014-12-03', null, null, -805, 2, 'squash-4293-performance-end_to_end'),
(-16, '', 'liquibase', '2014-12-03', null, null, -806, 2, 'squash-4293-security-partner'),
(-17, '', 'liquibase', '2014-12-03', null, null, -807, 2, 'squash-4293-atdd-partner'),

(-20, '', 'liquibase', '2014-12-03', null, null, -808, 4, 'squash-4299-def_business-def_ergonomic'),
(-21, '', 'liquibase', '2014-12-03', null, null, -809, 4, 'squash-4299-def_functional-def_noicon'),
(-22, '', 'liquibase', '2014-12-03', null, null, -810, 4, 'squash-4299-def_non_functional-def_performance'),
(-23, '', 'liquibase', '2014-12-03', null, null, -811, 4, 'squash-4299-def_security-def_technical'),
(-24, '', 'liquibase', '2014-12-03', null, null, -812, 4, 'squash-4299-def_test_requirement-def_undefined'),
(-25, '', 'liquibase', '2014-12-03', null, null, -813, 4, 'squash-4299-def_use_case-def_user_story');

INSERT INTO TEST_CASE (TCLN_ID, UUID, VERSION, PREREQUISITE, TC_NATURE, TC_TYPE) VALUES
(-10, 'fc8c56d7-7540-4db1-892d-8b774db23ab4', 1, '', 12, 20),
(-11, 'fc8c56d7-7540-4db2-892d-8b774db23ab6', 1, '', 13, 21),
(-12, 'fc8c56d7-7540-4db3-892d-8b774db23ab8', 1, '', 14, 22),
(-13, 'fc8c56d7-7540-4db4-892d-8b774db23ab7', 1, '', 15, 23),
(-14, 'fc8c56d7-7540-4db5-892d-8b774db23ab2', 1, '', 16, 24),
(-15, 'fc8c56d7-7540-4db6-892d-8b774db23ab1', 1, '', 17, 25),
(-16, 'fc8c56d7-7540-4db7-892d-8b774db23ab3', 1, '', 18, 26),
(-17, 'fc8c56d7-7540-4db8-892d-8b774db23ab5', 1, '', 19, 26),

(-20, 'fc8c45d7-7540-4db1-892d-8b774db23ab1', 1, '', -1, -2),
(-21, 'fc8c33d7-7540-4db2-892d-8b774db23ab2', 1, '', -3, -4),
(-22, 'fc8c22d7-7540-4db3-892d-8b774db23ab3', 1, '', -5, -6),
(-23, 'fc8c11d7-7540-4db4-892d-8b774db23ab4', 1, '', -7, -8),
(-24, 'fc8c11d8-7540-4db4-892d-8b774db23ab4', 1, '', -9, -10),
(-25, 'fc8c11d9-7540-4db4-892d-8b774db23ab4', 1, '', -11, -12);

INSERT INTO EXECUTION (EXECUTION_ID, TCLN_ID, CREATED_BY, CREATED_ON, NAME, PREREQUISITE, TC_STATUS, IMPORTANCE,
REFERENCE, TC_NAT_CODE, TC_NAT_ICON_NAME, TC_TYP_CODE, TC_TYP_ICON_NAME) VALUES
(-10, -10, 'liquibase', '2014-12-03', 'squash-4293-undefined-undefined', '', 'WORK_IN_PROGRESS', 'LOW', '', 'NAT_UNDEFINED', 'indeterminate_checkbox', 'TYP_UNDEFINED', 'indeterminate_checkbox'),
(-11, -11, 'liquibase', '2014-12-03', 'squash-4293-functional-compliance', '', 'WORK_IN_PROGRESS', 'LOW', '', 'NAT_FUNCTIONAL_TESTING', 'indeterminate_checkbox', 'TYP_COMPLIANCE_TESTING', 'indeterminate_checkbox'),
(-12, -12, 'liquibase', '2014-12-03', 'squash-4293-business-correction', '', 'WORK_IN_PROGRESS', 'LOW', '', 'NAT_BUSINESS_TESTING', 'indeterminate_checkbox', 'TYP_CORRECTION_TESTING', 'indeterminate_checkbox'),
(-13, -13, 'liquibase', '2014-12-03', 'squash-4293-user-evolution', '', 'WORK_IN_PROGRESS', 'LOW', '', 'NAT_USER_TESTING', 'indeterminate_checkbox', 'TYP_EVOLUTION_TESTING', 'indeterminate_checkbox'),
(-14, -14, 'liquibase', '2014-12-03', 'squash-4293-non_functional-regression', '', 'WORK_IN_PROGRESS', 'LOW', '', 'NAT_NON_FUNCTIONAL_TESTING', 'indeterminate_checkbox', 'TYP_REGRESSION_TESTING', 'indeterminate_checkbox'),
(-15, -15, 'liquibase', '2014-12-03', 'squash-4293-performance-end_to_end', '', 'WORK_IN_PROGRESS', 'LOW', '', 'NAT_PERFORMANCE_TESTING', 'indeterminate_checkbox', 'TYP_END_TO_END_TESTING', 'indeterminate_checkbox'),
(-16, -16, 'liquibase', '2014-12-03', 'squash-4293-security-partner', '', 'WORK_IN_PROGRESS', 'LOW', '', 'NAT_SECURITY_TESTING', 'indeterminate_checkbox', 'TYP_PARTNER_TESTING', 'indeterminate_checkbox'),
(-17, -17, 'liquibase', '2014-12-03', 'squash-4293-atdd-partner', '', 'WORK_IN_PROGRESS', 'LOW', '', 'NAT_ATDD', 'indeterminate_checkbox', 'TYP_PARTNER_TESTING', 'indeterminate_checkbox'),

(-20, -20, 'liquibase', '2014-12-03', 'squash-4299-def_business-def_ergonomic', '', 'WORK_IN_PROGRESS', 'LOW', '', 'DEF_CAT_BUSINESS', 'def_cat_business', 'DEF_CAT_ERGONOMIC', 'def_cat_ergonomic'),
(-21, -21, 'liquibase', '2014-12-03', 'squash-4299-def_functional-def_noicon', '', 'WORK_IN_PROGRESS', 'LOW', '', 'DEF_CAT_FUNCTIONAL', 'def_cat_functional', 'DEF_CAT_NOICON', 'def_cat_noicon'),
(-22, -22, 'liquibase', '2014-12-03', 'squash-4299-def_non_functional-def_performance', '', 'WORK_IN_PROGRESS', 'LOW', '', 'DEF_CAT_NON_FUNCTIONAL', 'def_cat_non-functional', 'DEF_CAT_PERFORMANCE', 'def_cat_performance'),
(-23, -23, 'liquibase', '2014-12-03', 'squash-4299-def_security-def_technical', '', 'WORK_IN_PROGRESS', 'LOW', '', 'DEF_CAT_SECURITY', 'def_cat_security', 'DEF_CAT_TECHNICAL', 'def_cat_technical'),
(-24, -24, 'liquibase', '2014-12-03', 'squash-4299-def_test_requirement-def_undefined', '', 'WORK_IN_PROGRESS', 'LOW', '', 'DEF_CAT_TEST_REQUIREMENT', 'def_cat_test-requirement', 'DEF_CAT_UNDEFINED', 'def_cat_undefined'),
(-25, -25, 'liquibase', '2014-12-03', 'squash-4299-def_use_case-def_user_story', '', 'WORK_IN_PROGRESS', 'LOW', '', 'DEF_CAT_USE_CASE', 'def_cat_use-case', 'DEF_CAT_USER_STORY', 'def_cat_user-story');

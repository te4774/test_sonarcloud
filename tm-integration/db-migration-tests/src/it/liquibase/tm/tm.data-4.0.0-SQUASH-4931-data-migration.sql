INSERT INTO ACTION_WORD (ACTION_WORD_ID, LAST_IMPLEMENTATION_TECHNOLOGY, PROJECT_ID, TOKEN, CREATED_BY, CREATED_ON)
	VALUES (1, 'CUCUMBER', 1, 'T-bonjour', 'liquibase',  '2022-04-08'),
	       (2, 'CUCUMBER', 1, 'T-hello', 'liquibase', '2022-04-08'),
	       (3, 'ROBOT', 1, 'T-ola', 'liquibase', '2022-04-08');

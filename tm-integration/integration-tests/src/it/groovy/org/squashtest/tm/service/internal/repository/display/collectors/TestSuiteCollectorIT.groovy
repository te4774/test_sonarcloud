/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.collectors

import org.apache.commons.collections.CollectionUtils
import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.service.internal.repository.display.impl.collectors.TestSuiteCollector
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet("IterationCollectorIT.xml")
class TestSuiteCollectorIT extends DbunitDaoSpecification {

	@Inject
	TestSuiteCollector testSuiteCollector

	@Inject
	ActiveMilestoneHolder activeMilestoneHolder

	def setup() {
		activeMilestoneHolder.setActiveMilestone(-1L)
	}

	def cleanup() {
		activeMilestoneHolder.clearContext()
	}

	@Unroll
	def "Should collect test suites"() {
		when:
		def nodes = testSuiteCollector.collect(ids)

		then:
		nodes.size() == expectedNodes.size();
		for (id in ids) {
			def expectedNode = expectedNodes.get(id);
			def actualNode = nodes.get(id)

			assert expectedNode.id == actualNode.id;
			Map<String, Object> expectedData = expectedNode.data;
			def actualData = actualNode.data
			assert actualData.size() == expectedData.size()
			expectedData.forEach { key, expectedValue ->
				def actualValue = actualData.get(key)

				if (actualValue instanceof Collection && expectedValue instanceof Collection) {
					assert CollectionUtils.isEqualCollection(actualValue, expectedValue)
				} else {
					assert actualValue == expectedValue
				}
			}
		}

		where:
		ids             || expectedNodes
		null            || []
		[]              || []
		[-1L, -2L, -3L] || [
			(-1L): [id       : "TestSuite--1",
					projectId: -1L,
					data     : [
						ID              : -1L,
						NAME            : "test-suite-1",
						projectId       : -1L,
						EXECUTION_STATUS: "READY",
						CAMPAIGN_ID     : -2,
						MILESTONES      : [],
						MILESTONE_STATUS: [],
						BOUND_TO_BLOCKING_MILESTONE: false
					]
			],
			(-2L): [id       : "TestSuite--2",
					projectId: -1L,
					data     : [
						ID              : -2L,
						NAME            : "test-suite-2",
						projectId       : -1L,
						EXECUTION_STATUS: "READY",
						CAMPAIGN_ID     : -2,
						MILESTONES      : [],
						MILESTONE_STATUS: [],
						BOUND_TO_BLOCKING_MILESTONE: false
					]
			],
			(-3L): [id       : "TestSuite--3",
					projectId: -1L,
					data     : [
						ID              : -3L,
						NAME            : "test-suite-3",
						projectId       : -1L,
						EXECUTION_STATUS: "BLOCKED",
						CAMPAIGN_ID     : -4,
						MILESTONES      : [-1L, -2L],
						MILESTONE_STATUS: ["FINISHED", "LOCKED"],
						BOUND_TO_BLOCKING_MILESTONE: true
					]
			]
		]
	}
}

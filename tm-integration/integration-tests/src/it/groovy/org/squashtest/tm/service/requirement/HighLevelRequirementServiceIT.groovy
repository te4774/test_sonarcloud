/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.requirement


import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.exception.requirement.ForbiddenHighLevelLinkException
import org.squashtest.tm.service.internal.dto.HighLevelRequirementExceptionSummary
import org.squashtest.tm.service.internal.repository.RequirementDao
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class HighLevelRequirementServiceIT extends DbunitServiceSpecification {

	@Inject
	HighLevelRequirementService highLevelRequirementService

	@Inject
	RequirementDao requirementDao

	def "should successfully link a low level requirement to high level one"() {

		when:
		highLevelRequirementService.linkToHighLevelRequirement(-11L, -10L)

		then:
		def requirement = requirementDao.findById(-10L)
		requirement.highLevelRequirement != null
		requirement.highLevelRequirement.id == -11L

	}

	def "should reject link between two high level requirement"() {

		when:
		highLevelRequirementService.linkToHighLevelRequirement(-11L, -12L)

		then:
		thrown ForbiddenHighLevelLinkException

	}

	def "should reject link between the same high level requirement"() {

		when:
		highLevelRequirementService.linkToHighLevelRequirement(-11L, -11L)

		then:
		thrown ForbiddenHighLevelLinkException
	}

	def "should reject link between two standard requirements"() {

		when:
		highLevelRequirementService.linkToHighLevelRequirement(-10L, -13L)

		then:
		// NPE, the HighLevelRequirement with id -10L doesn't exist, so perm manager send a NPE
		thrown NullPointerException
	}

	def "should successfully unlink a low level requirement"() {
		given:
		highLevelRequirementService.linkToHighLevelRequirement(-11L, -10L)

		when:
		highLevelRequirementService.unlinkToHighLevelRequirement(-10L)

		then:
		def requirement = requirementDao.findById(-10L)
		requirement.highLevelRequirement == null

	}

	@DataSet
	def "should not directly link a child requirement to a high level requirement"() {
		given :
		def childRequirementId = -14L
		def highLevelRequirementId = -11L

		when :
		highLevelRequirementService.linkToHighLevelRequirement(highLevelRequirementId, childRequirementId)

		then :
		def childRequirement = requirementDao.findById( -14L)
		childRequirement.highLevelRequirement == null
	}

	@DataSet
	def "should unlink high level requirement reference from requirement and its child"() {
		given :
		def parentRequirementId = -13L
		def childRequirementId = -14L
		highLevelRequirementService.linkToHighLevelRequirement(-11L, parentRequirementId)

		when :
		def parentRequirement = (Requirement)findEntity(Requirement.class, parentRequirementId)
		parentRequirement.highLevelRequirement.id == -11L
		highLevelRequirementService.unlinkToHighLevelRequirement(parentRequirementId)

		then :
		def childRequirement = requirementDao.findById(childRequirementId)
		childRequirement.highLevelRequirement == null
		parentRequirement.highLevelRequirement == null
	}

	@DataSet
	def "should bind requirements to high level requirement"() {
		given :
			def highLevelRequirementId = -12L
			def lowLevelRequirementIds = [-10L, -13L]
			def summary = new HighLevelRequirementExceptionSummary()

		when :
			highLevelRequirementService.bindRequirementsToHighLevelRequirement(highLevelRequirementId, lowLevelRequirementIds, summary)

		then :
			def req10 = requirementDao.findById( -10L)
			def req13 = requirementDao.findById( -13L)

			req10.highLevelRequirement.id == -12L
			req13.highLevelRequirement.id == -12L
	}

	@DataSet
	def "should not bind to high level requirement in specific cases"() {
		given :
			def highLevelRequirementId = -12L
			def lowLevelRequirementIds = [-11L, -14L, -15L, -16L, -17L]
			def summary = new HighLevelRequirementExceptionSummary()

		and :
			highLevelRequirementService.linkToHighLevelRequirement(-11L, -17L)

		when :
			highLevelRequirementService.bindRequirementsToHighLevelRequirement(highLevelRequirementId, lowLevelRequirementIds, summary)

		then :
			def alreadyLinkedToAnotherHighLevelRequirement = requirementDao.findById( -17L)
			def highLevelReq11 = requirementDao.findById( -11L)
			def childReq14 = requirementDao.findById( -14L)
			def childReq16 = requirementDao.findById( -16L)
			def reqWithObsoleteStatus = requirementDao.findById( -15L)

			alreadyLinkedToAnotherHighLevelRequirement.highLevelRequirement.id == -11L
			highLevelReq11.highLevelRequirement == null
			childReq14.highLevelRequirement == null
			childReq16.highLevelRequirement == null
			reqWithObsoleteStatus.highLevelRequirement == null

			summary.alreadyLinked.size() == 0
			summary.alreadyLinkedToAnotherHighLevelRequirement.size() == 1
			summary.childRequirementsInSelection.size() == 2
			summary.requirementWithNotLinkableStatus.size() == 1
			summary.highLevelRequirementsInSelection.size() == 1
	}

	@DataSet
	def "should unbind requirement and its child from high level requirement"() {
		given :
			def highLevelRequirementId = -12L
			def lowLevelRequirementIds = [-13L, -17L]

		and :
			highLevelRequirementService.bindRequirementsToHighLevelRequirement(-12L, [-13L, -17L], new HighLevelRequirementExceptionSummary())

		when :
			highLevelRequirementService.unbindRequirementFromHighLevelRequirement(highLevelRequirementId, lowLevelRequirementIds)

		then :
			def parentReq13 = requirementDao.findById( -13L)
			def childReq14 = requirementDao.findById( -14L)
			def childReq16 = requirementDao.findById( -16L)
			def req17 = requirementDao.findById( -17L)

			parentReq13.highLevelRequirement == null
			childReq14.highLevelRequirement == null
			childReq16.highLevelRequirement == null
			req17.highLevelRequirement == null
	}

	@DataSet
	def "should not unbind child requirement in selection from high level requirement"() {
		given :
			def highLevelRequirementId = -12L
			def lowLevelRequirementIds = [-16L]

		and :
			highLevelRequirementService.linkToHighLevelRequirement(-12L, -13L)

		when :
			highLevelRequirementService.unbindRequirementFromHighLevelRequirement(highLevelRequirementId, lowLevelRequirementIds)

		then :
			def childReq16 = requirementDao.findById( -16L)
			childReq16.highLevelRequirement.id == -12L
	}

}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.search

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.query.Operation
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.service.internal.display.grid.GridFilterValue
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.requirement.HighLevelRequirementService
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.persistence.EntityManager

@UnitilsSupport
@Transactional
@DataSet('RequirementSearchServiceIT.high-level.xml')
class ForHighLevelRequirementSearchServiceIT extends DbunitServiceSpecification {

	static boolean isH2() {
		return System.properties['jooq.sql.dialect'] == null || System.properties['jooq.sql.dialect'] == 'H2'
	}

	def setBidirectionalReqReqVersion(Long reqId, Long reqVersionId) {
		def reqVer = entityManager.find(RequirementVersion.class, reqVersionId)
		def req = entityManager.find(Requirement.class, reqId)
		reqVer.setRequirement(req)
		// Flush is necessary as the dao use native query through jooq.
		entityManager.flush()
	}

	def setup() {
		def ids = [
			[-2L, -2L],
			[-3L, -3L],
			[-3L, -33L],
			[-4L, -4L],
			[-5L, -5L],
			[-5L, -15L],
			[-7L, -7L],
			[-8L, -8L],
			[-9L, -9L],
		]
		ids.each {
			setBidirectionalReqReqVersion(it[0], it[1])
		}
		highLevelRequirementService.linkToHighLevelRequirement(-4, -8)
	}

	@Inject
	private RequirementSearchService service

	@Inject
	private HighLevelRequirementService highLevelRequirementService

	@Inject
	EntityManager entityManager


	@Unroll
	def "should find high level requirement"() {
		given:
		def request = createBaseRequest()
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "REQUIREMENT_KIND"
		filterValue.values = kinds
		filterValue.operation = Operation.IN
		request.scope = ["Project--1", "Project--2"]
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds

		where:
		kinds                                                         || expectedIds
		["org.squashtest.tm.domain.requirement.HighLevelRequirement"] || [-7L, -4L]
		["org.squashtest.tm.domain.requirement.Requirement"]          || [-33, -15, -9, -8, -5, -3, -2]
		["org.squashtest.tm.domain.requirement.HighLevelRequirement",
		 "org.squashtest.tm.domain.requirement.Requirement"]          || [-33, -15, -9, -8, -7, -5, -4, -3, -2]
	}

	@Unroll
	def "should find low level requirement by scope extension"() {
		given:
		def request = createBaseRequest()
		request.scope = ["Project--1"]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds

		where:
		scope          || expectedIds
		["Project--1"] || [-33L, -15, -9, -8, -5, -4, -3, -2]

	}


	def createBaseRequest() {
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request
	}

}

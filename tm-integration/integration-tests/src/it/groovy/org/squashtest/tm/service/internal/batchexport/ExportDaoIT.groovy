/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.batchexport

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.batchexport.RequirementExportModel.LinkedLowLevelRequirementModel
import org.squashtest.tm.service.internal.batchexport.RequirementExportModel.RequirementLinkModel
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.domain.testcase.TestCaseKind.GHERKIN
import static org.squashtest.tm.domain.testcase.TestCaseKind.STANDARD

@UnitilsSupport
@Transactional
class ExportDaoIT extends DbunitServiceSpecification{

	@Inject
	private ExportDao exporter


	@DataSet("ExportDaoIT.should create models.xml")
	def "should create models"(){
		given :
		def testCaseIds = [-10L, -11L]

		when :
		ExportModel result = exporter.findModel(testCaseIds)

		then :
		result.testCases.size() == 2
		result.datasets.size() == 1
		result.parameters.size() == 1
		result.testSteps.size() == 2

	}

	@DataSet("ExportDaoIT.should create scripted test case model.xml")
	def "should create scripted test case models"(){
		given :
		def testCaseIds = [-10L, -11L]

		when :
		ExportModel result = exporter.findModel(testCaseIds)
		List<ExportModel.TestCaseModel> testCases = result.testCases
		ExportModel.TestCaseModel standardTestCaseModel = testCases.find{it.id == -10L}
		ExportModel.TestCaseModel scriptedTestCaseModel = testCases.find{it.id == -11L}


		then :
		testCases.size() == 2
		standardTestCaseModel.getTestCaseKind() == STANDARD

		scriptedTestCaseModel.getTestCaseKind() == GHERKIN
		scriptedTestCaseModel.getTcScript() == "Feature: three cucumbers and two tomatoes"

	}


	/*
	 * See dataset description in the dataset file
	 */
	@DataSet("export-req-links.xml")
	def "should collect the requirement links model"(){

		given :
			// setup : must fix the reciprocal references from requirement to requirement version
			// (it had to be stripped from the dataset because it wouldn't be inserted otherwise)
			// the following maps Requirement ids to RequirementVersion ids
			[
				(-255) : -255,
				(-256) : -256,
				(-257) : -259,
				(-258) : -260,
				(-259) : -261
			]
			.each { k,v -> executeSQL("update REQUIREMENT set current_version_id = $v where rln_id = $k") }


		and :
			def ids = -259l..-255l

		when :
			List<RequirementLinkModel> linkModels = exporter.findRequirementLinkModels(ids)

		then :
			linkModels.size() == 4

			// first, test what should be contained
			def req1v1_req3v1_related = model([reqPath : "/Test Project-1/Test Folder 1/Test Requirement 1", reqVersion : 1, relReqPath : "/Test Project-1/Test Folder 1/Test Requirement 3", relReqVersion : 1, relatedReqRole : "RELATED"])
			def req1v1_req3v3_duplicate = model([reqPath : "/Test Project-1/Test Folder 1/Test Requirement 1", reqVersion : 1, relReqPath : "/Test Project-1/Test Folder 1/Test Requirement 3", relReqVersion : 3, relatedReqRole : "DUPLICATE"])
			def req2v1_req1v1_child = model([reqPath : "/Test Project-1/Test Folder 1/Test Requirement 2", reqVersion : 1, relReqPath : "/Test Project-1/Test Folder 1/Test Requirement 1", relReqVersion : 1, relatedReqRole : "CHILD"])
			def req3v3_otherv1_child = model([reqPath : "/Test Project-1/Test Folder 1/Test Requirement 3", reqVersion : 3, relReqPath : "/Test Project-1/related to ex3", relReqVersion : 1, relatedReqRole : "CHILD"])

			linkModels.find { match(it, req1v1_req3v1_related) } != null
			linkModels.find { match(it, req1v1_req3v3_duplicate) } != null
			linkModels.find { match(it, req2v1_req1v1_child) } != null
			linkModels.find { match(it, req3v3_otherv1_child) } != null

			// now those that should not be found
			def unrelated = model([reqPath : "/Test Project-1/unrelated", reqVersion : 1, relReqPath : "/Test Project-1/related to ex3", relReqVersion : 1, relatedReqRole : "DUPLICATE"])

			linkModels.find { match(it, unrelated) } == null

			true

	}


	private boolean match(RequirementLinkModel model1, RequirementLinkModel model2){
		return model1.reqPath.equals(model2.reqPath) &&
				(model1.reqVersion == model2.reqVersion) &&
				model1.relReqPath.equals(model2.relReqPath) &&
				(model1.relReqVersion == model2.relReqVersion) &&
				model1.relatedReqRole.equals(model2.relatedReqRole)
	}

	private RequirementLinkModel model(Map ppts){
		return RequirementLinkModel.create(ppts["reqVersion"], ppts["relReqVersion"], ppts["relatedReqRole"], ppts["reqPath"], ppts["relReqPath"])
	}

	@DataSet("export-linked-low-level-reqs.xml")
	def "should collect the linked low level requirement models"(){

		given :
			// setup : must fix the reciprocal references from requirement to requirement version
			// (it had to be stripped from the dataset because it wouldn't be inserted otherwise)
			// the following maps Requirement ids to RequirementVersion ids
			[
					(-255) : -255,
					(-256) : -256,
					(-257) : -259,
					(-258) : -260,
					(-259) : -261
			].each { k,v -> executeSQL("update REQUIREMENT set current_version_id = $v where rln_id = $k") }

			// add high level req link
			[
					(-255) : -258,
					(-256) : -258,
					(-257) : -258
			].each { k,v -> executeSQL("update REQUIREMENT set high_level_requirement_id = $v where rln_id = $k") }

		and :
			def ids = [-255, -256, -259, -260, -261]

		when :
			List<LinkedLowLevelRequirementModel> linkedLowLevelReqModels = exporter.findLinkedLowLevelReqModels(ids)

		then :
			linkedLowLevelReqModels.size() == 3

			// first, test what should be contained
			def hlReq1_req1 = linkedLowLevelReqModel([highLevelReqPath : "/Test Project-1/HL Folder 1/HL Requirement 1", linkedLowLevelReqPath : "/Test Project-1/Req Folder 1/Test Requirement 1"])
			def hlReq1_req2 = linkedLowLevelReqModel([highLevelReqPath : "/Test Project-1/HL Folder 1/HL Requirement 1", linkedLowLevelReqPath : "/Test Project-1/Req Folder 1/Test Requirement 2"])
			def hlReq1_req3 = linkedLowLevelReqModel([highLevelReqPath : "/Test Project-1/HL Folder 1/HL Requirement 1", linkedLowLevelReqPath : "/Test Project-1/Req Folder 1/Test Requirement 3"])

			linkedLowLevelReqModels.find { matchLinkedLowLevelReqModel(it, hlReq1_req1) } != null
			linkedLowLevelReqModels.find { matchLinkedLowLevelReqModel(it, hlReq1_req2) } != null
			linkedLowLevelReqModels.find { matchLinkedLowLevelReqModel(it, hlReq1_req3) } != null

			// now those that should not be found
			def unrelated = linkedLowLevelReqModel([highLevelReqPath : "/Test Project-1/HL Folder 1/HL Requirement 1", linkedLowLevelReqPath : "/Test Project-1/unrelated"])

			linkedLowLevelReqModels.find { matchLinkedLowLevelReqModel(it, unrelated) } == null
	}


	private static boolean matchLinkedLowLevelReqModel(LinkedLowLevelRequirementModel model1, LinkedLowLevelRequirementModel model2){
		return model1.highLevelReqPath == model2.highLevelReqPath &&
				model1.linkedLowLevelReqPath == model2.linkedLowLevelReqPath
	}

	private static LinkedLowLevelRequirementModel linkedLowLevelReqModel(Map ppts){
		return LinkedLowLevelRequirementModel.createLinkLowLevelReqModel(ppts["highLevelReqPath"], ppts["linkedLowLevelReqPath"])
	}
}

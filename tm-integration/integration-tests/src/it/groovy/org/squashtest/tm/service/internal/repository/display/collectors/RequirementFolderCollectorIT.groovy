/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.collectors

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.service.internal.display.grid.DataRow
import org.squashtest.tm.service.internal.repository.display.impl.collectors.RequirementFolderCollector
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet("RequirementCollectorIT.xml")
class RequirementFolderCollectorIT extends DbunitDaoSpecification {

	@Inject
	RequirementFolderCollector requirementFolderCollector

	@Inject
	ActiveMilestoneHolder activeMilestoneHolder

	def setup() {
		activeMilestoneHolder.setActiveMilestone(-1L)
	}

	def cleanup() {
		activeMilestoneHolder.clearContext()
	}


	@Unroll
	def "Should collect requirement folders"() {
		when:
		def collectedNodes = requirementFolderCollector.collect(ids)
		then:
		collectedNodes.size() == expectedNodes.size()
		for (id in ids) {
			def expectedNode = expectedNodes.get(id)
			def collectedNode = collectedNodes.get(id)

			def expectedData = expectedNode.data
			def collectedData = collectedNode.data

			assert expectedNode.id == collectedNode.id
			assert expectedNode.state == collectedNode.state
			assert expectedData.size() == collectedData.size()
			expectedData.forEach { key, expectedValue ->
				def collectedValue = collectedData.get(key)
				assert expectedValue == collectedValue
			}
		}
		where:
		ids             | expectedNodes
		null            | []
		[]              | []
		[-1L, -6L, -8L] | [
			(-1L): [
				id       : "RequirementFolder--1",
				state    : DataRow.State.closed,
				projectId: -1L,
				data     : [
					RLN_ID                     : -1L,
					NAME                       : "Folder_1",
					projectId                  : -1L,
					CHILD_COUNT                : 2,
					"CUF_COLUMN_-1"            : "Hello",
					"CUF_COLUMN_-7"            : "Hola",
					IS_SYNCHRONIZED            : false,
					LAST_SYNC_STATUS           : null,
					REMOTE_SYNCHRONISATION_NAME: null,
					MILESTONES                 : [-1L],
					BOUND_TO_BLOCKING_MILESTONE  : false,
					HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ: false,
				],
			],
			(-6L): [
				id       : "RequirementFolder--6",
				state    : DataRow.State.closed,
				projectId: -2L,
				data     : [
					RLN_ID                     : -6L,
					NAME                       : "Folder_A",
					projectId                  : -2L,
					CHILD_COUNT                : 1,
					"CUF_COLUMN_-3"            : "73110",
					"CUF_COLUMN_-6"            : "Hei|Hej|Terve",
					IS_SYNCHRONIZED            : true,
					LAST_SYNC_STATUS           : "SUCCESS",
					REMOTE_SYNCHRONISATION_NAME: "sync1",
					MILESTONES                 : [],
					BOUND_TO_BLOCKING_MILESTONE  : false,
					HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ: true,
				]
			],
			(-8L): [
				id       : "RequirementFolder--8",
				state    : DataRow.State.leaf,
				projectId: -2L,
				data     : [
					RLN_ID                     : -8L,
					NAME                       : "Folder_B",
					projectId                  : -2L,
					CHILD_COUNT                : 0,
					"CUF_COLUMN_-3"            : "8059074",
					"CUF_COLUMN_-6"            : "Szia|Zdravo|Pryvit",
					IS_SYNCHRONIZED            : false,
					LAST_SYNC_STATUS           : null,
					REMOTE_SYNCHRONISATION_NAME: null,
					MILESTONES                 : [],
					BOUND_TO_BLOCKING_MILESTONE  : false,
					HAS_OUT_OF_PERIMETER_OR_DELETED_REMOTE_REQ: false,
				]
			]
		]
	}
}

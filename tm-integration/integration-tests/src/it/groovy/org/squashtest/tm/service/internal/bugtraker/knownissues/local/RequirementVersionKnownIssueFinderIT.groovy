/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.local

import org.springframework.data.domain.Pageable
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.core.foundation.collection.PagingAndSorting
import org.squashtest.tm.core.foundation.collection.SortOrder
import org.squashtest.tm.service.internal.repository.RequirementDao
import org.squashtest.tm.service.internal.repository.RequirementVersionDao
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet
class RequirementVersionKnownIssueFinderIT extends DbunitServiceSpecification {

	@Inject RequirementVersionKnownIssueFinder requirementVersionKnownIssueFinder

	@Inject RequirementVersionDao requirementVersionDao
	@Inject RequirementDao requirementDao

	def setup () {
		setBidirectionalReqReqVersion(-1L, -11L)
		setBidirectionalReqReqVersion(-1L, -12L)
		setBidirectionalReqReqVersion(-1L, -13L)
		setBidirectionalReqReqVersion(-2L, -21L)
	}

	def setBidirectionalReqReqVersion(Long reqId, Long reqVersionId) {
		def req = requirementDao.findById(reqId)
		def reqVer = requirementVersionDao.getOne(reqVersionId)
		if (reqVer != null && req != null) reqVer.setRequirement(req)
	}

	def "should find known issues for requirement version" () {
		when:
		def issues = requirementVersionKnownIssueFinder.getPaginatedKnownIssues(reqVersionId, mockPaging(0, 10))

		then:
		issues.size() == expectedIssueIds.size()
		issues.every({ issue -> expectedIssueIds.contains(issue.remoteIssueId) })
		expectedIssueIds.every({ expected -> issues.find({ issue -> issue.remoteIssueId == expected })})

		where:
		reqVersionId	|| expectedIssueIds
		-11L        	|| ["1", "2", "3", "4"]
		-12L        	|| []
		-13L		   	|| ["1", "5", "7"]

		// Own issues ("8") plus its children's issues (-13L)
		-21L		   	|| ["1", "5", "7", "8"]

		-99L			|| []
	}

	def "should get correct count for req version known issues" () {
		when:
		def count = requirementVersionKnownIssueFinder.countKnownIssues(reqVersionId)

		then:
		count == expectedCount

		where:
		reqVersionId	|| expectedCount
		-11L        	|| 4
		-12L        	|| 0
		-13L        	|| 3

		-21L		   	|| 4

		-99L			|| 0
	}

	def mockPaging(firstItemIndex, pageSize) {
		return new PagingAndSorting() {
			@Override
			int getFirstItemIndex() { return firstItemIndex }

			@Override
			int getPageSize() { return pageSize }

			@Override
			boolean shouldDisplayAll() { return false }

			@Override
			Pageable toPageable() { return null }

			@Override
			String getSortedAttribute() { return null }

			@Override
			SortOrder getSortOrder() { return SortOrder.DESCENDING }
		}
	}
}

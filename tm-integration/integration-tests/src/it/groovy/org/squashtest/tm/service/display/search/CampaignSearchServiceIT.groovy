/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.search

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.grid.GridFilterValue
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class CampaignSearchServiceIT extends DbunitServiceSpecification {

	static boolean isPostgresql() {
		return System.properties['jooq.sql.dialect'] == 'postgres'
	}

	@Inject
	private CampaignSearchService service

	@Unroll
	def "should find itpi last executed by"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "ITEM_TEST_PLAN_LASTEXECBY"
		filterValue.values = researchedNames
		filterValue.operation = 'IN'
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		researchedNames   || expectedIds          | expectedCount
		["bob"]           || [-3L, -2L, -1L]      | 3
		["robert"]        || [-4L]                | 1
		["bob", "robert"] || [-4L, -3L, -2L, -1L] | 4
	}

	def "should sort on dataset name column"() {
		given:
		def sort = new GridSort("datasetName", GridSort.SortDirection.DESC)
		sort.columnPrototype = "DATASET_NAME"

		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request.sort = [sort]

		when:
		def researchResult = service.search(request)

		then:
		// [SQUASH-3988]
		// has to do some conditional testing because one of the goal of this test is to check null dataset handling.
		// so we need nulls values and thus the order by became dependant of database...
		if (isPostgresql()) {
			researchResult.ids == [-5L, -4L, -1L, -2L, -3L]
		} else {
			researchResult.ids == [-2L, -3L, -5L, -4L, -1L]
		}
		researchResult.count == 5
	}

}

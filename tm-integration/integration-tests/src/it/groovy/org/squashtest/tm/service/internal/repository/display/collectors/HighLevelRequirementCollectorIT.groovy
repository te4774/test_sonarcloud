/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.display.collectors

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.service.internal.display.grid.DataRow
import org.squashtest.tm.service.internal.repository.display.impl.collectors.HighLevelRequirementCollector
import org.squashtest.tm.service.milestone.ActiveMilestoneHolder
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.persistence.EntityManager

import static org.squashtest.tm.jooq.domain.Tables.REQUIREMENT_SYNC_EXTENDER

@UnitilsSupport
@DataSet
class HighLevelRequirementCollectorIT extends DbunitDaoSpecification {

	@Inject
	HighLevelRequirementCollector highLevelRequirementCollector

	@Inject
	ActiveMilestoneHolder activeMilestoneHolder

	@Inject
	EntityManager entityManager

	def setBidirectionalReqReqVersion(Long reqId, Long reqVersionId) {
		def reqVer = entityManager.find(RequirementVersion.class, reqVersionId)
		def req = entityManager.find(Requirement.class, reqId)
		reqVer.setRequirement(req)
		// Flush is necessary as the dao use native query through jooq.
		entityManager.flush()
	}

	def setup() {
		activeMilestoneHolder.setActiveMilestone(-1L)
		def ids = [
			[-2L, -2L],
			[-3L, -3L],
			[-3L, -33L],
			[-4L, -4L],
			[-5L, -5L],
			[-5L, -15L],
			[-7L, -7L],
		]
		ids.each {
			setBidirectionalReqReqVersion(it[0], it[1])
		}
	}

	def cleanup() {
		activeMilestoneHolder.clearContext()
	}

	@Unroll
	def "Should collect high level requirements"() {

		when:
		def collectedNodes = highLevelRequirementCollector.collect(ids)

		then:
		collectedNodes.size() == expectedNodes.size()
		for (id in ids) {
			def expectedNode = expectedNodes.get(id)
			def collectedNode = collectedNodes.get(id)

			def expectedData = expectedNode.data
			def collectedData = collectedNode.data

			assert expectedNode.id == collectedNode.id
			assert expectedNode.state == collectedNode.state
			assert expectedData.size() == collectedData.size()
			expectedData.forEach { key, expectedValue ->
				def collectedValue = collectedData.get(key)
				assert expectedValue == collectedValue
			}
		}
		where:
		ids                       | expectedNodes
		null                      | []
		[]                        | []
		[-2L, -3L, -4L, -5L, -7L] | [
			(-2L): [
				id   : "HighLevelRequirement--2",
				state: DataRow.State.leaf,
				data : [
					RLN_ID                     : -2L,
					NAME                       : "REF-001 - Requirement_1",
					projectId                  : -1L,
					CHILD_COUNT                : 0,
					"CUF_COLUMN_-2"            : "Ni Hao",
					REFERENCE                  : "REF-001",
					CRITICALITY                : "UNDEFINED",
					REQUIREMENT_STATUS         : "WORK_IN_PROGRESS",
					HAS_DESCRIPTION            : true,
					REQ_CATEGORY_ICON          : "monitor",
					REQ_CATEGORY_LABEL         : "requirement.category.CAT_FUNCTIONAL",
					REQ_CATEGORY_TYPE          : "SYS",
					COVERAGE_COUNT             : 0,
					IS_SYNCHRONIZED            : false,
					MILESTONES                 : [],
					INDIRECT_MILESTONE_BIND    : false,
					DIRECT_MILESTONE_BIND      : false,
					ALL_MILESTONES             : [],
					BOUND_TO_BLOCKING_MILESTONE: false,
					REMOTE_REQ_PERIMETER_STATUS: null,
					REMOTE_SYNCHRONISATION_ID  : null
				]
			],
			(-3L): [
				id   : "HighLevelRequirement--3",
				state: DataRow.State.leaf,
				data : [
					RLN_ID                     : -3L,
					NAME                       : "Requirement_2_v2",
					projectId                  : -1L,
					CHILD_COUNT                : 0,
					"CUF_COLUMN_-2"            : "Konnichi Wa",
					REFERENCE                  : "",
					CRITICALITY                : "UNDEFINED",
					REQUIREMENT_STATUS         : "WORK_IN_PROGRESS",
					HAS_DESCRIPTION            : false,
					REQ_CATEGORY_ICON          : "monitor",
					REQ_CATEGORY_LABEL         : "requirement.category.CAT_FUNCTIONAL",
					REQ_CATEGORY_TYPE          : "SYS",
					COVERAGE_COUNT             : 2,
					IS_SYNCHRONIZED            : false,
					MILESTONES                 : [],
					INDIRECT_MILESTONE_BIND    : false,
					DIRECT_MILESTONE_BIND      : false,
					ALL_MILESTONES             : [],
					BOUND_TO_BLOCKING_MILESTONE: false,
					REMOTE_REQ_PERIMETER_STATUS: null,
					REMOTE_SYNCHRONISATION_ID  : null
				]
			],
			(-4L): [
				id   : "HighLevelRequirement--4",
				state: DataRow.State.leaf,
				data : [
					RLN_ID                     : -4L,
					NAME                       : "Requirement_3",
					projectId                  : -1L,
					CHILD_COUNT                : 0,
					"CUF_COLUMN_-2"            : "Annyong Haseyo",
					REFERENCE                  : "",
					CRITICALITY                : "UNDEFINED",
					REQUIREMENT_STATUS         : "WORK_IN_PROGRESS",
					HAS_DESCRIPTION            : false,
					REQ_CATEGORY_ICON          : "monitor",
					REQ_CATEGORY_LABEL         : "requirement.category.CAT_FUNCTIONAL",
					REQ_CATEGORY_TYPE          : "SYS",
					COVERAGE_COUNT             : 0,
					IS_SYNCHRONIZED            : false,
					MILESTONES                 : [],
					INDIRECT_MILESTONE_BIND    : false,
					DIRECT_MILESTONE_BIND      : false,
					ALL_MILESTONES             : [],
					BOUND_TO_BLOCKING_MILESTONE: false,
					REMOTE_REQ_PERIMETER_STATUS: null,
					REMOTE_SYNCHRONISATION_ID  : null
				]
			],
			(-5L): [
				id   : "HighLevelRequirement--5",
				state: DataRow.State.leaf,
				data : [
					RLN_ID                     : -5L,
					NAME                       : "Requirement_4_milestone",
					projectId                  : -1L,
					CHILD_COUNT                : 0,
					"CUF_COLUMN_-2"            : "Chao",
					REFERENCE                  : "",
					CRITICALITY                : "UNDEFINED",
					REQUIREMENT_STATUS         : "WORK_IN_PROGRESS",
					HAS_DESCRIPTION            : false,
					REQ_CATEGORY_ICON          : "monitor",
					REQ_CATEGORY_LABEL         : "requirement.category.CAT_FUNCTIONAL",
					REQ_CATEGORY_TYPE          : "SYS",
					COVERAGE_COUNT             : 0,
					IS_SYNCHRONIZED            : false,
					MILESTONES                 : [-1L],
					INDIRECT_MILESTONE_BIND    : false,
					DIRECT_MILESTONE_BIND      : true,
					ALL_MILESTONES             : [-1L],
					BOUND_TO_BLOCKING_MILESTONE: false,
					REMOTE_REQ_PERIMETER_STATUS: null,
					REMOTE_SYNCHRONISATION_ID  : null
				]
			],
			(-7L): [
				id   : "HighLevelRequirement--7",
				state: DataRow.State.leaf,
				data : [
					RLN_ID                     : -7L,
					NAME                       : "Requirement_A",
					projectId                  : -2L,
					CHILD_COUNT                : 0,
					"CUF_COLUMN_-4"            : "2020-01-01",
					"CUF_COLUMN_-5"            : "true",
					REFERENCE                  : "",
					CRITICALITY                : "UNDEFINED",
					REQUIREMENT_STATUS         : "WORK_IN_PROGRESS",
					HAS_DESCRIPTION            : false,
					REQ_CATEGORY_ICON          : "monitor",
					REQ_CATEGORY_LABEL         : "requirement.category.CAT_FUNCTIONAL",
					REQ_CATEGORY_TYPE          : "SYS",
					COVERAGE_COUNT             : 0,
					IS_SYNCHRONIZED            : false,
					MILESTONES                 : [],
					INDIRECT_MILESTONE_BIND    : false,
					DIRECT_MILESTONE_BIND      : false,
					ALL_MILESTONES             : [],
					BOUND_TO_BLOCKING_MILESTONE: false,
					REMOTE_REQ_PERIMETER_STATUS: null,
					REMOTE_SYNCHRONISATION_ID  : null
				]
			]
		]
	}
}

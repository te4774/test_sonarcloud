/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.campaign

import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.ContextHierarchy
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.it.config.EnabledAclSpecConfig
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.sql.Timestamp
import java.text.SimpleDateFormat

@UnitilsSupport
@Transactional
@DataSet
@ContextHierarchy([
	@ContextConfiguration(classes = [EnabledAclSpecConfig], name = "aclcontext", inheritLocations = false),
])
class TestSuiteDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private TestSuiteDisplayService testSuiteDisplayService

	def "should fetch a test suite test-plan"() {

		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def gridResponse = this.testSuiteDisplayService.findTestPlan(-1L, gridRequest)

		then:
		gridResponse.count == 6
		def rows = gridResponse.dataRows
		def itpi = rows.get(0)
		itpi.id == "-1"
		itpi.data.get("itemTestPlanId") == -1L
		itpi.data.get("projectName") == "pro"
		itpi.data.get("executionMode") == "MANUAL"
		itpi.data.get("testCaseReference") == "REF"
		itpi.data.get("testCaseName") == "test case 1"
		itpi.data.get("importance") == "VERY_HIGH"
		itpi.data.get("datasetName") == "Dataset1"
		itpi.data.get("executionStatus") == "SUCCESS"
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")

		def minDate = itpi.data.get("milestoneMinDate")
		def maxDate = itpi.data.get("milestoneMaxDate")
		minDate == dateFormat.parse("2010-02-01").toLocalDateTime()
		maxDate == dateFormat.parse("2020-02-01").toLocalDateTime()
		itpi.data.get("milestoneLabels") == "M1, M3, M2"
		Math.floor((Integer) itpi.data.get("successRate")) == 66
		itpi.data.get("user") == "Paul (JP01)"
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		itpi.data.get("lastExecutedOn") == dateTimeFormat.parse("2020-04-03 09:32:35")

		def otherItpi = rows.get(1)
		otherItpi.id == "-2"
		otherItpi.data.get("itemTestPlanId") == -2L
		otherItpi.data.get("executionStatus") == "FAILURE"
		otherItpi.data.get("successRate") == 0.0
		otherItpi.data.get("lastExecutedOn") == null
		otherItpi.data.get("user") == null
	}

	@DataSet("TestSuiteDisplayServiceIT.automation-configurations.xml")
	def "should fetch test suite test plan with item of different automation configurations"() {
		given:
			def gridRequest = new GridRequest()
			gridRequest.size = 25
		when:
			def gridResponse = this.testSuiteDisplayService.findTestPlan(-1L, gridRequest)
		then:
			gridResponse.count == 6
			def rows = gridResponse.dataRows
			rows.find {it.id == "-1" }.data.get("executionMode") == "AUTOMATED"
			rows.find {it.id == "-2" }.data.get("executionMode") == "AUTOMATED"
			rows.find {it.id == "-3" }.data.get("executionMode") == "MANUAL"
			rows.find {it.id == "-4" }.data.get("executionMode") == "MANUAL"
			rows.find {it.id == "-5" }.data.get("executionMode") == "MANUAL"
			rows.find {it.id == "-6" }.data.get("executionMode") == "MANUAL"
	}

	def "should fetch an iteration test plan and append available datasets"() {
		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25

		when:
		def gridResponse = this.testSuiteDisplayService.findTestPlan(-1L, gridRequest)

		then:
		gridResponse.dataRows.get(0).data.get("datasetName") == "Dataset1"
		gridResponse.dataRows.get(0).data.get("availableDatasets").size() == 2
		gridResponse.dataRows.get(1).data.get("datasetName") == null
	}
}

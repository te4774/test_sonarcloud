/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.search


import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
@DataSet
class CampaignSearchGridDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private CampaignSearchGridDisplayService service

	def "should return a grid response" () {

		given:
		def ids = [-1L]
		def researchResult = new ResearchResult(ids, 1)

		when:
		def response = service.fetchResearchRows(researchResult)
		then:
		response.count == 1
		def dataRows = response.dataRows
		def firstItpi = dataRows[0]
		firstItpi.data["id"] == -1L
		firstItpi.data["label"] == "test case 1"
		firstItpi.data["executionExecutionMode"] == "MANUAL"
		firstItpi.data["iterationName"] == "Iteration 1"
		firstItpi.data["campaignName"] == "CAMPAIGN 1"
		firstItpi.data["automatable"] == "M"
		firstItpi.data["projectName"] == "bar"
	}

	@DataSet("CampaignSearchGridDisplayServiceIT.automation-configurations.xml")
	def "should fetch search result with items of different automation configurations"() {
		given:
			def researchResult = new ResearchResult([-11L, -22L, -33L, -44L, -55L, -66L], 6)
		when:
			def response = service.fetchResearchRows(researchResult)
		then:
			response.count == 6
			def rows = response.dataRows
			rows.find {it.id == "-11" }.data.get("executionExecutionMode") == "AUTOMATED"
			rows.find {it.id == "-22" }.data.get("executionExecutionMode") == "AUTOMATED"
			rows.find {it.id == "-33" }.data.get("executionExecutionMode") == "MANUAL"
			rows.find {it.id == "-44" }.data.get("executionExecutionMode") == "MANUAL"
			rows.find {it.id == "-55" }.data.get("executionExecutionMode") == "MANUAL"
			rows.find {it.id == "-66" }.data.get("executionExecutionMode") == "MANUAL"
	}
}

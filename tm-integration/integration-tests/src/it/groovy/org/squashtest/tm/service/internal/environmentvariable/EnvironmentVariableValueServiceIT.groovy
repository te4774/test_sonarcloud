/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable

import org.squashtest.it.basespecs.DbunitServiceSpecification
import spock.unitils.UnitilsSupport
import org.squashtest.tm.domain.environmentvariable.EVBindableEntity
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableValue
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableValueService
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao
import org.squashtest.tm.service.internal.repository.EnvironmentVariableValueDao
import org.unitils.dbunit.annotation.DataSet
import org.springframework.transaction.annotation.Transactional

import javax.inject.Inject


@Transactional
@UnitilsSupport
@DataSet("EnvironmentVariableBindingService.xml")
class EnvironmentVariableValueServiceIT extends DbunitServiceSpecification {

	@Inject
	EnvironmentVariableValueService environmentVariableValueService

	@Inject
	EnvironmentVariableValueDao environmentVariableValueDao

	@Inject
	EnvironmentVariableBindingDao environmentVariableBindingDao

	def "Should remove environment variable value by binding ids"() {
		given:
		List<Long> bindingIds = Arrays.asList(-1L, -2L)

		when:
		environmentVariableValueService.removeEnvironmentVariableValuesByBindingIds(bindingIds)

		then:
		List<EnvironmentVariableValue> environmentVariableValues = environmentVariableValueDao.findAllByBindingIds(bindingIds)
		environmentVariableValues.isEmpty()
	}

	def "Should edit environment variable value from server"() {
		given:
		Long environmentVariableId = -1L
		Long testAutomationServerId = -1L
		String value = "test new value"

		when:
		environmentVariableValueService.editEnvironmentVariableValueFromServer(testAutomationServerId,
			environmentVariableId, value)

		then:
		EnvironmentVariableBinding binding = environmentVariableBindingDao
			.findByServerIdAndEvId(testAutomationServerId, environmentVariableId)
		EnvironmentVariableValue updatedValue = environmentVariableValueDao
			.findSingleEnvironmentVariableValue(binding, EVBindableEntity.TEST_AUTOMATION_SERVER, testAutomationServerId)
		updatedValue.getValue() == value
	}

	def "should update environment variable value from project"() {
		given:
		String updatedValue = "updatedValue"
		EnvironmentVariableBinding binding = environmentVariableBindingDao
			.findByServerIdAndEvId(-1L, -3L)

		EnvironmentVariableValue existingValue = environmentVariableValueDao.
			findSingleEnvironmentVariableValue(binding, EVBindableEntity.PROJECT, -1L)
		String oldValue = existingValue.getValue()

		when:
		environmentVariableValueService.editEnvironmentVariableValueFromProject(-1L, -3L, updatedValue)

		then:
		existingValue != null
		oldValue != updatedValue
		existingValue.getValue() == updatedValue
	}

	def "should create environment variable value from project"() {
		given:
		String updatedValue = "updatedValue"
		EnvironmentVariableBinding binding = environmentVariableBindingDao
			.findByServerIdAndEvId(-1L, -1L)

		EnvironmentVariableValue existingValue = environmentVariableValueDao.
			findSingleEnvironmentVariableValue(binding, EVBindableEntity.PROJECT, -1L)


		when:
		environmentVariableValueService.editEnvironmentVariableValueFromProject(-1L, -1L, updatedValue)

		then:
		existingValue == null
		EnvironmentVariableValue createdValue = environmentVariableValueDao.
			findSingleEnvironmentVariableValue(binding, EVBindableEntity.PROJECT, -1L)
		createdValue != null
	}

	def "should reset default value from project view"() {
		given:
		EnvironmentVariableBinding binding = environmentVariableBindingDao
			.findByServerIdAndEvId(-1L, -3L)
		EnvironmentVariableValue existingProjectValue = environmentVariableValueDao.
			findSingleEnvironmentVariableValue(binding, EVBindableEntity.PROJECT, -1L)

		when:
		def result = environmentVariableValueService.resetDefaultValue(-1L, -3L, EVBindableEntity.PROJECT.name())

		then:
		existingProjectValue != null
		EnvironmentVariableValue deletedProjectValue = environmentVariableValueDao.
			findSingleEnvironmentVariableValue(binding, EVBindableEntity.PROJECT, -1L)
		deletedProjectValue == null
		result instanceof String
	}

}

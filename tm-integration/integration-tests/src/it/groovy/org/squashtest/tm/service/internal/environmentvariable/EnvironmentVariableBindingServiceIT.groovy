/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.environmentvariable

import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.unitils.dbunit.annotation.DataSet
import org.springframework.transaction.annotation.Transactional
import spock.unitils.UnitilsSupport
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableBindingService
import org.squashtest.tm.service.environmentvariable.EnvironmentVariableValueService
import org.squashtest.tm.service.internal.repository.CustomEnvironmentVariableBindingDao
import org.squashtest.tm.service.internal.repository.EnvironmentVariableBindingDao
import org.squashtest.tm.service.internal.repository.EnvironmentVariableDao
import org.squashtest.tm.service.internal.repository.TestAutomationServerDao
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding
import org.squashtest.tm.domain.testautomation.TestAutomationServer
import javax.persistence.EntityExistsException;

import javax.inject.Inject

@Transactional
@UnitilsSupport
@DataSet("EnvironmentVariableBindingService.xml")
class EnvironmentVariableBindingServiceIT extends DbunitServiceSpecification {

	@Inject
	EnvironmentVariableBindingService environmentVariableBindingService

	@Inject
	EnvironmentVariableDao environmentVariableDao

	@Inject
	EnvironmentVariableBindingDao environmentVariableBindingDao

	@Inject
	TestAutomationServerDao serverDao

	@Inject
	CustomEnvironmentVariableBindingDao customEnvironmentVariableBindingDao

	@Inject
	EnvironmentVariableValueService environmentVariableValueService



	def "should create new binding"() {
		given:
		List<Long> environmentVariableIds = Arrays.asList(-2L)

		when:
		environmentVariableBindingService.createNewBindings(-1L, environmentVariableIds)

		then:
		EnvironmentVariableBinding newBinding = environmentVariableBindingDao.findByServerIdAndEvId(-1L, -2L)
		newBinding.getTestAutomationServer().getId() == -1
		newBinding.getEnvironmentVariable().getId() == -2

	}

	def "should fail when create new binding"() {
		given:
		List<Long> environmentVariableIds = Arrays.asList(-1L)

		when:
		environmentVariableBindingService.createNewBindings(-1L, environmentVariableIds)

		then:
		thrown EntityExistsException
	}

	def "should unbind environment variable"() {
		given:
		List<Long> environmentVariableIds = Arrays.asList(-1L)

		when:
		environmentVariableBindingService.unbind(-1L, environmentVariableIds)

		then:
		EnvironmentVariableBinding deletedBinding = environmentVariableBindingDao.findByServerIdAndEvId(-1L, -1L)
		deletedBinding == null
	}

	def "should unbind environment variable by evId"() {
		given:
		EnvironmentVariableBinding bindingToDelete = environmentVariableBindingDao.findByServerIdAndEvId(-1L, -1L)
		Long environmentVariableId = bindingToDelete.getEnvironmentVariable().getId()
		when:
		environmentVariableBindingService.unbindByEnvironmentVariableId(environmentVariableId)

		then:
		EnvironmentVariableBinding deletedBinding = environmentVariableBindingDao.findByServerIdAndEvId(-1L, -1L)
		deletedBinding == null
	}


}

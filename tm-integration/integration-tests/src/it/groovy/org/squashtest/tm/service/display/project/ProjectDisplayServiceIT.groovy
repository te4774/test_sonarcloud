/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.project

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.service.internal.display.dto.MilestoneProjectViewDto
import org.squashtest.tm.service.internal.display.grid.GridFilterValue
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.internal.display.grid.GridSort
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import java.text.SimpleDateFormat

@UnitilsSupport
@Transactional
@DataSet
class ProjectDisplayServiceIT extends DbunitServiceSpecification {

	@Inject
	private ProjectDisplayService projectDisplayService

	def "should fetch projects"() {

		given:
		def gridRequest = new GridRequest()
		gridRequest.size = 25
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")

		when:
		def gridResponse = projectDisplayService.findAll(gridRequest)

		then:
		gridResponse.count == 3
		def rows = gridResponse.dataRows
		rows.get(0).data.projectId == -101L
		rows.get(0).data.name == "Project-101"
		rows.get(0).data.label == "Label-101"
		rows.get(0).data.createdBy == "admin"
		dateFormat.parse((String) rows.get(0).data.createdOn) == dateFormat.parse("2020-04-09")
		rows.get(0).data.lastModifiedBy == "rudy"
		rows.get(0).data.lastModifiedOn == null
		rows.get(0).data.hasPermissions == true
		rows.get(0).data.bugtrackerName == "Server-103"
		rows.get(0).data.executionServer == "TA01"
		rows.get(0).data.isTemplate == false

		rows.get(1).data.projectId == -103L
		rows.get(1).data.name == "Project-102"
		rows.get(1).data.label == "Label-102"
		rows.get(1).data.createdBy == "admin"
		dateFormat.parse((String) rows.get(1).data.createdOn) == dateFormat.parse("2020-04-09")
		rows.get(1).data.lastModifiedBy == "admin"
		rows.get(1).data.lastModifiedOn == null
		rows.get(1).data.hasPermissions == false
		rows.get(1).data.bugtrackerName == null
		rows.get(1).data.executionServer == null
		rows.get(1).data.isTemplate == false

		rows.get(2).data.projectId == -102L
		rows.get(2).data.name == "Project-103"
		rows.get(2).data.label == "Label-103"
		rows.get(2).data.createdBy == "georges"
		dateFormat.parse((String) rows.get(2).data.createdOn) == dateFormat.parse("2020-04-09")
		rows.get(2).data.lastModifiedBy == "georges"
		rows.get(2).data.lastModifiedOn == null
		rows.get(2).data.hasPermissions == false
		rows.get(2).data.bugtrackerName == null
		rows.get(2).data.executionServer == null
		rows.get(2).data.isTemplate == true
	}

	def "should fetch project view dto for a given project id"() {

		when:
		def projectViewDto = projectDisplayService.getProjectView(-101L)

		then:

		projectViewDto.name == "Project-101"
		projectViewDto.label == "Label-101"
		projectViewDto.testCaseNatureId == 2
		projectViewDto.testCaseTypeId == 3
		projectViewDto.requirementCategoryId == 1
		projectViewDto.allowAutomationWorkflow
		projectViewDto.attachmentListId == -101
		!projectViewDto.hasData
		projectViewDto.bugTrackerBinding.bugTrackerId == -103
		projectViewDto.availableBugtrackers.size() == 1
		projectViewDto.availableBugtrackers.get(0).name == "Server-103"
		projectViewDto.taServerId == -102
		projectViewDto.availableTestAutomationServers.size() == 1
		projectViewDto.availableTestAutomationServers.get(0).name == "TA01"
		projectViewDto.boundTestAutomationProjects.size() == 1
		projectViewDto.boundTestAutomationProjects.get(0).remoteName == "taProject1"
		projectViewDto.bugtrackerProjectNames.size() == 2
		projectViewDto.bugtrackerProjectNames.get(0) == "projectName1"
		projectViewDto.bugtrackerProjectNames.get(1) == "projectName2"

		List<MilestoneProjectViewDto> boundMilestonesInformation = projectViewDto.boundMilestonesInformation
		boundMilestonesInformation.size() == 2
		boundMilestonesInformation.sort { m1, m2 -> m1.milestone.id <=> m2.milestone.id }
		boundMilestonesInformation.get(0).milestone.label == "My milestone 8"
		!projectViewDto.allowTcModifDuringExec
	}

	@Unroll
	def "should check if a project has data"() {

		when:
		def hasData = projectDisplayService.hasProjectData(projectId)

		then:
		hasData == result

		where:

		projectId | result
		-101L     | false
		-102L     | true
	}


	def "should fetch project templates references"() {
		when:
		def projectTemplates = projectDisplayService.getTemplateNamedReferences()

		then:
		projectTemplates.size() == 1
		projectTemplates.get(0).name == "Project-103"
	}

	def "should find available milestones to add to a given project"() {
		when:
		def availableMilestones = projectDisplayService.findAvailableMilestones(projectId)

		then:
		availableMilestones.globalMilestones.size() == globalMilestones
		availableMilestones.otherMilestones.size() == otherMilestones

		where:

		// Locked milestones should be ignored
		projectId | globalMilestones | otherMilestones
		-101L     | 2                | 3
		-102L     | 3                | 1
	}

	@Unroll("should allow sorting on #column")
	def "should allow sorting"() {
		when:
		def gridRequest = new GridRequest()
		gridRequest.sort.add(new GridSort(column, GridSort.SortDirection.ASC))
		gridRequest.setSize(25)
		this.projectDisplayService.findAll(gridRequest)

		then:
		noExceptionThrown()

		where:
		column << ["name",
				   "label",
				   "createdBy",
				   "createdOn",
				   "lastModifiedBy",
				   "lastModifiedOn",
				   "hasPermissions",
				   "bugtrackerName",
				   "executionServer",
				   "isTemplate"]
	}

	@Unroll
	def "should filter on multi columns"() {
		when:
		def gridRequest = new GridRequest()

		def name = new GridFilterValue()
		name.id = "name"
		name.operation = "LIKE"
		name.values = [searchingValue]

		def label = new GridFilterValue()
		label.id = "label"
		label.operation = "LIKE"
		label.values = [searchingValue]

		def createdBy = new GridFilterValue()
		createdBy.id = "createdBy"
		createdBy.operation = "LIKE"
		createdBy.values = [searchingValue]

		def lastModifiedBy = new GridFilterValue()
		lastModifiedBy.id = "lastModifiedBy"
		lastModifiedBy.operation = "LIKE"
		lastModifiedBy.values = [searchingValue]

		gridRequest.filterValues = [name, label, createdBy, lastModifiedBy]

		gridRequest.setSearchOnMultiColumns(true)
		gridRequest.setSize(25)
		def gridResponse = this.projectDisplayService.findAll(gridRequest)

		then:

		gridResponse.count == size

		where:

		searchingValue | size
		"Project"      | 3
		"Project-101"  | 1
		"101"          | 1
		"rudy"         | 1
		"georges"      | 1
		"admin"        | 2

	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate

import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.core.foundation.collection.ColumnFiltering
import org.squashtest.tm.core.foundation.collection.PagingAndMultiSorting
import org.squashtest.tm.core.foundation.collection.Sorting
import org.squashtest.tm.domain.EntityReference
import org.squashtest.tm.domain.EntityType
import org.squashtest.tm.domain.execution.ExecutionStatusReport
import org.squashtest.tm.service.internal.dto.AutomatedSuiteDto
import org.squashtest.tm.service.internal.repository.AutomatedSuiteDao
import org.squashtest.tm.service.testautomation.model.AutomatedSuitePreview.SquashAutomProjectPreview
import org.squashtest.tm.service.testautomation.model.AutomatedSuitePreview.TestCaseReferenceAndName
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject

import static org.squashtest.tm.domain.execution.ExecutionStatus.*

@UnitilsSupport
@DataSet("HibernateAutomatedSuiteDaoIT.sandbox.xml")
public class HibernateAutomatedSuiteDaoIT extends DbunitDaoSpecification {

	@Inject
	AutomatedSuiteDao suiteDao;

	def suiteid = "-12345"
	def iterationId = -1L
	def testSuiteId = -11L


	def "should create a new suite"() {

		expect:
		def suite = suiteDao.createNewSuite()
		suite.id != null

	}

	def "should find all the extenders associated to that suite"() {

		when:
		def extenders = suiteDao.findAllExtenders(suiteid)

		then:
		extenders*.id as Set == [-110L, -120L, -130L, -210L, -220L] as Set

	}

	def "should find all the extenders of executions waiting to be run"() {

		expect:
		suiteDao.findAllWaitingExtenders(suiteid)*.id == [-210L]

	}

	def "should find all the extenders of executions currently running"() {

		expect:
		suiteDao.findAllRunningExtenders(suiteid)*.id == [-130L]

	}

	def "should find all completed executions"() {

		expect:
		suiteDao.findAllCompletedExtenders(suiteid)*.id as Set == [-110L, -120L, -220L] as Set

	}

	def "should find all extenders by statys"() {

		expect:
		suiteDao.findAllExtendersByStatus(suiteid, [FAILURE, RUNNING])*.id as Set == [-220L, -130L] as Set

	}

	def "should find all suites linked to an iteration"() {

		given:
		ColumnFiltering filter = ColumnFiltering.UNFILTERED
		PagingAndMultiSorting paging = new TestPagingMultiSorting()

		when:

		List<AutomatedSuiteDto> result = suiteDao.findAutomatedSuitesByIterationID(iterationId, paging, filter)

		then:

		result*.suiteId as Set == ["-12345", "-6789", "-1111", "-2222"] as Set
		result[0].suiteId == "-6789"

	}

	def "should count all suites linked to an iteration"() {

		given:
		ColumnFiltering filter = ColumnFiltering.UNFILTERED

		expect:

		suiteDao.countSuitesByIterationId(iterationId, filter) == 4

	}

	def "should find all suites linked to a test suite"() {

		given:
		ColumnFiltering filter = ColumnFiltering.UNFILTERED
		PagingAndMultiSorting paging = new TestPagingMultiSorting()

		when:

		List<AutomatedSuiteDto> result = suiteDao.findAutomatedSuitesByTestSuiteID(testSuiteId, paging, filter)

		then:

		result*.suiteId as List == ["-12345", "-1111"]
		result[0].suiteId == "-12345"

	}

	def "should count all suites linked to a test suite"() {

		given:
		ColumnFiltering filter = ColumnFiltering.UNFILTERED

		expect:

		suiteDao.countSuitesByTestSuiteId(testSuiteId, filter) == 2

	}

	def "should get status report for an automated suite"() {

		when:

		ExecutionStatusReport statusReport = suiteDao.getStatusReport(suiteid)

		then:

		statusReport.getTotal() == 5
		statusReport.get(SUCCESS) == 1
		statusReport.get(BLOCKED) == 1
		statusReport.get(RUNNING) == 1
		statusReport.get(READY) == 1
		statusReport.get(FAILURE) == 1
		statusReport.anyOf(FAILURE)
		!statusReport.anyOf(SETTLED)
		!statusReport.allOf(SUCCESS)
		statusReport.allOf(SUCCESS, BLOCKED, RUNNING, READY, FAILURE)

	}

	@Unroll
	@DataSet("HibernateAutomatedSuiteDaoIT.find whether itpi selection contains squashAutom tests.xml")
	def "should find whether itpi selection contains squashAutom tests"() {
		given:
			EntityReference contextReference = new EntityReference(contextType, contextId)
		when:
			boolean result = suiteDao.itpiSelectionContainsSquashAutomTest(contextReference, testPlanSubsetIds)
		then:
			result == expectedResult
		where:
			contextType				| contextId | testPlanSubsetIds | expectedResult
			EntityType.ITERATION	| -7 		| []				| true
			EntityType.ITERATION	| -9 		| []				| false
			EntityType.ITERATION	| -7 		| [-2, -3]			| true
			EntityType.ITERATION	| -7 		| [-3, -7]			| false
			EntityType.TEST_SUITE	| -3 		| []				| true
			EntityType.TEST_SUITE	| -11 		| []				| false
			EntityType.TEST_SUITE	| -3 		| [-11, -12]		| true
			EntityType.TEST_SUITE	| -3 		| [-12, -17]		| false
	}

	@DataSet("HibernateAutomatedSuiteDaoIT.find squash autom server previews.xml")
	def "should find all squashAutom server previews for an Iteration when execution popup opens"() {
		given:
			EntityReference contextEntity = new EntityReference(EntityType.ITERATION, -33)
		and:
			def testCases1 = [
				new TestCaseReferenceAndName("TC67", "Account integrity verification"),
				new TestCaseReferenceAndName("TC99", "Connection special cases")
			]
		and:
			def testCases2 = [
				new TestCaseReferenceAndName("TC12", "Graphical requirements"),
				new TestCaseReferenceAndName("TC9", "Nominal performance")
			]
		when:
			Map<Long, SquashAutomProjectPreview> result = suiteDao.findAllSquashAutomProjectPreviews(contextEntity, [])
		then:
			result.size() == 2

			SquashAutomProjectPreview server1 = result.get(-3L)
			server1 != null
			server1.projectId == -3
			server1.projectName == "LEAF Project"
			server1.serverId == -7
			server1.serverName == "Local orchestrator"
			server1.testCases.size() == 2
			server1.testCases.containsAll(testCases1)

			SquashAutomProjectPreview server2 = result.get(-9L)
			server2 != null
			server2.projectId == -9
			server2.projectName == "PID Project"
			server2.serverId == -11
			server2.serverName == "MMB orchestrator"
			server2.testCases.size() == 2
			server2.testCases.containsAll(testCases2)
	}

	@DataSet("HibernateAutomatedSuiteDaoIT.find squash autom server previews.xml")
	def "should find all squashAutom server previews for selected ITPIs when execution popup opens"() {
		given:
			EntityReference contextEntity = new EntityReference(EntityType.ITERATION, -33)
			def testPlanSubsetIds = [-101, -104]
		and:
			def testCases1 =
				[new TestCaseReferenceAndName("TC67", "Account integrity verification")]
		and:
			def testCases2 =
				[new TestCaseReferenceAndName("TC9", "Nominal performance")]
		when:
			Map<Long, SquashAutomProjectPreview> result = suiteDao.findAllSquashAutomProjectPreviews(contextEntity, testPlanSubsetIds)
		then:
			result.size() == 2

			SquashAutomProjectPreview server1 = result.get(-3L)
			server1 != null
			server1.projectId == -3
			server1.projectName == "LEAF Project"
			server1.serverId == -7
			server1.serverName == "Local orchestrator"
			server1.testCases.size() == 1
			server1.testCases.containsAll(testCases1)

			SquashAutomProjectPreview server2 = result.get(-9L)
			server2 != null
			server2.projectId == -9
			server2.projectName == "PID Project"
			server2.serverId == -11
			server2.serverName == "MMB orchestrator"
			server2.testCases.size() == 1
			server2.testCases.containsAll(testCases2)
	}

	@DataSet("HibernateAutomatedSuiteDaoIT.find squash autom server previews.xml")
	def "should find all squashAutom server previews for a TestSuite when execution popup opens"() {
		given:
			EntityReference contextEntity = new EntityReference(EntityType.TEST_SUITE, -8)
		and:
			def testCases1 = [
				new TestCaseReferenceAndName("TC67", "Account integrity verification"),
				new TestCaseReferenceAndName("TC99", "Connection special cases")
			]
		and:
			def testCases2 = [
				new TestCaseReferenceAndName("TC12", "Graphical requirements")
			]
		when:
			Map<Long, SquashAutomProjectPreview> result = suiteDao.findAllSquashAutomProjectPreviews(contextEntity, [])
		then:
			result.size() == 2

			SquashAutomProjectPreview server1 = result.get(-3L)
			server1 != null
			server1.projectId == -3
			server1.projectName == "LEAF Project"
			server1.serverId == -7
			server1.serverName == "Local orchestrator"
			server1.testCases.size() == 2
			server1.testCases.containsAll(testCases1)

			SquashAutomProjectPreview server2 = result.get(-9L)
			server2 != null
			server2.projectId == -9
			server2.projectName == "PID Project"
			server2.serverId == -11
			server2.serverName == "MMB orchestrator"
			server2.testCases.size() == 1
			server2.testCases.containsAll(testCases2)
	}

	@DataSet("HibernateAutomatedSuiteDaoIT.find squash autom server previews.xml")
	def "should find all squashAutom server previews for selected TSTPIs when execution popup opens"() {
		given:
			EntityReference contextEntity = new EntityReference(EntityType.TEST_SUITE, -8)
			def testPlanSubsetIds = [-102, -103]
		and:
			def testCases1 =
				[new TestCaseReferenceAndName("TC99", "Connection special cases")]
		and:
			def testCases2 =
				[new TestCaseReferenceAndName("TC12", "Graphical requirements")]
		when:
			Map<Long, SquashAutomProjectPreview> result = suiteDao.findAllSquashAutomProjectPreviews(contextEntity, testPlanSubsetIds)
		then:
			result.size() == 2

			SquashAutomProjectPreview server1 = result.get(-3L)
			server1 != null
			server1.projectId == -3
			server1.projectName == "LEAF Project"
			server1.serverId == -7
			server1.serverName == "Local orchestrator"
			server1.testCases.size() == 1
			server1.testCases.containsAll(testCases1)

			SquashAutomProjectPreview server2 = result.get(-9L)
			server2 != null
			server2.projectId == -9
			server2.projectName == "PID Project"
			server2.serverId == -11
			server2.serverName == "MMB orchestrator"
			server2.testCases.size() == 1
			server2.testCases.containsAll(testCases2)
	}

}

class TestPagingMultiSorting implements PagingAndMultiSorting {

	@Override
	int getFirstItemIndex() {
		return 0
	}

	@Override
	int getPageSize() {
		return 50
	}

	@Override
	boolean shouldDisplayAll() {
		return false
	}

	@Override
	List<Sorting> getSortings() {
		return Collections.emptyList()
	}

}

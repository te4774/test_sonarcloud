/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.display.search

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.query.Operation
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.service.internal.display.grid.GridFilterValue
import org.squashtest.tm.service.internal.display.grid.GridRequest
import org.squashtest.tm.service.requirement.HighLevelRequirementService
import org.unitils.dbunit.annotation.DataSet
import spock.lang.Unroll
import spock.unitils.UnitilsSupport

import javax.inject.Inject
import javax.persistence.EntityManager

import static org.squashtest.tm.service.internal.display.search.filter.CurrentVersionFilterHandler.CurrentVersionFilterValue.*
import static org.squashtest.tm.service.internal.display.search.filter.RequirementVersionHasChildrenFilterHandler.HasChildrenFilterValue.HAS_CHILDREN
import static org.squashtest.tm.service.internal.display.search.filter.RequirementVersionHasChildrenFilterHandler.HasChildrenFilterValue.NO_CHILDREN
import static org.squashtest.tm.service.internal.display.search.filter.RequirementVersionHasDescriptionFilterHandler.HasDescriptionFilterValue.HAS_DESCRIPTION
import static org.squashtest.tm.service.internal.display.search.filter.RequirementVersionHasDescriptionFilterHandler.HasDescriptionFilterValue.NO_DESCRIPTION
import static org.squashtest.tm.service.internal.display.search.filter.RequirementVersionHasParentFilterHandler.HasParentFilterValue.HAS_PARENT
import static org.squashtest.tm.service.internal.display.search.filter.RequirementVersionHasParentFilterHandler.HasParentFilterValue.NO_PARENT

@UnitilsSupport
@Transactional
@DataSet
class RequirementSearchServiceIT extends DbunitServiceSpecification {

	static boolean isH2() {
		return System.properties['jooq.sql.dialect'] == null || System.properties['jooq.sql.dialect'] == 'H2'
	}

	def setBidirectionalReqReqVersion(Long reqId, Long reqVersionId) {
		def reqVer = entityManager.find(RequirementVersion.class, reqVersionId)
		def req = entityManager.find(Requirement.class, reqId)
		reqVer.setRequirement(req)
		// Flush is necessary as the dao use native query through jooq.
		entityManager.flush()
	}

	def setup() {
		def ids = [
			[-2L, -2L],
			[-3L, -3L],
			[-3L, -33L],
			[-4L, -4L],
			[-5L, -5L],
			[-5L, -15L],
			[-7L, -7L],
		]
		ids.each {
			setBidirectionalReqReqVersion(it[0], it[1])
		}
	}

	@Inject
	private RequirementSearchService service

	@Inject
	EntityManager entityManager

	@Unroll
	def "should find requirement versions by project perimeter"() {
		given:
		def request = createBaseRequest()
		request.scope = scope

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		scope                        || expectedIds                    | expectedCount
		["Project--1"]               || [-33, -15, -5, -4, -3, -2]     | 6
		["Project--2"]               || [-7]                           | 1
		["Project--1", "Project--2"] || [-33, -15, -7, -5, -4, -3, -2] | 7
		[]                           || [-33, -15, -7, -5, -4, -3, -2] | 7
	}

	def "should throw if perimeter is incorrect"() {
		given:
		def request = createBaseRequest()
		request.scope = ["Project--3"]

		when:
		service.search(request)

		then:
		thrown IllegalArgumentException
	}

	@Unroll
	def "should find requirement versions with current version filter"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "REQUIREMENT_VERSION_CURRENT_VERSION"
		filterValue.values = [currentVersion]
		filterValue.operation = Operation.EQUALS
		def request = createBaseRequest()
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		currentVersion           || expectedIds                    | expectedCount
		ALL.name()               || [-33, -15, -7, -5, -4, -3, -2] | 7
		CURRENT.name()           || [-7, -5, -4, -3, -2]           | 5
		LAST_NON_OBSOLETE.name() || [-33, -7, -5, -4, -2]          | 5
	}

	@Unroll
	def "should find requirement versions with hasDescription filter"() {
		given:
		def noDescriptionFilter = new GridFilterValue()
		noDescriptionFilter.columnPrototype = "REQUIREMENT_VERSION_HAS_DESCRIPTION"
		noDescriptionFilter.values = [hasDecription]
		noDescriptionFilter.operation = Operation.IN
		def request = createBaseRequest()
		request.filterValues = [noDescriptionFilter]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		hasDecription          || expectedIds                | expectedCount
		NO_DESCRIPTION.name()  || [-33, -15, -7, -5, -4, -3] | 6
		HAS_DESCRIPTION.name() || [-2]                       | 1
	}


	@Unroll
	def "should find requirement versions with hasChildren filter"() {
		given:
		def hasChildrenFilter = new GridFilterValue()
		hasChildrenFilter.columnPrototype = "REQUIREMENT_VERSION_HAS_CHILDREN"
		hasChildrenFilter.values = [hasChildren]
		hasChildrenFilter.operation = Operation.IN
		def request = createBaseRequest()
		request.filterValues = [hasChildrenFilter]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		hasChildren         || expectedIds                | expectedCount
		NO_CHILDREN.name()  || [-33, -15, -7, -5, -3, -2] | 6
		HAS_CHILDREN.name() || [-4]                       | 1
	}

	@Unroll
	def "should find requirement versions with hasParent filter"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "REQUIREMENT_VERSION_HAS_PARENT"
		filterValue.values = [hasParent]
		filterValue.operation = Operation.IN
		def request = createBaseRequest()
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		hasParent         || expectedIds           | expectedCount
		NO_PARENT.name()  || [-33, -7, -4, -3, -2] | 5
		HAS_PARENT.name() || [-15, -5]             | 2
	}

	@Unroll
	def "should find requirement versions with link type filter"() {
		given:
		def filterValue = new GridFilterValue()
		filterValue.columnPrototype = "REQUIREMENT_VERSION_HAS_LINK_TYPE"
		filterValue.values = [roleCode]
		filterValue.operation = operation
		def request = createBaseRequest()
		request.filterValues = [filterValue]

		when:
		def researchResult = service.search(request)

		then:
		researchResult.ids == expectedIds
		researchResult.count == expectedCount

		where:
		operation      | roleCode || expectedIds                | expectedCount
		"NONE"         | "code2"  || [-33, -15, -7, -5, -4, -3] | 6
		"AT_LEAST_ONE" | "code2"  || [-2]                       | 1
		"NONE"         | "code1"  || [-33, -15, -7, -5, -3, -2] | 6
		"AT_LEAST_ONE" | "code1"  || [-4]                       | 1
		"NONE"         | "code3"  || [-33, -15, -7, -4, -2]     | 5
		"AT_LEAST_ONE" | "code3"  || [-5, -3]                   | 2
	}

	def createBaseRequest() {
		def request = new GridRequest()
		request.page = 0
		request.size = 25
		request
	}

}

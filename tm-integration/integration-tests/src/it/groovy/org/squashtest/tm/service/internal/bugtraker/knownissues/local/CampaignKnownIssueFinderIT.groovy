/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.bugtracker.knownissues.local

import org.springframework.data.domain.Pageable
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.core.foundation.collection.PagingAndSorting
import org.squashtest.tm.core.foundation.collection.SortOrder
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@DataSet("TestCaseKnownIssueFinderIT.xml")
class CampaignKnownIssueFinderIT extends DbunitServiceSpecification {

	@Inject CampaignKnownIssueFinder campaignKnownIssueFinder

	def "should find known issues for campaign" () {
		when:
		def issues = campaignKnownIssueFinder.getPaginatedKnownIssues(campaignId, mockPaging(0, 10))

		then:
		issues.every({ issue -> expectedIssueIds.contains(issue.remoteIssueId) })
		expectedIssueIds.every({ expected -> issues.find({ issue -> issue.remoteIssueId == expected })})
		issues.size() == expectedIssueIds.size()

		where:
		campaignId || expectedIssueIds
		-1L        || ["1", "2", "3", "4", "5", "7"]
		-2L		   || []
	}

	def "should get correct count for campaign known issues" () {
		when:
		def count = campaignKnownIssueFinder.countKnownIssues(campaignId)

		then:
		count == expectedCount

		where:
		campaignId || expectedCount
		-1L        || 6
		-2L        || 0
	}

	def mockPaging(firstItemIndex, pageSize) {
		return new PagingAndSorting() {
			@Override
			int getFirstItemIndex() { return firstItemIndex }

			@Override
			int getPageSize() { return pageSize }

			@Override
			boolean shouldDisplayAll() { return false }

			@Override
			Pageable toPageable() { return null }

			@Override
			String getSortedAttribute() { return null }

			@Override
			SortOrder getSortOrder() { return SortOrder.DESCENDING }
		}
	}
}

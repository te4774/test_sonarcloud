/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.internal.repository.hibernate

import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitDaoSpecification
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.scm.ScmRepository
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.service.internal.repository.ScmRepositoryDao
import org.squashtest.tm.service.internal.display.dto.ScmRepositoryDto
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@UnitilsSupport
@Transactional
class CustomScmRepositoryDaoIT extends DbunitDaoSpecification {

	@Inject
	ScmRepositoryDao scmRepositoryDao

	@DataSet("CustomScmRepositoryDaoIT.xml")
	def "should find all scripted and keyword test cases by id then group them by their repository"(){
		given:
		def testcaseIds = [-1L,-2L,-3L,-4L,-5L,-6L,-7L,-8L,-9L,-10L,-11L,-12L,-13L,-14L,-15L]
		when:
		def res = scmRepositoryDao.findScriptedAndKeywordTestCasesGroupedByRepoById(testcaseIds)
		then:
		res != null
		res.size() == 2
		def entrySet = res.entrySet()
		def entry1 = entrySet.find{it.getKey().getId() == -1L}
		def entry2 = entrySet.find{it.getKey().getId() == -2L}
		and:
			entry1 != null
			def repo1 = entry1.getKey()
			repo1 != null
			repo1.getId() == -1L
			repo1.getName() == "First Repository"
			def repo1TestCases = entry1.getValue()
			repo1TestCases != null
			repo1TestCases.size() == 5
			def repo1TcIds = repo1TestCases.collect{it.id}
			repo1TcIds.containsAll([-3L, -4L, -5L, -10L, -11L])
		and:
			entry2 != null
			def repo2 = entry2.getKey()
			repo2 != null
			repo2.getId() == -2L
			repo2.getName() == "Second Repository"
			def repo2TestCases = entry2.getValue()
			repo2TestCases != null
			repo2TestCases.size() == 3
			def repo2TcIds = repo2TestCases.collect{it.id}
			repo2TcIds.containsAll([-12L, -13L, -14L])

	}

	@DataSet
	def "should find if one of the scm repositories is already bound to a project or a test case"() {
		expect:
			scmRepositoryDao.isOneRepositoryBoundToProjectOrTestCase(scmRepoIds) == expectedResult
		where:
			scmRepoIds 		| expectedResult
			[-1L, -2L, -3L] | true
			[-2L] 			| true
			[-3L] 			| true
			[-4L] 			| false
	}

	@DataSet
	def "should get all cloned scm repositories in the given scm server ordered by name"() {
		when:
			List<ScmRepository> result = scmRepositoryDao.findClonedByScmServerOrderByName(-1L)
		then:
			result != null
			result.size() == 3
			def repo1 = result[0]
			repo1.id == -3L && repo1.name == "A Third Repository" && repo1.workingBranch == "main"
			def repo2 = result[1]
			repo2.id == -1L && repo2.name == "First Repository" && repo2.workingBranch == "master"
			def repo3 = result[2]
			repo3.id == -2L && repo3.name == "Second Repository" && repo3.workingBranch == "master"
	}

	@DataSet
	def "should get all declared scm repositories as a list of ScmRepositoryDto"() {
		when:
			List<ScmRepositoryDto> result = scmRepositoryDao.getAllDeclaredScmRepositories()
		then:
			result != null
			result.size() == 5
			result.any {it.id == -1L && it.friendlyUrl == "toto/First Repository (master)" }
			result.any {it.id == -2L && it.friendlyUrl == "toto/Second Repository (master)" }
			result.any {it.id == -3L && it.friendlyUrl == "toto/A Third Repository (main)" }
			result.any {it.id == -4L && it.friendlyUrl == "toto/A Fourth Repository (main)" }
			result.any {it.id == -5L && it.friendlyUrl == "server/another repository (develop)" }
	}

	@DataSet
	def "should unbind scm repositories from their projects"() {
		given:
			def projectIds = [-1L, -2L, -3L, -4L]
			def scmRepoIds = [-1L, -2L, -3L, -4L]
		when:
			scmRepositoryDao.releaseScmRepositoriesFromProjects(scmRepoIds)
		then:
			projectIds.each {
				assert findEntity(Project.class, it).scmRepository == null
			}
	}

	@DataSet
	def "should unbind scm repositories from their test cases"() {
		given:
			def testCaseIds = [-16L, -17L]
			def scmRepoIds = [-2L, -3L]
		when:
			scmRepositoryDao.releaseScmRepositoriesFromTestCases(scmRepoIds)
		then:
			testCaseIds.each {
				assert findEntity(TestCase.class, it).scmRepository == null
			}
	}

	@DataSet
	def "should check if name and branch are already used by another repository in the scm server"() {
		expect:
			scmRepositoryDao.isRepositoryNameAndBranchAlreadyInUse(serverId, repoName, repoBranch) == expectedResult
		where:
			serverId	|	repoName					|	repoBranch	|	expectedResult
			-1			|	"First Repository"			|	"master"	|	true
			-1			|	"First Repository"			|	"main"		|	false
			-1			|	"another repository"		|	"develop"	|	false
			-2			|	"another repository"		|	"develop"	|	true
			-2			|	"non-existent repository"	|	"main"		|	false
			-2			|	"another repository"		|	"fix-04"	|	false

	}

}

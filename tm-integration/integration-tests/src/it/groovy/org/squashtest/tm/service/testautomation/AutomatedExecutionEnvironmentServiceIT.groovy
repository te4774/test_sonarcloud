/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.service.testautomation

import org.spockframework.util.NotThreadSafe
import org.springframework.transaction.annotation.Transactional
import org.squashtest.it.basespecs.DbunitServiceSpecification
import org.squashtest.tm.domain.servers.TokenAuthCredentials
import org.squashtest.tm.service.testautomation.environment.AutomatedExecutionEnvironmentService
import org.unitils.dbunit.annotation.DataSet
import spock.unitils.UnitilsSupport

import javax.inject.Inject

@NotThreadSafe
@UnitilsSupport
@Transactional
@DataSet("AutomatedExecutionEnvironmentServiceIT.xml")
/**
 * The test data is partly defined in TestAutomationServerTestConfig (that's where the mocks for automated
 * execution servers are defined)
 */
class AutomatedExecutionEnvironmentServiceIT extends DbunitServiceSpecification {

	@Inject
	AutomatedExecutionEnvironmentService service

	def "should determine if a server supports execution environments"() {
		expect:
		service.doesServerSupportAutomatedExecutionEnvironments(-1L)
		! service.doesServerSupportAutomatedExecutionEnvironments(-2L)
	}

	def "should fetch available execution environments"() {
		when:
		def environments = service.getAllAccessibleEnvironments(-1L)

		then:
		environments.size() == 2
		environments.collect {env -> env.name}.containsAll(["env1", "env2"])
	}

	def "should fetch available execution environments with credentials"() {
		given:
		def credentials = new TokenAuthCredentials()

		when:
		def environments = service.getAllAccessibleEnvironments(-1L, credentials)

		then:
		environments.size() == 2
	}

	def "should throw if fetching execution environments when non supported"() {
		when:
		service.getAllAccessibleEnvironments(-2L)

		then:
		thrown(UnsupportedOperationException)
	}

}

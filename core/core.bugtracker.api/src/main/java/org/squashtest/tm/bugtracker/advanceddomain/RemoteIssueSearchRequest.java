/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.advanceddomain;

import java.util.List;
import java.util.Optional;

/**
 * Holds a list of search terms used by connectors when looking for a remote issue.
 */
public class RemoteIssueSearchRequest {
	private final List<RemoteIssueSearchTerm> searchTerms;

	public RemoteIssueSearchRequest(List<RemoteIssueSearchTerm> searchTerms) {
		this.searchTerms = searchTerms;
	}

	public List<RemoteIssueSearchTerm> getSearchTerms() {
		return searchTerms;
	}

	public boolean hasSearchTerm(String searchTermId) {
		return searchTerms.stream().anyMatch(term -> term.getId().equals(searchTermId));
	}

	public String getSearchTermStringValue(String searchTermId) {
		Optional<RemoteIssueSearchTerm> optionalSearchTerm = searchTerms.stream()
			.filter(term -> term.getId().equals(searchTermId))
			.findFirst();

		if (optionalSearchTerm.isPresent()) {
			return optionalSearchTerm.get().getStringValue();
		}

		throw new IllegalArgumentException("No search term could be found with id " + searchTermId);
	}
}

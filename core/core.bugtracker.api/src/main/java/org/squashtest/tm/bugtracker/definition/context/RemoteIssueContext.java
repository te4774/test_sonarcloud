/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.definition.context;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Describes the context in which a remote issue was reported. It holds data about the execution and its steps such as
 * ids and descriptions.
 * This info is notably used to generate default descriptions in issue report forms.
 */
public class RemoteIssueContext {
	private final List<ExecutionStepInfo> executionSteps;
	private final Long buggedStepId;
	private final TestCaseInfo testCase;
	private final ExecutionInfo execution;

	// Description in plain text provided by TM core. Connector plugins may use it or build their own.
	private final String defaultDescription;

	public RemoteIssueContext(List<ExecutionStepInfo> executionSteps,
							  Long buggedStepId,
							  TestCaseInfo testCase,
							  ExecutionInfo execution,
							  String defaultDescription) {
		this.executionSteps = Collections.unmodifiableList(executionSteps);
		this.buggedStepId = buggedStepId;
		this.testCase = testCase;
		this.execution = execution;
		this.defaultDescription = defaultDescription;
	}

	public List<ExecutionStepInfo> getExecutionSteps() {
		return executionSteps;
	}

	/**
	 * @return The step this issue was reported from. May be null if the issue was reported for an execution
	 */
	public Long getBuggedStepId() {
		return buggedStepId;
	}

	public TestCaseInfo getTestCase() {
		return testCase;
	}

	public ExecutionInfo getExecution() {
		return execution;
	}

	public boolean issueWasReportedInStep() {
		return buggedStepId != null;
	}

	public Optional<ExecutionStepInfo> findBuggedStep() {
		return getExecutionSteps().stream()
			.filter(step -> step.getId().equals(getBuggedStepId()))
			.findFirst();
	}

	public String getDefaultDescription() {
		return defaultDescription;
	}

	public RemoteIssueContext withDefaultDescription(String defaultDescription) {
		return new RemoteIssueContext(executionSteps, buggedStepId, testCase, execution, defaultDescription);
	}

	/* Public no-arg constructor for serialization only! */
	public RemoteIssueContext() {
		executionSteps = null;
		buggedStepId = null;
		testCase = null;
		execution = null;
		defaultDescription = null;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.definition.context.formatter;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

public abstract class BaseRemoteIssueContextFormatter {

	private final MessageSource messageSource;

	protected BaseRemoteIssueContextFormatter(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	protected String getLocaleTestCaseContext() {
		return messageSource.getMessage("label.Background", null, getLocale());
	}

	protected String getLocalePrerequisite() {
		return messageSource.getMessage("test-case.prerequisite.label", null, getLocale());
	}

	protected String getLocaleTestCaseLabel() {
		return messageSource.getMessage("issue.default.description.testCase", null, getLocale());
	}

	protected String getLocaleExecutionLabel() {
		return messageSource.getMessage("issue.default.description.execution", null, getLocale());
	}

	protected String getLocaleConcernedStepLabel() {
		return messageSource.getMessage("issue.default.description.concernedStep", null, getLocale());
	}

	protected String getLocaleDescriptionTitleLabel() {
		return messageSource.getMessage("issue.default.description.description", null, getLocale());
	}

	protected String getLocaleTestStepsTitleLabel() {
		return messageSource.getMessage("label.testSteps", null, getLocale());
	}

	protected String getLocaleActionLabel() {
		return messageSource.getMessage("issue.default.additionalInformation.action", null, getLocale());
	}

	protected String getLocaleExpectedResultLabel() {
		return messageSource.getMessage("issue.default.additionalInformation.expectedResult", null, getLocale());
	}

	protected String getLocaleScriptLabel() {
		return messageSource.getMessage("issue.default.additionalInformation.script", null, getLocale());
	}

	protected String getLocaleStepLabel() {
		return messageSource.getMessage("issue.default.additionalInformation.step", null, getLocale());
	}

	protected Locale getLocale() {
		return LocaleContextHolder.getLocale();
	}

}

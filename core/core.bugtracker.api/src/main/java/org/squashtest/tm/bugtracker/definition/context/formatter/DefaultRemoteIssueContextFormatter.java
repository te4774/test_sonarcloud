/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.definition.context.formatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import org.squashtest.tm.bugtracker.definition.context.ExecutionStepInfo;
import org.squashtest.tm.bugtracker.definition.context.KeywordExecutionStepInfo;
import org.squashtest.tm.bugtracker.definition.context.RemoteIssueContext;
import org.squashtest.tm.bugtracker.definition.context.ScriptedExecutionStepInfo;
import org.squashtest.tm.bugtracker.definition.context.StandardExecutionStepInfo;
import org.squashtest.tm.bugtracker.definition.context.TestCaseInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Generates the default description for the remote issue report form, from a RemoteIssueContext.
 * The generated text is formatted with indentation ("tab" characters) but other formats can be achieved by sub-classing.
 * That's what does GitLab and Jira bugtracker plugins so beware not to break these when modifying this class.
 */
public class DefaultRemoteIssueContextFormatter extends BaseRemoteIssueContextFormatter {

	public DefaultRemoteIssueContextFormatter(MessageSource messageSource) {
		super(messageSource);
	}

	public static String getDefaultDescription(final RemoteIssueContext context, MessageSource messageSource) {
		return new DefaultRemoteIssueContextFormatter(messageSource).buildDescription(context);
	}

	private String buildDescription(final RemoteIssueContext context) {
		final IndentedTextBuilder builder = new IndentedTextBuilder()
			.append(buildTestCaseTitle(context))
			.append(buildExecutionInfo(context))
			.append(buildIssuedStepNumber(context))
			.appendEmptyLine()
			.append(getLocaleDescriptionTitleLabel())
			.appendEmptyLine();

		if (context.issueWasReportedInStep()) {
			appendAdditionalInformation(context, builder);
		}

		return builder.build();
	}

	protected String buildTestCaseTitle(final RemoteIssueContext context) {
		final String reference = context.getTestCase().getReference();
		final String name = context.getTestCase().getName();

		if (reference == null || StringUtils.isEmpty(reference)) {
			return String.format("%s : %s", getLocaleTestCaseLabel(), name);
		} else {
			return String.format("%s : [%s] %s", getLocaleTestCaseLabel(), reference, name);
		}
	}

	protected String buildExecutionInfo(final RemoteIssueContext context) {
		final String url = context.getExecution().getUrl();
		return String.format(getLocaleExecutionLabel() + " : %s", url);
	}


	protected String buildIssuedStepNumber(final RemoteIssueContext context) {
		final Optional<ExecutionStepInfo> buggedStep = context.getExecutionSteps().stream()
			.filter(step -> step.getId().equals(context.getBuggedStepId()))
			.findFirst();

		if (buggedStep.isPresent()) {
			Integer stepNumber = context.getExecutionSteps().indexOf(buggedStep.get()) + 1;
			Integer stepCount = context.getExecutionSteps().size();
			return String.format(getLocaleConcernedStepLabel() + " : %d/%d", stepNumber, stepCount);
		} else {
			return "";
		}
	}

	protected void appendAdditionalInformation(final RemoteIssueContext context, final IndentedTextBuilder builder) {
		switch(context.getTestCase().getKind()) {
			case STANDARD:
				appendStandardAdditionalInformation(context, builder);
				break;
			case GHERKIN:
				appendScriptedAdditionalInformation(context, builder);
				break;
			case KEYWORD:
				appendKeywordAdditionalInformation(context, builder);
				break;
			default:
				throw new IllegalArgumentException("Unhandled test case kind " + context.getTestCase().getKind());
		}
	}

	protected void appendStandardAdditionalInformation(final RemoteIssueContext context, final IndentedTextBuilder builder) {
		builder
			.append("- " + getLocalePrerequisite())
			.append(new Block()
				.append(context.getExecution().getPrerequisite()))
			.appendEmptyLine();

		for (ExecutionStepInfo step : context.getExecutionSteps()) {
			final StandardExecutionStepInfo standardStep = (StandardExecutionStepInfo)step;

			builder
				.append("- " + buildCurrentStepNumber(step, context))
				.append(new Block()
					.append("- " + getLocaleActionLabel())
					.append(new Block().append(standardStep.getAction()))
					.appendEmptyLine()
					.append("- " + getLocaleExpectedResultLabel())
					.append(new Block().append(standardStep.getExpectedResult()))
					.appendEmptyLine()
				);

			if (step.getId().equals(context.getBuggedStepId())) {
				break;
			}
		}
	}

	protected void appendScriptedAdditionalInformation(final RemoteIssueContext context, final IndentedTextBuilder builder) {
		final String prerequisite = context.getExecution().getPrerequisite();

		if (StringUtils.isNotEmpty(prerequisite)) {
			builder
				.append("- " + getLocaleTestCaseContext())
				.append(new Block().append(context.getExecution().getPrerequisite()))
				.appendEmptyLine();
		}

		final Optional<ExecutionStepInfo> optionalStep = context.findBuggedStep();

		if (optionalStep.isPresent() && optionalStep.get().getTestCaseKind().equals(TestCaseInfo.Kind.GHERKIN)) {
			final ScriptedExecutionStepInfo step = (ScriptedExecutionStepInfo)optionalStep.get();

			builder
				.append("- " + getLocaleScriptLabel())
				.append(new Block()
					.append(step.getScript()))
				.appendEmptyLine();
		}
	}

	protected void appendKeywordAdditionalInformation(final RemoteIssueContext context, final IndentedTextBuilder builder) {
		final String allSteps = context.getExecutionSteps().stream()
			.map(step -> ((KeywordExecutionStepInfo)step).getAction())
			.collect(Collectors.joining(IndentedTextBuilder.NEW_LINE));

		final Optional<ExecutionStepInfo> optionalStep = context.findBuggedStep();

		optionalStep.ifPresent(executionStepInfo -> builder
			.append("- " + buildCurrentStepNumber(executionStepInfo, context))
			.append(new Block()
				.append("- " + getLocaleTestStepsTitleLabel())
				.append(new Block().append(allSteps)))
			.appendEmptyLine()
			.build());
	}

	protected String buildCurrentStepNumber(ExecutionStepInfo step, RemoteIssueContext context) {
		final int stepNumber = step.getExecutionStepOrder() + 1;
		final int stepCount = context.getExecutionSteps().size();
		return String.format(getLocaleStepLabel() + " %d/%d", stepNumber, stepCount);
	}
}

class IndentedTextBuilder {
	public static final String NEW_LINE = "\n";
	public static final String INDENT = "\t";
	private final Block rootBlock;

	IndentedTextBuilder() {
		this.rootBlock = new Block();
	}

	IndentedTextBuilder append(Element element) {
		this.rootBlock.append(element);
		return this;
	}

	IndentedTextBuilder append(String content) {
		this.rootBlock.append(content);
		return this;
	}

	IndentedTextBuilder appendEmptyLine() {
		return this.append("");
	}

	String build() {
		return rootBlock.build(INDENT, -1);
	}
}

abstract class Element {
	abstract String build(String indent, int level);
}

class Block extends Element {

	private final List<Element> elements = new ArrayList<>();

	String build(String indent, int level) {
		return elements.stream()
			.map(el -> el.build(indent, level + 1))
			.collect(Collectors.joining(IndentedTextBuilder.NEW_LINE));
	}

	Block append(Element element) {
		this.elements.add(element);
		return this;
	}

	Block append(String content) {
		this.elements.add(new Terminal(content));
		return this;
	}

	Block appendEmptyLine() {
		return this.append("");
	}
}

class Terminal extends Element {
	private final String content;

	Terminal(String content) {
		this.content = content;
	}

	String build(String indent, int level) {
		final String[] lines = content.split(IndentedTextBuilder.NEW_LINE);

		return Arrays.stream(lines)
			.map(line -> StringUtils.repeat(indent, level) + line)
			.collect(Collectors.joining(IndentedTextBuilder.NEW_LINE));
	}
}

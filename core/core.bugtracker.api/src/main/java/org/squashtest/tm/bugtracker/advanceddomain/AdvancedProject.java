/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.advanceddomain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.squashtest.tm.bugtracker.definition.RemoteProject;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * Squash representation of a remote project in advanced BugTrackers (such as Jira, GitLab, Azure DevOps...).
 * This class must be fully serializable.
 *
 * About schemes : a field scheme is a collection of fields with an ID. They are used for switchable field layouts (e.g.
 * changing a ticket type in Jira). You should both add a scheme in the schemes map and add a field in each scheme that
 * will act as a scheme selector (by raising the flag Field.rendering.inputType.fieldSchemeSelector). Typically, a scheme
 * selector is a dropdown list whose option IDs match the scheme IDs.
 */
public class AdvancedProject implements RemoteProject {

	private String id;

	private String name;

	@JsonProperty("schemes")
	private Map<String, List<Field>> schemeMap = new HashMap<>();

	public AdvancedProject() {
	}

	/**
	 * Static factory method to create an Advanced Project.
	 * Newer code should use this method instead of the deprecated constructor.
	 *
	 * @param id remote project id
	 * @param name remote project name
	 * @param schemes a map of field collections for each possible form layout
	 * @return a new AdvancedProject
	 */
	public static AdvancedProject create(String id, String name, Map<String, List<Field>> schemes) {
		final AdvancedProject advancedProject = new AdvancedProject();
		advancedProject.id = requireNonNull(id, "id cannot be null");
		advancedProject.name = requireNonNull(name, "name cannot be null");
		advancedProject.schemeMap = requireNonNull(schemes, "schemes cannot be null");
		return advancedProject;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	public Map<String, List<Field>> getSchemeMap() {
		return schemeMap;
	}

	public void setSchemeMap(Map<String, List<Field>> schemes) {
		this.schemeMap = schemes;
	}

	public Collection<Field> getFieldScheme(String schemeName) {
		return schemeMap.get(schemeName);
	}

	/* Deprecated methods */

	/**
	 * Deprecated : this is a legacy constructor.
	 * Newer code should use AdvancedProject.create(String, String, Map<String, List<Field>> instead.
	 */
	@Deprecated
	public AdvancedProject(String id, String name, MultiValueMap schemes) {
		this.id = requireNonNull(id, "id cannot be null");
		this.name = requireNonNull(name, "name cannot be null");
		requireNonNull(schemes, "schemes cannot be null");
		setSchemes(schemes);
	}

	/**
	 * Deprecated : as of TM 3.0.0, the internal type was changed from a MultiValueMap to a standard Map with appropriate
	 * generic type args. This makes serialization/deserialization more robust. In order to keep compatibility with existing
	 * plugins, we keep the original getter/setter signatures. Newer code should use getSchemeMap().
 	 */
	@Deprecated
	@JsonIgnore
	public MultiMap getSchemes() {
		MultiValueMap multiValueMap = new MultiValueMap();
		schemeMap.forEach(multiValueMap::putAll);
		return multiValueMap;
	}

	/**
	 * think of it as a Map<String, Collection<Field>>
	 *
	 * Deprecated : as of 3.0.0, you should use setSchemeMap(Map<String, List<Field) instead.
	 */
	@Deprecated
	@JsonIgnore
	public void setSchemes(MultiValueMap multiValueMap) {
		this.schemeMap = new HashMap<>();

		((Map<?, ?>)multiValueMap).keySet().forEach(key -> {
			if (key instanceof String && multiValueMap.get(key) instanceof List) {
				final List<Field> list = (List<Field>) multiValueMap.get(key);
				this.schemeMap.put((String) key, list);
			}
		});
	}
}

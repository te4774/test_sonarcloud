/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.bugtracker.definition.context;

public class ExecutionInfo {
	private final Long id;
	private final String url;
	private final String prerequisite;
	private final String htmlPrerequisite;

	public ExecutionInfo(Long id, String url, String prerequisite, String htmlPrerequisite) {
		this.id = id;
		this.url = url;
		this.prerequisite = prerequisite;
		this.htmlPrerequisite = htmlPrerequisite;
	}

	public Long getId() {
		return id;
	}

	public String getUrl() {
		return url;
	}

	public String getPrerequisite() {
		return prerequisite;
	}

	public String getHtmlPrerequisite() {
		return htmlPrerequisite;
	}

	/* No args constructor for serialization only ! */
	public ExecutionInfo() {
		id = null;
		url = null;
		prerequisite = null;
		htmlPrerequisite = null;
	}
}

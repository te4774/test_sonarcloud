/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.csp.core.bugtracker.core.markdown;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Base class for markdown-formatted text builders.
 *
 * Note : because nearly all methods return a reference to *this* object (fluent builder) and we still want to be able
 * to retrieve the child class concrete type, we have this atypical generic pattern where `This` is the subclass itself.
 *
 * @see BasicMarkdownBuilder for a concrete sub-class
 * @see ExtendedMarkdownBuilder for extended features
 * @param <This> the concrete type of the builder
 */
public abstract class AbstractMarkdownBuilder<This extends AbstractMarkdownBuilder<This>> {
    protected final List<Instruction<This>> instructions = new ArrayList<>();

    protected abstract This getThis();
    protected abstract This createInnerBuilder();

    public String getLineSeparator() { return "\n"; }

    public This plainText(String content) {
        return createInnerBuilder().appendPlainText(content);
    }

    public String link(String text, String url) {
        return String.format("[%s](%s)", text, url);
    }

    public String image(String text, String url) {
        return String.format("!%s", link(text, url));
    }

    public This appendPlainText(String content) {
        instructions.add(createPlainTextInstruction(content));
        return getThis();
    }

    public This appendParagraph(String content) {
        instructions.add(createParagraphInstruction(content));
        return getThis();
    }

    public This appendHeader1(String content) {
        instructions.add(createHeader1Instruction(plainText(content)));
        return getThis();
    }

    public This appendHeader2(String content) {
        instructions.add(createHeader2Instruction(plainText(content)));
        return getThis();
    }

    public This appendHeader3(String content) {
        instructions.add(createHeader3Instruction(plainText(content)));
        return getThis();
    }

    List<String> buildLines() {
        return instructions.stream()
                .flatMap(el -> el.buildLines().stream())
                .collect(Collectors.toList());
    }

    public String build() {
    	final List<String> lines = buildLines();
        return String.join(getLineSeparator(), removeRedundantEmptyLines(lines));
    }

	private List<String> removeRedundantEmptyLines(List<String> lines) {
		final List<String> output = new ArrayList<>();

		boolean previousLineWasEmpty = true;

		for (final String line : lines) {
			if (line.isEmpty()) {
				if (!previousLineWasEmpty) {
					output.add(line);
				}
			} else {
				output.add(line);
			}

			previousLineWasEmpty = line.isEmpty();
		}

		// Remove last line if empty
		if (lines.get(lines.size() - 1).isEmpty()) {
			lines.remove(lines.size() - 1);
		}

		return output;
	}

    protected Instruction<This> createPlainTextInstruction(String content) {
        return new PlainTextInstruction<>(content);
    }

    protected Instruction<This> createParagraphInstruction(String content) {
        return new ParagraphInstruction<>(getThis(), content);
    }

    protected Instruction<This> createHeader1Instruction(This innerBuilder) {
        return new Header1Instruction<>(innerBuilder);
    }

    protected Instruction<This> createHeader2Instruction(This innerBuilder) {
        return new Header2Instruction<>(innerBuilder);
    }

    protected Instruction<This> createHeader3Instruction(This innerBuilder) {
        return new Header3Instruction<>(innerBuilder);
    }

	/**
	 * An instruction apply a text transformation on its content. The content can be plain text (a String) or a inner
	 * builder when embedding makes sense.
	 *
	 * Instructions are kept package-private for the sake of simplicity but you can make them public if you need to
	 * subclass them.
	 *
	 * @param <T> type of the concrete builder
	 */
	abstract static class Instruction<T extends AbstractMarkdownBuilder<T>> {
		abstract List<String> buildLines();
	}

	static class PlainTextInstruction<T extends AbstractMarkdownBuilder<T>> extends Instruction<T> {
		private final String content;

		PlainTextInstruction(String content) {
			this.content = content;
		}

		@Override
		List<String> buildLines() {
			return Collections.singletonList(content);
		}
	}

	static class ParagraphInstruction<T extends AbstractMarkdownBuilder<T>> extends Instruction<T> {
		private final T parentBuilder;
		private final String content;

		ParagraphInstruction(T parentBuilder, String content) {
			this.parentBuilder = parentBuilder;
			this.content = content;
		}

		@Override
		List<String> buildLines() {
			return Collections.singletonList(content + parentBuilder.getLineSeparator());
		}
	}

	abstract static class BaseHeaderInstruction<T extends AbstractMarkdownBuilder<T>> extends Instruction<T> {
		private static final String PREFIX_CHARACTER = "#";

		protected final int headerRank;
		private final T innerBuilder;

		protected BaseHeaderInstruction(int headerRank, T innerBuilder) {
			this.headerRank = headerRank;
			this.innerBuilder = innerBuilder;
		}

		@Override
		List<String> buildLines() {
			final String prefix = getPrefix();

			List<String> content = innerBuilder.buildLines()
				.stream()
				.map(line -> prefix + line)
				.collect(Collectors.toList());

			// Insert a new line before each header
			for (int i = 0; i < content.size() + 1; i += 2) {
				content.add(i, "");
			}

			return content;
		}

		public String getPrefix() {
			return String.join("", Collections.nCopies(headerRank, PREFIX_CHARACTER)) + " ";
		}
	}

	static class Header1Instruction<T extends AbstractMarkdownBuilder<T>> extends BaseHeaderInstruction<T> {
		Header1Instruction(T content) {
			super(1, content);
		}
	}

	static class Header2Instruction<T extends AbstractMarkdownBuilder<T>> extends BaseHeaderInstruction<T> {
		Header2Instruction(T content) {
			super(2, content);
		}
	}

	static class Header3Instruction<T extends AbstractMarkdownBuilder<T>> extends BaseHeaderInstruction<T> {
		Header3Instruction(T content) {
			super(3, content);
		}
	}

	abstract static class FencedInstruction<T extends AbstractMarkdownBuilder<T>> extends Instruction<T> {
		protected final String startTag;
		protected final String endTag;
		protected final T innerBuilder;

		protected FencedInstruction(String startTag, String endTag, T innerBuilder) {
			this.startTag = startTag;
			this.endTag = endTag;
			this.innerBuilder = innerBuilder;
		}

		@Override
		List<String> buildLines() {
			List<String> content = innerBuilder.buildLines();
			List<String> result = new ArrayList<>();
			result.addAll(Arrays.asList("", startTag));
			result.addAll(content);
			result.addAll(Arrays.asList(endTag, ""));
			return result;
		}
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.csp.core.bugtracker.spi;

import org.squashtest.csp.core.bugtracker.core.BugTrackerRemoteException;
import org.squashtest.csp.core.bugtracker.core.ProjectNotFoundException;
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedIssue;
import org.squashtest.tm.bugtracker.advanceddomain.AdvancedProject;
import org.squashtest.tm.bugtracker.advanceddomain.DelegateCommand;
import org.squashtest.tm.bugtracker.advanceddomain.FieldValue;
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueFindContext;
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueSearchForm;
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueSearchRequest;
import org.squashtest.tm.bugtracker.definition.Attachment;
import org.squashtest.tm.bugtracker.definition.RemoteIssue;
import org.squashtest.tm.bugtracker.definition.context.BugTrackerBindingInfo;
import org.squashtest.tm.bugtracker.definition.context.RemoteIssueContext;

import java.net.URL;
import java.util.List;
import java.util.Optional;

public interface AdvancedBugTrackerConnector extends BugtrackerConnectorBase {

	/**
	 * @param issueId remote issue identifier
	 * @return the URL where one can browse the issue.
	 */
	URL makeViewIssueUrl(String issueId);

	/**
	 * @param projectName remote project name
	 * @return a project, given its name, with metadata such as which versions or categories are defined in there.
	 * @throws ProjectNotFoundException remote project could not be found
	 * @throws BugTrackerRemoteException generic remote exception
	 */
	AdvancedProject findProject(String projectName) throws ProjectNotFoundException, BugTrackerRemoteException;

	/**
	 * @see #findProject(String), except that one uses the Id.
	 * @throws ProjectNotFoundException remote project could not be found
	 * @throws BugTrackerRemoteException generic remote exception
	 */
	AdvancedProject findProjectById(String projectId) throws ProjectNotFoundException, BugTrackerRemoteException;

	/**
	 * Must create an issue on the remote bugtracker, then return it
	 *
	 * @param issue Squash representation of the issue to create
	 * @return the 'persisted' issue of it (ie, having its id)
	 * @throws BugTrackerRemoteException generic remote exception
	 */
	AdvancedIssue createIssue(RemoteIssue issue) throws BugTrackerRemoteException;

	/**
	 * Must return ready-to-fill issue, ie with empty fields and its project configured with as many metadata as
	 * possible related to issue creation.
	 *
	 * The default implementation falls back to legacy createReportIssueTemplate(projectName).
	 *
	 * @param projectName the remote project name
	 * @return report template
	 */
	default RemoteIssue createReportIssueTemplate(String projectName, RemoteIssueContext context) {
		return createReportIssueTemplate(projectName);
	}

	/**
	 * Retrieve a remote known issue by remote key
	 */
	AdvancedIssue findIssue(String key);

	/**
	 * Retrieve many remote known issues by key.
	 * In version 3.0.0, findKnownIssues(issueKeyList, context) was added for connectors that need extra information to do
	 * the job.
	 *
	 * Note : for performance reasons, the returned issues may not be "full-formed" issues in the sense that some data
	 * may be missing such as the possible field schemes.
	 *
	 * @param issueKeyList remote issue keys
	 * @return list of non-null matching issues
	 */
	List<AdvancedIssue> findIssues(List<String> issueKeyList);

	/**
	 * Retrieve known issues providing additional context that could be needed to generate a known issue.
	 * @param issueKeyList
	 * @param context
	 * @return known issues filled with the relevant data
	 */
	default List<AdvancedIssue> findKnownIssues(List<String> issueKeyList, RemoteIssueFindContext context) {
		return findIssues(issueKeyList);
	}

	/**
	 * @return the search form to display when searching for existing issues
	 */
	default RemoteIssueSearchForm createIssueSearchForm(BugTrackerBindingInfo bugTrackerBindingInfo) {
		return RemoteIssueSearchForm.defaultForm();
	}

	/**
	 * Performs a bugtracker-specific search.
	 *
	 * It is designed for simple text-based search with one to many fields and it only returns one element.
	 * The search terms IDs should match the RemoteIssueSearchForm that was returned by createIssueSearchForm().
	 *
	 * The implementation should return a "full" AdvancedIssue because we'll want to display as much info as possible.
	 * It should at least have the field values set and the scheme used (whose values should be present in the scheme
	 * map).
	 *
	 * The default implementation (for legacy code) falls back to findIssue(), expecting a single search term to be
	 * provided.
	 *
	 * @param searchRequest the search terms
	 * @return matching issue if any, wrapped in optional
	 */
	default Optional<AdvancedIssue> searchIssue(RemoteIssueSearchRequest searchRequest) {
		String issueKey = searchRequest.getSearchTerms().stream()
			.findFirst()
			.orElseThrow(() -> new IllegalArgumentException(
				"You've reached the legacy fallback implementation for AdvancedBugTrackerConnector.searchIssue" +
					" but you haven't provided any search terms."))
			.getStringValue();

		return Optional.ofNullable(findIssue(issueKey));
	}

	/**
	 * Post the given attachments to the issue identified by remoteIssueKey
	 *
	 * @param remoteIssueKey concerned remote issue key
	 * @param attachments data to upload
	 */
	void forwardAttachments(String remoteIssueKey, List<Attachment> attachments);

	/**
	 * Executes a delegate command and may return a result. The resulting object must be string-serializable, as it will
	 * be jsonified and brought to the Squash UI.
	 *
	 * Note : the return type is free but {@link FieldValue} is preferred when applicable
	 *
	 * @param command to perform
	 * @return serializable command result
	 */
	Object executeDelegateCommand(DelegateCommand command);

	/**
	 * Create Issue links of type Blocks between the issue identified by remoteIssueKey and each issue identified by key of remoteReqIds
	 * @param remoteIssueKey
	 * @param remoteReqIds
	 */
	void linkIssues(String remoteIssueKey, List<String> remoteReqIds);


	/* Deprecated methods */

	/**
	 * Must return ready-to-fill issue, ie with empty fields and its project configured with as many metadata as
	 * possible related to issue creation.
	 *
	 * Deprecated from 3.0. Newer code should implement createReportIssueTemplate(projectName, context).
	 *
	 * @param projectName the remote project name
	 * @return report template
	 */
	@Deprecated default RemoteIssue createReportIssueTemplate(String projectName) { throw new UnsupportedOperationException("Deprecated"); }
}

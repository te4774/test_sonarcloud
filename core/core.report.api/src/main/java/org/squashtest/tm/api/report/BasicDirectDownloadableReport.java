/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.report;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;
import org.springframework.web.servlet.ModelAndView;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.api.report.form.Input;
import org.squashtest.tm.core.foundation.i18n.Labelled;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

public abstract class BasicDirectDownloadableReport extends Labelled implements Report, InitializingBean {

	private StandardReportCategory category = StandardReportCategory.VARIOUS;
	private StandardReportType type = StandardReportType.GENERIC;

	private String descriptionKey;
	private Input[] form = {};

	/**
	 * @see org.squashtest.tm.api.report.Report#getCategory()
	 */
	@Override
	public StandardReportCategory getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(StandardReportCategory category) {
		this.category = category;
	}

	/**
	 * @see org.squashtest.tm.api.report.Report#getType()
	 */
	@Override
	public StandardReportType getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(StandardReportType type) {
		this.type = type;
	}

	/**
	 * @see org.squashtest.tm.api.report.Report#getDescriptionKey()
	 */
	@Override
	public String getDescriptionKey() {
		return descriptionKey;
	}

	/**
	 * @param descriptionKey
	 *            the descriptionKey to set
	 */
	public void setDescriptionKey(String descriptionKey) {
		this.descriptionKey = descriptionKey;
	}


	/**
	 * @see org.squashtest.tm.api.report.Report#getDescription()
	 */
	@Override
	public String getDescription() {
		return getMessage(descriptionKey);
	}

	/**
	 * @param form
	 *            the form to set
	 */
	public void setForm(Input[] form) {
		if(form == null) {
			this.form = null;
		} else {
			this.form = Arrays.copyOf(form, form.length);
		}
	}

	/**
	 * @see org.squashtest.tm.api.report.Report#getForm()
	 */
	@Override
	public Input[] getForm() {
		return form;
	}


	/**
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(category, "Category property should not be null");

		Assert.notNull(type, "Type property should not be null");

		Assert.notNull(form, "Form property should not be null");

		Assert.notNull(descriptionKey, "descriptionKey property should not be null");

		Assert.notNull(getLabelKey(), "labelKey property should not be null");
	}

	@Override
	public ReportView[] getViews() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ModelAndView buildModelAndView(int viewIndex, String format, Map<String, Criteria> criteria) {
		throw new UnsupportedOperationException();
	}

	@Override
	public abstract File generateReport(Map<String, Criteria> crit);
}

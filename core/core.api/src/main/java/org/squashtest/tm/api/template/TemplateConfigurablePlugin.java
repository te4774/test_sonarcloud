/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.api.template;

import org.squashtest.tm.api.plugin.ConfigurablePlugin;
import org.squashtest.tm.api.plugin.EntityReference;

/**
 * Interface for plugins that accept a configuration on a project template binding. This configuration will be stored,
 * just as for standard ConfigurablePlugins, in database via the LibraryPluginBinding entity.
 * TemplateConfigurablePlugin also allow to synchronise configurations from a template source to a project target.
 * What 'synchronisation' means really depends on the plugin.
 */
public interface TemplateConfigurablePlugin extends ConfigurablePlugin {

    /**
     * Must return the URI path that lead to a configuration page for this plugin to use when dealing with project
     * templates.
     *
     * @param context a ProjectTemplate entity reference
     * @return a path (URL segment) for this plugin's configuration page
     * @see ConfigurablePlugin#getConfigurationPath(EntityReference)
     */
    String getTemplateConfigurationPath(EntityReference context);

    /**
     * Perform a synchronisation of the configuration from template to project.
     *
     * @param templateId ID for the source template
     * @param targetProjectId ID for the project that will be synced
     */
    void synchroniseTemplateConfiguration(Long templateId, Long targetProjectId);

}

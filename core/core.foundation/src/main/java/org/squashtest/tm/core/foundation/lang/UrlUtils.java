/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.core.foundation.lang;

import org.squashtest.tm.core.foundation.exception.InvalidUrlException;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Objects;

/**
 * @author Gregory Fouquet
 *
 */
public final class UrlUtils {

	private UrlUtils() {
		super();
	}

	/**
	 * Coerces the given string into a URL. When it's not coercible, throws a {@link InvalidUrlException}
	 *
	 * @param url url
	 * @return URL
	 * @throws InvalidUrlException InvalidUrlException
	 */
	public static URL toUrl(String url) throws InvalidUrlException {
		try {
			return new URL(url);
		} catch (MalformedURLException e) {
			throw new InvalidUrlException(e);
		}
	}

	/**
	 * Coerces the given string into a URL or a null if the input is null.
	 * @see UrlUtils#toUrl(String)
	 *
	 * @param url a string to be coerced into an url
	 * @return URL or null
	 * @throws InvalidUrlException InvalidUrlException
	 */
	public static URL toUrlOrNull(String url) throws InvalidUrlException {
		return url == null ? null : UrlUtils.toUrl(url);
	}

	/**
	 * Construct a URL given a base path and a relative path.
	 * Introduced in 4.0.0 for SquashAUTOM connector plugin.
	 *
	 * @param baseUrl a well-formed absolute URL. The trailing "/" is optional
	 * @param relativePath a relative path from the base URL, such as "toto", "./toto" or "../toto"
	 * @return the resolved URL
	 */
	public static URL appendPath(String baseUrl, String relativePath) {
		Objects.requireNonNull(baseUrl, "baseUrl should not be null.");
		Objects.requireNonNull(relativePath, "relativePath should not be null.");

		if (!baseUrl.endsWith("/")) {
			baseUrl = baseUrl + "/";
		}

		final URI uri = URI.create(baseUrl);

		try {
			return uri.resolve(relativePath)
				.normalize()
				.toURL();
		} catch (MalformedURLException malformedURLException) {
			final String errorMessage = String.format("Cannot build URL from base '%s' and relative path '%s'.",
				baseUrl, relativePath);
			throw new IllegalArgumentException(errorMessage, malformedURLException);
		}
	}
}

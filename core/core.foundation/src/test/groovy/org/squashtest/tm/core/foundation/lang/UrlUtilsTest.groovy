/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.core.foundation.lang

import spock.lang.Specification

class UrlUtilsTest extends Specification {
	def "should build URL from base and relative path"() {
		when:
		def builtUrl = UrlUtils.appendPath(baseUrl, relativePath)

		then:
		builtUrl.toExternalForm() == expectedUrl

		where:
		baseUrl 					| relativePath	|  expectedUrl
		"http://127.0.0.1" 			| 'path'		| "http://127.0.0.1/path"
		"http://127.0.0.1" 			| '/path'		| "http://127.0.0.1/path"
		"http://127.0.0.1" 			| './path'		| "http://127.0.0.1/path"
		"http://127.0.0.1" 			| 'path/'		| "http://127.0.0.1/path/"

		"http://127.0.0.1/" 		| 'path'		| "http://127.0.0.1/path"
		"http://127.0.0.1/" 		| '/path'		| "http://127.0.0.1/path"
		"http://127.0.0.1/" 		| './path'		| "http://127.0.0.1/path"

		"http://127.0.0.1/toto" 	| './path'		| "http://127.0.0.1/toto/path"
		"http://127.0.0.1/toto" 	| '../path'		| "http://127.0.0.1/path"

		"http://127.0.0.1/toto" 	| '/path'		| "http://127.0.0.1/path"
		"http://127.0.0.1/toto/" 	| '/path'		| "http://127.0.0.1/path"
	}
}

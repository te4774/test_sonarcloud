/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.internal.service;

import org.squashtest.tm.domain.NamedReference;

import java.util.List;

public interface UserAccessService {
	/**
	 * Return a list of users that have any permission on any of the provided projects or have admin role. Users
	 * that belong to a team with permissions are also included. Duplicate users are removed.
	 * This method is notably used in the campaign assistant plugin.
	 *
	 * @param projectIds the projects to check access to
	 * @return a list of users as named references
	 */
	List<NamedReference> getUsersWhoCanAccessProjects(List<Long> projectIds);
}

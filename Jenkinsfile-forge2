
/**
    Requires the following Jenkins plugins :
        - Mercurial
        - Maven Release
        - Pipeline Maven Integration
        - Sonarqube
        - Badge
        - Utility Steps

*/


pipeline {

	agent {
		kubernetes {
			inheritFrom 'maven-builder-jdk8'
			workspaceVolume persistentVolumeClaimWorkspaceVolume(claimName: 'squashtest-tm-staging-pipeline-pvc', readOnly: false)
			yamlFile 'pipeline-v2/stages/main-build/pod-template.yaml'
			yamlMergeStrategy merge()
			defaultContainer 'maven'
		}
	}

	environment {
		JAVA_TOOL_OPTIONS = "-Duser.timezone=Europe/Paris"
		// this locates the maven local repository within the persistent volume, allowing use to persist the maven cache
		// and save build time
		MAVEN_OPTS = "-Xmx1024m -Dmaven.repo.local=/home/jenkins/agent/.m2"
	}

	// parameters
	parameters{
		choice(name : 'build_type', description : 'Choose between regular or release build', choices: "regular\nrelease")
		string(name : "release_version", description: "the release version (if release)", defaultValue : "")
		string(name : "next_version", description: "the next development version (if release)", defaultValue : "")
	}


	stages {

		stage('Main Build'){

			environment{
				NODE_OPTIONS="--max_old_space_size=6144"
			}

			steps {
				script {
					def mainbuild = load "${workspace}/pipeline-v2/stages/main-build/steps.groovy"
					mainbuild()
				}

			}
			post {
				always {
					junit "**/karma-junit-results/**/*.xml"
					junit allowEmptyResults: true, testResults: '**/target/surefire-reports/*.xml'
				}
			}
		}
	}
}

